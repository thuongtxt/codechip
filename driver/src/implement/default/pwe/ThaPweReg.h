/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : ThaPweReg.h
 * 
 * Created Date: Nov 20, 2012
 *
 * Description : Pseudowire Encapsulation registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPWEREG_H_
#define _THAPWEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*--------------------------- PWE ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: PWENC PW Header Control
Reg Addr: 0x300400-0x3007FF
          The address format for these registers is 0x300400 + pwid
          Where: pwid (0 � 1023) Pseudowire Identification
Reg Desc: These registers are used to configure Pseudowire header
------------------------------------------------------------------------------*/
#define cThaRegPWEncPWHdrCtrl        (0x300400)

/*--------------------------------------
BitField Name: PWEncCEPEBMEnCtrl
BitField Type: R/W
BitField Desc: Only for CEP PW. EBM field of Control Word is absent or not.
               - 1: enable
               - 0: disable
BitField Bits: 11
--------------------------------------*/
#define cThaPWEncCEPEPAREnCtrlMask                      cBit12
#define cThaPWEncCEPEPAREnCtrlShift                     12

/*--------------------------------------
BitField Name: PWEncCEPEBMEnCtrl
BitField Type: R/W
BitField Desc: Only for CEP PW. EBM field of Control Word is absent or not.
               - 1: enable
               - 0: disable
BitField Bits: 11
--------------------------------------*/
#define cThaPWEncCEPEBmEnCtrlMask                      cBit11
#define cThaPWEncCEPEBmEnCtrlShift                     11

/*--------------------------------------
BitField Name: PWEncRbitEngEnCtrl
BitField Type: R/W
BitField Desc: R bit from engine enable
               - 1: enable
               - 0: disable
BitField Bits: 8
--------------------------------------*/
#define cThaPWEncRbitEngEnCtrlMask                     cBit8
#define cThaPWEncRbitEngEnCtrlShift                    8

/*--------------------------------------
BitField Name: PWEncLbitEngEnCtrl
BitField Type: R/W
BitField Desc: L bit from engine enable
               - 1: enable
               - 0: disable
BitField Bits: 7
--------------------------------------*/
#define cThaPWEncLbitEngEnCtrlMask                     cBit7
#define cThaPWEncLbitEngEnCtrlShift                    7

/*--------------------------------------
BitField Name: PWEncMbitValueCtrl
BitField Type: R/W
BitField Desc: M bit value from CPU
BitField Bits: 6_5
--------------------------------------*/
#define cThaPWEncMbitValCtrlMask                       cBit6_5
#define cThaPWEncMbitValCtrlShift                      5

/*--------------------------------------
BitField Name: PWEncRbitValueCtrl
BitField Type: R/W
BitField Desc: R bit value from CPU
BitField Bits: 4
--------------------------------------*/
#define cThaPWEncRbitValCtrlMask                       cBit4
#define cThaPWEncRbitValCtrlShift                      4

/*--------------------------------------
BitField Name: PWEncLbitValueCtrl
BitField Type: R/W
BitField Desc: L bit value from CPU
BitField Bits: 3
--------------------------------------*/
#define cThaPWEncLbitValCtrlMask                       cBit3
#define cThaPWEncLbitValCtrlShift                      3

/*--------------------------------------
BitField Name: PWEncPwHdrCtrl
BitField Type: R/W
BitField Desc: configure PW header field
               - 0: disable
               - 1: only Control Word
               - 2: Control Word and RTP
               - 3: RTP and Control Word
BitField Bits: 2_1
--------------------------------------*/
#define cThaPWEncPwHdrCtrlMask                         cBit2_1
#define cThaPWEncPwHdrCtrlShift                        1

/*--------------------------------------
BitField Name: PWEncSuprEnCtrl
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 0
--------------------------------------*/
#define cThaPWEncSuprEnCtrlMask                        cBit0
#define cThaPWEncSuprEnCtrlShift                       0

/*------------------------------------------------------------------------------
Reg Name: PWENC RTP 9 Bits Header Control
Reg Addr: 0x300000
Reg Desc: These registers are used to configure the first 9 bits of RTP field
------------------------------------------------------------------------------*/
#define cThaRegPWEncRtp9BitsHdrCtrl                 (0x300000)

#define cThaPwRtpFirst9Bits                         0x100 /* V=2, P=0, X=0, CC=0, M=0 */

/*------------------------------------------------------------------------------
Reg Name: PWENC Tx PW packets Counters
Reg Addr: 0x308200 � 0x30834F (R_O), 0x308000 � 0x30814F(R2C)
Reg Desc: The register provides counter for number of the transmitted packet
------------------------------------------------------------------------------*/
#define cThaRegPwEncTxPktCnt(r2c)               ((r2c) ? 0x308000 : 0x308200)

/*------------------------------------------------------------------------------
Reg Name: PWENC Tx PW bytes Counters
Reg Addr: 0x301600 � 0x30174F (R_O), 0x301400 � 0x30154F(R2C)
Reg Desc: The register provides counter for number of the transmitted bytes
------------------------------------------------------------------------------*/
#define cThaRegPwEncTxByteCnt(r2c)             ((r2c) ? 0x301400 : 0x301600)

/*------------------------------------------------------------------------------
Reg Name: PWENC LBit Counters
Reg Addr: 0x304200 � 0x30434F (R_O), 0x304000 � 0x30414F(R2C)
Reg Desc: The register provides counter for number of the transmitted packet with L bit
------------------------------------------------------------------------------*/
#define cThaRegPwEncLBitPktCnt(r2c)                 ((r2c) ? 0x304000 : 0x304200)

/*------------------------------------------------------------------------------
Reg Name: PWENC RBit Counters
Reg Addr: 0x306200 � 0x30634F (R_O), 0x306000 � 0x30614F(R2C)
Reg Desc: The register provides counter for number of the transmitted packet with R bit
------------------------------------------------------------------------------*/
#define cThaRegPwEncRBitPktCnt(r2c)                 ((r2c) ? 0x306000 : 0x306200)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PWENC MBit Counters
Reg Addr: 0x30A200 – 0x30A34F (R_O), 0x30A000 – 0x30A14F(R2C)
Reg Desc: The register provides counter for number of the transmitted bytes
------------------------------------------------------------------------------*/
#define cThaRegPwEncTxMBitCnt(r2c)     ((r2c) ? 0x30A000 : 0x30A200)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PWENC PBit Counters
Reg Addr: 0x30C200 – 0x30C34F (R_O), 0x30C000 – 0x30C14F(R2C)
Reg Desc: The register provides counter for number of the transmitted bytes
------------------------------------------------------------------------------*/
#define cThaRegPwEncTxPBitCnt(r2c)              ((r2c) ? 0x30C000 : 0x30C200)

/*--------------------------- PLA ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: PWPDH Payload Assemble Engine Status
Reg Addr: 0x2C2400-0x2C27FF
          The address format for these registers is 0x2C2400 + pwid
          Where: pwid (0 � 1023) Pseudowire identification
Reg Desc: These registers are used to save status in the engine.
------------------------------------------------------------------------------*/
#define cThaRegPWPdhPlaEngStat       (0x2C2400)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Adding Protocol To Prevent Burst
Reg Addr: 0x2C0003
Reg Desc: This register is to add a pseudo-wire that does not cause bottle-neck
          at transmit PW cause ACR performance bad at receive side
------------------------------------------------------------------------------*/
#define cThaRegPDHPWAddingProtocolToPreventBurst          0x2C0003

#define cThaAddPwIdMask                                   cBit12_4
#define cThaAddPwIdShift                                  4

#define cThaAddPwStateMask                                cBit3_0
#define cThaAddPwStateShift                               0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW  Payload Assemble CEP Fractional EBM Control
Reg Addr: 0x2C1000-0x2C100B
The address format for these registers is 0x2C1000 + stsid
Where: stsid (0 � 11) STS Identification0x301800
Reg Desc: These registers are used to configure EBM bit mask of each STS channel in the CEP fractional mode
------------------------------------------------------------------------------*/
#define cThaRegThalassaPdhPWPldAssembleCEPFractionalEBmCtrl 0x2C1000UL

/*--------------------------------------
BitField Name: PDHPWPlaAsmCEPEBMEmptyCtrl
BitField Type: R/W
BitField Desc:
BitField Bits: 44
--------------------------------------*/
#define cThaPdhPWPlaAsmCEPEBmEmptyCtrlMask            cBit12
#define cThaPdhPWPlaAsmCEPEBmEmptyCtrlShift           12

/*--------------------------------------
BitField Name: PDHPWPlaAsmCEPEBMTug3EnCtrl
BitField Type: R/W
BitField Desc:
BitField Bits: 42
--------------------------------------*/
#define cThaPdhPWPlaAsmCEPEBmTug3EnCtrlMask            cBit10
#define cThaPdhPWPlaAsmCEPEBmTug3EnCtrlShift           10

/*--------------------------------------
BitField Name: PDHPWPlaAsmCEPEBMVtg7TypeCtrl
BitField Type: R/W
BitField Desc: VT type for VT group#7 other: reserve
               - 2'b00: VT1.5
               - 2'b01: VT2
BitField Bits: 41_40
--------------------------------------*/
#define cThaPdhPWPlaAsmCEPEBmVtg7TypeCtrlMask          cBit9_8
#define cThaPdhPWPlaAsmCEPEBmVtg7TypeCtrlShift         8

/*--------------------------------------
BitField Name: PDHPWPlaAsmCEPEBMVtg7DatCtrl
BitField Type: R/W
BitField Desc: 4 bits mask for VT group#7 All 4 bits are used to indicate
               whether VT1.5 tributaries are carried within the VTG. The first
               3 bits read from right to left are used to indicate whether VT2
               tributaries are carried within the VTG. The first 2 bits are
               used to indicate whether VT3 tributaries are carried within the
               VTG. The rightmost bit is used to indicate whether a VT6 us
               carried within the VTG. The VTs within the VTG are numbered from
               right to left, starting from the first VT as the first bit on
               the right.
BitField Bits: 39_36
--------------------------------------*/
#define cThaPdhPWPlaAsmCEPEBmVtg7DatCtrlMask(tu)       (cBit4 << (tu))
#define cThaPdhPWPlaAsmCEPEBmVtg7DatCtrlShift(tu)      (4 + (tu))

/*--------------------------------------
BitField Name: PDHPWPlaAsmCEPEBMVtg6TypeCtrl
BitField Type: R/W
BitField Desc: VT type for VT group#6 other: reserve
               - 2'b00: VT1.5
               - 2'b01: VT2
BitField Bits: 35_34
--------------------------------------*/
#define cThaPdhPWPlaAsmCEPEBmVtg6TypeCtrlMask          cBit3_2
#define cThaPdhPWPlaAsmCEPEBmVtg6TypeCtrlShift         2

/*--------------------------------------
BitField Name: PDHPWPlaAsmCEPEBMVtg6DatCtrl
BitField Type: R/W
BitField Desc: 4 bits mask for VT group#6 All 4 bits are used to indicate
               whether VT1.5 tributaries are carried within the VTG. The first
               3 bits read from right to left are used to indicate whether VT2
               tributaries are carried within the VTG. The first 2 bits are
               used to indicate whether VT3 tributaries are carried within the
               VTG. The rightmost bit is used to indicate whether a VT6 us
               carried within the VTG. The VTs within the VTG are numbered from
               right to left, starting from the first VT as the first bit on
               the right.
BitField Bits: 33_30
--------------------------------------*/
#define cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwHeadMask(tu)  (cBit0 << ((tu) % 2))
#define cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwHeadShift(tu) (0 + ((tu) % 2))
#define cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwTailMask(tu)  (cBit30 << (tu))
#define cThaPdhPWPlaAsmCEPEBmVtg6DatCtrlHwTailShift(tu) (30 + (tu))

#define cThaPdhPWPlaAsmCEPEBmVtgDatCtrlMask(tug, tu)      (cBit0 << ((tug) * 6 + tu))
#define cThaPdhPWPlaAsmCEPEBmVtgDatCtrlShift(tug, tu)     (tug * 6 + tu)

#define cThaPdhPWPlaAsmCEPEBmVtgTypeCtrlMask(tug)     (cBit1_0 << ((tug) * 6 + 4))
#define cThaPdhPWPlaAsmCEPEBmVtgTypeCtrlShift(tug)    ((tug) * 6 + 4)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW  Payload Assemble CEP  Fractional J1 Return Control
Address: 0x2C1200-0x2C120B
The address format for these registers is 0x2C120B + stsid
Where: stsid (0 � 11) STS Identification
Description: These registers are used to configure number of byte data of only a
selected set of  VTs within a VC-3 (fractional VC-3 CEP) or an VC-4 (fractional VC-4 CEP)
which J1 byte will be return. The value of the register strictly depends on value of the
Thalassa PDHPW  Payload Assemble CEP Fractional EBM Control  register.
------------------------------------------------------------------------------*/
#define cThaPdhPWPlaCepFractionalJ1ReturnControl 0x2C1200UL

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Payload Assemble CEP Fractional EBM  New Enable Control
Address : 0x2C1400-0x2C140B
          The address format for these registers is 0x2C140B + stsid
          Where: stsid (0 � 11) STS Identification
Reg desc: These registers are used to configure to enable a new EBM value in the
          Thalassa PDHPW  Payload Assemble CEP Fractional EBM Control  register
------------------------------------------------------------------------------*/
#define cThaPdhPwPlaCepFractionalNewEbmEnable 0x2C1400

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Ethernet PAD Control
Reg Name: 0x302000
Where:
Reg Desc: This register configures mode to insert number of PAD bytes of AF6FH1
transmit Ethernet
 ------------------------------------------------------------------------------*/
#define cThaRegPwTransmitEthPadControl 0x302000

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Payload Assemble TOH Mapping Control
Address : 0x2E0020-0x2E0037
          For OC-3
          The address format for these registers is 0x2E0020 + stsid*4 + lineid
          Where: stsid (0 � 2) sts identificaition per OC3
                 lineid (0 � 3) line identificaition
          For OC-12
          The address format for these registers is 0x2E0020 + stsid*2 + oc12id
          Where: stsid (0 � 11) sts identificaition
                 oc12id (0 � 1) oc12 identification

Reg desc:  These registers are used to indicate which byte of TOH transported through PW
------------------------------------------------------------------------------*/
#define cThaPdhPwPlaTohMappingControl 0x2E0020

#define cThaPdhPWPlaAsmTOHPWIDMask  cBit17_9
#define cThaPdhPWPlaAsmTOHPWIDShift 9
#define cThaPdhPWPlaAsmTOHPWIDDw    1

#define cThaPdhPWPlaAsmTOHFirstByteEnMask  cBit8
#define cThaPdhPWPlaAsmTOHFirstByteEnShift 8
#define cThaPdhPWPlaAsmTOHFirstByteEnDw    1

#define cThaPdhPWPlaAsmTOHFirstByteRowMask  cBit7_4
#define cThaPdhPWPlaAsmTOHFirstByteRowShift 4
#define cThaPdhPWPlaAsmTOHFirstByteRowDw    1

#define cThaPdhPWPlaAsmTOHFirstByteColMask  cBit3_2
#define cThaPdhPWPlaAsmTOHFirstByteColShift 2
#define cThaPdhPWPlaAsmTOHFirstByteColDw    1

#define cThaPdhPWPlaAsmTOHLastByteEnMask  cBit1
#define cThaPdhPWPlaAsmTOHLastByteEnShift 1
#define cThaPdhPWPlaAsmTOHLastByteEnDw    1

#define cThaPdhPWPlaAsmTOHLastByteRowHeadMask  cBit0
#define cThaPdhPWPlaAsmTOHLastByteRowHeadShift 0
#define cThaPdhPWPlaAsmTOHLastByteRowHeadDw    1

#define cThaPdhPWPlaAsmTOHLastByteRowTailMask  cBit31_29
#define cThaPdhPWPlaAsmTOHLastByteRowTailShift 29
#define cThaPdhPWPlaAsmTOHLastByteRowTailDw    0

#define cThaPdhPWPlaAsmTOHLastByteColMask  cBit28_27
#define cThaPdhPWPlaAsmTOHLastByteColShift 27
#define cThaPdhPWPlaAsmTOHLastByteColDw    0

#define cThaPdhPWPlaAsmTOHByteEnCtrlMask(row, col)  (cBit0 << (((row) * 3) + (col)))
#define cThaPdhPWPlaAsmTOHByteEnCtrlShift(row, col) (((row) * 3) + (col))
#define cThaPdhPWPlaAsmTOHByteEnCtrlDw    0
#define cThaPdhPWPlaAsmTOHByteBitmapMask  cBit26_0
#define cThaPdhPWPlaAsmTOHByteBitmapShift 0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Payload Assemble TOH Line Enable Control
Address : 0x2E0000
Reg desc:  This register are used to enable line bus
------------------------------------------------------------------------------*/
#define cThaPdhPwPlaTohLineEnableControl 0x2E0000
#define cThaPdhPwPlaTohLineEnableMask(lineId) (cBit0 << (lineId))
#define cThaPdhPwPlaTohLineEnableShift(lineId) (lineId)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Payload Assemble TOH  Physical Line Convert  Control
Reg Addr: 0x2E0100
Reg Desc: These registers are used to convert physical OCN line to logical OCN
          line which the TOH Assembling engine uses to process
------------------------------------------------------------------------------*/
#define cThaRegThalassaPdhPWPldAssembleTohPhyLineConvCtrl 0x2E0100

/*--------------------------------------
BitField Name: PDHPWPlaAsmTOHLineEnCtrl
BitField Type: R/W
BitField Desc: Physical OCN line enable
BitField Bits: 2 << (lineId * 4)
--------------------------------------*/
#define cThaPDHPWPlaAsmTOHLineEnCtrlMask(lineid)  (cBit2 << (lineid * 4))
#define cThaPDHPWPlaAsmTOHLineEnCtrlShift(lineid) (2 + (lineid * 4))

/*--------------------------------------
BitField Name: PDHPWPlaAsmTOHLogicLineCtrl
BitField Type: R/W
BitField Desc: Logical OCN line
BitField Bits: 1_0 << (lineId * 4)
--------------------------------------*/
#define cThaPDHPWPlaAsmTOHLogicLineCtrlMask(lineid)  (cBit1_0 << (lineid * 4))
#define cThaPDHPWPlaAsmTOHLogicLineCtrlShift(lineid) (lineid * 4)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDHPW Payload Assemble TOH  Payload Size Control
Reg Addr: 0x2E0040-0x2E0043
          The address format for these registers is 0x2E0040 + lineid
          Where: lineid (0 - 3) line identificaition

Reg Desc: These registers are used to indicate which number of STM-1/4 TOH
          frames of a TOH Psedowire packet
------------------------------------------------------------------------------*/
#define cThaRegThalassaPdhPWPldAssembleTohPldSizeCtrl 0x2E0040

/*--------------------------------------
BitField Name: PDHPWPlaAsmTOHPldSizeCtrl
BitField Type: R/W
BitField Desc: number of STM-1/4 TOH frames of a TOH Psedowire packet, value
               zero for 1 frame, value 1 for 2 frames, etc..
BitField Bits: 4_0
--------------------------------------*/
#define cThaPdhPWPlaAsmTohPldSizeCtrlMask  cBit4_0
#define cThaPdhPWPlaAsmTohPldSizeCtrlShift 0

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Transmit Ethernet PAD Control
Reg Name: 0x302000
Where:
Reg Desc: This register configures mode to insert number of PAD bytes of AF6FH1
transmit Ethernet
 ------------------------------------------------------------------------------*/
#define cThaRegPwTransmitEthPadControl 0x302000

#define cThaRegPwTxEthPadModeMask  cBit1_0
#define cThaRegPwTxEthPadModeShift 0

#ifdef __cplusplus
}
#endif
#endif /* _THAPWEREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE internal module
 * 
 * File        : ThaModulePweInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPWEINTERNAL_H_
#define _THAMODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"

#include "../man/ThaDeviceInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../cos/ThaModuleCosInternal.h"
#include "../cla/pw/ThaModuleClaPwInternal.h"
#include "../pw/ThaPwReg.h"
#include "../util/ThaUtil.h"

#include "ThaModulePwe.h"
#include "ThaPweReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mFieldMask(module, field)                                              \
        mMethodsGet(module)->field##Mask(module)
#define mFieldShift(module, field)                                             \
        mMethodsGet(module)->field##Shift(module)

#define mRo(clear) ((clear) ? 0 : 1)
#define mPwPweOffset(self, pw) mMethodsGet((ThaModulePwe)self)->PwPweDefaultOffset((ThaModulePwe)self, pw)
#define mPwPlaOffset(self, pw) mMethodsGet((ThaModulePwe)self)->PwPlaDefaultOffset((ThaModulePwe)self, pw)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePweMethods
    {
    /* To control hardware registers */
    uint32 (*BaseAddress)(ThaModulePwe self);
    uint32 (*PlaBaseAddress)(ThaModulePwe self);
    eAtRet (*DefaultSet)(ThaModulePwe self);
    uint32 (*PwPweDefaultOffset)(ThaModulePwe self, AtPw pw);
    uint32 (*PwPlaDefaultOffset)(ThaModulePwe self, AtPw pw);
    
    /* For PW adding/removing process */
    eAtRet (*PwAddingPrepare)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwAddingStart)(ThaModulePwe self, AtPw pw);
    eBool  (*PwRemovingShouldStart)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwRemovingStart)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwRemovingFinish)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwRamStatusClear)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwRemovingPrepare)(ThaModulePwe self, AtPw pw);
    
    /* PW properties */
    eAtRet (*PwEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwIsEnabled)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwTypeSet)(ThaModulePwe self, AtPw pw, eAtPwType pwType);
    eAtRet (*PwPsnTypeSet)(ThaModulePwe self, AtPw pw, AtPwPsn psn);
    eAtRet (*PwSupressEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwSupressIsEnabled)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwStmPortSet)(ThaModulePwe self, AtPw pwAdapter, uint8 lineId);
    eAtRet (*PwJitterAttenuatorEnable)(ThaModulePwe self, AtPw pwAdapter, eBool enable);
    uint8 (*HwPwType)(ThaModulePwe self, AtPw pwAdapter, eAtPwType pwType);

    /* PW payload */
    eAtRet (*PayloadSizeSet)(ThaModulePwe self, AtPw pw, uint16 payloadSize);
    uint16 (*PayloadSizeGet)(ThaModulePwe self, AtPw pw);
    uint16 (*NumFramesInPacketGet)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwNxDs0Set)(ThaModulePwe self, AtPw pw, AtPdhNxDS0 nxDs0);
    uint8 (*PwNumDs0TimeslotsGet)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwNumDs0TimeslotsSet)(ThaModulePwe self, AtPw pw, uint16 numTimeslots);
    eAtRet (*PwNumFramesInPacketSet)(ThaModulePwe self, AtPw pw, uint16 numFramesInPacket);
    uint16 (*PwCesMaxNumFramesInPacket)(ThaModulePwe self);
    eAtRet (*HdlcPwPayloadTypeSet)(ThaModulePwe self, ThaPwHdlcAdapter adapter, eAtPwHdlcPayloadType payloadType);

    /* Control word */
    eAtRet (*PwRtpEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eAtRet (*PwCwEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwCwIsEnabled)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwCwAutoTxLBitEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwCwAutoTxLBitIsEnabled)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwCwAutoTxRBitEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwCwAutoTxRBitIsEnabled)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwCwAutoTxMBitEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwCwAutoTxMBitIsEnabled)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwCwAutoTxNPBitEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwCwAutoTxNPBitIsEnabled)(ThaModulePwe self, AtPw pw);
    eBool (*PwCwAutoTxNPBitIsConfigurable)(ThaModulePwe self);

    /* Counters */
    uint32 (*PwTxPacketsGet)(ThaModulePwe self, AtPw pw, eBool clear);
    uint32 (*PwTxBytesGet)(ThaModulePwe self, AtPw pw, eBool clear);
    uint32 (*PwTxLbitPacketsGet)(ThaModulePwe self, AtPw pw, eBool clear);
    uint32 (*PwTxRbitPacketsGet)(ThaModulePwe self, AtPw pw, eBool clear);
    uint32 (*PwTxMbitPacketsGet)(ThaModulePwe self, AtPw pw, eBool clear);
    uint32 (*PwTxNbitPacketsGet)(ThaModulePwe self, AtPw pw, eBool clear);
    uint32 (*PwTxPbitPacketsGet)(ThaModulePwe self, AtPw pw, eBool clear);

    /* Alarm/error forcing */
    eAtRet (*PwTxAlarmForce)(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType);
    eAtRet (*PwTxAlarmUnForce)(ThaModulePwe self, AtPw pw, eAtPwAlarmType alarmType);
    uint32 (*PwTxForcedAlarmGet)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwTxLBitForce)(ThaModulePwe self, AtPw pw, eBool force);
    eBool  (*PwTxLBitIsForced)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwTxRBitForce)(ThaModulePwe self, AtPw pw, eBool force);
    eBool  (*PwTxRBitIsForced)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwTxMBitForce)(ThaModulePwe self, AtPw pw, eBool force);
    eBool  (*PwTxMBitIsForced)(ThaModulePwe self, AtPw pw);

    /* PW header */
    eAtRet (*NumVlansSet)(ThaModulePwe self, AtPw pw, uint8 numVlans);
    eAtRet (*HeaderWrite)(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte, AtPwPsn psn);
    eAtRet (*HeaderRead)(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 bufferSize);
    eAtRet (*RawHeaderWrite)(ThaModulePwe self, AtPw pw, uint8 *buffer, uint8 headerLenInByte);

    /* Registers */
    uint32 (*PWPdhPlaPldSizeCtrl)(ThaModulePwe self);
    uint32 (*PwTxEnCtrl)(ThaModulePwe self);
    uint32 (*PWPdhPlaEngStat)(ThaModulePwe self);
    uint32 (*PDHPWAddingProtocolToPreventBurst)(ThaModulePwe self, AtPw pw);
    uint32 (*PDHPWAddingPwMask)(ThaModulePwe self, AtPw pw);
    uint32 (*PDHPWAddingStateMask)(ThaModulePwe self, AtPw pw);
    uint32 (*PDHPWPlaSignalingInverseLookupControl)(ThaModulePwe self);

    /* Bit fields */
    mDefineMaskShiftField(ThaModulePwe, PWPdhPlaAsmPldTypeCtrl)
    mDefineMaskShiftField(ThaModulePwe, PWPdhPlaAsmPldSizeCtrl)

    /* Debug */
    void (*PwDebug)(ThaModulePwe self, AtPw pwAdapter);
    void (*PwRegsShow)(ThaModulePwe self, AtPw pw);
    void (*PwPlaRegsShow)(ThaModulePwe self, AtPw pw);
    void (*PwPweRegsShow)(ThaModulePwe self, AtPw pw);
    void (*ProfileInfoDisplay)(ThaModulePwe self);
    void (*CacheUsageGet)(ThaModulePwe self, tThaPweCache *cache);
    eBool (*CanAccessCacheUsage)(ThaModulePwe self);
    void (*EthFlowRegsShow)(ThaModulePwe self, AtEthFlow flow);

    /* CEP fractional */
    eAtRet (*VcFragmentTypeSet)(ThaModulePwe self, AtSdhChannel sdhVc);
    eAtRet (*PwCepEquip)(ThaModulePwe, AtPw pw, AtChannel subChannel, eBool equip);
    eAtRet (*PwHeaderCepEbmEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    void (*PwCepFractionalEquippedChannelsGet)(ThaModulePwe self, AtPw pw, AtList equippedChannels);

    /* TOH pseudowire */
    eAtRet (*PwTohByteEnable)(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
    eAtRet (*PwTohFirstByteEnable)(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
    eAtRet (*PwTohLastByteEnable)(ThaModulePwe self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
    eAtRet (*PwTohEnable)(ThaModulePwe self, AtPw pseudowire, eBool enable);
    eAtRet (*PwTohPayloadSizeSet)(ThaModulePwe self, AtPw pw, uint32 numFramePerPacket);
    uint16 (*PwTohPayloadSizeGet)(ThaModulePwe self, AtPw pw);
    eBool (*PwTohIsEnabled)(ThaModulePwe self, AtPw pseudowire);
    eAtRet (*BindLineToPw)(ThaModulePwe self, AtSdhLine line, AtPw pw);
    tThaTohByte* (*PwTohFirstByteGet)(ThaModulePwe self, AtPw pw, tThaTohByte *tohByte);
    tThaTohByte* (*PwTohLastByteGet)(ThaModulePwe self, AtPw pw, tThaTohByte *tohByte);
    uint8 (*PwTohBytesBitmapGet)(ThaModulePwe self, AtPw pw, uint32 *bitmaps);
    uint16 (*PwTohMaxNumFrames)(ThaModulePwe self);

    /* Counter reg */
    uint32 (*CounterOffset)(AtPw pw);

    /* PTCH */
    eAtRet (*PwPtchInsertionModeSet)(ThaModulePwe self, AtPw pw, eAtPtchMode insertMode);
    eAtPtchMode (*PwPtchInsertionModeGet)(ThaModulePwe self, AtPw pw);
    eAtRet (*PwPtchServiceEnable)(ThaModulePwe self, AtPw pw, eBool enable);
    eBool (*PwPtchServiceIsEnabled)(ThaModulePwe self, AtPw pw);
    eAtRet (*EthFlowPtchInsertionModeSet)(ThaModulePwe self, AtEthFlow flow, eAtPtchMode insertMode);
    eAtPtchMode (*EthFlowPtchInsertionModeGet)(ThaModulePwe self, AtEthFlow flow);
    eAtRet (*EthFlowPtchServiceEnable)(ThaModulePwe self, AtEthFlow flow, eBool enable);
    eBool (*EthFlowPtchServiceIsEnabled)(ThaModulePwe self, AtEthFlow flow);

    eBool (*TxPwActiveHwIsReady)(ThaModulePwe self);
    eAtRet (*TxPwActiveForce)(ThaModulePwe self, AtPw pwAdapter, eBool enable);
    eBool (*TxPwActiveIsForced)(ThaModulePwe self, AtPw pwAdapter);

    /* Subport VLANs */
    eAtRet (*SubPortVlanBypass)(ThaModulePwe self, eBool enable);
    eBool (*SubPortVlanIsBypassed)(ThaModulePwe self);

    /* Cache blocks */
    uint32 (*MaxNumCacheBlocks)(ThaModulePwe self);
    uint32 (*NumFreeBlocksHwGet)(ThaModulePwe self);

    /* IDLE pattern for trunk condition TDM to PSN: rx tdm got los/lof/ais */
    eAtRet (*PwIdleCodeSet)(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 payloadIdleCode);
    eAtRet (*PwCesopCasIdleCode1Set)(ThaModulePwe self, ThaPwAdapter pwAdapter, uint8 signallingIdleCode);
    uint8  (*PwCesopCasIdleCode1Get)(ThaModulePwe self, ThaPwAdapter pwAdapter);
    eAtRet (*PwIdleCodeInsertionEnable)(ThaModulePwe self, ThaPwAdapter pwAdapter, eBool autoIdleEnable);
    }tThaModulePweMethods;

typedef struct tThaModulePwe
    {
    tAtModule super;
    const tThaModulePweMethods * methods;

    /* Private data */
    uint32 maxPwAddingWaitTimeInMs;
    }tThaModulePwe;

typedef struct tThaModulePweV2Methods
    {
    uint32 (*PwTxEthHdrValCtrl)(ThaModulePweV2 self);
    uint32 (*PwRamStatusRegister)(ThaModulePweV2 self);

    /* APS&HSPW reg */
    uint32 (*Pseudowire_Transmit_HSPW_Protection_Control)(ThaModulePweV2 self, uint32 groupIdx);
    uint32 (*Pseudowire_Transmit_UPSR_Group_Control)(ThaModulePweV2 self, uint32 groupIdx);
    uint32 (*Pseudowire_Transmit_UPSR_and_HSPW_Control)(ThaModulePweV2 self, uint32 pwIdx);
    uint32 (*Pseudowire_Transmit_HSPW_Label_Control)(ThaModulePweV2 self, uint32 pwIdx);
    eAtRet (*ApsGroupPwAdd)(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
    eAtRet (*ApsGroupPwRemove)(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
    eAtRet (*HsGroupPwAdd)(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
    eAtRet (*HsGroupPwRemove)(ThaModulePweV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
    eBool  (*ShouldResetAllPwGroups)(ThaModulePweV2 self);
    eAtRet (*ApsGroupEnable)(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable);
    eBool  (*ApsGroupIsEnabled)(ThaModulePweV2 self, AtPwGroup pwGroup);
    eAtRet (*HsGroupLabelSetSelect)(ThaModulePweV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
    eAtRet (*HsGroupSelectedLabelSetGet)(ThaModulePweV2 self, AtPwGroup pwGroup);

    eAtRet (*HsGroupEnable)(ThaModulePweV2 self, AtPwGroup pwGroup, eBool enable);
    eBool  (*HsGroupIsEnabled)(ThaModulePweV2 self, AtPwGroup pwGroup);

    eAtRet (*ApsGroupPwHwAdd)(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter);
    eBool  (*ApsGroupPwIsInHwGroup)(ThaModulePweV2 self, uint32 pwGroupHwId, AtPw pwAdapter);
    eAtRet (*ApsGroupHwEnable)(ThaModulePweV2 self, uint32 pwGroupHwId, eBool enable);
    eBool  (*ApsGroupHwIsEnabled)(ThaModulePweV2 self, uint32 pwGroupHwId);
    eAtRet (*TxPwActiveForce)(ThaModulePweV2 self, uint32 pwGroupHwId, eBool enable);
    eBool  (*TxPwActiveIsForced)(ThaModulePweV2 self, uint32 pwGroupHwId);
    }tThaModulePweV2Methods;

typedef struct tThaModulePweV2
    {
    tThaModulePwe super;
    const tThaModulePweV2Methods * methods;

    /* Private data */
    }tThaModulePweV2;

typedef struct tThaPdhPwProductModulePwe
    {
    tThaModulePweV2 super;
    }tThaPdhPwProductModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModulePweObjectInit(AtModule module, AtDevice device);
AtModule ThaModulePweV2ObjectInit(AtModule module, AtDevice device);
AtModule ThaPdhPwProductModulePweObjectInit(AtModule self, AtDevice device);

void ThaModulePweRegDisplay(AtPw pw, const char* regName, uint32 address);
void ThaModulePweLongRegDisplay(AtPw pw, const char* regName, uint32 address);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPWEINTERNAL_H_ */


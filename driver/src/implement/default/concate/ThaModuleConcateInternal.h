/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : ThaModuleConcateInternal.h
 * 
 * Created Date: Oct 9, 2014
 *
 * Description : Concate module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECONCATEINTERNAL_H_
#define _THAMODULECONCATEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/concate/AtModuleConcateInternal.h"
#include "ThaModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleConcateMethods
    {
    eAtRet (*DefaultSet)(ThaModuleConcate self);

    /* Offset */
    uint32 (*HoVcDefaultOffset)(ThaModuleConcate self, AtChannel hoVc);
    uint32 (*Tu3VcDefaultOffset)(ThaModuleConcate self, AtChannel tu3Vc);
    uint32 (*Vc1xDefaultOffset)(ThaModuleConcate self, AtChannel vc1x);
    uint32 (*De1DefaultOffset)(ThaModuleConcate self, AtChannel de1);
    uint32 (*De3DefaultOffset)(ThaModuleConcate self, AtChannel de3);

    /* Sink channel binding */
    eAtRet (*BindVc1xToSinkConcateGroup)(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group);
    eAtRet (*BindHoVcToSinkConcateGroup)(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group);
    eAtRet (*BindTu3VcToSinkConcateGroup)(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group);
    eAtRet (*BindDe3ToSinkConcateGroup)(ThaModuleConcate self, AtChannel de3, AtConcateGroup group);
    eAtRet (*BindDe1ToSinkConcateGroup)(ThaModuleConcate self, AtChannel de1, AtConcateGroup group);

    /* Source channel binding */
    eAtRet (*BindVc1xToSourceConcateGroup)(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group);
    eAtRet (*BindHoVcToSourceConcateGroup)(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group);
    eAtRet (*BindTu3VcToSourceConcateGroup)(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group);
    eAtRet (*BindDe3ToSourceConcateGroup)(ThaModuleConcate self, AtChannel de3, AtConcateGroup group);
    eAtRet (*BindDe1ToSourceConcateGroup)(ThaModuleConcate self, AtChannel de1, AtConcateGroup group);

    /* Member adding/removing */
    eAtRet (*SourceMemberAdd)(ThaModuleConcate self, ThaVcgMember member);
    eAtRet (*SourceMemberRemove)(ThaModuleConcate self, ThaVcgMember member);
    eAtRet (*SinkMemberAdd)(ThaModuleConcate self, ThaVcgMember member, eBool lcasEnable);
    eAtRet (*SinkMemberRemove)(ThaModuleConcate self, ThaVcgMember member);

    /* Member payload */
    eAtRet (*SourceMemberVcxPayloadSet)(ThaModuleConcate self, AtConcateMember member, eBool enable);
    eAtRet (*SinkMemberVcxPayloadSet)(ThaModuleConcate self, AtConcateMember member, eBool enable);
    eAtRet (*SourceMemberVc1xPayloadSet)(ThaModuleConcate self, AtConcateMember member, eBool enable);
    eAtRet (*SinkMemberVc1xPayloadSet)(ThaModuleConcate self, AtConcateMember member, eBool enable);

    /* ID conversion */
    eAtRet (*SdhChannelStsIdSw2HwGet)(ThaModuleConcate self, AtSdhChannel sdhChannel, uint8 swSts, uint16 *hwSts);
    eAtRet (*PdhChannelHwIdGet)(ThaModuleConcate self, AtPdhChannel pdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt);
    eAtRet (*SdhChannelHwIdGet)(ThaModuleConcate self, AtSdhChannel sdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt);

    /* Factoring */
    AtConcateMember (*CreateVcgMemberObjectForSdhVc)(ThaModuleConcate self, AtSdhVc vc, AtConcateGroup group);
    AtConcateMember (*CreateVcgMemberObjectForSdhVc1x)(ThaModuleConcate self, AtSdhVc vc1x, AtConcateGroup group);

    /* For debugging */
    void (*SdhChannelRegsShow)(ThaModuleConcate self, AtSdhChannel sdhChannel);
    void (*PdhChannelRegsShow)(ThaModuleConcate self, AtPdhChannel pdhChannel);
    }tThaModuleConcateMethods;

typedef struct tThaModuleConcate
    {
    tAtModuleConcate super;
    const tThaModuleConcateMethods *methods;
    }tThaModuleConcate;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleConcate ThaModuleConcateObjectInit(AtModuleConcate self, AtDevice device);

#endif /* _THAMODULECONCATEINTERNAL_H_ */


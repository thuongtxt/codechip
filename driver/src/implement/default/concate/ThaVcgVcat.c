/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : ThaVcgVcat.c
 *
 * Created Date: May 27, 2014
 *
 * Description : VCAT VCG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "binder/ThaVcgBinder.h"
#include "member/ThaVcgMember.h"
#include "ThaModuleConcateReg.h"
#include "ThaVcgVcatInternal.h"


/*--------------------------- Define -----------------------------------------*/
#define cLongRegSize 2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaVcgVcat)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVcgVcatMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;
static tAtConcateGroupMethods   m_AtConcateGroupOverride;
static tAtVcgMethods            m_AtVcgOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods  = NULL;
static const tAtChannelMethods      *m_AtChannelMethods = NULL;
static const tAtConcateGroupMethods *m_AtConcateGroupMethods = NULL;
static const tAtVcgMethods          *m_AtVcgMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void CurrentSqSet(AtVcg self, uint32 sequence)
    {
    mThis(self)->currentSq = sequence;
    }

static uint32 CurrentSqGet(AtVcg self)
    {
    return mThis(self)->currentSq;
    }

static eBool LcasIsEnabled(AtVcg self)
    {
    return mThis(self)->enableLcas;
    }

static eAtModuleConcateRet HoldOffTimerSet(AtVcg self, uint32 hoTimeMs)
    {
    uint32 regAddr = cAf4RegMapSkThr(AtChannelIdGet((AtChannel)self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    mFieldIns(&regVal, cAf4MapSkThrHOMask, cAf4MapSkThrHOShift, hoTimeMs);
    mFieldIns(&regVal, cAf4MapSkThrHODisMask, cAf4MapSkThrHODisShift, hoTimeMs ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 HoldOffTimerGet(AtVcg self)
    {
    uint32 vcgId  = AtChannelIdGet((AtChannel) self);
    uint32 regVal = mChannelHwRead(self, cAf4RegMapSkThr(vcgId), cAtModuleConcate);
    return mRegField(regVal, cAf4MapSkThrHO);
    }

static eAtModuleConcateRet WtrTimerSet(AtVcg self, uint32 timerMs)
    {
	uint32 vcgId   = AtChannelIdGet((AtChannel)self);
	uint32 regAddr = cAf4RegMapSkThr(vcgId);
	uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mFieldIns(&regVal, cAf4MapSkThrWTRMask, cAf4MapSkThrWTRShift, timerMs);
    mFieldIns(&regVal, cAf4MapSkThrWTRDisMask, cAf4MapSkThrWTRDisShift, timerMs ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 WtrTimerGet(AtVcg self)
    {
	uint32 vcgId  = AtChannelIdGet((AtChannel)self);
	uint32 regVal = mChannelHwRead(self, cAf4RegMapSkThr(vcgId), cAtModuleConcate);
	return mRegField(regVal, cAf4MapSkThrWTR);
    }

static eAtModuleConcateRet RmvTimerSet(AtVcg self, uint32 timerMs)
    {
    uint32 vcgId   = AtChannelIdGet((AtChannel) self);
    uint32 regAddr = cAf4RegMapSkThr(vcgId);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);

    mFieldIns(&regVal, cAf4MapSkThrRmvMask, cAf4MapSkThrRmvShift, timerMs);
    mFieldIns(&regVal, cAf4MapSkThrRmvDisMask, cAf4MapSkThrRmvDisShift, timerMs ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 RmvTimerGet(AtVcg self)
    {
    uint32 vcgId  = AtChannelIdGet((AtChannel) self);
    uint32 regVal = mChannelHwRead(self, cAf4RegMapSkThr(vcgId), cAtModuleConcate);
    return mRegField(regVal, cAf4MapSkThrRmv);
    }

static eAtModuleConcateRet SinkMemberAdd(AtVcg self, AtConcateMember member)
    {
    eAtModuleConcateRet ret = m_AtVcgMethods->SinkMemberAdd(self, member);
    if (ret != cAtOk)
        return ret;
    return ThaVcgMemberSinkAdd((ThaVcgMember)member, (AtConcateGroup)self, AtVcgLcasIsEnabled(self));
    }

static eAtModuleConcateRet SinkMemberRemove(AtVcg self, AtConcateMember member)
    {
    eAtModuleConcateRet ret = m_AtVcgMethods->SinkMemberRemove(self, member);
    if (ret != cAtOk)
        return ret;
    return ThaVcgMemberSinkRemove((ThaVcgMember)member);
    }

static eAtModuleConcateRet SourceMemberAdd(AtVcg self, AtConcateMember member)
    {
    eAtRet ret = m_AtVcgMethods->SourceMemberAdd(self, member);
    if (ret != cAtOk)
        return ret;
    return ThaVcgMemberSourceAdd((ThaVcgMember)member, (AtConcateGroup)self);
    }

static eAtModuleConcateRet SourceMemberRemove(AtVcg self, AtConcateMember member)
    {
    eAtModuleConcateRet ret = m_AtVcgMethods->SourceMemberRemove(self, member);
    if (ret != cAtOk)
        return ret;

    return ThaVcgMemberSourceRemove((ThaVcgMember)member);
    }

static uint8 SourceRsAckGet(AtVcg self)
    {
	uint32 regVal = mChannelHwRead(self, cAf4RegMapRsAck(AtChannelIdGet((AtChannel) self)), cAtModuleConcate);
	return (regVal & cAf4TxRsAckMask) ? 1 : 0;
    }

static uint8 SinkRsAckGet(AtVcg self)
    {
	uint32 regVal = mChannelHwRead(self, cAf4RegMapRsAck(AtChannelIdGet((AtChannel) self)), cAtModuleConcate);
	return (regVal & cAf4RxRsAckMask) ? 1 : 0;
    }

static eAtRet HolddingReset(AtChannel self)
    {
    uint32 vcgId = AtChannelIdGet(self);

    mChannelHwWrite(self, cAf4RegMapWriteAcsDataHolding1, cAf4RegMapWriteAcsDataHolding1RwMsk, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapSoRecMst(vcgId), cAf4RegMapSoRecMstRwMsk, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapSkTransMst(vcgId), cAf4RegMapSkTransMstRwMsk, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapWriteAcsDataHolding1, cAf4RegMapWriteAcsDataHolding1RstVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SoMstReset(AtChannel self)
    {
    mChannelHwWrite(self, cAf4RegMapSoRxMST(AtChannelIdGet(self)), cAf4RegMapSoRxMSTRstVal, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet ClearStatus(AtChannel self)
    {
    uint32 vcgId = AtChannelIdGet(self);

    mChannelHwWrite(self, cAf4RegMapSoVcgStat(vcgId), 0x0, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapSkVcgStat(vcgId), 0x0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet TimersDefaultSet(AtVcg self)
    {
    eAtRet ret;

    ret  = AtVcgHoldOffTimerSet((AtVcg)self, 100);
    ret |= AtVcgRmvTimerSet((AtVcg)self, 500);
    ret |= AtVcgWtrTimerSet((AtVcg)self, 300);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Default timers */
    ret  = TimersDefaultSet((AtVcg)self);
    ret |= mMethodsGet(mThis(self))->DefaultSet(mThis(self));

    return ret;
    }

static eAtRet LongRead(AtVcg self, uint32 address, uint32 *values, uint8 regSizeInDwords)
    {
    values[0] = mChannelHwRead(self, address, cAtModuleConcate);
    if (regSizeInDwords > 0)
        values[1] = mChannelHwRead(self, cAf4RegMapReadAcsDataHolding1, cAtModuleConcate);
    return cAtOk;
    }

static uint8 SinkMstGet(AtVcg self, AtConcateMember member)
    {
    uint32 longRegVal[cLongRegSize];
    uint32 vcgId = AtChannelIdGet((AtChannel) self);
    uint32 sqValue = AtConcateMemberSinkSequenceGet(member);

    LongRead(self, cAf4RegMapSkTransMst(vcgId), longRegVal, cLongRegSize);

    if (sqValue < 32)
        return (longRegVal[0] & cAf4SkTransMstMask(sqValue)) ? cAtTrue : cAtFalse;
    if (sqValue < 64)
        return (longRegVal[1] & cAf4SkTransMstMask(sqValue - 32)) ? cAtTrue : cAtFalse;

    return 1;
    }

static uint8 SourceMstGet(AtVcg self, AtConcateMember member)
    {
    uint32 longRegVal[cLongRegSize];
    uint32 sqValue = AtConcateMemberSourceSequenceGet(member);
    uint32 vcgId   = AtChannelIdGet((AtChannel)self);

    LongRead(self, cAf4RegMapSoRecMst(vcgId), longRegVal, cLongRegSize);

    if (sqValue < 32)
        return (longRegVal[0] & cAf4SoRecMstMask(sqValue)) ? cAtTrue : cAtFalse;
    if (sqValue < 64)
        return (longRegVal[1] & cAf4SoRecMstMask(sqValue - 32)) ? cAtTrue : cAtFalse;

    return 1;
    }

static eBool LcasIsSupported(AtVcg self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static eAtModuleConcateRet LcasEnable(AtVcg self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtConcateGroup vcg = (AtConcateGroup)self;
    uint32 member_i;
    uint32 sourceMembNum = AtConcateGroupNumSourceMembersGet(vcg);
    uint32 sinkMembNum = AtConcateGroupNumSinkMembersGet(vcg);
    AtConcateMember member;
    AtChannel channel;

    if (enable == mThis(self)->enableLcas)
        return cAtOk;

    ret = m_AtVcgMethods->LcasEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    /* TODO: This procedure of changing between LCAS and non-LCAS is not
     * hitless. We may need to investigate a better way. */

    /* Remove all members at source */
    for (member_i = 0; member_i < sourceMembNum; member_i++)
        {
        member  = AtConcateGroupSourceMemberGetByIndex(vcg, member_i);
        channel = AtConcateMemberChannelGet(member);

        if (mThis(self)->enableLcas)
            ret |= SourceMemberRemove(self, member);

        /* Unbind source member from VCG */
        ret |= AtChannelTxEnable((AtChannel)member, cAtFalse);
        ret |= AtChannelBindToSourceGroup(channel, NULL);
        }

    /* Remove all members at sink */
    for (member_i = 0; member_i < sinkMembNum; member_i++)
        {
        member  = AtConcateGroupSinkMemberGetByIndex(vcg, member_i);
        channel = AtConcateMemberChannelGet(member);

        if (mThis(self)->enableLcas)
            ret |= SinkMemberRemove(self, member);

        /* Unbind sink member from VCG */
        ret |= AtChannelRxEnable((AtChannel)member, cAtFalse);
        ret |= AtChannelBindToSinkGroup(channel, NULL);
        }

    /* Reset VCG */
    mMethodsGet(mThis(self))->HwReset(mThis(self));

    /* Update DB */
    mThis(vcg)->enableLcas = enable;

    /* Re-provision members */
    for (member_i = 0; member_i < sourceMembNum; member_i++)
        {
        member      = AtConcateGroupSourceMemberGetByIndex(vcg, member_i);
        channel     = AtConcateMemberChannelGet(member);
        ret |= AtChannelBindToSourceGroup(channel, vcg);
        ret |= AtChannelTxEnable((AtChannel)member, cAtTrue);

        /* Need to make sure that the sequence in VCAT be consecutive */
        if (!enable)
            ret |= AtConcateMemberSourceSequenceSet(member, member_i);
        }

    for (member_i = 0; member_i < sinkMembNum; member_i++)
        {
        member      = AtConcateGroupSinkMemberGetByIndex(vcg, member_i);
        channel     = AtConcateMemberChannelGet(member);
        ret |= AtChannelBindToSinkGroup(channel, vcg);
        ret |= AtChannelRxEnable((AtChannel)member, cAtTrue);

        /* Default expected sink sequence number at member index */
        if (!enable)
            ret |= AtConcateMemberSinkExpectedSequenceSet(member, member_i);
        }

    /* Re-add all hardware members into the VCG if LCAS is disabled. It is
     * required so that VCAT starts to work. */
    if (!enable)
        {
        for (member_i = 0; member_i < sourceMembNum; member_i++)
            {
            member = AtConcateGroupSourceMemberGetByIndex(vcg, member_i);
            ret   |= ThaVcgMemberSourceAdd((ThaVcgMember)member, vcg);
            }

        for (member_i = 0; member_i < sinkMembNum; member_i++)
            {
            member = AtConcateGroupSinkMemberGetByIndex(vcg, member_i);
            ret   |= ThaVcgMemberSinkAdd((ThaVcgMember)member, vcg, cAtFalse);
            }
        }

    return ret;
    }

static eAtRet DefaultSet(ThaVcgVcat self)
    {
    AtChannel vcg = (AtChannel)self;
    eAtRet ret = cAtOk;

    ret |= HolddingReset(vcg);
    ret |= ClearStatus(vcg);
    ret |= SoMstReset(vcg);

    return ret;
    }

static AtConcateMember SourceMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret;
    AtConcateMember member;

    /* Already provisioned */
    /* TODO: this check should be moved to top API to eliminate the code repetition */
    member = AtConcateGroupSourceMemberGetByChannel(self, channel);
    if (member != NULL)
        return member;

    member = m_AtConcateGroupMethods->SourceMemberProvision(self, channel);
    if (member == NULL)
        return NULL;

    if (AtVcgLcasIsEnabled((AtVcg)self))
        return member;

    /* Need to make sure that the sequence in VCAT be consecutive */
    if (ThaVcgVcatSourceMemberSequenceAssign((AtVcg)self, member) != cAtOk)
        {
        m_AtConcateGroupMethods->SourceMemberDeprovision(self, channel);
        return NULL;
        }

    /* VCAT also requires hardware adding */
    ret = ThaVcgMemberSourceAdd((ThaVcgMember)member, self);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)self, cSevCritical, AtSourceLocation,
                    "Cannot add VCAT source member %s, ret = %s",
                    AtObjectToString((AtObject)member), AtRet2String(ret));
        }

    return member;
    }

static eAtModuleConcateRet SourceMemberDeprovision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret;
    AtConcateMember member;

    if (AtVcgLcasIsEnabled((AtVcg)self))
        return m_AtConcateGroupMethods->SourceMemberDeprovision(self, channel);

    member = AtConcateGroupSourceMemberGetByChannel(self, channel);
    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    /* Remove VCAT source member from hardware */
    ret = ThaVcgMemberSourceRemove((ThaVcgMember)member);

    /* Need to make sure that the sequence in VCAT be consecutive */
    ret |= ThaVcgVcatSourceMemberSequenceRearrange((AtVcg)self, member);
    if (ret != cAtOk)
        return ret;

    return m_AtConcateGroupMethods->SourceMemberDeprovision(self, channel);
    }

static AtConcateMember SinkMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret;
    AtConcateMember member;

    /* Already provisioned */
    /* TODO: this check should be moved to top API to eliminate the code repetition */
    member = AtConcateGroupSinkMemberGetByChannel(self, channel);
    if (member != NULL)
        return member;

    member = m_AtConcateGroupMethods->SinkMemberProvision(self, channel);
    if (member == NULL)
        return NULL;

    if (AtVcgLcasIsEnabled((AtVcg)self))
        return member;

    /* Default expected sink sequence number */
    ret = AtConcateMemberSinkExpectedSequenceSet(member, (uint32)(AtConcateGroupNumSinkMembersGet(self) - 1));

    /* Add VCAT sink member to hardware */
    ret |= ThaVcgMemberSinkAdd((ThaVcgMember)member, self, cAtFalse);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)self, cSevCritical, AtSourceLocation,
                    "Cannot add VCAT sink member %s, ret = %s",
                    AtObjectToString((AtObject)member), AtRet2String(ret));
        }

    return member;
    }

static eAtModuleConcateRet SinkMemberDeprovision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret;
    AtConcateMember member;

    if (AtVcgLcasIsEnabled((AtVcg)self))
        return m_AtConcateGroupMethods->SinkMemberDeprovision(self, channel);

    member = AtConcateGroupSinkMemberGetByChannel(self, channel);
    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    /* Remove VCAT sink member from hardware */
    ret  = ThaVcgMemberSinkRemove((ThaVcgMember)member);
    if (ret != cAtOk)
        return ret;

    /* Need to make sure that the sequence in VCAT be consecutive */
    ret |= ThaVcgVcatSinkMemberSequenceRearrange((AtVcg)self, member);
    if (ret != cAtOk)
        return ret;

    return m_AtConcateGroupMethods->SinkMemberDeprovision(self, channel);
    }

static void HwReset(ThaVcgVcat self)
    {
    ThaVcgReset((AtVcg)self);
    }

static void MethodsInit(ThaVcgVcat self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, HwReset);
        }

    mMethodsSet(self, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaVcgVcat object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(enableLcas);
    mEncodeUInt(currentSq);
    }

static void OverrideAtObject(AtVcg self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtVcg self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtConcateGroup(AtVcg self)
    {
    AtConcateGroup group = (AtConcateGroup)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateGroupMethods = mMethodsGet(group);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateGroupOverride, m_AtConcateGroupMethods, sizeof(m_AtConcateGroupOverride));

        mMethodOverride(m_AtConcateGroupOverride, SourceMemberProvision);
        mMethodOverride(m_AtConcateGroupOverride, SourceMemberDeprovision);
        mMethodOverride(m_AtConcateGroupOverride, SinkMemberProvision);
        mMethodOverride(m_AtConcateGroupOverride, SinkMemberDeprovision);
        }

    mMethodsSet(group, &m_AtConcateGroupOverride);
    }

static void OverrideAtVcg(AtVcg self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal        = AtSharedDriverOsalGet();
        m_AtVcgMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgOverride, m_AtVcgMethods, sizeof(m_AtVcgOverride));

        mMethodOverride(m_AtVcgOverride, LcasIsSupported);
        mMethodOverride(m_AtVcgOverride, LcasEnable);
        mMethodOverride(m_AtVcgOverride, LcasIsEnabled);
        mMethodOverride(m_AtVcgOverride, HoldOffTimerSet);
        mMethodOverride(m_AtVcgOverride, HoldOffTimerGet);
        mMethodOverride(m_AtVcgOverride, RmvTimerSet);
        mMethodOverride(m_AtVcgOverride, RmvTimerGet);
        mMethodOverride(m_AtVcgOverride, WtrTimerSet);
        mMethodOverride(m_AtVcgOverride, WtrTimerGet);
        mMethodOverride(m_AtVcgOverride, SinkMemberAdd);
        mMethodOverride(m_AtVcgOverride, SinkMemberRemove);
        mMethodOverride(m_AtVcgOverride, SourceMemberAdd);
        mMethodOverride(m_AtVcgOverride, SourceMemberRemove);
        mMethodOverride(m_AtVcgOverride, SourceRsAckGet);
        mMethodOverride(m_AtVcgOverride, SinkRsAckGet);
        mMethodOverride(m_AtVcgOverride, SinkMstGet);
        mMethodOverride(m_AtVcgOverride, SourceMstGet);
        }

    mMethodsSet(self, &m_AtVcgOverride);
    }

static void Override(AtVcg self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtConcateGroup(self);
    OverrideAtVcg(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcgVcat);
    }

AtVcg ThaVcgVcatObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtVcgObjectInit(self, concateModule, vcatId, memberType) == NULL)
        return NULL;

    /* Initialize local value */
    mThis(self)->currentSq = 0;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtVcg ThaVcgVcatNew(AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtVcg newVcg = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVcg == NULL)
        return NULL;

    /* Construct it */
    return ThaVcgVcatObjectInit(newVcg, concateModule, vcatId, memberType);
    }

eAtRet ThaVcgVcatSourceMemberSequenceAssign(AtVcg self, AtConcateMember addedMember)
    {
    uint32 currentSq = CurrentSqGet(self);

    /* Next sequence for next new member */
    CurrentSqSet(self, currentSq + 1);

    return AtConcateMemberSourceSequenceSet(addedMember, currentSq);
    }

eAtRet ThaVcgVcatSourceMemberSequenceRearrange(AtVcg self, AtConcateMember removedMember)
    {
    uint32 numMembers = AtConcateGroupNumSourceMembersGet((AtConcateGroup)self);
    uint32 removedSq  = AtConcateMemberSourceSequenceGet(removedMember);
    uint32 member_i;
    eAtModuleConcateRet ret = cAtOk;

    for (member_i = 0; member_i < numMembers; member_i++)
        {
        AtConcateMember member = AtConcateGroupSourceMemberGetByIndex((AtConcateGroup)self, member_i);
        uint32 memberSq = AtConcateMemberSourceSequenceGet(member);
        if (memberSq > removedSq)
            ret |= AtConcateMemberSourceSequenceSet(member, memberSq - 1);
        }

    /* Update current sequences */
    CurrentSqSet(self, CurrentSqGet(self) - 1);

    return ret;
    }

void ThaVcgReset(AtVcg self)
    {
    uint32 vcgId = AtChannelIdGet((AtChannel)self);

    /* Source direction */
    mChannelHwWrite(self, cAf4RegMapWriteAcsDataHolding1, cAf4RegMapSoVcgStatRstVal, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapSoVcgStat(vcgId), cAf4RegMapSoVcgStatRstVal, cAtModuleConcate);

    mChannelHwWrite(self, cAf4RegMapWriteAcsDataHolding1, cAf4RegMapSoRecMstRstVal, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapSoRecMst(vcgId), cAf4RegMapSoRecMstRstVal, cAtModuleConcate);

    /* Sink direction */
    mChannelHwWrite(self, cAf4RegMapWriteAcsDataHolding1, cAf4RegMapSkVcgStatRstVal, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapSkVcgStat(vcgId), cAf4RegMapSkVcgStatRstVal, cAtModuleConcate);

    mChannelHwWrite(self, cAf4RegMapWriteAcsDataHolding1, cAf4RegMapSkTransMstRstVal, cAtModuleConcate);
    mChannelHwWrite(self, cAf4RegMapSkTransMst(vcgId), cAf4RegMapSkTransMstRstVal, cAtModuleConcate);
    }

eAtRet ThaVcgVcatSinkMemberSequenceRearrange(AtVcg self, AtConcateMember removedMember)
    {
    uint32 numMembers = AtConcateGroupNumSinkMembersGet((AtConcateGroup)self);
    uint32 removedSq  = AtConcateMemberSinkExpectedSequenceGet(removedMember);
    uint32 member_i;
    eAtModuleConcateRet ret = cAtOk;

    for (member_i = 0; member_i < numMembers; member_i++)
        {
        AtConcateMember member = AtConcateGroupSinkMemberGetByIndex((AtConcateGroup)self, member_i);
        uint32 memberSq = AtConcateMemberSinkExpectedSequenceGet(member);
        if (memberSq > removedSq)
            ret |= AtConcateMemberSinkExpectedSequenceSet(member, memberSq - 1);
        }

    return ret;
    }

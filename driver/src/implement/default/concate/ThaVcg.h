/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : ThaVcg.h
 * 
 * Created Date: Jun 6, 2014
 *
 * Description : Concate group common declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCG_H_
#define _THAVCG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConcateGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* VCAT/LCAS concrete classes */
AtVcg ThaVcgVcatNew(AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType);
AtVcg Tha60290081VcgHoVcatNew(AtModuleConcate module, uint32 vcgId, eAtConcateMemberType memberType);
AtVcg Tha60290081VcgLoVcatNew(AtModuleConcate module, uint32 vcgId, eAtConcateMemberType memberType);

/* Non-VCAT concrete classes */
AtConcateGroup ThaVcgNonVcatNew(AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType);
AtConcateGroup Tha60290081VcgNonVcatNew(AtModuleConcate module, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType);

eAtRet ThaVcgVcatSourceMemberSequenceAssign(AtVcg self, AtConcateMember addedMember);
eAtRet ThaVcgVcatSourceMemberSequenceRearrange(AtVcg self, AtConcateMember removedMember);
eAtRet ThaVcgVcatSinkMemberSequenceRearrange(AtVcg self, AtConcateMember removedMember);
void ThaVcgReset(AtVcg self);

#endif /* _THAVCG_H_ */


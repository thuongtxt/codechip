/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : ThaVcgMemberSdhVc1x.c
 *
 * Created Date: Jul 12, 2019
 *
 * Description : SDH VC1x - VCG member
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModuleConcate.h"
#include "ThaVcgMemberInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtConcateMemberMethods  m_AtConcateMemberOverride;
static tThaVcgMemberMethods     m_ThaVcgMemberOverride;

/* Save super implementation */
static const tAtConcateMemberMethods    *m_AtConcateMemberMethods = NULL;
static const tThaVcgMemberMethods       *m_ThaVcgMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel SdhVc(ThaVcgMember self)
    {
    return (AtSdhChannel)AtConcateMemberChannelGet((AtConcateMember)self);
    }

static uint32 DefaultOffset(ThaVcgMember self)
    {
    return ThaSdhChannelConcateDefaultOffset(SdhVc(self));
    }

static ThaModuleConcate ModuleConcate(AtConcateMember self)
    {
    return (ThaModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static eAtModuleConcateRet SourceInit(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SourceInit(self);
    if (ret != cAtOk)
        return ret;

    return ThaModuleConcateSourceMemberVc1xPayloadSet(ModuleConcate(self), self, cAtTrue);
    }

static eAtModuleConcateRet SinkInit(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SinkInit(self);
    if (ret != cAtOk)
        return ret;

    return ThaModuleConcateSinkMemberVc1xPayloadSet(ModuleConcate(self), self, cAtTrue);
    }

static eAtRet SourceHardwareCleanup(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SourceHardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    return ThaModuleConcateSourceMemberVc1xPayloadSet(ModuleConcate(self), self, cAtFalse);
    }

static eAtRet SinkHardwareCleanup(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SinkHardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    return ThaModuleConcateSinkMemberVc1xPayloadSet(ModuleConcate(self), self, cAtFalse);
    }

static void OverrideAtConcateMember(AtConcateMember self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateMemberMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateMemberOverride, m_AtConcateMemberMethods, sizeof(m_AtConcateMemberOverride));

        mMethodOverride(m_AtConcateMemberOverride, SourceInit);
        mMethodOverride(m_AtConcateMemberOverride, SinkInit);
        mMethodOverride(m_AtConcateMemberOverride, SourceHardwareCleanup);
        mMethodOverride(m_AtConcateMemberOverride, SinkHardwareCleanup);
        }

    mMethodsSet(self, &m_AtConcateMemberOverride);
    }

static void OverrideThaVcgMember(AtConcateMember self)
    {
    ThaVcgMember member = (ThaVcgMember)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgMemberMethods = mMethodsGet(member);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgMemberOverride, mMethodsGet(member), sizeof(m_ThaVcgMemberOverride));

        mMethodOverride(m_ThaVcgMemberOverride, DefaultOffset);
        }

    mMethodsSet(member, &m_ThaVcgMemberOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideAtConcateMember(self);
    OverrideThaVcgMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcgMemberSdhVc1x);
    }

AtConcateMember ThaVcgMemberSdhVc1xObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgMemberObjectInit(self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateMember ThaVcgMemberSdhVc1xNew(AtConcateGroup vcg, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateMember newMember = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMember == NULL)
        return NULL;

    /* Construct it */
    return ThaVcgMemberSdhVcObjectInit(newMember, vcg, channel);
    }

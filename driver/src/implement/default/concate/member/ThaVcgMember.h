/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Af4VcgMember.h
 * 
 * Created Date: Jul 22, 2014
 *
 * Description : VCG member
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF4VCGMEMBER_H_
#define _AF4VCGMEMBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConcateMember.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcgMember * ThaVcgMember;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete members */
AtConcateMember ThaVcgMemberSdhVcNew(AtConcateGroup vcg, AtChannel channel);
AtConcateMember ThaVcgMemberSdhVc1xNew(AtConcateGroup vcg, AtChannel channel);
AtConcateMember ThaVcgMemberPdhDe3New(AtConcateGroup vcg, AtChannel channel);
AtConcateMember ThaVcgMemberPdhDe1New(AtConcateGroup vcg, AtChannel channel);

eAtRet ThaVcgMemberSourceAdd(ThaVcgMember self, AtConcateGroup vcg);
eAtRet ThaVcgMemberSinkAdd(ThaVcgMember self, AtConcateGroup vcg, eBool lcasEnable);
eAtRet ThaVcgMemberSinkRemove(ThaVcgMember self);
eAtRet ThaVcgMemberSourceRemove(ThaVcgMember self);

eAtRet ThaVcgMemberSinkReset(ThaVcgMember self);
eAtRet ThaVcgMemberSourceReset(ThaVcgMember self);

/* HW ID allocation */
uint32 ThaVcgMemberHwIdAllocate(ThaVcgMember self);
void ThaVcgMemberHwIdDeAllocate(ThaVcgMember self);
void ThaVcgMemberHwIdSet(ThaVcgMember self, uint32 hwId);

/* Utils */
uint32 ThaVcgMemberDefaultOffset(ThaVcgMember self);
uint8 ThaVcgMemberStsId(ThaVcgMember self);

#endif /* _AF4VCGMEMBER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : Af4VcgMember.c
 *
 * Created Date: May 27, 2014
 *
 * Description : AF4 VCG member
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../ThaModuleConcate.h"
#include "../ThaModuleConcateReg.h"
#include "../ThaVcg.h"
#include "ThaVcgMemberInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaVcgMember)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVcgMemberMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtChannelMethods       m_AtChannelOverride;
static tAtConcateMemberMethods m_AtConcateMemberOverride;

/* Save super implementation */
static const tAtObjectMethods        *m_AtObjectMethods   = NULL;
static const tAtChannelMethods       *m_AtChannelMethods   = NULL;
static const tAtConcateMemberMethods *m_AtConcateMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleConcate ModuleConcate(AtConcateMember self)
    {
    return (AtModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static eAtLcasMemberSourceState SourceStateGet(AtConcateMember self)
    {
    uint32 regAddr = cAf4RegMapSourceMst + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;
    eAtRet ret;

    ret = ThaModuleConcateSourceIndirectRead(ModuleConcate(self), cAf4IndrSourceRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return cAtLcasMemberSourceStateInvl;

    switch (mRegField(regVal, cAf4SoMemFsm))
        {
        case 0x0: return cAtLcasMemberSourceStateIdle;
        case 0x1: return cAtLcasMemberSourceStateAdd;
        case 0x2: return cAtLcasMemberSourceStateRmv;
        case 0x3: return cAtLcasMemberSourceStateNorm;
        case 0x4: return cAtLcasMemberSourceStateDnu;
        case 0x5: return cAtLcasMemberSourceStateFixed;
        default : return cAtLcasMemberSourceStateInvl;
        }
    }

static eAtLcasMemberSinkState SinkStateGet(AtConcateMember self)
    {
    uint32 offset = mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regAddr = cAf4RegMapSnkMst + offset;
    uint32 regVal;
    eAtRet ret;
    AtModuleConcate concateModule = ModuleConcate(self);

    ret = ThaModuleConcateSinkIndirectRead(concateModule, cAf4IndrSinkRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return cAtLcasMemberSinkStateInvl;

    switch (mRegField(regVal, cAf4SkMemFsm))
        {
        /* Work-around IDLE --> FAIL: if member is in IDLE state and it is added
         * into one VCG, its state have to be FAIL state */
        case 0x0:
            {
            regAddr = cAf4RegMapSnkMemberCtrlCfg + offset;
            ret = ThaModuleConcateSinkIndirectRead(concateModule, cAf4IndrSinkRegTypeMembCtrl, regAddr, &regVal);
            if (ret != cAtOk)
                return cAtLcasMemberSinkStateInvl;

            if (mRegField(regVal, cAf4SkNmsCmd)== cAf4SkNmsCmdAdd)
                return cAtLcasMemberSinkStateFail;
            return cAtLcasMemberSinkStateIdle;
            }

        case 0x1: return cAtLcasMemberSinkStateFail;
        case 0x2: return cAtLcasMemberSinkStateOk;
        case 0x3: return cAtLcasMemberSinkStateFixed;
        default:  return cAtLcasMemberSinkStateIdle;
        }
    }

static eAtModuleConcateRet SourceSequenceSet(AtConcateMember self, uint32 sourceSequence)
    {
    uint32 regAddr = cAf4RegMapSoExpSeq + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;
    eAtRet ret;
    AtVcg vcg = (AtVcg)AtConcateMemberConcateGroupGet(self);
    eBool lcasEnabled = AtVcgLcasIsEnabled(vcg);

    if (!lcasEnabled)
        return cAtErrorNotEditable;

    ret = ThaModuleConcateSourceIndirectRead(ModuleConcate(self), cAf4IndrSourceRegTypeTxSeq, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    mFieldIns(&regVal, cAf4SoMemExpSeqEnMask, cAf4SoMemExpSeqEnShift, 1);
    mFieldIns(&regVal, cAf4SoMemExpSeqMask, cAf4SoMemExpSeqShift, sourceSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 LcasSourceSequenceGet(AtConcateMember self)
    {
    uint32 regVal;
    uint32 regAddr = cAf4RegMapSourceMst;
    eAtRet ret;

    regAddr = regAddr + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    ret = ThaModuleConcateSourceIndirectRead(ModuleConcate(self), cAf4IndrSourceRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return cAf4SoMemAcpSeqMaxVal;

    return mRegField(regVal, cAf4SoMemAcpSeq);
    }

static uint32 VcatSourceSequenceGet(AtConcateMember self)
    {
    uint32 regVal;
    uint32 regAddr = cAf4RegMapSoExpSeq;
    eAtRet ret;

    regAddr = regAddr + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    ret = ThaModuleConcateSourceIndirectRead(ModuleConcate(self), cAf4IndrSourceRegTypeTxSeq, regAddr, &regVal);
    if (ret != cAtOk)
        return cAf4SoMemAcpSeqMaxVal;

    return mRegField(regVal, cAf4SoMemExpSeq);
    }

static uint32 SourceSequenceGet(AtConcateMember self)
    {
    AtVcg vcg = (AtVcg)AtConcateMemberConcateGroupGet(self);
    eBool lcasEnabled = AtVcgLcasIsEnabled(vcg);
    if (lcasEnabled)
        return LcasSourceSequenceGet(self);

    return VcatSourceSequenceGet(self);
    }

static eAtModuleConcateRet SinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence)
    {
    uint32 regAddr, regVal;

    regAddr = cAf4RegMapSkExpSeq + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mFieldIns(&regVal, cAf4SkMemExpSeqMask, cAf4SkMemExpSeqShift, sinkSequence);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static uint32 SinkExpectedSequenceGet(AtConcateMember self)
    {
    uint32 regAddr, regVal;
    uint32 expSinkSequence = 0;

    regAddr = cAf4RegMapSkExpSeq + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    mFieldGet(regVal, cAf4SkMemExpSeqMask, cAf4SkMemExpSeqShift, uint32, &expSinkSequence);

    return expSinkSequence;
    }

static uint32 SinkSequenceGet(AtConcateMember self)
    {
    uint32 regVal;
    uint32 regAddr = cAf4RegMapSnkMst;
    eAtRet ret;

    regAddr = regAddr + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    ret = ThaModuleConcateSinkIndirectRead(ModuleConcate(self), cAf4IndrSinkRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return cAf4SkMemAcpSeqMaxVal;

    return mRegField(regVal, cAf4SkMemAcpSeq);
    }

static eAtLcasMemberCtrl SourceControlGet(AtConcateMember self)
    {
    uint32 regAddr = cAf4RegMapSourceMst + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;
    eAtRet ret;

    ret = ThaModuleConcateSourceIndirectRead(ModuleConcate(self), cAf4IndrSourceRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return cAtLcasMemberCtrlInvl;

    switch (mRegField(regVal, cAf4SoMemTxCw))
        {
        case 0x0: return cAtLcasMemberCtrlFixed;
        case 0x1: return cAtLcasMemberCtrlAdd;
        case 0x2: return cAtLcasMemberCtrlNorm;
        case 0x3: return cAtLcasMemberCtrlEos;
        case 0x5: return cAtLcasMemberCtrlIdle;
        case 0xF: return cAtLcasMemberCtrlDnu;
        default : return cAtLcasMemberCtrlInvl;
        }
    }

static eAtLcasMemberCtrl SinkControlGet(AtConcateMember self)
    {
    uint32 regAddr = cAf4RegMapSnkMst + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;
    eAtRet ret;

    ret = ThaModuleConcateSinkIndirectRead(ModuleConcate(self), cAf4IndrSinkRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return cAtLcasMemberCtrlInvl;

    switch(mRegField(regVal, cAf4SkMemRxCw))
        {
        case 0x0: return cAtLcasMemberCtrlFixed;
        case 0x1: return cAtLcasMemberCtrlAdd;
        case 0x2: return cAtLcasMemberCtrlNorm;
        case 0x3: return cAtLcasMemberCtrlEos;
        case 0x5: return cAtLcasMemberCtrlIdle;
        case 0xF: return cAtLcasMemberCtrlDnu;
        default:
            return cAtLcasMemberCtrlInvl;
        }
    }

static uint32 DefaultOffset(ThaVcgMember self)
    {
    AtUnused(self);
    return 0xCAFECAFE;
    }

static uint32 SinkDefectGet(AtConcateMember self)
    {
    uint32 alarms = 0;
    uint32 offset = mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal = mChannelHwRead(self, cAf4RegMapMemStat + offset, cAtModuleConcate);

    if (regVal & cAf4RegMapMemStatGIDMask)
        alarms |= cAtConcateMemberAlarmTypeGidM;
    if (regVal & cAf4RegMapMemIntrStatLOMMask)
        alarms |= cAtConcateMemberAlarmTypeLom;
    if (regVal & cAf4RegMapMemIntrStatCRCMask)
        alarms |= cAtConcateMemberAlarmTypeCrc;
    if (regVal & cAf4RegMapMemIntrStatMNDMask)
        alarms |= cAtConcateMemberAlarmTypeMnd;
    if (regVal & cAf4RegMapMemIntrStatSQMMask)
        alarms |= cAtConcateMemberAlarmTypeSqm;

    return alarms;
    }

static uint32 SinkDefectHistoryRead2Clear(AtConcateMember self, eBool read2Clear)
    {
    uint32 regAddr = cAf4RegMapMemIntrStat + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleConcate);
    uint32 alarms  = 0;

    if (regVal & cAf4RegMapMemIntrStatGIDMask)
        alarms |= cAtConcateMemberAlarmTypeGidM;
    if (regVal & cAf4RegMapMemIntrStatLOMMask)
        alarms |= cAtConcateMemberAlarmTypeLom;
    if (regVal & cAf4RegMapMemIntrStatMNDMask)
        alarms |= cAtConcateMemberAlarmTypeMnd;
    if (regVal & cAf4RegMapMemIntrStatSQMMask)
        alarms |= cAtConcateMemberAlarmTypeSqm;
    if (regVal & cAf4RegMapMemIntrStatCRCMask)
        alarms |= cAtConcateMemberAlarmTypeCrc;

    if (read2Clear)
        mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return alarms;
    }

static uint32 SinkDefectHistoryGet(AtConcateMember self)
    {
    return SinkDefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 SinkDefectHistoryClear(AtConcateMember self)
    {
    return SinkDefectHistoryRead2Clear(self, cAtTrue);
    }

static eAtRet SourceHardwareCleanup(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SourceHardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    return ThaVcgMemberSourceReset(mThis(self));
    }

static eAtRet SinkHardwareCleanup(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SinkHardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    return ThaVcgMemberSinkReset(mThis(self));
    }

static uint32 AlarmGet(AtChannel self)
    {
    return mMethodsGet(self)->DefectGet(self);
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    return mMethodsGet(self)->DefectHistoryGet(self);
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    return mMethodsGet(self)->DefectHistoryClear(self);
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = cAf4RegMapSnkMemberCtrlCfg + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;

    ThaModuleConcateSinkIndirectRead(ModuleConcate((AtConcateMember)self), cAf4IndrSinkRegTypeMembCtrl, regAddr, &regVal);
    mFieldIns(&regVal, cAf4SkMemEnaMask, cAf4SkMemEnaShift, (enable) ? 0x1 : 0x0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 regAddr = cAf4RegMapSnkMemberCtrlCfg + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;

    ThaModuleConcateSinkIndirectRead(ModuleConcate((AtConcateMember)self), cAf4IndrSinkRegTypeMembCtrl, regAddr, &regVal);
    return mRegField(regVal, cAf4SkMemEna) ? cAtTrue : cAtFalse;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr  = cAf4RegMapMemberCtrlCfg + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;

    ThaModuleConcateSourceIndirectRead(ModuleConcate((AtConcateMember)self), cAf4IndrSourceRegTypeMembCtrl, regAddr, &regVal);
    mFieldIns(&regVal, cAf4SoMemEnaMask, cAf4SoMemEnaShift, (enable) ? 0x1 : 0x0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    uint32 regAddr  = cAf4RegMapMemberCtrlCfg + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal;

    ThaModuleConcateSourceIndirectRead(ModuleConcate((AtConcateMember)self), cAf4IndrSourceRegTypeMembCtrl, regAddr, &regVal);
    return mRegField(regVal, cAf4SoMemEna) ? cAtTrue : cAtFalse;
    }

static eAtRet SourceAdd(ThaVcgMember self, AtConcateGroup vcg)
    {
    AtUnused(vcg);
    return ThaModuleConcateSourceMemberAdd((ThaModuleConcate)ModuleConcate((AtConcateMember)self), self);
    }

static eAtRet SinkAdd(ThaVcgMember self, AtConcateGroup vcg, eBool lcasEnable)
    {
    AtUnused(vcg);
    return ThaModuleConcateSinkMemberAdd((ThaModuleConcate)ModuleConcate((AtConcateMember)self), self, lcasEnable);
    }

static eAtRet SinkRemove(ThaVcgMember self)
    {
    return ThaModuleConcateSinkMemberRemove((ThaModuleConcate)ModuleConcate((AtConcateMember)self), self);
    }

static eAtRet SourceRemove(ThaVcgMember self)
    {
    return ThaModuleConcateSourceMemberRemove((ThaModuleConcate)ModuleConcate((AtConcateMember)self), self);
    }

static eAtRet SinkReset(ThaVcgMember self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet SourceReset(ThaVcgMember self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint32 HwIdAllocate(ThaVcgMember self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void HwIdDeAllocate(ThaVcgMember self)
    {
    AtUnused(self);
    }

static eAtRet Init(AtChannel self)
    {
    return m_AtChannelMethods->Init(self);
    }

static eAtRet HwIdGet(AtChannel self)
    {
    return mThis(self)->hwId;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaVcgMember object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(hwId);
    }

static void MethodsInit(ThaVcgMember self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, SourceAdd);
        mMethodOverride(m_methods, SinkAdd);
        mMethodOverride(m_methods, SinkRemove);
        mMethodOverride(m_methods, SourceRemove);
        mMethodOverride(m_methods, SinkReset);
        mMethodOverride(m_methods, SourceReset);
        mMethodOverride(m_methods, HwIdAllocate);
        mMethodOverride(m_methods, HwIdDeAllocate);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtConcateMember self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtConcateMember self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtConcateMember(AtConcateMember self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateMemberMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateMemberOverride, m_AtConcateMemberMethods, sizeof(m_AtConcateMemberOverride));

        mMethodOverride(m_AtConcateMemberOverride, SourceStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkStateGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SourceSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceSet);
        mMethodOverride(m_AtConcateMemberOverride, SinkExpectedSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkSequenceGet);
        mMethodOverride(m_AtConcateMemberOverride, SourceControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkControlGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectHistoryGet);
        mMethodOverride(m_AtConcateMemberOverride, SinkDefectHistoryClear);
        mMethodOverride(m_AtConcateMemberOverride, SourceHardwareCleanup);
        mMethodOverride(m_AtConcateMemberOverride, SinkHardwareCleanup);
        }

    mMethodsSet(self, &m_AtConcateMemberOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtConcateMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcgMember);
    }

AtConcateMember ThaVcgMemberObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtConcateMemberObjectInit(self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaVcgMember)self);
    m_methodsInit = 1;

    mThis(self)->hwId = cInvalidUint32;

    return self;
    }

eAtRet ThaVcgMemberSourceAdd(ThaVcgMember self, AtConcateGroup vcg)
    {
    if (self)
        return mMethodsGet(self)->SourceAdd(self, vcg);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaVcgMemberSinkAdd(ThaVcgMember self, AtConcateGroup vcg, eBool lcasEnable)
    {
    if (self)
        return mMethodsGet(self)->SinkAdd(self, vcg, lcasEnable);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaVcgMemberSinkRemove(ThaVcgMember self)
    {
    if (self)
        return mMethodsGet(self)->SinkRemove(self);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaVcgMemberSourceRemove(ThaVcgMember self)
    {
    if (self)
        return mMethodsGet(self)->SourceRemove(self);
    return cAtErrorObjectNotExist;
    }

uint32 ThaVcgMemberDefaultOffset(ThaVcgMember self)
    {
    if (self)
        return mMethodsGet(self)->DefaultOffset(self);
    return cBit31_0;
    }

uint8 ThaVcgMemberStsId(ThaVcgMember self)
    {
    if (self)
        return mMethodsGet(self)->StsId(self);
    return 255;
    }

eAtRet ThaVcgMemberSinkReset(ThaVcgMember self)
    {
    if (self)
        return mMethodsGet(self)->SinkReset(self);
    return cAtErrorNullPointer;
    }

eAtRet ThaVcgMemberSourceReset(ThaVcgMember self)
    {
    if (self)
        return mMethodsGet(self)->SourceReset(self);
    return cAtErrorNullPointer;
    }

uint32 ThaVcgMemberHwIdAllocate(ThaVcgMember self)
    {
    if (self)
        return mMethodsGet(self)->HwIdAllocate(self);
    return cInvalidUint32;
    }

void ThaVcgMemberHwIdDeAllocate(ThaVcgMember self)
    {
    if (self)
        mMethodsGet(self)->HwIdDeAllocate(self);
    }

void ThaVcgMemberHwIdSet(ThaVcgMember self, uint32 hwId)
    {
    if (self)
        self->hwId = hwId;
    }

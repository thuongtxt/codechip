/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Af4VcgMemberInternal.h
 * 
 * Created Date: Jul 23, 2014
 *
 * Description : VCG member
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF4VCGMEMBERINTERNAL_H_
#define _AF4VCGMEMBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/concate/AtConcateMemberInternal.h"
#include "ThaVcgMember.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcgMemberMethods
    {
    uint32 (*DefaultOffset)(ThaVcgMember self);
    eAtRet (*SourceAdd)(ThaVcgMember self, AtConcateGroup vcg);
    eAtRet (*SinkAdd)(ThaVcgMember self, AtConcateGroup vcg, eBool lcasEnable);
    eAtRet (*SinkRemove)(ThaVcgMember self);
    eAtRet (*SourceRemove)(ThaVcgMember self);
    eAtRet (*SinkReset)(ThaVcgMember self);
    eAtRet (*SourceReset)(ThaVcgMember self);

    /* HW ID allocation */
    uint32 (*HwIdAllocate)(ThaVcgMember self);
    void (*HwIdDeAllocate)(ThaVcgMember self);

    /* Deprecated methods, do not use for new develop */
    eAtRet (*SoMemSizeInit)(ThaVcgMember self);
    eAtRet (*SkMemSizeInit)(ThaVcgMember self);
    uint8 (*StsId)(ThaVcgMember self);
    uint8 (*VtgId)(ThaVcgMember self);
    uint8 (*LoMode)(ThaVcgMember self);
    uint8 (*HoMode)(ThaVcgMember self);
    }tThaVcgMemberMethods;

typedef struct tThaVcgMember
    {
    tAtConcateMember super;
    const tThaVcgMemberMethods *methods;

    /* Private data */
    uint32 hwId;
    }tThaVcgMember;

typedef struct tThaVcgMemberSdhVc
    {
    tThaVcgMember super;
    }tThaVcgMemberSdhVc;

typedef struct tThaVcgMemberSdhVc1x
    {
    tThaVcgMember super;
    }tThaVcgMemberSdhVc1x;

typedef struct tThaVcgMemberPdhDe3
    {
    tThaVcgMember super;
    }tThaVcgMemberPdhDe3;

typedef struct tThaVcgMemberPdhDe1
    {
    tThaVcgMember super;
    }tThaVcgMemberPdhDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConcateMember ThaVcgMemberObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);
AtConcateMember ThaVcgMemberSdhVcObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);
AtConcateMember ThaVcgMemberSdhVc1xObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);
AtConcateMember ThaVcgMemberPdhDe3ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);
AtConcateMember ThaVcgMemberPdhDe1ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel);

#endif /* _AF4VCGMEMBERINTERNAL_H_ */


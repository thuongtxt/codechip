/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : ThaVcgMemberPdhDe1.c
 *
 * Created Date: Jul 23, 2014
 *
 * Description : DS1/E1 VCG member
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModuleConcate.h"
#include "ThaVcgMemberInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtConcateMemberMethods  m_AtConcateMemberOverride;
static tThaVcgMemberMethods     m_ThaVcgMemberOverride;

/* Save super implementation */
static const tAtConcateMemberMethods    *m_AtConcateMemberMethods = NULL;
static const tThaVcgMemberMethods       *m_ThaVcgMemberMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static ThaModuleConcate ModuleConcate(AtConcateMember self)
    {
    return (ThaModuleConcate)AtChannelModuleGet((AtChannel)self);
    }

static eAtModuleConcateRet SourceInit(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SourceInit(self);
    if (ret != cAtOk)
        return ret;

    return ThaModuleConcateSourceMemberPdhDe1Set(ModuleConcate(self), self);
    }

static eAtModuleConcateRet SinkInit(AtConcateMember self)
    {
    eAtRet ret = m_AtConcateMemberMethods->SinkInit(self);
    if (ret != cAtOk)
        return ret;

    return ThaModuleConcateSinkMemberPdhDe1Set(ModuleConcate(self), self);
    }

static void OverrideAtConcateMember(AtConcateMember self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtConcateMemberMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateMemberOverride, m_AtConcateMemberMethods, sizeof(m_AtConcateMemberOverride));

        mMethodOverride(m_AtConcateMemberOverride, SourceInit);
        mMethodOverride(m_AtConcateMemberOverride, SinkInit);
        }

    mMethodsSet(self, &m_AtConcateMemberOverride);
    }

static void OverrideThaVcgMember(AtConcateMember self)
    {
    ThaVcgMember member = (ThaVcgMember)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVcgMemberMethods = mMethodsGet(member);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcgMemberOverride, mMethodsGet(member), sizeof(m_ThaVcgMemberOverride));

        }

    mMethodsSet(member, &m_ThaVcgMemberOverride);
    }

static void Override(AtConcateMember self)
    {
    OverrideAtConcateMember(self);
    OverrideThaVcgMember(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcgMemberPdhDe1);
    }

AtConcateMember ThaVcgMemberPdhDe1ObjectInit(AtConcateMember self, AtConcateGroup vcg, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcgMemberObjectInit(self, vcg, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateMember ThaVcgMemberPdhDe1New(AtConcateGroup vcg, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateMember newMember = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMember == NULL)
        return NULL;

    /* Construct it */
    return ThaVcgMemberPdhDe1ObjectInit(newMember, vcg, channel);
    }

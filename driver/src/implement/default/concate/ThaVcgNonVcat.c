/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : ThaVcgNonVcat.c
 *
 * Created Date: May 27, 2014
 *
 * Description : Non-VCAT VCG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaVcgNonVcatInternal.h"
#include "ThaVcg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtConcateGroupMethods   m_AtConcateGroupOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumMembersGet(AtConcateGroup self)
    {
    AtUnused(self);
    return 1;
    }

static void OverrideAtConcateGroup(AtConcateGroup self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtConcateGroupOverride, mMethodsGet(self), sizeof(m_AtConcateGroupOverride));

        mMethodOverride(m_AtConcateGroupOverride, MaxNumMembersGet);
        }

    mMethodsSet(self, &m_AtConcateGroupOverride);
    }

static void Override(AtConcateGroup self)
    {
    OverrideAtConcateGroup(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcgNonVcat);
    }

AtConcateGroup ThaVcgNonVcatObjectInit(AtConcateGroup self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtConcateGroupObjectInit(self, concateModule, vcgId, memberType, concateType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtConcateGroup ThaVcgNonVcatNew(AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtConcateGroup newVcg = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newVcg == NULL)
        return NULL;

    /* Construct it */
    return ThaVcgNonVcatObjectInit(newVcg, concateModule, vcgId, memberType, concateType);
    }

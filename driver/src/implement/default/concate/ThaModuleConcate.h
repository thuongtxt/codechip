/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : ThaModuleConcate.h
 * 
 * Created Date: Jun 6, 2014
 *
 * Description : Concate module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECONCATE_H_
#define _THAMODULECONCATE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhVc.h"
#include "member/ThaVcgMember.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidBufferId    0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleConcate * ThaModuleConcate;

typedef enum eThaStsConcatePayloadType
    {
    cThaStsConcateModeSts   = 0,    /* STS/VC-3 payload mode */
    cThaStsConcateModeDs3   = 1,    /* DS3 payload mode */
    cThaStsConcateModeE3    = 2,    /* E3 payload mode */
    cThaStsConcateModeVtDe1 = 3     /* DS1/E1 or VT/VC1x payload mode */
    }eThaStsConcatePayloadType;

typedef enum eThaVtConcatePayloadType
    {
    cThaVtConcateModeVt15   = 0,    /* VT1.5/VC11 payload mode */
    cThaVtConcateModeVt2    = 1,    /* VT2/VC12 payload mode */
    cThaVtConcateModeDs1    = 2,    /* DS1 payload mode */
    cThaVtConcateModeE1     = 3     /* E1 payload mode */
    }eThaVtConcatePayloadType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleConcate ThaModuleConcateNew(AtDevice device);

/* Channel ID conversion */
eAtRet ThaSdhChannelConcateHwStsGet(AtSdhChannel sdhChannel, uint8 sts1Id, uint16 *hwSts);
eAtRet ThaSdhChannelConcateHwIdGet(AtSdhChannel sdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt);
eAtRet ThaPdhChannelConcateHwIdGet(AtPdhChannel pdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt);

/* Binding channel */
eAtRet ThaModuleConcateBindVc1xToSinkConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group);
eAtRet ThaModuleConcateBindHoVcToSinkConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group);
eAtRet ThaModuleConcateBindTu3VcToSinkConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group);
eAtRet ThaModuleConcateBindDe3ToSinkConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group);
eAtRet ThaModuleConcateBindDe1ToSinkConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group);
eAtRet ThaModuleConcateBindVc1xToSourceConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group);
eAtRet ThaModuleConcateBindHoVcToSourceConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group);
eAtRet ThaModuleConcateBindTu3VcToSourceConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group);
eAtRet ThaModuleConcateBindDe3ToSourceConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group);
eAtRet ThaModuleConcateBindDe1ToSourceConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group);

/* Member add/remove */
eAtRet ThaModuleConcateSourceMemberAdd(ThaModuleConcate self, ThaVcgMember member);
eAtRet ThaModuleConcateSourceMemberRemove(ThaModuleConcate self, ThaVcgMember member);
eAtRet ThaModuleConcateSinkMemberAdd(ThaModuleConcate self, ThaVcgMember member, eBool lcasEnable);
eAtRet ThaModuleConcateSinkMemberRemove(ThaModuleConcate self, ThaVcgMember member);

/* Utils */
void ThaModuleConcateCfgFifoFlush(AtModuleConcate self, eBool en);
uint32 ThaSdhChannelConcateDefaultOffset(AtSdhChannel sdhVc);

eBool ThaConcateGroupCanBeCreated(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType);
void ThaGlobalConcateTypeSet(AtModuleConcate self, eAtConcateGroupType concateType);

/* Member payload */
eAtRet ThaModuleConcateSourceMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable);
eAtRet ThaModuleConcateSinkMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable);
eAtRet ThaModuleConcateSourceMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable);
eAtRet ThaModuleConcateSinkMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable);
eAtRet ThaModuleConcateSourceMemberPdhDe1Set(ThaModuleConcate self, AtConcateMember member);
eAtRet ThaModuleConcateSinkMemberPdhDe1Set(ThaModuleConcate self, AtConcateMember member);
eAtRet ThaModuleConcateSourceMemberPdhDe3Set(ThaModuleConcate self, AtConcateMember member);
eAtRet ThaModuleConcateSinkMemberPdhDe3Set(ThaModuleConcate self, AtConcateMember member);

/* Indirect */
eAtRet ThaModuleConcateSourceIndirectRead(AtModuleConcate self, uint8 regType, uint32 address, uint32 *data);
eAtRet ThaModuleConcateSinkIndirectRead(AtModuleConcate self, uint8 regType, uint32 address, uint32 *data);

/* Debug */
void ThaModuleConcateSdhChannelRegsShow(AtSdhChannel sdhChannel);
void ThaModuleConcatePdhChannelRegsShow(AtPdhChannel pdhChannel);
void ThaModuleConcateStsMappingDisplay(AtSdhChannel sdhChannel);
void ThaModuleConcateVc1xMappingDisplay(AtSdhChannel sdhChannel);
void ThaModuleConcatePdhMappingDisplay(AtPdhChannel pdhChannel);
void ThaModuleConcateLongRegDisplay(AtChannel channel, const char* regName, uint32 address);
void ThaModuleConcateRegDisplay(AtChannel channel, const char* regName, uint32 address);

/* Concrete classes */
AtModuleConcate Tha60290081ModuleConcateNew(AtDevice device);

#endif /* _THAMODULECONCATE_H_ */

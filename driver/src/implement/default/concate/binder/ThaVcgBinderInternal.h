/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : Af4VcgBinderInternal.h
 * 
 * Created Date: Jul 22, 2014
 *
 * Description : VCG binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF4VCGBINDERINTERNAL_H_
#define _AF4VCGBINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/concate/binder/AtVcgBinder.h"
#include "../../../../generic/concate/binder/AtVcgBinderInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "ThaVcgBinder.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcgBinderMethods
    {
    uint32 (*DefaultOffset)(ThaVcgBinder self);
    eAtRet (*SinkVcgAssign)(ThaVcgBinder self, AtConcateGroup vcg);
    eAtRet (*SinkVcgDeassign)(ThaVcgBinder self, AtConcateGroup vcg);
    eAtRet (*SourceVcgAssign)(ThaVcgBinder self, AtConcateGroup vcg);
    eAtRet (*SourceVcgDeassign)(ThaVcgBinder self, AtConcateGroup vcg);
    eAtRet (*TxSeqSwControl)(ThaVcgBinder self, eBool swControlEnabled);

    /* For debug purpose */
    void (*ChannelRegsShow)(ThaVcgBinder self);
    void (*IdMappingShow)(ThaVcgBinder self);
    }tThaVcgBinderMethods;

typedef struct tThaVcgBinder
    {
    tAtVcgBinder super;
    const tThaVcgBinderMethods *methods;
    }tThaVcgBinder;

typedef struct tThaVcgBinderPdhDe1
    {
    tAtVcgBinderPdhDe1 super;
    }tThaVcgBinderPdhDe1;

typedef struct tThaVcgBinderPdhDe3
    {
    tAtVcgBinderPdhDe3 super;
    }tThaVcgBinderPdhDe3;

typedef struct tThaVcgBinderSdhVc
    {
    tAtVcgBinderSdhVc super;
    }tThaVcgBinderSdhVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaVcgBinder ThaVcgBinderObjectInit(ThaVcgBinder self, AtChannel channel);

#endif /* _AF4VCGBINDERINTERNAL_H_ */

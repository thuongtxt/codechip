/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : ThaVcgBinder.c
 *
 * Created Date: Jul 22, 2014
 *
 * Description : VCG binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtVcg.h"
#include "../../../../generic/concate/AtConcateGroupInternal.h"
#include "../member/ThaVcgMemberInternal.h"
#include "../ThaModuleConcate.h"
#include "../ThaModuleConcateReg.h"
#include "ThaVcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaVcgBinder)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVcgBinderMethods m_methods;

/* Override */
static tAtVcgBinderMethods m_AtVcgBinderOverride;

/* Save super implementation */
static const tAtVcgBinderMethods *m_AtVcgBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(ThaVcgBinder self)
    {
    AtUnused(self);
    return 0xDEADCAFE;
    }

static eAtRet SinkVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    uint32 regAddr = cAf4RegMapSnkMemberCtrlCfg + mMethodsGet(self)->DefaultOffset(self);
    AtConcateMember member = AtVcgBinderSinkMemberGet((AtVcgBinder)self);
    AtModuleConcate concateModule = (AtModuleConcate)AtChannelModuleGet((AtChannel)member);
    uint32 regVal;

    ThaModuleConcateSinkIndirectRead(concateModule, cAf4IndrSinkRegTypeMembCtrl, regAddr, &regVal);

    mFieldIns(&regVal, cAf4SkG8040MdMask, cAf4SkG8040MdShift, AtConcateGroupIsNoneVcatG8040(group) ? 0x1 : 0x0);
    mFieldIns(&regVal, cAf4SkVcatMdMask, cAf4SkVcatMdShift, AtConcateGroupIsVcatVcg(group) ? 1 : 0);
    mFieldIns(&regVal, cAf4SkLcasMdMask, cAf4SkLcasMdShift, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
    mFieldIns(&regVal, cAf4SkMemEnaMask, cAf4SkMemEnaShift, 0x1);
    mFieldIns(&regVal, cAf4SkVcgIdMask, cAf4SkVcgIdShift, AtChannelIdGet((AtChannel)group));
    AtVcgBinderWrite((AtVcgBinder)self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceVcgAssign(ThaVcgBinder self, AtConcateGroup group)
    {
    uint32 regAddr  = cAf4RegMapMemberCtrlCfg + mMethodsGet(self)->DefaultOffset(self);
    AtConcateMember member = AtVcgBinderSourceMemberGet((AtVcgBinder)self);
    AtModuleConcate concateModule = (AtModuleConcate)AtChannelModuleGet((AtChannel)member);
    eBool swControlEnabled = (AtConcateGroupIsLcasVcg(group) ? cAtFalse : (AtConcateGroupIsVcatVcg(group) ? cAtTrue : cAtFalse));
    uint32 regVal;

    ThaModuleConcateSourceIndirectRead(concateModule, cAf4IndrSourceRegTypeMembCtrl, regAddr, &regVal);
    mFieldIns(&regVal, cAf4SoG8040MdMask, cAf4SoG8040MdShift, AtConcateGroupIsNoneVcatG8040(group) ? 0x1 : 0x0);
    mFieldIns(&regVal, cAf4SoVcatMdMask, cAf4SoVcatMdShift, AtConcateGroupIsVcatVcg(group) ? 1 : 0);
    mFieldIns(&regVal, cAf4SoLcasMdMask, cAf4SoLcasMdShift, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
    mFieldIns(&regVal, cAf4SoMemEnaMask, cAf4SoMemEnaShift, 0x1);
    mFieldIns(&regVal, cAf4SoVcgIdMask, cAf4SoVcgIdShift, AtChannelIdGet((AtChannel)group));

    /* Enable Add command */
    mFieldIns(&regVal, cAf4SoNmsCmdMask, cAf4SoNmsCmdShift, mBoolToBin(swControlEnabled));
    AtVcgBinderWrite((AtVcgBinder)self, regAddr, regVal, cAtModuleConcate);

    /* Enable to control sequence in VCAT mode */
    return ThaVcgBinderTxSeqSwControl(self, swControlEnabled);
    }

static AtConcateMember SinkProvision(AtVcgBinder self, AtConcateGroup group)
    {
    AtConcateMember newMember = m_AtVcgBinderMethods->SinkProvision(self, group);
    if (newMember)
        mMethodsGet(mThis(self))->SinkVcgAssign(mThis(self), group);
    return newMember;
    }

static AtConcateMember SourceProvision(AtVcgBinder self, AtConcateGroup group)
    {
    AtConcateMember newMember = m_AtVcgBinderMethods->SourceProvision(self, group);
    if (newMember)
        mMethodsGet(mThis(self))->SourceVcgAssign(mThis(self), group);
    return newMember;
    }

static eAtRet SourceVcgDeassign(ThaVcgBinder self, AtConcateGroup group)
    {
    AtVcgBinder binder = (AtVcgBinder)self;
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);

    AtUnused(group);

    AtVcgBinderWrite(binder, cAf4RegMapMemberCtrlCfg + offset, 0x0, cAtModuleConcate);
    AtVcgBinderWrite(binder, cAf4RegMapSourceMst + offset, 0x0, cAtModuleConcate);
    AtVcgBinderWrite(binder, cAf4RegMapSoExpSeq + offset, 0x0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceDeProvision(AtVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret;

    ret = mMethodsGet(mThis(self))->SourceVcgDeassign(mThis(self), group);
    if (ret != cAtOk)
        return ret;

    return m_AtVcgBinderMethods->SourceDeProvision(self, group);
    }

static eAtRet SinkVcgDeassign(ThaVcgBinder self, AtConcateGroup group)
    {
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);

    AtUnused(group);

    AtVcgBinderWrite((AtVcgBinder)self, cAf4RegMapSnkMemberCtrlCfg + offset, 0x0, cAtModuleConcate);
    AtVcgBinderWrite((AtVcgBinder)self, cAf4RegMapSnkMst + offset, 0x0, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkDeProvision(AtVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret = m_AtVcgBinderMethods->SinkDeProvision(self, group);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->SinkVcgDeassign(mThis(self), group);
    }

static eAtRet TxSeqSwControl(ThaVcgBinder self, eBool swControlEnabled)
    {
    uint32 regAddr, regVal;
    eAtRet ret;
    AtModuleConcate concateModule = (AtModuleConcate)AtChannelModuleGet((AtChannel)self);
    regAddr = cAf4RegMapSoExpSeq + mMethodsGet(self)->DefaultOffset(self);
    ret = ThaModuleConcateSourceIndirectRead(concateModule, cAf4IndrSourceRegTypeTxSeq, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    mFieldIns(&regVal, cAf4SoMemExpSeqEnMask, cAf4SoMemExpSeqEnShift, mBoolToBin(swControlEnabled));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static void ChannelRegsShow(ThaVcgBinder self)
    {
    mMethodsGet(self)->IdMappingShow(self);
    }

static void IdMappingShow(ThaVcgBinder self)
    {
    AtUnused(self);
    }

static void MethodsInit(ThaVcgBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, SinkVcgAssign);
        mMethodOverride(m_methods, SourceVcgAssign);
        mMethodOverride(m_methods, SourceVcgDeassign);
        mMethodOverride(m_methods, SinkVcgDeassign);
        mMethodOverride(m_methods, TxSeqSwControl);
        mMethodOverride(m_methods, ChannelRegsShow);
        mMethodOverride(m_methods, IdMappingShow);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtVcgBinder(ThaVcgBinder self)
    {
    AtVcgBinder binder = (AtVcgBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtVcgBinderMethods = mMethodsGet(binder);
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgBinderOverride, m_AtVcgBinderMethods, sizeof(m_AtVcgBinderOverride));

        mMethodOverride(m_AtVcgBinderOverride, SinkProvision);
        mMethodOverride(m_AtVcgBinderOverride, SourceProvision);
        mMethodOverride(m_AtVcgBinderOverride, SourceDeProvision);
        mMethodOverride(m_AtVcgBinderOverride, SinkDeProvision);
        }

    mMethodsSet(binder, &m_AtVcgBinderOverride);
    }

static void Override(ThaVcgBinder self)
    {
    OverrideAtVcgBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcgBinder);
    }

ThaVcgBinder ThaVcgBinderObjectInit(ThaVcgBinder self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtVcgBinderObjectInit((AtVcgBinder)self, channel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet ThaVcgBinderSinkVcgAssign(ThaVcgBinder self, AtConcateGroup vcg)
    {
    if (self)
        return mMethodsGet(self)->SinkVcgAssign(self, vcg);
    return cAtErrorNullPointer;
    }

eAtRet ThaVcgBinderSinkVcgDeassign(ThaVcgBinder self, AtConcateGroup vcg)
    {
    if (self)
        return mMethodsGet(self)->SinkVcgDeassign(self, vcg);
    return cAtErrorNullPointer;
    }

eAtRet ThaVcgBinderSourceVcgAssign(ThaVcgBinder self, AtConcateGroup vcg)
    {
    if (self)
        return mMethodsGet(self)->SourceVcgAssign(self, vcg);
    return cAtErrorNullPointer;
    }

eAtRet ThaVcgBinderSourceVcgDeassign(ThaVcgBinder self, AtConcateGroup vcg)
    {
    if (self)
        return mMethodsGet(self)->SourceVcgDeassign(self, vcg);
    return cAtErrorNullPointer;
    }

eAtRet ThaVcgBinderTxSeqSwControl(ThaVcgBinder self, eBool swControlEnabled)
    {
    if (self)
        return mMethodsGet(self)->TxSeqSwControl(self, swControlEnabled);
    return cAtErrorObjectNotExist;
    }

void ThaVcgBinderChannelRegsShow(ThaVcgBinder self)
    {
    if (self)
        mMethodsGet(self)->ChannelRegsShow(self);
    }

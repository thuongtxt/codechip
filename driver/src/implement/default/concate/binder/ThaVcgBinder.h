/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : ThaVcgBinder.h
 * 
 * Created Date: Jul 22, 2014
 *
 * Description : VCG binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCGBINDER_H_
#define _THAVCGBINDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConcateGroup.h"
#include "AtModuleSdh.h"
#include "AtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcgBinder * ThaVcgBinder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/* Concrete binders */
/* NOTE: These default binders do not inherit from ThaVcgBinder */
AtVcgBinder ThaVcgBinderSdhVcNew(AtSdhVc vc);
AtVcgBinder ThaVcgBinderPdhDe3New(AtPdhDe3 de3);
AtVcgBinder ThaVcgBinderPdhDe1New(AtPdhDe1 de1);

/* For classes inherited from ThaVcgBinder */
ThaVcgBinder Tha60210012VcgBinderPdhDe1New(AtPdhDe1 de1);
ThaVcgBinder Tha60210012VcgBinderPdhDe3New(AtPdhDe3 de3);

eAtRet ThaVcgBinderSinkVcgAssign(ThaVcgBinder self, AtConcateGroup vcg);
eAtRet ThaVcgBinderSinkVcgDeassign(ThaVcgBinder self, AtConcateGroup vcg);

eAtRet ThaVcgBinderSourceVcgAssign(ThaVcgBinder self, AtConcateGroup vcg);
eAtRet ThaVcgBinderSourceVcgDeassign(ThaVcgBinder self, AtConcateGroup vcg);
eAtRet ThaVcgBinderTxSeqSwControl(ThaVcgBinder self, eBool swControlEnabled);
void ThaVcgBinderChannelRegsShow(ThaVcgBinder self);

#endif /* _THAVCGBINDER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : ThaVcgBinderPdhDe3.c
 *
 * Created Date: Jul 22, 2014
 *
 * Description : Default DE3 VCG binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe3.h"
#include "../ThaModuleConcate.h"
#include "ThaVcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtVcgBinderMethods  m_AtVcgBinderOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void OverrideAtVcgBinder(AtVcgBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgBinderOverride, mMethodsGet(self), sizeof(m_AtVcgBinderOverride));
        }

    mMethodsSet(self, &m_AtVcgBinderOverride);
    }

static void Override(AtVcgBinder self)
    {
    OverrideAtVcgBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcgBinderPdhDe3);
    }

static AtVcgBinder ObjectInit(AtVcgBinder self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtVcgBinderPdhDe3ObjectInit(self, de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtVcgBinder ThaVcgBinderPdhDe3New(AtPdhDe3 de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtVcgBinder newBinder = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBinder == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBinder, de3);
    }

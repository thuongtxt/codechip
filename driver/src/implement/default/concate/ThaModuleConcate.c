/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : ThaModuleConcate.c
 *
 * Created Date: May 7, 2014
 *
 * Description : Concate module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtVcg.h"
#include "AtSdhChannel.h"
#include "AtPdhDe3.h"
#include "AtPdhDe1.h"
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../generic/concate/AtConcateGroupInternal.h"
#include "../../../generic/pdh/AtPdhChannelInternal.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../default/sdh/ThaModuleSdh.h"
#include "../../default/ocn/ThaModuleOcn.h"
#include "../../default/man/ThaDevice.h"
#include "../pdh/ThaModulePdh.h"
#include "binder/ThaVcgBinder.h"
#include "member/ThaVcgMember.h"
#include "ThaModuleConcateInternal.h"
#include "ThaModuleConcateReg.h"
#include "ThaVcg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaModuleConcate)self)
#define cIndrTimeOut  100 /* 1 ms */
#define cRmvTimeOutMs 2000
#define cWaitTimeOut  200

#define cAf4NumStsInOc48     48
#define cAf4NumVtgInSts      7
#define cAf4NumVtInVtg       4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleConcateMethods m_methods;

/* Override */
static tAtModuleMethods        m_AtModuleOverride;
static tAtModuleConcateMethods m_AtModuleConcateOverride;

/* Save super implementation */
static const tAtModuleMethods        *m_AtModuleMethods        = NULL;
static const tAtModuleConcateMethods *m_AtModuleConcateMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleConcate ModuleConcate(AtChannel channel)
    {
    return (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cAtModuleConcate);
    }

static uint32 RegTypeToIndrCtrlAddress(uint8 regType)
    {
    switch(regType)
        {
        case cAf4IndrSinkRegTypeMembStat:
            return cAf4RegSinkMembStatIndirectCtrl;

        case cAf4IndrSinkRegTypeMembCtrl:
            return cAf4RegSinkMembCtrlIndirectCtrl;

        default:
            return 0xCAFECAFE;
        }
    }

static uint32 RegTypeToIndrDataAddress(uint8 regType)
    {
    switch(regType)
        {
        case cAf4IndrSinkRegTypeMembStat:
            return cAf4RegSinkMembStatIndirectData;

        case cAf4IndrSinkRegTypeMembCtrl:
            return cAf4RegSinkMembCtrlIndirectData;

        default:
            return 0xCAFECAFE;
        }
    }

static eBool IsNonVcatConcateType(eAtConcateGroupType concateType)
    {
    if ((concateType == cAtConcateGroupTypeNVcat)      ||
        (concateType == cAtConcateGroupTypeNVcat_g804) ||
        (concateType == cAtConcateGroupTypeNVcat_g8040))
        return cAtTrue;
    return cAtFalse;
    }

static eBool NoneVcatModeIsUsed(AtModuleConcate self)
    {
    uint32 regVal = mModuleHwRead(self, cAf4RegMapMastReg);
    return (regVal & cAf4NonVcatMdMask) ? cAtTrue : cAtFalse;
    }

static eAtRet DefaultSet(ThaModuleConcate self)
    {
    uint32 sts_i, vtg_i, vt_i;
    uint32 offset;

    for (sts_i = 0; sts_i < cAf4NumStsInOc48; sts_i ++)
        {
        mModuleHwWrite(self, cAf4RegMapSourceModeCfg(sts_i), cAf4RegMapSourceModeCfgRstVal);
        mModuleHwWrite(self, cAf4RegMapSnkModeCfg(sts_i), cAf4RegMapSnkModeCfgRstVal);
        }

    for(sts_i = 0; sts_i < cAf4NumStsInOc48; sts_i ++)
        {
        for(vtg_i = 0; vtg_i < cAf4NumVtgInSts; vtg_i ++)
            {
            for(vt_i = 0; vt_i < cAf4NumVtInVtg; vt_i ++)
                {
                offset = (sts_i * 32) + (vtg_i * 4) + vt_i;
                mModuleHwWrite(self, cAf4RegMapMemberCtrlCfg + offset, cAf4RegMapMemberCtrlCfgRstVal);
                mModuleHwWrite(self, cAf4RegMapSourceMst + offset,cAf4RegMapSourceMstRstVal);
                mModuleHwWrite(self, cAf4RegMapSnkMemberCtrlCfg + offset, cAf4RegMapSnkMemberCtrlCfgRstVal);
                mModuleHwWrite(self, cAf4RegMapSnkMst + offset, cAf4RegMapSnkModeCfgRstVal);
                mModuleHwWrite(self, cAf4RegMapSoExpSeq + offset, cAf4RegMapSoExpSeqRstVal);
                }
            }
        }

    mModuleHwWrite(self, 0x720104, 0xFF1F);
    mModuleHwWrite(self, 0x741000, 0x1);
    mModuleHwWrite(self, 0x510000, 0x0);

    return cAtOk;
    }

static eAtRet HwActivate(AtModule self, eBool en)
    {
    uint32 regVal = mModuleHwRead(self, cAf4RegMapMastReg);
    mFieldIns(&regVal, cAf4ActMask, cAf4ActShift, en == cAtTrue ? 0x1 : 0x0);
    mModuleHwWrite(self, cAf4RegMapMastReg, regVal);
    return cAtOk;
    }

static eAtRet Activate(AtModule self)
    {
    return HwActivate(self, cAtTrue);
    }

static eAtRet Deactivate(AtModule self)
    {
    return HwActivate(self, cAtFalse);
    }

static eBool IsActive(AtModule self)
    {
    uint32 regVal = mModuleHwRead(self, cAf4RegMapMastReg);
    return mRegField(regVal, cAf4Act) ? cAtTrue : cAtFalse;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtModuleDeactivate(self);
    ret |= mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    ret |= AtModuleActivate(self);

    return cAtOk;
    }

static eBool GroupTypeIsSupported(AtModuleConcate self, eAtConcateGroupType concateType)
    {
    AtUnused(self);
    if ((concateType == cAtConcateGroupTypeVcat) || (concateType == cAtConcateGroupTypeNVcat))
        return cAtTrue;
    return cAtFalse;
    }

static eBool GroupMemberTypeIsSupported(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);

    switch ((uint32)memberType)
        {
        case cAtConcateMemberTypeVc4:  return cAtTrue;
        case cAtConcateMemberTypeVc3:  return cAtTrue;
        case cAtConcateMemberTypeVc12: return cAtTrue;
        case cAtConcateMemberTypeVc11: return cAtTrue;
        default: return cAtFalse;
        }
    }

static AtConcateGroup NonVcatVcgObjectCreate(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    AtConcateGroup newGroup;

    newGroup = ThaVcgNonVcatNew(self, vcgId, memberType, concateType);
    if (newGroup)
        ThaGlobalConcateTypeSet(self, concateType);

    return newGroup;
    }

static AtConcateGroup VcatVcgObjectCreate(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType)
    {
    AtConcateGroup newGroup;

    newGroup = (AtConcateGroup)ThaVcgVcatNew(self, vcgId, memberType);
    if (newGroup)
        ThaGlobalConcateTypeSet(self, cAtConcateGroupTypeVcat);

    return newGroup;
    }

static uint32 MaxGroupGet(AtModuleConcate self)
    {
    AtUnused(self);
    return 48;
    }

static AtVcgBinder CreateVcgBinderForVc(AtModuleConcate self, AtSdhVc vc)
    {
    AtUnused(self);
    return (AtVcgBinder)ThaVcgBinderSdhVcNew(vc);
    }

static AtVcgBinder CreateVcgBinderForDe1(AtModuleConcate self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return ThaVcgBinderPdhDe1New(de1);
    }

static AtVcgBinder CreateVcgBinderForDe3(AtModuleConcate self, AtPdhDe3 de3)
    {
    AtUnused(self);
    return ThaVcgBinderPdhDe3New(de3);
    }

static eBool IsVcgFixedToGfpChannel(AtModuleConcate self)
    {
    AtUnused(self);
    /* Default implement assigns a fixed binding between GFP channel and a
     * concatenation group. */
    return cAtTrue;
    }

static AtConcateMember CreateConcateMemberForSdhVc(AtModuleConcate self, AtSdhVc vc, AtConcateGroup group)
    {
    uint8 channelType = AtSdhChannelTypeGet((AtSdhChannel)vc);

    if ((channelType == cAtSdhChannelTypeVc12) ||
        (channelType == cAtSdhChannelTypeVc11))
        return mMethodsGet(mThis(self))->CreateVcgMemberObjectForSdhVc1x(mThis(self), vc, group);

    return mMethodsGet(mThis(self))->CreateVcgMemberObjectForSdhVc(mThis(self), vc, group);
    }

static AtConcateMember CreateConcateMemberForPdhDe1(AtModuleConcate self, AtPdhDe1 de1, AtConcateGroup group)
    {
    AtUnused(self);
    return ThaVcgMemberPdhDe1New(group, (AtChannel)de1);
    }

static AtConcateMember CreateConcateMemberForPdhDe3(AtModuleConcate self, AtPdhDe3 de3, AtConcateGroup group)
    {
    AtUnused(self);
    return ThaVcgMemberPdhDe3New(group, (AtChannel)de3);
    }

static eBool CanCreateGroup(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType, eAtConcateMemberType memberType)
    {
    AtUnused(memberType);
    return ThaConcateGroupCanBeCreated(self, vcgId, concateType);
    }

static AtConcateMember CreateVcgMemberObjectForSdhVc(ThaModuleConcate self, AtSdhVc vc, AtConcateGroup group)
    {
    AtUnused(self);
    return ThaVcgMemberSdhVcNew(group, (AtChannel)vc);
    }

static AtConcateMember CreateVcgMemberObjectForSdhVc1x(ThaModuleConcate self, AtSdhVc vc1x, AtConcateGroup group)
    {
    AtUnused(self);
    return ThaVcgMemberSdhVc1xNew(group, (AtChannel)vc1x);
    }

static void OverrideAtModule(AtModuleConcate self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, Activate);
        mMethodOverride(m_AtModuleOverride, Deactivate);
        mMethodOverride(m_AtModuleOverride, IsActive);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleConcate(AtModuleConcate self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleConcateMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleConcateOverride, m_AtModuleConcateMethods, sizeof(m_AtModuleConcateOverride));

        mMethodOverride(m_AtModuleConcateOverride, GroupMemberTypeIsSupported);
        mMethodOverride(m_AtModuleConcateOverride, GroupTypeIsSupported);
        mMethodOverride(m_AtModuleConcateOverride, MaxGroupGet);
        mMethodOverride(m_AtModuleConcateOverride, NonVcatVcgObjectCreate);
        mMethodOverride(m_AtModuleConcateOverride, VcatVcgObjectCreate);
        mMethodOverride(m_AtModuleConcateOverride, CreateVcgBinderForVc);
        mMethodOverride(m_AtModuleConcateOverride, CreateVcgBinderForDe1);
        mMethodOverride(m_AtModuleConcateOverride, CreateVcgBinderForDe3);
        mMethodOverride(m_AtModuleConcateOverride, IsVcgFixedToGfpChannel);
        mMethodOverride(m_AtModuleConcateOverride, CreateConcateMemberForSdhVc);
        mMethodOverride(m_AtModuleConcateOverride, CreateConcateMemberForPdhDe1);
        mMethodOverride(m_AtModuleConcateOverride, CreateConcateMemberForPdhDe3);
        mMethodOverride(m_AtModuleConcateOverride, CanCreateGroup);
        }

    mMethodsSet(self, &m_AtModuleConcateOverride);
    }

static void Override(AtModuleConcate self)
    {
	OverrideAtModule(self);
	OverrideAtModuleConcate(self);
    }

static uint32 HoVcDefaultOffset(ThaModuleConcate self, AtChannel hoVc)
    {
    AtUnused(self);
    return ThaSdhChannelConcateDefaultOffset((AtSdhChannel)hoVc);
    }

static uint32 Tu3VcDefaultOffset(ThaModuleConcate self, AtChannel tu3Vc)
    {
    AtUnused(self);
    return ThaSdhChannelConcateDefaultOffset((AtSdhChannel)tu3Vc);
    }

static uint32 Vc1xDefaultOffset(ThaModuleConcate self, AtChannel vc1x)
    {
    AtUnused(self);
    return ThaSdhChannelConcateDefaultOffset((AtSdhChannel)vc1x);
    }

static uint32 De1DefaultOffset(ThaModuleConcate self, AtChannel de1)
    {
    AtSdhChannel vc1x = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
    AtUnused(self);

    if (vc1x)
        return ThaSdhChannelConcateDefaultOffset(vc1x);

    return 0xDEADCAFE;
    }

static uint32 De3DefaultOffset(ThaModuleConcate self, AtChannel de3)
    {
    AtSdhChannel vc3 = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3);
    AtUnused(self);

    if (vc3)
        return ThaSdhChannelConcateDefaultOffset(vc3);

    return 0xDEADCAFE;
    }

static eAtRet BindChannelToSinkConcateGroup(ThaModuleConcate self, AtChannel channel,
                                            AtConcateGroup group,
                                            uint32 (*ChannelOffset)(ThaModuleConcate, AtChannel))
    {
    uint32 offset = ChannelOffset(self, channel);

    if (group)
        {
        uint32 regAddr = cAf4RegMapSnkMemberCtrlCfg + ChannelOffset(self, channel);
        uint32 regVal;
        eAtRet ret;

        ret = ThaModuleConcateSinkIndirectRead((AtModuleConcate)self, cAf4IndrSinkRegTypeMembCtrl, regAddr, &regVal);
        if (ret != cAtOk)
            return ret;

        mFieldIns(&regVal, cAf4SkG8040MdMask, cAf4SkG8040MdShift, AtConcateGroupIsNoneVcatG8040(group) ? 0x1 : 0x0);
        mFieldIns(&regVal, cAf4SkVcatMdMask, cAf4SkVcatMdShift, AtConcateGroupIsVcatVcg(group) ? 1 : 0);
        mFieldIns(&regVal, cAf4SkLcasMdMask, cAf4SkLcasMdShift, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
        mFieldIns(&regVal, cAf4SkMemEnaMask, cAf4SkMemEnaShift, 0x1);
        mFieldIns(&regVal, cAf4SkVcgIdMask, cAf4SkVcgIdShift, AtChannelIdGet((AtChannel)group));
        mChannelHwWrite(channel, regAddr, regVal, cAtModuleConcate);
        }
    else
        {
        mChannelHwWrite(channel, cAf4RegMapSnkMemberCtrlCfg + offset, 0x0, cAtModuleConcate);
        mChannelHwWrite(channel, cAf4RegMapSnkMst + offset, 0x0, cAtModuleConcate);
        }

    return cAtOk;
    }

static eAtRet BindVc1xToSinkConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group)
    {
    return BindChannelToSinkConcateGroup(self, vc1x, group, mMethodsGet(self)->Vc1xDefaultOffset);
    }

static eBool IsVc4(AtSdhChannel channel)
    {
    return (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc4) ? cAtTrue : cAtFalse;
    }

static eAtRet BindHoVcToSinkConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    eAtRet ret;
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint32 regVal, regAddr;
    uint16 mapSts;

    ret = BindChannelToSinkConcateGroup(self, hoVc, group, mMethodsGet(self)->HoVcDefaultOffset);
    if (ret != cAtOk)
        return ret;

    /* Default implementation does not support rate higher than VC-4 */
    if (!IsVc4(channel))
        return cAtOk;

    ret = ThaSdhChannelConcateHwStsGet(channel, AtSdhChannelSts1Get(channel), &mapSts);
    if (ret != cAtOk)
        return ret;

    if (group)
        {
        regAddr = cAf4RegMapSkVc4Ctrl(mapSts);
        regVal  = mChannelHwRead(hoVc, regAddr, cAtModuleConcate);
        mFieldIns(&regVal, cAf4RegMapSkVc4CtrlEnMask(mapSts), cAf4RegMapSkVc4CtrlEnShift(mapSts), IsVc4(channel) ? 0x1 : 0x0);
        mChannelHwWrite(hoVc, regAddr, regVal, cAtModuleConcate);
        }
    else
        {
        uint8 idx;

        for (idx = 0; idx < 3; idx++)
            {
            uint32 nextMapSts = mapSts + (idx * 16UL);
            uint32 offset = (nextMapSts * 32);
            mChannelHwWrite(hoVc, cAf4RegMapSnkMemberCtrlCfg + offset, 0x0, cAtModuleConcate);
            mChannelHwWrite(hoVc, cAf4RegMapSnkMst + offset, 0x0, cAtModuleConcate);
            }
        }

    return cAtOk;
    }

static eAtRet BindTu3VcToSinkConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group)
    {
    return BindChannelToSinkConcateGroup(self, tu3Vc, group, mMethodsGet(self)->Tu3VcDefaultOffset);
    }

static eAtRet BindDe3ToSinkConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group)
    {
    return BindChannelToSinkConcateGroup(self, de3, group, mMethodsGet(self)->De3DefaultOffset);
    }

static eAtRet BindDe1ToSinkConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group)
    {
    return BindChannelToSinkConcateGroup(self, de1, group, mMethodsGet(self)->De1DefaultOffset);
    }

static eAtRet BindChannelToSourceConcateGroup(ThaModuleConcate self, AtChannel channel,
                                              AtConcateGroup group,
                                              uint32 (*ChannelOffset)(ThaModuleConcate, AtChannel))
    {
    uint32 offset = ChannelOffset(self, channel);

    if (group)
        {
        uint32 regAddr  = cAf4RegMapMemberCtrlCfg + offset;
        uint32 regVal;
        eBool swControlEnabled = (AtConcateGroupIsLcasVcg(group) ? cAtFalse : (AtConcateGroupIsVcatVcg(group) ? cAtTrue : cAtFalse));
        eAtRet ret;

        ret = ThaModuleConcateSourceIndirectRead((AtModuleConcate)self, cAf4IndrSourceRegTypeMembCtrl, regAddr, &regVal);
        if (ret != cAtOk)
            return ret;

        mFieldIns(&regVal, cAf4SoG8040MdMask, cAf4SoG8040MdShift, AtConcateGroupIsNoneVcatG8040(group) ? 0x1 : 0x0);
        mFieldIns(&regVal, cAf4SoVcatMdMask, cAf4SoVcatMdShift, AtConcateGroupIsVcatVcg(group) ? 1 : 0);
        mFieldIns(&regVal, cAf4SoLcasMdMask, cAf4SoLcasMdShift, AtConcateGroupIsLcasVcg(group) ? 1 : 0);
        mFieldIns(&regVal, cAf4SoMemEnaMask, cAf4SoMemEnaShift, 0x1);
        mFieldIns(&regVal, cAf4SoVcgIdMask, cAf4SoVcgIdShift, AtChannelIdGet((AtChannel)group));

        /* Enable Add command */
        mFieldIns(&regVal, cAf4SoNmsCmdMask, cAf4SoNmsCmdShift, mBoolToBin(swControlEnabled));
        mChannelHwWrite(channel, regAddr, regVal, cAtModuleConcate);
        }
    else
        {
        mChannelHwWrite(channel, cAf4RegMapMemberCtrlCfg + offset, 0x0, cAtModuleConcate);
        mChannelHwWrite(channel, cAf4RegMapSourceMst + offset, 0x0, cAtModuleConcate);
        mChannelHwWrite(channel, cAf4RegMapSoExpSeq + offset, 0x0, cAtModuleConcate);
        }

    return cAtOk;
    }

static eAtRet BindVc1xToSourceConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group)
    {
    return BindChannelToSourceConcateGroup(self, vc1x, group, mMethodsGet(self)->Vc1xDefaultOffset);
    }

static eAtRet BindHoVcToSourceConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    eAtRet ret;
    AtSdhChannel channel = (AtSdhChannel)hoVc;
    uint32 regVal;
    uint16 mapSts;

    ret = BindChannelToSourceConcateGroup(self, hoVc, group, mMethodsGet(self)->HoVcDefaultOffset);
    if (ret != cAtOk)
        return ret;

    /* Default implementation does not support rate higher than VC-4 */
    if (!IsVc4(channel))
        return cAtOk;

    ret = ThaSdhChannelConcateHwStsGet(channel, AtSdhChannelSts1Get(channel), &mapSts);
    if (ret != cAtOk)
        return ret;

    if (group)
        {
        regVal  = mChannelHwRead(hoVc, cAf4RegMapSoVc4Ctrl, cAtModuleConcate);
        mFieldIns(&regVal, cAf4RegMapSoVc4CtrlEnMask(mapSts), cAf4RegMapSoVc4CtrlEnShift(mapSts), IsVc4(channel) ? 0x1 : 0x0);
        mChannelHwWrite(hoVc, cAf4RegMapSoVc4Ctrl, regVal, cAtModuleConcate);
        }
    else
        {
        uint8 idx;

        regVal  = mChannelHwRead(hoVc, cAf4RegMapSoVc4Ctrl, cAtModuleConcate);
        mFieldIns(&regVal, cAf4RegMapSoVc4CtrlEnMask(mapSts), cAf4RegMapSoVc4CtrlEnShift(mapSts), 0x0);
        mChannelHwWrite(hoVc, cAf4RegMapSoVc4Ctrl, regVal, cAtModuleConcate);

        for (idx = 0; idx < 3; idx++)
            {
            uint32 nextMapSts = mapSts + (idx * 16UL);
            uint32 offset = (nextMapSts * 32UL);

            mChannelHwWrite(hoVc, cAf4RegMapMemberCtrlCfg + offset, 0x0, cAtModuleConcate);
            mChannelHwWrite(hoVc, cAf4RegMapSourceMst + offset, 0x0, cAtModuleConcate);
            mChannelHwWrite(hoVc, cAf4RegMapSoExpSeq + offset, 0x0, cAtModuleConcate);
            }
        }

    return cAtOk;
    }

static eAtRet BindTu3VcToSourceConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group)
    {
    return BindChannelToSourceConcateGroup(self, tu3Vc, group, mMethodsGet(self)->Tu3VcDefaultOffset);
    }

static eAtRet BindDe3ToSourceConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group)
    {
    return BindChannelToSourceConcateGroup(self, de3, group, mMethodsGet(self)->De3DefaultOffset);
    }

static eAtRet BindDe1ToSourceConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group)
    {
    return BindChannelToSourceConcateGroup(self, de1, group, mMethodsGet(self)->De1DefaultOffset);
    }

static eAtRet SourceMemberAdd(ThaModuleConcate self, ThaVcgMember member)
    {
    uint32 regAddr, regVal;
    eAtRet ret;

    regAddr = cAf4RegMapMemberCtrlCfg + ThaVcgMemberDefaultOffset(member);
    ret = ThaModuleConcateSourceIndirectRead((AtModuleConcate)self, cAf4IndrSourceRegTypeMembCtrl, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    if (regVal & cAf4SoNmsCmdMask)
        return cAtConcateErrorMemberAdded;

    mFieldIns(&regVal, cAf4SoNmsCmdMask, cAf4SoNmsCmdShift, 0x1);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkMemberAdd(ThaModuleConcate self, ThaVcgMember member, eBool lcasEnable)
    {
    uint32 regVal, regAddr;
    eAtRet ret;

    regAddr = cAf4RegMapSnkMemberCtrlCfg + ThaVcgMemberDefaultOffset(member);

    ret = ThaModuleConcateSinkIndirectRead((AtModuleConcate)self, cAf4IndrSinkRegTypeMembCtrl, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    if ((mRegField(regVal, cAf4SkNmsCmd) == cAf4SkNmsCmdAdd) && lcasEnable)
        return cAtConcateErrorMemberAdded;

    mFieldIns(&regVal, cAf4SkNmsCmdMask, cAf4SkNmsCmdShift, lcasEnable ? cAf4SkNmsCmdAdd : cAf4SkNmsCmdRmv);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);
    return cAtOk;
    }

static eAtRet SinkMemberRemove(ThaModuleConcate self, ThaVcgMember member)
    {
    uint32              regVal, regAddr;
    uint32              elapseTime = 0;
    tAtOsalCurTime      curTime;
    tAtOsalCurTime      startTime;
    uint8               currentCmd;
    eAtLcasMemberSinkState  state;
    AtOsal osal = AtSharedDriverOsalGet();
    eBool isSimulated;
    uint32 offset = ThaVcgMemberDefaultOffset(member);
    AtConcateGroup vcg;
    eAtLcasMemberSinkState  expectedState = cAtLcasMemberSinkStateIdle;
    eAtRet ret;

    /* Take care in case of VCAT non-LCAS mode */
    vcg = AtConcateMemberConcateGroupGet((AtConcateMember)member);
    if ((AtConcateGroupConcatTypeGet(vcg) == cAtConcateGroupTypeVcat) &&
        (!AtVcgLcasIsEnabled((AtVcg)vcg)))
        expectedState = cAtLcasMemberSinkStateFixed;

    /* Make RMV command */
    regAddr = cAf4RegMapSnkMemberCtrlCfg + offset;
    ret = ThaModuleConcateSinkIndirectRead((AtModuleConcate)self, cAf4IndrSinkRegTypeMembCtrl, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    mFieldGet(regVal, cAf4SkNmsCmdMask, cAf4SkNmsCmdShift, uint8, &currentCmd);
    mFieldIns(&regVal, cAf4SkNmsCmdMask, cAf4SkNmsCmdShift, 0x0);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    /* Check member is being removed */
    regAddr = cAf4RegMapSnkMst + offset;
    ret = ThaModuleConcateSinkIndirectRead((AtModuleConcate)self, cAf4IndrSinkRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    state   = mRegField(regVal, cAf4SkMemFsm);

    /* Wait for hardware */
    isSimulated = AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)member));
    if (state != expectedState)
        {
        mMethodsGet(osal)->CurTimeGet(osal, &startTime);
        while (elapseTime < cRmvTimeOutMs)
            {
            ret = ThaModuleConcateSinkIndirectRead((AtModuleConcate)self, cAf4IndrSinkRegTypeMembStat, regAddr, &regVal);
            if (ret != cAtOk)
                return ret;

            state = mRegField(regVal, cAf4SkMemFsm);
            if (state == expectedState)
                break;

            if (isSimulated)
                break;

            /* Retry */
            mMethodsGet(osal)->CurTimeGet(osal, &curTime);
            elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &curTime, &startTime);
            }

        /* Still not done */
        if (state != expectedState)
            return cAtErrorDevBusy;
        }

    /* Continue waiting for other state */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while(elapseTime < cWaitTimeOut)
        {
        regVal = mChannelHwRead(member, cAf4RegMapReadAcsDataHolding2, cAtModuleConcate);

        /* Due to HW have some limitations that need to delay between 2 times of removing member so
           SW just checks 2 HW status bits, if is true, stop to wait. Vice versa, need to wait until
           timeout and don't return error for this */
        if ((regVal & cBit17) && (regVal & cBit16))
            return cAtOk;

        if (isSimulated)
            return cAtOk;

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &curTime, &startTime);
        }

    return cAtOk;
    }

static eAtRet SourceMemberRemove(ThaModuleConcate self, ThaVcgMember member)
    {
    uint32 regVal, regAddr;
    uint32 elapseTime;
    tAtOsalCurTime startTime, curTime;
    eAtLcasMemberSourceState state;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 offset = ThaVcgMemberDefaultOffset(member);
    eAtRet ret;

    regAddr = cAf4RegMapMemberCtrlCfg + offset;
    ret = ThaModuleConcateSourceIndirectRead((AtModuleConcate)self, cAf4IndrSourceRegTypeMembCtrl, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    mFieldIns(&regVal, cAf4SoNmsCmdMask,  cAf4SoNmsCmdShift, 0x0);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    regAddr = cAf4RegMapSourceMst + offset;
    ret = ThaModuleConcateSourceIndirectRead((AtModuleConcate)self, cAf4IndrSourceRegTypeMembStat, regAddr, &regVal);
    if (ret != cAtOk)
        return ret;

    state = mRegField(regVal, cAf4SoMemFsm);
    if (state == cAtLcasMemberSourceStateIdle)
        return cAtOk;

    /* Wait for hardware */
    elapseTime = 0x0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while(elapseTime < cRmvTimeOutMs)
        {
        ret = ThaModuleConcateSourceIndirectRead((AtModuleConcate)self, cAf4IndrSourceRegTypeMembStat, regAddr, &regVal);
        if (ret != cAtOk)
            return ret;

        state = mRegField(regVal, cAf4SoMemFsm);
        if (state == cAtLcasMemberSourceStateIdle)
            return cAtOk;

        if (AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)member)))
            return cAtOk;

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &curTime, &startTime);
        }

    return cAtErrorDevBusy;
    }

static eAtRet SdhChannelStsIdSw2HwGet(ThaModuleConcate self, AtSdhChannel sdhChannel, uint8 swSts, uint16 *hwSts)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    AtUnused(swSts);
    if (hwSts)
        *hwSts = cInvalidUint16;
    return cAtErrorNotImplemented;
    }

static eAtRet PdhChannelHwIdGet(ThaModuleConcate self, AtPdhChannel pdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    AtUnused(self);
    AtUnused(pdhChannel);
    if (hwSts)
        *hwSts = cInvalidUint16;
    if (hwVtg)
        *hwVtg = cInvalidUint8;
    if (hwVt)
        *hwVt = cInvalidUint8;
    return cAtErrorNotImplemented;
    }

static eAtRet SdhChannelHwIdGet(ThaModuleConcate self, AtSdhChannel sdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    eAtRet ret = ThaSdhChannelConcateHwStsGet(sdhChannel, AtSdhChannelSts1Get(sdhChannel), hwSts);
    AtUnused(self);

    if (hwVtg)
        *hwVtg = AtSdhChannelTug2Get(sdhChannel);
    if (hwVt)
        *hwVt  = AtSdhChannelTu1xGet(sdhChannel);
    return ret;
    }

static eBool SlaveStsIdIsValid(ThaModuleConcate self, AtSdhChannel sdhChannel, uint8 swSts)
    {
    uint8 masterSts = AtSdhChannelSts1Get(sdhChannel);
    AtUnused(self);
    if ((swSts < masterSts) || (swSts - masterSts >= AtSdhChannelNumSts(sdhChannel)))
        return cAtFalse;
    return cAtTrue;
    }

static eAtRet SinkVtgTypeSet(ThaModuleConcate self, AtConcateMember member, uint16 stsId, uint8 vtgId, eThaVtConcatePayloadType payloadType)
    {
    uint32 regAddr = cAf4RegMapSnkModeCfg(stsId);
    uint32 regVal  = mChannelHwRead(member, regAddr, cAtModuleConcate);
    AtUnused(self);
    mFieldIns(&regVal, cAf4SkLoMdGrpMask(vtgId), cAf4SkLoMdGrpShift(vtgId), payloadType);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceVtgTypeSet(ThaModuleConcate self, AtConcateMember member, uint16 stsId, uint8 vtgId, eThaVtConcatePayloadType payloadType)
    {
    uint32 regAddr = cAf4RegMapSourceModeCfg(stsId);
    uint32 regVal  = mChannelHwRead(member, regAddr, cAtModuleConcate);
    AtUnused(self);
    mFieldIns(&regVal, cAf4SoLoMdGrpMask(vtgId), cAf4SoLoMdGrpShift(vtgId), payloadType);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceStsPayloadTypeSet(ThaModuleConcate self, AtConcateMember member, uint16 stsId, eThaStsConcatePayloadType payloadType)
    {
    uint32 regAddr = cAf4RegMapSourceModeCfg(stsId);
    uint32 regVal  = mChannelHwRead(member, regAddr, cAtModuleConcate);
    AtUnused(self);
    mFieldIns(&regVal, cAf4SoHoMdMask, cAf4SoHoMdShift, payloadType);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SinkStsPayloadTypeSet(ThaModuleConcate self, AtConcateMember member, uint16 stsId, eThaStsConcatePayloadType payloadType)
    {
    uint32 regAddr = cAf4RegMapSnkModeCfg(stsId);
    uint32 regVal  = mChannelHwRead(member, regAddr, cAtModuleConcate);
    AtUnused(self);
    mFieldIns(&regVal, cAf4SkHoMdMask, cAf4SkHoMdShift, payloadType);
    mChannelHwWrite(member, regAddr, regVal, cAtModuleConcate);

    return cAtOk;
    }

static eAtRet SourceMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel channel;
    uint8 stsId, numSts, sts_i;
    uint16 hwSts;
    AtUnused(enable);

    channel = (AtSdhChannel)AtConcateMemberChannelGet(member);
    stsId = AtSdhChannelSts1Get(channel);
    numSts = AtSdhChannelNumSts(channel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        ret |= ThaSdhChannelConcateHwStsGet(channel, (uint8)(stsId + sts_i), &hwSts);
        if (ret != cAtOk)
            return ret;

        ret |= SourceStsPayloadTypeSet(self, member, hwSts, cThaStsConcateModeSts);
        }

    return ret;
    }

static eAtRet SinkMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel channel;
    uint8 stsId, numSts, sts_i;
    uint16 hwSts;
    AtUnused(enable);

    channel = (AtSdhChannel)AtConcateMemberChannelGet((AtConcateMember)member);
    stsId = AtSdhChannelSts1Get(channel);
    numSts = AtSdhChannelNumSts(channel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        ret |= ThaSdhChannelConcateHwStsGet(channel, (uint8)(stsId + sts_i), &hwSts);
        if (ret != cAtOk)
            return ret;

        ret |= SinkStsPayloadTypeSet(self, member, hwSts, cThaStsConcateModeSts);
        }

    return ret;
    }

static eAtRet SourceMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    eAtRet ret;
    AtSdhChannel channel;
    uint8 channelType;
    uint16 stsId;
    uint8 vtgId, vtId;
    AtUnused(enable);

    channel = (AtSdhChannel)AtConcateMemberChannelGet(member);
    channelType = AtSdhChannelTypeGet(channel);

    ret = ThaSdhChannelConcateHwIdGet(channel, &stsId, &vtgId, &vtId);
    if (ret != cAtOk)
        return ret;

    if (channelType == cAtSdhChannelTypeVc12)
        {
        ret  = SourceStsPayloadTypeSet(self, member, stsId, cThaStsConcateModeVtDe1);
        ret |= SourceVtgTypeSet(self, member, stsId, vtgId, cThaVtConcateModeVt2);
        return ret;
        }

    if (channelType == cAtSdhChannelTypeVc11)
        {
        ret  = SourceStsPayloadTypeSet(self, member, stsId, cThaStsConcateModeVtDe1);
        ret |= SourceVtgTypeSet(self, member, stsId, vtgId, cThaVtConcateModeVt15);
        return ret;
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet SinkMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    eAtRet ret;
    AtSdhChannel channel;
    uint8 channelType;
    uint16 stsId;
    uint8 vtgId, vtId;
    AtUnused(enable);

    channel = (AtSdhChannel)AtConcateMemberChannelGet(member);
    channelType = AtSdhChannelTypeGet(channel);

    ret = ThaSdhChannelConcateHwIdGet(channel, &stsId, &vtgId, &vtId);
    if (ret != cAtOk)
        return ret;

    if (channelType == cAtSdhChannelTypeVc12)
        {
        ret  = SinkStsPayloadTypeSet(self, member, stsId, cThaStsConcateModeVtDe1);
        ret |= SinkVtgTypeSet(self, member, stsId, vtgId, cThaVtConcateModeVt2);
        return ret;
        }

    if (channelType == cAtSdhChannelTypeVc11)
        {
        ret  = SinkStsPayloadTypeSet(self, member, stsId, cThaStsConcateModeVtDe1);
        ret |= SinkVtgTypeSet(self, member, stsId, vtgId, cThaVtConcateModeVt15);
        return ret;
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet SourceMemberPdhDe3Set(ThaModuleConcate self, AtConcateMember member)
    {
    eAtRet ret;
    AtPdhChannel channel;
    uint8 vtgId, vtId;
    uint16 hwSts;
    uint8 payloadType;

    channel = (AtPdhChannel)AtConcateMemberChannelGet(member);

    ret = ThaPdhChannelConcateHwIdGet(channel, &hwSts, &vtgId, &vtId);
    if (ret != cAtOk)
        return ret;

    payloadType = AtPdhDe3IsDs3((AtPdhDe3)channel) ? cThaStsConcateModeDs3 : cThaStsConcateModeE3;
    return SourceStsPayloadTypeSet(self, member, hwSts, payloadType);
    }

static eAtRet SinkMemberPdhDe3Set(ThaModuleConcate self, AtConcateMember member)
    {
    eAtRet ret;
    AtPdhChannel channel;
    uint8 vtgId, vtId;
    uint16 hwSts;
    uint8 payloadType;

    channel = (AtPdhChannel)AtConcateMemberChannelGet(member);

    ret = ThaPdhChannelConcateHwIdGet(channel, &hwSts, &vtgId, &vtId);
    if (ret != cAtOk)
        return ret;

    payloadType = AtPdhDe3IsDs3((AtPdhDe3)channel) ? cThaStsConcateModeDs3 : cThaStsConcateModeE3;
    return SinkStsPayloadTypeSet(self, member, hwSts, payloadType);
    }

static eAtRet SourceMemberPdhDe1Set(ThaModuleConcate self, AtConcateMember member)
    {
    eAtRet ret;
    AtPdhChannel channel;
    uint8 vtgId, vtId;
    uint16 hwSts;
    uint8 payloadType;

    channel = (AtPdhChannel)AtConcateMemberChannelGet(member);

    ret = ThaPdhChannelConcateHwIdGet(channel, &hwSts, &vtgId, &vtId);
    if (ret != cAtOk)
        return ret;

    payloadType = AtPdhDe1IsE1((AtPdhDe1)channel) ? cThaVtConcateModeE1 : cThaVtConcateModeDs1;
    ret |= SourceStsPayloadTypeSet(self, member, hwSts, cThaStsConcateModeVtDe1);
    ret |= SourceVtgTypeSet(self, member, hwSts, vtgId, payloadType);

    return ret;
    }

static eAtRet SinkMemberPdhDe1Set(ThaModuleConcate self, AtConcateMember member)
    {
    eAtRet ret;
    AtPdhChannel channel;
    uint8 vtgId, vtId;
    uint16 hwSts;
    uint8 payloadType;

    channel = (AtPdhChannel)AtConcateMemberChannelGet(member);

    ret = ThaPdhChannelConcateHwIdGet(channel, &hwSts, &vtgId, &vtId);
    if (ret != cAtOk)
        return ret;

    payloadType = AtPdhDe1IsE1((AtPdhDe1)channel) ? cThaVtConcateModeE1 : cThaVtConcateModeDs1;
    ret |= SinkStsPayloadTypeSet(self, member, hwSts, cThaStsConcateModeVtDe1);
    ret |= SinkVtgTypeSet(self, member, hwSts, vtgId, payloadType);

    return ret;
    }

static void SdhChannelRegsShow(ThaModuleConcate self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);

    if (AtSdhChannelIsHoVc(sdhChannel))
        ThaModuleConcateStsMappingDisplay(sdhChannel);
    else
        ThaModuleConcateVc1xMappingDisplay(sdhChannel);

    AtPrintc(cSevInfo, "* CONCATE registers of channel '%s':\r\n", AtObjectToString((AtObject)sdhChannel));
    }

static void PdhChannelRegsShow(ThaModuleConcate self, AtPdhChannel pdhChannel)
    {
    AtUnused(self);

    ThaModuleConcatePdhMappingDisplay(pdhChannel);
    AtPrintc(cSevInfo, "* CONCATE registers of channel '%s':\r\n", AtObjectToString((AtObject)pdhChannel));
    }

static void MethodsInit(ThaModuleConcate self)
    {
    if (!m_methodsInit)
         {
         AtOsal osal = AtSharedDriverOsalGet();
         mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

         mMethodOverride(m_methods, DefaultSet);

         mMethodOverride(m_methods, HoVcDefaultOffset);
         mMethodOverride(m_methods, Tu3VcDefaultOffset);
         mMethodOverride(m_methods, Vc1xDefaultOffset);
         mMethodOverride(m_methods, De1DefaultOffset);
         mMethodOverride(m_methods, De3DefaultOffset);

         mMethodOverride(m_methods, BindVc1xToSinkConcateGroup);
         mMethodOverride(m_methods, BindHoVcToSinkConcateGroup);
         mMethodOverride(m_methods, BindTu3VcToSinkConcateGroup);
         mMethodOverride(m_methods, BindDe3ToSinkConcateGroup);
         mMethodOverride(m_methods, BindDe1ToSinkConcateGroup);
         mMethodOverride(m_methods, BindVc1xToSourceConcateGroup);
         mMethodOverride(m_methods, BindHoVcToSourceConcateGroup);
         mMethodOverride(m_methods, BindTu3VcToSourceConcateGroup);
         mMethodOverride(m_methods, BindDe3ToSourceConcateGroup);
         mMethodOverride(m_methods, BindDe1ToSourceConcateGroup);

         mMethodOverride(m_methods, SourceMemberAdd);
         mMethodOverride(m_methods, SourceMemberRemove);
         mMethodOverride(m_methods, SinkMemberAdd);
         mMethodOverride(m_methods, SinkMemberRemove);

         mMethodOverride(m_methods, SourceMemberVcxPayloadSet);
         mMethodOverride(m_methods, SinkMemberVcxPayloadSet);
         mMethodOverride(m_methods, SourceMemberVc1xPayloadSet);
         mMethodOverride(m_methods, SinkMemberVc1xPayloadSet);

         mMethodOverride(m_methods, SdhChannelStsIdSw2HwGet);
         mMethodOverride(m_methods, PdhChannelHwIdGet);
         mMethodOverride(m_methods, SdhChannelHwIdGet);

         mMethodOverride(m_methods, CreateVcgMemberObjectForSdhVc);
         mMethodOverride(m_methods, CreateVcgMemberObjectForSdhVc1x);

         mMethodOverride(m_methods, SdhChannelRegsShow);
         mMethodOverride(m_methods, PdhChannelRegsShow);
         }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleConcate);
    }

AtModuleConcate ThaModuleConcateObjectInit(AtModuleConcate self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleConcateObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleConcate ThaModuleConcateNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleConcate concateModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (concateModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleConcateObjectInit(concateModule, device);
    }

uint32 ThaSdhChannelConcateDefaultOffset(AtSdhChannel sdhVc)
    {
    uint16 mapSts;

    if (ThaSdhChannelConcateHwStsGet(sdhVc, AtSdhChannelSts1Get(sdhVc), &mapSts) != cAtOk)
        return cInvalidUint32;

    return (mapSts * 32UL) + (AtSdhChannelTug2Get(sdhVc)) * 4UL + (AtSdhChannelTu1xGet(sdhVc));
    }

/*
 * Almost LCAS/VCAT design uses the flat HW STS ID space for simplified design.
 * So we need this function to convert an STS1 of a SDH channel to be HW STS ID
 * in VCAT/LCAS module.
 * */
eAtRet ThaSdhChannelConcateHwStsGet(AtSdhChannel sdhChannel, uint8 sts1Id, uint16 *hwSts)
    {
    eAtRet ret;
    ThaModuleConcate self = (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhChannel), cAtModuleConcate) ;
    AtVcgBinderSdhVc binder = (AtVcgBinderSdhVc)AtChannelVcgBinder((AtChannel)sdhChannel);
    uint8 masterSts = AtSdhChannelSts1Get(sdhChannel);
    uint16 *allHwSts, localHwSts;
    uint8 sts_i;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (!SlaveStsIdIsValid(self, sdhChannel, sts1Id))
        return cAtErrorInvlParm;

    allHwSts = AtVcgBinderSdhVcAllHwSts(binder);
    if (allHwSts == NULL)
        return cAtErrorRsrcNoAvail;

    /* Calculate if necessary */
    sts_i = (uint8)(sts1Id - masterSts);
    if (allHwSts[sts_i] == cBit15_0)
        {
        ret = mMethodsGet(self)->SdhChannelStsIdSw2HwGet(self, sdhChannel, sts1Id, &localHwSts);
        if (ret != cAtOk)
            return ret;

        allHwSts[sts_i] = localHwSts;
        }

    /* Calculated, just return from database */
    *hwSts = allHwSts[sts_i];

    return cAtOk;
    }

eAtRet ThaSdhChannelConcateHwIdGet(AtSdhChannel sdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    ThaModuleConcate self = (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhChannel), cAtModuleConcate);

    if (self)
        return mMethodsGet(self)->SdhChannelHwIdGet(self, sdhChannel, hwSts, hwVtg, hwVt);

    return cAtOk;
    }

eAtRet ThaPdhChannelConcateHwIdGet(AtPdhChannel pdhChannel, uint16 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    eAtRet ret = cAtErrorNullPointer;
    ThaModuleConcate concateModule = (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pdhChannel), cAtModuleConcate);

    if (concateModule)
        ret = mMethodsGet(concateModule)->PdhChannelHwIdGet(concateModule, pdhChannel, hwSts, hwVtg, hwVt);

    return ret;
    }

eBool ThaConcateGroupCanBeCreated(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType)
    {
    if (AtModuleConcateNumCreatedGroups(self) == 0)
        return cAtTrue;

    if (AtModuleConcateGroupGet(self, vcgId) != NULL)
        return cAtFalse;

    if (IsNonVcatConcateType(concateType) == NoneVcatModeIsUsed(self))
        return cAtTrue;

    return cAtFalse;
    }

void ThaGlobalConcateTypeSet(AtModuleConcate self, eAtConcateGroupType concateType)
    {
    uint32 regVal = mModuleHwRead(self, cAf4RegMapMastReg);
    mFieldIns(&regVal, cAf4NonVcatMdMask, cAf4NonVcatMdShift, IsNonVcatConcateType(concateType) ? 0x1 : 0x0);
    mModuleHwWrite(self, cAf4RegMapMastReg, regVal);
    }

eAtRet ThaModuleConcateSourceIndirectRead(AtModuleConcate self, uint8 regType, uint32 address, uint32 *data)
    {
    uint32 regVal, regAdr;
    eBool           isSimulated;
    uint32          elapseTime = 0;
    tAtOsalCurTime  curTime;
    tAtOsalCurTime  startTime;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Read data */
    regAdr = cAf4RegSourceIndirectCtrl;
    regVal = mModuleHwRead(self, regAdr);
    mFieldIns(&regVal, cAf4SourceTypeSelectMask, cAf4SourceTypeSelectShift, regType);
    mFieldIns(&regVal, cAf4SourceWriteNotReadMask, cAf4SourceWriteNotReadShift, 0x0);
    mFieldIns(&regVal, cAf4SourceIndrAddressMask, cAf4SourceIndrAddressShift, address);
    mModuleHwWrite(self, regAdr, regVal);

    /* If running is simulation mode */
    isSimulated = AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self));
    if (isSimulated)
        {
        *data   = mModuleHwRead(self, address);
        return cAtOk;
        }

    /* Wait for hardware */
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cIndrTimeOut)
        {
        regVal = mModuleHwRead(self, regAdr);
        if ((regVal & cAf4SourceRdWrPendMask) == 0)
            break;

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &curTime, &startTime);
        }

    /* Still not done */
    if ((regVal & cAf4SourceRdWrPendMask))
        return cAtErrorIndrAcsTimeOut;
    else
        *data   = mModuleHwRead(self, cAf4RegSourceIndirectData);

    return cAtOk;
    }

eAtRet ThaModuleConcateSinkIndirectRead(AtModuleConcate self, uint8 regType, uint32 address, uint32 *data)
    {
    uint32 regVal, regAdr;
    eBool           isSimulated;
    uint32          elapseTime = 0;
    tAtOsalCurTime  curTime;
    tAtOsalCurTime  startTime;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Read data */
    regAdr = RegTypeToIndrCtrlAddress(regType);
    regVal = mModuleHwRead(self, regAdr);
    mFieldIns(&regVal, cAf4SinkMembCtrlMemCtrlMask, cAf4SinkMembCtrlMemCtrlShift, 0x1);
    mFieldIns(&regVal, cAf4SinkMembCtrlWriteNotReadMask, cAf4SinkMembCtrlWriteNotReadShift, 0x0);
    mFieldIns(&regVal, cAf4SinkMembCtrlIndrAddressMask, cAf4SinkMembCtrlIndrAddressShift, address);
    mModuleHwWrite(self, regAdr, regVal);

    /* If running is simulation mode */
    isSimulated = AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self));
    if (isSimulated)
        {
        *data   = mModuleHwRead(self, address);
        return cAtOk;
        }

    /* Wait for hardware */
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cIndrTimeOut)
        {
        regVal = mModuleHwRead(self, regAdr);
        if ((regVal & cAf4SinkMembCtrlRdWrPendMask) == 0)
            break;

        /* Retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &curTime, &startTime);
        }

    /* Still not done */
    if ((regVal & cAf4SinkMembCtrlRdWrPendMask))
        return cAtErrorIndrAcsTimeOut;

    *data = mModuleHwRead(self, RegTypeToIndrDataAddress(regType));
    return cAtOk;
    }

eAtRet ThaModuleConcateSourceMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    if (self && member)
        return mMethodsGet(self)->SourceMemberVcxPayloadSet(self, member, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSinkMemberVcxPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    if (self && member)
        return mMethodsGet(self)->SinkMemberVcxPayloadSet(self, member, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSourceMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    if (self && member)
        return mMethodsGet(self)->SourceMemberVc1xPayloadSet(self, member, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSinkMemberVc1xPayloadSet(ThaModuleConcate self, AtConcateMember member, eBool enable)
    {
    if (self && member)
        return mMethodsGet(self)->SinkMemberVc1xPayloadSet(self, member, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSourceMemberPdhDe3Set(ThaModuleConcate self, AtConcateMember member)
    {
    if (self && member)
        return SourceMemberPdhDe3Set(self, member);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSinkMemberPdhDe3Set(ThaModuleConcate self, AtConcateMember member)
    {
    if (self && member)
        return SinkMemberPdhDe3Set(self, member);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSourceMemberPdhDe1Set(ThaModuleConcate self, AtConcateMember member)
    {
    if (self && member)
        return SourceMemberPdhDe1Set(self, member);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSinkMemberPdhDe1Set(ThaModuleConcate self, AtConcateMember member)
    {
    if (self && member)
        return SinkMemberPdhDe1Set(self, member);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindVc1xToSinkConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindVc1xToSinkConcateGroup(self, vc1x, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindHoVcToSinkConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindHoVcToSinkConcateGroup(self, hoVc, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindTu3VcToSinkConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindTu3VcToSinkConcateGroup(self, tu3Vc, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindDe3ToSinkConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindDe3ToSinkConcateGroup(self, de3, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindDe1ToSinkConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindDe1ToSinkConcateGroup(self, de1, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindVc1xToSourceConcateGroup(ThaModuleConcate self, AtChannel vc1x, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindVc1xToSourceConcateGroup(self, vc1x, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindHoVcToSourceConcateGroup(ThaModuleConcate self, AtChannel hoVc, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindHoVcToSourceConcateGroup(self, hoVc, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindTu3VcToSourceConcateGroup(ThaModuleConcate self, AtChannel tu3Vc, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindTu3VcToSourceConcateGroup(self, tu3Vc, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindDe3ToSourceConcateGroup(ThaModuleConcate self, AtChannel de3, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindDe3ToSourceConcateGroup(self, de3, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateBindDe1ToSourceConcateGroup(ThaModuleConcate self, AtChannel de1, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindDe1ToSourceConcateGroup(self, de1, group);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSourceMemberAdd(ThaModuleConcate self, ThaVcgMember member)
    {
    if (self)
        return mMethodsGet(self)->SourceMemberAdd(self, member);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSourceMemberRemove(ThaModuleConcate self, ThaVcgMember member)
    {
    if (self)
        return mMethodsGet(self)->SourceMemberRemove(self, member);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSinkMemberAdd(ThaModuleConcate self, ThaVcgMember member, eBool lcasEnable)
    {
    if (self)
        return mMethodsGet(self)->SinkMemberAdd(self, member, lcasEnable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleConcateSinkMemberRemove(ThaModuleConcate self, ThaVcgMember member)
    {
    if (self)
        return mMethodsGet(self)->SinkMemberRemove(self, member);
    return cAtErrorNullPointer;
    }

void ThaModuleConcateSdhChannelRegsShow(AtSdhChannel sdhChannel)
    {
    ThaModuleConcate self = ModuleConcate((AtChannel)sdhChannel);
    if (self)
        mMethodsGet(self)->SdhChannelRegsShow(self, sdhChannel);
    }

void ThaModuleConcatePdhChannelRegsShow(AtPdhChannel pdhChannel)
    {
    ThaModuleConcate self = ModuleConcate((AtChannel)pdhChannel);
    if (self)
        mMethodsGet(self)->PdhChannelRegsShow(self, pdhChannel);
    }

void ThaModuleConcateStsMappingDisplay(AtSdhChannel sdhChannel)
    {
    uint8 sts_i;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 sts1 = AtSdhChannelSts1Get(sdhChannel);

    AtPrintc(cSevInfo,   "* SW-STS    Slice-oc48    HW-STS(0-47)  HW-VcatSts\r\n");
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 stsId =  (uint8)(sts1 + sts_i);
        uint16 vcatSts;
        uint8 slice, hwSts;

        ThaSdhChannelHwStsGet(sdhChannel, cThaModuleOcn, stsId, &slice, &hwSts);
        ThaSdhChannelConcateHwStsGet(sdhChannel, stsId, &vcatSts);

        AtPrintc(cSevNormal, "%6u    %5u         %6u         %6u\r\n", stsId, slice, hwSts, vcatSts);
        }

    AtPrintc(cSevNormal, "\r\n");
    }

void ThaModuleConcateVc1xMappingDisplay(AtSdhChannel sdhChannel)
    {
    uint16 hwSts;
    uint8 hwVtg, hwVt;

    ThaModuleSdhStsMappingDisplay(sdhChannel);
    if (ThaSdhChannelConcateHwIdGet(sdhChannel, &hwSts, &hwVtg, &hwVt) != cAtOk)
        return;

    AtPrintc(cSevInfo,   "* LOVCAT  HW-STS   HW-VTG   HW-VT\r\n");
    AtPrintc(cSevNormal, "           %3u       %u       %u\r\n", hwSts, hwVtg, hwVt);
    AtPrintc(cSevNormal, "\r\n");
    }

void ThaModuleConcateLongRegDisplay(AtChannel channel, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegLongValueDisplay(channel, address, cAtModuleConcate);
    }

void ThaModuleConcateRegDisplay(AtChannel channel, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay(channel, address, cAtModuleConcate);
    }

void ThaModuleConcatePdhMappingDisplay(AtPdhChannel pdhChannel)
    {
    uint16 hwSts;
    uint8 hwVtg, hwVt;

    ThaModulePdhChannelIdsShow((ThaModulePdh)AtChannelModuleGet((AtChannel)pdhChannel), pdhChannel);
    if (ThaPdhChannelConcateHwIdGet(pdhChannel, &hwSts, &hwVtg, &hwVt) != cAtOk)
        return;

    AtPrintc(cSevInfo,   "* LOVCAT  HW-STS   HW-VTG   HW-VT\r\n");
    AtPrintc(cSevNormal, "           %3u       %u       %u\r\n", hwSts, hwVtg, hwVt);
    AtPrintc(cSevNormal, "\r\n");
    }

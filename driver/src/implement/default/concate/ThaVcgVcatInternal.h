/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : ThaModuleConcateInternal.h
 * 
 * Created Date: Oct 9, 2014
 *
 * Description : Concate module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCGVCATINTERNAL_H_
#define _THAVCGVCATINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/concate/AtVcgInternal.h"
#include "ThaVcg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcgVcat * ThaVcgVcat;

typedef struct tThaVcgVcatMethods
    {
    eAtRet (*DefaultSet)(ThaVcgVcat self);
    void (*HwReset)(ThaVcgVcat self);

    }tThaVcgVcatMethods;

typedef struct tThaVcgVcat
    {
    tAtVcg super;
    const tThaVcgVcatMethods *methods;

    /* Private data */
    eBool enableLcas;
    uint32 currentSq;
    }tThaVcgVcat;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtVcg ThaVcgVcatObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType);

#endif /* _THAVCGVCATINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : ThaVcgNonVcatInternal.h
 * 
 * Created Date: Sep 8, 2019
 *
 * Description : Default non-VCAT/CCAT representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCGNONVCATINTERNAL_H_
#define _THAVCGNONVCATINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/concate/AtConcateGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcgNonVcat
    {
    tAtConcateGroup super;
    }tThaVcgNonVcat;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConcateGroup ThaVcgNonVcatObjectInit(AtConcateGroup self,
                                       AtModuleConcate concateModule,
                                       uint32 vcgId,
                                       eAtConcateMemberType memberType,
                                       eAtConcateGroupType concateType);

#ifdef __cplusplus
}
#endif
#endif /* _THAVCGNONVCATINTERNAL_H_ */


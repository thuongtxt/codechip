/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : ThaModuleConcateReg.h
 * 
 * Created Date: Jun 6, 2014
 *
 * Description : Concate registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECONCATEREG_H_
#define _THAMODULECONCATEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: Master Register
Reg Addr: 0x700001
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapMastReg                              0x700001

#define cAf4RegMapMastRegRstVal                        0x00000000
#define cAf4RegMapMastRegRwMsk                         0x00000007
#define cAf4RegMapMastRegRwcMsk                        0x00000000


#define cAf4FifoFlushMask                              cBit2
#define cAf4FifoFlushShift                             2

/*--------------------------------------
BitField Name: NonVcatMd
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 1
--------------------------------------*/
#define cAf4NonVcatMdMask                              cBit1
#define cAf4NonVcatMdShift                             1
#define cAf4NonVcatMdMaxVal                            0x1
#define cAf4NonVcatMdMinVal                            0x0
#define cAf4NonVcatMdRstVal                            0x0

/*--------------------------------------
BitField Name: Active
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 0
--------------------------------------*/
#define cAf4ActMask                                    cBit0
#define cAf4ActShift                                   0
#define cAf4ActMaxVal                                  0x1
#define cAf4ActMinVal                                  0x0
#define cAf4ActRstVal                                  0x0




/*------------------------------------------------------------------------------
Reg Name: Write Access Data Holding 1
Reg Addr: 0x700002
Reg Desc: This register is used for CPU to write to registers that has more
          than 32 bits.
------------------------------------------------------------------------------*/
#define cAf4RegMapWriteAcsDataHolding1                 0x700002

#define cAf4RegMapWriteAcsDataHolding1RstVal           0x00000000
#define cAf4RegMapWriteAcsDataHolding1RwMsk            0xFFFFFFFF
#define cAf4RegMapWriteAcsDataHolding1RwcMsk           0x00000000
/*--------------------------------------
BitField Name: WrDat_Hold1[31:0]
BitField Type: R/W
BitField Desc: Data holding register To write data to the register that has
               more than 32 bits, CPU must write high double word value to this
               register first. This value will be attached with low double word
               value from the CPU data bus when CPU writes data to the low
               double word. It is note that CPU must always write high double
               word data to this register before write to low double word
               otherwise the garbage data in this register will be written to
               the register under configuration.
BitField Bits: 31_0
--------------------------------------*/
#define cAf4WrDatHold1Mask                             cBit31_0
#define cAf4WrDatHold1Shift                            0
#define cAf4WrDatHold1MaxVal                           0xFFFFFFFF
#define cAf4WrDatHold1MinVal                           0x0
#define cAf4WrDatHold1RstVal                           0x0




/*------------------------------------------------------------------------------
Reg Name: Write Access Data Holding 2
Reg Addr: 0x700003
Reg Desc: This register is used for CPU to write to registers that has more
          than 64 bits.
------------------------------------------------------------------------------*/
#define cAf4RegMapWriteAcsDataHolding2                 0x700003

#define cAf4RegMapWriteAcsDataHolding2RstVal           0x00000000
#define cAf4RegMapWriteAcsDataHolding2RwMsk            0xFFFFFFFF
#define cAf4RegMapWriteAcsDataHolding2RwcMsk           0x00000000
/*--------------------------------------
BitField Name: WrDat_Hold2[31:0]
BitField Type: R/W
BitField Desc: Data holding register To write data to the register that has
               more than 64 bits, CPU must write high double word value to this
               register first. This value will be attached with low double word
               value from the CPU data bus when CPU writes data to the low
               double word. It is note that CPU must always write high double
               word data to this register before write to low double word
               otherwise the garbage data in this register will be written to
               the register under configuration.
BitField Bits: 31_0
--------------------------------------*/
#define cAf4WrDatHold2Mask                             cBit31_0
#define cAf4WrDatHold2Shift                            0
#define cAf4WrDatHold2MaxVal                           0xFFFFFFFF
#define cAf4WrDatHold2MinVal                           0x0
#define cAf4WrDatHold2RstVal                           0x0




/*------------------------------------------------------------------------------
Reg Name: Read Access Data Holding 1
Reg Addr: 0x700004
Reg Desc: This register is used for CPU to write to registers that has more
          than 32 bits.
------------------------------------------------------------------------------*/
#define cAf4RegMapReadAcsDataHolding1                  0x700004

#define cAf4RegMapReadAcsDataHolding1RstVal            0x00000000
#define cAf4RegMapReadAcsDataHolding1RwMsk             0xFFFFFFFF
#define cAf4RegMapReadAcsDataHolding1RwcMsk            0x00000000
/*--------------------------------------
BitField Name: RdDat_Hold1[31:0]
BitField Type: R/W
BitField Desc: Data holding register To get data from the register that has
               more than 32 bits. It is note that CPU must always read low
               double word data before read from high double word.
BitField Bits: 31_0
--------------------------------------*/
#define cAf4RdDatHold1Mask                             cBit31_0
#define cAf4RdDatHold1Shift                            0
#define cAf4RdDatHold1MaxVal                           0xFFFFFFFF
#define cAf4RdDatHold1MinVal                           0x0
#define cAf4RdDatHold1RstVal                           0x0




/*------------------------------------------------------------------------------
Reg Name: Read Access Data Holding 2
Reg Addr: 0x700005
Reg Desc: This register is used for CPU to write to registers that has more
          than 64 bits.
------------------------------------------------------------------------------*/
#define cAf4RegMapReadAcsDataHolding2                  0x700005

#define cAf4RegMapReadAcsDataHolding2RstVal            0x00000000
#define cAf4RegMapReadAcsDataHolding2RwMsk             0xFFFFFFFF
#define cAf4RegMapReadAcsDataHolding2RwcMsk            0x00000000
/*--------------------------------------
BitField Name: RdDat_Hold2[31:0]
BitField Type: R/W
BitField Desc: Data holding register To get data from the register that has
               more than 64 bits. It is note that CPU must always read low
               double word data before read from high double word.
BitField Bits: 31_0
--------------------------------------*/
#define cAf4RdDatHold2Mask                             cBit31_0
#define cAf4RdDatHold2Shift                            0
#define cAf4RdDatHold2MaxVal                           0xFFFFFFFF
#define cAf4RdDatHold2MinVal                           0x0
#define cAf4RdDatHold2RstVal                           0x0




/*------------------------------------------------------------------------------
Reg Name: Source Mode Configuration
Reg Addr: 0x720000-0x72000B
          The address format for these registers is 0x720000 + sts_id
          Where: sts_id: 0 - 11
Reg Desc: This register is used to
------------------------------------------------------------------------------*/
#define cAf4RegMapSourceModeCfg(wsts_id)               (0x720000UL + (wsts_id))

#define cAf4RegMapSourceModeCfgRstVal                  0x00000000
#define cAf4RegMapSourceModeCfgRwMsk                   0x00033FFF
#define cAf4RegMapSourceModeCfgRwcMsk                  0x00000000
/*
#define cAf4UnusedMask                                 cBit31_18
#define cAf4UnusedShift                                18
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: So_HoMd
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 00: STS-VC3 Mode
               - 01: DS3 Mode
               - 10: E3 Mode
               - 11: Lo-order Mode (DS1 or E1 or VT1.5 or VT2)
BitField Bits: 17_16
--------------------------------------*/
#define cAf4SoHoMdMask                                 cBit17_16
#define cAf4SoHoMdShift                                16
#define cAf4SoHoMdMaxVal                               0x3
#define cAf4SoHoMdMinVal                               0x0
#define cAf4SoHoMdRstVal                               0x0

/*
#define cAf4UnusedMask                                 cBit15_14
#define cAf4UnusedShift                                14
#define cAf4UnusedRstVal                               0x0
*/

#define cAf4SoLoMdGrpMask(grpId)                       (cBit1_0 << grpId * 2)
#define cAf4SoLoMdGrpShift(grpId)                      (grpId * 2)

/*--------------------------------------
BitField Name: So_LoMd_Grp#6
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 13_12
--------------------------------------*/
#define cAf4SoLoMdGrp6Mask                             cBit13_12
#define cAf4SoLoMdGrp6Shift                            12
#define cAf4SoLoMdGrp6MaxVal                           0x3
#define cAf4SoLoMdGrp6MinVal                           0x0
#define cAf4SoLoMdGrp6RstVal                           0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp#5
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 11_10
--------------------------------------*/
#define cAf4SoLoMdGrp5Mask                             cBit11_10
#define cAf4SoLoMdGrp5Shift                            10
#define cAf4SoLoMdGrp5MaxVal                           0x3
#define cAf4SoLoMdGrp5MinVal                           0x0
#define cAf4SoLoMdGrp5RstVal                           0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp#4
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 9_8
--------------------------------------*/
#define cAf4SoLoMdGrp4Mask                             cBit9_8
#define cAf4SoLoMdGrp4Shift                            8
#define cAf4SoLoMdGrp4MaxVal                           0x3
#define cAf4SoLoMdGrp4MinVal                           0x0
#define cAf4SoLoMdGrp4RstVal                           0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp#3
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 7_6
--------------------------------------*/
#define cAf4SoLoMdGrp3Mask                             cBit7_6
#define cAf4SoLoMdGrp3Shift                            6
#define cAf4SoLoMdGrp3MaxVal                           0x3
#define cAf4SoLoMdGrp3MinVal                           0x0
#define cAf4SoLoMdGrp3RstVal                           0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp#2
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 5_4
--------------------------------------*/
#define cAf4SoLoMdGrp2Mask                             cBit5_4
#define cAf4SoLoMdGrp2Shift                            4
#define cAf4SoLoMdGrp2MaxVal                           0x3
#define cAf4SoLoMdGrp2MinVal                           0x0
#define cAf4SoLoMdGrp2RstVal                           0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp#1
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 3_2
--------------------------------------*/
#define cAf4SoLoMdGrp1Mask                             cBit3_2
#define cAf4SoLoMdGrp1Shift                            2
#define cAf4SoLoMdGrp1MaxVal                           0x3
#define cAf4SoLoMdGrp1MinVal                           0x0
#define cAf4SoLoMdGrp1RstVal                           0x0

/*--------------------------------------
BitField Name: So_LoMd_Grp#0
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 1_0
--------------------------------------*/
#define cAf4SoLoMdGrp0Mask                             cBit1_0
#define cAf4SoLoMdGrp0Shift                            0
#define cAf4SoLoMdGrp0MaxVal                           0x3
#define cAf4SoLoMdGrp0MinVal                           0x0
#define cAf4SoLoMdGrp0RstVal                           0x0


/*------------------------------------------------------------------------------
Reg Name: Member Control Configuration
Reg Addr: 0x720800-0x72017B
          The address format for these registers is 0x720040 + sts_id*32 +
          vtg_id*4 + vt_id
          Where: sts_id: 0-11
          vtg_id: 0-6
          vt_id: 0-3
Reg Desc: This register is used to
------------------------------------------------------------------------------*/
#define cAf4RegMapMemberCtrlCfg                        0x720800

#define cAf4RegMapMemberCtrlCfgRstVal                  0x00000000
#define cAf4RegMapMemberCtrlCfgRwMsk                   0x0001F0FF
#define cAf4RegMapMemberCtrlCfgRwcMsk                  0x00000000
/*
#define cAf4UnusedMask                                 cBit31_17
#define cAf4UnusedShift                                17
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: So_Mem_Ena
BitField Type: R/W
BitField Desc: Member Enable bit
               - 1: Enable Member
               - 0: Disable Member
BitField Bits: 16
--------------------------------------*/
#define cAf4SoMemEnaMask                               cBit16
#define cAf4SoMemEnaShift                              16
#define cAf4SoMemEnaMaxVal                             0x1
#define cAf4SoMemEnaMinVal                             0x0
#define cAf4SoMemEnaRstVal                             0x0

/*--------------------------------------
BitField Name: So_G8040_Md
BitField Type: R/W
BitField Desc: DS3/E3/DS1/E1 None-Vcat Mode with standard G8040 or G804
               Configuration
               - 1: ITU G.8040 Mode
               - 0: ITU G.804 Mode
BitField Bits: 15
--------------------------------------*/
#define cAf4SoG8040MdMask                              cBit15
#define cAf4SoG8040MdShift                             15
#define cAf4SoG8040MdMaxVal                            0x1
#define cAf4SoG8040MdMinVal                            0x0
#define cAf4SoG8040MdRstVal                            0x0

/*--------------------------------------
BitField Name: So_Vcat_Md
BitField Type: R/W
BitField Desc: Member Enable with VCAT mode
               - 1: VCAT Mode
               - 0: none-VCAT Mode
BitField Bits: 14
--------------------------------------*/
#define cAf4SoVcatMdMask                               cBit14
#define cAf4SoVcatMdShift                              14
#define cAf4SoVcatMdMaxVal                             0x1
#define cAf4SoVcatMdMinVal                             0x0
#define cAf4SoVcatMdRstVal                             0x0

/*--------------------------------------
BitField Name: So_Lcas_Md
BitField Type: R/W
BitField Desc: Member Enable with LCAS mode
               - 1: LCAS Mode
               - 0: none-LCAS Mode
BitField Bits: 13
--------------------------------------*/
#define cAf4SoLcasMdMask                               cBit13
#define cAf4SoLcasMdShift                              13
#define cAf4SoLcasMdMaxVal                             0x1
#define cAf4SoLcasMdMinVal                             0x0
#define cAf4SoLcasMdRstVal                             0x0

/*--------------------------------------
BitField Name: So_Nms_Cmd
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 12
--------------------------------------*/
#define cAf4SoNmsCmdMask                               cBit12
#define cAf4SoNmsCmdShift                              12
#define cAf4SoNmsCmdMaxVal                             0x1
#define cAf4SoNmsCmdMinVal                             0x0
#define cAf4SoNmsCmdRstVal                             0x0

/*
#define cAf4UnusedMask                                 cBit11_8
#define cAf4UnusedShift                                8
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: So_VcgId
BitField Type: R/W
BitField Desc: VCG ID 0-255
BitField Bits: 7_0
--------------------------------------*/
#define cAf4SoVcgIdMask                                cBit7_0
#define cAf4SoVcgIdShift                               0
#define cAf4SoVcgIdMaxVal                              0xFF
#define cAf4SoVcgIdMinVal                              0x0
#define cAf4SoVcgIdRstVal                              0x0


/*------------------------------------------------------------------------------
Reg Name: Source VC4 control
Reg Addr: 0x7200C0
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapSoVc4Ctrl                            (0x7200C0)

#define cAf4RegMapSoVc4CtrlRstVal                      0x00000000
#define cAf4RegMapSoVc4CtrlRwMsk                       0xFFFFFFFF
#define cAf4RegMapSoVc4CtrlRwcMsk                      0x00000000
#define cAf4RegMapSoVc4CtrlEnMask(mem)                 (cBit0 << (mem))
#define cAf4RegMapSoVc4CtrlEnShift(mem)                (mem)

/*------------------------------------------------------------------------------
Reg Name: Source Member Status
Reg Addr: 0x721000-0x72117B
          The address format for these registers is 0x721000 + sts_id*32 +
          vtg_id*4 + vt_id
          Where: sts_id: 0-11
          vtg_id: 0-6
          vt_id: 0-3
Reg Desc: This register is used to
------------------------------------------------------------------------------*/
#define cAf4RegMapSourceMst                            0x721000

#define cAf4RegMapSourceMstRstVal                      0x00000000
#define cAf4RegMapSourceMstRwMsk                       0x000007FF
#define cAf4RegMapSourceMstRwcMsk                      0x00000000
/*
#define cAf4UnusedMask                                 cBit31_11
#define cAf4UnusedShift                                11
#define cAf4UnusedRstVal                               0x0
*/

#define cAf4SoMemTxCwMask                              cBit21_18
#define cAf4SoMemTxCwShift                             18

#define cAf4SoMemAcpSeqMask                            cBit17_12
#define cAf4SoMemAcpSeqShift                           12
#define cAf4SoMemAcpSeqMaxVal                          0x3f
#define cAf4SoMemAcpSeqMinVal                          0x0
#define cAf4SoMemAcpSeqRstVal                          0x0

/*--------------------------------------
BitField Name: So_Mem_RemSta
BitField Type: R/W
BitField Desc: Member Remove status.
               - 1: removal done
               - 0: removal is being or member is deleted out of a VCG.
BitField Bits: 10
--------------------------------------*/
#define cAf4SoMemRemStaMask                            cBit10
#define cAf4SoMemRemStaShift                           10
#define cAf4SoMemRemStaMaxVal                          0x1
#define cAf4SoMemRemStaMinVal                          0x0
#define cAf4SoMemRemStaRstVal                          0x0

/*--------------------------------------
BitField Name: So_Mem_ReqSta
BitField Type: R/W
BitField Desc: Member Request Data status.
               - 1: Data are being requested
               - 0: Data are stopped
BitField Bits: 9
--------------------------------------*/
#define cAf4SoMemReqStaMask                            cBit9
#define cAf4SoMemReqStaShift                           9
#define cAf4SoMemReqStaMaxVal                          0x1
#define cAf4SoMemReqStaMinVal                          0x0
#define cAf4SoMemReqStaRstVal                          0x0

/*--------------------------------------
BitField Name: So_Mem_HwSta
BitField Type: R/W
BitField Desc: Member Accepted Sequence ID (0-63).
BitField Bits: 8_3
--------------------------------------*/
#define cAf4SoMemHwStaMask                            cBit8_3
#define cAf4SoMemHwStaShift                           3
#define cAf4SoMemHwStaMaxVal                          0x3F
#define cAf4SoMemHwStaMinVal                          0x0
#define cAf4SoMemHwStaRstVal                          0x0

/*--------------------------------------
BitField Name: So_Mem_Fsm
BitField Type: R/W
BitField Desc: Member Finite-State-Machine
               - 000: IDLE state
               - 001: ADD state
               - 010: REMOVE state
               - 011: NORM or EOS state
               - 100: DNU state
               - 111: FIX state (used for VCAT none-LCAS mode)
BitField Bits: 2_0
--------------------------------------*/
#define cAf4SoMemFsmMask                               cBit2_0
#define cAf4SoMemFsmShift                              0
#define cAf4SoMemFsmMaxVal                             0x7
#define cAf4SoMemFsmMinVal                             0x0
#define cAf4SoMemFsmRstVal                             0x0



/*------------------------------------------------------------------------------
Reg Name: Transmit Sequence Configuration
Reg Addr: 0x725000-0x7255FB
          The address format for these registers is 0x725000 + sts_id*32 +
          vtg_id*4 + vt_id
          Where: sts_id: 0-11
          vtg_id: 0-6
          vt_id: 0-3
Reg Desc:  These registers are used for SW  to assign sequence for VCAT mode.
------------------------------------------------------------------------------*/
#define cAf4RegMapSoExpSeq                            0x725000

#define cAf4RegMapSoExpSeqRstVal                      0x00000000
#define cAf4SoMemExpSeqEnMask                         cBit8
#define cAf4SoMemExpSeqEnShift                        8
#define cAf4SoMemExpSeqMask                           cBit7_0
#define cAf4SoMemExpSeqShift                          0



/*------------------------------------------------------------------------------
Reg Name: Remove Thresshold
Reg Addr: 0x750000
Reg Desc:
------------------------------------------------------------------------------*/
#define cAf4RegMapSkRmvThr                             0x750000
#define cAf4RegMapSkRmvThrRstVal                       0xFF
#define cAf4SkRmvThrMask                               cBit7_0
#define cAf4SkRmvThrShift                              0

/*------------------------------------------------------------------------------
Reg Name: Hold-Off Threshold
Reg Addr: 0x750001
Reg Desc:
------------------------------------------------------------------------------*/
#define cAf4RegMapSkHoThr                             (0x750001)
#define cAf4RegMapSkHoThrRstVal                       0xC7
#define cAf4SkHoThrMask                               cBit7_0
#define cAf4SkHoThrShift                              0

/*------------------------------------------------------------------------------
Reg Name: Wait-To-Restore Threshold
Reg Addr: 0x750002
Reg Desc:
------------------------------------------------------------------------------*/
#define cAf4RegMapSkWTRThr                             (0x750002)
#define cAf4RegMapSkWTRThrRstVal                       0xC7
#define cAf4SkWTRThrMask                               cBit11_0
#define cAf4SkWTRThrShift                              0


/*------------------------------------------------------------------------------
Reg Name: Sink Mode Configuration
Reg Addr: 0x740000-0x74000B
          The address format for these registers is 0x740000 + sts_id
          Where: sts_id: 0 - 11
Reg Desc: This register is used to
------------------------------------------------------------------------------*/
#define cAf4RegMapSnkModeCfg(wsts_id)                  (0x740000UL + ((wsts_id)%4)*0x100UL+((wsts_id)/4))

#define cAf4RegMapSnkModeCfgRstVal                     0x00000000
#define cAf4RegMapSnkModeCfgRwMsk                      0x00033FFF
#define cAf4RegMapSnkModeCfgRwcMsk                     0x00000000
/*
#define cAf4UnusedMask                                 cBit31_18
#define cAf4UnusedShift                                18
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: Sk_HoMd
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 00: STS-VC3 Mode
               - 01: DS3 Mode
               - 10: E3 Mode
               - 11: Lo-order Mode (DS1 or E1 or VT1.5 or VT2)
BitField Bits: 17_16
--------------------------------------*/
#define cAf4SkHoMdMask                                 cBit17_16
#define cAf4SkHoMdShift                                16
#define cAf4SkHoMdMaxVal                               0x3
#define cAf4SkHoMdMinVal                               0x0
#define cAf4SkHoMdRstVal                               0x0

/*
#define cAf4UnusedMask                                 cBit15_14
#define cAf4UnusedShift                                14
#define cAf4UnusedRstVal                               0x0
*/

#define cAf4SkLoMdGrpMask(grpId)                       (cBit1_0 << grpId * 2)
#define cAf4SkLoMdGrpShift(grpId)                      (grpId * 2)

/*--------------------------------------
BitField Name: Sk_LoMd_Grp#6
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 13_12
--------------------------------------*/
#define cAf4SkLoMdGrp6Mask                             cBit13_12
#define cAf4SkLoMdGrp6Shift                            12
#define cAf4SkLoMdGrp6MaxVal                           0x3
#define cAf4SkLoMdGrp6MinVal                           0x0
#define cAf4SkLoMdGrp6RstVal                           0x0

/*--------------------------------------
BitField Name: Sk_LoMd_Grp#5
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 11_10
--------------------------------------*/
#define cAf4SkLoMdGrp5Mask                             cBit11_10
#define cAf4SkLoMdGrp5Shift                            10
#define cAf4SkLoMdGrp5MaxVal                           0x3
#define cAf4SkLoMdGrp5MinVal                           0x0
#define cAf4SkLoMdGrp5RstVal                           0x0

/*--------------------------------------
BitField Name: Sk_LoMd_Grp#4
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 9_8
--------------------------------------*/
#define cAf4SkLoMdGrp4Mask                             cBit9_8
#define cAf4SkLoMdGrp4Shift                            8
#define cAf4SkLoMdGrp4MaxVal                           0x3
#define cAf4SkLoMdGrp4MinVal                           0x0
#define cAf4SkLoMdGrp4RstVal                           0x0

/*--------------------------------------
BitField Name: Sk_LoMd_Grp#3
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 7_6
--------------------------------------*/
#define cAf4SkLoMdGrp3Mask                             cBit7_6
#define cAf4SkLoMdGrp3Shift                            6
#define cAf4SkLoMdGrp3MaxVal                           0x3
#define cAf4SkLoMdGrp3MinVal                           0x0
#define cAf4SkLoMdGrp3RstVal                           0x0

/*--------------------------------------
BitField Name: Sk_LoMd_Grp#2
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 5_4
--------------------------------------*/
#define cAf4SkLoMdGrp2Mask                             cBit5_4
#define cAf4SkLoMdGrp2Shift                            4
#define cAf4SkLoMdGrp2MaxVal                           0x3
#define cAf4SkLoMdGrp2MinVal                           0x0
#define cAf4SkLoMdGrp2RstVal                           0x0

/*--------------------------------------
BitField Name: Sk_LoMd_Grp#1
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 3_2
--------------------------------------*/
#define cAf4SkLoMdGrp1Mask                             cBit3_2
#define cAf4SkLoMdGrp1Shift                            2
#define cAf4SkLoMdGrp1MaxVal                           0x3
#define cAf4SkLoMdGrp1MinVal                           0x0
#define cAf4SkLoMdGrp1RstVal                           0x0

/*--------------------------------------
BitField Name: Sk_LoMd_Grp#0
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 1_0
--------------------------------------*/
#define cAf4SkLoMdGrp0Mask                             cBit1_0
#define cAf4SkLoMdGrp0Shift                            0
#define cAf4SkLoMdGrp0MaxVal                           0x3
#define cAf4SkLoMdGrp0MinVal                           0x0
#define cAf4SkLoMdGrp0RstVal                           0x0



/*------------------------------------------------------------------------------
Reg Name: Source sink VC4 control
Reg Addr: 0x7400C0
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapSkVc4Ctrl(wsts_id)                       (0x7400C0UL + 0x100UL * ((wsts_id)%4))
#define cAf4RegMapSkVc4CtrlRstVal							0x00000000
#define cAf4RegMapSkVc4CtrlRwMsk							0xFFFFFFFF
#define cAf4RegMapSkVc4CtrlRwcMsk							0x00000000
#define cAf4RegMapSkVc4CtrlEnMask(wsts_id)                 (cBit0 << ((wsts_id)/4))
#define cAf4RegMapSkVc4CtrlEnShift(wsts_id)                ((wsts_id)/4)


/*------------------------------------------------------------------------------
Reg Name: Sink Member Control Configuration
Reg Addr: 0x748800-0x74897B
          The address format for these registers is 0x748800 + sts_id*32 +
          vtg_id*4 + vt_id
          Where: sts_id: 0-11
          vtg_id: 0-6
          vt_id: 0-3
Reg Desc: This register is used to
------------------------------------------------------------------------------*/
#define cAf4RegMapSnkMemberCtrlCfg                     0x748800

#define cAf4RegMapSnkMemberCtrlCfgRstVal               0x00000000
#define cAf4RegMapSnkMemberCtrlCfgRwMsk                0x0001F0FF
#define cAf4RegMapSnkMemberCtrlCfgRwcMsk               0x00000000
/*
#define cAf4UnusedMask                                 cBit31_17
#define cAf4UnusedShift                                17
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: Sk_Mem_Ena
BitField Type: R/W
BitField Desc: Member Enable bit
               - 1: Enable Member
               - 0: Disable Member
BitField Bits: 16
--------------------------------------*/
#define cAf4SkMemEnaMask                               cBit16
#define cAf4SkMemEnaShift                              16
#define cAf4SkMemEnaMaxVal                             0x1
#define cAf4SkMemEnaMinVal                             0x0
#define cAf4SkMemEnaRstVal                             0x0

/*--------------------------------------
BitField Name: Sk_G8040_Md
BitField Type: R/W
BitField Desc: DS3/E3/DS1/E1 None-Vcat Mode with standard G8040 or G804
               Configuration
               - 1: ITU G.8040 Mode
               - 0: ITU G.804 Mode
BitField Bits: 15
--------------------------------------*/
#define cAf4SkG8040MdMask                              cBit15
#define cAf4SkG8040MdShift                             15
#define cAf4SkG8040MdMaxVal                            0x1
#define cAf4SkG8040MdMinVal                            0x0
#define cAf4SkG8040MdRstVal                            0x0

/*--------------------------------------
BitField Name: Sk_Vcat_Md
BitField Type: R/W
BitField Desc: Member Enable with VCAT mode
               - 1: VCAT Mode
               - 0: none-VCAT Mode
BitField Bits: 14
--------------------------------------*/
#define cAf4SkVcatMdMask                               cBit14
#define cAf4SkVcatMdShift                              14
#define cAf4SkVcatMdMaxVal                             0x1
#define cAf4SkVcatMdMinVal                             0x0
#define cAf4SkVcatMdRstVal                             0x0

/*--------------------------------------
BitField Name: Sk_Lcas_Md
BitField Type: R/W
BitField Desc: Member Enable with LCAS mode
               - 1: LCAS Mode
               - 0: none-LCAS Mode
BitField Bits: 13
--------------------------------------*/
#define cAf4SkLcasMdMask                               cBit13
#define cAf4SkLcasMdShift                              13
#define cAf4SkLcasMdMaxVal                             0x1
#define cAf4SkLcasMdMinVal                             0x0
#define cAf4SkLcasMdRstVal                             0x0

/*--------------------------------------
BitField Name: Sk_Nms_Cmd
BitField Type: R/W
BitField Desc: NMS Command used only LCAS mode. Should be set to 0 for
               none-LCAS mode
               - 1: Add Command
               - 0: Remove Command.
BitField Bits: 12
--------------------------------------*/
#define cAf4SkNmsCmdMask                               cBit12
#define cAf4SkNmsCmdShift                              12
#define cAf4SkNmsCmdMaxVal                             0x1
#define cAf4SkNmsCmdMinVal                             0x0
#define cAf4SkNmsCmdRstVal                             0x0
#define cAf4SkNmsCmdAdd                                1
#define cAf4SkNmsCmdRmv                                0

/*
#define cAf4UnusedMask                                 cBit11_8
#define cAf4UnusedShift                                8
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: Sk_VcgId
BitField Type: R/W
BitField Desc: VCG ID 0-255
BitField Bits: 7_0
--------------------------------------*/
#define cAf4SkVcgIdMask                                cBit7_0
#define cAf4SkVcgIdShift                               0
#define cAf4SkVcgIdMaxVal                              0xFF
#define cAf4SkVcgIdMinVal                              0x0
#define cAf4SkVcgIdRstVal                              0x0



/*------------------------------------------------------------------------------
Reg Name: Sink Member Status
Reg Addr: 0x750800-0x75097B
          The address format for these registers is 0x750800 + sts_id*32 +
          vtg_id*4 + vt_id
          Where: sts_id: 0-11
          vtg_id: 0-6
          vt_id: 0-3
Reg Desc: This register is used to
------------------------------------------------------------------------------*/
#define cAf4RegMapSnkMst                               0x750800

#define cAf4RegMapSnkMstRstVal                         0x00000000
#define cAf4RegMapSnkMstRwMsk                          0x00003FFB
#define cAf4RegMapSnkMstRwcMsk                         0x00000000
/*
#define cAf4UnusedMask                                 cBit31_14
#define cAf4UnusedShift                                14
#define cAf4UnusedRstVal                               0x0
*/
#define cAf4SkMemRxCwMask                              cBit17_14
#define cAf4SkMemRxCwShift                             14

/*--------------------------------------
BitField Name: Sk_FASSta
BitField Type: R/W
BitField Desc: FAS framing state (for VT mode)
               - 0: Loss of FAS framing
               - 1: In-Frame
BitField Bits: 13
--------------------------------------*/
#define cAf4SkFasStaMask                               cBit13
#define cAf4SkFasStaShift                              13
#define cAf4SkFasStaMaxVal                             0x1
#define cAf4SkFasStaMinVal                             0x0
#define cAf4SkFasStaRstVal                             0x0

/*--------------------------------------
BitField Name: Sk_K4Sta
BitField Type: R/W
BitField Desc: K4 framing state
               - 0: K4 OOM
               - 1: K4 IM
BitField Bits: 12
--------------------------------------*/
#define cAf4SkK4StaMask                                cBit12
#define cAf4SkK4StaShift                               12
#define cAf4SkK4StaMaxVal                              0x1
#define cAf4SkK4StaMinVal                              0x0
#define cAf4SkK4StaRstVal                              0x0

/*--------------------------------------
BitField Name: Sk_MFI1Sta
BitField Type: R/W
BitField Desc: MFI1 state
               - 0: OOM1
               - 1: IM1
BitField Bits: 11
--------------------------------------*/
#define cAf4SkMFI1StaMask                              cBit11
#define cAf4SkMFI1StaShift                             11
#define cAf4SkMFI1StaMaxVal                            0x1
#define cAf4SkMFI1StaMinVal                            0x0
#define cAf4SkMFI1StaRstVal                            0x0

/*--------------------------------------
BitField Name: Sk_MFI2Sta
BitField Type: R/W
BitField Desc: MFI2 state
               - 0: OOM2
               - 1: IM2
BitField Bits: 10
--------------------------------------*/
#define cAf4SkMFI2StaMask                              cBit10
#define cAf4SkMFI2StaShift                             10
#define cAf4SkMFI2StaMaxVal                            0x1
#define cAf4SkMFI2StaMinVal                            0x0
#define cAf4SkMFI2StaRstVal                            0x0

/*--------------------------------------
BitField Name: Sk_Mem_AcpSeq
BitField Type: R/W
BitField Desc: Member Accepted Sequence ID (0-63).
BitField Bits: 9_4
--------------------------------------*/
#define cAf4SkMemAcpSeqMask                            cBit9_4
#define cAf4SkMemAcpSeqShift                           4
#define cAf4SkMemAcpSeqMaxVal                          0x3F
#define cAf4SkMemAcpSeqMinVal                          0x0
#define cAf4SkMemAcpSeqRstVal                          0x0

/*--------------------------------------
BitField Name: Sk_Mem_DatSta
BitField Type: R/W
BitField Desc: Member Data Status
               - 1: data is available
               - 0: data is unavailable
BitField Bits: 3
--------------------------------------*/
#define cAf4SkMemDatStaMask                            cBit3
#define cAf4SkMemDatStaShift                           3
#define cAf4SkMemDatStaMaxVal                          0x1
#define cAf4SkMemDatStaMinVal                          0x0
#define cAf4SkMemDatStaRstVal                          0x0

/*
#define cAf4UnusedMask                                 cBit2
#define cAf4UnusedShift                                2
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: Sk_Mem_Fsm
BitField Type: R/W
BitField Desc: Member Finite-State-Machine
               - 00: IDLE state
               - 01: FAIL state
               - 10: OK state
               - 11: FIX state (used for VCAT none-LCAS mode)
BitField Bits: 1_0
--------------------------------------*/
#define cAf4SkMemFsmMask                               cBit1_0
#define cAf4SkMemFsmShift                              0
#define cAf4SkMemFsmMaxVal                             0x3
#define cAf4SkMemFsmMinVal                             0x0
#define cAf4SkMemFsmRstVal                             0x0



/*------------------------------------------------------------------------------
Reg Name: Sink Transmit MST
Reg Addr: 0x756000
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapSkTransMst(vcgId)                    (0x755100+(vcgId))

#define cAf4RegMapSkTransMstRstVal                     0xFFFFFFFF
#define cAf4RegMapSkTransMstRwMsk                      0xFFFFFFFF
#define cAf4RegMapSkTransMstRwcMsk                     0x00000000
#define cAf4SkTransMstMask(mem)                        (cBit0 << (mem))
#define cAf4SkTransMstShift(mem)                       (mem)



/*------------------------------------------------------------------------------
Reg Name: Source Receive MST
Reg Addr: 0x755000
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapSoRecMst(vcgId)                      (0x755000+(vcgId))

#define cAf4RegMapSoRecMstRstVal                       0xFFFFFFFF
#define cAf4RegMapSoRecMstRwMsk                        0xFFFFFFFF
#define cAf4RegMapSoRecMstRwcMsk                       0x00000000
#define cAf4SoRecMstMask(mem)                          (cBit0 << (mem))
#define cAf4SoRecMstShift(mem)                         (mem)

/*------------------------------------------------------------------------------
Reg Name: RS-ACK
Reg Addr: 0x700101
Reg Desc:
------------------------------------------------------------------------------*/
#define cAf4RegMapRsAck(vcgId)                          (0x755200+(vcgId))
#define cAf4RxRsAckMask                                 cBit1
#define cAf4RxRsAckShift                                1
#define cAf4TxRsAckMask                                 cBit0
#define cAf4TxRsAckShift                                0

/*------------------------------------------------------------------------------
Reg Name: Expected Sequence Configuration
Reg Addr: 0x751800-0x751DFB
          The address format for these registers is 0x751800 + sts_id*32 +
          vtg_id*4 + vt_id
          Where: sts_id: 0-11
          vtg_id: 0-6
          vt_id: 0-3
Reg Desc: This register is used to
------------------------------------------------------------------------------*/
#define cAf4RegMapSkExpSeq                            0x751800

#define cAf4RegMapSkExpSeqRstVal                      0x00000000
#define cAf4SkMemExpSeqMask                           cBit7_0
#define cAf4SkMemExpSeqShift                          0
#define cAf4SkMemExpSeqMaxVal                         0x3f
#define cAf4SkMemExpSeqMinVal                         0x0
#define cAf4SkMemExpSeqRstVal                         0x0





/*------------------------------------------------------------------------------
Reg Name:  2.3.20. Channel Current Status
Reg Addr: 0x757000
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapMemStat                               0x757000

#define cAf4RegMapMemStatGIDMask                        (cBit6)
#define cAf4RegMapMemStatGIDShift                       (6)
#define cAf4RegMapMemStatLOAMask                        (cBit5)
#define cAf4RegMapMemStatLOAShift                       (5)
#define cAf4RegMapMemStatLOMMask                        (cBit4)
#define cAf4RegMapMemStatLOMShift                       (4)
#define cAf4RegMapMemStatMNDMask                        (cBit3)
#define cAf4RegMapMemStatMNDShift                       (3)
#define cAf4RegMapMemStatSQMMask                        (cBit2)
#define cAf4RegMapMemStatSQMShift                       (2)
#define cAf4RegMapMemStatSQNCMask                       (cBit1)
#define cAf4RegMapMemStatSQNCShift                      (1)
#define cAf4RegMapMemStatCRCMask                        (cBit0)
#define cAf4RegMapMemStatCRCShift                       (0)

/*------------------------------------------------------------------------------
Reg Name:   2.3.19. Channel Interrupt Status
Reg Addr: 0x756800
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapMemIntrStat                                0x756800

#define cAf4RegMapMemIntrStatGIDMask                        (cBit6)
#define cAf4RegMapMemIntrStatGIDShift                       (6)
#define cAf4RegMapMemIntrStatLOAMask                        (cBit5)
#define cAf4RegMapMemIntrStatLOAShift                       (5)
#define cAf4RegMapMemIntrStatLOMMask                        (cBit4)
#define cAf4RegMapMemIntrStatLOMShift                       (4)
#define cAf4RegMapMemIntrStatMNDMask                        (cBit3)
#define cAf4RegMapMemIntrStatMNDShift                       (3)
#define cAf4RegMapMemIntrStatSQMMask                        (cBit2)
#define cAf4RegMapMemIntrStatSQMShift                       (2)
#define cAf4RegMapMemIntrStatSQNCMask                       (cBit1)
#define cAf4RegMapMemIntrStatSQNCShift                      (1)
#define cAf4RegMapMemIntrStatCRCMask                        (cBit0)
#define cAf4RegMapMemIntrStatCRCShift                       (0)


/*------------------------------------------------------------------------------
Reg Name: Test Register
Reg Addr: 0x700100
Reg Desc: This register is used to control the DDR II Controller.
------------------------------------------------------------------------------*/
#define cAf4RegMapTestReg                              0x700100

#define cAf4RegMapTestRegRstVal                        0x00000000
#define cAf4RegMapTestRegRwMsk                         0x0000000F
#define cAf4RegMapTestRegRwcMsk                        0x00000000
/*
#define cAf4UnusedMask                                 cBit31_4
#define cAf4UnusedShift                                4
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: DdrTestMode[2:0]
BitField Type: R/W
BitField Desc: Test Mode. DdrTestMode[2]: DdrTestMode[1]: Unused
               DdrTestMode[0]: Test Mode enable bit
               - 1: PRBS mode
               - 0: Fix-Pattern mode
BitField Bits: 3_1
--------------------------------------*/
#define cAf4DdrTestModeMask                            cBit3_1
#define cAf4DdrTestModeShift                           1
#define cAf4DdrTestModeMaxVal                          0x7
#define cAf4DdrTestModeMinVal                          0x0
#define cAf4DdrTestModeRstVal                          0x0

/*--------------------------------------
BitField Name: DdrFifoFlush
BitField Type: R/W
BitField Desc: Flush Fifo
BitField Bits: 0
--------------------------------------*/
#define cAf4DdrFifoFlushMask                           cBit0
#define cAf4DdrFifoFlushShift                          0
#define cAf4DdrFifoFlushMaxVal                         0x1
#define cAf4DdrFifoFlushMinVal                         0x0
#define cAf4DdrFifoFlushRstVal                         0x0




/*------------------------------------------------------------------------------
Reg Name: Read-To-Read Threshold Control
Reg Addr: 0x700101
Reg Desc: This register is the Read-to-Read threshold of DDR II Controller.
------------------------------------------------------------------------------*/
#define cAf4RegMapReadToReadThresCtrl                  0x700101

#define cAf4RegMapReadToReadThresCtrlRstVal            0x0A080604
#define cAf4RegMapReadToReadThresCtrlRwMsk             0xFFFFFFFF
#define cAf4RegMapReadToReadThresCtrlRwcMsk            0x00000000
/*--------------------------------------
BitField Name: DdrR2RThresh4[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_24
--------------------------------------*/
#define cAf4DdrR2RThresh4Mask                          cBit31_24
#define cAf4DdrR2RThresh4Shift                         24
#define cAf4DdrR2RThresh4MaxVal                        0xFF
#define cAf4DdrR2RThresh4MinVal                        0x0
#define cAf4DdrR2RThresh4RstVal                        0xA

/*--------------------------------------
BitField Name: DdrR2RThresh3[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 23_16
--------------------------------------*/
#define cAf4DdrR2RThresh3Mask                          cBit23_16
#define cAf4DdrR2RThresh3Shift                         16
#define cAf4DdrR2RThresh3MaxVal                        0xFF
#define cAf4DdrR2RThresh3MinVal                        0x0
#define cAf4DdrR2RThresh3RstVal                        0x8

/*--------------------------------------
BitField Name: DdrR2RThresh2[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 15_8
--------------------------------------*/
#define cAf4DdrR2RThresh2Mask                          cBit15_8
#define cAf4DdrR2RThresh2Shift                         8
#define cAf4DdrR2RThresh2MaxVal                        0xFF
#define cAf4DdrR2RThresh2MinVal                        0x0
#define cAf4DdrR2RThresh2RstVal                        0x6

/*--------------------------------------
BitField Name: DdrR2RThresh1[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 7_0
--------------------------------------*/
#define cAf4DdrR2RThresh1Mask                          cBit7_0
#define cAf4DdrR2RThresh1Shift                         0
#define cAf4DdrR2RThresh1MaxVal                        0xFF
#define cAf4DdrR2RThresh1MinVal                        0x0
#define cAf4DdrR2RThresh1RstVal                        0x4




/*------------------------------------------------------------------------------
Reg Name: Read-To-Write Threshold Control
Reg Addr: 0x700102
Reg Desc: This register is the Read-to-Write threshold of DDR II Controller.
------------------------------------------------------------------------------*/
#define cAf4RegMapReadToWriteThresCtrl                 0x700102

#define cAf4RegMapReadToWriteThresCtrlRstVal           0x0A080604
#define cAf4RegMapReadToWriteThresCtrlRwMsk            0xFFFFFFFF
#define cAf4RegMapReadToWriteThresCtrlRwcMsk           0x00000000
/*--------------------------------------
BitField Name: DdrR2WThresh4[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_24
--------------------------------------*/
#define cAf4DdrR2WThresh4Mask                          cBit31_24
#define cAf4DdrR2WThresh4Shift                         24
#define cAf4DdrR2WThresh4MaxVal                        0xFF
#define cAf4DdrR2WThresh4MinVal                        0x0
#define cAf4DdrR2WThresh4RstVal                        0xA

/*--------------------------------------
BitField Name: DdrR2WThresh3[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 23_16
--------------------------------------*/
#define cAf4DdrR2WThresh3Mask                          cBit23_16
#define cAf4DdrR2WThresh3Shift                         16
#define cAf4DdrR2WThresh3MaxVal                        0xFF
#define cAf4DdrR2WThresh3MinVal                        0x0
#define cAf4DdrR2WThresh3RstVal                        0x8

/*--------------------------------------
BitField Name: DdrR2WThresh2[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 15_8
--------------------------------------*/
#define cAf4DdrR2WThresh2Mask                          cBit15_8
#define cAf4DdrR2WThresh2Shift                         8
#define cAf4DdrR2WThresh2MaxVal                        0xFF
#define cAf4DdrR2WThresh2MinVal                        0x0
#define cAf4DdrR2WThresh2RstVal                        0x6

/*--------------------------------------
BitField Name: DdrR2WThresh1[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 7_0
--------------------------------------*/
#define cAf4DdrR2WThresh1Mask                          cBit7_0
#define cAf4DdrR2WThresh1Shift                         0
#define cAf4DdrR2WThresh1MaxVal                        0xFF
#define cAf4DdrR2WThresh1MinVal                        0x0
#define cAf4DdrR2WThresh1RstVal                        0x4




/*------------------------------------------------------------------------------
Reg Name: Write-To-Read Threshold Control
Reg Addr: 0x700103
Reg Desc: This register is the Write-to-Read threshold of DDR II Controller.
------------------------------------------------------------------------------*/
#define cAf4RegMapWriteToReadThresCtrl                 0x700103

#define cAf4RegMapWriteToReadThresCtrlRstVal           0x0B090705
#define cAf4RegMapWriteToReadThresCtrlRwMsk            0xFFFFFFFF
#define cAf4RegMapWriteToReadThresCtrlRwcMsk           0x00000000
/*--------------------------------------
BitField Name: DdrW2RThresh4[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_24
--------------------------------------*/
#define cAf4DdrW2RThresh4Mask                          cBit31_24
#define cAf4DdrW2RThresh4Shift                         24
#define cAf4DdrW2RThresh4MaxVal                        0xFF
#define cAf4DdrW2RThresh4MinVal                        0x0
#define cAf4DdrW2RThresh4RstVal                        0xB

/*--------------------------------------
BitField Name: DdrW2RThresh3[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 23_16
--------------------------------------*/
#define cAf4DdrW2RThresh3Mask                          cBit23_16
#define cAf4DdrW2RThresh3Shift                         16
#define cAf4DdrW2RThresh3MaxVal                        0xFF
#define cAf4DdrW2RThresh3MinVal                        0x0
#define cAf4DdrW2RThresh3RstVal                        0x9

/*--------------------------------------
BitField Name: DdrW2RThresh2[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 15_8
--------------------------------------*/
#define cAf4DdrW2RThresh2Mask                          cBit15_8
#define cAf4DdrW2RThresh2Shift                         8
#define cAf4DdrW2RThresh2MaxVal                        0xFF
#define cAf4DdrW2RThresh2MinVal                        0x0
#define cAf4DdrW2RThresh2RstVal                        0x7

/*--------------------------------------
BitField Name: DdrW2RThresh1[7:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 7_0
--------------------------------------*/
#define cAf4DdrW2RThresh1Mask                          cBit7_0
#define cAf4DdrW2RThresh1Shift                         0
#define cAf4DdrW2RThresh1MaxVal                        0xFF
#define cAf4DdrW2RThresh1MinVal                        0x0
#define cAf4DdrW2RThresh1RstVal                        0x5




/*------------------------------------------------------------------------------
Reg Name: Common Threshold Control
Reg Addr: 0x700104
Reg Desc: This register is the common thresholds of DDR II Controller.
------------------------------------------------------------------------------*/
#define cAf4RegMapComThresCtrl                         0x700104

#define cAf4RegMapComThresCtrlRstVal                   0x12F9448C
#define cAf4RegMapComThresCtrlRwMsk                    0x7FFFFFFF
#define cAf4RegMapComThresCtrlRwcMsk                   0x00000000
/*
#define cAf4UnusedMask                                 cBit31
#define cAf4UnusedShift                                31
#define cAf4UnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: DdrRefreshThresh[11:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 30_19
--------------------------------------*/
#define cAf4DdrRefreshThreshMask                       cBit30_19
#define cAf4DdrRefreshThreshShift                      19
#define cAf4DdrRefreshThreshMaxVal                     0xFFF
#define cAf4DdrRefreshThreshMinVal                     0x0
#define cAf4DdrRefreshThreshRstVal                     0x25F

/*--------------------------------------
BitField Name: DdrValidThresh[4:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 18_14
--------------------------------------*/
#define cAf4DdrValidThreshMask                         cBit18_14
#define cAf4DdrValidThreshShift                        14
#define cAf4DdrValidThreshMaxVal                       0x1F
#define cAf4DdrValidThreshMinVal                       0x0
#define cAf4DdrValidThreshRstVal                       0x5

/*--------------------------------------
BitField Name: DdrRefreshLatency[6:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 13_7
--------------------------------------*/
#define cAf4DdrRefreshLatencyMask                      cBit13_7
#define cAf4DdrRefreshLatencyShift                     7
#define cAf4DdrRefreshLatencyMaxVal                    0x7F
#define cAf4DdrRefreshLatencyMinVal                    0x0
#define cAf4DdrRefreshLatencyRstVal                    0x9

/*--------------------------------------
BitField Name: DdrSameBankThresh[6:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 6_0
--------------------------------------*/
#define cAf4DdrSameBankThreshMask                      cBit6_0
#define cAf4DdrSameBankThreshShift                     0
#define cAf4DdrSameBankThreshMaxVal                    0x7F
#define cAf4DdrSameBankThreshMinVal                    0x0
#define cAf4DdrSameBankThreshRstVal                    0xC




/*------------------------------------------------------------------------------
Reg Name: Test Fixed-Pattern Write
Reg Addr: 0x700105
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapTestFixedpatWrite                    0x700105

#define cAf4RegMapTestFixedpatWriteRstVal              0x00000000
#define cAf4RegMapTestFixedpatWriteRwMsk               0xFFFFFFFF
#define cAf4RegMapTestFixedpatWriteRwcMsk              0x00000000
/*--------------------------------------
BitField Name: FixPatern
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_0
--------------------------------------*/
#define cAf4FixPaternMask                              cBit31_0
#define cAf4FixPaternShift                             0
#define cAf4FixPaternMaxVal                            0xFFFFFFFF
#define cAf4FixPaternMinVal                            0x0
#define cAf4FixPaternRstVal                            0x0




/*------------------------------------------------------------------------------
Reg Name: Test Data Mask
Reg Addr: 0x700106
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapTestDataMask                         0x700106

#define cAf4RegMapTestDataMaskRstVal                   0x00000000
#define cAf4RegMapTestDataMaskRwMsk                    0xFFFFFFFF
#define cAf4RegMapTestDataMaskRwcMsk                   0x00000000
/*--------------------------------------
BitField Name: DataMask
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_0
--------------------------------------*/
#define cAf4DataMaskMask                               cBit31_0
#define cAf4DataMaskShift                              0
#define cAf4DataMaskMaxVal                             0xFFFFFFFF
#define cAf4DataMaskMinVal                             0x0
#define cAf4DataMaskRstVal                             0x0




/*------------------------------------------------------------------------------
Reg Name: Test Data Read After Mask
Reg Addr: 0x700108
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapTestDataReadAfterMask                0x700108

#define cAf4RegMapTestDataReadAfterMaskRstVal          0x00000000
#define cAf4RegMapTestDataReadAfterMaskRwMsk           0xFFFFFFFF
#define cAf4RegMapTestDataReadAfterMaskRwcMsk          0x00000000
/*--------------------------------------
BitField Name: FixPatern
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_0
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
FixPatern in 2.4.8.Test Data Read After Mask
 have declare in 2.4.6. Test Fixed-Pattern Write
*/
#define cAf4FixPaternMask                              cBit31_0
#define cAf4FixPaternShift                             0
#define cAf4FixPaternMaxVal                            0xFFFFFFFF
#define cAf4FixPaternMinVal                            0x0
#define cAf4FixPaternRstVal                            0x0




/*------------------------------------------------------------------------------
Reg Name: Test Data Read Before Mask
Reg Addr: 0x70010F
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegMapTestDataReadBeforeMask               0x70010F

#define cAf4RegMapTestDataReadBeforeMaskRstVal         0x00000000
#define cAf4RegMapTestDataReadBeforeMaskRwMsk          0xFFFFFFFF
#define cAf4RegMapTestDataReadBeforeMaskRwcMsk         0x00000000
/*--------------------------------------
BitField Name: FixPaternMask
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_0
--------------------------------------*/
#define cAf4FixPaternMaskMask                          cBit31_0
#define cAf4FixPaternMaskShift                         0
#define cAf4FixPaternMaskMaxVal                        0xFFFFFFFF
#define cAf4FixPaternMaskMinVal                        0x0
#define cAf4FixPaternMaskRstVal                        0x0




/*------------------------------------------------------------------------------
Reg Name: Test Error Counter
Reg Addr: 0x700109
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegapTestErrCnt                            0x700109

#define cAf4RegMapTestErrCntRstVal                     0x00000000
#define cAf4RegMapTestErrCntRwMsk                      0xFFFFFFFF
#define cAf4RegMapTestErrCntRwcMsk                     0x00000000
/*--------------------------------------
BitField Name: ErrCnt
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 31_0
--------------------------------------*/
#define cAtErrCntMask                                 cBit31_0
#define cAtErrCntShift                                0
#define cAtErrCntMaxVal                               0xFFFFFFFF
#define cAtErrCntMinVal                               0x0
#define cAtErrCntRstVal                               0x0


/*------------------------------------------------------------------------------
Reg Name: Threshold
Reg Addr: 0x755400
Reg Desc:
------------------------------------------------------------------------------*/
#define cAf4RegMapSkThr(vcgId)                          (0x755400 + (vcgId))

#define cAf4MapSkThrRmvDisMask                          cBit31
#define cAf4MapSkThrRmvDisShift                         31
#define cAf4MapSkThrWTRDisMask                          cBit30
#define cAf4MapSkThrWTRDisShift                         30
#define cAf4MapSkThrHODisMask                           cBit29
#define cAf4MapSkThrHODisShift                          29

#define cAf4MapSkThrRmvMask                             cBit28_21
#define cAf4MapSkThrRmvShift                            21
#define cAf4MapSkThrWTRMask                             cBit20_8
#define cAf4MapSkThrWTRShift                            8
#define cAf4MapSkThrHOMask                              cBit7_0
#define cAf4MapSkThrHOShift                             0


/*------------------------------------------------------------------------------
Reg Name: Source Member Status
Reg Addr: 0x721000-0x72117B
          The address format for these registers is 0x721000 + sts_id*32 +
          vtg_id*4 + vt_id
          Where: sts_id: 0-11
          vtg_id: 0-6
          vt_id: 0-3
Reg Desc: This register is used to
------------------------------------------------------------------------------*/



#define cAf4RegMapSkVcgEn(vcgId)                      (0x74b000 + (vcgId))
#define cAf4RegMapSoVcgStat(vcgId)                    (0x721800 + (vcgId))
#define cAf4RegMapSoVcgStatRstVal                     0x0

#define cAf4Ds1InStsMask(vtg, vt)                     (cBit0 << (((vtg) * 4) + (vt)))
#define cAf4Ds1InStsShift(vtg, vt)                    (((vtg) * 4) + (vt))


#define cAf4RegMapSkVcgStat(vcgId)                    (0x751000+(vcgId))
#define cAf4RegMapSkVcgStatRstVal                     0x0

#define cAf4RegMapSoRxMST(vcgId)                    (0x726000+(vcgId))
#define cAf4RegMapSoRxMSTRstVal                     0xFFFFFFFF


/*------------------------------------------------------------------------------
Reg Name:  2.2.2. Source Indirect Control
Reg Addr: 0x72010A
Reg Desc: Description:
------------------------------------------------------------------------------*/
#define cAf4RegSourceIndirectCtrl                      0x72010A
#define cAf4RegSourceIndirectCtrlRstVal                0x00000000

/*--------------------------------------
BitField Name: RdWr Pend
BitField Type: RO
BitField Desc: Read/Write in-progress
BitField Bits: 28
--------------------------------------*/
#define cAf4SourceRdWrPendMask                         cBit28
#define cAf4SourceRdWrPendShift                        28

/*--------------------------------------
BitField Name: Type Select
BitField Type: R/W
BitField Desc: Select Transmit Sequence Configuration addresses
- Tx sequence Select: 0x100
- MemStat Select    : 0x010
- MembCtrl Select   : 0x001
BitField Bits: 27
--------------------------------------*/
#define cAf4SourceTypeSelectMask                        cBit27_25
#define cAf4SourceTypeSelectShift                       25

/*--------------------------------------
BitField Name: TxSeq Select
BitField Type: R/W
BitField Desc: Select Transmit Sequence Configuration addresses
BitField Bits: 27
--------------------------------------*/
#define cAf4SourceTxSequenceMask                        cBit27
#define cAf4SourceTxSequenceShift                       27

/*--------------------------------------
BitField Name: MemStat Select
BitField Type: R/W
BitField Desc: select Source Member Status addresses
BitField Bits: 26
--------------------------------------*/
#define cAf4SourceMembStatMask                          cBit26
#define cAf4SourceMembStatShift                         26

/*--------------------------------------
BitField Name: MembCtrl Select
BitField Type: R/W
BitField Desc: select Member Control Configuration addresses
BitField Bits: 25
--------------------------------------*/
#define cAf4SourceMemCtrlMask                           cBit25
#define cAf4SourceMemCtrlShift                          25

/*--------------------------------------
BitField Name: Indirect WriteNotRead
BitField Type: R/W
BitField Desc: Command read/write
BitField Bits: 24
--------------------------------------*/
#define cAf4SourceWriteNotReadMask                      cBit24
#define cAf4SourceWriteNotReadShift                     24

/*--------------------------------------
BitField Name: Indirect Address Access
BitField Type: R/W
BitField Desc: Command read/write
BitField Bits: 23_0
--------------------------------------*/
#define cAf4SourceIndrAddressMask                       cBit23_0
#define cAf4SourceIndrAddressShift                      0

/*------------------------------------------------------------------------------
Reg Name:   2.2.3. Source Indirect Data
Reg Addr: 0x72010B
Reg Desc: This register is used to show data when read/write indirect.
------------------------------------------------------------------------------*/
#define cAf4RegSourceIndirectData                      0x72010B

/*------------------------------------------------------------------------------
Reg Name: 2.3.10. Sink Member Control Indirect Control
Reg Addr: 0x748009
Reg Desc: Description: This register is used to control indirect read/write at sink
------------------------------------------------------------------------------*/
#define cAf4RegSinkMembCtrlIndirectCtrl                      0x748009
#define cAf4RegSinkMembCtrlIndirectCtrlRstVal                0x00000000

/*--------------------------------------
BitField Name: RdWr Pend
BitField Type: RO
BitField Desc: Read/Write in-progress
BitField Bits: 26
--------------------------------------*/
#define cAf4SinkMembCtrlRdWrPendMask                         cBit26
#define cAf4SinkMembCtrlRdWrPendShift                        26

/*--------------------------------------
BitField Name: MembCtrl Select
BitField Type: R/W
BitField Desc: select Member Control Configuration addresses
BitField Bits: 25
--------------------------------------*/
#define cAf4SinkMembCtrlMemCtrlMask                           cBit25
#define cAf4SinkMembCtrlMemCtrlShift                          25

/*--------------------------------------
BitField Name: Indirect WriteNotRead
BitField Type: R/W
BitField Desc: Command read/write
BitField Bits: 24
--------------------------------------*/
#define cAf4SinkMembCtrlWriteNotReadMask                      cBit24
#define cAf4SinkMembCtrlWriteNotReadShift                     24

/*--------------------------------------
BitField Name: Indirect Address Access
BitField Type: R/W
BitField Desc: Command read/write
BitField Bits: 23_0
--------------------------------------*/
#define cAf4SinkMembCtrlIndrAddressMask                       cBit23_0
#define cAf4SinkMembCtrlIndrAddressShift                      0

/*------------------------------------------------------------------------------
Reg Name:  2.3.11. Sink Member Control Indirect Data
Reg Addr: 0x74800A
Reg Desc: This register is used to show data when read/write indirect.
------------------------------------------------------------------------------*/
#define cAf4RegSinkMembCtrlIndirectData                      0x74800A

/*------------------------------------------------------------------------------
Reg Name:  2.3.13. Sink Member Status Indirect Control
Reg Addr: 0x750004
Reg Desc: Description: This register is used to control indirect read/write at sink
------------------------------------------------------------------------------*/
#define cAf4RegSinkMembStatIndirectCtrl                      0x750004
#define cAf4RegSinkMembStatIndirectCtrlRstVal                0x00000000

/*--------------------------------------
BitField Name: RdWr Pend
BitField Type: RO
BitField Desc: Read/Write in-progress
BitField Bits: 26
--------------------------------------*/
#define cAf4SinkMembStatRdWrPendMask                         cBit26
#define cAf4SinkMembStatRdWrPendShift                        26

/*--------------------------------------
BitField Name: MembCtrl Select
BitField Type: R/W
BitField Desc: select Member Control Configuration addresses
BitField Bits: 25
--------------------------------------*/
#define cAf4SinkMembStatMemCtrlMask                           cBit25
#define cAf4SinkMembStatMemCtrlShift                          25

/*--------------------------------------
BitField Name: Indirect WriteNotRead
BitField Type: R/W
BitField Desc: Command read/write
BitField Bits: 24
--------------------------------------*/
#define cAf4SinkMembStatWriteNotReadMask                      cBit24
#define cAf4SinkMembStatWriteNotReadShift                     24

/*--------------------------------------
BitField Name: Indirect Address Access
BitField Type: R/W
BitField Desc: Command read/write
BitField Bits: 23_0
--------------------------------------*/
#define cAf4SinkMembStatIndrAddressMask                       cBit23_0
#define cAf4SinkMembStatIndrAddressShift                      0

/*------------------------------------------------------------------------------
Reg Name: 2.3.14. Sink Member Status Indirect Data
Reg Addr: 0x750005
Reg Desc: This register is used to show data when read/write indirect.
------------------------------------------------------------------------------*/
#define cAf4RegSinkMembStatIndirectData                      0x750005

#define cAf4IndrSourceRegTypeTxSeq                              0x4
#define cAf4IndrSourceRegTypeMembStat                           0x2
#define cAf4IndrSourceRegTypeMembCtrl                           0x1

#define cAf4IndrSinkRegTypeMembStat                             0x1
#define cAf4IndrSinkRegTypeMembCtrl                             0x2

#endif /* _THAMODULECONCATEREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : ThaModuleDemap.c
 *
 * Created Date: Mar 21, 2013
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pw/ThaModulePw.h"
#include "ThaModuleDemapInternal.h"
#include "ThaModuleMapInternal.h"
#include "ThaModuleMapReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegDemapVc3StuffDisMask(sts)     (cBit0 << (sts))
#define cRegDemapVc3StuffDisShift(sts)    (sts)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(sefl) ((ThaModuleDemap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleDemapMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* Save super implementations */
static const tAtModuleMethods             *m_AtModuleMethods = NULL;
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleDemap, DmapChnType)
mDefineMaskShift(ThaModuleDemap, DmapFirstTs)
mDefineMaskShift(ThaModuleDemap, DmapTsEn)
mDefineMaskShift(ThaModuleDemap, DmapPwId)

static uint32 De1DmapChnCtrl(ThaModuleDemap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cThaRegDmapChnCtrl;
    }

static uint32 VcDmapChnCtrl(ThaModuleDemap self, AtSdhVc vc)
    {
    AtUnused(vc);
    return mMethodsGet(self)->De1DmapChnCtrl(self, NULL);
    }

static uint32 De3DmapChnCtrl(ThaModuleDemap self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    return mMethodsGet(self)->De1DmapChnCtrl(self, NULL);
    }

static uint32 De3LiuOverCepDemapCtrl(ThaModuleDemap self)
    {
    AtUnused(self);
    return cThaRegDmapDe3LiuOverCepCtrl;
    }

static eAtRet DemapVc3StuffEnable(ThaModuleDemap self, AtSdhVc channel, eBool enable)
    {
    eAtRet ret;
    uint8 slice, stsInSlice;
    uint32 address, regVal;

    ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)channel, cThaModuleDemap, &slice, &stsInSlice);
    if (ret != cAtOk)
        return ret;

    address = ThaModuleAbstractMapSdhSliceOffset((ThaModuleAbstractMap)self, (AtSdhChannel)channel, slice) +
              mMethodsGet(self)->De3LiuOverCepDemapCtrl(self);
    regVal = mChannelHwRead(channel, address, cThaModuleDemap);
    mFieldIns(&regVal,
              cRegDemapVc3StuffDisMask(stsInSlice),
              cRegDemapVc3StuffDisShift(stsInSlice),
              (enable) ? 0 : 1);
    mChannelHwWrite(channel, address, regVal, cThaModuleDemap);

    return cAtOk;
    }

static eBool DemapVc3StuffIsEnabled(ThaModuleDemap self, AtSdhVc channel)
    {
    eAtRet ret;
    uint8 slice, stsInSlice;
    uint32 address, regVal;

    ret = ThaSdhChannel2HwMasterStsId((AtSdhChannel)channel, cThaModuleDemap, &slice, &stsInSlice);
    if (ret != cAtOk)
        return cAtFalse;

    address = ThaModuleAbstractMapSdhSliceOffset((ThaModuleAbstractMap)self, (AtSdhChannel)channel, slice) +
              mMethodsGet(self)->De3LiuOverCepDemapCtrl(self);
    regVal = mChannelHwRead(channel, address, cThaModuleDemap);
    return (regVal & cRegDemapVc3StuffDisMask(stsInSlice)) ? cAtFalse : cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleDemap);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    localAddress = localAddress & cBit23_0;
    if ((localAddress >= 0x1A0000) &&
        (localAddress <= 0x1A01FF))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet De1Init(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    uint8 i;
    ThaModuleDemap demapModule = (ThaModuleDemap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)de1), cThaModuleDemap);
    static const uint8 numberOfTimeSlots = 32;
    uint32 offset;

    if (demapModule == NULL)
        return cAtErrorNullPointer;

    for (i = 0; i < numberOfTimeSlots; i++)
        {
        offset = mMethodsGet(self)->Ds0DefaultOffset(self, (AtPdhDe1)de1, i);
        mChannelHwWrite(de1, mMethodsGet(demapModule)->De1DmapChnCtrl(demapModule, de1) + offset, 0x0, cThaModuleDemap);
        }

    return cAtOk;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleMapRegisterIteratorCreate(self);
    }

static eAtRet TimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 regVal = 0;
    uint32 mask, shift;

    AtUnused(timeslot);

    mask  = mFieldMask(demapModule, DmapFirstTs);
    shift = mFieldShift(demapModule, DmapFirstTs);
    mFieldIns(&regVal, mask, shift, slotInfo->firstTimeslot);

    mask  = mFieldMask(demapModule, DmapTsEn);
    shift = mFieldShift(demapModule, DmapTsEn);
    mFieldIns(&regVal, mask, shift, mBoolToBin(slotInfo->enabled));

    mask  = mFieldMask(demapModule, DmapChnType);
    shift = mFieldShift(demapModule, DmapChnType);
    mFieldIns(&regVal, mask, shift, slotInfo->chnType);

    mask  = mFieldMask(demapModule, DmapPwId);
    shift = mFieldShift(demapModule, DmapPwId);
    mFieldIns(&regVal, mask, shift, slotInfo->pwId);

    mChannelHwWrite(channel, address, regVal, cThaModuleDemap);

    return cAtOk;
    }

static eAtRet TimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 regVal = mChannelHwRead(channel, address, cThaModuleDemap);
    uint32 fieldMask, fieldShift;

    AtUnused(timeslot);

    fieldMask  = mFieldMask(demapModule, DmapFirstTs);
    fieldShift = mFieldShift(demapModule, DmapFirstTs);
    slotInfo->firstTimeslot = (uint8)mRegField(regVal, field);

    fieldMask  = mFieldMask(demapModule, DmapChnType);
    fieldShift = mFieldShift(demapModule, DmapChnType);
    slotInfo->chnType = (uint8)mRegField(regVal, field);

    fieldMask  = mFieldMask(demapModule, DmapPwId);
    fieldShift = mFieldShift(demapModule, DmapPwId);
    slotInfo->pwId = (uint16)mRegField(regVal, field);

    fieldMask  = mFieldMask(demapModule, DmapTsEn);
    fieldShift = mFieldShift(demapModule, DmapTsEn);
    slotInfo->enabled = (uint8)mRegField(regVal, field);

    return cAtOk;
    }

static eAtRet BindDs0ToEncapChannel(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtEncapChannel encapChannel)
    {
    return ThaModuleAbstractMapBindDs0ToChannel(self, de1, ds0BitMask, (AtChannel)encapChannel, cBindEncapType);
    }

static eAtRet BindDe1ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe1 de1, AtConcateGroup concateGroup)
    {
    const uint32 ds1Ds0BitMask = cBit23_0;
    const uint32 e1Ds0BitMask = cBit31_1;   /* E1 PCM-31CRC only. */
    AtChannel concateChannel = mMethodsGet(self)->ConcateChannelForBinding(self, (AtChannel)de1, concateGroup);
    return ThaModuleAbstractMapBindDs0ToChannel(self, de1, AtPdhDe1IsE1(de1) ? e1Ds0BitMask : ds1Ds0BitMask, concateChannel, cBindVcgType);
    }

static AtChannel ConcateChannelForBinding(ThaModuleAbstractMap self, AtChannel channel, AtConcateGroup concateGroup)
    {
    AtUnused(self);
    AtUnused(channel);
    return (AtChannel)concateGroup;
    }

static uint8 PwAtmHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 3;
    }

static uint8 PwCesCasHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 2;
    }

static eAtRet Ds0RegDisplay(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask)
    {
    uint8 timeslotId;
    uint32 offset, address;
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 bit_i = cBit0;

    timeslotId = 0;
    while (ds0BitMask != 0)
        {
        /* Only care 1-bit */
        if (ds0BitMask & bit_i)
            {
            offset = mMethodsGet(self)->Ds0DefaultOffset(self, de1, timeslotId);
            address = mMethodsGet(demapModule)->De1DmapChnCtrl(demapModule, de1) + offset;
            ThaDeviceChannelRegValueDisplay((AtChannel)de1, address, cThaModuleDemap);
            }

        /* Next slot */
        ds0BitMask = ds0BitMask & (~bit_i);
        bit_i      = bit_i << 1;
        timeslotId = (uint8)(timeslotId + 1);
        }

    return cAtOk;
    }

static void De1RegShow(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    uint32 ds0BitMask = ThaPdhDe1SatopDs0BitMaskGet((ThaPdhDe1)de1);

    ThaDeviceRegNameDisplay("Demap Channel Control");
    Ds0RegDisplay(self, de1, ds0BitMask);
    }

static void PdhChannelRegsShow(ThaModuleAbstractMap self, AtPdhChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);
    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        De1RegShow(self, (AtPdhDe1)pdhChannel);
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "demap";
    }

static uint32 De1ChnCtrl(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    return mMethodsGet(mThis(self))->De1DmapChnCtrl(mThis(self), de1);
    }

static uint32 VcChnCtrl(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    return mMethodsGet(mThis(self))->VcDmapChnCtrl(mThis(self), vc);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, De1Init);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDs0ToEncapChannel);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PwAtmHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PwCesCasHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PdhChannelRegsShow);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe1ToConcateGroup);
        mMethodOverride(m_ThaModuleAbstractMapOverride, ConcateChannelForBinding);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1ChnCtrl);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcChnCtrl);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoGet);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, TypeString);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    }

static void MethodsInit(ThaModuleDemap self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Override bit fields */
        mBitFieldOverride(ThaModuleDemap, m_methods, DmapChnType)
        mBitFieldOverride(ThaModuleDemap, m_methods, DmapFirstTs)
        mBitFieldOverride(ThaModuleDemap, m_methods, DmapTsEn)
        mBitFieldOverride(ThaModuleDemap, m_methods, DmapPwId)

        /* Override register address */
        mMethodOverride(m_methods, De1DmapChnCtrl);
        mMethodOverride(m_methods, VcDmapChnCtrl);
        mMethodOverride(m_methods, De3DmapChnCtrl);
        mMethodOverride(m_methods, De3LiuOverCepDemapCtrl);
        }

    mMethodsSet(self, &m_methods);
    }

AtModule ThaModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleAbstractMapObjectInit(self, cThaModuleDemap, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaModuleDemap)self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleDemapObjectInit(newModule, device);
    }

uint32 ThaModuleDemapPartOffset(ThaModuleAbstractMap self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleDemap, partId);
    }

uint32 ThaModuleDemapVcDmapChnCtrl(ThaModuleDemap self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->VcDmapChnCtrl(self, vc);
    return cInvalidUint32;
    }

eAtRet ThaModuleDemapVc3StuffEnable(ThaModuleDemap self, AtSdhVc channel, eBool enable)
    {
    if (self && channel)
        return DemapVc3StuffEnable(self, channel, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModuleDemapVc3StuffIsEnabled(ThaModuleDemap self, AtSdhVc channel)
    {
    if (self && channel)
        return DemapVc3StuffIsEnabled(self, channel);
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : ThaModuleMapRegisterIteratorInternal.h
 * 
 * Created Date: Jan 12, 2013
 *
 * Description : Register iterator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMAPREGISTERITERATORINTERNAL_H_
#define _THAMODULEMAPREGISTERITERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleMapRegisterIterator * ThaModuleMapRegisterIterator;

typedef struct tThaModuleMapRegisterIterator
    {
    tAtModuleRegisterIterator super;

    /* Private data */
    uint32 de1Id;
    uint32 timeslotId;
    }tThaModuleMapRegisterIterator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMAPREGISTERITERATORINTERNAL_H_ */


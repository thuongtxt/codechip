/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Demap
 * 
 * File        : ThaModuleDemap.h
 * 
 * Created Date: Oct 27, 2017
 *
 * Description : Module Demap Interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEDEMAP_H_
#define _THAMODULEDEMAP_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleDemap * ThaModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaModuleDemapVcDmapChnCtrl(ThaModuleDemap self, AtSdhVc vc);
eAtRet ThaModuleDemapVc3StuffEnable(ThaModuleDemap self, AtSdhVc channel, eBool enable);
eBool ThaModuleDemapVc3StuffIsEnabled(ThaModuleDemap self, AtSdhVc channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEDEMAP_H_ */


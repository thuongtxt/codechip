/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : ThaModuleAbstractMap.h
 * 
 * Created Date: Mar 21, 2013
 *
 * Description : MAP abstract module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEABSTRACTMAP_H_
#define _THAMODULEABSTRACTMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"
#include "AtPdhNxDs0.h"
#include "AtSdhVc.h"
#include "AtPw.h"
#include "../pw/adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaMapInvalidPwChannelId   cBit31_0
#define cThaMapInvalidEncapHwId     cBit31_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleAbstractMap * ThaModuleAbstractMap;

typedef struct tNxDs0BoundPw
    {
    uint32 nxDs0Mask;
    uint16 pwHwId;
    uint8 cesopMode;
    }tNxDs0BoundPw;

typedef enum eThaMapChannelType
    {
    cThaMapChannelTypeUnknown,
    cThaMapChannelHoLineVc,
    cThaMapChannelLoLineVc,
    cThaMapChannelDe1,
    cThaMapChannelDe3,
    cThaMapChannelNxDs0,
    cThaMapChannelDs1,
    cThaMapChannelE1,
    cThaMapChannelDs3,
    cThaMapChannelE3,
    cThaMapChannelVt6,
    cThaMapChannelVt15,
    cThaMapChannelVt2,
    cThaMapChannelSts
    }eThaMapChannelType;

typedef struct tThaMapTimeslotInfo
    {
    uint8 chnType;
    uint8 firstTimeslot;
    uint8 enabled;
    uint16 pwId;
    }tThaMapTimeslotInfo;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete modules */
AtModule ThaModuleMapNew(AtDevice device);
AtModule ThaModuleDemapNew(AtDevice device);
AtModule ThaModuleStmMapNew(AtDevice device);
AtModule ThaModuleStmDemapNew(AtDevice device);

eAtRet ThaModuleAbstractMapDs0Enable(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId, eBool enable);
eBool ThaModuleAbstractMapDs0IsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId);
eAtRet ThaModuleAbstractMapBindNxDs0ToEncap(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0, AtEncapChannel encapChannel);
eAtRet ThaModuleAbstractMapDs0EncapConnectionEnable(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask, eBool enable);
eBool ThaModuleAbstractMapDs0EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask);
uint32 ThaModuleAbstractMapNxDs0BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0);
eAtRet ThaModuleAbstractMapHwBindNxDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0, uint32 pwId);
eAtRet ThaModuleAbstractMapBindNxDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0, AtPw pw);
uint32 ThaModuleAbstractMapNxDs0BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0);
void ThaModuleAbstractMapTimeslotInfoInit(tThaMapTimeslotInfo *tsInfo);
void ThaModuleAbstractMapTimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo);
void ThaModuleAbstractMapTimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo);
eAtRet ThaModuleAbstractMapBindDs0ToChannel(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtChannel channel, uint8 mapType);
eAtRet ThaModuleAbstractMapBindVc1xToChannel(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtChannel boundChannel, uint8 mapType);
uint32 ThaModuleAbstractMapTimeslotAddressFromBase(ThaModuleAbstractMap self, uint32 address, uint32 slotid);

eAtRet ThaModuleAbstractMapDe1FrmModeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType);
eAtRet ThaModuleAbstractMapDe1Init(ThaModuleAbstractMap self, AtPdhDe1 de1);
eAtRet ThaModuleAbstractMapBindDe1ToEncap(ThaModuleAbstractMap self, AtPdhDe1 de1, AtEncapChannel encapChannel);
uint32 ThaModuleAbstractMapDe1NumDs0s(ThaModuleAbstractMap self, AtPdhDe1 de1);
eAtRet ThaModuleAbstractMapDe1LoopcodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 loopcode);
uint8 ThaModuleAbstractMapDe1LoopcodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1);
eAtRet ThaModuleAbstractMapDe1IdleCodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 idleCode);
uint8 ThaModuleAbstractMapDe1IdleCodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1);
eAtRet ThaModuleAbstractMapDe1IdleEnable(ThaModuleAbstractMap self, AtPdhDe1 de1, eBool enable);
eBool ThaModuleAbstractMapDe1IdleIsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1);
uint32 ThaModuleAbstractMapDe1BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1);
uint32 ThaModuleAbstractMapDe1BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1);
eAtRet ThaModuleAbstractMapBindDe1ToPseudowire(ThaModuleAbstractMap self, AtPdhDe1 de1, AtPw pw);

eAtRet ThaModuleAbstractMapDe3FrameModeSet(ThaModuleAbstractMap self, AtPdhDe3 de3, eAtPdhDe3FrameType frameType);
eAtRet ThaModuleAbstractMapBindDe3ToEncap(ThaModuleAbstractMap self, AtPdhDe3 de3, AtEncapChannel encapChannel);
eAtRet ThaModuleAbstractMapDe3EncapConnectionEnable(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool enable);
eBool ThaModuleAbstractMapDe3EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtPdhDe3 de3);
eAtRet ThaModuleAbstractMapHwBindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, uint32 pwId);
eAtRet ThaModuleAbstractMapBindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, AtPw pw);
uint32 ThaModuleAbstractMapDe3BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3);
uint32 ThaModuleAbstractMapDe3BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3);

eAtRet ThaModuleAbstractMapHwBindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, uint32 pwId);
eAtRet ThaModuleAbstractMapBindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, AtPw pw);
eAtRet ThaModuleAbstractMapHwBindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, uint32 pwId);
eAtRet ThaModuleAbstractMapBindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw);
eAtRet ThaModuleAbstractMapAuVcFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc);
eAtRet ThaModuleAbstractMapVc1xFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc, uint8 frameType);
uint8 ThaModuleAbstractMapPwHwChannelType(ThaModuleAbstractMap self, AtPw pw);

eAtRet ThaModuleAbstractMapBindVc1xToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel);
eAtRet ThaModuleAbstractMapBindHoVcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel);
eAtRet ThaModuleAbstractMapBindTu3VcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel);
eAtRet ThaModuleAbstractMapHoVcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc sdhVc);
uint8  ThaModuleAbstractMapVc1xNumTimeSlots(AtSdhChannel sdhChannel);
eAtRet ThaModuleAbstractMapVcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable);
eBool ThaModuleAbstractMapVcxEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx);
eAtRet ThaModuleAbstractMapVc1xEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vc1x, eBool enable);
eBool ThaModuleAbstractMapVc1xEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vc1x);
uint32 ThaModuleAbstractMapVc1xBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x);
uint32 ThaModuleAbstractMapAuVcBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode);
uint32 ThaModuleAbstractMapVc1xBoundEncapHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x);
uint32 ThaModuleAbstractMapAuVcBoundEncapHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc);
eAtRet ThaModuleAbstractMapTu3VcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc tu3Vc);

eAtRet ThaModuleAbstractMapBindAuVcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup);
eAtRet ThaModuleAbstractMapBindTu3VcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup);
eAtRet ThaModuleAbstractMapBindVc1xToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup);
eAtRet ThaModuleAbstractMapBindDe3ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe3 de3, AtConcateGroup concateGroup);
eAtRet ThaModuleAbstractMapBindDe1ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe1 de1, AtConcateGroup concateGroup);
uint32 ThaModuleAbstractMapVc1xBoundConcateHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x);
uint32 ThaModuleAbstractMapDe3BoundConcateHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3);
uint32 ThaModuleAbstractMapDe1BoundConcateHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1);

/* For registers */
uint32 ThaModuleAbstractMapSdhSliceOffset(ThaModuleAbstractMap self, AtSdhChannel channel, uint8 slice);
uint32 ThaModuleAbstractMapHoSliceOffset(ThaModuleAbstractMap self, uint8 slice);
uint32 ThaModuleAbstractMapChannelCtrlAddressFormula(ThaModuleAbstractMap self, eThaMapChannelType channelType, uint32 stsid, uint32 vtgid, uint32 vtid, uint32 slotid);
uint32 ThaModuleAbstractMapAuVcDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId);
uint32 ThaModuleAbstractMapVcDe3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3);
uint32 ThaModuleAbstractMapVc1xDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc1x);

/* For debugging */
void ThaModuleAbstractMapSdhChannelRegsShow(AtSdhChannel sdhChannel);
void ThaModuleAbstractMapPdhChannelRegsShow(AtPdhChannel pdhChannel);

/* Channelized BERT */
eAtRet ThaModuleAbstractMapDe3Channelize(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool channelized);
eAtRet ThaModuleAbstractMapAuVcUnchannelize(ThaModuleAbstractMap self, AtSdhVc vc, eBool unchannelized);
eAtRet ThaModuleAbstractMapAuVcChannelizeRestore(ThaModuleAbstractMap self, AtSdhVc vc);

/* Product concretes */
AtModule Tha60070041ModuleDemapNew(AtDevice device);
AtModule Tha60070041ModuleMapNew(AtDevice device);
AtModule Tha60091023ModuleMapNew(AtDevice device);
AtModule Tha60150011ModuleDemapNew(AtDevice device);
AtModule Tha60150011ModuleMapNew(AtDevice device);
AtModule Tha60210011ModuleDemapNew(AtDevice device);
AtModule Tha61210011ModuleDemapNew(AtDevice device);
AtModule Tha60210011ModuleMapNew(AtDevice device);
AtModule Tha61210011ModuleMapNew(AtDevice device);
AtModule Tha60210021ModuleMapNew(AtDevice device);
AtModule Tha60210021ModuleDemapNew(AtDevice device);
AtModule Tha60240021ModuleMapNew(AtDevice device);
AtModule Tha60210031ModuleMapNew(AtDevice device);
AtModule Tha60210031ModuleDemapNew(AtDevice device);
AtModule Tha6A000010ModuleMapNew(AtDevice device);
AtModule Tha6A000010ModuleDemapNew(AtDevice device);
AtModule Tha6A033111ModuleMapNew(AtDevice device);
AtModule Tha6A033111ModuleDemapNew(AtDevice device);
AtModule Tha60210012ModuleMapNew(AtDevice device);
AtModule Tha60210012ModuleDemapNew(AtDevice device);
AtModule Tha60210061ModuleMapNew(AtDevice device);
AtModule Tha60210061ModuleDemapNew(AtDevice device);
AtModule Tha60290011ModuleMapNew(AtDevice device);
AtModule Tha60290011ModuleDemapNew(AtDevice device);
AtModule Tha60210051ModuleMapNew(AtDevice device);
AtModule Tha6A290021ModuleMapNew(AtDevice device);
AtModule Tha6A290021ModuleDemapNew(AtDevice device);
AtModule Tha60290022ModuleDemapNew(AtDevice device);
AtModule Tha60290022ModuleMapNew(AtDevice device);
AtModule Tha6A290022ModuleMapNew(AtDevice device);
AtModule Tha6A290022ModuleDemapNew(AtDevice device);
AtModule Tha6A290011ModuleDemapNew(AtDevice device);
AtModule Tha6A290011ModuleMapNew(AtDevice device);
AtModule Tha60291011ModuleMapNew(AtDevice device);
AtModule Tha60291011ModuleDemapNew(AtDevice device);
AtModule Tha60291022ModuleDemapNew(AtDevice device);
AtModule Tha60291022ModuleMapNew(AtDevice device);
AtModule Tha60290081ModuleMapNew(AtDevice device);
AtModule Tha60290081ModuleDemapNew(AtDevice device);
AtModule Tha6A290081ModuleMapNew(AtDevice device);
AtModule Tha6A290081ModuleDemapNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEABSTRACTMAP_H_ */

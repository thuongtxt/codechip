/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ThaModuleStmMapRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : MAP register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"

#include "../../../util/AtIteratorInternal.h"
#include "ThaModuleMapRegisterIteratorInternal.h"
#include "ThaModuleStmMapReg.h"
#include "ThaModuleMap.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumTimeslots 32
#define cMaxNumVtsInVtg  3 /* Use E1 register type to test all registers */
#define cMaxNumStss      12


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eMapRegType
    {
    cMapRegTypeDs0,
    cMapRegTypeVt
    }eMapRegType;

typedef struct tThaModuleStmMapRegisterIterator * ThaModuleStmMapRegisterIterator;

typedef struct tThaModuleStmMapRegisterIterator
    {
    tThaModuleMapRegisterIterator super;

    /* Private data */
    eMapRegType regType;
    uint8 sts;
    uint8 vtg;
    uint8 vt;
    uint8 timeslot;
    }tThaModuleStmMapRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleStmMapRegisterIterator);
    }

static eMapRegType RegType(AtIterator self)
    {
    return ((ThaModuleStmMapRegisterIterator)self)->regType;
    }

static void RegTypeSet(AtIterator self, eMapRegType regType)
    {
    ((ThaModuleStmMapRegisterIterator)self)->regType = regType;
    }

static void NextChannel(AtModuleRegisterIterator self)
    {
    ThaModuleStmMapRegisterIterator iterator = (ThaModuleStmMapRegisterIterator)self;

    if (RegType((AtIterator)self) == cMapRegTypeDs0)
        {
        iterator->timeslot = (uint8)(iterator->timeslot + 1);
        if (iterator->timeslot < cMaxNumTimeslots)
            return;
        iterator->timeslot = 0;
        }

    iterator->vt = (uint8)(iterator->vt + 1);
    if (iterator->vt < cMaxNumVtsInVtg)
        return;
    iterator->vt = 0;

    iterator->vtg = (uint8)(iterator->vtg + 1);
    if (iterator->vtg < cMaxNumVtsInVtg)
        return;
    iterator->vtg = 0;

    iterator->sts = (uint8)(iterator->sts + 1);
    if (iterator->sts < cMaxNumStss)
        return;
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    ThaModuleStmMapRegisterIterator iterator = (ThaModuleStmMapRegisterIterator)self;
    eMapRegType regType;

    NextChannel(self);

    regType = RegType((AtIterator)self);
    if (regType == cMapRegTypeDs0)
        return (672UL * iterator->sts) + (96UL * iterator->vtg) + (32UL * iterator->vt) + iterator->timeslot;
    if (regType == cMapRegTypeVt)
        return (32UL * iterator->sts + (4UL * iterator->vtg) + iterator->vt);

    return 0;
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    ThaModuleStmMapRegisterIterator iterator = (ThaModuleStmMapRegisterIterator)self;
    return (iterator->sts >= cMaxNumStss) ? cAtTrue : cAtFalse;
    }

static void ResetChannel(AtModuleRegisterIterator self)
    {
    ThaModuleStmMapRegisterIterator iterator = (ThaModuleStmMapRegisterIterator)self;

    iterator->sts      = 0;
    iterator->vtg      = 0;
    iterator->vt       = 0;
    iterator->timeslot = 0;
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    mNextReg(iterator, 0, cThaRegDmapChnCtrl);
    RegTypeSet(self, cMapRegTypeDs0);
    mNextChannelRegister(iterator, cThaRegDmapChnCtrl, cThaRegMapChnCtrl);
    mNextChannelRegister(iterator, cThaRegMapChnCtrl, cThaRegMapLineCtrl);
    RegTypeSet(self, cMapRegTypeVt);
    mNextChannelRegister(iterator, cThaRegMapLineCtrl, cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NoChannelLeft);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, ResetChannel);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleStmMapRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator ThaModuleStmMapRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return ThaModuleStmMapRegisterIteratorObjectInit(newIterator, module);
    }

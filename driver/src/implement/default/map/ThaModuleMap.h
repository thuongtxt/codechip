/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP (internal module)
 * 
 * File        : ThaModuleMap.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMAP_H_
#define _THAMODULEMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannel.h"
#include "ThaModuleAbstractMap.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleMap * ThaModuleMap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleMapObjectInit(AtModule module, AtDevice device);

uint32 ThaModuleMapPartOffset(ThaModuleAbstractMap self, uint8 partId);
uint32 ThaModuleDemapPartOffset(ThaModuleAbstractMap self, uint8 partId);
uint32 ThaModuleMapTimingRefCtrlRegister(ThaModuleAbstractMap self);

uint8 ThaModuleMapPwTypeSw2Hw(AtPw pwAdapter);
eAtRet ThaModuleMapDe1GetNxDs0BoundPwList(ThaModuleMap self, AtPdhDe1 de1, AtList listOfNxDs0BBoundPw);

eAtRet ThaModuleMapMapLineControlVcDefaultSet(ThaModuleMap self, AtSdhChannel vc);
eAtRet ThaModuleMapMapLineControlDe3DefaultSet(ThaModuleMap self, AtPdhChannel de3);

/* Registers */
AtIterator ThaModuleStmMapRegisterIteratorCreate(AtModule module);

/* Product concretes */
AtModule Tha60070041ModuleMapNew(AtDevice device);
AtModule Tha60091023ModuleMapNew(AtDevice device);
AtModule Tha60150011ModuleMapNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMAP_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : ThaModuleAbstractMapInternal.h
 * 
 * Created Date: Mar 21, 2013
 *
 * Description : MAP abstract module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEABSTRACTMAPINTERNAL_H_
#define _THAMODULEABSTRACTMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe1.h"
#include "AtPdhDe3.h"

#include "../../../generic/man/AtModuleInternal.h"
#include "../../../generic/common/AtChannelInternal.h"

#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"

#include "ThaModuleAbstractMap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cBindEncapType 4
#define cBindVcgType   7

/*--------------------------- Macros -----------------------------------------*/
#define mFieldMask(module, field)                                              \
        mMethodsGet(module)->field##Mask(module)
#define mFieldShift(module, field)                                             \
        mMethodsGet(module)->field##Shift(module)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleAbstractMapMethods
    {
    /* DS0 */
    eAtRet (*Ds0Enable)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId, eBool enable);
    eBool  (*Ds0IsEnabled)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId);
    eAtRet (*BindDs0ToEncapChannel)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtEncapChannel encapChannel);
    eAtRet (*Ds0EncapConnectionEnable)(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask, eBool enable);
    eBool  (*Ds0EncapConnectionIsEnabled)(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask);
    uint32 (*Ds0DefaultOffset)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId);
    eAtRet (*HwBindDs0ToPseudowire)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, uint32 pwId);
    eAtRet (*BindDs0ToPseudowire)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtPw pw);
    uint32 (*Ds0BoundPwHwIdGet)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask);
    uint32 (*Ds0BoundEncapHwIdGet)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask);
    eAtRet (*TimeslotInfoSet)(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo);
    eAtRet (*TimeslotInfoGet)(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo);

    /* DS1/E1 */
    eAtRet (*De1FrameModeSet)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType);
    eAtRet (*De1LoopcodeSet)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 loopcode);
    uint8 (*De1LoopcodeGet)(ThaModuleAbstractMap self, AtPdhDe1 de1);
    eAtRet (*De1Init)(ThaModuleAbstractMap self, AtPdhDe1 de1);
    uint32 (*De1DefaultOffset)(ThaModuleAbstractMap self, AtPdhDe1 de1);
    uint32 (*De1ChnCtrl)(ThaModuleAbstractMap self, AtPdhDe1 de1);
    eAtRet (*De1IdleCodeSet)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 idleCode);
    uint8 (*De1IdleCodeGet)(ThaModuleAbstractMap self, AtPdhDe1 de1);
    eAtRet (*De1IdleEnable)(ThaModuleAbstractMap self, AtPdhDe1 de1, eBool enable);
    eBool (*De1IdleIsEnabled)(ThaModuleAbstractMap self, AtPdhDe1 de1);

    /* DS3/E3 */
    eAtRet (*BindDe3ToEncap)(ThaModuleAbstractMap self, AtPdhDe3 de3, AtEncapChannel encapChannel);
    eAtRet (*De3FrameModeSet)(ThaModuleAbstractMap self, AtPdhDe3 de3, eAtPdhDe3FrameType frameType);
    uint32 (*De3DefaultOffset)(ThaModuleAbstractMap self, AtPdhDe3 de3);
    eAtRet (*De3EncapConnectionEnable)(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool enable);
    eBool  (*De3EncapConnectionIsEnabled)(ThaModuleAbstractMap self, AtPdhDe3 de3);
    eAtRet (*HwBindDe3ToPseudowire)(ThaModuleAbstractMap self, AtPdhDe3 de3, uint32 pwId);
    eAtRet (*BindDe3ToPseudowire)(ThaModuleAbstractMap self, AtPdhDe3 de3, AtPw pw);
    uint32 (*De3BoundPwHwIdGet)(ThaModuleAbstractMap self, AtPdhDe3 de3);
    uint32 (*De3BoundEncapHwIdGet)(ThaModuleAbstractMap self, AtPdhDe3 de3);

    /* SONET/SDH */
    uint8  (*CoreIdOfSts)(ThaModuleAbstractMap self, uint8 stsId);
    eAtRet (*HwBindHoVcToEncap)(ThaModuleAbstractMap self, AtSdhVc sdhVc, eBool enable, uint32 hwChannelId);
    eAtRet (*BindVc1xToEncap)(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel);
    eAtRet (*BindHoVcToEncap)(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel);
    eAtRet (*BindTu3VcToEncap)(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel);
    eAtRet (*HoVcSignalTypeSet)(ThaModuleAbstractMap self, AtSdhVc sdhVc);
    eAtRet (*Tu3VcSignalTypeSet)(ThaModuleAbstractMap self, AtSdhVc sdhVc);

    /* VC1x */
    eAtRet (*HwBindVc1xToPseudowire)(ThaModuleAbstractMap self, AtSdhVc vc1x, uint32 pwId);
    eAtRet (*BindVc1xToPseudowire)(ThaModuleAbstractMap self, AtSdhVc vc1x, AtPw pw);
    uint32 (*Vc1xDefaultOffset)(ThaModuleAbstractMap self, AtSdhVc vc1x);
    eAtRet (*Vc1xFrameModeSet)(ThaModuleAbstractMap self, AtSdhVc vc, uint8 frameType);
    eAtRet (*Vc1xEncapConnectionEnable)(ThaModuleAbstractMap self, AtSdhVc vc1x, eBool enable);
    eBool  (*Vc1xEncapConnectionIsEnabled)(ThaModuleAbstractMap self, AtSdhVc vc1x);
    uint32 (*Vc1xBoundPwHwIdGet)(ThaModuleAbstractMap self, AtSdhVc vc1x);
    uint32 (*Vc1xBoundEncapHwIdGet)(ThaModuleAbstractMap self, AtSdhVc vc1x);

    /* VC4/VC3 */
    eAtRet (*HwBindAuVcToPseudowire)(ThaModuleAbstractMap self, AtSdhVc sdhVc, uint32 pwId);
    eAtRet (*BindAuVcToPseudowire)(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw);
    uint32 (*AuVcStsDefaultOffset)(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId);
    eAtRet (*AuVcFrameModeSet)(ThaModuleAbstractMap self, AtSdhVc vc);
    eAtRet (*VcxEncapConnectionEnable)(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable);
    eBool  (*VcxEncapConnectionIsEnabled)(ThaModuleAbstractMap self, AtSdhVc vcx);
    uint32 (*AuVcBoundPwHwIdGet)(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode);
    uint32 (*AuVcBoundEncapHwIdGet)(ThaModuleAbstractMap self, AtSdhVc vc);
    uint32 (*VcChnCtrl)(ThaModuleAbstractMap self, AtSdhVc vc);

    /* Concatenation group */
    eAtRet (*BindAuVcToConcateGroup)(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup);
    eAtRet (*BindTu3VcToConcateGroup)(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup);
    eAtRet (*BindVc1xToConcateGroup)(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup);
    eAtRet (*BindDe3ToConcateGroup)(ThaModuleAbstractMap self, AtPdhDe3 de3, AtConcateGroup concateGroup);
    eAtRet (*BindDe1ToConcateGroup)(ThaModuleAbstractMap self, AtPdhDe1 de1, AtConcateGroup concateGroup);
    uint32 (*Vc1xBoundConcateHwIdGet)(ThaModuleAbstractMap self, AtSdhVc vc1x);
    uint32 (*De3BoundConcateHwIdGet)(ThaModuleAbstractMap self, AtPdhDe3 de3);
    uint32 (*De1BoundConcateHwIdGet)(ThaModuleAbstractMap self, AtPdhDe1 de1);
    AtChannel (*ConcateChannelForBinding)(ThaModuleAbstractMap self, AtChannel channel, AtConcateGroup concateGroup);

    /* Encapsulation */
    uint8 (*EncapHwChannelType)(ThaModuleAbstractMap self, AtEncapChannel encapChannel);

    /* PW */
    uint8 (*PwHwChannelType)(ThaModuleAbstractMap self, AtPw pw);
    uint8 (*PwCepHwChannelType)(ThaModuleAbstractMap self);
    uint8 (*PwCesCasHwChannelType)(ThaModuleAbstractMap self);
    uint8 (*PwCesBasicHwChannelType)(ThaModuleAbstractMap self);
    uint8 (*PwSatopHwChannelType)(ThaModuleAbstractMap self);
    uint8 (*PwAtmHwChannelType)(ThaModuleAbstractMap self);
    uint8 (*PwHdlcPppHwChannelType)(ThaModuleAbstractMap self);

    /* Registers */
    uint32 (*SdhSliceOffset)(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice);

    /* For debugging */
    void (*SdhChannelRegsShow)(ThaModuleAbstractMap self, AtSdhChannel sdhChannel);
    void (*PdhChannelRegsShow)(ThaModuleAbstractMap self, AtPdhChannel pdhChannel);
	uint32 (*HaDs0BoundPwHwIdGet)(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask);
    uint32 (*HaDe3BoundPwHwIdGet)(ThaModuleAbstractMap self, AtPdhDe3 de3);
    uint32 (*HaVc1xBoundPwHwIdGet)(ThaModuleAbstractMap self, AtSdhVc vc1x);
    uint32 (*HaAuVcBoundPwHwIdGet)(ThaModuleAbstractMap self, AtSdhVc vc);
    
    /* Slice management. Note, although just TFI-5 products use this, but it
     * would be better to place here to reduce duplicated declaration between MAP
     * and DEMAP modules */
    uint8 (*NumHoSlices)(ThaModuleAbstractMap self);
    uint8 (*NumLoSlices)(ThaModuleAbstractMap self);
    uint32 (*HoSliceOffset)(ThaModuleAbstractMap self, uint8 slice);

    eBool (*NeedEnableAfterBindToPw)(ThaModuleAbstractMap self, AtPw pw);
    eBool (*NeedEnableAfterBindEncapChannel)(ThaModuleAbstractMap self, AtChannel channel);
    uint32 (*ChannelCtrlAddressFormula)(ThaModuleAbstractMap self, eThaMapChannelType channelType, uint32 stsid, uint32 vtgid, uint32 vtid, uint32 slotid);
    uint32 (*TimeslotAddressFromBase)(ThaModuleAbstractMap self, uint32 address, uint32 slotid);
    
    /* For channelized BERT */
    eAtRet (*De3Channelize)(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool channelized);
    eAtRet (*AuVcUnchannelize)(ThaModuleAbstractMap self, AtSdhVc vc, eBool channelized);
    eAtRet (*AuVcChannelizeRestore)(ThaModuleAbstractMap self, AtSdhVc vc);
    }tThaModuleAbstractMapMethods;

typedef struct tThaModuleAbstractMap
    {
    tAtModule super;

    /* Internal methods */
    const tThaModuleAbstractMapMethods *methods;
    /* Async Init */
    uint32 asyncInitState;
    uint32 asyncInitLoSliceState;

    }tThaModuleAbstractMap;

/*--------------------------- Forward declarations ---------------------------*/
extern uint8 PdhDe1Ds0TimeSlotGet(uint32 bitMask, uint8 *timeSlots);

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleAbstractMapObjectInit(AtModule self, eAtModule moduleId, AtDevice device);
void ThaModuleAbstractAsyncInitStateSet(AtModule self, uint32 state);
uint32 ThaModuleAbstractAsyncInitStateGet(AtModule self);
void ThaModuleAbstractAsyncInitLoSliceStateSet(AtModule self, uint32 state);
uint32 ThaModuleAbstractAsyncInitLoSliceStateGet(AtModule self);

uint8 ThaStmMapDemapHoVcHwStsList(ThaModuleAbstractMap self, AtSdhVc sdhVc, uint8 *pStsLst);
uint8 ThaModuleStmMapDemapNumTimeSlotInVc1x(AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEABSTRACTMAPINTERNAL_H_ */

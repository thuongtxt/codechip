/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ThaModuleAbstractMap.c
 *
 * Created Date: Mar 21, 2013
 *
 * Description : MAP abstract module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "ThaModuleAbstractMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThaBitClear(pReg, pos)                                              \
    {\
    *(pReg) = (uint32)(*(pReg) & (~(cBit0 << (pos))));\
    }

/*--------------------------- Macros -----------------------------------------*/
#define cThaMapPwChannelInvalid       0xFFFFFFFFUL

#define mThis(self) ((ThaModuleAbstractMap)self)
/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleAbstractMapMethods m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet De1FrameModeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType)
    {
	AtUnused(frameType);
	AtUnused(de1);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static uint32 Ds0DefaultOffset(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId)
    {
	AtUnused(self);
    return (AtChannelIdGet((AtChannel)de1) * 32) + timeslotId;
    }

static uint32 AuVcStsDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    uint8 hwSts, slice;
    AtUnused(self);
    ThaSdhChannelHwStsGet((AtSdhChannel)vc, cThaModuleMap, stsId, &slice, &hwSts);
    return ThaModuleAbstractMapChannelCtrlAddressFormula(self, cThaMapChannelSts, hwSts, 0, 0, 0);
    }

static uint32 De1DefaultOffset(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
	AtUnused(self);
    return AtChannelIdGet((AtChannel)de1);
    }

static uint32 De3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)de3);
    }

static uint8 SdhVc1xHwStsGet(AtSdhChannel self)
    {
    uint8 hwSlice, hwSts;
    ThaSdhChannel2HwMasterStsId(self, cThaModuleMap, &hwSlice, &hwSts);
    return hwSts;
    }

static uint32 Vc1xDefaultOffset(ThaModuleAbstractMap self, AtSdhVc sdhVc)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
	AtUnused(self);
    return (uint32)((672 * SdhVc1xHwStsGet(sdhChannel))    +
                    (96 * AtSdhChannelTug2Get(sdhChannel)) +
                    (ThaModuleAbstractMapVc1xNumTimeSlots(sdhChannel)* AtSdhChannelTu1xGet(sdhChannel)));
    }

static eAtRet De1Init(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet De1LoopcodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 loopcode)
    {
    AtUnused(de1);
    AtUnused(self);
    AtUnused(loopcode);
    /* Let concrete class do */
    return cAtOk;
    }

static uint8 De1LoopcodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    AtUnused(de1);
    AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet De1IdleCodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 idleCode)
    {
    AtUnused(de1);
    AtUnused(self);
    AtUnused(idleCode);
    /* Let concrete class do */
    return cAtOk;
    }

static uint8 De1IdleCodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return 0;
    }

static eAtRet De1IdleEnable(ThaModuleAbstractMap self, AtPdhDe1 de1, eBool enable)
    {
    AtUnused(self);
    AtUnused(de1);
    AtUnused(enable);
    return cAtOk;
    }

static eBool De1IdleIsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cAtFalse;
    }

static eAtRet Ds0EncapConnectionEnable(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask, eBool enable)
    {
    uint8 timeSlots[32];
    uint8 numberTimeSlot, i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initial value */
    mMethodsGet(osal)->MemInit(osal, timeSlots, 0x0, sizeof(timeSlots));
    numberTimeSlot = PdhDe1Ds0TimeSlotGet(bitMask, timeSlots);

    for (i = 0; i < numberTimeSlot; i++)
        mMethodsGet(self)->Ds0Enable(self, (AtPdhDe1)phyChannel, timeSlots[i], enable);

    return cAtOk;
    }

static eBool Ds0EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask)
    {
    uint8 timeSlots[32];
    uint8 numberTimeSlot, i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initial value */
    mMethodsGet(osal)->MemInit(osal, timeSlots, 0x0, sizeof(timeSlots));
    numberTimeSlot = PdhDe1Ds0TimeSlotGet(bitMask, timeSlots);

    for (i = 0; i < numberTimeSlot; i++)
        {
        if (!mMethodsGet(self)->Ds0IsEnabled(self, (AtPdhDe1)phyChannel, timeSlots[i]))
            return cAtFalse;
        }

    return (numberTimeSlot != 0) ? cAtTrue : cAtFalse;
    }

static eAtRet BindDs0ToEncapChannel(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtEncapChannel encapChannel)
    {
	AtUnused(encapChannel);
	AtUnused(ds0BitMask);
	AtUnused(de1);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet De3FrameModeSet(ThaModuleAbstractMap self, AtPdhDe3 de3, eAtPdhDe3FrameType frameType)
    {
	AtUnused(frameType);
	AtUnused(de3);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static uint8 CoreIdOfSts(ThaModuleAbstractMap self, uint8 stsId)
    {
	AtUnused(stsId);
    return AtModuleDefaultCoreGet((AtModule)self);
    }

static eAtRet BindDe3ToEncap(ThaModuleAbstractMap self, AtPdhDe3 de3, AtEncapChannel encapChannel)
    {
	AtUnused(encapChannel);
	AtUnused(de3);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet HwBindDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, uint32 pwId)
    {
	AtUnused(self);
	AtUnused(de1);
	AtUnused(ds0BitMask);
	AtUnused(pwId);
	return cAtOk;
    }
    
static eAtRet BindDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtPw pw)
    {
    uint8 timeslotId;
    uint32 offset;
    uint32 address;
    uint32 bit_i = cBit0;
    uint8 isFirstSlot = 1;

    if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
        isFirstSlot = 0;

    timeslotId = 0;
    while (ds0BitMask != 0)
        {
        /* Only care 1-bit */
        if (ds0BitMask & bit_i)
            {
            tThaMapTimeslotInfo tsInfo;

            offset = mMethodsGet(self)->Ds0DefaultOffset(self, de1, timeslotId);
            address = mMethodsGet(self)->De1ChnCtrl(self, de1) + offset;

            ThaModuleAbstractMapTimeslotInfoInit(&tsInfo);

            /* Bind */
            if (pw)
                {
                tsInfo.chnType = mMethodsGet(self)->PwHwChannelType(self, pw);
                tsInfo.firstTimeslot = isFirstSlot;
                tsInfo.enabled = (uint8)mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, pw));
                tsInfo.pwId = (uint16)AtChannelHwIdGet((AtChannel)pw);
                }

            ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)de1, timeslotId, address, &tsInfo);

            if (isFirstSlot)
                isFirstSlot = 0;
            }

        /* Next slot */
        ds0BitMask = ds0BitMask & (~bit_i);
        bit_i      = bit_i << 1;
        timeslotId = (uint8)(timeslotId + 1);
        }

    return cAtOk;
    }

static eAtRet De3EncapConnectionEnable(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool enable)
    {
	AtUnused(enable);
	AtUnused(de3);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eBool De3EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet BindVc1xToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
	AtUnused(encapChannel);
	AtUnused(sdhVc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet BindHoVcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
	AtUnused(encapChannel);
	AtUnused(sdhVc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet BindTu3VcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    AtUnused(encapChannel);
    AtUnused(sdhVc);
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet HwBindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, uint32 pwId)
	{
	AtUnused(self);
	AtUnused(vc1x);
	AtUnused(pwId);
	/* Let concrete class do */
	return cAtOk;
	}
static eAtRet BindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, AtPw pw)
    {
    uint32 address, timeslot_i;
    uint16 numTimeslots = ThaModuleStmMapDemapNumTimeSlotInVc1x(vc1x);
    tThaMapTimeslotInfo tsInfo;

    ThaModuleAbstractMapTimeslotInfoInit(&tsInfo);

    address = mMethodsGet(self)->VcChnCtrl(self, vc1x) + mMethodsGet(self)->Vc1xDefaultOffset(self, vc1x);
    if (pw)
        {
        tsInfo.firstTimeslot = 0;
        tsInfo.enabled = (uint8)mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, pw));
        tsInfo.chnType = mMethodsGet(self)->PwHwChannelType(self, pw);
        tsInfo.pwId = (uint16)AtChannelHwIdGet((AtChannel)pw);
        }

    timeslot_i = 0;
    ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)vc1x, timeslot_i, address, &tsInfo);

    /* Set default value = 0 for others timeslot */
    ThaModuleAbstractMapTimeslotInfoInit(&tsInfo);
    for (timeslot_i = 1; timeslot_i < numTimeslots; timeslot_i++)
        {
        uint32 timeslotAddress = ThaModuleAbstractMapTimeslotAddressFromBase(self, address, timeslot_i);
        ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)vc1x, timeslot_i, timeslotAddress, &tsInfo);
        }

    return cAtOk;
    }

static eAtRet HwBindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc sdhVc, uint32 pwId)
	{
	AtUnused(self);
	AtUnused(sdhVc);
	AtUnused(pwId);
	/* Let concrete class do */
	return cAtOk;
	}

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(vc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet AuVcFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
	AtUnused(vc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet Vc1xFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc, uint8 frameType)
    {
	AtUnused(frameType);
	AtUnused(vc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static uint8 CESoPPwHwChannelType(ThaModuleAbstractMap self, AtPw pw)
    {
    if (AtPwCESoPModeGet((AtPwCESoP)pw) == cAtPwCESoPModeWithCas)
        return mMethodsGet(self)->PwCesCasHwChannelType(self);

    return mMethodsGet(self)->PwCesBasicHwChannelType(self);
    }

static uint8 PwHwChannelType(ThaModuleAbstractMap self, AtPw pwAdapter)
    {
    AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
    eAtPwType pwType = AtPwTypeGet(pw);

    if (pwType == cAtPwTypeSAToP)   return mMethodsGet(self)->PwSatopHwChannelType(self);
    if (pwType == cAtPwTypeCESoP)   return CESoPPwHwChannelType(self, pw);
    if (pwType == cAtPwTypeCEP)     return mMethodsGet(self)->PwCepHwChannelType(self);
    if (pwType == cAtPwTypeATM)     return mMethodsGet(self)->PwAtmHwChannelType(self);
    if (pwType == cAtPwTypeHdlc)    return mMethodsGet(self)->PwHdlcPppHwChannelType(self);

    return 0;
    }

static uint8 PwCepHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 6;
    }

static uint8 PwCesCasHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 PwCesBasicHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 PwSatopHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 PwAtmHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 PwHdlcPppHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 4;
    }

static eAtRet HoVcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc sdhVc)
    {
	AtUnused(sdhVc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

/* This function is to get the shift (bit ordinal with 0 being LSB) of a single bit
 in a 32-bit dword (from LSB to MSB)*/
static uint8 Convert32BitToShiftL(uint32 value)
    {
    uint8 shift = 31;

    if ((value & cBit15_0) != 0)
        {
        value = (uint32)(value << 16);
        shift = (uint8)(shift - 16);
        }

    if ((value & cBit23_16) != 0)
        {
        value = (uint32)(value << 8);
        shift = (uint8)(shift - 8);
        }

    if ((value & cBit27_24) != 0)
        {
        value = (uint32)(value << 4);
        shift = (uint8)(shift - 4);
        }

    if ((value & cBit29_28) != 0)
        {
        value = (uint32)(value << 2);
        shift = (uint8)(shift - 2);
        }

    if ((value & cBit30) != 0)
        shift = (uint8)(shift - 1);

    return shift;
    }

static uint32 SdhSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    AtUnused(slice);
    return 0x0;
    }

static ThaModuleAbstractMap ModuleMap(AtChannel channel)
    {
    return (ThaModuleAbstractMap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleMap);
    }

static ThaModuleAbstractMap ModuleDemap(AtChannel channel)
    {
    return (ThaModuleAbstractMap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleDemap);
    }

static void SdhChannelRegsShow(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    /* Let specific product do */
    AtUnused(self);
    AtUnused(sdhChannel);
    }

static void PdhChannelRegsShow(ThaModuleAbstractMap self, AtPdhChannel pdhChannel)
    {
    /* Let specific product do */
    AtUnused(self);
    AtUnused(pdhChannel);
    }

static uint8 NumHoSlices(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 NumLoSlices(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 0;
    }

static eBool NeedEnableAfterBindToPw(ThaModuleAbstractMap self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static eAtRet HwBindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, uint32 pwId)
	{
	AtUnused(self);
	AtUnused(de3);
	AtUnused(pwId);
	return cAtErrorNotImplemented;
	}

static eAtRet BindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, AtPw pw)
    {
    AtUnused(self);
    AtUnused(de3);
    AtUnused(pw);
    return cAtErrorNotImplemented;
    }

static uint32 Ds0BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask)
    {
    AtUnused(ds0BitMask);
    AtUnused(de1);
    AtUnused(self);
    /* Let concrete class do */
    return cInvalidUint32;
    }

static uint32 Ds0BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask)
    {
    return mMethodsGet(self)->Ds0BoundPwHwIdGet(self, de1, ds0BitMask);
    }

static uint32 Vc1xBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    AtUnused(vc1x);
    AtUnused(self);
    /* Let concrete class do */
    return cInvalidUint32;
    }

static uint32 Vc1xBoundEncapHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    return mMethodsGet(self)->Vc1xBoundPwHwIdGet(self, vc1x);
    }

static uint32 AuVcBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode)
    {
    AtUnused(vc);
    AtUnused(self);
    AtUnused(cepMode);

    /* Let concrete class do */
    return cInvalidUint32;
    }

static uint32 AuVcBoundEncapHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    eAtPwCepMode cepMode;
    return mMethodsGet(self)->AuVcBoundPwHwIdGet(self, vc, &cepMode);
    }

static uint32 De3BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return cInvalidUint32;
    }

static uint32 De3BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    return mMethodsGet(self)->De3BoundPwHwIdGet(self, de3);
    }

static uint8 EncapHwChannelType(ThaModuleAbstractMap self, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(encapChannel);
    /* Let subclass determine */
    return 0;
    }

static eAtRet BindAuVcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup)
    {
    AtUnused(concateGroup);
    AtUnused(vc);
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet BindTu3VcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup)
    {
    AtUnused(concateGroup);
    AtUnused(vc);
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet BindVc1xToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup)
    {
    AtUnused(concateGroup);
    AtUnused(vc);
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet BindDe3ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe3 de3, AtConcateGroup concateGroup)
    {
    AtUnused(concateGroup);
    AtUnused(de3);
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet BindDe1ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe1 de1, AtConcateGroup concateGroup)
    {
    AtUnused(concateGroup);
    AtUnused(de1);
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static uint32 Vc1xBoundConcateHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    return mMethodsGet(self)->Vc1xBoundPwHwIdGet(self, vc1x);
    }

static uint32 De3BoundConcateHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    return mMethodsGet(self)->De3BoundPwHwIdGet(self, de3);
    }

static uint32 De1BoundConcateHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    return ThaModuleAbstractMapDe1BoundPwHwIdGet(self, de1);
    }

static AtChannel ConcateChannelForBinding(ThaModuleAbstractMap self, AtChannel channel, AtConcateGroup concateGroup)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(concateGroup);
    return NULL;
    }

static eBool NeedEnableAfterBindEncapChannel(ThaModuleAbstractMap self, AtChannel channel)
    {
    AtUnused(self);
    return (channel) ? cAtTrue : cAtFalse;
    }

static eAtRet Tu3VcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc tu3Vc)
    {
    return mMethodsGet(self)->HoVcSignalTypeSet(self, tu3Vc);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleAbstractMap object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(asyncInitState);
    mEncodeUInt(asyncInitLoSliceState);
    }

static uint32 HoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    return cInvalidUint32;
    }

static uint32 ChannelCtrlAddressFormula(ThaModuleAbstractMap self, eThaMapChannelType channelType, uint32 stsid, uint32 vtgid, uint32 vtid, uint32 slotid)
    {
    switch ((uint32)channelType)
        {
        case cThaMapChannelDs1 : return (672 * stsid) + (96 * vtgid) + (24 * vtid) + slotid;
        case cThaMapChannelVt15: return (672 * stsid) + (96 * vtgid) + (24 * vtid) + slotid;
        case cThaMapChannelE1  : return (672 * stsid) + (96 * vtgid) + (32 * vtid) + slotid;
        case cThaMapChannelVt2 : return (672 * stsid) + (96 * vtgid) + (32 * vtid) + slotid;
        case cThaMapChannelVt6 : return (672 * stsid) + (96 * vtgid);
        case cThaMapChannelDs3 : return (672 * stsid);
        case cThaMapChannelE3  : return (672 * stsid);
        case cThaMapChannelDe3 : return (672 * stsid);
        case cThaMapChannelSts : return (672 * stsid);
        default:
            AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Cannot determine the address formula because of invalid channel type %d\r\n", channelType);
            return cInvalidUint32;
        }
    }

static eThaMapChannelType Vc1xMapChannelType(AtSdhVc vc1x)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)vc1x);

    if (sdhChannelType == cAtSdhChannelTypeVc12)
        return cThaMapChannelVt2;

    if (sdhChannelType == cAtSdhChannelTypeVc11)
        return cThaMapChannelVt15;

    return cThaMapChannelTypeUnknown;
    }

static uint32 TimeslotAddressFromBase(ThaModuleAbstractMap self, uint32 address, uint32 slotid)
    {
    AtUnused(self);
    return address + slotid;
    }

static eAtRet TimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(timeslot);
    AtUnused(address);
    AtUnused(slotInfo);
    return cAtOk;
    }

static eAtRet TimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(timeslot);
    AtUnused(address);
    AtUnused(slotInfo);
    return cAtOk;
    }

static uint32 De1ChnCtrl(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cInvalidUint32;
    }

static eAtRet BindDs0ToChannel(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtChannel channel, uint8 mapType)
    {
    uint8 timeslot_i;
    uint32 offset;
    uint32 address;
    uint8 ds0List[32];
    uint8  numDs0s;
    AtOsal osal = AtSharedDriverOsalGet();
    eBool enable = cAtTrue;
    tThaMapTimeslotInfo tsInfo;

    /* When bind physical to encap channel some product,
     * need disable first and then enable in after step */
    if (mapType == cBindEncapType) /* Bind to encap channel */
        enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, channel);

    mMethodsGet(osal)->MemInit(osal, ds0List, 0x0, sizeof(ds0List));
    numDs0s = PdhDe1Ds0TimeSlotGet(ds0BitMask, ds0List);

    for (timeslot_i = 0; timeslot_i < numDs0s; timeslot_i++)
        {
        offset = mMethodsGet(self)->Ds0DefaultOffset(self, de1, ds0List[timeslot_i]);
        address = mMethodsGet(self)->De1ChnCtrl(self, de1) + offset;
        AtOsalMemInit(&tsInfo, 0, sizeof(tsInfo));

        /* Bind */
        if (channel)
            {
            tsInfo.firstTimeslot = (timeslot_i == 0) ? 1 : 0;
            tsInfo.chnType = mapType;
            tsInfo.pwId = (uint16)AtChannelHwIdGet(channel);
            }

        ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)de1, timeslot_i, address, &tsInfo);
        }

    if (channel == NULL)
        return cAtOk;

    /* Enable all slots */
    for (timeslot_i = 0; timeslot_i < numDs0s; timeslot_i++)
        {
        offset = mMethodsGet(self)->Ds0DefaultOffset(self, de1, ds0List[timeslot_i]);
        address = mMethodsGet(self)->De1ChnCtrl(self, de1) + offset;
        ThaModuleAbstractMapTimeslotInfoGet(self, (AtChannel)de1, timeslot_i, address, &tsInfo);
        tsInfo.enabled = enable ? 1 : 0;
        ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)de1, timeslot_i, address, &tsInfo);
        }

    return cAtOk;
    }

static eAtRet BindVc1xToChannel(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtChannel boundChannel, uint8 mapType)
    {
    uint8 timeslot_i;
    eBool enable;
    uint32 regBase = mMethodsGet(self)->VcChnCtrl(self, sdhVc) + mMethodsGet(self)->Vc1xDefaultOffset(self, sdhVc);

    /* When bind physical to encap channel some product,
     * need disable first and then enable in after step */
    if (mapType == cBindVcgType)
        enable = boundChannel ? cAtTrue : cAtFalse;
    else
        enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, boundChannel);

    for (timeslot_i = 0; timeslot_i < ThaModuleAbstractMapVc1xNumTimeSlots((AtSdhChannel)sdhVc); timeslot_i++)
        {
        uint32 regAddr = ThaModuleAbstractMapTimeslotAddressFromBase(self, regBase, timeslot_i);
        tThaMapTimeslotInfo tsInfo;

        ThaModuleAbstractMapTimeslotInfoInit(&tsInfo);

        if (enable)
            {
            tsInfo.firstTimeslot = (timeslot_i == 0) ? 1 : 0;
            tsInfo.chnType = mapType;
            tsInfo.enabled = enable;
            tsInfo.pwId = (uint16)AtChannelHwIdGet(boundChannel);
            }

        ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)sdhVc, timeslot_i, regAddr, &tsInfo);
        }

    return cAtOk;
    }

static uint32 VcChnCtrl(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    AtUnused(self);
    AtUnused(vc);
    return cInvalidUint32;
    }

static eAtRet Ds0Enable(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId, eBool enable)
    {
    uint32 offset   = mMethodsGet(self)->Ds0DefaultOffset(self, de1, timeslotId);
    uint32 address  = mMethodsGet(self)->De1ChnCtrl(self, de1) + offset;
    tThaMapTimeslotInfo tsInfo;

    ThaModuleAbstractMapTimeslotInfoGet(self, (AtChannel)de1, timeslotId, address, &tsInfo);
    tsInfo.enabled = (uint8)mBoolToBin(enable);
    ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)de1, timeslotId, address, &tsInfo);

    return cAtOk;
    }

static eBool Ds0IsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId)
    {
    uint32 offset   = mMethodsGet(self)->Ds0DefaultOffset(self, de1, timeslotId);
    uint32 address  = mMethodsGet(self)->De1ChnCtrl(self, de1) + offset;
    tThaMapTimeslotInfo tsInfo;
    ThaModuleAbstractMapTimeslotInfoGet(self, (AtChannel)de1, timeslotId, address, &tsInfo);
    return tsInfo.enabled ? cAtTrue : cAtFalse;
    }

static eAtRet Vc1xEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    uint8 timeslot_i;
    uint32 regBase = mMethodsGet(self)->VcChnCtrl(self, vcx) + mMethodsGet(self)->Vc1xDefaultOffset(self, vcx);

    for (timeslot_i = 0; timeslot_i < ThaModuleAbstractMapVc1xNumTimeSlots((AtSdhChannel)vcx); timeslot_i++)
        {
        uint32 regAddr = ThaModuleAbstractMapTimeslotAddressFromBase(self, regBase, timeslot_i);
        tThaMapTimeslotInfo tsInfo;
        ThaModuleAbstractMapTimeslotInfoGet(self, (AtChannel)vcx, timeslot_i, regAddr, &tsInfo);
        tsInfo.enabled = (uint8)mBoolToBin(enable);
        ThaModuleAbstractMapTimeslotInfoSet(self, (AtChannel)vcx, timeslot_i, regAddr, &tsInfo);
        }

    return cAtOk;
    }

static eBool Vc1xEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx)
    {
    uint8 timeslot_i;
    uint32 mapChnCtrl = mMethodsGet(self)->VcChnCtrl(self, vcx);
    uint8 numTimeSlot = ThaModuleAbstractMapVc1xNumTimeSlots((AtSdhChannel) vcx);
    uint32 regBase = mapChnCtrl + mMethodsGet(self)->Vc1xDefaultOffset(self, vcx);

    for (timeslot_i = 0; timeslot_i < ThaModuleAbstractMapVc1xNumTimeSlots((AtSdhChannel)vcx); timeslot_i++)
        {
        uint32 regAddr = ThaModuleAbstractMapTimeslotAddressFromBase(self, regBase, timeslot_i);
        tThaMapTimeslotInfo tsInfo;
        ThaModuleAbstractMapTimeslotInfoGet(self, (AtChannel)vcx, timeslot_i, regAddr, &tsInfo);
        if (tsInfo.enabled == 0)
            return cAtFalse;
        }

    return (numTimeSlot != 0) ? cAtTrue : cAtFalse;
    }

static eAtRet De3Channelize(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool channelized)
    {
    AtUnused(self);
    AtUnused(de3);
    AtUnused(channelized);
    return cAtErrorNotImplemented;
    }

static eAtRet AuVcUnchannelize(ThaModuleAbstractMap self, AtSdhVc vc, eBool unchannelized)
    {
    AtUnused(self);
    AtUnused(vc);
    AtUnused(unchannelized);
    return cAtOk;
    }

static eAtRet AuVcChannelizeRestore(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    AtUnused(self);
    AtUnused(vc);
    return cAtOk;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtModule self)
    {
    ThaModuleAbstractMap module = (ThaModuleAbstractMap)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, De1FrameModeSet);
        mMethodOverride(m_methods, Ds0DefaultOffset);
        mMethodOverride(m_methods, Vc1xDefaultOffset);
        mMethodOverride(m_methods, AuVcStsDefaultOffset);
        mMethodOverride(m_methods, De1DefaultOffset);
        mMethodOverride(m_methods, De3DefaultOffset);
        mMethodOverride(m_methods, De1Init);
        mMethodOverride(m_methods, De1LoopcodeSet);
        mMethodOverride(m_methods, De1LoopcodeGet);
        mMethodOverride(m_methods, De1IdleCodeSet);
        mMethodOverride(m_methods, De1IdleCodeGet);
        mMethodOverride(m_methods, De1IdleEnable);
        mMethodOverride(m_methods, De1IdleIsEnabled);
        mMethodOverride(m_methods, Ds0EncapConnectionEnable);
        mMethodOverride(m_methods, Ds0EncapConnectionIsEnabled);
        mMethodOverride(m_methods, BindDs0ToEncapChannel);
        mMethodOverride(m_methods, De3FrameModeSet);
        mMethodOverride(m_methods, CoreIdOfSts);
        mMethodOverride(m_methods, BindDe3ToEncap);
        mMethodOverride(m_methods, De3EncapConnectionEnable);
        mMethodOverride(m_methods, De3EncapConnectionIsEnabled);
        mMethodOverride(m_methods, BindVc1xToEncap);
        mMethodOverride(m_methods, BindHoVcToEncap);
        mMethodOverride(m_methods, BindTu3VcToEncap);
        mMethodOverride(m_methods, HoVcSignalTypeSet);
        mMethodOverride(m_methods, HwBindDs0ToPseudowire);
        mMethodOverride(m_methods, BindDs0ToPseudowire);
        mMethodOverride(m_methods, HwBindVc1xToPseudowire);
        mMethodOverride(m_methods, BindVc1xToPseudowire);
        mMethodOverride(m_methods, HwBindAuVcToPseudowire);
        mMethodOverride(m_methods, BindAuVcToPseudowire);
        mMethodOverride(m_methods, AuVcFrameModeSet);
        mMethodOverride(m_methods, Vc1xFrameModeSet);
        mMethodOverride(m_methods, PwHwChannelType);
        mMethodOverride(m_methods, PwCepHwChannelType);
        mMethodOverride(m_methods, PwCesBasicHwChannelType);
        mMethodOverride(m_methods, PwCesCasHwChannelType);
        mMethodOverride(m_methods, PwSatopHwChannelType);
        mMethodOverride(m_methods, PwHdlcPppHwChannelType);
        mMethodOverride(m_methods, PwAtmHwChannelType);
        mMethodOverride(m_methods, SdhChannelRegsShow);
        mMethodOverride(m_methods, PdhChannelRegsShow);
        mMethodOverride(m_methods, NumHoSlices);
        mMethodOverride(m_methods, NumLoSlices);
        mMethodOverride(m_methods, NeedEnableAfterBindToPw);
        mMethodOverride(m_methods, SdhSliceOffset);
        mMethodOverride(m_methods, HwBindDe3ToPseudowire);
        mMethodOverride(m_methods, BindDe3ToPseudowire);
        mMethodOverride(m_methods, Ds0BoundPwHwIdGet);
        mMethodOverride(m_methods, De3BoundPwHwIdGet);
        mMethodOverride(m_methods, AuVcBoundPwHwIdGet);
        mMethodOverride(m_methods, Vc1xBoundPwHwIdGet);
        mMethodOverride(m_methods, EncapHwChannelType);
        mMethodOverride(m_methods, BindAuVcToConcateGroup);
        mMethodOverride(m_methods, BindTu3VcToConcateGroup);
        mMethodOverride(m_methods, BindVc1xToConcateGroup);
        mMethodOverride(m_methods, BindDe3ToConcateGroup);
        mMethodOverride(m_methods, BindDe1ToConcateGroup);
        mMethodOverride(m_methods, Vc1xBoundConcateHwIdGet);
        mMethodOverride(m_methods, De3BoundConcateHwIdGet);
        mMethodOverride(m_methods, De1BoundConcateHwIdGet);
        mMethodOverride(m_methods, ConcateChannelForBinding);
        mMethodOverride(m_methods, Ds0BoundEncapHwIdGet);
        mMethodOverride(m_methods, De3BoundEncapHwIdGet);
        mMethodOverride(m_methods, Vc1xBoundEncapHwIdGet);
        mMethodOverride(m_methods, AuVcBoundEncapHwIdGet);
        mMethodOverride(m_methods, NeedEnableAfterBindEncapChannel);
        mMethodOverride(m_methods, Tu3VcSignalTypeSet);
        mMethodOverride(m_methods, HoSliceOffset);
        mMethodOverride(m_methods, ChannelCtrlAddressFormula);
        mMethodOverride(m_methods, TimeslotAddressFromBase);
        mMethodOverride(m_methods, TimeslotInfoGet);
        mMethodOverride(m_methods, TimeslotInfoSet);
        mMethodOverride(m_methods, De1ChnCtrl);
        mMethodOverride(m_methods, VcChnCtrl);
        mMethodOverride(m_methods, Ds0Enable);
        mMethodOverride(m_methods, Ds0IsEnabled);
        mMethodOverride(m_methods, Vc1xEncapConnectionEnable);
        mMethodOverride(m_methods, Vc1xEncapConnectionIsEnabled);
        mMethodOverride(m_methods, De3Channelize);
        mMethodOverride(m_methods, AuVcUnchannelize);
        mMethodOverride(m_methods, AuVcChannelizeRestore);
        }

    mMethodsSet(module, &m_methods);
    }

AtModule ThaModuleAbstractMapObjectInit(AtModule self, eAtModule moduleId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModuleAbstractMap));

    /* Super constructor */
    if (AtModuleObjectInit(self, moduleId, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void ThaModuleAbstractAsyncInitStateSet(AtModule self, uint32 state)
    {
    mThis(self)->asyncInitState = state;
    }

uint32 ThaModuleAbstractAsyncInitStateGet(AtModule self)
    {
    return mThis(self)->asyncInitState;
    }

void ThaModuleAbstractAsyncInitLoSliceStateSet(AtModule self, uint32 state)
    {
    mThis(self)->asyncInitLoSliceState = state;
    }

uint32 ThaModuleAbstractAsyncInitLoSliceStateGet(AtModule self)
    {
    return mThis(self)->asyncInitLoSliceState;
    }

/* TODO: Need to check what function use it and clean its code */
uint8 PdhDe1Ds0TimeSlotGet(uint32 bitMask, uint8 *timeSlots)
    {
    uint8 numberTimeSlot = 0;

    while (bitMask != 0)
        {
        uint8 timeSlot = Convert32BitToShiftL(bitMask);
        mThaBitClear(&bitMask, timeSlot);
        timeSlots[numberTimeSlot] = timeSlot;
        numberTimeSlot = (uint8)(numberTimeSlot + 1);
        }

    /* Although this is not possible, but this is to make source code analysis
     * tool be happy */
    if (numberTimeSlot > 32)
        numberTimeSlot = 32;

    return numberTimeSlot;
    }

/* This function is to set binding physical encapsulation*/
eAtRet ThaModuleAbstractMapBindDe1ToEncap(ThaModuleAbstractMap self, AtPdhDe1 de1, AtEncapChannel encapChannel)
    {
    if (self)
        {
        uint32 ds0BitMask = ThaPdhDe1EncapDs0BitMaskGet((ThaPdhDe1)de1);
        return mMethodsGet(self)->BindDs0ToEncapChannel(self, de1, ds0BitMask, encapChannel);
        }

    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapDs0Enable(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->Ds0Enable(self, de1, timeslotId, enable);
    return cAtError;
    }

eBool ThaModuleAbstractMapDs0IsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId)
    {
    if (self)
        return mMethodsGet(self)->Ds0IsEnabled(self, de1, timeslotId);
    return cAtFalse;
    }

eAtRet ThaModuleAbstractMapBindNxDs0ToEncap(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindDs0ToEncapChannel(self, AtPdhNxDS0De1Get(nxDs0), AtPdhNxDS0BitmapGet(nxDs0), encapChannel);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapDs0EncapConnectionEnable(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->Ds0EncapConnectionEnable(self, phyChannel, bitMask, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModuleAbstractMapDs0EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtChannel phyChannel, uint32 bitMask)
    {
    if (self)
        return mMethodsGet(self)->Ds0EncapConnectionIsEnabled(self, phyChannel, bitMask);
    return cAtFalse;
    }

eAtRet ThaModuleAbstractMapDe1FrmModeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType)
    {
    if (self)
        return mMethodsGet(self)->De1FrameModeSet(self, (AtPdhDe1)de1, frameType);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapDe1LoopcodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 loopcode)
    {
    if (self)
        return mMethodsGet(self)->De1LoopcodeSet(self, de1, loopcode);
    return cAtErrorNullPointer;
    }

uint8 ThaModuleAbstractMapDe1LoopcodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1LoopcodeGet(self, de1);
    return 0;
    }

eAtRet ThaModuleAbstractMapDe1IdleCodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 idleCode)
    {
    if (self)
        return mMethodsGet(self)->De1IdleCodeSet(self, de1, idleCode);
    return cAtErrorNullPointer;
    }

uint8 ThaModuleAbstractMapDe1IdleCodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1IdleCodeGet(self, de1);
    return 0;
    }

eAtRet ThaModuleAbstractMapDe1IdleEnable(ThaModuleAbstractMap self, AtPdhDe1 de1, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->De1IdleEnable(self, de1, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModuleAbstractMapDe1IdleIsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1IdleIsEnabled(self, de1);
    return cAtFalse;
    }

eAtRet ThaModuleAbstractMapDe1Init(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1Init(self, de1);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindDe3ToEncap(ThaModuleAbstractMap self, AtPdhDe3 de3, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindDe3ToEncap(self, de3, encapChannel);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapDe3FrameModeSet(ThaModuleAbstractMap self, AtPdhDe3 de3, eAtPdhDe3FrameType frameType)
    {
    if (self)
        return mMethodsGet(self)->De3FrameModeSet(self, de3, frameType);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapDe3EncapConnectionEnable(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->De3EncapConnectionEnable(self, de3, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModuleAbstractMapDe3EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->De3EncapConnectionIsEnabled(self, de3);
    return cAtFalse;
    }

eAtRet ThaModuleAbstractMapBindVc1xToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindVc1xToEncap(self, sdhVc, encapChannel);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindHoVcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindHoVcToEncap(self, sdhVc, encapChannel);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindTu3VcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindTu3VcToEncap(self, sdhVc, encapChannel);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapTu3VcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc tu3Vc)
    {
    if (self)
        return mMethodsGet(self)->Tu3VcSignalTypeSet(self, tu3Vc);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapHoVcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc sdhVc)
    {
    if (self)
        return mMethodsGet(self)->HoVcSignalTypeSet(self, sdhVc);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapVcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->VcxEncapConnectionEnable(self, vcx, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModuleAbstractMapVcxEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx)
    {
    if (self)
        return mMethodsGet(self)->VcxEncapConnectionIsEnabled(self, vcx);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapVc1xEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vc1x, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->Vc1xEncapConnectionEnable(self, vc1x, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModuleAbstractMapVc1xEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    if (self)
        return mMethodsGet(self)->Vc1xEncapConnectionIsEnabled(self, vc1x);
    return cAtFalse;
    }

uint8 ThaModuleAbstractMapVc1xNumTimeSlots(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet(sdhChannel);
    if (sdhChannelType == cAtSdhChannelTypeVc12)
        return 32;
    if (sdhChannelType == cAtSdhChannelTypeVc11)
        return 24;

    return 0;
    }

uint32 ThaModuleAbstractMapSdhSliceOffset(ThaModuleAbstractMap self, AtSdhChannel channel, uint8 slice)
    {
    if (self)
        return mMethodsGet(self)->SdhSliceOffset(self, channel, slice);
    return 0;
    }

void ThaModuleAbstractMapSdhChannelRegsShow(AtSdhChannel sdhChannel)
    {
    ThaModuleAbstractMap moduleMap   = ModuleMap((AtChannel)sdhChannel);
    ThaModuleAbstractMap moduleDemap = ModuleDemap((AtChannel)sdhChannel);

    ThaModuleSdhStsMappingDisplay(sdhChannel);
    AtPrintc(cSevInfo, "* MAP/DEMAP registers of channel '%s':\r\n", AtObjectToString((AtObject)sdhChannel));
    if (moduleMap)
        mMethodsGet(moduleMap)->SdhChannelRegsShow(moduleMap, sdhChannel);

    if (moduleDemap)
        mMethodsGet(moduleDemap)->SdhChannelRegsShow(moduleDemap, sdhChannel);
    }

void ThaModuleAbstractMapPdhChannelRegsShow(AtPdhChannel pdhChannel)
    {
    ThaModuleAbstractMap moduleMap   = ModuleMap((AtChannel)pdhChannel);
    ThaModuleAbstractMap moduleDemap = ModuleDemap((AtChannel)pdhChannel);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);
    if (sdhChannel)
        ThaModuleSdhStsMappingDisplay(sdhChannel);

    AtPrintc(cSevInfo, "* MAP/DEMAP registers of channel '%s':\r\n", AtObjectToString((AtObject)pdhChannel));
    if (moduleMap)
        mMethodsGet(moduleMap)->PdhChannelRegsShow(moduleMap, pdhChannel);

    if (moduleDemap)
        mMethodsGet(moduleDemap)->PdhChannelRegsShow(moduleDemap, pdhChannel);
    }

eAtRet ThaModuleAbstractMapHwBindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, uint32 pwId)
    {
    if (self)
        return mMethodsGet(self)->HwBindVc1xToPseudowire(self, vc1x, pwId);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindVc1xToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc1x, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->BindVc1xToPseudowire(self, vc1x, pw);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapVc1xBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->Vc1xBoundPwHwIdGet(self, vc);
    return cInvalidUint32;
    }

uint32 ThaModuleAbstractMapVc1xBoundEncapHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->Vc1xBoundEncapHwIdGet(self, vc);
    return cInvalidUint32;
    }

eAtRet ThaModuleAbstractMapHwBindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, uint32 pwId)
    {
    if (self)
        return mMethodsGet(self)->HwBindAuVcToPseudowire(self, vc, pwId);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->BindAuVcToPseudowire(self, vc, pw);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapAuVcBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode)
    {
    if (self)
        return mMethodsGet(self)->AuVcBoundPwHwIdGet(self, vc, cepMode);
    return cInvalidUint32;
    }

uint32 ThaModuleAbstractMapAuVcBoundEncapHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->AuVcBoundEncapHwIdGet(self, vc);
    return cInvalidUint32;
    }

eAtRet ThaModuleAbstractMapAuVcFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->AuVcFrameModeSet(self, vc);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapVc1xFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc, uint8 frameType)
    {
    if (self)
        return mMethodsGet(self)->Vc1xFrameModeSet(self, vc, frameType);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindDe1ToPseudowire(ThaModuleAbstractMap self, AtPdhDe1 de1, AtPw pw)
    {
    if (self)
        {
        uint32 ds0BitMask = ThaPdhDe1SatopDs0BitMaskGet((ThaPdhDe1)de1);

        /* Just allow bind De1 to pseudowire in case of SATOP */
        if (pw && (AtPwTypeGet(pw) != cAtPwTypeSAToP))
            return cAtErrorModeNotSupport;

        return mMethodsGet(self)->BindDs0ToPseudowire(self, de1, ds0BitMask, pw);
        }

    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapDe1BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    if (self)
        {
        uint32 ds0BitMask = ThaPdhDe1SatopDs0BitMaskGet((ThaPdhDe1)de1);
        return mMethodsGet(self)->Ds0BoundPwHwIdGet(self, de1, ds0BitMask);
        }

    return cInvalidUint32;
    }

uint32 ThaModuleAbstractMapDe1BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    if (self)
        {
        uint32 ds0BitMask = ThaPdhDe1SatopDs0BitMaskGet((ThaPdhDe1)de1);
        return mMethodsGet(self)->Ds0BoundEncapHwIdGet(self, de1, ds0BitMask);
        }

    return cInvalidUint32;
    }

eAtRet ThaModuleAbstractMapHwBindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, uint32 pwId)
	{
	if (self)
		return mMethodsGet(self)->HwBindDe3ToPseudowire(self, de3, pwId);
	return cAtErrorNullPointer;
	}

eAtRet ThaModuleAbstractMapBindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, AtPw pw)
    {
    if (self)
        {
        /* Just allow bind De3 to pseudowire in case of SATOP */
        if (pw && (AtPwTypeGet(pw) != cAtPwTypeSAToP))
            return cAtErrorModeNotSupport;

        return mMethodsGet(self)->BindDe3ToPseudowire(self, de3, pw);
        }

    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapDe3BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->De3BoundPwHwIdGet(self, de3);
    return cInvalidUint32;
    }

uint32 ThaModuleAbstractMapDe3BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->De3BoundEncapHwIdGet(self, de3);
    return cInvalidUint32;
    }

eAtRet ThaModuleAbstractMapHwBindNxDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0, uint32 pwId)
    {
    if (self)
        return mMethodsGet(self)->HwBindDs0ToPseudowire(self, AtPdhNxDS0De1Get(nxDs0), AtPdhNxDS0BitmapGet(nxDs0), pwId);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindNxDs0ToPseudowire(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->BindDs0ToPseudowire(self, AtPdhNxDS0De1Get(nxDs0), AtPdhNxDS0BitmapGet(nxDs0), pw);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapNxDs0BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0)
    {
    if (self)
        return mMethodsGet(self)->Ds0BoundPwHwIdGet(self, AtPdhNxDS0De1Get(nxDs0), AtPdhNxDS0BitmapGet(nxDs0));
    return cInvalidUint32;
    }

uint32 ThaModuleAbstractMapNxDs0BoundEncapHwIdGet(ThaModuleAbstractMap self, AtPdhNxDS0 nxDs0)
    {
    if (self)
        return mMethodsGet(self)->Ds0BoundEncapHwIdGet(self, AtPdhNxDS0De1Get(nxDs0), AtPdhNxDS0BitmapGet(nxDs0));
    return cInvalidUint32;
    }

eAtRet ThaModuleAbstractMapBindAuVcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup)
    {
    if (self)
        return mMethodsGet(self)->BindAuVcToConcateGroup(self, vc, concateGroup);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindTu3VcToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup)
    {
    if (self)
        return mMethodsGet(self)->BindTu3VcToConcateGroup(self, vc, concateGroup);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleAbstractMapBindVc1xToConcateGroup(ThaModuleAbstractMap self, AtSdhVc vc, AtConcateGroup concateGroup)
    {
    if (self)
        return mMethodsGet(self)->BindVc1xToConcateGroup(self, vc, concateGroup);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapVc1xBoundConcateHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    if (self)
        return mMethodsGet(self)->Vc1xBoundConcateHwIdGet(self, vc1x);
    return cInvalidUint32;
    }

eAtRet ThaModuleAbstractMapBindDe3ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe3 de3, AtConcateGroup concateGroup)
    {
    if (self)
        return mMethodsGet(self)->BindDe3ToConcateGroup(self, de3, concateGroup);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapDe3BoundConcateHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->De3BoundConcateHwIdGet(self, de3);
    return cInvalidUint32;
    }

eAtRet ThaModuleAbstractMapBindDe1ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe1 de1, AtConcateGroup concateGroup)
    {
    if (self)
        return mMethodsGet(self)->BindDe1ToConcateGroup(self, de1, concateGroup);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleAbstractMapDe1BoundConcateHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1BoundConcateHwIdGet(self, de1);
    return cInvalidUint32;
    }

uint32 ThaModuleAbstractMapHoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    if (self)
        return mMethodsGet(self)->HoSliceOffset(self, slice);
    return cInvalidUint32;
    }

uint8 ThaModuleAbstractMapPwHwChannelType(ThaModuleAbstractMap self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwHwChannelType(self, pw);
    return 0;
    }

uint32 ThaModuleAbstractMapChannelCtrlAddressFormula(ThaModuleAbstractMap self,
                                                     eThaMapChannelType channelType,
                                                     uint32 stsid, uint32 vtgid, uint32 vtid,
                                                     uint32 slotid)
    {
    if (self)
        return mMethodsGet(self)->ChannelCtrlAddressFormula(self, channelType, stsid, vtgid, vtid, slotid);
    return cInvalidUint32;
    }

uint32 ThaModuleAbstractMapAuVcDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 hwSlice, hwSts;
    uint32 sliceOffset;

    AtUnused(self);

    /* Get MAP STS ID */
    ThaSdhChannelHwStsGet(sdhChannel, AtModuleTypeGet((AtModule)self), stsId, &hwSlice, &hwSts);
    sliceOffset = ThaModuleAbstractMapSdhSliceOffset(self, sdhChannel, hwSlice);
    return ThaModuleAbstractMapChannelCtrlAddressFormula(self, cThaMapChannelSts, hwSts, 0, 0, 0) + sliceOffset;
    }

uint32 ThaModuleAbstractMapVcDe3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    uint8 hwSlice, hwSts;
    uint32 sliceOffset;
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3);

    /* Get MAP STS ID */
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, AtModuleTypeGet((AtModule)self), &hwSlice, &hwSts);
    sliceOffset = ThaModuleAbstractMapSdhSliceOffset(self, vc, hwSlice);

    return ThaModuleAbstractMapChannelCtrlAddressFormula(self, cThaMapChannelDe3, hwSts, 0, 0, 0) + sliceOffset;
    }

uint32 ThaModuleAbstractMapVc1xDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)vc1x;
    uint8 hwSlice, hwSts;
    uint32 sliceOffset, channelOffset;
    eThaMapChannelType channelType = Vc1xMapChannelType(vc1x);

    AtUnused(self);
    ThaSdhChannel2HwMasterStsId(sdhChannel, AtModuleTypeGet((AtModule)self), &hwSlice, &hwSts);
    sliceOffset = ThaModuleAbstractMapSdhSliceOffset(self, sdhChannel, hwSlice);
    channelOffset = ThaModuleAbstractMapChannelCtrlAddressFormula(self, channelType,
                                                  hwSts,
                                                  AtSdhChannelTug2Get(sdhChannel),
                                                  AtSdhChannelTu1xGet(sdhChannel),
                                                  0);

    return channelOffset + sliceOffset;
    }

void ThaModuleAbstractMapTimeslotInfoInit(tThaMapTimeslotInfo *tsInfo)
    {
    AtOsalMemInit(tsInfo, 0, sizeof(tThaMapTimeslotInfo));
    }

uint32 ThaModuleAbstractMapTimeslotAddressFromBase(ThaModuleAbstractMap self, uint32 address, uint32 slotid)
    {
    if (self)
        return mMethodsGet(self)->TimeslotAddressFromBase(self, address, slotid);
    return cInvalidUint32;
    }

void ThaModuleAbstractMapTimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    if (self)
        mMethodsGet(self)->TimeslotInfoSet(self, channel, timeslot, address, slotInfo);
    }

void ThaModuleAbstractMapTimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    if (self == NULL)
        return;

    ThaModuleAbstractMapTimeslotInfoInit(slotInfo);
    mMethodsGet(self)->TimeslotInfoGet(self, channel, timeslot, address, slotInfo);
    }

eAtRet ThaModuleAbstractMapBindDs0ToChannel(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtChannel channel, uint8 mapType)
    {
    if (self)
        return BindDs0ToChannel(self, de1, ds0BitMask, channel, mapType);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaModuleAbstractMapBindVc1xToChannel(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtChannel boundChannel, uint8 mapType)
    {
    if (self)
        return BindVc1xToChannel(self, sdhVc, boundChannel, mapType);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaModuleAbstractMapDe3Channelize(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool channelized)
    {
    if (self)
        return mMethodsGet(self)->De3Channelize(self, de3, channelized);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaModuleAbstractMapAuVcUnchannelize(ThaModuleAbstractMap self, AtSdhVc vc, eBool unchannelized)
    {
    if (self)
        return mMethodsGet(self)->AuVcUnchannelize(self, vc, unchannelized);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaModuleAbstractMapAuVcChannelizeRestore(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->AuVcChannelizeRestore(self, vc);
    return cAtErrorNullPointer;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : ThaModuleStmMapInternal.h
 * 
 * Created Date: Jun 24, 2014
 *
 * Description : Module MAP of STM products
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESTMMAPINTERNAL_H_
#define _THAMODULESTMMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleMapInternal.h"
#include "ThaModuleDemapInternal.h"
#include "ThaModuleStmMap.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModuleMapMask(module, field, channel)                                     \
        mMethodsGet((ThaModuleMap)module)->field##Mask((ThaModuleMap)module, channel)
#define mModuleMapShift(module, field, channel)                                    \
        mMethodsGet((ThaModuleMap)module)->field##Shift((ThaModuleMap)module, channel)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleStmMapMethods
    {
    uint32 (*MapLineCtrl)(ThaModuleStmMap self, AtSdhChannel sdhChannel);
    uint32 (*SdhChannelMapLineOffset)(ThaModuleStmMap self, AtSdhChannel sdhChannel, uint8 stsId, uint8 vtgId, uint8 vtId);
    uint32 (*PdhDe2De1MapLineOffset)(ThaModuleStmMap self, AtPdhDe1 de1);
    uint32 (*PdhDe3MapLineOffset)(ThaModuleStmMap self, AtPdhDe3 de3);
    uint32 (*PdhDe3De2De1MapLineOffset)(ThaModuleStmMap self, AtPdhDe3 de3, uint32 de2Id, uint32 de1Id);
    eAtRet (*DefaultSet)(ThaModuleStmMap self);
    void   (*Vc4_4cConfigurationClear)(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 stsId);
    }tThaModuleStmMapMethods;

typedef struct tThaModuleStmMap
    {
    tThaModuleMap super;
    const tThaModuleStmMapMethods* methods;
    }tThaModuleStmMap;

typedef struct tThaModuleStmDemap
    {
    tThaModuleDemap super;
    }tThaModuleStmDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleStmMapObjectInit(AtModule self, AtDevice device);
AtModule ThaModuleStmDemapObjectInit(AtModule self, AtDevice device);

AtIterator ThaModuleStmMapRegisterIteratorCreate(AtModule module);

#endif /* _THAMODULESTMMAPINTERNAL_H_ */


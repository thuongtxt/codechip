/*-----------------------------------------------------------------------------
 *

 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : ThaModuleStmMap.h
 * 
 * Created Date: Nov 16, 2012
 *
 * Description : STM MAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESTMMAP_H_
#define _THAMODULESTMMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleMap.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cMapFrameTypeCepFractionalVc3 2
#define cMapFrameTypeCepFractionalVc1x 1

#define cMapFrameTypeCepBasic  3
#define cMapFrameTypeVtWithPoh 3

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleStmMap * ThaModuleStmMap;
typedef struct tThaModuleStmDemap * ThaModuleStmDemap;

typedef enum eThaDatMapPldMd
    {
    cThaCfgMapPldVt15,        /* VT1.5 */
    cThaCfgMapPldVt2,         /* VT2 */
    cThaCfgMapPldDs1,         /* DS1 */
    cThaCfgMapPldE1           /* E1 */
    } eThaDatMapPldMd;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaMapVtgPldMdSet(ThaModuleMap self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, eThaDatMapPldMd pldMd);
eAtRet ThaMapVtPldMdSet(ThaModuleMap self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eThaDatMapPldMd pldMd);
eThaDatMapPldMd ThaMapVtPldMdGet(ThaModuleMap self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);

void ThaModuleStmMapVc4_4cConfigurationClear(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 stsId);

uint32 ThaModuleStmMapMapLineCtrl(ThaModuleStmMap self, AtSdhChannel channel);
uint32 ThaModuleMapMapTimeSrcMastMask(ThaModuleMap self, AtSdhChannel sdhChannel);
uint32 ThaModuleMapMapTimeSrcMastShift(ThaModuleMap self, AtSdhChannel sdhChannel);
uint32 ThaModuleMapMapTimeSrcIdMask(ThaModuleMap self, AtSdhChannel sdhChannel);
uint32 ThaModuleMapMapTimeSrcIdShift(ThaModuleMap self, AtSdhChannel sdhChannel);
uint32 ThaModuleStmMapSdhChannelMapLineOffset(ThaModuleStmMap self, AtSdhChannel sdhChannel, uint8 stsId, uint8 vtgId, uint8 vtId);
uint32 ThaModuleStmMapPdhDe1MapLineOffset(ThaModuleStmMap self, AtPdhDe1 de1);
uint32 ThaModuleStmMapPdhDe3MapLineOffset(ThaModuleStmMap self, AtPdhDe3 de3);
uint32 ThaModuleStmMapPdhDe2De1MapLineOffset(ThaModuleStmMap self, AtPdhDe1 de1);
uint32 ThaModuleStmMapPdhDe3De2De1MapLineOffset(ThaModuleStmMap self, AtPdhDe3 de3, uint32 de2Id, uint32 de1Id);
uint32 ThaStmMapDemapDs0DefaultOffset(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId);

/* Default implementation */
eAtRet ThaModuleStmMapBindAuVcToPseudowire(ThaModuleStmMap self, AtSdhVc vc, AtPw pw);
eAtRet ThaModuleStmMapAuVcFrameModeSet(ThaModuleStmMap self, AtSdhVc vc);
eAtRet ThaModuleStmMapVcxEncapConnectionEnable(ThaModuleStmMap self, AtSdhVc vcx, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESTMMAP_H_ */


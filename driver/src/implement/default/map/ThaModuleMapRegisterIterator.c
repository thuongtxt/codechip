/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ThaModuleMapRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : MAP register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "AtModulePdh.h"

#include "../../../util/AtIteratorInternal.h"
#include "ThaModuleMapReg.h"
#include "ThaModuleMapRegisterIteratorInternal.h"
#include "ThaModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumDs0InDe1 32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleMapRegisterIterator);
    }

static uint32 MaxNumDe1s(AtModuleRegisterIterator self)
    {
    AtModule thisModule = AtModuleRegisterIteratorModuleGet(self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtModuleDeviceGet(thisModule), cAtModulePdh);

    return AtModulePdhNumberOfDe1sGet(pdhModule);
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    ThaModuleMapRegisterIterator mapRegIterator = (ThaModuleMapRegisterIterator)self;

    if (mapRegIterator->de1Id >= MaxNumDe1s(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    ThaModuleMapRegisterIterator iterator = (ThaModuleMapRegisterIterator)self;

    iterator->timeslotId = iterator->timeslotId + 1;

    /* Next DS1/E1 */
    if (iterator->timeslotId == cMaxNumDs0InDe1)
        {
        iterator->timeslotId = 0;
        iterator->de1Id      = iterator->de1Id + 1;
        }

    /* And its offset */
    return (iterator->de1Id * 32) + iterator->timeslotId;
    }

static void ResetChannel(AtModuleRegisterIterator self)
    {
    ThaModuleMapRegisterIterator iterator = (ThaModuleMapRegisterIterator)self;

    iterator->de1Id      = 0;
    iterator->timeslotId = 0;
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    mNextReg(iterator, 0, cThaRegDmapChnCtrl);
    mNextChannelRegister(iterator, cThaRegDmapChnCtrl, cThaRegMapChnCtrl);
    mNextChannelRegister(iterator, cThaRegMapChnCtrl , cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NoChannelLeft);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, ResetChannel);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleMapRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;
	
	/* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator ThaModuleMapRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());
    return ThaModuleMapRegisterIteratorObjectInit(newIterator, module);
    }

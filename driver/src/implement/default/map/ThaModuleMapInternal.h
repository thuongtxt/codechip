/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP internal module
 * 
 * File        : ThaModuleMapInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description : MAP sub module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMAPINTERNAL_H_
#define _THAMODULEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleAbstractMapInternal.h"
#include "ThaModuleMap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleMapMethods
    {
    eAtRet (*De1GetNxDs0BoundPw)(ThaModuleMap self, AtPdhDe1 de1, AtList listOfNxDs0BBoundPw);

    /* Register polymorphism */
    uint32 (*De1MapChnCtrl)(ThaModuleMap self, AtPdhDe1 de1);
    uint32 (*VcMapChnCtrl)(ThaModuleMap self, AtSdhVc vc);
    uint32 (*De3MapChnCtrl)(ThaModuleMap self, AtPdhDe3 de3);
    uint32 (*MapTimingRefCtrl)(ThaModuleMap self);

    /* Bit fields polymorphism for "Thalassa Map Channel Control"*/
    uint32 (*MapChnTypeMask)(ThaModuleMap self);
    uint8  (*MapChnTypeShift)(ThaModuleMap self);
    uint32 (*MapFirstTsMask)(ThaModuleMap self);
    uint8  (*MapFirstTsShift)(ThaModuleMap self);
    uint32 (*MapTsEnMask)(ThaModuleMap self);
    uint8  (*MapTsEnShift)(ThaModuleMap self);
    uint32 (*MapPWIdFieldMask)(ThaModuleMap self);
    uint8  (*MapPWIdFieldShift)(ThaModuleMap self);

    /* Bit fields polymorphism for "Thalassa Map Timing Reference Control"*/
    mDefineMaskShiftField(ThaModuleMap, MapTmDs1E1IdleCode)
    mDefineMaskShiftField(ThaModuleMap, MapTmDs1E1IdleEnable)
    mDefineMaskShiftField(ThaModuleMap, MapTmDs1Loopcode)
    mDefineMaskShiftField(ThaModuleMap, MapTmDs1E1FrmMd)
    mDefineMaskShiftField(ThaModuleMap, MapTmDs1E1Md)
    mDefineMaskShiftField(ThaModuleMap, MapTmSrcId)

    uint32 (*MapFrmTypeMask)(ThaModuleMap self, AtSdhChannel sdhChannel);
    uint8  (*MapFrmTypeShift)(ThaModuleMap self, AtSdhChannel sdhChannel);

    uint32 (*MapSigTypeMask)(ThaModuleMap self, AtSdhChannel sdhChannel);
    uint8  (*MapSigTypeShift)(ThaModuleMap self, AtSdhChannel sdhChannel);

    uint32 (*MapTimeSrcMastMask)(ThaModuleMap self, AtSdhChannel sdhChannel);
    uint8  (*MapTimeSrcMastShift)(ThaModuleMap self, AtSdhChannel sdhChannel);

    uint32 (*MapTimeSrcIdMask)(ThaModuleMap self, AtSdhChannel sdhChannel);
    uint8  (*MapTimeSrcIdShift)(ThaModuleMap self, AtSdhChannel sdhChannel);

    eAtRet (*MapLineControlVcDefaultSet)(ThaModuleMap self, AtSdhChannel vc);
    eAtRet (*MapLineControlDe3DefaultSet)(ThaModuleMap self, AtPdhChannel vc);
    eAtRet (*HoBertHwDefault)(ThaModuleMap self, AtSdhChannel channel);
    }tThaModuleMapMethods;

typedef struct tThaModuleMap
    {
    tThaModuleAbstractMap super;
    const tThaModuleMapMethods *methods;

    /* Private data */
    }tThaModuleMap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIterator ThaModuleMapRegisterIteratorCreate(AtModule module);
void ThaModuleMapHoBertHwDefault(ThaModuleMap self, AtSdhChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMAPINTERNAL_H_ */


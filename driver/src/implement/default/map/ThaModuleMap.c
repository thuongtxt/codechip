/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP (internal)
 *
 * File        : ThaModuleMap.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : MAP internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCESoP.h"
#include "ThaModuleMapInternal.h"
#include "ThaModuleMapReg.h"
#include "../pw/adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaModuleMap)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleMapMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* Save super implementations */
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapChnType)
mDefineMaskShift(ThaModuleMap, MapFirstTs)
mDefineMaskShift(ThaModuleMap, MapTsEn)
mDefineMaskShift(ThaModuleMap, MapPWIdField)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1IdleCode)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1IdleEnable)
mDefineMaskShift(ThaModuleMap, MapTmDs1Loopcode)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1FrmMd)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1Md)
mDefineMaskShift(ThaModuleMap, MapTmSrcId)
mDefineRegAdress(ThaModuleMap, MapTimingRefCtrl)

static uint32 De1MapChnCtrl(ThaModuleMap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cThaRegMapChnCtrl;
    }

static uint32 VcMapChnCtrl(ThaModuleMap self, AtSdhVc vc)
    {
    AtUnused(vc);
    return mMethodsGet(self)->De1MapChnCtrl(self, NULL);
    }

static uint32 De3MapChnCtrl(ThaModuleMap self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    return mMethodsGet(self)->De1MapChnCtrl(self, NULL);
    }

/* This function is to set frame mode in Map module, it just supports for function
 * FrameTypeSet in ThaPdhDe1.c file */
static eAtRet De1FrameModeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType)
    {
    uint8 de1Md = 0;
    uint8 ds1FrmMd = 0;
    uint32 address;
    uint32 regVal, mask, shift;
    ThaModuleMap mapModule = (ThaModuleMap)self;

    /* Convert software value to hardware value */
    switch (frameType)
        {
        case cAtPdhDs1J1UnFrm:
            ds1FrmMd = 0;
            de1Md = 1;
            break;
        case  cAtPdhDs1FrmSf:
        case cAtPdhDs1FrmEsf:
        case cAtPdhDs1FrmDDS:
        case cAtPdhDs1FrmSLC:
        case cAtPdhJ1FrmSf:
        case  cAtPdhJ1FrmEsf:
            ds1FrmMd = 1; /* Set when E1/DS1/J1 frame mode */
            de1Md = 1;
            break;
        case cAtPdhE1UnFrm:
            ds1FrmMd = 0;
            de1Md = 0;
            break;
        case cAtPdhE1Frm:
        case cAtPdhE1MFCrc:
            ds1FrmMd = 1;
            de1Md = 0;
            break;
        default:
            break;
        }

    address = ThaModuleMapTimingRefCtrlRegister(self) + mMethodsGet(self)->De1DefaultOffset(self, de1);
    regVal = mChannelHwRead(de1, address, cThaModuleMap);
    mask  = mFieldMask(mapModule, MapTmDs1E1FrmMd);
    shift = mFieldShift(mapModule, MapTmDs1E1FrmMd);
    mFieldIns(&regVal, mask, shift, ds1FrmMd);
    mask  = mFieldMask(mapModule, MapTmDs1E1Md);
    shift = mFieldShift(mapModule, MapTmDs1E1Md);
    mFieldIns(&regVal, mask, shift, de1Md);
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);

    return cAtOk;
    }

static eAtRet De1Init(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    uint8 i;
    static const uint8 numberOfTimeSlots = 32;
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 offset;

    for (i = 0; i < numberOfTimeSlots; i++)
        {
        offset = mMethodsGet(self)->Ds0DefaultOffset(self, (AtPdhDe1)de1, i);
        mChannelHwWrite(de1, mMethodsGet(mapModule)->De1MapChnCtrl(mapModule, de1) + offset, 0x0, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet De1LoopcodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 loopcode)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = ThaModuleMapTimingRefCtrlRegister(self) + mMethodsGet(self)->De1DefaultOffset(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1Loopcode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1Loopcode);
    mFieldIns(&regVal, mask, shift, loopcode);
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);
    return cAtOk;
    }

static uint8 De1LoopcodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = ThaModuleMapTimingRefCtrlRegister(self) + mMethodsGet(self)->De1DefaultOffset(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1Loopcode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1Loopcode);
    uint8 loopcode = 0;
    mFieldGet(regVal, mask, shift, uint8, &loopcode);
    return loopcode;
    }

static eAtRet De1IdleCodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 idleCode)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = ThaModuleMapTimingRefCtrlRegister(self) + mMethodsGet(self)->De1DefaultOffset(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleCode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleCode);
    mFieldIns(&regVal, mask, shift, idleCode);
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);
    return cAtOk;
    }

static uint8 De1IdleCodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = ThaModuleMapTimingRefCtrlRegister(self) + mMethodsGet(self)->De1DefaultOffset(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleCode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleCode);
    uint8 idleCode = 0;
    mFieldGet(regVal, mask, shift, uint8, &idleCode);
    return idleCode;
    }

static eAtRet De1IdleEnable(ThaModuleAbstractMap self, AtPdhDe1 de1, eBool enable)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = ThaModuleMapTimingRefCtrlRegister(self) + mMethodsGet(self)->De1DefaultOffset(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleEnable);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleEnable);
    mFieldIns(&regVal, mask, shift, (enable) ? 1 : 0);
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);
    return cAtOk;
    }

static eBool De1IdleIsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = ThaModuleMapTimingRefCtrlRegister(self) + mMethodsGet(self)->De1DefaultOffset(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleEnable);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleEnable);
    uint8 idleEnable = 0;
    mFieldGet(regVal, mask, shift, uint8, &idleEnable);
    return (idleEnable) ? cAtTrue : cAtFalse;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x1A0400) &&
        (localAddress <= 0x1A080F))
        return cAtTrue;

    return cAtFalse;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleMapRegisterIteratorCreate(self);
    }

static eAtRet BindDs0ToEncapChannel(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask, AtEncapChannel encapChannel)
    {
    return ThaModuleAbstractMapBindDs0ToChannel(self, de1, ds0BitMask, (AtChannel)encapChannel, cBindEncapType);
    }

static eAtRet BindDe1ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe1 de1, AtConcateGroup concateGroup)
    {
    const uint32 ds1Ds0BitMask = cBit23_0;
    const uint32 e1Ds0BitMask = cBit31_1;   /* E1 PCM-31CRC*/
    AtChannel concateChannel = mMethodsGet(self)->ConcateChannelForBinding(self, (AtChannel)de1, concateGroup);
    return ThaModuleAbstractMapBindDs0ToChannel(self, de1, AtPdhDe1IsE1(de1) ? e1Ds0BitMask : ds1Ds0BitMask, concateChannel, cBindVcgType);
    }

static AtChannel ConcateChannelForBinding(ThaModuleAbstractMap self, AtChannel channel, AtConcateGroup concateGroup)
    {
    AtUnused(self);
    AtUnused(channel);
    return (AtChannel)concateGroup;
    }

static uint8 PwAtmHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 3;
    }

static uint8 PwCesCasHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 2;
    }

static eAtRet TimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 regVal = 0;
    uint32 mask, shift;

    AtUnused(timeslot);

    mask  = mFieldMask(mapModule, MapFirstTs);
    shift = mFieldShift(mapModule, MapFirstTs);
    mFieldIns(&regVal, mask, shift, slotInfo->firstTimeslot);

    mask  = mFieldMask(mapModule, MapTsEn);
    shift = mFieldShift(mapModule, MapTsEn);
    mFieldIns(&regVal, mask, shift, mBoolToBin(slotInfo->enabled));

    mask  = mFieldMask(mapModule, MapChnType);
    shift = mFieldShift(mapModule, MapChnType);
    mFieldIns(&regVal, mask, shift, slotInfo->chnType);

    mask  = mFieldMask(mapModule, MapPWIdField);
    shift = mFieldShift(mapModule, MapPWIdField);
    mFieldIns(&regVal, mask, shift, slotInfo->pwId);

    mChannelHwWrite(channel, address, regVal, cThaModuleMap);

    return cAtOk;
    }

static eAtRet TimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 regVal = mChannelHwRead(channel, address, cThaModuleMap);
    uint32 fieldMask, fieldShift;

    AtUnused(timeslot);

    fieldMask  = mFieldMask(mapModule, MapFirstTs);
    fieldShift = mFieldShift(mapModule, MapFirstTs);
    slotInfo->firstTimeslot = (uint8)mRegField(regVal, field);

    fieldMask  = mFieldMask(mapModule, MapTsEn);
    fieldShift = mFieldShift(mapModule, MapTsEn);
    slotInfo->enabled = (uint8)mRegField(regVal, field);

    fieldMask  = mFieldMask(mapModule, MapChnType);
    fieldShift = mFieldShift(mapModule, MapChnType);
    slotInfo->chnType = (uint8)mRegField(regVal, field);

    fieldMask  = mFieldMask(mapModule, MapPWIdField);
    fieldShift = mFieldShift(mapModule, MapPWIdField);
    slotInfo->pwId = (uint16)mRegField(regVal, field);

    return cAtOk;
    }

static eAtRet De1GetNxDs0BoundPw(ThaModuleMap self, AtPdhDe1 de1, AtList NxDs0BindPwGroups)
    {
    uint8 numDs0InDe1 = (AtPdhDe1IsE1(de1)) ? 32 : 24;
    uint8 ds0_i, cesDs0_i;
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    tNxDs0BoundPw *nxDs0Group;
    uint32 offset, address;
    eAtPwCESoPMode cesMode;

    for (ds0_i = 0; ds0_i < numDs0InDe1; ds0_i++)
        {
        tThaMapTimeslotInfo tsInfo;

        offset = mMethodsGet(abstractMap)->Ds0DefaultOffset(abstractMap, de1, ds0_i);
        address = mMethodsGet(self)->De1MapChnCtrl(self, de1) + offset;

        ThaModuleAbstractMapTimeslotInfoGet(abstractMap, (AtChannel)de1, ds0_i, address, &tsInfo);
        if (!tsInfo.firstTimeslot)
            continue;

        if (tsInfo.chnType == mMethodsGet(abstractMap)->PwCesBasicHwChannelType(abstractMap))
            cesMode = cAtPwCESoPModeBasic;

        else if (tsInfo.chnType == mMethodsGet(abstractMap)->PwCesCasHwChannelType(abstractMap))
            cesMode = cAtPwCESoPModeWithCas;

        else
            continue;

        /* Found out a PW cesop */
        nxDs0Group = AtOsalMemAlloc(sizeof(tNxDs0BoundPw));
        if (nxDs0Group == NULL)
            return cAtError;

        AtOsalMemInit(nxDs0Group, 0, sizeof(tNxDs0BoundPw));

        nxDs0Group->pwHwId = tsInfo.pwId;

        nxDs0Group->cesopMode = cesMode;
        nxDs0Group->nxDs0Mask |= (cBit0 << ds0_i);

        /* Search all ds0 belong to this group */
        for (cesDs0_i = ds0_i; cesDs0_i < numDs0InDe1; cesDs0_i++)
            {
            uint16 pwHwId;

            offset  = mMethodsGet(abstractMap)->Ds0DefaultOffset(abstractMap, de1, cesDs0_i);
            address = mMethodsGet(self)->De1MapChnCtrl(self, de1) + offset;

            ThaModuleAbstractMapTimeslotInfoGet(abstractMap, (AtChannel)de1, cesDs0_i, address, &tsInfo);
            pwHwId = tsInfo.pwId;
            if (pwHwId == nxDs0Group->pwHwId)
                nxDs0Group->nxDs0Mask |= (cBit0 << cesDs0_i);
            }

        AtListObjectAdd(NxDs0BindPwGroups, (AtObject)nxDs0Group);
        }

    return cAtOk;
    }

static eAtRet Ds0RegDisplay(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask)
    {
    uint8 timeslotId;
    uint32 offset;
    uint32 address;
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 bit_i = cBit0;

    timeslotId = 0;
    while (ds0BitMask != 0)
        {
        /* Only care 1-bit */
        if (ds0BitMask & bit_i)
            {
            offset = mMethodsGet(self)->Ds0DefaultOffset(self, de1, timeslotId);
            address = mMethodsGet(mapModule)->De1MapChnCtrl(mapModule, de1) + offset;
            ThaDeviceChannelRegValueDisplay((AtChannel)de1, address, cThaModuleMap);
            }

        /* Next slot */
        ds0BitMask = ds0BitMask & (~bit_i);
        bit_i      = bit_i << 1;
        timeslotId = (uint8)(timeslotId + 1);
        }

    return cAtOk;
    }

static void De1RegShow(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    uint32 ds0BitMask = ThaPdhDe1SatopDs0BitMaskGet((ThaPdhDe1)de1);

    ThaDeviceRegNameDisplay("Map Channel Control");
    Ds0RegDisplay(self, de1, ds0BitMask);
    }

static void PdhChannelRegsShow(ThaModuleAbstractMap self, AtPdhChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);
    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        De1RegShow(self, (AtPdhDe1)pdhChannel);
    }

static eAtRet MapLineControlVcDefaultSet(ThaModuleMap self, AtSdhChannel vc)
    {
    AtUnused(self);
    AtUnused(vc);
    return cAtOk;
    }

static eAtRet MapLineControlDe3DefaultSet(ThaModuleMap self, AtPdhChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return cAtOk;
    }

static eAtRet HoBertHwDefault(ThaModuleMap self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtOk;
    }

static uint32 Ds0BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint32 ds0BitMask)
    {
    uint8 timeslotId = 0;
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 bit_i = cBit0;
    uint8 isFirstSlot = 1;
    uint32 pwHwId = cInvalidUint32;

    while (ds0BitMask != 0)
        {
        /* Only care 1-bit */
        if (ds0BitMask & bit_i)
            {
            tThaMapTimeslotInfo tsInfo;
            uint32 offset = mMethodsGet(self)->Ds0DefaultOffset(self, de1, timeslotId);
            uint32 address = mMethodsGet(mapModule)->De1MapChnCtrl(mapModule, de1) + offset;
            ThaModuleAbstractMapTimeslotInfoGet(self, (AtChannel)de1, timeslotId, address, &tsInfo);

            if (isFirstSlot)
                {
                pwHwId = tsInfo.pwId;
                isFirstSlot = 0;
                }

            else if (pwHwId != tsInfo.pwId)
                return cInvalidUint32;
            }

        /* Next slot */
        ds0BitMask = ds0BitMask & (~bit_i);
        bit_i      = bit_i << 1;
        timeslotId = (uint8)(timeslotId + 1);
        }

    return pwHwId;
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "map";
    }

static uint32 De1ChnCtrl(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    return mMethodsGet(mThis(self))->De1MapChnCtrl(mThis(self), de1);
    }

static uint32 VcChnCtrl(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    return mMethodsGet(mThis(self))->VcMapChnCtrl(mThis(self), vc);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, De1FrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1Init);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1LoopcodeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1LoopcodeGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleCodeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleCodeGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDs0ToEncapChannel);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PwAtmHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PwCesCasHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PdhChannelRegsShow);
        mMethodOverride(m_ThaModuleAbstractMapOverride, Ds0BoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe1ToConcateGroup);
        mMethodOverride(m_ThaModuleAbstractMapOverride, ConcateChannelForBinding);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1ChnCtrl);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcChnCtrl);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, TypeString);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    }

static void MethodsInit(AtModule self)
    {
    ThaModuleMap module = (ThaModuleMap)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mBitFieldOverride(ThaModuleMap, m_methods, MapChnType)
        mBitFieldOverride(ThaModuleMap, m_methods, MapFirstTs)
        mBitFieldOverride(ThaModuleMap, m_methods, MapTsEn)
        mBitFieldOverride(ThaModuleMap, m_methods, MapPWIdField)

        mBitFieldOverride(ThaModuleMap, m_methods, MapTmDs1E1IdleCode)
        mBitFieldOverride(ThaModuleMap, m_methods, MapTmDs1E1IdleEnable)
        mBitFieldOverride(ThaModuleMap, m_methods, MapTmDs1Loopcode)
        mBitFieldOverride(ThaModuleMap, m_methods, MapTmDs1E1FrmMd)
        mBitFieldOverride(ThaModuleMap, m_methods, MapTmDs1E1Md)
        mBitFieldOverride(ThaModuleMap, m_methods, MapTmSrcId)

        /* Override register address */
        mMethodOverride(m_methods, De3MapChnCtrl);
        mMethodOverride(m_methods, De1MapChnCtrl);
        mMethodOverride(m_methods, VcMapChnCtrl);
        mMethodOverride(m_methods, De1GetNxDs0BoundPw);
        mMethodOverride(m_methods, MapTimingRefCtrl);
        mMethodOverride(m_methods, MapLineControlVcDefaultSet);
        mMethodOverride(m_methods, MapLineControlDe3DefaultSet);
        mMethodOverride(m_methods, HoBertHwDefault);
        }

    mMethodsSet(module, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleMap);
    }

AtModule ThaModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleAbstractMapObjectInit(self, cThaModuleMap, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleMapObjectInit(newModule, device);
    }

uint32 ThaModuleMapPartOffset(ThaModuleAbstractMap self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleMap, partId);
    }

eAtRet ThaModuleMapDe1GetNxDs0BoundPwList(ThaModuleMap self, AtPdhDe1 de1, AtList listOfNxDs0BoundPw)
    {
    if (self)
        return mMethodsGet(self)->De1GetNxDs0BoundPw(self, de1, listOfNxDs0BoundPw);
    return cAtPwTypeInvalid;
    }

uint32 ThaModuleMapTimingRefCtrlRegister(ThaModuleAbstractMap self)
    {
    if (self)
        return mMethodsGet(mThis(self))->MapTimingRefCtrl(mThis(self));
    return 0x0;
    }

eAtRet ThaModuleMapMapLineControlVcDefaultSet(ThaModuleMap self, AtSdhChannel vc)
    {
    if (self)
        return mMethodsGet(self)->MapLineControlVcDefaultSet(self, vc);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleMapMapLineControlDe3DefaultSet(ThaModuleMap self, AtPdhChannel de3)
    {
    if (self)
        return mMethodsGet(self)->MapLineControlDe3DefaultSet(self, de3);
    return cAtErrorNullPointer;
    }
    
void ThaModuleMapHoBertHwDefault(ThaModuleMap self, AtSdhChannel channel)
    {
    if (self)
        mMethodsGet(self)->HoBertHwDefault(self, channel);
    }
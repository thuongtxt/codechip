/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement 0x803700with Arrive Technologies.
 *
 * Block       : DATA MAP
 *
 * File        : thamapreg.h
 *
 * Created Date: 02-Apr-09
 *
 * Description : This file contain all register definitions of DATA MAP block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _THAMODULEMAPREG_HEADER
#define _THAMODULEMAPREG_HEADER
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : Thalassa Demap DE3LIU over CEP
Reg Addr   : 0x1A4301
Reg Formula:
    Where  :
Reg Desc   :
The registers provide the idle pattern

------------------------------------------------------------------------------*/
#define cThaRegDmapDe3LiuOverCepCtrl                  0x1A4301

/*--------------------------------------
BitField Name: DS3LIUoverCEP
BitField Type: RW
BitField Desc: DS3LIU path over VC3-CEP, each bit for per channel, 0:VC3; 1:TU3
BitField Bits: [31:0]
--------------------------------------*/
#define cThaDmapDe3LiuOverCepMask                     cBit31_0
#define cThaDmapDe3LiuOverCepShift                    0

/*------------------------------------------------------------------------------
 Reg Name: Thalassa Demap Channel Control
 Reg Addr: 0x1A0000 - 0x1A03FF
 The address format for these registers is 0x1A0000 + pid[4:0]*32 + tsid[4:0]
 Where: pid: (0 � 31): DS1/E1 physical ID
 tsid[4:0]    : (0 � 31): DS0 timeslot ID
 Reg Desc: The registers provide the pseudo-wire configurations for PDH ?
 pseudo-wire side. Bit description:
 ------------------------------------------------------------------------------*/
#define cThaRegDmapChnCtrl                            0x1A0000

/*--------------------------------------
 BitField Name: DemapChannelType[2:0]
 BitField Type: R/W
 BitField Desc: Specify which type of mapping for this.
 - 0: Disable
 - 1: SAToP    2: ATM  3: PLCP 4: ATM with Nibble mode for E3
 G.751
 - 5: PLCP with Nibble mode for E3 G.751
 - 6: Unused
 - 7: Unused[9:0]  DemapPWId[9:0]: PW ID field that uses for
 SAToP mode.
 BitField Bits: 12_10
 --------------------------------------*/
#define cThaDmapChnTypeMask                            cBit11_9
#define cThaDmapChnTypeShift                           9

/*--------------------------------------
 BitField Name: DemapFirstTs
 BitField Type: R/W
 BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
 BitField Bits: 9
 --------------------------------------*/
#define cThaDmapFirstTsMask                            cBit8
#define cThaDmapFirstTsShift                           8

/*--------------------------------------
 BitField Name: DemapTsEn
 BitField Type: R/W
 BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
 BitField Bits: 8
 --------------------------------------*/
#define cThaDmapTsEnMask                               cBit7
#define cThaDmapTsEnShift                              7

/*--------------------------------------
 BitField Name: DemapPwId[9:0]
 BitField Type: R/W
 BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
 BitField Bits: 9_0
 --------------------------------------*/
#define cThaDmapPwIdMask                               cBit6_0
#define cThaDmapPwIdShift                              0

#define cThaRegDmapChnCtrlNoneEditableFieldsMask cBit31_12

/*------------------------------------------------------------------------------
 Reg Name: Thalassa Map Channel Control
 Reg Addr: 0x1A0400 - 0x1A07FF
 The address format for these registers is 0x1A0400 + pid[4:0]*32 + tsid[4:0]
 Where: pid: (0 � 31): DS1/E1 physical ID
 tsid[4:0]   : (0 � 31): DS0 timeslot ID
 Reg Desc: The registers provide the pseudo-wire configurations for pseudo-wire
 to PDH side. Bit description:
 ------------------------------------------------------------------------------*/
#define cThaRegMapChnCtrl                           0x1A0400

#define cThaMapChnTypeMask                          cBit16_14
#define cThaMapChnTypeShift                         14

/*--------------------------------------
 BitField Name: MapPWIdField[9:0]
 BitField Type: R/W
 BitField Desc: PWID for the DS3/E3 physical port in SAToP mode
 BitField Bits: 17_8
 --------------------------------------*/
#define cThaMapFirstTsMask                          cBit13
#define cThaMapFirstTsShift                         13

#define cThaMapTsEnMask                             cBit12
#define cThaMapTsEnShift                            12

#define cThaMapChnIdMask                            cBit11_7
#define cThaMapChnIdShift                           7

#define cThaMapPWIdFieldMask                        cBit6_0
#define cThaMapPWIdFieldShift                       0

#define cThaRegMapChnCtrlNoneEditableFieldsMask (cBit31_17 | cBit13 | cBit11_7)

/*------------------------------------------------------------------------------
 Reg Name: Thalassa Map Timing Reference Control
 Reg Addr: 0x1A0800 - 0x1A081F
 The address format for these registers is 0x1A0800 + pid[4:0]
 Where: pid: (0 � 31): DS1/E1 physical ID
 Description: The registers provide the timing reference source for physical
 DS1/E1 ports.
 ------------------------------------------------------------------------------*/
#define cThaRegMapTimingRefCtrl                  0x1A0800

/*--------------------------------------
 BitField Name: cThaMapTmDs1E1IdleCodeMask
 BitField Type: R/W
 BitField Desc:
 --------------------------------------*/
#define cThaMapTmDs1E1IdleCodeMask                   cBit14_7
#define cThaMapTmDs1E1IdleCodeShift                  7

/*--------------------------------------
 BitField Name: cThaMapTmDs1E1IdleEnableMask
 BitField Type: R/W
 BitField Desc:
 --------------------------------------*/
#define cThaMapTmDs1E1IdleEnableMask                 cBit6
#define cThaMapTmDs1E1IdleEnableShift                6

/*--------------------------------------
 BitField Name: cThaMapTmDs1LoopcodeMask
 BitField Type: R/W
 BitField Desc: bit [10:8] MapDs1Loopcode[2:0]:
DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU
Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down
 BitField Bits: 6
 --------------------------------------*/
#define cThaMapTmDs1LoopcodeMask                     cBit10_8
#define cThaMapTmDs1LoopcodeShift                    8

/*--------------------------------------
 BitField Name: cThaMapTmDs1E1FrmMdMask
 BitField Type: R/W
 BitField Desc: bit [6] MapDs1FrmMode[0]:
 1: DS1 SF/ESF mode
 0: Other modes (default)
 BitField Bits: 6
 --------------------------------------*/
#define cThaMapTmDs1E1FrmMdMask                      cBit6
#define cThaMapTmDs1E1FrmMdShift                     6

/*--------------------------------------
 BitField Name: MapTimeDs1E1Md
 BitField Type: R/W
 BitField Desc: MapTimeDs1E1Md[0]:
 0: E1 mode (default)
 1: DS1 mode
 BitField Bits: 5
 --------------------------------------*/
#define cThaMapTmDs1E1MdMask                      cBit5
#define cThaMapTmDs1E1MdShift                     5

/*--------------------------------------
 BitField Name: MaptimeSrcId
 BitField Type: R/W
 BitField Desc: The physical port ID uses for timing reference.
 BitField Bits: 4_0
 --------------------------------------*/
#define cThaMapTmSrcIdMask                      cBit4_0
#define cThaMapTmSrcIdShift                     0


#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMAPREG_HEADER */



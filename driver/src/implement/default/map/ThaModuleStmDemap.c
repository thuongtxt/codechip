/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ThaModuleStmDemap.c
 *
 * Created Date: Mar 21, 2013
 *
 * Description : DEMAP sub module of STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleStmMapReg.h"
#include "../../../generic/pdh/AtPdhChannelInternal.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../sdh/ThaModuleSdh.h"
#include "../pw/adapters/ThaPwAdapter.h"
#include "ThaModuleStmMapInternal.h"
#include "../util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleDemapMethods       m_ThaModuleDemapOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* To save super implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern uint32 ThaModuleStmMapDemapDe3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3);

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleDemap, DmapChnType)
mDefineMaskShift(ThaModuleDemap, DmapFirstTs)
mDefineMaskShift(ThaModuleDemap, DmapTsEn)
mDefineMaskShift(ThaModuleDemap, DmapPwId)

static uint32 De1DmapChnCtrl(ThaModuleDemap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cThaRegDmapChnCtrl;
    }

static uint32 Ds0DefaultOffset(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId)
    {
    return ThaStmMapDemapDs0DefaultOffset(self, de1, timeslotId) +
           ThaModuleDemapPartOffset(self, ThaPdhDe1PartId((ThaPdhDe1)de1));
    }

static uint32 Vc1xDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    return m_ThaModuleAbstractMapMethods->Vc1xDefaultOffset(self, vc1x) +
           ThaModuleDemapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc1x));
    }

static uint32 AuVcStsDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    return m_ThaModuleAbstractMapMethods->AuVcStsDefaultOffset(self, vc, stsId) +
           ThaModuleMapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc));
    }

static uint32 De3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    return ThaModuleStmMapDemapDe3DefaultOffset(self, de3);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x1A0000) &&
        (localAddress <= 0x1A1F7F))
        return cAtTrue;

    return cAtFalse;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleStmMapRegisterIteratorCreate(self);
    }

static eAtRet BindDe3ToChannel(ThaModuleAbstractMap self, AtPdhDe3 de3, AtChannel channel, uint8 mapType)
    {
    uint32 regAddr, regVal = 0;
    eBool enable;
    ThaModuleDemap demap = (ThaModuleDemap)self;
    uint32 mask, shift;

    /* When bind physical to encap channel some product,
     * need disable first and then enable in after step */
    if (mapType == cBindVcgType)
        enable = channel ? cAtTrue : cAtFalse;
	else
    	enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, channel);


    /* Need to disable hardware before reseting configuration */
    regAddr = mMethodsGet(demap)->De3DmapChnCtrl(demap, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    if (!enable)
        {
        regVal = mChannelHwRead(de3, regAddr, cThaModuleDemap);

        mask  = mFieldMask(demap, DmapTsEn);
        shift = mFieldShift(demap, DmapTsEn);
        mFieldIns(&regVal, mask, shift, enable);
        mChannelHwWrite(de3, regAddr, regVal, cThaModuleDemap);
        }

    /* Apply new configuration */
    mask = mFieldMask(demap, DmapFirstTs);
    shift = mFieldShift(demap, DmapFirstTs);
    mFieldIns(&regVal, mask, shift, 0);

    mask = mFieldMask(demap, DmapChnType);
    shift = mFieldShift(demap, DmapChnType);
    mFieldIns(&regVal, mask, shift, mapType);

    mask  = mFieldMask(demap, DmapTsEn);
    shift = mFieldShift(demap, DmapTsEn);
    mFieldIns(&regVal, mask, shift, enable);

    mask  = mFieldMask(demap, DmapPwId);
    shift = mFieldShift(demap, DmapPwId);
    mFieldIns(&regVal, mask, shift, AtChannelHwIdGet((AtChannel)channel));
    mChannelHwWrite(de3, regAddr, regVal, cThaModuleDemap);

    return cAtOk;
    }

static eAtRet BindDe3ToEncap(ThaModuleAbstractMap self, AtPdhDe3 de3, AtEncapChannel encapChannel)
    {
    return BindDe3ToChannel(self, de3, (AtChannel)encapChannel, mMethodsGet(self)->EncapHwChannelType(self, encapChannel));
    }

static eAtRet BindDe3ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe3 de3, AtConcateGroup concateGroup)
    {
    AtChannel concateChannel = mMethodsGet(self)->ConcateChannelForBinding(self, (AtChannel)de3, concateGroup);
    return BindDe3ToChannel(self, de3, concateChannel, 7);
    }

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint8  numSts;
    uint8  sts_i;
    uint32 mask;
    uint32 shift;
	uint8 startSts = AtSdhChannelSts1Get((AtSdhChannel)vc);

    numSts = AtSdhChannelNumSts((AtSdhChannel)vc);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 offset  = mMethodsGet(self)->AuVcStsDefaultOffset(self, vc, (uint8)(startSts + sts_i));
        uint32 address = mMethodsGet(demapModule)->VcDmapChnCtrl(demapModule, vc) + offset;
        uint32 regVal  = 0;
        if (pw)
            {
            mask = mFieldMask(demapModule, DmapFirstTs);
            shift = mFieldShift(demapModule, DmapFirstTs);
            mFieldIns(&regVal, mask  , shift, 0);

            mask  = mFieldMask(demapModule, DmapTsEn);
            shift = mFieldShift(demapModule, DmapTsEn);
            mFieldIns(&regVal, mask, shift, mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, pw)));

            mask = mFieldMask(demapModule, DmapChnType);
            shift = mFieldShift(demapModule, DmapChnType);
            mFieldIns(&regVal, mask, shift, mMethodsGet(self)->PwHwChannelType(self, pw));

            mask = mFieldMask(demapModule, DmapPwId);
            shift = mFieldShift(demapModule, DmapPwId);
            mFieldIns(&regVal, mask, shift, AtChannelHwIdGet((AtChannel)pw));
            }
        mChannelHwWrite(vc, address, regVal, cThaModuleDemap);
        }
    return cAtOk;
    }

static eAtRet De3EncapConnectionEnable(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool enable)
    {
    uint32 regAddr, regVal = 0;
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 mask, shift;

    /* Need to disable hardware before reseting configuration */
    regAddr = mMethodsGet(demapModule)->De3DmapChnCtrl(demapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    regVal = mChannelHwRead(de3, regAddr, cThaModuleDemap);
    mask  = mFieldMask(demapModule, DmapTsEn);
    shift = mFieldShift(demapModule, DmapTsEn);
    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
    mChannelHwWrite(de3, regAddr, regVal, cThaModuleDemap);

    return cAtOk;
    }

static eBool De3EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 regAddr = mMethodsGet(demapModule)->De3DmapChnCtrl(demapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 regVal = mChannelHwRead(de3, regAddr, cThaModuleDemap);
    uint32 mask = mFieldMask(demapModule, DmapTsEn);

    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static eAtRet VcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    /* Need to disable hardware before reseting configuration */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 stsId    = (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i);
        uint32 regAddr = mMethodsGet(demapModule)->VcDmapChnCtrl(demapModule, vcx) + mMethodsGet(self)->AuVcStsDefaultOffset(self, vcx, stsId);
        uint32 regVal  = mChannelHwRead(vcx, regAddr, cThaModuleDemap);
        uint32 mask    = mFieldMask(demapModule, DmapTsEn);
        uint32 shift   = mFieldShift(demapModule, DmapTsEn);
        mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
        mChannelHwWrite(vcx, regAddr, regVal, cThaModuleDemap);
        }

    return cAtOk;
    }

static eBool VcxEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 stsId    = (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i);
        uint32 regAddr = mMethodsGet(demapModule)->VcDmapChnCtrl(demapModule, vcx) + mMethodsGet(self)->AuVcStsDefaultOffset(self, vcx, stsId);
        uint32 regVal  = mChannelHwRead(vcx, regAddr, cThaModuleDemap);
        uint32 mask    = mFieldMask(demapModule, DmapTsEn);

        if ((regVal & mask) == 0)
            return cAtFalse;
        }

    return (numSts != 0) ? cAtTrue : cAtFalse;
    }

static eAtRet De1Init(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    uint8 i, numberOfTimeSlots;
    uint32 offset;
    ThaModuleDemap demapModule = (ThaModuleDemap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)de1), cThaModuleDemap);
    numberOfTimeSlots = ThaPdhDe1NumDs0Timeslots(de1);

    if (demapModule == NULL)
        return cAtErrorNullPointer;

    for (i = 0; i < numberOfTimeSlots; i++)
        {
        offset = mMethodsGet(self)->Ds0DefaultOffset(self, (AtPdhDe1)de1, i);
        mChannelHwWrite(de1, mMethodsGet(demapModule)->De1DmapChnCtrl(demapModule, de1) + offset, 0x0, cThaModuleDemap);
        }

    return cAtOk;
    }

static eAtRet BindVc1xToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    return ThaModuleAbstractMapBindVc1xToChannel(self, sdhVc, (AtChannel)encapChannel, mMethodsGet(self)->EncapHwChannelType(self, encapChannel));
    }

static eAtRet BindVc1xToConcateGroup(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtConcateGroup concateGroup)
    {
    AtChannel concateChannel = mMethodsGet(self)->ConcateChannelForBinding(self, (AtChannel)sdhVc, concateGroup);
    return ThaModuleAbstractMapBindVc1xToChannel(self, sdhVc, concateChannel, 7);
    }

static eAtRet BindHoVcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    uint8 sts_i;
    eBool enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, (AtChannel)encapChannel);
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);
    uint32 mask, shift;
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = mMethodsGet(demapModule)->VcDmapChnCtrl(demapModule, sdhVc) + mMethodsGet(self)->AuVcStsDefaultOffset(self, sdhVc, (uint8)(stsId + sts_i));
        uint32 regVal  = 0;

        if (enable)
            {
            mask = mFieldMask(demapModule, DmapFirstTs);
            shift = mFieldShift(demapModule, DmapFirstTs);
            mFieldIns(&regVal, mask, shift, 0);

            mask = mFieldMask(demapModule, DmapChnType);
            shift = mFieldShift(demapModule, DmapChnType);
            mFieldIns(&regVal, mask, shift, mMethodsGet(self)->EncapHwChannelType(self, encapChannel));

            mask = mFieldMask(demapModule, DmapTsEn);
            shift = mFieldShift(demapModule, DmapTsEn);
            mFieldIns(&regVal, mask, shift, enable);

            mask = mFieldMask(demapModule, DmapPwId);
            shift = mFieldShift(demapModule, DmapPwId);
            mFieldIns(&regVal, mask, shift, AtChannelHwIdGet((AtChannel)encapChannel));
            }

        /* Clear all value when unbind */
        mChannelHwWrite(sdhVc, regAddr, regVal, cThaModuleDemap);
        }

    return cAtOk;
    }

static eAtRet BindTu3VcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    return BindHoVcToEncap(self, sdhVc, encapChannel);
    }

static uint8 PwCesCasHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet BindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, AtPw pw)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 address = mMethodsGet(demapModule)->De3DmapChnCtrl(demapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 regVal = 0;
    uint32 mask, shift;

    if (pw)
        {
        mask = mFieldMask(demapModule, DmapFirstTs);
        shift = mFieldShift(demapModule, DmapFirstTs);
        mFieldIns(&regVal, mask  , shift, 0);

        mFieldIns(&regVal, mFieldMask(demapModule, DmapTsEn), mFieldShift(demapModule, DmapTsEn), mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, pw)));

        mask = mFieldMask(demapModule, DmapChnType);
        shift = mFieldShift(demapModule, DmapChnType);
        mFieldIns(&regVal, mask  , shift, mMethodsGet(self)->PwHwChannelType(self, pw));

        mask = mFieldMask(demapModule, DmapPwId);
        shift = mFieldShift(demapModule, DmapPwId);
        mFieldIns(&regVal, mask, shift, AtChannelHwIdGet((AtChannel)pw));
        }

    mChannelHwWrite(de3, address, regVal, cThaModuleDemap);

    return cAtOk;
    }

static eBool SdhChannelIsVcx(AtSdhChannel sdhChannel)
    {
    if (AtSdhChannelIsHoVc(sdhChannel))
        return cAtTrue;

    if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc3)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SdhChannelIsVc1x(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    if ((channelType == cAtSdhChannelTypeVc12)    ||
        (channelType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

static void VtDemapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    uint16 numTimeslots = ThaModuleStmMapDemapNumTimeSlotInVc1x((AtSdhVc)sdhChannel);
    uint16 slot_i;

    ThaDeviceRegNameDisplay(regName);

    for (slot_i = 0; slot_i < numTimeslots; slot_i++)
        {
        uint32 address = regBaseAddress + mMethodsGet(self)->Vc1xDefaultOffset(self, (AtSdhVc)sdhChannel) + slot_i;
        ThaDeviceChannelRegValueDisplay((AtChannel)sdhChannel, address, cThaModuleDemap);
        }
    }

static void StsMultiDemapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address = regBaseAddress + mMethodsGet(self)->AuVcStsDefaultOffset(self, (AtSdhVc)sdhChannel, (uint8)(startSts + sts_i));
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleDemap);
        }
    }

static void SdhChannelRegsShow(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    uint32 address;
    eBool isVcx  = SdhChannelIsVcx(sdhChannel);
    eBool isVc1x = SdhChannelIsVc1x(sdhChannel);

    if (!isVcx && !isVc1x)
        return;

    address = mMethodsGet((ThaModuleDemap)self)->VcDmapChnCtrl((ThaModuleDemap)self, (AtSdhVc)sdhChannel);
    if (isVcx)
        StsMultiDemapChannelDisplay(self, sdhChannel, "Demap Channel Control", address);
    if (isVc1x)
        VtDemapChannelDisplay(self, sdhChannel, "Demap Channel Control", address);
    }

static void De3RegShow(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 address = mMethodsGet(demapModule)->De3DmapChnCtrl(demapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);

    ThaDeviceRegNameDisplay("Demap Channel Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cThaModuleDemap);
    }

static void De1RegShow(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;
    uint32 numTimeslots = ThaModuleAbstractMapDe1NumDs0s(self, de1);
    uint16 slot_i;
    uint32 address = mMethodsGet(demapModule)->De1DmapChnCtrl(demapModule, de1);

    ThaDeviceRegNameDisplay("Demap Channel Control");

    for (slot_i = 0; slot_i < numTimeslots; slot_i++)
        {
        uint32 offset = ThaStmMapDemapDs0DefaultOffset(self, de1, (uint8)slot_i);
        ThaDeviceChannelRegValueDisplay((AtChannel)de1, address + offset, cThaModuleDemap);
        }
    }

static void PdhChannelRegsShow(ThaModuleAbstractMap self, AtPdhChannel pdhChannel)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

    if (sdhVc)
        {
        SdhChannelRegsShow(self, sdhVc);
        return;
        }

    if ((channelType == cAtPdhChannelTypeE3) || (channelType == cAtPdhChannelTypeDs3))
        De3RegShow(self, (AtPdhDe3)pdhChannel);

    else if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        De1RegShow(self, (AtPdhDe1)pdhChannel);
    }

static uint8 EncapHwChannelType(ThaModuleAbstractMap self, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(encapChannel);
    return 5;
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaModuleAbstractMapOverride,
                                  m_ThaModuleAbstractMapMethods,
                                  sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, Ds0DefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe3ToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3EncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3EncapConnectionIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1Init);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindVc1xToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindHoVcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindTu3VcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PwCesCasHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcStsDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe3ToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhChannelRegsShow);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PdhChannelRegsShow);
        mMethodOverride(m_ThaModuleAbstractMapOverride, EncapHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe3ToConcateGroup);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindVc1xToConcateGroup);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_ThaModuleDemapOverride));

        /* Override bit fields */
        mBitFieldOverride(demapModule, m_ThaModuleDemapOverride, DmapChnType)
        mBitFieldOverride(demapModule, m_ThaModuleDemapOverride, DmapFirstTs)
        mBitFieldOverride(demapModule, m_ThaModuleDemapOverride, DmapTsEn)
        mBitFieldOverride(demapModule, m_ThaModuleDemapOverride, DmapPwId)

        /* Override register address */
        mMethodOverride(m_ThaModuleDemapOverride, De1DmapChnCtrl);
        }

    mMethodsSet(demapModule, &m_ThaModuleDemapOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleDemap(self);
    OverrideThaModuleAbstractMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleStmDemap);
    }

AtModule ThaModuleStmDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super construction */
    if (ThaModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleStmDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleStmDemapObjectInit(newModule, device);
    }

uint8 ThaModuleStmMapDemapNumTimeSlotInVc1x(AtSdhVc vc)
    {
    return (AtSdhChannelTypeGet((AtSdhChannel)vc) == cAtSdhChannelTypeVc12) ? 32 : 24;
    }

uint32 ThaModuleAbstractMapDe1NumDs0s(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);

    AtUnused(self);
    if (sdhVc)
        return (AtSdhChannelTypeGet(sdhVc) == cAtSdhChannelTypeVc12) ? 32 : 24;

    return AtPdhDe1IsE1(de1) ? 32 : 24;
    }

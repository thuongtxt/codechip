/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2009, 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement 0x803700with Arrive Technologies.
 *
 * Block       : DATA MAP
 *
 * File        :
 *
 * Created Date: 20-Dec-12
 *
 * Description : This file contain all register definitions of DATA MAP block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESTMMAPREG_HEADER
#define _THAMODULESTMMAPREG_HEADER

#include "ThaModuleMapReg.h"

/*------------------------------------------------------------------------------
 Reg Name: Thalassa Demap Channel Control
 Reg Addr: 0x1A0000 - 0x1A03FF
 The address format for these registers is 0x1A0000 + pid[4:0]*32 + tsid[4:0]
 Where: pid: (0 � 31): DS1/E1 physical ID
 tsid[4:0]    : (0 � 31): DS0 timeslot ID
 Reg Desc: The registers provide the pseudo-wire configurations for PDH ?
 pseudo-wire side. Bit description:
 ------------------------------------------------------------------------------*/
#undef cThaRegDmapChnCtrl
#define cThaRegDmapChnCtrl                       0x1A0000

/*--------------------------------------
 BitField Name: DemapFirstTs
 BitField Type: R/W
 BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
 BitField Bits: 9
 --------------------------------------*/
#undef cThaDmapFirstTsMask
#undef cThaDmapFirstTsShift
#define cThaDmapFirstTsMask                            cBit14
#define cThaDmapFirstTsShift                           14

/*--------------------------------------
 BitField Name: DemapChannelType[2:0]
 BitField Type: R/W
 BitField Desc: Specify which type of mapping for this.
    0: SAToP encapsulation from DS1/E1, DS3/E3
    1: CESoP encapsulation from DS1/E1
    2: ATM over DS1/ E1, SONET/SDH (unused)
    3: IMA over DS1/E1, SONET/SDH(unused)
    4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused)
    5: MLPPP over DS1/E1, SONET/SDH(unused)
    6: CEP encapsulation from SONET/SDH
    7: PLCP encapsulation from DS3/E3(unused)
 BitField Bits: 12_10
 --------------------------------------*/
#undef cThaDmapChnTypeMask
#undef cThaDmapChnTypeShift
#define cThaDmapChnTypeMask                            cBit13_11
#define cThaDmapChnTypeShift                           11

/*--------------------------------------
 BitField Name: DemapTsEn
 BitField Type: R/W
 BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
 BitField Bits: 8
 --------------------------------------*/
#undef cThaDmapTsEnMask
#undef cThaDmapTsEnShift
#define cThaDmapTsEnMask                               cBit10
#define cThaDmapTsEnShift                              10

/*--------------------------------------
 BitField Name: DemapPwId[9:0]
 BitField Type: R/W
 BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
 BitField Bits: 9_0
 --------------------------------------*/
#undef cThaDmapPwIdMask
#undef cThaDmapPwIdShift
#undef cThaDmapPwIdMaxVal
#undef cThaDmapPwIdShift
#define cThaDmapPwIdMask                               cBit9_0
#define cThaDmapPwIdShift                              0

/*------------------------------------------------------------------------------
 Reg Name: Thalassa Map Channel Control
 Reg Addr: 0x1B2000 - 0x1B3F7F
 The address format for these registers is
                DS1/VT1.5:  0x1B2000 + 672*stsid + 96*vtgid + 24*vtid + slotid
                E1/VT2: 0x1B2000 + 672*stsid + 96*vtgid + 32*vtid + slotid
                VT6:        0x1B2000 + 672*stsid + 96*vtgid
                DS3/E3: 0x1B2000 + 672*stsid
 Where:  stsid: (0 – 11) is STS identification number
                vtgid: (0 – 6) is VT Group identification number
                vtid: (0 – 3) is VT identification number
                slotid: (0 - 31) is time slot number. It varies from 0 to 23 in DS1 and from 0 to 31 in E1
 ------------------------------------------------------------------------------*/
#undef cThaRegMapChnCtrl
#define cThaRegMapChnCtrl                           0x1B2000

/*--------------------------------------
 BitField Name: MapFirstTs
 BitField Type: R/W
 BitField Desc:
 BitField Bits: 14
 --------------------------------------*/
#undef cThaMapFirstTsMask
#undef cThaMapFirstTsShift
#define cThaMapFirstTsMask                          cBit14
#define cThaMapFirstTsShift                         14

/*--------------------------------------
 BitField Name: MapChannelType[2:0]
 BitField Type: R/W
 BitField Desc: Specify which type of mapping for this.
    0: SAToP encapsulation from DS1/E1, DS3/E3
    1: CESoP encapsulation from DS1/E1
    2: ATM over DS1/ E1, SONET/SDH (unused)
    3: IMA over DS1/E1, SONET/SDH(unused)
    4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused)
    5: MLPPP over DS1/E1, SONET/SDH(unused)
    6: CEP encapsulation from SONET/SDH
    7: PLCP encapsulation from DS3/E3(unused)
 BitField Bits: 13_11
 --------------------------------------*/
#undef cThaMapChnTypeMask
#undef cThaMapChnTypeShift
#define cThaMapChnTypeMask                          cBit13_11
#define cThaMapChnTypeShift                         11

/*--------------------------------------
 BitField Name: MmapTsEn
 BitField Type: R/W
 BitField Desc:
 BitField Bits: 10
 --------------------------------------*/
#undef cThaMapTsEnMask
#undef cThaMapTsEnShift
#define cThaMapTsEnMask                             cBit10
#define cThaMapTsEnShift                            10

/*--------------------------------------
 BitField Name: DemapPwId[9:0]
 BitField Type: R/W
 BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
 BitField Bits: 9_0
 --------------------------------------*/
#undef cThaMapPWIdFieldMask
#undef cThaMapPWIdFieldShift
#define cThaMapPWIdFieldMask                        cBit9_0
#define cThaMapPWIdFieldShift                       0

#undef cThaRegMapChnCtrlNoneEditableFieldsMask
#define cThaRegMapChnCtrlNoneEditableFieldsMask cBit31_15

/*------------------------------------------------------------------------------
Reg Name: Map Line Control
Reg Addr: 0x1B0000 - 0x1B017F
          The address format for these registers is 0x1B0000 + 32*stsid +
          4*vtgid + vtid
          Where: stsid: (0 - 11) is STS identification number
          vtgid: (0 - 6) is VT Group identification number
          vtid: (0 - 3) is VT identification number
Reg Desc: The registers provide the per line configurations for STS/VT/DS1/E1
          line.
------------------------------------------------------------------------------*/
#define cThaRegMapLineCtrl                          0x1B0000

/*--------------------------------------
BitField Name: MapFrameType[1:0]
BitField Type: R/W
BitField Desc: depend on MapSigType[3:0], the MapFrameType[1:0] bit field can
               have difference meaning.
               - 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3
                 framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff
               - 1: DS1 ESF/E1 CRC/E3 G.832(unused)/unused
               - 2:unused,
               - 3:VT with POH/STS with POH, Stuff.
BitField Bits: 15_14
--------------------------------------*/
#define cThaMapFrmTypeMask                             cBit15_14
#define cThaMapFrmTypeShift                            14

/*--------------------------------------
BitField Name: MapSigType[3:0]
BitField Type: R/W
BitField Desc: , the MapFrameType[1:0] bit field can have difference meaning.
               - 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3
                 framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff
               - 1: DS1 ESF/E1 CRC/E3 G.832(unused)/unused
               - 2:unused,
               - 3:VT with POH/STS with POH, Stuff.
BitField Bits: 13_10
--------------------------------------*/
#define cThaMapSigTypeMask                             cBit13_10
#define cThaMapSigTypeShift                            10

/*--------------------------------------
BitField Name: MapTimeSrcMaster
BitField Type: R/W
BitField Desc: This bit is used to indicate the master timing or the master VC3
               in VC4/VC4-Xc.
BitField Bits: 9
--------------------------------------*/
#define cThaMapTimeSrcMastMask                         cBit9
#define cThaMapTimeSrcMastShift                        9

/*--------------------------------------
BitField Name: MapTimeSrcId[8:0]
BitField Type: R/W
BitField Desc: The reference line ID used for timing reference or the master
               VC3 ID in VC4/VC4-Xc.
BitField Bits: 8_0
--------------------------------------*/
#define cThaMapTimeSrcIdMask                           cBit8_0
#define cThaMapTimeSrcIdShift                          0

#define cThaRegMapLineCtrlNoneEditableFieldsMask       cBit31_16

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESTMMAPREG_HEADER */
#ifdef __cplusplus
extern "C" {
#endif

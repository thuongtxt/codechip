/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : ThaModuleDemapInternal.h
 * 
 * Created Date: Mar 21, 2013
 *
 * Description : DEMAP sub module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEDEMAPINTERNAL_H_
#define _THAMODULEDEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleAbstractMapInternal.h"
#include "ThaModuleDemap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleDemapMethods
    {
    /* Register polymorphism */
    uint32 (*De1DmapChnCtrl)(ThaModuleDemap self, AtPdhDe1 de1);
    uint32 (*VcDmapChnCtrl)(ThaModuleDemap self, AtSdhVc vc);
    uint32 (*De3DmapChnCtrl)(ThaModuleDemap self, AtPdhDe3 de3);
    uint32 (*De3LiuOverCepDemapCtrl)(ThaModuleDemap self);

    /* Bit field polymorphism */
    uint32 (*DmapChnTypeMask)(ThaModuleDemap self);
    uint8  (*DmapChnTypeShift)(ThaModuleDemap self);
    uint32 (*DmapFirstTsMask)(ThaModuleDemap self);
    uint8  (*DmapFirstTsShift)(ThaModuleDemap self);
    uint32 (*DmapTsEnMask)(ThaModuleDemap self);
    uint8  (*DmapTsEnShift)(ThaModuleDemap self);
    uint32 (*DmapPwIdMask)(ThaModuleDemap self);
    uint8  (*DmapPwIdShift)(ThaModuleDemap self);
    }tThaModuleDemapMethods;

typedef struct tThaModuleDemap
    {
    tThaModuleAbstractMap super;
    const tThaModuleDemapMethods *methods;

    /* Private data */
    }tThaModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleDemapObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEDEMAPINTERNAL_H_ */


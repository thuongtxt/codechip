/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : ThaModuleStmMap.c
 *
 * Created Date: Oct 4, 2012
 *
 * Description : Internal mapping module for STM card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleStmMapInternal.h"
#include "ThaModuleStmMapReg.h"
#include "AtPdhNxDs0.h"
#include "AtSdhVc.h"
#include "../sdh/ThaModuleSdh.h"

#include "../../../generic/pdh/AtPdhChannelInternal.h"
#include "../../../generic/pdh/AtPdhDe3Internal.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "../sdh/ThaModuleSdh.h"
#include "../pdh/ThaModulePdhInternal.h"
#include "../pdh/ThaPdhVcDe1.h"
#include "../pdh/ThaPdhDe2De1.h"
#include "../cdr/ThaModuleCdrStm.h"

/*--------------------------- Define -----------------------------------------*/
/* For CEP */
#define cCepFractionalVc3     2
#define cCepFractionalVc1x    1
#define cCepBasic             3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaModuleStmMap)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleStmMapMethods m_methods;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleMapMethods         m_ThaModuleMapOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* To save super implementation */
static const tAtModuleMethods             *m_AtModuleMethods = NULL;
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern uint32 ThaModuleStmMapDemapDe3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3);

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapChnType)
mDefineMaskShift(ThaModuleMap, MapFirstTs)
mDefineMaskShift(ThaModuleMap, MapTsEn)
mDefineMaskShift(ThaModuleMap, MapPWIdField)

static uint32 MapFrmTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapFrmTypeMask;
    }

static uint8 MapFrmTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapFrmTypeShift;
    }

static uint32 MapSigTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapSigTypeMask;
    }

static uint8 MapSigTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapSigTypeShift;
    }

static uint32 MapTimeSrcMastMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcMastMask;
    }

static uint8 MapTimeSrcMastShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcMastShift;
    }

static uint32 MapTimeSrcIdMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcIdMask;
    }

static uint8 MapTimeSrcIdShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcIdShift;
    }

static uint32 De1MapChnCtrl(ThaModuleMap self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cThaRegMapChnCtrl;
    }

static uint32 De1MapLineControl(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
    return ThaModuleStmMapMapLineCtrl((ThaModuleStmMap)self, vc);
    }

static uint32 De1MapLineOffset(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
    if (vc)
        return ThaModuleStmMapSdhChannelMapLineOffset((ThaModuleStmMap)self, vc, AtSdhChannelSts1Get(vc), AtSdhChannelTug2Get(vc), AtSdhChannelTu1xGet(vc));
    else
        return ThaModuleStmMapPdhDe2De1MapLineOffset((ThaModuleStmMap)self, de1);
    }

static uint32 De1LoopcodeRegister(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    return De1MapLineControl(self, de1) + De1MapLineOffset(self, de1);
    }

static eAtRet De1LoopcodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 loopcode)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = De1LoopcodeRegister(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1Loopcode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1Loopcode);
    mFieldIns(&regVal, mask, shift, loopcode);
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);
    return cAtOk;
    }

static uint8 De1LoopcodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = De1LoopcodeRegister(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1Loopcode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1Loopcode);
    uint8 loopcode = 0;
    mFieldGet(regVal, mask, shift, uint8, &loopcode);
    return loopcode;
    }

static uint32 De1IdleCodeRegister(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    return De1MapLineControl(self, de1) + De1MapLineOffset(self, de1);
    }

static eAtRet De1IdleCodeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 idleCode)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = De1IdleCodeRegister(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleCode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleCode);
    mFieldIns(&regVal, mask, shift, idleCode);
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);
    return cAtOk;
    }

static uint8 De1IdleCodeGet(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = De1IdleCodeRegister(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleCode);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleCode);
    uint8 idleCode = 0;
    mFieldGet(regVal, mask, shift, uint8, &idleCode);
    return idleCode;
    }

static eAtRet De1IdleEnable(ThaModuleAbstractMap self, AtPdhDe1 de1, eBool enable)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = De1IdleCodeRegister(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleEnable);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleEnable);
    mFieldIns(&regVal, mask, shift, (enable) ? 1 : 0);
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);
    return cAtOk;
    }

static eBool De1IdleIsEnabled(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = De1IdleCodeRegister(self, de1);
    uint32 regVal = mChannelHwRead(de1, address, cThaModuleMap);
    uint32 mask  = mFieldMask(mapModule, MapTmDs1E1IdleEnable);
    uint32 shift = mFieldShift(mapModule, MapTmDs1E1IdleEnable);
    uint8 idleEnable = 0;
    mFieldGet(regVal, mask, shift, uint8, &idleEnable);
    return (idleEnable) ? cAtTrue : cAtFalse;
    }

static uint32 PartOffset(ThaModuleMap self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleMap, partId);
    }

static uint32 Ds0DefaultOffset(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId)
    {
    return ThaStmMapDemapDs0DefaultOffset(self, de1, timeslotId) +
           ThaModuleMapPartOffset(self, ThaPdhDe1PartId((ThaPdhDe1)de1));
    }

static uint32 Vc1xDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    return m_ThaModuleAbstractMapMethods->Vc1xDefaultOffset(self, vc1x) +
           ThaModuleMapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc1x));
    }

static uint32 AuVcStsDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    return m_ThaModuleAbstractMapMethods->AuVcStsDefaultOffset(self, vc, stsId) +
           ThaModuleMapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc));
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x1B0000) &&
        (localAddress <= 0x1B3F7F))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 De1SignalType(uint16 frameType)
    {
    if ((frameType == cAtPdhDs1J1UnFrm) ||
        (frameType == cAtPdhE1UnFrm))
        return 3;

    if ((frameType == cAtPdhDs1FrmSf) ||
        (frameType == cAtPdhJ1FrmSf) ||
        (frameType == cAtPdhE1Frm))
        return 0;

    if ((frameType == cAtPdhDs1FrmEsf) ||
        (frameType == cAtPdhJ1FrmEsf) ||
        (frameType == cAtPdhE1MFCrc))
        return 1;

    return 2;
    }

static uint8 De1SigType(uint16 frameType)
    {
    uint8 ret = 0;
    if ((frameType == cAtPdhDs1J1UnFrm) || (frameType == cAtPdhDs1FrmSf) ||
        (frameType == cAtPdhJ1FrmSf) || (frameType == cAtPdhDs1FrmEsf) ||
        (frameType == cAtPdhJ1FrmEsf))
        {
        ret = 0;
        }
    else if ( (frameType == cAtPdhE1UnFrm) || (frameType == cAtPdhE1MFCrc) ||
        (frameType == cAtPdhE1Frm))
        ret = 1;
    else
        ret = 0;

    return ret;
    }

static eAtRet De1FrameModeSet(ThaModuleAbstractMap self, AtPdhDe1 de1, uint16 frameType)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
    uint32 offset  = ThaPdhDe1MapLineOffset((ThaPdhDe1)de1);
    uint32 address = ThaModuleStmMapMapLineCtrl((ThaModuleStmMap)self, sdhChannel) + offset;
    uint32 regVal  = mChannelHwRead(de1, address, cThaModuleMap);

    mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, sdhChannel), mModuleMapShift(self, MapFrmType, sdhChannel), De1SignalType(frameType));
    mFieldIns(&regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), De1SigType(frameType));
    mChannelHwWrite(de1, address, regVal, cThaModuleMap);

    return cAtOk;
    }

static eBool IsE3FramingType(eAtPdhDe3FrameType frameType)
    {
    return mOutOfRange(frameType, cAtPdhE3Unfrm, cAtPdhE3FrmG751) ? cAtFalse : cAtTrue;
    }

static uint8 De3FrameTypeSw2Hw(eAtPdhDe3FrameType frameType)
    {
    if ((frameType == cAtPdhDs3Unfrm) ||
        (frameType == cAtPdhE3Unfrm))
        return 3;

    if (frameType == cAtPdhE3Frmg832) return 1;
    if (frameType == cAtPdhE3FrmG751) return 0;

    /* Can be PW? */
    return 2;
    }

static uint32 De3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    return ThaModuleStmMapDemapDe3DefaultOffset(self, de3);
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleStmMapRegisterIteratorCreate(self);
    }

static eAtRet BindDe3ToChannel(ThaModuleAbstractMap self, AtPdhDe3 de3, AtChannel channel, uint8 mapType)
    {
    uint32 regAddr, regVal = 0;
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 mask, shift;
    eBool enable;

    /* When bind physical to encap channel some product,
     * need disable first and then enable in after step */
    if (mapType == cBindVcgType)
        enable = channel ? cAtTrue : cAtFalse;
	else
	    enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, channel);
        
    /* Need to disable hardware before reseting configuration */
    regAddr = mMethodsGet(moduleMap)->De3MapChnCtrl(moduleMap, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    if (!enable)
        {
        regVal = mChannelHwRead(de3, regAddr, cThaModuleMap);
        mask  = mFieldMask(moduleMap, MapTsEn);
        shift = mFieldShift(moduleMap, MapTsEn);
        mFieldIns(&regVal, mask, shift, enable);
        mChannelHwWrite(de3, regAddr, regVal, cThaModuleMap);
        }

    /* Apply hardware configuration */
    regVal = mChannelHwRead(de3, regAddr, cThaModuleMap);
    mask  = mFieldMask(moduleMap, MapFirstTs);
    shift = mFieldShift(moduleMap, MapFirstTs);
    mFieldIns(&regVal, mask, shift, 0);

    mask  = mFieldMask(moduleMap, MapChnType);
    shift = mFieldShift(moduleMap, MapChnType);
    mFieldIns(&regVal, mask, shift, mapType);

    mask  = mFieldMask(moduleMap, MapTsEn);
    shift = mFieldShift(moduleMap, MapTsEn);
    mFieldIns(&regVal, mask, shift, enable);

    mask  = mFieldMask(moduleMap, MapPWIdField);
    shift = mFieldShift(moduleMap, MapPWIdField);
    mFieldIns(&regVal, mask, shift, AtChannelHwIdGet(channel));
    mChannelHwWrite(de3, regAddr, regVal, cThaModuleMap);

    return cAtOk;
    }

static eAtRet BindDe3ToEncap(ThaModuleAbstractMap self, AtPdhDe3 de3, AtEncapChannel encapChannel)
    {
    return BindDe3ToChannel(self, de3, (AtChannel)encapChannel, mMethodsGet(self)->EncapHwChannelType(self, encapChannel));
    }

static eAtRet BindDe3ToConcateGroup(ThaModuleAbstractMap self, AtPdhDe3 de3, AtConcateGroup concateGroup)
    {
    AtChannel concateChannel = mMethodsGet(self)->ConcateChannelForBinding(self, (AtChannel)de3, concateGroup);
    return BindDe3ToChannel(self, de3, concateChannel, 7);
    }

static eAtRet De3ChannelizedFrameTypeSet(ThaModuleAbstractMap self, AtPdhDe3 de3, eAtPdhDe3FrameType frameType)
    {
    uint32 de2_i, de1_i;
    eAtPdhDe1FrameType de1FrameType = cAtPdhE1UnFrm;
    ThaPdhDe1 de1;
    uint8 numDe1 = 3;

    if (AtPdhDe3IsChannelizedToDs1(frameType))
        {
        de1FrameType = cAtPdhDs1J1UnFrm;
        numDe1 = 4;
        }

    for (de2_i = 0; de2_i < 7; de2_i++)
        {
        for (de1_i = 0; de1_i < numDe1; de1_i++)
            {
            uint32 offset  = ThaModuleStmMapPdhDe3De2De1MapLineOffset(mThis(self), de3, de2_i, de1_i);
            uint32 regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), NULL) + offset;
            uint32 regVal  = mChannelHwRead(de3, regAddr, cThaModuleMap);
            mFieldIns(&regVal, mModuleMapMask(self, MapSigType, NULL), mModuleMapShift(self, MapSigType, NULL), De1SigType(de1FrameType));
            mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, NULL), mModuleMapShift(self, MapFrmType, NULL), De1SignalType(de1FrameType));
            mChannelHwWrite(de3, regAddr, regVal, cThaModuleMap);

            /* Update cache */
            de1 = (ThaPdhDe1)AtPdhDe3De1Get(de3, (uint8)de2_i, (uint8)de1_i);
            ThaPdhDe1SwFrameTypeSet(de1, de1FrameType);
            }
        }

    return cAtOk;
    }

static eAtRet De3UnChannelizedFrameTypeSet(ThaModuleAbstractMap self, AtPdhDe3 de3, eAtPdhDe3FrameType frameType)
    {
    uint32 regAddr, regVal, offset;
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3);

    if (sdhVc)
        offset = ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhVc, AtSdhChannelSts1Get(sdhVc), 0, 0);
    else
        offset = ThaModuleStmMapPdhDe3MapLineOffset(mThis(self), de3);

    regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), NULL) + offset;
    regVal  = mChannelHwRead(de3, regAddr, cThaModuleMap);
    mFieldIns(&regVal, mModuleMapMask(self, MapSigType, NULL), mModuleMapShift(self, MapSigType, NULL), IsE3FramingType(frameType) ? 3 : 2);
    mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, NULL), mModuleMapShift(self, MapFrmType, NULL), De3FrameTypeSw2Hw(frameType)); /* Fixed by hardware */
    mChannelHwWrite(de3, regAddr, regVal, cThaModuleMap);

    return cAtOk;
    }

static eAtRet De3FrameModeSet(ThaModuleAbstractMap self, AtPdhDe3 de3, eAtPdhDe3FrameType frameType)
    {
    if (AtPdhDe3IsChannelizedFrameType(frameType))
        return De3ChannelizedFrameTypeSet(self, de3, frameType);
    return De3UnChannelizedFrameTypeSet(self, de3, frameType);
    }

static uint8 VcMapSignalType(AtSdhChannel vc)
    {
    uint8 vcType = AtSdhChannelTypeGet(vc);

    switch (vcType)
        {
        case cAtSdhChannelTypeVc11:    return 4;
        case cAtSdhChannelTypeVc12:    return 5;
        case cAtSdhChannelTypeVc4:     return 9;
        case cAtSdhChannelTypeVc4_4c:  return 10;
        case cAtSdhChannelTypeVc4_16c: return 11;
        case cAtSdhChannelTypeVc4_nc:  return 11;
        case cAtSdhChannelTypeVc4_64c: return 11;
        case cAtSdhChannelTypeVc3:     return AtSdhVcStuffIsEnabled((AtSdhVc)vc) ? 8 : 12;
        default:
            break;
        }

    return 0xFF;
    }

static uint32 Vc1xBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    uint32 address, regVal;
    AtSdhChannel sdhChannel;
    uint8 tugId, vtId;
    uint8 mapSignalType;
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 mask, shift, temp;

    /* Check with map line control to see if this channel is mapped */
    sdhChannel = (AtSdhChannel)vc1x;
    tugId = AtSdhChannelTug2Get(sdhChannel);
    vtId = AtSdhChannelTu1xGet(sdhChannel);

    address = ThaModuleStmMapMapLineCtrl(mThis(self), sdhChannel) +
              ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhChannel, AtSdhChannelSts1Get(sdhChannel), tugId, vtId);
    regVal  = mChannelHwRead(vc1x, address, cThaModuleMap);

    mFieldGet(regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), uint8, &mapSignalType);
    if (mapSignalType != VcMapSignalType(sdhChannel))
        return cThaModulePwInvalidPwId;

    /* Get PW id */
    address = mMethodsGet(moduleMap)->VcMapChnCtrl(moduleMap, vc1x) + mMethodsGet(self)->Vc1xDefaultOffset(self, vc1x);
    regVal = mChannelHwRead(vc1x, address, cThaModuleMap);

    mask  = mFieldMask(moduleMap, MapChnType);
    shift = mFieldShift(moduleMap, MapChnType);
    mFieldGet(regVal, mask, shift, uint32, &temp);
    if (temp != mMethodsGet(self)->PwCepHwChannelType(self))
        return cThaModulePwInvalidPwId;

    mask  = mFieldMask(moduleMap, MapPWIdField);
    shift = mFieldShift(moduleMap, MapPWIdField);
    mFieldGet(regVal, mask, shift, uint32, &temp);

    return temp;
    }

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)vc);
    uint8 sts_i;
    uint8 startSts = AtSdhChannelSts1Get((AtSdhChannel)vc);
    uint32 mask, shift;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
    	uint32 offset  = mMethodsGet(self)->AuVcStsDefaultOffset(self, vc, (uint8)(startSts + sts_i));
        uint32 address = mMethodsGet(mapModule)->VcMapChnCtrl(mapModule, vc) + offset;
        uint32 regVal  = 0;

        if (pw)
            {
            mask  = mFieldMask(mapModule, MapFirstTs);
            shift = mFieldShift(mapModule, MapFirstTs);
            mFieldIns(&regVal, mask  , shift, 0);

            mask  = mFieldMask(mapModule, MapTsEn);
            shift = mFieldShift(mapModule, MapTsEn);
            mFieldIns(&regVal, mask, shift, mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, pw)));

            mask  = mFieldMask(mapModule, MapChnType);
            shift = mFieldShift(mapModule, MapChnType);
            mFieldIns(&regVal, mask, shift, mMethodsGet(self)->PwHwChannelType(self, pw));

            mask  = mFieldMask(mapModule, MapPWIdField);
            shift = mFieldShift(mapModule, MapPWIdField);
            mFieldIns(&regVal, mask, shift, AtChannelHwIdGet((AtChannel)pw));
            }

        mChannelHwWrite(vc, address, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static uint32 AuVcBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode)
    {
    uint8 stsId = AtSdhChannelSts1Get((AtSdhChannel)vc);
    static const uint8 cVtgDontCare = 0;
    static const uint8 cVtDontCare = 0;
    AtSdhChannel sdhChannel;
    uint8 mapSignalType, mapFrameType;
    uint32 offset, address, regVal;
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 mask, shift, temp;

    sdhChannel = (AtSdhChannel)vc;
    address = ThaModuleStmMapMapLineCtrl(mThis(self), sdhChannel) +
              ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhChannel, stsId, cVtgDontCare, cVtDontCare);
    regVal  = mChannelHwRead(vc, address, cThaModuleMap);

    mFieldGet(regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), uint8, &mapSignalType);
    if (mapSignalType != VcMapSignalType(sdhChannel))
        return cThaModulePwInvalidPwId;

    /* Map frame type decide CEP mode */
    mFieldGet(regVal, mModuleMapMask(self, MapFrmType, sdhChannel), mModuleMapShift(self, MapFrmType, sdhChannel), uint8, &mapFrameType);
    if (mapFrameType == cMapFrameTypeCepBasic)
        *cepMode = cAtPwCepModeBasic;
    else if (mapFrameType == cMapFrameTypeCepFractionalVc1x || mapFrameType == cMapFrameTypeCepFractionalVc3)
        *cepMode = cAtPwCepModeFractional;
    else
        return cThaModulePwInvalidPwId;

    /* Focus first STS */
    offset  = mMethodsGet(self)->AuVcStsDefaultOffset(self, vc, stsId);
    address = mMethodsGet(mapModule)->VcMapChnCtrl(mapModule, vc) + offset;
    regVal  = mChannelHwRead(vc, address, cThaModuleMap);

    mask  = mFieldMask(mapModule, MapChnType);
    shift = mFieldShift(mapModule, MapChnType);
    mFieldGet(regVal, mask, shift, uint32, &temp);
    if (temp != mMethodsGet(self)->PwCepHwChannelType(self))
        return cThaModulePwInvalidPwId;

    mask  = mFieldMask(mapModule, MapPWIdField);
    shift = mFieldShift(mapModule, MapPWIdField);
    mFieldGet(regVal, mask, shift, uint32, &temp);

    return temp;
    }

static uint8 HoVcHwSignalType(ThaModuleAbstractMap self, AtSdhVc sdhVc)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)sdhVc);
	AtUnused(self);

    if (sdhChannelType == cAtSdhChannelTypeVc4_4c) return 0xA;
    if (sdhChannelType == cAtSdhChannelTypeVc4)    return 0x9;
    if (sdhChannelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet((AtSdhChannel)sdhVc);
        if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            return 0x8;

        /* TU-3 */
        return 12;
        }

    return 0;
    }

static uint8 HoVcHwFrameType(ThaModuleAbstractMap self, AtSdhVc sdhVc)
    {
    AtUnused(self);
    if (AtSdhChannelTypeGet((AtSdhChannel)sdhVc) != cAtSdhChannelTypeVc4_4c)
        return 3;

    return 0;
    }

static eAtRet Vc1xFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc, uint8 frameType)
    {
    uint32 address, regVal;
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 tugId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);

    address = ThaModuleStmMapMapLineCtrl(mThis(self), sdhChannel) +
              ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhChannel, AtSdhChannelSts1Get(sdhChannel), tugId, vtId);
    regVal  = mChannelHwRead(vc, address, cThaModuleMap);

    mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, sdhChannel), mModuleMapShift(self, MapFrmType, sdhChannel), frameType);
    mFieldIns(&regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), VcMapSignalType(sdhChannel));

    mChannelHwWrite(vc, address, regVal, cThaModuleMap);

    return cAtOk;
    }

static uint8 CepTug3Vc3Tug2MapFrameType(AtSdhChannel tug3vc3)
    {
    AtUnused(tug3vc3);
    return cCepFractionalVc1x;
    }

static uint8 CepVc4Tug3MapFrameType(AtSdhChannel vc4)
    {
    AtSdhChannel tug3 = AtSdhChannelSubChannelGet(vc4, 0);
    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        return HoVcHwFrameType(NULL, (AtSdhVc)vc4);

    return CepTug3Vc3Tug2MapFrameType(tug3);
    }

static uint8 CepVc3De3MapFrameType(AtSdhChannel vc3)
    {
    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)vc3))
        return cCepBasic;

    return cCepFractionalVc3;
    }

static uint8 CepMapFrameType(AtSdhVc self)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)self;
    uint8 mapType = AtSdhChannelMapTypeGet(sdhVc);

    if (mapType == cAtSdhVcMapTypeVc4Map3xTug3s)
        return CepVc4Tug3MapFrameType(sdhVc);

    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        return CepTug3Vc3Tug2MapFrameType(sdhVc);

    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        return CepVc3De3MapFrameType(sdhVc);

    return cCepBasic;
    }

static eAtRet AuVcUnchannelize(ThaModuleAbstractMap self, AtSdhVc vc, eBool unChannelized)
    {
    static const uint8 cVtgDontCare = 0;
    static const uint8 cVtDontCare = 0;
    uint8  sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint8 signalType = VcMapSignalType(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 frameType = (uint8)(unChannelized ? cCepBasic : CepMapFrameType(vc));

    AtUnused(self);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address = ThaModuleStmMapMapLineCtrl(mThis(self), sdhChannel) +
                         ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhChannel, (uint8)(stsId + sts_i), cVtgDontCare, cVtDontCare);
        uint32 regVal  = mChannelHwRead(vc, address, cThaModuleMap);

        mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, sdhChannel), mModuleMapShift(self, MapFrmType, sdhChannel), frameType);
        mFieldIns(&regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), signalType);
        mChannelHwWrite(vc, address, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet De3EncapConnectionEnable(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool enable)
    {
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 regAddr = mMethodsGet(moduleMap)->De3MapChnCtrl(moduleMap, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 regVal  = mChannelHwRead(de3, regAddr, cThaModuleMap);
    uint32 mask    = mFieldMask(moduleMap, MapTsEn);
    uint32 shift   = mFieldShift(moduleMap, MapTsEn);

    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
    mChannelHwWrite(de3, regAddr, regVal, cThaModuleMap);

    return cAtOk;
    }

static eBool De3EncapConnectionIsEnabled(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 regAddr = mMethodsGet(moduleMap)->De3MapChnCtrl(moduleMap, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 regVal  = mChannelHwRead(de3, regAddr, cThaModuleMap);
    uint32 mask    = mFieldMask(moduleMap, MapTsEn);

    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static eAtRet De1Init(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    uint8 timeslot_i;
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 numberOfTimeSlots = (uint32)ThaPdhDe1NumDs0Timeslots(de1);

    for (timeslot_i = 0; timeslot_i < numberOfTimeSlots; timeslot_i++)
        {
        uint32 offset = mMethodsGet(self)->Ds0DefaultOffset(self, (AtPdhDe1)de1, timeslot_i);
        mChannelHwWrite(de1, mMethodsGet(mapModule)->De1MapChnCtrl(mapModule, de1) + offset, 0x0, cThaModuleMap);
        }
        
    return cAtOk;    
    }

static eAtRet BindVc1xToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    return ThaModuleAbstractMapBindVc1xToChannel(self, sdhVc, (AtChannel)encapChannel, mMethodsGet(self)->EncapHwChannelType(self, encapChannel));
    }

static eAtRet BindVc1xToConcateGroup(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtConcateGroup concateGroup)
    {
    AtChannel concateChannel = mMethodsGet(self)->ConcateChannelForBinding(self, (AtChannel)sdhVc, concateGroup);
    return ThaModuleAbstractMapBindVc1xToChannel(self, sdhVc, concateChannel, 7);
    }

static eAtRet BindHoVcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    uint8 sts_i;
    eBool enable = mMethodsGet(self)->NeedEnableAfterBindEncapChannel(self, (AtChannel)encapChannel);
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);
    uint32 mask, shift;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = mMethodsGet(moduleMap)->VcMapChnCtrl(moduleMap, sdhVc) + mMethodsGet(self)->AuVcStsDefaultOffset(self, sdhVc, (uint8)(stsId + sts_i));
        uint32 regVal  = 0;

        if (enable)
            {
            mask  = mFieldMask(moduleMap, MapFirstTs);
            shift = mFieldShift(moduleMap, MapFirstTs);
            mFieldIns(&regVal, mask, shift, 0);

            mask  = mFieldMask(moduleMap, MapChnType);
            shift = mFieldShift(moduleMap, MapChnType);
            mFieldIns(&regVal, mask, shift, mMethodsGet(self)->EncapHwChannelType(self, encapChannel));

            mask  = mFieldMask(moduleMap, MapTsEn);
            shift = mFieldShift(moduleMap, MapTsEn);
            mFieldIns(&regVal, mask, shift, enable);

            mask  = mFieldMask(moduleMap, MapPWIdField);
            shift = mFieldShift(moduleMap, MapPWIdField);
            mFieldIns(&regVal, mask, shift, AtChannelHwIdGet((AtChannel)encapChannel));
            }

        mChannelHwWrite(sdhVc, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet BindTu3VcToEncap(ThaModuleAbstractMap self, AtSdhVc sdhVc, AtEncapChannel encapChannel)
    {
    return BindHoVcToEncap(self, sdhVc, encapChannel);
    }

static eAtRet HoVcSignalTypeSet(ThaModuleAbstractMap self, AtSdhVc sdhVc)
    {
    static const uint8 cVtgDontCare = 0;
    static const uint8 cVtDontCare = 0;
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), sdhChannel) +
                         ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhChannel, (uint8)(stsId + sts_i), cVtgDontCare, cVtDontCare);
        uint32 regVal  = mChannelHwRead(sdhVc, regAddr, cThaModuleMap);

        mFieldIns(&regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), HoVcHwSignalType(self, sdhVc));
        mFieldIns(&regVal, mModuleMapMask(self, MapFrmType, sdhChannel), mModuleMapShift(self, MapFrmType, sdhChannel), HoVcHwFrameType(self, sdhVc));
        mChannelHwWrite(sdhVc, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static uint32 MaxNumSts(ThaModuleMap self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    return ThaModuleOcnNumStsInOneSlice(ocnModule);
    }

static uint8 NumParts(ThaModuleMap self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModuleMap);
    }

static eAtRet PartDefaultSet(ThaModuleMap self, uint8 partId)
    {
    uint8 sts_i, vtg_i, vt_i;
    uint32 partOffset = PartOffset(self, partId);

    for (sts_i = 0; sts_i < MaxNumSts(self); sts_i++)
        {
        for (vtg_i = 0; vtg_i < 7; vtg_i++)
            {
            for (vt_i = 0; vt_i < 4; vt_i++)
                {
                uint32 offset;
                uint32 regAddr, regVal;

                offset = (uint32)(sts_i << 5) + (uint32)(vtg_i << 2) + vt_i + partOffset;
                regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), NULL) + offset;
                regVal = 0;
                mFieldIns(&regVal, mModuleMapMask(self, MapTimeSrcMast, NULL), mModuleMapShift(self, MapTimeSrcMast, NULL), 1);
                mFieldIns(&regVal, mModuleMapMask(self, MapTimeSrcId, NULL), mModuleMapShift(self, MapTimeSrcId, NULL), offset);
                mModuleHwWrite(self, regAddr, regVal);
                }
            }
        }

    return cAtOk;
    }

static eAtRet MapLineControlVcDefaultSet(ThaModuleMap self, AtSdhChannel vc)
    {
    uint8 sts_i;
    uint8 numSts = AtSdhChannelNumSts(vc);
    uint8 stsId  = AtSdhChannelSts1Get(vc);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePdh);
    uint32 regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), vc);

    if (ThaModulePdhInbandLoopcodeIsSupported(pdhModule) == cAtFalse)
        return cAtOk;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regOffset;
        uint32 regVal;
        eAtSdhChannelType channelType = AtSdhChannelTypeGet(vc);
        uint8 vtg_i = AtSdhChannelTug2Get(vc);
        uint8 vt_i = AtSdhChannelTu1xGet(vc);

        if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
            regOffset = ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), vc, (uint8)(stsId + sts_i), vtg_i, vt_i);
        else
            regOffset = ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), vc, (uint8)(stsId + sts_i), 0, 0);

        regVal = mChannelHwRead(vc, regAddr + regOffset, cThaModuleMap);
        mFieldIns(&regVal, mFieldMask(self, MapTmDs1Loopcode), mFieldShift(self, MapTmDs1Loopcode), 0);
        mChannelHwWrite(vc, regAddr + regOffset, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eAtRet MapLineControlDe3DefaultSet(ThaModuleMap self, AtPdhChannel de3)
    {
    uint32 regAddr, regVal, offset;
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet(de3);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePdh);

    if (ThaModulePdhInbandLoopcodeIsSupported(pdhModule) == cAtFalse)
        return cAtOk;

    if (sdhVc)
        offset = ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhVc, AtSdhChannelSts1Get(sdhVc), 0, 0);
    else
        offset = ThaModuleStmMapPdhDe3MapLineOffset(mThis(self), (AtPdhDe3)de3);

    regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), NULL) + offset;
    regVal  = mChannelHwRead(de3, regAddr, cThaModuleMap);
    mFieldIns(&regVal, mFieldMask(self, MapTmDs1Loopcode), mFieldShift(self, MapTmDs1Loopcode), 0);
    mChannelHwWrite(de3, regAddr, regVal, cThaModuleMap);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleStmMap self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts((ThaModuleMap)self); part_i++)
        ret |= PartDefaultSet((ThaModuleMap)self, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eAtRet VcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 mapChnCtrl = mMethodsGet((ThaModuleMap)self)->VcMapChnCtrl((ThaModuleMap)self, vcx);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 mask, shift;

    /* Need to disable hardware before reseting configuration */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = mapChnCtrl + mMethodsGet(self)->AuVcStsDefaultOffset(self, vcx, (uint8)(stsId + sts_i));
        uint32 regVal  = mChannelHwRead(vcx, regAddr, cThaModuleMap);
        mask  = mFieldMask(moduleMap, MapTsEn);
        shift = mFieldShift(moduleMap, MapTsEn);
        mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
        mChannelHwWrite(vcx, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eBool VcxEncapConnectionIsEnabled(ThaModuleAbstractMap self, AtSdhVc vcx)
    {
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 mapChnCtrl = mMethodsGet((ThaModuleMap)self)->VcMapChnCtrl((ThaModuleMap)self, vcx);
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    ThaModuleMap moduleMap = (ThaModuleMap)self;

    /* Need to disable hardware before reseting configuration */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regAddr = mapChnCtrl + mMethodsGet(self)->AuVcStsDefaultOffset(self, vcx, (uint8)(stsId + sts_i));
        uint32 regVal = mChannelHwRead(vcx, regAddr, cThaModuleMap);
        uint32 mask = mFieldMask(moduleMap, MapTsEn);

        if ((regVal & mask) == 0)
            return cAtFalse;
        }

    return (numSts != 0) ? cAtTrue : cAtFalse;
    }

static uint8 VtHwPldModeGet(eThaDatMapPldMd pldMd)
    {
    if (pldMd == cThaCfgMapPldVt15) return 4;
    if (pldMd == cThaCfgMapPldVt2)  return 5;
    if (pldMd == cThaCfgMapPldDs1)  return 0;
    if (pldMd == cThaCfgMapPldE1)   return 1;

    /* Default */
    return 0;
    }

static uint32 SdhChannelPartOffset(ThaModuleStmMap self, AtSdhChannel sdhChannel)
    {
    return PartOffset((ThaModuleMap)self, ThaModuleSdhPartOfChannel(sdhChannel));
    }

static uint32 SdhChannelMapLineOffset(ThaModuleStmMap self, AtSdhChannel sdhChannel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 slice, hwSts;
    ThaSdhChannelHwStsGet(sdhChannel, cThaModuleMap, stsId, &slice, &hwSts);

    return (uint32)(hwSts << 5) + (uint32)(vtgId << 2) + vtId + SdhChannelPartOffset(self, sdhChannel);
    }

static uint32 PdhDe2De1MapLineOffset(ThaModuleStmMap self, AtPdhDe1 de1)
    {
    uint8 partId = ThaPdhDe1PartId((ThaPdhDe1)de1);
    uint32 slice, hwIdInSlice, de2Id, de1Id;
    uint32 sliceOffset;
    AtSdhChannel sdhChannel = ThaPdhDe1ShortestVcGet((ThaPdhDe1)de1);
    ThaPdhDe1Ds0OffsetFactorForMap((ThaPdhDe1)de1, &slice, &hwIdInSlice, &de2Id, &de1Id);

    sliceOffset = ThaModuleAbstractMapSdhSliceOffset((ThaModuleAbstractMap)self, sdhChannel, (uint8)slice);
    return (hwIdInSlice * 32UL) + (de2Id * 4UL) + de1Id + sliceOffset +
            PartOffset((ThaModuleMap)self, partId);
    }

static uint32 PdhDe3De2De1MapLineOffset(ThaModuleStmMap self, AtPdhDe3 de3, uint32 de2Id, uint32 de1Id)
    {
    uint8 slice, hwIdInSlice;
    uint32 sliceOffset;

    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cThaModuleMap, &slice, &hwIdInSlice);
    sliceOffset = ThaModuleAbstractMapSdhSliceOffset((ThaModuleAbstractMap)self, (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3), slice);

    return (hwIdInSlice * 32UL) + (de2Id * 4UL) + de1Id + sliceOffset;
    }

static uint32 PdhDe3MapLineOffset(ThaModuleStmMap self, AtPdhDe3 de3)
    {
    uint8 slice, hwIdInSlice;
    uint32 sliceOffset;

    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cThaModuleMap, &slice, &hwIdInSlice);
    sliceOffset = ThaModuleAbstractMapSdhSliceOffset((ThaModuleAbstractMap)self, (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de3), slice);

    return (hwIdInSlice * 32UL) + sliceOffset;
    }

static uint32 MapLineCtrl(ThaModuleStmMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaRegMapLineCtrl;
    }

static eAtRet BindDe3ToPseudowire(ThaModuleAbstractMap self, AtPdhDe3 de3, AtPw pw)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = mMethodsGet(mapModule)->De3MapChnCtrl(mapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 regVal = 0;
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 mask, shift;
    if (pw)
        {
        mask  = mFieldMask(moduleMap, MapFirstTs);
        shift = mFieldShift(moduleMap, MapFirstTs);
        mFieldIns(&regVal, mask  , shift, 0);

        mask  = mFieldMask(moduleMap, MapTsEn);
        shift = mFieldShift(moduleMap, MapTsEn);
        mFieldIns(&regVal, mask  , shift, mBoolToBin(mMethodsGet(self)->NeedEnableAfterBindToPw(self, pw)));

        mask  = mFieldMask(moduleMap, MapChnType);
        shift = mFieldShift(moduleMap, MapChnType);
        mFieldIns(&regVal, mask  , shift, mMethodsGet(self)->PwHwChannelType(self, pw));

        mask  = mFieldMask(moduleMap, MapPWIdField);
        shift = mFieldShift(moduleMap, MapPWIdField);
        mFieldIns(&regVal, mask  , shift, AtChannelHwIdGet((AtChannel)pw));
        }

    mChannelHwWrite(de3, address, regVal, cThaModuleMap);

    return cAtOk;
    }

static eBool SdhChannelIsVcx(AtSdhChannel sdhChannel)
    {
    if (AtSdhChannelIsHoVc(sdhChannel))
        return cAtTrue;

    if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc3)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SdhChannelIsVc1x(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    if ((channelType == cAtSdhChannelTypeVc12)    ||
        (channelType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

static void VtMapLineDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 address)
    {
    uint8 sts = AtSdhChannelSts1Get(sdhChannel);
    uint8 vtg = AtSdhChannelTug2Get(sdhChannel);
    uint8 tu  = AtSdhChannelTu1xGet(sdhChannel);

    ThaDeviceRegNameDisplay(regName);
    address = address + ThaModuleStmMapSdhChannelMapLineOffset((ThaModuleStmMap)self, sdhChannel, sts, vtg, tu);
    ThaDeviceChannelRegValueDisplay((AtChannel)sdhChannel, address, cThaModuleMap);
    }

static void StsMultiMapLineDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address = regBaseAddress + ThaModuleStmMapSdhChannelMapLineOffset((ThaModuleStmMap)self, sdhChannel, (uint8)(startSts + sts_i), 0, 0);
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleMap);
        }
    }

static void VtMapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    uint16 numTimeslots = ThaModuleStmMapDemapNumTimeSlotInVc1x((AtSdhVc)sdhChannel);
    uint16 slot_i;

    /* There is product not support this register */
    if (regBaseAddress == cBit31_0)
        return;

    ThaDeviceRegNameDisplay(regName);
    for (slot_i = 0; slot_i < numTimeslots; slot_i++)
        {
        uint32 address = regBaseAddress + mMethodsGet(self)->Vc1xDefaultOffset(self, (AtSdhVc)sdhChannel) + slot_i;
        ThaDeviceChannelRegValueDisplay((AtChannel)sdhChannel, address, cThaModuleMap);
        }
    }

static void StsMultiMapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    /* There is product not support this register */
    if (regBaseAddress == cBit31_0)
        return;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address = regBaseAddress + mMethodsGet(self)->AuVcStsDefaultOffset(self, (AtSdhVc)sdhChannel, (uint8)(startSts + sts_i));
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleMap);
        }
    }

static void SdhChannelRegsShow(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    uint32 address;
    eBool isVcx  = SdhChannelIsVcx(sdhChannel);
    eBool isVc1x = SdhChannelIsVc1x(sdhChannel);

    if (isVcx)
        {
        address = mMethodsGet((ThaModuleStmMap)self)->MapLineCtrl((ThaModuleStmMap)self, sdhChannel);
        StsMultiMapLineDisplay(self, sdhChannel, "Map Line Control", address);

        address = mMethodsGet((ThaModuleMap)self)->VcMapChnCtrl((ThaModuleMap)self, (AtSdhVc)sdhChannel);
        StsMultiMapChannelDisplay(self, sdhChannel, "Map Channel Control", address);
        return;
        }

    if (isVc1x)
        {
        address = mMethodsGet((ThaModuleStmMap)self)->MapLineCtrl((ThaModuleStmMap)self, sdhChannel);
        VtMapLineDisplay(self, sdhChannel, "Map Line Control", address);

        address = mMethodsGet((ThaModuleMap)self)->VcMapChnCtrl((ThaModuleMap)self, (AtSdhVc)sdhChannel);
        VtMapChannelDisplay(self, sdhChannel, "Map Channel Control", address);
        }
    }

static void De3RegShow(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = mMethodsGet(mapModule)->De3MapChnCtrl(mapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 offset;

    ThaDeviceRegNameDisplay("Map Channel Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cThaModuleMap);

    offset = ThaModuleStmMapPdhDe3MapLineOffset(mThis(self), de3);
    address = ThaModuleStmMapMapLineCtrl(mThis(self), NULL) + offset;

    ThaDeviceRegNameDisplay("Map Line Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cThaModuleMap);
    }

static void De1RegShow(ThaModuleAbstractMap self, AtPdhDe1 de1)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 numTimeslots = ThaModuleAbstractMapDe1NumDs0s(self, de1);
    uint16 slot_i;
    uint32 address = ThaModuleStmMapMapLineCtrl(mThis(self), NULL) + De1MapLineOffset(self, de1);

    ThaDeviceRegNameDisplay("Map Line Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de1, address, cThaModuleMap);

    ThaDeviceRegNameDisplay("Map Channel Control");
    address = mMethodsGet(mapModule)->De1MapChnCtrl(mapModule, de1);
    for (slot_i = 0; slot_i < numTimeslots; slot_i++)
        {
        uint32 offset = ThaStmMapDemapDs0DefaultOffset(self, de1, (uint8)slot_i);
        ThaDeviceChannelRegValueDisplay((AtChannel)de1, address + offset, cThaModuleMap);
        }
    }

static void PdhChannelRegsShow(ThaModuleAbstractMap self, AtPdhChannel pdhChannel)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

    if (sdhVc)
        {
        SdhChannelRegsShow(self, sdhVc);
        return;
        }

    if ((channelType == cAtPdhChannelTypeE3) || (channelType == cAtPdhChannelTypeDs3))
        De3RegShow(self, (AtPdhDe3)pdhChannel);

    else if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        De1RegShow(self, (AtPdhDe1)pdhChannel);
    }

static void Vc4_4cConfigurationClear(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 stsId)
    {
    static const uint8 cVtgDontCare = 0;
    static const uint8 cVtDontCare = 0;
    uint8 sigType;

    uint32 offset = ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), sdhChannel, stsId, cVtgDontCare, cVtDontCare);
    uint32 regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), sdhChannel) + offset;
    uint32 regVal  = mChannelHwRead(sdhChannel, regAddr, cThaModuleMap);

    mFieldGet(regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), uint8, &sigType);
    if (sigType == 0xA)
        mFieldIns(&regVal, mModuleMapMask(self, MapSigType, sdhChannel), mModuleMapShift(self, MapSigType, sdhChannel), 0x9);

    mChannelHwWrite(sdhChannel, regAddr, regVal, cThaModuleMap);
    }

static uint32 De3BoundPwHwIdGet(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;
    uint32 address = mMethodsGet(mapModule)->De3MapChnCtrl(mapModule, de3) + mMethodsGet(self)->De3DefaultOffset(self, de3);
    uint32 regVal = mChannelHwRead(de3, address, cThaModuleMap);
    ThaModuleMap moduleMap = (ThaModuleMap)self;
    uint32 pwHwIdMask = mFieldMask(moduleMap, MapPWIdField);
    uint32 pwHwIdShift = mFieldShift(moduleMap, MapPWIdField);

    return mRegField(regVal, pwHwId);
    }

static uint8 EncapHwChannelType(ThaModuleAbstractMap self, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(encapChannel);
    return 5;
    }

static eAtRet De3Channelize(ThaModuleAbstractMap self, AtPdhDe3 de3, eBool channelized)
    {
    eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de3);

    if (channelized)
        return De3ChannelizedFrameTypeSet(self, de3, frameType);

    return De3UnChannelizedFrameTypeSet(self, de3, AtPdhDe3UnChannelizedFrameType(frameType));
    }

static eAtRet Tug2ChannelizeRestore(ThaModuleAbstractMap self, AtSdhChannel tug2)
    {
    AtSdhChannel tu1x = NULL;
    AtSdhChannel vc1x = 0;

    if (tug2)
        tu1x = AtSdhChannelSubChannelGet(tug2, 0);

    if (tu1x)
        vc1x = AtSdhChannelSubChannelGet(tu1x, 0);

    if (vc1x == NULL)
        return cAtOk;

    if (AtSdhChannelMapTypeGet(vc1x) == cAtSdhVcMapTypeVc1xMapDe1)
        {
        AtPdhDe1 de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc1x);
        return ThaModuleAbstractMapDe1FrmModeSet(self, de1, AtPdhChannelFrameTypeGet((AtPdhChannel)de1));
        }

    return ThaModuleAbstractMapVc1xFrameModeSet(self, (AtSdhVc)vc1x, cMapFrameTypeCepBasic);
    }

static eAtRet Vc3ChannelizeRestore(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)vc);

    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        {
        AtSdhChannel tug2 = AtSdhChannelSubChannelGet((AtSdhChannel)vc, 0);
        return Tug2ChannelizeRestore(self, tug2);
        }

    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        {
        AtPdhDe3 de3 = (AtPdhDe3)AtSdhChannelMapChannelGet((AtSdhChannel)vc);
        return ThaModuleAbstractMapDe3FrameModeSet(self, de3, AtPdhChannelFrameTypeGet((AtPdhChannel)de3));
        }

    /* C3 mapping */
    if (mapType == cAtSdhVcMapTypeVc3MapC3)
        return ThaModuleAbstractMapHoVcSignalTypeSet(self, vc);

    return cAtOk;
    }

static eAtRet Vc4ChannelizeRestore(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    uint8 numTug3s = AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)vc);
    uint8 tug3_i;
    eAtRet ret = cAtOk;

    for (tug3_i = 0; tug3_i < numTug3s; tug3_i++)
        {
        AtSdhChannel tug3 = AtSdhChannelSubChannelGet((AtSdhChannel)vc, tug3_i);
        uint8 mapType;

        if (tug3 == NULL)
            continue;

        mapType = AtSdhChannelMapTypeGet(tug3);
        if (mapType == cAtSdhTugMapTypeTug3Map7xTug2s)
            {
            AtSdhChannel tug2 = AtSdhChannelSubChannelGet(tug3, 0);
            ret |= Tug2ChannelizeRestore(self, tug2);
            continue;
            }

        if (mapType == cAtSdhTugMapTypeTug3MapVc3)
            {
            AtSdhChannel tu3 = AtSdhChannelSubChannelGet(tug3, 0);
            AtSdhChannel vc3 = AtSdhChannelSubChannelGet(tu3, 0);
            ret |= Vc3ChannelizeRestore(self, (AtSdhVc)vc3);
            continue;
            }
        }

    return ret;
    }

static eAtRet AuVcChannelizeRestore(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    eAtRet ret = cAtOk;
    eAtSdhChannelType vcType = AtSdhChannelTypeGet((AtSdhChannel)vc);

    ret |= AuVcUnchannelize(self, vc, cAtFalse);

    if (vcType == cAtSdhChannelTypeVc3)
        ret |= Vc3ChannelizeRestore(self, vc);
    if (vcType == cAtSdhChannelTypeVc4)
        ret |= Vc4ChannelizeRestore(self, vc);

    return ret;
    }

static eAtRet AuVcFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    return ThaModuleAbstractMapAuVcUnchannelize(self, vc, cAtFalse);
    }

static void MethodsInit(AtModule self)
    {
    ThaModuleStmMap module = (ThaModuleStmMap)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SdhChannelMapLineOffset);
        mMethodOverride(m_methods, PdhDe2De1MapLineOffset);
        mMethodOverride(m_methods, PdhDe3MapLineOffset);
        mMethodOverride(m_methods, PdhDe3De2De1MapLineOffset);
        mMethodOverride(m_methods, MapLineCtrl);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, Vc4_4cConfigurationClear);
        }

    mMethodsSet(module, &m_methods);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, De1FrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, Ds0DefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe3ToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3FrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3EncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3EncapConnectionIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1Init);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1LoopcodeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1LoopcodeGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleCodeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleCodeGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De1IdleIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindVc1xToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindHoVcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindTu3VcToEncap);
        mMethodOverride(m_ThaModuleAbstractMapOverride, HoVcSignalTypeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcFrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, Vc1xFrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionIsEnabled);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcStsDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe3ToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhChannelRegsShow);
        mMethodOverride(m_ThaModuleAbstractMapOverride, PdhChannelRegsShow);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3BoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcBoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, Vc1xBoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, EncapHwChannelType);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindVc1xToConcateGroup);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindDe3ToConcateGroup);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3Channelize);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcUnchannelize);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcChannelizeRestore);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleMapOverride));

        /* Override bit fields */
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapPWIdField)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapFrmType)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapSigType)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTimeSrcMast)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTimeSrcId)

        /* Override register address */
        mMethodOverride(m_ThaModuleMapOverride, De1MapChnCtrl);
        mMethodOverride(m_ThaModuleMapOverride, MapLineControlVcDefaultSet);
        mMethodOverride(m_ThaModuleMapOverride, MapLineControlDe3DefaultSet);
        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleMap(self);
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    }

static AtHal HalForSts(ThaModuleMap self, uint8 stsId)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint8 coreId = mMethodsGet(mapModule)->CoreIdOfSts(mapModule, stsId);

    return AtDeviceIpCoreHalGet(device, coreId);
    }

static uint32 ChannelRead(ThaModuleMap self, uint8 stsId, uint32 address)
    {
    return mModuleHwReadByHal(self, address, HalForSts(self, stsId));
    }

static void ChannelWrite(ThaModuleMap self, uint8 stsId, uint32 address, uint32 value)
    {
    mModuleHwWriteByHal(self, address, value, HalForSts(self, stsId));
    }

static eThaDatMapPldMd VtPldModeFromHwGet(uint8 value)
    {
    if (value == 4) return cThaCfgMapPldVt15;
    if (value == 5) return cThaCfgMapPldVt2;
    if (value == 0) return cThaCfgMapPldDs1;
    if (value == 1) return cThaCfgMapPldE1;

    /* Default */
    return cThaCfgMapPldE1;
    }

AtModule ThaModuleStmMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModuleStmMap));

    /* Super construction */
    if (ThaModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleStmMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModuleStmMap));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleStmMapObjectInit(newModule, device);
    }

uint32 ThaModuleStmMapDemapDe3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    AtSdhVc sdhVc = AtPdhChannelVcInternalGet((AtPdhChannel)de3);
    return mMethodsGet(self)->AuVcStsDefaultOffset(self, sdhVc, AtSdhChannelSts1Get((AtSdhChannel)sdhVc));
    }

eAtRet ThaMapVtPldMdSet(ThaModuleMap self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId, eThaDatMapPldMd pldMd)
    {
    uint32 regAddr, regVal;
    uint8 hwPldMd, hwStsId, slice;

    ThaSdhChannelHwStsGet(channel, cThaModuleMap, stsId, &slice, &hwStsId);

    regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), channel) +
              ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), channel, stsId, vtgId, vtId);
    regVal  = ChannelRead(self, hwStsId, regAddr);
    hwPldMd = VtHwPldModeGet(pldMd);
    mFieldIns(&regVal, mModuleMapMask(self, MapSigType, channel), mModuleMapShift(self, MapSigType, channel), hwPldMd);
    ChannelWrite(self, hwStsId, regAddr, regVal);

    return cAtOk;
    }

eThaDatMapPldMd ThaMapVtPldMdGet(ThaModuleMap self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint32 regAddr, regVal;
    uint8 hwStsId, slice, value;

    ThaSdhChannelHwStsGet(channel, cThaModuleMap, stsId, &slice, &hwStsId);

    regAddr = ThaModuleStmMapMapLineCtrl(mThis(self), channel) +
              ThaModuleStmMapSdhChannelMapLineOffset(mThis(self), channel, stsId, vtgId, vtId);
    regVal  = ChannelRead(self, hwStsId, regAddr);
    mFieldGet(regVal, mModuleMapMask(self, MapSigType, channel), mModuleMapShift(self, MapSigType, channel), uint8, &value);

    return VtPldModeFromHwGet(value);
    }

eAtRet ThaMapVtgPldMdSet(ThaModuleMap self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, eThaDatMapPldMd pldMd)
    {
    uint8 numVt = 0, vtId;
    eAtRet ret = cAtOk;

    /* Calculate number of VTs base on payload mode */
    if ((pldMd == cThaCfgMapPldVt15) || (pldMd == cThaCfgMapPldDs1))
        numVt = 4;
    if ((pldMd == cThaCfgMapPldVt2) || (pldMd == cThaCfgMapPldE1))
        numVt = 3;

    for (vtId = 0; vtId < numVt; vtId++)
        ret |= ThaMapVtPldMdSet(self, channel, stsId, vtgId, vtId, pldMd);

    return ret;
    }

void ThaModuleStmMapVc4_4cConfigurationClear(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 stsId)
    {
    if (self)
        mMethodsGet(mThis(self))->Vc4_4cConfigurationClear(self, sdhChannel, stsId);
    }

uint32 ThaModuleMapMapTimeSrcMastMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mModuleMapMask(self, MapTimeSrcMast, sdhChannel);
    return 0;
    }

uint32 ThaModuleMapMapTimeSrcMastShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mModuleMapShift(self, MapTimeSrcMast, sdhChannel);
    return 0;
    }

uint32 ThaModuleMapMapTimeSrcIdMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mModuleMapMask(self, MapTimeSrcId, sdhChannel);
    return 0;
    }

uint32 ThaModuleMapMapTimeSrcIdShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mModuleMapShift(self, MapTimeSrcId, sdhChannel);
    return 0;
    }

uint32 ThaModuleStmMapPdhDe1MapLineOffset(ThaModuleStmMap self, AtPdhDe1 de1)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)de1);
    return ThaModuleStmMapSdhChannelMapLineOffset(self, vc, AtSdhChannelSts1Get(vc), AtSdhChannelTug2Get(vc), AtSdhChannelTu1xGet(vc));
    }

uint32 ThaModuleStmMapPdhDe2De1MapLineOffset(ThaModuleStmMap self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->PdhDe2De1MapLineOffset(self, de1);
    return cBit31_0;
    }

uint32 ThaModuleStmMapPdhDe3MapLineOffset(ThaModuleStmMap self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->PdhDe3MapLineOffset(self, de3);
    return cBit31_0;
    }

uint32 ThaModuleStmMapPdhDe3De2De1MapLineOffset(ThaModuleStmMap self, AtPdhDe3 de3, uint32 de2Id, uint32 de1Id)
    {
    if (self)
        return mMethodsGet(self)->PdhDe3De2De1MapLineOffset(self, de3, de2Id, de1Id);
    return cBit31_0;
    }

uint32 ThaModuleStmMapSdhChannelMapLineOffset(ThaModuleStmMap self, AtSdhChannel sdhChannel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    if (self)
        return mMethodsGet(self)->SdhChannelMapLineOffset(self, sdhChannel, stsId, vtgId, vtId);
    return 0;
    }

uint32 ThaModuleStmMapMapLineCtrl(ThaModuleStmMap self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mMethodsGet(self)->MapLineCtrl(self, sdhChannel);
    return 0;
    }

uint32 ThaStmMapDemapDs0DefaultOffset(ThaModuleAbstractMap self, AtPdhDe1 de1, uint8 timeslotId)
    {
    uint32 sliceOffset, slice, hwIdInSlice, hwVtg, hwVt;
    AtSdhChannel sdhChannel = ThaPdhDe1ShortestVcGet((ThaPdhDe1)de1);
    eThaMapChannelType channelType = AtPdhDe1IsE1(de1) ? cThaMapChannelE1 : cThaMapChannelDs1;

    ThaPdhDe1Ds0OffsetFactorForMap((ThaPdhDe1)de1, &slice, &hwIdInSlice, &hwVtg, &hwVt);
    sliceOffset = ThaModuleAbstractMapSdhSliceOffset(self, sdhChannel, (uint8)slice);
    return ThaModuleAbstractMapChannelCtrlAddressFormula(self, channelType, hwIdInSlice, hwVtg, hwVt, timeslotId) + sliceOffset;
    }

eAtRet ThaModuleStmMapBindAuVcToPseudowire(ThaModuleStmMap self, AtSdhVc vc, AtPw pw)
    {
    if (self)
        return BindAuVcToPseudowire((ThaModuleAbstractMap)self, vc, pw);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleStmMapAuVcFrameModeSet(ThaModuleStmMap self, AtSdhVc vc)
    {
    if (self)
        return AuVcFrameModeSet((ThaModuleAbstractMap)self, vc);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleStmMapVcxEncapConnectionEnable(ThaModuleStmMap self, AtSdhVc vcx, eBool enable)
    {
    if (self)
        return VcxEncapConnectionEnable((ThaModuleAbstractMap)self, vcx, enable);
    return cAtErrorNullPointer;
    }

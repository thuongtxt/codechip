/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaModuleEth.h
 *
 * Created Date: Sep 24, 2012
 *
 * Description : ETH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEETH_H_
#define _THAMODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h" /* Super class */
#include "AtSerdesController.h"
#include "oamflow/ThaOamFlowManager.h"
#include "controller/ThaEthFlowHeaderProvider.h"
#include "../physical/ThaEthSerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleEth * ThaModuleEth;

typedef struct tThaEthFlowController   * ThaEthFlowController;
typedef struct tThaMpEthFlowController * ThaMpEthFlowController;
typedef struct tThaOamFlowController   * ThaOamFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete modules */
AtModuleEth ThaModuleEthNew(AtDevice device);
AtModuleEth ThaModuleEthStmPppNew(AtDevice device);
AtModuleEth ThaModuleEthPwNew(AtDevice device);
AtModuleEth ThaModuleEthPwV2New(AtDevice device);
AtModuleEth ThaModuleEthPppNew(AtDevice device);

eAtRet ThaModuleEthAllPortMacsActivate(AtModuleEth self, eBool activate);

/* Part management */
uint32 ThaModuleEthPartOffset(ThaModuleEth self, uint8 partId);
uint8 ThaModuleEthPartOfPort(ThaModuleEth self, AtEthPort port);
uint8 ThaModuleEthLocalEthPortId(ThaModuleEth self, AtEthPort port);
uint32 ThaModuleEthNumMpFlowPerPart(ThaModuleEth self);
ThaEthFlowHeaderProvider ThaModuleEthFlowHeaderProviderGet(ThaModuleEth self);

/* Flow management */
uint32 ThaModuleEthMaxNumMpFlows(ThaModuleEth self);
eBool ThaModuleEthMpFlowIsValid(ThaModuleEth self, uint32 flowId);
uint32 ThaModuleEthFreeMpFlowGet(ThaModuleEth self);
eAtRet ThaModuleEthMpFlowUse(ThaModuleEth self, uint32 flowId);
eAtRet ThaModuleEthMpFlowUnuse(ThaModuleEth self, uint32 flowId);
AtEthFlow ThaModuleEthOamFlowCreate(AtModuleEth self, AtHdlcLink link);
eAtRet ThaModuleEthOamFlowDelete(AtModuleEth self, AtEthFlow oamFlow);
ThaOamFlowManager ThaModuleEthOamFlowManager(ThaModuleEth self);
AtEthFlow ThaModuleEthMpOamFlowCreate(AtModuleEth self, AtHdlcBundle bundle);
eAtRet ThaModuleEthMpOamFlowDelete(AtModuleEth self, AtEthFlow oamFlow);

uint16 ThaModuleEthSVlanTpid(ThaModuleEth self, AtEthPort port);
uint16 ThaModuleEthCVlanTpid(ThaModuleEth self);

/* VLAN management */
eAtRet ThaModuleEthIngressVlanUse(ThaModuleEth self, AtEthFlow flow, uint32 vlanId);
eAtRet ThaModuleEthIngressVlanUnUse(ThaModuleEth self, AtEthFlow flow, uint32 vlanId);
eBool  ThaModuleEthIngressVlanIsInused(ThaModuleEth self, AtEthFlow flow, uint32 vlanId);

/* Eth port management */
eBool ThaModuleEthPortIsUsed(ThaModuleEth self, uint8 portId);
eBool ThaModuleEthPortPllCanBeChecked(ThaModuleEth self, uint8 portId);
eBool ThaModuleEthPortHasSourceMac(ThaModuleEth self, uint8 portId);
eBool ThaModuleEthPortHasIpAddress(ThaModuleEth self, uint8 portId);
eBool ThaModuleEthPortShouldByPassFcs(ThaModuleEth self, uint8 portId);
eBool ThaModuleEthPort10GbSupported(ThaModuleEth self);
eBool ThaModuleEthUsePortSourceMacForAllFlows(ThaModuleEth self);
eBool ThaModuleEthHas10GDiscardCounter(ThaModuleEth self);
eBool ThaModuleEthPortIsInternalMac(ThaModuleEth self, uint8 portId);

/* ETH port designated for DCC transport */
AtEthPort ThaModuleEthDccEthPortGet(ThaModuleEth self);
eAtRet ThaModuleEthTxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength);
uint32 ThaModuleEthTxDccPacketMaxLengthGet(ThaModuleEth self);
eAtRet ThaModuleEthRxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength);
uint32 ThaModuleEthRxDccPacketMaxLengthGet(ThaModuleEth self);
eBool ThaModuleEthNeedMultipleLanes(ThaModuleEth self);

/* SERDES */
AtSerdesController ThaModuleEthPortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort);
AtPrbsEngine ThaModuleEthPortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId);
ThaEthSerdesManager ThaModuleEthSerdesManager(ThaModuleEth self);
uint16 ThaModuleEthQsgmiiSerdesNumLanes(ThaModuleEth self, uint8 serdesId);
eBool ThaModuleEthQsgmiiSerdesLaneIsUsed(ThaModuleEth self, uint8 serdesId, uint16 laneId);
uint32 ThaModuleEthQsgmiiLaneBaseAddress(ThaModuleEth self, uint8 serdesId, uint16 laneId);
uint32 ThaModuleEthQsgmiiSerdesLaneRegisterWithOffset(ThaModuleEth self, uint8 serdesId, uint16 laneId, uint32 offset);
uint32 ThaModuleEthQsgmiiLaneCurrentStatusRegister(ThaModuleEth self, uint8 serdesId, uint16 laneId);
uint32 ThaModuleEthQsgmiiLaneStickyStatusRegister(ThaModuleEth self, uint8 serdesId, uint16 laneId);
eBool ThaModuleEthSerdesApsSupported(ThaModuleEth self);
eBool ThaModuleEthHasSerdesGroup(ThaModuleEth self);

/* APS */
eBool ThaModuleEthApsIsSupported(ThaModuleEth self);
eBool ThaModuleEthFullSerdesPrbsSupported(ThaModuleEth self);
eBool ThaModuleEthCanEnableTxSerdes(ThaModuleEth self);

/* Interrupt */
eBool ThaModuleEthInterruptIsSupported(ThaModuleEth self);

/* For registers */
uint32 ThaModuleEthMacBaseAddress(ThaModuleEth self, AtEthPort port);
uint32 ThaModuleEthDiagBaseAddress(ThaModuleEth self, uint32 serdesId);

/* Flow controller management */
ThaEthFlowController ThaModuleEthPppFlowControllerGet(ThaModuleEth self);
ThaEthFlowController ThaModuleEthOamFlowControllerGet(ThaModuleEth self);
ThaEthFlowController ThaModuleEthCiscoHdlcFlowControllerGet(ThaModuleEth self);
ThaEthFlowController ThaModuleEthMpFlowControllerGet(ThaModuleEth self);
ThaEthFlowController ThaModuleEthFrFlowControllerGet(ThaModuleEth self);
ThaEthFlowController ThaModuleEthEncapFlowControllerGet(ThaModuleEth self);
ThaEthFlowController ThaModuleEthPwFlowControllerGet(ThaModuleEth self);

/* Group management */
eAtRet Tha60210051ModuleEthXfiGroupSet(AtModuleEth self, uint32 groupId);
uint32 Tha60210051ModuleEthXfiGroupGet(AtModuleEth self);
eAtRet Tha60210051ModuleEthExtraXfiGroupSet(AtModuleEth self, uint32 groupId);
uint32 Tha60210051ModuleEthExtraXfiGroupGet(AtModuleEth self);

/* For debugging */
void ThaModuleEthPortRegShow(AtEthPort port);
eAtRet ThaModuleEthDebugCountersModuleSet(ThaModuleEth self, uint32 module);
uint32 ThaModuleEthDebugCountersModuleGet(ThaModuleEth self);
eBool ThaModuleEthFsmIsSupported(ThaModuleEth self);
eAtRet ThaModuleEthTxFsmPinEnable(ThaModuleEth self, eBool enabled);
eBool ThaModuleEthTxFsmPinIsEnabled(ThaModuleEth self);
eBool ThaModuleEthTxFsmPinStatusGet(ThaModuleEth self);
eAtRet ThaModuleEthRxFsmPinEnable(ThaModuleEth self, eBool enabled);
eBool ThaModuleEthRxFsmPinIsEnabled(ThaModuleEth self);
eBool ThaModuleEthRxFsmPinStatusGet(ThaModuleEth self);
void ThaModuleEthEthFlowRegsShow(AtEthFlow flow);

/* Product concretes */
AtModuleEth Tha60070013ModuleEthNew(AtDevice device);
AtModuleEth Tha60031071ModuleEthNew(AtDevice device);
AtModuleEth Tha60070013ModuleEthNew(AtDevice device);
AtModuleEth Tha60030111ModuleEthNew(AtDevice device);
AtModuleEth Tha60001031ModuleEthNew(AtDevice device);
AtModuleEth Tha60030011ModuleEthNew(AtDevice device);
AtModuleEth Tha60031021ModuleEthNew(AtDevice device);
AtModuleEth Tha60070041ModuleEthNew(AtDevice device);
AtModuleEth Tha60091023ModuleEthNew(AtDevice device);
AtModuleEth Tha60031031EpModuleEthNew(AtDevice device);
AtModuleEth Tha60031031ModuleEthNew(AtDevice device);
AtModuleEth Tha60031131ModuleEthNew(AtDevice device);
AtModuleEth Tha60150011ModuleEthNew(AtDevice device);
AtModuleEth Tha60035021ModuleEthNew(AtDevice device);
AtModuleEth Tha60070061ModuleEthNew(AtDevice device);
AtModuleEth Tha60200011ModuleEthNew(AtDevice device);
AtModuleEth Tha60071011ModuleEthNew(AtDevice device);
AtModuleEth Tha60071021ModuleEthNew(AtDevice device);
AtModuleEth Tha60030051ModuleEthNew(AtDevice device);
AtModuleEth Tha60031032ModuleEthNew(AtDevice device);
AtModuleEth Tha60031033ModuleEthNew(AtDevice device);
AtModuleEth Tha60031035ModuleEthNew(AtDevice device);
AtModuleEth Tha60071032ModuleEthNew(AtDevice device);
AtModuleEth Tha60091132ModuleEthNew(AtDevice device);
AtModuleEth Tha60091135ModuleEthNew(AtDevice device);
AtModuleEth Tha65031032ModuleEthNew(AtDevice device);
AtModuleEth Tha60030022ModuleEthNew(AtDevice device);
AtModuleEth Tha60060011ModuleEthNew(AtDevice device);
AtModuleEth Tha60030101ModuleEthNew(AtDevice device);
AtModuleEth Tha61150011ModuleEthNew(AtDevice device);
AtModuleEth Tha60210031ModuleEthNew(AtDevice device);
AtModuleEth Tha60210011ModuleEthNew(AtDevice device);
AtModuleEth Tha61210011ModuleEthNew(AtDevice device);
AtModuleEth Tha60290011ModuleEthNew(AtDevice device);
AtModuleEth Tha62290011ModuleEthNew(AtDevice device);
AtModuleEth ThaStmPwProductModuleEthNew(AtDevice device);
AtModuleEth ThaPdhPwProductModuleEthNew(AtDevice device);
AtModuleEth Tha60210021ModuleEthNew(AtDevice device);
AtModuleEth Tha61210031ModuleEthNew(AtDevice device);
AtModuleEth Tha60210012ModuleEthNew(AtDevice device);
AtModuleEth Tha6A000010ModuleEthNew(AtDevice device);
AtModuleEth Tha6A033111ModuleEthNew(AtDevice device);
AtModuleEth Tha60210051ModuleEthNew(AtDevice device);
AtModuleEth Tha6a210031ModuleEthNew(AtDevice device);
AtModuleEth Tha60210061ModuleEthNew(AtDevice device);
AtModuleEth Tha60290022ModuleEthNew(AtDevice device);
AtModuleEth Tha6A290011ModuleEthNew(AtDevice device);
AtModuleEth Tha6A290021ModuleEthNew(AtDevice device);
AtModuleEth Tha6A290022ModuleEthNew(AtDevice device);
AtModuleEth Tha60290051ModuleEthNew(AtDevice device);
AtModuleEth Tha60290061ModuleEthNew(AtDevice device);
AtModuleEth Tha60290081ModuleEthNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEETH_H_ */

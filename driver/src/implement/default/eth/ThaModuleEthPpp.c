/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaModuleEthPpp.c
 *
 * Created Date: Apr 4, 2015
 *
 * Description : ETH for PPP product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEncap.h"
#include "ThaModuleEthInternal.h"
#include "ThaEthFlow.h"
#include "controller/ThaEthFlowHeaderProvider.h"
#include "oamflow/ThaOamFlowManager.h"
#include "../mpig/ThaModuleMpig.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthFlow FlowCreate(AtModuleEth self, uint16 flowId)
    {
    return ThaModuleEthPppFlowCreate(self, flowId);
    }

static uint16 MaxFlowsGet(AtModuleEth self)
    {
    return ThaModuleEthMaxFlowsGet((ThaModuleEth)self);
    }

static ThaEthFlowControllerFactory FlowControllerFactory(ThaModuleEth self)
    {
    AtUnused(self);
    return ThaEthFlowControllerFactoryDefault();
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return ThaEthFlowHeaderProviderNew((AtModuleEth)self);
    }

static ThaOamFlowManager OamFlowManagerObjectCreate(ThaModuleEth self)
    {
    return ThaOamFlowManagerDefaultNew((AtModuleEth)self);
    }

static eAtModuleEthRet ISISMacSet(AtModuleEth self, const uint8 *mac)
    {
    /* Let module MPIG do this because register belong to PPP module */
    ThaModuleMpig moduleMpig = (ThaModuleMpig)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleMpig);
    if (moduleMpig)
        return ThaModuleMpigISISMacSet(moduleMpig, mac);

    return cAtError;
    }

static eAtModuleEthRet ISISMacGet(AtModuleEth self, uint8 *mac)
    {
    /* Let module PPP do this because register belong to PPP module */
    ThaModuleMpig moduleMpig = (ThaModuleMpig)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleMpig);
    if (moduleMpig)
        return ThaModuleMpigISISMacGet(moduleMpig, mac);

    return cAtError;
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, FlowControllerFactory);
        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        mMethodOverride(m_ThaModuleEthOverride, OamFlowManagerObjectCreate);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));
        mMethodOverride(m_AtModuleEthOverride, FlowCreate);
        mMethodOverride(m_AtModuleEthOverride, MaxFlowsGet);
        mMethodOverride(m_AtModuleEthOverride, ISISMacSet);
        mMethodOverride(m_AtModuleEthOverride, ISISMacGet);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleEthPpp);
    }

AtModuleEth ThaModuleEthPppObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth ThaModuleEthPppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaModuleEthPppObjectInit(newModule, device);
    }

AtEthFlow ThaModuleEthPppFlowCreate(AtModuleEth self, uint16 flowId)
    {
    AtEthFlow flow = ThaEthFlowNew(flowId, self);
    AtChannelInit((AtChannel)flow);
    return flow;
    }

uint16 ThaModuleEthMaxFlowsGet(ThaModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    uint32 numMpFlows = ThaModuleEthMaxNumMpFlows(self);
    return (uint16)(numMpFlows + AtModuleEncapMaxChannelsGet(encapModule));
    }

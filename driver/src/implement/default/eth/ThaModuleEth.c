/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaModuleEth.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa Ethernet module default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanController.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaIpCoreInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../ppp/ThaMpigReg.h"
#include "../util/ThaUtil.h"
#include "../physical/ThaSerdesController.h"
#include "../prbs/ThaPrbsEngineSerdes.h"

#include "ThaModuleEthInternal.h"
#include "ThaEthPort.h"
#include "ThaEthFlow.h"
#include "ThaEthPortReg.h"
#include "ThaEthFlowFlowControlInternal.h"
#include "controller/ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaNumBitInDword 32

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)     ((ThaModuleEth)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleEthMethods m_methods;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtModuleEthMethods m_AtModuleEthOverride;
static tAtModuleMethods    m_AtModuleOverride;

/* Cache super implementation */
static const tAtModuleMethods    *m_AtModuleMethods    = NULL;
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtModuleEthMethods *m_AtModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumParts(ThaModuleEth self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cAtModuleEth);
    }

static void AllMpFlowMsruDefaultSet(ThaModuleEth self, uint8 partId)
    {
    uint32 flow_i;

    for (flow_i = 0; flow_i < ThaModuleEthNumMpFlowPerPart(self); flow_i++)
        {
        uint32 regAddr = (cThaRegMPIGRSQPktAssemblerCtrl + flow_i) + ThaModuleEthPartOffset(self, partId);
        uint32 regVal  = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, cThaMPIGRsqPkaMsruMask, cThaMPIGRsqPkaMsruShift, 0x7F);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static void AllMpFlowMrruDefaultSet(ThaModuleEth self, uint8 partId)
    {
    uint32 flow_i;

    for (flow_i = 0; flow_i < ThaModuleEthNumMpFlowPerPart(self); flow_i++)
        {
        uint32 regAddr = (cThaRegMPIGRSQPktAssemblerCtrl + flow_i) + ThaModuleEthPartOffset(self, partId);
        uint32 regVal  = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, cThaMPIGRsqPkaMrruMask, cThaMPIGRsqPkaMrruShift, 0);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static eAtRet DefaultSet(ThaModuleEth self)
    {
    uint8 part_i;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        {
        AllMpFlowMsruDefaultSet(self, part_i);
        AllMpFlowMrruDefaultSet(self, part_i);
        }

    if (ThaModuleEthFsmIsSupported(self))
        {
        ThaModuleEthTxFsmPinEnable(self, cAtTrue);
        ThaModuleEthRxFsmPinEnable(self, cAtTrue);
        }

    return cAtOk;
    }

static uint32 FlowUsageMemorySize(ThaModuleEth self)
    {
    uint32 maxNumMpFlows = ThaModuleEthMaxNumMpFlows(self);

    if (maxNumMpFlows == 0)
        return 0;

    return maxNumMpFlows * sizeof(eBool);
    }

static eAtRet FlowUsageInit(ThaModuleEth self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memSize = FlowUsageMemorySize(self);

    if (memSize == 0)
        return cAtOk;

    mMethodsGet(osal)->MemInit(osal, self->flowsAreUsed, 0, memSize);
    self->numUsedFlows = 0;

    return cAtOk;
    }

static eAtRet FlowUsageDeallocate(ThaModuleEth self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, self->flowsAreUsed);
    self->flowsAreUsed = NULL;

    return cAtOk;
    }

static eAtRet FlowUsageAllocate(ThaModuleEth self)
    {
    uint32 memSize;
    AtOsal osal;
    eBool *flowUsages;

    memSize = FlowUsageMemorySize(self);
    if (memSize == 0)
        return cAtOk;

    /* If memory is already allocated, just initialize it */
    if (self->flowsAreUsed)
        return FlowUsageInit(self);

    /* Allocate memory for ethernet flow usages (if it has not) */
    osal = AtSharedDriverOsalGet();
    flowUsages = mMethodsGet(osal)->MemAlloc(osal, memSize);
    if (flowUsages == NULL)
        return cAtErrorRsrcNoAvail;
    self->flowsAreUsed = flowUsages;

    /* Initialize */
    return FlowUsageInit(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleEth);
    }

static void ClearDatabase(ThaModuleEth self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self->vlanIdBitMask , 0, 512);

    FlowUsageInit(self);
    }

static eAtRet Init(AtModule self)
    {
    ThaModuleEth ethModule;

    /* Super */
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Additional configuration */
    ethModule = mThis(self);
    ClearDatabase(ethModule);
    if (mThis(self)->serdesManager)
        ret |= ThaEthSerdesManagerInit(mThis(self)->serdesManager);
    ret |= DefaultSet(ethModule);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);

    if (mMethodsGet(mThis(self))->SerdesCauseInterrupt(mThis(self), glbIntr))
        mMethodsGet(mThis(self))->SerdesInterruptProcess(mThis(self), glbIntr, hal);

    if (mMethodsGet(mThis(self))->PortCauseInterrupt(mThis(self), glbIntr))
        mMethodsGet(mThis(self))->PortInterruptProcess(mThis(self), glbIntr, hal);
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCoreEthHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = AtModuleDeviceGet(self);

    if (!ThaModuleEthInterruptIsSupported(mThis(self)))
        return enable ? cAtErrorModeNotSupport : cAtOk;

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            InterruptHwEnable(ipCore, enable);

        /* Store to DB */
        mThis(self)->interruptEnabled = enable;
        }

    return cAtOk;
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return mThis(self)->interruptEnabled;
    }

static void FlowControllerDelete(ThaEthFlowController *controller)
    {
    AtObjectDelete((AtObject)*controller);
    *controller = NULL;
    }

static void AllFlowControllersDelete(ThaModuleEth self)
    {
    FlowControllerDelete(&(self->ciscoHdlcFlowController));
    FlowControllerDelete(&(self->pppFlowController));
    FlowControllerDelete(&(self->mpFlowController));
    FlowControllerDelete(&(self->oamFlowController));
    FlowControllerDelete(&(self->pktFlowController));
    FlowControllerDelete(&(self->frFlowController));
    FlowControllerDelete(&(self->encapFlowController));
    FlowControllerDelete(&(self->pwFlowController));
    }

static void Delete(AtObject self)
    {
    ThaModuleEth thisModule = mThis(self);

    FlowUsageDeallocate(thisModule);
    AtObjectDelete((AtObject)mThis(self)->oamFlowManager);
    mThis(self)->oamFlowManager = NULL;
    AllFlowControllersDelete(thisModule);
    AtObjectDelete((AtObject)mThis(self)->flowHeaderProvider);
    mThis(self)->flowHeaderProvider = NULL;
    AtObjectDelete((AtObject)mThis(self)->serdesManager);
    mThis(self)->serdesManager = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
	AtUnused(self);
    return 1;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    localAddress = localAddress & cBit23_0;
    if ((localAddress >= 0x400000) &&
        (localAddress <= 0x43FFFF))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet SerdesManagerSetup(AtModule self)
    {
    if (mThis(self)->serdesManager)
        AtObjectDelete((AtObject)mThis(self)->serdesManager);

    mThis(self)->serdesManager = mMethodsGet(mThis(self))->SerdesManagerCreate(mThis(self));
    return ThaEthSerdesManagerSetup(mThis(self)->serdesManager);
    }

static eBool DebugCountersModuleIsSupported(ThaModuleEth self, uint32 module)
    {
    AtUnused(self);
    if ((module == cAtModuleEth) || (module == cThaModulePmc))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DebugCountersModuleSet(ThaModuleEth self, uint32 module)
    {
    if (DebugCountersModuleIsSupported(self, module))
        {
        self->countersModule = module;
        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet Setup(AtModule self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    ThaModuleEth ethModule = mThis(self);
    eAtRet ret;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    SerdesManagerSetup(self);

    if (mThis(self)->flowHeaderProvider == NULL)
        mThis(self)->flowHeaderProvider = mMethodsGet(mThis(self))->FlowHeaderProviderCreate(mThis(self));

    mThis(self)->flowControllerFactory = mMethodsGet(mThis(self))->FlowControllerFactory(mThis(self));
    if (mThis(self)->oamFlowManager == NULL)
        mThis(self)->oamFlowManager = mMethodsGet(mThis(self))->OamFlowManagerObjectCreate(mThis(self));

    /* Set default counter mode */
    mThis(self)->countersModule = ThaDeviceDefaultCounterModule(device, cAtModuleEth);

    /* Allocate flow resource usage */
    return FlowUsageAllocate(ethModule);
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return ThaEthPortNew(portId, self);
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleEthRegisterIteratorCreate(self);
    }

static uint32 MaxNumMpFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return 128;
    }

static void StickiesClear(ThaModuleEth self)
    {
    static const uint32 cAllOne = 0xFFFFFFFF;

    mModuleHwWrite(self, cThaDebugGbeSticky0, cAllOne);
    mModuleHwWrite(self, cThaDebugGbeSticky1, cAllOne);
    }

static eAtRet DebugGeSticky(ThaModuleEth self)
    {
    mModuleDebugRegPrint(self, GbeSticky0);
    mModuleDebugRegPrint(self, GbeSticky1);
    StickiesClear(self);
    return cAtOk;
    }

static eAtRet Debug(AtModule self)
    {
    AtModule counterModule;

    /* Information of super */
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    mMethodsGet(mThis(self))->DebugGeSticky(mThis(self));

    /* Display counter module */
    counterModule = AtDeviceModuleGet(AtModuleDeviceGet(self), mThis(self)->countersModule);
    AtPrintc(cSevNormal, "* Counter module: %s\r\n", AtModuleTypeString(counterModule));

    return cAtOk;
    }

static uint16 CVlanTpid(ThaModuleEth self)
    {
	AtUnused(self);
    return 0x8100;
    }

static ThaEthFlowController PppFlowControllerCreate(ThaModuleEth self)
    {
    return ThaEthFlowControllerFactoryPppFlowControllerCreate(self->flowControllerFactory, (AtModuleEth)self);
    }

static ThaEthFlowController OamFlowControllerCreate(ThaModuleEth self)
    {
    return ThaEthFlowControllerFactoryOamFlowControllerCreate(self->flowControllerFactory, (AtModuleEth)self);
    }

static ThaEthFlowController MpFlowControllerCreate(ThaModuleEth self)
    {
    return ThaEthFlowControllerFactoryMpFlowControllerCreate(self->flowControllerFactory, (AtModuleEth)self);
    }

static ThaEthFlowController CiscoHdlcFlowControllerCreate(ThaModuleEth self)
    {
    return ThaEthFlowControllerFactoryCiscoHdlcFlowControllerCreate(self->flowControllerFactory, (AtModuleEth)self);
    }

static ThaEthFlowController FrFlowControllerCreate(ThaModuleEth self)
    {
    return ThaEthFlowControllerFactoryFrFlowControllerCreate(self->flowControllerFactory, (AtModuleEth)self);
    }

static ThaEthFlowController EncapFlowControllerCreate(ThaModuleEth self)
    {
    return ThaEthFlowControllerFactoryEncapFlowControllerCreate(self->flowControllerFactory, (AtModuleEth)self);
    }

static ThaEthFlowController PwFlowControllerCreate(ThaModuleEth self)
    {
    return ThaEthFlowControllerFactoryPwFlowControllerCreate(self->flowControllerFactory, (AtModuleEth)self);
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
	AtUnused(self);
    return ThaEthPortSerdesControllerNew(ethPort, AtChannelIdGet((AtChannel)ethPort));
    }

static eBool PortPllCanBeChecked(ThaModuleEth self, uint8 portId)
    {
    /* Do not need to check when warm restore */
    if (AtDeviceWarmRestoreIsStarted(AtModuleDeviceGet((AtModule)self)))
        return cAtFalse;

    return ThaModuleEthPortIsUsed(self, portId);
    }

static eBool PortIsUsed(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    /* By default, all ports can be used */
    return cAtTrue;
    }

static eBool EthVlanIdIsValid(ThaModuleEth self, uint32 vlanId)
    {
    const uint32 cVlanIdMax = 4096;
	AtUnused(self);
    return (vlanId < cVlanIdMax) ? cAtTrue : cAtFalse;
    }

static AtEthFlowControl PortFlowControlCreate(AtModuleEth self, AtEthPort port)
    {
    return ThaEthPortFlowControlNew((AtModule)self, port);
    }

static ThaEthFlowController FlowControllerGet(ThaModuleEth self, ThaEthFlowController* controller,
                                              ThaEthFlowController (*controllerCreateFunc)(ThaModuleEth))
    {
    if (self == NULL)
        return NULL;

    if (*controller)
        return *controller;

    *controller = controllerCreateFunc(self);
    return *controller;
    }

static uint8 VlanToLookUp(ThaModuleEth self)
    {
	AtUnused(self);
    /* Always use CVLAN to lookup */
    return 0;
    }

static uint32 VlanIdFromVlanDesc(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
	AtUnused(flow);
    return desc->vlans[mMethodsGet(self)->VlanToLookUp(self)].vlanId;
    }

static eBool PortSerdesPrbsIsSupported(ThaModuleEth self, AtEthPort ethPort)
    {
	AtUnused(ethPort);
	AtUnused(self);
    /* Let concrete determine */
    return cAtFalse;
    }

static AtPrbsEngine PortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
	AtUnused(self);
	AtUnused(serdesId);
    return ThaPrbsEngineSerdesEthPortNew(AtEthPortSerdesController(ethPort));
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    /* Concrete must know */
    AtUnused(self);
    return NULL;
    }

static eBool PortHasSourceMac(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    return cAtFalse;
    }

static eBool Port10GbSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool Port10GbPreambleCanBeConfigured(ThaModuleEth self)
    {
    /* Default is enable if 10GB is supported */
    if (mMethodsGet(self)->Port10GbSupported(self))
        return cAtTrue;
    return cAtFalse;
    }

static eBool PortHasIpAddress(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    return cAtFalse;
    }

static eBool UsePortSourceMacForAllFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool PortShouldByPassFcs(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    return cAtTrue;
    }

static eBool ApsIsSupported(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet DidChangeFlowEgressVlan(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *vlanDesc)
    {
	AtUnused(vlanDesc);
	AtUnused(flow);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet IngressVlanUse(ThaModuleEth self, AtEthFlow flow, uint32 vlanId)
    {
    uint32 vlanDwordIndex = vlanId / cThaNumBitInDword;
    uint32 vlanBitIndex   = cBit0 << (vlanId % cThaNumBitInDword);
	AtUnused(flow);

    self->vlanIdBitMask[vlanDwordIndex] |= vlanBitIndex;

    return cAtOk;
    }

static eAtRet IngressVlanUnUse(ThaModuleEth self, AtEthFlow flow, uint32 vlanId)
    {
    uint32 vlanDwordIndex = vlanId / cThaNumBitInDword;
    uint32 vlanBitIndex   = cBit0 << (vlanId % cThaNumBitInDword);
	AtUnused(flow);

    self->vlanIdBitMask[vlanDwordIndex] &= (~vlanBitIndex);

    return cAtOk;
    }

static eBool IngressVlanIsUsed(ThaModuleEth self, AtEthFlow flow, uint32 vlanId)
    {
    uint32 vlanDwordIndex  = vlanId / cThaNumBitInDword;
    uint32 vlanBitIndex    = cBit0 << (vlanId % cThaNumBitInDword);
	AtUnused(flow);

    if ((self->vlanIdBitMask[vlanDwordIndex] & vlanBitIndex))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 StartVersionHas10GDiscardCounter(ThaModuleEth self)
    {
	AtUnused(self);
    return cBit31_0;
    }

static eBool Has10GDiscardCounter(ThaModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionHas10GDiscardCounter(self);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static ThaEthFlowControllerFactory FlowControllerFactory(ThaModuleEth self)
    {
    AtUnused(self);
    return NULL;
    }

static ThaOamFlowManager OamFlowManagerObjectCreate(ThaModuleEth self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return 0x401000;
    }

static void PortRegShow(ThaModuleEth self, AtEthPort port)
    {
    /* Concrete product will do */
    AtUnused(self);
    AtUnused(port);
    }

static uint8 PartOfPort(ThaModuleEth self, AtEthPort port)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);

    if (ThaDeviceNumPartsOfModule(device, cAtModuleEth) == 1)
        return 0;

    return (uint8)AtChannelIdGet((AtChannel)port);
    }

static uint32 DiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return 0xFFFFFF;
    }

static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 NumSerdesControllers(AtModuleEth self)
    {
    return ThaEthSerdesManagerNumSerdesController(mThis(self)->serdesManager);
    }

static eBool SerdesIdIsValid(AtModuleEth self, uint32 serdesId)
    {
    return (serdesId < AtModuleEthNumSerdesControllers(self)) ? cAtTrue : cAtFalse;
    }

static AtSerdesController SerdesController(AtModuleEth self, uint32 serdesId)
    {
    if (SerdesIdIsValid(self, serdesId))
        return ThaEthSerdesManagerSerdesController(mThis(self)->serdesManager, serdesId);

    return NULL;
    }

static uint16 QsgmiiSerdesNumLanes(ThaModuleEth self, uint8 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return 4;
    }

static uint32 StartVersionSupportsSerdesAps(ThaModuleEth self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool SerdesApsSupported(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportVersion;

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    startSupportVersion = mMethodsGet(mThis(self))->StartVersionSupportsSerdesAps(mThis(self));
    return (AtDeviceVersionNumber(device) >= startSupportVersion);
    }

static eBool FullSerdesPrbsSupported(ThaModuleEth self)
    {
    /* Concrete need to determine, but commonly, it should support full */
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnableTxSerdes(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SerdesCauseInterrupt(ThaModuleEth self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtFalse;
    }

static void SerdesInterruptProcess(ThaModuleEth self, uint32 ethIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(ethIntr);
    AtUnused(hal);
    return ;
    }

static eBool PortCauseInterrupt(ThaModuleEth self, uint32 glbIntr)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    return cAtFalse;
    }

static void PortInterruptProcess(ThaModuleEth self, uint32 ethIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(ethIntr);
    AtUnused(hal);
    }

static uint32 SerdesIdHwToSw(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    return serdesId;
    }

static uint32 SerdesIdSwToHw(ThaModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    return serdesId;
    }

static uint32 SerdesIdToEthPortId(ThaModuleEth self, uint32 serdesId)
    {
    uint32 numPorts = AtModuleEthMaxPortsGet((AtModuleEth)self);
    return serdesId % numPorts;
    }

static uint32 EthPortIdToSerdesId(ThaModuleEth self, uint32 ethPortId)
    {
    AtUnused(self);
    return ethPortId;
    }

static eBool QsgmiiSerdesLaneIsUsed(ThaModuleEth self, uint8 serdesId, uint16 laneId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    AtUnused(laneId);
    return cAtTrue;
    }

static uint16 CVlanDefaultTpid(AtModuleEth self)
    {
    return ThaModuleEthCVlanTpid(mThis(self));
    }

static uint16 SVlanDefaultTpid(AtModuleEth self)
    {
    return ThaModuleEthSVlanTpid(mThis(self), AtModuleEthPortGet(self, 0));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleEth object = (ThaModuleEth)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(numUsedFlows);
    mEncodeUInt(interruptEnabled);

    mEncodeObject(flowControllerFactory);
    mEncodeObject(pppFlowController);
    mEncodeObject(oamFlowController);
    mEncodeObject(mpFlowController);
    mEncodeObject(ciscoHdlcFlowController);
    mEncodeObject(frFlowController);
    mEncodeObject(pktFlowController);
    mEncodeObject(flowHeaderProvider);
    mEncodeObject(oamFlowManager);
    mEncodeObject(serdesManager);
    mEncodeObject(encapFlowController);
    mEncodeObject(pwFlowController);
    mEncodeBoolArray(flowsAreUsed, ThaModuleEthMaxNumMpFlows((ThaModuleEth)self));
    mEncodeUInt32Array(vlanIdBitMask, mCount(object->vlanIdBitMask));
    mEncodeUInt(countersModule);
    }

static eBool PortIsInternalMac(ThaModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtTrue;
    }

static eBool HasSerdesGroup(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool FsmIsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtModule ModulePwe(ThaModuleEth self)
    {
    return (AtModule)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePwe);
    }

static eAtRet TxFsmPinEnable(ThaModuleEth self, eBool enabled)
    {
    return AtModuleRoleInputEnable(ModulePwe(self), enabled);
    }

static eBool TxFsmPinIsEnabled(ThaModuleEth self)
    {
    return AtModuleRoleInputIsEnabled(ModulePwe(self));
    }

static eBool TxFsmPinStatusGet(ThaModuleEth self)
    {
    return AtModuleRoleInputStatus(ModulePwe(self)) == cAtDeviceRoleActive ? 1 : 0;
    }

static eAtRet RxFsmPinEnable(ThaModuleEth self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtErrorModeNotSupport;
    }

static eBool RxFsmPinIsEnabled(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RxFsmPinStatusGet(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool InterruptIsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtEthPort DccEthPortGet(ThaModuleEth self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet TxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    AtUnused(self);
    AtUnused(maxLength);
    return cAtErrorNotImplemented;
    }

static uint32 TxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet RxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    AtUnused(self);
    AtUnused(maxLength);
    return cAtErrorNotImplemented;
    }

static uint32 RxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    AtUnused(self);
    return 0;
    }

static eBool NeedMultipleLanes(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(ThaModuleEth self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModuleEth(ThaModuleEth self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, m_AtModuleEthMethods, sizeof(m_AtModuleEthOverride));
        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, PortFlowControlCreate);
        mMethodOverride(m_AtModuleEthOverride, NumSerdesControllers);
        mMethodOverride(m_AtModuleEthOverride, SerdesController);
        mMethodOverride(m_AtModuleEthOverride, SerdesApsSupported);
        mMethodOverride(m_AtModuleEthOverride, CVlanDefaultTpid);
        mMethodOverride(m_AtModuleEthOverride, SVlanDefaultTpid);
        }

    mMethodsSet(ethModule, &m_AtModuleEthOverride);
    }

static void OverrideAtModule(ThaModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(ThaModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

static void MethodsInit(ThaModuleEth self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MaxNumMpFlows);
        mMethodOverride(m_methods, CVlanTpid);
        mMethodOverride(m_methods, FlowHeaderProviderCreate);
        mMethodOverride(m_methods, PortSerdesControllerCreate);
        mMethodOverride(m_methods, PortIsUsed);
        mMethodOverride(m_methods, PortPllCanBeChecked);
        mMethodOverride(m_methods, VlanToLookUp);
        mMethodOverride(m_methods, VlanIdFromVlanDesc);
        mMethodOverride(m_methods, PortSerdesPrbsIsSupported);
        mMethodOverride(m_methods, PortSerdesPrbsEngineCreate);
        mMethodOverride(m_methods, PortHasSourceMac);
        mMethodOverride(m_methods, PortHasIpAddress);
        mMethodOverride(m_methods, UsePortSourceMacForAllFlows);
        mMethodOverride(m_methods, PortShouldByPassFcs);
        mMethodOverride(m_methods, Port10GbSupported);
        mMethodOverride(m_methods, ApsIsSupported);
        mMethodOverride(m_methods, DidChangeFlowEgressVlan);
        mMethodOverride(m_methods, IngressVlanUse);
        mMethodOverride(m_methods, IngressVlanUnUse);
        mMethodOverride(m_methods, IngressVlanIsUsed);
        mMethodOverride(m_methods, StartVersionHas10GDiscardCounter);
        mMethodOverride(m_methods, FlowControllerFactory);
        mMethodOverride(m_methods, OamFlowManagerObjectCreate);
        mMethodOverride(m_methods, MacBaseAddress);
        mMethodOverride(m_methods, PortRegShow);
        mMethodOverride(m_methods, PartOfPort);
        mMethodOverride(m_methods, Port10GbPreambleCanBeConfigured);
        mMethodOverride(m_methods, DiagBaseAddress);
        mMethodOverride(m_methods, SerdesManagerCreate);
        mMethodOverride(m_methods, QsgmiiSerdesNumLanes);
        mMethodOverride(m_methods, QsgmiiSerdesLaneIsUsed);
        mMethodOverride(m_methods, StartVersionSupportsSerdesAps);
        mMethodOverride(m_methods, FullSerdesPrbsSupported);
        mMethodOverride(m_methods, CanEnableTxSerdes);
        mMethodOverride(m_methods, SerdesCauseInterrupt);
        mMethodOverride(m_methods, SerdesInterruptProcess);
        mMethodOverride(m_methods, PortCauseInterrupt);
        mMethodOverride(m_methods, PortInterruptProcess);
        mMethodOverride(m_methods, SerdesIdHwToSw);
        mMethodOverride(m_methods, SerdesIdSwToHw);
        mMethodOverride(m_methods, SerdesIdToEthPortId);
        mMethodOverride(m_methods, EthPortIdToSerdesId);
        mMethodOverride(m_methods, PortIsInternalMac);
        mMethodOverride(m_methods, HasSerdesGroup);
        mMethodOverride(m_methods, DebugCountersModuleSet);
        mMethodOverride(m_methods, FsmIsSupported);
        mMethodOverride(m_methods, TxFsmPinEnable);
        mMethodOverride(m_methods, TxFsmPinIsEnabled);
        mMethodOverride(m_methods, TxFsmPinStatusGet);
        mMethodOverride(m_methods, RxFsmPinEnable);
        mMethodOverride(m_methods, RxFsmPinIsEnabled);
        mMethodOverride(m_methods, RxFsmPinStatusGet);
        mMethodOverride(m_methods, InterruptIsSupported);
        mMethodOverride(m_methods, DebugGeSticky);
        mMethodOverride(m_methods, DccEthPortGet);
        mMethodOverride(m_methods, TxDccPacketMaxLengthSet);
        mMethodOverride(m_methods, TxDccPacketMaxLengthGet);
        mMethodOverride(m_methods, RxDccPacketMaxLengthSet);
        mMethodOverride(m_methods, RxDccPacketMaxLengthGet);
        mMethodOverride(m_methods, NeedMultipleLanes);
        }

    mMethodsSet(self, &m_methods);
    }

AtModuleEth ThaModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleEthObjectInit((AtModuleEth)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleEth ThaModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleEthObjectInit(newModule, device);
    }

eAtRet ThaModuleEthAllPortMacsActivate(AtModuleEth self, eBool activate)
    {
    eAtRet ret = cAtOk;
    AtIterator portIterator;
    ThaEthPort port;

    /* Iterate all ports and activate */
    portIterator = AtModuleEthPortIteratorCreate(self);
    while ((port = (ThaEthPort)AtIteratorNext(portIterator)) != NULL)
        {
        if (mMethodsGet(mThis(self))->PortIsInternalMac(mThis(self), (uint8)AtChannelIdGet((AtChannel)port)))
            ret |= ThaEthPortMacActivate(port, activate);
        }

    AtObjectDelete((AtObject)portIterator);

    return ret;
    }

uint32 ThaModuleEthMaxNumMpFlows(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumMpFlows(self);
    return 0;
    }

eBool ThaModuleEthMpFlowIsValid(ThaModuleEth self, uint32 flowId)
    {
    return (flowId < ThaModuleEthMaxNumMpFlows(self)) ? cAtTrue : cAtFalse;
    }

uint32 ThaModuleEthFreeMpFlowGet(ThaModuleEth self)
    {
    uint32 flow_i;
    uint32 maxNumFlows = ThaModuleEthMaxNumMpFlows(self);

    if (self == NULL)
    	return 0;

    if (self->numUsedFlows >= maxNumFlows)
        return maxNumFlows;

    for (flow_i = 0; flow_i < maxNumFlows; flow_i++)
        {
        eBool flowIsUsed = self->flowsAreUsed[flow_i];
        if (!flowIsUsed)
            return flow_i;
        }

    return maxNumFlows;
    }

eAtRet ThaModuleEthMpFlowUse(ThaModuleEth self, uint32 flowId)
    {
	if (self == NULL)
		return cAtErrorNullPointer;

    if (!ThaModuleEthMpFlowIsValid(self, flowId))
        return cAtErrorInvlParm;

    self->flowsAreUsed[flowId] = cAtTrue;
    self->numUsedFlows = self->numUsedFlows + 1;

    return cAtOk;
    }

eAtRet ThaModuleEthMpFlowUnuse(ThaModuleEth self, uint32 flowId)
    {
	if (self == NULL)
		return cAtErrorNullPointer;

    if (!ThaModuleEthMpFlowIsValid(self, flowId))
        return cAtErrorInvlParm;

    self->flowsAreUsed[flowId] = cAtFalse;
    self->numUsedFlows = self->numUsedFlows - 1;

    return cAtOk;
    }

uint16 ThaModuleEthSVlanTpid(ThaModuleEth self, AtEthPort port)
    {
	AtDevice device = AtModuleDeviceGet((AtModule)self);

	/* To avoid access on testbench which is so slow */
	if (AtDeviceTestbenchIsEnabled(device))
	    return 0x88A8;

    return ThaModuleClaEthPortSVlanTpid((ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla), port);
    }

uint16 ThaModuleEthCVlanTpid(ThaModuleEth self)
    {
	if (self)
		return mMethodsGet(self)->CVlanTpid(self);
	return 0;
    }

uint8 ThaModuleEthPartOfPort(ThaModuleEth self, AtEthPort port)
    {
	if (self)
	    return mMethodsGet(self)->PartOfPort(self, port);
	return 0;
    }

uint8 ThaModuleEthLocalEthPortId(ThaModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    return (uint8)AtChannelIdGet((AtChannel)port);
    }

eAtRet ThaModuleEthIngressVlanUse(ThaModuleEth self, AtEthFlow flow, uint32 vlanId)
    {
    if (self == NULL)
        return cAtError;

    if (!EthVlanIdIsValid(self, vlanId))
        return cAtErrorInvlParm;

    return mMethodsGet(self)->IngressVlanUse(self, flow, vlanId);
    }

eAtRet ThaModuleEthIngressVlanUnUse(ThaModuleEth self, AtEthFlow flow, uint32 vlanId)
    {
    if (self == NULL)
        return cAtError;

    if (!EthVlanIdIsValid(self, vlanId) || (self == NULL))
        return cAtErrorInvlParm;

    return mMethodsGet(self)->IngressVlanUnUse(self, flow, vlanId);
    }

eBool ThaModuleEthIngressVlanIsInused(ThaModuleEth self, AtEthFlow flow, uint32 vlanId)
    {
    if (self == NULL)
        return cAtFalse;

    if (!EthVlanIdIsValid(self, vlanId))
        return cAtFalse;

    return mMethodsGet(self)->IngressVlanIsUsed(self, flow, vlanId);
    }

ThaEthFlowController ThaModuleEthPppFlowControllerGet(ThaModuleEth self)
    {
    if (self)
        return FlowControllerGet(self, &(self->pppFlowController), PppFlowControllerCreate);
    return NULL;
    }

ThaEthFlowController ThaModuleEthOamFlowControllerGet(ThaModuleEth self)
    {
	if (self)
		return FlowControllerGet(self, &(self->oamFlowController), OamFlowControllerCreate);
	return NULL;
    }

ThaEthFlowController ThaModuleEthMpFlowControllerGet(ThaModuleEth self)
    {
	if (self)
		return FlowControllerGet(self, &(self->mpFlowController), MpFlowControllerCreate);
	return NULL;
    }

ThaEthFlowController ThaModuleEthCiscoHdlcFlowControllerGet(ThaModuleEth self)
    {
	if (self)
		return FlowControllerGet(self, &(self->ciscoHdlcFlowController), CiscoHdlcFlowControllerCreate);
	return NULL;
    }

ThaEthFlowController ThaModuleEthFrFlowControllerGet(ThaModuleEth self)
    {
	if (self)
		return FlowControllerGet(self, &(self->frFlowController), FrFlowControllerCreate);
	return NULL;
    }

ThaEthFlowController ThaModuleEthEncapFlowControllerGet(ThaModuleEth self)
    {
    if (self)
        return FlowControllerGet(self, &(self->encapFlowController), EncapFlowControllerCreate);
    return NULL;
    }

ThaEthFlowController ThaModuleEthPwFlowControllerGet(ThaModuleEth self)
    {
    if (self)
        return FlowControllerGet(self, &(self->pwFlowController), PwFlowControllerCreate);
    return NULL;
    }

uint32 ThaModuleEthNumMpFlowPerPart(ThaModuleEth self)
    {
    ThaDevice device;
    uint32 maxMpFlow;

    if (self == NULL)
        return 0;

    maxMpFlow = mMethodsGet(self)->MaxNumMpFlows(self);
    device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return maxMpFlow / ThaDeviceNumPartsOfModule(device, cAtModuleEth);
    }

uint32 ThaModuleEthPartOffset(ThaModuleEth self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cAtModuleEth, partId);
    }

AtSerdesController ThaModuleEthPortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    if (self)
        return mMethodsGet(self)->PortSerdesControllerCreate(self, ethPort);
    return NULL;
    }

eBool ThaModuleEthPortIsUsed(ThaModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(self)->PortIsUsed(self, portId);
    return cAtFalse;
    }

eBool ThaModuleEthPortPllCanBeChecked(ThaModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(self)->PortPllCanBeChecked(self, portId);
    return cAtFalse;
    }

AtEthFlow ThaModuleEthOamFlowCreate(AtModuleEth self, AtHdlcLink link)
    {
    if (self)
        return ThaOamFlowManagerOamFlowCreate(mThis(self)->oamFlowManager, link);
    return NULL;
    }

eAtRet ThaModuleEthOamFlowDelete(AtModuleEth self, AtEthFlow oamFlow)
    {
    if (self)
        return ThaOamFlowManagerOamFlowDelete(mThis(self)->oamFlowManager, oamFlow);
    return cAtErrorNullPointer;
    }

AtPrbsEngine ThaModuleEthPortSerdesPrbsEngineCreate(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId)
    {
    if (self == NULL)
        return NULL;

    if (!mMethodsGet(self)->PortSerdesPrbsIsSupported(self, ethPort))
        return NULL;

    return mMethodsGet(self)->PortSerdesPrbsEngineCreate(self, ethPort, serdesId);
    }

ThaEthFlowHeaderProvider ThaModuleEthFlowHeaderProviderGet(ThaModuleEth self)
    {
    return self ? self->flowHeaderProvider : NULL;
    }

eBool ThaModuleEthPortHasSourceMac(ThaModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(self)->PortHasSourceMac(self, portId);
    return cAtFalse;
    }

eBool ThaModuleEthPortHasIpAddress(ThaModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(self)->PortHasIpAddress(self, portId);
    return cAtFalse;
    }

eBool ThaModuleEthPortShouldByPassFcs(ThaModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(self)->PortShouldByPassFcs(self, portId);
    return cAtFalse;
    }

eBool ThaModuleEthApsIsSupported(ThaModuleEth self)
    {
    return (eBool)(self ? mMethodsGet(self)->ApsIsSupported(self) : cAtFalse);
    }

eBool ThaModuleEthPort10GbSupported(ThaModuleEth self)
    {
    return (eBool)(self ? mMethodsGet(self)->Port10GbSupported(self) : cAtFalse);
    }

eBool ThaModuleEthUsePortSourceMacForAllFlows(ThaModuleEth self)
    {
    return (eBool)(self ? mMethodsGet(self)->UsePortSourceMacForAllFlows(self) : cAtFalse);
    }

eAtRet ThaModuleEthDidChangeFlowEgressVlan(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *vlanDesc)
    {
    if (self)
        return mMethodsGet(self)->DidChangeFlowEgressVlan(self, flow, vlanDesc);

    return cAtError;
    }

uint8 ThaModuleEthVlanToLookUp(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->VlanToLookUp(self);
    return 0;
    }

eBool ThaModuleEthHas10GDiscardCounter(ThaModuleEth self)
    {
    if (self)
        return Has10GDiscardCounter(self);
    return cAtFalse;
    }

ThaOamFlowManager ThaModuleEthOamFlowManager(ThaModuleEth self)
    {
    return self ? self->oamFlowManager : NULL;
    }

uint32 ThaModuleEthMacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->MacBaseAddress(self, port);
    return cBit31_0;
    }

void ThaModuleEthPortRegShow(AtEthPort port)
    {
    ThaModuleEth self = (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)port), cAtModuleEth);
    if (self)
        mMethodsGet(self)->PortRegShow(self, port);
    }

uint32 ThaModuleEthDiagBaseAddress(ThaModuleEth self, uint32 serdesId)
    {
    if (self)
        return mMethodsGet(self)->DiagBaseAddress(self, serdesId);
    return 0x0;
    }

ThaEthSerdesManager ThaModuleEthSerdesManager(ThaModuleEth self)
    {
    return self ? self->serdesManager : NULL;
    }

uint32 ThaModuleEthQsgmiiLaneBaseAddress(ThaModuleEth self, uint8 serdesId, uint16 laneId)
    {
    uint32 serdesBaseAddress = ThaModuleEthDiagBaseAddress(self, serdesId);
    uint32 numLanes;
    uint32 laneFlatId;

    /* When SERDES APS is supported, hardware remove most of diagnostic logic
     * and just keep one lane for diagnostic */
    if (AtModuleEthSerdesApsSupported((AtModuleEth)self))
        return serdesBaseAddress;

    /* And some of older images still support full diagnostic. But they do not
     * support SERDES APS */
    numLanes   = mMethodsGet(self)->QsgmiiSerdesNumLanes(self, serdesId);
    laneFlatId = (serdesId * numLanes) + laneId;
    return serdesBaseAddress + (laneFlatId * 0x1000UL);
    }

uint16 ThaModuleEthQsgmiiSerdesNumLanes(ThaModuleEth self, uint8 serdesId)
    {
    if (self)
        return mMethodsGet(self)->QsgmiiSerdesNumLanes(self, serdesId);
    return 0;
    }

eBool ThaModuleEthQsgmiiSerdesLaneIsUsed(ThaModuleEth self, uint8 serdesId, uint16 laneId)
    {
    if (self)
        return mMethodsGet(self)->QsgmiiSerdesLaneIsUsed(self, serdesId, laneId);
    return cAtFalse;
    }

uint32 ThaModuleEthQsgmiiSerdesLaneRegisterWithOffset(ThaModuleEth self, uint8 serdesId, uint16 laneId, uint32 offset)
    {
    return ThaModuleEthQsgmiiLaneBaseAddress(self, serdesId, laneId) + offset;
    }

uint32 ThaModuleEthQsgmiiLaneCurrentStatusRegister(ThaModuleEth self, uint8 serdesId, uint16 laneId)
    {
    return ThaModuleEthQsgmiiSerdesLaneRegisterWithOffset(self, serdesId, laneId, 0x10);
    }

uint32 ThaModuleEthQsgmiiLaneStickyStatusRegister(ThaModuleEth self, uint8 serdesId, uint16 laneId)
    {
    return ThaModuleEthQsgmiiSerdesLaneRegisterWithOffset(self, serdesId,laneId, 0x0);
    }

eBool ThaModuleEthFullSerdesPrbsSupported(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->FullSerdesPrbsSupported(self);
    return cAtFalse;
    }

eBool ThaModuleEthCanEnableTxSerdes(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->CanEnableTxSerdes(self);
    return cAtFalse;
    }

eBool ThaModuleEthPortIsInternalMac(ThaModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(self)->PortIsInternalMac(self, portId);
    return cAtFalse;
    }

eBool ThaModuleEthHasSerdesGroup(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->HasSerdesGroup(self);
    return cAtFalse;
    }

AtEthFlow ThaModuleEthMpOamFlowCreate(AtModuleEth self, AtHdlcBundle bundle)
    {
    if (self)
        return ThaOamFlowManagerMpOamFlowCreate(mThis(self)->oamFlowManager, bundle);
    return NULL;
    }

eAtRet ThaModuleEthMpOamFlowDelete(AtModuleEth self, AtEthFlow oamFlow)
    {
    if (self)
        return ThaOamFlowManagerMpOamFlowDelete(mThis(self)->oamFlowManager, oamFlow);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleEthDebugCountersModuleSet(ThaModuleEth self, uint32 module)
    {
    if (self)
        return mMethodsGet(self)->DebugCountersModuleSet(self, module);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleEthDebugCountersModuleGet(ThaModuleEth self)
    {
    return self ? self->countersModule : cAtModuleEth;
    }

eBool ThaModuleEthFsmIsSupported(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->FsmIsSupported(self);
    return cAtFalse;
    }

eAtRet ThaModuleEthTxFsmPinEnable(ThaModuleEth self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->TxFsmPinEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool ThaModuleEthTxFsmPinIsEnabled(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->TxFsmPinIsEnabled(self);
    return cAtFalse;
    }

eBool ThaModuleEthTxFsmPinStatusGet(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->TxFsmPinStatusGet(self);
    return cAtFalse;
    }

eAtRet ThaModuleEthRxFsmPinEnable(ThaModuleEth self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->RxFsmPinEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool ThaModuleEthRxFsmPinIsEnabled(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->RxFsmPinIsEnabled(self);
    return cAtFalse;
    }

eBool ThaModuleEthRxFsmPinStatusGet(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->RxFsmPinStatusGet(self);
    return cAtFalse;
    }

eBool ThaModuleEthInterruptIsSupported(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->InterruptIsSupported(self);
    return cAtFalse;
    }

AtEthPort ThaModuleEthDccEthPortGet(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->DccEthPortGet(self);
    return NULL;
    }

eAtRet ThaModuleEthTxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (self)
        return mMethodsGet(self)->TxDccPacketMaxLengthSet(self, maxLength);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleEthTxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->TxDccPacketMaxLengthGet(self);
    return 0;
    }

eAtRet ThaModuleEthRxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (self)
        return mMethodsGet(self)->RxDccPacketMaxLengthSet(self, maxLength);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleEthRxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->RxDccPacketMaxLengthGet(self);
    return 0;
    }
eBool ThaModuleEthNeedMultipleLanes(ThaModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->NeedMultipleLanes(self);
    return 0;
    }

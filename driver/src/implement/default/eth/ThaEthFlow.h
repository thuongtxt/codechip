/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : ThaEthFlow.h
 * 
 * Created Date: Sep 17, 2012
 *
 * Description : Thalassa Ethernet flow
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOW_H_
#define _THAETHFLOW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthFlow.h"
#include "AtModuleEth.h"
#include "AtChannelClasses.h"
#include "ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlow * ThaEthFlow;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete flows */
AtEthFlow ThaEthFlowNew(uint32 flowId, AtModuleEth module);
AtEthFlow ThaStmEthFlowNew(uint32 flowId, AtModuleEth module);
AtEthFlow ThaOamFlowNew(uint32 flowId, AtModuleEth module);
AtEthFlow Tha60290081EthFlowNew(uint32 flowId, AtModuleEth module);

/* Ethernet flow controller */
ThaEthFlowController ThaEthFlowControllerGet(ThaEthFlow self);
ThaEthFlowController ThaEthFlowControllerSet(ThaEthFlow self, ThaEthFlowController controller);

uint32 ThaEthFlowHwFlowIdGet(ThaEthFlow self);
void ThaEthFlowHwFlowIdSet(ThaEthFlow self, uint32 hwFlowId);
uint8 ThaEthFlowClassInMpBundleGet(ThaEthFlow self);
eAtRet ThaEthFlowMrruSet(ThaEthFlow self, uint32 mrru);
eAtRet ThaEthFlowMaxDelaySet(ThaEthFlow self, uint8 maxDelay);
uint32 ThaEthFlowMaxDelayGet(ThaEthFlow self);
eAtRet ThaEthFlowTxTrafficEnable(ThaEthFlow self, eBool enable);
eBool ThaEthFlowTxTrafficIsEnabled(ThaEthFlow self);
eAtRet ThaEthFlowRxTrafficEnable(ThaEthFlow self, eBool enable);
eAtRet ThaEthFlowResequenceBufferRelease(AtEthFlow flow);

uint32 ThaEthVlanIdGet(const tAtEthVlanDesc *desc);
uint8 ThaEthFlowVlanPriorityGet(ThaEthFlow self, const tAtEthVlanDesc *desc);
uint32 ThaEthFlowPartOffset(ThaEthFlow self, eAtModule moduleId);

#ifdef __cplusplus
}
#endif

#endif /* _THAETHFLOW_H_ */

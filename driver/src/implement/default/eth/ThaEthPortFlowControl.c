/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthPortFlowControl.c
 *
 * Created Date: Feb 18, 2014
 *
 * Description : Flow control for ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthPortFlowControlInternal.h"
#include "ThaEthPort.h"
#include "ThaEthPortReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtEthFlowControlMethods m_AtEthFlowControlOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(AtEthFlowControl self)
    {
    ThaEthPort port = (ThaEthPort)AtEthFlowControlChannelGet(self);
    return ThaEthPortDefaultOffset(port) + ThaEthPortMacBaseAddress(port);
    }

static uint32 IPEthernetTripleSpdSimpleMACTxterCtrl(AtEthFlowControl self)
    {
    return cThaRegIPEthernetTripleSpdSimpleMACTxterCtrl + DefaultOffset(self);
    }

static uint32 IPEthernetTripleSpdSimpleMACflowCtrlTimeIntvl(AtEthFlowControl self)
    {
    return cThaRegIPEthernetTripleSpdSimpleMACflowCtrlTimeIntvl + DefaultOffset(self);
    }

static eAtModuleEthRet Enable(AtEthFlowControl self, eBool enable)
    {
    uint32 regAddr = IPEthernetTripleSpdSimpleMACTxterCtrl(self);
    uint32 regVal  = AtEthFlowControlRead(self, regAddr, cAtModuleEth);

    mFieldIns(&regVal, cThaIpEthTripSmacTxPauDisMask, cThaIpEthTripSmacTxPauDisShift, enable ? 0 : 1);
    AtEthFlowControlWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool IsEnabled(AtEthFlowControl self)
    {
    uint32 regAddr = IPEthernetTripleSpdSimpleMACTxterCtrl(self);
    uint32 regVal  = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return (regVal & cThaIpEthTripSmacTxPauDisMask) ? cAtFalse : cAtTrue;
    }

static eAtModuleEthRet PauseFramePeriodSet(AtEthFlowControl self, uint32 periodMs)
    {
    uint32 regAddr = IPEthernetTripleSpdSimpleMACflowCtrlTimeIntvl(self);
    uint32 regVal  = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripSmacTimeIntvl, periodMs);
    AtEthFlowControlWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 PauseFramePeriodGet(AtEthFlowControl self)
    {
    uint32 regAddr = IPEthernetTripleSpdSimpleMACflowCtrlTimeIntvl(self);
    uint32 regVal  = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cThaIpEthTripSmacTimeIntvl);
    }

static eAtModuleEthRet PauseFrameQuantaSet(AtEthFlowControl self, uint32 quanta)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdSimpleMACflowCtrlQuanta + DefaultOffset(self);
    uint32 regVal  = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacQuantaMask, cThaIpEthTripSmacQuantaShift, quanta);
    AtEthFlowControlWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 PauseFrameQuantaGet(AtEthFlowControl self)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdSimpleMACflowCtrlQuanta + DefaultOffset(self);
    uint32 regVal  = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cThaIpEthTripSmacQuanta);
    }

static void OverrideAtEthFlowControl(AtEthFlowControl self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowControlOverride, mMethodsGet(self), sizeof(m_AtEthFlowControlOverride));

        mMethodOverride(m_AtEthFlowControlOverride, Enable);
        mMethodOverride(m_AtEthFlowControlOverride, IsEnabled);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodSet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodGet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFrameQuantaSet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFrameQuantaGet);
        }

    mMethodsSet(self, &m_AtEthFlowControlOverride);
    }

static void Override(AtEthFlowControl self)
    {
    OverrideAtEthFlowControl(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthPortFlowControl);
    }

AtEthFlowControl ThaEthPortFlowControlObjectInit(AtEthFlowControl self, AtModule module, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
	AtUnused(module);
    /* Clear memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthFlowControlObjectInit(self, (AtChannel)port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthFlowControl ThaEthPortFlowControlNew(AtModule module, AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlowControl newFlowControl = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlowControl == NULL)
        return NULL;

    /* Construct it */
    return ThaEthPortFlowControlObjectInit(newFlowControl, module, port);
    }

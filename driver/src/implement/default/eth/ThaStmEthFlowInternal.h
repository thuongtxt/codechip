/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaStmEthFlowInternal.h
 * 
 * Created Date: Aug 16, 2013
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMETHFLOWINTERNAL_H_
#define _THASTMETHFLOWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaEthFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmEthFlowMethods
    {
    uint8 dumy;
    }tThaStmEthFlowMethods;

typedef struct tThaStmEthFlow
    {
    tThaEthFlow super;
    const tThaStmEthFlowMethods *methods;

    }tThaStmEthFlow;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow ThaStmEthFlowObjectInit(AtEthFlow self, uint16 flowId, AtModuleEth module);


#ifdef __cplusplus
}
#endif
#endif /* _THASTMETHFLOWINTERNAL_H_ */


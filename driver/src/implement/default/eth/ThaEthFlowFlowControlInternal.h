/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH module
 * 
 * File        : ThaEthFlowFlowControlInternal.h
 * 
 * Created Date: Oct 4, 2013
 *
 * Description : ETH Port Flow Controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWFLOWCONTROLINTERNAL_H_
#define _THAETHFLOWFLOWCONTROLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/eth/AtEthFlowControlInternal.h"
#include "ThaEthFlowFlowControl.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlowFlowControl *ThaEthFlowFlowControl;

typedef struct tThaEthFlowFlowControlMethods
    {
	void (*ThresholdApply)(ThaEthFlowFlowControl self);
    }tThaEthFlowFlowControlMethods;

typedef struct tThaEthFlowFlowControl
    {
    tAtEthFlowControl super;
    const tThaEthFlowFlowControlMethods *methods;

    /* Private data */
    uint32 highThreshold;
    uint32 lowThreshold;
    }tThaEthFlowFlowControl;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlowControl ThaEthFlowFlowControlObjectInit(AtEthFlowControl self, AtModule module, AtEthFlow flow);
void ThaEthFlowFlowControlThresholdApply(ThaEthFlowFlowControl self);

/* Access private data */
eAtRet ThaEthFlowFlowControlHighThresholdSet(ThaEthFlowFlowControl self, uint32 threshold);
uint32 ThaEthFlowFlowControlHighThresholdGet(ThaEthFlowFlowControl self);
eAtRet ThaEthFlowFlowControlLowThresholdSet(ThaEthFlowFlowControl self, uint32 threshold);
uint32 ThaEthFlowFlowControlLowThresholdGet(ThaEthFlowFlowControl self);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORTFLOWCONTROLLERINTERNAL_H_ */


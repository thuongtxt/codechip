/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ETHERNET
 *
 * File        : ThaModuleEthInternal.h
 *
 * Created Date: Sep 24, 2012
 *
 * Description : Thalasss default Ethernet module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEETHINTERNAL_H_
#define _THAMODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/eth/AtModuleEthInternal.h"
#include "../physical/ThaEthSerdesManager.h"
#include "ThaModuleEth.h"
#include "ThaEthPortFlowControl.h"
#include "oamflow/ThaOamFlowManager.h"
#include "controller/ThaEthFlowControllerFactory.h"
#include "ThaEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleEthMethods
    {
    uint32 (*MaxNumMpFlows)(ThaModuleEth self);
    uint16 (*CVlanTpid)(ThaModuleEth self);
    eBool (*ApsIsSupported)(ThaModuleEth self);
    uint8 (*PartOfPort)(ThaModuleEth self, AtEthPort port);
    eBool (*PortIsInternalMac)(ThaModuleEth self, uint8 portId);
    eBool (*InterruptIsSupported)(ThaModuleEth self);

    /* Flow factory */
    ThaEthFlowControllerFactory (*FlowControllerFactory)(ThaModuleEth self);
    ThaEthFlowHeaderProvider (*FlowHeaderProviderCreate)(ThaModuleEth self);
    ThaOamFlowManager (*OamFlowManagerObjectCreate)(ThaModuleEth self);

    /* SERDES */
    AtSerdesController (*PortSerdesControllerCreate)(ThaModuleEth self, AtEthPort ethPort);
    eBool (*PortSerdesPrbsIsSupported)(ThaModuleEth self, AtEthPort ethPort);
    AtPrbsEngine (*PortSerdesPrbsEngineCreate)(ThaModuleEth self, AtEthPort ethPort, uint32 serdesId);
    ThaEthSerdesManager (*SerdesManagerCreate)(ThaModuleEth self);
    uint32 (*StartVersionSupportsSerdesAps)(ThaModuleEth self);
    eBool (*FullSerdesPrbsSupported)(ThaModuleEth self);
    eBool (*CanEnableTxSerdes)(ThaModuleEth self);
    eBool (*SerdesCauseInterrupt)(ThaModuleEth self, uint32 glbIntr);
    void (*SerdesInterruptProcess)(ThaModuleEth self, uint32 ethIntr, AtHal hal);
    eBool (*HasSerdesGroup)(ThaModuleEth self);
    eBool (*PortCauseInterrupt)(ThaModuleEth self, uint32 glbIntr);
    void (*PortInterruptProcess)(ThaModuleEth self, uint32 ethIntr, AtHal hal);

    eBool (*PortIsUsed)(ThaModuleEth self, uint8 portId);
    eBool (*PortPllCanBeChecked)(ThaModuleEth self, uint8 portId);
    eBool (*PortHasSourceMac)(ThaModuleEth self, uint8 portId);
    eBool (*PortHasIpAddress)(ThaModuleEth self, uint8 portId);
    eBool (*UsePortSourceMacForAllFlows)(ThaModuleEth self);
    eBool (*PortShouldByPassFcs)(ThaModuleEth self, uint8 portId);
    eBool (*Port10GbSupported)(ThaModuleEth self);
    eBool (*Port10GbPreambleCanBeConfigured)(ThaModuleEth self);

    uint32 (*VlanIdFromVlanDesc)(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *desc);
    uint8  (*VlanToLookUp)(ThaModuleEth self);
    eAtRet (*DidChangeFlowEgressVlan)(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *vlanDesc);

    eAtRet (*IngressVlanUse)(ThaModuleEth self, AtEthFlow flow, uint32 vlanId);
    eAtRet (*IngressVlanUnUse)(ThaModuleEth self, AtEthFlow flow, uint32 vlanId);
    eBool (*IngressVlanIsUsed)(ThaModuleEth self, AtEthFlow flow, uint32 vlanId);

    /* For registers */
    uint32 (*MacBaseAddress)(ThaModuleEth self, AtEthPort port);
    uint32 (*DiagBaseAddress)(ThaModuleEth self, uint32 serdesId);
    uint16 (*QsgmiiSerdesNumLanes)(ThaModuleEth self, uint8 serdesId);
    eBool (*QsgmiiSerdesLaneIsUsed)(ThaModuleEth self, uint8 serdesId, uint16 laneId);

    /* Backward compatible */
    uint32 (*StartVersionHas10GDiscardCounter)(ThaModuleEth self);

    /* For debugging */
    void (*PortRegShow)(ThaModuleEth self, AtEthPort port);
    eAtRet (*DebugCountersModuleSet)(ThaModuleEth self, uint32 module);
    eAtRet (*DebugGeSticky)(ThaModuleEth self);

    /* SERDES */
    uint32 (*SerdesIdHwToSw)(ThaModuleEth self, uint32 serdesId);
    uint32 (*SerdesIdSwToHw)(ThaModuleEth self, uint32 serdesId);
    uint32 (*SerdesIdToEthPortId)(ThaModuleEth self, uint32 serdesId);
    uint32 (*EthPortIdToSerdesId)(ThaModuleEth self, uint32 ethPortId);
    eBool (*SerdesHasDrp)(ThaModuleEth self, uint32 serdesId);

    /* FSM */
    eBool  (*FsmIsSupported)(ThaModuleEth self);
    eAtRet (*TxFsmPinEnable)(ThaModuleEth self, eBool enabled);
    eBool  (*TxFsmPinIsEnabled)(ThaModuleEth self);
    eBool  (*TxFsmPinStatusGet)(ThaModuleEth self);
    eAtRet (*RxFsmPinEnable)(ThaModuleEth self, eBool enabled);
    eBool  (*RxFsmPinIsEnabled)(ThaModuleEth self);
    eBool  (*RxFsmPinStatusGet)(ThaModuleEth self);

    /* Some projects has designated port for DCC transport. */
    AtEthPort (*DccEthPortGet)(ThaModuleEth self);
    eAtRet (*TxDccPacketMaxLengthSet)(ThaModuleEth self, uint32 maxLength);
    uint32 (*TxDccPacketMaxLengthGet)(ThaModuleEth self);
    eAtRet (*RxDccPacketMaxLengthSet)(ThaModuleEth self, uint32 maxLength);
    uint32 (*RxDccPacketMaxLengthGet)(ThaModuleEth self);
    eBool (*NeedMultipleLanes)(ThaModuleEth self);
    }tThaModuleEthMethods;

typedef struct tThaModuleEth
    {
    tAtModuleEth super;
    const tThaModuleEthMethods *methods;

    /* Private data */
    uint32 numUsedFlows;
    eBool *flowsAreUsed;
    uint32 vlanIdBitMask[128];

    eBool interruptEnabled;

    /* Controllers manage Ethernet flow */
    ThaEthFlowControllerFactory flowControllerFactory;
    ThaEthFlowController pppFlowController;
    ThaEthFlowController oamFlowController;
    ThaEthFlowController mpFlowController;
    ThaEthFlowController ciscoHdlcFlowController;
    ThaEthFlowController frFlowController;
    ThaEthFlowController encapFlowController;
    ThaEthFlowController pwFlowController;

    /* Controller manages packet flow control */
    ThaEthFlowController pktFlowController;

    /* Ethernet flow header provider */
    ThaEthFlowHeaderProvider flowHeaderProvider;

    /* Cache all OAM flows and Flow packet control */
    ThaOamFlowManager oamFlowManager;

    /* For physical */
    ThaEthSerdesManager serdesManager;

    /* For retrieving counter */
    uint32 countersModule;
    }tThaModuleEth;

typedef struct tThaModuleEthPpp
    {
    tThaModuleEth super;
    }tThaModuleEthPpp;

typedef struct tThaModuleEthStmPpp
    {
    tThaModuleEthPpp super;
    }tThaModuleEthStmPpp;

typedef struct tThaModuleEthPw
    {
    tThaModuleEth super;
    }tThaModuleEthPw;

typedef struct tThaModuleEthPwV2
    {
    tThaModuleEthPw super;
    }tThaModuleEthPwV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth ThaModuleEthObjectInit(AtModuleEth self, AtDevice device);
AtModuleEth ThaModuleEthPppObjectInit(AtModuleEth self, AtDevice device);
AtModuleEth ThaModuleEthStmPppObjectInit(AtModuleEth self, AtDevice device);
AtModuleEth ThaModuleEthPwV2ObjectInit(AtModuleEth self, AtDevice device);

AtIterator ThaModuleEthRegisterIteratorCreate(AtModule module);

uint16 ThaModuleEthMaxFlowsGet(ThaModuleEth self);
AtEthFlow ThaModuleEthPppFlowCreate(AtModuleEth self, uint16 flowId);
eAtRet ThaModuleEthDidChangeFlowEgressVlan(ThaModuleEth self, AtEthFlow flow, const tAtEthVlanDesc *vlanDesc);
uint8 ThaModuleEthVlanToLookUp(ThaModuleEth self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEETHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthFlowFlowControl.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : Flow control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWFLOWCONTROL_H_
#define _THAETHFLOWFLOWCONTROL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Product concretes */
AtEthFlowControl Tha60060011EthFlowFlowControlNew(AtModule module, AtEthFlow flow);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWFLOWCONTROL_H_ */


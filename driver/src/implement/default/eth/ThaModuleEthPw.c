/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETHERNET
 *
 * File        : ThaModuleEthPw.c
 *
 * Created Date: Mar 26, 2013
 *
 * Description : Ethernet module of PW product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleEthInternal.h"
#include "ThaEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;

/* Cache super implementation */
static const tAtModuleEthMethods *m_AtModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleEthPw);
    }

static uint16 MaxFlowsGet(AtModuleEth self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 MaxNumMpFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return 0;
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
	AtUnused(self);
	AtUnused(port);
    return cAtEthPortInterface1000BaseX;
    }

static eBool PortHasSourceMac(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    return cAtTrue;
    }

static eBool PortHasIpAddress(ThaModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, m_AtModuleEthMethods, sizeof(m_AtModuleEthOverride));
        mMethodOverride(m_AtModuleEthOverride, MaxFlowsGet);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule  = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, MaxNumMpFlows);
        mMethodOverride(m_ThaModuleEthOverride, PortHasSourceMac);
        mMethodOverride(m_ThaModuleEthOverride, PortHasIpAddress);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideThaModuleEth(self);
    OverrideAtModuleEth(self);
    }

AtModuleEth ThaModuleEthPwObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEthObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth ThaModuleEthPwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleEthPwObjectInit(newModule, device);
    }

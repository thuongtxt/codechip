/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaOamFlowInternal.h
 * 
 * Created Date: Aug 22, 2016
 *
 * Description : OAM Flow
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAOAMFLOWINTERNAL_H_
#define _THAOAMFLOWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaEthFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaOamFlow
    {
    tThaEthFlow super;
    }tThaOamFlow;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow ThaOamFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THAOAMFLOWINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaOamFlowManagerDefault.c
 *
 * Created Date: Apr 5, 2015
 *
 * Description : ETH Flow manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaOamFlowManagerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/encap/AtHdlcLinkInternal.h"
#include "../../../../generic/eth/AtEthFlowInternal.h"
#include "../ThaEthFlow.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaOamFlowManagerDefault *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtEthFlow (*OamFlowFuncCreate)(ThaOamFlowManager self, AtChannel channel);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;
static tThaOamFlowManagerMethods m_ThaOamFlowManagerOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AllOamFlowsDeleteHelper(AtList oamFlows)
    {
    if (oamFlows == NULL)
        return;

    /* Delete all OAM flows */
    while (AtListLengthGet(oamFlows) > 0)
        {
        AtObject flow   = AtListObjectRemoveAtIndex(oamFlows, 0);
        AtHdlcLink link = AtEthFlowHdlcLinkGet((AtEthFlow)flow);

        /* Need to notify its link that this OAM flow will no longer exist */
        if (link)
            AtHdlcLinkOamFlowSet(link, NULL);

        AtObjectDelete(flow);
        }

    /* And the list holding them */
    AtObjectDelete((AtObject)(oamFlows));
    }

static void AllOamFlowsDelete(ThaOamFlowManager self)
    {
    AllOamFlowsDeleteHelper(mThis(self)->linkOamFlows);
    mThis(self)->linkOamFlows = NULL;
    }

static void AllOamMpFlowsDelete(ThaOamFlowManager self)
    {
    AllOamFlowsDeleteHelper(mThis(self)->bundleOamFlows);
    mThis(self)->bundleOamFlows = NULL;
    }

static AtList OamFlowListCreate(ThaOamFlowManager self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaOamFlowManagerModuleGet(self));
    AtModuleEncap moduleEncap = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    uint32 maxNumOfOam = 0;
    if (moduleEncap == NULL)
        return NULL;

    maxNumOfOam = AtModuleEncapMaxChannelsGet(moduleEncap);
    return AtListCreate(maxNumOfOam);
    }

static AtList OamFlowList(ThaOamFlowManager self)
    {
    if (self == NULL)
        return NULL;

    /* Already created */
    if (mThis(self)->linkOamFlows)
        return mThis(self)->linkOamFlows;

    /* Create one */
    mThis(self)->linkOamFlows = OamFlowListCreate(self);
    return mThis(self)->linkOamFlows;
    }

static AtList OamBundleFlowListCreate(ThaOamFlowManager self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaOamFlowManagerModuleGet(self));
    AtModuleEncap moduleEncap = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    uint32 maxNumOfOam = 0;

    if (moduleEncap == NULL)
        return NULL;

    maxNumOfOam = AtModuleEncapMaxBundlesGet(moduleEncap);
    return AtListCreate(maxNumOfOam);
    }

static AtList BundleOamFlowList(ThaOamFlowManager self)
    {
    if (self == NULL)
        return NULL;

    /* Already created */
    if (mThis(self)->bundleOamFlows)
        return mThis(self)->bundleOamFlows;

    /* Create one */
    mThis(self)->bundleOamFlows = OamBundleFlowListCreate(self);
    return mThis(self)->bundleOamFlows;
    }

static eAtRet OamFlowDeleteHelper(AtEthFlow oamFlow, AtList oamFlows, void (*OamFlowSetFunc)(AtEthFlow oamFlow))
    {
    if (oamFlows == NULL)
        return cAtOk;

    /* Ignore if this flow is not created by this module */
    if (!AtListContainsObject(oamFlows, (AtObject)oamFlow))
        return cAtOk;

    /* Remove it out of the list then destroy */
    AtListObjectRemove(oamFlows, (AtObject)oamFlow);
    OamFlowSetFunc(oamFlow);
    AtObjectDelete((AtObject)oamFlow);

    return cAtOk;
    }

static AtEthFlow OamFlowCreateHelper(ThaOamFlowManager self, AtChannel channel, AtList oamFlows, OamFlowFuncCreate funcCreate)
    {
    AtEthFlow newFlow = NULL;
    if (oamFlows == NULL)
        return NULL;

    /* Create new flow and try to cache it */
    newFlow = funcCreate(self, channel);
    if (newFlow == NULL)
        return NULL;

    if (AtListObjectAdd(oamFlows, (AtObject)newFlow) != cAtOk)
        {
        AtObjectDelete((AtObject)newFlow);
        return NULL;
        }

    return newFlow;
    }

static AtEthFlow OamFlowObjectCreate(ThaOamFlowManager self, AtHdlcLink link)
    {
    return ThaOamFlowNew(AtChannelIdGet((AtChannel)link), ThaOamFlowManagerModuleGet(self));
    }

static void OamPppFlowClear(AtEthFlow oamFlow)
    {
    AtEthFlowHdlcLinkSet(oamFlow, NULL);
    }

static eAtRet OamFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow)
    {
    return OamFlowDeleteHelper(oamFlow, mThis(self)->linkOamFlows, OamPppFlowClear);
    }

static AtEthFlow OamFlowCreate(ThaOamFlowManager self, AtHdlcLink link)
    {
    return OamFlowCreateHelper(self,
                               (AtChannel)link,
                               OamFlowList(self),
                               (OamFlowFuncCreate)mMethodsGet(self)->OamFlowObjectCreate);
    }

static eAtRet AllOamFlowsSourceMacAddressSet(ThaOamFlowManager self, AtEthPort port, uint8 *address)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet((AtChannel)port);
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtIterator encapIterator = AtModuleEncapChannelIteratorCreate(encapModule);
    uint8 portId;
    AtEncapChannel encapChannel;
    AtUnused(self);

    AtUnused(self);

    if (encapIterator == NULL)
        return cAtOk;

    portId = (uint8)AtChannelIdGet((AtChannel)port);
    while ((encapChannel = (AtEncapChannel)AtIteratorNext(encapIterator)) != NULL)
        {
        tAtEthVlanDesc egVlan;
        AtHdlcLink link = (AtHdlcLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);
        AtEthFlow flow = AtHdlcLinkOamFlowGet(link);
        if (flow == NULL)
            continue;

        if ((AtEthFlowEgressVlanGet(flow, &egVlan) == cAtOk) && egVlan.ethPortId == portId)
            ret |= AtEthFlowEgressSourceMacSet(flow, address);
        }

    AtObjectDelete((AtObject)encapIterator);
    return ret;
    }

static void Delete(AtObject self)
    {
    AllOamFlowsDelete((ThaOamFlowManager)self);
    AllOamMpFlowsDelete((ThaOamFlowManager)self);
    m_AtObjectMethods->Delete(self);
    }

static void OamMpFlowClear(AtEthFlow oamFlow)
    {
    AtEthFlowHdlcBundleSet(oamFlow, NULL);
    }

static eAtRet OamMpFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow)
    {
    return OamFlowDeleteHelper(oamFlow, mThis(self)->bundleOamFlows, OamMpFlowClear);
    }

static AtEthFlow OamMpFlowCreate(ThaOamFlowManager self, AtHdlcBundle bundle)
    {
    return OamFlowCreateHelper(self,
                               (AtChannel)bundle,
                               BundleOamFlowList(self),
                               (OamFlowFuncCreate)mMethodsGet(self)->OamMpFlowObjectCreate);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tThaOamFlowManagerDefault* object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeList(linkOamFlows);
    mEncodeList(bundleOamFlows);
    }

static void OverrideAtObject(ThaOamFlowManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaOamFlowManager(ThaOamFlowManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaOamFlowManagerOverride, mMethodsGet(self), sizeof(m_ThaOamFlowManagerOverride));

        mMethodOverride(m_ThaOamFlowManagerOverride, OamFlowCreate);
        mMethodOverride(m_ThaOamFlowManagerOverride, OamFlowObjectCreate);
        mMethodOverride(m_ThaOamFlowManagerOverride, OamFlowDelete);
        mMethodOverride(m_ThaOamFlowManagerOverride, AllOamFlowsSourceMacAddressSet);
        mMethodOverride(m_ThaOamFlowManagerOverride, OamMpFlowCreate);
        mMethodOverride(m_ThaOamFlowManagerOverride, OamMpFlowDelete);
        }

    mMethodsSet(self, &m_ThaOamFlowManagerOverride);
    }

static void Override(ThaOamFlowManager self)
    {
    OverrideAtObject(self);
    OverrideThaOamFlowManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaOamFlowManagerDefault);
    }

ThaOamFlowManager ThaOamFlowManagerDefaultObjectInit(ThaOamFlowManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaOamFlowManagerObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaOamFlowManager ThaOamFlowManagerDefaultNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaOamFlowManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaOamFlowManagerDefaultObjectInit(newManager, ethModule);
    }

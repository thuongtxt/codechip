/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaOamFlowManager.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : OAM flow manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAOAMFLOWMANAGER_H_
#define _THAOAMFLOWMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleClasses.h"
#include "AtHdlcBundle.h"
#include "AtHdlcLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaOamFlowManager * ThaOamFlowManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaOamFlowManager ThaOamFlowManagerDefaultNew(AtModuleEth ethModule);
ThaOamFlowManager Tha60210012OamFlowManagerNew(AtModuleEth ethModule);

AtEthFlow ThaOamFlowManagerOamFlowCreate(ThaOamFlowManager self, AtHdlcLink link);
eAtRet ThaOamFlowManagerOamFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow);

AtEthFlow ThaOamFlowManagerMpOamFlowCreate(ThaOamFlowManager self, AtHdlcBundle bundle);
eAtRet ThaOamFlowManagerMpOamFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow);

eAtRet ThaOamFlowManagerAllOamFlowsSourceMacAddressSet(ThaOamFlowManager self, AtEthPort port, uint8 *address);
AtModuleEth ThaOamFlowManagerModuleGet(ThaOamFlowManager self);

#ifdef __cplusplus
}
#endif
#endif /* _THAOAMFLOWMANAGER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaOamFlowManager.c
 *
 * Created Date: Apr 5, 2015
 *
 * Description : OAM flow manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaOamFlowManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaOamFlowManagerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthFlow OamFlowCreate(ThaOamFlowManager self, AtHdlcLink link)
    {
    AtUnused(self);
    AtUnused(link);
    return NULL;
    }

static eAtRet OamFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow)
    {
    AtUnused(self);
    AtUnused(oamFlow);
    return cAtErrorNotImplemented;
    }

static eAtRet AllOamFlowsSourceMacAddressSet(ThaOamFlowManager self, AtEthPort port, uint8 *address)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(address);
    return cAtErrorNotImplemented;
    }

static AtEthFlow OamFlowObjectCreate(ThaOamFlowManager self, AtHdlcLink link)
    {
    AtUnused(self);
    AtUnused(link);
    return NULL;
    }

static AtEthFlow OamMpFlowObjectCreate(ThaOamFlowManager self, AtHdlcBundle bundle)
    {
    AtUnused(self);
    AtUnused(bundle);
    return NULL;
    }

static AtEthFlow OamMpFlowCreate(ThaOamFlowManager self, AtHdlcBundle bundle)
    {
    AtUnused(self);
    AtUnused(bundle);
    return NULL;
    }

static eAtRet OamMpFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow)
    {
    AtUnused(self);
    AtUnused(oamFlow);
    return cAtErrorNotImplemented;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaOamFlowManager object = (ThaOamFlowManager)(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(ethModule);
    }

static void OverrideAtObject(ThaOamFlowManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaOamFlowManager self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaOamFlowManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, OamFlowCreate);
        mMethodOverride(m_methods, OamFlowObjectCreate);
        mMethodOverride(m_methods, OamFlowDelete);
        mMethodOverride(m_methods, AllOamFlowsSourceMacAddressSet);
        mMethodOverride(m_methods, OamMpFlowCreate);
        mMethodOverride(m_methods, OamMpFlowObjectCreate);
        mMethodOverride(m_methods, OamMpFlowDelete);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaOamFlowManager);
    }

ThaOamFlowManager ThaOamFlowManagerObjectInit(ThaOamFlowManager self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->ethModule = ethModule;

    return self;
    }

AtEthFlow ThaOamFlowManagerOamFlowCreate(ThaOamFlowManager self, AtHdlcLink link)
    {
    if (self)
        return mMethodsGet(self)->OamFlowCreate(self, link);
    return NULL;
    }

eAtRet ThaOamFlowManagerOamFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow)
    {
    if (self)
        return mMethodsGet(self)->OamFlowDelete(self, oamFlow);
    return cAtErrorNullPointer;
    }

eAtRet ThaOamFlowManagerAllOamFlowsSourceMacAddressSet(ThaOamFlowManager self, AtEthPort port, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->AllOamFlowsSourceMacAddressSet(self, port, address);
    return cAtErrorNullPointer;
    }

AtModuleEth ThaOamFlowManagerModuleGet(ThaOamFlowManager self)
    {
    return self ? self->ethModule : NULL;
    }

AtEthFlow ThaOamFlowManagerMpOamFlowCreate(ThaOamFlowManager self, AtHdlcBundle bundle)
    {
    if (self)
        return mMethodsGet(self)->OamMpFlowCreate(self, bundle);
    return NULL;
    }

eAtRet ThaOamFlowManagerMpOamFlowDelete(ThaOamFlowManager self, AtEthFlow oamFlow)
    {
    if (self)
        return mMethodsGet(self)->OamMpFlowDelete(self, oamFlow);
    return cAtErrorNullPointer;
    }

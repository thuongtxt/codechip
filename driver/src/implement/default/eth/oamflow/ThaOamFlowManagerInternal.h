/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaOamFlowManagerInternal.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : OAM flow manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAOAMFLOWMANAGERINTERNAL_H_
#define _THAOAMFLOWMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "AtChannelClasses.h"
#include "AtModuleEth.h"
#include "ThaOamFlowManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaOamFlowManagerMethods
    {
    AtEthFlow (*OamFlowCreate)(ThaOamFlowManager self, AtHdlcLink link);
    AtEthFlow (*OamFlowObjectCreate)(ThaOamFlowManager self, AtHdlcLink link);
    eAtRet (*OamFlowDelete)(ThaOamFlowManager self, AtEthFlow oamFlow);
    AtEthFlow (*OamMpFlowCreate)(ThaOamFlowManager self, AtHdlcBundle bundle);
    AtEthFlow (*OamMpFlowObjectCreate)(ThaOamFlowManager self, AtHdlcBundle bundle);
    eAtRet (*OamMpFlowDelete)(ThaOamFlowManager self, AtEthFlow oamFlow);
    eAtRet (*AllOamFlowsSourceMacAddressSet)(ThaOamFlowManager self, AtEthPort port, uint8 *address);
    }tThaOamFlowManagerMethods;

typedef struct tThaOamFlowManager
    {
    tAtObject super;
    const tThaOamFlowManagerMethods *methods;

    /* Private data */
    AtModuleEth ethModule;
    }tThaOamFlowManager;

typedef struct tThaOamFlowManagerDefault
    {
    tThaOamFlowManager super;

    /* Private data */
    AtList linkOamFlows;
    AtList bundleOamFlows;
    }tThaOamFlowManagerDefault;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaOamFlowManager ThaOamFlowManagerObjectInit(ThaOamFlowManager self, AtModuleEth ethModule);
ThaOamFlowManager ThaOamFlowManagerDefaultObjectInit(ThaOamFlowManager self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAOAMFLOWMANAGERINTERNAL_H_ */


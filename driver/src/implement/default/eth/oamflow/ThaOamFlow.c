/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaOamFlow.c
 *
 * Created Date: Nov 22, 2013
 *
 * Description : OAM flow associated with PPP Link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../ppp/ThaPppLinkInternal.h"
#include "ThaOamFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtHdlcLink hdlcLink = AtEthFlowHdlcLinkGet((AtEthFlow)self);
    if (hdlcLink)
        {
        AtHdlcLinkOamFlowDelete(hdlcLink);
        AtHdlcLinkOamFlowSet(hdlcLink, NULL);
        }

    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtEthFlow self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaOamFlow);
    }

static void Override(AtEthFlow self)
    {
    OverrideAtObject(self);
    }

AtEthFlow ThaOamFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    const uint32 cInvalidHwFlowId = 0xFFFFFFFF;

    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowObjectInit((AtEthFlow)self, flowId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* To indicate that there is no hardware flow is assigned to this flow */
    ((ThaEthFlow)self)->hwFlowId = cInvalidHwFlowId;

    return self;
    }

AtEthFlow ThaOamFlowNew(uint32 flowId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlow newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ThaOamFlowObjectInit(newFlow, flowId, module);
    }

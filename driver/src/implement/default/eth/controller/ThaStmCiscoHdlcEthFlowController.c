/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaStmCiscoHdlcEthFlowController.c
 *
 * Created Date: Jun 5, 2013
 *
 * Description : Thalassa Ethernet traffic flow controller implementation for Cisco-HDLC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../../man/ThaDeviceInternal.h"
#include "ThaEthFlowControllerInternal.h"
#include "ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(channel)  (ThaStmCiscoHdlcEthFlowController)channel

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasEthernetType(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
    return ThaEthFlowHeaderProviderCiscoFlowHasEthType(ThaEthFlowControllerHeaderProvider(self));
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(flow);
    return ThaEthFlowHeaderProviderCiscoEthHeaderNoVlanLengthGet(ThaEthFlowControllerHeaderProvider(self));
    }

static uint16 EthVlanLookupTypeGet(ThaEthFlowController self)
    {
	AtUnused(self);
    return 5;
    }

static void OverrideThaEthFlowController(ThaStmCiscoHdlcEthFlowController self)
    {
    ThaEthFlowController flow = (ThaEthFlowController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, mMethodsGet(flow), sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, HasEthernetType);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthVlanLookupTypeGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthHeaderNoVlanLengthGet);
        }

    mMethodsSet(flow, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaStmCiscoHdlcEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmCiscoHdlcEthFlowController);
    }

ThaEthFlowController ThaStmCiscoHdlcEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPppEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController ThaStmCiscoHdlcEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ThaStmCiscoHdlcEthFlowControllerObjectInit(newFlow, module);
    }

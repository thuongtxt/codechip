/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaStmPppEthFlowController.c
 *
 * Created Date: May 17, 2013
 *
 * Description : Thalassa Ethernet traffic flow controller implementation for PPP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"

#include "../../ppp/ThaStmPmcReg.h"
#include "../../ppp/ThaMpegStmReg.h"
#include "../../ppp/ThaPppLinkInternal.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../encap/ThaModuleEncapInternal.h"
#include "../../encap/ThaHdlcChannelInternal.h"

#include "../ThaEthFlowInternal.h"

#include "ThaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Counter ID of flow that map to link */
#define cThaPMCMpigLinkTxByte 11
#define cThaPMCMpigLinkTxPkt  10
#define cThaPMCMpegLinkRxByte 0
#define cThaPMCMpegLinkRxPkt  5
#define cThaPMCMpegLinkRxDisc 7

#define cThaMlpppIndrCounterCheckTime       8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(channel)  (ThaStmPppEthFlowController)channel

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Save super implementation */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterAddress(ThaEthFlowController self, AtChannel flow)
    {
    return mMethodsGet(self)->HwFlowId(self, (AtEthFlow)flow);
    }

static uint32 PmcMpigOffset(AtChannel flow, uint8 partId)
    {
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet(flow), cThaModulePmcMpig, partId);
    }

static eAtRet MpigCntGet(ThaEthFlowController self, AtChannel flow, tAtEthFlowCounters *pCounter, eBool r2c)
    {
    uint32 counterAddress;
    uint16 actualCounterAddress;
    uint8  counterPosition;
    AtOsal osal;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint8  i;
    static const uint32 cTimeoutMs = 20;
    uint8  partId = (uint8)mMethodsGet(self)->PartId(self, (AtEthFlow)flow);
    uint8  allCounters[] = {cThaPMCMpigLinkTxByte, cThaPMCMpigLinkTxPkt};
    uint32 pmcMpigOffset = PmcMpigOffset(flow, partId);

    if (!ThaPmcIndirectCounterIsAccessible(flow, cThaModulePmcMpig, partId))
        return cAtErrorDevBusy;

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    counterAddress = CounterAddress(self, flow);
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, counterAddress);

    /* Mask of counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self,
                        cThaRegThalassaPMMpxgCntsAddressRequestCtrl + pmcMpigOffset,
                        longRegVal,
                        cThaNumDwordsInLongReg,
                        cThaModulePmcMpig);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpig, cTimeoutMs, partId))
        return cAtErrorDevBusy;

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpig, cTimeoutMs, partId))
            return cAtOk;

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self,
                               cThaRegThalassaPMMpxgCntsIndrRegRptStat + pmcMpigOffset,
                               longRegVal,
                               cThaNumDwordsInLongReg,
                               cThaModulePmcMpig) == 0)
            return cAtErrorDevFail;

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &actualCounterAddress);
        if (actualCounterAddress != counterAddress)
            return cAtErrorDevFail;

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);

        if (pCounter)
            {
            mPmcCounterGet(PMCMpigLinkTxByte, &(pCounter->txByte));
            mPmcCounterGet(PMCMpigLinkTxPkt, &(pCounter->txPacket));
            pCounter->txGoodByte = pCounter->txByte;
            pCounter->txGoodPkt  = pCounter->txPacket;
            }
        }

    return cAtOk;
    }

static uint32 PmcMpegOffset(AtChannel flow, uint8 partId)
    {
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet(flow), cThaModulePmcMpeg, partId);
    }

static eAtRet MpegCntGet(ThaEthFlowController self, AtChannel flow, tAtEthFlowCounters *pCounter, eBool r2c)
    {
    uint32 counterAddress;
    uint16 actualCounterAddress;
    uint8  counterPosition;
    AtOsal osal;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint8  i;
    static const uint32 cTimeoutMs = 20;
    uint8  partId = (uint8)mMethodsGet(self)->PartId(self, (AtEthFlow)flow);
    uint8  allCounters[] = {cThaPMCMpegLinkRxByte,
                             cThaPMCMpegLinkRxPkt,
                             cThaPMCMpegLinkRxDisc};

    uint32 pmcMpegOffset = PmcMpegOffset(flow, partId);

    if (!ThaPmcIndirectCounterIsAccessible(flow, cThaModulePmcMpeg, partId))
        return cAtErrorDevBusy;

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    counterAddress = CounterAddress(self, flow);
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, counterAddress);

    /* Mask of counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self,
                        cThaRegThalassaPMMpxgCntsAddressRequestCtrl + pmcMpegOffset,
                        longRegVal,
                        cThaNumDwordsInLongReg,
                        cThaModulePmcMpeg);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpeg, cTimeoutMs, partId))
        return cAtErrorDevBusy;

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpeg, cTimeoutMs, partId))
            return cAtOk;

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self,
                               cThaRegThalassaPMMpxgCntsIndrRegRptStat + pmcMpegOffset,
                               longRegVal,
                               cThaNumDwordsInLongReg,
                               cThaModulePmcMpeg) == 0)
            return cAtErrorDevFail;

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &actualCounterAddress);
        if (actualCounterAddress != counterAddress)
            return cAtErrorDevFail;

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);

        if (pCounter)
            {
            mPmcCounterGet(PMCMpegLinkRxByte, &(pCounter->rxByte));
            mPmcCounterGet(PMCMpegLinkRxPkt , &(pCounter->rxPacket));
            mPmcCounterGet(PMCMpegLinkRxDisc, &(pCounter->rxDiscardPkt));
            pCounter->rxGoodPkt  = pCounter->rxPacket;
            pCounter->rxGoodByte = pCounter->rxByte;
            }
        }

    return cAtOk;
    }

static eBool IsSimulated(AtChannel flow)
    {
    AtDevice device = AtChannelDeviceGet(flow);
    return AtDeviceIsSimulated(device);
    }

static eAtRet AllCountersReadToClear(ThaEthFlowController self, AtChannel flow, void *pAllCounters, eBool clear)
    {
    eAtRet ret;
    AtOsal osal = AtSharedDriverOsalGet();
    tAtEthFlowCounters *counters = (tAtEthFlowCounters *)pAllCounters;

    if (counters == NULL)
        return cAtErrorNullPointer;

    mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(tAtEthFlowCounters));

    if (IsSimulated(flow))
        return cAtOk;

    ret = MpegCntGet(self, flow, counters, clear);
    if (ret != cAtOk)
        return ret;

    ret = MpigCntGet(self, flow, counters, clear);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet AllCountersGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return AllCountersReadToClear(self, (AtChannel)flow, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return AllCountersReadToClear(self, (AtChannel)flow, pAllCounters, cAtTrue);
    }

static void OverrideThaEthFlowController(ThaStmPppEthFlowController self)
    {
    ThaEthFlowController flow = (ThaEthFlowController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(flow);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersClear);
        }

    mMethodsSet(flow, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaStmPppEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPppEthFlowController);
    }

ThaEthFlowController ThaStmPppEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController ThaStmPppEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPppEthFlowControllerObjectInit(newFlow, module);
    }

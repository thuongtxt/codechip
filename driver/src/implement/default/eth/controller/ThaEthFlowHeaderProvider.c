/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthFlowHeaderProvider.c
 *
 * Created Date: Nov 28, 2013
 *
 * Description : ETH Flow header provider.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaEthFlowHeaderProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEthFlowHeaderProviderMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PsnMode(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return 0xA;
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    /* Default Ethernet header includes DA, SA and Ethernet type */
    return cNumByteOfDestMac + cNumByteOfSourceMac + cNumByteOfEthernetType;
    }

static uint16 CiscoEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    /* In case of Cisco HDLC, there is no field Ethernet Type */
    return cNumByteOfDestMac + cNumByteOfSourceMac;
    }

static uint16 OamEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
    return mMethodsGet(self)->EthHeaderNoVlanLengthGet(self);
    }

static uint32 VlanIdSw2Hw(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId)
    {
	AtUnused(flow);
	AtUnused(self);
    return vlanId;
    }

static eBool PppFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool MpFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool CiscoFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool OamFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint8 *MpFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnType)
    {
    static uint8 arrayType = cThaArrMlpppPktPsn;
	AtUnused(self);
    if (numPsnType)
        *numPsnType = 1;
    return &arrayType;
    }

static uint8 *OamFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnType)
    {
    static uint8 arrayType = cThaArrPppCtrlPsn;
	AtUnused(self);
    if (numPsnType)
        *numPsnType = 1;
    return &arrayType;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaEthFlowHeaderProvider object = (ThaEthFlowHeaderProvider)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(ethModule);
    }

static void OverrideAtObject(ThaEthFlowHeaderProvider self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaEthFlowHeaderProvider self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthFlowHeaderProvider);
    }

static void MethodsInit(ThaEthFlowHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PsnMode);
        mMethodOverride(m_methods, EthHeaderNoVlanLengthGet);
        mMethodOverride(m_methods, CiscoEthHeaderNoVlanLengthGet);
        mMethodOverride(m_methods, OamEthHeaderNoVlanLengthGet);
        mMethodOverride(m_methods, VlanIdSw2Hw);
        mMethodOverride(m_methods, PppFlowHasEthType);
        mMethodOverride(m_methods, MpFlowHasEthType);
        mMethodOverride(m_methods, OamFlowHasEthType);
        mMethodOverride(m_methods, CiscoFlowHasEthType);
        mMethodOverride(m_methods, MpFlowPsnArrayTypes);
        mMethodOverride(m_methods, OamFlowPsnArrayTypes);
        }

    mMethodsSet(self, &m_methods);
    }

ThaEthFlowHeaderProvider ThaEthFlowHeaderProviderObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->ethModule = ethModule;

    return self;
    }

ThaEthFlowHeaderProvider ThaEthFlowHeaderProviderNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return ThaEthFlowHeaderProviderObjectInit(newProvider, ethModule);
    }

AtModuleEth ThaEthFlowHeaderProviderModuleGet(ThaEthFlowHeaderProvider self)
    {
    return self ? self->ethModule : NULL;
    }

uint8 ThaEthFlowHeaderProviderPsnMode(ThaEthFlowHeaderProvider self)
    {
    if (self)
        return mMethodsGet(self)->PsnMode(self);
    return 0x0;
    }

uint16 ThaEthFlowHeaderProviderEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
    if (self)
        return mMethodsGet(self)->EthHeaderNoVlanLengthGet(self);
    return 0x0;
    }

uint16 ThaEthFlowHeaderProviderCiscoEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
    if (self)
        return mMethodsGet(self)->CiscoEthHeaderNoVlanLengthGet(self);
    return 0x0;
    }

uint16 ThaEthFlowHeaderProviderOamEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
    if (self)
        return mMethodsGet(self)->OamEthHeaderNoVlanLengthGet(self);
    return 0;
    }

uint32 ThaEthFlowHeaderProviderVlanIdSw2Hw(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId)
    {
    if (self)
        return mMethodsGet(self)->VlanIdSw2Hw(self, flow, vlanId);
    return cInvalidValue;
    }

eBool ThaEthFlowHeaderProviderOamFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
    if (self)
        return mMethodsGet(self)->OamFlowHasEthType(self);
    return cAtFalse;
    }

eBool ThaEthFlowHeaderProviderCiscoFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
    if (self)
        return mMethodsGet(self)->CiscoFlowHasEthType(self);
    return cAtFalse;
    }

uint8 *ThaEthFlowHeaderProviderPppFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes)
    {
    static uint8 arrayType = cThaArrPppPktPsn;
	AtUnused(self);

    if (numPsnTypes)
        *numPsnTypes = 1;
    return &arrayType;
    }

uint8 *ThaEthFlowHeaderProviderMpFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes)
    {
    if (self)
        return mMethodsGet(self)->MpFlowPsnArrayTypes(self, numPsnTypes);
    return NULL;
    }

uint8 *ThaEthFlowHeaderProviderOamFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes)
    {
    if (self)
        return mMethodsGet(self)->OamFlowPsnArrayTypes(self, numPsnTypes);
    return NULL;
    }

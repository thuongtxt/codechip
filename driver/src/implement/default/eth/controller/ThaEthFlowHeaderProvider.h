/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthFlowHeaderProvider.h
 * 
 * Created Date: Nov 28, 2013
 *
 * Description : ETH Flow header provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWHEADERPROVIDER_H_
#define _THAETHFLOWHEADERPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eThaPsnArrayType
    {
    cThaArrPppCtrlPsn   = 0x4,
    cThaArrPppPktPsn    = 0x5,
    cThaArrMlpppCtrlPsn = 0x8,
    cThaArrMlpppPktPsn  = 0x9
    }eThaPsnArrayType;

typedef struct tThaEthFlowHeaderProvider * ThaEthFlowHeaderProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowHeaderProvider ThaEthFlowHeaderProviderNew(AtModuleEth ethModule);

uint8 ThaEthFlowHeaderProviderPsnMode(ThaEthFlowHeaderProvider self);
uint32 ThaEthFlowHeaderProviderVlanIdSw2Hw(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId);

uint8 *ThaEthFlowHeaderProviderPppFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes);
uint8 *ThaEthFlowHeaderProviderMpFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes);
uint8 *ThaEthFlowHeaderProviderOamFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes);

eBool ThaEthFlowHeaderProviderOamFlowHasEthType(ThaEthFlowHeaderProvider self);
eBool ThaEthFlowHeaderProviderCiscoFlowHasEthType(ThaEthFlowHeaderProvider self);

eBool ThaEthFlowHeaderProviderHasEthType(ThaEthFlowHeaderProvider self);
uint16 ThaEthFlowHeaderProviderEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self);
uint16 ThaEthFlowHeaderProviderCiscoEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self);
uint16 ThaEthFlowHeaderProviderOamEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self);

AtModuleEth ThaEthFlowHeaderProviderModuleGet(ThaEthFlowHeaderProvider self);

/* Product concretes */
ThaEthFlowHeaderProvider Tha60060011EthFlowHeaderProviderNew(AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60030051EthFlowHeaderProviderNew(AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60091023EthFlowHeaderProviderNew(AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60091132EthFlowHeaderProviderNew(AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60031032EthFlowHeaderProviderNew(AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60091135EthFlowHeaderProviderNew(AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60031035EthFlowHeaderProviderNew(AtModuleEth ethModule);
ThaEthFlowHeaderProvider Tha60290081EthFlowHeaderProviderNew(AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWHEADERPROVIDER_H_ */


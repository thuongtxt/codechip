/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaMpEthFlowController.c
 *
 * Created Date: May 16, 2013
 *
 * Description : Thalassa Ethernet traffic flow controller implementation for MLPPP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModulePpp.h"
#include "../../../../generic/encap/AtHdlcLinkInternal.h"
#include "../../ppp/ThaPppLinkInternal.h"
#include "../../ppp/ThaMpigReg.h"
#include "../../ppp/ThaMpegReg.h"
#include "../../ppp/ThaPmcReg.h"
#include "../../ppp/ThaModulePppInternal.h"
#include "../../ppp/ThaMpBundle.h"
#include "../../cla/ThaModuleCla.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../util/ThaUtil.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../ThaEthFlowInternal.h"
#include "../ThaModuleEthInternal.h"
#include "ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(channel)  (ThaMpEthFlowController)channel
#define mAddressOfCntRegister(controller, flow, address)                       \
     mMethodsGet((ThaMpEthFlowController)controller)->AddressOfCntRegister((ThaMpEthFlowController)controller, flow, address);

/* Interrupt resequence */
#define cThaRsDuplIntr        cBit15
#define cThaRsqPaChklntr      cBit14
#define cThaRsqPaDisclntr     cBit13
#define cThaRsqPaMrruIntr     cBit12
#define cThaRsqPaSeqIntr      cBit11
#define cThaRsqPaMsbIntr      cBit10
#define cThaRsqPaOutIntr      cBit9
#define cThaRsqPaMsruIntr     cBit8
#define cThaRsqLostMsbIntr    cBit7
#define cThaRsqFrgWindIntr    cBit6
#define cThaRsqFrgThshIntr    cBit5
#define cThaRsqTimeOutIntr    cBit4
#define cThaRsqFgbFulIntr     cBit3
#define cThaRsqFlowBusyIntr   cBit2
#define cThaRsqLbpOverIntr    cBit1
#define cThaRsqRsFsmSChgIntr  cBit0

#define cThaMaxNumberOfVlanOnFlow 8
#define cEthFlowInvalidHwFlowId 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaMpEthFlowControllerMethods m_methods;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Save super implementation */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MpFlowId(ThaEthFlow flow)
    {
    return flow->hwFlowId;
    }

static uint32 HwFlowId(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(self);
    /* In this case, it is MP flow ID */
    return MpFlowId((ThaEthFlow)flow);
    }

static ThaMpBundle BundleOfFlow(ThaEthFlow self)
    {
    return (ThaMpBundle)(AtEthFlowHdlcBundleGet((AtEthFlow)self));
    }

static uint32 LocalBundleId(ThaEthFlow self)
    {
    AtModulePpp pppModule;

    if (BundleOfFlow(self))
        return ThaModulePppLocalBundleId(BundleOfFlow(self));

    /* Return invalid ID */
    pppModule = (AtModulePpp)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePpp);
    return AtModulePppMaxBundlesGet(pppModule);
    }

static ThaModuleCla ClaModule(ThaEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

/*
 * If lookup VLAN to Mp-Flow, offset is Mp-Flow ID
 * If lookup VLAN to Bundle, offset is Bund ID * 8 + PRICode
 */
static uint32 ClaFlowLookupCtrlOffset(ThaEthFlow flow, uint8 vlanPrio)
    {
    if (ThaModuleClaPppVlanLookupModeGet(ClaModule(flow), flow))
        return MpFlowId(flow);
    else
        return (LocalBundleId(flow) * cThaMaxNumberOfVlanOnFlow) + vlanPrio + ThaMpBundlePartOffset(BundleOfFlow(flow));
    }

/*
 * If lookup VLAN to Mp-Flow, return MP flow ID
 * If lookup VLAN to Bundle, return Bundle ID
 */
static uint32 VlanLookupId(ThaEthFlowController self, AtEthFlow flow)
    {
    ThaEthFlow thaFlow = (ThaEthFlow)flow;
	AtUnused(self);

    if (ThaModuleClaPppVlanLookupModeGet(ClaModule(thaFlow), thaFlow))
        return MpFlowId(thaFlow);

    return LocalBundleId(thaFlow);
    }

static uint8 PartId(ThaEthFlowController self, AtEthFlow flow)
    {
    ThaModulePpp modulePpp = (ThaModulePpp)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cAtModulePpp);
	AtUnused(self);
    return ThaMpBundlePartOfPpp(modulePpp, BundleOfFlow((ThaEthFlow)flow));
    }

static void DebugResequenceIntrPrint(uint32 intrBitmap)
    {
    /* Print interrupt alarm status link  */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Internal status: ");
    if (intrBitmap == 0)
        {
        AtPrintc(cSevInfo, "None\r\n");
        return;
        }

    if (intrBitmap & cThaRsDuplIntr      ) AtPrintc(cSevCritical, " RsqRsDupl");
    if (intrBitmap & cThaRsqPaChklntr    ) AtPrintc(cSevCritical, " RsqPaChk");
    if (intrBitmap & cThaRsqPaDisclntr   ) AtPrintc(cSevCritical, " RsqPaDisc");
    if (intrBitmap & cThaRsqPaMrruIntr   ) AtPrintc(cSevCritical, " RsqPaMrru");
    if (intrBitmap & cThaRsqPaSeqIntr    ) AtPrintc(cSevCritical, " RsqPaSeq");
    if (intrBitmap & cThaRsqPaMsbIntr    ) AtPrintc(cSevCritical, " RsqPaMsb");
    if (intrBitmap & cThaRsqPaOutIntr    ) AtPrintc(cSevCritical, " RsqPaOut");
    if (intrBitmap & cThaRsqPaMsruIntr   ) AtPrintc(cSevCritical, " RsqPaMsru");
    if (intrBitmap & cThaRsqLostMsbIntr  ) AtPrintc(cSevCritical, " RsqLostMsb");
    if (intrBitmap & cThaRsqFrgWindIntr  ) AtPrintc(cSevCritical, " RsqFrgWind");
    if (intrBitmap & cThaRsqFrgThshIntr  ) AtPrintc(cSevCritical, " RsqFrgThsh");
    if (intrBitmap & cThaRsqTimeOutIntr  ) AtPrintc(cSevCritical, " RsqTimeOut");
    if (intrBitmap & cThaRsqFgbFulIntr   ) AtPrintc(cSevCritical, " RsqFgbFul");
    if (intrBitmap & cThaRsqFlowBusyIntr ) AtPrintc(cSevCritical, " RsqFlowBusy");
    if (intrBitmap & cThaRsqLbpOverIntr  ) AtPrintc(cSevCritical, " RsqLbpOver");
    if (intrBitmap & cThaRsqRsFsmSChgIntr) AtPrintc(cSevCritical, " RsqRsFsmSChg");

    AtPrintc(cSevNormal, "\r\n");
    }

static uint32 MpigOffset(ThaEthFlow flow)
    {
    uint8 partId = ThaEthFlowControllerPartIdGet(ThaEthFlowControllerGet(flow), (AtEthFlow)flow);
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet((AtChannel)flow), cThaModulePmcMpig, partId);
    }

static void DebugResequenceStatusPrint(ThaEthFlow flow)
    {
    uint32 address;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint32 currTxSeq;
    uint32 regValue;
    uint32 hwFlowId = MpFlowId(flow);

    address = cThaRegMPIGRSQResequenceStat + hwFlowId + MpigOffset(flow);
    mChannelHwLongRead(flow, address, longRegVal, cThaNumDwordsInLongReg, cThaModuleMpig);

    /* Print interrupt alarm status link  */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Interrupt Link status:\r\n");

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsPktCDwIndex], cThaMPIGRsqCurrRsPktC);
    AtPrintc(cSevNormal, "  rsqCurrRsMaxSta : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsBlkCDwIndex], cThaMPIGRsqCurrRsBlkC);
    AtPrintc(cSevNormal, "  rsqCurrRsBlkC   : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsLongDwIndex], cThaMPIGRsqCurrRsLong);
    AtPrintc(cSevNormal, "  rsqCurrRsLong   : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsMaxStaDwIndex], cThaMPIGRsqCurrRsMaxSta);
    AtPrintc(cSevNormal, "  rsqCurrRsMaxSta : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsBuffDwIndex], cThaMPIGRsqCurrRsBuff);
    AtPrintc(cSevNormal, "  rsqEarlySqCnt   : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrPaHeadDwIndex], cThaMPIGRsqCurrPaHead);
    AtPrintc(cSevNormal, "  rsqCurSq        : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsExptDwIndex], cThaMPIGRsqCurrRsExpt);
    AtPrintc(cSevNormal, "  rsqExptSq       : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsCntDwIndex], cThaMPIGRsqCurrRsCnt);
    AtPrintc(cSevNormal, "  rsqRsqCnt       : %u\r\n", regValue);

    regValue = mRegField(longRegVal[cThaMPIGRsqCurrRsFsmDwIndex], cThaMPIGRsqCurrRsFsm);
    AtPrintc(cSevNormal, "  rsqRsFsm        : %u\r\n", regValue);

    address = cThaMPEGFlowSequenceStatus + MpigOffset(flow);
    regValue = mChannelHwRead(flow, address, cThaModuleMpeg);

    currTxSeq = mRegField(regValue, cThaMPEGMpFlowCurrSequence);
    AtPrintc(cSevNormal, "  txSequence      : %u\r\n", currTxSeq);
    }

static uint32 RsqIntrDefaultOffset(ThaEthFlow self)
    {
    uint16 idValue, bitId;
    uint32 partOffset = MpigOffset(self);

    idValue = (uint16)((MpFlowId(self) & cBit10_5) >> 5);
    bitId = (uint16)(MpFlowId(self) & cBit4_0);
    return (uint32)((idValue * 32UL) + bitId + partOffset);
    }

static eAtRet Debug(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 address, regValue;
    uint16 intrBitmap;
    ThaEthFlow thaFlow = (ThaEthFlow)flow;

    /* Get Link interrupt status */
    address = cThaRegMPIGRSQPerAlmIntrStat + RsqIntrDefaultOffset(thaFlow);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    intrBitmap = regValue & cBit15_0;

    /* Print Link interrupt status */
    DebugResequenceIntrPrint(intrBitmap);

    /* Clear Interrupt status */
    address = cThaRegMPIGRSQPerAlmIntrStat + RsqIntrDefaultOffset(thaFlow);
    mChannelHwWrite(self, address, cBit15_0, cThaModuleMpig);

    /* Print resequence status */
    DebugResequenceStatusPrint(thaFlow);

    AtPrintc(cSevNormal, "* Hardware information\r\n");
    AtPrintc(cSevNormal, "  Hardware flowId: %u\r\n", MpFlowId(thaFlow));

    return cAtOk;
    }

static uint32 MpigDatDeQueRegisterAddress(ThaEthFlowController self)
    {
	AtUnused(self);
    return cThaRegMPIGDATDeQueMpFlowCtrl;
    }

static uint16 EthVlanLookupTypeGet(ThaEthFlowController self)
    {
    static const uint8 cAtEthFlowMlpppLookupType = 0;
	AtUnused(self);
    return cAtEthFlowMlpppLookupType;
    }

static uint32 AddressOfCntRegister(ThaMpEthFlowController self, AtEthFlow flow, uint32 baseAddress)
    {
	AtUnused(self);
    return MpFlowId((ThaEthFlow)flow) + MpigOffset((ThaEthFlow)flow) + baseAddress;
    }

static eAtRet FlowMpigCntGet(ThaEthFlowController self, AtEthFlow flow, tAtEthFlowCounters *pCounter, eBool r2c)
    {
    uint32  address, value;
    uint32  readOnlyOffset;

    /* Get offset counter */
    readOnlyOffset = (r2c == cAtTrue) ? 0 : cThaAddrOffsetCounterReadOnly;

    /* Get good bytes count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpigFlowGoodBytePkt(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpig);
    if (pCounter)
        pCounter->txGoodByte = value;

    /* Get good packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpigFlowGoodPkt(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpig);
    if (pCounter)
        pCounter->txGoodPkt = value;

    /* Get good fragment packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpigFlowGoodFragment(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpig);
    if (pCounter)
        pCounter->txGoodFragment = value;

    /* Get Mrru exceed packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpigFlowMRRUPkt(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpig);
    if (pCounter)
        pCounter->mrruExceed = value;

    /* Get total fragment packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpigFlowTotalFragment(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpig);
    if (pCounter)
        pCounter->txFragment = value;

    /* Get violation fragment packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpigFlowViolateMFragment(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpig);
    if (pCounter)
        pCounter->windowViolation = value;

    /* Get discard fragment packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpigFlowDiscardFragmentPkt(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpig);
    if (pCounter)
        pCounter->txDiscardFragment = value;

    return cAtOk;
    }

static eAtRet FlowMpegCntGet(ThaEthFlowController self, AtEthFlow flow, tAtEthFlowCounters *pCounter, eBool r2c)
    {
    uint32  address, value;
    uint32  readOnlyOffset;

    /* Get offset counter */
    readOnlyOffset = (r2c == cAtTrue) ? 0 : cThaAddrOffsetCounterReadOnly;

    /* Get good byte count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpegFlowGoodByte(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpeg);
    if (pCounter)
        pCounter->rxGoodByte = value;

    /* Get good packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpegFlowGoodPkt(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpeg);
    if (pCounter)
        pCounter->rxGoodPkt = value;

    /* Get fragment packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpegFlowFragment(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpeg);
    if (pCounter)
        pCounter->rxFragment = value;

    /* Get discard packet count */
    address = mAddressOfCntRegister(self, flow, cThaRegPmcMpegFlowDiscardPkt(readOnlyOffset));
    value = mChannelHwRead(self, address, cThaModuleMpeg);
    if (pCounter)
        pCounter->rxDiscardPkt = value;

    return cAtOk;
    }

static eAtRet AllCountersGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    eAtRet ret = cAtOk;
    tAtEthFlowCounters *counters = (tAtEthFlowCounters*) pAllCounters;
    AtOsal osal = AtSharedDriverOsalGet();

    if (counters == NULL)
        return cAtErrorNullPointer;

    mMethodsGet(osal)->MemInit(osal, pAllCounters, 0, sizeof(counters));
    ret |= FlowMpegCntGet(self, flow, counters, cAtFalse);
    ret |= FlowMpigCntGet(self, flow, counters, cAtFalse);

    return ret;
    }

static eAtRet AllCountersClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    eAtRet ret = cAtOk;
    tAtEthFlowCounters *counters = (tAtEthFlowCounters*) pAllCounters;
    AtOsal osal = AtSharedDriverOsalGet();

    if (counters == NULL)
        return cAtErrorNullPointer;

    mMethodsGet(osal)->MemInit(osal, counters, sizeof(counters), 0);
    ret |= FlowMpegCntGet(self, flow, counters, cAtTrue);
    ret |= FlowMpigCntGet(self, flow, counters, cAtTrue);

    return ret;
    }

static eAtRet FlowIdAllocate(ThaEthFlow self)
    {
    uint32 freeFlow;
    eAtRet ret;
    uint32 flowIdInPart;
    ThaModuleEth ethModule  = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 numMpFlowPerPart = ThaModuleEthNumMpFlowPerPart(ethModule);

    if (numMpFlowPerPart == 0)
    	return cAtErrorRsrcNoAvail;

    /* Get free flow */
    freeFlow = ThaModuleEthFreeMpFlowGet(ethModule);
    if (!ThaModuleEthMpFlowIsValid(ethModule, freeFlow))
        return cAtErrorRsrcNoAvail;

    /* Use this */
    ret = ThaModuleEthMpFlowUse(ethModule, freeFlow);
    if (ret != cAtOk)
        return ret;

    flowIdInPart = freeFlow % numMpFlowPerPart;
    self->hwFlowId = flowIdInPart;

    return cAtOk;
    }

static uint32 BundlePartOffset(ThaEthFlow self)
    {
    if (BundleOfFlow(self) == NULL)
        return 200;

    return LocalBundleId(self) + ThaMpBundlePartOffset(BundleOfFlow(self));
    }

static uint32 MaxThreshold(uint8 seqMd)
    {
    return (seqMd == cThaMLPSequenceModeShort) ? 3000 : 7000;
    }

static uint32 AdjustThreshold(ThaEthFlow self, uint32 threshold)
    {
    uint32 regAddr, regVal;
    uint8  seqMd;

    regAddr = cThaRegMPIGDATLookupBundleCtrl + BundlePartOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldGet(regVal, cThaMLPSequenceModeMask, cThaMLPSequenceModeShift, uint8, &seqMd);

    return (threshold > MaxThreshold(seqMd)) ? MaxThreshold(seqMd) : threshold;
    }

static uint32 DefaultThreshold(ThaEthFlow self)
    {
    const uint32 cFragmentSize        = 64;
    const uint32 cThaEthFlowBandwidth = 64000;
    uint32 threshold = (cThaEthFlowBandwidth * ThaEthFlowMaxDelayGet(self)) / (8 * cFragmentSize);
    return AdjustThreshold(self, threshold);
    }

static eAtRet ThresholdSet(ThaEthFlow self)
    {
    uint32 regAddr, regVal;
    uint32 threshold = DefaultThreshold(self);

    regAddr = cThaRegMPIGRSQResequenceCtrl + MpFlowId(self) + MpigOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, cThaMPIGRsqCtlThshMask, cThaMPIGRsqCtlThshShift, threshold);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel, uint8 classNumber)
    {
    eAtRet     ret;
    AtMpBundle bundle = (AtMpBundle)channel;
    ThaEthFlow flow   = (ThaEthFlow)ethFlow;

    /* Do nothing if this flow is already added to this bundle with same class number */
    if (((AtMpBundle)BundleOfFlow(flow) == bundle) && (ThaEthFlowClassInMpBundleGet(flow) == classNumber))
        return cAtOk;

    /* Allocate hardware flow ID */
    ret = FlowIdAllocate(flow);
    if (ret != cAtOk)
        return ret;

    /* Update database */
    AtEthFlowHdlcBundleSet(ethFlow, (AtHdlcBundle)bundle);
    flow->classId  = classNumber;

    /* Super do common progress */
    ret = m_ThaEthFlowControllerMethods->Activate(self, ethFlow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    /* Configure HW flow */
    ThaEthFlowMaxDelaySet(flow, AtHdlcBundleMaxDelayGet((AtHdlcBundle)bundle));
    ThaEthFlowMrruSet(flow, AtMpBundleMrruGet(bundle));
    AtHdlcBundleFragmentSizeSet((AtHdlcBundle)bundle, AtHdlcBundleFragmentSizeGet((AtHdlcBundle)bundle));
    ThresholdSet(flow);

    return cAtOk;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    ThaEthFlow   thaFlow   = (ThaEthFlow)flow;
    eAtRet ret;

    m_ThaEthFlowControllerMethods->DeActivate(self, flow);
    ret = ThaEthFlowResequenceBufferRelease(flow);

    /* Update database */
    ThaEthFlowControllerSet((ThaEthFlow)flow, NULL);
    ThaModuleEthMpFlowUnuse(ethModule, MpFlowId(thaFlow));
    AtEthFlowHdlcBundleSet(flow, NULL);
    thaFlow->classId  = 0xFF;
    thaFlow->hwFlowId = cEthFlowInvalidHwFlowId;

    return ret;
    }

static uint8 *PsnArrayTypes(ThaEthFlowController self, uint8 *numPsnTypes)
    {
    return ThaEthFlowHeaderProviderMpFlowPsnArrayTypes(ThaEthFlowControllerHeaderProvider(self), numPsnTypes);
    }

static uint32 FlowLookupId(ThaEthFlow flow)
    {
    /* If Lookup VLAN to mp flow, return Bundle ID
     * If lookup VLAN to bundle, return MP flow ID */
    return ThaModuleClaPppVlanLookupModeGet(ClaModule(flow), flow) ? LocalBundleId(flow) : MpFlowId(flow);
    }

static eAtRet IngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    ThaEthFlow thaFlow = (ThaEthFlow)flow;
    uint32 vlanOffSet = ClaFlowLookupCtrlOffset(thaFlow, ThaEthFlowVlanPriorityGet(thaFlow, desc));

    /* Super */
    m_ThaEthFlowControllerMethods->IngressVlanEnable(self, flow, desc);

    return ThaModuleClaFlowLookupCtrl(ClaModule(thaFlow), thaFlow, FlowLookupId(thaFlow), ThaEthFlowClassInMpBundleGet(thaFlow), 0x0, vlanOffSet, cAtTrue);
    }

static eAtRet IngressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    uint32 vlanOffSet;
    ThaEthFlow thaFlow      = (ThaEthFlow)flow;
    uint32 numberOfVlanDesc = AtEthFlowIngressNumVlansGet(flow);

    m_ThaEthFlowControllerMethods->IngressVlanDisable(self, flow, desc);

    /* Disable when lookup from VLAN priority to mp flow ID.
     * When lookup from MpFlowID to Bundle id, disable only when this is the last VLAN of flow */
    vlanOffSet = ClaFlowLookupCtrlOffset(thaFlow, ThaEthFlowVlanPriorityGet(thaFlow, desc));
    if (!ThaModuleClaPppVlanLookupModeGet(ClaModule(thaFlow), thaFlow) || (numberOfVlanDesc == 1))
        ThaModuleClaFlowLookupCtrl(ClaModule(thaFlow), thaFlow, 0, 0, 0, vlanOffSet, cAtFalse);

    return cAtOk;
    }

static void OverrideThaEthFlowController(ThaMpEthFlowController self)
    {
    ThaEthFlowController flow = (ThaEthFlowController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(flow);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthVlanLookupTypeGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, HwFlowId);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnArrayTypes);
        mMethodOverride(m_ThaEthFlowControllerOverride, MpigDatDeQueRegisterAddress);
        mMethodOverride(m_ThaEthFlowControllerOverride, VlanLookupId);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, IngressVlanDisable);
        mMethodOverride(m_ThaEthFlowControllerOverride, Debug);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersClear);
        mMethodOverride(m_ThaEthFlowControllerOverride, PartId);
        }

    mMethodsSet(flow, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaMpEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static void MethodsInit(ThaMpEthFlowController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, AddressOfCntRegister);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaMpEthFlowController);
    }

ThaEthFlowController ThaMpEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController ThaMpEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ThaMpEthFlowControllerObjectInit(newFlow, module);
    }


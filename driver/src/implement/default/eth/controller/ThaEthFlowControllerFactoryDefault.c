/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthFlowControllerFactory.c
 *
 * Created Date: Apr 4, 2015
 *
 * Description : ETH Flow controller factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthFlowControllerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerFactoryMethods m_ThaEthFlowControllerFactoryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowController PppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaPppEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController OamFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaOamFlowControllerNew(ethModule);
    }

static ThaEthFlowController MpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaMpEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController CiscoHdlcFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaCiscoHdlcEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController FrFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaFrEthFlowControllerNew(ethModule);
    }

static void OverrideThaEthFlowControllerFactory(ThaEthFlowControllerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerFactoryOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerFactoryOverride));

        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, PppFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, MpFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, CiscoHdlcFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, FrFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, OamFlowControllerCreate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerFactoryOverride);
    }

static void Override(ThaEthFlowControllerFactory self)
    {
    OverrideThaEthFlowControllerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthFlowControllerFactoryDefault);
    }

ThaEthFlowControllerFactory ThaEthFlowControllerFactoryDefaultObjectInit(ThaEthFlowControllerFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Constructor */
    if (ThaEthFlowControllerFactoryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowControllerFactory ThaEthFlowControllerFactoryDefault(void)
    {
    static ThaEthFlowControllerFactory m_factory = NULL;
    static tThaEthFlowControllerFactoryDefault factory;

    if (m_factory == NULL)
        m_factory = ThaEthFlowControllerFactoryDefaultObjectInit((ThaEthFlowControllerFactory)&factory);
    return m_factory;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthFlowController.h
 * 
 * Created Date: May 14, 2013
 *
 * Description : Eth flow controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWCONTROLLER_H_
#define _THAETHFLOWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete controllers for PDH products (work with LIU) */
ThaEthFlowController ThaMpEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController ThaPppEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController ThaCiscoHdlcEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController ThaFrEthFlowControllerNew(AtModuleEth module);

/* Concrete controllers for STM products */
ThaEthFlowController ThaStmMpEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController ThaStmPppEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController ThaStmCiscoHdlcEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController ThaStmFrEthFlowControllerNew(AtModuleEth module);

ThaEthFlowController ThaOamFlowControllerNew(AtModuleEth module);
ThaEthFlowController ThaPacketEthFlowControllerNew(AtModuleEth module);

uint8 ThaEthFlowControllerPartIdGet(ThaEthFlowController self, AtEthFlow flow);
ThaEthFlowHeaderProvider ThaEthFlowControllerHeaderProvider(ThaEthFlowController self);

void ThaEthFlowControllerFlowControlThresholdInit(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel);
eAtRet ThaEthFlowControllerFlowActivate(ThaEthFlowController flowController, AtEthFlow flow, AtChannel channel, uint8 classNumber);
eAtRet ThaEthFlowControllerFlowDeActivate(ThaEthFlowController self, AtEthFlow flow);
uint32 ThaEthFlowControllerMpigDatDeQueRegisterAddress(ThaEthFlowController self);

/* Product concretes */
ThaEthFlowController Tha60031032MpEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60031032OamFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60031032PppEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60060011StmPppEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60060011StmMpEthFlowControllerNew(AtModuleEth module);
ThaEthFlowController Tha60060011OamFlowControllerNew(AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWCONTROLLER_H_ */


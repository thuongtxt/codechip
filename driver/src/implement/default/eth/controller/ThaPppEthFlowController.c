/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaPppEthFlowController.c
 *
 * Created Date: May 16, 2013
 *
 * Description : Thalassa Ethernet traffic flow controller implementation for PPP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModulePpp.h"

#include "../../../../generic/encap/AtHdlcLinkInternal.h"

#include "../../ppp/ThaPppLinkInternal.h"
#include "../../cla/ThaModuleCla.h"
#include "../../ppp/ThaMpigReg.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../util/ThaUtil.h"
#include "../../encap/ThaModuleEncap.h"
#include "../../encap/ThaHdlcChannelInternal.h"
#include "../../sdh/ThaModuleSdh.h"

#include "../ThaEthFlowInternal.h"
#include "ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  (ThaPppEthFlowController)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Save super implementation */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPppLink LinkOfFlow(ThaEthFlow self)
    {
    return (ThaPppLink)AtEthFlowHdlcLinkGet((AtEthFlow)self);
    }

static uint32 LinkIdOfFlow(ThaEthFlow self)
    {
    AtModuleEncap encapModule;

    if (LinkOfFlow(self))
        return ThaModuleEncapLocalPppLinkId(LinkOfFlow(self));

    /* Return invalid ID */
    encapModule = (AtModuleEncap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEncap);
    return AtModuleEncapMaxChannelsGet(encapModule);
    }

static uint32 HwFlowId(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    return LinkIdOfFlow((ThaEthFlow)flow);
    }

static uint8 *PsnArrayTypes(ThaEthFlowController self, uint8 *numArrayTypes)
    {
    return ThaEthFlowHeaderProviderPppFlowPsnArrayTypes(ThaEthFlowControllerHeaderProvider(self), numArrayTypes);
    }

static uint8 PartId(ThaEthFlowController self, AtEthFlow flow)
    {
    ThaModuleEncap moduleEncap = (ThaModuleEncap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cAtModuleEncap);
    AtHdlcLink link = (AtHdlcLink)LinkOfFlow((ThaEthFlow)flow);
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet(link);
	AtUnused(self);
    return ThaHdlcChannelPartOfEncap(moduleEncap, channel);
    }

static eAtRet TxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    AtHdlcLink hdlcLink = AtEthFlowHdlcLinkGet(flow);
    ThaEthFlow oamFlow;
    eAtRet ret;

    ret = m_ThaEthFlowControllerMethods->TxTrafficEnable(self, flow, enable);
    if (ret != cAtOk)
        return ret;

    /* PPP OAM */
    oamFlow = (ThaEthFlow)AtHdlcLinkOamFlowGet(hdlcLink);
    if (oamFlow)
        return ThaEthFlowTxTrafficEnable(oamFlow, enable);

    return cAtOk;
    }

static uint32 VlanLookupId(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(self);
    /* With flow that is bound to PPP link Hardware, VLAN lookup ID is also Link ID */
    return LinkIdOfFlow((ThaEthFlow)flow);
    }

static uint16 EthVlanLookupTypeGet(ThaEthFlowController self)
    {
	AtUnused(self);
    return 2;
    }

static uint32 MpigDatDeQueRegisterAddress(ThaEthFlowController self)
    {
	AtUnused(self);
    return cThaRegMPIGDATDeQuePppLinkCtrl;
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel, uint8 classNumber)
    {
    eAtRet     ret;
    ThaEthFlow flow = (ThaEthFlow)ethFlow;
    AtHdlcLink link = (AtHdlcLink)channel;

    if ((AtHdlcLink)LinkOfFlow((ThaEthFlow)flow) == link)
        return cAtOk;

    AtEthFlowHdlcLinkSet(ethFlow, link);
    ret = m_ThaEthFlowControllerMethods->Activate(self, ethFlow, channel, classNumber);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    m_ThaEthFlowControllerMethods->DeActivate(self, flow);

    ThaEthFlowControllerSet((ThaEthFlow)flow, NULL);
    AtEthFlowHdlcLinkSet(flow, NULL);

    return cAtOk;
    }

static eAtRet AllCountersGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return ThaPppEthFlowControllerAllCountersFromPppLinkGet(self, flow, pAllCounters);
    }

static eAtRet AllCountersClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return ThaPppEthFlowControllerAllCountersFromPppLinkClear(self, flow, pAllCounters);
    }

static void OverrideThaEthFlowController(ThaPppEthFlowController self)
    {
    ThaEthFlowController flow = (ThaEthFlowController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(flow);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, Activate);
        mMethodOverride(m_ThaEthFlowControllerOverride, DeActivate);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthVlanLookupTypeGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, HwFlowId);
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnArrayTypes);
        mMethodOverride(m_ThaEthFlowControllerOverride, MpigDatDeQueRegisterAddress);
        mMethodOverride(m_ThaEthFlowControllerOverride, VlanLookupId);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersClear);
        mMethodOverride(m_ThaEthFlowControllerOverride, PartId);
        }

    mMethodsSet(flow, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaPppEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPppEthFlowController);
    }

ThaEthFlowController ThaPppEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController ThaPppEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaPppEthFlowControllerObjectInit(newController, module);
    }

/* TODO: counter retrieving should be moved to a class to be reused among E1
 * and STM product. That mean these functions should be refactored
 * - ThaPppEthFlowControllerAllCountersFromPppLinkGet
 * - ThaPppEthFlowControllerAllCountersFromPppLinkClear
 */
eAtRet ThaPppEthFlowControllerAllCountersFromPppLinkGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    AtHdlcLink          pppLink;
    tAtPppLinkCounters  pppLinkCnt;
    AtOsal              osal;
    tAtEthFlowCounters *counters = (tAtEthFlowCounters*)pAllCounters;
	AtUnused(self);

    if (counters == NULL)
        return cAtErrorNullPointer;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(counters));

    pppLink = AtEthFlowHdlcLinkGet(flow);
    if (AtChannelAllCountersGet((AtChannel)pppLink, &pppLinkCnt) != cAtOk)
        return cAtOk;

    counters->txGoodPkt    = pppLinkCnt.txPlain + pppLinkCnt.txOam;
    counters->rxDiscardPkt = pppLinkCnt.rxDiscardPkt;
    counters->rxGoodPkt    = pppLinkCnt.rxPlain + pppLinkCnt.rxOam;

    return cAtOk;
    }

eAtRet ThaPppEthFlowControllerAllCountersFromPppLinkClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    AtOsal              osal;
    tAtEthFlowCounters  *counters = (tAtEthFlowCounters*)pAllCounters;
	AtUnused(flow);
	AtUnused(self);

    if (counters == NULL)
        return cAtErrorNullPointer;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, counters, sizeof(counters), 0);

    return cAtOk;
    }

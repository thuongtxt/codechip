/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaOamFlowController.c
 *
 * Created Date: Jan 24, 2013
 *
 * Description : Thalassa Ethernet OAM flow controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaEthFlowInternal.h"
#include "../../ppp/ThaMpigReg.h"
#include "../../man/ThaDeviceInternal.h"

#include "ThaEthFlowControllerInternal.h"
#include "../../ppp/ThaPppLink.h"
#include "ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Save super implementation */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 *PsnArrayTypes(ThaEthFlowController self, uint8 *numArrayTypes)
    {
    return ThaEthFlowHeaderProviderOamFlowPsnArrayTypes(ThaEthFlowControllerHeaderProvider(self), numArrayTypes);
    }

static uint32 MpigDatDeQueRegisterAddress(ThaEthFlowController self)
    {
	AtUnused(self);
    return cThaRegMPIGDATDeQueOamLinkCtrl;
    }

static eAtRet TxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    uint32  offsetByFlow = mMethodsGet(self)->MpigFlowOffset(self, flow);
    uint32  regAddr = MpigDatDeQueRegisterAddress(self) + offsetByFlow;
    uint32  regVal;

    regVal = mChannelHwRead(flow, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, cThaOutEnableMask, cThaOutEnableShift, mBoolToBin(enable));
    mChannelHwWrite(flow, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(flow);
    return ThaEthFlowHeaderProviderOamEthHeaderNoVlanLengthGet(ThaEthFlowControllerHeaderProvider(self));
    }

static eBool HasEthernetType(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
    return ThaEthFlowHeaderProviderOamFlowHasEthType(ThaEthFlowControllerHeaderProvider(self));
    }

static eAtRet RxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    ThaPppLink pppLink = (ThaPppLink)AtEthFlowHdlcLinkGet(flow);
	AtUnused(self);
    ThaPppLinkOamTxEnable(pppLink, enable);
    return cAtOk;
    }

static void OverrideThaEthFlowController(ThaOamFlowController self)
    {
    ThaEthFlowController flowController = (ThaEthFlowController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(flowController);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, PsnArrayTypes);
        mMethodOverride(m_ThaEthFlowControllerOverride, MpigDatDeQueRegisterAddress);
        mMethodOverride(m_ThaEthFlowControllerOverride, TxTrafficEnable);
        mMethodOverride(m_ThaEthFlowControllerOverride, EthHeaderNoVlanLengthGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, HasEthernetType);
        mMethodOverride(m_ThaEthFlowControllerOverride, RxTrafficEnable);
        }

    mMethodsSet(flowController, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaOamFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaOamFlowController);
    }

ThaEthFlowController ThaOamFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaOamFlowController)self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController ThaOamFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ThaOamFlowControllerObjectInit(newFlow, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthFlowControllerFactory.c
 *
 * Created Date: Apr 4, 2015
 *
 * Description : ETH Flow controller factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthFlowControllerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEthFlowControllerFactoryMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowController PppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController OamFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController MpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController CiscoHdlcFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController FrFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController EncapFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static ThaEthFlowController PwFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    AtUnused(ethModule);
    return NULL;
    }

static void MethodsInit(ThaEthFlowControllerFactory self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PppFlowControllerCreate);
        mMethodOverride(m_methods, OamFlowControllerCreate);
        mMethodOverride(m_methods, MpFlowControllerCreate);
        mMethodOverride(m_methods, CiscoHdlcFlowControllerCreate);
        mMethodOverride(m_methods, FrFlowControllerCreate);
        mMethodOverride(m_methods, EncapFlowControllerCreate);
        mMethodOverride(m_methods, PwFlowControllerCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthFlowControllerFactory);
    }

ThaEthFlowControllerFactory ThaEthFlowControllerFactoryObjectInit(ThaEthFlowControllerFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController ThaEthFlowControllerFactoryPppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    if (self)
        return mMethodsGet(self)->PppFlowControllerCreate(self, ethModule);
    return NULL;
    }

ThaEthFlowController ThaEthFlowControllerFactoryOamFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    if (self)
        return mMethodsGet(self)->OamFlowControllerCreate(self, ethModule);
    return NULL;
    }

ThaEthFlowController ThaEthFlowControllerFactoryMpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    if (self)
        return mMethodsGet(self)->MpFlowControllerCreate(self, ethModule);
    return NULL;
    }

ThaEthFlowController ThaEthFlowControllerFactoryCiscoHdlcFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    if (self)
        return mMethodsGet(self)->CiscoHdlcFlowControllerCreate(self, ethModule);
    return NULL;
    }

ThaEthFlowController ThaEthFlowControllerFactoryFrFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    if (self)
        return mMethodsGet(self)->FrFlowControllerCreate(self, ethModule);
    return NULL;
    }

ThaEthFlowController ThaEthFlowControllerFactoryEncapFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    if (self)
        return mMethodsGet(self)->EncapFlowControllerCreate(self, ethModule);
    return NULL;
    }

ThaEthFlowController ThaEthFlowControllerFactoryPwFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    if (self)
        return mMethodsGet(self)->PwFlowControllerCreate(self, ethModule);
    return NULL;
    }

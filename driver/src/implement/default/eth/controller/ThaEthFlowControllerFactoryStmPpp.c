/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthFlowControllerFactoryStmPpp.c
 *
 * Created Date: Apr 4, 2015
 *
 * Description : ETH Flow controller factory for STM PPP product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthFlowControllerFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaEthFlowControllerFactoryStmPpp
    {
    tThaEthFlowControllerFactoryDefault super;
    }tThaEthFlowControllerFactoryStmPpp;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerFactoryMethods m_ThaEthFlowControllerFactoryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowController PppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaStmPppEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController MpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaStmMpEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController CiscoHdlcFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaStmCiscoHdlcEthFlowControllerNew(ethModule);
    }

static ThaEthFlowController FrFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule)
    {
    AtUnused(self);
    return ThaStmFrEthFlowControllerNew(ethModule);
    }

static void OverrideThaEthFlowControllerFactory(ThaEthFlowControllerFactory self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerFactoryOverride, mMethodsGet(self), sizeof(m_ThaEthFlowControllerFactoryOverride));

        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, PppFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, MpFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, CiscoHdlcFlowControllerCreate);
        mMethodOverride(m_ThaEthFlowControllerFactoryOverride, FrFlowControllerCreate);
        }

    mMethodsSet(self, &m_ThaEthFlowControllerFactoryOverride);
    }

static void Override(ThaEthFlowControllerFactory self)
    {
    OverrideThaEthFlowControllerFactory(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthFlowControllerFactoryStmPpp);
    }

static ThaEthFlowControllerFactory ObjectInit(ThaEthFlowControllerFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Constructor */
    if (ThaEthFlowControllerFactoryDefaultObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowControllerFactory ThaEthFlowControllerFactoryStmPppFactory(void)
    {
    static ThaEthFlowControllerFactory m_factory = NULL;
    static tThaEthFlowControllerFactoryStmPpp factory;

    if (m_factory == NULL)
        m_factory = ObjectInit((ThaEthFlowControllerFactory)&factory);
    return m_factory;
    }

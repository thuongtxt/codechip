/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaEthFlowController.c
 *
 * Created Date: May 14, 2013
 *
 * Description : Thalassa Ethernet traffic flow controller default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModulePpp.h"
#include "../../../../generic/encap/AtHdlcLinkInternal.h"
#include "../../ppp/ThaPppLinkInternal.h"
#include "../../ppp/ThaMpigReg.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../util/ThaUtil.h"
#include "ThaEthFlowControllerInternal.h"
#include "../ThaEthFlowInternal.h"
#include "../ThaModuleEthInternal.h"
#include "ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/
/* Flow with DA/SA + ETH TYPE + VLAN (optional) */
#define cAtEthFlowBasicHdrLength  14
#define cAtEthFlow1VlanHdrLength  18
#define cAtEthFlow2VlanHdrLength  22
#define cThaMaxNumberOfVlanOnFlow 8

/* Flow with ETH TYPE + VLAN (optional) */
#define cAtEthFlowNoDaSaBasicHdrLength  2
#define cAtEthFlowNoDaSa1VlanHdrLength  6
#define cAtEthFlowNoDaSa2VlanHdrLength  10

/* Flow with DA/SA + ETH TYPE + VLAN (optional) */
#define cAtEthFlowNoDaSaNoEthTypeBasicHdrLength              0
#define cAtEthFlowNoDaSaNoEthType1VlanHdrLength              4
#define cAtEthFlowNoDaSaNoEthType2VlanHdrLength              8


/*--------------------------- Macros -----------------------------------------*/
#define mThis(channel) (ThaEthFlowController)channel
#define mMpigDatDeQueRegister(controller) (mMethodsGet(controller)->MpigDatDeQueRegisterAddress(controller))
#define mThaWordGet(pBuf, dIndex)       (uint16)((pBuf[(dIndex) + 0] << 8) | (pBuf[(dIndex) + 1]))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaEthFlowControllerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Uint32ToArrayUint8(const uint32 *pIn, uint16 inNum, uint8 *pOut)
    {
    uint32 dwIndex = 0;

    for (dwIndex = 0; dwIndex < inNum; dwIndex ++)
        {
        pOut[(dwIndex << 2)]     = (uint8)((pIn[dwIndex] >> 24) & cBit7_0);
        pOut[(dwIndex << 2) + 1] = (pIn[dwIndex] >> 16) & cBit7_0;
        pOut[(dwIndex << 2) + 2] = (pIn[dwIndex] >> 8) & cBit7_0;
        pOut[(dwIndex << 2) + 3] = pIn[dwIndex] & cBit7_0;
        }
    }

static ThaModuleEth EthModule(ThaEthFlowController self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eAtRet EthernetTypeSet(ThaEthFlowController self, AtEthFlow flow)
    {
    eAtRet ret;
    uint8  buffer[cThaPsnHdrMaxLength];
    uint32 psnLen;
    uint16 ethTypeStartPosition = 2 * cAtMacAddressLen;
    AtOsal osal = AtSharedDriverOsalGet();
    eBool hasDaSa = mMethodsGet(self)->MacIsProgrammable(self, flow);

    /* Get header */
    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);
    ret = mMethodsGet(self)->PsnBufferGet(self, flow, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;

    switch (psnLen)
        {
        /* Basic header: DA-SA-ETH-Type */
        case 0:
        case cAtEthFlowBasicHdrLength:
            psnLen = hasDaSa ? cAtEthFlowBasicHdrLength : cAtEthFlowNoDaSaBasicHdrLength;
            break;

        /* 1 VLAN */
        case cAtEthFlow1VlanHdrLength:
        	if (hasDaSa)
        		ethTypeStartPosition = (uint16)(ethTypeStartPosition + cAtEthFlowVlanTypeLength + cAtEthFlowVlanLength);
        	else
        		ethTypeStartPosition = (uint16)(cAtEthFlowVlanTypeLength + cAtEthFlowVlanLength);
            break;

        /* 2 VLAN */
        case cAtEthFlow2VlanHdrLength:
        	if (hasDaSa)
        		ethTypeStartPosition = (uint16)(ethTypeStartPosition + (2 * (cAtEthFlowVlanTypeLength + cAtEthFlowVlanLength)));
        	else
        		ethTypeStartPosition = (uint16)((2 * (cAtEthFlowVlanTypeLength + cAtEthFlowVlanLength)));
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    /* Put Ethernet type */
    buffer[ethTypeStartPosition]     = 0x88;
    buffer[ethTypeStartPosition + 1] = 0x63;

    return mMethodsGet(self)->PsnBufferSet(self, flow, buffer, psnLen);
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(flow);
    return ThaEthFlowHeaderProviderEthHeaderNoVlanLengthGet(ThaEthFlowControllerHeaderProvider(self));
    }

static uint32 MpigDatDeQueRegisterAddress(ThaEthFlowController self)
    {
	AtUnused(self);
    /* Let sub-class controller does */
    return 0;
    }

static eAtRet ActivateFail(ThaEthFlow flow, eAtRet ret)
    {
    AtEthFlowHdlcBundleSet((AtEthFlow)flow, NULL);
    AtEthFlowHdlcLinkSet((AtEthFlow)flow, NULL);
    ThaEthFlowControllerSet(flow, NULL);

    return ret;
    }

static eAtRet FlowPsnHdrGet(ThaEthFlow self, uint8* destMacAddr, uint8* srcMacAddr, tAtEthVlanDesc* egressVlan)
    {
    AtOsal      osal    = AtSharedDriverOsalGet();
    AtEthFlow   atFlow  = (AtEthFlow)self;

    mMethodsGet(osal)->MemInit(osal, egressVlan, 0, sizeof(tAtEthVlanDesc));
    mMethodsGet(osal)->MemInit(osal, srcMacAddr, 0, cAtMacAddressLen);
    mMethodsGet(osal)->MemInit(osal, destMacAddr, 0, cAtMacAddressLen);

    AtEthFlowEgressSourceMacGet(atFlow, srcMacAddr);

    /* Get destination MAC for this flow */
    AtEthFlowEgressDestMacGet(atFlow, destMacAddr);
    return AtEthFlowEgressVlanGet(atFlow, egressVlan);
    }

static uint32 MpigOffset(ThaEthFlowController self, AtEthFlow flow)
    {
    uint8 partId = (uint8)mMethodsGet(self)->PartId(self, flow);
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet((AtChannel)flow), cThaModuleMpig, partId);
    }

static uint32 MpigFlowOffset(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 moduleOffset = MpigOffset(self, flow);
    return ThaEthFlowHwFlowIdGet((ThaEthFlow)flow) + moduleOffset;
    }

static eAtRet NumberEgVlanSet(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum)
    {
    uint32 offsetByFlow = MpigFlowOffset(self, flow);
    uint32 regVal, address = mMpigDatDeQueRegister(self) + offsetByFlow;

    regVal = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regVal, cThaVlanModeMask, cThaVlanModeShift, vlanTagNum);
    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    return cAtOk;
    }

static void FlowControlThresholdInit(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel)
    {
	AtUnused(channel);
	AtUnused(ethFlow);
	AtUnused(self);
    }

static tAtEthVlanDesc *IngressVlansClone(ThaEthFlowController self, AtEthFlow ethFlow)
    {
    uint32 numVlans = AtEthFlowIngressNumVlansGet(ethFlow);
    uint32 vlan_i;
    tAtEthVlanDesc *vlans;
    AtOsal osal;
    uint32 memorySize;

    AtUnused(self);

    if (numVlans == 0)
        return NULL;

    osal = AtSharedDriverOsalGet();
    memorySize = sizeof(tAtEthVlanDesc) * numVlans;
    vlans = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (vlans == NULL)
        return NULL;
    mMethodsGet(osal)->MemInit(osal, vlans, 0, memorySize);

    for (vlan_i = 0; vlan_i < numVlans; vlan_i++)
        {
        tAtEthVlanDesc descriptor;
        if (AtEthFlowIngressVlanAtIndex(ethFlow, vlan_i, &descriptor) == NULL)
            {
            mMethodsGet(osal)->MemFree(osal, vlans);
            return NULL;
            }

        mMethodsGet(osal)->MemCpy(osal, &(vlans[vlan_i]), &descriptor, sizeof(tAtEthVlanDesc));
        }

    return vlans;
    }

static eAtRet AllIngressVlansRemove(ThaEthFlowController self, AtEthFlow ethFlow)
    {
    AtUnused(self);

    while (AtEthFlowIngressNumVlansGet(ethFlow) > 0)
        {
        tAtEthVlanDesc vlanDesc;
        eAtRet ret;

        if (AtEthFlowIngressVlanAtIndex(ethFlow, 0, &vlanDesc) == NULL)
            return cAtErrorNullPointer;

        ret = AtEthFlowIngressVlanRemove(ethFlow, &vlanDesc);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet AllIngressVlansAdd(ThaEthFlowController self, AtEthFlow ethFlow)
    {
    eAtRet ret;
    AtOsal osal = AtSharedDriverOsalGet();
    tAtEthVlanDesc *descs = NULL;
    uint32 vlanNum;
    uint32 vlanIndex;

    /* Get VLAN lookup information at IG direction */
    vlanNum = AtEthFlowIngressNumVlansGet(ethFlow);
    descs = IngressVlansClone(self, ethFlow);
    if ((vlanNum > 0) && (descs == NULL))
        return cAtErrorRsrcNoAvail;

    AllIngressVlansRemove(self, ethFlow);
    for (vlanIndex = 0; vlanIndex < vlanNum; vlanIndex++)
        {
        ret = AtEthFlowIngressVlanAdd(ethFlow, &descs[vlanIndex]);
        if (ret != cAtOk)
            {
            mMethodsGet(osal)->MemFree(osal, descs);
            return ActivateFail((ThaEthFlow)ethFlow, ret);
            }
        }

    mMethodsGet(osal)->MemFree(osal, descs);

    return cAtOk;
    }

static eAtRet Activate(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel, uint8 classNumber)
    {
    eAtRet         ret;
    uint8          srcMacAddr[cAtMacAddressLen];
    uint8          destMacAddr[cAtMacAddressLen];
    tAtEthVlanDesc egressVlan;
    ThaEthFlow flow = (ThaEthFlow)ethFlow;
    eBool trafficIsEnabled = ThaEthFlowTxTrafficIsEnabled(flow);

    AtUnused(classNumber);

    if (mMethodsGet(self)->MacIsProgrammable(self, ethFlow))
        ret = FlowPsnHdrGet(flow, destMacAddr, srcMacAddr, &egressVlan);
    else
        ret = AtEthFlowEgressVlanGet(ethFlow, &egressVlan);

    if (ret != cAtOk)
        return ret;

    /* Had all information from database, from this time, flow need to know about it's controller */
    ThaEthFlowControllerSet(flow, self);
    ret = mMethodsGet(self)->EthHdrSet(self, ethFlow, destMacAddr, srcMacAddr, &egressVlan);
    if (ret != cAtOk)
        return ActivateFail(flow, ret);

    /* Reset database, and add all Vlans that we got before, it 's time to apply to hardware */
    ret = mMethodsGet(self)->AllIngressVlansAdd(self, ethFlow);
    if (ret != cAtOk)
        return ActivateFail(flow, ret);

    /* Enable flow traffic if it is enabled previously */
    ret = ThaEthFlowTxTrafficEnable(flow, trafficIsEnabled);
    if (ret != cAtOk)
        return ActivateFail(flow, ret);

    /* Initialize threshold for eth flow control */
    mMethodsGet(self)->FlowControlThresholdInit(self, ethFlow, channel);

    return cAtOk;
    }

static void PsnLengthReset(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 offsetByFlow = MpigFlowOffset(self, flow);
    uint32 regAddr  = mMethodsGet(self)->MpigDatDeQueRegisterAddress(self) + offsetByFlow;
    uint32 regVal;

    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, cThaHdrLenMask, cThaHdrLenShift, 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);
    }

static eAtRet DeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    uint8  vlan_i;

    for (vlan_i = 0; vlan_i < AtEthFlowIngressNumVlansGet(flow); vlan_i++)
        {
        tAtEthVlanDesc vlan;
        AtEthFlowIngressVlanAtIndex(flow, vlan_i, &vlan);
        mMethodsGet(self)->IngressVlanDisable(self, flow, &vlan);
        }

    /* Flow PSN Length reset */
    mMethodsGet(self)->PsnLengthReset(self, flow);

    /* Clear DB */
    ThaEthFlowControllerSet((ThaEthFlow) flow, NULL);
    return cAtOk;
    }

static uint32 HwFlowId(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(self);
    return ((ThaEthFlow)flow)->hwFlowId;
    }

static uint8 *PsnArrayTypes(ThaEthFlowController self, uint8 *numPsnType)
    {
	AtUnused(self);
    /* Let sub-class controller does */
    if (numPsnType)
        *numPsnType = 0;
    return NULL;
    }

static uint8 PsnMode(ThaEthFlowController self)
    {
    return ThaEthFlowHeaderProviderPsnMode(ThaEthFlowControllerHeaderProvider(self));
    }

static uint16 EthVlanLookupTypeGet(ThaEthFlowController self)
    {
	AtUnused(self);
    /* Let sub-class controller does */
    return 0;
    }

static uint32 VlanLookupId(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    /* Let sub-class controller does */
    return 0;
    }

static uint32 VlanIdSw2Hw(ThaEthFlowController self, AtEthFlow flow, uint32 vlanId)
    {
    return ThaEthFlowHeaderProviderVlanIdSw2Hw(ThaEthFlowControllerHeaderProvider(self), flow, vlanId);
    }

/* This is default Ethernet header, Ethernet header includes DA, SA, EthernetType and VLAN (if have) */
static eAtRet EthHdrSet(ThaEthFlowController self, AtEthFlow flow, uint8* destMacAddr, uint8* srcMacAddr, tAtEthVlanDesc* egressVlan)
    {
    eAtRet ret = cAtOk;

    if (mMethodsGet(self)->MacIsProgrammable(self, flow))
        {
        ret |= mMethodsGet(flow)->EgressDestMacSet(flow, destMacAddr);
        ret |= mMethodsGet(flow)->EgressSourceMacSet(flow, srcMacAddr);
        }

    if (mMethodsGet(self)->HasEthernetType(self, flow))
        ret |= mMethodsGet(self)->EthernetTypeSet(self, flow);

    if (egressVlan->numberOfVlans > 0)
        ret |= AtEthFlowEgressVlanSet(flow, egressVlan);

    return ret;
    }

static eAtRet TxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    uint32  offsetByFlow = MpigFlowOffset(self, flow);
    uint32  regAddr = mMpigDatDeQueRegister(self) + offsetByFlow;
    uint32  regVal;

    regVal = mChannelHwRead(flow, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, cThaOutEnableMask, cThaOutEnableShift, mBoolToBin(enable));
    mChannelHwWrite(flow, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

static eBool TxTrafficIsEnabled(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 offsetByFlow = MpigFlowOffset(self, flow);
    uint32 regAddr = mMpigDatDeQueRegister(self) + offsetByFlow;
    uint32 regVal;
    eBool  enable;

    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldGet(regVal, cThaOutEnableMask, cThaOutEnableShift, eBool, &enable);

    return enable;
    }

static eAtRet AllCountersGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
	AtUnused(pAllCounters);
	AtUnused(flow);
	AtUnused(self);
    /* Let sub-class controller does */
    return cAtError;
    }

static eAtRet AllCountersClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
	AtUnused(pAllCounters);
	AtUnused(flow);
	AtUnused(self);
    /* Let sub-class controller does */
    return cAtError;
    }

static eAtRet Debug(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    /* Let sub-class controller does */
    return cAtError;
    }

static uint8 PartId(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    /* Let sub-class controller does */
    return 0;
    }

static ThaModuleCla ClaModule(AtEthFlow flow)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)flow);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eAtRet IngressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
    ThaModuleEth ethModule;
    uint32 lookupVlanId;
    uint32 vlanId;
    uint16 lkType;

    ethModule = EthModule(self);
    if (ethModule == NULL)
        return cAtErrorNullPointer;

    lookupVlanId = mMethodsGet(self)->VlanLookupId(self, flow);
    vlanId = mMethodsGet(self)->VlanIdSw2Hw(self, flow, mMethodsGet(ethModule)->VlanIdFromVlanDesc(ethModule, flow, desc));
    lkType = mMethodsGet(self)->EthVlanLookupTypeGet(self);
    return ThaModuleClaPppVlanLookupCtrl(ClaModule(flow), (ThaEthFlow)flow, lkType, lookupVlanId, vlanId, cAtFalse);
    }

static eAtRet IngressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc)
    {
	uint32 vlanId;
    uint16 lkType;
    uint32 lookupVlanId;
    ThaEthFlow thaFlow;
    ThaModuleEth ethModule;

    ethModule = EthModule(self);
    if (ethModule == NULL)
    	return cAtErrorNullPointer;

    thaFlow      = (ThaEthFlow)flow;
    lkType       = mMethodsGet(self)->EthVlanLookupTypeGet(self);
    lookupVlanId = mMethodsGet(self)->VlanLookupId(self, flow);
    vlanId       = mMethodsGet(self)->VlanIdSw2Hw(self, flow, mMethodsGet(ethModule)->VlanIdFromVlanDesc(ethModule, flow, desc));

    /* Enable or not depend on how user configure the flow,
     * if flow is disable, VLAN lookup will also be disabled */
    ThaModuleClaPppVlanLookupCtrl(ClaModule(flow), thaFlow, lkType, lookupVlanId, vlanId, thaFlow->flowConfig.enable);
    return cAtOk;
    }

static eBool IsSimulated(AtEthFlow self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceIsSimulated(device);
    }

static tThaEthFlowSimulation *SimulationDatabaseCreate(AtEthFlow self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaEthFlowSimulation);
    tThaEthFlowSimulation *simulationDatabase = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    AtUnused(self);
    if (simulationDatabase)
        mMethodsGet(osal)->MemInit(osal, simulationDatabase, 0, memorySize);

    return simulationDatabase;
    }

static tThaEthFlowSimulation *SimulationDatabase(AtEthFlow self)
    {
    ThaEthFlow ethFlow = (ThaEthFlow)self;

    if (ethFlow->simulation == NULL)
        ethFlow->simulation = SimulationDatabaseCreate(self);

    return ethFlow->simulation;
    }

static eAtRet SimulateEgressPsnHeaderSet(AtEthFlow self,
                                         uint8* pBuffer,
                                         uint8 arrayType,
                                         uint32 psnLen)
    {
    tThaEthFlowSimulation *simulation = SimulationDatabase(self);
    AtOsal osal = AtSharedDriverOsalGet();
    AtUnused(arrayType);

    if (psnLen > sizeof(simulation->egressPsnHeader))
        psnLen = sizeof(simulation->egressPsnHeader);

    mMethodsGet(osal)->MemCpy(osal, simulation->egressPsnHeader, pBuffer, psnLen);
    simulation->egressPsnHeaderLength = psnLen;

    return cAtOk;
    }

static eAtRet SimulateEgressPsnHeaderGet(AtEthFlow self, uint8* pBuffer, uint8 arrayType, uint32* psnLen)
    {
    tThaEthFlowSimulation *simulation = SimulationDatabase(self);
    AtOsal osal = AtSharedDriverOsalGet();
    AtUnused(arrayType);

    mMethodsGet(osal)->MemCpy(osal, pBuffer, simulation->egressPsnHeader, simulation->egressPsnHeaderLength);
    *psnLen = simulation->egressPsnHeaderLength;

    return cAtOk;
    }

static eAtRet PsnBufferGet(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32* psnLen)
    {
    uint32 regVal[cThaLongRegMaxSize];
    uint8  i, j;
    uint32 offsetByFlow, moduleOffset;
    uint32 regAddr, channelId;
    uint32 headerMaxLenghInDwords = cThaPsnHdrMaxLength / 32;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 numArrayType;
    uint8 *arrayTypes = mMethodsGet(self)->PsnArrayTypes(self, &numArrayType);

    if (IsSimulated(flow))
        {
        SimulateEgressPsnHeaderGet(flow, pBuffer, arrayTypes[0], psnLen);
        if (*psnLen == 0)
            *psnLen = mMethodsGet(self)->EthHeaderNoVlanLengthGet(self, flow);

        return cAtOk;
        }

    /* Initialize buffer */
    mMethodsGet(osal)->MemInit(osal, regVal, 0, 4 * cThaLongRegMaxSize);

    channelId    = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    offsetByFlow = MpigFlowOffset(self, flow);
    moduleOffset = MpigOffset(self, flow);

    regVal[0] = mChannelHwRead(self, mMpigDatDeQueRegister(self) + offsetByFlow, cThaModuleMpig);
    mFieldGet(regVal[0], cThaHdrLenMask, cThaHdrLenShift, uint32, psnLen);

    /* Check device ready */
    regAddr = cThaRegMPIGDATDeQueCpuReadCtrl + moduleOffset;
    regVal[0] = mModuleHwRead(self, regAddr);
    if ((regVal[0] & cThaReqEnbMask) != 0)
        return cAtErrorDevBusy;

    /* Get PSN header */
    for (j = 0; j < headerMaxLenghInDwords; j++)
        {
        regVal[0] = 0;
        mFieldIns(&regVal[0], cThaSegIDMask, cThaSegIDShift, j);
        mFieldIns(&regVal[0], cThaBlkIdMask, cThaBlkIdShift, channelId);
        mFieldIns(&regVal[0], cThaBlkTypeMask, cThaBlkTypeShift, arrayTypes[0]);
        mFieldIns(&regVal[0],
                  cThaMPIGDATDeQueCpuReadEoPMask,
                  cThaMPIGDATDeQueCpuReadEoPShift,
                  (j == (headerMaxLenghInDwords - 1)) ? 1 : 0);
        mFieldIns(&regVal[0], cThaMPIGDATDeQueCpuReadSoPMask, cThaMPIGDATDeQueCpuReadSoPShift, (j == 0) ? 1 : 0);
        mFieldIns(&regVal[0], cThaReqEnbMask, cThaReqEnbShift, 1);
        mFieldIns(&regVal[0], cThaHdrEnbMask, cThaHdrEnbShift, 1);
        mChannelHwWrite(self, regAddr, regVal[0], cThaModuleMpig);

        /* Wait for HW finish */
        if (!ThaHwFinished((AtChannel)self, regAddr, cThaReqEnbMask, cThaWrIndrTimeOut, cThaModuleMpig))
            return cAtErrorIndrAcsTimeOut;

        /* HW finished, read information */
        for (i = 0; i < 4; i++)
            {
            mChannelHwLongRead(self, cThaRegMPIGDATDeQueOamDatacache + i + moduleOffset, regVal, cThaLongRegMaxSize, cThaModuleMpig);
            Uint32ToArrayUint8(&regVal[1], 1, pBuffer);
            pBuffer = pBuffer + 4;
            Uint32ToArrayUint8(&regVal[0], 1, pBuffer);
            pBuffer = pBuffer + 4;
            }
        }

    /* If PSN length is 0, set it to default */
    if (*psnLen == 0)
        *psnLen = mMethodsGet(self)->EthHeaderNoVlanLengthGet(self, flow);

    return cAtOk;
    }

static eBool PsnHeaderWritten(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32  regVal[cThaLongRegMaxSize];
    uint32  dTimeOut;
    tAtOsalCurTime  curTime;
    tAtOsalCurTime  startTime;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Init buffer */
    mMethodsGet(osal)->MemInit(osal, regVal, 0, 4 * cThaLongRegMaxSize);

    /* Get starting time */
    dTimeOut = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (dTimeOut < cThaWrIndrTimeOut)
        {
        mChannelHwLongRead(self, cThaRegMPIGDATSegProCpuWrCtrl + MpigOffset(self, flow), regVal, cThaLongRegMaxSize, cThaModuleMpig);
        if ((regVal[cThaWrReqDwIndex] & cThaWrReqMask) == 0)
            break;

        /* Get current time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);

        /* Calculate the difference between the previous time and current
         * time in milliseconds */
        dTimeOut = mTimeIntervalInMsGet(startTime, curTime);
        }

    if (dTimeOut >= cThaWrIndrTimeOut)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet PsnBufferSet(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32 psnLen)
    {
    uint32 regVal[cThaLongRegMaxSize];
    uint32 channelId;
    uint8 psnMode;
    uint8 numArrayTypes, arrayType_i;
    uint8 *arrayTypes = mMethodsGet(self)->PsnArrayTypes(self, &numArrayTypes);
    uint32 offsetByFlow = MpigFlowOffset(self, flow);
    AtOsal osal = AtSharedDriverOsalGet();

    if (IsSimulated(flow))
        return SimulateEgressPsnHeaderSet(flow, pBuffer, arrayTypes[0], psnLen);

    /* Initialize buffer */
    mMethodsGet(osal)->MemInit(osal, regVal, 0, 4 * cThaLongRegMaxSize);

    /* PSN control */
    psnMode = mMethodsGet(self)->PsnMode(self);
    channelId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);

    /* Wait for HW finish */
    if (!PsnHeaderWritten(self, flow))
        return cAtErrorIndrAcsTimeOut;

    for (arrayType_i = 0; arrayType_i < numArrayTypes; arrayType_i++)
        {
        uint32 qword_i;
        uint8 numWrittenBytes;
        uint8 * pCurrentByte = pBuffer;
        uint32 numQwords = ((psnLen / 8) + 1);

        for (qword_i = 1; qword_i <= numQwords; qword_i++)
            {
            static const uint8 cNumDwordsInQword = 2;
            uint32 i;

            numWrittenBytes = (uint8)(pCurrentByte - pBuffer);
            if (numWrittenBytes >= psnLen)
                break;

            /* Need two build two dwords */
            for (i = cNumDwordsInQword; i >= 1; i--)
                {
                uint8 dword_i = (uint8)(i - 1);
                uint32 byte_i;

                regVal[dword_i] = 0;
                for (byte_i = 0; byte_i < 3; byte_i++)
                    {
                    numWrittenBytes = (uint8)(pCurrentByte - pBuffer);
                    if (numWrittenBytes < psnLen)
                        {
                        regVal[dword_i] |= *pCurrentByte;
                        pCurrentByte++;
                        }
                    regVal[dword_i] <<= 8;
                    }

                /* For the last byte of this dword */
                numWrittenBytes = (uint8)(pCurrentByte - pBuffer);
                if (numWrittenBytes < psnLen)
                    {
                    regVal[dword_i] |= *pCurrentByte;
                    pCurrentByte++;
                    }
                }

            numWrittenBytes = (uint8)(pCurrentByte - pBuffer);
            mFieldIns(&regVal[cThaCurLenDwIndex], cThaCurLenMask, cThaCurLenShift, numWrittenBytes - 1);
            mFieldIns(&regVal[cThaFlwIDDwIndex], cThaFlwIDMask, cThaFlwIDShift, channelId);
            mFieldIns(&regVal[cThaSoPDwIndex], cThaSoPMask, cThaSoPShift, (qword_i == 1) ? 1 : 0);
            mFieldIns(&regVal[cThaEoPDwIndex], cThaEoPMask, cThaEoPShift, (numWrittenBytes >= psnLen) ? 1 : 0);
            mFieldIns(&regVal[cThaWrReqDwIndex], cThaWrReqMask, cThaWrReqShift, 1);

            /* Apply the same configuration for all of array types */
            mFieldIns(&regVal[cThaBlkTypDwIndex], cThaBlkTypMask, cThaBlkTypShift, arrayTypes[arrayType_i]);
            mChannelHwLongWrite(self, cThaRegMPIGDATSegProCpuWrCtrl + MpigOffset(self, flow), regVal, cThaLongRegMaxSize, cThaModuleMpig);

            /* Wait for HW finish */
            if (!PsnHeaderWritten(self, flow))
                return cAtErrorIndrAcsTimeOut;
            }
         }

    regVal[0] = mChannelHwRead(self, mMpigDatDeQueRegister(self) + offsetByFlow, cThaModuleMpig);
    mFieldIns(&regVal[0], cThaHdrLenMask, cThaHdrLenShift, psnLen);
    mFieldIns(&regVal[0], cThaPSNModeMask, cThaPSNModeShift, psnMode);
    mChannelHwWrite(self, mMpigDatDeQueRegister(self) + offsetByFlow, regVal[0], cThaModuleMpig);

    return cAtOk;
    }

static eAtRet RxTrafficEnable(ThaEthFlowController self, AtEthFlow flow, eBool enable)
    {
    uint32 i, numVlans;

    /* Allocate memory in case of too much VLANs */
    numVlans = AtEthFlowIngressNumVlansGet(flow);
    if (numVlans == 0)
        return cAtOk;

    /* Get all VLANs and enable/disable them */
    for (i = 0; i < numVlans; i++)
        {
        tAtEthVlanDesc vlan;
        AtEthFlowIngressVlanAtIndex(flow, i, &vlan);

        if (enable)
            mMethodsGet(self)->IngressVlanEnable(self, flow, &vlan);
        else
            mMethodsGet(self)->IngressVlanDisable(self, flow, &vlan);
        }

    return cAtOk;
    }

static eBool HasEthernetType(ThaEthFlowController self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    return cAtTrue;
    }

static eBool MacIsProgrammable(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtTrue;
    }

static uint16 CVlanTpid(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    return AtEthFlowEgressCVlanTpIdGet(flow);
    }

static uint16 SVlanTpid(ThaEthFlowController self, AtEthFlow flow)
    {
    AtUnused(self);
    return AtEthFlowEgressSVlanTpIdGet(flow);
    }

static uint8 VlanStartIndex(ThaEthFlowController self, AtEthFlow ethFlow)
    {
    uint8 destMacStartIndex = mMethodsGet(self)->StartEthHeaderInPsnBuffer(self);

    if (mMethodsGet(self)->MacIsProgrammable(self, ethFlow))
        return (uint8)(destMacStartIndex + 2 * cAtMacAddressLen);

    return destMacStartIndex;
    }

static uint32 InsertVlanToBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    uint8 vlanStartIndex = VlanStartIndex(self, ethFlow);
    uint32 vlanInDesc;
    uint16 vlanProtocolId;
    uint8 bufferIndex = vlanStartIndex;
    uint8 vlanTagNum = egressVlan->numberOfVlans;
    uint8 numByteUsedForVlan = (uint8)(vlanTagNum * (cAtEthFlowVlanLength + cAtEthFlowVlanTypeLength));
    uint8 indexAfterVlan = (uint8)(vlanStartIndex + numByteUsedForVlan);

    /* Shift all bytes follow Source MAC to have space for VLANs */
    while (bufferIndex < psnLen)
        {
        buffer[indexAfterVlan] = buffer[bufferIndex];
        indexAfterVlan++;
        bufferIndex++;
        }

    /* Insert VLANs */
    bufferIndex = vlanStartIndex;

    /* Start with the outer most VLAN */
    for (vlanInDesc = vlanTagNum; vlanInDesc > 0; vlanInDesc--)
        {
        /* If only one VLAN, it is C-VLAN */
        if (vlanTagNum == 1)
            vlanProtocolId = CVlanTpid(self, ethFlow);

        /* If there are 2 VLANs */
        else if ((vlanInDesc == 2) && (vlanTagNum == 2))
            vlanProtocolId = SVlanTpid(self, ethFlow);
        else
            vlanProtocolId = CVlanTpid(self, ethFlow);

        /* Construct VLAN Type */
        buffer[bufferIndex] = (uint8)(vlanProtocolId >> 8);
        bufferIndex++;
        buffer[bufferIndex] = vlanProtocolId & cBit7_0;
        bufferIndex++;

        /* Construct VLAN priority - cfi - vlan Id */
        buffer[bufferIndex] = (uint8)(((egressVlan->vlans[vlanInDesc - 1].priority & cBit2_0) << 5) |
                                      ((egressVlan->vlans[vlanInDesc - 1].cfi & cBit0) << 4)        |
                                      ((egressVlan->vlans[vlanInDesc - 1].vlanId >> 8) & cBit3_0));
        bufferIndex++;

        buffer[bufferIndex] = egressVlan->vlans[vlanInDesc - 1].vlanId & cBit7_0;
        bufferIndex++;
        }

    return psnLen + numByteUsedForVlan;
    }

static uint32 LastIndexOfVlanInBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanTag* vlan, uint32 psnLen)
    {
    uint8 startIndex = VlanStartIndex(self, ethFlow);
    uint8 vlanMsb = (uint8)(((vlan->priority & cBit2_0) << 5) | ((vlan->cfi & cBit0) << 4) | ((vlan->vlanId >> 8) & cBit3_0));
    uint8 vlanLsb = vlan->vlanId & cBit7_0;
    uint32 vlanLastIndex = psnLen;

    while (startIndex < psnLen)
        {
        if ((buffer[startIndex] == vlanMsb) && (buffer[startIndex + 1] == vlanLsb))
            vlanLastIndex = startIndex;

        startIndex = (uint8)(startIndex + 2);
        }

    return vlanLastIndex;
    }

static uint32 RemoveVlanFromBuffer(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen)
    {
    uint8 numByteUsedForVlan = cAtEthFlowVlanLength + cAtEthFlowVlanTypeLength;
    uint32 indexAfterVlan, indexUsedForVlan, vlan_i;

    for (vlan_i = 0; vlan_i < egressVlan->numberOfVlans; vlan_i++)
        {
        uint32 vlanStartIndex = LastIndexOfVlanInBuffer(self, ethFlow, buffer, &(egressVlan->vlans[vlan_i]), psnLen);
        if ((vlanStartIndex >= psnLen) || (vlanStartIndex < cAtEthFlowVlanTypeLength))
            continue;

        /* Move back 2 TPID bytes */
        vlanStartIndex = vlanStartIndex - 2;
        indexAfterVlan = vlanStartIndex + numByteUsedForVlan;
        indexUsedForVlan = vlanStartIndex;

        /* Shift all bytes follow VLAN to replace bytes used for VLAN */
        while (indexAfterVlan < psnLen)
            {
            buffer[indexUsedForVlan] = buffer[indexAfterVlan];
            indexUsedForVlan++;
            indexAfterVlan++;
            }

        psnLen = psnLen - numByteUsedForVlan;
        }

    return psnLen;
    }

static uint32 CounterGet(ThaEthFlowController self, AtEthFlow flow, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(counterType);
    return 0;
    }

static uint32 CounterClear(ThaEthFlowController self, AtEthFlow flow, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(counterType);
    return 0;
    }

static uint32 VlanTpidSet(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanTag* vlan, uint16 tpid, uint32 psnLen)
    {
    uint32 vlanStartIndex = LastIndexOfVlanInBuffer(self, ethFlow, buffer, vlan, psnLen);
    if ((vlanStartIndex >= psnLen) || (vlanStartIndex < cAtEthFlowVlanTypeLength))
        return psnLen;

    /* Update tpid for VLAN */
    buffer[vlanStartIndex - 1] = (uint8)tpid;
    buffer[vlanStartIndex - 2] = (uint8)(tpid >> 8);

    return vlanStartIndex;
    }

static eAtRet EgressVlanEnable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *egressVlan)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(egressVlan);
    return cAtErrorModeNotSupport;
    }

static eAtRet EgressVlanDisable(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *egressVlan)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(egressVlan);
    return cAtErrorModeNotSupport;
    }

static eAtRet EgressCVlanTpidSet(ThaEthFlowController self, AtEthFlow flow, uint16 cVlanTpid)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(cVlanTpid);
    return cAtErrorModeNotSupport;
    }

static eAtRet EgressSVlanTpidSet(ThaEthFlowController self, AtEthFlow flow, uint16 sVlanTpid)
    {
    AtUnused(self);
    AtUnused(flow);
    AtUnused(sVlanTpid);
    return cAtErrorModeNotSupport;
    }

static eBool ShouldControlEgressVlanByPsnBuffer(ThaEthFlowController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 StartEthHeaderInPsnBuffer(ThaEthFlowController self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(ThaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Activate);
        mMethodOverride(m_methods, DeActivate);
        mMethodOverride(m_methods, EthHeaderNoVlanLengthGet);
        mMethodOverride(m_methods, HwFlowId);
        mMethodOverride(m_methods, PsnArrayTypes);
        mMethodOverride(m_methods, EthVlanLookupTypeGet);
        mMethodOverride(m_methods, MpigDatDeQueRegisterAddress);
        mMethodOverride(m_methods, VlanLookupId);
        mMethodOverride(m_methods, VlanIdSw2Hw);
        mMethodOverride(m_methods, PsnMode);
        mMethodOverride(m_methods, EthHdrSet);
        mMethodOverride(m_methods, EthernetTypeSet);
        mMethodOverride(m_methods, TxTrafficEnable);
        mMethodOverride(m_methods, TxTrafficIsEnabled);
        mMethodOverride(m_methods, AllCountersGet);
        mMethodOverride(m_methods, AllCountersClear);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, IngressVlanDisable);
        mMethodOverride(m_methods, IngressVlanEnable);
        mMethodOverride(m_methods, PsnBufferSet);
        mMethodOverride(m_methods, PsnBufferGet);
        mMethodOverride(m_methods, NumberEgVlanSet);
        mMethodOverride(m_methods, PartId);
        mMethodOverride(m_methods, RxTrafficEnable);
        mMethodOverride(m_methods, HasEthernetType);
     	mMethodOverride(m_methods, MacIsProgrammable);
        mMethodOverride(m_methods, InsertVlanToBuffer);
        mMethodOverride(m_methods, RemoveVlanFromBuffer);
        mMethodOverride(m_methods, FlowControlThresholdInit);
        mMethodOverride(m_methods, MpigFlowOffset);
        mMethodOverride(m_methods, PsnLengthReset);
        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, VlanTpidSet);
        mMethodOverride(m_methods, EgressVlanEnable);
        mMethodOverride(m_methods, EgressVlanDisable);
        mMethodOverride(m_methods, EgressCVlanTpidSet);
        mMethodOverride(m_methods, EgressSVlanTpidSet);
        mMethodOverride(m_methods, ShouldControlEgressVlanByPsnBuffer);
        mMethodOverride(m_methods, StartEthHeaderInPsnBuffer);
        mMethodOverride(m_methods, AllIngressVlansAdd);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthFlowController);
    }

ThaEthFlowController ThaEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthFlowObjectInit((AtEthFlow)self, 0, module) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit(mThis(self));

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

eAtRet ThaEthFlowControllerFlowActivate(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber)
    {
    if (self)
        return mMethodsGet(self)->Activate(self, flow, channel, classNumber);

    return cAtError;
    }

eAtRet ThaEthFlowControllerFlowDeActivate(ThaEthFlowController self, AtEthFlow flow)
    {
    if (self == NULL)
        return cAtError;

    return mMethodsGet(self)->DeActivate(self, flow);
    }

uint8 ThaEthFlowControllerPartIdGet(ThaEthFlowController self, AtEthFlow flow)
    {
    if (self)
        return (uint8)mMethodsGet(self)->PartId(self, flow);
    return 0;
    }

ThaEthFlowHeaderProvider ThaEthFlowControllerHeaderProvider(ThaEthFlowController self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    return ThaModuleEthFlowHeaderProviderGet(ethModule);
    }

void ThaEthFlowControllerFlowControlThresholdInit(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel)
    {
    if (self)
        mMethodsGet(self)->FlowControlThresholdInit(self, ethFlow, channel);
    }

eBool ThaEthFlowControllerMacIsProgrammable(ThaEthFlowController self, AtEthFlow flow)
    {
    if (self)
        return mMethodsGet(self)->MacIsProgrammable(self, flow);

    return cAtTrue;
    }

uint32 ThaEthFlowControllerMpigDatDeQueRegisterAddress(ThaEthFlowController self)
    {
    if (self)
        return mMethodsGet(self)->MpigDatDeQueRegisterAddress(self);

    return cInvalidUint32;
    }


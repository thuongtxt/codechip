/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthFlowControllerInternal.h
 * 
 * Created Date: May 15, 2013
 *
 * Description : ETH Flow controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWCONTROLLERINTERNAL_H_
#define _THAETHFLOWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMpBundle.h"
#include "AtPppLink.h"

#include "../../../../generic/eth/AtEthFlowInternal.h"
#include "../ThaEthFlow.h"
#include "ThaEthFlowController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cNumByteOfDestMac       6
#define cNumByteOfSourceMac     6
#define cNumByteOfVlanTag       4
#define cNumByteOfEthernetType  2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlowControllerMethods
    {
    eAtRet (*Activate)(ThaEthFlowController self, AtEthFlow flow, AtChannel channel, uint8 classNumber);
    eAtRet (*DeActivate)(ThaEthFlowController self, AtEthFlow flow);
    uint32 (*HwFlowId)(ThaEthFlowController self, AtEthFlow flow);

    uint8  *(*PsnArrayTypes)(ThaEthFlowController self, uint8 *numPsnType);
    uint16 (*EthVlanLookupTypeGet)(ThaEthFlowController self);
    uint32 (*MpigDatDeQueRegisterAddress)(ThaEthFlowController self);
    uint16 (*EthHeaderNoVlanLengthGet)(ThaEthFlowController self, AtEthFlow flow);
    uint8  (*PsnMode)(ThaEthFlowController self);
    uint8  (*StartEthHeaderInPsnBuffer)(ThaEthFlowController self);

    uint32 (*VlanLookupId)(ThaEthFlowController self, AtEthFlow flow);
    uint32 (*VlanIdSw2Hw)(ThaEthFlowController self, AtEthFlow flow, uint32 vlanId);
    eAtRet (*EthHdrSet)(ThaEthFlowController self, AtEthFlow flow, uint8* destMacAddr, uint8* srcMacAddr, tAtEthVlanDesc* egressVlan);
    eBool  (*HasEthernetType)(ThaEthFlowController self, AtEthFlow flow);
    eAtRet (*EthernetTypeSet)(ThaEthFlowController self, AtEthFlow flow);
    eBool  (*MacIsProgrammable)(ThaEthFlowController self, AtEthFlow flow);
    
    eAtRet (*TxTrafficEnable)(ThaEthFlowController self, AtEthFlow flow, eBool enable);
    eBool  (*TxTrafficIsEnabled)(ThaEthFlowController self, AtEthFlow flow);
    
    eAtRet (*RxTrafficEnable)(ThaEthFlowController self, AtEthFlow flow, eBool enable);

    eAtRet (*AllCountersGet)(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters);
    eAtRet (*AllCountersClear)(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters);
    uint32 (*CounterGet)(ThaEthFlowController self, AtEthFlow flow, uint16 counterType);
    uint32 (*CounterClear)(ThaEthFlowController self, AtEthFlow flow, uint16 counterType);

    eAtRet (*Debug)(ThaEthFlowController self, AtEthFlow flow);

    eAtRet (*IngressVlanDisable)(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc);
    eAtRet (*IngressVlanEnable)(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc);
    eAtRet (*AllIngressVlansAdd)(ThaEthFlowController self, AtEthFlow flow);

    eAtRet (*EgressVlanDisable)(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc);
    eAtRet (*EgressVlanEnable)(ThaEthFlowController self, AtEthFlow flow, const tAtEthVlanDesc *desc);
    eAtRet (*EgressCVlanTpidSet)(ThaEthFlowController self, AtEthFlow flow, uint16 cVlanTpid);
    eAtRet (*EgressSVlanTpidSet)(ThaEthFlowController self, AtEthFlow flow, uint16 sVlanTpid);
    eBool (*ShouldControlEgressVlanByPsnBuffer)(ThaEthFlowController self);

    eAtRet (*PsnBufferGet)(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32* psnLen);
    eAtRet (*PsnBufferSet)(ThaEthFlowController self, AtEthFlow flow, uint8* pBuffer, uint32 psnLen);

    eAtRet (*NumberEgVlanSet)(ThaEthFlowController self, AtEthFlow flow, uint8 vlanTagNum);
    uint8  (*PartId)(ThaEthFlowController self, AtEthFlow flow);

	void   (*FlowControlThresholdInit)(ThaEthFlowController self, AtEthFlow ethFlow, AtChannel channel);
	uint32 (*MpigFlowOffset)(ThaEthFlowController self, AtEthFlow flow);

    /* Build Vlan */
    uint32 (*InsertVlanToBuffer)(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen);
    uint32 (*RemoveVlanFromBuffer)(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanDesc *egressVlan, uint32 psnLen);
    uint32 (*VlanTpidSet)(ThaEthFlowController self, AtEthFlow ethFlow, uint8* buffer, const tAtEthVlanTag* vlan, uint16 tpid, uint32 psnLen);

    void   (*PsnLengthReset)(ThaEthFlowController self, AtEthFlow flow);

    }tThaEthFlowControllerMethods;

/* Ethernet flow controller */
typedef struct tThaEthFlowController
    {
    tAtEthFlow super;
    const tThaEthFlowControllerMethods *methods;

    }tThaEthFlowController;

/* PPP Ethernet flow controller */
typedef struct tThaPppEthFlowController * ThaPppEthFlowController;

typedef struct tThaPppEthFlowController
    {
    tThaEthFlowController super;
    }tThaPppEthFlowController;

/* MLPPP Ethernet flow controller */
typedef struct tThaMpEthFlowControllerMethods
    {
    uint32 (*AddressOfCntRegister)(ThaMpEthFlowController self, AtEthFlow flow, uint32 regBaseAddress);
    }tThaMpEthFlowControllerMethods;

typedef struct tThaMpEthFlowController
    {
    tThaEthFlowController super;
    const tThaMpEthFlowControllerMethods *methods;

    }tThaMpEthFlowController;

/* STM MLPPP Ethernet flow controller */
typedef struct tThaStmPppEthFlowController * ThaStmPppEthFlowController;

typedef struct tThaStmPppEthFlowController
    {
    tThaPppEthFlowController super;
    }tThaStmPppEthFlowController;

typedef struct tThaStmMpEthFlowController * ThaStmMpEthFlowController;

typedef struct tThaStmMpEthFlowController
    {
    tThaMpEthFlowController super;
    }tThaStmMpEthFlowController;

typedef struct tThaOamFlowController
    {
    tThaPppEthFlowController super;
    }tThaOamFlowController;

typedef struct tThaCiscoHdlcEthFlowController * ThaCiscoHdlcEthFlowController;

typedef struct tThaCiscoHdlcEthFlowController
    {
    tThaPppEthFlowController super;
    }tThaCiscoHdlcEthFlowController;

typedef struct tThaStmCiscoHdlcEthFlowController * ThaStmCiscoHdlcEthFlowController;

typedef struct tThaStmCiscoHdlcEthFlowController
    {
    tThaStmPppEthFlowController super;
    }tThaStmCiscoHdlcEthFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
ThaEthFlowController ThaEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);
ThaEthFlowController ThaPppEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);
ThaEthFlowController ThaMpEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);
ThaEthFlowController ThaCiscoHdlcEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);

ThaEthFlowController ThaStmEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);
ThaEthFlowController ThaStmPppEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);
ThaEthFlowController ThaOamFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);
ThaEthFlowController ThaStmCiscoHdlcEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);

eBool ThaEthFlowControllerMacIsProgrammable(ThaEthFlowController self, AtEthFlow flow);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWCONTROLLERINTERNAL_H_ */


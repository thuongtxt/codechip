/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthFlowHeaderProviderInternal.h
 * 
 * Created Date: Nov 28, 2013
 *
 * Description : ETH Flow header provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWHEADERPROVIDERINTERNAL_H_
#define _THAETHFLOWHEADERPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h"
#include "ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cNumByteOfDestMac       6
#define cNumByteOfSourceMac     6
#define cNumByteOfEthernetType  2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlowHeaderProviderMethods
    {
    uint8 (*PsnMode)(ThaEthFlowHeaderProvider self);
    uint16 (*EthHeaderNoVlanLengthGet)(ThaEthFlowHeaderProvider self);
    uint16 (*CiscoEthHeaderNoVlanLengthGet)(ThaEthFlowHeaderProvider self);
    uint16 (*OamEthHeaderNoVlanLengthGet)(ThaEthFlowHeaderProvider self);
    uint32 (*VlanIdSw2Hw)(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId);

    uint8 *(*MpFlowPsnArrayTypes)(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes);
    uint8 *(*OamFlowPsnArrayTypes)(ThaEthFlowHeaderProvider self, uint8 *numPsnTypes);

    eBool (*PppFlowHasEthType)(ThaEthFlowHeaderProvider self);
    eBool (*MpFlowHasEthType)(ThaEthFlowHeaderProvider self);
    eBool (*OamFlowHasEthType)(ThaEthFlowHeaderProvider self);
    eBool (*CiscoFlowHasEthType)(ThaEthFlowHeaderProvider self);
    }tThaEthFlowHeaderProviderMethods;

typedef struct tThaEthFlowHeaderProvider
    {
    tAtObject super;
    const tThaEthFlowHeaderProviderMethods *methods;

    /* Private data */
    AtModuleEth ethModule;
    }tThaEthFlowHeaderProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowHeaderProvider ThaEthFlowHeaderProviderObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWHEADERPROVIDERINTERNAL_H_ */


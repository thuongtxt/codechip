/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthFlowControllerFactory.h
 * 
 * Created Date: Apr 4, 2015
 *
 * Description : ETH Flow controller factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWCONTROLLERFACTORY_H_
#define _THAETHFLOWCONTROLLERFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtModuleEth.h"
#include "ThaEthFlowController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlowControllerFactory * ThaEthFlowControllerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete factories */
ThaEthFlowControllerFactory ThaEthFlowControllerFactoryDefault(void);
ThaEthFlowControllerFactory ThaEthFlowControllerFactoryStmPppFactory(void);

/* Factory methods */
ThaEthFlowController ThaEthFlowControllerFactoryPppFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
ThaEthFlowController ThaEthFlowControllerFactoryOamFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
ThaEthFlowController ThaEthFlowControllerFactoryMpFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
ThaEthFlowController ThaEthFlowControllerFactoryCiscoHdlcFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
ThaEthFlowController ThaEthFlowControllerFactoryFrFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
ThaEthFlowController ThaEthFlowControllerFactoryEncapFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
ThaEthFlowController ThaEthFlowControllerFactoryPwFlowControllerCreate(ThaEthFlowControllerFactory self, AtModuleEth ethModule);

/* Product concretes */
ThaEthFlowControllerFactory Tha60060011EthFlowControllerFactory(void);
ThaEthFlowControllerFactory Tha60290081EthFlowControllerFactory(void);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWCONTROLLERFACTORY_H_ */


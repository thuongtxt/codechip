/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthFlowControllerFactoryInternal.h
 * 
 * Created Date: Apr 4, 2015
 *
 * Description : ETH Flow controller factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWCONTROLLERFACTORYINTERNAL_H_
#define _THAETHFLOWCONTROLLERFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/AtDriverInternal.h" /* For OSAL */
#include "ThaEthFlowControllerFactory.h"
#include "ThaEthFlowController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlowControllerFactoryMethods
    {
    ThaEthFlowController (*PppFlowControllerCreate)(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
    ThaEthFlowController (*OamFlowControllerCreate)(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
    ThaEthFlowController (*MpFlowControllerCreate)(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
    ThaEthFlowController (*CiscoHdlcFlowControllerCreate)(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
    ThaEthFlowController (*FrFlowControllerCreate)(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
    ThaEthFlowController (*EncapFlowControllerCreate)(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
    ThaEthFlowController (*PwFlowControllerCreate)(ThaEthFlowControllerFactory self, AtModuleEth ethModule);
    }tThaEthFlowControllerFactoryMethods;

typedef struct tThaEthFlowControllerFactory
    {
    tAtObject super;
    const tThaEthFlowControllerFactoryMethods *methods;
    }tThaEthFlowControllerFactory;

typedef struct tThaEthFlowControllerFactoryDefault
    {
    tThaEthFlowControllerFactory super;
    }tThaEthFlowControllerFactoryDefault;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowControllerFactory ThaEthFlowControllerFactoryObjectInit(ThaEthFlowControllerFactory self);
ThaEthFlowControllerFactory ThaEthFlowControllerFactoryDefaultObjectInit(ThaEthFlowControllerFactory self);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWCONTROLLERFACTORYINTERNAL_H_ */

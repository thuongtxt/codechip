/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaStmMpEthFlowController.c
 *
 * Created Date: May 17, 2013
 *
 * Description : Thalassa Ethernet traffic flow controller implementation for MLPPP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"

#include "AtModulePpp.h"

#include "../../ppp/ThaStmPmcReg.h"
#include "../../ppp/ThaMpegStmReg.h"
#include "../../man/ThaDeviceInternal.h"

#include "../ThaEthFlowInternal.h"

#include "ThaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaNumDwordsInLongReg   4

/* Define number counter in Hw */
#define cThaPMCMpegMpFlowGoodPkt     0
#define cThaPMCMpegMpFlowFrag        1
#define cThaPMCMpegMpFlowNullFrag    2
#define cThaPMCMpegMpFlowGoodByte    3
#define cThaPMCMpegMpFlowDiscPkt     4
#define cThaPMCMpegMpFlowDiscbyte    5
#define cThaPMCMpigMpFlowMrruPkt     6
#define cThaPMCMpigMpFlowGoodPkt     8
#define cThaPMCMpigMpFlowFrag        9
#define cThaPMCMpigMpFlowNullFrag    10
#define cThaPMCMpigMpFlowGoodByte    11
#define cThaPMCMpigMpFlowRsqDiscFrag 16
#define cThaPMCMpigMpFlowRsqViolM    15

#define cThaMlpppIndrCounterCheckTime       8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(channel)  (ThaStmMpEthFlowController)channel

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaEthFlowControllerMethods m_ThaEthFlowControllerOverride;

/* Save super implementation */
static const tThaEthFlowControllerMethods *m_ThaEthFlowControllerMethods;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool ThaPmcIndirectCounterIsAccessible(AtChannel self, eAtModule moduleId, uint8 partId);
extern eBool ThaPmcIndirectCounterIsValid(AtChannel self, eAtModule moduleId, uint32 timeoutMs, uint8  partId);

/*--------------------------- Implementation ---------------------------------*/
static uint32 MpegOffset(ThaEthFlowController self, AtEthFlow flow)
    {
    uint8 partId = (uint8)mMethodsGet(self)->PartId(self, flow);
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet((AtChannel)flow), cThaModuleMpeg, partId);
    }

static uint32 FlowMpegOffset(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 moduleOffset = MpegOffset(self, flow);
    return ThaEthFlowHwFlowIdGet((ThaEthFlow)flow) + moduleOffset;
    }

static eAtRet Debug(ThaEthFlowController self, AtEthFlow flow)
    {
    uint32 regValue, address;
    uint32 seqValue;

    address = cThaMPEGMlpppFlowSequenceStatus + FlowMpegOffset(self, flow);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldGet(regValue,
              cThaMPEGMpflowCurrSequenceMaks,
              cThaMPEGMpflowCurrSequenceShift,
              uint32,
              &seqValue);
    AtPrintc(cSevNormal, "  TxCurrSequence   : %u\r\n", seqValue);

    return cAtOk;
    }

static uint32 CounterAddress(ThaEthFlowController self, AtChannel flow)
    {
    return mMethodsGet(self)->HwFlowId(self, (AtEthFlow)flow) + 0x800;
    }

static uint8 PartIdGet(ThaEthFlowController self, AtChannel flow)
    {
    return (uint8)mMethodsGet(self)->PartId(self, (AtEthFlow)flow);
    }

static uint32 PmcMpigOffset(AtChannel flow, uint8 partId)
    {
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet(flow), cThaModulePmcMpig, partId);
    }

static eAtRet MpigCntGet(ThaEthFlowController self, AtChannel flow, tAtEthFlowCounters *pCounter, eBool r2c)
    {
    static const uint32 cTimeoutMs = 20;
    uint32 counterAddress;
    uint16 actualCounterAddress;
    uint8 counterPosition;
    AtOsal osal;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint8 i;
    uint8 partId = PartIdGet(self, flow);
    uint8 allCounters[] = {cThaPMCMpigMpFlowMrruPkt,
                           cThaPMCMpigMpFlowGoodPkt,
                           cThaPMCMpigMpFlowFrag,
                           cThaPMCMpigMpFlowNullFrag,
                           cThaPMCMpigMpFlowGoodByte,
                           cThaPMCMpigMpFlowRsqDiscFrag,
                           cThaPMCMpigMpFlowRsqViolM};

    uint32 pmcMpigOffset = PmcMpigOffset(flow, partId);

    if (!ThaPmcIndirectCounterIsAccessible(flow, cThaModulePmcMpig, partId))
        return cAtErrorDevBusy;

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    counterAddress = CounterAddress(self, flow);
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, counterAddress);

    /* Mask of counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self,
                        cThaRegThalassaPMMpxgCntsAddressRequestCtrl + pmcMpigOffset,
                        longRegVal,
                        cThaNumDwordsInLongReg,
                        cThaModulePmcMpig);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpig, cTimeoutMs, partId))
        return cAtErrorDevBusy;

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpig, cTimeoutMs, partId))
            return cAtOk;

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self,
                               cThaRegThalassaPMMpxgCntsIndrRegRptStat + pmcMpigOffset,
                               longRegVal,
                               cThaNumDwordsInLongReg,
                               cThaModulePmcMpig) == 0)
            return cAtErrorDevFail;

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &actualCounterAddress);
        if (actualCounterAddress != counterAddress)
            return cAtErrorDevFail;

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);

        if (pCounter)
            {
            mPmcCounterGet(PMCMpigMpFlowGoodByte          , &(pCounter->txGoodByte));
            mPmcCounterGet(PMCMpigMpFlowGoodPkt           , &(pCounter->txPacket));
            mPmcCounterGet(PMCMpigMpFlowFrag              , &(pCounter->txFragment));
            mPmcCounterGet(PMCMpigMpFlowNullFrag          , &(pCounter->txNullFragment));
            mPmcCounterGet(PMCMpigMpFlowGoodPkt           , &(pCounter->txGoodPkt));
            mPmcCounterGet(PMCMpigMpFlowFrag              , &(pCounter->txGoodFragment));
            mPmcCounterGet(PMCMpigMpFlowRsqDiscFrag       , &(pCounter->txDiscardFragment));
            mPmcCounterGetIn2Dwords(PMCMpigMpFlowRsqViolM , &(pCounter->windowViolation));
            mPmcCounterGet(PMCMpigMpFlowMrruPkt           , &(pCounter->mrruExceed));
            }
        }

    return cAtOk;
    }

static uint32 PmcMpegOffset(AtChannel flow, uint8 partId)
    {
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet(flow), cThaModulePmcMpeg, partId);
    }

static eAtRet MpegCntGet(ThaEthFlowController self, AtChannel flow, tAtEthFlowCounters *pCounter, eBool r2c)
    {
    static const uint32 cTimeoutMs = 20;
    uint32 counterAddress;
    uint16 actualCounterAddress;
    uint8 counterPosition;
    AtOsal osal;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint8 i;
    uint8 partId = PartIdGet(self, flow);
    uint8 allCounters[] = {cThaPMCMpegMpFlowGoodPkt,
                           cThaPMCMpegMpFlowFrag,
                           cThaPMCMpegMpFlowNullFrag,
                           cThaPMCMpegMpFlowGoodByte,
                           cThaPMCMpegMpFlowDiscPkt,
                           cThaPMCMpegMpFlowDiscbyte};

    uint32 pmcMpegOffset = PmcMpegOffset(flow, partId);

    if (!ThaPmcIndirectCounterIsAccessible(flow, cThaModulePmcMpeg, partId))
        return cAtErrorDevBusy;

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    counterAddress = CounterAddress(self, flow);
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, counterAddress);

    /* Mask of counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self,
                        cThaRegThalassaPMMpxgCntsAddressRequestCtrl + pmcMpegOffset,
                        longRegVal,
                        cThaNumDwordsInLongReg,
                        cThaModulePmcMpeg);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpeg, cTimeoutMs, partId))
        return cAtErrorDevBusy;

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(flow, cThaModulePmcMpeg, cTimeoutMs, partId))
            return cAtOk;

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self,
                               cThaRegThalassaPMMpxgCntsIndrRegRptStat + pmcMpegOffset,
                               longRegVal,
                               cThaNumDwordsInLongReg,
                               cThaModulePmcMpeg) == 0)
            return cAtErrorDevFail;

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &actualCounterAddress);
        if (actualCounterAddress != counterAddress)
            return cAtErrorDevFail;

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);

        if (pCounter != NULL)
            {
            mPmcCounterGet(PMCMpegMpFlowGoodByte, &(pCounter->rxByte));
            mPmcCounterGet(PMCMpegMpFlowGoodPkt,  &(pCounter->rxPacket));
            mPmcCounterGet(PMCMpegMpFlowGoodByte, &(pCounter->rxGoodByte));
            mPmcCounterGet(PMCMpegMpFlowGoodPkt,  &(pCounter->rxGoodPkt));
            mPmcCounterGet(PMCMpegMpFlowFrag,     &(pCounter->rxFragment));
            mPmcCounterGet(PMCMpegMpFlowNullFrag, &(pCounter->rxNullFragment));
            mPmcCounterGet(PMCMpegMpFlowDiscPkt,  &(pCounter->rxDiscardPkt));
            mPmcCounterGet(PMCMpegMpFlowDiscbyte, &(pCounter->rxDiscardByte));
            }
        }

    return cAtOk;
    }

static eAtRet AllCountersReadToClear(ThaEthFlowController self, AtChannel flow, void *pAllCounters, eBool clear)
    {
    eAtRet              ret;
    AtOsal              osal = AtSharedDriverOsalGet();
    tAtEthFlowCounters  *counters = (tAtEthFlowCounters *)pAllCounters;

    if (counters == NULL)
        return cAtErrorNullPointer;

    mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(tAtEthFlowCounters));

    ret = MpegCntGet(self, flow, counters, clear);
    if (ret != cAtOk)
        return ret;

    ret = MpigCntGet(self, flow, counters, clear);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet AllCountersGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return AllCountersReadToClear(self, (AtChannel)flow, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters)
    {
    return AllCountersReadToClear(self, (AtChannel)flow, pAllCounters, cAtTrue);
    }

static void OverrideThaEthFlowController(ThaStmMpEthFlowController self)
    {
    ThaEthFlowController flow = (ThaEthFlowController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowControllerMethods = mMethodsGet(flow);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowControllerOverride, m_ThaEthFlowControllerMethods, sizeof(m_ThaEthFlowControllerOverride));
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersGet);
        mMethodOverride(m_ThaEthFlowControllerOverride, AllCountersClear);
        mMethodOverride(m_ThaEthFlowControllerOverride, Debug);
        }

    mMethodsSet(flow, &m_ThaEthFlowControllerOverride);
    }

static void Override(ThaStmMpEthFlowController self)
    {
    OverrideThaEthFlowController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmMpEthFlowController);
    }

static ThaEthFlowController ObjectInit(ThaEthFlowController self, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaMpEthFlowControllerObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowController ThaStmMpEthFlowControllerNew(AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController newFlow = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlow, module);
    }

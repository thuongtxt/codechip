/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthPort10GbReg.h
 * 
 * Created Date: Feb 18, 2014
 *
 * Description : 10G ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPORT10GBREG_H_
#define _THAETHPORT10GBREG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaEthPortReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cThaReg10GMacBaseAddress 0x3C1000

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Global Interface Control
Reg Addr: 0x02
          The address format for these registers is 0x02 + EthTripPrjOffset
          Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Reg Desc: This register is Interface Control
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl (0x000002)

/*--------------------------------------
BitField Name: IpEth10GRxUserSpeed
BitField Type: R/W
BitField Desc: User speed at RX side (38.88Mhz per unit)
BitField Bits: 5_0
--------------------------------------*/

/*--------------------------------------
BitField Name: IpEth10GHigigEn
BitField Type: R/W
BitField Desc: Enable ZTE Higig mode. If disable, normal mode in the 802.3 standard is used
    0x1: enable
    0x0: disable
BitField Bits: 8
--------------------------------------*/
#define cThaIpEth10GHigigEnableMask             cBit8
#define cThaIpEth10GHigigEnableShift            8

/*--------------------------------------
BitField Name: IpEth10GDicEn
BitField Type: R/W
BitField Desc: Enable DIC (deficit idle count) mode
    0x1: enable
    0x0: disable
BitField Bits: 9
--------------------------------------*/

/*--------------------------------------
BitField Name: IpEth10GSwicthDis
BitField Type: R/W
BitField Desc: Disable switch ETH port function. If disable, only user of core#1
is connected to port#1 and only user of core#2 is connected to port#2
    0x1: enable
    0x0: disable
BitField Bits: 11
--------------------------------------*/
#define cThaIpEth10GSwicthDisableMask          cBit11
#define cThaIpEth10GSwicthDisableShift         11

/*--------------------------------------
BitField Name: IpEth10GRxSwicthPort
BitField Type: R/W
BitField Desc: current port which the user receive data if switch ETH port function is enable
    bit[0]: current port which user of core#1 receive data
    bit[1]: current port which user of core#2 receive data
BitField Bits: 13_12
--------------------------------------*/
#define cThaIpEth10GRxSwicthPortMask    cBit13_12
#define cThaIpEth10GRxSwicthPortShift   12

/*--------------------------------------
BitField Name: IpEth10GTxSwicthPort
BitField Type: R/W
BitField Desc: current port which the user receive data if switch ETH port function is enable
    bit[0]: current port which user of core#1 receive data
    bit[1]: current port which user of core#2 receive data
BitField Bits: 15_14
--------------------------------------*/
#define cThaIpEth10GTxSwicthPortMask    cBit15_14
#define cThaIpEth10GTxSwicthPortShift   14

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Preamble Value#1 Control
Reg Addr: 0x05
         The address format for these registers is 0x05 + EthTripPrjOffset
Where:   EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Reg Desc: This register is value of preamble field
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpdPreambleVal1Ctrl (0x000005)

/*--------------------------------------
BitField Name: IpEth10GDatPreValue
BitField Type: R/W
BitField Desc: Value of preamble:
               With Higig mode: value is 8'h00
               With normal mode: value is 8'h55
BitField Bits: 7_0
--------------------------------------*/
#define cThaIpEth10GDatPreValueMask            cBit7_0
#define cThaIpEth10GDatPreValueShift           0

/*--------------------------------------
BitField Name: IpEth10GStartPreValue
BitField Type: R/W
BitField Desc: Value of start of preamble
BitField Bits: 15_8
--------------------------------------*/
#define cThaIpEth10GStartPreValueMask          cBit15_8
#define cThaIpEth10GStartPreValueShift         8

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Preamble Value#2 Control
Reg Addr: 0x06
         The address format for these registers is 0x06 + EthTripPrjOffset
Where:   EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Reg Desc: This register is value of preamble field
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpdPreambleVal2Ctrl (0x000006)

/*--------------------------------------
BitField Name: IpEth10GEndFrameValue
BitField Type: R/W
BitField Desc: Value to indicate end of frame
BitField Bits: 7_0
--------------------------------------*/
#define cThaIpEth10GEndFrameValueMask            cBit7_0
#define cThaIpEth10GEndFrameValueShift           0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Preamble Value#3 Control
Reg Addr: 0x07
         The address format for these registers is 0x07 + EthTripPrjOffset
Where:   EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Reg Desc: This register is value of preamble field
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpdPreambleVal3Ctrl (0x000007)

/*--------------------------------------
BitField Name: IpEth10GErrCodeValue
BitField Type: R/W
BitField Desc: Error code
BitField Bits: 7_0
--------------------------------------*/
#define cThaIpEth10GErrCodeValueMask            cBit7_0
#define cThaIpEth10GErrCodeValueShift           0

/*--------------------------------------
BitField Name: IpEth10GIdleCodeValue
BitField Type: R/W
BitField Desc: Idle code
BitField Bits: 15_8
--------------------------------------*/
#define cThaIpEth10GIdleCodeValueMask           cBit15_8
#define cThaIpEth10GIdleCodeValueShift          8

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Link State
Reg Addr: 0x0E
         The address format for these registers is 0x0E + EthTripPrjOffset
Where:   EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Reg Desc: This register is indicate link status
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpeedLinkStatus (0x00000E)

/*--------------------------------------
BitField Name: IpEth10GLinkState
BitField Type: R/W
BitField Desc: Link statu
    1: Up
    0: down
BitField Bits: 0
--------------------------------------*/
#define cThaIpEth10GLinkStateMask(portId)      (cBit0 << (portId))

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Simple MAC Active Control
Reg Addr: 0x40
          The address format for these registers is 0x40 + EthTripPrjOffset
          Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Reg Desc: This register is Active Control
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl (0x000040)

/*--------------------------------------
BitField Name: IpEthTripSmacLoopin
BitField Type: R/W
BitField Desc: Loop In Enable for port#1 (TX -> RX)
    1: Enable
    0: Disable
BitField Bits:
--------------------------------------*/
#define cThaIpEthTripSmacLoopinPortMask(portId)                 (cBit1 << (portId))
#define cThaIpEthTripSmacLoopinPortShift(portId)                (1 + (portId))

/*--------------------------------------
BitField Name: IpEthTripSmacLoopout1
BitField Type: R/W
BitField Desc:  Loop out Enable for port#1 (RX → TX)
    1: Enable
    0: Disable
BitField Bits: Bit5
--------------------------------------*/
#define cThaIpEthTripSmacLoopoutPortMask(portId)                 (cBit5 << (portId))
#define cThaIpEthTripSmacLoopoutPortShift(portId)                (5 + (portId))

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Receiver Control
The address format for these registers is 0x42 + EthTripPrjOffset
Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Description: This register is Simple MAC Receiver Control
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbSpeedMacReceiverCtrl (0x000042)

/*--------------------------------------
BitField Name: IpEth10GSmacRxFcsPass
BitField Type: R/W
BitField Desc: Enable pass 4 byte FCS
    1: Enable
    0: Disable
BitField Bits: Bit0
--------------------------------------*/
#define cThaIpEth10GSmacRxFcsPassMask                 cBit0
#define cThaIpEth10GSmacRxFcsPassShift                0

/*--------------------------------------
BitField Name: IpEth10GSmacRxIpg
BitField Type: R/W
BitField Desc: Number of Inter packet Gap can detected for Receiver sides
BitField Bits: Bit11:8
--------------------------------------*/
#define cThaIpEth10GSmacRxIpgMask                    cBit11_8
#define cThaIpEth10GSmacRxIpgShift                   8

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Transmitter Control
The address format for these registers is 0x43 + EthTripPrjOffset
Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Description: This register is Simple MAC Transmitter Control
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbSpeedMacTransmitCtrl (0x000043)

/*--------------------------------------
BitField Name: IpEth10GSmacTxPauDis
BitField Type: R/W
BitField Desc: Enable Pause  frame generator
    1: Disable
    0: Enable
BitField Bits: Bit1
--------------------------------------*/
#define cThaIpEth10GSmacTxPauDisMask                  cBit1
#define cThaIpEth10GSmacTxPauDisShift                 1

/*--------------------------------------
BitField Name: IpEth10GSamcTxPadDis
BitField Type: R/W
BitField Desc: Enable PAD insertion
    1: Disable
    0: Enable
BitField Bits: Bit2
--------------------------------------*/

/*--------------------------------------
BitField Name: IpEth10GSmacTxIpg
BitField Type: R/W
BitField Desc: Enable PAD insertion
    1: Disable
    0: Enable
BitField Bits: Bit12:8
--------------------------------------*/
#define cThaIpEth10GSmacTxIpgMask                     cBit12_8
#define cThaIpEth10GSmacTxIpgShift                    8

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Pause Frame Quanta Control
The address format for these registers is 0x47 + EthTripPrjOffset
Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Description: This register is value of Quanta in the Pause Frame in case of standard 802.3 10G
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlQuanta (0x000047)

/*--------------------------------------
BitField Name: IpEth10GSmacPauseQuanta
BitField Type: R/W
BitField Desc: Quanta value in Pause Frame
BitField Bits: Bit15:0
--------------------------------------*/
#define cThaIpEth10GSmacPauseQuantaMask                 cBit15_0
#define cThaIpEth10GSmacPauseQuantaShift                0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Pause Frame Timer Control
The address format for these registers is 0x46 + EthTripPrjOffset
Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Description: This register is interval between the two Pause Frame
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlTimeIntvl (0x000046)

/*--------------------------------------
BitField Name: IpEth10GSmacPauseTimer
BitField Type: R/W
BitField Desc: Interval between the two Pause Frame. The unit is 1 clk@156.52Mhz
BitField Bits: Bit15:0
--------------------------------------*/
#define cThaIpEth10GSmacPauseTimerMask                 cBit15_0
#define cThaIpEth10GSmacPauseTimerShift                0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Pause Frame Threshold Control
The address format for these registers is 0x56 + EthTripPrjOffset
Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
Description: TThis register is the configured threshold to generate Pause Frame
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlThreshold (0x000056)

/*--------------------------------------
BitField Name: IpEth10GSmacPauseThresLow
BitField Type: R/W
BitField Desc:  Lower-threshold of the buffer to generate Pause Frame to start traffic
from the Far Sender (only for higig mode). There are 8 values of these fields for 8 threshold of the buffer.
BitField Bits: Bit2:0
--------------------------------------*/
#define cThaIpEth10GSmacPauseThresLowMask                 cBit2_0
#define cThaIpEth10GSmacPauseThresLowShift                0

/*--------------------------------------
BitField Name: IpEth10GSmacPauseThresHigh
BitField Type: R/W
BitField Desc: Upper-threshold of the buffer to generate Pause Frame to stop traffic from the Far Sender
BitField Bits: Bit6:4
--------------------------------------*/
#define cThaIpEth10GSmacPauseThresHighMask                 cBit6_4
#define cThaIpEth10GSmacPauseThresHighShift                4

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Pause Frame Counter Port1
The address format for these registers is 0x58 + EthTripPrjOffset + R2C
Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset
    R2C: = 0 : read only
         = 1 : read-to-clear
Description: This register is counter of Pause Frame in the port#1
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripSpdSimpleMACflowCtrlCounterP1 (0x000058)
#define cThaRegIpEthernet10GbTripSpdSimpleMACflowCtrlCounterP2 (0x00005A)

/*--------------------------------------
BitField Name: IpEth10GSmacPauseCounterP1
BitField Type: R/W
BitField Desc:
BitField Bits: Bit15:0
--------------------------------------*/
#define cThaIpEth10GSmacPauseCounterMask                   cBit15_0
#define cThaIpEth10GSmacPauseCounterShift                  0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Global Mode Control
The address format for these registers is 0x04 + EthTripPrjOffset
Where: EthTripPrjOffset: Ethernet Triple Speed  Project address offset

Description: This register is Interface Control
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTripSpdGlobalModeCtrl (0x000004)

/*--------------------------------------
BitField Name: IpEth10GSpeed
BitField Type: R/W
BitField Desc:
BitField Bits: Bit1:0
--------------------------------------*/
#define cThaIpEth10GSpeedMask                    cBit1_0
#define cThaIpEth10GSpeedShift                   0

/*--------------------------------------
BitField Name: IpEth10GInterface
BitField Type: R/W
BitField Desc:
BitField Bits: Bit4
--------------------------------------*/
#define cThaIpEth10GInterfaceMask                 cBit4
#define cThaIpEth10GInterfaceShift                4

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Tx Packet Counter
Address: 0x80
The address format for these registers is 0x80 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: This register is counter of transmitted packets
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbSpeedMacTranmsitPackets (0x80)

/*--------------------------------------
BitField Name: IpEth10GSmacTxPktCnt
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GSmacTxPktCntMask               cBit31_0
#define cThaIpEth10GSmacTxPktCntShift              0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Tx Byte Counter
Address: 0x82
The address format for these registers is 0x82 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: This register is counter of transmitted bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbSpeedMacTranmsitBytes (0x82)

/*--------------------------------------
BitField Name: IpEth10GSmacTxByteCnt
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GSmacTxByteCntMask              cBit31_0
#define cThaIpEth10GSmacTxByteCntShift             0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length Transmit smaller than 64 bytes counter
Address: 0x84
The address format for these registers is 0x84 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet with length smaller than 64 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenTransmitsmallerthan64bytes (0x84)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenTransmitsmallerthan64bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenTransmitsmallerthan64bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenTransmitsmallerthan64bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length Transmit from 65 to 127 bytes counter
Address: 0x86
The address format for these registers is 0x86 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet Transmit with length from 65 to 127 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenTransmitfrom65to127bytes (0x86)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenTransmitfrom65to127bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenTransmitfrom65to127bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenTransmitfrom65to127bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length Transmit from 128 to 255 bytes counter
Address: 0x88
The address format for these registers is 0x88 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet Transmit with length from 128 to 255 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenTransmitfrom128to255bytes (0x88)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenTransmitfrom128to255bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenTransmitfrom128to255bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenTransmitfrom128to255bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length Transmit from 256 to 511 bytes counter
Address: 0x8A
The address format for these registers is 0x8A + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet Transmit with length from 256 to 511 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenTransmitfrom256to511bytes (0x8A)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenTransmitfrom256to511bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenTransmitfrom256to511bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenTransmitfrom256to511bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length Transmit from 512 to 1023 bytes counter
Address: 0x8C
The address format for these registers is 0x8C + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet Transmit with length from 512 to 1023 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenTransmitfrom512to1023bytes (0x8C)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenTransmitfrom512to1023bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenTransmitfrom512to1023bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenTransmitfrom512to1023bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length Transmit from 1024 to 1528 bytes counter
Address: 0x8E
The address format for these registers is 0x8E + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet Transmit with length from 1024 to 1528 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenTransmitfrom1024to1528bytes (0x8E)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenTransmitfrom1024to1528bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenTransmitfrom1024to1528bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenTransmitfrom1024to1528bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Transmit Packet Jumbo Length bytes counter
Address: 0x90
The address format for these registers is 0x90 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet Transmit with Jumbo Length
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbTransmitPacketJumboLenbytes (0x90)

/*--------------------------------------
BitField Name: IpEth10GbTransmitPacketJumboLenbytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbTransmitPacketJumboLenbytesMask      cBit31_0
#define cThaIpEth10GbTransmitPacketJumboLenbytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Rx Packet Counter
Address: 0x92
The address format for these registers is 0x92 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: This register is counter of received packets
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbSpeedMacReceivedPackets (0x92)

/*--------------------------------------
BitField Name: IpEth10GSmacRxPktCnt
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GSmacRxPktCntMask                 cBit31_0
#define cThaIpEth10GSmacRxPktCntShift                0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Rx Byte Counter
Address: 0x94
The address format for these registers is 0x94 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: This register is counter of received bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbSpeedMacReceivedBytes (0x94)

/*--------------------------------------
BitField Name: IpEth10GSmacRxByteCnt
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GSmacRxByteCntMask                 cBit31_0
#define cThaIpEth10GSmacRxByteCntShift                0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Speed Simple MAC Rx FCS Error Packet Counter
Address: 0x96
The address format for these registers is 0x96 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: This register is counter of received FCS error packets
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbSpeedMacReceivedFcsErrorPackets (0x96)

/*--------------------------------------
BitField Name: IpEth10GSmacRxFCSErrPktCnt
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GSmacRxFCSErrPktCntMask                   cBit31_0
#define cThaIpEth10GSmacRxFCSErrPktCntShift                  0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length received smaller than 64 bytes counter
Address: 0x98
The address format for these registers is 0x98 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet with length received smaller than 64 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenReceivedsmallerthan64bytes (0x98)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenReceivedsmallerthan64bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenReceivedsmallerthan64bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenReceivedsmallerthan64bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length received from 65 to 127 bytes counter
Address: 0x9A
The address format for these registers is 0x9A + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet with length received from 65 to 127 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenReceivedfrom65to127bytes (0x9A)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenReceivedfrom65to127bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenReceivedfrom65to127bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenReceivedfrom65to127bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length Received from 128 to 255 bytes counter
Address: 0x9C
The address format for these registers is 0x9C + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet with length received from 128 to 255 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenReceivedfrom128to255bytes (0x9C)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenReceivedfrom128to255bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenReceivedfrom128to255bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenReceivedfrom128to255bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length received from 256 to 511 bytes counter
Address: 0x9E
The address format for these registers is 0x9E + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet with length received from 256 to 511 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenReceivedfrom256to511bytes (0x9E)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenReceivedfrom256to511bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenReceivedfrom256to511bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenReceivedfrom256to511bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length received from 512 to 1023 bytes counter
Address: 0xA0
The address format for these registers is 0xA0 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet with length received from 512 to 1023 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenReceivedfrom512to1023bytes (0xA0)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenReceivedfrom512to1023bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenReceivedfrom512to1023bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenReceivedfrom512to1023bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Length received from 1024 to 1528 bytes counter
Address: 0xA2
The address format for these registers is 0xA2 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet with length received from 1024 to 1528 bytes
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbPacketLenReceivedfrom1024to1528bytes (0xA2)

/*--------------------------------------
BitField Name: IpEth10GbPacketLenReceivedfrom1024to1528bytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbPacketLenReceivedfrom1024to1528bytesMask      cBit31_0
#define cThaIpEth10GbPacketLenReceivedfrom1024to1528bytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Jumbo Length bytes counter received
Address: 0xA4
The address format for these registers is 0xA4 + EthPhyPortID*0x40 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet received with Jumbo Length
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbReceivedPacketJumboLenbytes (0xA4)

/*--------------------------------------
BitField Name: IpEth10GbPacketJumboLenbytes
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaIpEth10GbReceivedPacketJumboLenbytesMask      cBit31_0
#define cThaIpEth10GbReceivedPacketJumboLenbytesShift     0

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet 10G Packet Discard byte counter received
Address: 0xA6
The address format for these registers is 0xA6 + EthPhyPortID*0x10 + EthTripPrjOffset + R2C
Where: EthPhyPortID: Ethernet Physical Port ID
       EthTripPrjOffset: Ethernet Triple Speed  Project address offset
       R2C: = 1 : read only
            = 0 : read-to-clear
Description: The register provides counter for number of packet discard at received direction
------------------------------------------------------------------------------*/
#define cThaRegIpEthernet10GbReceivedPacketDiscard       (0xA6)

/*--------------------------------------
BitField Name: IpEthernet10GbReceivedPacketDiscard
BitField Type: R/W
BitField Desc:
BitField Bits: Bit31:0
--------------------------------------*/
#define cThaRegIpEthernet10GbReceivedPacketDiscardMask      cBit31_0
#define cThaRegIpEthernet10GbReceivedPacketDiscardShift     0
#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORT10GBREG_H_ */


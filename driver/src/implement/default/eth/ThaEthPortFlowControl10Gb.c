/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Eth Module
 *
 * File        : ThaEthPortFlowControl10Gb.c
 *
 * Created Date: Feb 21, 2014
 *
 * Description : Eth Port Flow Control 10Gb
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEth.h"
#include "ThaEthPortFlowControlInternal.h"
#include "ThaEthPortInternal.h"
#include "ThaEthPort10GbReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEthFlowControlMethods m_AtEthFlowControlOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PortOffset(AtEthFlowControl self)
    {
    ThaEthPort port = (ThaEthPort)AtEthFlowControlChannelGet(self);
    return ThaEthPortMacBaseAddress(port);
    }

static uint32 SimpleMACflowCtrlCounterRegister(AtEthFlowControl self, eBool read2Clear)
    {
    AtEthPort port = (AtEthPort)AtEthFlowControlChannelGet(self);
    uint32 offset = PortOffset(self) + (read2Clear ? 1 : 0);

    if (AtChannelIdGet((AtChannel)port) == 0)
        return cThaRegIpEthernet10GbTripSpdSimpleMACflowCtrlCounterP1 + offset;
    else
        return cThaRegIpEthernet10GbTripSpdSimpleMACflowCtrlCounterP2 + offset;
    }

static uint32 FlowCtrlThresholdRegister(AtEthFlowControl self)
    {
    return cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlThreshold + PortOffset(self);
    }

static uint32 TransmitCtrlRegister(AtEthFlowControl self)
    {
    return cThaRegIpEthernet10GbSpeedMacTransmitCtrl + PortOffset(self);
    }

static uint16 TxPauseFrameCounterRead2Clear(AtEthFlowControl self, eBool read2Clear)
    {
    uint32 regAddr  = SimpleMACflowCtrlCounterRegister(self, read2Clear);
    uint32 regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return (uint16)mRegField(regValue, cThaIpEth10GSmacPauseCounter);
    }

static uint16 TxPauseFrameCounterGet(AtEthFlowControl self)
    {
    return TxPauseFrameCounterRead2Clear(self, cAtFalse);
    }

static uint16 TxPauseFrameCounterClear(AtEthFlowControl self)
    {
    return TxPauseFrameCounterRead2Clear(self, cAtTrue);
    }

static uint16 RxPauseFrameCounterGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static uint16 RxPauseFrameCounterClear(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static eAtModuleEthRet HighThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
    uint32 regAddr  = FlowCtrlThresholdRegister(self);
    uint32 regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regValue, cThaIpEth10GSmacPauseThresHigh, threshold);
    AtEthFlowControlWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint32 HighThresholdGet(AtEthFlowControl self)
    {
    uint32 regValue = AtEthFlowControlRead(self, FlowCtrlThresholdRegister(self), cAtModuleEth);
    return mRegField(regValue, cThaIpEth10GSmacPauseThresHigh);
    }

static eAtModuleEthRet LowThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
    uint32 regAddr = FlowCtrlThresholdRegister(self);
    uint32 regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regValue, cThaIpEth10GSmacPauseThresLow, threshold);
    AtEthFlowControlWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint32 LowThresholdGet(AtEthFlowControl self)
    {
    uint32 regValue = AtEthFlowControlRead(self, FlowCtrlThresholdRegister(self), cAtModuleEth);
    return mRegField(regValue, cThaIpEth10GSmacPauseThresLow);
    }

static eAtModuleEthRet Enable(AtEthFlowControl self, eBool enable)
    {
    uint32 regAddr  = TransmitCtrlRegister(self);
    uint32 regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regValue, cThaIpEth10GSmacTxPauDis, enable);
    AtEthFlowControlWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eBool IsEnabled(AtEthFlowControl self)
    {
    uint32 regValue = AtEthFlowControlRead(self, TransmitCtrlRegister(self), cAtModuleEth);
    return mBinToBool(mRegField(regValue, cThaIpEth10GSmacTxPauDis));
    }

static eAtModuleEthRet PauseFramePeriodSet(AtEthFlowControl self, uint32 pauseFrameInterval)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlTimeIntvl + PortOffset(self);
    uint32 regValue;

    regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GSmacPauseTimer, pauseFrameInterval);
    AtEthFlowControlWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint32 PauseFramePeriodGet(AtEthFlowControl self)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlTimeIntvl + PortOffset(self);
    uint32 regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return mRegField(regValue, cThaIpEth10GSmacPauseTimer);
    }

static eAtModuleEthRet PauseFrameQuantaSet(AtEthFlowControl self, uint32 quanta)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlQuanta + PortOffset(self);
    uint32 regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GSmacPauseQuanta, quanta);
    AtEthFlowControlWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint32 PauseFrameQuantaGet(AtEthFlowControl self)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpdSimpleMACflowCtrlQuanta + PortOffset(self);
    uint32 regValue = AtEthFlowControlRead(self, regAddr, cAtModuleEth);
    return mRegField(regValue, cThaIpEth10GSmacPauseQuanta);
    }

static void OverrideAtEthFlowControl(AtEthFlowControl self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowControlOverride, mMethodsGet(self), sizeof(m_AtEthFlowControlOverride));

        mMethodOverride(m_AtEthFlowControlOverride, TxPauseFrameCounterGet);
        mMethodOverride(m_AtEthFlowControlOverride, TxPauseFrameCounterClear);
        mMethodOverride(m_AtEthFlowControlOverride, RxPauseFrameCounterGet);
        mMethodOverride(m_AtEthFlowControlOverride, RxPauseFrameCounterClear);
        mMethodOverride(m_AtEthFlowControlOverride, HighThresholdSet);
        mMethodOverride(m_AtEthFlowControlOverride, HighThresholdGet);
        mMethodOverride(m_AtEthFlowControlOverride, LowThresholdSet);
        mMethodOverride(m_AtEthFlowControlOverride, LowThresholdGet);
        mMethodOverride(m_AtEthFlowControlOverride, Enable);
        mMethodOverride(m_AtEthFlowControlOverride, IsEnabled);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodSet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodGet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFrameQuantaSet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFrameQuantaGet);
        }

    mMethodsSet(self, &m_AtEthFlowControlOverride);
    }

static void Override(AtEthFlowControl self)
    {
    OverrideAtEthFlowControl(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthPortFlowControl10Gb);
    }

static AtEthFlowControl ObjectInit(AtEthFlowControl self, AtModule module, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortFlowControlObjectInit(self, module, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthFlowControl ThaEthPortFlowControl10GbNew(AtModule module, AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlowControl newFlowCtrl = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlowCtrl == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newFlowCtrl, module, port);
    }

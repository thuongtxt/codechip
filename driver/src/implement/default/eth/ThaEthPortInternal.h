/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : ThaEthPortInternal.h
 * 
 * Created Date: Sep 20, 2012
 *
 * Description : Thalassa Ethernet port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPORTINTERNAL_H_
#define _THAETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/eth/AtEthPortInternal.h"
#include "ThaEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mSaveCounter(fieldName, call, counterType)                             \
    if (AtChannelCounterIsSupported(self, counterType))                        \
        {                                                                      \
        values = call;                                                         \
        if (counters)                                                          \
            counters->fieldName = values;                                      \
        }

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthPortMethods
    {
    eBool (*FcsByPassed)(ThaEthPort self);

    /* Loop back set */
    eAtRet (*LoopInEnable)(ThaEthPort self, eBool enable);
    eBool (*LoopInIsEnabled)(ThaEthPort self);

    eAtRet (*LoopOutEnable)(ThaEthPort self, eBool enable);
    eBool (*LoopOutIsEnabled)(ThaEthPort self);

	/* To control hardware switching/bridging */
    eAtRet (*HwSwitch)(ThaEthPort self, ThaEthPort toPort);
    eAtRet (*HwSwitchRelease)(ThaEthPort self);
    eAtRet (*HwBridge)(ThaEthPort self, ThaEthPort toPort);
    eAtRet (*HwBridgeRelease)(ThaEthPort self);

    eAtRet (*MacActivate)(ThaEthPort self, eBool activate);
    eAtRet (*PcsReset)(ThaEthPort self);
    eAtRet (*HwAutoControlPhysicalEnable)(ThaEthPort self, eBool enable);
    eBool (*PllIsLocked)(AtEthPort self);
    uint32 (*MaxBandwidthInKbps)(ThaEthPort self);
    uint32 (*RemainingBpsOfMaxBandwidth)(ThaEthPort self);

    /* Counter */
    uint32 (*OutComingBytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutComingPackets)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketLensmaller64bytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketLenfrom65to127bytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketLenfrom128to255bytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketLenfrom256to511bytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketLenfrom512to1024bytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketLenfrom1025to1528bytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketLenfrom1529to2047bytes)(ThaEthPort self, eBool r2c);
    uint32 (*OutPacketJumboLength)(ThaEthPort self, eBool r2c);
    uint32 (*InComBytes)(ThaEthPort self, eBool r2c);
    uint32 (*InComPackets)(ThaEthPort self, eBool r2c);
    uint32 (*InComErrorFcs)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketLensmaller64bytes)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketLenfrom65to127bytes)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketLenfrom128to255bytes)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketLenfrom256to511bytes)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketLenfrom512to1024bytes)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketLenfrom1025to1528bytes)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketLenfrom1529to2047bytes)(ThaEthPort self, eBool r2c);
    uint32 (*InPacketJumboLength)(ThaEthPort self, eBool r2c);
    uint32 (*InDiscardedPackets)(ThaEthPort self, eBool r2c);
    uint32 (*CounterLocalAddress)(ThaEthPort self, uint16 counterType);

    uint32 (*DefaultOffset)(ThaEthPort port);
    void (*ShowCurrentBandwidth)(ThaEthPort self);
    }tThaEthPortMethods;

typedef struct tThaEthPortBandwidth
    {
    uint32 bandWidthInKbps;
    uint32 bandWidthInBps;
    }tThaEthPortBandwidth;

typedef struct tThaEthPort
    {
    tAtEthPort super;
    tThaEthPortMethods *methods;

    /* Private data */
    uint8 isEnabled; /* Hardware does not support enable/disable, so let do it by software */
    uint8 srcMac[cAtMacAddressLen];
    uint8 destMac[cAtMacAddressLen];

    /* Physical */
    AtSerdesController serdesController;

    /* For APS */
    AtEthPort switchToPort;
    AtEthPort switchFromPort;
    AtEthPort bridgeToPort;
    AtEthPort bridgeFromPort;

    /* For bandwidth */
    tThaEthPortBandwidth provisionedBandWidth;
    tThaEthPortBandwidth runningBandWidth;

    AtList listeners;
    }tThaEthPort;

typedef struct tThaEthPort10GbMethods
    {
    eBool (*SupportCounters)(ThaEthPort10Gb self);
    eBool (*DebugCounterIsSupported)(ThaEthPort10Gb self, uint8 counterType);
    uint8 (*DefaultTxIpgGet)(ThaEthPort10Gb self);
    uint8 (*DefaultRxIpgGet)(ThaEthPort10Gb self);
    }tThaEthPort10GbMethods;

typedef struct tThaEthPort10Gb
    {
    tThaEthPort super;
    const tThaEthPort10GbMethods* methods;

    }tThaEthPort10Gb;

typedef struct tListenerWraper
    {
    void* userData;
    tAtEthPortPropertyListener listener;
    }tListenerWraper;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort ThaEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort ThaStmEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort ThaEthPort10GbObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

void ThaEthPortShowCurrentBandwidth(ThaEthPort self);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORTINTERNAL_H_ */


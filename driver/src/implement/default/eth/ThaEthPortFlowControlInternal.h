/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthPortFlowControlInternal.h
 * 
 * Created Date: Feb 18, 2014
 *
 * Description : ETH Port flow control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPORTFLOWCONTROLINTERNAL_H_
#define _THAETHPORTFLOWCONTROLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/eth/AtEthFlowControlInternal.h"
#include "ThaEthPortFlowControl.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthPortFlowControl
    {
    tAtEthFlowControl super;
    }tThaEthPortFlowControl;

typedef struct tThaEthPortFlowControl10Gb
    {
    tThaEthPortFlowControl super;
    }tThaEthPortFlowControl10Gb;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlowControl ThaEthPortFlowControlObjectInit(AtEthFlowControl self, AtModule module, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORTFLOWCONTROLINTERNAL_H_ */


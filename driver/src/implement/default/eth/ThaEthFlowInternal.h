/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthFlowInternal.h
 *
 * Created Date: Aug 31, 2012
 *
 * Author      : ntdung
 *
 * Description : ETH Flow
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAETHFLOWINTERNAL_H_
#define _THAETHFLOWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMpBundle.h"
#include "AtPppLink.h"
#include "AtFrVirtualCircuit.h"
#include "../../../generic/eth/AtEthFlowInternal.h"
#include "./controller/ThaEthFlowControllerInternal.h"
#include "ThaEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtEthFlowVlanTypeLength       2
#define cAtEthFlowVlanLength           2
#define cThaPsnHdrMaxLength            128

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthFlowSimulation
    {
    /* Egress PSN header */
    uint8 egressPsnHeader[64];
    uint32 egressPsnHeaderLength;
    }tThaEthFlowSimulation;

typedef struct tThaEthFlow
    {
    tAtEthFlow super;

    /* Private data */
    uint32 hwFlowId;
    uint8  classId;
    eBool egTrafficEn;
	uint16 cvlanTpid;
	uint16 svlanTpid;
	
    /* For simulation */
    tThaEthFlowSimulation *simulation;

    /* Database to store flow configuration information */
    tAtEthVlanFlowConfig flowConfig;

    /* To cached the controller which helped to bind this flow to link or bundle,
     * After binding, flow also need to know which controller helped it to bind to use in another job */
    ThaEthFlowController flowController;
    }tThaEthFlow;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow ThaEthFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module);

eAtRet ThaPppEthFlowControllerAllCountersFromPppLinkGet(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters);
eAtRet ThaPppEthFlowControllerAllCountersFromPppLinkClear(ThaEthFlowController self, AtEthFlow flow, void *pAllCounters);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHFLOWINTERNAL_H_ */


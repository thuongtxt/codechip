/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaEthPort.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa Ethernet port default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtNumber.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/eth/AtEthPortInternal.h"
#include "../../../generic/physical/AtSerdesControllerInternal.h"
#include "../cla/ThaModuleCla.h"
#include "../cos/ThaModuleCos.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "../pw/adapters/ThaPwAdapter.h"
#include "../diag/querier/ThaQueriers.h"

#include "ThaEthPortInternal.h"
#include "ThaModuleEth.h"
#include "ThaEthPortReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaEthPort)self)
#define mDefaultOffset(self) (ThaEthPortDefaultOffset(mThis(self)) + ThaEthPortMacBaseAddress(mThis(self)))
#define mPortId(self) (uint8)AtChannelIdGet((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEthPortMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtEthPortMethods    m_AtEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethod  = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool PllCanBeChecked(AtEthPort self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    return ThaModuleEthPortPllCanBeChecked(ethModule, mPortId(self));
    }

static eBool IsSimulated(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceIsSimulated(device);
    }

static eBool PllIsLocked(AtEthPort self)
    {
    uint32 regVal, address;
    tAtOsalCurTime curTime, startTime;
    uint32 timeout;
    uint32 lockedCount;
    AtOsal osal;
    
    static const uint32 cThaPllClkWaitTimeToRecheck = 50000; /* 50ms */
    static const uint32 cThaPllClkCheckTimeout      = 5000;
    static const uint8  cThaPllClkCheckMaxCount     = 10;
    static const uint16 cThaPllClkGeCheckPattern    = 0x253;

    if (IsSimulated(self))
        return cAtTrue;

    /* If PLL checking is not applicable, just ignore */
    if (!PllCanBeChecked(self))
        return cAtTrue;

    timeout     = 0;
    lockedCount = 0;
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (timeout < cThaPllClkCheckTimeout)
        {
        /* Make sure that the read pattern is the same with the written one */
        address = cThaRegIPEthernetTripleSpdGlbCtrl + mDefaultOffset(self);
        mChannelHwWrite(self, address, cThaPllClkGeCheckPattern, cAtModuleEth);
        regVal = mChannelHwRead(self, address, cAtModuleEth);
        if (regVal == cThaPllClkGeCheckPattern)
            lockedCount++;
        else
            lockedCount = 0;

        /* PLL is stable */
        if (lockedCount == cThaPllClkCheckMaxCount)
            return cAtTrue;

        /* Calculate elapse time to detect timeout */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        timeout = mTimeIntervalInMsGet(startTime, curTime);

        /* Give hardware 50ms */
        mMethodsGet(osal)->USleep(osal, cThaPllClkWaitTimeToRecheck);
        }

    /* PLL is not stable */
    return cAtFalse;
    }

static ThaModuleEth EthModule(ThaEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static eAtRet PcsReset(ThaEthPort self)
    {
    uint32 offset = mDefaultOffset(self);
    uint32 regVal;

    /* Speed Global Control */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdGlbCtrl + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCtlElDepthMask,
              cThaIpEthTripGlbCtlElDepthShift,
              cThaIpEthTripGlbCtlElDepthRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCtlSpStbMask,
              cThaIpEthTripGlbCtlSpStbShift,
              cThaIpEthTripGlbCtlSpStbRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCtlSpIsPhyMask,
              cThaIpEthTripGlbCtlSpIsPhyShift,
              cThaIpEthTripGlbCtlSpIsPhyRstVal);

    mFieldIns(&regVal,
              cThaIpEthTripGlbCtlRstMask,
              cThaIpEthTripGlbCtlRstShift,
              cThaIpEthTripGlbCtlRstRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCtlIntEnMask,
              cThaIpEthTripGlbCtlIntEnShift,
              cThaIpEthTripGlbCtlIntEnRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCtlActMask,
              cThaIpEthTripGlbCtlActShift,
              cThaIpEthTripGlbCtlActRstVal);
    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdGlbCtrl + offset, regVal, cAtModuleEth);

    /* Speed Global Interface Control */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdGlbIntfCtrl + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCfgTbiMask,
              cThaIpEthTripGlbCfgTbiShift,
              cThaIpEthTripGlbCfgTbiRstVal);
    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdGlbIntfCtrl + offset, regVal, cAtModuleEth);

    /* Speed PCS Advertisement */
    regVal = mChannelHwRead(self,
                            cThaRegIPEthernetTripleSpdPcsAdvertisementWith1000BASEXMd + offset,
                            cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripPcsAnAdvPauMask,
              cThaIpEthTripPcsAnAdvPauShift,
              cThaIpEthTripPcsAnAdvPauRstVal);
    mChannelHwWrite(self,
                    cThaRegIPEthernetTripleSpdPcsAdvertisementWith1000BASEXMd + offset,
                    regVal,
                    cAtModuleEth);

    /* Speed PCS An Next Page Transmitter */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsAnNextPageTxter + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripPcsAnNpTxNextPageMask,
              cThaIpEthTripPcsAnNpTxNextPageShift,
              cThaIpEthTripPcsAnNpTxNextPageRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripPcsAnNpTxMsgPageMask,
              cThaIpEthTripPcsAnNpTxMsgPageShift,
              cThaIpEthTripPcsAnNpTxMsgPageRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripPcsAnNpTxAckMask,
              cThaIpEthTripPcsAnNpTxAckShift,
              cThaIpEthTripPcsAnNpTxAckRstVal);
    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdPcsAnNextPageTxter + offset, regVal, cAtModuleEth);

    /* Speed PCS Option Control */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsOptCtrl + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripPcsCtlFifoAdjC2Mask,
              cThaIpEthTripPcsCtlFifoAdjC2Shift,
              cThaIpEthTripPcsCtlFifoAdjC2RstVal);
    mFieldIns(&regVal,
              cThaIpEthTripPcsCtlFifoDepthMask,
              cThaIpEthTripPcsCtlFifoDepthShift,
              cThaIpEthTripPcsCtlFifoDepthRstVal);
    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdPcsOptCtrl + offset, regVal, cAtModuleEth);

   /* Speed PCS Option Link Timer */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsOptLinkTimer + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripPcsCfgLinkTimerSgmiiMask,
              cThaIpEthTripPcsCfgLinkTimerSgmiiShift,
              cThaIpEthTripPcsCfgLinkTimerSgmiiRstVal);

    mFieldIns(&regVal,
              cThaIpEthTripPcsCfgLinkTimerBasexMask,
              cThaIpEthTripPcsCfgLinkTimerBasexShift,
              cThaIpEthTripPcsCfgLinkTimerBasexRstVal);

    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdPcsOptLinkTimer + offset, regVal, cAtModuleEth);

    /* Speed PCS Option Sync Timer */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsOptSyncTimer + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripPcsCfgSyncTimerMask,
              cThaIpEthTripPcsCfgSyncTimerShift,
              cThaIpEthTripPcsCfgSyncTimerRstVal);
    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdPcsOptSyncTimer + offset, regVal, cAtModuleEth);

    /* Speed PCS Option PRBS Test Control */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsOptPrpsTestCtrl + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIPEthTripPcsTestFixDatMask,
              cThaIPEthTripPcsTestFixDatShift,
              cThaIPEthTripPcsTestFixDatRstVal);
    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdPcsOptPrpsTestCtrl + offset, regVal, cAtModuleEth);

    /* Speed Simple MAC Active Control */
    ThaEthPortMacActivate(mThis(self), cAtTrue);

    /* Speed Simple MAC Receiver Control */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripSmacRxMaskMask,
              cThaIpEthTripSmacRxMaskShift,
              cThaIpEthTripSmacRxMaskRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripSmacRxPauSaChkMask,
              cThaIpEthTripSmacRxPauSaChkShift,
              cThaIpEthTripSmacRxPauSaChkRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripSamcRxPauDaChkMask,
              cThaIpEthTripSamcRxPauDaChkShift,
              cThaIpEthTripSamcRxPauDaChkRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripSmacRxPauDiChkMask,
              cThaIpEthTripSmacRxPauDiChkShift,
              cThaIpEthTripSmacRxPauDiChkRstVal);
    mFieldIns(&regVal,
              cThaIpEthTripSmacRxFcsPassMask,
              cThaIpEthTripSmacRxFcsPassShift,
              mMethodsGet(self)->FcsByPassed(self) ? 1 : 0);
    mChannelHwWrite(self, cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl + offset, regVal, cAtModuleEth);

    /* Speed Simple MAC Data Inter Frame */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdSimpleMACDataInterFrm + offset, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripSmacIdleTxDataMask,
              cThaIpEthTripSmacIdleTxDataShift,
              cThaIpEthTripSmacIdleTxDataRstVal);
    mChannelHwWrite(self,
                    cThaRegIPEthernetTripleSpdSimpleMACDataInterFrm + offset,
                    regVal,
                    cAtModuleEth);

    return cAtOk;
    }

static eAtRet HwAutoControlPhysicalEnable(ThaEthPort self, eBool enable)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdGlbCtrl + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cThaIpEthTripCfgCtlSelect, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static void InUsedBandwidthReset(ThaEthPort self)
    {
    AtOsalMemInit(&self->provisionedBandWidth, 0, sizeof(self->provisionedBandWidth));
    AtOsalMemInit(&self->runningBandWidth, 0, sizeof(self->runningBandWidth));
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtEthPort port;

    /* Do not need to initialize unused ports */
    if (!ThaModuleEthPortIsUsed(EthModule(mThis(self)), mPortId(self)))
        return cAtOk;

    /* Check if PLL is locked */
    if (!mMethodsGet(mThis(self))->PllIsLocked((AtEthPort)self))
        mChannelError(self, cAtErrorDevicePllNotLocked);

    mMethodsGet(mThis(self))->PcsReset(mThis(self));

    /* Super */
    ret = m_AtChannelMethod->Init(self);
    if (ret != cAtOk)
        return ret;

    /* As hardware recommend, RX IPG <= (TX IPG - 4). And the safe RX IPG
     * should be 4. The following values are recommended by hardware. */
    port = (AtEthPort)self;
    if (AtEthPortTxIpgIsConfigurable(port))
        AtEthPortTxIpgSet(port, 12);
    if (AtEthPortRxIpgIsConfigurable(port))
        AtEthPortRxIpgSet(port, 4);

    /* Let software fully control hardware physical parameters */
    mMethodsGet(mThis(self))->HwAutoControlPhysicalEnable(mThis(self), cAtFalse);

    InUsedBandwidthReset(mThis(self));

    return ret;
    }

static eAtRet LoopInEnable(ThaEthPort self, eBool enable)
    {
    uint32 regVal, regAddr;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopinMask, cThaIpEthTripSmacLoopinShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    uint32 regVal, regAddr;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacLoopoutMask, cThaIpEthTripSmacLoopoutShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;

    /* Just one mode should be enabled at a time */
    switch (loopbackMode)
        {
        case cAtLoopbackModeLocal:
            ret |= mMethodsGet(mThis(self))->LoopOutEnable(mThis(self), cAtFalse);
            ret |= mMethodsGet(mThis(self))->LoopInEnable(mThis(self), cAtTrue);
            break;

        case cAtLoopbackModeRemote:
            ret |= mMethodsGet(mThis(self))->LoopInEnable(mThis(self), cAtFalse);
            ret |= mMethodsGet(mThis(self))->LoopOutEnable(mThis(self), cAtTrue);
            break;

        case cAtLoopbackModeRelease:
            ret |= mMethodsGet(mThis(self))->LoopInEnable(mThis(self), cAtFalse);
            ret |= mMethodsGet(mThis(self))->LoopOutEnable(mThis(self), cAtFalse);
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    return ret;
    }

static eBool LoopInIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cThaIpEthTripSmacLoopinMask)
        return cAtTrue;

    return cAtFalse;
    }

static eBool LoopOutIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cThaIpEthTripSmacLoopoutMask)
        return cAtTrue;

    return cAtFalse;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (mMethodsGet(mThis(self))->LoopInIsEnabled(mThis(self)))
        return cAtLoopbackModeLocal;

    if (mMethodsGet(mThis(self))->LoopOutIsEnabled(mThis(self)))
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static void ListenerDelete(AtObject listener)
    {
    AtOsalMemFree(listener);
    }

static void ListenerListDelete(AtEthPort self)
    {
    AtListDeleteWithObjectHandler(mThis(self)->listeners, ListenerDelete);
    mThis(self)->listeners = NULL;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->serdesController);
    mThis(self)->serdesController = NULL;

    ListenerListDelete((AtEthPort)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static ThaModuleCla ClaModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    ThaClaController claController = (ThaClaController)ThaModuleClaEthPortControllerGet(ClaModule(self));
    AtSerdesController serdes;

    ThaEthPortSoftEnable((ThaEthPort)self, enable);
    serdes = AtEthPortSerdesController((AtEthPort)self);
    if (serdes && AtSerdesControllerCanEnable(serdes, enable))
        AtSerdesControllerEnable(serdes, enable);

    ThaClaControllerChannelEnable(claController, self, enable);

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return mThis(self)->isEnabled;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regVal;
    uint8  alarm;

    /* Check if link is down */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsOptStat + mDefaultOffset(self), cAtModuleEth);
    mFieldGet(regVal, cThaIpEthTripPcsStaLinkFailMask, cThaIpEthTripPcsStaLinkFailShift, uint8, &alarm);

    return alarm ? cAtEthPortAlarmLinkUp : cAtEthPortAlarmLinkDown;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 regVal;
    uint8  alarm;

    /* Check if link is down */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsOptStat + mDefaultOffset(self), cAtModuleEth);
    mFieldGet(regVal, cThaIpEthTripPcsIntLinkReqMask, cThaIpEthTripPcsIntLinkReqShift, uint8, &alarm);

    return alarm ? cAtEthPortAlarmLinkDown : cAtEthPortAlarmLinkUp;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 alarm = DefectHistoryGet(self);
    uint32 regAddr = cThaRegIPEthernetTripleSpdPcsOptStat + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    regVal |= cThaIpEthTripPcsIntLinkReqMask;
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return alarm;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtEthPortAlarmLinkDown;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regValue = 0;
    uint32 regAddr;

    /* Link down */
    if (defectMask & cAtEthPortAlarmLinkDown)
        {
        regAddr = cThaRegIPEthernetTripleSpdPcsOptCtrl + mDefaultOffset(self);
        regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
        mFieldIns(&regValue, cThaIpEthTripPcsIntLinkEnMask, cThaIpEthTripPcsIntLinkEnShift, (enableMask & cAtEthPortAlarmLinkDown) ? 1 : 0);
        mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);
        }

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regVal = 0;

    /* Link down */
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsOptCtrl + mDefaultOffset(self), cAtModuleEth);
    if (regVal & cThaIpEthTripPcsIntLinkEnMask)
        return cAtEthPortAlarmLinkDown;

    return 0;
    }

static ThaModuleCos CosModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModuleCos)AtDeviceModuleGet(device, cThaModuleCos);
    }

static uint32 CounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    ThaModuleCos cosModule = CosModule(self);
    ThaModuleCla claModule = ClaModule(self);
    AtEthPort ethPort = (AtEthPort)self;

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets:
            return mMethodsGet(mThis(self))->OutComingPackets(mThis(self), read2Clear);
        case cAtEthPortCounterTxBytes:
            return mMethodsGet(mThis(self))->OutComingBytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxOamPackets:
            return ThaModuleCosEthPortOamTxPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPeriodOamPackets:
            return ThaModuleCosEthPortOamFrequencyTxPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPwPackets:
            return ThaModuleCosEthPortPWTxPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen0_64:
            return mMethodsGet(mThis(self))->OutPacketLensmaller64bytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxPacketsLen65_127:
            return mMethodsGet(mThis(self))->OutPacketLenfrom65to127bytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxPacketsLen128_255:
            return mMethodsGet(mThis(self))->OutPacketLenfrom128to255bytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxPacketsLen256_511:
            return mMethodsGet(mThis(self))->OutPacketLenfrom256to511bytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxPacketsLen512_1024:
            return mMethodsGet(mThis(self))->OutPacketLenfrom512to1024bytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxPacketsLen1025_1528:
            return mMethodsGet(mThis(self))->OutPacketLenfrom1025to1528bytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxPacketsLen1529_2047:
            return mMethodsGet(mThis(self))->OutPacketLenfrom1529to2047bytes(mThis(self), read2Clear);
        case cAtEthPortCounterTxPacketsJumbo:
            return mMethodsGet(mThis(self))->OutPacketJumboLength(mThis(self), read2Clear);
        case cAtEthPortCounterTxTopPackets:
            return ThaModuleCosEthPortPktTimePktToPRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPackets:
            return mMethodsGet(mThis(self))->InComPackets(mThis(self), read2Clear);
        case cAtEthPortCounterRxBytes:
            return mMethodsGet(mThis(self))->InComBytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxErrEthHdrPackets:
            return ThaModuleClaRxHdrErrPacketsRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrBusPackets:
            return ThaModuleClaEthPktBusErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrFcsPackets:
            return mMethodsGet(mThis(self))->InComErrorFcs(mThis(self), read2Clear);
        case cAtEthPortCounterRxOversizePackets:
            return ThaModuleClaEthPktoversizeRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxUndersizePackets:
            return ThaModuleClaEthPktundersizeRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen0_64:
            return mMethodsGet(mThis(self))->InPacketLensmaller64bytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxPacketsLen65_127:
            return mMethodsGet(mThis(self))->InPacketLenfrom65to127bytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxPacketsLen128_255:
            return mMethodsGet(mThis(self))->InPacketLenfrom128to255bytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxPacketsLen256_511:
            return mMethodsGet(mThis(self))->InPacketLenfrom256to511bytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxPacketsLen512_1024:
            return mMethodsGet(mThis(self))->InPacketLenfrom512to1024bytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxPacketsLen1025_1528:
            return mMethodsGet(mThis(self))->InPacketLenfrom1025to1528bytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxPacketsLen1529_2047:
            return mMethodsGet(mThis(self))->InPacketLenfrom1529to2047bytes(mThis(self), read2Clear);
        case cAtEthPortCounterRxPacketsJumbo:
            return mMethodsGet(mThis(self))->InPacketJumboLength(mThis(self), read2Clear);
        case cAtEthPortCounterRxPwUnsupportedPackets:
            return ThaModuleClaPWunSuppedRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrPwLabelPackets:
            return ThaModuleClaHCBElookupnotmatchPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxDiscardedPackets:
            return mMethodsGet(mThis(self))->InDiscardedPackets(mThis(self), read2Clear);
        case cAtEthPortCounterRxErrPausePackets:
            return ThaModuleClaPauFrmErrPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxBrdCastPackets:
            return ThaModuleClaBcastRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMultCastPackets:
            return ThaModuleClaMcastRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxArpPackets:
            return ThaModuleClaARPRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxOamPackets:
            return ThaModuleClaEthOamRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxEfmOamPackets:
            return ThaModuleClaEthOamRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrEfmOamPackets:
            return ThaModuleClaEthOamRxErrPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxEthOamType1Packets:
            return ThaModuleClaEthOamtype0RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxEthOamType2Packets:
            return ThaModuleClaEthOamtype1RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIpv4Packets:
            return ThaModuleClaIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrIpv4Packets:
            return ThaModuleClaIPv4PktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIcmpIpv4Packets:
            return ThaModuleClaICMPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIpv6Packets:
            return ThaModuleClaIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrIpv6Packets:
            return ThaModuleClaIPv6PktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIcmpIpv6Packets:
            return ThaModuleClaICMPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMefPackets:
            return ThaModuleClaMEFRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrMefPackets:
            return ThaModuleClaMEFErrRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsPackets:
            return ThaModuleClaMPLSRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrMplsPackets:
            return ThaModuleClaMPLSErrRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsErrOuterLblPackets:
            return ThaModuleClaMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsIpv4Packets:
            return ThaModuleClaMPLSoverIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsIpv6Packets:
            return ThaModuleClaMPLSoverIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrL2tpv3Packets:
            return ThaModuleClaL2TPPktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrUdpPackets:
            return ThaModuleClaUDPPktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxUdpIpv4Packets:
            return ThaModuleClaUDPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxUdpIpv6Packets:
            return ThaModuleClaUDPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrPsnPackets:
            return ThaModuleClaRxErrPsnPacketsRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsSendToCpu:
            return ThaModuleClaEthPktForwardtoCpuRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsSendToPw:
            return ThaModuleClaEthPktForwardtoPDaRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxTopPackets:
            return ThaModuleClaEthPktTimePktToPRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketDaMis:
            return 0;

        case cAtEthPortCounterRxL2tpv3Ipv4Packets:
            return ThaModuleClaL2TPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxL2tpv3Ipv6Packets:
            return ThaModuleClaL2TPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxMplsDataPackets:
            return ThaModuleClaMPLSDataRxPktwithPHPMdRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaMPLSDataRxPktwithoutPHPMdRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxLdpIpv4Packets:
            return ThaModuleClaLDPIPv4withPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxLdpIpv6Packets:
            return ThaModuleClaLDPIPv6withPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxL2tpv3Packets:
            return ThaModuleClaL2TPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaL2TPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxUdpPackets:
            return ThaModuleClaUDPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaUDPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterTxPausePackets:
            if (read2Clear)
                return AtEthFlowControlTxPauseFrameCounterClear(AtEthPortFlowControlGet(ethPort));
            else
                return AtEthFlowControlTxPauseFrameCounterGet(AtEthPortFlowControlGet(ethPort));

        case cAtEthPortCounterRxPausePackets:
            {
            AtEthFlowControl flowControl = AtEthPortFlowControlGet(ethPort);
            if (flowControl)
                {
                if (read2Clear)
                    return AtEthFlowControlRxPauseFrameCounterClear(flowControl);
                return AtEthFlowControlRxPauseFrameCounterGet(flowControl);
                }

            return ThaModuleClaPauFrmRxPktRead2Clear(claModule, ethPort, read2Clear);
            }

        default:
            return 0;
        }
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtTrue);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterReadToClear(self, counterType, cAtFalse);
    }

static uint32 CounterLocalAddress(ThaEthPort self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cInvalidUint32;
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool read2Clear)
    {
    tAtEthPortCounters *counters = (tAtEthPortCounters *)pAllCounters;
    AtOsal osal = AtSharedDriverOsalGet();
    ThaModuleCos cosModule = CosModule(self);
    ThaModuleCla claModule = ClaModule(self);
    AtEthPort ethPort = (AtEthPort)self;
    uint32 values;

    /* Read-only must have input counters */
    if ((pAllCounters == NULL) && (!read2Clear))
        return cAtErrorNullPointer;

    if (counters)
        mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(tAtEthPortCounters));

    mSaveCounter(txPackets                ,
                 mMethodsGet(mThis(self))->OutComingPackets(mThis(self), read2Clear), cAtEthPortCounterTxPackets);
    mSaveCounter(txBytes                  ,
                 mMethodsGet(mThis(self))->OutComingBytes(mThis(self), read2Clear), cAtEthPortCounterTxBytes);
    mSaveCounter(txOamPackets             ,
                 ThaModuleCosEthPortOamTxPktRead2Clear(cosModule, ethPort, read2Clear), cAtEthPortCounterTxOamPackets);
    mSaveCounter(txPeriodOamPackets       ,
                 ThaModuleCosEthPortOamFrequencyTxPktRead2Clear(cosModule, ethPort, read2Clear), cAtEthPortCounterTxPeriodOamPackets);
    mSaveCounter(txPwPackets              ,
                 ThaModuleCosEthPortPWTxPktRead2Clear(cosModule, ethPort, read2Clear), cAtEthPortCounterTxPwPackets);
    mSaveCounter(txPacketsLen0_64         ,
                 mMethodsGet(mThis(self))->OutPacketLensmaller64bytes(mThis(self), read2Clear), cAtEthPortCounterTxPacketsLen0_64);
    mSaveCounter(txPacketsLen65_127       ,
                 mMethodsGet(mThis(self))->OutPacketLenfrom65to127bytes(mThis(self), read2Clear), cAtEthPortCounterTxPacketsLen65_127);
    mSaveCounter(txPacketsLen128_255      ,
                 mMethodsGet(mThis(self))->OutPacketLenfrom128to255bytes(mThis(self), read2Clear), cAtEthPortCounterTxPacketsLen128_255);
    mSaveCounter(txPacketsLen256_511      ,
                 mMethodsGet(mThis(self))->OutPacketLenfrom256to511bytes(mThis(self), read2Clear), cAtEthPortCounterTxPacketsLen256_511);
    mSaveCounter(txPacketsLen512_1024     ,
                 mMethodsGet(mThis(self))->OutPacketLenfrom512to1024bytes(mThis(self), read2Clear), cAtEthPortCounterTxPacketsLen512_1024);
    mSaveCounter(txPacketsLen1025_1528    ,
                 mMethodsGet(mThis(self))->OutPacketLenfrom1025to1528bytes(mThis(self), read2Clear), cAtEthPortCounterTxPacketsLen1025_1528);
    mSaveCounter(txPacketsJumbo           ,
                 mMethodsGet(mThis(self))->OutPacketJumboLength(mThis(self), read2Clear), cAtEthPortCounterTxPacketsJumbo);
    mSaveCounter(txTopPackets             ,
                 ThaModuleCosEthPortPktTimePktToPRead2Clear(cosModule, ethPort, read2Clear), cAtEthPortCounterTxTopPackets);

    mSaveCounter(rxPackets                ,
                 mMethodsGet(mThis(self))->InComPackets(mThis(self), read2Clear), cAtEthPortCounterRxPackets);
    mSaveCounter(rxBytes                  ,
                 mMethodsGet(mThis(self))->InComBytes(mThis(self), read2Clear), cAtEthPortCounterRxBytes);
    mSaveCounter(rxErrBusPackets          ,
                 ThaModuleClaEthPktBusErrRead2Clear(claModule, ethPort, read2Clear),cAtEthPortCounterRxErrBusPackets );
    mSaveCounter(rxErrFcsPackets          ,
                 mMethodsGet(mThis(self))->InComErrorFcs(mThis(self), read2Clear), cAtEthPortCounterRxErrFcsPackets);
    mSaveCounter(rxOversizePackets        ,
                 ThaModuleClaEthPktoversizeRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxOversizePackets);
    mSaveCounter(rxUndersizePackets       ,
                 ThaModuleClaEthPktundersizeRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxUndersizePackets);
    mSaveCounter(rxPacketsLen0_64         ,
                 mMethodsGet(mThis(self))->InPacketLensmaller64bytes(mThis(self), read2Clear), cAtEthPortCounterRxPacketsLen0_64);
    mSaveCounter(rxPacketsLen65_127       ,
                 mMethodsGet(mThis(self))->InPacketLenfrom65to127bytes(mThis(self), read2Clear), cAtEthPortCounterRxPacketsLen65_127);
    mSaveCounter(rxPacketsLen128_255      ,
                 mMethodsGet(mThis(self))->InPacketLenfrom128to255bytes(mThis(self), read2Clear), cAtEthPortCounterRxPacketsLen128_255);
    mSaveCounter(rxPacketsLen256_511      ,
                 mMethodsGet(mThis(self))->InPacketLenfrom256to511bytes(mThis(self), read2Clear), cAtEthPortCounterRxPacketsLen256_511);
    mSaveCounter(rxPacketsLen512_1024     ,
                 mMethodsGet(mThis(self))->InPacketLenfrom512to1024bytes(mThis(self), read2Clear), cAtEthPortCounterRxPacketsLen512_1024);
    mSaveCounter(rxPacketsLen1025_1528    ,
                 mMethodsGet(mThis(self))->InPacketLenfrom1025to1528bytes(mThis(self), read2Clear), cAtEthPortCounterRxPacketsLen1025_1528);
    mSaveCounter(rxPacketsJumbo           ,
                 mMethodsGet(mThis(self))->InPacketJumboLength(mThis(self), read2Clear), cAtEthPortCounterRxPacketsJumbo);
    mSaveCounter(rxPwUnsupportedPackets   ,
                 ThaModuleClaPWunSuppedRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxPwUnsupportedPackets);
    mSaveCounter(rxErrPwLabelPackets      ,
                 ThaModuleClaHCBElookupnotmatchPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrPwLabelPackets);
    mSaveCounter(rxDiscardedPackets       ,
                 mMethodsGet(mThis(self))->InDiscardedPackets(mThis(self), read2Clear),cAtEthPortCounterRxDiscardedPackets);
    mSaveCounter(rxPausePackets           ,
                 ThaModuleClaPauFrmRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxPausePackets);
    mSaveCounter(rxErrPausePackets        ,
                 ThaModuleClaPauFrmErrPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrPausePackets);
    mSaveCounter(rxBrdCastPackets         ,
                 ThaModuleClaBcastRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxBrdCastPackets);
    mSaveCounter(rxMultCastPackets        ,
                 ThaModuleClaMcastRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxMultCastPackets);
    mSaveCounter(rxArpPackets             ,
                 ThaModuleClaARPRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxArpPackets);
    mSaveCounter(rxOamPackets             ,
                 ThaModuleClaEthOamRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxOamPackets);
    mSaveCounter(rxEfmOamPackets          ,
                 ThaModuleClaEthOamRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxEfmOamPackets);
    mSaveCounter(rxErrEfmOamPackets       ,
                 ThaModuleClaEthOamRxErrPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrEfmOamPackets);
    mSaveCounter(rxEthOamType1Packets     ,
                 ThaModuleClaEthOamtype0RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxEthOamType1Packets);
    mSaveCounter(rxEthOamType2Packets     ,
                 ThaModuleClaEthOamtype1RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxEthOamType2Packets);
    mSaveCounter(rxIpv4Packets            ,
                 ThaModuleClaIPv4RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxIpv4Packets);
    mSaveCounter(rxErrIpv4Packets         ,
                 ThaModuleClaIPv4PktErrRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrIpv4Packets);
    mSaveCounter(rxIcmpIpv4Packets        ,
                 ThaModuleClaICMPv4RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxIcmpIpv4Packets);
    mSaveCounter(rxIpv6Packets            ,
                 ThaModuleClaIPv6RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxIpv6Packets);
    mSaveCounter(rxErrIpv6Packets         ,
                 ThaModuleClaIPv6PktErrRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrIpv6Packets);
    mSaveCounter(rxIcmpIpv6Packets        ,
                 ThaModuleClaICMPv6RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxIcmpIpv6Packets);
    mSaveCounter(rxMefPackets             ,
                 ThaModuleClaMEFRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxMefPackets);
    mSaveCounter(rxErrMefPackets          ,
                 ThaModuleClaMEFErrRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrMefPackets);
    mSaveCounter(rxMplsPackets            ,
                 ThaModuleClaMPLSRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxMplsPackets);
    mSaveCounter(rxErrMplsPackets         ,
                 ThaModuleClaMPLSErrRxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrMplsPackets);
    mSaveCounter(rxMplsErrOuterLblPackets ,
                 ThaModuleClaMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxMplsErrOuterLblPackets);
    mSaveCounter(rxMplsDataPackets        ,
                 ThaModuleClaMPLSDataRxPktwithPHPMdRead2Clear(claModule, ethPort, read2Clear) +
                 ThaModuleClaMPLSDataRxPktwithoutPHPMdRead2Clear(claModule, ethPort, read2Clear),
                 cAtEthPortCounterRxMplsDataPackets);
    mSaveCounter(rxLdpIpv4Packets         ,
                 ThaModuleClaLDPIPv4withPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear) +
                 ThaModuleClaLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear),
                 cAtEthPortCounterRxLdpIpv4Packets);
    mSaveCounter(rxLdpIpv6Packets         ,
                 ThaModuleClaLDPIPv6withPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear) +
                 ThaModuleClaLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear),
                 cAtEthPortCounterRxLdpIpv6Packets);
    mSaveCounter(rxMplsIpv4Packets        ,
                 ThaModuleClaMPLSoverIPv4RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxMplsIpv4Packets);
    mSaveCounter(rxMplsIpv6Packets        ,
                 ThaModuleClaMPLSoverIPv6RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxMplsIpv6Packets);
    mSaveCounter(rxErrL2tpv3Packets       ,
                 ThaModuleClaL2TPPktErrRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrL2tpv3Packets);
    mSaveCounter(rxL2tpv3Ipv4Packets      ,
                 ThaModuleClaL2TPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxL2tpv3Ipv4Packets);
    mSaveCounter(rxL2tpv3Ipv6Packets      ,
                 ThaModuleClaL2TPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxL2tpv3Ipv6Packets);
    mSaveCounter(rxErrUdpPackets          ,
                 ThaModuleClaUDPPktErrRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrUdpPackets);
    mSaveCounter(rxUdpIpv4Packets         ,
                 ThaModuleClaUDPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxUdpIpv4Packets);
    mSaveCounter(rxUdpIpv6Packets         ,
                 ThaModuleClaUDPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxUdpIpv6Packets);
    mSaveCounter(rxPacketsSendToCpu       ,
                 ThaModuleClaEthPktForwardtoCpuRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxPacketsSendToCpu);
    mSaveCounter(rxPacketsSendToPw        ,
                 ThaModuleClaEthPktForwardtoPDaRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxPacketsSendToPw);
    mSaveCounter(rxTopPackets             ,
                 ThaModuleClaEthPktTimePktToPRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxTopPackets);
    mSaveCounter(rxErrEthHdrPackets       ,
                 ThaModuleClaRxHdrErrPacketsRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrEthHdrPackets);
    mSaveCounter(rxErrPsnPackets          ,
                 ThaModuleClaRxErrPsnPacketsRead2Clear(claModule, ethPort, read2Clear), cAtEthPortCounterRxErrPsnPackets);

    if (counters)
        {
        counters->rxPacketDaMis            = 0;
        counters->rxL2tpv3Packets          = counters->rxL2tpv3Ipv4Packets + counters->rxL2tpv3Ipv6Packets;
        counters->rxUdpPackets             = counters->rxUdpIpv4Packets + counters->rxUdpIpv6Packets;
        }

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return ThaModuleEthLocalEthPortId(EthModule(mThis(self)), (AtEthPort)self);
    }

static eBool HasSourceMac(AtEthPort self)
    {
    return ThaModuleEthPortHasSourceMac(EthModule((ThaEthPort)self), mPortId(self));
    }

static eBool HasIpAddress(ThaEthPort self)
    {
    return ThaModuleEthPortHasIpAddress(EthModule(self), mPortId(self));
    }

static eAtRet AllOamFlowsSourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    ThaOamFlowManager manager = ThaModuleEthOamFlowManager(ethModule);
    return ThaOamFlowManagerAllOamFlowsSourceMacAddressSet(manager, self, address);
    }

static eAtRet AllFlowsSourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    eAtRet ret = cAtOk;
    AtEthFlow flow;
    AtIterator flowIterator = AtModuleEthFlowIteratorCreate((AtModuleEth)EthModule((ThaEthPort)self));
    uint8 portId;

    if (flowIterator == NULL)
        return cAtError;

    portId = mPortId(self);
    while ((flow = (AtEthFlow)AtIteratorNext(flowIterator)) != NULL)
        {
        tAtEthVlanDesc egVlan;
        if ((AtEthFlowEgressVlanGet(flow, &egVlan) == cAtOk) && egVlan.ethPortId == portId)
            ret |= AtEthFlowEgressSourceMacSet(flow, address);
        }

    AtObjectDelete((AtObject)flowIterator);

    /* Set SMAC for OAM flow if any */
    ret |= AllOamFlowsSourceMacAddressSet(self, address);
    return ret;
    }

static eAtRet SaveSourceMac(AtEthPort self, uint8 *address)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, mThis(self)->srcMac, address, cAtMacAddressLen);
    return cAtOk;
    }

static void SourceMacDidChangeNotify(AtEthPort self, uint8 *address)
    {
    tListenerWraper* wrapper;
    AtIterator iterator;

    if (mThis(self)->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(mThis(self)->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->listener.SourceMacDidChange)
            wrapper->listener.SourceMacDidChange(self, address, wrapper->userData);
        }

    AtObjectDelete((AtObject)iterator);
    }

static eBool SourceMacIsChanged(AtEthPort self, uint8 *address)
    {
    uint8 currentMac[cAtMacAddressLen];
    uint8 byte_i;

    if (AtEthPortSourceMacAddressGet(self, currentMac) != cAtOk)
        return cAtTrue;

    for (byte_i = 0; byte_i < cAtMacAddressLen; byte_i++)
        {
        if (currentMac[byte_i] != address[byte_i])
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    eAtRet ret = cAtOk;

    if (!SourceMacIsChanged(self, address))
        return SaveSourceMac(self, address);

    if (AtEthPortHasSourceMac(self))
        ret |= ThaModuleClaEthPortMacSet(ClaModule((AtChannel)self), self, address);

    SourceMacDidChangeNotify(self, address);
    if (!ThaModuleEthUsePortSourceMacForAllFlows(EthModule(mThis(self))))
        {
        SaveSourceMac(self, address);
        return ret;
        }

    /* Apply SMAC to all flows */
    ret |= AllFlowsSourceMacAddressSet(self, address);
    SaveSourceMac(self, address);
    return ret;
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    AtOsal osal;

    if (AtEthPortHasSourceMac(self))
        return ThaModuleClaEthPortMacGet(ClaModule((AtChannel)self), self, address);

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, address, ((ThaEthPort)self)->srcMac, cAtMacAddressLen);

    return cAtOk;
    }

static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, mThis(self)->destMac, address, cAtMacAddressLen);
    return cAtOk;
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, address, mThis(self)->destMac, cAtMacAddressLen);
    return cAtOk;
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort self, eBool enable)
    {
    return ThaModuleClaEthPortMacCheckEnable(ClaModule((AtChannel)self), self, enable);
    }

static eBool MacCheckingIsEnabled(AtEthPort self)
    {
    return ThaModuleClaEthPortMacCheckIsEnabled(ClaModule((AtChannel)self), self);
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
	AtUnused(self);
    switch (speed)
        {
        case cAtEthPortSpeed10M:
        case cAtEthPortSpeed100M:
        case cAtEthPortSpeed1000M:
            return cAtTrue;

        case cAtEthPortSpeed2500M:
        case cAtEthPortSpeed10G:
        case cAtEthPortSpeed40G:
        case cAtEthPortSpeedUnknown:
        case cAtEthPortSpeedAutoDetect:
        case cAtEthPortSpeedNA:
        default:
            return cAtFalse;
        }
    }

static uint8 PortSpeedSw2Hw(eAtEthPortSpeed speed)
    {
    if (speed == cAtEthPortSpeed10M)   return 0;
    if (speed == cAtEthPortSpeed100M)  return 1;
    if (speed == cAtEthPortSpeed1000M) return 2;

    /* Unused speed */
    return 3;
    }

/* Ethernet port speed */
static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 offset;
    uint8 hwSpeedMode;

    if (!SpeedIsSupported(self, speed))
        return cAtErrorModeNotSupport;

    /* Global speed */
    offset = mDefaultOffset(self);
    regAddr = cThaRegIPEthernetTripleSpdGlbIntfCtrl + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    hwSpeedMode = PortSpeedSw2Hw(speed);
    mFieldIns(&regVal, cThaIpEthTripGlbCfgSpdMask, cThaIpEthTripGlbCfgSpdShift, hwSpeedMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* PCS speed */
    regAddr = cThaRegIPEthernetTripleSpdPcsCtrl + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripPcsSpdLsbMask, cThaIpEthTripPcsSpdLsbShift, hwSpeedMode & cBit0);
    mFieldIns(&regVal, cThaIpEthTripPcsSpdMsbMask, cThaIpEthTripPcsSpdMsbShift, (hwSpeedMode >> 1) & cBit0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtEthPortSpeed PortSpeedHw2Sw(uint8 hwSpeed)
    {
    if (hwSpeed == 0) return cAtEthPortSpeed10M;
    if (hwSpeed == 1) return cAtEthPortSpeed100M;
    if (hwSpeed == 2) return cAtEthPortSpeed1000M;

    return cAtEthPortSpeedUnknown;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    uint32 regVal, address;
    uint8 hwSpeed;

    address = cThaRegIPEthernetTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModuleEth);
    mFieldGet(regVal, cThaIpEthTripGlbCfgSpdMask, cThaIpEthTripGlbCfgSpdShift, uint8, &hwSpeed);

    return PortSpeedHw2Sw(hwSpeed);
    }

static uint8 DuplexModeSw2Hw(eAtEthPortDuplexMode duplexMode)
    {
    if (duplexMode == cAtEthPortWorkingModeHalfDuplex) return 0;
    if (duplexMode == cAtEthPortWorkingModeFullDuplex) return 1;

    return 1;
    }

static eAtEthPortDuplexMode DuplexModeHw2Sw(uint8 hwDuplexMode)
    {
    if (hwDuplexMode == 0) return cAtEthPortWorkingModeHalfDuplex;
    if (hwDuplexMode == 1) return cAtEthPortWorkingModeFullDuplex;

    /* This mode not support */
    return cAtEthPortWorkingModeAutoDetect;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
    uint32 regVal, address;

    address = cThaRegIPEthernetTripleSpdPcsCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripPcsFullduplexMask, cThaIpEthTripPcsFullduplexShift, DuplexModeSw2Hw(duplexMode));
    mChannelHwWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
    uint32 regVal, address;
    uint8 hwDuplexMode;

    address = cThaRegIPEthernetTripleSpdPcsCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModuleEth);
    mFieldGet(regVal, cThaIpEthTripPcsFullduplexMask, cThaIpEthTripPcsFullduplexShift, uint8, &hwDuplexMode);

    return DuplexModeHw2Sw(hwDuplexMode);
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    uint32 regVal;
    uint32 regAddr;

    if (!AtEthPortTxIpgIsInRange(self, txIpg))
        return cAtErrorOutOfRangParm;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACTxterCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacTxIpgMask, cThaIpEthTripSmacTxIpgShift, txIpg);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    uint32 regVal;
    uint8  txIpg;
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdSimpleMACTxterCtrl + mDefaultOffset(self), cAtModuleEth);
    mFieldGet(regVal, cThaIpEthTripSmacTxIpgMask, cThaIpEthTripSmacTxIpgShift, uint8, &txIpg);
    return txIpg;
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    uint32 regVal;
    uint32 regAddr;

    if (!AtEthPortRxIpgIsInRange(self, rxIpg))
        return cAtErrorOutOfRangParm;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripSmacRxIpgMask, cThaIpEthTripSmacRxIpgShift, rxIpg);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    uint32 regVal;
    uint8  rxIpg;
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl + mDefaultOffset(self), cAtModuleEth);
    mFieldGet(regVal, cThaIpEthTripSmacRxIpgMask, cThaIpEthTripSmacRxIpgShift, uint8, &rxIpg);
    return rxIpg;
    }

static eAtEthPortAutoNegState AutoNegStateHw2Sw(uint32 hwState)
    {
    switch (hwState)
        {
        case 0x0: return cAtEthPortAutoNegEnable;
        case 0x1: return cAtEthPortAutoNegRestart;
        case 0x2: return cAtEthPortAutoNegDisableLinkOk;
        case 0x3: return cAtEthPortAutoNegAbilityDetect;
        case 0x4: return cAtEthPortAutoNegAckDetect;
        case 0x5: return cAtEthPortAutoNegNextPageWait;
        case 0x6: return cAtEthPortAutoNegCompleteAck;
        case 0x7: return cAtEthPortAutoNegIdleDetect;
        case 0x8: return cAtEthPortAutoNegLinkOk;
        default:
            return cAtEthPortAutoNegInvalid;
        }
    }

static eAtEthPortAutoNegState AutoNegStateGet(AtEthPort self)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdPcsOptFSMStat0 + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return AutoNegStateHw2Sw(mRegField(regVal, cThaIpEthTripPcsCurAnFsm));
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
    uint32 regVal;
    uint32 regAddr;

    /* Enable/disable Auto-Neg */
    regAddr = cThaRegIPEthernetTripleSpdPcsCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cThaIpEthTripPcsAnEnMask, cThaIpEthTripPcsAnEnShift, enable);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
    uint32 regVal;
    eBool enable;
    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdPcsCtrl + mDefaultOffset(self), cAtModuleEth);
    mFieldGet(regVal, cThaIpEthTripPcsAnEnMask, cThaIpEthTripPcsAnEnShift, eBool, &enable);
    return enable;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    uint32 regVal;
    uint32 regAddr;
    uint8  interfaceTbi;

    interfaceTbi = ((interface == cAtEthPortInterface1000BaseX) ||
                    (interface == cAtEthPortInterfaceSgmii)) ? 1 : 0;

    regAddr = cThaRegIPEthernetTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCfgSgmiiMask,
              cThaIpEthTripGlbCfgSgmiiShift,
              (interface == cAtEthPortInterfaceSgmii) ? 1 : 0);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCfgRgmiiMask,
              cThaIpEthTripGlbCfgRgmiiShift,
              (interface == cAtEthPortInterfaceRgmii) ? 1 : 0);
    mFieldIns(&regVal,
              cThaIpEthTripGlbCfgTbiMask,
              cThaIpEthTripGlbCfgTbiShift,
              interfaceTbi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    uint32 regVal;

    regVal = mChannelHwRead(self, cThaRegIPEthernetTripleSpdGlbIntfCtrl + mDefaultOffset(self), cAtModuleEth);
    if (regVal & cThaIpEthTripGlbCfgSgmiiMask)
        return cAtEthPortInterfaceSgmii;

    if (regVal & cThaIpEthTripGlbCfgRgmiiMask)
        return cAtEthPortInterfaceRgmii;

    if (!(regVal & cThaIpEthTripGlbCfgTbiMask))
        return cAtEthPortInterfaceGmii;

    /* 1000BaseX as default */
    return cAtEthPortInterface1000BaseX;
    }

static eAtRet AllConfigSet(AtChannel self, void *pAllConfig)
    {
    tAtEthPortAllConfig *configs = (tAtEthPortAllConfig *)pAllConfig;
    eAtRet ret = cAtOk;
    AtEthPort port = (AtEthPort)self;

    ret |= SourceMacAddressSet(port, configs->srcMacAddr);
    ret |= InterfaceSet(port, configs->interface);
    ret |= SpeedSet(port, configs->speed);
    ret |= DuplexModeSet(port, configs->workingMd);
    ret |= AutoNegEnable(port, configs->autoNegEn);
    ret |= TxIpgSet(port, (uint8)configs->numTxIpg);
    ret |= RxIpgSet(port, (uint8)configs->numRxIpg);
    ret |= AtEthPortFlowControlEnable(port, configs->flowCtrlEn);
    ret |= AtEthPortPauseFrameIntervalSet(port, configs->pauFrmInterval);
    ret |= AtEthPortPauseFrameExpireTimeSet(port, configs->pauFrmExpTime);
    ret |= MacCheckingEnable(port, configs->macChkEn);

    return ret;
    }

static eBool FcsByPassed(ThaEthPort self)
    {
    return ThaModuleEthPortShouldByPassFcs(EthModule(self), mPortId(self));
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    ThaEthSerdesManager serdesManager = ThaModuleEthSerdesManager(ethModule);

    if (serdesManager)
        return ThaEthSerdesManagerSerdesController(serdesManager, AtChannelIdGet((AtChannel)self));

    if (mThis(self)->serdesController == NULL)
        mThis(self)->serdesController = ThaModuleEthPortSerdesControllerCreate(ethModule, self);

    return mThis(self)->serdesController;
    }

static eAtModuleEthRet MaxPacketSizeSet(AtEthPort self, uint32 maxPacketSize)
    {
    return ThaModuleClaEthPortMaxPacketSizeSet(ClaModule((AtChannel)self), self, maxPacketSize);
    }

static uint32 MaxPacketSizeGet(AtEthPort self)
    {
    return ThaModuleClaEthPortMaxPacketSizeGet(ClaModule((AtChannel)self), self);
    }

static eAtModuleEthRet MinPacketSizeSet(AtEthPort self, uint32 minPacketSize)
    {
    return ThaModuleClaEthPortMinPacketSizeSet(ClaModule((AtChannel)self), self, minPacketSize);
    }

static uint32 MinPacketSizeGet(AtEthPort self)
    {
    return ThaModuleClaEthPortMinPacketSizeGet(ClaModule((AtChannel)self), self);
    }

static eAtEthPortInterface DefaultInterface(AtEthPort self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    if (ThaDeviceIsEp(device))
        return cAtEthPortInterface1000BaseX;
    return m_AtEthPortMethods->DefaultInterface(self);
    }

static uint32 ProvisionedBandwidthInKbpsGet(AtEthPort self)
    {
    return mThis(self)->provisionedBandWidth.bandWidthInKbps;
    }

static uint32 RunningBandwidthInKbpsGet(AtEthPort self)
    {
    return mThis(self)->runningBandWidth.bandWidthInKbps;
    }

static eAtModuleEthRet Switch(AtEthPort self, AtEthPort toPort)
    {
    eAtRet ret;

    if (!ThaModuleEthApsIsSupported((ThaModuleEth)AtChannelModuleGet((AtChannel)self)))
        return cAtErrorModeNotSupport;

    /* Release switch */
    if ((toPort == NULL) || (self == toPort))
        return AtEthPortSwitchRelease(self);

    /* Switch already was made */
    if (mThis(self)->switchToPort == toPort)
        return cAtOk;

    /* They are all busy */
    if ((mThis(self)->switchToPort || mThis(self)->switchFromPort) ||
        (mThis(toPort)->switchToPort || mThis(toPort)->switchFromPort))
        return cAtErrorChannelBusy;

    /*Set to hardware */
    ret = mMethodsGet(mThis(self))->HwSwitch(mThis(self), mThis(toPort));
    mThis(self)->switchToPort     = toPort;
    mThis(toPort)->switchFromPort = self;

    return ret;
    }

static AtEthPort SwitchedPortGet(AtEthPort self)
    {
    if (!ThaModuleEthApsIsSupported((ThaModuleEth)AtChannelModuleGet((AtChannel)self)))
        return NULL;

    return mThis(self)->switchToPort;
    }

static eAtModuleEthRet SwitchRelease(AtEthPort self)
    {
    AtEthPort switchedPort;
    eAtRet ret;

    if (!ThaModuleEthApsIsSupported((ThaModuleEth)AtChannelModuleGet((AtChannel)self)))
        return cAtOk;

    switchedPort = AtEthPortSwitchedPortGet(self);
    if (switchedPort == NULL)
        return cAtOk;

    ret = mMethodsGet(mThis(self))->HwSwitchRelease(mThis(self));
    mThis(self)->switchToPort = NULL;
    mThis(switchedPort)->switchFromPort = NULL;

    return ret;
    }

static eAtModuleEthRet Bridge(AtEthPort self, AtEthPort toPort)
    {
    eAtRet ret;

    if (!ThaModuleEthApsIsSupported((ThaModuleEth)AtChannelModuleGet((AtChannel)self)))
        return cAtErrorModeNotSupport;

    /* Release switch */
    if ((toPort == NULL) || (self == toPort))
        return AtEthPortBridgeRelease(self);

    /* Switch already was made */
    if (mThis(self)->bridgeToPort == toPort)
        return cAtOk;

    /* They are all busy */
    if ((mThis(self)->bridgeToPort   || mThis(self)->bridgeFromPort) ||
        (mThis(toPort)->bridgeToPort || mThis(toPort)->bridgeFromPort))
        return cAtErrorChannelBusy;

    ret = mMethodsGet(mThis(self))->HwBridge(mThis(self), mThis(toPort));

    mThis(self)->bridgeToPort     = toPort;
    mThis(toPort)->bridgeFromPort = self;

    return ret;
    }

static AtEthPort BridgedPortGet(AtEthPort self)
    {
    if (!ThaModuleEthApsIsSupported((ThaModuleEth)AtChannelModuleGet((AtChannel)self)))
        return NULL;

    return mThis(self)->bridgeToPort;
    }

static eAtModuleEthRet BridgeRelease(AtEthPort self)
    {
    AtEthPort bridgedPort = AtEthPortSwitchedPortGet(self);
    eAtRet ret;

    if (!ThaModuleEthApsIsSupported((ThaModuleEth)AtChannelModuleGet((AtChannel)self)))
        return cAtOk;

    if (bridgedPort == NULL)
        return cAtOk;

    ret = mMethodsGet(mThis(self))->HwBridgeRelease(mThis(self));
    mThis(self)->bridgeToPort = NULL;
    mThis(bridgedPort)->bridgeFromPort = NULL;

    return ret;
    }

static eAtRet HwSwitch(ThaEthPort self, ThaEthPort toPort)
    {
	AtUnused(toPort);
	AtUnused(self);
    return cAtError;
    }

static eAtRet HwSwitchRelease(ThaEthPort self)
    {
	AtUnused(self);
    return cAtError;
    }

static eAtRet HwBridge(ThaEthPort self, ThaEthPort toPort)
    {
	AtUnused(toPort);
	AtUnused(self);
    return cAtError;
    }

static eAtRet HwBridgeRelease(ThaEthPort self)
    {
	AtUnused(self);
    return cAtError;
    }

static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
    uint32 regAddr, regVal;
    uint8 hwActivate;

    if (!ThaModuleEthPortIsUsed(EthModule(mThis(self)), mPortId(self)))
        return cAtOk;

    regAddr = cThaRegIPEthernetTripleSpdSimpleMACActCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    hwActivate = activate ? 1 : 0;
    mFieldIns(&regVal, cThaIpEthTripSmacTxActMask, cThaIpEthTripSmacTxActShift, hwActivate);
    mFieldIns(&regVal, cThaIpEthTripSmacRxActMask, cThaIpEthTripSmacRxActShift, hwActivate);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
    if (HasIpAddress(mThis(self)))
        return ThaModuleClaEthPortIpV4Set(ClaModule((AtChannel)self), self, address);

    return cAtErrorNotApplicable;
    }

static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
    if (HasIpAddress(mThis(self)))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, address, 0x0, cAtIpv4AddressLen);
        return ThaModuleClaEthPortIpV4Get(ClaModule((AtChannel)self), self, address);
        }

    return cAtErrorNotApplicable;
    }

static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
    if (HasIpAddress(mThis(self)))
        return ThaModuleClaEthPortIpV6Set(ClaModule((AtChannel)self), self, address);
    return cAtErrorNotApplicable;
    }

static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
    if (HasIpAddress(mThis(self)))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, address, 0x0, cAtIpv6AddressLen);
        return ThaModuleClaEthPortIpV6Get(ClaModule((AtChannel)self), self, address);
        }

    return cAtErrorNotApplicable;
    }

static uint32 OutComingPackets(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortOutcomingPktRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutComingBytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortOutcomingbyteRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketLensmaller64bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktLensmallerthan64bytesRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketLenfrom65to127bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktLenfrom65to127bytesRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketLenfrom128to255bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktLenfrom128to255bytesRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketLenfrom256to511bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktLenfrom256to511bytesRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketLenfrom512to1024bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktLenfrom512to1024bytesRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketLenfrom1025to1528bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktLenfrom1025to1528bytesRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketLenfrom1529to2047bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktLenfrom1529to2047bytesRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 OutPacketJumboLength(ThaEthPort self, eBool r2c)
    {
    return ThaModuleCosEthPortPktJumboLenRead2Clear(CosModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InComBytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaIncombyteRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InComPackets(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaIncomPktRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InComErrorFcs(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktFcsErrRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketLensmaller64bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktLensmallerthan64bytesRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketLenfrom65to127bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktLenfrom65to127bytesRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketLenfrom128to255bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktLenfrom128to255bytesRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketLenfrom256to511bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktLenfrom256to511bytesRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketLenfrom512to1024bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktLenfrom512to1024bytesRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketLenfrom1025to1528bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktLenfrom1025to1528bytesRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketLenfrom1529to2047bytes(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktLenfrom1529to2047bytesRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InPacketJumboLength(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktJumboLenRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static uint32 InDiscardedPackets(ThaEthPort self, eBool r2c)
    {
    return ThaModuleClaEthPktDscdRead2Clear(ClaModule((AtChannel)self), (AtEthPort)self, r2c);
    }

static eAtRet WarmRestore(AtChannel self)
    {
    if (AtEthPortHasSourceMac((AtEthPort)(self)))
        {
        AtEthPort ethPort = (AtEthPort)self;
        uint8 mac[cAtMacAddressLen];
        eAtRet ret = ThaModuleClaEthPortMacGet(ClaModule(self), ethPort, mac);

        if (ret != cAtOk)
            return ret;

        SaveSourceMac(ethPort, mac);
        }

    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaEthPort object = (ThaEthPort)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(isEnabled);
    mEncodeUInt8Array(srcMac, cAtMacAddressLen);
    mEncodeUInt8Array(destMac, cAtMacAddressLen);
    mEncodeUInt(provisionedBandWidth.bandWidthInKbps);
    mEncodeUInt(provisionedBandWidth.bandWidthInBps);

    mEncodeNone(listeners);
    mEncodeNone(runningBandWidth);

    mEncodeObject(serdesController);

    mEncodeChannelIdString(switchToPort);
    mEncodeChannelIdString(switchFromPort);
    mEncodeChannelIdString(bridgeToPort);
    mEncodeChannelIdString(bridgeFromPort);
    }

static void ShowCurrentBandwidth(ThaEthPort self)
    {
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "* Bandwidth information:\r\n");
    AtPrintc(cSevNormal, "  Maximum bandwidth     : %s,%03u bps\r\n", AtNumberDigitGroupingString(mMethodsGet(self)->MaxBandwidthInKbps(self)), mMethodsGet(self)->RemainingBpsOfMaxBandwidth(self));
    AtPrintc(cSevNormal, "  Provisioned bandwidth : %s,%03u bps\r\n", AtNumberDigitGroupingString(self->provisionedBandWidth.bandWidthInKbps), self->provisionedBandWidth.bandWidthInBps);
    AtPrintc(cSevNormal, "  Running bandwidth     : %s,%03u bps\r\n", AtNumberDigitGroupingString(self->runningBandWidth.bandWidthInKbps), self->runningBandWidth.bandWidthInBps);
    }

static eAtRet Debug(AtChannel self)
    {
    /* Super */
    eAtRet ret = m_AtChannelMethod->Debug(self);
    if (ret != cAtOk)
        return ret;

    mMethodsGet(mThis(self))->ShowCurrentBandwidth(mThis(self));
    return cAtOk;
    }

/* Return true if increase */
static eBool AdjustmentCalculate(uint64 oldBwInBps, uint64 newBwInBps, tThaEthPortBandwidth* offset)
    {
    uint64 offsetInBps;
    eBool isIncrease;

    if (newBwInBps > oldBwInBps)
        {
        isIncrease = cAtTrue;
        offsetInBps = newBwInBps - oldBwInBps;
        }
    else
        {
        isIncrease = cAtFalse;
        offsetInBps = oldBwInBps - newBwInBps;
        }

    offset->bandWidthInKbps = (uint32)(offsetInBps / 1000);
    offset->bandWidthInBps  = (uint32)(offsetInBps % 1000);
    return isIncrease;
    }

static eBool BandwidthIsValid(ThaEthPort self, tThaEthPortBandwidth *bandwidth)
    {
    if (bandwidth->bandWidthInKbps > mMethodsGet(self)->MaxBandwidthInKbps(self))
        return cAtFalse;

    if (bandwidth->bandWidthInKbps < mMethodsGet(self)->MaxBandwidthInKbps(self))
        return cAtTrue;

    if (bandwidth->bandWidthInBps > mMethodsGet(self)->RemainingBpsOfMaxBandwidth(self))
        return cAtFalse;

    return cAtTrue;
    }

static tThaEthPortBandwidth* BandwidthPlus(tThaEthPortBandwidth bw1, const tThaEthPortBandwidth* bw2, tThaEthPortBandwidth* sum)
    {
    sum->bandWidthInKbps = bw1.bandWidthInKbps + bw2->bandWidthInKbps;
    sum->bandWidthInBps  = bw1.bandWidthInBps  + bw2->bandWidthInBps;

    if (sum->bandWidthInBps >= 1000)
        {
        sum->bandWidthInBps = sum->bandWidthInBps % 1000;
        sum->bandWidthInKbps += 1;
        }

    return sum;
    }

static tThaEthPortBandwidth* BandwidthMinus(tThaEthPortBandwidth minuend, const tThaEthPortBandwidth* subtrahend, tThaEthPortBandwidth* difference)
    {
    if (minuend.bandWidthInKbps < subtrahend->bandWidthInKbps)
        {
        difference->bandWidthInKbps = 0;
        difference->bandWidthInBps = 0;
        return difference;
        }

    difference->bandWidthInKbps = minuend.bandWidthInKbps - subtrahend->bandWidthInKbps;
    if (minuend.bandWidthInBps < subtrahend->bandWidthInBps)
        {
        if (difference->bandWidthInKbps == 0)
            difference->bandWidthInBps = 0;
        else
            {
            difference->bandWidthInBps = minuend.bandWidthInBps + 1000 - subtrahend->bandWidthInBps;
            difference->bandWidthInKbps -= 1;
            }
        }
    else
        difference->bandWidthInBps = minuend.bandWidthInBps - subtrahend->bandWidthInBps;

    return difference;
    }

static eBool ProvisionedBandwidthInBpsCanAdd(ThaEthPort self, const tThaEthPortBandwidth* addingBw)
    {
    tThaEthPortBandwidth sumBw;

    return BandwidthIsValid(self, BandwidthPlus(self->provisionedBandWidth, addingBw, &sumBw));
    }

static eBool ProvisionedBandwidthInBpsCanRemove(ThaEthPort self, const tThaEthPortBandwidth* removingBw)
    {
    if (self->provisionedBandWidth.bandWidthInKbps < removingBw->bandWidthInKbps)
        return cAtFalse;

    if ((self->provisionedBandWidth.bandWidthInKbps == removingBw->bandWidthInKbps) &&
        (self->provisionedBandWidth.bandWidthInBps < removingBw->bandWidthInBps))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 MaxBandwidthInKbps(ThaEthPort self)
    {
    /* Default layer should not check BW constrain */
    AtUnused(self);
    return 0;
    }

static uint32 RemainingBpsOfMaxBandwidth(ThaEthPort self)
    {
    /* Default layer should not check BW constrain */
    AtUnused(self);
    return 0;
    }

static AtList ListenerList(AtEthPort self)
    {
    if (mThis(self)->listeners == NULL)
        mThis(self)->listeners = AtListCreate(0);

    return mThis(self)->listeners;
    }

static tListenerWraper* NewListenerWrapper(void* userData, const tAtEthPortPropertyListener* listener)
    {
    tListenerWraper* newWrapper = AtOsalMemAlloc(sizeof(tListenerWraper));
    if (newWrapper == NULL)
        return NULL;

    newWrapper->userData = userData;
    newWrapper->listener.SourceMacDidChange = listener->SourceMacDidChange;
    newWrapper->listener.SerdesPowerControlWillPowerDown = listener->SerdesPowerControlWillPowerDown;
    return newWrapper;
    }

static tListenerWraper* ListenerWrapperSearch(AtEthPort self, void* userData, const tAtEthPortPropertyListener* listener)
    {
    AtIterator iterator;
    tListenerWraper* wrapper = NULL;

    if (mThis(self)->listeners == NULL)
        return NULL;

    iterator = AtListIteratorCreate(mThis(self)->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if ((wrapper->userData == userData) &&
            (wrapper->listener.SourceMacDidChange == listener->SourceMacDidChange) &&
            (wrapper->listener.SerdesPowerControlWillPowerDown == listener->SerdesPowerControlWillPowerDown))
            break;
        }

    AtObjectDelete((AtObject)iterator);
    return wrapper;
    }

static eAtRet SoftEnable(ThaEthPort self, eBool enable)
    {
    mThis(self)->isEnabled = enable;
    return cAtOk;
    }

static uint32 DefaultOffset(ThaEthPort port)
    {
    return (0x100 * AtChannelHwIdGet((AtChannel)port)) + ThaEthPortPartOffset(port);
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    if (AtChannelDefectGet((AtChannel)self) & cAtEthPortAlarmLinkUp)
        return cAtEthPortLinkStatusUp;
    return cAtEthPortLinkStatusDown;
    }

static eBool AutoNegIsComplete(AtEthPort self)
    {
    uint32 regAddr = cThaRegIPEthernetTripleSpdPcsOptStat + mDefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cThaIpEthTripPcsStaAnCompeteMask) ? cAtTrue : cAtFalse;
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return ThaEthPortSharedQuerier();
    }

static void OverrideAtObject(ThaEthPort self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(ThaEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethod = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethod, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllConfigSet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(ThaEthPort self)
    {
    AtEthPort port = (AtEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, AutoNegStateGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsComplete);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeSet);
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeGet);
        mMethodOverride(m_AtEthPortOverride, MinPacketSizeSet);
        mMethodOverride(m_AtEthPortOverride, MinPacketSizeGet);
        mMethodOverride(m_AtEthPortOverride, DefaultInterface);
        mMethodOverride(m_AtEthPortOverride, ProvisionedBandwidthInKbpsGet);
        mMethodOverride(m_AtEthPortOverride, RunningBandwidthInKbpsGet);
        mMethodOverride(m_AtEthPortOverride, Switch);
        mMethodOverride(m_AtEthPortOverride, SwitchedPortGet);
        mMethodOverride(m_AtEthPortOverride, SwitchRelease);
        mMethodOverride(m_AtEthPortOverride, Bridge);
        mMethodOverride(m_AtEthPortOverride, BridgedPortGet);
        mMethodOverride(m_AtEthPortOverride, BridgeRelease);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressGet);
        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        mMethodOverride(m_AtEthPortOverride, HasSourceMac);
        }

    mMethodsSet(port, &m_AtEthPortOverride);
    }

static void Override(ThaEthPort self)
    {
    OverrideAtEthPort(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaEthPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, FcsByPassed);
        mMethodOverride(m_methods, LoopInEnable);
        mMethodOverride(m_methods, LoopOutEnable);
        mMethodOverride(m_methods, LoopInIsEnabled);
        mMethodOverride(m_methods, LoopOutIsEnabled);
        mMethodOverride(m_methods, HwSwitch);
        mMethodOverride(m_methods, HwSwitchRelease);
        mMethodOverride(m_methods, HwBridgeRelease);
        mMethodOverride(m_methods, HwBridge);
        mMethodOverride(m_methods, MacActivate);
        mMethodOverride(m_methods, PcsReset);
        mMethodOverride(m_methods, PllIsLocked);
        mMethodOverride(m_methods, HwAutoControlPhysicalEnable);
        mMethodOverride(m_methods, OutComingBytes);
        mMethodOverride(m_methods, InComBytes);
        mMethodOverride(m_methods, OutComingPackets);
        mMethodOverride(m_methods, InComPackets);
        mMethodOverride(m_methods, InComErrorFcs);
        mMethodOverride(m_methods, OutPacketLensmaller64bytes);
        mMethodOverride(m_methods, OutPacketLenfrom65to127bytes);
        mMethodOverride(m_methods, OutPacketLenfrom128to255bytes);
        mMethodOverride(m_methods, OutPacketLenfrom256to511bytes);
        mMethodOverride(m_methods, OutPacketLenfrom512to1024bytes);
        mMethodOverride(m_methods, OutPacketLenfrom1025to1528bytes);
        mMethodOverride(m_methods, OutPacketLenfrom1529to2047bytes);
        mMethodOverride(m_methods, OutPacketJumboLength);
        mMethodOverride(m_methods, InPacketLensmaller64bytes);
        mMethodOverride(m_methods, InPacketLenfrom65to127bytes);
        mMethodOverride(m_methods, InPacketLenfrom128to255bytes);
        mMethodOverride(m_methods, InPacketLenfrom256to511bytes);
        mMethodOverride(m_methods, InPacketLenfrom512to1024bytes);
        mMethodOverride(m_methods, InPacketLenfrom1025to1528bytes);
        mMethodOverride(m_methods, InPacketLenfrom1529to2047bytes);
        mMethodOverride(m_methods, InPacketJumboLength);
        mMethodOverride(m_methods, InDiscardedPackets);
        mMethodOverride(m_methods, CounterLocalAddress);
        mMethodOverride(m_methods, MaxBandwidthInKbps);
        mMethodOverride(m_methods, RemainingBpsOfMaxBandwidth);
        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, ShowCurrentBandwidth);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthPort);
    }

AtEthPort ThaEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Override */
    Override(mThis(self));

    /* Initialize implementation */
    MethodsInit(mThis(self));

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtEthPort ThaEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ThaEthPortObjectInit(newPort, portId, module);
    }

uint32 ThaEthPortPartOffset(ThaEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset((ThaDevice)device, cAtModuleEth, ThaModuleEthPartOfPort(EthModule(self), (AtEthPort)self));
    }

uint32 ThaEthPortDefaultOffset(ThaEthPort port)
    {
    if (port)
        return mMethodsGet(port)->DefaultOffset(port);
    return 0;
    }

uint32 ThaEthPortMacBaseAddress(ThaEthPort self)
    {
    return ThaModuleEthMacBaseAddress(EthModule(self), (AtEthPort)self);
    }

eAtRet ThaEthPortMacActivate(ThaEthPort self, eBool activate)
    {
    if (self)
        return mMethodsGet(self)->MacActivate(self, activate);
    return cAtError;
    }

eAtRet ThaEthPortProvisionedBandwidthAdjust(ThaEthPort self, uint64 removeBwInBps, uint64 addBwInBps)
    {
    eBool isIncrease;
    tThaEthPortBandwidth offset;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (removeBwInBps == addBwInBps)
        return cAtOk;

    isIncrease = AdjustmentCalculate(removeBwInBps, addBwInBps, &offset);
    if (isIncrease)
        BandwidthPlus(self->provisionedBandWidth, &offset, &self->provisionedBandWidth);

    else
        BandwidthMinus(self->provisionedBandWidth, &offset, &self->provisionedBandWidth);

    return cAtOk;
    }

eBool ThaEthPortProvisionBandwidthCanAdjust(ThaEthPort self, uint64 removeBwInBps, uint64 addBwInBps)
    {
    eBool isIncrease;
    tThaEthPortBandwidth offset;

    if (self == NULL)
        return cAtFalse;

    if (removeBwInBps == addBwInBps)
        return cAtTrue;

    isIncrease = AdjustmentCalculate(removeBwInBps, addBwInBps, &offset);
    if (isIncrease)
        return ProvisionedBandwidthInBpsCanAdd(self, &offset);

    return ProvisionedBandwidthInBpsCanRemove(self, &offset);
    }

eAtRet ThaEthPortRunningBandwidthAdjust(ThaEthPort self, uint64 removeBwInBps, uint64 addBwInBps)
    {
    eBool isIncrease;
    tThaEthPortBandwidth offset;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (removeBwInBps == addBwInBps)
        return cAtOk;

    isIncrease = AdjustmentCalculate(removeBwInBps, addBwInBps, &offset);
    if (isIncrease)
        BandwidthPlus(self->runningBandWidth, &offset, &self->runningBandWidth);

    else
        BandwidthMinus(self->runningBandWidth, &offset, &self->runningBandWidth);

    return cAtOk;
    }

uint64 ThaEthPortPwRemainingBandwidthInBpsGet(ThaEthPort self, uint64 removeBwInBps)
    {
    tThaEthPortBandwidth bwAfterRemove;
    tThaEthPortBandwidth bwRemoving;
    tThaEthPortBandwidth maxBandwidth;
    tThaEthPortBandwidth remainingBandwidth;
    
    if (self == NULL)
        return 0;

    bwRemoving.bandWidthInKbps = (uint32)(removeBwInBps / 1000);
    bwRemoving.bandWidthInBps  = (uint32)(removeBwInBps % 1000);

    /* Total provisioned bandwidth without pw bandwidth */
    BandwidthMinus(self->provisionedBandWidth, &bwRemoving, &bwAfterRemove);

    maxBandwidth.bandWidthInKbps = mMethodsGet(self)->MaxBandwidthInKbps(self);
    maxBandwidth.bandWidthInBps = mMethodsGet(self)->RemainingBpsOfMaxBandwidth(self);

    /* Remaining bandwidth */
    BandwidthMinus(maxBandwidth, &bwAfterRemove, &remainingBandwidth);

    return (uint64)((uint64)remainingBandwidth.bandWidthInKbps * 1000 + remainingBandwidth.bandWidthInBps);
    }

eAtRet ThaEthPortPropertyListenerAdd(AtEthPort self, const tAtEthPortPropertyListener* listener, void* userData)
    {
    /* Identical listener is existing */
    if (ListenerWrapperSearch(self, userData, listener))
        return cAtOk;

    return AtListObjectAdd(ListenerList(self), (AtObject)NewListenerWrapper(userData, listener));
    }

eAtRet ThaEthPortPropertyListenerRemove(AtEthPort self, const tAtEthPortPropertyListener* listener, void* userData)
    {
    eAtRet ret;
    tListenerWraper* wrapper = ListenerWrapperSearch(self, userData, listener);
    if (wrapper == NULL)
        return cAtOk;

    ret = AtListObjectRemove(mThis(self)->listeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);
    return ret;
    }

eAtRet ThaEthPortSoftEnable(ThaEthPort self, eBool enable)
    {
	if (self)
	    return SoftEnable(self, enable);

	return cAtErrorNullPointer;
    }

uint32 ThaEthPortCounterLocalAddress(ThaEthPort self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterLocalAddress(self, counterType);

    return cInvalidUint32;
    }

void ThaEthPortShowCurrentBandwidth(ThaEthPort self)
    {
    if (self)
        mMethodsGet(self)->ShowCurrentBandwidth(self);
    }

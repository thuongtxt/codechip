/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthPort10Gb.c
 *
 * Created Date: Feb 18, 2014
 *
 * Description : 10G ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleEthInternal.h"
#include "ThaEthPortInternal.h"
#include "ThaEthPort10GbReg.h"
#include "../cla/ThaModuleCla.h"
#include "../cos/ThaModuleCos.h"
#include "../man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self) ThaEthPortMacBaseAddress((ThaEthPort)self)
#define mPortId(self) (uint8)AtChannelIdGet((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEthPort10GbMethods m_methods;

/* Override */
static tThaEthPortMethods        m_ThaEthPortOverride;
static tAtEthPortMethods         m_AtEthPortOverride;
static tAtChannelMethods         m_AtChannelOverride;

/* Save super implementation */
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods  = NULL;
static const tAtChannelMethods  *m_AtChannelMethod   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(ThaEthPort self, eBool r2c)
    {
    return (0x40 * AtChannelIdGet((AtChannel)self)) + mDefaultOffset(self) + (r2c ? 0 : 1);
    }

static ThaModuleEth EthModule(ThaEthPort self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eAtRet LoopInEnable(ThaEthPort self, eBool enable)
    {
    uint8 portId   = mPortId(self);
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    mFieldIns(&regVal,
              cThaIpEthTripSmacLoopinPortMask(portId),
              cThaIpEthTripSmacLoopinPortShift(portId),
              mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    uint8 portId   = mPortId(self);
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal,
              cThaIpEthTripSmacLoopoutPortMask(portId),
              cThaIpEthTripSmacLoopoutPortShift(portId),
              mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool LoopInIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint8 portId   = mPortId(self);

    if (regVal & cThaIpEthTripSmacLoopinPortMask(portId))
        return cAtTrue;

    return cAtFalse;
    }

static eBool LoopOutIsEnabled(ThaEthPort self)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpeedMacActiveCtrl + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint8 portId   = mPortId(self);
    if (regVal & cThaIpEthTripSmacLoopoutPortMask(portId))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HwSwitch(ThaEthPort self, ThaEthPort toPort)
    {
    uint8 selectedPortId = mPortId(toPort);
    uint32 regAddr       = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    uint32 regValue      = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint8 swValue = (selectedPortId == 0) ? 0x0 : 0x3;

    mFieldIns(&regValue, cThaIpEth10GTxSwicthPortMask, cThaIpEth10GTxSwicthPortShift, swValue);
    mFieldIns(&regValue, cThaIpEth10GRxSwicthPortMask, cThaIpEth10GRxSwicthPortShift, swValue);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtRet HwSwitchRelease(ThaEthPort self)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset((ThaEthPort)self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    mFieldIns(&regValue, cThaIpEth10GTxSwicthPortMask, cThaIpEth10GTxSwicthPortShift, 0x2);
    mFieldIns(&regValue, cThaIpEth10GRxSwicthPortMask, cThaIpEth10GRxSwicthPortShift, 0x2);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    uint32 regAddr, regValue;

    if (!AtEthPortTxIpgIsInRange(self, txIpg))
        return cAtErrorOutOfRangParm;

    regAddr  = cThaRegIpEthernet10GbSpeedMacTransmitCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GSmacTxIpg, txIpg);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbSpeedMacTransmitCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (uint8)mRegField(regValue, cThaIpEth10GSmacTxIpg);
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    uint32 regAddr, regValue;

    if (!AtEthPortRxIpgIsInRange(self, rxIpg))
        return cAtErrorOutOfRangParm;

    regAddr  = cThaRegIpEthernet10GbSpeedMacReceiverCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GSmacRxIpg, rxIpg);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbSpeedMacReceiverCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (uint8)mRegField(regValue, cThaIpEth10GSmacRxIpg);
    }

static eBool InterfaceIsSupported(eAtEthPortInterface interface)
    {
    if ((interface == cAtEthPortInterfaceXGMii) ||
        (interface == cAtEthPortInterfaceXAUI))
        return cAtTrue;
    return cAtFalse;
    }

static uint8 InterfaceSw2Hw(eAtEthPortInterface interface)
    {
    if (interface == cAtEthPortInterfaceXAUI) return 1;
    if (interface == cAtEthPortInterfaceXGMii) return 0;

    return 0xFF;
    }

static eAtEthPortInterface InterfaceHw2Sw(uint32 intfVal)
    {
    if (intfVal == 0) return cAtEthPortInterfaceXGMii;
    if (intfVal == 1) return cAtEthPortInterfaceXAUI;

    return cAtEthPortInterfaceUnknown;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    uint32 regAddr, regValue;

    if (!InterfaceIsSupported(interface))
        return cAtErrorModeNotSupport;

    regAddr  = cThaRegIpEthernet10GbTripSpdGlobalModeCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GInterface, InterfaceSw2Hw(interface));
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripSpdGlobalModeCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    return InterfaceHw2Sw(mRegField(regValue, cThaIpEth10GInterface));
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
	AtUnused(self);
    if (speed == cAtEthPortSpeed10G)
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    uint32 regAddr, regValue;

    if (!AtEthPortSpeedIsSupported(self, speed))
        return cAtErrorModeNotSupport;

    regAddr  = cThaRegIpEthernet10GbTripSpdGlobalModeCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GSpeed, 0x3);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
	AtUnused(self);
    return cAtEthPortSpeed10G;
    }

static eAtModuleEthRet DicEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regValue, cThaIpEth10GHigigEnable, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eBool DicIsEnabled(AtEthPort self)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mBinToBool(mRegField(regValue, cThaIpEth10GHigigEnable));
    }

static eAtModuleEthRet Preamble1Set(AtEthPort self, uint8 value, uint8 startOfPre)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdPreambleVal1Ctrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GDatPreValue, value);
    mRegFieldSet(regValue, cThaIpEth10GStartPreValue, startOfPre);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet Preamble2Set(AtEthPort self, uint8 value, uint8 startOfPre)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdPreambleVal2Ctrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GStartPreValue, value);
    mRegFieldSet(regValue, cThaIpEth10GEndFrameValue, startOfPre);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet Preamble3Set(AtEthPort self, uint8 errorCode, uint8 idleCode)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdPreambleVal3Ctrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regValue, cThaIpEth10GErrCodeValue, errorCode);
    mRegFieldSet(regValue, cThaIpEth10GIdleCodeValue, idleCode);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet PreambleDefault(AtEthPort self, eBool enableHigig)
    {
	eAtModuleEthRet ret = cAtOk;
	ThaModuleEth module = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);

    /* If Module is not allow to configure Preamble_ETH by Software */
	if (!mMethodsGet(module)->Port10GbPreambleCanBeConfigured(module))
        return cAtOk;

	ret |= Preamble1Set(self, enableHigig ? 0x0 : 0x55, 0xFB);
    ret |= Preamble2Set(self, enableHigig ? 0x0 : 0xD5, 0xFD);
    ret |= Preamble3Set(self, 0xFE, 0x7);

    return ret;
    }

static eAtModuleEthRet HiGigEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regValue, cThaIpEth10GHigigEnable, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);

    return PreambleDefault(self, enable);
    }

static eBool HiGigIsEnabled(AtEthPort self)
    {
    uint32 regAddr = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mBinToBool(mRegField(regValue, cThaIpEth10GHigigEnable));
    }

static void FcsRemoved(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;
    uint32 regAddr  = cThaRegIpEthernet10GbSpeedMacReceiverCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);
    eBool fcsByPassed = mMethodsGet(port)->FcsByPassed(port);

    mRegFieldSet(regValue, cThaIpEth10GSmacRxFcsPass, mBoolToBin(fcsByPassed));
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);
    }

static void SwitchFeatureEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regValue, cThaIpEth10GSwicthDisable, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regValue, cAtModuleEth);
    }

static eBool SwitchFeatureIsEnabled(AtEthPort self)
    {
    uint32 regAddr  = cThaRegIpEthernet10GbTripleSpdGlbIntfCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return (regValue & cThaIpEth10GSwicthDisableMask) ? cAtFalse : cAtTrue;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtEthPort port = (AtEthPort)self;

    /* Default configure */
    ret |= AtEthPortInterfaceSet(port, cAtEthPortInterfaceXGMii);
    ret |= AtEthPortSpeedSet(port, cAtEthPortSpeed10G);
    ret |= AtEthPortDuplexModeSet(port, cAtEthPortWorkingModeFullDuplex);
    ret |= AtEthPortHiGigEnable(port, cAtTrue);

    /* As hardware recommend, In mode Higig RX IPG = 4, TX IPG = 8 */
    if (AtEthPortHiGigIsEnabled(port))
        {
        ret |= AtEthPortTxIpgSet(port, 8);
        ret |= AtEthPortRxIpgSet(port, 4);
        }

    /* Disable flow control */
    ret |= AtEthPortFlowControlEnable(port, cAtFalse);

    /* Set default flow control follow recommended of hardware */
    ret |= AtEthPortPauseFrameIntervalSet(port, 0x3FF);
    ret |= AtEthFlowControlPauseFrameQuantaSet(AtEthPortFlowControlGet(port), 0x7FF);

    /* Disable remove FCS at Ethernet, this feature do by module CLA */
    FcsRemoved(port);

    SwitchFeatureEnable(port, cAtTrue);
    AtSerdesControllerInit(AtEthPortSerdesController(port));

    return ret;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regVal  = mChannelHwRead(self, cThaRegIpEthernet10GbTripleSpeedLinkStatus + mDefaultOffset(self), cAtModuleEth);
    uint8 portId   = mPortId(self);

    if (regVal & cThaIpEth10GLinkStateMask(portId))
        return cAtEthPortAlarmLinkUp;

    return cAtEthPortAlarmLinkDown;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
	AtUnused(self);
    if (duplexMode != cAtEthPortWorkingModeFullDuplex)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
	AtUnused(self);
    return cAtEthPortWorkingModeFullDuplex;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
	AtUnused(self);
    if (enable)
        return cAtErrorModeNotSupport;

    return cAtOk;
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaModuleCos CosModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModuleCos)AtDeviceModuleGet(device, cThaModuleCos);
    }

static ThaModuleCla ClaModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static uint32 DebugCounterReadToClear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    ThaModuleCos cosModule = CosModule(self);
    ThaModuleCla claModule = ClaModule(self);
    AtEthPort ethPort = (AtEthPort)self;

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets:
            return ThaModuleCosEthPortOutcomingPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxBytes:
            return ThaModuleCosEthPortOutcomingbyteRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxOamPackets:
            return ThaModuleCosEthPortOamTxPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPeriodOamPackets:
            return ThaModuleCosEthPortOamFrequencyTxPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPwPackets:
            return ThaModuleCosEthPortPWTxPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen0_64:
            return ThaModuleCosEthPortPktLensmallerthan64bytesRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen65_127:
            return ThaModuleCosEthPortPktLenfrom65to127bytesRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen128_255:
            return ThaModuleCosEthPortPktLenfrom128to255bytesRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen256_511:
            return ThaModuleCosEthPortPktLenfrom256to511bytesRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen512_1024:
            return ThaModuleCosEthPortPktLenfrom512to1024bytesRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen1025_1528:
            return ThaModuleCosEthPortPktLenfrom1025to1528bytesRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsLen1529_2047:
            return ThaModuleCosEthPortPktLenfrom1529to2047bytesRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxPacketsJumbo:
            return ThaModuleCosEthPortPktJumboLenRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxTopPackets:
            return ThaModuleCosEthPortPktTimePktToPRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxBrdCastPackets:
            return ThaModuleCosEthPortPktBcastPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxMultCastPackets:
            return ThaModuleCosEthPortPktMcastPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterTxUniCastPackets:
            return ThaModuleCosEthPortPktUnicastPktRead2Clear(cosModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPackets:
            return ThaModuleClaIncomPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxBytes:
            return ThaModuleClaIncombyteRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrEthHdrPackets:
            return ThaModuleClaRxHdrErrPacketsRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrBusPackets:
            return ThaModuleClaEthPktBusErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrFcsPackets:
            return ThaModuleClaEthPktFcsErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxOversizePackets:
            return ThaModuleClaEthPktoversizeRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxUndersizePackets:
            return ThaModuleClaEthPktundersizeRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen0_64:
            return ThaModuleClaEthPktLensmallerthan64bytesRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen65_127:
            return ThaModuleClaEthPktLenfrom65to127bytesRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen128_255:
            return ThaModuleClaEthPktLenfrom128to255bytesRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen256_511:
            return ThaModuleClaEthPktLenfrom256to511bytesRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen512_1024:
            return ThaModuleClaEthPktLenfrom512to1024bytesRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen1025_1528:
            return ThaModuleClaEthPktLenfrom1025to1528bytesRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsLen1529_2047:
            return ThaModuleClaEthPktLenfrom1529to2047bytesRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsJumbo:
            return ThaModuleClaEthPktJumboLenRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPwUnsupportedPackets:
            return ThaModuleClaPWunSuppedRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrPwLabelPackets:
            return ThaModuleClaHCBElookupnotmatchPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxDiscardedPackets:
            return ThaModuleClaEthPktDscdRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrPausePackets:
            return ThaModuleClaPauFrmErrPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxBrdCastPackets:
            return ThaModuleClaBcastRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMultCastPackets:
            return ThaModuleClaMcastRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxUniCastPackets:
            return ThaModuleClaUnicastRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxArpPackets:
            return ThaModuleClaARPRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxOamPackets:
            return ThaModuleClaEthOamRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxEfmOamPackets:
            return ThaModuleClaEthOamRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrEfmOamPackets:
            return ThaModuleClaEthOamRxErrPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxEthOamType1Packets:
            return ThaModuleClaEthOamtype0RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxEthOamType2Packets:
            return ThaModuleClaEthOamtype1RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIpv4Packets:
            return ThaModuleClaIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrIpv4Packets:
            return ThaModuleClaIPv4PktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIcmpIpv4Packets:
            return ThaModuleClaICMPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIpv6Packets:
            return ThaModuleClaIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrIpv6Packets:
            return ThaModuleClaIPv6PktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxIcmpIpv6Packets:
            return ThaModuleClaICMPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMefPackets:
            return ThaModuleClaMEFRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrMefPackets:
            return ThaModuleClaMEFErrRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsPackets:
            return ThaModuleClaMPLSRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrMplsPackets:
            return ThaModuleClaMPLSErrRxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsErrOuterLblPackets:
            return ThaModuleClaMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsIpv4Packets:
            return ThaModuleClaMPLSoverIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxMplsIpv6Packets:
            return ThaModuleClaMPLSoverIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrL2tpv3Packets:
            return ThaModuleClaL2TPPktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrUdpPackets:
            return ThaModuleClaUDPPktErrRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxUdpIpv4Packets:
            return ThaModuleClaUDPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxUdpIpv6Packets:
            return ThaModuleClaUDPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxErrPsnPackets:
            return ThaModuleClaRxErrPsnPacketsRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsSendToCpu:
            return ThaModuleClaEthPktForwardtoCpuRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketsSendToPw:
            return ThaModuleClaEthPktForwardtoPDaRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxTopPackets:
            return ThaModuleClaEthPktTimePktToPRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxPacketDaMis:
            return 0;

        case cAtEthPortCounterRxL2tpv3Ipv4Packets:
            return ThaModuleClaL2TPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear);
        case cAtEthPortCounterRxL2tpv3Ipv6Packets:
            return ThaModuleClaL2TPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxMplsDataPackets:
            return ThaModuleClaMPLSDataRxPktwithPHPMdRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaMPLSDataRxPktwithoutPHPMdRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxLdpIpv4Packets:
            return ThaModuleClaLDPIPv4withPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxLdpIpv6Packets:
            return ThaModuleClaLDPIPv6withPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxL2tpv3Packets:
            return ThaModuleClaL2TPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaL2TPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxUdpPackets:
            return ThaModuleClaUDPIPv4RxPktRead2Clear(claModule, ethPort, read2Clear) +
                   ThaModuleClaUDPIPv6RxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterTxPausePackets:
            if (read2Clear)
                return AtEthFlowControlTxPauseFrameCounterClear(AtEthPortFlowControlGet(ethPort));
            else
                return AtEthFlowControlTxPauseFrameCounterGet(AtEthPortFlowControlGet(ethPort));

        case cAtEthPortCounterRxPausePackets:
            if (AtEthPortFlowControlGet(ethPort))
                {
                if (read2Clear)
                    return AtEthFlowControlRxPauseFrameCounterClear(AtEthPortFlowControlGet(ethPort));
                else
                    return AtEthFlowControlRxPauseFrameCounterGet(AtEthPortFlowControlGet(ethPort));
                }

            return ThaModuleClaPauFrmRxPktRead2Clear(claModule, ethPort, read2Clear);

        case cAtEthPortCounterRxPhysicalError:
            return ThaModuleClaPhysicalErrorRxPktRead2Clear(claModule, ethPort, read2Clear);

        default:
            return 0;
        }
    }

static void CounterPrint(AtChannel self, const char *counterTypeString, uint8 counterTypes, uint8 counterTypeIsError)
    {
    eAtSevLevel counterColor;
    uint32 counterValue;

    if (!ThaEthPort10GbDebugCounterIsSupported((ThaEthPort10Gb)self, counterTypes))
        return;

    counterValue = DebugCounterReadToClear(self, counterTypes, cAtTrue);
    if (counterTypeIsError)
        counterColor = (counterValue > 0) ? cSevCritical : cSevNormal;
    else
        counterColor = (counterValue > 0) ? cSevInfo : cSevNormal;

    AtPrintc(cSevNormal, "  %-30s:   ", counterTypeString);
    AtPrintc(counterColor, "%u\r\n", counterValue);
    }

static eAtRet ShowDebugCounters(AtChannel self)
    {
    static const uint8 cCounterError = 1;
    static const uint8 cCounterNeutral = 0;

    /* Print PPP OAM counters  */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "* Debug counters:\r\n");
    CounterPrint(self, "txPackets",                cAtEthPortCounterTxPackets,               cCounterNeutral);
    CounterPrint(self, "txBytes",                  cAtEthPortCounterTxBytes,                 cCounterNeutral);
    CounterPrint(self, "txOamPackets",             cAtEthPortCounterTxOamPackets,            cCounterNeutral);
    CounterPrint(self, "txPeriodOamPackets",       cAtEthPortCounterTxPeriodOamPackets,      cCounterNeutral);
    CounterPrint(self, "txPwPackets",              cAtEthPortCounterTxPwPackets,             cCounterNeutral);
    CounterPrint(self, "txPacketsLen0_64",         cAtEthPortCounterTxPacketsLen0_64,        cCounterNeutral);
    CounterPrint(self, "txPacketsLen65_127",       cAtEthPortCounterTxPacketsLen65_127,      cCounterNeutral);
    CounterPrint(self, "txPacketsLen128_255",      cAtEthPortCounterTxPacketsLen128_255,     cCounterNeutral);
    CounterPrint(self, "txPacketsLen256_511",      cAtEthPortCounterTxPacketsLen256_511,     cCounterNeutral);
    CounterPrint(self, "txPacketsLen512_1024",     cAtEthPortCounterTxPacketsLen512_1024,    cCounterNeutral);
    CounterPrint(self, "txPacketsLen1025_1528",    cAtEthPortCounterTxPacketsLen1025_1528,   cCounterNeutral);
    CounterPrint(self, "txPacketsLen1529_2047",    cAtEthPortCounterTxPacketsLen1529_2047,   cCounterNeutral);
    CounterPrint(self, "txPacketsJumbo",           cAtEthPortCounterTxPacketsJumbo,          cCounterNeutral);
    CounterPrint(self, "txTopPackets",             cAtEthPortCounterTxTopPackets,            cCounterNeutral);
    CounterPrint(self, "txBrdCastPackets",         cAtEthPortCounterTxBrdCastPackets,        cCounterNeutral);
    CounterPrint(self, "txMultCastPackets",        cAtEthPortCounterTxMultCastPackets,       cCounterNeutral);
    CounterPrint(self, "txUniCastPackets",         cAtEthPortCounterTxUniCastPackets,        cCounterNeutral);
    CounterPrint(self, "rxPackets",                cAtEthPortCounterRxPackets,               cCounterNeutral);
    CounterPrint(self, "rxBytes",                  cAtEthPortCounterRxBytes,                 cCounterNeutral);
    CounterPrint(self, "rxErrEthHdrPackets",       cAtEthPortCounterRxErrEthHdrPackets,      cCounterError);
    CounterPrint(self, "rxErrBusPackets",          cAtEthPortCounterRxErrBusPackets,         cCounterError);
    CounterPrint(self, "rxErrFcsPackets",          cAtEthPortCounterRxErrFcsPackets,         cCounterError);
    CounterPrint(self, "rxOversizePackets",        cAtEthPortCounterRxOversizePackets,       cCounterError);
    CounterPrint(self, "rxUndersizePackets",       cAtEthPortCounterRxUndersizePackets,      cCounterError);
    CounterPrint(self, "rxPacketsLen0_64",         cAtEthPortCounterRxPacketsLen0_64,        cCounterNeutral);
    CounterPrint(self, "rxPacketsLen65_127",       cAtEthPortCounterRxPacketsLen65_127,      cCounterNeutral);
    CounterPrint(self, "rxPacketsLen128_255",      cAtEthPortCounterRxPacketsLen128_255,     cCounterNeutral);
    CounterPrint(self, "rxPacketsLen256_511",      cAtEthPortCounterRxPacketsLen256_511,     cCounterNeutral);
    CounterPrint(self, "rxPacketsLen512_1024",     cAtEthPortCounterRxPacketsLen512_1024,    cCounterNeutral);
    CounterPrint(self, "rxPacketsLen1025_1528",    cAtEthPortCounterRxPacketsLen1025_1528,   cCounterNeutral);
    CounterPrint(self, "rxPacketsLen1529_2947",    cAtEthPortCounterRxPacketsLen1529_2047,   cCounterNeutral);
    CounterPrint(self, "rxPacketsJumbo",           cAtEthPortCounterRxPacketsJumbo,          cCounterNeutral);
    CounterPrint(self, "rxPwUnsupportedPackets",   cAtEthPortCounterRxPwUnsupportedPackets,  cCounterError);
    CounterPrint(self, "rxErrPwLabelPackets",      cAtEthPortCounterRxErrPwLabelPackets,     cCounterError);
    CounterPrint(self, "rxDiscardedPackets",       cAtEthPortCounterRxDiscardedPackets,      cCounterError);
    CounterPrint(self, "rxPausePackets",           cAtEthPortCounterRxPausePackets,          cCounterNeutral);
    CounterPrint(self, "rxErrPausePackets",        cAtEthPortCounterRxErrPausePackets,       cCounterError);
    CounterPrint(self, "rxBrdCastPackets",         cAtEthPortCounterRxBrdCastPackets,        cCounterNeutral);
    CounterPrint(self, "rxMultCastPackets",        cAtEthPortCounterRxMultCastPackets,       cCounterNeutral);
    CounterPrint(self, "rxUniCastPackets",         cAtEthPortCounterRxUniCastPackets,        cCounterNeutral);
    CounterPrint(self, "rxArpPackets",             cAtEthPortCounterRxArpPackets,            cCounterNeutral);
    CounterPrint(self, "rxOamPackets",             cAtEthPortCounterRxOamPackets,            cCounterNeutral);
    CounterPrint(self, "rxEfmOamPackets",          cAtEthPortCounterRxEfmOamPackets,         cCounterNeutral);
    CounterPrint(self, "rxErrEfmOamPackets",       cAtEthPortCounterRxErrEfmOamPackets,      cCounterError);
    CounterPrint(self, "rxEthOamType1Packets",     cAtEthPortCounterRxEthOamType1Packets,    cCounterNeutral);
    CounterPrint(self, "rxEthOamType2Packets",     cAtEthPortCounterRxEthOamType2Packets,    cCounterNeutral);
    CounterPrint(self, "rxIpv4Packets",            cAtEthPortCounterRxIpv4Packets,           cCounterNeutral);
    CounterPrint(self, "rxErrIpv4Packets",         cAtEthPortCounterRxErrIpv4Packets,        cCounterError);
    CounterPrint(self, "rxIcmpIpv4Packets",        cAtEthPortCounterRxIcmpIpv4Packets,       cCounterNeutral);
    CounterPrint(self, "rxIpv6Packets",            cAtEthPortCounterRxIpv6Packets,           cCounterNeutral);
    CounterPrint(self, "rxErrIpv6Packets",         cAtEthPortCounterRxErrIpv6Packets,        cCounterError);
    CounterPrint(self, "rxIcmpIpv6Packets",        cAtEthPortCounterRxIcmpIpv6Packets,       cCounterNeutral);
    CounterPrint(self, "rxMefPackets",             cAtEthPortCounterRxMefPackets,            cCounterNeutral);
    CounterPrint(self, "rxErrMefPackets",          cAtEthPortCounterRxErrMefPackets,         cCounterError);
    CounterPrint(self, "rxMplsPackets",            cAtEthPortCounterRxMplsPackets,           cCounterNeutral);
    CounterPrint(self, "rxErrMplsPackets",         cAtEthPortCounterRxErrMplsPackets,        cCounterError);
    CounterPrint(self, "rxMplsErrOuterLblPackets", cAtEthPortCounterRxMplsErrOuterLblPackets,cCounterError);
    CounterPrint(self, "rxMplsDataPackets",        cAtEthPortCounterRxMplsDataPackets,       cCounterNeutral);
    CounterPrint(self, "rxLdpIpv4Packets",         cAtEthPortCounterRxLdpIpv4Packets,        cCounterNeutral);
    CounterPrint(self, "rxLdpIpv6Packets",         cAtEthPortCounterRxLdpIpv6Packets,        cCounterNeutral);
    CounterPrint(self, "rxMplsIpv4Packets",        cAtEthPortCounterRxMplsIpv4Packets,       cCounterNeutral);
    CounterPrint(self, "rxMplsIpv6Packets",        cAtEthPortCounterRxMplsIpv6Packets,       cCounterNeutral);
    CounterPrint(self, "rxErrL2tpv3Packets",       cAtEthPortCounterRxErrL2tpv3Packets,      cCounterError);
    CounterPrint(self, "rxL2tpv3Ipv4Packets",      cAtEthPortCounterRxL2tpv3Ipv4Packets,     cCounterNeutral);
    CounterPrint(self, "rxL2tpv3Ipv6Packets",      cAtEthPortCounterRxL2tpv3Ipv6Packets,     cCounterNeutral);
    CounterPrint(self, "rxL2tpv3Packets",          cAtEthPortCounterRxL2tpv3Packets,         cCounterNeutral);
    CounterPrint(self, "rxErrUdpPackets",          cAtEthPortCounterRxErrUdpPackets,         cCounterError);
    CounterPrint(self, "rxUdpIpv4Packets",         cAtEthPortCounterRxUdpIpv4Packets,        cCounterNeutral);
    CounterPrint(self, "rxUdpIpv6Packets",         cAtEthPortCounterRxUdpIpv6Packets,        cCounterNeutral);
    CounterPrint(self, "rxUdpPackets",             cAtEthPortCounterRxUdpPackets,            cCounterNeutral);
    CounterPrint(self, "rxErrPsnPackets",          cAtEthPortCounterRxErrPsnPackets,         cCounterError);
    CounterPrint(self, "rxPacketsSendToCpu",       cAtEthPortCounterRxPacketsSendToCpu,      cCounterNeutral);
    CounterPrint(self, "rxPacketsSendToPw",        cAtEthPortCounterRxPacketsSendToPw,       cCounterNeutral);
    CounterPrint(self, "rxTopPackets",             cAtEthPortCounterRxTopPackets,            cCounterNeutral);
    CounterPrint(self, "rxPacketDaMis",            cAtEthPortCounterRxPacketDaMis,           cCounterNeutral);
    CounterPrint(self, "rxPhysicalError",          cAtEthPortCounterRxPhysicalError,         cCounterNeutral);

    return cAtOk;
    }

static eAtRet Debug(AtChannel self)
    {
    /* Super */
    eAtRet ret = m_AtChannelMethod->Debug(self);
    if (ret != cAtOk)
        return ret;

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo, "* Switch/Bridge: ");
    AtPrintc(cSevNormal, "%s\r\n", SwitchFeatureIsEnabled((AtEthPort)self) ? "Enable" : "Disable");
    ret |= ShowDebugCounters(self);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eBool SupportCounters(ThaEthPort10Gb self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CountersAreSupported(ThaEthPort self)
    {
    ThaEthPort10Gb ethport10Gb = (ThaEthPort10Gb)self;
    return mMethodsGet(ethport10Gb)->SupportCounters(ethport10Gb);
    }

static uint32 OutComingBytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutComingBytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbSpeedMacTranmsitBytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GSmacTxByteCnt);
    }

static uint32 OutComingPackets(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutComingPackets(self, r2c);

    regAddr  = cThaRegIpEthernet10GbSpeedMacTranmsitPackets + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GSmacTxPktCnt);
    }

static uint32 OutPacketLensmaller64bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutPacketLensmaller64bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenTransmitsmallerthan64bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenTransmitsmallerthan64bytes);
    }

static uint32 OutPacketLenfrom65to127bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutPacketLenfrom65to127bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenTransmitfrom65to127bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenTransmitfrom65to127bytes);
    }

static uint32 OutPacketLenfrom128to255bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutPacketLenfrom128to255bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenTransmitfrom128to255bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenTransmitfrom128to255bytes);
    }

static uint32 OutPacketLenfrom256to511bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutPacketLenfrom256to511bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenTransmitfrom256to511bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenTransmitfrom256to511bytes);
    }

static uint32 OutPacketLenfrom512to1024bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutPacketLenfrom512to1024bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenTransmitfrom512to1023bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenTransmitfrom512to1023bytes);
    }

static uint32 OutPacketLenfrom1025to1528bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutPacketLenfrom1025to1528bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenTransmitfrom1024to1528bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenTransmitfrom1024to1528bytes);
    }

static uint32 OutPacketJumboLength(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->OutPacketJumboLength(self, r2c);

    regAddr  = cThaRegIpEthernet10GbTransmitPacketJumboLenbytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbTransmitPacketJumboLenbytes);
    }

static uint32 InComPackets(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InComPackets(self, r2c);

    regAddr  = cThaRegIpEthernet10GbSpeedMacReceivedPackets + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GSmacRxPktCnt);
    }

static uint32 InComBytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InComBytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbSpeedMacReceivedBytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GSmacRxByteCnt);
    }

static uint32 InComErrorFcs(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InComErrorFcs(self, r2c);

    regAddr  = cThaRegIpEthernet10GbSpeedMacReceivedFcsErrorPackets + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GSmacRxFCSErrPktCnt);
    }

static uint32 InPacketLensmaller64bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InPacketLensmaller64bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenReceivedsmallerthan64bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenReceivedsmallerthan64bytes);
    }

static uint32 InPacketLenfrom65to127bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InPacketLenfrom65to127bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenReceivedfrom65to127bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenReceivedfrom65to127bytes);
    }

static uint32 InPacketLenfrom128to255bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InPacketLenfrom128to255bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenReceivedfrom128to255bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenReceivedfrom128to255bytes);
    }

static uint32 InPacketLenfrom256to511bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InPacketLenfrom256to511bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenReceivedfrom256to511bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenReceivedfrom256to511bytes);
    }

static uint32 InPacketLenfrom512to1024bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InPacketLenfrom512to1024bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenReceivedfrom512to1023bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenReceivedfrom512to1023bytes);
    }

static uint32 InPacketLenfrom1025to1528bytes(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InPacketLenfrom1025to1528bytes(self, r2c);

    regAddr  = cThaRegIpEthernet10GbPacketLenReceivedfrom1024to1528bytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbPacketLenReceivedfrom1024to1528bytes);
    }

static uint32 InPacketJumboLength(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InPacketJumboLength(self, r2c);

    regAddr  = cThaRegIpEthernet10GbReceivedPacketJumboLenbytes + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaIpEth10GbReceivedPacketJumboLenbytes);
    }

static uint32 InDiscardedPackets(ThaEthPort self, eBool r2c)
    {
    uint32 regAddr, regValue;

    if (!CountersAreSupported(self))
        return m_ThaEthPortMethods->InDiscardedPackets(self, r2c);

    regAddr  = cThaRegIpEthernet10GbReceivedPacketDiscard + CounterOffset(self, r2c);
    regValue = mChannelHwRead(self, regAddr, cAtModuleEth);

    return mRegField(regValue, cThaRegIpEthernet10GbReceivedPacketDiscard);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if ((counterType == cAtEthPortCounterTxPackets)            ||
        (counterType == cAtEthPortCounterTxBytes)              ||
        (counterType == cAtEthPortCounterTxPacketsLen0_64)     ||
        (counterType == cAtEthPortCounterTxPacketsLen65_127)   ||
        (counterType == cAtEthPortCounterTxPacketsLen128_255)  ||
        (counterType == cAtEthPortCounterTxPacketsLen256_511)  ||
        (counterType == cAtEthPortCounterTxPacketsLen512_1024) ||
        (counterType == cAtEthPortCounterTxPacketsLen1025_1528)||
        (counterType == cAtEthPortCounterTxPacketsJumbo)       ||
        (counterType == cAtEthPortCounterRxPackets)            ||
        (counterType == cAtEthPortCounterRxBytes)              ||
        (counterType == cAtEthPortCounterRxErrFcsPackets)      ||
        (counterType == cAtEthPortCounterRxPacketsLen0_64)     ||
        (counterType == cAtEthPortCounterRxPacketsLen65_127)   ||
        (counterType == cAtEthPortCounterRxPacketsLen128_255)  ||
        (counterType == cAtEthPortCounterRxPacketsLen256_511)  ||
        (counterType == cAtEthPortCounterRxPacketsLen512_1024) ||
        (counterType == cAtEthPortCounterRxPacketsLen1025_1528)||
        (counterType == cAtEthPortCounterRxPacketsJumbo))
        return cAtTrue;

    if ((counterType == cAtEthPortCounterRxDiscardedPackets) &&
        ThaModuleEthHas10GDiscardCounter(EthModule((ThaEthPort)self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool DebugCounterIsSupported(ThaEthPort10Gb self, uint8 counterType)
    {
    eAtEthPortCounterType typeCounter = counterType;

    AtUnused(self);
    if ((typeCounter == cAtEthPortCounterTxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterTxBrdCastPackets)      ||
        (typeCounter == cAtEthPortCounterTxMultCastPackets)     ||
        (typeCounter == cAtEthPortCounterTxUniCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxPacketsLen1529_2047) ||
        (typeCounter == cAtEthPortCounterRxUniCastPackets)      ||
        (typeCounter == cAtEthPortCounterRxPhysicalError))
        return cAtFalse;

    return cAtTrue;
    }

static uint8 DefaultTxIpgGet(ThaEthPort10Gb self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 DefaultRxIpgGet(ThaEthPort10Gb self)
    {
    AtUnused(self);
    return 4;
    }

static void MethodsInit(ThaEthPort10Gb self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SupportCounters);
        mMethodOverride(m_methods, DebugCounterIsSupported);
        mMethodOverride(m_methods, DefaultTxIpgGet);
        mMethodOverride(m_methods, DefaultRxIpgGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethod = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethod, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort ethPort = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(ethPort);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, LoopInEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopInIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, LoopOutEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, HwSwitch);
        mMethodOverride(m_ThaEthPortOverride, HwSwitchRelease);
        mMethodOverride(m_ThaEthPortOverride, OutComingBytes);
        mMethodOverride(m_ThaEthPortOverride, InComBytes);
        mMethodOverride(m_ThaEthPortOverride, OutComingPackets);
        mMethodOverride(m_ThaEthPortOverride, InComPackets);
        mMethodOverride(m_ThaEthPortOverride, InComErrorFcs);
        mMethodOverride(m_ThaEthPortOverride, OutPacketLensmaller64bytes);
        mMethodOverride(m_ThaEthPortOverride, OutPacketLenfrom65to127bytes);
        mMethodOverride(m_ThaEthPortOverride, OutPacketLenfrom128to255bytes);
        mMethodOverride(m_ThaEthPortOverride, OutPacketLenfrom256to511bytes);
        mMethodOverride(m_ThaEthPortOverride, OutPacketLenfrom512to1024bytes);
        mMethodOverride(m_ThaEthPortOverride, OutPacketLenfrom1025to1528bytes);
        mMethodOverride(m_ThaEthPortOverride, OutPacketJumboLength);
        mMethodOverride(m_ThaEthPortOverride, InPacketLensmaller64bytes);
        mMethodOverride(m_ThaEthPortOverride, InPacketLenfrom65to127bytes);
        mMethodOverride(m_ThaEthPortOverride, InPacketLenfrom128to255bytes);
        mMethodOverride(m_ThaEthPortOverride, InPacketLenfrom256to511bytes);
        mMethodOverride(m_ThaEthPortOverride, InPacketLenfrom512to1024bytes);
        mMethodOverride(m_ThaEthPortOverride, InPacketLenfrom1025to1528bytes);
        mMethodOverride(m_ThaEthPortOverride, InPacketJumboLength);
        mMethodOverride(m_ThaEthPortOverride, InDiscardedPackets);
        }

    mMethodsSet(ethPort, &m_ThaEthPortOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, HiGigEnable);
        mMethodOverride(m_AtEthPortOverride, HiGigIsEnabled);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, DicEnable);
        mMethodOverride(m_AtEthPortOverride, DicIsEnabled);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthPort10Gb);
    }

AtEthPort ThaEthPort10GbObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    MethodsInit((ThaEthPort10Gb)self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort ThaEthPort10GbNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ThaEthPort10GbObjectInit(newPort, portId, module);
    }

eBool ThaEthPort10GbDebugCounterIsSupported(ThaEthPort10Gb self, uint8 counterType)
    {
    if (self)
        return mMethodsGet(self)->DebugCounterIsSupported(self, counterType);
    return cAtFalse;
    }

uint8 ThaEthPort10GbDefaultTxIpgGet(ThaEthPort10Gb self)
    {
    if (self)
        return mMethodsGet(self)->DefaultTxIpgGet(self);

    return 0;
    }

uint8 ThaEthPort10GbDefaultRxIpgGet(ThaEthPort10Gb self)
    {
    if (self)
        return mMethodsGet(self)->DefaultRxIpgGet(self);

    return 0;
    }

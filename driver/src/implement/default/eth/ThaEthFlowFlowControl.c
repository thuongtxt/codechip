/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH module
 *
 * File        : ThaEthPortFLowController.c
 *
 * Created Date: Oct 4, 2013
 *
 * Description : ETH port flow controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaEthFlowFlowControlInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEthFlowFlowControlMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ThresholdApply(ThaEthFlowFlowControl self)
	{
	AtUnused(self);
	}

static void Override(ThaEthFlowFlowControl self)
    {
	AtUnused(self);
    /* Will add in future */
    }

static void MethodsInit(ThaEthFlowFlowControl self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ThresholdApply);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEthFlowFlowControl);
    }

AtEthFlowControl ThaEthFlowFlowControlObjectInit(AtEthFlowControl self, AtModule module, AtEthFlow ethFlow)
    {
    AtOsal osal = AtSharedDriverOsalGet();
	AtUnused(module);
    /* Clear memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthFlowControlObjectInit(self, (AtChannel)ethFlow) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaEthFlowFlowControl)self);
    MethodsInit((ThaEthFlowFlowControl)self);
    m_methodsInit = 1;

    return self;
    }

void ThaEthFlowFlowControlThresholdApply(ThaEthFlowFlowControl self)
	{
	if (self)
		mMethodsGet(self)->ThresholdApply(self);
	}

eAtRet ThaEthFlowFlowControlHighThresholdSet(ThaEthFlowFlowControl self, uint32 threshold)
    {
    if (self)
        self->highThreshold = threshold;
    return cAtOk;
    }

uint32 ThaEthFlowFlowControlHighThresholdGet(ThaEthFlowFlowControl self)
    {
    return self ? self->highThreshold : 0;
    }

eAtRet ThaEthFlowFlowControlLowThresholdSet(ThaEthFlowFlowControl self, uint32 threshold)
    {
    if (self)
        self->lowThreshold = threshold;
    return cAtOk;
    }

uint32 ThaEthFlowFlowControlLowThresholdGet(ThaEthFlowFlowControl self)
    {
    return self ? self->lowThreshold : 0;
    }

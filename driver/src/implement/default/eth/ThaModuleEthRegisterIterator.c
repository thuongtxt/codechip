/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaModuleEthRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : ETH register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "AtModuleEth.h"

#include "../../../util/AtIteratorInternal.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "ThaModuleEthInternal.h"
#include "ThaEthPortReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModuleEthRegisterIterator
    {
    tAtModuleRegisterIterator super;
    }tThaModuleEthRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleEthRegisterIterator);
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtModuleRegisterIteratorModuleGet(self);
    return AtModuleEthMaxPortsGet(ethModule);
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    return m_AtModuleRegisterIteratorMethods->NextOffset(self) * 0x100;
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    mNextReg(iterator, 0, cThaRegIPEthernetTripleSpdGlbCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdGlbCtrl                   , cThaRegIPEthernetTripleSpdGlbIntfCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdGlbIntfCtrl               , cThaRegIPEthernetTripleSpdPcsCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdPcsCtrl                   , cThaRegIPEthernetTripleSpdPcsAnNextPageTxter);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdPcsAnNextPageTxter        , cThaRegIPEthernetTripleSpdPcsOptCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdPcsOptCtrl                , cThaRegIPEthernetTripleSpdPcsOptSyncTimer);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdPcsOptSyncTimer           , cThaRegIPEthernetTripleSpdPcsOptPrpsTestCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdPcsOptPrpsTestCtrl        , cThaRegIPEthernetTripleSpdSimpleMACActCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdSimpleMACActCtrl          , cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl          , cThaRegIPEthernetTripleSpdSimpleMACTxterCtrl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdSimpleMACTxterCtrl        , cThaRegIPEthernetTripleSpdSimpleMACDataInterFrm);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdSimpleMACDataInterFrm     , cThaRegIPEthernetTripleSpdSimpleMACflowCtrlTimeIntvl);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdSimpleMACflowCtrlTimeIntvl, cThaRegIPEthernetTripleSpdSimpleMACflowCtrlQuanta);
    mNextChannelRegister(iterator, cThaRegIPEthernetTripleSpdSimpleMACflowCtrlQuanta   , cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, MaxNumChannels);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleEthRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator ThaModuleEthRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return ThaModuleEthRegisterIteratorObjectInit(newIterator, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaModuleEthPw.h
 * 
 * Created Date: Jul 8, 2013
 *
 * Description : Ethernet module of PW products
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEETHPW_H_
#define _THAMODULEETHPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleEth.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEETHPW_H_ */


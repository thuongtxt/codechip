/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaEthPort.h
 *
 * Created Date: Sep 15, 2012
 *
 * Description : Default Thalassa Ethernet port implementation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPORT_H_
#define _THAETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEthPort     * ThaEthPort;
typedef struct tThaEthPort10Gb * ThaEthPort10Gb;

typedef struct tAtEthPortPropertyListener
    {
    void (*SourceMacDidChange)(AtEthPort self, uint8 *mac, void *userData);
    void (*SerdesPowerControlWillPowerDown)(AtEthPort self, void *userData);
    }tAtEthPortPropertyListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtEthPort ThaEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort ThaStmEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort ThaEthPort10GbNew(uint8 portId, AtModuleEth module);

eAtRet ThaEthPortMacActivate(ThaEthPort self, eBool activate);
uint32 ThaEthPortDefaultOffset(ThaEthPort port);
uint32 ThaEthPortPartOffset(ThaEthPort self);
uint32 ThaEthPortMacBaseAddress(ThaEthPort self);
eBool ThaEthPort10GbDebugCounterIsSupported(ThaEthPort10Gb self, uint8 counterType);
eAtRet ThaEthPortSoftEnable(ThaEthPort self, eBool enable);
uint8 ThaEthPort10GbDefaultTxIpgGet(ThaEthPort10Gb self);
uint8 ThaEthPort10GbDefaultRxIpgGet(ThaEthPort10Gb self);
uint32 ThaEthPortCounterLocalAddress(ThaEthPort self, uint16 counterType);

/* Bandwidth control */
eAtRet ThaEthPortProvisionedBandwidthAdjust(ThaEthPort self, uint64 removeBwInBps, uint64 addBwInBps);
eBool ThaEthPortProvisionBandwidthCanAdjust(ThaEthPort self, uint64 removeBwInBps, uint64 addBwInBps);
eAtRet ThaEthPortRunningBandwidthAdjust(ThaEthPort self, uint64 removeBwInBps, uint64 addBwInBps);
uint64 ThaEthPortPwRemainingBandwidthInBpsGet(ThaEthPort self, uint64 removeBwInBps);

/* Listener control */
eAtRet ThaEthPortPropertyListenerAdd(AtEthPort self, const tAtEthPortPropertyListener* listener, void* userData);
eAtRet ThaEthPortPropertyListenerRemove(AtEthPort self, const tAtEthPortPropertyListener* listener, void* userData);

/* Product concretes */
AtEthPort Tha60031021EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60091023EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60030111EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60150011EthPort10GbNew(uint8 portId, AtModuleEth module);
AtEthPort Tha61150011EthPort10GbNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60031031EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort ThaPdhPwProductEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210021EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210031EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210011EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha61210011EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha61210031EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha6A210031EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha6A000010EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha6A033111EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210051EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210051EthPortCrossPointNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60210051EthExtraPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha62290011Axi4EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha6A290011EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha6A290021EthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha6A290022EthPortNew(uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORT_H_ */


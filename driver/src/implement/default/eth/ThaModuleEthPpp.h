/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaModuleEthPpp.h
 * 
 * Created Date: Apr 4, 2015
 *
 * Description : ETH for PPP product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEETHPPP_H_
#define _THAMODULEETHPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEETHPPP_H_ */


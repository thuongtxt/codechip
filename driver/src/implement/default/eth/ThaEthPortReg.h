/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthPortReg.h
 * 
 * Created Date: Nov 21, 2012
 *
 * Description : ETH Port registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPORTREG_H_
#define _THAETHPORTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name: Mac Address Bit31_0 Control
Reg Addr: 0x000100
Reg Desc: This register configures bit 31 to 0 of MAC address.
------------------------------------------------------------------------------*/
#define cThaRegEthMacAddr31_0   0x000100

/*------------------------------------------------------------------------------
Reg Name: Mac Address Bit47_32 Control
Reg Addr: 0x000101
Reg Desc: This register configures bit 47 to 32 of MAC address.
------------------------------------------------------------------------------*/
#define cThaRegEthMacAddr47_32   0x000101

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Global Control
Reg Addr: 0x01
          The address format for these registers is 0x01 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is global Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdGlbCtrl (0x000001)

/*--------------------------------------
BitField Name: IpEthTripGlbCtlElDepth
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 10_8
--------------------------------------*/
#define cThaIpEthTripGlbCtlElDepthMask                 cBit10_8
#define cThaIpEthTripGlbCtlElDepthShift                8
#define cThaIpEthTripGlbCtlElDepthRstVal               0x2

/*--------------------------------------
BitField Name: IpEthTripGlbCtlSpStable
BitField Type: R/W
BitField Desc: Software must write 0x1
BitField Bits: 6
--------------------------------------*/
#define cThaIpEthTripGlbCtlSpStbMask                   cBit6
#define cThaIpEthTripGlbCtlSpStbShift                  6
#define cThaIpEthTripGlbCtlSpStbRstVal                 0x1

/*--------------------------------------
BitField Name: IpEthTripGlbCtlSpIsPhy
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: Enable SMAC auto update speed operation by PHY device (or
                 PCS)
               - 0: Disable
BitField Bits: 5
--------------------------------------*/
#define cThaIpEthTripGlbCtlSpIsPhyMask                 cBit5
#define cThaIpEthTripGlbCtlSpIsPhyShift                5
#define cThaIpEthTripGlbCtlSpIsPhyRstVal               0x0

/*--------------------------------------
BitField Name: IpEthTripCfgCtlSelect
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: Enable engine used con-fig interface by register IP
                 Ethernet Triple Speed Global Interface Control
               - 0: Disable
BitField Bits: 4
--------------------------------------*/
#define cThaIpEthTripCfgCtlSelectMask                  cBit4
#define cThaIpEthTripCfgCtlSelectShift                 4

/*--------------------------------------
BitField Name: IpEthTripGlbCtlReset
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 2
--------------------------------------*/
#define cThaIpEthTripGlbCtlRstMask                     cBit2
#define cThaIpEthTripGlbCtlRstShift                    2
#define cThaIpEthTripGlbCtlRstRstVal                   0x0

/*--------------------------------------
BitField Name: IpEthTripGlbCtlIntEn
BitField Type: R/W
BitField Desc: Interrupt Enable
               - 1: Enable Interrupt
               - 0: Disable Interrupt
BitField Bits: 1
--------------------------------------*/
#define cThaIpEthTripGlbCtlIntEnMask                   cBit1
#define cThaIpEthTripGlbCtlIntEnShift                  1
#define cThaIpEthTripGlbCtlIntEnRstVal                 0x1

/*--------------------------------------
BitField Name: IpEthTripGlbCtlActive
BitField Type: R/W
BitField Desc: Port Active
               - 1: Active
               - 0: No Active
BitField Bits: 0
--------------------------------------*/
#define cThaIpEthTripGlbCtlActMask                     cBit0
#define cThaIpEthTripGlbCtlActShift                    0
#define cThaIpEthTripGlbCtlActRstVal                   0x1

#define cThaRegIPEthernetTripleSpdGlbCtrlNoneEditableFieldsMask (cBit15_11 | cBit7 | cBit3)

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Global Interface Control
Reg Addr: 0x04
          The address format for these registers is 0x04 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Interface Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdGlbIntfCtrl   (0x000004)

/*--------------------------------------
BitField Name: IpEthTripGlbCfgRgmii
BitField Type: R/W
BitField Desc: RGMII interface Active
               - 0x1: RGMII Active
               - 0x0: GMII/MII Active
BitField Bits: 6
--------------------------------------*/
#define cThaIpEthTripGlbCfgRgmiiMask                   cBit6
#define cThaIpEthTripGlbCfgRgmiiShift                  6

/*--------------------------------------
BitField Name: IpEthTripGlbCfgSgmii
BitField Type: R/W
BitField Desc: SGMII interface status
               - 0x1: PCS-SGMII
               - 0x0: PCS-1000Base-X
BitField Bits: 5
--------------------------------------*/
#define cThaIpEthTripGlbCfgSgmiiMask                   cBit5
#define cThaIpEthTripGlbCfgSgmiiShift                  5

/*--------------------------------------
BitField Name: IpEthTripGlbCfgTbi
BitField Type: R/W
BitField Desc: TBI interface status
               - 0x1:  TBI interface Acitve (include PCS 1000Base-X or SGMII)
               - 0x0:  None PCS
BitField Bits: 4
--------------------------------------*/
#define cThaIpEthTripGlbCfgTbiMask                     cBit4
#define cThaIpEthTripGlbCfgTbiShift                    4
#define cThaIpEthTripGlbCfgTbiRstVal                   0x1

/*--------------------------------------
BitField Name: IpEthTripGlbCfgSpeed
BitField Type: R/W
BitField Desc: Speed operation status
               - 0x0: 10Mbps
               - 0x1: 100Mbps
               - 0x2: 1000Mbps
               - 0x3: Unused
BitField Bits: 1_0
--------------------------------------*/
#define cThaIpEthTripGlbCfgSpdMask                     cBit1_0
#define cThaIpEthTripGlbCfgSpdShift                    0


#define cThaRegIPEthernetTripleSpdGlbIntfCtrlNoneEditableFieldsMask (cBit11_10 | cBit3_2)

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Control
Reg Addr: 0x20
          The address format for these registers is 0x20 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is PCS Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsCtrl   (0x000020)

/*--------------------------------------
BitField Name: IpEthTripPcsSpeedLsb
BitField Type: R_O
BitField Desc: Speed Selection (LSB)
BitField Bits: 13
--------------------------------------*/
#define cThaIpEthTripPcsSpdLsbMask                     cBit13
#define cThaIpEthTripPcsSpdLsbShift                    13

/*--------------------------------------
BitField Name: IpEthTripPcsAnEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: Enable Auto-Negotiation process
               - 0: Disable Auto-Negotiation process
BitField Bits: 12
--------------------------------------*/
#define cThaIpEthTripPcsAnEnMask                       cBit12
#define cThaIpEthTripPcsAnEnShift                      12

/*--------------------------------------
BitField Name: IpEthTripPcsFullduplex
BitField Type: R_O
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: Full-Duplex Mode
               - 0: Haft-duplex Mode
BitField Bits: 8
--------------------------------------*/
#define cThaIpEthTripPcsFullduplexMask                 cBit8
#define cThaIpEthTripPcsFullduplexShift                8

/*--------------------------------------
BitField Name: IpEthTripPcsSpeedMsb
BitField Type: R_O
BitField Desc: , IpEthTripPcsSpeedLsb[0]}
               - 0: Speed 10Mbps
               - 1: Speed 100Mbps
               - 2: Speed 1000Mbps
               - 3: Unused
BitField Bits: 6
--------------------------------------*/
#define cThaIpEthTripPcsSpdMsbMask                     cBit6
#define cThaIpEthTripPcsSpdMsbShift                    6

#define cThaRegIPEthernetTripleSpdPcsCtrlNoneEditableFieldsMask (cBit13 | cBit8_0)

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Advertisement With 1000BASE X mode
Reg Addr: 0x24
          The address format for these registers is 0x24 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Auto-Negotiation Advertisement
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsAdvertisementWith1000BASEXMd (0x000024)

/*--------------------------------------
BitField Name: IpEthTripPcsAnAdvPause
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 0 : No PAUSE
               - 1 : Symmetric PAUSE
               - 2 : Asymmetric PAUSE towards link partner
               - 3 : Both Symmetric PAUSE and Asymmetric PAUSE supported
BitField Bits: 8_7
--------------------------------------*/
#define cThaIpEthTripPcsAnAdvPauMask                   cBit8_7
#define cThaIpEthTripPcsAnAdvPauShift                  7
#define cThaIpEthTripPcsAnAdvPauRstVal                 0x3

/*--------------------------------------
BitField Name: SGMII defined value sent from
BitField Type: the MAC to the PHY
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 15_0
--------------------------------------*/
/*
RESET_VAL_CONVERT:-ERRORS_RESET_VALUE: R_O
 */

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS An Next Page Transmitter
Reg Addr: 0x27
          The address format for these registers is 0x27 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Auto-Negotiation Next Page Transmitter
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsAnNextPageTxter (0x000027)

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpTxNextPage
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: Additional Next Page(s) will follow
               - 0: Last page
BitField Bits: 15
--------------------------------------*/
#define cThaIpEthTripPcsAnNpTxNextPageMask             cBit15
#define cThaIpEthTripPcsAnNpTxNextPageShift            15
#define cThaIpEthTripPcsAnNpTxNextPageRstVal           0x0

/*
#define cThaUnusedMask                                 cBit14
#define cThaUnusedShift                                14
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpTxMessagePage
BitField Type: R/W
BitField Desc: Message Page
               - 1: Message Page
               - 0: Unformatted Page
BitField Bits: 13
--------------------------------------*/
#define cThaIpEthTripPcsAnNpTxMsgPageMask              cBit13
#define cThaIpEthTripPcsAnNpTxMsgPageShift             13
#define cThaIpEthTripPcsAnNpTxMsgPageRstVal            0x1

/*--------------------------------------
BitField Name: IpEthTripPcsAnNpTxAck
BitField Type: R/W
BitField Desc: Acknowledge
               - 1: Comply with message
               - 0: Cannot comply with message
BitField Bits: 12
--------------------------------------*/
#define cThaIpEthTripPcsAnNpTxAckMask                  cBit12
#define cThaIpEthTripPcsAnNpTxAckShift                 12
#define cThaIpEthTripPcsAnNpTxAckRstVal                0x0

/*--------------------------------------
BitField Name: IpEthTripPcaAnNpTxMeassge
BitField Type: R_O
BitField Desc: Message Code Field or Unformatted Page, (00000000001 is Null
               Message Code)
BitField Bits: 10_0
--------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsAnNextPageTxterNoneEditableFieldsMask (cBit14 | cBit11_0)

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Option Control
Reg Addr: 0x32
          The address format for these registers is 0x32 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is PCS Option Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsOptCtrl (0x000032)

/*--------------------------------------
BitField Name: IpEthTripPcsCtlFifoAdjC2
BitField Type: R/W
BitField Desc: Enable rate match FIFO used adjust C2 code word when
               Auto-Negotiation enable
               - 0 : Disable
               - 1 : Enable
BitField Bits: 14_12
--------------------------------------*/
#define cThaIpEthTripPcsCtlFifoAdjC2Mask               cBit15
#define cThaIpEthTripPcsCtlFifoAdjC2Shift              15
#define cThaIpEthTripPcsCtlFifoAdjC2RstVal             0x1

/*--------------------------------------
BitField Name: IpEthTripPcsCtlFifoDepth
BitField Type: R/W
BitField Desc: Rate match FIFO depth, support support up to SGMII speed 10Mbps
               jumbo frame 12K bytes
               - 0 : +/- 4 words
               - 1 : +/- 8 words
               - 2 : +/- 16 words
               - 3 : +/- 32 words
               - 4 : +/- 64 words
               - 5 : +/- 128 words
               - 6 : +/- 256 words
               - 7 : Unused
BitField Bits: 14_12
--------------------------------------*/
#define cThaIpEthTripPcsCtlFifoDepthMask               cBit14_12
#define cThaIpEthTripPcsCtlFifoDepthShift              12
#define cThaIpEthTripPcsCtlFifoDepthRstVal             0x2

/*--------------------------------------
BitField Name: IpEthTripPcsIntLinkEn
BitField Type: R/W
BitField Desc: Enable interrupt Link status
               - 1 :  Enable
               - 0 :  Disable
BitField Bits: 1
--------------------------------------*/
#define cThaIpEthTripPcsIntLinkEnMask                  cBit1
#define cThaIpEthTripPcsIntLinkEnShift                 1

/*--------------------------------------
BitField Name: IpEthTripPcsIntAnEn
BitField Type: R/W
BitField Desc: Enable interrupt Auto-Negotiation
               - 1 :  Enable
               - 0 :  Disable
BitField Bits: 0
--------------------------------------*/

#define cThaRegIPEthernetTripleSpdPcsOptCtrlNoneEditableFieldsMask (cBit31_16 | cBit2)

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Option Status
Reg Addr: 0x33
          The address format for these registers is 0x33 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is PCS Option Status
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsOptStat    (0x000033)

/*--------------------------------------
BitField Name: IpEthTripPcsStaLinkFail
BitField Type: R_O
BitField Desc: Link fail status
               - 1: Link down
               - 0: Link up
BitField Bits: 3
--------------------------------------*/
#define cThaIpEthTripPcsStaLinkFailMask                cBit3
#define cThaIpEthTripPcsStaLinkFailShift               3

/*--------------------------------------
BitField Name: IpEthTripPcsStaAnCompete
BitField Type: R_O
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 2
--------------------------------------*/
#define cThaIpEthTripPcsStaAnCompeteMask               cBit2
#define cThaIpEthTripPcsStaAnCompeteShift              2

/*--------------------------------------
BitField Name: IpEthTripPcsIntLinkReq
BitField Type: R/W/C
BitField Desc: Link interrupt request
               - 1: Interrupt request
               - 0: No Interrupt request
BitField Bits: 1
--------------------------------------*/
#define cThaIpEthTripPcsIntLinkReqMask                 cBit1
#define cThaIpEthTripPcsIntLinkReqShift                1

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Option FSM Status 0
Reg Addr: 0x34
          The address format for these registers is 0x34 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is PCS FSM Current Status 0, used to HW debug
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsOptFSMStat0    (0x000034)

/*--------------------------------------
BitField Name: IpEthTripPcsCurAnFsm
BitField Type: R_O
BitField Desc: Auto-Negotiation Current FSM
               - 0x0 : AN_ENABLE
               - 0x1 : AN_RESTART
               - 0x2 : AN_DIS_LINK_OK
               - 0x3 : ABILITY_DET
               - 0x4 : ACK_DET
               - 0x5 : NEXT_PAGE_WAIT
               - 0x6 : COMPLETE_ACK
               - 0x7 : DLE_DET
               - 0x8 : LINK_OK
BitField Bits: 7_4
--------------------------------------*/
#define cThaIpEthTripPcsCurAnFsmMask                   cBit7_4
#define cThaIpEthTripPcsCurAnFsmShift                  4

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Option Link Timer
Reg Addr: 0x38
          The address format for these registers is 0x38 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is PCS Link Timer, unit = 65535 ns ~ 65us
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsOptLinkTimer (0x000038)

/*--------------------------------------
BitField Name: IpEthTripPcsCfgLinkTimerSgmii
BitField Type: R/W
BitField Desc: Auto-Negotiation cycle by the Link timer with SGMII standard.
               Default (0x19h * 65us ~ 1.6ms)
BitField Bits: 15_8
--------------------------------------*/
#define cThaIpEthTripPcsCfgLinkTimerSgmiiMask          cBit15_8
#define cThaIpEthTripPcsCfgLinkTimerSgmiiShift         8
#define cThaIpEthTripPcsCfgLinkTimerSgmiiRstVal        0x19

/*--------------------------------------
BitField Name: IpEthTripPcsCfgLinkTimerBasex
BitField Type: R/W
BitField Desc: Auto-Negotiation cycle by the Link timer with 1000 BASE-XI
               standard, Default  (0x99h * 65us ~ 10ms)
BitField Bits: 7_0
--------------------------------------*/
#define cThaIpEthTripPcsCfgLinkTimerBasexMask          cBit7_0
#define cThaIpEthTripPcsCfgLinkTimerBasexShift         0
#define cThaIpEthTripPcsCfgLinkTimerBasexRstVal        0x99

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Option Sync Timer
Reg Addr: 0x39
          The address format for these registers is 0x39 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is PCS Sync Timer, unit = 65535 ns ~ 65us
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsOptSyncTimer (0x000039)

/*--------------------------------------
BitField Name: IpEthTripPcsCfgSyncTimer
BitField Type: R/W
BitField Desc: standard. Default (0x7Fh * 65us ~ 8.25ms)
BitField Bits: 7_0
--------------------------------------*/
#define cThaIpEthTripPcsCfgSyncTimerMask               cBit7_0
#define cThaIpEthTripPcsCfgSyncTimerShift              0
#define cThaIpEthTripPcsCfgSyncTimerRstVal             0x7F


#define cThaRegIPEthernetTripleSpdPcsOptSyncTimerNoneEditableFieldsMask cBit31_9

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed PCS Option PRBS Test Control
Reg Addr: 0x3C
          The address format for these registers is 0x3C + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is PRBS test status
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdPcsOptPrpsTestCtrl (0x00003C)

/*--------------------------------------
BitField Name: IPEthTripPcsTestFixDat
BitField Type: R/W
BitField Desc: Fix Data generation
BitField Bits: 13_4
--------------------------------------*/
#define cThaIPEthTripPcsTestFixDatMask                 cBit13_4
#define cThaIPEthTripPcsTestFixDatShift                4
#define cThaIPEthTripPcsTestFixDatRstVal               0x2DA

/*--------------------------------------
BitField Name: IPEthTripPcsTestMode
BitField Type: R/W
BitField Desc: Test mode
BitField Bits: 3_0
--------------------------------------*/

#define cThaRegIPEthernetTripleSpdPcsOptPrpsTestCtrlNoneEditableFieldsMask cBit31_16



/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Simple MAC Active Control
Reg Addr: 0x40
          The address format for these registers is 0x40 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Active Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdSimpleMACActCtrl (0x000040)

/*
#define cThaUnusedMask                                 cBit15_6
#define cThaUnusedShift                                6
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: IpEthTripSmacLoopout
BitField Type: R/W
BitField Desc: Loop out Enable (TX ? RX)
               - 1: Enable
               - 0: Disable
BitField Bits: 5
--------------------------------------*/
#define cThaIpEthTripSmacLoopoutMask                   cBit5
#define cThaIpEthTripSmacLoopoutShift                  5

/*--------------------------------------
BitField Name: IpEthTripSmacTxActive
BitField Type: R/W
BitField Desc: Active transmitter side
               - 1: Active
               - 0: No Active
BitField Bits: 4
--------------------------------------*/
#define cThaIpEthTripSmacTxActMask                     cBit4
#define cThaIpEthTripSmacTxActShift                    4

/*--------------------------------------
BitField Name: IpEthTripSmacLoopin
BitField Type: R/W
BitField Desc: Loop In Enable (TX ? RX)
               - 1: Enable
               - 0: Disable
BitField Bits: 1
--------------------------------------*/
#define cThaIpEthTripSmacLoopinMask                    cBit1
#define cThaIpEthTripSmacLoopinShift                   1

/*--------------------------------------
BitField Name: IpEthTripSmacRxActive
BitField Type: R/W
BitField Desc: Active receiver side
               - 1: Active
               - 0: No Active
BitField Bits: 0
--------------------------------------*/
#define cThaIpEthTripSmacRxActMask                     cBit0
#define cThaIpEthTripSmacRxActShift                    0

#define cThaRegIPEthernetTripleSpdSimpleMACActCtrlNoneEditableFieldsMask (cBit31_6 | cBit3_2)

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Simple MAC Receiver Control
Reg Addr: 0x42
          The address format for these registers is 0x42 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Simple MAC Receiver Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdSimpleMACRxrCtrl  (0x000042)

/*--------------------------------------
BitField Name: IpEthTripSmacRxMask
BitField Type: R/W
BitField Desc: Mask Error forward to user
BitField Bits: 15_12
--------------------------------------*/
#define cThaIpEthTripSmacRxMaskMask                    cBit15_12
#define cThaIpEthTripSmacRxMaskShift                   12
#define cThaIpEthTripSmacRxMaskRstVal                  0xF

/*--------------------------------------
BitField Name: IpEthTripSmacRxIpg
BitField Type: R/W
BitField Desc: Number of Inter packet Gap can detected for Receiver sides
BitField Bits: 11_8
--------------------------------------*/
#define cThaIpEthTripSmacRxIpgMask                     cBit11_8
#define cThaIpEthTripSmacRxIpgShift                    8

/*--------------------------------------
BitField Name: IpEthTripSmacRxPrm
BitField Type: R/W
BitField Desc: Number of preamble bytes that receiver side can be received,
               this is maximum preamble bytes need to detect and one SFD byte
BitField Bits: 7_4
--------------------------------------*/

/*--------------------------------------
BitField Name: IpEthTripSmacRxPauSaChk
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 3
--------------------------------------*/
#define cThaIpEthTripSmacRxPauSaChkMask                cBit3
#define cThaIpEthTripSmacRxPauSaChkShift               3
#define cThaIpEthTripSmacRxPauSaChkRstVal              0x1

/*--------------------------------------
BitField Name: IpEthTripSamcRxPauDaChk
BitField Type: R/W
BitField Desc: Enable Pause  frame terminate DA check
               - 1: Enable
               - 0: Disable
BitField Bits: 2
--------------------------------------*/
#define cThaIpEthTripSamcRxPauDaChkMask                cBit2
#define cThaIpEthTripSamcRxPauDaChkShift               2
#define cThaIpEthTripSamcRxPauDaChkRstVal              0x1

/*--------------------------------------
BitField Name: IpEthTripSmacRxPauDiChk
BitField Type: R/W
BitField Desc: Disable Pause frame termination
               - 1: Disable
               - 0: Enable
BitField Bits: 1
--------------------------------------*/
#define cThaIpEthTripSmacRxPauDiChkMask                cBit1
#define cThaIpEthTripSmacRxPauDiChkShift               1
#define cThaIpEthTripSmacRxPauDiChkRstVal              0x0

/*--------------------------------------
BitField Name: IpEthTripSmacRxFcsPass
BitField Type: R/W
BitField Desc: Enable pass 4 byte FCS
               - 1: Enable
               - 0: Disable
BitField Bits: 0
--------------------------------------*/
#define cThaIpEthTripSmacRxFcsPassMask                 cBit0
#define cThaIpEthTripSmacRxFcsPassShift                0

#define cThaRegIPEthernetTripleSpdSimpleMACRxrCtrlNoneEditableFieldsMask cBit31_16

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Simple MAC Transmitter Control
Reg Addr: 0x43
          The address format for these registers is 0x43 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Simple MAC Transmitter Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdSimpleMACTxterCtrl    (0x43)

/*--------------------------------------
BitField Name: IpEthTripSmacTxIpg
BitField Type: R/W
BitField Desc: Number of Inter packet Gap generated for Transmitter sides.
               Value range of parameter is from 4 to 31
BitField Bits: 12_8
--------------------------------------*/
#define cThaIpEthTripSmacTxIpgMask                     cBit12_8
#define cThaIpEthTripSmacTxIpgShift                    8

/*--------------------------------------
BitField Name: IpEthTripSmacTxPauDis
BitField Type: R/W
BitField Desc: Enable Pause  frame generator
               - 1: Disable
               - 0: Enable
BitField Bits: 1
--------------------------------------*/
#define cThaIpEthTripSmacTxPauDisMask                  cBit1
#define cThaIpEthTripSmacTxPauDisShift                 1

/*--------------------------------------
BitField Name: IpEthTripSmacTxFcsIns
BitField Type: R/W
BitField Desc: Enable FCS Insertion
               - 1: Enable
               - 0: Disable
BitField Bits: 0
--------------------------------------*/

#define cThaRegIPEthernetTripleSpdSimpleMACTxterCtrlNoneEditableFieldsMask cBit31_16

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Simple MAC Data Inter Frame
Reg Addr: 0x45
          The address format for these registers is 0x45 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Simple MAC Data Inter Frame Control
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdSimpleMACDataInterFrm (0x000045)

/*--------------------------------------
BitField Name: IpEthTripSmacIdleRxData
BitField Type: R_O
BitField Desc: Data Inter Frame Receive from PHY or PCS
BitField Bits: 15_8
--------------------------------------*/

/*--------------------------------------
BitField Name: IpEthTripSmacIdleTxData
BitField Type: R/W
BitField Desc: Data Inter Frame Transmit to PHY or PCS
BitField Bits: 7_0
--------------------------------------*/
#define cThaIpEthTripSmacIdleTxDataMask                cBit7_0
#define cThaIpEthTripSmacIdleTxDataShift               0
#define cThaIpEthTripSmacIdleTxDataRstVal              0xDD

#define cThaRegIPEthernetTripleSpdSimpleMACDataInterFrmNoneEditableFieldsMask cBit31_8

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Simple MAC flow Control Time Interval
Reg Addr: 0x46
          The address format for these registers is 0x46 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID

Reg Desc: This register is Simple MAC the Time Interval, it used for engine
          flow control.
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdSimpleMACflowCtrlTimeIntvl (0x000046)

/*--------------------------------------
BitField Name: IpEthTripSmacTimeInterval
BitField Type: R/W
BitField Desc: In an time interval, a pause frame with quanta value that is
               configured bel0, will be transmitted if received packet FIFO is
               full, unit value is 512 bits time or 64 bytes time
BitField Bits: 15_0
--------------------------------------*/
#define cThaIpEthTripSmacTimeIntvlMask                 cBit15_0
#define cThaIpEthTripSmacTimeIntvlShift                0

#define cThaRegIPEthernetTripleSpdSimpleMACflowCtrlTimeIntvlNoneEditableFieldsMask cBit31_16

/*------------------------------------------------------------------------------
Reg Name: IP Ethernet Triple Speed Simple MAC flow Control Quanta
Reg Addr: 0x47
          The address format for these registers is 0x47 + PhyPortID*0x100
          Where: PhyPortID (0 - 3): Ethernet Physical Port ID
Reg Desc: This register is Simple MAC the Quanta, it used for engine flow
          control.
------------------------------------------------------------------------------*/
#define cThaRegIPEthernetTripleSpdSimpleMACflowCtrlQuanta   (0x000047)

/*--------------------------------------
BitField Name: IpEthTripSmacQuanta
BitField Type: R/W
BitField Desc: Quanta value of pause ON frame, unit is 512 bits time or 64
               bytes time
BitField Bits: 15_0
--------------------------------------*/
#define cThaIpEthTripSmacQuantaMask                    cBit15_0
#define cThaIpEthTripSmacQuantaShift                   0

#define cThaRegIPEthernetTripleSpdSimpleMACflowCtrlQuantaNoneEditableFieldsMask cBit31_16

/*================================================================================
 *              FOR DEBUG
 *================================================================================*/
/* This register is used dump Rx ethernet packet */

#define cThaDebugGbeSticky0  0x40100A
#define cThaDebugGbeSticky1  0x401048

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORTREG_H_ */


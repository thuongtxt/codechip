/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaEthPortFlowControl.h
 * 
 * Created Date: Feb 21, 2014
 *
 * Description : ETH Port flow control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAETHPORTFLOWCONTROL_H_
#define _THAETHPORTFLOWCONTROL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthFlowControl.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlowControl ThaEthPortFlowControlNew(AtModule module, AtEthPort port);
AtEthFlowControl ThaEthPortFlowControl10GbNew(AtModule module, AtEthPort port);

/* Product concretes */
AtEthFlowControl Tha60290021EthPortFlowControlNew(AtModule module, AtEthPort port);
AtEthFlowControl Tha60290022EthPortFlowControlNew(AtModule module, AtEthPort port);
AtEthFlowControl Tha60290051EthPortFlowControlNew(AtModule module, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THAETHPORTFLOWCONTROL_H_ */


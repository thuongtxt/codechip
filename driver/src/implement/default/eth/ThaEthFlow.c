/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaEthFlow.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa Ethernet traffic flow default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModulePpp.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/encap/AtHdlcLinkInternal.h"
#include "../ppp/ThaPppLinkInternal.h"
#include "../ppp/ThaMpigReg.h"
#include "../cla/ThaModuleCla.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "ThaEthFlow.h"
#include "ThaEthFlowInternal.h"
#include "ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaCfgReqAccsFsmIniRst		0x2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaEthFlow)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtEthFlowMethods m_AtEthFlowOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthFlowMethods *m_AtEthFlowMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static void IngressVlanEnable(ThaEthFlow self, const tAtEthVlanDesc *desc);
static void IngressVlanDisable(ThaEthFlow self, const tAtEthVlanDesc *desc);

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth EthModule(ThaEthFlow self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eBool FlowIsActivated(ThaEthFlow self)
    {
    return (self->flowController != NULL);
    }

static eAtRet Init(AtChannel self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Super */
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Set default source MAC of flows is port's MAC */
    mThis(self)->flowConfig.useFlowSMAC = cAtFalse;
    mMethodsGet(osal)->MemInit(osal, mThis(self)->flowConfig.srcMacAddress, 0,
                               sizeof(mThis(self)->flowConfig.srcMacAddress));

    /* Enable it */
    AtChannelEnable(self, cAtTrue);

    return ret;
    }

static eBool IsBusy(AtEthFlow self)
    {
    return FlowIsActivated(mThis(self));
    }

static void SimulationDatabaseDelete(AtEthFlow self)
    {
    ThaEthFlow ethFlow = (ThaEthFlow)self;
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, ethFlow->simulation);
    ethFlow->simulation = NULL;
    }

static void Delete(AtObject self)
    {
    /* Delete private data */
    SimulationDatabaseDelete((AtEthFlow)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void WillDelete(AtObject self)
    {
    AtEthFlow object = (AtEthFlow)self;
    if (object->pw)
        {
        ThaPwAdapter pwAdapter = ThaPwAdapterGet(object->pw);
        ThaPwAdapterEthFlowSet(pwAdapter, NULL);
        }

    m_AtObjectMethods->WillDelete(self);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    /* Save to database, VLAN is enable or disable will base on database's value */
    mThis(self)->flowConfig.enable = enable;

    return ThaEthFlowRxTrafficEnable(mThis(self), enable);
    }

static eBool IsEnabled(AtChannel self)
    {
    return mThis(self)->flowConfig.enable;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    AtOsal osal;
    ThaEthFlowController controller;

    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    osal = AtSharedDriverOsalGet();
    controller = ThaEthFlowControllerGet(mThis(self));
    mMethodsGet(osal)->MemInit(osal, pAllCounters, 0, sizeof(tAtEthFlowCounters));
    if (controller)
        return mMethodsGet(controller)->AllCountersGet(controller, (AtEthFlow)self, pAllCounters);

    return cAtOk;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    AtOsal osal;
    ThaEthFlowController controller;

    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    controller = ThaEthFlowControllerGet(mThis(self));
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, pAllCounters, sizeof(tAtEthFlowCounters), 0);
    if (controller)
        return mMethodsGet(controller)->AllCountersClear(controller, (AtEthFlow)self, pAllCounters);

    return cAtOk;
    }

static uint8 *DestMacBuffer(ThaEthFlowController controller, uint8 *buffer)
    {
    return buffer + mMethodsGet(controller)->StartEthHeaderInPsnBuffer(controller);
    }

static eAtModuleEthRet EgressDestMacSet(AtEthFlow self, uint8 *egressDestMac)
    {
    uint8 buffer[cThaPsnHdrMaxLength];
    uint32 psnLen;
    AtOsal osal = AtSharedDriverOsalGet();

    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);

    if (FlowIsActivated(mThis(self)))
        {
        eAtRet ret;
        ThaEthFlowController controller = ThaEthFlowControllerGet(mThis(self));
        if (controller == NULL)
            return cAtErrorNullPointer;

        /* Get current PSN buffer */
        ret = mMethodsGet(controller)->PsnBufferGet(controller, self, buffer, &psnLen);
        if (ret != cAtOk)
            return ret;

        /* Copy new MAC address */
        mMethodsGet(osal)->MemCpy(osal, DestMacBuffer(controller, buffer), egressDestMac, cAtMacAddressLen);

        /* Set back to hardware */
        ret = mMethodsGet(controller)->PsnBufferSet(controller, self, buffer, psnLen);
        if (ret != cAtOk)
            return ret;
        }

    mMethodsGet(osal)->MemCpy(osal, mThis(self)->flowConfig.destMacAddress, egressDestMac, cAtMacAddressLen);

    return cAtOk;
    }

static eAtModuleEthRet EgressDestMacGet(AtEthFlow self, uint8 *egressDestMac)
    {
    eAtRet ret = cAtOk;
    uint8 buffer[cThaPsnHdrMaxLength];
    uint32 psnLen;
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController controller;

    /* Get from database if flow has not been activated */
    if (!FlowIsActivated(mThis(self)))
        {
        mMethodsGet(osal)->MemCpy(osal, egressDestMac,
                                  mThis(self)->flowConfig.destMacAddress,
                                  cAtMacAddressLen);
        return cAtOk;
        }

    /* Get current PSN buffer */
    controller = ThaEthFlowControllerGet(mThis(self));
    ret = mMethodsGet(controller)->PsnBufferGet(controller, self, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;
    mMethodsGet(osal)->MemCpy(osal, egressDestMac, DestMacBuffer(controller, buffer), cAtMacAddressLen);

    return cAtOk;
    }

static uint8 *SourceMacBuffer(ThaEthFlowController controller, uint8 *buffer)
    {
    return buffer + cAtMacAddressLen + mMethodsGet(controller)->StartEthHeaderInPsnBuffer(controller);
    }

static eAtRet EgressSourceMacHwSet(AtEthFlow self, uint8 *egressSourceMac)
    {
    eAtRet ret = cAtOk;
    uint8 buffer[cThaPsnHdrMaxLength];
    uint32 psnLen;
    AtOsal osal;
    ThaEthFlowController controller;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);

    /* Get current PSN buffer */
    controller = ThaEthFlowControllerGet(mThis(self));
    if (controller == NULL)
        return cAtErrorNullPointer;

    ret = mMethodsGet(controller)->PsnBufferGet(controller, self, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;

    /* Change SMAC field */
    mMethodsGet(osal)->MemCpy(osal, SourceMacBuffer(controller, buffer), egressSourceMac, cAtMacAddressLen);

    /* Set back to hardware */
    return mMethodsGet(controller)->PsnBufferSet(controller, self, buffer, psnLen);
    }

static eAtModuleEthRet EgressSourceMacSet(AtEthFlow self, uint8 *sourceMac)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (FlowIsActivated(mThis(self)))
        {
        eAtRet ret = EgressSourceMacHwSet(self, sourceMac);
        if (ret != cAtOk)
            return ret;
        }

    mThis(self)->flowConfig.useFlowSMAC = cAtTrue;
    mMethodsGet(osal)->MemCpy(osal, mThis(self)->flowConfig.srcMacAddress, sourceMac, cAtMacAddressLen);

    return cAtOk;
    }

static eAtRet EgressSourceMacHwGet(AtEthFlow self, uint8 *sourceMac)
    {
    eAtRet ret;
    uint8 buffer[cThaPsnHdrMaxLength];
    uint32 psnLen;
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowController controller = ThaEthFlowControllerGet(mThis(self));
    if (controller == NULL)
        return cAtErrorNullPointer;

    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);

    /* Get current PSN buffer */
    ret = mMethodsGet(controller)->PsnBufferGet(controller, self, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;

    mMethodsGet(osal)->MemCpy(osal, sourceMac, SourceMacBuffer(controller, buffer), cAtMacAddressLen);
    return cAtOk;
    }

static eAtModuleEthRet EgressSourceMacGet(AtEthFlow self, uint8 *sourceMac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    eBool useFlowSourceMac;

    /* If flow has been already activated, always get from hardware to have real information */
    if (FlowIsActivated(mThis(self)))
        return EgressSourceMacHwGet(self, sourceMac);

    /* It has not been activated yet */
    useFlowSourceMac = mThis(self)->flowConfig.useFlowSMAC;

    /* Use port source MAC */
    if (!useFlowSourceMac)
        {
        AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
        AtEthPort port = AtModuleEthPortGet((AtModuleEth)ethModule, mThis(self)->flowConfig.egTrafficDesc.ethPortId);
        return AtEthPortSourceMacAddressGet(port, sourceMac);
        }

    /* Use flow source MAC, get from DB */
    mMethodsGet(osal)->MemCpy(osal, sourceMac,
                              mThis(self)->flowConfig.srcMacAddress,
                              sizeof(mThis(self)->flowConfig.srcMacAddress));
    return cAtOk;
    }

static eAtModuleEthRet EgressVlanSet(AtEthFlow self, const tAtEthVlanDesc *egressVlan)
    {
    eAtRet ret = m_AtEthFlowMethods->EgressVlanSet(self, egressVlan);
    if (ret != cAtOk)
        return ret;

    ret = ThaModuleEthDidChangeFlowEgressVlan(EthModule(mThis(self)), self, egressVlan);
    if (ret != cAtOk)
        return ret;

    AtOsalMemCpy(&mThis(self)->flowConfig.egTrafficDesc, egressVlan, sizeof(tAtEthVlanDesc));
    return cAtOk;
    }

static eAtModuleEthRet EgressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    eAtRet ret;
    uint32 psnLen;
    ThaEthFlowController controller;
    uint8 buffer[cThaPsnHdrMaxLength];
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlow flow = mThis(self);

    /* Let super deal with database */
    ret = m_AtEthFlowMethods->EgressVlanAdd(self, desc);
    if (ret != cAtOk)
        return ret;

    /* Just exit if flow has not been activated */
    if (!FlowIsActivated(flow))
        return cAtOk;

    controller = ThaEthFlowControllerGet(mThis(self));

    if (!mMethodsGet(controller)->ShouldControlEgressVlanByPsnBuffer(controller))
        return mMethodsGet(controller)->EgressVlanEnable(controller, self, desc);

    /* Get current PSN buffer */
    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);
    ret = mMethodsGet(controller)->PsnBufferGet(controller, self, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;

    /* Get Number of VLAN Tag */
    psnLen = mMethodsGet(controller)->InsertVlanToBuffer(controller, self, buffer, desc, psnLen);

    /* Set back to hardware */
    ret = mMethodsGet(controller)->PsnBufferSet(controller, self, buffer, psnLen);
    if (ret != cAtOk)
        return ret;

    /* Set number of Egress VLAN */
    return mMethodsGet(controller)->NumberEgVlanSet(controller, self, desc->numberOfVlans);
    }

static eAtModuleEthRet EgressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    eAtRet ret;
    uint32 psnLen;
    ThaEthFlowController controller;
    uint8 buffer[cThaPsnHdrMaxLength];
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlow flow = mThis(self);

    /* Let Supper deal with database */
    ret = m_AtEthFlowMethods->EgressVlanRemove(self, desc);
    if (ret != cAtOk)
        return ret;

    /* Just exit if flow has not been activated */
    if (!FlowIsActivated(flow))
        return cAtOk;

    controller = ThaEthFlowControllerGet(mThis(self));

    if (!mMethodsGet(controller)->ShouldControlEgressVlanByPsnBuffer(controller))
        return mMethodsGet(controller)->EgressVlanDisable(controller, self, desc);

    /* Get current PSN buffer */
    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);
    ret = mMethodsGet(controller)->PsnBufferGet(controller, self, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;

    /* Get Number of VLAN Tag */
    psnLen = mMethodsGet(controller)->RemoveVlanFromBuffer(controller, self, buffer, desc, psnLen);

    /* Set back to hardware */
    ret = mMethodsGet(controller)->PsnBufferSet(controller, self, buffer, psnLen);
    if (ret != cAtOk)
        return ret;

    /* Set number of Egress VLAN */
    return mMethodsGet(controller)->NumberEgVlanSet(controller, self, 0);
    }

static void IngressVlanDisable(ThaEthFlow self, const tAtEthVlanDesc *desc)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet(self);
    if (controller)
        mMethodsGet(controller)->IngressVlanDisable(controller, (AtEthFlow)self, desc);
    }

static void IngressVlanEnable(ThaEthFlow self, const tAtEthVlanDesc *desc)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet(self);
    if (controller == NULL)
        return;

    mMethodsGet(controller)->IngressVlanEnable(controller, (AtEthFlow)self, desc);
    }

static eBool IngressVlanCanUse(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    ThaModuleEth ethModule = EthModule(mThis(self));
    uint32 igVlanId = mMethodsGet(ethModule)->VlanIdFromVlanDesc(ethModule, self, desc);
    return ThaModuleEthIngressVlanIsInused(ethModule, self, igVlanId) ? cAtFalse : cAtTrue;
    }

static eAtModuleEthRet IngressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    uint32 igVlanId;
    ThaModuleEth ethModule = EthModule(mThis(self));
    eAtRet ret;

    /* Let super deal with database */
    ret = m_AtEthFlowMethods->IngressVlanAdd(self, desc);
    if (ret != cAtOk)
        return ret;

    /* Configure VLAN look up */
    if (FlowIsActivated(mThis(self)))
        IngressVlanEnable(mThis(self), desc);

    igVlanId = mMethodsGet(ethModule)->VlanIdFromVlanDesc(ethModule, self, desc);
    ThaModuleEthIngressVlanUse(ethModule, self, igVlanId);

    return cAtOk;
    }

static eAtModuleEthRet IngressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    eAtRet ret;
    uint32 vlanId;
    ThaModuleEth ethModule;

    /* Let super deal with database */
    ret = m_AtEthFlowMethods->IngressVlanRemove(self, desc);
    if (ret != cAtOk)
        return ret;

    /* Update hardware */
    if (FlowIsActivated(mThis(self)))
        IngressVlanDisable(mThis(self), desc);

    ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    vlanId = mMethodsGet(ethModule)->VlanIdFromVlanDesc(ethModule, self, desc);
    ThaModuleEthIngressVlanUnUse((ThaModuleEth)AtChannelModuleGet((AtChannel)self), self, vlanId);

    return cAtOk;
    }

static eAtRet AllConfigSet(AtChannel self, void *pAllConfig)
    {
    AtEthFlow ethflow = (AtEthFlow)self;
    tAtEthVlanFlowConfig *configs = (tAtEthVlanFlowConfig *)pAllConfig;
    uint8 srcMacAddr[cAtMacAddressLen];
    uint8 idx;
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(self);
    AtEthPort port = AtModuleEthPortGet(ethModule, configs->egTrafficDesc.ethPortId);
    eAtRet ret = cAtOk;

    /* Get source MAC for this flow */
    if (configs->useFlowSMAC)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, srcMacAddr, configs->srcMacAddress, sizeof(srcMacAddr));
        }
    else
        AtEthPortSourceMacAddressGet(port, srcMacAddr);

    /* Set MAC addresses */
    ret |= AtEthFlowEgressSourceMacSet(ethflow, srcMacAddr);
    ret |= AtEthFlowEgressDestMacSet(ethflow, configs->destMacAddress);

    /* Set Egress VLAN */
    ret |= AtEthFlowEgressVlanSet(ethflow, &configs->egTrafficDesc);

    /* Add lookup VLAN at IG direction */
    for (idx = 0; idx < configs->numberOfIgTrafficDesc; idx++)
        ret |= AtEthFlowIngressVlanAdd(ethflow, &(configs->igTrafficDesc[idx]));

    /* Enable */
    ret |= AtChannelEnable(self, configs->enable);
    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet(mThis(self));

    AtPrintc(cSevInfo, "=================================================\r\n");
    AtPrintc(cSevNormal, "= %s.%s\r\n",
             AtChannelTypeString((AtChannel)self),
             AtChannelIdString((AtChannel)self));
    AtPrintc(cSevInfo, "=================================================\r\n");

    /* Super */
    m_AtChannelMethods->Debug(self);

    if (controller)
        mMethodsGet(controller)->Debug(controller, (AtEthFlow)self);

    return cAtOk;
    }

static uint32 ChannelOffset(ThaEthFlow self)
    {
    uint32 hwFlowId = ThaEthFlowHwFlowIdGet(self);
    return hwFlowId + ThaEthFlowPartOffset(self, cThaModuleMpig);
    }

static eBool IsSimulated(AtEthFlow self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceIsSimulated(device);
    }

static eBool RsqBufferStateApplied(AtEthFlow flow, uint8 state)
    {
    uint32 curState, address;
    uint32 longRegVal[cThaLongRegMaxSize];

    if (IsSimulated(flow))
        return cAtTrue;

    /* Request done */
    address = cThaMPIGRsqReqFsmAccess + ThaEthFlowPartOffset(mThis(flow), cAtModuleEth);
    longRegVal[0] = mChannelHwRead(flow, address, cThaModuleMpig);
    if (mRegField(longRegVal[0], cThaMPIGRsqAccDone) == 0)
        return cAtFalse;

    /* New state must != state and != state + 1 then finish */
    address = cThaRegMPIGRSQResequenceStat + ChannelOffset(mThis(flow));
    mChannelHwLongRead(flow, address, longRegVal, 4, cThaModuleMpig);
    curState = mRegField(longRegVal[cThaMPIGRsqCurrRsFsmDwIndex], cThaMPIGRsqCurrRsFsm);

    if ((curState == state) || (curState == (uint32)(state + 1)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleEthRet ResequenceFsmStateSet(AtEthFlow flow, uint8 state)
    {
    uint32 hwFlowId = ThaEthFlowHwFlowIdGet(mThis(flow));
    uint32 regAddr, regValue;
    uint32 elapseTime;
    tAtOsalCurTime curTime, startTime;
    AtOsal osal = AtSharedDriverOsalGet();
    static const uint8 timeOutInMs = 100;

    regValue = 0;
    mRegFieldSet(regValue, cThaMPIGRsqAccReqFsm, state);
    mRegFieldSet(regValue, cThaMPIGRsqAccReqFlowId, hwFlowId);
    mRegFieldSet(regValue, cThaMPIGRsqAccReq, 0x1);

    regAddr = cThaMPIGRsqReqFsmAccess + ThaEthFlowPartOffset(mThis(flow), cAtModuleEth);
    mChannelHwWrite(flow, regAddr, regValue, cThaModuleMpig);
    mChannelHwWrite(flow, regAddr, regValue, cThaModuleMpig);

    /* Wait for HW to finish update */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < timeOutInMs)
        {
        if (RsqBufferStateApplied(flow, state))
            return cAtOk;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorIndrAcsTimeOut;
    }

static eBool HeaderIsValid(uint8 *egressSourceMac, uint8 *egressDestMac, const tAtEthVlanDesc *egressVlan)
    {
    if ((egressSourceMac == NULL) || (egressDestMac == NULL))
        return cAtFalse;

    if (!AtEthVlanDescIsValid(egressVlan))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet EgressHeaderSet(AtEthFlow self, uint8 *egressSourceMac, uint8 *egressDestMac, const tAtEthVlanDesc *egressVlan)
    {
    eAtRet ret;
    if (!HeaderIsValid(egressSourceMac, egressDestMac, egressVlan))
        return cAtErrorInvlParm;

    ret = AtEthFlowEgressSourceMacSet(self, egressSourceMac);
    ret |= AtEthFlowEgressDestMacSet(self, egressDestMac);
    ret |= AtEthFlowEgressVlanSet(self, egressVlan);

    return ret;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    if ((counterType == cAtEthFlowCounterTxGoodBytes)              ||
        (counterType == cAtEthFlowCounterTxFragmentPackets)        ||
        (counterType == cAtEthFlowCounterTxNullFragmentPackets)    ||
        (counterType == cAtEthFlowCounterTxGoodPackets)            ||
        (counterType == cAtEthFlowCounterTxGoodFragmentPackets)    ||
        (counterType == cAtEthFlowCounterTxDiscardFragmentPackets) ||
        (counterType == cAtEthFlowCounterTxWindowViolationPackets) ||
        (counterType == cAtEthFlowCounterRxGoodBytes)              ||
        (counterType == cAtEthFlowCounterRxGoodPackets)            ||
        (counterType == cAtEthFlowCounterRxFragmentPackets)        ||
        (counterType == cAtEthFlowCounterRxNullFragmentPackets)    ||
        (counterType == cAtEthFlowCounterRxDiscardPackets)         ||
        (counterType == cAtEthFlowCounterRxDiscardBytes)           ||
        (counterType == cAtEthFlowCounterTxMRRUExceedPackets))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return ThaEthFlowHwFlowIdGet((ThaEthFlow)self);
    }

static eBool EgressMacIsProgrammable(AtEthFlow self)
    {
    return ThaEthFlowControllerMacIsProgrammable(ThaEthFlowControllerGet(mThis(self)), self);
    }

static AtObject SerializeIgVlanAtIndex(AtCoder self, void *objects, uint32 objectIndex)
    {
    tAtEthVlanDesc *vlans = (tAtEthVlanDesc *)objects;
    AtUnused(self);
    return (AtObjectAny)&(vlans[objectIndex]);
    }

static void SerializeFlowConfig(AtObject config, AtCoder encoder)
    {
    tAtEthVlanFlowConfig *object = (tAtEthVlanFlowConfig *)config;

    AtCoderEncodeObjectsWithHandlers(encoder,
                                     object->igTrafficDesc, object->numberOfIgTrafficDesc,
                                     SerializeIgVlanAtIndex, "igTrafficDesc", AtUtilVlanDescriptorSerialize);
    AtCoderEncodeObjectWithHandler(encoder, (AtObjectAny)&(object->egTrafficDesc), "egTrafficDesc", AtUtilVlanDescriptorSerialize);
    mEncodeUInt8Array(destMacAddress, cAtMacAddressLen);
    mEncodeUInt8Array(srcMacAddress, cAtMacAddressLen);
    mEncodeUInt(enable);
    mEncodeUInt(useFlowSMAC);
    }

static void SerializeSimulation(AtObject simulation, AtCoder encoder)
    {
    tThaEthFlowSimulation *object = (tThaEthFlowSimulation *)simulation;
    mEncodeUInt8Array(egressPsnHeader, object->egressPsnHeaderLength);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaEthFlow object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(hwFlowId);
    mEncodeUInt(classId);
    mEncodeUInt(egTrafficEn);
    mEncodeUInt(cvlanTpid);
    mEncodeUInt(svlanTpid);
    SerializeSimulation((AtObjectAny)&(object->simulation), encoder);
    AtCoderEncodeObjectWithHandler(encoder, (AtObjectAny)&(object->flowConfig), "flowConfig", SerializeFlowConfig);
    mEncodeObjectDescription(flowController);
    }

static eAtModuleEthRet EgressVlanTpidSet(AtEthFlow self, const tAtEthVlanTag* cVlan, uint16 cVlanTpid, const tAtEthVlanTag* sVlan, uint16 sVlanTpid)
    {
    eAtRet ret;
    uint32 psnLen, vlanIndex;
    ThaEthFlowController controller;
    uint8 buffer[cThaPsnHdrMaxLength];
    AtOsal osal = AtSharedDriverOsalGet();

    controller = ThaEthFlowControllerGet(mThis(self));

    if (!mMethodsGet(controller)->ShouldControlEgressVlanByPsnBuffer(controller))
        {
        ret = mMethodsGet(controller)->EgressCVlanTpidSet(controller, self, cVlanTpid);
        if (sVlan)
            ret |= mMethodsGet(controller)->EgressSVlanTpidSet(controller, self, sVlanTpid);
        return ret;
        }

    /* Get current PSN buffer */
    mMethodsGet(osal)->MemInit(osal, buffer, 0, cThaPsnHdrMaxLength);
    ret = mMethodsGet(controller)->PsnBufferGet(controller, self, buffer, &psnLen);
    if (ret != cAtOk)
        return ret;

    /* Get Number of VLAN Tag */
    vlanIndex = mMethodsGet(controller)->VlanTpidSet(controller, self, buffer, cVlan, cVlanTpid, psnLen);
    if (sVlan)
        vlanIndex = mMethodsGet(controller)->VlanTpidSet(controller, self, buffer, sVlan, sVlanTpid, vlanIndex);

    /* Set back to hardware */
    return mMethodsGet(controller)->PsnBufferSet(controller, self, buffer, psnLen);
    }
    
static eAtRet EgressCVlanTpIdSet(AtEthFlow self, uint16 tpid)
    {
    tAtEthVlanDesc desc;

    /* Get the most inner VLAN */
    if (AtEthFlowEgressVlanAtIndex(self, 0, &desc) == NULL)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "There is no VLAN or get VLAn fail\r\n");
        return cAtErrorNotApplicable;
        }

    if (desc.numberOfVlans == 0)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "There is no VLAN or get VLAn fail\r\n");
        return cAtErrorNotApplicable;
        }

    mThis(self)->cvlanTpid = tpid;
    if (!FlowIsActivated(mThis(self)))
        return cAtOk;

    return EgressVlanTpidSet(self, &desc.vlans[0], tpid, NULL, 0);
    }

static eAtRet EgressSVlanTpIdSet(AtEthFlow self, uint16 tpid)
    {
    tAtEthVlanDesc desc;

    /* Get the most inner VLAN */
    if (AtEthFlowEgressVlanAtIndex(self, 0, &desc) == NULL)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "There is no VLAN or get VLAn fail\r\n");
        return cAtErrorNotApplicable;
        }

    if (desc.numberOfVlans == 0)
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "There is no VLAN or get VLAn fail\r\n");
        return cAtErrorNotApplicable;
        }

    mThis(self)->svlanTpid = tpid;
    if (!FlowIsActivated(mThis(self)))
        return cAtOk;

    return EgressVlanTpidSet(self, &desc.vlans[0], AtEthFlowEgressCVlanTpIdGet(self), &desc.vlans[1], tpid);
    }

static uint16 EgressCVlanTpIdGet(AtEthFlow self)
    {
    if (mThis(self)->cvlanTpid == 0)
        return mMethodsGet(self)->EgressCVlanDefaultTpId(self);

    return mThis(self)->cvlanTpid;
    }

static uint16 EgressSVlanTpIdGet(AtEthFlow self)
    {
    if (mThis(self)->svlanTpid == 0)
        return mMethodsGet(self)->EgressSVlanDefaultTpId(self);

    return mThis(self)->svlanTpid;
    }

static void OverrideAtObject(ThaEthFlow self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, WillDelete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(ThaEthFlow self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllConfigSet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthFlow(ThaEthFlow self)
    {
    AtEthFlow flow = (AtEthFlow)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthFlowMethods = mMethodsGet(flow);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowOverride, mMethodsGet(flow), sizeof(m_AtEthFlowOverride));
        mMethodOverride(m_AtEthFlowOverride, EgressHeaderSet);
        mMethodOverride(m_AtEthFlowOverride, EgressDestMacSet);
        mMethodOverride(m_AtEthFlowOverride, EgressDestMacGet);
        mMethodOverride(m_AtEthFlowOverride, EgressSourceMacSet);
        mMethodOverride(m_AtEthFlowOverride, EgressSourceMacGet);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanAdd);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanRemove);
        mMethodOverride(m_AtEthFlowOverride, IngressVlanCanUse);
        mMethodOverride(m_AtEthFlowOverride, IsBusy);
        mMethodOverride(m_AtEthFlowOverride, EgressMacIsProgrammable);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanAdd);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanRemove);
        mMethodOverride(m_AtEthFlowOverride, EgressVlanSet);
        mMethodOverride(m_AtEthFlowOverride, EgressCVlanTpIdSet);
        mMethodOverride(m_AtEthFlowOverride, EgressCVlanTpIdGet);
        mMethodOverride(m_AtEthFlowOverride, EgressSVlanTpIdSet);
        mMethodOverride(m_AtEthFlowOverride, EgressSVlanTpIdGet);
        }

    mMethodsSet(flow, &m_AtEthFlowOverride);
    }

static void Override(ThaEthFlow self)
    {
    OverrideAtEthFlow(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

AtEthFlow ThaEthFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaEthFlow));

    /* Super constructor */
    if (AtEthFlowObjectInit((AtEthFlow)self, flowId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    /* To indicate that there is no hardware flow is assigned to this flow */
    (mThis(self))->hwFlowId = cInvalidUint32;

    return self;
    }

AtEthFlow ThaEthFlowNew(uint32 flowId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlow newFlow = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaEthFlow));
    if (newFlow == NULL)
        return NULL;

    /* Construct it */
    return ThaEthFlowObjectInit(newFlow, flowId, module);
    }

eAtRet ThaEthFlowTxTrafficEnable(ThaEthFlow self, eBool enable)
    {
    eAtRet ret;
    ThaEthFlowController flowController = ThaEthFlowControllerGet(self);

    /* If flow is activated */
    if (flowController)
        {
        ret = mMethodsGet(flowController)->TxTrafficEnable(flowController, (AtEthFlow)self, cAtTrue);
        if (ret != cAtOk)
            return ret;
        }

    /* Save to database */
    self->egTrafficEn = enable;

    return cAtOk;
    }

eBool ThaEthFlowTxTrafficIsEnabled(ThaEthFlow self)
    {
    ThaEthFlowController flowController = ThaEthFlowControllerGet(self);

    /* If flow is activated */
    if (flowController)
        return mMethodsGet(flowController)->TxTrafficIsEnabled(flowController, (AtEthFlow)self);

    /* Get from database */
    else
        return self->egTrafficEn;
    }

eAtRet ThaEthFlowRxTrafficEnable(ThaEthFlow self, eBool enable)
    {
    eAtRet ret;
    ThaEthFlowController flowController = ThaEthFlowControllerGet(self);

    /* If flow is activated */
    if (flowController)
        {
        ret = mMethodsGet(flowController)->RxTrafficEnable(flowController, (AtEthFlow)self, enable);
        if (ret != cAtOk)
            return ret;
        }

    /* Save to database */
    self->egTrafficEn = enable;

    return cAtOk;
    }

uint32 ThaEthFlowHwFlowIdGet(ThaEthFlow self)
    {
    ThaEthFlowController controller = ThaEthFlowControllerGet(self);
    if (controller)
        return mMethodsGet(controller)->HwFlowId(controller, (AtEthFlow)self);

    return cInvalidUint32;
    }

eAtRet ThaEthFlowMaxDelaySet(ThaEthFlow self, uint8 maxDelay)
    {
    uint32 regAddr, regVal;

    if (!FlowIsActivated(self))
        return cAtOk;

    regAddr = cThaRegMPIGRSQResequenceTimeout + ChannelOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, cThaMPIGRsqTmoThshTimerMask, cThaMPIGRsqTmoThshTimerShift, maxDelay);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

uint32 ThaEthFlowMaxDelayGet(ThaEthFlow self)
    {
    uint32 regAddr, regVal;
    uint32 maxDelay;

    if (!FlowIsActivated(self))
        return AtHdlcBundleMaxDelayGet((AtHdlcBundle)AtEthFlowMpBundleGet((AtEthFlow)self));

    regAddr = cThaRegMPIGRSQResequenceTimeout + ChannelOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldGet(regVal, cThaMPIGRsqTmoThshTimerMask, cThaMPIGRsqTmoThshTimerShift, uint32, &maxDelay);

    return maxDelay;
    }

eAtRet ThaEthFlowMrruSet(ThaEthFlow self, uint32 mrru)
    {
    uint32 regAddr, regVal;

    if (!FlowIsActivated(self))
        return cAtOk;

    regAddr = cThaRegMPIGRSQPktAssemblerCtrl + ChannelOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, cThaMPIGRsqPkaMrruMask, cThaMPIGRsqPkaMrruShift, mrru);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

eAtRet ThaEthFlowResequenceBufferRelease(AtEthFlow flow)
    {
    return ResequenceFsmStateSet(flow, cThaCfgReqAccsFsmIniRst);
    }

uint8 ThaEthFlowClassInMpBundleGet(ThaEthFlow self)
    {
    return self->classId;
    }

ThaEthFlowController ThaEthFlowControllerGet(ThaEthFlow self)
    {
    return self ? self->flowController : NULL;
    }

ThaEthFlowController ThaEthFlowControllerSet(ThaEthFlow self, ThaEthFlowController controller)
    {
    self->flowController = controller;
    return self->flowController;
    }

uint8 ThaEthFlowVlanPriorityGet(ThaEthFlow self, const tAtEthVlanDesc *desc)
    {
    ThaModuleEth ethModule = EthModule(self);
    if (ethModule == NULL)
        return 0;

    if (desc->numberOfVlans == 0)
        return 0;
    return desc->vlans[mMethodsGet(ethModule)->VlanToLookUp(ethModule)].priority;
    }

uint32 ThaEthFlowPartOffset(ThaEthFlow self, eAtModule moduleId)
    {
    uint8 partId = ThaEthFlowControllerPartIdGet(ThaEthFlowControllerGet(self), (AtEthFlow)self);
    return ThaDeviceModulePartOffset((ThaDevice)AtChannelDeviceGet((AtChannel)self), moduleId, partId);
    }

void ThaEthFlowHwFlowIdSet(ThaEthFlow self, uint32 hwFlowId)
    {
    if (self)
        (mThis(self))->hwFlowId = hwFlowId;
    }

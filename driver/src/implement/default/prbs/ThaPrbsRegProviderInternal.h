/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsRegProviderInternal.h
 * 
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSREGPROVIDERINTERNAL_H_
#define _THAPRBSREGPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtDriverInternal.h" /* For OSAL */
#include "AtObject.h" /* For object related macros */
#include "ThaPrbsRegProvider.h"
#include "ThaModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsRegProviderMethods
    {
    uint32 (*BaseAddress)(ThaPrbsRegProvider self);

    /* Factor to calculation channel ID */
    uint32 (*TdmChannelIdSliceFactor)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*TdmChannelIdStsFactor)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*TdmChannelIdVtgFactor)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*TdmChannelIdVtFactor)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*MonTdmChannelIdSliceFactor)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);

    /* Registers */
    uint32 (*RegMapBERTGenSelectedChannel)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTGenNxDs0ConcateControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTGenMode)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTGenFixedPatternControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTGenErrorRateInsert)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTGenSingleBitErrInsert)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTGenGoodBitCounter)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonSelectedChannel)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonNxDs0ConcateControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonGlobalControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonStickyEnable)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonSticky)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonMode)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonFixedPatternControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonErrorCounter)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonGoodBitCounter)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonLossBitCounter)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonCounterLoadId)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonErrorCounterLoading)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonGoodBitCounterLoading)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonLossBitCounterLoading)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegMapBERTMonStatus)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonSelectedChannel)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonNxDs0ConcateControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonGlobalControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonStickyEnable)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonSticky)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonMode)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonFixedPatternControl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonErrorCounter)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonGoodBitCounter)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonLossBitCounter)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonCounterLoadId)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonErrorCounterLoading)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonGoodBitCounterLoading)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonLossBitCounterLoading)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegDemapBERTMonStatus)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
    uint32 (*RegPdaPwPrbsGenCtrl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegPdaPwPrbsMonCtrl)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);

    /* Bit fields */
    uint32 (*RegMapBERTGenTxPtgEnMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegMapBERTGenTxPtgIdMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegDemapBERTMonRxPtgEnMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegDemapBERTMonRxPtgIdMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegMapBERTMonTxPtgEnMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegMapBERTMonTxPtgIdMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegMapBERTMonTdmMonAndPsnMonSelectMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegMapBERTGenTxBerMdMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint8  (*RegMapBERTGenTxBerMdShift)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegMapBERTGenTxBerErrMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint8  (*RegMapBERTGenTxBerErrShift)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint32 (*RegMapBERTGenTxSingleErrMask)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    uint8  (*RegMapBERTGenTxSingleErrShift)(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
    }tThaPrbsRegProviderMethods;

typedef struct tThaPrbsRegProvider
    {
    tAtObject super;
    const tThaPrbsRegProviderMethods *methods;
    }tThaPrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPrbsRegProvider ThaPrbsRegProviderObjectInit(ThaPrbsRegProvider self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSREGPROVIDERINTERNAL_H_ */


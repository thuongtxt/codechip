/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngineAuVc.c
 *
 * Created Date: Nov 12, 2013
 *
 * Description : PRBS engine for AU VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhVc.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaSdhVc.h"
#include "../prbs/ThaModulePrbs.h"
#include "ThaPrbsEngineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPrbsEngineAuVc)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPrbsEngineAuVcMethods m_methods;

/* Override */
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelIdWithFactors(ThaPrbsEngine self, uint32 sliceFactor, uint32 stsFactor)
    {
    uint8 slice, hwSts;
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice, &hwSts);
    return (sliceFactor * slice) + (stsFactor * hwSts);
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ChannelIdWithFactors(self,
                                ThaModulePrbsTdmChannelIdSliceFactor(module),
                                ThaModulePrbsTdmChannelIdStsFactor(module));
    }

static uint32 RxChannelId(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ChannelIdWithFactors(self,
                                ThaModulePrbsMonTdmChannelIdSliceFactor(module),
                                ThaModulePrbsTdmChannelIdStsFactor(module));
    }

static uint8 HwSliceId(ThaPrbsEngine self)
    {
    uint8 slice, hwSts;
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice, &hwSts);
    return slice;
    }

static eBool IsTu3Vc(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
	if (channelType != cAtSdhChannelTypeVc3)
		return cAtFalse;

    return (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeTu3) ? cAtTrue : cAtFalse;
    }

static void PohInsert(ThaPrbsEngineAuVc self, AtSdhChannel channel, eBool enable)
	{
    /* Only need to care master STS */
    uint8 swStsId = AtSdhChannelSts1Get(channel);

    AtUnused(self);

	if (IsTu3Vc(channel))
		ThaOcnVtPohInsertEnable(channel, swStsId, 0, 0, enable);
	else
		ThaOcnStsPohInsertEnable(channel, swStsId, enable);
	}

static void PohInsertHandle(ThaPrbsEngineAuVc self, eBool prbsEnable)
    {
    AtSdhChannel channel = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtSdhChannel pohInsertionChannel = mMethodsGet(self)->ChannelForPohInsertion(mThis(self), channel);
    /* POH insertion can happen not at the channel where PRBS starts. This is true
     * for products that have XC */
    if (pohInsertionChannel)
        {
        if (prbsEnable)
            mMethodsGet(mThis(self))->PohInsert(mThis(self), pohInsertionChannel, cAtTrue);
        else
            {
            /* If channel bound to PW, POH insert must be disabled */
            eBool pohShouldBeInserted = (AtChannelBoundPwGet((AtChannel)channel)) ? cAtFalse : cAtTrue;
            mMethodsGet(mThis(self))->PohInsert(mThis(self), pohInsertionChannel, pohShouldBeInserted);
            }
        }
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    /* Do nothing if reconfigure */
    if (AtPrbsEngineTxIsEnabled(self) == enable)
        return cAtOk;

    mMethodsGet(mThis(self))->PohInsertHandle(mThis(self), enable);

    return m_AtPrbsEngineMethods->TxEnable(self, enable);
    }

static AtSdhChannel ChannelForPohInsertion(ThaPrbsEngineAuVc self, AtSdhChannel channel)
    {
    AtUnused(self);
    return channel;
    }

static eAtRet AllSubCircuitsPwSquel(ThaPrbsEngine self, eBool squel)
    {
    return ThaPrbsEngineAllSdhSubChannelsPwSquel((AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self), squel);
    }

static ThaModulePrbs PrbsModule(ThaPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (ThaModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eBool ChannelizedPrbsAllowed(ThaPrbsEngine self)
    {
    return AtModulePrbsChannelizedPrbsAllowed((AtModulePrbs)PrbsModule(self));
    }

static eAtRet Vc4Terminate(AtSdhChannel vc, eBool terminated)
    {
    uint8 numSts1s = AtSdhChannelNumSts(vc);
    uint8 startSts1 = AtSdhChannelSts1Get(vc);
    uint8 sts_i;
    eAtRet ret = cAtOk;

    for (sts_i = 0; sts_i < numSts1s; sts_i++)
        {
        uint8 sts1 = (uint8)(startSts1 + sts_i);
        ret |= ThaModuleOcnRxStsTermEnable(vc, sts1, terminated);
        }

    return ret;
    }

static eAtRet Channelize(ThaPrbsEngine self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    eAtRet ret = cAtOk;

    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc3)
        ret |= ThaOcnVc3PldSet(vc, ThaSdhVcOcnVc3PldType(vc, AtSdhChannelMapTypeGet(vc)));

    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4)
        {
        uint8 mapType = AtSdhChannelMapTypeGet(vc);
        eBool terminated = (mapType == cAtSdhVcMapTypeVc4Map3xTug3s) ? cAtTrue : cAtFalse;
        ret |= Vc4Terminate(vc, terminated);
        }

    return ret;
    }

static eAtRet Unchannelize(ThaPrbsEngine self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    eAtRet ret = cAtOk;

    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc3)
        ret |= ThaOcnVc3PldSet(vc, cThaOcnVc3PldC3);

    if (AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc4)
        ret |= Vc4Terminate(vc, cAtFalse);

    return ret;
    }

static eBool IsChannelized(ThaPrbsEngine self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(vc);
    uint8 mapType = AtSdhChannelMapTypeGet(vc);

    if (mapType == cAtSdhVcMapTypeNone)
        return cAtFalse;

    switch ((uint32)vcType)
        {
        case cAtSdhChannelTypeVc3:
            return (mapType == cAtSdhVcMapTypeVc3MapC3) ? cAtFalse : cAtTrue;
        case cAtSdhChannelTypeVc4:
            return (mapType == cAtSdhVcMapTypeVc4MapC4) ? cAtFalse : cAtTrue;

        default:
            return cAtFalse;
        }
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, HwSliceId);
        mMethodOverride(m_ThaPrbsEngineOverride, AllSubCircuitsPwSquel);
        mMethodOverride(m_ThaPrbsEngineOverride, ChannelizedPrbsAllowed);
        mMethodOverride(m_ThaPrbsEngineOverride, Channelize);
        mMethodOverride(m_ThaPrbsEngineOverride, Unchannelize);
        mMethodOverride(m_ThaPrbsEngineOverride, IsChannelized);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

static void MethodsInit(ThaPrbsEngineAuVc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PohInsert);
        mMethodOverride(m_methods, ChannelForPohInsertion);
        mMethodOverride(m_methods, PohInsertHandle);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineAuVc);
    }

AtPrbsEngine ThaPrbsEngineAuVcObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineObjectInit(self, (AtChannel)vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine ThaPrbsEngineAuVcNew(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaPrbsEngineAuVcObjectInit(newEngine, vc, engineId);
    }

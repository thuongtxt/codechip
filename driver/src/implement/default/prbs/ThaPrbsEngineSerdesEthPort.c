/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngineSerdesEthPort.c
 *
 * Created Date: Nov 6, 2013
 *
 * Description : SDH Line SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPrbsEngineSerdesInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineSerdesMethods m_ThaPrbsEngineSerdesOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DiagnosticControlRegister(ThaPrbsEngineSerdes self)
    {
	AtUnused(self);
    return 0xF00043;
    }

static uint32 DiagnosticAlarmRegister(ThaPrbsEngineSerdes self)
    {
	AtUnused(self);
    return 0xF00053;
    }

static void OverrideThaPrbsEngineSerdes(AtPrbsEngine self)
    {
    ThaPrbsEngineSerdes engine = (ThaPrbsEngineSerdes)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineSerdesOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEngineSerdesOverride));

        mMethodOverride(m_ThaPrbsEngineSerdesOverride, DiagnosticControlRegister);
        mMethodOverride(m_ThaPrbsEngineSerdesOverride, DiagnosticAlarmRegister);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineSerdesOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideThaPrbsEngineSerdes(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineSerdesEthPort);
    }

AtPrbsEngine ThaPrbsEngineSerdesEthPortObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineSerdesObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine ThaPrbsEngineSerdesEthPortNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ThaPrbsEngineSerdesEthPortObjectInit(newEngine, serdesController);
    }

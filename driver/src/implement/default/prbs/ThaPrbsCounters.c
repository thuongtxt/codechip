/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsCounters.c
 *
 * Created Date: Aug 17, 2018
 *
 * Description : Default PRBS counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPrbsEngine.h"
#include "../../../generic/man/AtDriverInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaPrbsCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtPrbsCountersMethods   m_AtPrbsCountersOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint64 *CounterAddress(AtPrbsCounters self, uint16 counterType)
    {
    ThaPrbsCounters counters = AtCast(ThaPrbsCounters, self);
    switch (counterType)
        {
        case cAtPrbsEngineCounterTxBit:      return &counters->txBit;
        case cAtPrbsEngineCounterRxBit:      return &counters->rxBit;
        case cAtPrbsEngineCounterRxBitError: return &counters->rxErrorBit;
        case cAtPrbsEngineCounterRxSync:     return &counters->rxSyncBit;
        case cAtPrbsEngineCounterRxBitLoss:  return &counters->rxLossBit;
        default:                             return NULL;
        }
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(txBit);
    mEncodeNone(rxBit);
    mEncodeNone(rxErrorBit);
    mEncodeNone(rxSyncBit);
    mEncodeNone(rxLossBit);
    }

static void OverrideAtPrbsCounters(ThaPrbsCounters self)
    {
    AtPrbsCounters counters = (AtPrbsCounters)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsCountersOverride, mMethodsGet(counters), sizeof(m_AtPrbsCountersOverride));

        mMethodOverride(m_AtPrbsCountersOverride, CounterAddress);
        }

    mMethodsSet(counters, &m_AtPrbsCountersOverride);
    }

static void OverrideAtObject(ThaPrbsCounters self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPrbsCounters self)
    {
    OverrideAtPrbsCounters(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsCounters);
    }

ThaPrbsCounters ThaPrbsCountersObjectInit(ThaPrbsCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsCountersObjectInit((AtPrbsCounters)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsCounters ThaPrbsCountersNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPrbsCounters self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ThaPrbsCountersObjectInit(self);
    }


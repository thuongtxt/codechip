/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineNxDs0Internal.h
 * 
 * Created Date: May 1, 2017
 *
 * Description : NxDS0 PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINENXDS0INTERNAL_H_
#define _THAPRBSENGINENXDS0INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineNxDs0
    {
    tThaPrbsEngine super;
    }tThaPrbsEngineNxDs0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEngineNxDs0ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINENXDS0INTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineSerdes.h
 * 
 * Created Date: Nov 6, 2013
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINESERDES_H_
#define _THAPRBSENGINESERDES_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPrbsEngine.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineSerdes * ThaPrbsEngineSerdes;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtPrbsEngine ThaPrbsEngineSerdesSdhLineNew(AtSerdesController serdesController);
AtPrbsEngine ThaPrbsEngineSerdesEthPortNew(AtSerdesController serdesController);

AtSerdesController ThaPrbsEngineSerdesController(ThaPrbsEngineSerdes self);

/* Concrete products */
AtPrbsEngine Tha60091132PrbsEngineSerdesEthPortNew(AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINESERDES_H_ */


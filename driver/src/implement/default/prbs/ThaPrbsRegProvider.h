/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsRegProvider.h
 * 
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSREGPROVIDER_H_
#define _THAPRBSREGPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Providers */
ThaPrbsRegProvider ThaPrbsRegProviderDefaultProvider(void);
ThaPrbsRegProvider Tha60210011PrbsRegProvider(void);
ThaPrbsRegProvider Tha60210031PrbsRegProvider(void);
ThaPrbsRegProvider Tha60210031PrbsRegProviderV2(void);
ThaPrbsRegProvider Tha60210031PrbsRegProviderV3(void);
ThaPrbsRegProvider Tha60210021PrbsRegProvider(void);
ThaPrbsRegProvider Tha60210051PrbsRegProvider(void);
ThaPrbsRegProvider Tha60290011PrbsRegProvider(void);
ThaPrbsRegProvider Tha60290011PrbsRegProviderV2(void);
ThaPrbsRegProvider Tha60290021PrbsRegProvider(void);
ThaPrbsRegProvider Tha60290022PrbsRegProvider(void);
ThaPrbsRegProvider Tha60291011PrbsRegProvider(void);

uint32 ThaPrbsRegProviderBaseAddress(ThaPrbsRegProvider self);

uint32 ThaPrbsRegProviderTdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderTdmChannelIdStsFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderTdmChannelIdVtgFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderTdmChannelIdVtFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderMonTdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);

uint32 ThaPrbsRegProviderRegMapBERTGenSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTGenNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTGenMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTGenFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTGenSingleBitErrInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTGenGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice);
uint32 ThaPrbsRegProviderRegPdaPwPrbsGenCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegPdaPwPrbsMonCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);

/* Bit fields */
uint32 ThaPrbsRegProviderRegMapBERTGenTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegMapBERTGenTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegDemapBERTMonRxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegMapBERTMonSideSelectMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegMapBERTGenTxBerMdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint8  ThaPrbsRegProviderRegMapBERTGenTxBerMdShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegMapBERTGenTxBerErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint8  ThaPrbsRegProviderRegMapBERTGenTxBerErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint32 ThaPrbsRegProviderRegMapBERTGenTxSingleErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);
uint8  ThaPrbsRegProviderRegMapBERTGenTxSingleErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSREGPROVIDER_H_ */


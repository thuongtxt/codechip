/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineDe3Internal.h
 * 
 * Created Date: May 2, 2017
 *
 * Description : DE3 PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINEDE3INTERNAL_H_
#define _THAPRBSENGINEDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPrbsEnginePdhChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineDe3
    {
    tThaPrbsEnginePdhChannel super;
    }tThaPrbsEngineDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINEDE3INTERNAL_H_ */


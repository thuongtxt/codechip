/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngineVc1x.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhChannel.h"
#include "AtSdhVc.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaSdhVc.h"
#include "../map/ThaModuleStmMap.h"
#include "../man/ThaDevice.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "../pdh/ThaStmModulePdh.h"
#include "ThaPrbsEngineVc1xInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaPrbsEngineVc1x *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods        m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods       m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineVc1x);
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    uint8 slice, hwSts;
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint8 vtgId = AtSdhChannelTug2Get(vc);
    uint8 vtId = AtSdhChannelTu1xGet(vc);
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice, &hwSts);
    return (ThaModulePrbsTdmChannelIdSliceFactor(module) * slice) +
           (ThaModulePrbsTdmChannelIdStsFactor(module)   * hwSts) +
           (ThaModulePrbsTdmChannelIdVtgFactor(module)   * vtgId) +
           (ThaModulePrbsTdmChannelIdVtFactor(module)    * vtId);
    }

static uint8 HwSliceId(ThaPrbsEngine self)
    {
    uint8 slice, hwSts;
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice, &hwSts);
    return slice;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    AtSdhChannel channel = (AtSdhChannel)AtPrbsEngineChannelGet(self);
    uint8 swStsId, tug2Id, vtId;

    /* Do nothing if reconfigure */
    if (AtPrbsEngineTxIsEnabled(self) == enable)
        return cAtOk;

    /* Only need to care master STS */
    swStsId = AtSdhChannelSts1Get(channel);
    tug2Id = AtSdhChannelTug2Get(channel) ;
    vtId = AtSdhChannelTu1xGet(channel) ;

    if (enable)
        ThaOcnVtPohInsertEnable(channel, swStsId, tug2Id, vtId, cAtTrue);
    else
        {
        /* If channel bound to PW, POH insert must be disabled */
        eBool pohShouldBeInserted = (AtChannelBoundPwGet((AtChannel)channel)) ? cAtFalse : cAtTrue;
        ThaOcnVtPohInsertEnable(channel, swStsId,  tug2Id, vtId, pohShouldBeInserted);
        }

    return m_AtPrbsEngineMethods->TxEnable(self, enable);
    }

static eAtRet MapPohEnable(AtPrbsEngine self)
    {
    AtSdhVc vc = (AtSdhVc)AtPrbsEngineChannelGet(self);
    AtDevice device = AtChannelDeviceGet((AtChannel)vc);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleAbstractMapVc1xFrameModeSet(mapModule, vc, cMapFrameTypeVtWithPoh);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* If the SDH mapping just stop at C1x, POH insertion may not be configured
     * at the MAP module. Updating the SDH Mapping logic is too risk, so we put
     * this logic here to configure correct POH insertion at MAP module when
     * PRBS engine is created on VC-1x */
    ret = MapPohEnable(self);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static ThaModulePrbs PrbsModule(ThaPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (ThaModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eBool ChannelizedPrbsAllowed(ThaPrbsEngine self)
    {
    return AtModulePrbsChannelizedPrbsAllowed((AtModulePrbs)PrbsModule(self));
    }

static eAtRet AllSubCircuitsPwSquel(ThaPrbsEngine self, eBool forced)
    {
    return ThaPrbsEngineAllSdhSubChannelsPwSquel((AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self), forced);
    }

static eBool IsChannelized(ThaPrbsEngine self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint8 mapType = AtSdhChannelMapTypeGet(vc);
    return (mapType == cAtSdhVcMapTypeVc1xMapDe1) ? cAtTrue : cAtFalse;
    }

static ThaModuleCdr CdrModule(ThaPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    }

static AtModulePdh PdhModule(ThaPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    }

static ThaModuleAbstractMap MapModule(ThaPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static eAtRet MapTypeSet(ThaPrbsEngine self, uint8 mapType)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    eAtRet ret = cAtOk;
    ThaModuleAbstractMap mapModule = MapModule(self);

    ret |= ThaCdrVtPldSet(CdrModule(self), vc, mapType);
    ret |= ThaPdhVtMapMdSet(PdhModule(self), vc, ThaSdhVc1xPdhVcMapMode(vc, mapType));
    ret |= ThaMapVtPldMdSet((ThaModuleMap)mapModule,
                            vc,
                            AtSdhChannelSts1Get(vc),
                            AtSdhChannelTug2Get(vc),
                            AtSdhChannelTu1xGet(vc),
                            ThaSdhVc1xMapTypeOfMapModule(vc, mapType));

    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelMapTypeGet(vc) == cAtSdhVcMapTypeVc1xMapDe1)
        {
        AtPdhDe1 de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(vc);
        return ThaModuleAbstractMapDe1FrmModeSet(mapModule, de1, AtPdhChannelFrameTypeGet((AtPdhChannel)de1));
        }

    return ThaModuleAbstractMapVc1xFrameModeSet(mapModule, (AtSdhVc)vc, cMapFrameTypeCepBasic);
    }

static eAtRet Unchannelize(ThaPrbsEngine self)
    {
    return MapTypeSet(self, cAtSdhVcMapTypeVc1xMapC1x);
    }

static eAtRet Channelize(ThaPrbsEngine self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return MapTypeSet(self, AtSdhChannelMapTypeGet(vc));
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, HwSliceId);
        mMethodOverride(m_ThaPrbsEngineOverride, ChannelizedPrbsAllowed);
        mMethodOverride(m_ThaPrbsEngineOverride, AllSubCircuitsPwSquel);
        mMethodOverride(m_ThaPrbsEngineOverride, IsChannelized);
        mMethodOverride(m_ThaPrbsEngineOverride, Unchannelize);
        mMethodOverride(m_ThaPrbsEngineOverride, Channelize);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

AtPrbsEngine ThaPrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel vc1x, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineObjectInit(self, (AtChannel)vc1x, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine ThaPrbsEngineVc1xNew(AtSdhChannel vc1x, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaPrbsEngineVc1xObjectInit(newEngine, vc1x, engineId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaModulePrbsReg.h
 * 
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPRBSREG_H_
#define _THAMODULEPRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* MAP GEN */
#define cThaRegMapBERTGenSelectedChannel               0x1B8500
#define cThaRegMapBERTGenTxPtgEnMask                   cBit9
#define cThaRegMapBERTGenTxPtgEnShift                  9

#define cThaRegMapBERTGenTxPtgIdMask                   cBit8_0
#define cThaRegMapBERTGenTxPtgIdShift                  0

#define cThaRegMapBERTGenNxDs0ConcateControl           0x1B8360


#define cThaRegMapBERTGenMode                          0x1B8300
#define cThaRegMapBERTGenTxPtgSwapMask                 cBit11
#define cThaRegMapBERTGenTxPtgSwapShift                11
#define cThaRegMapBERTGenTxPtgInvMask                  cBit10
#define cThaRegMapBERTGenTxPtgInvShift                 10
#define cThaRegMapBERTGenTxPtmPatBitMask               cBit9_5
#define cThaRegMapBERTGenTxPtmPatBitShift              5
#define cThaRegMapBERTGenTxPtgModeMask                 cBit4_0
#define cThaRegMapBERTGenTxPtgModeShift                0


#define cThaRegMapBERTGenFixedPatternControl           0x1B8310
#define cThaRegMapBERTGenTxFixPatMask                  cBit31_0
#define cThaRegMapBERTGenTxFixPatShift                 0

#define cThaRegMapBERTGenErrorRateInsert               0x1B8320
#define cThaRegMapBERTGenTxBerErrMask                  cBit3
#define cThaRegMapBERTGenTxBerErrShift                 3
#define cThaRegMapBERTGenTxBerMdMask                   cBit2_0
#define cThaRegMapBERTGenTxBerMdShift                  0

#define cThaRegMapBERTGenSingleBitErrInsert            0x1B837E
#define cThaRegMapBERTGenTxPtgErrIDInsMask             cBit3_0
#define cThaRegMapBERTGenTxPtgErrIDInsShift            0

#define cThaRegMapBERTGenGoodBitCounter                0x1B8380
#define cThaRegMapBERTGenGoodBitCounterMask            cBit31_0
#define cThaRegMapBERTGenGoodBitCounterShift           0

/* MAP MONITORING */
#define cThaRegMapBERTMonSelectedChannel               0x1B8510
#define cThaRegMapBERTMonTxPtgEnMask                   cBit10
#define cThaRegMapBERTMonTxPtgEnShift                  10

#define cThaRegMapBERTMonTxPtgIdMask                   cBit9_0
#define cThaRegMapBERTMonTxPtgIdShift                  0

#define cThaRegMapBERTMonNxDs0ConcateControl           0x1B84C0

#define cThaRegMapBERTMonGlobalControl                 0x1B8400
#define cThaRegMapBERTMonTxPtmLosThrMask               cBit11_8
#define cThaRegMapBERTMonTxPtmLosThrShift              8

#define cThaRegMapBERTMonTxPtmSynThrMask               cBit7_4
#define cThaRegMapBERTMonTxPtmSynThrShift              4

#define cThaRegMapBERTMonStickyEnable                  0x1B8401
#define cThaRegMapBERTMonTxPtmStkEnMask(engineId)      (cBit0 << (engineId))
#define cThaRegMapBERTMonTxPtmStkEnShift(engineId)     (engineId)

#define cThaRegMapBERTMonSticky                        0x1B8402
#define cThaRegMapBERTMonTxPtmStkMask(engineId)       (cBit0 << (engineId))
#define cThaRegMapBERTMonTxPtmStkShift(engineId)      (engineId)

#define cThaRegMapBERTMonMode                          0x1B8410
#define cThaRegMapBERTMonTxPtmSwapMask                 cBit11
#define cThaRegMapBERTMonTxPtmSwapShift                11
#define cThaRegMapBERTMonTxPtmInvMask                  cBit10
#define cThaRegMapBERTMonTxPtmInvShift                 10
#define cThaRegMapBERTMonTxPtmPatBitMask               cBit9_5
#define cThaRegMapBERTMonTxPtmPatBitShift              5
#define cThaRegMapBERTMonTxPtgModeMask                 cBit4_0
#define cThaRegMapBERTMonTxPtgModeShift                0

#define cThaRegMapBERTMonFixedPatternControl           0x1B8420
#define cThaRegMapBERTMonTxFixPatMask                  cBit31_0
#define cThaRegMapBERTMonTxFixPatShift                 0

#define cThaRegMapBERTMonErrorCounter                  0x1B8460
#define cThaRegMapBERTMonErrorCounterMask              cBit31_0
#define cThaRegMapBERTMonErrorCounterShift             0

#define cThaRegMapBERTMonGoodBitCounter                0x1B8480
#define cThaRegMapBERTMonGoodBitCounterMask            cBit31_0
#define cThaRegMapBERTMonGoodBitCounterShift           0

#define cThaRegMapBERTMonLossBitCounter                0x1B84A0
#define cThaRegMapBERTMonLossBitCounterMask            cBit31_0
#define cThaRegMapBERTMonLossBitCounterShift           0

#define cThaRegMapBERTMonCounterLoadId                 0x1B8403
#define cThaRegMapBERTMonTxPtmLoadIDMask               cBit3_0
#define cThaRegMapBERTMonTxPtmLoadIDShift              0

#define cThaRegMapBERTMonErrorCounterLoading           0x1B8404
#define cThaRegMapBERTMonErrorCounterLoadingMask       cBit31_0
#define cThaRegMapBERTMonErrorCounterLoadingShift      0

#define cThaRegMapBERTMonGoodBitCounterLoading         0x1B8405
#define cThaRegMapBERTMonGoodBitCounterLoadingMask     cBit31_0
#define cThaRegMapBERTMonGoodBitCounterLoadingShift    0

#define cThaRegMapBERTMonLossBitCounterLoading         0x1B8406
#define cThaRegMapBERTMonLossBitCounterLoadingMask     cBit31_0
#define cThaRegMapBERTMonLossBitCounterLoadingShift    0


/* DEMAP MONITORING */
#define cThaRegDemapBERTMonSelectedChannel             0x1A4200
#define cThaRegDemapBERTMonRxPtgEnMask                 cBit9
#define cThaRegDemapBERTMonRxPtgEnShift                9

#define cThaRegDemapBERTMonRxPtgIdMask                 cBit8_0
#define cThaRegDemapBERTMonRxPtgIdShift                0

#define cThaRegDemapBERTMonNxDs0ConcateControl         0x1A41C0

#define cThaRegDemapBERTMonGlobalControl               0x1A4100
#define cThaRegDemapBERTMonRxPtmLosThrMask             cBit11_8
#define cThaRegDemapBERTMonRxPtmLosThrShift            8

#define cThaRegDemapBERTMonRxPtmSynThrMask             cBit7_4
#define cThaRegDemapBERTMonRxPtmSynThrShift            4

#define cThaRegDemapBERTMonStickyEnable                0x1A4101
#define cThaRegDemapBERTMonRxPtmStkEnMask(engineId)    (cBit0 << (engineId))
#define cThaRegDemapBERTMonRxPtmStkEnShift(engineId)   (engineId)

#define cThaRegDemapBERTMonSticky                      0x1A4102
#define cThaRegDemapBERTMonRxPtmStkMask(engineId)     (cBit0 << (engineId))
#define cThaRegDemapBERTMonRxPtmStkShift(engineId)    (engineId)

#define cThaRegDemapBERTMonMode                        0x1A4110
#define cThaRegDemapBERTMonRxPtmSwapMask               cBit11
#define cThaRegDemapBERTMonRxPtmSwapShift              11
#define cThaRegDemapBERTMonRxPtmInvMask                cBit10
#define cThaRegDemapBERTMonRxPtmInvShift               10
#define cThaRegDemapBERTMonRxPtmPatBitMask             cBit9_5
#define cThaRegDemapBERTMonRxPtmPatBitShift            5
#define cThaRegDemapBERTMonRxPtgModeMask               cBit4_0
#define cThaRegDemapBERTMonRxPtgModeShift              0


#define cThaRegDemapBERTMonFixedPatternControl         0x1A4120
#define cThaRegDemapBERTMonRxFixPatMask                cBit31_0
#define cThaRegDemapBERTMonRxFixPatShift               0

#define cThaRegDemapBERTMonErrorCounter                0x1A4160
#define cThaRegDemapBERTMonErrorCounterMask            cBit31_0
#define cThaRegDemapBERTMonErrorCounterShift           0

#define cThaRegDemapBERTMonGoodBitCounter              0x1A4180
#define cThaRegDemapBERTMonGoodBitCounterMask          cBit31_0
#define cThaRegDemapBERTMonGoodBitCounterShift         0

#define cThaRegDemapBERTMonLossBitCounter              0x1A41A0
#define cThaRegDemapBERTMonLossBitCounterMask          cBit31_0
#define cThaRegDemapBERTMonLossBitCounterShift         0

#define cThaRegDemapBERTMonCounterLoadId               0x1A4103
#define cThaRegDemapBERTMonRxPtmLoadIDMask             cBit3_0
#define cThaRegDemapBERTMonRxPtmLoadIDShift            0

#define cThaRegDemapBERTMonErrorCounterLoading         0x1A4104
#define cThaRegDemapBERTMonErrorCounterLoadingMask     cBit31_0
#define cThaRegDemapBERTMonErrorCounterLoadingShift    0

#define cThaRegDemapBERTMonGoodBitCounterLoading       0x1A4105
#define cThaRegDemapBERTMonGoodBitCounterLoadingMask   cBit31_0
#define cThaRegDemapBERTMonGoodBitCounterLoadingShift  0

#define cThaRegDemapBERTMonLossBitCounterLoading       0x1A4106
#define cThaRegDemapBERTMonLossBitCounterLoadingMask   cBit31_0
#define cThaRegDemapBERTMonLossBitCounterLoadingShift  0

/* BERT status */
#define cThaRegMapBERTMonStatus                        0x1B8440
#define cThaRegDemapBERTMonStatus                      0x1A4140

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPRBSREG_H_ */


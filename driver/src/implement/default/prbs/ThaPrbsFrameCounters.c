/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsFrameCounters.c
 *
 * Created Date: Oct 8, 2018
 *
 * Description : Default framed PRBS counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPrbsEngine.h"
#include "../../../generic/man/AtDriverInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaPrbsCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtPrbsCountersMethods   m_AtPrbsCountersOverride;

/* Save super implementation */
static const tAtObjectMethods          *m_AtObjectMethods = NULL;
static const tAtPrbsCountersMethods    *m_AtPrbsCountersMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint64 *CounterAddress(AtPrbsCounters self, uint16 counterType)
    {
    ThaPrbsFrameCounters counters = AtCast(ThaPrbsFrameCounters, self);
    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame:       return &counters->txFrame;
        case cAtPrbsEngineCounterRxFrame:       return &counters->rxFrame;
        case cAtPrbsEngineCounterRxErrorFrame:  return &counters->rxErrorFrame;
        default:
            return m_AtPrbsCountersMethods->CounterAddress(self, counterType);
        }
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(txFrame);
    mEncodeNone(rxFrame);
    mEncodeNone(txErrorFrame);
    }

static void OverrideAtPrbsCounters(ThaPrbsCounters self)
    {
    AtPrbsCounters counters = (AtPrbsCounters)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsCountersMethods = mMethodsGet(counters);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsCountersOverride, mMethodsGet(counters), sizeof(m_AtPrbsCountersOverride));

        mMethodOverride(m_AtPrbsCountersOverride, CounterAddress);
        }

    mMethodsSet(counters, &m_AtPrbsCountersOverride);
    }

static void OverrideAtObject(ThaPrbsCounters self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPrbsCounters self)
    {
    OverrideAtPrbsCounters(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsFrameCounters);
    }

ThaPrbsCounters ThaPrbsFrameCountersObjectInit(ThaPrbsCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsCountersObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsCounters ThaPrbsFrameCountersNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPrbsCounters self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ThaPrbsFrameCountersObjectInit(self);
    }


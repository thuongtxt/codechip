/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaModulePrbs.c
 *
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../pdh/ThaPdhDe1.h"
#include "../pdh/ThaPdhDe3.h"
#include "../pdh/ThaPdhNxDs0.h"
#include "../sdh/ThaSdhVc.h"
#include "../man/ThaDevice.h"
#include "ThaModulePrbsInternal.h"
#include "ThaModulePrbsReg.h"
#include "ThaPrbsEngine.h"
#include "ThaPrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePrbs)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtModuleMethods      m_AtModuleOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tThaModulePrbsMethods m_ThaModulePrbsOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine CachePrbsEngine(ThaModulePrbs self, uint32 engineId, AtPrbsEngine engine)
    {
    if (self->engines == NULL)
        {
        uint32 memSize = AtModulePrbsMaxNumEngines((AtModulePrbs)self) * sizeof(engine);
        self->engines = AtOsalMemAlloc(memSize);

        if (self->engines == NULL)
            return NULL;
        AtOsalMemInit(self->engines, 0, memSize);
        }

    self->engines[engineId] = engine;
    return engine;
    }

static AtPrbsEngine HoVcCachePrbsEngine(ThaModulePrbs self, uint32 engineId, AtPrbsEngine engine)
    {
    return mMethodsGet(self)->CachePrbsEngine(self, engineId, engine);
    }

static eBool EngineIdIsValid(AtModulePrbs self, uint32 engineId)
    {
    return (engineId < AtModulePrbsMaxNumEngines(self)) ? cAtTrue : cAtFalse;
    }

static eBool EngineIdIsValidForCreating(AtModulePrbs self, uint32 engineId)
    {
    if (!EngineIdIsValid(self, engineId))
        return cAtFalse;

    if (AtModulePrbsEngineGet(self, engineId) != NULL)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 HwEngineId(ThaModulePrbs self, uint32 engineId, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(channel);
    return engineId;
    }

static eAtRet ResourceCheck(ThaModulePrbs self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtOk;
    }

static eAtRet HoVcResourceCheck(ThaModulePrbs self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtOk;
    }

static eBool SquelchingCondtionShouldApply(AtChannel self)
    {
    ThaModulePw modulePw = (ThaModulePw)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePw);
    AtPw pw = AtChannelBoundPwGet(self);
    if (ThaModulePwShouldHandleCircuitAisForcingOnEnabling(modulePw) && pw && !AtChannelIsEnabled((AtChannel)pw))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DoSquelching(AtChannel self)
    {
    if (SquelchingCondtionShouldApply(self))
        return AtChannelTxSquelch(self, cAtTrue);

    return cAtOk;
    }

static eAtRet UndoSquelching(AtChannel self)
    {
    if (SquelchingCondtionShouldApply(self))
        return AtChannelTxSquelch(self, cAtFalse);

    return cAtOk;
    }

static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtPrbsEngine engine;
    eAtRet ret;

    if (!EngineIdIsValidForCreating(self, engineId))
        return NULL;

    if (AtChannelPrbsEngineGet((AtChannel)de1))
        return NULL;

    if ((AtPdhDe1Ds0MaskGet(de1) != 0) && !AtModulePrbsChannelizedPrbsAllowed((AtModulePrbs)self))
        {
        AtChannelLog((AtChannel)de1, cAtLogLevelCritical, AtSourceLocation, "Can not create PRBS engine because DE1 has NxDs0\r\n");
        return NULL;
        }

    ret = mMethodsGet(mThis(self))->ResourceCheck(mThis(self), (AtChannel)de1);
    if (ret != cAtOk)
        return NULL;

    engine = mMethodsGet(mThis(self))->De1PrbsEngineObjectCreate(mThis(self), engineId, de1);
    AtChannelPrbsEngineSet((AtChannel)de1, engine);
    mMethodsGet(mThis(self))->CachePrbsEngine(mThis(self), engineId, engine);

    if (engine)
        {
        AtPrbsEngineInit(engine);
        UndoSquelching((AtChannel)de1);
        }

    return engine;
    }

static eBool CanCreateDe3ChannelizedPrbsEngine(AtModulePrbs self, AtPdhDe3 de3)
    {
    uint16 frameType;

    if (!AtModulePrbsChannelizedPrbsAllowed((AtModulePrbs)self))
        return cAtFalse;

    /* M13 does not have corresponding un-channelized mode */
    frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de3);
    if ((frameType == cAtPdhDs3FrmM13Chnl28Ds1s) ||
        (frameType == cAtPdhDs3FrmM13Chnl21E1s))
        return cAtFalse;

    return cAtTrue;
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtPrbsEngine engine;
    eAtRet ret;

    if (!EngineIdIsValidForCreating(self, engineId))
        return NULL;

    if (AtChannelPrbsEngineGet((AtChannel)de3))
        return NULL;

    if (AtPdhDe3IsChannelized(de3) && !CanCreateDe3ChannelizedPrbsEngine(self, de3))
        {
        AtChannelLog((AtChannel)de3, cAtLogLevelWarning, AtSourceLocation,
                     "Can not create PRBS engine because frame type of DS3/E3 is "
                     "channelized or it does not have corresponding un-channelized mode\r\n");
        return NULL;
        }

    ret = mMethodsGet(mThis(self))->ResourceCheck(mThis(self), (AtChannel)de3);
    if (ret != cAtOk)
        return NULL;

    engine = mMethodsGet(mThis(self))->De3PrbsEngineObjectCreate(mThis(self), engineId, de3);
    mMethodsGet(mThis(self))->CachePrbsEngine(mThis(self), engineId, engine);
    AtChannelPrbsEngineSet((AtChannel)de3, engine);

    if (engine)
        {
        AtPrbsEngineInit(engine);
        UndoSquelching((AtChannel)de3);
        }

    return engine;
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtPrbsEngine engine;
    eAtRet ret;

    if (!EngineIdIsValidForCreating(self, engineId))
        return NULL;

    if (AtChannelPrbsEngineGet((AtChannel)nxDs0))
        return NULL;

    ret = mMethodsGet(mThis(self))->ResourceCheck(mThis(self), (AtChannel)nxDs0);
    if (ret != cAtOk)
        return NULL;

    engine = mMethodsGet(mThis(self))->NxDs0PrbsEngineObjectCreate(mThis(self), engineId, nxDs0);
    mMethodsGet(mThis(self))->CachePrbsEngine(mThis(self), engineId, engine);
    AtChannelPrbsEngineSet((AtChannel)nxDs0, engine);

    if (engine)
        {
        AtPrbsEngineInit(engine);
        UndoSquelching((AtChannel)nxDs0);
        }

    return engine;
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtRet ret;
    eBool isHoVc;
    AtPrbsEngine engine = NULL;
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);

    if (!EngineIdIsValidForCreating(self, engineId))
        return NULL;

    if (AtChannelPrbsEngineGet((AtChannel)sdhVc))
        return NULL;

    isHoVc = AtSdhChannelIsHoVc(sdhVc);

    /* There is no product support BERT for channelized HO VC */
    if (isHoVc && AtSdhChannelIsChannelizedVc(sdhVc) && (!AtModulePrbsChannelizedPrbsAllowed(self)))
        return NULL;

    if (isHoVc)
        ret = mMethodsGet(mThis(self))->HoVcResourceCheck(mThis(self), (AtChannel)sdhVc);
    else
        ret = mMethodsGet(mThis(self))->ResourceCheck(mThis(self), (AtChannel)sdhVc);

    if (ret != cAtOk)
        return NULL;

    if (isHoVc)
        {
        engine = mMethodsGet(mThis(self))->AuVcPrbsEngineObjectCreate(mThis(self), engineId, sdhVc);
        mMethodsGet(mThis(self))->HoVcCachePrbsEngine(mThis(self), engineId, engine);
        }
    else if (channelType == cAtSdhChannelTypeVc3)
        {
        engine = mMethodsGet(mThis(self))->Tu3VcPrbsEngineObjectCreate(mThis(self), engineId, sdhVc);
        mMethodsGet(mThis(self))->CachePrbsEngine(mThis(self), engineId, engine);
        }
    else if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
        {
        /* if VC1x is mapping to De1, Create PRBS at VC1x may risk to handle many logic at current design code
         * To make easy, just reject when VC1x have map De1 */
        if ((AtSdhChannelMapTypeGet(sdhVc) != cAtSdhVcMapTypeVc1xMapC1x) && (!AtModulePrbsChannelizedPrbsAllowed(self)))
            {
            AtChannelLog((AtChannel)sdhVc, cAtLogLevelCritical, AtSourceLocation, "Can not create PRBS engine because VC1x is mapping to DS1/E1\r\n");
            return NULL;
            }

        engine = mMethodsGet(mThis(self))->Vc1xPrbsEngineObjectCreate(mThis(self), engineId, sdhVc);
        mMethodsGet(mThis(self))->CachePrbsEngine(mThis(self), engineId, engine);
        }

    AtChannelPrbsEngineSet((AtChannel)sdhVc, engine);

    if (engine)
        {
        AtPrbsEngineInit(engine);
        UndoSquelching((AtChannel)sdhVc);
        }

    return engine;
    }

static AtPrbsEngine PwPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPw pw)
    {
    AtPrbsEngine engine = mMethodsGet(mThis(self))->PwPrbsEngineObjectCreate(mThis(self), engineId, pw);
    if (engine)
        AtPrbsEngineInit(engine);

    return engine;
    }

static void PartDefaultSet(AtModule self, uint8 partId)
    {
    uint32 regVal;
    uint32 regAddr;
    ThaModulePrbs module = (ThaModulePrbs)self;

    mModuleHwWrite(self, ThaRegMapBERTMonStickyEnable(module, 0, partId), 0);
    mModuleHwWrite(self, ThaRegDemapBERTMonStickyEnable(module, 0, partId), 0);

    regVal = 0;
    regAddr = ThaRegMapBERTMonGlobalControl(module, 0, partId);
    mRegFieldSet(regVal, cThaRegMapBERTMonTxPtmLosThr, 0x2);
    mRegFieldSet(regVal, cThaRegMapBERTMonTxPtmSynThr, 0x2);
    mModuleHwWrite(self, regAddr, regVal);

    regVal = 0;
    regAddr = ThaRegDemapBERTMonGlobalControl(module, 0, partId);
    mRegFieldSet(regVal, cThaRegDemapBERTMonRxPtmLosThr, 0x2);
    mRegFieldSet(regVal, cThaRegDemapBERTMonRxPtmSynThr, 0x2);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void DefaultSet(AtModule self)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModulePrbs); part_i++)
        PartDefaultSet(self, part_i);
    }

static void EngineRegisterInit(ThaModulePrbs self, uint8 partId, uint32 engineId)
    {
    /* Disable Tx Engine */
    uint32 regAddr = ThaRegMapBERTGenSelectedChannel(self, engineId, partId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 mask = ThaRegMapBERTGenTxPtgEnMask(self);
    uint32 shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, 0);
    mask = ThaRegMapBERTGenTxPtgIdMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, cInvalidUint32);
    mModuleHwWrite(self, regAddr, regVal);

    /* Disable Tx Error */
    regAddr = ThaRegMapBERTGenErrorRateInsert(self, engineId, partId);
    regVal  = mModuleHwRead(self, regAddr);
    mask    = ThaRegMapBERTGenTxBerErrMask(self);
    shift   = ThaRegMapBERTGenTxBerErrShift(self);
    mFieldIns(&regVal, mask, shift, 0);
    mModuleHwWrite(self, regAddr, regVal);
    }

static void PartRegisterInit(AtModule self, uint8 partId)
    {
    uint32 engine_i;
    uint32 numbOfEngines = AtModulePrbsMaxNumEngines((AtModulePrbs)self);

    for (engine_i = 0; engine_i < numbOfEngines; engine_i++)
        mMethodsGet(mThis(self))->EngineRegisterInit(mThis(self), partId, engine_i);
    }

static void RegisterInit(AtModule self)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModulePrbs); part_i++)
        PartRegisterInit(self, part_i);
    }

static eAtRet HoPrbsEngineRegisterInit(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    DefaultSet(self);
    RegisterInit(self);

    return mMethodsGet(mThis(self))->HoPrbsEngineRegisterInit(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "Total 16 engines: vc3, vc4, vc1x, de1, de3, nxds0";
    }

static void StatusClear(AtModule self)
    {
    uint32 engineId;

    m_AtModuleMethods->StatusClear(self);

    if (mThis(self)->engines == NULL)
        return;

    for (engineId = 0; engineId < AtModulePrbsMaxNumEngines((AtModulePrbs)self); engineId++)
        {
        if (mThis(self)->engines[engineId])
            AtPrbsEngineStatusClear(mThis(self)->engines[engineId]);
        }
    }

static uint32 MaxNumEngines(AtModulePrbs self)
    {
    AtUnused(self);
    return 16;
    }

static AtPrbsEngine PrbsEngineGet(AtModulePrbs self, uint32 engineId)
    {
    if (!EngineIdIsValid(self, engineId))
        return NULL;

    return mThis(self)->engines ? mThis(self)->engines[engineId] : NULL;
    }

static void PrbsEngineCachedRemove(ThaModulePrbs self, uint32 engineId)
    {
    if (self->engines == NULL)
        return;

    if (self->engines[engineId] == NULL)
        return;

    self->engines[engineId] = NULL;
    }

static eAtRet EngineDelete(AtModulePrbs self, uint32 engineId)
    {
    AtChannel channel;
    AtPrbsEngine engine = AtModulePrbsEngineGet(self, engineId);

    if (engine == NULL)
        return cAtErrorNullPointer;

    channel = AtPrbsEngineChannelGet(engine);
    DoSquelching(channel);
    AtChannelPrbsEngineSet(channel, NULL);

    /* Try to disable and unforce engine before deleting */
    AtPrbsEngineHwCleanup(engine);
    AtObjectDelete((AtObject)engine);
    PrbsEngineCachedRemove(mThis(self), engineId);

    return cAtOk;
    }

static eAtRet AllPrbsEngineDelete(ThaModulePrbs self)
    {
    uint32 engineId;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->engines == NULL)
        return cAtOk;

    /* Delete each PRBS engine */
    for (engineId = 0; engineId < AtModulePrbsMaxNumEngines((AtModulePrbs)self); engineId++)
        {
        if (self->engines[engineId])
            AtModulePrbsEngineDelete((AtModulePrbs)self, engineId);
        }

    /* Delete memory that holds all engine Objects */
    mMethodsGet(osal)->MemFree(osal, self->engines);
    self->engines = NULL;

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    /* Delete private data */
    AllPrbsEngineDelete((ThaModulePrbs)self);

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static eAtRet EngineObjectDelete(AtModulePrbs self, AtPrbsEngine engine)
    {
    if (engine == NULL)
        return cAtOk;

    if (mThis(self)->engines)
        mThis(self)->engines[AtPrbsEngineIdGet(engine)] = NULL;

    AtPrbsEngineEnable(engine, cAtFalse);
    AtPrbsEngineErrorForce(engine, cAtFalse);
    ThaPrbsEngineResourceRemove((ThaPrbsEngine)engine);
    ThaPrbsEngineTxChannelIdSet((ThaPrbsEngine)engine, cInvalidUint32);
    AtObjectDelete((AtObject)engine);
    return cAtOk;
    }

static uint32 PrbsPwPdaDefaultOffset(ThaModulePrbs self, AtPrbsEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    return 0x0;
    }

static uint32 PsnChannelId(ThaModulePrbs self, AtPrbsEngine engine)
    {
    AtChannel channel = AtPrbsEngineChannelGet(engine);
    AtPw pw = AtChannelBoundPwGet(channel);
    AtUnused(self);
    if (pw == NULL)
        return 0;

    return AtChannelIdGet((AtChannel)pw);
    }

static AtPrbsEngine NxDs0PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    return ThaPrbsEngineNxDs0New(nxDs0, engineId);
    }

static AtPrbsEngine De1PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    return ThaPrbsEngineDe1New(de1, engineId);
    }

static AtPrbsEngine De3PrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    return ThaPrbsEngineDe3New(de3, engineId);
    }

static AtPrbsEngine AuVcPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return ThaPrbsEngineAuVcNew(sdhVc, engineId);
    }

static AtPrbsEngine Tu3VcPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return ThaPrbsEngineAuVcNew(sdhVc, engineId);
    }

static AtPrbsEngine Vc1xPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return ThaPrbsEngineVc1xNew(sdhVc, engineId);
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtPw pw)
    {
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(pw);
    return NULL;
    }

static uint32 ForceEnableValue(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    return 1;
    }

static eBool HoVcPrbsIsSupported(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MaxNumHoPrbsEngine(ThaModulePrbs self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxNumLoPrbsEngine(ThaModulePrbs self)
    {
    AtUnused(self);
    return AtModulePrbsMaxNumEngines((AtModulePrbs)self);
    }

static void ResourceRemove(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    }

static uint32 ErrorRateSw2Hw(ThaModulePrbs self, eAtBerRate errorRate)
    {
    AtUnused(self);

    if (errorRate == cAtBerRate1E3)
        return 1;
    if (errorRate == cAtBerRate1E4)
        return 2;
    if (errorRate == cAtBerRate1E5)
        return 3;
    if (errorRate == cAtBerRate1E6)
        return 4;
    if (errorRate == cAtBerRate1E7)
        return 5;
    if (errorRate == cAtBerRate1E8)
        return 6;
    if (errorRate == cAtBerRate1E9)
        return 7;

    return 0;
    }

static eAtBerRate ErrorRateHw2Sw(ThaModulePrbs self, uint32 errorRate)
    {
    AtUnused(self);

    if (errorRate == 1)
        return cAtBerRate1E3;
    if (errorRate == 2)
        return cAtBerRate1E4;
    if (errorRate == 3)
        return cAtBerRate1E5;
    if (errorRate == 4)
        return cAtBerRate1E6;
    if (errorRate == 5)
        return cAtBerRate1E7;
    if (errorRate == 6)
        return cAtBerRate1E8;
    if (errorRate == 7)
        return cAtBerRate1E9;

    return cAtBerRateUnknown;
    }

static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    AtUnused(self);
    return ThaPrbsRegProviderDefaultProvider();
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModulePrbs object = (ThaModulePrbs)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(engines, AtModulePrbsMaxNumEngines((AtModulePrbs)self));
    mEncodeNone(regProvider);
    }

static eBool De3InvertModeShouldSwap(ThaModulePrbs self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return cAtFalse;
    }

static eBool PwPrbsIsSupported(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void HwEngineIdInUseSet(ThaModulePrbs self, ThaPrbsEngine engine, uint32 hwEngineId)
    {
    AtUnused(self);
    ThaPrbsEngineHwEngineIdSet(engine, hwEngineId);
    }

static eBool PrbsCounterReadIndirectly(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    return cAtTrue;
    }

static eBool ErrorForcingRateIsSupported(ThaModulePrbs self, eAtBerRate errorRate)
    {
    AtUnused(self);
    if (errorRate == cAtBerRateUnknown)
        return cAtFalse;

    if (errorRate == cAtBerRate1E2)
        return cAtFalse;

    if (errorRate > cAtBerRate1E9)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 GroupIdGet(ThaModulePrbs self, uint32 engineId)
    {
    AtUnused(self);
    AtUnused(engineId);
    return 0;
    }

static uint32 LocalIdGet(ThaModulePrbs self, uint32 engineId)
    {
    AtUnused(self);
    return engineId;
    }

static eBool PrbsEngineIsCommonMonSide(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PrbsEngineMonSideSet(ThaModulePrbs self, ThaPrbsEngine engine, eAtPrbsSide side)
    {
    AtUnused(self);
    AtUnused(engine);
    AtUnused(side);
    return cAtOk;
    }

static eAtPrbsSide PrbsEngineMonSideGet(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    return cAtPrbsSideUnknown;
    }

static AtPrbsEngine EthPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(port);
    return NULL;
    }

static AtPrbsEngine EthPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtEthPort port)
    {
    AtPrbsEngine engine;

    if (!EngineIdIsValidForCreating(self, engineId))
        return NULL;

    if (AtChannelPrbsEngineGet((AtChannel)port))
        return NULL;

    engine = mMethodsGet(mThis(self))->EthPrbsEngineObjectCreate(mThis(self), engineId, port);
    if (engine == NULL)
        return NULL;

    mMethodsGet(mThis(self))->CachePrbsEngine(mThis(self), engineId, engine);
    AtChannelPrbsEngineSet((AtChannel)port, engine);
    AtPrbsEngineInit(engine);

    return engine;
    }

static eBool DynamicAllocation(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FlexiblePsnSide(ThaModulePrbs self)
    {
    /* So far, applications have to handle this */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldResetFixPattern(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void HwFlush(ThaModulePrbs self)
    {
    AtUnused(self);
    }

static void MethodsInit(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_ThaModulePrbsOverride, PrbsPwPdaDefaultOffset);
        mMethodOverride(m_ThaModulePrbsOverride, PsnChannelId);
        mMethodOverride(m_ThaModulePrbsOverride, ForceEnableValue);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcPrbsIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, HwEngineId);
        mMethodOverride(m_ThaModulePrbsOverride, CachePrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcCachePrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, ResourceCheck);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcResourceCheck);
        mMethodOverride(m_ThaModulePrbsOverride, MaxNumHoPrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, MaxNumLoPrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, ResourceRemove);
        mMethodOverride(m_ThaModulePrbsOverride, ErrorRateSw2Hw);
        mMethodOverride(m_ThaModulePrbsOverride, ErrorRateHw2Sw);
        mMethodOverride(m_ThaModulePrbsOverride, ErrorForcingRateIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        mMethodOverride(m_ThaModulePrbsOverride, EngineRegisterInit);
        mMethodOverride(m_ThaModulePrbsOverride, HwEngineIdInUseSet);
        mMethodOverride(m_ThaModulePrbsOverride, DynamicAllocation);
        mMethodOverride(m_ThaModulePrbsOverride, FlexiblePsnSide);
        mMethodOverride(m_ThaModulePrbsOverride, ShouldResetFixPattern);

        /* Internal PRBS engine */
        mMethodOverride(m_ThaModulePrbsOverride, NxDs0PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, De1PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, De3PrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, AuVcPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, Tu3VcPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, Vc1xPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, EthPrbsEngineObjectCreate);

        mMethodOverride(m_ThaModulePrbsOverride, De3InvertModeShouldSwap);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, HoPrbsEngineRegisterInit);

        /* Counter read directly or loadId */
        mMethodOverride(m_ThaModulePrbsOverride, PrbsCounterReadIndirectly);

        /* To manage GroupId */
        mMethodOverride(m_ThaModulePrbsOverride, GroupIdGet);
        mMethodOverride(m_ThaModulePrbsOverride, LocalIdGet);
        mMethodOverride(m_ThaModulePrbsOverride, HwFlush);

        /* TDM mon and PSN mon are the same register */
        mMethodOverride(m_ThaModulePrbsOverride, PrbsEngineIsCommonMonSide);
        mMethodOverride(m_ThaModulePrbsOverride, PrbsEngineMonSideSet);
        mMethodOverride(m_ThaModulePrbsOverride, PrbsEngineMonSideGet);
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void OverrideAtObject(AtModulePrbs self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NxDs0PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, PwPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, EthPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, MaxNumEngines);
        mMethodOverride(m_AtModulePrbsOverride, PrbsEngineGet);
        mMethodOverride(m_AtModulePrbsOverride, EngineDelete);
        mMethodOverride(m_AtModulePrbsOverride, EngineObjectDelete);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePrbs);
    }

AtModulePrbs ThaModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaModulePrbs)self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs ThaModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePrbsObjectInit(newModule, device);
    }

uint32 ThaModulePrbsPsnChannelId(ThaModulePrbs self, AtPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->PsnChannelId(self, engine);
    return 0;
    }

ThaPrbsRegProvider ThaModulePrbsRegProvider(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->RegProvider(self);
    return NULL;
    }

uint32 ThaRegMapBERTGenSelectedChannel(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTGenSelectedChannel(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTGenNxDs0ConcateControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTGenNxDs0ConcateControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTGenMode(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTGenMode(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTGenFixedPatternControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTGenFixedPatternControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTGenErrorRateInsert(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTGenErrorRateInsert(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTGenSingleBitErrInsert(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTGenSingleBitErrInsert(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTGenGoodBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTGenGoodBitCounter(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonSelectedChannel(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonSelectedChannel(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonNxDs0ConcateControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonNxDs0ConcateControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonGlobalControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonGlobalControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonStickyEnable(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonStickyEnable(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonSticky(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonSticky(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonMode(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonMode(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonFixedPatternControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonFixedPatternControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonErrorCounter(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonErrorCounter(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonGoodBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonGoodBitCounter(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonLossBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonLossBitCounter(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonCounterLoadId(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonCounterLoadId(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonErrorCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonErrorCounterLoading(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonGoodBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonGoodBitCounterLoading(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTMonLossBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonLossBitCounterLoading(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonSelectedChannel(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonSelectedChannel(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonNxDs0ConcateControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonNxDs0ConcateControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonGlobalControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonGlobalControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonStickyEnable(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonStickyEnable(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonSticky(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonSticky(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonMode(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonMode(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonFixedPatternControl(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonFixedPatternControl(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonErrorCounter(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonErrorCounter(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonGoodBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonGoodBitCounter(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonLossBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonLossBitCounter(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonCounterLoadId(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonCounterLoadId(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonErrorCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonErrorCounterLoading(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonGoodBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonGoodBitCounterLoading(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonLossBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonLossBitCounterLoading(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegMapBERTGenTxPtgEnMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxPtgEnMask(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegMapBERTGenTxPtgIdMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxPtgIdMask(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegDemapBERTMonRxPtgEnMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegDemapBERTMonRxPtgEnMask(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegDemapBERTMonRxPtgIdMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegDemapBERTMonRxPtgIdMask(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegMapBERTMonTxPtgEnMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTMonTxPtgEnMask(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegMapBERTMonTxPtgIdMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTMonTxPtgIdMask(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegMapBERTMonSideSelectMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTMonSideSelectMask(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegMapBERTGenTxBerMdMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxBerMdMask(ThaModulePrbsRegProvider(self), self);
    }

uint8 ThaRegMapBERTGenTxBerMdShift(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxBerMdShift(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegMapBERTGenTxBerErrMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxBerErrMask(ThaModulePrbsRegProvider(self), self);
    }

uint8 ThaRegMapBERTGenTxBerErrShift(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxBerErrShift(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegMapBERTGenTxSingleErrMask(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxSingleErrMask(ThaModulePrbsRegProvider(self), self);
    }

uint8 ThaRegMapBERTGenTxSingleErrShift(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegMapBERTGenTxSingleErrShift(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaModulePrbsForceEnableValue(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->ForceEnableValue(self, engine);
    return 0;
    }

uint32 ThaRegPdaPwPrbsGenCtrl(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegPdaPwPrbsGenCtrl(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaRegPdaPwPrbsMonCtrl(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderRegPdaPwPrbsMonCtrl(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaPrbsPwPdaDefaultOffset(ThaModulePrbs self, AtPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->PrbsPwPdaDefaultOffset(self, engine);
    return 0;
    }

eBool ThaModulePrbsHoVcPrbsIsSupported(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->HoVcPrbsIsSupported(self);
    return cAtFalse;
    }

void ThaModulePrbsResourceRemove(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    if (self)
        mMethodsGet(self)->ResourceRemove(self, engine);
    }

uint32 ThaModulePrbsErrorRateSw2Hw(ThaModulePrbs self, eAtBerRate errorRate)
    {
    if (self)
        return mMethodsGet(self)->ErrorRateSw2Hw(self, errorRate);
    return 0;
    }

eAtBerRate ThaModulePrbsErrorRateHw2Sw(ThaModulePrbs self, uint32 errorRate)
    {
    if (self)
        return mMethodsGet(self)->ErrorRateHw2Sw(self, errorRate);
    return cAtBerRateUnknown;
    }

eBool ThaModulePrbsErrorForcingRateIsSupported(ThaModulePrbs self, eAtBerRate errorRate)
    {
    if (self)
        return mMethodsGet(self)->ErrorForcingRateIsSupported(self, errorRate);
    return cAtFalse;
    }

uint32 ThaRegMapBERTMonStatus(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegMapBERTMonStatus(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaRegDemapBERTMonStatus(ThaModulePrbs self, uint32 engineId, uint32 slice)
    {
    return ThaPrbsRegProviderRegDemapBERTMonStatus(ThaModulePrbsRegProvider(self), self, engineId, slice);
    }

uint32 ThaModulePrbsTdmChannelIdSliceFactor(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderTdmChannelIdSliceFactor(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaModulePrbsMonTdmChannelIdSliceFactor(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderMonTdmChannelIdSliceFactor(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaModulePrbsTdmChannelIdStsFactor(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderTdmChannelIdStsFactor(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaModulePrbsTdmChannelIdVtgFactor(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderTdmChannelIdVtgFactor(ThaModulePrbsRegProvider(self), self);
    }

uint32 ThaModulePrbsTdmChannelIdVtFactor(ThaModulePrbs self)
    {
    return ThaPrbsRegProviderTdmChannelIdVtFactor(ThaModulePrbsRegProvider(self), self);
    }

eBool ThaModulePrbsDe3InvertModeShouldSwap(ThaModulePrbs self, eAtPrbsMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->De3InvertModeShouldSwap(self, prbsMode);
    return cAtFalse;
    }

eBool ThaModulePrbsPwPrbsIsSupported(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PwPrbsIsSupported(self);
    return cAtFalse;
    }

uint32 ThaModulePrbsMaxNumLoPrbsEngine(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumLoPrbsEngine(self);
    return 0;
    }

uint32 ThaModulePrbsMaxNumHoPrbsEngine(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumHoPrbsEngine(self);
    return 0;
    }

void ThaModulePrbsHwEngineIdInUseSet(ThaModulePrbs self, ThaPrbsEngine engine, uint32 hwEngineId)
    {
    if (self)
        mMethodsGet(self)->HwEngineIdInUseSet(self, engine, hwEngineId);
    }

eBool ThaModulePrbsCounterReadIndirectly(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    if ((self) && (engine))
        return mMethodsGet(self)->PrbsCounterReadIndirectly(self, engine);
    return cAtFalse;
    }

uint32 ThaModulePrbsGroupIdGet(ThaModulePrbs self, uint32 engineId)
    {
    if (self)
        return mMethodsGet(self)->GroupIdGet(self, engineId);
    return 0;
    }

uint32 ThaModulePrbsLocalIdGet(ThaModulePrbs self, uint32 engineId)
    {
    if (self)
        return mMethodsGet(self)->LocalIdGet(self, engineId);
    return 0;
    }

eBool ThaModulePrbsFlexiblePsnSide(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->FlexiblePsnSide(self);
    return cAtFalse;
    }

eBool ThaModulePrbsShouldResetFixPattern(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->ShouldResetFixPattern(self);
    return cAtFalse;
    }

void ThaModulePrbsHwFlush(ThaModulePrbs self)
    {
    if (self)
        mMethodsGet(self)->HwFlush(self);
    }

eBool ThaModulePrbsEngineIsCommonMonSide(ThaModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsEngineIsCommonMonSide(self);
    return cAtFalse;
    }

eAtRet ThaModulePrbsEngineMonSideSet(ThaModulePrbs self, ThaPrbsEngine engine, eAtPrbsSide side)
    {
    if (self)
        return mMethodsGet(self)->PrbsEngineMonSideSet(self, engine, side);
    return cAtErrorNullPointer;
    }

eAtPrbsSide ThaModulePrbsEngineMonSideGet(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->PrbsEngineMonSideGet(self, engine);
    return cAtPrbsSideUnknown;
    }

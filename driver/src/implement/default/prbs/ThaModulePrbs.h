/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaModulePrbs.h
 * 
 * Created Date: Aug 23, 2013
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPRBS_H_
#define _THAMODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePrbs.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePrbs * ThaModulePrbs;
typedef struct tThaPrbsRegProvider * ThaPrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs ThaModulePrbsNew(AtDevice device);

ThaPrbsRegProvider ThaModulePrbsRegProvider(ThaModulePrbs self);
eBool ThaModulePrbsFlexiblePsnSide(ThaModulePrbs self);
eBool ThaModulePrbsShouldResetFixPattern(ThaModulePrbs self);
void ThaModulePrbsHwFlush(ThaModulePrbs self);

/* Product concretes */
AtModulePrbs Tha60210031ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60210031ModulePrbsV2New(AtDevice device);
AtModulePrbs Tha60210021ModulePrbsNew(AtDevice device);
AtModulePrbs Tha61210011ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60210011ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60210051ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60210012ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60210061ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60290011ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60290022ModulePrbsNew(AtDevice device);
AtModulePrbs Tha6A033111ModulePrbsNew(AtDevice device);
AtModulePrbs Tha6A290011ModulePrbsNew(AtDevice device);
AtModulePrbs Tha6A290021ModulePrbsNew(AtDevice device);
AtModulePrbs Tha6A290022ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60290022ModulePrbsV2New(AtDevice device);
AtModulePrbs Tha60291011ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60290051ModulePrbsNew(AtDevice device);
AtModulePrbs Tha60290081ModulePrbsNew(AtDevice device);
AtModulePrbs Tha6A290081ModulePrbsNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPRBS_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineDe1Internal.h
 * 
 * Created Date: May 1, 2017
 *
 * Description : DE1 PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINEDE1INTERNAL_H_
#define _THAPRBSENGINEDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPrbsEnginePdhChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineDe1
    {
    tThaPrbsEnginePdhChannel super;
    }tThaPrbsEngineDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1, uint32 engineId);
uint32 ThaPrbsEngineDe1HwIdGet(ThaPdhDe1 de1, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINEDE1INTERNAL_H_ */


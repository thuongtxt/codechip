/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngineNxDs0.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for NxDS0
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"
#include "../man/ThaDevice.h"
#include "ThaModulePrbsReg.h"
#include "ThaPrbsEngineNxDs0Internal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaSdhVc.h"
#include "../pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods   *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineNxDs0);
    }

static void Write(AtPrbsEngine self, uint32 regAddr, uint32 regVal)
    {
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    }

static ThaModulePrbs PrbsModule(AtPrbsEngine self)
    {
    return ThaPrbsEngineModuleGet(self);
    }

static uint32 TimeslotBitMap(ThaPrbsEngine self)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return AtPdhNxDS0BitmapGet(nxDs0);
    }

static void TimeslotDefaultSet(ThaPrbsEngine self, uint32 bitMap)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = PrbsModule(engine);

    Write(engine, ThaRegMapBERTGenNxDs0ConcateControl(module, engineId, slice), bitMap);
    Write(engine, ThaRegMapBERTMonNxDs0ConcateControl(module, engineId, slice), 1);
    Write(engine, ThaRegDemapBERTMonNxDs0ConcateControl(module, engineId, slice), bitMap);
    }

static eBool ShouldUpdateMapBERTMonNxDs0ConcateControl(AtPrbsEngine self)
    {
    return ThaModulePrbsEngineIsCommonMonSide(PrbsModule(self));
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPrbsEngineChannelGet(self);
    uint32 bitMap = AtPdhNxDS0BitmapGet(nxDs0);
    ThaModulePrbs module = PrbsModule(self);
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    eAtRet ret;

    ret = m_AtPrbsEngineMethods->MonitoringSideSet(self, side);

    if (!ShouldUpdateMapBERTMonNxDs0ConcateControl(self))
        return ret;

    /* TDM monitoring requires NxDS0 bit-map, but PSN monitoring requires
     * to enable timeslot #0 only */
    if (side == cAtPrbsSideTdm)
        Write(self, ThaRegMapBERTMonNxDs0ConcateControl(module, engineId, slice), bitMap);
    else
        Write(self, ThaRegMapBERTMonNxDs0ConcateControl(module, engineId, slice), 1);

    return ret;
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtPdhChannel de1 = (AtPdhChannel)AtPdhNxDS0De1Get(nxDs0);
    return ThaPrbsEngineDe1ChannelIdGet(self, (ThaPdhDe1)de1);
    }

static uint32 RxChannelId(ThaPrbsEngine self)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtPdhChannel de1 = (AtPdhChannel)AtPdhNxDS0De1Get(nxDs0);
    return ThaPrbsEngineDe1RxChannelIdGet(self, (ThaPdhDe1)de1);
    }

static uint8 HwSliceId(ThaPrbsEngine self)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhNxDS0De1Get(nxDs0);
    return ThaPdhDe1SliceGet(de1);
    }

static AtChannel CircuitForLoopbackHandling(ThaPrbsEngine self)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return (AtChannel)AtPdhNxDS0De1Get(nxds0);
    }

static uint8 CircuitLocalLoopbackMode(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAtPdhLoopbackModeLocalLine;
    }

static void OverrideAtPrbsEngine(ThaPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        }

    mMethodsSet(engine, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, HwSliceId);
        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotDefaultSet);
        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotBitMap);
        mMethodOverride(m_ThaPrbsEngineOverride, CircuitForLoopbackHandling);
        mMethodOverride(m_ThaPrbsEngineOverride, CircuitLocalLoopbackMode);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine((ThaPrbsEngine)self);
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

AtPrbsEngine ThaPrbsEngineNxDs0ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineObjectInit(self, (AtChannel)nxDs0, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine ThaPrbsEngineNxDs0New(AtPdhNxDS0 nxDs0, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaPrbsEngineNxDs0ObjectInit(newEngine, nxDs0, engineId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaModulePrbsInternal.h
 * 
 * Created Date: May 8, 2015
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPRBSINTERNAL_H_
#define _THAMODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "ThaPrbsEngine.h"
#include "ThaModulePrbs.h"
#include "ThaPrbsRegProvider.h"
#include "../util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThaModulePrbs(engine)  ThaModulePrbs ThaPrbsEngineModuleGet(engine)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePrbsMethods
    {
    AtPrbsEngine (*De3PrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtPdhDe3 de3);
    AtPrbsEngine (*De1PrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtPdhDe1 de1);
    AtPrbsEngine (*NxDs0PrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0);
    AtPrbsEngine (*AuVcPrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc);
    AtPrbsEngine (*Tu3VcPrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc);
    AtPrbsEngine (*Vc1xPrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc);
    AtPrbsEngine (*PwPrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtPw pw);
    AtPrbsEngine (*EthPrbsEngineObjectCreate)(ThaModulePrbs self, uint32 engineId, AtEthPort port);
    uint32 (*HwEngineId)(ThaModulePrbs self, uint32 engineId, AtChannel channel);
    ThaPrbsRegProvider (*RegProvider)(ThaModulePrbs self);
    eBool (*FlexiblePsnSide)(ThaModulePrbs self);
    eBool (*ShouldResetFixPattern)(ThaModulePrbs self);

    uint32 (*ForceEnableValue)(ThaModulePrbs self, ThaPrbsEngine engine);

    /* PSN side channel ID*/
    uint32 (*PsnChannelId)(ThaModulePrbs self, AtPrbsEngine engine);
    uint32 (*PrbsPwPdaDefaultOffset)(ThaModulePrbs self, AtPrbsEngine engine);

    eBool (*DynamicAllocation)(ThaModulePrbs self);
    eBool (*HoVcPrbsIsSupported)(ThaModulePrbs self);
    AtPrbsEngine (*CachePrbsEngine)(ThaModulePrbs self, uint32 engineId, AtPrbsEngine engine);
    AtPrbsEngine (*HoVcCachePrbsEngine)(ThaModulePrbs self, uint32 engineId, AtPrbsEngine engine);
    uint32 (*MaxNumHoPrbsEngine)(ThaModulePrbs self);
    uint32 (*MaxNumLoPrbsEngine)(ThaModulePrbs self);
    eAtRet (*ResourceCheck)(ThaModulePrbs self, AtChannel channel);
    eAtRet (*HoVcResourceCheck)(ThaModulePrbs self, AtChannel channel);
    void (*ResourceRemove)(ThaModulePrbs self, ThaPrbsEngine engine);
    uint32 (*ErrorRateSw2Hw)(ThaModulePrbs self, eAtBerRate errorRate);
    eAtBerRate (*ErrorRateHw2Sw)(ThaModulePrbs self, uint32 errorRate);
    eBool (*ErrorForcingRateIsSupported)(ThaModulePrbs self, eAtBerRate errorRate);

    eBool (*De3InvertModeShouldSwap)(ThaModulePrbs self, eAtPrbsMode prbsMode);
    eBool (*PwPrbsIsSupported)(ThaModulePrbs self);
    void (*EngineRegisterInit)(ThaModulePrbs self, uint8 partId, uint32 engineId);
    void (*HwEngineIdInUseSet)(ThaModulePrbs self, ThaPrbsEngine engine, uint32 hwEngineId);
    eAtRet (*HoPrbsEngineRegisterInit)(ThaModulePrbs self);

    /* Counters Load */
    eBool (*PrbsCounterReadIndirectly)(ThaModulePrbs self, ThaPrbsEngine engine);

    /* Local GroupId */
    uint32 (*GroupIdGet)(ThaModulePrbs self, uint32 engineId);
    uint32 (*LocalIdGet)(ThaModulePrbs self, uint32 engineId);

    /* Hw flush */
    void (*HwFlush)(ThaModulePrbs self);

    /* Option for TDM monitor and PSN monitor in the same HW register */
    eBool (*PrbsEngineIsCommonMonSide)(ThaModulePrbs self);
    eAtRet (*PrbsEngineMonSideSet)(ThaModulePrbs self, ThaPrbsEngine engine, eAtPrbsSide side);
    eAtPrbsSide (*PrbsEngineMonSideGet)(ThaModulePrbs self, ThaPrbsEngine engine);
    }tThaModulePrbsMethods;

typedef struct tThaModulePrbs
    {
    tAtModulePrbs super;
    const tThaModulePrbsMethods *methods;

    /* Private data */
    AtPrbsEngine *engines; /* To cached all created PRBS engines */
    ThaPrbsRegProvider regProvider;
    }tThaModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs ThaModulePrbsObjectInit(AtModulePrbs self, AtDevice device);
eBool ThaModulePrbsHoVcPrbsIsSupported(ThaModulePrbs self);
void ThaModulePrbsResourceRemove(ThaModulePrbs self, ThaPrbsEngine engine);
uint32 ThaModulePrbsErrorRateSw2Hw(ThaModulePrbs self, eAtBerRate errorRate);
eAtBerRate ThaModulePrbsErrorRateHw2Sw(ThaModulePrbs self, uint32 errorRate);
eBool ThaModulePrbsErrorForcingRateIsSupported(ThaModulePrbs self, eAtBerRate errorRate);

/* Register access */
uint32 ThaRegMapBERTGenSelectedChannel(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTGenNxDs0ConcateControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTGenMode(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTGenFixedPatternControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTGenErrorRateInsert(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTGenSingleBitErrInsert(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTGenGoodBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonSelectedChannel(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonNxDs0ConcateControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonGlobalControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonStickyEnable(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonSticky(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonMode(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonFixedPatternControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonErrorCounter(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonGoodBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonLossBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonCounterLoadId(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonErrorCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonGoodBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegMapBERTMonLossBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonSelectedChannel(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonNxDs0ConcateControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonGlobalControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonStickyEnable(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonSticky(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonMode(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonFixedPatternControl(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonErrorCounter(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonGoodBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonLossBitCounter(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonCounterLoadId(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonErrorCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonGoodBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonLossBitCounterLoading(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaModulePrbsPsnChannelId(ThaModulePrbs self, AtPrbsEngine engine);
uint32 ThaModulePrbsForceEnableValue(ThaModulePrbs self, ThaPrbsEngine engine);
uint32 ThaRegPdaPwPrbsGenCtrl(ThaModulePrbs self);
uint32 ThaRegPdaPwPrbsMonCtrl(ThaModulePrbs self);
uint32 ThaPrbsPwPdaDefaultOffset(ThaModulePrbs self, AtPrbsEngine engine);
uint32 ThaRegMapBERTMonStatus(ThaModulePrbs self, uint32 engineId, uint32 slice);
uint32 ThaRegDemapBERTMonStatus(ThaModulePrbs self, uint32 engineId, uint32 slice);

/* Register mask */
uint32 ThaRegMapBERTGenTxPtgEnMask(ThaModulePrbs self);
uint32 ThaRegMapBERTGenTxPtgIdMask(ThaModulePrbs self);
uint32 ThaRegDemapBERTMonRxPtgEnMask(ThaModulePrbs self);
uint32 ThaRegDemapBERTMonRxPtgIdMask(ThaModulePrbs self);
uint32 ThaRegMapBERTMonTxPtgEnMask(ThaModulePrbs self);
uint32 ThaRegMapBERTMonTxPtgIdMask(ThaModulePrbs self);
uint32 ThaRegMapBERTMonSideSelectMask(ThaModulePrbs self);

/* Register mask shift */
uint32 ThaRegMapBERTGenTxBerMdMask(ThaModulePrbs self);
uint8 ThaRegMapBERTGenTxBerMdShift(ThaModulePrbs self);
uint32 ThaRegMapBERTGenTxBerErrMask(ThaModulePrbs self);
uint8 ThaRegMapBERTGenTxBerErrShift(ThaModulePrbs self);
uint32 ThaRegMapBERTGenTxSingleErrMask(ThaModulePrbs self);
uint8 ThaRegMapBERTGenTxSingleErrShift(ThaModulePrbs self);

uint32 ThaModulePrbsTdmChannelIdSliceFactor(ThaModulePrbs self);
uint32 ThaModulePrbsTdmChannelIdStsFactor(ThaModulePrbs self);
uint32 ThaModulePrbsTdmChannelIdVtgFactor(ThaModulePrbs self);
uint32 ThaModulePrbsTdmChannelIdVtFactor(ThaModulePrbs self);
uint32 ThaModulePrbsMonTdmChannelIdSliceFactor(ThaModulePrbs self);

eBool ThaModulePrbsDe3InvertModeShouldSwap(ThaModulePrbs self, eAtPrbsMode prbsMode);
eBool ThaModulePrbsPwPrbsIsSupported(ThaModulePrbs self);

uint32 ThaModulePrbsMaxNumLoPrbsEngine(ThaModulePrbs self);
uint32 ThaModulePrbsMaxNumHoPrbsEngine(ThaModulePrbs self);
void ThaModulePrbsHwEngineIdInUseSet(ThaModulePrbs self, ThaPrbsEngine engine, uint32 hwEngineId);

/* To control read counter */
eBool ThaModulePrbsCounterReadIndirectly(ThaModulePrbs self, ThaPrbsEngine engine);

/* To manage groupId from EngineId */
uint32 ThaModulePrbsGroupIdGet(ThaModulePrbs self, uint32 engineId);
uint32 ThaModulePrbsLocalIdGet(ThaModulePrbs self, uint32 engineId);

/* HW used TDM mon and PSN mon the same register */
eBool ThaModulePrbsEngineIsCommonMonSide(ThaModulePrbs self);
eAtRet ThaModulePrbsEngineMonSideSet(ThaModulePrbs self, ThaPrbsEngine engine, eAtPrbsSide side);
eAtPrbsSide ThaModulePrbsEngineMonSideGet(ThaModulePrbs self, ThaPrbsEngine engine);

#endif /* _THAMODULEPRBSINTERNAL_H_ */


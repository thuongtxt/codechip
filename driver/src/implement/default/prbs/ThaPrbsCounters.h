/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsCounters.h
 * 
 * Created Date: Aug 17, 2018
 *
 * Description : Default PRBS counters
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSCOUNTERS_H_
#define _THAPRBSCOUNTERS_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsCounters * ThaPrbsCounters;
typedef struct tThaPrbsFrameCounters * ThaPrbsFrameCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPrbsCounters ThaPrbsCountersNew(void);
ThaPrbsCounters ThaPrbsFrameCountersNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSCOUNTERS_H_ */


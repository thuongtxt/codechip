/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngineSerdes.c
 *
 * Created Date: Nov 6, 2013
 *
 * Description : SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPrbsEngineSerdesInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPrbsEngineSerdes)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPrbsEngineSerdesMethods m_methods;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DiagnosticControlRegister(ThaPrbsEngineSerdes self)
    {
	AtUnused(self);
    /* Sub class must override */
    return cInvalidValue;
    }

static uint32 DiagnosticAlarmRegister(ThaPrbsEngineSerdes self)
    {
	AtUnused(self);
    /* Sub class must override */
    return cInvalidValue;
    }

static uint32 SerdesControllerId(ThaPrbsEngineSerdes self)
    {
    return (uint8)AtSerdesControllerIdGet(self->serdesController);
    }

static uint32 EnableMask(ThaPrbsEngineSerdes self)
    {
    uint32 serdesId = mMethodsGet(self)->SerdesControllerId(self);
    return cBit0 << (serdesId * 4);
    }

static uint32 EnableShift(ThaPrbsEngineSerdes self)
    {
    uint32 serdesId = mMethodsGet(self)->SerdesControllerId(self);
    return serdesId * 4;
    }

static uint32 AlignEnableMask(ThaPrbsEngineSerdes self)
    {
    uint32 serdesId = mMethodsGet(self)->SerdesControllerId(self);
    return cBit1 << (serdesId * 4);
    }

static uint32 AlignEnableShift(ThaPrbsEngineSerdes self)
    {
    uint32 serdesId = mMethodsGet(self)->SerdesControllerId(self);
    return (serdesId * 4) + 1;
    }

static uint32 ErrorForceMask(ThaPrbsEngineSerdes self)
    {
    uint32 serdesId = mMethodsGet(self)->SerdesControllerId(self);
    return cBit2 << (serdesId * 4);
    }

static uint32 ErrorForceShift(ThaPrbsEngineSerdes self)
    {
    uint32 serdesId = mMethodsGet(self)->SerdesControllerId(self);
    return (serdesId * 4) + 2;
    }

static uint32 ErrorStatusMask(ThaPrbsEngineSerdes self)
    {
    return cBit0 << mMethodsGet(self)->SerdesControllerId(self);
    }

static eAtModule ModuleId(AtPrbsEngine self)
    {
    AtModule module = AtChannelModuleGet(AtPrbsEngineChannelGet(self));
    return AtModuleTypeGet(module);
    }

static eAtModulePrbsRet AlignEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->DiagnosticControlRegister(mThis(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, ModuleId(self));

    mFieldIns(&regVal, AlignEnableMask(mThis(self)), AlignEnableShift(mThis(self)), enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, ModuleId(self));

    return cAtOk;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->DiagnosticControlRegister(mThis(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, ModuleId(self));

    mFieldIns(&regVal, EnableMask(mThis(self)), EnableShift(mThis(self)), enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, ModuleId(self));

    AlignEnable(self, enable);

    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->DiagnosticControlRegister(mThis(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, ModuleId(self));

    return (regVal & EnableMask(mThis(self))) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->DiagnosticControlRegister(mThis(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, ModuleId(self));

    mFieldIns(&regVal,
              ErrorForceMask(mThis(self)),
              ErrorForceShift(mThis(self)),
              force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, ModuleId(self));

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->DiagnosticControlRegister(mThis(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, ModuleId(self));

    return (regVal & ErrorForceMask(mThis(self))) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->DiagnosticAlarmRegister(mThis(self));
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, ModuleId(self));
    uint32 mask    = ErrorStatusMask(mThis(self));
    uint32 alarm   = (regVal & mask) ? cAtPrbsEngineAlarmTypeLossSync : 0;

    AtPrbsEngineWrite(self, regAddr, mask, ModuleId(self));

    return alarm;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
	AtUnused(self);
    if (prbsMode == cAtPrbsModePrbs15)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
	AtUnused(self);
    return cAtPrbsModePrbs15;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == cAtPrbsModePrbs15) ? cAtTrue : cAtFalse;
    }

static void MethodsInit(ThaPrbsEngineSerdes self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DiagnosticControlRegister);
        mMethodOverride(m_methods, DiagnosticAlarmRegister);
        mMethodOverride(m_methods, SerdesControllerId);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineSerdes);
    }

AtPrbsEngine ThaPrbsEngineSerdesObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, AtSerdesControllerPhysicalPortGet(serdesController), 0) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->serdesController = serdesController;

    return self;
    }

AtSerdesController ThaPrbsEngineSerdesController(ThaPrbsEngineSerdes self)
    {
    return self ? self->serdesController : NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsRegProvider.c
 *
 * Created Date: Jan 6, 2016
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../util/ThaUtil.h"
#include "ThaModulePrbsReg.h"
#include "ThaPrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPrbsRegProviderMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 EngineOffset(uint32 engineId, uint32 slice)
    {
    return engineId + (slice * 0x020000UL);
    }

static uint32 RegMapBERTGenTxBerMdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdMask;
    }

static uint8  RegMapBERTGenTxBerMdShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerMdShift;
    }

static uint32 RegMapBERTGenTxBerErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrMask;
    }

static uint8  RegMapBERTGenTxBerErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxBerErrShift;
    }

static uint32 RegMapBERTGenTxSingleErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxPtgErrIDInsMask;
    }

static uint8  RegMapBERTGenTxSingleErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxPtgErrIDInsShift;
    }

static uint32 RegMapBERTGenSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenSelectedChannel + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTGenNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenNxDs0ConcateControl + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTGenMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenMode + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTGenFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenFixedPatternControl + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenErrorRateInsert + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTGenSingleBitErrInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenSingleBitErrInsert + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTGenGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenGoodBitCounter + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonSelectedChannel + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonNxDs0ConcateControl + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGlobalControl + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonStickyEnable + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonSticky + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonMode + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonFixedPatternControl + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonErrorCounter + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGoodBitCounter + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonLossBitCounter + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonCounterLoadId + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonErrorCounterLoading + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonGoodBitCounterLoading + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonLossBitCounterLoading + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonSelectedChannel + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonNxDs0ConcateControl + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGlobalControl + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonStickyEnable + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonSticky + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonMode + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonFixedPatternControl + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonErrorCounter + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGoodBitCounter + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonLossBitCounter + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonCounterLoadId + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonErrorCounterLoading + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonGoodBitCounterLoading + EngineOffset(engineId, slice);
    }

static uint32 RegDemapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonLossBitCounterLoading + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTGenTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxPtgEnMask;
    }

static uint32 RegMapBERTGenTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenTxPtgIdMask;
    }

static uint32 RegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonRxPtgEnMask;
    }

static uint32 RegDemapBERTMonRxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonRxPtgIdMask;
    }

static uint32 RegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonTxPtgEnMask;
    }

static uint32 RegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegDemapBERTMonStatus + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonStatus + EngineOffset(engineId, slice);
    }

static uint32 RegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cThaRegMapBERTMonTxPtgIdMask;
    }

static uint32 RegMapBERTMonTdmMonAndPsnMonSelectMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 0;
    }

static uint32 RegPdaPwPrbsGenCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 0x0;
    }

static uint32 RegPdaPwPrbsMonCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 0x0;
    }

static uint32 TdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 0;
    }

static uint32 TdmChannelIdStsFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 32;
    }

static uint32 TdmChannelIdVtgFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 4;
    }

static uint32 TdmChannelIdVtFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 1;
    }

static uint32 BaseAddress(ThaPrbsRegProvider self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MonTdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    return ThaPrbsRegProviderTdmChannelIdSliceFactor(self, prbsModule);
    }

static void MethodsInit(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, TdmChannelIdSliceFactor);
        mMethodOverride(m_methods, TdmChannelIdStsFactor);
        mMethodOverride(m_methods, TdmChannelIdVtgFactor);
        mMethodOverride(m_methods, TdmChannelIdVtFactor);
        mMethodOverride(m_methods, MonTdmChannelIdSliceFactor);

        /* Registers */
        mMethodOverride(m_methods, RegMapBERTGenSelectedChannel);
        mMethodOverride(m_methods, RegMapBERTGenNxDs0ConcateControl);
        mMethodOverride(m_methods, RegMapBERTGenMode);
        mMethodOverride(m_methods, RegMapBERTGenFixedPatternControl);
        mMethodOverride(m_methods, RegMapBERTGenErrorRateInsert);
        mMethodOverride(m_methods, RegMapBERTGenSingleBitErrInsert);
        mMethodOverride(m_methods, RegMapBERTGenGoodBitCounter);
        mMethodOverride(m_methods, RegMapBERTMonSelectedChannel);
        mMethodOverride(m_methods, RegMapBERTMonNxDs0ConcateControl);
        mMethodOverride(m_methods, RegMapBERTMonGlobalControl);
        mMethodOverride(m_methods, RegMapBERTMonStickyEnable);
        mMethodOverride(m_methods, RegMapBERTMonSticky);
        mMethodOverride(m_methods, RegMapBERTMonMode);
        mMethodOverride(m_methods, RegMapBERTMonFixedPatternControl);
        mMethodOverride(m_methods, RegMapBERTMonErrorCounter);
        mMethodOverride(m_methods, RegMapBERTMonGoodBitCounter);
        mMethodOverride(m_methods, RegMapBERTMonLossBitCounter);
        mMethodOverride(m_methods, RegMapBERTMonCounterLoadId);
        mMethodOverride(m_methods, RegMapBERTMonErrorCounterLoading);
        mMethodOverride(m_methods, RegMapBERTMonGoodBitCounterLoading);
        mMethodOverride(m_methods, RegMapBERTMonLossBitCounterLoading);
        mMethodOverride(m_methods, RegMapBERTMonStatus);
        mMethodOverride(m_methods, RegDemapBERTMonSelectedChannel);
        mMethodOverride(m_methods, RegDemapBERTMonNxDs0ConcateControl);
        mMethodOverride(m_methods, RegDemapBERTMonGlobalControl);
        mMethodOverride(m_methods, RegDemapBERTMonStickyEnable);
        mMethodOverride(m_methods, RegDemapBERTMonSticky);
        mMethodOverride(m_methods, RegDemapBERTMonMode);
        mMethodOverride(m_methods, RegDemapBERTMonFixedPatternControl);
        mMethodOverride(m_methods, RegDemapBERTMonErrorCounter);
        mMethodOverride(m_methods, RegDemapBERTMonGoodBitCounter);
        mMethodOverride(m_methods, RegDemapBERTMonLossBitCounter);
        mMethodOverride(m_methods, RegDemapBERTMonCounterLoadId);
        mMethodOverride(m_methods, RegDemapBERTMonErrorCounterLoading);
        mMethodOverride(m_methods, RegDemapBERTMonGoodBitCounterLoading);
        mMethodOverride(m_methods, RegDemapBERTMonLossBitCounterLoading);
        mMethodOverride(m_methods, RegDemapBERTMonStatus);
        mMethodOverride(m_methods, RegPdaPwPrbsGenCtrl);
        mMethodOverride(m_methods, RegPdaPwPrbsMonCtrl);

        /* Bit fields */
        mMethodOverride(m_methods, RegMapBERTGenTxPtgEnMask);
        mMethodOverride(m_methods, RegMapBERTGenTxPtgIdMask);
        mMethodOverride(m_methods, RegDemapBERTMonRxPtgEnMask);
        mMethodOverride(m_methods, RegDemapBERTMonRxPtgIdMask);
        mMethodOverride(m_methods, RegMapBERTMonTxPtgEnMask);
        mMethodOverride(m_methods, RegMapBERTMonTxPtgIdMask);
        mMethodOverride(m_methods, RegMapBERTMonTdmMonAndPsnMonSelectMask);
        mMethodOverride(m_methods, RegMapBERTGenTxBerMdMask);
        mMethodOverride(m_methods, RegMapBERTGenTxBerMdShift);
        mMethodOverride(m_methods, RegMapBERTGenTxBerErrMask);
        mMethodOverride(m_methods, RegMapBERTGenTxBerErrShift);
        mMethodOverride(m_methods, RegMapBERTGenTxSingleErrMask);
        mMethodOverride(m_methods, RegMapBERTGenTxSingleErrShift);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsRegProvider);
    }

ThaPrbsRegProvider ThaPrbsRegProviderObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider ThaPrbsRegProviderDefaultProvider(void)
    {
    static tThaPrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = ThaPrbsRegProviderObjectInit(&shareProvider);
    return pShareProvider;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenSelectedChannel(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenNxDs0ConcateControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenMode(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenFixedPatternControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenErrorRateInsert(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenSingleBitErrInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenSingleBitErrInsert(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenGoodBitCounter(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonSelectedChannel(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonNxDs0ConcateControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonGlobalControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonStickyEnable(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonSticky(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonMode(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonFixedPatternControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonErrorCounter(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonGoodBitCounter(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonLossBitCounter(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonCounterLoadId(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonErrorCounterLoading(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonGoodBitCounterLoading(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonLossBitCounterLoading(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonStatus(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonSelectedChannel(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonNxDs0ConcateControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonNxDs0ConcateControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonGlobalControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonGlobalControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonStickyEnable(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonStickyEnable(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonSticky(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonMode(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonFixedPatternControl(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonErrorCounter(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonGoodBitCounter(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonLossBitCounter(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonCounterLoadId(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonCounterLoadId(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonErrorCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonErrorCounterLoading(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonGoodBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonGoodBitCounterLoading(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonLossBitCounterLoading(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonLossBitCounterLoading(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonStatus(self, prbsModule, engineId, slice);
    return 0;
    }

uint32 ThaPrbsRegProviderRegPdaPwPrbsGenCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegPdaPwPrbsGenCtrl(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegPdaPwPrbsMonCtrl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegPdaPwPrbsMonCtrl(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxPtgEnMask(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxPtgIdMask(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonRxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonRxPtgEnMask(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegDemapBERTMonRxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegDemapBERTMonRxPtgIdMask(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonTxPtgEnMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonTxPtgEnMask(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonTxPtgIdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonTxPtgIdMask(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTMonSideSelectMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTMonTdmMonAndPsnMonSelectMask(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenTxBerMdMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxBerMdMask(self, prbsModule);
    return 0;
    }

uint8  ThaPrbsRegProviderRegMapBERTGenTxBerMdShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxBerMdShift(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenTxBerErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxBerErrMask(self, prbsModule);
    return 0;
    }

uint8  ThaPrbsRegProviderRegMapBERTGenTxBerErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxBerErrShift(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderRegMapBERTGenTxSingleErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxSingleErrMask(self, prbsModule);
    return 0;
    }

uint8  ThaPrbsRegProviderRegMapBERTGenTxSingleErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->RegMapBERTGenTxSingleErrShift(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderTdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->TdmChannelIdSliceFactor(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderTdmChannelIdStsFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->TdmChannelIdStsFactor(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderTdmChannelIdVtgFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->TdmChannelIdVtgFactor(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderTdmChannelIdVtFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->TdmChannelIdVtFactor(self, prbsModule);
    return 0;
    }

uint32 ThaPrbsRegProviderBaseAddress(ThaPrbsRegProvider self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return cInvalidUint32;
    }

uint32 ThaPrbsRegProviderMonTdmChannelIdSliceFactor(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    if (self)
        return mMethodsGet(self)->MonTdmChannelIdSliceFactor(self, prbsModule);
    return 0;
    }

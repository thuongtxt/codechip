/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsCountersInternal.h
 * 
 * Created Date: Oct 8, 2018
 *
 * Description : Default PRBS counters
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSCOUNTERSINTERNAL_H_
#define _THAPRBSCOUNTERSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/prbs/AtPrbsCountersInternal.h"
#include "ThaPrbsCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsCounters
    {
    tAtPrbsCounters super;

    /* Private data */
    uint64 rxErrorBit;
    uint64 rxSyncBit;
    uint64 rxLossBit;
    uint64 rxBit;
    uint64 txBit;
    }tThaPrbsCounters;

typedef struct tThaPrbsFrameCounters
    {
    tThaPrbsCounters super;

    /* Private data */
    uint64 txFrame;
    uint64 rxFrame;
    uint64 rxErrorFrame;
    }tThaPrbsFrameCounters;

/*--------------------------- Forward declarations ---------------------------*/
ThaPrbsCounters ThaPrbsCountersObjectInit(ThaPrbsCounters self);
ThaPrbsCounters ThaPrbsFrameCountersObjectInit(ThaPrbsCounters self);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSCOUNTERSINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineVc1xInternal.h
 * 
 * Created Date: May 1, 2017
 *
 * Description : VC1x PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINEVC1XINTERNAL_H_
#define _THAPRBSENGINEVC1XINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineVc1x
    {
    tThaPrbsEngine super;
    }tThaPrbsEngineVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel vc1x, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINEVC1XINTERNAL_H_ */


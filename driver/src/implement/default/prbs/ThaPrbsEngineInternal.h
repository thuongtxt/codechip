/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineInternal.h
 * 
 * Created Date: Apr 15, 2015
 *
 * Description : PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINEINTERNAL_H_
#define _THAPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../pdh/ThaPdhDe1.h"
#include "ThaModulePrbsInternal.h"
#include "ThaPrbsEngine.h"
#include "ThaPrbsCounters.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThaPrbsEngine(self) ((ThaPrbsEngine)self)
#define mHwEngine(self) ThaPrbsEngineHwEngineId(mThaPrbsEngine(self))
#define mHwSlice(self) ThaPrbsEngineHwSliceId(mThaPrbsEngine(self))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineMethods
    {
    uint32 (*ChannelId)(ThaPrbsEngine self);
    void (*TxChannelIdSet)(ThaPrbsEngine self, uint32 channelId);
    uint32 (*HwEngineId)(ThaPrbsEngine self);
    uint8 (*HwSliceId)(ThaPrbsEngine self);
    uint8 (*PartId)(ThaPrbsEngine self);
    void (*HwDefault)(ThaPrbsEngine self);
    void (*ResourceRemove)(ThaPrbsEngine self);
    uint32 (*RxChannelId)(ThaPrbsEngine self);
    uint32 (*TimeslotBitMap)(ThaPrbsEngine self);
    void (*TimeslotDefaultSet)(ThaPrbsEngine self, uint32 bitMap);
    eAtRet (*TdmSideRxEnable)(ThaPrbsEngine self, eBool enable);

    /* To work with PSN generating */
    eBool (*FlexiblePsnSide)(ThaPrbsEngine self);
    uint8 (*CircuitLocalLoopbackMode)(ThaPrbsEngine self);
    AtChannel (*CircuitForLoopbackHandling)(ThaPrbsEngine self);

    /* Utils */
    uint32 (*ErrorRateSw2Hw)(ThaPrbsEngine self, eAtBerRate errorRate);
    eAtBerRate (*ErrorRateHw2Sw)(ThaPrbsEngine self, uint32 errorRate);

    uint32 (*BertGenSelectedChannelRegAddress)(ThaPrbsEngine self);
    uint32 (*BertGenEnableMask)(ThaPrbsEngine self);
    uint32 (*BertGenChannelIdMask)(ThaPrbsEngine self);

    uint32 (*BertMonTdmSelectedChannelRegAddress)(ThaPrbsEngine self);
    uint32 (*BertMonTdmEnableMask)(ThaPrbsEngine self);
    uint32 (*BertMonTdmChannelIdMask)(ThaPrbsEngine self);

    uint32 (*BertMonPsnSelectedChannelRegAddress)(ThaPrbsEngine self);
    uint32 (*BertMonPsnEnableMask)(ThaPrbsEngine self);
    uint32 (*BertMonPsnChannelIdMask)(ThaPrbsEngine self);

    uint32 (*BertGenSelectModeRegAddress)(ThaPrbsEngine self);
    uint32 (*BertGenSwapModeMask)(ThaPrbsEngine self);
    uint32 (*BertGenInvertDataMask)(ThaPrbsEngine self);
    uint32 (*BertGenPrbsMode)(ThaPrbsEngine self);

    uint32 (*BertGenErrorRateInsertRegAddress)(ThaPrbsEngine self);

    uint32 (*TdmMonitoringStatusRegAddress)(ThaPrbsEngine self, uint32 engineId, uint32 slice);
    uint32 (*PsnMonitoringStatusRegAddress)(ThaPrbsEngine self, uint32 engineId, uint32 slice);

    eBool (*TxInvertModeShouldSwap)(ThaPrbsEngine self, eAtPrbsMode mode);
    eBool (*RxInvertModeShouldSwap)(ThaPrbsEngine self, eAtPrbsMode mode);

    void (*Restore)(ThaPrbsEngine self);

    /* To support channelized PRBS */
    eBool (*IsChannelized)(ThaPrbsEngine self);
    eAtRet (*Channelize)(ThaPrbsEngine self);
    eAtRet (*Unchannelize)(ThaPrbsEngine self);
    eAtRet (*AllSubCircuitsPwSquel)(ThaPrbsEngine self, eBool forced);
    eBool (*ChannelizedPrbsAllowed)(ThaPrbsEngine self);

    /* HW enable/disable engine */
    eBool (*HwTxTdmIsEnabled)(ThaPrbsEngine self);
    eAtModulePrbsRet (*HwTxTdmEnable)(ThaPrbsEngine self, eBool enable);
    eAtRet (*HwRxPsnEnable)(ThaPrbsEngine self, eBool enable);
    eBool (*HwRxPsnIsEnabled)(ThaPrbsEngine self, eBool enable);
    }tThaPrbsEngineMethods;

typedef struct tThaPrbsEngine
    {
    tAtPrbsEngine super;
    const tThaPrbsEngineMethods *methods;

    /* Private data */
    ThaModulePrbs module;
    uint32 hwEngineId;

    /* To support PSN side */
    eAtPrbsSide softwareGeneratingSide;
    uint8 cachedLoopbackMode;
    eBool loopbackListened;
    eBool listeningEnabled;
    eBool psnRxEnabled;
    eBool isHwTxEnabled;
    eBool isHwRxTdmEnabled;
    }tThaPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId);
AtPrbsEngine ThaPrbsEngineAuVcObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId);
AtPrbsEngine ThaPrbsEngineVc1xObjectInit(AtPrbsEngine self, AtSdhChannel vc1x, uint32 engineId);

uint32 ThaPrbsEngineDe1ChannelIdGet(ThaPrbsEngine self, ThaPdhDe1 de1);
uint32 ThaPrbsEngineDe1RxChannelIdGet(ThaPrbsEngine self, ThaPdhDe1 de1);

#endif /* _THAPRBSENGINEINTERNAL_H_ */


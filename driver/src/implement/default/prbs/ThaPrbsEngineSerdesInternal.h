/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineSerdesInternal.h
 * 
 * Created Date: Nov 6, 2013
 *
 * Description : SERDES PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINESERDESINTERNAL_H_
#define _THAPRBSENGINESERDESINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/prbs/AtPrbsEngineInternal.h"
#include "ThaPrbsEngineSerdes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineSerdesMethods
    {
    uint32 (*DiagnosticControlRegister)(ThaPrbsEngineSerdes self);
    uint32 (*DiagnosticAlarmRegister)(ThaPrbsEngineSerdes self);
    uint32 (*SerdesControllerId)(ThaPrbsEngineSerdes self);
    }tThaPrbsEngineSerdesMethods;

typedef struct tThaPrbsEngineSerdes
    {
    tAtPrbsEngine super;
    const tThaPrbsEngineSerdesMethods *methods;

    /* Private data */
    AtSerdesController serdesController;
    }tThaPrbsEngineSerdes;

typedef struct tThaPrbsEngineSerdesEthPort
    {
    tThaPrbsEngineSerdes super;
    }tThaPrbsEngineSerdesEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEngineSerdesObjectInit(AtPrbsEngine self, AtSerdesController serdesController);
AtPrbsEngine ThaPrbsEngineSerdesEthPortObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINESERDESINTERNAL_H_ */


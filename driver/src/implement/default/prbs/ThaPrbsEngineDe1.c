/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngineDe1.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for DE1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhChannel.h"
#include "ThaModulePrbsInternal.h"
#include "ThaPrbsEngineInternal.h"
#include "ThaPrbsEngineDe1Internal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaSdhVc.h"
#include "../pdh/ThaModulePdh.h"
#include "../pdh/ThaPdhDe2De1.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsEngineMethods           m_ThaPrbsEngineOverride;
static tThaPrbsEnginePdhChannelMethods m_ThaPrbsEnginePdhChannelOverride;
static tAtPrbsEngineMethods    		   m_AtPrbsEngineOverride;

/* Save super implementation */
static const tThaPrbsEngineMethods *m_ThaPrbsEngineMethods = NULL;
static const tAtPrbsEngineMethods  *m_AtPrbsEngineMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineDe1);
    }

static eAtRet ChannelHwIdGet(ThaPdhDe1 de1, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)de1;
    AtPdhChannel de2 = (AtPdhChannel)AtPdhChannelParentChannelGet(pdhChannel);
    AtSdhChannel vc  = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);

    /* DS1/E1 over SDH */
    if (vc)
        {
        *hwVtg = AtSdhChannelTug2Get(vc);
        *hwVt  = AtSdhChannelTu1xGet(vc);
        return ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, hwSlice, hwSts);
        }

    /* M13 */
    if (de2)
        return ThaPdhDe2De1HwIdGet((ThaPdhDe1)de1, hwSlice, hwSts, hwVtg, hwVt, cAtModulePrbs);

    /* DS1/E1 LIU */
    *hwSts = 0;
    *hwVtg = 0;
    *hwVt = (uint8)AtChannelIdGet((AtChannel)de1);

    return cAtOk;
    }

static uint32 ChannelIdWithFactors(ThaPrbsEngine self, ThaPdhDe1 de1,
                                   uint32 sliceFactor, uint32 stsFactor, uint32 vtgFactor, uint32 vtFactor)
    {
    uint8 slice = 0, hwSts = 0, vtgId = 0, vtId = 0;
    eAtRet ret = ChannelHwIdGet(de1, &slice, &hwSts, &vtgId, &vtId);
    AtUnused(self);

    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)de1, cAtLogLevelCritical, AtSourceLocation, "Channel ID converting fail\r\n");
        return cBit31_0;
        }

    return (sliceFactor * slice) + (stsFactor * hwSts) + (vtgFactor * vtgId) + (vtFactor * vtId);
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return ThaPrbsEngineDe1ChannelIdGet(self, de1);
    }

static uint32 RxChannelId(ThaPrbsEngine self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return ThaPrbsEngineDe1RxChannelIdGet(self, de1);
    }

static uint8 HwSliceId(ThaPrbsEngine self)
    {
    return ThaPdhDe1SliceGet((ThaPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self));
    }

static eBool ModuleCanDetectLosByAllZeroPattern(ThaPrbsEnginePdhChannel self, ThaModulePdh module)
    {
    AtUnused(self);
    return ThaModulePdhDetectDe1LosByAllZeroPattern(module);
    }

static uint32 TimeslotBitMap(ThaPrbsEngine self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    eAtPdhDe1FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de1);

    if ((frameType == cAtPdhDs1FrmSf) || (frameType == cAtPdhDs1FrmEsf))
        return cBit23_0;

    if ((frameType == cAtPdhE1Frm) || (frameType == cAtPdhE1MFCrc))
        {
        if (AtPdhDe1SignalingIsEnabled(de1))
            return cBit15_1 | cBit31_17;
        return cBit31_1;
        }

    return m_ThaPrbsEngineMethods->TimeslotBitMap(self);
    }

static eBool IsUnframed(ThaPrbsEnginePdhChannel self, uint16 frameMode)
    {
    AtUnused(self);
    return AtPdhDe1IsUnframeMode(frameMode);
    }

static eBool BitOrderIsSupported(AtChannel de1, eAtPrbsBitOrder order)
    {
    if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)de1)) && (order == cAtPrbsBitOrderLsb))
        return cAtFalse;
    return cAtTrue;
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtChannel de1 = AtPrbsEngineChannelGet(self);

    if (de1 == NULL)
        return cAtErrorNullPointer;

    if (BitOrderIsSupported(de1, order))
        return m_AtPrbsEngineMethods->RxBitOrderSet(self, order);

    return cAtErrorModeNotSupport;
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtChannel de1 = AtPrbsEngineChannelGet(self);

    if (de1 == NULL)
	    return cAtErrorNullPointer;

    if (BitOrderIsSupported(de1, order))
        return m_AtPrbsEngineMethods->TxBitOrderSet(self, order);

    return cAtErrorModeNotSupport;
    }

static ThaPdhDe1 De1(ThaPrbsEnginePdhChannel self)
    {
    return (ThaPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    }

static eAtRet TxFramerBypass(ThaPrbsEnginePdhChannel self, eBool bypass)
    {
    ThaPdhDe1 de1 = De1(self);
    eAtRet ret = cAtOk;

    ret |= ThaPdhDe1TxFramerBypass(de1, bypass);
    ret |= ThaPdhDe1MapBypass(de1, bypass);

    return ret;
    }

static eAtRet RxFramerMonOnlyEnable(ThaPrbsEnginePdhChannel self, eBool enabled)
    {
    ThaPdhDe1 de1 = De1(self);
    return ThaPdhDe1RxFramerMonitorOnlyEnable(de1, enabled);
    }

static eBool IsChannelized(ThaPrbsEngine self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return AtPdhDe1Ds0MaskGet(de1) ? cAtTrue : cAtFalse;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, HwSliceId);
        mMethodOverride(m_ThaPrbsEngineOverride, TimeslotBitMap);
        mMethodOverride(m_ThaPrbsEngineOverride, IsChannelized);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void OverrideThaPrbsEnginePdhChannel(AtPrbsEngine self)
    {
    ThaPrbsEnginePdhChannel engine = (ThaPrbsEnginePdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEnginePdhChannelOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEnginePdhChannelOverride));

        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, IsUnframed);
        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, ModuleCanDetectLosByAllZeroPattern);
        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, TxFramerBypass);
        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, RxFramerMonOnlyEnable);
        }

    mMethodsSet(engine, &m_ThaPrbsEnginePdhChannelOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine(self);
    OverrideThaPrbsEnginePdhChannel(self);
    }

AtPrbsEngine ThaPrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEnginePdhChannelObjectInit(self, (AtPdhChannel)de1, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine ThaPrbsEngineDe1New(AtPdhDe1 de1, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaPrbsEngineDe1ObjectInit(newEngine, de1, engineId);
    }

uint32 ThaPrbsEngineDe1ChannelIdGet(ThaPrbsEngine self, ThaPdhDe1 de1)
    {
    ThaModulePrbs prbsModule = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ChannelIdWithFactors(self, de1,
                                ThaModulePrbsTdmChannelIdSliceFactor(prbsModule),
                                ThaModulePrbsTdmChannelIdStsFactor(prbsModule),
                                ThaModulePrbsTdmChannelIdVtgFactor(prbsModule),
                                ThaModulePrbsTdmChannelIdVtFactor(prbsModule));
    }

uint32 ThaPrbsEngineDe1RxChannelIdGet(ThaPrbsEngine self, ThaPdhDe1 de1)
    {
    ThaModulePrbs prbsModule = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ChannelIdWithFactors(self, de1,
                                ThaModulePrbsMonTdmChannelIdSliceFactor(prbsModule),
                                ThaModulePrbsTdmChannelIdStsFactor(prbsModule),
                                ThaModulePrbsTdmChannelIdVtgFactor(prbsModule),
                                ThaModulePrbsTdmChannelIdVtFactor(prbsModule));
    }

uint32 ThaPrbsEngineDe1HwIdGet(ThaPdhDe1 de1, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    if (de1)
        return ChannelHwIdGet(de1, hwSlice, hwSts, hwVtg, hwVt);
    return cInvalidUint32;
    }

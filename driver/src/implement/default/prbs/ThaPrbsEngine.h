/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngine.h
 * 
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINE_H_
#define _THAPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/prbs/AtPrbsEngineInternal.h"
#include "AtPw.h"
#include "ThaModulePrbs.h"
#include "ThaPrbsCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngine     * ThaPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete engines */
AtPrbsEngine ThaPrbsEngineDe1New(AtPdhDe1 de1, uint32 engineId);
AtPrbsEngine ThaPrbsEngineDe3New(AtPdhDe3 de3, uint32 engineId);
AtPrbsEngine ThaPrbsEngineNxDs0New(AtPdhNxDS0 nxDs0, uint32 engineId);
AtPrbsEngine ThaPrbsEngineVc1xNew(AtSdhChannel vc1x, uint32 engineId);
AtPrbsEngine ThaPrbsEngineAuVcNew(AtSdhChannel vc, uint32 engineId);

ThaModulePrbs ThaPrbsEngineModuleGet(AtPrbsEngine self);
uint32 ThaPrbsEngineHwEngineId(ThaPrbsEngine self);
uint8 ThaPrbsEngineHwSliceId(ThaPrbsEngine self);

/* Concrete products */
AtPrbsEngine Tha60210021NxDs0PrbsEngineNew(AtPdhNxDS0 nxDs0, uint32 engineId);
AtPrbsEngine Tha60210011AuVcPrbsEngineNew(AtSdhChannel vc, uint32 engineId);
AtPrbsEngine Tha60210021PwPrbsEngineNew(AtPw pw, uint32 engineId);
AtPrbsEngine Tha60210051PwPrbsEngineNew(AtPw pw, uint32 engineId);
AtPrbsEngine Tha6021SerialLinePrbsEngineNew(AtChannel channel, uint32 engineId);
AtPrbsEngine Tha6021PdhLinePrbsEngineNew(AtChannel channel, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineDe1New(AtPdhDe1 de1, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineDe3New(AtPdhDe3 de3, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineVc1xNew(AtSdhChannel vc1x, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineAuVcNew(AtSdhChannel vc, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineAuVcSts192cNew(AtSdhChannel vc, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineAuVcSts192cV2New(AtSdhChannel vc, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineTu3VcNew(AtSdhChannel vc, uint32 engineId);
AtPrbsEngine Tha60290022PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0, uint32 engineId);
AtPrbsEngine Tha60210061AuVcPrbsEngineEc1New(AtSdhChannel vc, uint32 engineId);
AtPrbsEngine Tha60210061PrbsEngineVc1xEc1New(AtSdhChannel vc1x, uint32 engineId);
AtPrbsEngine Tha60210061PrbsEngineDe3Ec1New(AtPdhDe3 de3, uint32 engineId);
AtPrbsEngine Tha60210061PrbsEngineDe1Ec1New(AtPdhDe1 de1, uint32 engineId);
AtPrbsEngine Tha60210061PrbsEngineNxDs0Ec1New(AtPdhNxDS0 nxDs0, uint32 engineId);
AtPrbsEngine Tha60290081PrbsEngineAuVcNew(AtSdhChannel vc, uint32 engineId);

/* Utils */
uint32 ThaPrbsEngineErrorRateSw2Hw(ThaPrbsEngine self, eAtBerRate errorRate);
eAtBerRate ThaPrbsEngineErrorRateHw2Sw(ThaPrbsEngine self, uint32 errorRate);
eBool ThaPrbsEngineBitOrderIsSupported(eAtPrbsBitOrder order);
eBool ThaPrbsEngineErrorRateIsSupported(eAtBerRate errorRate);

/* Common BERT monitor */
uint32 ThaPrbsEngineBertMonSelectMask(ThaPrbsEngine self);
uint32 ThaPrbsEngineBertMonSelectedChannelRegAddress(ThaPrbsEngine self);

void ThaPrbsEngineHwEngineIdSet(ThaPrbsEngine self, uint32 hwEngineId);
uint32 ThaPrbsEngineHwEngineIdGet(ThaPrbsEngine self);
void ThaPrbsEngineResourceRemove(ThaPrbsEngine self);
uint32 ThaPrbsEngineChannelId(ThaPrbsEngine self);
void ThaPrbsEngineTxChannelIdSet(ThaPrbsEngine self, uint32 channelId);

void ThaPrbsEngineHwCleanUp(ThaPrbsEngine self);
eAtModulePrbsRet ThaPrbsEngineDefaultClassInit(ThaPrbsEngine self);
eAtModulePrbsRet ThaPrbsEngineDefaultClassMonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side);
eAtModulePrbsRet ThaPrbsEngineDefaultClassRxEnable(AtPrbsEngine self, eBool enable);
eBool ThaPrbsEngineDefaultClassRxIsEnabled(AtPrbsEngine self);
uint32 ThaPrbsEngineDefaultClassAlarmGet(AtPrbsEngine self);
eAtRet ThaPrbsEnginePwDidBind(ThaPrbsEngine self, AtPw pw);
eAtRet ThaPrbsEnginePwWillBind(ThaPrbsEngine self, AtPw pw);
eBool ThaPrbsEngineFlexiblePsnSide(ThaPrbsEngine self);

uint32 ThaPrbsEngineTxInvertModeSw2Hw(ThaPrbsEngine self, eBool invert);
eBool ThaPrbsEngineTxInvertModeHw2Sw(ThaPrbsEngine self, uint32 invert);
uint32 ThaPrbsEngineRxInvertModeSw2Hw(ThaPrbsEngine self, eBool invert);
eBool ThaPrbsEngineRxInvertModeHw2Sw(ThaPrbsEngine self, uint32 invert);

/* HA */
void ThaPrbsEngineRestore(ThaPrbsEngine self);

eAtRet ThaPrbsEngineRxTdmSideEnable(ThaPrbsEngine self, eBool enable);
eBool ThaPrbsEngineRxTdmSideIsEnabled(AtPrbsEngine self);
eAtModulePrbsRet ThaPrbsEngineTdmSideAllCountersDirectLatch(AtPrbsEngine self);
uint32 ThaPrbsEngineTdmSideAlarmGet(AtPrbsEngine self);
uint32 ThaPrbsEngineTimeslotBitMap(ThaPrbsEngine self);

eAtRet ThaPrbsEngineAllSdhSubChannelsPwSquel(AtSdhChannel vc, eBool squel);
eAtRet ThaPrbsEngineAllPdhSubChannelsPwSquel(AtPdhChannel pdhChannel, eBool squel);
eAtRet ThaPrbsEngineChannelizedPrbsDefaultSet(ThaPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngineDe3.c
 *
 * Created Date: Apr 17, 2015
 *
 * Description : PRBS engine for DE3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhChannel.h"
#include "../../../generic/pdh/AtPdhDe3Internal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaSdhVc.h"
#include "../pdh/ThaModulePdh.h"
#include "../prbs/ThaModulePrbs.h"
#include "../cdr/ThaModuleCdr.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "../map/ThaModuleAbstractMap.h"
#include "../man/ThaDevice.h"
#include "ThaPrbsEngineDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods    			m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods           	m_ThaPrbsEngineOverride;
static tThaPrbsEnginePdhChannelMethods 	m_ThaPrbsEnginePdhChannelOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods 	*m_AtPrbsEngineMethods  = NULL;
static const tThaPrbsEngineMethods 	*m_ThaPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngineDe3);
    }

static uint32 ChannelIdWithFactors(ThaPrbsEngine self, uint32 sliceFactor, uint32 stsFactor)
    {
    uint8 slice, hwSts;
    AtPdhChannel de3 = (AtPdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    ThaPdhChannelHwIdGet(de3, cAtModulePrbs, &slice, &hwSts);
    return (sliceFactor * slice) + (stsFactor * hwSts);
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ChannelIdWithFactors(self,
                                ThaModulePrbsTdmChannelIdSliceFactor(module),
                                ThaModulePrbsTdmChannelIdStsFactor(module));
    }

static uint32 RxChannelId(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ChannelIdWithFactors(self,
                                ThaModulePrbsMonTdmChannelIdSliceFactor(module),
                                ThaModulePrbsTdmChannelIdStsFactor(module));
    }

static uint8 HwSliceId(ThaPrbsEngine self)
    {
    uint8 slice = 0, hwSts = 0;
    AtPdhChannel de3 = (AtPdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    ThaPdhChannelHwIdGet(de3, cAtModulePdh, &slice, &hwSts);

    return slice;
    }

static eBool ModuleCanDetectLosByAllZeroPattern(ThaPrbsEnginePdhChannel self, ThaModulePdh module)
    {
    AtUnused(self);
    return ThaModulePdhDetectDe3LosByAllZeroPattern(module);
    }

static eBool IsUnframed(ThaPrbsEnginePdhChannel self, uint16 frameMode)
    {
    AtUnused(self);
    return AtPdhDe3FrameTypeIsUnframed(frameMode);
    }

static eBool TxInvertModeShouldSwap(ThaPrbsEngine self, eAtPrbsMode mode)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    return ThaModulePrbsDe3InvertModeShouldSwap(ThaPrbsEngineModuleGet(engine), mode);
    }

static eBool RxInvertModeShouldSwap(ThaPrbsEngine self, eAtPrbsMode mode)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    return ThaModulePrbsDe3InvertModeShouldSwap(ThaPrbsEngineModuleGet(engine), mode);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eBool inverted = AtPrbsEngineTxIsInverted(self);
    eAtRet ret = m_AtPrbsEngineMethods->TxModeSet(self, prbsMode);
    if (ret != cAtOk)
        return ret;

    /* After configuring PRBS mode, inversion can be changed, make sure that it
     * has previous configuration by reapplying this */
    return AtPrbsEngineTxInvert(self, inverted);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eBool inverted = AtPrbsEngineRxIsInverted(self);
    eAtRet ret = m_AtPrbsEngineMethods->RxModeSet(self, prbsMode);
    if (ret != cAtOk)
        return ret;

    /* After configuring PRBS mode, inversion can be changed, make sure that it
     * has previous configuration by reapplying this */
    return AtPrbsEngineRxInvert(self, inverted);
    }

static eBool BitOrderIsSupported(AtChannel de3, eAtPrbsBitOrder order)
    {
    if (AtPdhDe3FrameTypeIsUnframed(AtPdhChannelFrameTypeGet((AtPdhChannel)de3)) && (order == cAtPrbsBitOrderLsb))
        return cAtFalse;
    return cAtTrue;
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtChannel de3 = AtPrbsEngineChannelGet(self);

    if (de3 == NULL)
        return cAtErrorNullPointer;

    if (BitOrderIsSupported(de3, order))
        return m_AtPrbsEngineMethods->RxBitOrderSet(self, order);

    return cAtErrorModeNotSupport;
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    AtChannel de3 = AtPrbsEngineChannelGet(self);

    if (de3 == NULL)
        return cAtErrorNullPointer;

    if (BitOrderIsSupported(de3, order))
        return m_AtPrbsEngineMethods->TxBitOrderSet(self, order);

    return cAtErrorModeNotSupport;
    }

static ThaPdhDe3 De3(ThaPrbsEnginePdhChannel self)
    {
    return (ThaPdhDe3)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    }

static eAtRet TxFramerBypass(ThaPrbsEnginePdhChannel self, eBool bypass)
    {
    ThaPdhDe3 de3 = De3(self);
    eAtRet ret = cAtOk;

    ret |= ThaPdhDe3TxFramerBypass(de3, bypass);
    ret |= ThaPdhDe3MapBypass(de3, bypass);

    return ret;
    }

static eAtRet RxFramerMonOnlyEnable(ThaPrbsEnginePdhChannel self, eBool enabled)
    {
    return ThaPdhDe3RxFramingMonOnlyEnable(De3(self), enabled);
    }

static eBool IsChannelized(ThaPrbsEngine self)
    {
    AtPdhChannel de3 = (AtPdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint16 frameType = AtPdhChannelFrameTypeGet(de3);

    if ((frameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (frameType == cAtPdhDs3FrmCbitChnl21E1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl28Ds1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl21E1s)   ||
        (frameType == cAtPdhE3FrmG751Chnl16E1s))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ChannelizeEnable(ThaPrbsEngine self, eBool channelized)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet((AtChannel)de3);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de3);
    uint16 appliedFrameType = (uint16)(channelized ? frameType : AtPdhDe3UnChannelizedFrameType(frameType));
    eAtRet ret = cAtOk;
    eBool shouldBypass = (channelized && AtChannelBoundPwGet((AtChannel)de3));

    ret |= ThaPdhDe3HwFrameTypeSet((ThaPdhDe3)de3, appliedFrameType);
    if (shouldBypass && !AtPdhDe3FrameTypeIsUnframed(frameType))
        ret |= ThaPdhDe3FramingBypass((ThaPdhDe3)de3, cAtTrue);

    ret |= ThaModuleCdrChannelizeDe3(cdrModule, de3, channelized);
    if (shouldBypass)
        ret |= ThaCdrDe3UnChannelizedModeSet(cdrModule, (AtPdhChannel)de3, cAtTrue);

    if (!shouldBypass)
        ret |= ThaModuleAbstractMapDe3Channelize(mapModule, de3, channelized);

    return ret;
    }

static eAtRet Channelize(ThaPrbsEngine self)
    {
    return ChannelizeEnable(self, cAtTrue);
    }

static eAtRet Unchannelize(ThaPrbsEngine self)
    {
    return ChannelizeEnable(self, cAtFalse);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, m_ThaPrbsEngineMethods, sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, ChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, RxChannelId);
        mMethodOverride(m_ThaPrbsEngineOverride, HwSliceId);

        mMethodOverride(m_ThaPrbsEngineOverride, TxInvertModeShouldSwap);
        mMethodOverride(m_ThaPrbsEngineOverride, RxInvertModeShouldSwap);
        mMethodOverride(m_ThaPrbsEngineOverride, IsChannelized);
        mMethodOverride(m_ThaPrbsEngineOverride, Channelize);
        mMethodOverride(m_ThaPrbsEngineOverride, Unchannelize);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void OverrideThaPrbsEnginePdhChannel(AtPrbsEngine self)
    {
    ThaPrbsEnginePdhChannel engine = (ThaPrbsEnginePdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEnginePdhChannelOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEnginePdhChannelOverride));

        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, IsUnframed);
        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, ModuleCanDetectLosByAllZeroPattern);
        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, TxFramerBypass);
        mMethodOverride(m_ThaPrbsEnginePdhChannelOverride, RxFramerMonOnlyEnable);
        }

    mMethodsSet(engine, &m_ThaPrbsEnginePdhChannelOverride);
    }

static void Override(AtPrbsEngine self)
    {
	OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    OverrideThaPrbsEnginePdhChannel(self);
    }

AtPrbsEngine ThaPrbsEngineDe3ObjectInit(AtPrbsEngine self, AtPdhDe3 de3, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEnginePdhChannelObjectInit(self, (AtPdhChannel)de3, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine ThaPrbsEngineDe3New(AtPdhDe3 de3, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaPrbsEngineDe3ObjectInit(newEngine, de3, engineId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : ThaPrbsEngine.c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for TDM which can be SDH VC or PDH channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDevice.h"
#include "../pw/adapters/ThaPwAdapter.h"
#include "ThaPrbsEngineInternal.h"
#include "ThaModulePrbsInternal.h"
#include "ThaModulePrbsReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidUint16 0xFFFF
#define cGlobalIndex 0
#define cInvalidLoopbackMode cBit7_0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaPrbsEngine *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPrbsEngineMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtPrbsEngineMethods    m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEngine);
    }

static uint8 PartId(ThaPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 Read(AtPrbsEngine self, uint32 regAddr)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    return mChannelHwRead(channel, regAddr, cAtModulePrbs);
    }

static void Write(AtPrbsEngine self, uint32 regAddr, uint32 regVal)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePrbs);
    }

static uint32 ChannelId(ThaPrbsEngine self)
    {
    /* Let sub class do */
    AtUnused(self);
    return 0;
    }

static uint32 TimeslotBitMap(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cBit0;
    }

static void TimeslotDefaultSet(ThaPrbsEngine self, uint32 bitMap)
    {
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    AtPrbsEngine engine = (AtPrbsEngine)self;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(engine);
    Write(engine, ThaRegMapBERTGenNxDs0ConcateControl(module, engineId, slice), bitMap);
    Write(engine, ThaRegMapBERTMonNxDs0ConcateControl(module, engineId, slice), bitMap);
    Write(engine, ThaRegDemapBERTMonNxDs0ConcateControl(module, engineId, slice), bitMap);
    }

static eAtRet ChannelizedPrbsDefaultSet(ThaPrbsEngine self)
    {
    eAtRet ret = cAtOk;

    if (mMethodsGet(self)->ChannelizedPrbsAllowed(self) && mMethodsGet(self)->IsChannelized(self))
        {
        ret |= mMethodsGet(self)->AllSubCircuitsPwSquel(self, cAtTrue);
        ret |= mMethodsGet(self)->Unchannelize(self);
        }

    return ret;
    }

static void HwDefault(ThaPrbsEngine self)
    {
    uint32 bitMap = mMethodsGet(self)->TimeslotBitMap(self);
    mMethodsGet(self)->TimeslotDefaultSet(self, bitMap);
    ChannelizedPrbsDefaultSet(self);
    }

static void TxChannelIdSet(ThaPrbsEngine self, uint32 channelId)
    {
    uint32 regAddr = mMethodsGet(self)->BertGenSelectedChannelRegAddress(self);
    uint32 regVal = Read((AtPrbsEngine)self, regAddr);
    uint32 mask = mMethodsGet(self)->BertGenChannelIdMask(self);
    uint32 shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, channelId);
    Write((AtPrbsEngine)self, regAddr, regVal);
    }

static void CircuitLoopbackDidChange(AtPrbsEngine self, uint8 newLoopbackMode)
    {
    if (!mThis(self)->listeningEnabled)
        return;

    if (newLoopbackMode == cAtLoopbackModeRelease)
        mThis(self)->cachedLoopbackMode = cInvalidLoopbackMode;
    else
        mThis(self)->cachedLoopbackMode = newLoopbackMode;
    }

static void CircuitPropertyWillChange(AtObject circuit, uint32 propertyId, AtSize value, void *userData)
    {
    AtUnused(circuit);
    AtUnused(propertyId);
    AtUnused(value);
    AtUnused(userData);
    }

static void CircuitPropertyDidChange(AtObject circuit, uint32 propertyId, AtSize value, void *userData)
    {
    AtUnused(circuit);

    if (propertyId == cAtChannelPropertyIdLoopback)
        {
        uint8 loopbackMode = (uint8)((AtSize)value);
        CircuitLoopbackDidChange((AtPrbsEngine)userData, loopbackMode);
        }
    }

static void CircuitListenerWillRemove(AtObject circuit, void *userData)
    {
    ThaPrbsEngine self = mThis(userData);
    AtUnused(circuit);
    self->loopbackListened = cAtFalse;
    }

static const tAtObjectPropertyListener *CircuitPropertyListener(void)
    {
    static tAtObjectPropertyListener listener;
    static const tAtObjectPropertyListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.PropertyWillChange = CircuitPropertyWillChange;
    listener.PropertyDidChange  = CircuitPropertyDidChange;
    listener.ListenerWillRemove = CircuitListenerWillRemove;
    pListener = &listener;

    return pListener;
    }

static void CircuitLoopbackListen(AtPrbsEngine self, eBool listen)
    {
    AtObject channel;

    if (mBoolToBin(mThis(self)->loopbackListened) == mBoolToBin(listen))
        return;

    channel = (AtObject)mMethodsGet(mThis(self))->CircuitForLoopbackHandling(mThis(self));
    if (listen)
        AtObjectPropertyListenerAdd(channel, CircuitPropertyListener(), self);
    else
        AtObjectPropertyListenerRemove(channel, CircuitPropertyListener(), self);

    mThis(self)->loopbackListened = listen;
    mThis(self)->listeningEnabled = cAtTrue;
    }

static eBool CommonBertMonRegister(AtPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    return ThaModulePrbsEngineIsCommonMonSide(module);
    }

static eAtRet BertMonSideSelect(AtPrbsEngine self, eAtPrbsSide side)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    return ThaModulePrbsEngineMonSideSet(module, mThis(self), side);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtPrbsSide defaultSide = cAtPrbsSideTdm;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    mMethodsGet(mThis(self))->TxChannelIdSet(mThis(self), mMethodsGet(mThis(self))->ChannelId(mThis(self)));
    m_AtPrbsEngineMethods->MonitoringSideSet(self, defaultSide);
    m_AtPrbsEngineMethods->GeneratingSideSet(self, defaultSide);

    if (mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)))
        {
        AtChannel circuit = mMethodsGet(mThis(self))->CircuitForLoopbackHandling(mThis(self));
        uint8 loopbackMode = AtChannelLoopbackGet(circuit);
        if (loopbackMode != cAtLoopbackModeRelease)
            mThis(self)->cachedLoopbackMode = loopbackMode;
        CircuitLoopbackListen(self, cAtTrue);
        }

    AtPrbsEngineEnable(self, cAtFalse);
    AtPrbsEngineErrorForce(self, cAtFalse);

    AtPrbsEngineModeSet(self, cAtPrbsModePrbs15);
    AtPrbsEngineBitOrderSet(self, cAtPrbsBitOrderMsb);
    AtPrbsEngineInvert(self, cAtFalse);

    if (ThaModulePrbsShouldResetFixPattern(module))
        {
        AtPrbsEngineTxFixedPatternSet(self, 0x0);
        AtPrbsEngineRxFixedPatternSet(self, 0x0);
        }

    mMethodsGet(mThis(self))->HwDefault(mThis(self));

    if (CommonBertMonRegister(self))
        BertMonSideSelect(self, defaultSide);

    return cAtOk;
    }

static eBool SeparateTwoDirections(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 BertGenSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTGenSelectedChannel(module, mHwEngine(self), mHwSlice(self));
    }

static uint32 BertGenEnableMask(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTGenTxPtgEnMask(module);
    }

static uint32 BertGenChannelIdMask(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTGenTxPtgIdMask(module);
    }

static eBool ShouldPreventConfigureToHw(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtDevice device = AtChannelDeviceGet(channel);
    eBool ret = AtDeviceAccessible(device) ? cAtFalse : cAtTrue;

    return ret;
    }

static eBool HwTxTdmIsEnabled(ThaPrbsEngine self)
    {
    if (ShouldPreventConfigureToHw((AtPrbsEngine)self))
        return mThis(self)->isHwTxEnabled;
    else
        {
        uint32 regAddr = mMethodsGet(mThis(self))->BertGenSelectedChannelRegAddress(mThis(self));
        uint32 mask = mMethodsGet(mThis(self))->BertGenEnableMask(mThis(self));
        return (Read((AtPrbsEngine)self, regAddr) & mask) ? cAtTrue : cAtFalse;
        }
    }

static eAtModulePrbsRet HwTxTdmEnable(ThaPrbsEngine self, eBool enable)
    {
    uint32 channelId = mMethodsGet(mThis(self))->ChannelId(mThis(self));
    uint32 regAddr = mMethodsGet(mThis(self))->BertGenSelectedChannelRegAddress(mThis(self));
    uint32 regVal = Read((AtPrbsEngine)self, regAddr);
    uint32 mask = mMethodsGet(mThis(self))->BertGenEnableMask(mThis(self));
    uint32 shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));
    mask = mMethodsGet(mThis(self))->BertGenChannelIdMask(mThis(self));
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, channelId);
    Write((AtPrbsEngine)self, regAddr, regVal);
    mThis(self)->isHwTxEnabled = enable;
    return cAtOk;
    }

static eBool LoopbackShouldBeMade(AtPrbsEngine self)
    {
    if (mThis(self)->softwareGeneratingSide == cAtPrbsSidePsn)
        return mMethodsGet(mThis(self))->HwTxTdmIsEnabled(mThis(self));
    return cAtFalse;
    }

static eAtRet HandleLoopback(AtPrbsEngine self)
    {
    AtChannel circuit = mMethodsGet(mThis(self))->CircuitForLoopbackHandling(mThis(self));
    eBool loopbackShouldBeMade = LoopbackShouldBeMade(self);
    eAtRet ret = cAtOk;
    uint8 loopbackMode = AtChannelLoopbackGet(circuit);

    /* When enabling, need to make the local loopback */
    if (loopbackShouldBeMade)
        {
        /* The loopback is explicitly made before, then save it */
        if (loopbackMode != cAtLoopbackModeRelease)
            mThis(self)->cachedLoopbackMode = loopbackMode;

        ret = AtChannelLoopbackSet(circuit, mMethodsGet(mThis(self))->CircuitLocalLoopbackMode(mThis(self)));
        if (ret != cAtOk)
            mThis(self)->cachedLoopbackMode = cInvalidLoopbackMode;

        return ret;
        }

    /* When disabling, need to release or revert loopback mode */
    if (mThis(self)->cachedLoopbackMode == cInvalidLoopbackMode)
        {
        if (loopbackMode == mMethodsGet(mThis(self))->CircuitLocalLoopbackMode(mThis(self)))
            ret |= AtChannelLoopbackSet(circuit, cAtLoopbackModeRelease);
        }
    else
        ret = AtChannelLoopbackSet(circuit, mThis(self)->cachedLoopbackMode);

    return ret;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtPw pw = AtChannelBoundPwGet(channel);
    eBool needToProtectPda = ((pw) && (AtPrbsEngineGeneratingSideGet(self) == cAtPrbsSideTdm)) ? cAtTrue : cAtFalse;
    eBool pwIsEnabled = cAtFalse;
    eBool oldEnabled = AtPrbsEngineTxIsEnabled(self);

    if (needToProtectPda)
        {
        pwIsEnabled = AtChannelIsEnabled((AtChannel)pw);
        AtChannelEnable((AtChannel)pw, cAtFalse);
        }

    mMethodsGet(mThis(self))->HwTxTdmEnable(mThis(self), enable);

    if (needToProtectPda)
        AtChannelEnable((AtChannel)pw, pwIsEnabled);

    if (mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)) && (mBoolToBin(oldEnabled) != mBoolToBin(enable)))
        {
        eAtRet ret;

        mThis(self)->listeningEnabled = cAtFalse;
        ret = HandleLoopback(self);
        mThis(self)->listeningEnabled = cAtTrue;

        return ret;
        }

    return cAtOk;
    }

static eBool LocalLoopbackMade(AtPrbsEngine self)
    {
    AtChannel channel = mMethodsGet(mThis(self))->CircuitForLoopbackHandling(mThis(self));
    uint8 localLoopbackMode = mMethodsGet(mThis(self))->CircuitLocalLoopbackMode(mThis(self));
    if (AtChannelLoopbackGet(channel) == localLoopbackMode)
        return cAtTrue;
    return cAtFalse;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    eBool txEnabled = mMethodsGet(mThis(self))->HwTxTdmIsEnabled(mThis(self));

    if (!mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)))
        return txEnabled;

    if (txEnabled && (mThis(self)->softwareGeneratingSide == cAtPrbsSidePsn))
        return LocalLoopbackMade(self);

    return txEnabled;
    }

static uint32 BertMonTdmSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegDemapBERTMonSelectedChannel(module, mHwEngine(self), mHwSlice(self));
    }

static uint32 BertMonTdmEnableMask(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegDemapBERTMonRxPtgEnMask(module);
    }

static uint32 BertMonTdmChannelIdMask(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegDemapBERTMonRxPtgIdMask(module);
    }

static uint32 RxChannelId(ThaPrbsEngine self)
    {
    return mMethodsGet(self)->ChannelId(self);
    }

static eAtRet TdmSideRxEnable(ThaPrbsEngine self, eBool enable)
	{
    uint32 mask, shift;
    uint32 channelId = mMethodsGet(self)->RxChannelId(self);
    uint32 regAddr = mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    uint32 regVal = Read((AtPrbsEngine)self, regAddr);

    if (ShouldPreventConfigureToHw((AtPrbsEngine)self))
        {
        mThis(self)->isHwRxTdmEnabled = enable;
        return cAtOk;
        }


    if (channelId == cInvalidUint32)
        {
        if (enable)
            return cAtErrorInvalidOperation;

        mThis(self)->isHwRxTdmEnabled = enable;
        return cAtOk;
        }

    mask  = mMethodsGet(self)->BertMonTdmEnableMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));

    mask  = mMethodsGet(self)->BertMonTdmChannelIdMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, channelId);

    mask  = ThaPrbsEngineBertMonSelectMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, 0);

    Write((AtPrbsEngine)self, regAddr, regVal);

    mThis(self)->isHwRxTdmEnabled = enable;
    return cAtOk;
	}

static uint32 BertMonPsnSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTMonSelectedChannel(module, mHwEngine(self), mHwSlice(self));
    }

static uint32 BertMonPsnEnableMask(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTMonTxPtgEnMask(module);
    }

static uint32 BertMonPsnChannelIdMask(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTMonTxPtgIdMask(module);
    }

static eAtRet HwRxPsnEnable(ThaPrbsEngine self, eBool enable)
    {
    uint32 regAddr, regVal, mask, shift;
    AtPrbsEngine engine = (AtPrbsEngine)self;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(engine);
    uint32 psnChannelId;

    regAddr = mMethodsGet(self)->BertMonPsnSelectedChannelRegAddress(self);
    regVal  = Read(engine, regAddr);

    mask  = mMethodsGet(self)->BertMonPsnEnableMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, mBoolToBin(enable));

    psnChannelId = ThaModulePrbsPsnChannelId(module, engine);
    mask  = mMethodsGet(self)->BertMonPsnChannelIdMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, psnChannelId);

    mask  = ThaPrbsEngineBertMonSelectMask(self);
    shift = AtRegMaskToShift(mask);
    mFieldIns(&regVal, mask, shift, 1);
    Write(engine, regAddr, regVal);

    return cAtOk;
    }

static eAtRet PsnSideRxEnable(AtPrbsEngine self, eBool enable)
	{
    AtPw pw = AtChannelBoundPwGet(AtPrbsEngineChannelGet(self));

    mThis(self)->psnRxEnabled = enable;

    if (pw)
        return mMethodsGet(mThis(self))->HwRxPsnEnable(mThis(self), enable);

    if (enable)
        return mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)) ? cAtOk : cAtErrorInvalidOperation;

    return cAtOk;
	}

static uint32 BertMonSelectMask(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTMonSideSelectMask(module);
    }

static uint32 BertMonSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    /* We currently reuse TDM Mon register for common monitor. */
    return mMethodsGet(self)->BertMonTdmSelectedChannelRegAddress(self);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
    	return mMethodsGet(mThis(self))->TdmSideRxEnable(mThis(self), enable);

    if (side == cAtPrbsSidePsn)
        return PsnSideRxEnable(self, enable);

    return cAtErrorInvlParm;
    }

static eBool RxTdmSideIsEnabled(AtPrbsEngine self)
    {
    if (ShouldPreventConfigureToHw(self))
        {
        return mThis(self)->isHwRxTdmEnabled;
        }
    else
        {
        uint32 mask = mMethodsGet(mThis(self))->BertMonTdmEnableMask(mThis(self));
        uint32 regAddr = mMethodsGet(mThis(self))->BertMonTdmSelectedChannelRegAddress(mThis(self));
        return (Read(self, regAddr) & mask) ? cAtTrue : cAtFalse;
        }
    }

static eBool HasPwAssociation(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtPw pw = AtChannelBoundPwGet(channel);
    return pw ? cAtTrue : cAtFalse;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr, mask;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        return RxTdmSideIsEnabled(self);

    if (side == cAtPrbsSidePsn)
        {
        if (HasPwAssociation(self))
            {
        mask = mMethodsGet(mThis(self))->BertMonPsnEnableMask(mThis(self));
        regAddr = mMethodsGet(mThis(self))->BertMonPsnSelectedChannelRegAddress(mThis(self));
        return (Read(self, regAddr) & mask) ? cAtTrue : cAtFalse;
        }

        if (mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)))
            return mThis(self)->psnRxEnabled;
        }

    return cAtFalse;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr, regVal, mask, shift;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (AtPrbsEngineErrorIsForced(self) == force)
        return cAtOk;

    regAddr = mMethodsGet(mThis(self))->BertGenErrorRateInsertRegAddress(mThis(self));
    regVal  = Read(self, regAddr);
    mask    = ThaRegMapBERTGenTxBerErrMask(module);
    shift   = ThaRegMapBERTGenTxBerErrShift(module);
    mFieldIns(&regVal, mask, shift, (force) ? ThaModulePrbsForceEnableValue(module, mThis(self)) : 0);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 result;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = mMethodsGet(mThis(self))->BertGenErrorRateInsertRegAddress(mThis(self));
    uint32 regVal = Read(self, regAddr);
    uint32 mask    = ThaRegMapBERTGenTxBerErrMask(module);
    uint32 shift   = ThaRegMapBERTGenTxBerErrShift(module);

    mFieldGet(regVal, mask, shift, uint32, &result);
    return (result == 0) ? cAtFalse : cAtTrue;
    }

static uint32 AlarmHistoryHelper(AtPrbsEngine self, eBool r2c)
    {
    uint32 regAddr;
    uint32 regVal;
    uint8 alarm;
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 groupId = ThaModulePrbsGroupIdGet(module, engineId);
    uint32 localId = ThaModulePrbsLocalIdGet(module, engineId);

    if (side == cAtPrbsSideTdm)
        {
        regAddr = ThaRegDemapBERTMonSticky(module, groupId, slice);
        regVal = Read(self, regAddr);
        mFieldGet(regVal, cThaRegDemapBERTMonRxPtmStkMask(localId), cThaRegDemapBERTMonRxPtmStkShift(localId), uint8, &alarm);

        if (r2c)
            Write(self, regAddr, cThaRegDemapBERTMonRxPtmStkMask(localId));
        return (alarm) ? cAtPrbsEngineAlarmTypeLossSync : cAtPrbsEngineAlarmTypeNone;
        }

    if (side == cAtPrbsSidePsn)
        {
        regAddr = ThaRegMapBERTMonSticky(module, groupId, slice);
        regVal = Read(self, regAddr);
        mFieldGet(regVal, cThaRegMapBERTMonTxPtmStkMask(localId), cThaRegMapBERTMonTxPtmStkShift(localId), uint8, &alarm);

        if (r2c)
            Write(self, regAddr, cThaRegMapBERTMonTxPtmStkMask(localId));
        return (alarm) ? cAtPrbsEngineAlarmTypeLossSync : cAtPrbsEngineAlarmTypeNone;
        }

    return 0;
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtTrue);
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryHelper(self, cAtFalse);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    if (prbsMode == cAtPrbsModePrbs31)
        return cAtFalse;
    if (prbsMode == cAtPrbsModePrbs7)
        return cAtFalse;
    if (prbsMode == cAtPrbsModeInvalid)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 PrbsModeSw2Hw(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbs9)
        return 0;
    if (prbsMode == cAtPrbsModePrbs11)
        return 1;
    if (prbsMode == cAtPrbsModePrbs20rStdO153)
        return 3;
    if (prbsMode == cAtPrbsModePrbs20StdO151)
        return 4;
    if (prbsMode == cAtPrbsModePrbs20QrssStdO151)
        return 5;
    if (prbsMode == cAtPrbsModePrbs15)
        return 2;
    if (prbsMode == cAtPrbsModePrbs23)
        return 6;
    if (prbsMode == cAtPrbsModePrbsSeq)
        return 19;
    if ((prbsMode == cAtPrbsModePrbsFixedPattern1Byte) ||
        (prbsMode == cAtPrbsModePrbsFixedPattern2Bytes) ||
        (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes) ||
        (prbsMode == cAtPrbsModePrbsFixedPattern4Bytes))
        return 18;

    return 0;
    }

static eAtPrbsMode PrbsModeHw2Sw(AtPrbsEngine self, uint32 prbsMode)
    {
    if (prbsMode == 0)
        return cAtPrbsModePrbs9;
    if (prbsMode == 1)
        return cAtPrbsModePrbs11;
    if (prbsMode == 3)
        return cAtPrbsModePrbs20rStdO153;
    if (prbsMode == 4)
        return cAtPrbsModePrbs20StdO151;
    if (prbsMode == 5)
        return cAtPrbsModePrbs20QrssStdO151;
    if (prbsMode == 2)
        return cAtPrbsModePrbs15;
    if (prbsMode == 6)
        return cAtPrbsModePrbs23;
    if (prbsMode == 19)
        return cAtPrbsModePrbsSeq;
    if (prbsMode == 18)
        {
        ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
        uint32 regAddr = ThaRegMapBERTGenMode(module, mHwEngine(self), mHwSlice(self));
        uint32 regVal = Read(self, regAddr);
        uint32 numPatternBits = mRegField(regVal, cThaRegMapBERTGenTxPtmPatBit);

        if (numPatternBits == 7)
            return cAtPrbsModePrbsFixedPattern1Byte;
        if (numPatternBits == 15)
            return cAtPrbsModePrbsFixedPattern2Bytes;
        if (numPatternBits == 23)
            return cAtPrbsModePrbsFixedPattern3Bytes;
        if (numPatternBits == 31)
            return cAtPrbsModePrbsFixedPattern4Bytes;
        }

    return cAtPrbsModeInvalid;
    }

static uint32 TxInvertModeSw2Hw(ThaPrbsEngine self, eBool invert)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (mMethodsGet(self)->TxInvertModeShouldSwap(self, AtPrbsEngineTxModeGet(engine)))
        return invert ? 0 : 1;

    return invert ? 1 : 0;
    }

static eBool TxInvertModeHw2Sw(ThaPrbsEngine self, uint32 invert)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (mMethodsGet(self)->TxInvertModeShouldSwap(self, AtPrbsEngineTxModeGet(engine)))
        return invert ? cAtFalse : cAtTrue;

    return invert ? cAtTrue : cAtFalse;
    }

static uint32 RxInvertModeSw2Hw(ThaPrbsEngine self, eBool invert)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (mMethodsGet(self)->RxInvertModeShouldSwap(self, AtPrbsEngineRxModeGet(engine)))
        return invert ? 0 : 1;

    return invert ? 1 : 0;
    }

static eBool RxInvertModeHw2Sw(ThaPrbsEngine self, uint32 invert)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (mMethodsGet(self)->RxInvertModeShouldSwap(self, AtPrbsEngineRxModeGet(engine)))
        return invert ? cAtFalse : cAtTrue;

    return invert ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = ThaRegMapBERTGenMode(module, mHwEngine(self), mHwSlice(self));
    uint32 regVal = Read(self, regAddr);
    mRegFieldSet(regVal, cThaRegMapBERTGenTxPtgInv, TxInvertModeSw2Hw((ThaPrbsEngine)self, invert));
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = ThaRegMapBERTGenMode(module, mHwEngine(self), mHwSlice(self));
    uint32 regVal = Read(self, regAddr);
    return TxInvertModeHw2Sw((ThaPrbsEngine)self, mRegField(regVal, cThaRegMapBERTGenTxPtgInv));
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (side == cAtPrbsSideTdm)
        {
        regAddr = ThaRegDemapBERTMonMode(module, engineId, slice);
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cThaRegDemapBERTMonRxPtmInv, RxInvertModeSw2Hw((ThaPrbsEngine)self, invert));
        }
    else if (side == cAtPrbsSidePsn)
        {
        regAddr = ThaRegMapBERTMonMode(module, engineId, slice);
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cThaRegMapBERTMonTxPtmInv, RxInvertModeSw2Hw((ThaPrbsEngine)self, invert));
        }
    else
        return cAtErrorRsrcNoAvail;

    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    uint32 regAddr;
    uint32 regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (side == cAtPrbsSideTdm)
        {
        regAddr = ThaRegDemapBERTMonMode(module, engineId, slice);
        regVal = Read(self, regAddr);
        return RxInvertModeHw2Sw((ThaPrbsEngine)self, mRegField(regVal, cThaRegDemapBERTMonRxPtmInv));
        }

    if (side == cAtPrbsSidePsn)
        {
        regAddr = ThaRegMapBERTMonMode(module, engineId, slice);
        regVal = Read(self, regAddr);
        return RxInvertModeHw2Sw((ThaPrbsEngine)self, mRegField(regVal, cThaRegMapBERTMonTxPtmInv));
        }

    return cAtFalse;
    }

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrors)
    {
    ThaModulePrbs module;
    uint32 regAddr, regVal;
    uint32 engineId;
    uint32 txIdInsMask, txIdInsShift;

    if (numErrors == 0)
        return cAtOk;

    if (numErrors != 1)
        return cAtErrorModeNotSupport;

    module  = ThaPrbsEngineModuleGet(self);
    engineId = mHwEngine(self);

    regAddr = ThaRegMapBERTGenSingleBitErrInsert(module, ThaModulePrbsGroupIdGet(module, engineId), mHwSlice(self));
    regVal  = Read(self, regAddr);
    txIdInsMask  = ThaRegMapBERTGenTxSingleErrMask(module);
    txIdInsShift = ThaRegMapBERTGenTxSingleErrShift(module);
    mRegFieldSet(regVal, txIdIns, ThaModulePrbsLocalIdGet(module, engineId));
    Write(self, regAddr, regVal);
    AtOsalUSleep(500);

    return cAtOk;
    }

static eBool ErrorForcingRateIsSupported(AtPrbsEngine self, eAtBerRate errorRate)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaModulePrbsErrorForcingRateIsSupported(module, errorRate);
    }

static uint32 BertGenErrorRateInsertRegAddress(ThaPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTGenErrorRateInsert(module, mHwEngine(self), mHwSlice(self));
    }

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    uint32 regAddr, regVal;
    uint32 mask, shift;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (!AtPrbsEngineErrorForcingRateIsSupported(self, errorRate))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(mThis(self))->BertGenErrorRateInsertRegAddress(mThis(self));
    regVal  = Read(self, regAddr);
    mask  = ThaRegMapBERTGenTxBerMdMask(module);
    shift = ThaRegMapBERTGenTxBerMdShift(module);
    mFieldIns(&regVal, mask, shift, ThaPrbsEngineErrorRateSw2Hw(mThis(self), errorRate));
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
    {
    uint32 mask, shift, berRate;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = mMethodsGet(mThis(self))->BertGenErrorRateInsertRegAddress(mThis(self));
    uint32 regVal = Read(self, regAddr);
    mask  = ThaRegMapBERTGenTxBerMdMask(module);
    shift = ThaRegMapBERTGenTxBerMdShift(module);
    mFieldGet(regVal, mask, shift, uint32, &berRate);

    return ThaPrbsEngineErrorRateHw2Sw(mThis(self), berRate);
    }

static uint8 NumPatternBits(eAtPrbsMode prbsMode)
    {
    if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)
        return 7;

    if (prbsMode == cAtPrbsModePrbsFixedPattern2Bytes)
        return 15;

    if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
        return 23;

    if (prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)
        return 31;

    return 31;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr, regVal;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    regAddr = ThaRegMapBERTGenMode(module, mHwEngine(self), mHwSlice(self));
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cThaRegMapBERTGenTxPtmPatBit, NumPatternBits(prbsMode));
    mRegFieldSet(regVal, cThaRegMapBERTGenTxPtgMode, PrbsModeSw2Hw(prbsMode));
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = ThaRegMapBERTGenMode(module, mHwEngine(self), mHwSlice(self));
    uint32 regVal = Read(self, regAddr);
    return PrbsModeHw2Sw(self, mRegField(regVal, cThaRegMapBERTGenTxPtgMode));
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    if (side == cAtPrbsSideTdm)
        {
        regAddr = ThaRegDemapBERTMonMode(module, engineId, slice);
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cThaRegDemapBERTMonRxPtmPatBit, NumPatternBits(prbsMode));
        mRegFieldSet(regVal, cThaRegDemapBERTMonRxPtgMode, PrbsModeSw2Hw(prbsMode));
        }
    else if (side == cAtPrbsSidePsn)
        {
        regAddr = ThaRegMapBERTMonMode(module, engineId, slice);
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cThaRegMapBERTMonTxPtmPatBit, NumPatternBits(prbsMode));
        mRegFieldSet(regVal, cThaRegMapBERTMonTxPtgMode, PrbsModeSw2Hw(prbsMode));
        }
    else
        return cAtErrorRsrcNoAvail;

    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (side == cAtPrbsSideTdm)
        {
        regAddr = ThaRegDemapBERTMonMode(module, engineId, slice);
        regVal  = Read(self, regAddr);
        return PrbsModeHw2Sw(self, mRegField(regVal, cThaRegDemapBERTMonRxPtgMode));
        }

    if (side == cAtPrbsSidePsn)
        {
        regAddr = ThaRegMapBERTMonMode(module, engineId, slice);
        regVal  = Read(self, regAddr);
        return PrbsModeHw2Sw(self, mRegField(regVal, cThaRegMapBERTMonTxPtgMode));
        }

    return cAtPrbsModeInvalid;
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = ThaRegMapBERTGenFixedPatternControl(module, mHwEngine(self), mHwSlice(self));
    Write(self, regAddr, fixedPattern);
    return cAtOk;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = ThaRegMapBERTGenFixedPatternControl(module, mHwEngine(self), mHwSlice(self));
    return Read(self, regAddr);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = ThaRegDemapBERTMonFixedPatternControl(module, mHwEngine(self), mHwSlice(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = ThaRegMapBERTMonFixedPatternControl(module, mHwEngine(self), mHwSlice(self));
    else
        return cAtErrorRsrcNoAvail;

    Write(self, regAddr, fixedPattern);
    return cAtOk;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = ThaRegDemapBERTMonFixedPatternControl(module, mHwEngine(self), mHwSlice(self));
    else if (side == cAtPrbsSidePsn)
        regAddr = ThaRegMapBERTMonFixedPatternControl(module, mHwEngine(self), mHwSlice(self));
    else
        return 0;

    return Read(self, regAddr);
    }

static uint64 Counter64BitGet(AtPrbsEngine self, uint16 counterType)
    {
    return AtPrbsCountersCounterGet(AtPrbsEnginePrbsCountersGet(self), counterType);
    }

static uint64 Counter64BitClear(AtPrbsEngine self, uint16 counterType)
    {
    return AtPrbsCountersCounterClear(AtPrbsEnginePrbsCountersGet(self), counterType);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return (uint32)Counter64BitGet(self, counterType);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return (uint32)Counter64BitClear(self, counterType);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);

    if ((counterType == cAtPrbsEngineCounterRxBit) ||
        (counterType == cAtPrbsEngineCounterRxBitError) ||
        (counterType == cAtPrbsEngineCounterRxSync) ||
        (counterType == cAtPrbsEngineCounterRxBitLoss) ||
        (counterType == cAtPrbsEngineCounterTxBit))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePrbsRet AllCountersIndirectLatch(AtPrbsEngine self)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    AtPrbsCounters counters = AtPrbsEnginePrbsCountersGet(self);
    uint32 rxErrorBit, rxSyncBit, rxLossBit, txBit;

    txBit = Read(self, ThaRegMapBERTGenGoodBitCounter(module, engineId, slice)) & cThaRegMapBERTGenGoodBitCounterMask;
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterTxBit, txBit);

    if (side == cAtPrbsSideTdm)
        {
        Write(self, ThaRegDemapBERTMonCounterLoadId(module, cGlobalIndex, slice), engineId);
        rxErrorBit  = Read(self, ThaRegDemapBERTMonErrorCounterLoading(module, cGlobalIndex, slice)) & cThaRegDemapBERTMonErrorCounterLoadingMask;
        rxSyncBit   = Read(self, ThaRegDemapBERTMonGoodBitCounterLoading(module, cGlobalIndex, slice)) & cThaRegDemapBERTMonGoodBitCounterLoadingMask;
        rxLossBit   = Read(self, ThaRegDemapBERTMonLossBitCounterLoading(module, cGlobalIndex, slice)) & cThaRegDemapBERTMonLossBitCounterLoadingMask;

        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxSyncBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLossBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxLossBit + rxSyncBit);
        return cAtOk;
        }

    if (side == cAtPrbsSidePsn)
        {
        Write(self, ThaRegMapBERTMonCounterLoadId(module, cGlobalIndex, slice), engineId);
        rxErrorBit = Read(self, ThaRegMapBERTMonErrorCounterLoading(module, cGlobalIndex, slice)) & cThaRegMapBERTMonErrorCounterLoadingMask;
        rxSyncBit  = Read(self, ThaRegMapBERTMonGoodBitCounterLoading(module, cGlobalIndex, slice)) & cThaRegMapBERTMonGoodBitCounterLoadingMask;
        rxLossBit  = Read(self, ThaRegMapBERTMonLossBitCounterLoading(module, cGlobalIndex, slice)) & cThaRegMapBERTMonLossBitCounterLoadingMask;

        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxSyncBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLossBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxLossBit + rxSyncBit);
        return cAtOk;
        }

    return cAtErrorRsrcNoAvail;
    }

static eAtModulePrbsRet TdmSideAllCountersDirectLatch(AtPrbsEngine self)
    {
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    AtPrbsCounters counters = AtPrbsEnginePrbsCountersGet(self);
    uint32 rxErrorBit, rxSyncBit, rxLossBit, txBit;

    txBit = Read(self, ThaRegMapBERTGenGoodBitCounter(module, engineId, slice)) & cThaRegMapBERTGenGoodBitCounterMask;
    rxErrorBit = Read(self, ThaRegDemapBERTMonErrorCounter(module, engineId, slice)) & cThaRegDemapBERTMonErrorCounterLoadingMask;
    rxSyncBit  = Read(self, ThaRegDemapBERTMonGoodBitCounter(module, engineId, slice)) & cThaRegDemapBERTMonGoodBitCounterLoadingMask;
    rxLossBit  = Read(self, ThaRegDemapBERTMonLossBitCounter(module, engineId, slice)) & cThaRegDemapBERTMonLossBitCounterLoadingMask;

    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterTxBit, txBit);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBit);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxSyncBit);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLossBit);
    AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxLossBit + rxSyncBit);

    return cAtOk;
    }

static eAtModulePrbsRet AllCountersDirectLatch(AtPrbsEngine self)
    {
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint32 engineId = mHwEngine(self);
    uint8 slice = mHwSlice(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    AtPrbsCounters counters = AtPrbsEnginePrbsCountersGet(self);
    uint32 rxErrorBit, rxSyncBit, rxLossBit, txBit;

    if (side == cAtPrbsSideTdm)
        return TdmSideAllCountersDirectLatch(self);

    if (side == cAtPrbsSidePsn)
        {
        txBit      = Read(self, ThaRegMapBERTGenGoodBitCounter(module, engineId, slice)) & cThaRegMapBERTGenGoodBitCounterMask;;
        rxErrorBit = Read(self, ThaRegMapBERTMonErrorCounter(module, engineId, slice)) & cThaRegDemapBERTMonErrorCounterLoadingMask;
        rxSyncBit  = Read(self, ThaRegMapBERTMonGoodBitCounter(module, engineId, slice)) & cThaRegDemapBERTMonGoodBitCounterLoadingMask;
        rxLossBit  = Read(self, ThaRegMapBERTMonLossBitCounter(module, engineId, slice)) & cThaRegDemapBERTMonLossBitCounterLoadingMask;

        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterTxBit, txBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitError, rxErrorBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxSync, rxSyncBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBitLoss, rxLossBit);
        AtPrbsCountersCounterUpdate(counters, cAtPrbsEngineCounterRxBit, rxLossBit + rxSyncBit);

        return cAtOk;
        }

    return cAtErrorRsrcNoAvail;
    }

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    ThaPrbsEngine engine = mThis(self);

    AtUnused(clear);

    if (ThaModulePrbsCounterReadIndirectly(module, engine))
        return AllCountersIndirectLatch(self);

    return AllCountersDirectLatch(self);
    }

static eBool GeneratingSideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);

    switch (side)
        {
        case cAtPrbsSideTdm: return cAtTrue;
        case cAtPrbsSidePsn: return mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self));
        case cAtPrbsSideUnknown:
        default:
            return cAtFalse;
        }
    }

static eBool ShouldReapplyFixPattern(AtPrbsEngine self, eAtPrbsMode mode)
    {
    if (AtPrbsEngineCanUseAnyFixedPattern(self))
        return cAtTrue;

    switch ((uint32)mode)
        {
        case cAtPrbsModePrbsFixedPattern1Byte : return cAtTrue;
        case cAtPrbsModePrbsFixedPattern2Bytes: return cAtTrue;
        case cAtPrbsModePrbsFixedPattern3Bytes: return cAtTrue;
        case cAtPrbsModePrbsFixedPattern4Bytes: return cAtTrue;
        default: return cAtFalse;
        }
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    eAtModulePrbsRet ret;
    eBool txEnabled, txInverted;
    uint32 txFixedPattern;
    eAtPrbsMode currentPrbsMode;
    
    if (!GeneratingSideIsSupported(self, side))
        return cAtErrorModeNotSupport;
    
    if (AtPrbsEngineGeneratingSideGet(self) == side)
        return cAtOk;

    mThis(self)->softwareGeneratingSide = side;
    side = cAtPrbsSideTdm; /* It always generates to TDM */

    txEnabled = mMethodsGet(mThis(self))->HwTxTdmIsEnabled(mThis(self));

    currentPrbsMode = AtPrbsEngineTxModeGet(self);
    txFixedPattern = AtPrbsEngineTxFixedPatternGet(self);
    txInverted = AtPrbsEngineTxIsInverted(self);

    AtPrbsEngineTxEnable(self, cAtFalse);
    AtPrbsEngineErrorForce(self, cAtFalse);

    /* Call super to save data-base */
    ret  = m_AtPrbsEngineMethods->GeneratingSideSet(self, side);

    /* Configure */
    ret |= AtPrbsEngineTxModeSet(self, currentPrbsMode);
    if (ShouldReapplyFixPattern(self, currentPrbsMode))
        ret |= AtPrbsEngineTxFixedPatternSet(self, txFixedPattern);
    ret |= AtPrbsEngineTxInvert(self, txInverted);
    ret |= AtPrbsEngineTxEnable(self, txEnabled);

    return ret;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    if (mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)))
        {
        eAtPrbsSide side = mThis(self)->softwareGeneratingSide;
        if (side == cAtPrbsSidePsn)
            return side;
        }

    return m_AtPrbsEngineMethods->GeneratingSideGet(self);
    }

static eAtModulePrbsRet MonitoringSideCheck(AtPrbsEngine self, eAtPrbsSide side)
    {
    if (!AtPrbsEngineIsValidSide(side))
        return cAtErrorInvlParm;

    if ((side == cAtPrbsSidePsn) && (AtChannelBoundPwGet(AtPrbsEngineChannelGet(self)) == NULL))
        {
        if (mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)))
            return cAtOk;
        return cAtModulePrbsErrorNoBoundPw;
        }

    return cAtOk;
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    uint32 ret;
    eBool rxEnabled, rxInverted;
    uint32 rxFixedPattern;
    eAtPrbsMode currentPrbsMode;

    ret = MonitoringSideCheck(self, side);
    if (ret != cAtOk)
        return ret;

    if (side == AtPrbsEngineMonitoringSideGet(self))
        return cAtOk;

    if (ShouldPreventConfigureToHw(self))
        return m_AtPrbsEngineMethods->MonitoringSideSet(self, side);

    rxEnabled = AtPrbsEngineRxIsEnabled(self);
    currentPrbsMode = AtPrbsEngineRxModeGet(self);
    rxFixedPattern = AtPrbsEngineRxFixedPatternGet(self);
    rxInverted = AtPrbsEngineRxIsInverted(self);

    AtPrbsEngineRxEnable(self, cAtFalse);

    /* Call super to save data-base */
    ret = m_AtPrbsEngineMethods->MonitoringSideSet(self, side);

    /* Configure */
    ret |= AtPrbsEngineRxModeSet(self, currentPrbsMode);
    if (ShouldReapplyFixPattern(self, currentPrbsMode))
        ret |= AtPrbsEngineRxFixedPatternSet(self, rxFixedPattern);
    ret |= AtPrbsEngineRxInvert(self, rxInverted);
    ret |= AtPrbsEngineRxEnable(self, rxEnabled);

    if (CommonBertMonRegister(self))
        ret |= BertMonSideSelect(self, side);

    return ret;
    }

static eBool BitOrderIsSupported(eAtPrbsBitOrder order)
    {
    if (order == cAtPrbsBitOrderMsb)
        return cAtTrue;
    if (order == cAtPrbsBitOrderLsb)
        return cAtTrue;
    return cAtFalse;
    }

static eAtModulePrbsRet TxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr, regVal;

    if (!BitOrderIsSupported(order))
        return cAtErrorModeNotSupport;

    regAddr = ThaRegMapBERTGenMode(module, mHwEngine(self), mHwSlice(self));
    regVal  = Read(self, regAddr);
    mRegFieldSet(regVal, cThaRegMapBERTGenTxPtgSwap, (order == cAtPrbsBitOrderMsb) ? 0 : 1);
    Write(self, regAddr, regVal);
    return cAtOk;
    }

static eAtPrbsBitOrder TxBitOrderGet(AtPrbsEngine self)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);
    uint32 regAddr = ThaRegMapBERTGenMode(module, mHwEngine(self), mHwSlice(self));
    uint32 regVal = Read(self, regAddr);
    return (mRegField(regVal, cThaRegMapBERTGenTxPtgSwap) == 0) ? cAtPrbsBitOrderMsb : cAtPrbsBitOrderLsb;
    }

static eAtModulePrbsRet RxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order)
    {
    uint32 regAddr;
    uint32 regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (!BitOrderIsSupported(order))
        return cAtErrorModeNotSupport;

    if ((side == cAtPrbsSideTdm) || (side == cAtPrbsSidePsn))
        {
        regAddr = ThaRegDemapBERTMonMode(module, mHwEngine(self), mHwSlice(self));
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cThaRegDemapBERTMonRxPtmSwap, (order == cAtPrbsBitOrderMsb) ? 0 : 1);
        Write(self, regAddr, regVal);

        regAddr = ThaRegMapBERTMonMode(module, mHwEngine(self), mHwSlice(self));
        regVal = Read(self, regAddr);
        mRegFieldSet(regVal, cThaRegMapBERTMonTxPtmSwap, (order == cAtPrbsBitOrderMsb) ? 0 : 1);
        Write(self, regAddr, regVal);
        }
    else
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static eAtPrbsBitOrder RxBitOrderGet(AtPrbsEngine self)
    {
    uint32 regAddr, regVal;
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    ThaModulePrbs module = ThaPrbsEngineModuleGet(self);

    if (side == cAtPrbsSideTdm)
        {
        regAddr = ThaRegDemapBERTMonMode(module, mHwEngine(self), mHwSlice(self));
        regVal = Read(self, regAddr);
        return (mRegField(regVal, cThaRegDemapBERTMonRxPtmSwap) == 0) ? cAtPrbsBitOrderMsb : cAtPrbsBitOrderLsb;
        }

    if (side == cAtPrbsSidePsn)
        {
        regAddr = ThaRegMapBERTMonMode(module, mHwEngine(self), mHwSlice(self));
        regVal = Read(self, regAddr);
        return (mRegField(regVal, cThaRegMapBERTMonTxPtmSwap) == 0) ? cAtPrbsBitOrderMsb : cAtPrbsBitOrderLsb;
        }

    return cAtPrbsBitOrderUnknown;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    eAtRet ret = cAtOk;
    /* Can not change both generating and monitoring side to PSN side */
    if (!AtPrbsEngineSideIsSupported(self, side))
        return cAtErrorModeNotSupport;

    ret |= AtPrbsEngineGeneratingSideSet(self, side);
    ret |= AtPrbsEngineMonitoringSideSet(self, side);

    if (ret == cAtOk)
        return m_AtPrbsEngineMethods->SideSet(self, side);

    return ret;
    }

static void AllCountersClear(AtPrbsEngine self)
    {
    AtPrbsEngineAllCountersLatchAndClear(self, cAtTrue);
    AtPrbsEngineCounter64BitClear(self, cAtPrbsEngineCounterRxBit);
    AtPrbsEngineCounter64BitClear(self, cAtPrbsEngineCounterRxBitError);
    AtPrbsEngineCounter64BitClear(self, cAtPrbsEngineCounterRxSync);
    AtPrbsEngineCounter64BitClear(self, cAtPrbsEngineCounterRxBitLoss);
    AtPrbsEngineCounter64BitClear(self, cAtPrbsEngineCounterTxBit);
    }

static uint32 HwEngineId(ThaPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;
    ThaModulePrbs module = ThaPrbsEngineModuleGet(engine);
    return mMethodsGet(module)->HwEngineId(module, AtPrbsEngineIdGet(engine), AtPrbsEngineChannelGet(engine));
    }

static uint8 HwSliceId(ThaPrbsEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ErrorRateSw2Hw(ThaPrbsEngine self, eAtBerRate errorRate)
    {
    return ThaModulePrbsErrorRateSw2Hw(ThaPrbsEngineModuleGet((AtPrbsEngine)self), errorRate);
    }

static eAtBerRate ErrorRateHw2Sw(ThaPrbsEngine self, uint32 errorRate)
    {
    return ThaModulePrbsErrorRateHw2Sw(ThaPrbsEngineModuleGet((AtPrbsEngine)self), errorRate);
    }

static uint32 TdmMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegDemapBERTMonStatus(module, engineId, slice);
    }

static uint32 PsnMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    ThaModulePrbs module = ThaPrbsEngineModuleGet((AtPrbsEngine)self);
    return ThaRegMapBERTMonStatus(module, engineId, slice);
    }

static eBool HwAlarmOnSideGet(AtPrbsEngine self, uint32 addressOnSide, eAtPrbsSide side)
    {
    uint32 alarmRegFieldMask = cBit1_0;

    if ((side != cAtPrbsSideTdm) && (side != cAtPrbsSidePsn))
        return 0;

    /* PSN monitoring, if there is no PW, the fake alarm error is return */
    if (side == cAtPrbsSidePsn)
        {
        AtChannel channel = AtPrbsEngineChannelGet(self);
        AtPw pw = AtChannelBoundPwGet(channel);

        if ((pw == NULL) || !AtChannelIsEnabled((AtChannel)pw))
            return cAtPrbsEngineAlarmTypeLossSync;
        }

    return ((Read(self, addressOnSide) & alarmRegFieldMask) == 3) ? cAtPrbsEngineAlarmTypeNone : cAtPrbsEngineAlarmTypeLossSync;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddr = 0;
    uint32 engineId = mHwEngine(self);
    uint32 sliceId = mHwSlice(self);
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);

    if (side == cAtPrbsSideTdm)
        regAddr = mMethodsGet(mThis(self))->TdmMonitoringStatusRegAddress(mThis(self), engineId, sliceId);

    if (side == cAtPrbsSidePsn)
        regAddr = mMethodsGet(mThis(self))->PsnMonitoringStatusRegAddress(mThis(self), engineId, sliceId);

    return HwAlarmOnSideGet(self, regAddr, side);
    }

static const char *Side2String(eAtPrbsSide side)
    {
    if (side == cAtPrbsSidePsn) return "psn";
    if (side == cAtPrbsSideTdm) return "tdm";
    return "unknown";
    }

static void Debug(AtPrbsEngine self)
    {
    m_AtPrbsEngineMethods->Debug(self);

    AtPrintc(cSevNormal, "* HW engine ID: %d\r\n", mThis(self)->hwEngineId);

    if (mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)))
        {
        AtChannel channel = AtPrbsEngineChannelGet(self);
        AtPrintc(cSevNormal, "* SW PSN generating:\r\n");
        AtPrintc(cSevNormal, "    * Side: %s\r\n", Side2String(mThis(self)->softwareGeneratingSide));
        AtPrintc(cSevNormal, "    * loopbackListened: %s\r\n", mThis(self)->loopbackListened ? "listened" : "not yet");
        AtPrintc(cSevNormal, "    * listeningEnabled: %s\r\n", mThis(self)->listeningEnabled ? "enabled" : "disabled");
        if (mThis(self)->cachedLoopbackMode == cInvalidLoopbackMode)
            AtPrintc(cSevNormal, "    * circuitLoopbackMode: not cached\r\n");
        else
            AtPrintc(cSevNormal, "    * circuitLoopbackMode: %s\r\n", AtChannelLoopbackMode2String(channel, mThis(self)->cachedLoopbackMode));
        }
    }

static void ResourceRemove(ThaPrbsEngine self)
    {
    ThaModulePrbsResourceRemove(ThaPrbsEngineModuleGet((AtPrbsEngine)self), self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPrbsEngine object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(hwEngineId);
    mEncodeUInt(softwareGeneratingSide);
    mEncodeUInt(cachedLoopbackMode);
    mEncodeUInt(loopbackListened);
    mEncodeUInt(listeningEnabled);
    mEncodeUInt(psnRxEnabled);
    mEncodeUInt(isHwTxEnabled);
    mEncodeUInt(isHwRxTdmEnabled);
    mEncodeObjectDescription(module);

    mEncodeNone(counters);
    }

static eBool TxInvertModeShouldSwap(ThaPrbsEngine self, eAtPrbsMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtFalse;
    }

static eBool RxInvertModeShouldSwap(ThaPrbsEngine self, eAtPrbsMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtFalse;
    }

static eBool TxErrorInjectionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    if (side != cAtPrbsSidePsn)
        return cAtTrue;

    return mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self));
    }

static eBool ErrorForcingIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool InversionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void Restore(ThaPrbsEngine self)
    {
    uint32 i;
    uint32 hwChannelId;
    uint32 freeHwId = cInvalidUint32;
    uint32 channelId = ThaPrbsEngineChannelId(self);
    AtPrbsEngine engine = (AtPrbsEngine)self;
    ThaModulePrbs module = (ThaModulePrbs)ThaPrbsEngineModuleGet(engine);

    for (i = 0; i < ThaModulePrbsMaxNumLoPrbsEngine(module); i++)
        {
        uint32 regAddr = ThaRegMapBERTGenSelectedChannel(module, i, 0);
        uint32 regVal = Read(engine, regAddr);
        uint32 mask = ThaRegMapBERTGenTxPtgIdMask(module);
        uint32 shift = AtRegMaskToShift(mask);
        mFieldGet(regVal, mask, shift, uint32, &hwChannelId);
        if (channelId == hwChannelId)
            freeHwId = i;
        }

    if (freeHwId == cInvalidUint32)
        {
        AtChannelLog(AtPrbsEngineChannelGet(engine), cAtLogLevelCritical, AtSourceLocation, "Can not find BERT HW ID during HA restore\r\n");
        return;
        }

    ThaModulePrbsHwEngineIdInUseSet(module, self, freeHwId);
    }

static ThaModulePrbs PrbsModule(ThaPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    return (ThaModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eBool FlexiblePsnSide(ThaPrbsEngine self)
    {
    return ThaModulePrbsFlexiblePsnSide(PrbsModule(self));
    }

static uint8 CircuitLocalLoopbackMode(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAtLoopbackModeLocal;
    }

static void Delete(AtObject self)
    {
    if (mMethodsGet(mThis(self))->FlexiblePsnSide(mThis(self)))
        CircuitLoopbackListen((AtPrbsEngine)self, cAtFalse);

    m_AtObjectMethods->Delete(self);
    }

static AtChannel CircuitForLoopbackHandling(ThaPrbsEngine self)
    {
    return AtPrbsEngineChannelGet((AtPrbsEngine)self);
    }

static uint32 TdmSideAlarmGet(AtPrbsEngine self)
    {
    uint32 engineId = mHwEngine(self);
    uint32 sliceId = mHwSlice(self);
    eAtPrbsSide side = AtPrbsEngineMonitoringSideGet(self);
    uint32 regAddr = mMethodsGet(mThis(self))->TdmMonitoringStatusRegAddress(mThis(self), engineId, sliceId);
    return HwAlarmOnSideGet(self, regAddr, side);
    }

static eAtRet HwCleanup(AtPrbsEngine self)
    {
    eAtRet ret;
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    ret = m_AtPrbsEngineMethods->HwCleanup(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtPrbsEngineEnable(self, cAtFalse);
    ret |= AtPrbsEngineErrorForce(self, cAtFalse);

    ThaPrbsEngineResourceRemove(mThis(self));
    ThaPrbsEngineTxChannelIdSet(mThis(self), cInvalidUint32);

    if (mMethodsGet(engine)->ChannelizedPrbsAllowed(engine) && mMethodsGet(engine)->IsChannelized(engine))
        {
        ret |= mMethodsGet(engine)->Channelize(engine);
        ret |= mMethodsGet(engine)->AllSubCircuitsPwSquel(engine, cAtFalse);
        }

    return ret;
    }

static eAtRet Channelize(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet Unchannelize(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet AllSubCircuitsPwSquel(ThaPrbsEngine self, eBool forced)
    {
    AtUnused(self);
    AtUnused(forced);
    return cAtErrorNotImplemented;
    }

static eBool IsChannelized(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ChannelizedPrbsAllowed(ThaPrbsEngine self)
    {
    /* Let concrete channels determine it they have channelize cases */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PwSquel(AtPw pw, eBool squel)
    {
    return AtChannelEnable((AtChannel)pw, squel ? cAtFalse : cAtTrue);
    }

static eBool IsDe1(AtPdhChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

    if ((channelType == cAtPdhChannelTypeE1) ||
        (channelType == cAtPdhChannelTypeDs1))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet AllCesopSquel(AtPdhChannel de1, eBool squel)
    {
    AtIterator iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)de1);
    AtChannel nxDs0;
    eAtRet ret = cAtOk;

    while ((nxDs0 = (AtChannel)AtIteratorNext(iterator)) != NULL)
        {
        AtPw pw = AtChannelBoundPwGet(nxDs0);
        if (pw == NULL)
            continue;

        ret |= AtChannelEnable((AtChannel)pw, squel ? cAtFalse : cAtTrue);
        }

    AtObjectDelete((AtObject)iterator);

    return ret;
    }

static eAtRet PwDidBind(ThaPrbsEngine self, AtPw pw)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (pw == NULL)
        return cAtOk;

    if (AtPrbsEngineMonitoringSideGet(engine) != cAtPrbsSidePsn)
        return cAtOk;

    return mMethodsGet(self)->HwRxPsnEnable(self, self->psnRxEnabled);
    }

static eAtRet PwWillBind(ThaPrbsEngine self, AtPw pw)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (pw)
        return cAtOk;

    if (AtPrbsEngineMonitoringSideGet(engine) != cAtPrbsSidePsn)
        return cAtOk;

    return mMethodsGet(self)->HwRxPsnEnable(self, cAtFalse);
    }

static AtPrbsCounters CountersObjectCreate(AtPrbsEngine self)
    {
    AtUnused(self);
    return (AtPrbsCounters)ThaPrbsCountersNew();
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, SeparateTwoDirections);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInject);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, Counter64BitGet);
        mMethodOverride(m_AtPrbsEngineOverride, Counter64BitClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxBitOrderGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInjectionIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, InversionIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingRateIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, HwCleanup);
        mMethodOverride(m_AtPrbsEngineOverride, CountersObjectCreate);
        mMethodOverride(m_AtPrbsEngineOverride, AllCountersClear);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static void MethodsInit(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ChannelId);
        mMethodOverride(m_methods, TxChannelIdSet);
        mMethodOverride(m_methods, HwEngineId);
        mMethodOverride(m_methods, HwSliceId);
        mMethodOverride(m_methods, PartId);
        mMethodOverride(m_methods, ErrorRateSw2Hw);
        mMethodOverride(m_methods, ErrorRateHw2Sw);
        mMethodOverride(m_methods, HwDefault);
        mMethodOverride(m_methods, ResourceRemove);
        mMethodOverride(m_methods, RxChannelId);
        mMethodOverride(m_methods, TimeslotBitMap);
        mMethodOverride(m_methods, TimeslotDefaultSet);
        mMethodOverride(m_methods, TdmSideRxEnable);

        /* To work with PSN genrating */
        mMethodOverride(m_methods, FlexiblePsnSide);
        mMethodOverride(m_methods, CircuitLocalLoopbackMode);
        mMethodOverride(m_methods, CircuitForLoopbackHandling);

        mMethodOverride(m_methods, BertGenSelectedChannelRegAddress);
        mMethodOverride(m_methods, BertGenEnableMask);
        mMethodOverride(m_methods, BertGenChannelIdMask);

        mMethodOverride(m_methods, BertMonTdmSelectedChannelRegAddress);
        mMethodOverride(m_methods, BertMonTdmEnableMask);
        mMethodOverride(m_methods, BertMonTdmChannelIdMask);

        mMethodOverride(m_methods, BertMonPsnSelectedChannelRegAddress);
        mMethodOverride(m_methods, BertMonPsnEnableMask);
        mMethodOverride(m_methods, BertMonPsnChannelIdMask);

        mMethodOverride(m_methods, BertGenErrorRateInsertRegAddress);
        mMethodOverride(m_methods, TdmMonitoringStatusRegAddress);
        mMethodOverride(m_methods, PsnMonitoringStatusRegAddress);

        mMethodOverride(m_methods, TxInvertModeShouldSwap);
        mMethodOverride(m_methods, RxInvertModeShouldSwap);

        mMethodOverride(m_methods, Restore);

        mMethodOverride(m_methods, IsChannelized);
        mMethodOverride(m_methods, ChannelizedPrbsAllowed);
        mMethodOverride(m_methods, Channelize);
        mMethodOverride(m_methods, Unchannelize);
        mMethodOverride(m_methods, AllSubCircuitsPwSquel);

        mMethodOverride(m_methods, HwTxTdmEnable);
        mMethodOverride(m_methods, HwTxTdmIsEnabled);
        mMethodOverride(m_methods, HwRxPsnEnable);
        }

    mMethodsSet(self, &m_methods);
    }

AtPrbsEngine ThaPrbsEngineObjectInit(AtPrbsEngine self, AtChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->module = (ThaModulePrbs)AtDeviceModuleGet((AtDevice)AtChannelDeviceGet(channel), cAtModulePrbs);
    mThis(self)->softwareGeneratingSide = cAtPrbsSideUnknown;
    mThis(self)->cachedLoopbackMode = cInvalidLoopbackMode;

    return self;
    }

ThaModulePrbs ThaPrbsEngineModuleGet(AtPrbsEngine self)
    {
    return self ? mThis(self)->module : NULL;
    }

uint32 ThaPrbsEngineHwEngineId(ThaPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->HwEngineId(self);
    return cBit31_0;
    }

uint8 ThaPrbsEngineHwSliceId(ThaPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->HwSliceId(self);
    return cBit7_0;
    }

uint32 ThaPrbsEngineErrorRateSw2Hw(ThaPrbsEngine self, eAtBerRate errorRate)
	{
    return mMethodsGet(self)->ErrorRateSw2Hw(self, errorRate);
	}

eAtBerRate ThaPrbsEngineErrorRateHw2Sw(ThaPrbsEngine self, uint32 errorRate)
	{
    return mMethodsGet(self)->ErrorRateHw2Sw(self, errorRate);
	}

eBool ThaPrbsEngineBitOrderIsSupported(eAtPrbsBitOrder order)
    {
    return BitOrderIsSupported(order);
    }

void ThaPrbsEngineHwEngineIdSet(ThaPrbsEngine self, uint32 hwEngineId)
    {
    if (self)
        mThis(self)->hwEngineId = hwEngineId;
    }

uint32 ThaPrbsEngineHwEngineIdGet(ThaPrbsEngine self)
    {
    if (self)
        return mThis(self)->hwEngineId;
    return 0;
    }

void ThaPrbsEngineResourceRemove(ThaPrbsEngine self)
    {
    mMethodsGet(self)->ResourceRemove(self);
    }

uint32 ThaPrbsEngineChannelId(ThaPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->ChannelId(self);
    return 0;
    }

void ThaPrbsEngineTxChannelIdSet(ThaPrbsEngine self, uint32 channelId)
    {
    if (self)
        mMethodsGet(self)->TxChannelIdSet(self, channelId);
    }

void ThaPrbsEngineRestore(ThaPrbsEngine self)
    {
    if (self)
        mMethodsGet(self)->Restore(self);
    }

uint32 ThaPrbsEngineTxInvertModeSw2Hw(ThaPrbsEngine self, eBool invert)
    {
    if (self)
        return TxInvertModeSw2Hw(self, invert);
    return 0;
    }

eBool ThaPrbsEngineTxInvertModeHw2Sw(ThaPrbsEngine self, uint32 invert)
    {
    if (self)
        return TxInvertModeHw2Sw(self, invert);
    return cAtFalse;
    }

uint32 ThaPrbsEngineRxInvertModeSw2Hw(ThaPrbsEngine self, eBool invert)
    {
    if (self)
        return RxInvertModeSw2Hw(self, invert);
    return 0;
    }

eBool ThaPrbsEngineRxInvertModeHw2Sw(ThaPrbsEngine self, uint32 invert)
    {
    if (self)
        return RxInvertModeHw2Sw(self, invert);
    return cAtFalse;
    }

eAtRet ThaPrbsEngineRxTdmSideEnable(ThaPrbsEngine self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TdmSideRxEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPrbsEngineRxTdmSideIsEnabled(AtPrbsEngine self)
    {
    if (self)
        return RxTdmSideIsEnabled(self);
    return cAtFalse;
    }

eAtModulePrbsRet ThaPrbsEngineTdmSideAllCountersDirectLatch(AtPrbsEngine self)
    {
    if (self)
        return TdmSideAllCountersDirectLatch(self);
    return cAtErrorNullPointer;
    }

uint32 ThaPrbsEngineTdmSideAlarmGet(AtPrbsEngine self)
    {
    if (self)
        return TdmSideAlarmGet(self);
    return 0;
    }

uint32 ThaPrbsEngineTimeslotBitMap(ThaPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->TimeslotBitMap(self);
    return 0;
    }

eAtRet ThaPrbsEngineAllSdhSubChannelsPwSquel(AtSdhChannel vc, eBool squel)
    {
    uint8 subChannel_i;
    AtChannel mapChannel;
    eAtRet ret = cAtOk;
    AtPw pw = AtChannelBoundPwGet((AtChannel)vc);

    if (pw)
        return PwSquel(pw, squel);

    mapChannel = AtSdhChannelMapChannelGet(vc);
    if (mapChannel)
        return ThaPrbsEngineAllPdhSubChannelsPwSquel((AtPdhChannel)mapChannel, squel);

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(vc); subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(vc, subChannel_i);
        ret |= ThaPrbsEngineAllSdhSubChannelsPwSquel(subChannel, squel);
        }

    return ret;
    }

eAtRet ThaPrbsEngineAllPdhSubChannelsPwSquel(AtPdhChannel pdhChannel, eBool squel)
    {
    uint8 subChannel_i;
    eAtRet ret = cAtOk;
    AtPw pw = AtChannelBoundPwGet((AtChannel)pdhChannel);

    if (pw)
        return PwSquel(pw, squel);

    if (IsDe1(pdhChannel))
        return AllCesopSquel(pdhChannel, squel);

    for (subChannel_i = 0; subChannel_i < AtPdhChannelNumberOfSubChannelsGet(pdhChannel); subChannel_i++)
        {
        AtPdhChannel subChannel = AtPdhChannelSubChannelGet(pdhChannel, subChannel_i);
        ret |= ThaPrbsEngineAllPdhSubChannelsPwSquel(subChannel, squel);
        }

    return ret;
    }

eAtRet ThaPrbsEngineChannelizedPrbsDefaultSet(ThaPrbsEngine self)
    {
    if (self)
        return ChannelizedPrbsDefaultSet(self);
    return cAtErrorObjectNotExist;
    }

eAtModulePrbsRet ThaPrbsEngineDefaultClassInit(ThaPrbsEngine self)
    {
    return Init((AtPrbsEngine)self);
    }

eAtModulePrbsRet ThaPrbsEngineDefaultClassMonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    return MonitoringSideSet(self, side);
    }

eAtModulePrbsRet ThaPrbsEngineDefaultClassRxEnable(AtPrbsEngine self, eBool enable)
    {
    return RxEnable(self, enable);
    }

eBool ThaPrbsEngineDefaultClassRxIsEnabled(AtPrbsEngine self)
    {
    return RxIsEnabled(self);
    }

uint32 ThaPrbsEngineDefaultClassAlarmGet(AtPrbsEngine self)
    {
    return AlarmGet(self);
    }

eAtRet ThaPrbsEnginePwDidBind(ThaPrbsEngine self, AtPw pw)
    {
    if (self)
        return PwDidBind(self, pw);
    return cAtOk; /* DONOT return error code. Only care error code when engine is valid */
    }

eAtRet ThaPrbsEnginePwWillBind(ThaPrbsEngine self, AtPw pw)
    {
    if (self)
        return PwWillBind(self, pw);
    return cAtOk; /* DONOT return error code. Only care error code when engine is valid */
    }

eBool ThaPrbsEngineFlexiblePsnSide(ThaPrbsEngine self)
    {
    if (self)
        return mMethodsGet(self)->FlexiblePsnSide(self);
    return cAtFalse;
    }

uint32 ThaPrbsEngineBertMonSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    if (self)
        return BertMonSelectedChannelRegAddress(self);
    return cInvalidUint32;
    }

uint32 ThaPrbsEngineBertMonSelectMask(ThaPrbsEngine self)
    {
    if (self)
        return BertMonSelectMask(self);
    return 0;
    }

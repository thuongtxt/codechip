/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : .c
 *
 * Created Date: Aug 24, 2013
 *
 * Description : PRBS engine for DE1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPrbsEnginePdhChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPrbsEnginePdhChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPrbsEnginePdhChannelMethods m_methods;

/* Override */
static tAtPrbsEngineMethods  m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods m_ThaPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsUnframed(ThaPrbsEnginePdhChannel self, uint16 frameMode)
    {
    AtUnused(self);
    AtUnused(frameMode);
    return cAtFalse;
    }

static eBool CanDetectLosByAllZeroPattern(ThaPrbsEnginePdhChannel self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(channel);
    return mMethodsGet(self)->ModuleCanDetectLosByAllZeroPattern(self, module);
    }

static eBool ModuleCanDetectLosByAllZeroPattern(ThaPrbsEnginePdhChannel self, ThaModulePdh module)
    {
    AtUnused(self);
    AtUnused(module);
    return cAtTrue;
    }

static eBool DefaultLosDetection(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(channel);
    return mMethodsGet(mThis(self))->ModuleCanDetectLosByAllZeroPattern(mThis(self), module);
    }

static eBool ShouldControlLosDetection(AtPrbsEngine self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtPrbsEngineChannelGet(self);

    if (!mMethodsGet(mThis(self))->IsUnframed(mThis(self), AtPdhChannelFrameTypeGet(channel)))
        return cAtFalse;

    return CanDetectLosByAllZeroPattern(mThis(self));
    }

static eAtRet LosDetectionEnable(AtPrbsEngine self, eBool enabled)
    {
    AtPdhChannel channel = (AtPdhChannel)AtPrbsEngineChannelGet(self);
    return AtPdhChannelLosDetectionEnable(channel, enabled);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtRet ret;

    ret = m_AtPrbsEngineMethods->RxFixedPatternSet(self, fixedPattern);
    if (ret != cAtOk)
        return ret;

    if (AtPrbsEngineIsEnabled(self) && ShouldControlLosDetection(self))
        {
        eBool losDetectionEnabled = DefaultLosDetection(self);
        if (fixedPattern == 0)
            losDetectionEnabled = cAtFalse;
        return LosDetectionEnable(self, losDetectionEnabled);
        }

    return cAtOk;
    }

static eBool SAToPBound(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    AtPw boundPw;

    boundPw = AtChannelBoundPwGet(channel);
    if (boundPw == NULL)
        return cAtFalse;

    return (AtPwTypeGet(boundPw) == cAtPwTypeSAToP) ? cAtTrue : cAtFalse;
    }

static eBool ShouldControlMonOnly(ThaPrbsEnginePdhChannel self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);

    if (mMethodsGet(mThis(self))->IsUnframed(mThis(self), AtPdhChannelFrameTypeGet(channel)))
        return cAtFalse;

    return SAToPBound((AtPrbsEngine)self);
    }

static eAtModulePrbsRet MonOnlyEnable(ThaPrbsEnginePdhChannel self, eBool enable)
    {
    if (ShouldControlMonOnly(self))
        return mMethodsGet(self)->RxFramerMonOnlyEnable(self, enable ? cAtFalse : cAtTrue);
    return cAtOk;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret;

    ret = m_AtPrbsEngineMethods->RxEnable(self, enable);
    if (ret != cAtOk)
        return ret;

    ret |= ThaPrbsEnginePdhChannelMonOnlyEnable(mThis(self), enable);
    if (ret != cAtOk)
        return ret;

    if (ShouldControlLosDetection(self))
        {
        eBool losDetectionEnabled = DefaultLosDetection(self);

        if (enable && AtPrbsEngineAllZeroFixedPatternIsUsed(self))
            losDetectionEnabled = cAtFalse;

        ret |= LosDetectionEnable(self, losDetectionEnabled);
        }

    return ret;
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtRet ret;

    ret = m_AtPrbsEngineMethods->RxModeSet(self, prbsMode);
    if (ret != cAtOk)
        return ret;

    if (AtPrbsEngineRxIsEnabled(self) && ShouldControlLosDetection(self))
        {
        eBool losDetectionEnabled = DefaultLosDetection(self);

        if (AtPrbsEngineAllZeroFixedPatternIsUsed(self))
            losDetectionEnabled = cAtFalse;

        return LosDetectionEnable(self, losDetectionEnabled);
        }

    return cAtOk;
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    eAtRet ret;

    ret = m_AtPrbsEngineMethods->TxEnable(self, enable) ;
    if (ret != cAtOk)
        return ret;

    if (SAToPBound(self))
        ret |= mMethodsGet(mThis(self))->TxFramerBypass(mThis(self), enable ? cAtFalse : cAtTrue);

    return ret;
    }

static eAtRet TxFramerBypass(ThaPrbsEnginePdhChannel self, eBool bypass)
    {
    AtUnused(self);
    AtUnused(bypass);
    return cAtOk;
    }

static uint8 CircuitLocalLoopbackMode(ThaPrbsEngine self)
    {
    AtUnused(self);
    return cAtPdhLoopbackModeLocalLine;
    }

static eAtRet AllSubCircuitsPwSquel(ThaPrbsEngine self, eBool forced)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return ThaPrbsEngineAllPdhSubChannelsPwSquel(pdhChannel, forced);
    }

static ThaModulePrbs PrbsModule(ThaPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (ThaModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static eBool ChannelizedPrbsAllowed(ThaPrbsEngine self)
    {
    return AtModulePrbsChannelizedPrbsAllowed((AtModulePrbs)PrbsModule(self));
    }

static void OverrideThaPrbsEngine(AtPrbsEngine self)
    {
    ThaPrbsEngine engine = (ThaPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(engine), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, CircuitLocalLoopbackMode);
        mMethodOverride(m_ThaPrbsEngineOverride, AllSubCircuitsPwSquel);
        mMethodOverride(m_ThaPrbsEngineOverride, ChannelizedPrbsAllowed);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void MethodsInit(ThaPrbsEnginePdhChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, IsUnframed);
        mMethodOverride(m_methods, ModuleCanDetectLosByAllZeroPattern);
        mMethodOverride(m_methods, TxFramerBypass);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPrbsEnginePdhChannel);
    }

AtPrbsEngine ThaPrbsEnginePdhChannelObjectInit(AtPrbsEngine self, AtPdhChannel channel, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPrbsEngineObjectInit(self, (AtChannel)channel, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

eAtModulePrbsRet ThaPrbsEnginePdhChannelMonOnlyEnable(ThaPrbsEnginePdhChannel self, eBool enable)
    {
    if (self)
        return MonOnlyEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEngineAuVcInternal.h
 * 
 * Created Date: May 1, 2017
 *
 * Description : AU's VC PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINEAUVCINTERNAL_H_
#define _THAPRBSENGINEAUVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEngineAuVc * ThaPrbsEngineAuVc;

typedef struct tThaPrbsEngineAuVcMethods
    {
    void (*PohInsert)(ThaPrbsEngineAuVc self, AtSdhChannel channel, eBool enable);
    AtSdhChannel (*ChannelForPohInsertion)(ThaPrbsEngineAuVc self, AtSdhChannel channel);
    void (*PohInsertHandle)(ThaPrbsEngineAuVc self, eBool prbsEnable);
    }tThaPrbsEngineAuVcMethods;

typedef struct tThaPrbsEngineAuVc
    {
    tThaPrbsEngine super;
    const tThaPrbsEngineAuVcMethods *methods;
    }tThaPrbsEngineAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEngineAuVcObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINEAUVCINTERNAL_H_ */


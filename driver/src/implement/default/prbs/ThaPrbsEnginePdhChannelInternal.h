/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : ThaPrbsEnginePdhChannelInternal.h
 * 
 * Created Date: Feb 14, 2016
 *
 * Description : PRBS common implementation for PDH channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPRBSENGINEPDHCHANNELINTERNAL_H_
#define _THAPRBSENGINEPDHCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPrbsEngineInternal.h"
#include "../pdh/ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPrbsEnginePdhChannel * ThaPrbsEnginePdhChannel;

typedef struct tThaPrbsEnginePdhChannelMethods
    {
    eBool (*IsUnframed)(ThaPrbsEnginePdhChannel self, uint16 frameMode);
    eBool (*ModuleCanDetectLosByAllZeroPattern)(ThaPrbsEnginePdhChannel self, ThaModulePdh module);
    eAtRet (*TxFramerBypass)(ThaPrbsEnginePdhChannel self, eBool bypass);
    eAtRet (*RxFramerMonOnlyEnable)(ThaPrbsEnginePdhChannel self, eBool enabled);
    }tThaPrbsEnginePdhChannelMethods;

typedef struct tThaPrbsEnginePdhChannel
    {
    tThaPrbsEngine super;
    const tThaPrbsEnginePdhChannelMethods *methods;
    }tThaPrbsEnginePdhChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine ThaPrbsEnginePdhChannelObjectInit(AtPrbsEngine self, AtPdhChannel channel, uint32 engineId);
eAtModulePrbsRet ThaPrbsEnginePdhChannelMonOnlyEnable(ThaPrbsEnginePdhChannel self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THAPRBSENGINEPDHCHANNELINTERNAL_H_ */


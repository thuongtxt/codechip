/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaDefaultPohProcessorV1Internal.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : POH default processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEFAULTPOHPROCESSORV1INTERNAL_H_
#define _THADEFAULTPOHPROCESSORV1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaPohProcessorInternal.h"
#include "ThaModulePohV1Reg.h"
#include "ThaDefaultPohProcessorV1.h"

#include "../../man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tThaDefaultPohProcessorV1Methods
    {
    uint32 (*ExpectTtiOffset)(ThaDefaultPohProcessorV1 self, uint8 dwordIndex);
    uint32 (*InsertTtiOffset)(ThaDefaultPohProcessorV1 self, uint8 dwordIndex);
    uint32 (*DefaultOffset)(ThaDefaultPohProcessorV1 self);
    uint32 (*PohPathTerInsBufDeftOffset)(ThaDefaultPohProcessorV1 self, uint8 bId);
    uint8 (*TtiCaptureBufferId)(ThaDefaultPohProcessorV1 self);
    uint16 (*FrameTimeInUs)(ThaDefaultPohProcessorV1 self);
    }tThaDefaultPohProcessorV1Methods;

typedef struct tThaDefaultPohProcessorV1
    {
    tThaPohProcessor super;
    const tThaDefaultPohProcessorV1Methods *methods;

    /* Private data */
    uint8 ttiMode;
    uint8 enabled;
    }tThaDefaultPohProcessorV1;

/*--------------------------- Forward declarations ---------------------------*/
extern void ThaDefaultPohProcessorInsertionChannelGet(ThaDefaultPohProcessorV1 self, uint8 *slice, uint8 *sts, uint8 *vt);
extern eAtRet ThaTtiMsgGet(const tAtSdhTti *tti, uint8 *pTtiMsg, uint8 *pHwJ0Md, uint8 *pJ0Len);
extern void ThaDefaultPohProcessorCounterRead2ClearEnable(ThaDefaultPohProcessorV1 self, eBool enable);

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor ThaDefaultPohProcessorV1ObjectInit(ThaPohProcessor self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THADEFAULTPOHPROCESSORV1INTERNAL_H_ */


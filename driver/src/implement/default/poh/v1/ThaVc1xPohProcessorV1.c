/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaVc1xPohProcessorV1.c
 *
 * Created Date: Mar 19, 2013
 *
 * Description : VC-1x POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaVc1xPohProcessorV1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mFlatVt(tug2, tu) ((tug2 * 4UL) + tu)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVc1xPohProcessorV1Methods m_methods;

/* Override */
static tThaDefaultPohProcessorV1Methods m_ThaDefaultPohProcessorV1Override;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtSdhPathMethods m_AtSdhPathOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 TtiCaptureBufferId(ThaDefaultPohProcessorV1 self)
    {
	AtUnused(self);
    return 3;
    }

static uint16 FrameTimeInUs(ThaDefaultPohProcessorV1 self)
    {
	AtUnused(self);
    return 500;
    }

static void HwIdGet(ThaVc1xPohProcessorV1 self, uint8 *slice, uint8 *sts, uint8 *tug2, uint8 *tu)
    {
    AtSdhChannel vc = mVcGet(self);

    ThaSdhChannel2HwMasterStsId(vc, cThaModulePoh, slice, sts);
    *tug2 = AtSdhChannelTug2Get(vc);
    *tu = AtSdhChannelTu1xGet(vc);
    }

static uint32 PohCpeVtAlmStatDeftOffset(ThaVc1xPohProcessorV1 self)
    {
    uint8 stsId, sliceId, tug2, tu;
    HwIdGet(self, &sliceId, &stsId, &tug2, &tu);
    return ((sliceId * 512UL) + (stsId * 32UL) + mFlatVt(tug2, tu)) + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 ExpectTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
    uint8 stsId, sliceId, tug2, tu;
    HwIdGet((ThaVc1xPohProcessorV1)self, &sliceId, &stsId, &tug2, &tu);
    return (stsId * 512UL) + (mFlatVt(tug2, tu) * 16UL) + dwordIndex + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 DefaultOffset(ThaDefaultPohProcessorV1 self)
    {
    uint8 stsId, sliceId, tug2, tu;
    HwIdGet((ThaVc1xPohProcessorV1)self, &sliceId, &stsId, &tug2, &tu);
    return (stsId * 128UL) + (sliceId * 32UL) + mFlatVt(tug2, tu) + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 InsertTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
    uint8 stsId, sliceId, tug2, tu;
    HwIdGet((ThaVc1xPohProcessorV1)self, &sliceId, &stsId, &tug2, &tu);
    return (stsId * 512UL) + (mFlatVt(tug2, tu) * 16UL) + dwordIndex + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    uint32 regVal, address;
    uint8 enableValue;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get current configuration */
    address = cThaRegPohCpeInsCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* RDI when AIS/LOP */
    enableValue = enable ? 0 : 1;
    if ((alarmType & cAtSdhPathAlarmAis) || (alarmType & cAtSdhPathAlarmLop))
        mFieldIns(&regVal, cThaAisLopVMskMask, cThaAisLopVMskShift, enableValue);

    /* TIM */
    if (alarmType & cAtSdhPathAlarmTim)
        mFieldIns(&regVal, cThaTimVMskMask, cThaTimVMskShift, enableValue);

    /* PLM */
    if (alarmType & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal, cThaPlmVMskMask, cThaPlmVMskShift, enableValue);

    /* UNEQ */
    if (alarmType & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal, cThaUneqVMskMask, cThaUneqVMskShift, enableValue);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    /* Enable to forwarding AIS to downstream when TIM/PLM/VCAIS defect is detected */
    enableValue = (uint8)mBoolToBin(enable);
    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* TIM */
    if (alarmType & cAtSdhPathAlarmTim)
        mFieldIns(&regVal, cThaTimV2AisEnMask, cThaTimV2AisEnShift, enableValue);

    /* PLM */
    if (alarmType & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal, cThaPlmvAisv2AisEnMask, cThaPlmvAisv2AisEnShift, enableValue);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    uint32 regVal = mChannelHwRead(self, cThaRegPohCpeInsCtrl + mMethodsGet(processor)->DefaultOffset(processor), cThaModulePoh);
    uint32 mask = 0;

    if (regVal & cThaAisLopVMskMask)
        mask |= (cAtSdhPathAlarmAis | cAtSdhPathAlarmLop);

    if (regVal & cThaTimVMskMask)
        mask |= cAtSdhPathAlarmTim;

    if (regVal & cThaPlmVMskMask)
        mask |= cAtSdhPathAlarmPlm;

    if (regVal & cThaUneqVMskMask)
        mask |= cAtSdhPathAlarmUneq;

    return mask;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    uint32 regVal;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    regVal = mChannelHwRead(self, cThaRegPohCpeInsCtrl + mMethodsGet(processor)->DefaultOffset(processor), cThaModulePoh);

    /* RDI when AIS/LOP */
    if ((alarmType & cAtSdhPathAlarmAis) || (alarmType & cAtSdhPathAlarmLop))
        return (regVal & cThaAisLopVMskMask) ? cAtFalse : cAtTrue;

    /* TIM */
    if (alarmType & cAtSdhPathAlarmTim)
        return (regVal & cThaTimVMskMask) ? cAtFalse : cAtTrue;

    /* PLM */
    if (alarmType & cAtSdhPathAlarmPlm)
        return (regVal & cThaPlmVMskMask) ? cAtFalse : cAtTrue;

    /* UNEQ */
    if (alarmType & cAtSdhPathAlarmUneq)
        return (regVal & cThaUneqVMskMask) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

/* Hardware returns value with format V5[b8],K4[b5:b7]. Where K4 is
 * corresponding with Z7. Refer GR 253, Table 6-5.  RDI-V Bit Settings and
 * Interpretation to understand how ERDI-V is calculated. Pseudocode is:
 * if v5[8] is 1
 *     if z7[5:7] is 6
 *         return ERDI Connectivity defect;
 *     else
 *         return ERDI Server defect;
 *
 * else
 *     if z7[5:7] is 2
 *         return ERDI Payload;
 */
static uint32 ErdiTypeFromErdiValue(uint8 erdiValue)
    {
    uint8 v5, z7;

    v5 = (erdiValue & cBit3) ? 1 : 0;
    z7 = erdiValue & cBit2_0;
    if (v5 == 1)
        {
        if (z7 == 6)
            return cAtSdhPathAlarmErdiC;

        return cAtSdhPathAlarmErdiS;
        }

    /* V5 == 0 */
    if (z7 == 2)
        return cAtSdhPathAlarmErdiP;

    return 0;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 address, regVal;
    uint32 alarmState = 0;
    ThaVc1xPohProcessorV1 vc1xProcessor       = (ThaVc1xPohProcessorV1)self;
    ThaDefaultPohProcessorV1 defaultProcessor = (ThaDefaultPohProcessorV1)self;
    uint8 erdiValue;

    /* Get UNEQ-P/V, PLM-P/V, TIM-P/V */
    address = cThaRegPohCpeVtAlmStat + mMethodsGet((ThaVc1xPohProcessorV1)self)->PohCpeVtAlmStatDeftOffset(vc1xProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Convert to SW alarm */
    if (regVal & cThaPohVtUneqIntMask)
        alarmState |= cAtSdhPathAlarmUneq;
    if (regVal & cThaPohVtTtiTimIntMask)
        alarmState |= cAtSdhPathAlarmTim;
    if (regVal & cThaPohVtPlmIntMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohVtRfiDefIntMask)
        alarmState |= cAtSdhPathAlarmRfi;

    /* Get ERDI-P/V */
    address = cThaRegPohCpeStat2 + mMethodsGet(defaultProcessor)->DefaultOffset(defaultProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaERdiVValMask, cThaERdiVValShift, uint8, &erdiValue);
    alarmState |= ErdiTypeFromErdiValue(erdiValue);

    return alarmState;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 address, regVal;
    uint32 alarmState = 0;
    uint8 erdiValue;
    ThaVc1xPohProcessorV1 vc1xProcessor = (ThaVc1xPohProcessorV1)self;
    ThaDefaultPohProcessorV1 defaultProcessor = (ThaDefaultPohProcessorV1)self;

    /* Read hardware status */
    address = cThaRegPohCpeVtAlmIntrStk + mMethodsGet(vc1xProcessor)->PohCpeVtAlmStatDeftOffset(vc1xProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    if (regVal & cThaPohVtTtiTimIntMask)
        alarmState |= cAtSdhPathAlarmTim;
    if (regVal & cThaPohVtPlmIntMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohVtVcAisIntMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohVtUneqIntMask)
        alarmState |= cAtSdhPathAlarmUneq;
    if (regVal & cThaPohVtRfiDefIntMask)
        alarmState |= cAtSdhPathAlarmRfi;

    /* Get ERDI-P interrupt */
    if (regVal & cThaPohVtRdiStbChgIntMask) /* ERDI is changed value */
        {
        address = cThaRegPohCpeStat2 + mMethodsGet(defaultProcessor)->DefaultOffset(defaultProcessor);
        regVal = mChannelHwRead(self, address, cThaModulePoh);
        mFieldGet(regVal, cThaERdiVValMask, cThaERdiVValShift, uint8, &erdiValue);
        alarmState |= ErdiTypeFromErdiValue(erdiValue);
        }

    return alarmState;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtSdhPathAlarmAis   |
            cAtSdhPathAlarmTim   |
            cAtSdhPathAlarmPlm   |
            cAtSdhPathAlarmUneq  |
            cAtSdhPathAlarmRfi   |
            cAtSdhPathAlarmRdi   |
            cAtSdhPathAlarmErdiS |
            cAtSdhPathAlarmErdiP |
            cAtSdhPathAlarmErdiC);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address;
    ThaVc1xPohProcessorV1 vc1xProcessor = (ThaVc1xPohProcessorV1)self;

    /* Get current configuration */
    address = cThaRegPohCpeVtAlmIntrEnCtrl + mMethodsGet(vc1xProcessor)->PohCpeVtAlmStatDeftOffset(vc1xProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Configure AIS mask */
    if (defectMask & cAtSdhPathAlarmAis)
        mFieldIns(&regVal,
                  cThaPohVtVcAisIntEnbMask,
                  cThaPohVtVcAisIntEnbShift,
                  (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);

    /* Configure TTI mask */
    if (defectMask & cAtSdhPathAlarmTim)
        mFieldIns(&regVal,
                  cThaPohVtTtiTimIntEnbMask,
                  cThaPohVtTtiTimIntEnbShift,
                  (enableMask & cAtSdhPathAlarmTim) ? 1 : 0);

    /* Configure PLM mask */
    if (defectMask & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal,
                  cThaPohVtPlmIntEnbMask,
                  cThaPohVtPlmIntEnbShift,
                  (enableMask & cAtSdhPathAlarmPlm) ? 1 : 0);

    /* Configure UNEQ mask */
    if (defectMask & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal,
                  cThaPohVtUneqIntEnbMask,
                  cThaPohVtUneqIntEnbShift,
                  (enableMask & cAtSdhPathAlarmUneq) ? 1 : 0);

    /* Configure RDI mask */
    if (defectMask & (cAtSdhPathAlarmRdi | cAtSdhPathAlarmErdiS | cAtSdhPathAlarmErdiP | cAtSdhPathAlarmErdiC))
        mFieldIns(&regVal,
                  cThaPohVtRdiStbChgIntEnbMask,
                  cThaPohVtRdiStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmRdi) ? 1 : 0);

    /* Configure RFI mask */
    if (defectMask & cAtSdhPathAlarmRfi)
        mFieldIns(&regVal,
                  cThaPohVtRfiDefIntEnbMask,
                  cThaPohVtRfiDefIntEnbShift,
                  (enableMask & cAtSdhPathAlarmRfi) ? 1 : 0);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regVal, address, swMask = 0;
    ThaVc1xPohProcessorV1 vc1xProcessor = (ThaVc1xPohProcessorV1)self;

    /* Get current configuration */
    address = cThaRegPohCpeVtAlmIntrEnCtrl + mMethodsGet(vc1xProcessor)->PohCpeVtAlmStatDeftOffset(vc1xProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Get AIS mask */
    if (regVal & cThaPohVtVcAisIntEnbMask)
        swMask |= cAtSdhPathAlarmAis;

    /* Get TTI mask */
    if (regVal & cThaPohVtTtiTimIntEnbMask)
        swMask |= cAtSdhPathAlarmTim;

    /* Get PLM mask */
    if (regVal & cThaPohVtPlmIntEnbMask)
        swMask |= cAtSdhPathAlarmPlm;

    /* Get UNEQ mask */
    if (regVal & cThaPohVtUneqIntEnbMask)
        swMask |= cAtSdhPathAlarmUneq;

    /* Get RDI mask */
    if (regVal & cThaPohVtRdiStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmRdi;

    /* Get RFI mask */
    if (regVal & cThaPohVtRfiDefIntEnbMask)
        swMask |= cAtSdhPathAlarmRfi;

    return swMask;
    }

static uint32 PohPathTerInsBufDeftOffset(ThaDefaultPohProcessorV1 self, uint8 bId)
    {
    uint8 outSlice, outSts, outVt;

    ThaDefaultPohProcessorInsertionChannelGet(self, &outSlice, &outSts, &outVt);

    /* For STS/TU3/TU */
    return ((outSlice * 2048UL) + (outSts * 128UL) + (outVt * 4UL) + bId) + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 pslVal;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    address = cThaRegPohCpeStat2 + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaVslCurMask, cThaVslCurShift, uint8, &pslVal);

    return pslVal;
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 pslValue;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get the expected mask/shift of Path/V Signal Label */
    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaVslExptValMask, cThaVslExptValShift, uint8, &pslValue);

    return pslValue;
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    uint32 regVal, address;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get the expected mask/shift of Path/V Signal Label */
    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaVslExptValMask, cThaVslExptValShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    uint32 address;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    ThaDefaultPohProcessorCounterRead2ClearEnable(processor, read2Clear);

    /* BIP */
    if (counterType == cAtSdhPathCounterTypeBip)
        {
        address = cThaRegPohCpeCnt1BipErr + mMethodsGet(processor)->DefaultOffset(processor);
        return mChannelHwRead(self, address, cThaModulePoh) & cThaBip2ErrCntMask;
        }

    /* REI */
    if (counterType == cAtSdhPathCounterTypeRei)
        {
        address = cThaRegPohCpeCnt1Rei + mMethodsGet(processor)->DefaultOffset(processor);
        return mChannelHwRead(self, address, cThaModulePoh) & cThaReiVErrCntMask;
        }

    /* Unknown counter */
    return 0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    uint32 regVal, address;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaJ2TimMonEnableMask, cThaJ2TimMonEnableShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    uint32 regVal, address;
    eBool enable;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaJ2TimMonEnableMask, cThaJ2TimMonEnableShift, uint8, &enable);

    return mBinToBool(enable);
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    uint32 regVal, address;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Configure overhead byte (V5 byte) buffer using for transmit */
    address = cThaRegPohPathTerInsBuf +
              mMethodsGet(processor)->PohPathTerInsBufDeftOffset(processor, cThaPohV5ByteTerInsBuf);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaByteInsBufMask, cThaByteInsBufShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 psl;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get overhead byte (V5 byte) buffer using for transmit */
    address = cThaRegPohPathTerInsBuf +
              mMethodsGet(processor)->PohPathTerInsBufDeftOffset(processor, cThaPohV5ByteTerInsBuf);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaByteInsBufMask, cThaByteInsBufShift, uint8, &psl);

    return psl;
    }

static void OverrideThaDefaultPohProcessorV1(ThaVc1xPohProcessorV1 self)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDefaultPohProcessorV1Override, mMethodsGet(processor), sizeof(m_ThaDefaultPohProcessorV1Override));
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, TtiCaptureBufferId);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, PohPathTerInsBufDeftOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, FrameTimeInUs);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, ExpectTtiOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, DefaultOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, InsertTtiOffset);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV1Override);
    }

static void OverrideAtSdhChannel(ThaVc1xPohProcessorV1 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(ThaVc1xPohProcessorV1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(ThaVc1xPohProcessorV1 self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void Override(ThaVc1xPohProcessorV1 self)
    {
    OverrideThaDefaultPohProcessorV1(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    }

static void MethodsInit(ThaVc1xPohProcessorV1 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, PohCpeVtAlmStatDeftOffset);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVc1xPohProcessorV1);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDefaultPohProcessorV1ObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaVc1xPohProcessorV1)self);
    MethodsInit((ThaVc1xPohProcessorV1)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor ThaVc1xPohProcessorV1New(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

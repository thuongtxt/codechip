/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaDefaultPohProcessorV1.c
 *
 * Created Date: Mar 19, 2013
 *
 * Description : Default POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaDefaultPohProcessorV1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaDefaultPohProcessorV1Methods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tAtChannelMethods       m_AtChannelOverride;
static tAtSdhChannelMethods    m_AtSdhChannelOverride;
static tAtSdhPathMethods       m_AtSdhPathOverride;
static tThaPohProcessorMethods m_ThaPohProcessorOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PohPathTerInsBufDeftOffset(ThaDefaultPohProcessorV1 self, uint8 bId)
    {
	AtUnused(bId);
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static uint32 ExpectTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
	AtUnused(dwordIndex);
	AtUnused(self);
    return 0;
    }

static uint32 DefaultOffset(ThaDefaultPohProcessorV1 self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 InsertTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
	AtUnused(dwordIndex);
	AtUnused(self);
    return 0;
    }

static uint16 FrameTimeInUs(ThaDefaultPohProcessorV1 self)
    {
	AtUnused(self);
    return 125;
    }

void ThaDefaultPohProcessorInsertionChannelGet(ThaDefaultPohProcessorV1 self, uint8 *slice, uint8 *sts, uint8 *vt)
    {
    uint32 address, regVal;

    address = cThaRegPohCpeInsCtrl + mMethodsGet(self)->DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Get hardware identifier */
    mFieldGet(regVal, cThaSliceOutMask, cThaSliceOutShift, uint8, slice);
    mFieldGet(regVal, cThaStsOutMask, cThaStsOutShift, uint8, sts);
    if (vt)
        mFieldGet(regVal, cThaVtOutMask, cThaVtOutShift, uint8, vt);
    }

void ThaDefaultPohProcessorCounterRead2ClearEnable(ThaDefaultPohProcessorV1 self, eBool enable)
    {
    uint32 regVal, regAddr;

    regAddr = cThaRegPohCpeCntCtrl + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
    mFieldIns(&regVal, cThaR2CCntEnbMask, cThaR2CCntEnbShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    uint32 regVal, address;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Configure overhead byte (C2 byte) buffer using for transmit */
    address = cThaRegPohPathTerInsBuf +
              mMethodsGet(processor)->PohPathTerInsBufDeftOffset(processor, cThaPohC2ByteTerInsBuf);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaByteInsBufMask, cThaByteInsBufShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 psl;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get overhead byte (C2 byte) buffer using for transmit */
    address = cThaRegPohPathTerInsBuf +
              mMethodsGet(processor)->PohPathTerInsBufDeftOffset(processor, cThaPohC2ByteTerInsBuf);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaByteInsBufMask, cThaByteInsBufShift, uint8, &psl);

    return psl;
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    uint8 msgLen;
    uint8 byteId, position;
    uint32 regAddr, regVal;
    uint8 msgBuf[cAtSdhChannelMaxTtiLength];

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->TxTtiGet(self, tti);

    tti->mode = processor->ttiMode;

    /* Get message content */
    msgLen = AtSdhTtiLengthForTtiMode(tti->mode);
    if (!msgLen)
        return cAtError;

    for (byteId = 0; byteId < msgLen; byteId++)
        {
        position = byteId % cThaSonetNumBytesInDword;
        regAddr = cThaRegPohMsgInsBuf + mMethodsGet(processor)->InsertTtiOffset(processor, byteId / cThaSonetNumBytesInDword);
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        mFieldGet(regVal,
                  cThaTraceMsgInsByteMask(position),
                  cThaTraceMsgInsByteShift(position),
                  uint8,
                  &(msgBuf[byteId % msgLen]));
        }

    ThaModulePohTtiStrip(tti->mode, msgBuf, tti->message);

    return cAtOk;
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    uint8 msgBuf[cAtSdhChannelMaxTtiLength];
    uint8 byteId;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    uint8 msgLen, position;
    uint32 regAddr, regVal;

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->TxTtiSet(self, tti);

    /* Save message mode and insert CRC */
    processor->ttiMode = tti->mode;
    ThaTtiMsgGet(tti, msgBuf, NULL, &msgLen);

    /* Write message content */
    for (byteId = 0; byteId < cAtSdhChannelMaxTtiLength; byteId++)
        {
        position = byteId % cThaSonetNumBytesInDword;
        regAddr = cThaRegPohMsgInsBuf + mMethodsGet(processor)->InsertTtiOffset(processor, byteId / cThaSonetNumBytesInDword);
        regVal = mChannelHwRead(self, regAddr, cThaModulePoh);
        mFieldIns(&regVal, cThaTraceMsgInsByteMask(position), cThaTraceMsgInsByteShift(position), msgBuf[byteId % msgLen]);
        mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
        }

    return cAtOk;
    }

static uint8 TtiCaptureBufferId(ThaDefaultPohProcessorV1 self)
    {
	AtUnused(self);
    /* Let's subclass do */
    return 0;
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    uint32 address, regVal;
    uint8  byte_i;
    uint8 msgLen;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    uint8 message[cThaOcnJnLenIn64ByteMd];

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->ExpectedTtiGet(self, tti);

    /* Get message mode */
    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaJnFrmMdMask, cThaJnFrmMdShift, uint8, &(tti->mode));

    /* Get message content */
    msgLen = AtSdhTtiLengthForTtiMode(tti->mode);
    byte_i = 0;
    while (byte_i < msgLen)
        {
        uint8 i;

        address = cThaRegPohJnTtiMsgBuf + mMethodsGet(processor)->ExpectTtiOffset(processor, byte_i / 4);
        regVal  = mChannelHwRead(self, address, cThaModulePoh);
        for (i = 0; ((i < 4) && (byte_i < msgLen)); i++)
            {
            mFieldGet(regVal,
                      cThaRxJnMsgByteMask(i),
                      cThaRxJnMsgByteShift(i),
                      uint8,
                      &(message[byte_i]));
            byte_i++;
            }
        }

    ThaModulePohTtiStrip(tti->mode, message, tti->message);

    return cAtOk;
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    uint8 msgLen, hwJnMd;
    uint8 bufId;
    uint8 expectedMsg[cAtSdhChannelMaxTtiLength];
    uint32 regVal, address;
    uint8 byte_i;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    AtOsal osal;

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->ExpectedTtiSet(self, tti);

    /* Initialize memory */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, expectedMsg, 0, sizeof(expectedMsg));
    ThaTtiMsgGet(tti, expectedMsg, &hwJnMd, &msgLen);

    /* Configure J1/J2 frame mode, monitor mode, J1/J2 valid, message buffer ID */
    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaJnFrmMdMask, cThaJnFrmMdShift, hwJnMd);
    mFieldIns(&regVal, cThaJnTimExpMdMask, cThaJnTimExpMdShift, 1);
    mFieldIns(&regVal, cThaJnMsgCapEnbMask, cThaJnMsgCapEnbShift, 1);
    bufId = mMethodsGet(processor)->TtiCaptureBufferId(processor);
    mFieldIns(&regVal, cThaJnBufIDMask, cThaJnBufIDShift, bufId);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    /* Configure HW */
    byte_i = 0;
    while (byte_i < msgLen)
        {
        uint8 i;

        regVal = 0;
        for (i = 0; ((i < 4) && (byte_i < msgLen)); i++)
            {
            mFieldIns(&regVal,
                      cThaRxJnMsgByteMask(i),
                      cThaRxJnMsgByteShift(i),
                      expectedMsg[byte_i]);
            byte_i++;
            }

        address = cThaRegPohJnTtiMsgBuf + mMethodsGet(processor)->ExpectTtiOffset(processor, (uint8)((byte_i - 1) / 4));
        mChannelHwWrite(self, address, regVal, cThaModulePoh);
        }

    return cAtOk;
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    uint32 regVal, address;
    uint8  bufId;
    uint8  hwState;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    uint32 offset;
    uint32 elapseTimeMs;
    tAtOsalCurTime startTime, currentTime;
    AtOsal osal = AtSharedDriverOsalGet();
    eAtRet ret;
    static const uint8 timeoutMs = 5;

    /* Get Jn monitor frame mode from the POH CPE Control 1 Register */
    offset = mMethodsGet(processor)->DefaultOffset(processor);
    address = cThaRegPohCpeCtrl + offset;
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Get TTI mode the make capture request */
    mFieldGet(regVal, cThaJ1MsgCapFrmMdMask, cThaJ1MsgCapFrmMdShift, uint8, &(tti->mode));
    bufId = mMethodsGet(processor)->TtiCaptureBufferId(processor);
    mFieldIns(&regVal, cThaJnBufIDMask, cThaJnBufIDShift, bufId);
    mFieldIns(&regVal, cThaJnMsgCapEnbMask, cThaJnMsgCapEnbShift, 1);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    /* Timeout waiting for in-frame state */
    address = cThaRegPohCpeStat1 + offset;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    elapseTimeMs = 0;
    while (elapseTimeMs < timeoutMs)
        {
        regVal = mChannelHwRead(self, address, cThaModulePoh);
        mFieldGet(regVal, cThaJnStateMask, cThaJnStateShift, uint8, &hwState);
        if (hwState == cHwStateInframe)
            break;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, currentTime);
        if (elapseTimeMs >= timeoutMs)
            return cAtErrorIndrAcsTimeOut;
        }

    /* Get the message */
    ret = ThaModulePohV1TtiCapture((ThaModulePoh)AtChannelModuleGet((AtChannel)self),
                                   (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)processor),
                                   bufId,
                                   tti->mode,
                                   tti->message,
                                   mMethodsGet(processor)->FrameTimeInUs(processor));

    /* Stop capturing */
    address = cThaRegPohCpeCtrl + offset;
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaJnMsgCapEnbMask, cThaJnMsgCapEnbShift, 0);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return ret;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    processor->enabled = enable;

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    return processor->enabled;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    tAtSdhPathCounters *counters = (tAtSdhPathCounters *)pAllCounters;

    counters->bip = AtChannelCounterGet(self, cAtSdhPathCounterTypeBip);
    counters->rei = AtChannelCounterGet(self, cAtSdhPathCounterTypeRei);

    return cAtOk;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
	AtUnused(pAllCounters);
    AtChannelCounterClear(self, cAtSdhPathCounterTypeBip);
    AtChannelCounterClear(self, cAtSdhPathCounterTypeRei);

    return cAtOk;
    }

static eAtRet VcAisAffectEnable(ThaPohProcessor self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* This POH version, PLM and VC-AIS are set together,
     * so nothing to do in this function */
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaDefaultPohProcessorV1 object = (ThaDefaultPohProcessorV1)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(ttiMode);
    mEncodeUInt(enabled);
    }

static uint32 AllAlarmCanChangeAffecting(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop | cAtSdhPathAlarmTim | cAtSdhPathAlarmPlm | cAtSdhPathAlarmUneq;
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    return (alarmType & AllAlarmCanChangeAffecting(self)) ? cAtTrue : cAtFalse;
    }

static void OverrideAtObject(ThaDefaultPohProcessorV1 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPohProcessor(ThaDefaultPohProcessorV1 self)
    {
    ThaPohProcessor processor = (ThaPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaPohProcessorOverride,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaPohProcessorOverride));
        mMethodOverride(m_ThaPohProcessorOverride, VcAisAffectEnable);
        }

    mMethodsSet(processor, &m_ThaPohProcessorOverride);
    }

static void MethodsInit(ThaDefaultPohProcessorV1 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PohPathTerInsBufDeftOffset);
        mMethodOverride(m_methods, TtiCaptureBufferId);
        mMethodOverride(m_methods, ExpectTtiOffset);
        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, InsertTtiOffset);
        mMethodOverride(m_methods, FrameTimeInUs);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtSdhPath(ThaDefaultPohProcessorV1 self)
    {
    AtSdhPath path = (AtSdhPath)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(tAtSdhPathMethods));
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtSdhChannel(ThaDefaultPohProcessorV1 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(tAtSdhChannelMethods));

        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeAlarmAffecting);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(ThaDefaultPohProcessorV1 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaDefaultPohProcessorV1 self)
    {
    OverrideAtObject(self);
    OverrideAtSdhPath(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    OverrideThaPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDefaultPohProcessorV1);
    }

ThaPohProcessor ThaDefaultPohProcessorV1ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaDefaultPohProcessorV1)self);
    MethodsInit((ThaDefaultPohProcessorV1)self);
    m_methodsInit = 1;

    return self;
    }

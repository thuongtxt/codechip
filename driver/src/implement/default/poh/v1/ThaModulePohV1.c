/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaModulePohV1.c
 *
 * Created Date: Mar 20, 2013
 *
 * Description : POH module - work with old hardware version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModulePohInternal.h"
#include "../../sdh/ThaModuleSdh.h"
#include "ThaModulePohV1Reg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModulePohV1
    {
    tThaModulePoh super;

    /* Private data */
    }tThaModulePohV1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods m_ThaModulePohOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePohV1);
    }

static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
	AtUnused(self);
    return ThaSdhLineTtiProcessorV1New((AtSdhChannel)line);
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(self);
    return (ThaPohProcessor)ThaAuVcPohProcessorV1New(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(self);
    return (ThaPohProcessor)ThaTu3VcPohProcessorV1New(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(self);
    return (ThaPohProcessor)ThaVc1xPohProcessorV1New(vc);
    }

static uint8 NumParts(AtModule self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    return ThaDeviceNumPartsOfModule(device, cThaModulePoh);
    }

static uint32 PartOffset(AtModule self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    return ThaDeviceModulePartOffset(device, cThaModulePoh, partId);
    }

static eAtRet PartDefaultSet(AtModule self, uint8 partId)
    {
    uint32 regVal, regAddr;

    regAddr = cThaRegPohMastCtrl + PartOffset(self, partId);
    regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cThaPOHActMask, cThaPOHActShift, 0x1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DefaultSet(AtModule self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    /* Super initialization */
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Set default */
    return DefaultSet(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePoh(self);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePohObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePohV1New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

void ThaModulePohTtiStrip(eAtSdhTtiMode ttiMode, uint8 *message, uint8 *resultMessage)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 msgLen;

    msgLen = AtSdhTtiLengthForTtiMode(ttiMode);
    if (ttiMode == cAtSdhTtiMode16Byte)
        mMethodsGet(osal)->MemCpy(osal, resultMessage, &(message[1]), sizeof(uint8) * (uint32)(msgLen - 1));
    else
        {
        mMethodsGet(osal)->MemCpy(osal, resultMessage, message, sizeof(uint8) * msgLen);

        /* Also ignore CR/LF */
        if (ttiMode == cAtSdhTtiMode64Byte)
            {
            /* The following condition is to make LINT be happy */
            if (msgLen <= 2)
                return;
            resultMessage[msgLen - 1] = 0;
            resultMessage[msgLen - 2] = 0;
            }
        }
    }

eAtRet ThaModulePohV1TtiCapture(ThaModulePoh self,
                                AtSdhChannel sdhChannel,
                                uint8 bufId,
                                eAtSdhTtiMode ttiMode,
                                uint8 *pMsgBuf,
                                uint32 frameTimeInUs)
    {
    uint8  capturedMsg[cAtSdhChannelMaxTtiLength];
    uint8  byte_i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 msgLen;
    uint32 partOffset = PartOffset((AtModule)self, ThaModuleSdhPartOfChannel(sdhChannel));

    /* Wait HW searching SONET frame */
    mMethodsGet(osal)->USleep(osal, frameTimeInUs * cAtSdhChannelMaxTtiLength * 2);

    /* Get message */
    mMethodsGet(osal)->MemInit(osal, capturedMsg, 0, sizeof(uint8) * cAtSdhChannelMaxTtiLength);
    byte_i = 0;
    msgLen = AtSdhTtiLengthForTtiMode(ttiMode);
    while (byte_i < msgLen)
        {
        uint8  byteInDword;
        uint32 regAddr, regVal;
        uint32 offset = (bufId * 16UL) + (byte_i / 4UL) + partOffset;
        regAddr = cThaRegPohCapturedTtiMsgBuf + offset;
        regVal = mChannelHwRead(sdhChannel, regAddr, cThaModulePoh);
        for (byteInDword = 0; ((byteInDword < 4) && (byte_i < msgLen)); byteInDword++)
            {
            mFieldGet(regVal,
                      cThaCapTtiMsgByteMask(byteInDword),
                      cThaCapTtiMsgByteShift(byteInDword),
                      uint8,
                      &(capturedMsg[byte_i]));
            byte_i++;
            }
        }

    ThaModulePohTtiStrip(ttiMode, capturedMsg, pMsgBuf);

    /* Clear sticky */
    mChannelHwWrite(sdhChannel, cThaRegPohJnTtiMsgCapStat + partOffset, cThaPohTtiCapMsgStaMask(bufId), cThaModulePoh);

    return cAtOk;
    }

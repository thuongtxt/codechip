/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaTu3VcPohProcessorV1Internal.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : TU-3's VC POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATU3VCPOHPROCESSORV1INTERNAL_H_
#define _THATU3VCPOHPROCESSORV1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaAuVcPohProcessorV1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaTu3VcPohProcessorV1 * ThaTu3VcPohProcessorV1;

typedef struct tThaTu3VcPohProcessorV1
    {
    tThaDefaultPohProcessorV1 super;

    /* Private data */
    }tThaTu3VcPohProcessorV1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THATU3VCPOHPROCESSORV1INTERNAL_H_ */


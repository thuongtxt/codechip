/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaVc1xPohProcessorV1Internal.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : VC-1x POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVC1XPOHPROCESSORV1INTERNAL_H_
#define _THAVC1XPOHPROCESSORV1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaDefaultPohProcessorV1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVc1xPohProcessorV1 * ThaVc1xPohProcessorV1;

typedef struct tThaVc1xPohProcessorV1Methods
    {
    uint32 (*PohCpeVtAlmStatDeftOffset)(ThaVc1xPohProcessorV1 self);
    }tThaVc1xPohProcessorV1Methods;

typedef struct tThaVc1xPohProcessorV1
    {
    tThaDefaultPohProcessorV1 super;
    const tThaVc1xPohProcessorV1Methods *methods;

    /* Private data */
    }tThaVc1xPohProcessorV1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAVC1XPOHPROCESSORV1INTERNAL_H_ */


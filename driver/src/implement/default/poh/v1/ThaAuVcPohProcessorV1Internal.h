/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaAuVcPohProcessorV1Internal.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : AU's VC POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAAUVCPOHPROCESSORV1INTERNAL_H_
#define _THAAUVCPOHPROCESSORV1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaDefaultPohProcessorV1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAuVcPohProcessorV1 * ThaAuVcPohProcessorV1;

typedef struct tThaAuVcPohProcessorV1Methods
    {
    uint32 (*CpeAlarmStatusBaseRegister)(ThaAuVcPohProcessorV1 self);
    uint32 (*CpeAlarmInterruptStickyBaseRegister)(ThaAuVcPohProcessorV1 self);
    uint32 (*CpeAlmIntrEnCtrlBaseRegister)(ThaAuVcPohProcessorV1 self);
    }tThaAuVcPohProcessorV1Methods;

typedef struct tThaAuVcPohProcessorV1
    {
    tThaDefaultPohProcessorV1 super;
    const tThaAuVcPohProcessorV1Methods *methods;

    /* Private data */
    }tThaAuVcPohProcessorV1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor ThaAuVcPohProcessorV1ObjectInit(ThaPohProcessor self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THAAUVCPOHPROCESSORV1INTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaTu3VcPohProcessorV1.c
 *
 * Created Date: Mar 19, 2013
 *
 * Description : TU-3's VC POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaTu3VcPohProcessorV1Internal.h"
#include "ThaAuVcPohProcessorV1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaDefaultPohProcessorV1Methods m_ThaDefaultPohProcessorV1Override;
static tThaAuVcPohProcessorV1Methods    m_ThaAuVcPohProcessorV1Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ExpectTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
    uint8 stsId, sliceId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &sliceId, &stsId);
    return (stsId * 512UL) + (29UL * 16UL) + dwordIndex + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 DefaultOffset(ThaDefaultPohProcessorV1 self)
    {
    uint8 stsId, sliceId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &sliceId, &stsId);
    return (stsId * 128UL) + (sliceId * 32UL) + 29UL + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 InsertTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
    uint8 stsId, sliceId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &sliceId, &stsId);
    return (sliceId * 16UL) + (stsId * 512UL) + 480UL + dwordIndex + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 CpeAlarmStatusBaseRegister(ThaAuVcPohProcessorV1 self)
    {
	AtUnused(self);
    return cThaRegPohCpeTu3AlmStat;
    }

static uint32 CpeAlarmInterruptStickyBaseRegister(ThaAuVcPohProcessorV1 self)
    {
	AtUnused(self);
    return cThaRegPohCpeTu3AlmIntrStk;
    }

static uint32 CpeAlmIntrEnCtrlBaseRegister(ThaAuVcPohProcessorV1 self)
    {
	AtUnused(self);
    return cThaRegPohCpeTu3AlmIntrEnCtrl;
    }

static uint32 PohPathTerInsBufDeftOffset(ThaDefaultPohProcessorV1 self, uint8 bId)
    {
    uint8 outSlice, outSts;

    ThaDefaultPohProcessorInsertionChannelGet(self, &outSlice, &outSts, NULL);

    /* For STS/TU3/TU */
    return ((outSlice * 2048UL) + (outSts * 128UL) + 120UL + bId) + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static void OverrideThaDefaultPohProcessorV1(ThaTu3VcPohProcessorV1 self)
    {
    ThaDefaultPohProcessorV1 thaVc = (ThaDefaultPohProcessorV1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDefaultPohProcessorV1Override, mMethodsGet(thaVc), sizeof(m_ThaDefaultPohProcessorV1Override));
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, PohPathTerInsBufDeftOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, ExpectTtiOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, DefaultOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, InsertTtiOffset);
        }

    mMethodsSet(thaVc, &m_ThaDefaultPohProcessorV1Override);
    }

static void OverrideThaAuVcPohProcessorV1(ThaTu3VcPohProcessorV1 self)
    {
    ThaAuVcPohProcessorV1 vc = (ThaAuVcPohProcessorV1)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_ThaAuVcPohProcessorV1Override, 0, sizeof(m_ThaAuVcPohProcessorV1Override));

        mMethodOverride(m_ThaAuVcPohProcessorV1Override, CpeAlarmStatusBaseRegister);
        mMethodOverride(m_ThaAuVcPohProcessorV1Override, CpeAlarmInterruptStickyBaseRegister);
        mMethodOverride(m_ThaAuVcPohProcessorV1Override, CpeAlmIntrEnCtrlBaseRegister);
        }

    mMethodsSet(vc, &m_ThaAuVcPohProcessorV1Override);
    }

static void Override(ThaTu3VcPohProcessorV1 self)
    {
    OverrideThaDefaultPohProcessorV1(self);
    OverrideThaAuVcPohProcessorV1(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaTu3VcPohProcessorV1);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaAuVcPohProcessorV1ObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaTu3VcPohProcessorV1)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor ThaTu3VcPohProcessorV1New(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaModulePohV1Reg.h
 * 
 * Created Date: Mar 20, 2013
 *
 * Description : Register of old POH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPOHV1REG_H_
#define _THAMODULEPOHV1REG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* Length of message in byte mode */
#define cThaOcnJnLenInByteMd            1

/* Length of message in 16 byte mode */
#define cThaOcnJnLenIn16ByteMd          16

/* Length of message in 64 byte mode */
#define cThaOcnJnLenIn64ByteMd          64

/* Number of Byte in Double Word */
#define cThaSonetNumBytesInDword        4

/* Define CR and LF character */
#define cSonetCr                                    '\r'
#define cSonetLf                                    '\n'

#define cThaOcnJ0CapBufId                           1

/* Jn capturing state */
#define cHwStateInframe 2

/*------------------------------------------------------------------------------
Reg Name: POH Master Control
Reg Addr: 0x080006
Reg Desc: This Master Control Register controls the operation of Path Overhead
          Processor Engine.
------------------------------------------------------------------------------*/
#define cThaRegPohMastCtrl                            0x080006

/*-----------
BitField Name: POHUPSRAct
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: POHAct
BitField Type: R/W
BitField Desc: The UPSR processor will be activated if this bit set.
-----------*/
#define cThaPOHActMask                                cBit0
#define cThaPOHActShift                               0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line Enable Control
Reg Addr: 0x00040001
Reg Desc: Configure rate for 4 SONET/SDH lines at Rx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineEnCtrl                      0x00040001


/*-----------
BitField Name: OCNRxLinetEnb
BitField Type: R/W
BitField Desc: These 4 bits is used to configure OC-3/STM-1 modes for 4
               SONET/SDH lines at network side. The bit zero is used for line
               0. Bit is set to enable line.
------------*/
#define cThaOcnRxLineEnbMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnRxLineEnbShift(lineId)                   (lineId)


/*------------------------------------------------------------------------------
Reg Name: OCN Tx Line Enable Control
Reg Addr: 0x00040005
Reg Desc: Configure rate for 4 SONET/SDH lines at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxLineEnCtrl                      0x00040005


/*-----------
BitField Name: OCNTxLinetEnb
BitField Type: R/W
BitField Desc: These 4 bits is used to configure OC-3/STM-1 modes for 4
               SONET/SDH lines at network side. The bit zero is used for line
               0. Bit is set to enable line.
------------*/
#define cThaOcnTxLineEnbMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnTxLineEnbShift(lineId)                   (lineId)


/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line Rate Control
Reg Addr: 0x00040000
Reg Desc: Configure rate for 8 SONET/SDH lines at Rx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineRateCtrl                    0x00040000


/*-----------
BitField Name: OCNRxLineRate[7:0]
BitField Type: R/W
BitField Desc: These 8 bits is used to configure OC-3/STM-1 modes for 8
               SONET/SDH lines at network side. The bit zero is used for line
               0.
               - 0: The line is OC-3/STM-1 line
               - 1: The line is OC-12/STM-4 line
------------*/
#define cThaOcnRxLineRateMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnRxLineRateShift(lineId)                   (lineId)


/*------------------------------------------------------------------------------
Reg Name: OCN Tx Line Rate Control
Reg Addr: 0x00040004
Reg Desc: Configure rate for 8 SONET/SDH lines at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxLineRateCtrl                    0x00040004


/*-----------
BitField Name: OCNTxLinetRate[7:0]
BitField Type: R/W
BitField Desc: These 8 bits is used to configure OC-3/STM-1 modes for 8
               SONET/SDH lines at network side. The bit zero is used for line
               0.
               - 0: The line is OC-3/STM-1 line
               - 1: The line is OC-12/STM-4 line
------------*/
#define cThaOcnTxLineRateMask(lineId)                    (cBit0 << (lineId))
#define cThaOcnTxLineRateShift(lineId)                   (lineId)


/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Alarm Current Status
Reg Addr: 0x00074A20 - 0x00074A27
          The address format for these registers is 0x00074A20 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs
Reg Desc: This is the per Alarm current status of Rx Framer and TOH
          Monitoring. Each register is used to store 6 bits to store current
          status of 6 alarms in the line.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperAlmCurrentStat           0x074A20


/*--------------------------------------
BitField Name: RxLS1StbCurStatus
BitField Type: R/W/C
BitField Desc: S1 Stable current status in the related line. When it changes
               for 0 to 1 or vice versa, the  RxLS1StbStateChgIntr bit in the
               OCN Rx Line per Alarm Interrupt Status register of the related
               line is set.
               - 0: S1 value is not stable
               - 1: S1 value is stable
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLK1StbCurStatus
BitField Type: R/W/C
BitField Desc: K1 Stable current status in the related line. When it changes
               for 0 to 1 or vice versa, the  RxLK1StbStateChgIntr bit in the
               OCN Rx Line per Alarm Interrupt Status register of the related
               line is set.
               - 0: K1 value is not stable
               - 1: K1 value is stable
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLApsLDefCurStatus
BitField Type: R/W/C
BitField Desc: APS-L Defect current status in the related line. When it changes
               for 0 to 1 or vice versa, the  RxLApsLStateChgIntr bit in the
               OCN Rx Line per Alarm Interrupt Status register of the related
               line is set.
               - 0: APS-L defect is cleared
               - 1: APS-L defect is set
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLK2StbCurStatus
BitField Type: R/W/C
BitField Desc: K2 Stable current status in the related line. When it changes
               for 0 to 1 or vice versa, the  RxLK2StbStateChgIntr bit in the
               OCN Rx Line per Alarm Interrupt Status register of the related
               line is set.
               - 0: K2 value is not stable
               - 1: K2 value is stable
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLRdiILDefCurStatus
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxLRdiILDefCurStatMask                     cBit4

/*--------------------------------------
BitField Name: RxLAisLDefCurStatus
BitField Type: R/W/C
BitField Desc: AIS-L Defect current status in the related line. When it changes
               for 0 to 1 or vice versa, the  RxLAisLStateChgIntr bit in the
               OCN Rx Line per Alarm Interrupt Status register of the related
               line is set.
               - 0: AIS-L defect is cleared
               - 1: AIS-L defect is set
--------------------------------------*/
#define cThaRxLAisLDefCurStatMask                      cBit3

/*--------------------------------------
BitField Name: RxLOofCurStatus
BitField Type: R/W/C
BitField Desc: OOF current status in the related line. When it changes for 0 to
               1 or vice versa, the  RxLOofStateChgIntr bit in the OCN Rx Line
               per Alarm Interrupt Status register of the related line is set.
               - 0: not OOF state
               - 1: OOF state
--------------------------------------*/
#define cThaRxLOofCurStatMask                          cBit2

/*--------------------------------------
BitField Name: RxLLofCurStatus
BitField Type: R/W/C
BitField Desc: LOF current status in the related line. When it changes for 0 to
               1 or vice versa, the  RxLALofStateChgIntr bit in the OCN Rx Line
               per Alarm Interrupt Status register of the related line is set.
               - 0: not LOF state
               - 1: LOF state
--------------------------------------*/
#define cThaRxLLofCurStatMask                          cBit1

/*--------------------------------------
BitField Name: RxLLosCurStatus
BitField Type: R/W/C
BitField Desc: LOS current status in the related line. When it changes for 0 to
               1 or vice versa, the  RxLALosStateChgIntr bit in the OCN Rx Line
               per Alarm Interrupt Status register of the related line is set.
               - 0: not LOS state
               - 1: LOS state
--------------------------------------*/
#define cThaRxLLosCurStatMask                          cBit0


/*------------------------------------------------------------------------------
Reg Name: POH OCn BER Report
Reg Addr: 0x08681F-0x08687F
          The address format for these registers is 0x50681f + ocid*32 + 31
          Where: ocnid: 0->3
Reg Desc: The report register is used for report the result of Ocn BER
          detection to software.
------------------------------------------------------------------------------*/

/*-----------
BitField Name: OcnBerMonSfDef
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: OcnBerMonSfCur
BitField Type: R/W
BitField Desc: The current status of SF
-----------*/

/*-----------
BitField Name: OcnBerMonSdDef
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: OcnBerMonSdCur
BitField Type: R/W
BitField Desc: OCn BER Monitoring SD Current Status. The current state of SD
-----------*/

/*-----------
BitField Name: OcnBerMonCur
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 3'd1: BER = 10-3
               - 3'd2: BER = 10-4
               - 3'd3: BER = 10-5
               - 3'd4: BER = 10-6
               - 3'd5: BER = 10-7
               - 3'd6: BER = 10-8
               - 3'd7: BER = 10-9
               - 3'd0: BER = 10-10
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH SDH/SONET Report (POH J0 Report)
Reg Addr: 0x08B01F_0x50B07F
          The address format for these registers is 0x50B01F + LineID*32
          Where: Line ID: 0 -> 3
Reg Desc: This register is J0 Report Register.
------------------------------------------------------------------------------*/
#ifdef  INDR_SIM
#else
#endif

/*-----------
BitField Name: B2CntScanExcdIntr
BitField Type: R/W/C
BitField Desc: B2 counter scan exceed.
-----------*/

/*-----------
BitField Name: J0StatChgIntr
BitField Type: R/W/C
BitField Desc: Section Trace Identifier Message stable change Interrupt
               Status. The status of STI message had changed.
-----------*/

/*-----------
BitField Name: J0StbCur
BitField Type: R_O
BitField Desc: The current of status of STI message is stable. Active high.
-----------*/

/*-----------
BitField Name: J0TimIntr
BitField Type: R/W/C
BitField Desc: Section Trace Identifier Mismatch Interrupt Status. The Section
               Trace Identifier Mismatch.
-----------*/

/*-----------
BitField Name: J0TimCur
BitField Type: R_O
BitField Desc: The Section Trace Identifier Mismatch. Active high
-----------*/

/*-----------
BitField Name: J0OomIntr
BitField Type: R/W/C
BitField Desc: STI Out Of Multiframe Interrupt Status. The STI message is out
               of frame.
-----------*/

/*-----------
BitField Name: J0OomCur
BitField Type: R_O
BitField Desc: The Out of Frame of STI message. Active high.
-----------*/


/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Alarm Interrupt Status
Reg Addr: 0x00074A10 - 0x00074A17
          The address format for these registers is 0x00074A10 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs
Reg Desc: This is the per Alarm interrupt status of Rx framer and TOH
          monitoring. Each register is used to store 12 sticky bits for 12
          alarms in the line.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperAlmIntrStat              0x074A10


/*--------------------------------------
BitField Name: RxLReiErrCntrOvfIntr
BitField Type: R/W/C
BitField Desc: Set 1 when the REI error counter of the related line is
               overflowed in saturation mode, and it is generated an interrupt
               if it is enabled.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLB1ErrCntrOvflIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLB2ErrCntrOvflIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLS1StbStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while S1 Stable state change event happens in the related
               line, and it is generated an interrupt if it is enabled.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLK1StbStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while K1 Stable state change event happens in the related
               line, and it is generated an interrupt if it is enabled.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLApsLDefStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while APS-L Defect state change event happens in the
               related line, and it is generated an interrupt if it is enabled.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLK2StbStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while K2 Stable state change event happens in the related
               line, and it is generated an interrupt if it is enabled.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLRdiILDefStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxLRdiILDefStateChgIntrMask                cBit4

/*--------------------------------------
BitField Name: RxLAisLDefStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while AIS-L Defect state change event happens in the
               related line, and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLAisLDefStateChgIntrMask                 cBit3

/*--------------------------------------
BitField Name: RxLOofStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while OOF state change event happens in the related line,
               and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLOofStateChgIntrMask                     cBit2

/*--------------------------------------
BitField Name: RxLLofStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while LOF state change event happens in the related line,
               and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLLofStateChgIntrMask                     cBit1

/*--------------------------------------
BitField Name: RxLLosStateChgIntr
BitField Type: R/W/C
BitField Desc: Set 1 while LOS state change event happens in the related line,
               and it is generated an interrupt if it is enabled.
--------------------------------------*/
#define cThaRxLLosStateChgIntrMask                     cBit0


/*------------------------------------------------------------------------------
Reg Name: POH OCn BER State Change Per Line Interrupt Enable
Reg Addr: 0x0814F8
Reg Desc: Used to store BER interrupt enable of each OCn lines.
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: POHOcnBerSFStatChgIntrEnb[1:0]
BitField Type: R/W
BitField Desc: POH OCN BER SF State Change interrupt enable. If a bit is set,
               It enables the BER SF state change interrupt status of its
               corresponding channel is reported to software.
BitField Bits: 17_16
--------------------------------------*/


/*--------------------------------------
BitField Name: POHOcnBerSDStatChgIntrEnb[1:0]
BitField Type: R/W
BitField Desc: POH OCN BER SD State Change interrupt status. If a bit is set,
               It enables the BER SD state change interrupt status of its
               corresponding channel is reported to software.
BitField Bits: 1_0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH OCn BER State Chage Per Line Interrupt Status
Reg Addr: 0x0814F9
Reg Desc: Used to store BER interrupt status of each OCn lines.
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: POHOcnBerSFStatChgIntr[1:0]
BitField Type: R/W
BitField Desc: POH OCN BER SF State Change interrupt status. If a bit is set,
               the BER SF state change interrupt status of its corresponding
               channel is reported to software.
BitField Bits: 17_16
--------------------------------------*/


/*--------------------------------------
BitField Name: POHOcnBerSDStatChgIntr[1:0]
BitField Type: R/W
BitField Desc: POH OCN BER SD State Change interrupt status. If a bit is set,
               the BER SD state change interrupt status of its corresponding
               channel is reported to software.
BitField Bits: 1_0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH OCn  J0 TIM -L State Change Per Line Interrupt Sticky
Reg Addr: 0x008E002
Reg Desc: This is Used to store J0 TIM-L State Change interrupt enable of each OCn lines.
------------------------------------------------------------------------------*/
#define cThaRegPohTimLAlmIntrStatCtrl                0x008E002


/*--------------------------------------
BitField Name: PohOcnJ0TimLIntrStat
BitField Type: R/W
BitField Desc: POH OCN J0 TIM-L state change interrupt sticky of each OCN line.
               If a bit is set, the POH Engine has detected a change of J0 TIM-L current status.
               It will send an interrupt to SW when if the its corresponding POHOcnJ0TIMLStatChgIntrEnb OCN line is set.
BitField Bits: 7_0
--------------------------------------*/
#define cThaPohOcnJ0TimLIntrStatMask(lineId)              (((uint32)cBit0) << (lineId))


/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Alarm Interrupt Enable Control
Reg Addr: 0x00074A00 - 0x00074A07
          The address format for these registers is 0x00074A00 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs
Reg Desc: This is the per Alarm interrupt enable of Rx framer and TOH
          monitoring. Each register is used to store 12 bits to enable
          interrupts when the related alarms in related line happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperAlmIntrEnCtrl            0x074A00

/*
#define cThaUnusedMask                                 cBit31_12
#define cThaUnusedShift                                12
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: RxLReiErrCntrOvfIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable REI Error Counter Overflowed event in the
               related line to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLB1ErrCntrOvflIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLB2ErrCntrOvflIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLS1StbStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable S1 Stable state change event in the related line
               to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLK1StbStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable K1 Stable state change event in the related line
               to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLApsLDefStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable APS-L Defect Stable state change event in the
               related line to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLK2StbStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable K2 Stable state change event in the related line
               to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: RxLRdiILDefStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxLRdiLDefStateChgIntrEnMask              cBit4
#define cThaRxLRdiLDefStateChgIntrEnShift             4

/*--------------------------------------
BitField Name: RxLAisLDefStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable AIS-L Defect Stable state change event in the
               related line to generate an interrupt.
--------------------------------------*/
#define cThaRxLAisLDefStateChgIntrEnMask               cBit3
#define cThaRxLAisLDefStateChgIntrEnShift              3

/*--------------------------------------
BitField Name: RxLOofStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable OOF state change event in the related line to
               generate an interrupt.
--------------------------------------*/
#define cThaRxLOofStateChgIntrEnMask                   cBit2
#define cThaRxLOofStateChgIntrEnShift                  2

/*--------------------------------------
BitField Name: RxLLofStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable LOF state change event in the related line to
               generate an interrupt.
--------------------------------------*/
#define cThaRxLLofStateChgIntrEnMask                   cBit1
#define cThaRxLLofStateChgIntrEnShift                  1

/*--------------------------------------
BitField Name: RxLLosStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable LOS state change event in the related line to
               generate an interrupt.
--------------------------------------*/
#define cThaRxLLosStateChgIntrEnMask                   cBit0
#define cThaRxLLosStateChgIntrEnShift                  0


/*------------------------------------------------------------------------------
Reg Name: OCN Rx Line per Line Interrupt OR Status
Reg Addr: 0x00074A3F
Reg Desc: The register consists of 8 bits for 8 lines at Rx side. Each bit is
          used to store Interrupt OR status of the related line. If there are
          any bits in this register and they are enabled to raise interrupt,
          the Rx line level interrupt bit is set.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxLineperLineIntrOrStat           0x074A3F


/*--------------------------------------
BitField Name: OCNRxLLineIntr[7:0]
BitField Type: R_0
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaOcnRxLLineIntrLineIdMask(lineId)           (cBit0 << (lineId))

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Framer Per Channel Control 1
Reg Addr: 0x00040A00 - 0x00040A07
          The address format for these registers is 0x00040A00 + (SliceId*8192) + lineID
          Where:SliceId (0-1): STS-12 slice ID. Slice #0 for lines from 1 to 4, Slice #1 for lines from 5 to 8
                lineID: (0 - 3) SONET/SDH line IDs
Reg Desc: Each register is used to configure operation modes for Tx framer
          engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of
          the SONET/SDH line being relative with the address of the address of
          the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxFrmrPerChnCtrl1                  0x00040A00


/*-----------
BitField Name: OcnTxApsPat
BitField Type: R/W
BitField Desc: Pattern to insert into APS bytes (K1, K2).
------------*/

/*-----------
BitField Name: OcnTxK1Pat
BitField Type: R/W
BitField Desc: Pattern to insert into K1.
------------*/
#define cThaOcnTxK1PatMask                            cBit30_23
#define cThaOcnTxK1PatShift                           23

/*-----------
BitField Name: OcnTxK2Pat
BitField Type: R/W
BitField Desc: Pattern to insert into K2.
------------*/
#define cThaOcnTxK2PatMask                            cBit22_15
#define cThaOcnTxK2PatShift                           15

/*-----------
BitField Name: OcnTxS1EnB
BitField Type: R/W
BitField Desc: S1 enable
               - 1: Indicate that S1 byte inserted into S1 position is from
                    TOH output look-up table or OH insert port.
               - 0: Enable inserting S1 pattern from the OCN Tx Framer Per
                    Channel Control 2 into S1 position.
------------*/
#define cThaOcnTxS1EnBMask                            cBit14
#define cThaOcnTxS1EnBShift                           14

/*-----------
BitField Name: OcnTxApsEnB
BitField Type: R/W
BitField Desc: APS enable
               - 1: Indicate that APS bytes are inserted from TOH buffer or
                    TOH insert port.
               - 0: Enable insert APSPat[15:0] into APS bytes.
------------*/
#define cThaOcnTxApsEnBMask                           cBit13
#define cThaOcnTxApsEnBShift                          13

/*-----------
BitField Name: OcnTxZ0Md
BitField Type: R/W
BitField Desc: Modes for inserting Z0 bytes
               - 00   : All Z0 bytes are inserted by Z0pat1 in the OCN Z0
                        Insertion Pattern register.
               - 01   : All Z0 bytes are inserted by Z0pat2 the OCN Z0
                        Insertion Pattern register.
               - 10,11: 00: All Z0 bytes are inserted by TOH output look-up
                        table or OH insert port.
------------*/

/*-----------
BitField Name: OcnTxReiLEnB
BitField Type: R/W
BitField Desc: REI enable
               - 1: Disable insertion of REI-L errors detected at Rx side
                    into REI-L positions. REI position will be inserted from
                    TOH output look-up table or OH insert port.
               - 0: Enable automatically inserting REI errors detected at Rx
                    side into REI position.
------------*/

/*-----------
BitField Name: OcnTxRdiLEnB
BitField Type: R/W
BitField Desc: RDI-L enable
               - 1: Disable inserting RDI-L defect detected at Rx side into
                    K2[7:5]. K2[7:5] value will be inserted from TOH output
                    look-up table or OH insert port.
               - 0: Enable automatically inserting RDI-L defect detected at
                    Rx side into K2[7:5].
------------*/


/*--------------------------------------
BitField Name: OCNTxUnused
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/


/*-----------
BitField Name: OcnTxAutoB2EnB
BitField Type: R/W
BitField Desc: Auto B2 enable
               - 1: Disable inserting calculated B2 values into B2 positions
                    automatically. B2 bytes will be inserted from TOH output
                    look-up table or OH insert port.
               - 0: Enable automatically inserting calculated B2 values into
                    B2 positions.
------------*/
#define cThaOcnTxAutoB2EnBMask                        cBit7
#define cThaOcnTxAutoB2EnBShift                       7

/*-----------
BitField Name: OcnTxAutoB1EnB
BitField Type: R/W
BitField Desc: Auto B1 enable
               - 1: Disable inserting calculated B1 values into B1 positions
                    automatically. B1 bytes will be inserted from TOH output
                    look-up table or OH insert port.
               - 0: Enable automatically inserting calculated B1 values into
                    B1 positions.
------------*/
#define cThaOcnTxAutoB1EnBMask                        cBit6
#define cThaOcnTxAutoB1EnBShift                       6

/*-----------
BitField Name: OcnTxAutoA1A2EnB
BitField Type: R/W
BitField Desc: Auto A1A2 enable
               - 1: Disable generating frame patterns (A1 and A2 bytes)
                    automatically to insert into A1, A2 positions. Frame
                    patterns will be inserted from TOH output look-up table or
                    OH insert port.
               - 0: Automatically generate frame patterns to insert into A1,
                    A2 positions.
------------*/
#define cThaOcnTxAutoA1A2EnBMask                      cBit5
#define cThaOcnTxAutoA1A2EnBShift                     5

/*-----------
BitField Name: OcnTxRdiLThresSel
BitField Type: R/W
BitField Desc: Select the number of frames being inserted RDI-L defects when
               receive direction requests generating RDI-L at transmit
               direction.
               - 1: OcnTxRDIThres2[4:0] is selected.
               - 0: TxFrmRDIThres1[4:0] is selected.
------------*/

/*-----------
BitField Name: OcnTxApsProEn
BitField Type: R/W
BitField Desc: Enable to process APS
               - 1: Enable APS process.
               - 0: Disable APS process
------------*/
#define cThaOcnTxApsProEnMask                         cBit3

/*-----------
BitField Name: OcnTxRdiLFrc
BitField Type: R/W
BitField Desc: RDI-L force
               - 1: Force RDI-L defect into transmit data.
               - 0: Not force LOS
------------*/
#define cThaOcnTxRdiLFrcMask                          cBit2
#define cThaOcnTxRdiLFrcShift                         2

/*-----------
BitField Name: OcnTxAisLFrc
BitField Type: R/W
BitField Desc: AIS-L force
               - 1: Force AIS-L defect into transmit data.
               - 0: Not force AIS-L
------------*/
#define cThaOcnTxAisLFrcMask                          cBit1
#define cThaOcnTxAisLFrcShift                         1

/*-----------
BitField Name: OcnTxScrEnB
BitField Type: R/W
BitField Desc: Scramble enable
               - 1: Disable scrambling at Tx framer.
               - 0: Enable scrambling at Tx framer.
------------*/
#define cThaOcnTxScrEnBMask                           cBit0
#define cThaOcnTxScrEnBShift                          0


/*------------------------------------------------------------------------------
Reg Name: OCN Rx Framer Per Channel Control
Reg Addr: 0x00040200-0x00040207
          The address format for these registers is 0x00040200 + lineID
          Where: lineID (0 - 7) SONET/SDH line IDs at receive framer
Reg Desc: Each register is used to configure for Rx framer engine of the line
          being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxFrmrPerChnCtrl                     0x00040200

/*-----------
BitField Name: OcnRxAisLFrc
BitField Type: R/W
BitField Desc: AIS-L Force
               - 1: Force AIS-L state to downstream (to XC). This mode is
                    used while the line is in loopback out mode to avoid
                    misunderstanding data at downstream.
               - 0: Not force AIS-L (Normal mode)
------------*/
#define cThaOcnRxAisLFrcMask                          cBit4
#define cThaOcnRxAisLFrcShift                         4

/*-----------
BitField Name: OcnRxLofMd
BitField Type: R/W
BitField Desc: Mode of LOF.
               - 1: OOF counter is reset in In-frame state.
               - 0: OOF counter is only reset when In-frame state exists
                    continuously more than 3 ms (following GR253
                    recommendation).
------------*/

/*-----------
BitField Name: OcnRxLosThresSel
BitField Type: R/W
BitField Desc: Select the LOS threshold for detecting LOS defect.
               - 1: the LOS threshold register 2 is selected
               - 0: the LOS threshold register 1 is selected
------------*/

/*-----------
BitField Name: OcnRxOofFrc
BitField Type: R/W
BitField Desc: OOF force
               - 1: Force the framing engine to enter OOF state regardless
                    of the framing pattern value.
               - 0: Not force OOF so the engine is in normal mode.
------------*/

/*-----------
BitField Name: OcnRxScrEnB
BitField Type: R/W
BitField Desc: Scramble enable
               - 1: Disable de-scrambling of the coming data stream
               - 0: Enable de-scrambling of the coming data stream
------------*/
#define cThaOcnRxScrEnBMask                           cBit0
#define cThaOcnRxScrEnBShift                          0


/*------------------------------------------------------------------------------
Reg Name: OCN SONET/SDH Line Loop-In Control
Reg Addr: 0x00040017
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegOcnLineLoopInCtrl            0x00040017

#define cThaRegOcnLineLoopInMask(lineId)    (cBit0 << (lineId))
#define cThaRegOcnLineLoopInShift(lineId)   (lineId)

/*------------------------------------------------------------------------------
Reg Name: OCN SONET/SDH Line Loop-Out Control
Reg Addr: 0xF00044
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegOcnLineLoopOutCtrl            0xF00044

#define cThaRegOcnLineLoopOutMask(lineId)    (cBit0 << (lineId))
#define cThaRegOcnLineLoopOutShift(lineId)   (lineId)


/*------------------------------------------------------------------------------
Reg Name: OCN B1 Error Counter
Reg Addr: 0x0074900 - 0x0074907 (R_O); 0x0074940 - 0x0074947 (R2C)
          The address format for these registers is 0x0074900 + lineID (R_O);
          0x0074940 + lineID (R2C)
          Where: lineID (0 - 7) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store B1 error counter for the SONET/SDH
          line being relative with the address of the register. These counters
          are in saturation mode or rollover mode depending on the
          TohMonB1ErrCntrSatMd bit in the OCN counter control register.
------------------------------------------------------------------------------*/
#define cThaRegOcnB1ErrRoCnt                         0x0074900
#define cThaRegOcnB1ErrR2cCnt                        0x0074940

/*-----------
BitField Name: B1ErrCnt
BitField Type: R_O
BitField Desc: The B1 error counter. In case of saturation mode, when a B1
               error counter is over the B1 error counter scan threshold, the
               interrupt is generated, if it is enable.
------------*/
#define cThaB1ErrCntMask                              cBit22_0


/*------------------------------------------------------------------------------
Reg Name: OCN B2 Error Counter
Reg Addr: 0x0074908 - 0x007490F (R_O); 0x0074948 - 0x7494F (R2C)
          The address format for these registers is 0x0074908 + lineID (R_O);
          0x0074948 + lineID (R2C)
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store B2 error counter for the SONET/SDH
          line being relative with the address of the register. These counters
          are in saturation mode or rollover mode depending on the
          TohMonB2ErrCntrSatMd bit in the OCN counter control register.
------------------------------------------------------------------------------*/
#define cThaRegOcnB2ErrRoCnt                         0x0074908
#define cThaRegOcnB2ErrR2cCnt                        0x0074948

/*-----------
BitField Name: B2ErrCnt
BitField Type: R_O
BitField Desc: The B2 error counter. In case of saturation mode, when a B2
               error counter is over the B2 error counter scan threshold, the
               interrupt is generated, if it is enable.
------------*/
#define cThaOcnB2ErrCntMask                           cBit22_0


/*------------------------------------------------------------------------------
Reg Name: OCN REI-L Error Counter
Reg Addr: 0x0074928 - 0x007492F (R_O); 0x0074968 - 0x7496F (R2C)
          The address format for these registers is 0x0074928 + lineID (R_O);
          0x0074968 + lineID (R2C)
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store REI-L error counter for the SONET/SDH
          line being relative with the address of the register. These counters
          are in saturation mode or rollover mode depending on the
          TohMonReiErrCntrSatMd bit in the OCN counter control register.
------------------------------------------------------------------------------*/
#define cThaRegOcnReiLErrRoCnt                        0x0074928
#define cThaRegOcnReiLErrR2cCnt                       0x0074968

/*-----------
BitField Name: OcnReiLErrCnt
BitField Type: R_O
BitField Desc: The REI error counter. In case of saturation mode, when a REI
               error counter is over the REI error counter scan threshold, the
               interrupt is generated, if it is enable.
------------*/
#define cThaOcnReiLErrCntMask                         cBit22_0

/*------------------------------------------------------------------------------
Reg Name: OCN Tx Framer Per Channel Control 2
Reg Addr: 0x00040A10 - 0x00040A17
          The address format for these registers is 0x00040A10 + lineID
          Where: lineID: (0 - 7) SONET/SDH line IDs
Reg Desc: Each register is used to configure S1 pattern to insert into S1
          position of the SONET/SDH line being relative with the address of
          the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxFrmrPerChnCtrl2                   0x00040A10

/*-----------
BitField Name: OcnTxS1Pat
BitField Type: R/W
BitField Desc: S1 pattern for insertion at Tx framer.
------------*/
#define cThaOcnTxS1PatMask                            cBit7_0


/*------------------------------------------------------------------------------
Reg Name: OCN S1 Monitoring Status
Reg Addr: 0x0074920 - 0x0074927
          The address format for these registers is 0x0074920 + lineID
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store status when monitoring S1 bytes of
          the SONET/SDH line being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnS1MonStat                           0x0074920

/*-----------
BitField Name: SameS1Cnt
BitField Type: R/W
BitField Desc: The number of same contiguous S1 bytes. It is held at StbS1Thr
               value when the number of same contiguous S1 bytes is equal to
               or more than the StbS1Thr value. In this case, S1 bytes are
               stable.
------------*/

/*-----------
BitField Name: StbS1Val
BitField Type: R/W
BitField Desc: Stable S1 value. It is updated when detecting a new stable
               value.
------------*/
#define cThaStbS1ValMask                              cBit15_8
#define cThaStbS1ValShift                             8

/*-----------
BitField Name: CurS1
BitField Type: R/W
BitField Desc: Current S1 byte or stable value of S1 byte depending on
               S1Stable bit.
------------*/


/*------------------------------------------------------------------------------
Reg Name: OCN K1 Monitoring Status
Reg Addr: 0x0074910 - 0x0074917
          The address format for these registers is 0x0074910 + lineID
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store status when monitoring K1 bytes of
          the SONET/SDH line being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnK1MonStat                            0x0074910

/*-----------
BitField Name: CurApsDefect
BitField Type: R/W
BitField Desc: current APS Defect.
               - 1: APS defect is detected from APS bytes.
               - 0: APS defect isn't detected from APS bytes.
------------*/

/*-----------
BitField Name: SameK1Cnt
BitField Type: R/W
BitField Desc: The number of same contiguous K1 bytes. It is held at StbK1Thr
               value when the number of same contiguous K1 bytes is equal to
               or more than the StbK1Thr value. In this case, K1 bytes are
               stable.
------------*/

/*-----------
BitField Name: StbK1Val
BitField Type: R/W
BitField Desc: Stable K1 value. It is updated when detecting a new stable
               value.
------------*/
#define cThaStbK1ValMask                              cBit15_8
#define cThaStbK1ValShift                             8

/*-----------
BitField Name: CurK1
BitField Type: R/W
BitField Desc: Current K1 byte.
------------*/


/*------------------------------------------------------------------------------
Reg Name: OCN K2 Monitoring Status
Reg Addr: 0x0074918 - 0x007491F
          The address format for these registers is 0x0074918 + lineID
          Where: lineID (0 - 3) SONET/SDH line IDs at TOH monitoring
Reg Desc: Each register is used to store status when monitoring K2 bytes of
          the SONET/SDH line being relative with the address of the register.
------------------------------------------------------------------------------*/
#define cThaRegOcnK2MonStat                           0x0074918

/*-----------
BitField Name: Internal
BitField Type: R/W
BitField Desc:
------------*/

/*-----------
BitField Name: CurAisL
BitField Type: R/W
BitField Desc: Current AISL defect.
               - 1: AIS-L defect is detected from K2 bytes.
               - 0: AIS-L defect isn't detected from K2 bytes.
------------*/

/*-----------
BitField Name: CurRdiL
BitField Type: R/W
BitField Desc: current RDI-L Defect.
               - 1: RDI-L defect is detected from K2 bytes.
               - 0: RDI-L defect isn't detected from K2 bytes.
------------*/

/*-----------
BitField Name: SameK2Cnt
BitField Type: R/W
BitField Desc: The number of same contiguous K2 bytes. It is held at StbK2Thr
               value when the number of same contiguous K2 bytes is equal to
               or more than the StbK2Thr value. In this case, K2 bytes are
               stable.
------------*/

/*-----------
BitField Name: StbK2Val
BitField Type: R/W
BitField Desc: Stable K2 value. It is updated when detecting a new stable
               value.
------------*/
#define cThaStbK2ValMask                              cBit15_8
#define cThaStbK2ValShift                             8

/*-----------
BitField Name: CurK2
BitField Type: R/W
BitField Desc: Current K2 byte.
------------*/


/*------------------------------------------------------------------------------
Reg Name: OCN J0 Insertion Buffer (Thalassa)
Reg Addr: 0x0075800 - 0x7587F
          The address format for these registers is 0x0075800 + lineId*16 +
          wordID
          Where: wordId: (0 - 15): Double Word ID in 64-byte message.
          LineID: (0 - 7) SONET/SDH line IDs
Reg Desc: Each register stores one J0 byte in 64-byte message of the
          SONET/SDH line being relative with the address of the address of the
          register inserted into J0 positions.
------------------------------------------------------------------------------*/
#define cThaRegOcnJ0MsgInsBuf                            0x0075800

/*-----------
BitField Name: OcnTxJ0Msg
BitField Type: R/W
BitField Desc: J0 pattern for insertion.
------------*/
#define cThaOcnTxJ0MsgMask(position)                    ((uint32) cBit7_0 << ((uint32)(8 * position)))
#define cThaOcnTxJ0MsgShift(position)                   (8 * position)


/*------------------------------------------------------------------------------
Reg Name: POH SDH/SONET Control (POH J0 Control)
Reg Addr: 0x08881F-0x08883F
          The address format for these registers is 0x50_881F + LineID*128
          Where: LineID: 0 -> 3
Reg Desc: SDH/SONET Control register used for control the SDH/SONET monitor
          engine.
------------------------------------------------------------------------------*/
#define cThaRegPohSdhSonetCtrl                         0x08881F



/*-----------
BitField Name: B2BlkMd
BitField Type: R/W
BitField Desc: B2 Block mode
               - 1: The B2 errors accumulator counter counts B3 errors with
                   block mode
               - 0: The B2 errors accumulator counter counts B3 errors with
                   bit mode.
-----------*/

/*-----------
BitField Name: B2CntScanEn
BitField Type: R/W
BitField Desc: Enable to scan B2 counter. Active high.
-----------*/

/*-----------
BitField Name: StiOomIntrEn
BitField Type: R/W
BitField Desc: STI Out Of Multiframe Interrupt Enable. Enable this channel to
               send Out Of Multiframe interrupt signal.
-----------*/

/*-----------
BitField Name: StiTimIntrEn
BitField Type: R/W
BitField Desc: Section Trace Identifier Mismatch Interrupt Enable. Enable this
               channel to send Section Trace Identifier Mismatch Interrupt
               signal.
-----------*/

/*-----------
BitField Name: StiStbChgIntrEn
BitField Type: R/W
BitField Desc: Section Trace Identifier Message stable change Interrupt
               Enable. Enable this channel to send Section Trace Identifier
               Message stable changed interrupt signal.
-----------*/

/*-----------
BitField Name: J0TimMonEnable
BitField Type: R/W
BitField Desc: Enable/Disable TIM monitoring
               - 1: Enable to monitor J0 TIM function
               - 0: Disable to monitor J0 TIM function
-----------*/
#define cThaJ0TimMonEnableMask                        cBit12
#define cThaJ0TimMonEnableShift                       12

/*-----------
BitField Name: J0TimExpMd
BitField Type: R/W
BitField Desc: J0 TIM monitoring with Expected mode.
When this bit is set to 1 the POH Engine will compare the incoming
TTI message with Expected Message.
If this bit is 0 the POH Engine will store the incoming
TTI message to the registers
-----------*/
#define cThaJ0TimExpMdMask                               cBit11
#define cThaJ0TimExpMdShift                              11

/*-----------
BitField Name: J0FrmMd
BitField Type: R/W
BitField Desc: Indicates J0 Frame Mode
               - 2'b00: byte mode.
               - 2'b01: TTI message with 16 bytes length. The frame format
                   according ITU-T
               - 2'b10: TTI message with 64 bytes length. The frame format
                   has CR and LF at the end of frame
               - 2'b11: TTI message float mode. (jn byte captured cycle of a
                   buffer 64 bytes).
-----------*/
#define cThaJ0FrmMdMask                               cBit10_9
#define cThaJ0FrmMdShift                              9

/*-----------
BitField Name: J0MsgCapEnb
BitField Type: R/W
BitField Desc: Enable to capture full TTI message. There is a pool of
32 Buffers which are used for Message Capture Function.
    1: Enable to capture full TTI message. When it is set, the TTI message will be captured and written to J1MsgCapBUFID current message location.
    0: When TTI Message is captured this bit is set to ZERO.
-----------*/
#define cThaJ0MsgCapEnbMask                               cBit8
#define cThaJ0MsgCapEnbShift                              8

/*-----------
BitField Name: J0MsgCapFrmMd
BitField Type: R/W
BitField Desc: Message Capture Frame Mode.
This configuration field can be difference with J0TIMFrmMd
                - 2b00: TTI message float mode. (J0 byte captured cycle of a buffer 64 bytes).
                - 2b01: TTI message capture with 16 bytes length. The frame format according ITU-T
                - 2b10: TTI message capture with 64 bytes length. The frame format has CR and LF at the end of frame
                - 2b11: TTI message capture float mode. (J1 byte captured cycle of a buffer 64 bytes).
-----------*/
#define cThaJ0MsgCapFrmMdMask                                 cBit7_6
#define cThaJ0MsgCapFrmMdShift                                6

/*-----------
BitField Name: J0BufID
BitField Type: R/W
BitField Desc: When Jn_mon_mode asserted, this register is message buffer ID,
               that is location to store Trace Trail current message and
               accepted (or expected) message. When byte mode activated, This
               value is J0 bits[5:0] of J0 byte expected value.
-----------*/
#define cThaJ0BufIDMask                               cBit5_0
#define cThaJ0BufIDShift                              0


/*------------------------------------------------------------------------------
Reg Name: POH CPE J0 TTI Message
Reg Addr: 0x09C000 - 0x9DFFF
          The address format for these registers is 0x09C000 + LineID*512 + 31*16 + BID
          Where: LineID: 0 -> 7
                 BID: 0 -> 15
Reg Desc: This is the STI Message Storage. Each location has 4 bytes.
          The first byte is located at MSB this register.
          If the J0TimExpMd bit is set, these registers are used to store the STI expected message.
          Other wise when the J0TimExpMd bit is clear, these registers are used to store the STI received message.
------------------------------------------------------------------------------*/
#define cThaRegPohJnTtiMsgBuf                    0x09C000


#define cThaRxJnMsgByteMask(byteId)             ((uint32)cBit31_24 >> (uint32)((byteId) * 8))
#define cThaRxJnMsgByteShift(byteId)            (8 * (3 - ((byteId) % 4)))


/*-------------------------------------------------------------------------------
                                  STS PATH
* ------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC per Alarm Current Status
Reg Addr: 0x000521C0 - 0x000541CB
          The address format for these registers is 0x000521C0 + SliceID*8192
          + Stsid
          The address format for these registers is
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Alarm current status of STS/VC pointer interpreter
          and STS/VC VT/TU Demux. Each register is used to store 6 bits to
          store current status of 6 alarms in the STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCperAlmCurrentStat               0x0521C0


/*--------------------------------------
BitField Name: StsPiAisCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiUneqPCurStatMask                        cBit3

/*--------------------------------------
BitField Name: StsPiAisCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiAisCurStatMask                        cBit2

/*--------------------------------------
BitField Name: StsPILopCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPILopCurStatMask                        cBit1

/*--------------------------------------
BitField Name: StsLomCurStatus
BitField Type: R/W
BitField Desc: LOM current status in the related STS/VC. When it changes for 0
               to 1 or vice versa, the  StsPiStsLomStateChgIntr bit in the OCN
               Rx STS/VC per Alarm Interrupt Status register of the related
               STS/VC is set.
--------------------------------------*/
#define cThaStsLomCurStatMask                          cBit0


/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC per Alarm Interrupt Status
Reg Addr: 0x000521A0 - 0x000541AB
          The address format for these registers is 0x000521A0 + SliceID*8192
          + Stsid
          The address format for these registers is
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Alarm interrupt status of STS/VC pointer interpreter
          and STS/VC VT/TU Demux. Each register is used to store 6 sticky bits
          for 6 alarms in the STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCperAlmIntrStat             0x0521A0


/*--------------------------------------
BitField Name: StsPiStsConcDetIntr
BitField Type: R/W/C
BitField Desc: Set to 1 while an Concatenation Detection event is detected at
               STS/VC pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiStsNewPtrDetIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiStsNdfIntr
BitField Type: R/W/C
BitField Desc: Set to 1 while an NDF event is detected at STS/VC pointer
               interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiAisStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiUneqPStateChgIntrMask                   cBit3

/*--------------------------------------
BitField Name: StsPiAisStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiAisStateChgIntrMask                   cBit2

/*--------------------------------------
BitField Name: StsPILopStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPILopStateChgIntrMask                   cBit1

/*--------------------------------------
BitField Name: StsLomStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsLomStateChgIntrMask                     cBit0


/*------------------------------------------------------------------------------
Reg Name: OCN Rx PP Per STS/VC Payload Control
Reg Addr: 0x000520C0 - 0x000540CB
          The address format for these registers is 0x000520C0 + SliceID*8192 +
          StsID
          Where: SliceID: (0) STS-12 slice ID
          Stsid: (0-11) STS-1/VC-3/TUG-3 ID in the STS-12 slice
Reg Desc: These registers configure modes of STS-1/VC-3/TUG-3 channels at Rx
          pointer processor. Each register is used to configure the
          STS-1/VC-3/TUG-3 being relative with the address of the register to
          be in VT/TU mode or not. If it is in VT/TU mode, it is also used to
          configure this channel is in STS-1/VC-3 or TUG-3 mode and types of
          VT/TUs in the STS-1/VC-3/TUG-3.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxPpPerStsVcPldCtrl                 0x000520C0

/*-----------
BitField Name: OcnRxPpStsDemuxSpeType
BitField Type: R/W
BitField Desc: the kind of SPE: TUG-3 or VC-3/STS SPE
               - 0: Disable processing pointers of all VT/TUs in this SPE.
               - 1: VC-3 type or STS SPE containing VT/TU exception TU-3
               - 2: TUG-3 type containing VT/TU exception TU-3
               - 3: TUG-3 type containing TU-3.
------------*/
#define cThaOcnRxPpStsDemuxSpeTypeMask                cBit15_14
#define cThaOcnRxPpStsDemuxSpeTypeShift               14

/*-----------
BitField Name: OcnRxPpStsDemuxTug2Type
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2
               - 0: TU11 type
               - 1: TU12 type
               - 2: VT3 type
               - 3: TU2 type
------------*/
#define cThaOcnRxPpStsDemuxTug2TypeMask(tug2Id)                               \
    (cBit1_0 << (word)((tug2Id) * 2))
#define cThaOcnRxPpStsDemuxTug2TypeShift(tug2Id)    ((tug2Id) * 2)

/*--------------------------------------
BitField Name: OCNRxPpStsDemuxTug26Type[1:0]
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2 #6.
               - 0: TU11 type
               - 1: TU12 type
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNRxPpStsDemuxTug25Type[1:0]
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2 #5
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNRxPpStsDemuxTug24Type[1:0]
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2 #4
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNRxPpStsDemuxTug23Type[1:0]
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2 #3
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNRxPpStsDemuxTug22Type[1:0]
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2 #2
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNRxPpStsDemuxTug21Type[1:0]
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2 #1
--------------------------------------*/

/*--------------------------------------
BitField Name: OCNRxPpStsDemuxTug20Type[1:0]
BitField Type: R/W
BitField Desc: Define types of VT/TUs in TUG-2 #0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Alarm Status
Reg Addr: 0x008E240-0x008E25F
          The address format for these registers is 0x008E240 + SLICE*16 +
          STSID
          Where,
          SLICE is a Slice ID. It is from 0 to 1.
          STSID is a STS ID. It is from 0 to 11.
Reg Desc: This is the POH CPE STS alarm current status register. It will be
          used to indicate current status of the event.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStsAlmStat                     0x08E240
#define cThaRegPohCpeTu3AlmStat                     0x08E250


/*--------------------------------------
BitField Name: POHStsTtiOom
BitField Type: R/W
BitField Desc: It is the STS TTI Out of Multiframe current status bit. It will
               be set to one when there is a STS TTI OOM state is detected by
               the POH CPE engine and it clears to zero when the POH CPE engine
               does not detect STS TTI OOM state.
BitField Bits: 10
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsTtiTim
BitField Type: R/W
BitField Desc: It is the STS TTI TIM current status bit. It will be set to one
               when there is a STS TTI TIM state is detected by the POH CPE
               engine and it clears to zero when the POH CPE does not detect
               STS TTI TIM state.
BitField Bits: 9
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsB3CntExc
BitField Type: R/W
BitField Desc: It is the B3 Counter Exceed current status bit. It will be set
               to one when there is a B3 Counter Exceed state is detected by
               POH CPE engine and it clears to zero when POH CPE engine does
               not detect B3 Counter Exceed state.
BitField Bits: 8
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsReiCntExc
BitField Type:
BitField Desc: It is the REI-P Counter Exceed current status bit. It will be
               set to one when there is a REI-P Counter Exceed state is
               detected by POH CPE engine and it clears to zero when POH CPE
               engine does not detect REI-P Counter Exceed state.
BitField Bits: 7
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsPlm
BitField Type: R/W
BitField Desc: It is the PLM-P current status bit. t will be set to one when
               there is a PLM-P state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect PLM-P state.
BitField Bits: 6
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsVcAis
BitField Type: R/W
BitField Desc: It is the VC-AIS-P  current status bit. t will be set to one
               when there is a VC-AIS-P state is detected by POH CPE engine and
               it clears to zero when POH CPE engine does not detect VC-AIS-P
               state.
BitField Bits: 5
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsUneq
BitField Type: R/W
BitField Desc: It is the Uneq-P current status bit.t will be set to one when
               there is a UNEQ-P state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect UNEQ-P state.
BitField Bits: 4
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsTtiMsgStb
BitField Type: R/W
BitField Desc: It is the STS TTI Message Stable current status bit. It will be
               set to one when STS TTI message stable state is detected by POH
               CPE engine and it clears to zero when POH CPE engine does not
               detect STS TTI message stable state.
BitField Bits: 3
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsApsStb
BitField Type: R/W
BitField Desc: It is the APS-P Stable current status bit. It will be set to one
               when APS-P stable state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect APS-P stable
               state.
BitField Bits: 2
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsRdiStb
BitField Type: R/W
BitField Desc: It is the RDI-P Stable current status bit.It will be set to one
               when RDI-P stable state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect RDI-P stable
               state.
BitField Bits: 1
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsPslStb
BitField Type: R/W
BitField Desc: It is the SL-P  Stable current status bit.It will be set to one
               when SL-P stable state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect SL-P stable
               state.
BitField Bits: 0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Alarm Interrupt Sticky
Reg Addr: 0x008E220-0x008E23F
          The address format for these registers is 0x008E220 + SLICE*16 +
          STSID
          Where,
          SLICE is a Slice ID. It is from 0 to 1.
          STSID is a STS ID. It is from 0 to 11.
Reg Desc: This is the POH CPE STS alarm interrupt sticky register. It will be
          used to indicate and sticky the event states change when the POH CPE
          STS engine detected a changing of  event state.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStsAlmIntrStk                  0x08E220
#define cThaRegPohCpeTu3AlmIntrStk                  0x08E230


/*--------------------------------------
BitField Name: POHStsTtiOomInt
BitField Type: R/W/C
BitField Desc: It is the TTI Out of Multiframe Interrupt stiky bit. It will be
               set to one when there is a STS TTI OOM state detected by the POH
               CPE engine.
BitField Bits: 10
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsTtiTimInt
BitField Type: R/W/C
BitField Desc: It is the TTI TIM interrupt sticky bit. It will be set to one
               when there is a STS TTI TIM state detected by the POH CPE
               engine.
BitField Bits: 9
--------------------------------------*/
#define cThaPohStsTtiTimIntMask                        cBit9

/*--------------------------------------
BitField Name: POHStsB3CntExcInt
BitField Type: R/W/C
BitField Desc: It is the B3 Counter Exceed interrupt sticky bit. It will be set
               to one when there is a B3  Counter Exceed state detected by the
               POH CPE engine.
BitField Bits: 8
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsReiCntExcInt
BitField Type: R/W/C
BitField Desc: It is the REI-P Counter Exceed interrupt sticky bit. It will be
               set to one when there is a REI_P Count Exceed state detected by
               the POH CPE engine
BitField Bits: 7
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsPlmInt
BitField Type: R/W/C
BitField Desc: It is the PLM-P interrupt sticky bit. It will be set to one when
               there is a PLM_P detected by the POH CPE engine.
BitField Bits: 6
--------------------------------------*/
#define cThaPohStsPlmIntMask                           cBit6

/*--------------------------------------
BitField Name: POHStsVcAisInt
BitField Type: R/W/C
BitField Desc: It is the VC-AIS  interrupt sticky bit. It will be set to one
               when there is a VC_AIS de tected by the POH CPE engine.
BitField Bits: 5
--------------------------------------*/
#define cThaPohStsVcAisIntMask                         cBit5

/*--------------------------------------
BitField Name: POHStsUneqInt
BitField Type: R/W/C
BitField Desc: It is the Uneq-P interrupt sticky bit. It will be set to one
               when there is a UNEQ_P detected by POH CPE engine.
BitField Bits: 4
--------------------------------------*/
#define cThaPohStsUneqIntMask                          cBit4

/*--------------------------------------
BitField Name: POHStsTtiMsgStbChgInt
BitField Type: R/W/C
BitField Desc: It is the STS TTI Message Stable Change interrupt sticky bit. It
               will be set to one when there is a change of STS TTI message
               state at the POH CPE engine.
BitField Bits: 3
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsApsStbChgInt
BitField Type: R/W/C
BitField Desc: It is the APS-P Stable Change interrupt sticky bit. It will be
               set to one when there is a change of APS-P state at the POH CPE
               engine.
BitField Bits: 2
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsRdiStbChgInt
BitField Type: R/W/C
BitField Desc: It is the RDI-P Stable Change interrupt sticky bit. It will be
               set to one when there is a change of RDI_P state at the POH CPE
               engine.
BitField Bits: 1
--------------------------------------*/
#define cThaPohStsRdiStbChgIntMask                     cBit1

/*--------------------------------------
BitField Name: POHStsPslStbChgInt
BitField Type: R/W/C
BitField Desc: It is the SL-P  Stable Change interrupt sticky bit. It will be
               set to one when there is a change of Path SL state at the POH
               CPE engine.
BitField Bits: 0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE Status 2
Reg Addr: 0x089800-0x089FFF
          The address format for these registers is 0x50_9800 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 3
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc: This is CPE Status 2 register. There are two cases to use the POH
          CPE Status 2 register, one for STS/TU3 and one for VT.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStat2                            0x089800

/*------------------------------------------------------------------------------
Reg Name: In case of STS/TU3
Reg Addr: 0.3.5.1. In case of STS/TU3
Reg Desc:
------------------------------------------------------------------------------*/
/*In case of STS/TU3 in 10.3.5.1.In case of STS/TU3
 have declare in 10.3.3.1. In case of STS/TU3 */

/*-----------
BitField Name: ApsPVal
BitField Type: R_O
BitField Desc: The APS current value
-----------*/

/*-----------
BitField Name: ApsPExtraVal
BitField Type: R_O
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: ERdiPVal
BitField Type: R_O
BitField Desc: The ERDI-P current value
-----------*/
#define cThaERdiPValMask                              cBit15_13
#define cThaERdiPValShift                             13

/*-----------
BitField Name: ERdiPExtraVal
BitField Type: R_O
BitField Desc: )
-----------*/

/*-----------
BitField Name: C2PslVal
BitField Type: R_O
BitField Desc: PSL current value
-----------*/
#define cThaC2PslValMask                              cBit7_0
#define cThaC2PslValShift                             0

/*------------------------------------------------------------------------------
Reg Name: In case of VT
Reg Addr: 0.3.5.2. In case of VT
Reg Desc:
------------------------------------------------------------------------------*/

/*-----------
BitField Name: ApsVExtraVal
BitField Type: R_O
BitField Desc: The APS-V extra value (K4[b3:b4] )
-----------*/

/*-----------
BitField Name: ApsVVal
BitField Type: R_O
BitField Desc: : The APS-V value (K4[b1:b2])
-----------*/

/*-----------
BitField Name: ERdiVVal
BitField Type: R_O
BitField Desc: The ERDI-V value (V5[b8],K4[b5:b7])
-----------*/
#define cThaERdiVValMask                              cBit15_12
#define cThaERdiVValShift                             12

/*-----------
BitField Name: ERdiVExtraVal
BitField Type: R_O
BitField Desc: reserved bit)
-----------*/

/*-----------
BitField Name: VslCur
BitField Type: R_O
BitField Desc: The V- Signal Label current value
-----------*/
#define cThaVslCurMask                                cBit5_3
#define cThaVslCurShift                               3

/*-----------
BitField Name: VslAcp
BitField Type: R_O
BitField Desc: The V- Signal Label Accepted value
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE BER Report
Reg Addr: 0x084800-0x504DFD
          The address format for these registers is 0x504800 + STSID*128 +
          OcID*32 + VtID
          Where: STSID: 0-> 11
          OcID: 0 -> 1
          VtID: 0 -> 27 (for VT), 28 (for STS), 29(for TU)
Reg Desc: The report register is used for report the result of STS/VT BER
          detection to software.
------------------------------------------------------------------------------*/

/*-----------
BitField Name: POHBerMonSfDef
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: POHBerMonSfCur
BitField Type: R/W
BitField Desc: BER Monitoring SF Current Status
-----------*/

/*-----------
BitField Name: POHBerMonSdDef
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: POHBerMonSdCur
BitField Type: R/W
BitField Desc: BER Monitoring SD Current Status
-----------*/

/*-----------
BitField Name: POHBerMonCur
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 3'd0: BER = 10-3
               - 3'd1: BER = 10-4
               - 3'd2: BER = 10-5
               - 3'd3: BER = 10-6
               - 3'd4: BER = 10-7
               - 3'd5: BER = 10-8
               - 3'd6: BER = 10-9
               - 3'd7: BER = 10-10
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE BER Per Channel Interrupt Status
Reg Addr: 0x081480 - 0x0814AF
          The address format for these registers is 0x081480 + STSID*4 + OcID.
          Where: STSID: 0 ->11
          OcID: 0 -> 1
Reg Desc: The interrupt status of 672 VT channels & 24 STSs is reported to
          software via 24 POH CPE BER Per Channel Interrupt Status registers.
          Each register stores the interrupt status of 30 POH CPE channels.
          The first register at 0x081480 is for first 30 channels of STS#0 &
          OC#0; the second register at 0x081440 is for next and so on. Bit 0
          of register 0 is for channel 0; bit 1 of register 0 is for channel
          1; and so on.
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: POHTuBerIntrStat
BitField Type: W2C
BitField Desc: POH TU BER Interrupt status
BitField Bits: 29
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsBerIntrStat
BitField Type: W2C
BitField Desc: POH STS BER Interrupt status
BitField Bits: 28
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtBerIntrStat
BitField Type: W2C
BitField Desc: POH VT BER Interrupt Status. If a bit is set, the interrupt
               status of its corresponding channel is reported to software.
BitField Bits: 27_0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC per Alarm Interrupt Enable Control
Reg Addr: 0x00052180 - 0x0005418B
          The address format for these registers is 0x00052180 + SliceID*8192
          + Stsid
          The address format for these registers is
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Alarm interrupt enable of STS/VC pointer interpreter
          and STS/VC VT/TU Demux. Each register is used to store 6 bits to
          enable interrupts when the related alarms in related STS/VC happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCperAlmIntrEnCtrl           0x052180


/*--------------------------------------
BitField Name: StsPiStsConcDetIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable Concatenation Detection event in the related
               STS/VC to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiStsNewPtrDetIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiStsNdfIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable NDF event in the related STS/VC to generate an
               interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiAisStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: StsPiAisStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPiAisStateChgIntrEnMask                 cBit2
#define cThaStsPiAisStateChgIntrEnShift                2

/*--------------------------------------
BitField Name: StsPILopStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsPILopStateChgIntrEnMask                 cBit1
#define cThaStsPILopStateChgIntrEnShift                1

/*--------------------------------------
BitField Name: StsLomStateChgIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change LOM state event from LOM to no LOM and
               vice versa in the related STS/VC to generate an interrupt.
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Alarm Interrupt Enable Control
Reg Addr: 0x008E200-0x008E21F
          The address format for these registers is 0x008E200 + SLICE*16 +
          STSID
          Where,
          SLICE is a Slice ID. It is from 0 to 1.
          STSID is a STS ID. It is from 0 to 11.
Reg Desc: This is the per channel POH CPE STS alarm interrupt enable control
          register. It is using to enable each event an alarm when it is
          detected.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStsAlmIntrEnCtrl               0x08E200
#define cThaRegPohCpeTu3AlmIntrEnCtrl               0x08E210


/*--------------------------------------
BitField Name: POHStsTtiOomIntEnb
BitField Type: R/W
BitField Desc: It is the TTI-P message Out Of Multiframe interrupt enable
               control. It is active high. When it is high, the POH CPE will
               send an alarm signal to SW when the CPE engine detected a TTI-P
               OOM state.
BitField Bits: 10
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsTtiTimIntEnb
BitField Type: R/W
BitField Desc: It is the TTI-P TIM interrupt enable control. It is active high.
               When it is high, the POH CPE will send an alarm interrupt signal
               to SW when the CPE engine detected a TIM-P event.
BitField Bits: 9
--------------------------------------*/
#define cThaPohStsTtiTimIntEnbMask                     cBit9
#define cThaPohStsTtiTimIntEnbShift                    9

/*--------------------------------------
BitField Name: POHStsB3CntExcIntEnb
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 8
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsReiCntExcIntEnb
BitField Type: R/W
BitField Desc: It is the REI-P counter exceed a treshold interrupt enable
               control. It is active high. When it is high, the POH CPE will
               send an alarm interrupt signal to SW when the CPE engine
               detected the REI-P counter exceeded a treshold event.
BitField Bits: 7
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsPlmIntEnb
BitField Type: R/W
BitField Desc: It is the PLM-P defected interrupt enable control. It is active
               high. When it is high, the POH CPE will send an alarm interrupt
               signal to SW when the CPE engine detected PLM-P defected event.
BitField Bits: 6
--------------------------------------*/
#define cThaPohStsPlmIntEnbMask                        cBit6
#define cThaPohStsPlmIntEnbShift                       6

/*--------------------------------------
BitField Name: POHStsVcAisIntEnb
BitField Type: R/W
BitField Desc: It is the VC AIS defected interrupt enable control. It is active
               high. When it is high, the POH CPE will send an alarm interrupt
               signal to SW when the CPE engine detected VC AIS defected event.
BitField Bits: 5
--------------------------------------*/
#define cThaPohStsVcAisIntEnbMask                      cBit5
#define cThaPohStsVcAisIntEnbShift                     5

/*--------------------------------------
BitField Name: POHStsUneqIntEnb
BitField Type: R/W
BitField Desc: It is the Uneq-P defected interrupt enable control. It is active
               high. When it is high, the POH CPE will send an alarm interrupt
               signal to SW when the CPE engine detected Uneq-P defected event.
BitField Bits: 4
--------------------------------------*/
#define cThaPohStsUneqIntEnbMask                       cBit4
#define cThaPohStsUneqIntEnbShift                      4

/*--------------------------------------
BitField Name: POHStsTtiMsgStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the TTI-P message Stable Changed interrupt enable control.
               It is active high. When it is high, the POH CPE will send an
               alarm interrupt signal to SW when the CPE engine detected a
               stable change of TTI-P message.
BitField Bits: 3
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsApsStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the APS-P Stable Changed interrupt enable control. It is
               active high. When it is high, the POH CPE will send an alarm
               interrupt signal to SW when the CPE engine detected a stable
               change of APS-P.
BitField Bits: 2
--------------------------------------*/

/*--------------------------------------
BitField Name: POHStsRdiStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the RDI-P Stable Changed interrupt enable control. It is
               active high. When it is high, the POH CPE will send an alarm
               interrupt signal to SW when the CPE engine detected a stable
               change of RDI-P.
BitField Bits: 1
--------------------------------------*/
#define cThaPohStsRdiStbChgIntEnbMask                  cBit1
#define cThaPohStsRdiStbChgIntEnbShift                 1

/*--------------------------------------
BitField Name: POHStsPslStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the Path Signal Lable Stable Changed interrupt enable
               control. It is active high. When it is high, the POH CPE will
               send an alarm interrupt signal to SW when the CPE engine
               detected a stable change of Path SL.
BitField Bits: 0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE BER Control
Reg Addr: 0x084000-0x5045FD
          The address format for these registers is 0x504000 + STSID*128 +
          OcID*32 + VtID
          Where: STSID: 0-> 11
          OcID: 0 -> 3
          VtID: 0 -> 27 (for VT), 28 (for STS), 29(for TU)
Reg Desc: The control register is used for STS/VT BER detection
------------------------------------------------------------------------------*/

/*-----------
BitField Name: POHBerMonSfThres
BitField Type: R/W
BitField Desc: The threshold to declare SF condition happen.
               - 3'd0: Set SF if BER = 10-3
               - 3'd1: Set SF if BER >= 10-4
               - 3'd2: Set SF if BER >= 10-5
               - 3'd3: Set SF if BER >= 10-6
               - 3'd4: Set SF if BER >= 10-7
               - 3'd5: Set SF if BER >= 10-8
               - 3'd6: Set SF if BER >= 10-9
               - 3'd7: Set SF if BER >= 10-10
-----------*/

/*-----------
BitField Name: POHBerMonEn
BitField Type: R/W
BitField Desc: Enable BER monitors this channel.
-----------*/

/*-----------
BitField Name: POHBerMonRate
BitField Type: R/W
BitField Desc: For STS map (depending on Channel_Type): For VT map (depending
               on Channel_Type):
               - 3'b000: STS1
               - 3'b001: STS3c
               - 3'b010: STS6c
               - 3'b011: STS9c
               - 3'b100: STS12c
               - 3'b101: Unused
               - 3'b110: Unused
               - 3'b111: Unused
               - 3'b000: VT1.5 (TU11) rate
               - 3'b001: VT2 (TU12) rate
               - 3'b010: VT3 rate
               - 3'b011: VT6 (TU2) rate
               - 3'b1xx: Unused
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
POHBerMonRate in 10.6.23.POH CPE BER Control
 have declare in 10.6.17. POH OCn BER Control
*/

/*-----------
BitField Name: POHBerMonSfIntrEn
BitField Type: R/W
BitField Desc: BER Monitoring SF Interrupt Enable. Enable this channel to send
               SF Interrupt signal.
-----------*/

/*-----------
BitField Name: POHBerMonSdIntrEn
BitField Type: R/W
BitField Desc: BER Monitoring SD Interrupt Enable. Enable this channel to send
               SD Interrupt signal.
-----------*/

/*-----------
BitField Name: POHBerMonSdThres
BitField Type: R/W
BitField Desc: The threshold to declare SD condition happen.
               - 3'd0: Set SD if BER = 10-3
               - 3'd1: Set SD if BER >= 10-4
               - 3'd2: Set SD if BER >= 10-5
               - 3'd3: Set SD if BER >= 10-6
               - 3'd4: Set SD if BER >= 10-7
               - 3'd5: Set SD if BER >= 10-8
               - 3'd6: Set SD if BER >= 10-9
               - 3'd7: Set SD if BER >= 10-10
-----------*/


/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS/VC Per STS/VC Interrupt OR Status
Reg Addr: 0x00521EF - 0x00541EF
          The address format for these registers is 0x00521EF + SliceID*8192
          Where: SliceID: (0-1) STS-12 slice ID
Reg Desc: The register consists of 12 bits for 12 STS/VC of the related STS-12
          slice in the STS/VC Pointer interpreter block. Each bit is used to
          store Interrupt OR status of the related STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsVCPerStsVCIntrOrStat         0x0521EF

/*
#define cThaUnusedMask                                 cBit31_12
#define cThaUnusedShift                                12
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: OCNRxStsStsIntr[11:0]
BitField Type: R_0
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
               the OCN Rx STS/VC per Alarm Interrupt Status register  of the
               related STS/VC to be set and they are enabled to raise interrupt
               Bit 0 for STS-1/VC-3/TUG-3 #0, respectively.
--------------------------------------*/
#define cThaOcnRxStsStsIntrIdMask(stsId)               (cBit0 << (stsId))

/*------------------------------------------------------------------------------
Reg Name: POH CPE STS Per channel Alarm Interrupt Or Status
Reg Addr: 0x008E27F
Reg Desc: This register stores the interrupt status of 24 STS channels. Each
          register stores the interrupt status of 1 STS channels.  Bit 0 of
          this register is for STS channel 0; bit 1 of this register 0 is for
          STS channel 1; and so on.
------------------------------------------------------------------------------*/

/*--------------------------------------
BitField Name: PohCpePerChnIntOrStatSlc1[11:0]
BitField Type: R_O
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 27_16
--------------------------------------*/

/*--------------------------------------
BitField Name: PohCpePerChnIntOrStatSlc0[11:0]
BitField Type: R_O
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 11_0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: OCN Tx PG Per Channel Control
Reg Addr: 0x00062800-0x0006497F
          The address format for these registers is 0x00062800 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0) TUG-2/VTG ID
          VtnID: (0) VT/TU number ID in the TUG-2/VTG
Reg Desc: Each register is used to configure for STS/VC PG of the channel
          being relative with the address of the STS/VC at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxPGPerChnCtrl              0x00062800


/*--------------------------------------
BitField Name: TxPgSts_Vt_TUBipErins
BitField Type: R/W
BitField Desc: Set 1 to insert errors into TU3 B3 or TU BIP2.
  This bit is ignored when  TxPgSts_Vt_PohInsEn (bit18) bit is clear.
--------------------------------------*/
#define cThaTxPgStsVtBipErrInsMask                        cBit20
#define cThaTxPgStsVtBipErrInsShift                       20

/*--------------------------------------
BitField Name: TxPgSts_Vt_EparEn
BitField Type: R/W
BitField Desc: EPAR Mode
               - 1: Enable.
               - 0: Disable.
--------------------------------------*/
#define cThaTxPgStsVtEparEnMask                        cBit19
#define cThaTxPgStsVtEparEnShift                       19

/*--------------------------------------
BitField Name: TxPgSts_Vt_PohInsEn
BitField Type: R/W
BitField Desc: SS bits or VT/TU type insert enable bit
               - 1: Enable insertion Poh.
               - 0: Disable insertion.
--------------------------------------*/
#define cThaTxPgStsVtPohInsEnMask                        cBit18
#define cThaTxPgStsVtPohInsEnShift                       18

/*--------------------------------------
BitField Name: TxPg2StsSSInsEn
BitField Type: R/W
BitField Desc: SS bits or VT/TU type insert enable bit
               - 1: Enable insertion of TxPg2StsSSInsPat[1:0] into SS bits
                 position in STS/VC pointers.
               - 0: SS bits position in STS/VC pointer is passed through.
--------------------------------------*/
#define cThaTxPg2StsSSInsEnMask                        cBit15
#define cThaTxPg2StsSSInsEnShift                       15

/*--------------------------------------
BitField Name: TxPgSts_Vt_TuSSInsPat[1:0]
BitField Type: R/W
BitField Desc: Pattern to insert to SS bits or VT/TU type positions in
               STS/VC/VT/TU the pointer.
--------------------------------------*/
#define cThaTxPgStsVtTuSSInsPatMask                    cBit17_16
#define cThaTxPgStsVtTuSSInsPatShift                   16

/*--------------------------------------
BitField Name: TxPgSts_Vt_TuSSInsEn
BitField Type: R/W
BitField Desc: SS bits or VT/TU type insert enable bit
               - 1: Enable insertion of TxPg1Sts_Vt_TuSSInsPat[1:0] into SS
                 bits or VT/TU type position in STS/VC/VT/TU pointers.
               - 0: SS bits or VT/TU type position in STS/VC/VT/TU pointer is
                 passed through.
--------------------------------------*/

/*--------------------------------------
BitField Name: TxPgSts_Vt_TuUneqPFrc
BitField Type: R/W
BitField Desc: Path Un-equip force
               - 1: Insert all zeros into all payload bytes with valid
                 pointers.
               - 0: Path Un-equip forcing is disabled.
--------------------------------------*/
#define cThaTxPgStsVtTuUneqPFrcMask                    cBit14
#define cThaTxPgStsVtTuUneqPFrcShift                   14

/*--------------------------------------
BitField Name: TxPgSts_Vt_TuAisPFrc
BitField Type: R/W
BitField Desc: Path AIS force
               - 1: Insert all AIS into all payload bytes and pointer bytes.
               - 0: Path AIS forcing is disabled
--------------------------------------*/
#define cThaTxPgStsVtTuAisPFrcMask                     cBit13
#define cThaTxPgStsVtTuAisPFrcShift                    13

/*--------------------------------------
BitField Name: TxPgStsSlvInd
BitField Type: R/W
BitField Desc: Slave bit
               - 1: Indicate that the STS/VC is the slave STS/VC in the
                 concatenation.
               - 0: Indicate that the STS/VC is not in the concatenation or it
                 is the master STS/VC if it is the concatenation.
--------------------------------------*/
#define cThaTxPgStsSlvIndMask                          cBit8
#define cThaTxPgStsSlvIndShift                         8

/*--------------------------------------
BitField Name: TxPgStsMastID[4:0]
BitField Type: R/W
BitField Desc: (0-11): the master STS/VC ID. This is the ID of the master
               STS/VC/ in the concatenation that contains this STS/VC/. If this
               channel is not in the concatenation, Masterid[4:0] is ignored.
--------------------------------------*/
#define cThaTxPgStsMastIdMask                          cBit4_0
#define cThaTxPgStsMastIdShift                         0



/*------------------------------------------------------------------------------
Reg Name: OCN Tx POH Insertion Per Channel Control
Reg Addr: 0x000620C0-0x0005018B
          The address format for these registers is 0x000620C0 + SliceID*8192 +
          StsID
          Where: SliceID: (1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: Each register is used to configure for STS/VC PG of the channel
          being relative with the address of the STS/VC at Tx direction.
------------------------------------------------------------------------------*/
#define cThaRegOcnTxPohInsPerChnCtrl          0x000620C0


/*--------------------------------------
BitField Name: TxPg2StsB3ErIns
BitField Type: R/W
BitField Desc: et high to insert errors into B3 byte in VC-3/VC-4.
 This bit is ignored when POH insertion is disabled (VC-3/VC-4 CEP).
--------------------------------------*/
#define cThaTxPg2StsB3ErrInsMask                       cBit13
#define cThaTxPg2StsB3ErrInsShift                      13

/*--------------------------------------
BitField Name: TxPg2StsSSInsPat[1:0]
BitField Type: R/W
BitField Desc: Pattern to insert to SS bits  in STS/VC the pointer.
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsSSInsPat in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsSSInsPatMask                       cBit12_11
#define cThaTxPg2StsSSInsPatShift                      11

/*--------------------------------------
BitField Name: TxPg2StsUneqPFrc
BitField Type: R/W
BitField Desc: Path Un-equip force
               - 1: Insert all zeros into all payload bytes with valid
                 pointers.
               - 0: Path Un-equip forcing is disabled.
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsUneqPFrc in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsUneqPFrcMask                       cBit10
#define cThaTxPg2StsUneqPFrcShift                      10

/*--------------------------------------
BitField Name: TxPg2StsAisPFrc
BitField Type: R/W
BitField Desc: Path AIS force
               - 1: Insert all AIS into all payload bytes and pointer bytes.
               - 0: Path AIS forcing is disabled
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsAisPFrc in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsAisPFrcMask                        cBit9
#define cThaTxPg2StsAisPFrcShift                       9

/*--------------------------------------
BitField Name: TxPg2StsSlvInd
BitField Type: R/W
BitField Desc: Slave bit
               - 1: Indicate that the STS/VC is the slave STS/VC in the
                 concatenation.
               - 0: Indicate that the STS/VC is not in the concatenation or it
                 is the master STS/VC if it is the concatenation.
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsSlvInd in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsSlvIndMask                         cBit8
#define cThaTxPg2StsSlvIndShift                        8

/*--------------------------------------
BitField Name: TxPg2StsMastID[4:0]
BitField Type: R/W
BitField Desc: (0-11): the master STS/VC ID. This is the ID of the master
               STS/VC/ in the concatenation that contains this STS/VC/. If this
               channel is not in the concatenation, Masterid[4:0] is ignored.
               Arrive Technologies Inc. Register Description     This
               controlled document is the proprietary of Arrive Technologies
               Inc.. Any duplication, reproduction, or transmission to
               unauthorized parties is prohibited. Copyright Staff Website:
               www.arrivetechnologies.com        AT4848 - Highly-Integrated
               OC-48/STM-16 ADM-on-a-chip Register Descriptions      Copyright
               ? Staff. Arrive Technologies Inc.     Page ii Internal Doc.
               Subject to Change   Thalassa 6200 Register Descriptions
               Copyright ? Staff. Arrive Technologies Inc.     Page i Internal
               Doc. Subject to Change
               Thalassa 6200 Register Descriptions     Copyright ? Staff.
               Arrive Technologies Inc.     Page iv Internal Doc. Subject to
               Change
               Thalassa 6200 Register Descriptions     Copyright ? Staff.
               Arrive Technologies Inc.     Page v Internal Doc. Subject to
               Change
               AT4848 - Highly-Integrated OC-48/STM-16 ADM-on-a-chip Register
               Descriptions      Copyright ? Staff. Arrive Technologies Inc.
               Page 11 Internal Doc. Subject to Change   Thalassa 6200 Register
               Descriptions   Copyright ? Staff. Arrive Technologies Inc.
               Page 6 Internal Doc. Subject to Change
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
TxPg2StsMastID in 1.2.9.OCN Tx POH Insertion Slide 1 Per Channel Control
 have declare in 1.2.8.1. OCN Tx PG Slide 2 Per Channel Control
*/
#define cThaTxPg2StsMastIDMask                         cBit4_0
#define cThaTxPg2StsMastIDShift                        0


/*------------------------------------------------------------------------------
Reg Name: OCN Rx STS PI Per Channel Control
Reg Addr: 0x00052040 - 0x0005404B
          The address format for these registers is 0x00052040 + SliceID*8192 +
          Stsid
          Where: SliceID: (0-1) STS-12 slice ID
          Stsid: (0-11) STS-1/VC-3 ID
Reg Desc: Each register is used to configure for STS pointer interpreter
          engines of the STS-1/VC-3 being relative with the address of the
          address of the register. It is noted that these registers must be
          accessed in indirect mode through OCN Indirect Access Control
          register.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxStsPiPerChnCtrl                   0x00052040

/*-----------
BitField Name: RxStsPiStsTermEn
BitField Type: R/W
BitField Desc: Enable to terminate the related STS/VC. It means that STS POH
               defects related to the STS/VC to generate AIS to downstream:
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaRxStsPiStsTermEnMask                         cBit19
#define cThaRxStsPiStsTermEnShift                        19

/*-----------
BitField Name: RxStsPiAisFrc
BitField Type: R/W
BitField Desc: Per channel AIS force:
               - 1: Force Pointer Interpreter state machine into AIS state.
               - 0: Not force.
------------*/

/*-----------
BitField Name: RxStsPiSsDetPat
BitField Type: R/W
BitField Desc: This is the pattern that is used to compare with the extracted
               SS bits from receive direction.
------------*/
#define cThaRxStsPiSsDetPatMask                       cBit12_11
#define cThaRxStsPiSsDetPatShift                      11

/*-----------
BitField Name: RxStsPiSsDetEn
BitField Type: R/W
BitField Desc: SS detecting enable
               - 1: Enable checking SS bits in the pointer interpreter
                    engine.
               - 0: SS bit checking function in the pointer interpreter
                    engine is disabled.
------------*/
#define cThaRxStsPiSsDetEnMask                        cBit10
#define cThaRxStsPiSsDetEnShift                       10

/*-----------
BitField Name: RxStsPiAdjRule
BitField Type: R/W
BitField Desc: Select the rule for implementing pointer adjustment in the
               pointer interpreter
               - 1: The n of 5 rule is selected. This mode is applied for
                    SDH mode
               - 0: The 8 of 10 rule is selected. This mode is applied for
                    SONET mode
------------*/

/*-----------
BitField Name: RxStsPiSlvInd
BitField Type: R/W
BitField Desc: Slaver bit
               - 1: Indicate that the STS-1/VC-3 is the slaver STS-1/VC-3 in
                    the concatenation.
               - 0: Indicate that the STS-1/VC-3 is not in the concatenation
                    or the master STS-1/VC-3 if it is the concatenation.
------------*/
#define cThaRxStsPiSlvIndMask                         cBit8
#define cThaRxStsPiSlvIndShift                        8

/*-----------
BitField Name: RxStsPiMastId
BitField Type: R/W
BitField Desc: the master STS-1 ID. This is the ID of the master STS-1 in the
               concatenation that contains this STS-1. If this STS-1 is not in
               the concatenation, this field is ignored. It is noted that all
               STS-1/VC-3s in the concatenation group are in one SONET/SDH
               line.
------------*/
#define cThaRxStsPiMastIdMask                         cBit3_0
#define cThaRxStsPiMastIdShift                        0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per STS/VC Interrupt OR Status
Reg Addr: 0x0053CFF - 0x0057CFF
          The address format for these registers is 0x0053CFF + SliceID*8192
          Where: SliceID: (0-1) STS-12 slice ID
Reg Desc: The register consists of 12 bits for 12 STS/VCs of the related
          STS-12 slice in the VT/TU Pointer interpreter block. Each bit is
          used to store Interrupt OR status of the related STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerStsVCIntrOrStat (0x053CFF)
#define cThaRegOcnRxVtTuPerStsStsIdMask(stsId) (cBit0 << (stsId))

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per STS/VC Interrupt Enable Control
Reg Addr: 0x0053CFE - 0x0057CFE
          The address format for these registers is 0x0053CFE` + SliceID*8192
          Where: SliceID: (0-1) STS-12 slice ID
Reg Desc: The register consists of 12 bits for 12 STS/VCs in the related
          STS-12 slice to enable interrupts when alarms in related STS/VC
          happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerStsVCIntrEnCtrl (0x0053CFE)

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per VT/TU Interrupt OR Status
Reg Addr: 0x0053C00 - 0x0057C00
          The address format for these registers is 0x0053C00 + SliceID*8192 +
          Stsid
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: The register consists of 28 bits for 28 VT/TUs of the related STS/VC
          in the VT/TU Pointer interpreter block. Each bit is used to store
          Interrupt OR status of the related VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerVtTUIntrVtIdMask(vtId) (cBit0 << (vtId))

/*------------------------------------------------------------------------------
Reg Name: POH CPE Insert Control
Reg Addr: 0x090000-0x0A0FFF (update late)
          The address format for these registers is 0x52_0000 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 1
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPohCpeInsCtrl                          0x090000

/*-----------
BitField Name: StsOut
BitField Type: R/W
BitField Desc: STS out for terminate will be mapped of this channels.
-----------*/
#define cThaStsOutMask                                cBit31_28
#define cThaStsOutShift                               28

/*-----------
BitField Name: SliceOut
BitField Type: R/W
BitField Desc: Slice out for terminate will be mapped of this channels.
-----------*/
#define cThaSliceOutMask                              cBit27_26
#define cThaSliceOutShift                             26

/*-----------
BitField Name: K3InsSel
BitField Type: R/W
BitField Desc: K3 source insertion select
               - 1: Insert K3 byte from POH insertion processor
               - 0: Disable Insert K3 byte from POH insertion processor (K3
                   insert selection from CPU buffer).
-----------*/

/*-----------
BitField Name: K3DlkEn
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Enable insertion of data link from the Data Link
                   Controller to K3[b7-b8].
               - 0: Disable Data link. The K3_datalink_insert will be
                   inserted to K3[b7-b8].
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
K3DlkEn in 10.3.11.1.In case of STS/TU3 Terminate
 have declare in 10.3.3.1. In case of STS/TU3
*/

/*-----------
BitField Name: K3ApsPInsVal
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: K3SpareInsVal
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: K3DlkInsVal
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: G1InsSel
BitField Type: R/W
BitField Desc: G1 source insertion select
               - 1: insert G1 byte from POH insertion processor (auto
                    insert).
               - 0: Disable insert G1 byte from POH insertion processor (G1
                    insert selected from CPU buffer).
-----------*/

/*-----------
BitField Name: RdiPInsEn
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: enable insertion of RDI-P value from G1RdiPInsVal and
                   G1ERdiPInsVal.
               - 0: HW automatically inserts.
-----------*/

/*-----------
BitField Name: ERdiPEn
BitField Type: R/W
BitField Desc: Enable ERDI-P function.
               - 1: Enable ERDI-P.
               - 0: Disable ERDI-P. G1[b6-b7] inserted from
                   G1ERdiPInsVal[1:0].
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
ERdiPEn in 10.3.11.1.In case of STS/TU3 Terminate
 have declare in 10.3.3.1. In case of STS/TU3
*/

/*-----------
BitField Name: AisLopPMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-P/ERDI-P defect when AIS-P or LOP-P
                    detected.
               - 0: Enable (auto insert mode).
-----------*/
#define cThaAisLopPMskMask                            cBit7
#define cThaAisLopPMskShift                           7

/*-----------
BitField Name: TimPMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-P/ERDI-P defect when Tim-P detected.
               - 0: Enable (auto insert mode).
-----------*/
#define cThaTimPMskMask                               cBit6
#define cThaTimPMskShift                              6

/*-----------
BitField Name: UneqPMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-P/ERDI-P defect when UNEQ-P detected.
               - 0: Enable (auto insert mode).
-----------*/
#define cThaUneqPMskMask                              cBit5
#define cThaUneqPMskShift                             5

/*-----------
BitField Name: PlmPMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-P (ERDI-P) defect when PLM-P
                   detected.
               - 0: Enable (auto insert mode).
-----------*/
#define cThaPlmPMskMask                               cBit4
#define cThaPlmPMskShift                              4

/*-----------
BitField Name: G1RdiPInsVal
BitField Type: R/W
BitField Desc: ) insert value
-----------*/

/*-----------
BitField Name: G1ERdiPInsVal
BitField Type: R/W
BitField Desc: ERDI-P (G1[b6-b7]) insert value
-----------*/

/*-----------
BitField Name: G1SpareInsVal
BitField Type: R/W
BitField Desc: ) of G1 byte insert value.
-----------*/

/*------------------------------------------------------------------------------
Reg Name: In case of VT Terminate
Reg Addr: 0.3.11.2. In case of VT Terminate
Reg Desc:
------------------------------------------------------------------------------*/
/*-----------
BitField Name: VtOut
BitField Type: R/W
BitField Desc: VT ID which this current channel ID will be become after
               through the XC.
-----------*/
#define cThaVtOutMask                                 cBit25_21
#define cThaVtOutShift                                21

/*-----------
BitField Name: V5InsSel
BitField Type: R/W
BitField Desc: V5 source insertion select
                - 1: Insert V5 byte from VTOH insertion processor (auto insert)
                - 0: Disable Insert V5 byte from VTOH insertion processor(V5 insert
                     selected from CPU buffer)
-----------*/

/*-----------
BitField Name: VslInsVal
BitField Type: R/W
BitField Desc: The VT signal label insert value
-----------*/

/*-----------
BitField Name: AisLopVMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-V (ERDI-V) defect when AIS-V or LOP-V
                   detected.
               - 0: Enable (auto insert mode)
-----------*/
#define cThaAisLopVMskMask                            cBit16
#define cThaAisLopVMskShift                           16

/*-----------
BitField Name: TimVMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-V (ERDI-V) defect when TIM-V
                   detected.
               - 0: Enable (auto insert mode)
-----------*/
#define cThaTimVMskMask                               cBit15
#define cThaTimVMskShift                              15

/*-----------
BitField Name: UneqVMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-V (ERDI-V) defect when UNEQ-V
                   detected.
               - 0: Enable (auto insert mode)
-----------*/
#define cThaUneqVMskMask                              cBit14
#define cThaUneqVMskShift                             14

/*-----------
BitField Name: PlmVMsk
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Disable insert RDI-V (ERDI-V) defect when PLM-V
                   detected.
               - 0: Enable (auto insert mode)
-----------*/
#define cThaPlmVMskMask                               cBit13
#define cThaPlmVMskShift                              13

/*-----------
BitField Name: RfiVInsVal
BitField Type: R/W
BitField Desc: RFI-V insert value.
-----------*/

/*-----------
BitField Name: ERdiVEn
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Enable ERDI-V mode
               - 0: Disable, the ERDI-V will be insert by ERdiVInsVal
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
ERdiVEn in 10.3.11.2.In case of VT Terminate
 have declare in 10.3.3.2. In case of VT
*/

/*-----------
BitField Name: RdiVInsEn
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Enable insert RDI-V (ERDI-V) from RdiVInsVal value and
                   ERdiVInsVal.
               - 0: HW auto insert
-----------*/

/*-----------
BitField Name: RdiVInsVal
BitField Type: R/W
BitField Desc: RDI-V insert value
-----------*/

/*-----------
BitField Name: K4InsSel
BitField Type: R/W
BitField Desc: K4 source insertion select
               - 1: Insert K4 byte from VTOH insertion processor. (auto
                   insert)
               - 0: Disable insert K4 byte from VTOH insertion processor.
                   (K4 insert selected from CPU buffer)
-----------*/

/*-----------
BitField Name: K4b1b2InsVal
BitField Type: R/W
BitField Desc: K4[b1-b2] insert value
-----------*/

/*-----------
BitField Name: ApsVInsVal
BitField Type: R/W
BitField Desc: APS-V (K4[b3-b4]) insert value.
-----------*/

/*-----------
BitField Name: ERdiVInsVal
BitField Type: R/W
BitField Desc: ERDI-V insert value.
-----------*/

/*-----------
BitField Name: K4b8InsVal
BitField Type: R/W
BitField Desc: insert value.
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH Path Terminate Insertion Buffer
Reg Addr: 0x098000-0x099BEF
          The address format for these registers is 0x51_0000 + SLICE*2048 +
          STS*128 + VT*4 + BID
          Where: SLICE: 0->1
          STS: 0->11
          VT: 0->31 (0->27:VT; 28,29->STS; 30,31->TU3)
          BID: 2'b00: V5; 2'b01: J2; 2'b10: N2; 2'b11:K4
          In case STS/TU3:
          VT = 28, 30; BID[2:0]: 3'd0: J1; 3'd1: N1; 3'd2: C2; 3'd3:G1;
          VT = 29, 31; BID[2:0]: 3'd4: F2; 3'd5: H4; 3'd6: F3; 3'd7: K3
Reg Desc: Terminate Insert buffer store Overhead bytes for transmit.
------------------------------------------------------------------------------*/
#define cThaRegPohPathTerInsBuf                       0x098000


#define cThaPohV5ByteTerInsBuf                         0x0
#define cThaPohJ2ByteTerInsBuf                         0x1
#define cThaPohN2ByteTerInsBuf                         0x2
#define cThaPohK4ByteTerInsBuf                         0x3

#define cThaPohC2ByteTerInsBuf                         0x2


/*-----------
BitField Name: POHByteInsBuf
BitField Type: R/W
BitField Desc: Overhead bytes buffer using for transmit.
-----------*/
#define cThaByteInsBufMask                            cBit7_0
#define cThaByteInsBufShift                           0


/* POH K3 byte mask
K3 Byte Structure
+------+------+------+------+------+------+------+------+
|  b1  |  b2  |  b3  |  b4  |   b5 |b6    |b7    |b8    |
+------+------+------+------+------+------+------+------+
|            APS            |    Spare    |  DataLink   |
+------+------+-------------+------+------+------+------+ */



/* POH G1 byte mask
G1 Byte structure
+---------+--------------+------------+
|B1B4     |     ERDI     | Spare      |
|         +------+-------+            |
|         |RDI   |E-RDI  |            |
+---------+--------------+------------+
|b1-4     |b5    |b6-7   |b8          |
+---------+------+-------+------------+
*/



/* POH K4 byte
K4/Z7 byte structure
+------+------+------+------+------+------+------+------+
|  b1  |  b2  |  b3  |  b4  |   b5 |b6    |b7    |b8    |
+------+------+------+------+------+------+------+------+
|Ext-SL|LO-VC |   APS-V     |       Reserves     |Reserv|
+------+------+-------------+------+------+------+------+ */




/* POH V5 Byte
V5 Byte structure
+------+------+------+------+------+------+------+------+
|  b1  |  b2  |  b3  |  b4  |b5    |b6    |b7    |b8    |
+------+------+------+------+------+------+------+------+
| BIP-2       |  REI |   RFI|  Signal Label (VSL)|RDI-V |
+-------------+------+------+--------------------+------+ */





/*------------------------------------------------------------------------------
Reg Name: POH CPE Report
Reg Addr: 0x08B000-0x08B7FF
          The address format for these registers is 0x50_B000 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 3
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc: There are two cases to use the POH CPE Report register, one for
          STS/TU3 and one for VT.
------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: In case of STS/TU3
Reg Addr: 0.3.7.1. In case of STS/TU3
Reg Desc:
------------------------------------------------------------------------------*/
/*In case of STS/TU3 in 10.3.7.1.In case of STS/TU3
 have declare in 10.3.3.1. In case of STS/TU3 */


/*-----------
BitField Name: ReiPCntScanExcdIntr
BitField Type: R/W/C
BitField Desc: REI_P counter scan exceed
-----------*/

/*-----------
BitField Name: B3CntScanExcdIntr
BitField Type: R/W/C
BitField Desc: B3 counter scan exceed.
-----------*/

/*-----------
BitField Name: ApsPStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed APS status.
-----------*/

/*-----------
BitField Name: ApsPStbCur
BitField Type: R_O
BitField Desc: The APS stable status current.
-----------*/

/*-----------
BitField Name: UneqPIntr
BitField Type: R/W/C
BitField Desc: The sticky of UNEQ_P.
-----------*/

/*-----------
BitField Name: J1TimPIntr
BitField Type: R/W/C
BitField Desc: The sticky of J1_TIM.
-----------*/

/*-----------
BitField Name: J1TtiStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed J1 TTI status.
-----------*/

/*-----------
BitField Name: RdiPStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed RDI-P status.
-----------*/

/*-----------
BitField Name: RdiPStbCur
BitField Type: R_O
BitField Desc: The RDI-P stable status current.
-----------*/

/*-----------
BitField Name: C2PlmIntr
BitField Type: R/W/C
BitField Desc: The sticky of C2 PSL PLM.
-----------*/

/*-----------
BitField Name: C2PslStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed C2 PSL status.
-----------*/

/*-----------
BitField Name: C2PslStbCur
BitField Type: R_O
BitField Desc: The C2 PSL stable status current.
-----------*/


/*------------------------------------------------------------------------------
Reg Name: In case of VT
Reg Addr: 0.3.7.2. In case of VT
Reg Desc:
------------------------------------------------------------------------------*/

/*-----------
BitField Name: RfiCur
BitField Type: R_O
BitField Desc: The RFI_V status current.
-----------*/

/*-----------
BitField Name: RFIDefectIntr
BitField Type: R/W/C
BitField Desc: The sticky of RFI_V.
-----------*/

/*-----------
BitField Name: ReiVCntScanExcdIntr
BitField Type: R/W/C
BitField Desc: REI_V counter scan exceed.
-----------*/

/*-----------
BitField Name: Bip2CntScanExcdIntr
BitField Type: R/W/C
BitField Desc: BIP2 counter scan exceed.
-----------*/

/*-----------
BitField Name: ApsVStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed APS status.
-----------*/

/*-----------
BitField Name: ApsVStbCur
BitField Type: R_O
BitField Desc: The APS stable current status.
-----------*/

/*-----------
BitField Name: UneqVIntr
BitField Type: R/W/C
BitField Desc: The sticky of UNEQ_V.
-----------*/

/*-----------
BitField Name: J2TimVIntr
BitField Type: R/W/C
BitField Desc: The sticky of J2_TIM.
-----------*/

/*-----------
BitField Name: J2TtiStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed J2 TTI status.
-----------*/

/*-----------
BitField Name: RdiVStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed RDI-V status.
-----------*/

/*-----------
BitField Name: RdiVStbCur
BitField Type: R_O
BitField Desc: The RDI-V stable status current.
-----------*/

/*-----------
BitField Name: PlmVIntr
BitField Type: R/W/C
BitField Desc: The sticky of PLM-V.
-----------*/

/*-----------
BitField Name: VslStbChgIntr
BitField Type: R/W/C
BitField Desc: The report of changed VSL status.
-----------*/

/*-----------
BitField Name: VslStbCur
BitField Type: R_O
BitField Desc: The VSL stable status current.
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE Control
Reg Addr: 0x088800-0x088FFF
          The address format for these registers is 0x50_8800 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 3
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc: This is the Common Processor Engine Control Register. There are two
          cases to use the POH CPE Control register, one for STS/TU3 and one
          for VT.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeCtrl                             0x088800

/*------------------------------------------------------------------------------
Reg Name: In case of STS/TU3
Reg Addr: 0.3.3.1. In case of STS/TU3
Reg Desc:
------------------------------------------------------------------------------*/

/*-----------
BitField Name: B3BlkMd
BitField Type: R/W
BitField Desc: B3 Block mode
               - 1: The B3 errors accumulator counter counts B3 errors with
                   block mode
               - 0: The B3 errors accumulator counter counts B3 errors with
                   bit mode.
-----------*/

/*-----------
BitField Name: PlmVcais2AisEn
BitField Type: R/W
BitField Desc: Enable to forwarding AIS to downstream when PLM or VCAIS defect is detected
-----------*/
#define cThaPlmVcais2AisEnMask                           cBit30
#define cThaPlmVcais2AisEnShift                          30

/*-----------
BitField Name: ReiPBlk
BitField Type: R/W
BitField Desc: REI-P block mode
               - 1: The REI-P errors accumulator counter counts REI-P errors
                   with block mode
               - 0: The REI-P errors accumulator counter counts REI-P errors
                   with bit mode.
-----------*/

/*-----------
BitField Name: TimP2AisEn
BitField Type: R/W
BitField Desc: Enable scans REI-P counter. Active high
-----------*/
#define cThaTimP2AisEnMask                         cBit28
#define cThaTimP2AisEnShift                        28

/*-----------
BitField Name: ERdiPEn
BitField Type: R/W
BitField Desc: Enable ERDI-P mode. Active high.
-----------*/

/*-----------
BitField Name: ERdiPExtraEn
BitField Type: R/W
BitField Desc: ). It will be ignore when ERDI-P inactive. Active high.
-----------*/

/*-----------
BitField Name: RdiPStbSel
BitField Type: R/W
BitField Desc: The RDI-P stable threshold register selected.
               - 1: The RDI-P stable Threshold register number 1 is
                   selected.
               - 0: The RDI-P stable Threshold register number 0 is
                   selected.
-----------*/

/*-----------
BitField Name: K3DlkEn
BitField Type: R/W
BitField Desc: TBD (this mode only support for Path Terminate)
               - 1: Enable to process K3 data link bits (K3[7-8])
               - 0: Disable
-----------*/

/*-----------
BitField Name: PslExptVal
BitField Type: R/W
BitField Desc: The expected value of Path Signal Label
-----------*/
#define cThaPslExptValMask                            cBit23_16
#define cThaPslExptValShift                           16

/*-----------
BitField Name: PslStbSel/ApsPStbSel
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: ApsPExtra
BitField Type: R/W
BitField Desc: The APS extra mode
               - 00: Normal mode, only monitor stable of APS bits (K3[1-4]).
                   In this mode, the ApsPExtraVal[3:0] in the POH CPE Report
                   register is APS-P accepted value.
               - 01: Extra mode, monitor stable of K3 bits (K3[1-4],K3[5-6])
               - 10: Extra mode, monitor stable of K3 bits (K3[1-4],K3[7,8])
               - 11: Extra mode, monitor stable of K3 byte (K3[1-8]).
-----------*/

/*-----------
BitField Name: J1TimMonEnable
BitField Type: R/W
BitField Desc: The POH J1 TIM monitor enable configuration bit
               - 1: Enable to monitor J1 TIM function
               - 0: Disable to monitor J1 TIM function
-----------*/
#define cThaJ1TimMonEnableMask                              cBit12
#define cThaJ1TimMonEnableShift                             12
#define cThaJ1TimMonEnableMaxVal                            0x1
#define cThaJ1TimMonEnableMinVal                            0x0
#define cThaJ1TimMonEnableRstVal                            0x0

/*-----------
BitField Name: J1TimExpMd
BitField Type: R/W
BitField Desc: J1 TIM monitoring with Expected mode.
When this bit is set to 1 the POH Engine will compare the incoming
TTI message with Expected Message.
If this bit is 0 the POH Engine will store the incoming
TTI message to the registers
-----------*/
#define cThaJnTimExpMdMask                               cBit11
#define cThaJnTimExpMdShift                              11

/*-----------
BitField Name: J1FrmMd
BitField Type: R/W
BitField Desc: Indicates J1 Frame Mode
               - 2'b00: byte mode.
               - 2'b01: TTI message with 16 bytes length. The frame format
                        according ITU-T
               - 2'b10: TTI message with 64 bytes length. The frame format
                        has CR and LF at the end of frame
               - 2'b11: TTI message float mode. (J1 byte captured cycle of a
                        buffer 64 bytes).
-----------*/
#define cThaJnFrmMdMask                               cBit10_9
#define cThaJnFrmMdShift                              9

/*-----------
BitField Name: J1MsgCapEnb
BitField Type: R/W
BitField Desc: Enable to capture full TTI message. There is a pool of
32 Buffers which are used for Message Capture Function.
    1: Enable to capture full TTI message. When it is set, the TTI message will be captured and written to J1MsgCapBUFID current message location.
    0: When TTI Message is captured this bit is set to ZERO.
-----------*/
#define cThaJnMsgCapEnbMask                               cBit8
#define cThaJnMsgCapEnbShift                              8

/*-----------
BitField Name: J1MsgCapFrmMd
BitField Type: R/W
BitField Desc: Message Capture Frame Mode.
This configuration field can be difference with J0TIMFrmMd
                - 2b00: TTI message float mode. (J0 byte captured cycle of a buffer 64 bytes).
                - 2b01: TTI message capture with 16 bytes length. The frame format according ITU-T
                - 2b10: TTI message capture with 64 bytes length. The frame format has CR and LF at the end of frame
                - 2b11: TTI message capture float mode. (J1 byte captured cycle of a buffer 64 bytes).
-----------*/
#define cThaJ1MsgCapFrmMdMask                                 cBit7_6
#define cThaJ1MsgCapFrmMdShift                                6

/*-----------
BitField Name: J1BufID
BitField Type: R/W
BitField Desc: When J1MonMd asserted, this register is message buffer ID, that
               is location to store Trace Trail current message and accepted
               (or expected) message. When byte mode is activated, these bits
               are use to store the J1 expected value from bit 5 to bit 0.
-----------*/
#define cThaJnBufIDMask                               cBit5_0
#define cThaJnBufIDShift                              0


/*------------------------------------------------------------------------------
Reg Name: In case of VT
Reg Addr: 0.3.3.2. In case of VT
Reg Desc:
------------------------------------------------------------------------------*/

/*-----------
BitField Name: Bip2Blk
BitField Type: R/W
BitField Desc: Used to indicate the mode which the errors Accumulator counter
               counts with
               - 1: The errors Accumulator counter counts BIP2 error with
                    block mode
               - 0: The errors Accumulator counter counts BIP2 error with
                    bit mode
-----------*/

/*-----------
BitField Name: RfiVStbSel
BitField Type: R/W
BitField Desc: Used to select the POH RFI-V Stable Threshold Control register
               - 1: The RFI-V stable threshold control register
                   RFI-V_stable_1 is selected.
               - 0: The RFI-V stable threshold control register
                   RFI-V_stable_0 is selected.
-----------*/

/*-----------
BitField Name: ERdiVExtra
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
               - 1: Enables the K4 spare bit (b8) concatenated with E-RDI-V
                   to detect defect status
               - 0: Disables K4 spare bit (b8) concatenated with E-RDI-V to
                   detect defect status
-----------*/

/*-----------
BitField Name: RdiVStbSel
BitField Type: R/W
BitField Desc: Used to select the POH RDI-V Stable Threshold Control register
               - 1: The RDI-V stable threshold control register
                    RDI-V_stable_1 is selected.
               - 0: The RDI-V stable threshold control register
                    RDI-V_stable_0 is selected.
-----------*/

/*-----------
BitField Name: ERdiVEn
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: PlmvAisv2AisEn
BitField Type: R/W
BitField Desc: Enable to forwarding AIS to downstream when PLM or VCAIS defect is detected.
-----------*/
#define cThaPlmvAisv2AisEnMask                         cBit24
#define cThaPlmvAisv2AisEnShift                        24

/*-----------
BitField Name: TimV2AisEn
BitField Type: R/W
BitField Desc: Enable scans REI-V counter. Active high
-----------*/
#define cThaTimV2AisEnMask                         cBit23
#define cThaTimV2AisEnShift                        23

/*-----------
BitField Name: ApsVExtra
BitField Type: R/W
BitField Desc: The APS extra mode
               - 1: Extra mode, monitor stable of K4 bits (K4[1-4])
               - 0: Normal mode, only monitor stable of APS bits (K4[3-4])
-----------*/

/*-----------
BitField Name: ApsVStbSel
BitField Type: R/W
BitField Desc: Used to select the POH APS-V Stable Threshold Control register
               - 1: The APS-V stable threshold control register
                   APS-V_stable_1 is selected.
               - 0: The APS-V stable threshold control register
                   APS-V_stable_0 is selected.
-----------*/



/*-----------
BitField Name: VslExptVal
BitField Type: R/W
BitField Desc: The expected value of VT Signal Label.
-----------*/
#define cThaVslExptValMask                            cBit18_16
#define cThaVslExptValShift                           16

/*-----------
BitField Name: VslStbSel
BitField Type: R/W
BitField Desc: Used to select the POH VSL-V Stable Threshold Control register
               - 1: The VSL stable threshold control register VSL_stable_1
                   is selected.
               - 0: The VSL stable threshold control register VSL_stable_0
                   is selected.
-----------*/


/*-----------
BitField Name: J2TimMonEnable
BitField Type: R/W
BitField Desc: The POH J2 TIM monitor enable configuration bit
               - 1: Enable to monitor J2 TIM function
               - 0: Disable to monitor J2 TIM function
-----------*/
#define cThaJ2TimMonEnableMask                              cBit12
#define cThaJ2TimMonEnableShift                             12
#define cThaJ2TimMonEnableMaxVal                            0x1
#define cThaJ2TimMonEnableMinVal                            0x0
#define cThaJ2TimMonEnableRstVal                            0x0

/*-----------
BitField Name: J2TimExpMd
BitField Type: R/W
BitField Desc: J2 TIM monitoring with Expected mode.
When this bit is set to 1 the POH Engine will compare the incoming
TTI message with Expected Message.
If this bit is 0 the POH Engine will store the incoming
TTI message to the registers
-----------*/

/*-----------
BitField Name: J2FrmMd
BitField Type: R/W
BitField Desc: Indicates J2 Frame Mode
               - 2'b00: byte mode.
               - 2'b01: TTI message with 16 bytes length. The frame format
                   according ITU-T
               - 2'b10: TTI message with 64 bytes length. The frame format
                   has CR and LF at the end of frame
               - 2'b11: TTI message float mode. (J1 byte captured cycle of a
                   buffer 64 bytes).
-----------*/

/*-----------
BitField Name: J2MsgCapEnb
BitField Type: R/W
BitField Desc: Enable to capture full TTI message. There is a pool of
32 Buffers which are used for Message Capture Function.
    1: Enable to capture full TTI message. When it is set, the TTI message will be captured and written to J1MsgCapBUFID current message location.
    0: When TTI Message is captured this bit is set to ZERO.
-----------*/

/*-----------
BitField Name: J2MsgCapFrmMd
BitField Type: R/W
BitField Desc: Message Capture Frame Mode.
This configuration field can be difference with J0TIMFrmMd
                - 2b00: TTI message float mode. (J0 byte captured cycle of a buffer 64 bytes).
                - 2b01: TTI message capture with 16 bytes length. The frame format according ITU-T
                - 2b10: TTI message capture with 64 bytes length. The frame format has CR and LF at the end of frame
                - 2b11: TTI message capture float mode. (J1 byte captured cycle of a buffer 64 bytes).
-----------*/

/*-----------
BitField Name: J2BufID
BitField Type: R/W
BitField Desc: When J2MonMd asserted, this register is message buffer ID, that
               is location to store Trace Trail current message and accepted
               (or expected) message. When byte mode is activated, these bits
               are use to store the J2 expected value from bit 5 to bit 0.
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH Message Insertion Buffer for STS/TU
Reg Addr: 0x0A0000-0x0ABFFF
          The address format for these registers is 0x0A0000 + SLICE*16 +
          STS*512 + 448 + lwLoc
          Where: Where: Slice: 0 -> 1
          STS: 0 -> 11
          lwLoc : 0 -> 15
Reg Desc: CPE processes (1344 VT + 48 STS) channels, each channel has a
          message buffer with 64 bytes length. When this channel selected to
          process as Path Terminate, this buffer store (J1 or J2) a TTI
          message.
------------------------------------------------------------------------------*/
#define cThaRegPohMsgInsBuf                            0x0A0000

/*-----------
BitField Name: MsgByte0MsgByte1MsgByte2MsgByte3
BitField Type: R/W
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/
#define cThaTraceMsgInsByteMask(byteId)                ((uint32)cBit31_24 >> (uint32)((byteId) * 8))
#define cThaTraceMsgInsByteShift(byteId)               (8 * (3 - ((byteId) % 4)))

/*------------------------------------------------------------------------------
Reg Name: POH TTI Expected/Accepted Message Buffer
Reg Addr: 0x08D400-0x08D5FF
          The address format for these registers is 0x50_D400 + BufID*16 +
          WCNT
          Where: BufID: 0 -> 31
          WCNT: 0 -> 15
Reg Desc: TTI Expected Message Buffer use to store expected or accepted TTI
          Message. The MsgByte0 is MSB byte of message.
------------------------------------------------------------------------------*/




/*------------------------------------------------------------------------------
Reg Name: POH Path Terminate Jn Insertion Control
Reg Addr: 0x093800-0x0A082F
          The address format for these registers is 0x520800 + + STS*4 + SLICE
          Where
          SLICE[1:0]: 0 -> 1
          STS[3:0]: 0-> 11
Reg Desc: Path Terminate Insert configuration register ([27:0] Vtmode, [28]:
          STS, [29] Tu3)
------------------------------------------------------------------------------*/

/*-----------
BitField Name: J1Tu3InsMd
BitField Type: R/W
BitField Desc: The J1 Insert Mode.
               - 1'b0: Jn insert from CPU
               - 1'b1: Jn insert from buffer
-----------*/

/*-----------
BitField Name: J1StsInsMd
BitField Type: R/W
BitField Desc: The J1 Insert Mode.
               - 1'b0: Jn insert from CPU
               - 1'b1: Jn insert from buffer
-----------*/

/*-----------
BitField Name: J2InsMd
BitField Type: R/W
BitField Desc: The J2 Insert Mode.
               - 1'b0: Jn insert from CPU
               - 1'b1: Jn insert from buffer
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE Status 1
Reg Addr: 0x089000-0x5097FF
          The address format for these registers is 0x50_9000 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 3
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc: This is CPE Status 1 register. There are two cases to use the POH
          CPE Status 1 register, one for STS/TU3 and one for VT.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeStat1                           0x089000


/*------------------------------------------------------------------------------
Reg Name: In case of STS/TU3
Reg Addr:
Reg Desc:
------------------------------------------------------------------------------*/

/*-----------
BitField Name: J1TimPCur
BitField Type: R_O
BitField Desc: ERRORS_LACK_OF_DESCRIPTION
-----------*/

/*-----------
BitField Name: J1StbCur
BitField Type: R_O
BitField Desc: The Current status of TTI Message
-----------*/

/*-----------
BitField Name: J1Oom
BitField Type: R_O
BitField Desc: The current value of J2 out of frame.
               - 1'b1: J2 out of frame
               - 1'b0: J2 in frame
-----------*/


/*-----------
BitField Name: J1CurVal
BitField Type: R_O
BitField Desc: The current value of J1 byte. This register is only used in
               byte mode (J1FrmMd equal to 0x0)
-----------*/

/*-----------
BitField Name: J1AcptVal
BitField Type: R_O
BitField Desc: The Accepted value of J1 byte. This register is only used in
               byte mode (J1FrmMd equal to 0x0)
-----------*/

/*-----------
BitField Name: J1Tim
BitField Type: R_O
BitField Desc: Section Trace Identifier Mismatch
-----------*/

/*-----------
BitField Name: J1Stb
BitField Type: R_O
BitField Desc: J1 Message Stable
-----------*/

/*-----------
BitField Name: J1State
BitField Type: R_O
BitField Desc: J1 Message State
    2'b00: Out of frame
    2'b01: Going Frame
    2'b10: In Frame
    2'b11: Go Out Frame
-----------*/
#define cThaJnStateMask                          cBit9_8
#define cThaJnStateShift                         8

/*-----------
BitField Name: J1FrmCnt
BitField Type: R_O
BitField Desc: J1 Message Frame Count
-----------*/

/*-----------
BitField Name: J1ByteCnt
BitField Type: R_O
BitField Desc: J1 Message Byte Count
-----------*/

/*------------------------------------------------------------------------------
Reg Name: In case of VT
Reg Addr: 0.3.4.2. In case of VT
Reg Desc:
------------------------------------------------------------------------------*/

/*-----------
BitField Name: J2TimVCur
BitField Type: R_O
BitField Desc: The Current status of TIM-V
-----------*/

/*-----------
BitField Name: J2StbCur
BitField Type: R_O
BitField Desc: The Current status of TTI Message
-----------*/

/*-----------
BitField Name: J2Oom
BitField Type: R_O
BitField Desc: The current value of J2 out of frame.
               - 1'b1: J2 out of frame
               - 1'b0: J2 in frame
-----------*/

/*
#define cThaHwStatMask                                cBit28_16
#define cThaHwStatShift                               16
#define cThaHwStatRstVal                              0x0
*/

/*-----------
BitField Name: J2AcptVal
BitField Type: R_O
BitField Desc: The Accepted value of J2 byte. This register is only used in
               byte mode (J2FrmMd equal to 0x0)
-----------*/

/*-----------
BitField Name: J2Tim
BitField Type: R_O
BitField Desc: Section Trace Identifier Mismatch
-----------*/

/*-----------
BitField Name: J2Stb
BitField Type: R_O
BitField Desc: J2 Message Stable
-----------*/

/*-----------
BitField Name: J2State
BitField Type: R_O
BitField Desc: J2 Message State
    2'b00: Out of frame
    2'b01: Going Frame
    2'b10: In Frame
    2'b11: Go Out Frame
-----------*/

/*-----------
BitField Name: J2FrmCnt
BitField Type: R_O
BitField Desc: J2 Message Frame Count
-----------*/

/*-----------
BitField Name: J2ByteCnt
BitField Type: R_O
BitField Desc: J2 Message Byte Count
-----------*/

/*------------------------------------------------------------------------------
Reg Name: POH TTI Current Message Buffer
Reg Addr: 0x08D000-0x08D1FF
          The address format for these registers is 0x50_D000 + BufID*16 +
          WCNT
          Where: BufID: 0 -> 31
          WCNT: 0 -> 15
Reg Desc: TTI Current Message buffer use to store current TTI message. The
          MsgByte0 is MSB byte of message.
------------------------------------------------------------------------------*/



/*------------------------------------------------------------------------------
Reg Name: POH CPE VT Alarm Status
Reg Addr: 0x008F800-0x008FBFF
          The address format for these registers is 0x008F800 + SLICE*512 +
          STSID*32 + VTID
          Where,
          SLICE is a Slice ID. It is from 0 to 1.
          STSID is a STS ID. It is from 0 to 11.
          VTID is a VT ID. It is from 0 to 27.
Reg Desc: This is the POH CPE VT alarm current status register. It will be
          used to indicate current status of the event.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeVtAlmStat                      0x08F800

/*
#define cThaUnusedMask                                 cBit31_0
#define cThaUnusedShift                                0
#define cThaUnusedRstVal                               0x0
*/
/*
-ERRORS_StopBitsStr : 12:00 */

/*--------------------------------------
BitField Name: POHVtTtiOom
BitField Type: R/W
BitField Desc: It is the TTI-V Out of Multiframe current status bit. It will be
               set to one when there is a TTI-V OOM state is detected by the
               POH CPE engine and it clears to zero when the POH CPE engine
               does not detect TTI-V OOM state.
BitField Bits: 11
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtTtiTim
BitField Type: R/W
BitField Desc: It is the TTI-V TIM current status bit. It will be set to one
               when there is a TTI-V TIM state is detected by the POH CPE
               engine and it clears to zero when the POH CPE does not detect
               TTI-V TIM state.
BitField Bits: 10
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtBip2CntExc
BitField Type: R/W
BitField Desc: It is the BIP2 Counter Exceed current status bit. It will be set
               to one when there is a BIP2 Counter Exceed state is detected by
               POH CPE engine and it clears to zero when POH CPE engine does
               not detect BIP2 Counter Exceed state.
BitField Bits: 9
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtReiCntExc
BitField Type: R/W
BitField Desc: It is the REI-V Counter Exceed current status bit. It will be
               set to one when there is a REI-V Counter Exceed state is
               detected by POH CPE engine and it clears to zero when POH CPE
               engine does not detect REI-V Counter Exceed state.
BitField Bits: 8
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtPlm
BitField Type: R/W
BitField Desc: It is the PLM-V current status bit. t will be set to one when
               there is a PLM-V state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect PLM-V state.
BitField Bits: 6
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtVcAis
BitField Type: R/W
BitField Desc: It is the VT VC-AIS  current status bit. t will be set to one
               when there is a VC-AIS-V state is detected by POH CPE engine and
               it clears to zero when POH CPE engine does not detect VC-AIS-V
               state.
BitField Bits: 5
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtUneq
BitField Type: R/W
BitField Desc: It is the Uneq-V current status bit.t will be set to one when
               there is a UNEQ-V state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect UNEQ-V state.
BitField Bits: 4
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtRfiDef
BitField Type: R/W
BitField Desc: It is the RFI Defected current status bit. It will be set to one
               when there is a RFI-V state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect RFI-V state.
BitField Bits: 7
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtTtiMsgStb
BitField Type: R/W
BitField Desc: It is the TTI-V Message Stable current status bit. It will be
               set to one when TTI-V message stable state is detected by POH
               CPE engine and it clears to zero when POH CPE engine does not
               detect TTI-V message stable state.
BitField Bits: 3
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtApsStb
BitField Type: R/W
BitField Desc: It is the APS-V Stable current status bit. It will be set to one
               when APS-V stable state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect APS-V stable
               state.
BitField Bits: 2
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtRdiStb
BitField Type: R/W
BitField Desc: It is the RDI-V Stable current status bit.It will be set to one
               when RDI-V stable state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect RDI-V stable
               state.
BitField Bits: 1
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtVslStb
BitField Type: R/W
BitField Desc: It is the VSL  Stable current status bit.It will be set to one
               when VSL-V stable state is detected by POH CPE engine and it
               clears to zero when POH CPE engine does not detect VSL stable
               state.
BitField Bits: 0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE VT Alarm Interrupt Sticky
Reg Addr: 0x008F400-0x008F7FF
          The address format for these registers is 0x008F400 + SLICE*512 +
          STSID*32 + VTID
          Where,
          SLICE is a Slice ID. It is from 0 to 1.
          STSID is a STS ID. It is from 0 to 11.
          VTID is a VT ID. It is from 0 to 27.
Reg Desc: This is the POH CPE VT alarm interrupt sticky register. It will be
          used to indicate and sticky event when the POH CPE VT engine
          detected a changing of  event state.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeVtAlmIntrStk                   0x08F400

/*
#define cThaUnusedMask                                 cBit31_12
#define cThaUnusedShift                                12
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: POHVtTtiOomInt
BitField Type: R/W/C
BitField Desc: It is the TTI-V Out of Multiframe Interrupt stiky bit. It will
               be set to one when there is a TTI-V OOM state detected by the
               POH CPE engine.
BitField Bits: 11
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtTtiTimInt
BitField Type: R/W/C
BitField Desc: It is the TTI-V TIM interrupt sticky bit. It will be set to one
               when there is a TTI-V TIM state detected by the POH CPE engine.
BitField Bits: 10
--------------------------------------*/
#define cThaPohVtTtiTimIntMask                         cBit10

/*--------------------------------------
BitField Name: POHVtBip2CntExcInt
BitField Type: R/W/C
BitField Desc: It is the BIP2 Counter Exceed interrupt sticky bit. It will be
               set to one when there is a BIP2  Counter Exceed state detected
               by the POH CPE engine.
BitField Bits: 9
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtReiCntExcInt
BitField Type: R/W/C
BitField Desc: It is the REI-V Counter Exceed interrupt sticky bit. It will be
               set to one when there is a REI_V Counter Exceed state detected
               by the POH CPE engine
BitField Bits: 8
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtPlmInt
BitField Type: R/W/C
BitField Desc: It is the PLM-V interrupt sticky bit. It will be set to one when
               there is a PLM_V detected by the POH CPE engine.
BitField Bits: 6
--------------------------------------*/
#define cThaPohVtPlmIntMask                            cBit7

/*--------------------------------------
BitField Name: POHVtVcAisInt
BitField Type: R/W/C
BitField Desc: It is the VC-AIS-V  interrupt sticky bit. It will be set to one
               when there is a VC_AIS-V de tected by the POH CPE engine.
BitField Bits: 5
--------------------------------------*/
#define cThaPohVtVcAisIntMask                          cBit6

/*--------------------------------------
BitField Name: POHVtUneqInt
BitField Type: R/W/C
BitField Desc: It is the Uneq-V interrupt sticky bit. It will be set to one
               when there is a UNEQ_V detected by POH CPE engine.
BitField Bits: 4
--------------------------------------*/
#define cThaPohVtUneqIntMask                           cBit5

/*--------------------------------------
BitField Name: POHVtRfiDefInt
BitField Type: R/W/C
BitField Desc: It is the RFI Defected interrupt sticky bit. It will be set to
               one when there is a RFI_V detected by the POH CPE engine.
BitField Bits: 7
--------------------------------------*/
#define cThaPohVtRfiDefIntMask                         cBit4

/*--------------------------------------
BitField Name: POHVtTtiMsgStbChgInt
BitField Type: R/W/C
BitField Desc: It is the TTI-V Message Stable Change interrupt sticky bit. It
               will be set to one when there is a change of TTI-V message state
               at the POH CPE engine.
BitField Bits: 3
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtApsStbChgInt
BitField Type: R/W/C
BitField Desc: It is the APS-V Stable Change interrupt sticky bit. It will be
               set to one when there is a change of APS-V state at the POH CPE
               engine.
BitField Bits: 2
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtRdiStbChgInt
BitField Type: R/W/C
BitField Desc: It is the RDI-V Stable Change interrupt sticky bit. It will be
               set to one when there is a change of RDI_V state at the POH CPE
               engine.
BitField Bits: 1
--------------------------------------*/
#define cThaPohVtRdiStbChgIntMask                      cBit1

/*--------------------------------------
BitField Name: POHVtVslStbChgInt
BitField Type: R/W/C
BitField Desc: It is the VSL  Stable Change interrupt sticky bit. It will be
               set to one when there is a change of VSL state at the POH CPE
               engine.
BitField Bits: 0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE VT Alarm Interrupt Enable Control
Reg Addr: 0x008F000-0x008F3FF
          The address format for these registers is 0x008F000 + SLICE*512 +
          STSID*32 + VTID
          Where,
          SLICE is a Slice ID. It is from 0 to 1.
          STSID is a STS ID. It is from 0 to 11.
          VTID is a VT ID. It is from 0 to 27.
Reg Desc: This is the per channel POH CPE VT alarm interrupt enable control
          register. It is using to enable each event an alarm when it is
          detected.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeVtAlmIntrEnCtrl                0x08F000

/*
#define cThaUnusedMask                                 cBit31_12
#define cThaUnusedShift                                12
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: POHVtTtiOomIntEnb
BitField Type: R/W
BitField Desc: It is the TTI-V message Out Of Multiframe interrupt enable
               control. It is active high. When it is high, the POH CPE will
               send an alarm signal to SW when the CPE engine detected a TTI-V
               OOM state.
BitField Bits: 11
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtTtiTimIntEnb
BitField Type: R/W
BitField Desc: It is the TTI_V TIM interrupt enable control. It is active high.
               When it is high, the POH CPE will send an alarm interrupt signal
               to SW when the CPE engine detected a TTI-V TIM event.
BitField Bits: 10
--------------------------------------*/
#define cThaPohVtTtiTimIntEnbMask                      cBit10
#define cThaPohVtTtiTimIntEnbShift                     10

/*--------------------------------------
BitField Name: POHVtBip2CntExcIntEnb
BitField Type: R/W
BitField Desc: It is the BIP2 error counter exceed a treshold interrupt enable
               control. It is active high. When it is high, the POH CPE will
               send an alarm interrupt signal to SW when the CPE engine
               detected the BIP2errcnt exceeded a treshold event.
BitField Bits: 9
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtReiCntExcIntEnb
BitField Type: R/W
BitField Desc: It is the REI-V counter exceed a treshold interrupt enable
               control. It is active high. When it is high, the POH CPE will
               send an alarm interrupt signal to SW when the CPE engine
               detected the REI-V counter exceeded a treshold event.
BitField Bits: 8
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtRfiDefIntEnb
BitField Type: R/W
BitField Desc: It is the VT RFI-V defected interrupt enable control. It is
               active high. When it is high, the POH CPE will send an alarm
               interrupt signal to SW when the CPE engine detected the RFI
               defected event.
BitField Bits: 7
--------------------------------------*/
#define cThaPohVtRfiDefIntEnbMask                      cBit7
#define cThaPohVtRfiDefIntEnbShift                     7

/*--------------------------------------
BitField Name: POHVtPlmIntEnb
BitField Type: R/W
BitField Desc: It is the PLM-V defected interrupt enable control. It is active
               high. When it is high, the POH CPE will send an alarm interrupt
               signal to SW when the CPE engine detected PLM-V defected event.
BitField Bits: 6
--------------------------------------*/
#define cThaPohVtPlmIntEnbMask                         cBit6
#define cThaPohVtPlmIntEnbShift                        6

/*--------------------------------------
BitField Name: POHVtVcAisIntEnb
BitField Type: R/W
BitField Desc: It is the VC AIS defected interrupt enable control. It is active
               high. When it is high, the POH CPE will send an alarm interrupt
               signal to SW when the CPE engine detected VC AIS defected event.
BitField Bits: 5
--------------------------------------*/
#define cThaPohVtVcAisIntEnbMask                       cBit5
#define cThaPohVtVcAisIntEnbShift                      5

/*--------------------------------------
BitField Name: POHVtUneqIntEnb
BitField Type: R/W
BitField Desc: It is the Uneq-V defected interrupt enable control. It is active
               high. When it is high, the POH CPE will send an alarm interrupt
               signal to SW when the CPE engine detected Uneq-V defected event.
BitField Bits: 4
--------------------------------------*/
#define cThaPohVtUneqIntEnbMask                        cBit4
#define cThaPohVtUneqIntEnbShift                       4

/*--------------------------------------
BitField Name: POHVtTtiMsgStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the TTI-V message Stable Changed interrupt enable control.
               It is active high. When it is high, the POH CPE will send an
               alarm interrupt signal to SW when the CPE engine detected a
               stable change of TTI-V message.
BitField Bits: 3
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtApsStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the APS-V Stable Changed interrupt enable control. It is
               active high. When it is high, the POH CPE will send an alarm
               interrupt signal to SW when the CPE engine detected a stable
               change of APS.
BitField Bits: 2
--------------------------------------*/

/*--------------------------------------
BitField Name: POHVtRdiStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the RDI-V Stable Changed interrupt enable control. It is
               active high. When it is high, the POH CPE will send an alarm
               interrupt signal to SW when the CPE engine detected a stable
               change of RDI-V.
BitField Bits: 1
--------------------------------------*/
#define cThaPohVtRdiStbChgIntEnbMask                   cBit1
#define cThaPohVtRdiStbChgIntEnbShift                  1

/*--------------------------------------
BitField Name: POHVtVslStbChgIntEnb
BitField Type: R/W
BitField Desc: It is the VT Signal Lable Stable Changed interrupt enable
               control. It is active high. When it is high, the POH CPE will
               send an alarm interrupt signal to SW when the CPE engine
               detected a stable change of VSL.
BitField Bits: 0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE VT Per channel Alarm Interrupt Or Status
Reg Addr: 0x008FC00-0x008FC1F
          The address format for these registers is 0x008FC00 + SLICE*16 +
          STSID
          Where,
          SLICE is a slice ID. It is from 0 to 1.
          STSID is a STS ID. It is from 0 to 11.
Reg Desc: This register stores the interrupt status of 24*27 VT channels. Each
          register stores the interrupt status of 28 VT channels. The first
          register at 0x00201600 is for first 28 channels numbered from 0 to
          27; the second register at 0x00201601 is for next 28 channels
          numbered from 32 to 59; and so on. Bit 0 of register 0 is for VT
          channel 0; bit 1 of register 0 is for VT channel 1; and so on.
------------------------------------------------------------------------------*/

/*--------------------------------------
BitField Name: PohCpePerChnIntOrStat[27:0]
BitField Type: R_O
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 27_0
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE Counter Control
Reg Addr: 0x08005E
Reg Desc: The Counter mode controls CPE Counters.
------------------------------------------------------------------------------*/
#define cThaRegPohCpeCntCtrl                          0x08005E

/*-----------
BitField Name: R2CCntEnb
BitField Type: R/W
BitField Desc: Enable to access counter with Read to Clear mode. When this bit is set.
               - 1'b1: all counters B3BIP8, REI-P, V5BIP2, REI-V are clear to zero
                       when SW read to these counters.
               - 1'b0: counter will not clear.
-----------*/
#define cThaR2CCntEnbMask                               cBit5
#define cThaR2CCntEnbShift                              5

/*-----------
BitField Name: B2SatMd
BitField Type: R/W
BitField Desc: The Counter counts accumulate with saturation mode.
               - 1'b1: The Counter counts accumulate with saturation mode.
                   The accumulation counter saturates at maximum count and
                   does not therefore roll over to 0.
               - 1'b0: The Counter counts accumulate with roll over mode.
                   The accumulation counter counts at maximum count and roll
                   over to 0.
-----------*/

/*-----------
BitField Name: B3Bip8SatMd
BitField Type: R/W
BitField Desc: The saturated mode use to configure for B3-BIP8 accumulation
               counter.
-----------*/

/*-----------
BitField Name: ReiPSatMd
BitField Type: R/W
BitField Desc: The saturated mode use to configure for REI-P accumulation
               counter.
-----------*/

/*-----------
BitField Name: V5Bip2SatMd
BitField Type: R/W
BitField Desc: The saturated mode use to configure for V5-BIP2 accumulation
               counter.
-----------*/

/*-----------
BitField Name: ReiVSatMd
BitField Type: R/W
BitField Desc: The saturated mode use to configure for REI-V accumulation
               counter.
-----------*/


/*------------------------------------------------------------------------------
Reg Name: POH CPE Counter 1 BIP Errors
Reg Addr: 0x08C000-0x08C7FF
          The address format for these registers is 0x50_C000 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 3
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPohCpeCnt1BipErr                0x08C000

/*------------------------------------------------------------------------------
Reg Name: In case of STS/TU3
Reg Addr: 0.3.9.1. In case of STS/TU3
Reg Desc:
------------------------------------------------------------------------------*/
/*In case of STS/TU3 in 10.3.9.1.In case of STS/TU3
 have declare in 10.3.3.1. In case of STS/TU3 */

/*-----------
BitField Name: B3ErrCnt
BitField Type: R2C
BitField Desc: The B3 BIP-8 Errors accumulation counter.
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
B3ErrCnt in 10.3.9.1.In case of STS/TU3
 have declare in 10.3.8.1. In case of STS/TU3
*/
#define cThaB3ErrCntMask                           cBit23_0

/*------------------------------------------------------------------------------
Reg Name: In case of VT
Reg Addr: 0.3.9.2. In case of VT
Reg Desc:
------------------------------------------------------------------------------*/
/*In case of VT in 10.3.9.2.In case of VT
 have declare in 10.3.3.2. In case of V3 */

/*-----------
BitField Name: Bip2ErrCnt
BitField Type: R_O
BitField Desc: The BIP-2 Errors accumulation counter.
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
Bip2ErrCnt in 10.3.9.2.In case of VT
 have declare in 10.3.8.2. In case of VT
*/
#define cThaBip2ErrCntMask                         cBit23_0


/*------------------------------------------------------------------------------
Reg Name: POH CPE Counter 1 Read To Clear REI
Reg Addr: 0x08C800-0x08CFFF
          The address format for these registers is 0x08C800 + STS*128 +
          Slice*32 + VT
          Where: Slice: 0 -> 3
          STS: 0 -> 11
          VT: 0 -> 29 (0-27:VT; 28: STS; 29 Tu3)
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPohCpeCnt1Rei                   0x08C800

/*------------------------------------------------------------------------------
Reg Name: In Case of STS/TU3
Reg Addr: 0.3.10.1. In Case of STS/TU3
Reg Desc:
------------------------------------------------------------------------------*/
/*-----------
BitField Name: ReiPErrCnt
BitField Type: R2C
BitField Desc: The REI-P accumulation counter.
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
ReiPErrCnt in 10.3.10.1.In Case of STS/TU3
 have declare in 10.3.8.1. In case of STS/TU3
*/
#define cThaReiPErrCntMask                            cBit23_0

/*------------------------------------------------------------------------------
Reg Name: In Case of VT
Reg Addr: 0.3.10.2. In Case of VT
Reg Desc:
------------------------------------------------------------------------------*/
/*-----------
BitField Name: ReiVErrCnt
BitField Type: R2C
BitField Desc: The REI-V accumulation counter
-----------*/
/* ERRORS_BIT_FIELD_RE_DEFINE
ReiVErrCnt in 10.3.10.2.In Case of VT
 have declare in 10.3.8.2. In case of VT
*/
#define cThaReiVErrCntMask                            cBit23_0


/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU per Alarm Current Status
Reg Addr: 0x00053800 - 0x0005387B
          The address format for these registers is 0x00053800 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0-11) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: This is the per Alarm current status of VT/TU pointer interpreter.
          Each register is used to store 6 bits to store current status of 6
          alarms in the VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUperAlmCurrentStat           0x053800


#define cThaVtPiCepUneqCurStatMask                     cBit2

/*--------------------------------------
BitField Name: VtPiAisCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiAisCurStatMask                         cBit1

/*--------------------------------------
BitField Name: VtPiLopCurStatus
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiLopCurStatMask                         cBit0

/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU per Alarm Interrupt Status
Reg Addr: 0x00053400 - 0x0005347B
          The address format for these registers is 0x00053400 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0-11) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: This is the per Alarm interrupt status of VT/TU pointer interpreter
          . Each register is used to store 6 sticky bits for 6 alarms in the
          VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUperAlmIntrStat              0x053400

/*
#define cThaUnusedMask                                 cBit31_5
#define cThaUnusedShift                                5
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: VtPiStsConcDetIntr
BitField Type: R/W/C
BitField Desc: Set to 1 while an Concatenation Detection event is detected at
               VT/TU pointer interpreter.
--------------------------------------*/

/*--------------------------------------
BitField Name: VtPiStsNewPtrDetIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: VtPiStsNdfIntr
BitField Type: R/W/C
BitField Desc: Set to 1 while an NDF event is detected at VT/TU pointer
               interpreter.
--------------------------------------*/

#define cThaVtPiCepUneqVStateChgIntrMask                    cBit2

/*--------------------------------------
BitField Name: VtPiAisStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiAisStateChgIntrMask                    cBit1

/*--------------------------------------
BitField Name: VtPiLopStateChgIntr
BitField Type: R/W/C
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiLopStateChgIntrMask                    cBit0


/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU Per VT/TU Interrupt OR Status
Reg Addr: 0x0053C00 - 0x0057C00
          The address format for these registers is 0x0053C00 + SliceID*8192 +
          Stsid
          Where: SliceID: (0-1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
Reg Desc: The register consists of 28 bits for 28 VT/TUs of the related STS/VC
          in the VT/TU Pointer interpreter block. Each bit is used to store
          Interrupt OR status of the related VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUPerVtTUIntrOrStat           0x053C00


/*--------------------------------------
BitField Name: OCNRxVtVtIntr[27:0]
BitField Type: R_0
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
               the OCN Rx VT/TU per Alarm Interrupt Status register  of the
               related VT/TU to be set and they are enabled to raise interrupt
               Bit 0 for VT/TU #0, respectively.
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: OCN Rx VT/TU per Alarm Interrupt Enable Control
Reg Addr: 0x00053000-0x0005307B
          The address format for these registers is 0x00053000 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0-11) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: This is the per Alarm interrupt enable of VT/TU pointer interpreter.
          Each register is used to store 6 bits to enable interrupts when the
          related alarms in related VT/TU happen.
------------------------------------------------------------------------------*/
#define cThaRegOcnRxVtTUperAlmIntrEnCtrl            0x053000


/*--------------------------------------
BitField Name: VtPiStsConcDetIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable Concatenation Detection event in the related
               VT/TU to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: VtPiStsNewPtrDetIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: VtPiStsNdfIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable NDF event in the related VT/TU to generate an
               interrupt.
--------------------------------------*/


/*--------------------------------------
BitField Name: VtPiAisStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiAisStateChgIntrEnMask                  cBit1
#define cThaVtPiAisStateChgIntrEnShift                 1

/*--------------------------------------
BitField Name: VtPiLopStateChgIntrEn
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVtPiLopStateChgIntrEnMask                  cBit0
#define cThaVtPiLopStateChgIntrEnShift                 0


/*------------------------------------------------------------------------------
Reg Name: OCN Tx PG Per Slide 1 per Channel Control
Reg Addr: 0x00050800-0x0005097F
          The address format for these registers is 0x00050800 + SliceID*2048
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (1) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: Each register is used to configure for STS/VC/VT/TU PG of the
          channel being relative with the address of the STS/VC/VT/TU at Tx
          direction.
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: TxPg1Sts_Vt_TuSSInsPat[1:0]
BitField Type: R/W
BitField Desc: Pattern to insert to SS bits or VT/TU type positions in
               STS/VC/VT/TU the pointer.
--------------------------------------*/
#define cThaTxPg1Sts_Vt_TuSSInsPatMask                 cBit17_16
#define cThaTxPg1Sts_Vt_TuSSInsPatShift                16

/*--------------------------------------
BitField Name: TxPg1Sts_Vt_TuSSInsEn
BitField Type: R/W
BitField Desc: SS bits or VT/TU type insert enable bit
               - 1: Enable insertion of TxPg1Sts_Vt_TuSSInsPat[1:0] into SS
                 bits or VT/TU type position in STS/VC/VT/TU pointers.
               - 0: SS bits or VT/TU type position in STS/VC/VT/TU pointer is
                 passed through.
--------------------------------------*/
#define cThaTxPg1Sts_Vt_TuSSInsEnMask                  cBit15
#define cThaTxPg1Sts_Vt_TuSSInsEnShift                 15

/*--------------------------------------
BitField Name: TxPg1Sts_Vt_TuUneqPFrc
BitField Type: R/W
BitField Desc: Path Un-equip force
               - 1: Insert all zeros into all payload bytes with valid
                 pointers.
               - 0: Path Un-equip forcing is disabled.
--------------------------------------*/

/*--------------------------------------
BitField Name: TxPg1Sts_Vt_TuAisPFrc
BitField Type: R/W
BitField Desc: Path AIS force
               - 1: Insert all AIS into all payload bytes and pointer bytes.
               - 0: Path AIS forcing is disabled
--------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: OCN VT/TU PI Per Channel Control
Reg Addr: 0x00052400 � 0x0005457B
          The address format for these registers is 0x00052400 + SliceID*8192
          + StsID*32 + Tug2ID*4 + VtnID
          Where: SliceID: (0) STS-12 slice ID
          StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
          It is noted that for STS level, Tug2ID and VtnID have to be set zero
          value.
Reg Desc: Each register is used to configure for STS pointer generator and
          VT/TU pointer processor including VT pointer interpreter and VT/TU
          pointer generator of the related STS/VT/TU.
------------------------------------------------------------------------------*/
#define cThaRegOcnVtTUPIPerChnCtrl                  0x00052400


/*-----------
BitField Name: RxStsPiVtTermEn
BitField Type: R/W
BitField Desc: Enable to terminate the related VT/Tu. It means that VT/TU
               POH defects related to the VT/TU to generate AIS to
               downstream:
               - 1: Enable.
               - 0: Disable.
------------*/
#define cThaRxStsPiVtTermEnMask                         cBit19
#define cThaRxStsPiVtTermEnShift                        19

/*-----------
BitField Name: RxVtPiAisFrc
BitField Type: R/W
BitField Desc: Per channel AIS force:
               - 1: Force Pointer Interpreter state machine into AIS state.
               - 0: Not force.
------------*/

/*-----------
BitField Name: VtPiVtTypeDetPat
BitField Type: R/W
BitField Desc: This is the pattern that is used to compare with the received VT
               Type.
------------*/
#define cThaVtPiVtTypeDetPatMask                      cBit12_11
#define cThaVtPiVtTypeDetPatShift                     11

/*-----------
BitField Name: VtPiVtTypeDetEn
BitField Type: R/W
BitField Desc: VT-Type detection enable
               - 1: Enable checking VT Type in the pointer interpreter engine.
               - 0: VT Type checking function is disabled.
------------*/
#define cThaVtPiVtTypeDetEnMask                       cBit10
#define cThaVtPiVtTypeDetEnShift                      10

/*-----------
BitField Name: VtPiAdjRule
BitField Type: R/W
BitField Desc: Select the rule for implementing pointer adjustment in the
               pointer interpreter
               - 1: The n of 5 rule is selected. This mode is applied for
                    SDH mode
               - 0: The 8 of 10 rule is selected. This mode is applied for
                    SONET mode
------------*/


/*------------------------------------------------------------------------------
Reg Name: POH J0 Status 1
Reg Addr: 0x08901F_0x08903F
          The address format for these registers is 0x50901F + LineID*128
          Where: LineID: 0 -> 3
Reg Desc: J0 Status 1 Register for SDH/SONET Line.
------------------------------------------------------------------------------*/
#define cThaRegPohJ0Stat1                             0x08901F

/*-----------
BitField Name: J0Tim
BitField Type: R_O
BitField Desc: Section Trace Identifier Mismatch
-----------*/

/*-----------
BitField Name: J0Stb
BitField Type: R_O
BitField Desc: J0 Message Stable
-----------*/

/*-----------
BitField Name: J0State
BitField Type: R_O
BitField Desc: J0 Message State
    2'b00: Out of frame
    2'b01: Going Frame
    2'b10: In Frame
    2'b11: Go Out Frame
-----------*/
#define cThaJ0StateMask                          cBit9_8
#define cThaJ0StateShift                         8

/*-----------
BitField Name: J0FrmCnt
BitField Type: R_O
BitField Desc: J0 Message Frame Count
-----------*/

/*-----------
BitField Name: J0ByteCnt
BitField Type: R_O
BitField Desc: J0 Message Byte Count
-----------*/

/*------------------------------------------------------------------------------
Reg Name: OCN Framer Alarm Affect Control
Reg Addr: 0x0004022E
Reg Desc: Configure to enable to generate AIS-L into payload data to STS/VC XC or
          RDI-L into K2 byte at the transmit direction when TIM-L is detected.
------------------------------------------------------------------------------*/
#define cThaRegOcnFrmrAlmTimLAffCtrl                   0x0004022E

/*-----------
BitField Name: TimLAffAisLRdiLEn
BitField Type: R/W
BitField Desc: Enable/disable the insertion of L-AIS and L-RDI when detecting TIM-L condition at SONTOHMON.
                1: Enable.
                0: Disable.
------------*/
#define cThaTimLAffAisLRdiLEnMask                            cBit7_0
#define cThaTimLAffAisLRdiLEnShift                           0

/*------------------------------------------------------------------------------
Reg Name: POH J0 Section Trace Expected/Captured Message
Reg Addr: 0x0000/0x0100
          The address format for these registers is 0x0000/0x0100 + STSMAX * VTMAX * 16
          + STSMAX * 2 * 16 + LID*16 + DWID
          Where: STSMAX = 6, VTMAX = 28, LIDMAX = 2, LID: 0 → LIDMAX-1, DWID: 0 ->15
Reg Desc: This is the STI Message Storage. Each location has 4 bytes.
          The first byte is located at MSB this register.
          If the J0TimExpMd bit is set, these registers are used to store the STI expected message.
          Other wise when the J0TimExpMd bit is clear, these registers are used to store the STI received message.
------------------------------------------------------------------------------*/

#define cThaRxJ0MsgByteMask(byteId)             ((uint32)cBit31_24 >> (uint32)((byteId) * 8))
#define cThaRxJ0MsgByteShift(byteId)            (8 * (3 - ((byteId) % 4)))

/*------------------------------------------------------------------------------
Reg Name: POH TTI Captured Message Buffer
Reg Addr: 0x08D400 - 0x08D5FF
          The address format for these registers is 0x08D400 + BufID*16 + WCNT
          Where: BufID: 0 -> 31
                 WCNT: 0 -> 15
Reg Desc: This is buffer contains Captured TTI message
------------------------------------------------------------------------------*/
#define cThaRegPohCapturedTtiMsgBuf                    0x08D400

#define cThaCapTtiMsgByteMask(byteId)             ((uint32)cBit31_24 >> (uint32)((byteId) * 8))
#define cThaCapTtiMsgByteShift(byteId)            (8 * (3 - ((byteId) % 4)))

/*------------------------------------------------------------------------------
Reg Name: POH TTI Capturing Message Status
Reg Addr: 0x08D000
Reg Desc: This register contains 32 bit fields which are used to indicate
          the status of 32 Message Capturing Buffers. When the bit field JnMsgCapEnb
          in the CPE Control Register is set, the TTI Capturing engine will used the buffer
          JnMsgCapBufID  to store a TTI incoming message.
          When the message is captured in the buffer, this corresponding bit field of this
          buffer ID will be set.
          If the SW write zero to clear the JnMsgCapEnb in the CPE Control register
          this bit field will be cleared automatically by HW.
------------------------------------------------------------------------------*/
#define cThaRegPohJnTtiMsgCapStat                       0x08D000

#define cThaPohTtiCapMsgStaMask(bufId)              ((uint32)cBit0 << (bufId))

/*------------------------------------------------------------------------------
Reg Name: POH OCn  J0 TIM -L Per Line Current Status
Reg Addr: 0x008E001
Reg Desc: This is Used to store J0 TIM-L State Change interrupt enable of each OCn lines.
------------------------------------------------------------------------------*/
#define cThaRegPohTimLAlmCurStatCtrl                0x008E001


/*--------------------------------------
BitField Name: PohOcnJ0TimLStat
BitField Type: R/W
BitField Desc: POH OCN J0 TIM-L current status of each OCN line.
               If a bit is set, the POH Engine has detected a change of J0 TIM-L current status.
               It will send an interrupt to SW when if the its corresponding POHOcnJ0TIMLStatChgIntrEnb OCN line is set.
BitField Bits: 7_0
--------------------------------------*/
#define cThaPohOcnJ0TimLStatMask(lineId)              (((uint32)cBit0) << (lineId))

/*------------------------------------------------------------------------------
Reg Name: POH OCn  J0 TIM -L State Change Per Line Interrupt Enable
Reg Addr: 0x008E000
Reg Desc: This is Used to store J0 TIM-L State Change interrupt enable of each OCn lines.
------------------------------------------------------------------------------*/
#define cThaRegPohTimLAlmIntrEnCtrl                0x008E000


/*--------------------------------------
BitField Name: PohOcnJ0TimLIntrEnb
BitField Type: R/W
BitField Desc: POH OCN J0 TIM-L State Change interrupt enable register.
               If a bit is set, the POH Engine will send an interrupt to SW
               when it detected any status change of J0 TIM-L status of its corresponding channel.
BitField Bits: 7_0
--------------------------------------*/
#define cThaPohOcnJ0TimLIntrEnbMask(lineId)              (((uint32)cBit0) << (lineId))
#define cThaPohOcnJ0TimLIntrEnbShift(lineId)             (lineId)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPOHV1REG_H_ */



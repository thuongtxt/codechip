/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaSdhLineTtiProcessorV1.c
 *
 * Created Date: Mar 25, 2013
 *
 * Description : SDH Line TTI processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../ocn/v1/ThaModuleOcnV1Reg.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../ThaTtiProcessorInternal.h"
#include "ThaModulePohV1Reg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mLineId(self) ThaModuleSdhLineLocalId((AtSdhLine)ThaTtiProcessorChannelGet((ThaTtiProcessor)self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaSdhLineTtiProcessorV1 * ThaSdhLineTtiProcessorV1;

typedef struct tThaSdhLineTtiProcessorV1Methods
    {
    uint32 (*PohJ0TtiMsgBufDefaultOffset)(ThaSdhLineTtiProcessorV1 self, uint8 bufId);
    uint32 (*PohSdhSonetCtrlDefaultOffset)(ThaSdhLineTtiProcessorV1 self);
    }tThaSdhLineTtiProcessorV1Methods;

typedef struct tThaSdhLineTtiProcessorV1
    {
    tThaTtiProcessor super;
    const tThaSdhLineTtiProcessorV1Methods *methods;

    /* Private data */
    uint8 ttiMode;
    }tThaSdhLineTtiProcessorV1;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhLineTtiProcessorV1Methods m_methods;

/* Override */
static tThaTtiProcessorMethods   m_ThaTtiProcessorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLineTtiProcessorV1);
    }

static uint32 PohJ0TtiMsgBufDefaultOffset(ThaSdhLineTtiProcessorV1 self, uint8 wordId)
    {
    return (mLineId(self) * 16UL) + wordId + ThaTtiProcessorPartOffset((ThaTtiProcessor)self);
    }

static uint32 PohSdhSonetCtrlDefaultOffset(ThaSdhLineTtiProcessorV1 self)
    {
    return mLineId(self) * 128UL + ThaTtiProcessorPartOffset((ThaTtiProcessor)self);
    }

static uint32 AlarmGet(ThaTtiProcessor self)
    {
    uint32 regVal;

    regVal = ThaTtiProcessorRead(self, cThaRegPohTimLAlmCurStatCtrl + ThaTtiProcessorPartOffset(self), cThaModulePoh);
    return (regVal & cThaPohOcnJ0TimLStatMask(mLineId(self))) ? cAtSdhLineAlarmTim : 0;
    }

static uint32 AlarmInterruptGet(ThaTtiProcessor self)
    {
    uint32 regVal;

    regVal = ThaTtiProcessorRead(self, cThaRegPohTimLAlmIntrStatCtrl + ThaTtiProcessorPartOffset(self), cThaModulePoh);
    return (regVal & cThaPohOcnJ0TimLIntrStatMask(mLineId(self))) ? cAtSdhLineAlarmTim : 0;
    }

static uint32 AlarmInterruptClear(ThaTtiProcessor self)
    {
    uint32 currentStatus = ThaTtiProcessorAlarmInterruptGet(self);

    ThaTtiProcessorWrite(self, cThaRegPohTimLAlmIntrStatCtrl + ThaTtiProcessorPartOffset(self), cThaPohOcnJ0TimLIntrStatMask(mLineId(self)), cThaModulePoh);
    return currentStatus;
    }

static eAtRet TxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    uint8    numByteInJ0Msg;
    uint8    byteId, position;
    uint8    *pMsg;
    uint32   address, regVal;
    ThaSdhLineTtiProcessorV1 ttiProcessor = (ThaSdhLineTtiProcessorV1)self;

    /* Get Tx TTI mode */
    tti->mode = ttiProcessor->ttiMode;
    numByteInJ0Msg = AtSdhTtiLengthForTtiMode(tti->mode);

    /* Get message */
    pMsg = tti->message;
    for (byteId = 0; byteId < numByteInJ0Msg; byteId++)
        {
        uint32 offset;
        if ((tti->mode) == cAtSdhTtiMode16Byte)
            continue;

        /* Position of 1 character(1 uint8) in 1 Dword register */
        position = byteId % cThaSonetNumBytesInDword;
        offset  = mMethodsGet(ttiProcessor)->PohJ0TtiMsgBufDefaultOffset(ttiProcessor, byteId / cThaSonetNumBytesInDword);
        address = cThaRegOcnJ0MsgInsBuf + offset;
        regVal  = ThaTtiProcessorRead(self, address, cThaModulePoh);
        mFieldGet(regVal, cThaOcnTxJ0MsgMask(position), cThaOcnTxJ0MsgShift(position), uint8, pMsg);
        pMsg++;
        }

    return cAtOk;
    }

static eAtRet TxTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    uint32    regVal, address;
    uint8     byteId, position;
    uint8     hwJ0Md, msgLen;
    uint8     msgBuf[cAtSdhChannelMaxTtiLength];
    ThaSdhLineTtiProcessorV1 ttiProcessor = (ThaSdhLineTtiProcessorV1)self;

    /* Initialize message buffer */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, msgBuf, 0, sizeof(msgBuf));

    /* Store data to database */
    ttiProcessor->ttiMode = tti->mode;

    /* Get Tx TTI message */
    ThaTtiMsgGet(tti, msgBuf, &hwJ0Md, &msgLen);
    for (byteId = 0; byteId < cAtSdhChannelMaxTtiLength; byteId++)
        {
        position = byteId % cThaSonetNumBytesInDword;
        address = cThaRegOcnJ0MsgInsBuf + mMethodsGet(ttiProcessor)->PohJ0TtiMsgBufDefaultOffset(ttiProcessor, byteId / cThaSonetNumBytesInDword);
        regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);
        mFieldIns(&regVal, cThaOcnTxJ0MsgMask(position), cThaOcnTxJ0MsgShift(position), msgBuf[byteId % msgLen]);
        ThaTtiProcessorWrite(self, address, regVal, cThaModulePoh);
        }

    return cAtOk;
    }

static uint32 J0TtiMsgBufOffset(ThaTtiProcessor self, uint8 dwordId)
    {
    return (mLineId((ThaSdhLineTtiProcessorV1)self) * 512UL) + (31UL * 16UL) + dwordId + ThaTtiProcessorPartOffset(self);
    }

static eAtRet ExpectedTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    uint8 msgLen, hwJ0Md;
    uint8 expectedMsg[cAtSdhChannelMaxTtiLength];
    uint32 regVal, address;
    uint8 byte_i, localByte;
    ThaSdhLineTtiProcessorV1 ttiProcessor = (ThaSdhLineTtiProcessorV1)self;

    /* Initialize memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, expectedMsg, 0, sizeof(expectedMsg));
    ThaTtiMsgGet(tti, expectedMsg, &hwJ0Md, &msgLen);

    /* Configure HW */
    byte_i = 0;
    while (byte_i < msgLen)
        {
        regVal = 0;
        for (localByte = 0; ((localByte < 4) && (byte_i < msgLen)); localByte++)
            {
            mFieldIns(&regVal, cThaRxJ0MsgByteMask(localByte), cThaRxJ0MsgByteShift(localByte), expectedMsg[byte_i]);
            byte_i++;
            }

        address = cThaRegPohJnTtiMsgBuf + J0TtiMsgBufOffset(self, (uint8)((byte_i - 1) / 4));
        ThaTtiProcessorWrite(self, address, regVal, cThaModulePoh);
        }

    address = cThaRegPohSdhSonetCtrl + mMethodsGet(ttiProcessor)->PohSdhSonetCtrlDefaultOffset(ttiProcessor);
    regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaJ0TimMonEnableMask, cThaJ0TimMonEnableShift, 1);
    mFieldIns(&regVal, cThaJ0TimExpMdMask, cThaJ0TimExpMdShift, 1);
    mFieldIns(&regVal, cThaJ0FrmMdMask, cThaJ0FrmMdShift, hwJ0Md);
    mFieldIns(&regVal, cThaJ0MsgCapFrmMdMask, cThaJ0MsgCapFrmMdShift, hwJ0Md);
    mFieldIns(&regVal, cThaJ0BufIDMask, cThaJ0BufIDShift, cThaOcnJ0CapBufId);
    ThaTtiProcessorWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtRet ExpectedTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    uint32 address, regVal;
    uint8  position, byteId;
    uint8  numByteInJ0Msg;
    ThaSdhLineTtiProcessorV1 ttiProcessor = (ThaSdhLineTtiProcessorV1)self;

    address = cThaRegPohSdhSonetCtrl + mMethodsGet(ttiProcessor)->PohSdhSonetCtrlDefaultOffset(ttiProcessor);
    regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);

    /* Get J0 frame mode */
    mFieldGet(regVal, cThaJ0FrmMdMask, cThaJ0FrmMdShift, uint8, &(tti->mode));

    /* Base on the message mode, get the configuration from the hardware */
    numByteInJ0Msg = AtSdhTtiLengthForTtiMode(tti->mode);
    for (byteId = 0; byteId < numByteInJ0Msg; byteId++)
        {
        position = byteId % cThaSonetNumBytesInDword;
        address = cThaRegPohJnTtiMsgBuf + J0TtiMsgBufOffset(self, (byteId / cThaSonetNumBytesInDword));
        regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);
        mFieldGet(regVal, cThaRxJ0MsgByteMask(position), cThaRxJ0MsgByteShift(position), uint8, &(tti->message[byteId]));
        }

    return cAtOk;
    }

static ThaModulePoh PohModule(ThaTtiProcessor self)
    {
    AtSdhChannel channel = ThaTtiProcessorChannelGet(self);
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static eAtRet RxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    eAtRet ret;
    uint32 regAddr, regVal;
    uint8 hwJ0Md;
    uint8 hwJ0State;
    uint32 offset;
    ThaSdhLineTtiProcessorV1 ttiProcessor = (ThaSdhLineTtiProcessorV1)self;
    tAtOsalCurTime startTime, currentTime;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 elapseTimeMs;
    static const uint8 timeoutMs = 5;
    static const uint16 frameTimeInUs = 125;

    /* Get current mode */
    offset  = mMethodsGet(ttiProcessor)->PohSdhSonetCtrlDefaultOffset(ttiProcessor);
    regAddr = cThaRegPohSdhSonetCtrl + offset;
    regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
    mFieldGet(regVal, cThaJ0MsgCapFrmMdMask, cThaJ0MsgCapFrmMdShift, uint8, &hwJ0Md);
    tti->mode = hwJ0Md;

    /* Calculate the length and make request */
    mFieldIns(&regVal, cThaJ0BufIDMask, cThaJ0BufIDShift, cThaOcnJ0CapBufId);
    mFieldIns(&regVal, cThaJ0MsgCapEnbMask, cThaJ0MsgCapEnbShift, 1);
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);

    /* Timeout waiting for in-frame state */
    regAddr = cThaRegPohJ0Stat1 + offset;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    elapseTimeMs = 0;
    while (elapseTimeMs < timeoutMs)
        {
        regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
        mFieldGet(regVal, cThaJ0StateMask, cThaJ0StateShift, uint8, &hwJ0State);
        if (hwJ0State == cHwStateInframe)
            break;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, currentTime);
        if (elapseTimeMs >= timeoutMs)
            return cAtErrorIndrAcsTimeOut;
        }

    /* Get the message */
    ret = ThaModulePohV1TtiCapture(PohModule(self),
                                   ThaTtiProcessorChannelGet(self),
                                   cThaOcnJ0CapBufId,
                                   tti->mode,
                                   tti->message,
                                   frameTimeInUs);

    /* Stop capturing */
    regAddr = cThaRegPohSdhSonetCtrl + offset;
    regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
    mFieldIns(&regVal, cThaJ0MsgCapEnbMask, cThaJ0MsgCapEnbShift, 0);
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);

    return ret;
    }

static eAtRet TimAffectingEnable(ThaTtiProcessor self, eBool enable)
    {
    uint32 regVal;
    uint32 regAddr = cThaRegOcnFrmrAlmAffCtrl + ThaTtiProcessorPartOffset(self);

    regVal = ThaTtiProcessorRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cThaTimLAisPEnMask,
              cThaTimLAisPEnShift,
              enable ? 1 : 0);
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool TimAffectingIsEnabled(ThaTtiProcessor self)
    {
    uint32 regVal;

    regVal = ThaTtiProcessorRead(self, cThaRegOcnFrmrAlmAffCtrl + ThaTtiProcessorPartOffset(self), cThaModuleOcn);
    return (regVal & cThaTimLAisPEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet InterruptMaskSet(ThaTtiProcessor self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal;
    uint32 regAddr;
    uint32 lineId;

    if ((defectMask & cAtSdhLineAlarmTim) == 0)
        return cAtOk;

    regAddr = cThaRegPohTimLAlmIntrEnCtrl + ThaTtiProcessorPartOffset(self);
    regVal  = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
    lineId  = mLineId(self);
    mFieldIns(&regVal,
              cThaPohOcnJ0TimLIntrEnbMask(lineId),
              cThaPohOcnJ0TimLIntrEnbShift(lineId),
              (enableMask & cAtSdhLineAlarmTim) ? 1 : 0);
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(ThaTtiProcessor self)
    {
    uint32 regVal;

    regVal = ThaTtiProcessorRead(self, cThaRegPohTimLAlmIntrEnCtrl + ThaTtiProcessorPartOffset(self), cThaModulePoh);
    return (regVal & cThaPohOcnJ0TimLIntrEnbMask(mLineId(self))) ? cAtSdhLineAlarmTim : 0;
    }

static void OverrideThaTtiProcessor(ThaTtiProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaTtiProcessorOverride, mMethodsGet(self), sizeof(m_ThaTtiProcessorOverride));

        mMethodOverride(m_ThaTtiProcessorOverride, TxTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, TxTtiSet);
        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiSet);
        mMethodOverride(m_ThaTtiProcessorOverride, RxTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, AlarmGet);
        mMethodOverride(m_ThaTtiProcessorOverride, AlarmInterruptGet);
        mMethodOverride(m_ThaTtiProcessorOverride, AlarmInterruptClear);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingEnable);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingIsEnabled);
        mMethodOverride(m_ThaTtiProcessorOverride, InterruptMaskSet);
        mMethodOverride(m_ThaTtiProcessorOverride, InterruptMaskGet);
        }

    mMethodsSet(self, &m_ThaTtiProcessorOverride);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideThaTtiProcessor(self);
    }

static void MethodsInit(ThaSdhLineTtiProcessorV1 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PohJ0TtiMsgBufDefaultOffset);
        mMethodOverride(m_methods, PohSdhSonetCtrlDefaultOffset);
        }

    mMethodsSet(self, &m_methods);
    }

static ThaTtiProcessor ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaTtiProcessorObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaSdhLineTtiProcessorV1)self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor ThaSdhLineTtiProcessorV1New(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, sdhChannel);
    }

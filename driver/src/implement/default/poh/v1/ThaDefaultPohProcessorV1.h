/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaDefaultPohProcessorV1.h
 * 
 * Created Date: Mar 29, 2013
 *
 * Description : Default POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEFAULTPOHPROCESSORV1_H_
#define _THADEFAULTPOHPROCESSORV1_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaPohProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDefaultPohProcessorV1 * ThaDefaultPohProcessorV1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THADEFAULTPOHPROCESSORV1_H_ */


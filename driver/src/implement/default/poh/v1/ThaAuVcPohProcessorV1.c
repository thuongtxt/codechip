/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaAuVcPohProcessorV1.c
 *
 * Created Date: Mar 19, 2013
 *
 * Description : AU's VC POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaAuVcPohProcessorV1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAuVcPohProcessorV1Methods m_methods;

static tThaDefaultPohProcessorV1Methods m_ThaDefaultPohProcessorV1Override;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods m_AtSdhPathOverride;
static tAtChannelMethods m_AtChannelOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 InterruptDefaultOffset(ThaAuVcPohProcessorV1 self)
    {
    uint8 stsId, sliceId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &sliceId, &stsId);

    return (sliceId * 16UL) + stsId + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint8 TtiCaptureBufferId(ThaDefaultPohProcessorV1 self)
    {
	AtUnused(self);
    return 2;
    }

static uint32 ExpectTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
    uint8 stsId, sliceId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &sliceId, &stsId);
    return (stsId * 512UL) + (28UL * 16UL) + dwordIndex + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 DefaultOffset(ThaDefaultPohProcessorV1 self)
    {
    uint8 stsId, sliceId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &sliceId, &stsId);
    return (stsId * 128UL) + (sliceId * 32UL) + 28UL + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint32 InsertTtiOffset(ThaDefaultPohProcessorV1 self, uint8 dwordIndex)
    {
    uint8 stsId, sliceId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &sliceId, &stsId);
    return (sliceId * 16UL) + (stsId * 512UL) + 448UL + dwordIndex + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    uint32 regVal, address, offset;
    uint8 enableValue;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get current configuration */
    offset = mMethodsGet(processor)->DefaultOffset(processor);
    address = cThaRegPohCpeInsCtrl + offset;
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* RDI when AIS/LOP */
    enableValue = enable ? 0 : 1;
    if ((alarmType & cAtSdhPathAlarmAis) || (alarmType & cAtSdhPathAlarmLop))
        {
        mFieldIns(&regVal, cThaAisLopPMskMask, cThaAisLopPMskShift, enableValue);
        }

    /* TIM */
    if (alarmType & cAtSdhPathAlarmTim)
        {
        mFieldIns(&regVal, cThaTimPMskMask, cThaTimPMskShift, enableValue);
        }

    /* PLM */
    if (alarmType & cAtSdhPathAlarmPlm)
        {
        mFieldIns(&regVal, cThaPlmPMskMask, cThaPlmPMskShift, enableValue);
        }

    /* UNEQ */
    if (alarmType & cAtSdhPathAlarmUneq)
        {
        mFieldIns(&regVal, cThaUneqPMskMask, cThaUneqPMskShift, enableValue);
        }

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    /* Enable to forwarding AIS to downstream when TIM/PLM/VCAIS defect is detected */
    enableValue = (uint8)mBoolToBin(enable);
    address = cThaRegPohCpeCtrl + offset;
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* TIM */
    if (alarmType & cAtSdhPathAlarmTim)
        {
        mFieldIns(&regVal, cThaTimP2AisEnMask, cThaTimP2AisEnShift, enableValue);
        }

    /* PLM */
    if (alarmType & cAtSdhPathAlarmPlm)
        {
        mFieldIns(&regVal, cThaPlmVcais2AisEnMask, cThaPlmVcais2AisEnShift, enableValue);
        }

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    uint32 regVal = mChannelHwRead(self, cThaRegPohCpeInsCtrl + mMethodsGet(processor)->DefaultOffset(processor), cThaModulePoh);
    uint32 mask = 0;

    if (regVal & cThaAisLopPMskMask)
        mask |= (cAtSdhPathAlarmAis | cAtSdhPathAlarmLop);

    if (regVal & cThaTimPMskMask)
        mask |= cAtSdhPathAlarmTim;

    if (regVal & cThaPlmPMskMask)
        mask |= cAtSdhPathAlarmPlm;

    if (regVal & cThaUneqPMskMask)
        mask |= cAtSdhPathAlarmUneq;

    return mask;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    uint32 regVal;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get current configuration */
    regVal = mChannelHwRead(self, cThaRegPohCpeInsCtrl + mMethodsGet(processor)->DefaultOffset(processor), cThaModulePoh);

    /* RDI when AIS/LOP */
    if ((alarmType & cAtSdhPathAlarmAis) || (alarmType & cAtSdhPathAlarmLop))
        return (regVal & cThaAisLopPMskMask) ? cAtFalse : cAtTrue;

    /* TIM */
    if (alarmType & cAtSdhPathAlarmTim)
        return (regVal & cThaTimPMskMask) ? cAtFalse : cAtTrue;

    /* PLM */
    if (alarmType & cAtSdhPathAlarmPlm)
        return (regVal & cThaPlmPMskMask) ? cAtFalse : cAtTrue;

    /* UNEQ */
    if (alarmType & cAtSdhPathAlarmUneq)
        return (regVal & cThaUneqPMskMask) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static uint32 CpeAlarmStatusBaseRegister(ThaAuVcPohProcessorV1 self)
    {
	AtUnused(self);
    return cThaRegPohCpeStsAlmStat;
    }

static uint32 ErdiTypeFromErdiValue(uint8 erdiValue)
    {
    /* ERDI Payload defect */
    if (erdiValue == cERDIPayloadValue)
        return (uint32)cAtSdhPathAlarmErdiP;

    /* ERDI Connectivity defect */
    else if (erdiValue == cERDIConnectivityValue)
        return (uint32)cAtSdhPathAlarmErdiC;

    /* ERDI Server defect */
    else if (erdiValue & cBit2)
        return (uint32)cAtSdhPathAlarmErdiS;

    return 0;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 address, regVal;
    uint32 alarmState = 0;
    uint8 erdiValue;
    ThaAuVcPohProcessorV1 auVcProcessor = (ThaAuVcPohProcessorV1)self;
    ThaDefaultPohProcessorV1 defaultProcessor = (ThaDefaultPohProcessorV1)self;

    /* Read status */
    address = mMethodsGet(auVcProcessor)->CpeAlarmStatusBaseRegister(auVcProcessor) + InterruptDefaultOffset(auVcProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Convert to software value */
    if (regVal & cThaPohStsUneqIntMask)
        alarmState |= cAtSdhPathAlarmUneq;
    if (regVal & cThaPohStsPlmIntMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohStsVcAisIntMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohStsTtiTimIntMask)
        alarmState |= cAtSdhPathAlarmTim;

    /* Get ERDI-P */
    address = cThaRegPohCpeStat2 + mMethodsGet(defaultProcessor)->DefaultOffset(defaultProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaERdiPValMask, cThaERdiPValShift, uint8, &erdiValue);
    alarmState |= ErdiTypeFromErdiValue(erdiValue);

    return alarmState;
    }

static uint32 CpeAlarmInterruptStickyBaseRegister(ThaAuVcPohProcessorV1 self)
    {
	AtUnused(self);
    return cThaRegPohCpeStsAlmIntrStk;
    }

static uint32 DefectInterruptRead2Clear(AtSdhVc self, eBool readToClear)
    {
    uint32 address, regVal;
    uint32 alarmState = 0;
    uint8 erdiValue;
    ThaAuVcPohProcessorV1 auProcessor         = (ThaAuVcPohProcessorV1)self;
    ThaDefaultPohProcessorV1 defaultProcessor = (ThaDefaultPohProcessorV1)self;

    /* Get hardware status */
    address = mMethodsGet(auProcessor)->CpeAlarmInterruptStickyBaseRegister(auProcessor) + InterruptDefaultOffset(auProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    if (readToClear)
        mChannelHwWrite(self, address, regVal, cThaModulePoh);

    /* Return software value */
    if (regVal & cThaPohStsTtiTimIntMask)
        alarmState |= cAtSdhPathAlarmTim;
    if (regVal & cThaPohStsPlmIntMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohStsVcAisIntMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohStsUneqIntMask)
        alarmState |= cAtSdhPathAlarmUneq;

    /* Get ERDI-P interrupt */
    if (regVal & cThaPohStsRdiStbChgIntMask) /* ERDI is changed value */
        {
        address = cThaRegPohCpeStat2 + mMethodsGet(defaultProcessor)->DefaultOffset(defaultProcessor);
        regVal = mChannelHwRead(self, address, cThaModulePoh);
        mFieldGet(regVal, cThaERdiPValMask, cThaERdiPValShift, uint8, &erdiValue);
        alarmState |= ErdiTypeFromErdiValue(erdiValue);
        }

    return alarmState;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectInterruptRead2Clear((AtSdhVc)self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectInterruptRead2Clear((AtSdhVc)self, cAtTrue);
    }

static uint32 CpeAlmIntrEnCtrlBaseRegister(ThaAuVcPohProcessorV1 self)
    {
	AtUnused(self);
    return cThaRegPohCpeStsAlmIntrEnCtrl;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtSdhPathAlarmAis   |
            cAtSdhPathAlarmTim   |
            cAtSdhPathAlarmPlm   |
            cAtSdhPathAlarmUneq  |
            cAtSdhPathAlarmRdi   |
            cAtSdhPathAlarmErdiS |
            cAtSdhPathAlarmErdiP |
            cAtSdhPathAlarmErdiC);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV1 auProcessor = (ThaAuVcPohProcessorV1)self;

    /* Get current configuration */
    address = mMethodsGet(auProcessor)->CpeAlmIntrEnCtrlBaseRegister(auProcessor) + InterruptDefaultOffset(auProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Configure AIS mask */
    if (defectMask & cAtSdhPathAlarmAis)
        mFieldIns(&regVal,
                  cThaPohStsVcAisIntEnbMask,
                  cThaPohStsVcAisIntEnbShift,
                  (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);

    /* Configure TTI mask */
    if (defectMask & cAtSdhPathAlarmTim)
        mFieldIns(&regVal,
                  cThaPohStsTtiTimIntEnbMask,
                  cThaPohStsTtiTimIntEnbShift,
                  (enableMask & cAtSdhPathAlarmTim) ? 1 : 0);

    /* Configure PLM mask */
    if (defectMask & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal,
                  cThaPohStsPlmIntEnbMask,
                  cThaPohStsPlmIntEnbShift,
                  (enableMask & cAtSdhPathAlarmPlm) ? 1 : 0);

    /* Configure UNEQ mask */
    if (defectMask & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal,
                  cThaPohStsUneqIntEnbMask,
                  cThaPohStsUneqIntEnbShift,
                  (enableMask & cAtSdhPathAlarmUneq) ? 1 : 0);

    /* Configure RDI mask */
    if (defectMask & (cAtSdhPathAlarmRdi | cAtSdhPathAlarmErdiS | cAtSdhPathAlarmErdiP | cAtSdhPathAlarmErdiC))
        mFieldIns(&regVal,
                  cThaPohStsRdiStbChgIntEnbMask,
                  cThaPohStsRdiStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmRdi) ? 1 : 0);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regVal, address, swMask = 0;
    ThaAuVcPohProcessorV1 processor = (ThaAuVcPohProcessorV1)self;

    /* Get current configuration */
    address = mMethodsGet(processor)->CpeAlmIntrEnCtrlBaseRegister(processor) + InterruptDefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Get AIS mask */
    if (regVal & cThaPohStsVcAisIntEnbMask)
        swMask |= cAtSdhPathAlarmAis;

    /* Get TTI mask */
    if (regVal & cThaPohStsTtiTimIntEnbMask)
        swMask |= cAtSdhPathAlarmTim;

    /* Get PLM mask */
    if (regVal & cThaPohStsPlmIntEnbMask)
        swMask |= cAtSdhPathAlarmPlm;

    /* Get UNEQ mask */
    if (regVal & cThaPohStsUneqIntEnbMask)
        swMask |= cAtSdhPathAlarmUneq;

    /* Get RDI mask */
    if (regVal & cThaPohStsRdiStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmRdi;

    return swMask;
    }

static uint32 PohPathTerInsBufDeftOffset(ThaDefaultPohProcessorV1 self, uint8 bId)
    {
    uint8 outSlice, outSts;

    ThaDefaultPohProcessorInsertionChannelGet(self, &outSlice, &outSts, NULL);

    /* For STS/TU3/TU */
    return (outSlice * 2048UL) + (outSts * 128UL) + 112UL + bId + ThaPohProcessorPartOffset((ThaPohProcessor)self);
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 pslVal;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get the C2 PSL value from the POH CPE Status 2 Register */
    address = cThaRegPohCpeStat2 + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaC2PslValMask, cThaC2PslValShift, uint8, &pslVal);

    return pslVal;
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 pslValue;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get the expected mask/shift of Path/V Signal Label */
    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaPslExptValMask, cThaPslExptValShift, uint8, &pslValue);

    return pslValue;
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    uint32 regVal, address;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    /* Get the expected mask/shift of Path/V Signal Label */
    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaPslExptValMask, cThaPslExptValShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    uint32 regVal, address;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaJ1TimMonEnableMask, cThaJ1TimMonEnableShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    uint32 regVal, address;
    eBool enable;
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    address = cThaRegPohCpeCtrl + mMethodsGet(processor)->DefaultOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaJ1TimMonEnableMask, cThaJ1TimMonEnableShift, uint8, &enable);

    return mBinToBool(enable);
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;
    uint32 offset = mMethodsGet(processor)->DefaultOffset(processor);

    ThaDefaultPohProcessorCounterRead2ClearEnable(processor, read2Clear);

    /* BIP */
    if (counterType == cAtSdhPathCounterTypeBip)
        return mChannelHwRead(self, cThaRegPohCpeCnt1BipErr + offset, cThaModulePoh) & cThaB3ErrCntMask;

    /* REI */
    if (counterType == cAtSdhPathCounterTypeRei)
        return mChannelHwRead(self, cThaRegPohCpeCnt1Rei + offset, cThaModulePoh) & cThaReiPErrCntMask;

    /* Unknown counter */
    return 0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static void OverrideThaDefaultPohProcessorV1(ThaAuVcPohProcessorV1 self)
    {
    ThaDefaultPohProcessorV1 processor = (ThaDefaultPohProcessorV1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDefaultPohProcessorV1Override, mMethodsGet(processor), sizeof(m_ThaDefaultPohProcessorV1Override));
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, TtiCaptureBufferId);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, PohPathTerInsBufDeftOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, ExpectTtiOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, DefaultOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV1Override, InsertTtiOffset);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV1Override);
    }

static void OverrideAtSdhChannel(ThaAuVcPohProcessorV1 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(ThaAuVcPohProcessorV1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(ThaAuVcPohProcessorV1 self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void Override(ThaAuVcPohProcessorV1 self)
    {
    OverrideThaDefaultPohProcessorV1(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    }

static void MethodsInit(ThaAuVcPohProcessorV1 self)
    {
    ThaAuVcPohProcessorV1 vc = (ThaAuVcPohProcessorV1)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CpeAlarmStatusBaseRegister);
        mMethodOverride(m_methods, CpeAlarmInterruptStickyBaseRegister);
        mMethodOverride(m_methods, CpeAlmIntrEnCtrlBaseRegister);
        }

    mMethodsSet(vc, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaAuVcPohProcessorV1);
    }

ThaPohProcessor ThaAuVcPohProcessorV1ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDefaultPohProcessorV1ObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaAuVcPohProcessorV1)self);
    MethodsInit((ThaAuVcPohProcessorV1)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor ThaAuVcPohProcessorV1New(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ThaAuVcPohProcessorV1ObjectInit(newProcessor, vc);
    }


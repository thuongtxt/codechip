/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaPohProcessor.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPOHPROCESSOR_H_
#define _THAPOHPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPohProcessor * ThaPohProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc ThaPohProcessorVcGet(ThaPohProcessor self);
eAtRet ThaPohProcessorVcAisAffectEnable(ThaPohProcessor self, eBool enable);
eBool ThaPohProcessorCanEnableAlarmAffecting(ThaPohProcessor self, uint32 alarmType, eBool enable);

eAtRet ThaPohProcessorAutoRdiEnable(ThaPohProcessor self, uint32 alarmType, eBool enable);
uint32 ThaPohProcessorAutoRdiAlarms(ThaPohProcessor self);
eBool ThaPohProcessorIsSimulated(ThaPohProcessor self);
uint32 ThaPohProcessorDefaultAlarmsNeedAutoRdi(ThaPohProcessor self, eBool enable);
void ThaPohProcessorCounterAccumulate(ThaPohProcessor self, uint16 counterType, uint32 value);
eAtModuleSdhRet ThaPohProcessorDefaultAutoRdiWhenERdiUpdate(ThaPohProcessor self);

/* Enhanced RDIs */
eAtRet ThaPohProcessorTxERdiEnable(ThaPohProcessor self, eBool enable);
eBool ThaPohProcessorTxERdiIsEnabled(ThaPohProcessor self);
eAtRet ThaPohProcessorERdiMonitorEnable(ThaPohProcessor self, eBool enable);
eBool ThaPohProcessorERdiMonitorIsEnabled(ThaPohProcessor self);

/* Default concretes */
ThaPohProcessor ThaAuVcPohProcessorV2New(AtSdhVc vc);
ThaPohProcessor ThaVc1xPohProcessorV2New(AtSdhVc vc);
ThaPohProcessor ThaTu3VcPohProcessorV2New(AtSdhVc vc);
ThaPohProcessor ThaAuVcPohProcessorV1New(AtSdhVc vc);
ThaPohProcessor ThaVc1xPohProcessorV1New(AtSdhVc vc);
ThaPohProcessor ThaTu3VcPohProcessorV1New(AtSdhVc vc);

/* Product concretes */
ThaPohProcessor Tha60150011AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60150011Tu3VcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha60150011Vc1xPohProcessorNew(AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THAPOHPROCESSOR_H_ */


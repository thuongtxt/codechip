/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaPohProcessor.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPohProcessorInternal.h"
#include "../sdh/ThaModuleSdh.h"
#include "../man/ThaDeviceInternal.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPohProcessor)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPohProcessorMethods  m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tAtSdhPathMethods    *m_AtSdhPathMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPohProcessor);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPohProcessor object = (ThaPohProcessor)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeChannelIdString(vc);
    mEncodeUInt(partOffset);
    mEncodeNone(cache);
    mEncodeNone(simulation);
    }

static const char *TypeString(AtChannel self)
    {
    static char buf[64];
    AtChannel vc = (AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    AtSnprintf(buf, sizeof(buf), "%s_poh_processor", AtChannelTypeString(vc));
    return buf;
    }

static const char *IdString(AtChannel self)
    {
    AtChannel vc = (AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    return AtChannelIdString(vc);
    }

static tThaPohProcessorSimulation *SimulationDatabaseCreate(ThaPohProcessor self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaPohProcessorSimulation);
    tThaPohProcessorSimulation *database = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    AtUnused(self);
    if (database == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, database, 0, memorySize);
    return database;
    }

static void SimulationDatabaseDelete(ThaPohProcessor self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, self->simulation);
    self->simulation = NULL;
    }

static tThaPohProcessorSimulation *SimulationDatabase(ThaPohProcessor self)
    {
    if (self->simulation == NULL)
        self->simulation = SimulationDatabaseCreate(self);
    return self->simulation;
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    AtOsal osal;

    if (!ThaPohProcessorIsSimulated(mThis(self)))
        return m_AtSdhChannelMethods->TxTtiGet(self, tti);

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, tti, &(SimulationDatabase(mThis(self))->txTti), sizeof(tAtSdhTti));

    return cAtOk;
    }

static uint32 TtiLength(eAtSdhTtiMode ttiMode)
    {
    if (ttiMode == cAtSdhTtiMode1Byte)  return 1;
    if (ttiMode == cAtSdhTtiMode16Byte) return 16;
    if (ttiMode == cAtSdhTtiMode64Byte) return 64;

    return 0;
    }

static void TtiCopy(const tAtSdhTti *s_tti, tAtSdhTti *d_tti)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, d_tti, 0, sizeof(tAtSdhTti));
    mMethodsGet(osal)->MemCpy(osal, d_tti->message, s_tti->message, TtiLength(s_tti->mode));
    d_tti->mode = s_tti->mode;
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    if (!ThaPohProcessorIsSimulated(mThis(self)))
        return m_AtSdhChannelMethods->TxTtiSet(self, tti);

    TtiCopy(tti, &(SimulationDatabase(mThis(self))->txTti));

    return cAtOk;
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    AtOsal osal;

    if (!ThaPohProcessorIsSimulated(mThis(self)))
        return m_AtSdhChannelMethods->ExpectedTtiGet(self, tti);

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, tti, &(SimulationDatabase(mThis(self))->expectedTti), sizeof(tAtSdhTti));

    return cAtOk;
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    if (!ThaPohProcessorIsSimulated(mThis(self)))
        return m_AtSdhChannelMethods->ExpectedTtiSet(self, tti);

    TtiCopy(tti, &(SimulationDatabase(mThis(self))->expectedTti));

    return cAtOk;
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    if (!ThaPohProcessorIsSimulated(mThis(self)))
        return m_AtSdhChannelMethods->TtiCompareEnable(self, enable);

    SimulationDatabase(mThis(self))->ttiCompareEnable = enable;

    return cAtOk;
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    if (!ThaPohProcessorIsSimulated(mThis(self)))
        return m_AtSdhChannelMethods->TtiCompareIsEnabled(self);

    return SimulationDatabase(mThis(self))->ttiCompareEnable;
    }

static void Delete(AtObject self)
    {
    SimulationDatabaseDelete((ThaPohProcessor)self);
    m_AtObjectMethods->Delete(self);
    }

static eBool ERdiDatabaseIsEnabled(AtSdhPath self)
    {
    AtChannelLog((AtChannel)self, cAtLogLevelWarning, AtSourceLocation, "POH process does not cache ERDI enabling but its VC.\r\n");
    return m_AtSdhPathMethods->ERdiDatabaseIsEnabled(self);
    }

static void CounterAccumulate(ThaPohProcessor self, uint16 counterType, uint32 value)
    {
    AtUnused(self);
    AtUnused(counterType);
    AtUnused(value);
    }

static eAtRet AutoReiEnable(AtSdhChannel self, eBool enable)
    {
    AtUnused(self);
    return (enable) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool AutoReiIsEnabled(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtSdhPath(ThaPohProcessor self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, m_AtSdhPathMethods, sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, ERdiDatabaseIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtObject(ThaPohProcessor self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhChannel(ThaPohProcessor self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareEnable);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoReiIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static eAtRet VcAisAffectEnable(ThaPohProcessor self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub-class do */
    return cAtError;
    }

static eBool ShouldEnableTimBackwardRdiAsDefault(AtChannel self)
    {
    if (AtSdhChannelAlarmAffectingIsEnabled((AtSdhChannel)self, cAtSdhPathAlarmTim))
        return cAtTrue;

    if (ThaModuleSdhAlwaysDisableRdiBackwardWhenTimNotDownstreamAis((ThaModuleSdh)AtChannelModuleGet(self)))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 DefaultAlarmsNeedAutoRdi(AtChannel self, eBool enable)
    {
    uint32 alarms = cAtSdhPathAlarmLop | cAtSdhPathAlarmAis | cAtSdhPathAlarmUneq | cAtSdhPathAlarmPlm;

    if (enable && (ThaPohProcessorTxERdiIsEnabled((ThaPohProcessor)self) == cAtFalse))
        {
        if (AtSdhChannelModeGet((AtSdhChannel)self) == cAtSdhChannelModeSonet)
            return cAtSdhPathAlarmLop | cAtSdhPathAlarmAis;
        else
            alarms &= (uint32)(~cAtSdhPathAlarmPlm);
        }

    if (ShouldEnableTimBackwardRdiAsDefault(self))
        alarms |= cAtSdhPathAlarmTim;

    return alarms;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ThaPohProcessorAutoRdiEnable((ThaPohProcessor)self, DefaultAlarmsNeedAutoRdi(self, cAtTrue), cAtTrue);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tThaPohProcessorCache);
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    AtChannel vc = (AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    return AtChannelCanAccessRegister(vc, moduleId, address);
    }

static void OverrideAtChannel(ThaPohProcessor self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, CacheSize);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static eBool CanForceTxAlarms(ThaPohProcessor self, uint32 alarmType)
    {
    return (alarmType & AtChannelTxForcableAlarmsGet((AtChannel)self)) ? cAtTrue : cAtFalse;
    }

static uint32 TxForceAbleErrors(ThaPohProcessor self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet AutoRdiEnable(ThaPohProcessor self, uint32 alarmType, eBool enable)
    {
	AtUnused(enable);
	AtUnused(alarmType);
	AtUnused(self);
    return cAtOk;
    }

static uint32 AutoRdiAlarms(ThaPohProcessor self)
    {
	AtUnused(self);
    return 0x0;
    }

static eAtRet TxERdiEnable(ThaPohProcessor self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool TxERdiIsEnabled(ThaPohProcessor self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ERdiMonitorEnable(ThaPohProcessor self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool ERdiMonitorIsEnabled(ThaPohProcessor self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PartOffset(AtSdhVc self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cThaModuleOcn, ThaModuleSdhPartOfChannel((AtSdhChannel)self));
    }

static eBool InAccessible(ThaPohProcessor self)
    {
    return AtChannelInAccessible((AtChannel)ThaPohProcessorVcGet(self));
    }

static tThaPohProcessorCache *Cache(ThaPohProcessor self)
    {
    if (InAccessible(self))
        return (tThaPohProcessorCache *)AtChannelCacheGet((AtChannel)self);
    return NULL;
    }

static void CacheAutoRdiEnable(ThaPohProcessor self, uint32 alarmType, eBool enable)
    {
    tThaPohProcessorCache *cache = Cache(self);
    if(cache == NULL)
        return;

    if (enable)
        cache->autoRdiAlarms |= alarmType;
    else
        cache->autoRdiAlarms &= ~alarmType;
    }

static uint32 CacheAutoRdiAlarms(ThaPohProcessor self)
    {
    tThaPohProcessorCache *cache = Cache(self);
    return cache ? cache->autoRdiAlarms : 0;
    }

static void Override(ThaPohProcessor self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    }

static void MethodsInit(ThaPohProcessor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, VcAisAffectEnable);
        mMethodOverride(m_methods, CanForceTxAlarms);
        mMethodOverride(m_methods, TxForceAbleErrors);
        mMethodOverride(m_methods, AutoRdiEnable);
        mMethodOverride(m_methods, AutoRdiAlarms);
        mMethodOverride(m_methods, TxERdiEnable);
        mMethodOverride(m_methods, TxERdiIsEnabled);
        mMethodOverride(m_methods, ERdiMonitorEnable);
        mMethodOverride(m_methods, ERdiMonitorIsEnabled);
        mMethodOverride(m_methods, CounterAccumulate);
        }

    mMethodsSet(self, &m_methods);
    }

ThaPohProcessor ThaPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhPathObjectInit((AtSdhPath)self,
                            AtChannelIdGet((AtChannel)vc),
                            AtSdhChannelTypeGet((AtSdhChannel)vc),
                            (AtModuleSdh)AtChannelModuleGet((AtChannel)vc)) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Setup private data */
    self->vc = vc;
    self->partOffset = PartOffset(vc);

    return self;
    }

AtSdhVc ThaPohProcessorVcGet(ThaPohProcessor self)
    {
    return self ? self->vc : NULL;
    }

eBool ThaPohProcessorIsSimulated(ThaPohProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPohProcessorVcGet(self));
    return AtDeviceIsSimulated(device);
    }

uint32 ThaPohProcessorPartOffset(ThaPohProcessor self)
    {
    return self ? self->partOffset : 0;
    }

eAtRet ThaPohProcessorVcAisAffectEnable(ThaPohProcessor self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->VcAisAffectEnable(self, enable);

    return cAtError;
    }

eBool ThaPohProcessorCanEnableAlarmAffecting(ThaPohProcessor self, uint32 alarmType, eBool enable)
    {
	AtUnused(self);
    /* When LOP/AIS happen, AIS is always inserted downstream by standard.
     * Disable this action does not have any meaning */
    if (((alarmType & cAtSdhPathAlarmAis) || (alarmType & cAtSdhPathAlarmLop)) && (!enable))
        return cAtFalse;

    /* There is no specific control for Payload UNEQ. Instead, it should be
     * controlled via UNEQ. */
    if ((alarmType & cAtSdhPathAlarmPayloadUneq) && enable)
        return cAtFalse;

    return cAtTrue;
    }

eBool ThaPohProcessorTimBackwardRdiShouldEnableWhenAisDownstream(ThaPohProcessor self, eBool timDownstreamAis)
    {
    ThaModuleSdh sdhModule;

    if (timDownstreamAis)
        return cAtTrue;

    sdhModule = (ThaModuleSdh)AtChannelModuleGet((AtChannel)self);
    if (ThaModuleSdhAlwaysDisableRdiBackwardWhenTimNotDownstreamAis(sdhModule))
        return cAtFalse;

    return AtSdhPathERdiIsEnabled((AtSdhPath)self);
    }

eAtRet ThaPohProcessorAutoRdiEnable(ThaPohProcessor self, uint32 alarmType, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    CacheAutoRdiEnable(self, alarmType, enable);
    return mMethodsGet(self)->AutoRdiEnable(self, alarmType, enable);
    }

uint32 ThaPohProcessorAutoRdiAlarms(ThaPohProcessor self)
    {
    if (self == NULL)
        return 0;

    if (InAccessible(self))
        return CacheAutoRdiAlarms(self);

    return mMethodsGet(self)->AutoRdiAlarms(self);
    }

eAtRet ThaPohProcessorTxERdiEnable(ThaPohProcessor self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TxERdiEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPohProcessorTxERdiIsEnabled(ThaPohProcessor self)
    {
    if (self)
        return mMethodsGet(self)->TxERdiIsEnabled(self);
    return cAtFalse;
    }

eAtRet ThaPohProcessorERdiMonitorEnable(ThaPohProcessor self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->ERdiMonitorEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPohProcessorERdiMonitorIsEnabled(ThaPohProcessor self)
    {
    if (self)
        return mMethodsGet(self)->ERdiMonitorIsEnabled(self);
    return cAtFalse;
    }

eAtModuleSdhRet ThaPohProcessorTimAutoRdiUpdateWhenERdiEnable(ThaPohProcessor self, eBool enable)
    {
    eBool backwardRdi;
    ThaModuleSdh sdhModule;

    if (!enable)
        return cAtOk;

    backwardRdi = cAtTrue;
    sdhModule = (ThaModuleSdh)AtChannelModuleGet((AtChannel)self);
    if (!AtSdhChannelAlarmAffectingIsEnabled((AtSdhChannel)self, cAtSdhPathAlarmTim) &&
        ThaModuleSdhAlwaysDisableRdiBackwardWhenTimNotDownstreamAis(sdhModule))
        backwardRdi = cAtFalse;

    return ThaPohProcessorAutoRdiEnable(self, cAtSdhPathAlarmTim, backwardRdi);
    }

eAtModuleSdhRet ThaPohProcessorDefaultAutoRdiWhenERdiUpdate(ThaPohProcessor self)
    {
    eAtRet ret;
    uint32 autoRdiAlarms = ThaPohProcessorDefaultAlarmsNeedAutoRdi(self, cAtTrue);
    uint32 currentAutoRdiAlarms = ThaPohProcessorAutoRdiAlarms(self);

    /* Nothing changed or no defects to auto-RDI */
    if ((currentAutoRdiAlarms == 0) || (currentAutoRdiAlarms == autoRdiAlarms))
        return cAtOk;

    ret  = ThaPohProcessorAutoRdiEnable(self, currentAutoRdiAlarms, cAtFalse);
    ret |= ThaPohProcessorAutoRdiEnable(self, autoRdiAlarms, cAtTrue);
    return ret;
    }

ThaModuleOcn ThaPohProcessorModuleOcnGet(ThaPohProcessor self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

uint32 ThaPohProcessorDefaultAlarmsNeedAutoRdi(ThaPohProcessor self, eBool enable)
    {
    if (self)
        return DefaultAlarmsNeedAutoRdi((AtChannel)self, enable);
    return 0;
    }

void ThaPohProcessorCounterAccumulate(ThaPohProcessor self, uint16 counterType, uint32 value)
    {
    if (self)
        mMethodsGet(self)->CounterAccumulate(self, counterType, value);
    }

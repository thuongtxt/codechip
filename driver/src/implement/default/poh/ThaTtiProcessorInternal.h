/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaTtiProcessorInternal.h
 * 
 * Created Date: Mar 25, 2013
 *
 * Description : TTI processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATTIPROCESSORINTERNAL_H_
#define _THATTIPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/common/AtObjectInternal.h"

#include "../man/ThaDeviceInternal.h"

#include "ThaTtiProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaTtiProcessorMethods
    {
    eAtRet (*TxTtiGet)(ThaTtiProcessor self, tAtSdhTti *tti);
    eAtRet (*TxTtiSet)(ThaTtiProcessor self, const tAtSdhTti *tti);
    eAtRet (*ExpectedTtiGet)(ThaTtiProcessor self, tAtSdhTti *tti);
    eAtRet (*ExpectedTtiSet)(ThaTtiProcessor self, const tAtSdhTti *tti);
    eAtRet (*RxTtiGet)(ThaTtiProcessor self, tAtSdhTti *tti);
    eAtRet (*CompareEnable)(ThaTtiProcessor self, eBool enable);
    eBool (*CompareIsEnabled)(ThaTtiProcessor self);

    uint32 (*AlarmGet)(ThaTtiProcessor self);
    uint32 (*AlarmInterruptGet)(ThaTtiProcessor self);
    uint32 (*AlarmInterruptClear)(ThaTtiProcessor self);
    eAtRet (*TimAffectingEnable)(ThaTtiProcessor self, eBool enable);
    eBool (*TimAffectingIsEnabled)(ThaTtiProcessor self);
    eAtRet (*MonitorEnable)(ThaTtiProcessor self, eBool enable);
    eBool (*MonitorIsEnabled)(ThaTtiProcessor self);
    eBool (*HasAlarmForwarding)(ThaTtiProcessor self);

    eAtRet (*InterruptMaskSet)(ThaTtiProcessor self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(ThaTtiProcessor self);
    eAtRet (*WarmRestore)(ThaTtiProcessor self);

    eAtRet (*DefaultSet)(ThaTtiProcessor self);
    }tThaTtiProcessorMethods;

typedef struct tThaTtiProcessorSimulation
    {
    tAtSdhTti txTti;
    tAtSdhTti expectedTti;
    eBool ttiCompareEnable;
    }tThaTtiProcessorSimulation;

typedef struct tThaTtiProcessor
    {
    tAtObject super;
    const tThaTtiProcessorMethods *methods;

    /* Private data */
    AtSdhChannel sdhChannel;
    tThaTtiProcessorSimulation *simulation;
    }tThaTtiProcessor;

/*--------------------------- Forward declarations ---------------------------*/
extern eAtRet ThaTtiMsgGet(const tAtSdhTti *tti, uint8 *pTtiMsg, uint8 *pHwJ0Md, uint8 *pJ0Len);
extern eAtSdhTtiMode ThaDefaultPohProcessorV2TtiModeHw2Sw(uint8 value);

/*--------------------------- Entries ----------------------------------------*/
ThaTtiProcessor ThaTtiProcessorObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel);

/* Helper */
uint32 ThaTtiProcessorRead(ThaTtiProcessor self, uint32 address, eAtModule moduleId);
void ThaTtiProcessorWrite(ThaTtiProcessor self, uint32 address, uint32 value, eAtModule moduleId);

eAtRet ThaTtiProcessorLongRead(tAtSdhTti *tti, uint32 regAddr, AtChannel channel);
eAtRet ThaTtiProcessorLongWrite(const tAtSdhTti *tti, uint32 regAddr, AtChannel channel);

uint8 ThaTtiProcessorTtiModeSw2Hw(eAtSdhTtiMode value);
eAtSdhTtiMode ThaTtiProcessorTtiModeHw2Sw(uint32 value);

eBool ThaTtiProcessorIsSimulated(ThaTtiProcessor self);

#ifdef __cplusplus
}
#endif
#endif /* _THATTIPROCESSORINTERNAL_H_ */


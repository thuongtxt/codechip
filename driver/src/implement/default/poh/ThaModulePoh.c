/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH (internal)
 *
 * File        : ThaModulePoh.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : POH internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModulePohInternal.h"
#include "../man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePohMethods m_methods;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Override */
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
	AtUnused(line);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(vc);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(vc);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(vc);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static ThaModulePoh PohModuleFromChannel(AtChannel channel)
    {
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModulePoh);
    }

static void SdhChannelRegShow(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    /* Let concrete product do */
    AtUnused(self);
    AtUnused(sdhChannel);
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "poh";
    }

static eAtRet AllVtsPohMonEnable(ThaModulePoh self, AtSdhVc vc3, eBool enabled)
    {
    AtUnused(self);
    AtUnused(vc3);
    AtUnused(enabled);
    return cAtOk;
    }

static eAtRet LinePohMonEnable(ThaModulePoh self, AtSdhLine line, eBool enabled)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(enabled);
    return cAtOk;
    }

static uint32 StartVersionSupportPohMonitorEnabling(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool SoftStickyIsNeeded(ThaModulePoh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StspohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cInvalidUint32;
    }

static uint32 CpestsctrBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 CpeStsStatus(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 MsgstsexpBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 MsgstscurBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 MsgstsinsBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 IpmCnthiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 AlmMskhiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 AlmStahiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 AlmChghiBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 VtpohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cInvalidUint32;
    }

static uint32 MsgvtinsBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 IpmCntloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 AlmStaloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 AlmChgloBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 MsgvtcurBase(ThaModulePoh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 BaseAddress(ThaModulePoh self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, TypeString);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Additional methods setup go here */
        mMethodOverride(m_methods, LineTtiProcessorCreate);
        mMethodOverride(m_methods, AuVcPohProcessorCreate);
        mMethodOverride(m_methods, Tu3VcPohProcessorCreate);
        mMethodOverride(m_methods, Vc1xPohProcessorCreate);
        mMethodOverride(m_methods, SdhChannelRegShow);
        mMethodOverride(m_methods, AllVtsPohMonEnable);
        mMethodOverride(m_methods, LinePohMonEnable);
        mMethodOverride(m_methods, StartVersionSupportPohMonitorEnabling);
        mMethodOverride(m_methods, SoftStickyIsNeeded);
        mMethodOverride(m_methods, BaseAddress);

        /* Register base */
        mMethodOverride(m_methods, StspohgrbBase);
        mMethodOverride(m_methods, CpestsctrBase);
        mMethodOverride(m_methods, CpeStsStatus);
        mMethodOverride(m_methods, MsgstsexpBase);
        mMethodOverride(m_methods, MsgstscurBase);
        mMethodOverride(m_methods, MsgstsinsBase);
        mMethodOverride(m_methods, IpmCnthiBase);
        mMethodOverride(m_methods, AlmMskhiBase);
        mMethodOverride(m_methods, AlmStahiBase);
        mMethodOverride(m_methods, AlmChghiBase);
        mMethodOverride(m_methods, VtpohgrbBase);
        mMethodOverride(m_methods, MsgvtinsBase);
        mMethodOverride(m_methods, IpmCntloBase);
        mMethodOverride(m_methods, AlmStaloBase);
        mMethodOverride(m_methods, AlmChgloBase);
        mMethodOverride(m_methods, MsgvtcurBase);
        }

    mMethodsSet((ThaModulePoh)self, &m_methods);
    }

AtModule ThaModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModulePoh));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModulePoh, device) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Initialize additional methods */
    MethodsInit(self);
    m_methodsInit = 1;
    
    return self;
    }

ThaPohProcessor ThaModulePohAuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->AuVcPohProcessorCreate(self, vc);
    return NULL;
    }

ThaPohProcessor ThaModulePohTu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->Tu3VcPohProcessorCreate(self, vc);
    return NULL;
    }

ThaPohProcessor ThaModulePohVc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->Vc1xPohProcessorCreate(self, vc);
    return NULL;
    }

ThaTtiProcessor ThaModulePohLineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->LineTtiProcessorCreate(self, line);

    return NULL;
    }

void ThaModulePohSdhChannelRegShow(AtSdhChannel sdhChannel)
    {
    ThaModulePoh self = PohModuleFromChannel((AtChannel)sdhChannel);
    ThaModuleSdhStsMappingDisplay(sdhChannel);

    AtPrintc(cSevInfo, "* POH registers of channel '%s':\r\n", AtObjectToString((AtObject)sdhChannel));
    if (self)
        mMethodsGet(self)->SdhChannelRegShow(self, sdhChannel);
    }

eAtRet ThaModulePohAllVtsPohMonEnable(ThaModulePoh self, AtSdhVc vc3, eBool enabled)
    {
    if (ThaModulePohPathPohMonitorEnablingIsSupported(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->AllVtsPohMonEnable(self, vc3, enabled);
    }

eAtRet ThaModulePohLinePohMonEnable(ThaModulePoh self, AtSdhLine line, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->LinePohMonEnable(self, line, enabled);
    return cAtErrorNullPointer;
    }

eBool ThaModulePohPathPohMonitorEnablingIsSupported(ThaModulePoh self)
    {
    AtDevice device;

    if (self == NULL)
        return cAtFalse;

    device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    return (AtDeviceVersionNumber(device) >= mMethodsGet(self)->StartVersionSupportPohMonitorEnabling(self)) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePohSoftStickyIsNeeded(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->SoftStickyIsNeeded(self);
    return cAtFalse;
    }

uint32 ThaModulePohstspohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mMethodsGet(self)->StspohgrbBase(self, sdhChannel);
    return cInvalidUint32;
    }

uint32 ThaModulePohCpestsctrBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->CpestsctrBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohCpeStsStatus(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->CpeStsStatus(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohMsgstsexpBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->MsgstsexpBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohMsgstscurBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->MsgstscurBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohMsgstsinsBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->MsgstsinsBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohIpmCnthiBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->IpmCnthiBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohAlmMskhiBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->AlmMskhiBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohAlmStahiBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->AlmStahiBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohAlmChghiBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->AlmChghiBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohVtpohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel)
    {
    if (self)
        return mMethodsGet(self)->VtpohgrbBase(self, sdhChannel);
    return cInvalidUint32;
    }

uint32 ThaModulePohMsgvtinsBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->MsgvtinsBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohIpmCntloBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->IpmCntloBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohAlmStaloBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->AlmStaloBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohAlmChgloBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->AlmChgloBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohMsgvtcurBase(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->MsgvtcurBase(self);
    return cInvalidUint32;
    }

uint32 ThaModulePohBaseAddress(ThaModulePoh self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0;
    }

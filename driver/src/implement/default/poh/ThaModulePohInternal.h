/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH internal module
 * 
 * File        : ThaModulePohInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPOHINTERNAL_H_
#define _THAMODULEPOHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaModulePoh.h"
#include "AtSdhVc.h"
#include "../util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePohMethods
    {
    ThaTtiProcessor (*LineTtiProcessorCreate)(ThaModulePoh self, AtSdhLine line);
    ThaPohProcessor (*AuVcPohProcessorCreate)(ThaModulePoh self, AtSdhVc vc);
    ThaPohProcessor (*Tu3VcPohProcessorCreate)(ThaModulePoh self, AtSdhVc vc);
    ThaPohProcessor (*Vc1xPohProcessorCreate)(ThaModulePoh self, AtSdhVc vc);
    eAtRet (*AllVtsPohMonEnable)(ThaModulePoh self, AtSdhVc vc3, eBool enabled);
    eAtRet (*LinePohMonEnable)(ThaModulePoh self, AtSdhLine line, eBool enabled);
    uint32 (*StartVersionSupportPohMonitorEnabling)(ThaModulePoh self);
    void (*SdhChannelRegShow)(ThaModulePoh self, AtSdhChannel sdhChannel);
    eBool (*SoftStickyIsNeeded)(ThaModulePoh self);
    uint32 (*BaseAddress)(ThaModulePoh self);

    /* Register bases */
    uint32 (*StspohgrbBase)(ThaModulePoh self, AtSdhChannel sdhChannel);
    uint32 (*CpestsctrBase)(ThaModulePoh self);
    uint32 (*CpeStsStatus)(ThaModulePoh self);
    uint32 (*MsgstsexpBase)(ThaModulePoh self);
    uint32 (*MsgstscurBase)(ThaModulePoh self);
    uint32 (*MsgstsinsBase)(ThaModulePoh self);
    uint32 (*IpmCnthiBase)(ThaModulePoh self);
    uint32 (*AlmMskhiBase)(ThaModulePoh self);
    uint32 (*AlmStahiBase)(ThaModulePoh self);
    uint32 (*AlmChghiBase)(ThaModulePoh self);
    uint32 (*VtpohgrbBase)(ThaModulePoh self, AtSdhChannel sdhChannel);
    uint32 (*MsgvtinsBase)(ThaModulePoh self);
    uint32 (*IpmCntloBase)(ThaModulePoh self);
    uint32 (*AlmStaloBase)(ThaModulePoh self);
    uint32 (*AlmChgloBase)(ThaModulePoh self);
    uint32 (*MsgvtcurBase)(ThaModulePoh self);
    }tThaModulePohMethods;

typedef struct tThaModulePoh
    {
    tAtModule super;
    const tThaModulePohMethods *methods;

    }tThaModulePoh;

typedef struct tThaModulePohV2 * ThaModulePohV2;

typedef struct tThaModulePohV2Methods
    {
    eBool (*RaiseUNEQwhenRxAndExpectedPslAreZero)(ThaModulePohV2 self);
    uint8 (*NumSts)(ThaModulePohV2 self);
    eAtRet (*DefaultSet)(ThaModulePohV2 self);

    mDefineMaskShiftField(ThaModulePohV2, ThaPohIndirectAddress)
    }tThaModulePohV2Methods;

typedef struct tThaModulePohV2
    {
    tThaModulePoh super;
    const tThaModulePohV2Methods *methods;

    /* Private data */
    }tThaModulePohV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModulePohObjectInit(AtModule module, AtDevice device);
AtModule ThaModulePohV2ObjectInit(AtModule self, AtDevice device);

AtIterator ThaModulePohV2RegisterIteratorNew(AtModule module);
uint8 ThaModulePohV2NumSts(ThaModulePohV2 self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPOHINTERNAL_H_ */


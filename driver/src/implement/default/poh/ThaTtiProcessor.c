/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaTtiProcessor.c
 *
 * Created Date: Mar 25, 2013
 *
 * Description : TTI processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/common/AtChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../sdh/ThaModuleSdh.h"
#include "ThaTtiProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cJnMsgMask(byteIndex)  ((uint32) cBit7_0 << ((uint32)(cJnMsgShift(byteIndex))))
#define cJnMsgShift(byteIndex) (8 * (3 - (byteIndex)))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaTtiProcessorMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaTtiProcessor);
    }

static tThaTtiProcessorSimulation *SimulationDatabaseCreate(ThaTtiProcessor self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaTtiProcessorSimulation);
    tThaTtiProcessorSimulation *database = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    AtUnused(self);
    if (database == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, database, 0, memorySize);
    return database;
    }

static void SimulationDatabaseDelete(ThaTtiProcessor self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, self->simulation);
    self->simulation = NULL;
    }

static tThaTtiProcessorSimulation *SimulationDatabase(ThaTtiProcessor self)
    {
    if (self->simulation == NULL)
        self->simulation = SimulationDatabaseCreate(self);
    return self->simulation;
    }

static eAtRet TxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    AtOsal osal;

    if (!ThaTtiProcessorIsSimulated(self))
        return cAtError;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, tti, &(SimulationDatabase(self)->txTti), sizeof(tAtSdhTti));

    return cAtOk;
    }

static eAtRet TxTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    AtOsal osal;

    if (!ThaTtiProcessorIsSimulated(self))
        return cAtError;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, &(SimulationDatabase(self)->txTti), tti, sizeof(tAtSdhTti));

    return cAtOk;
    }

static eAtRet ExpectedTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    AtOsal osal;

    if (!ThaTtiProcessorIsSimulated(self))
        return cAtError;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, tti, &(SimulationDatabase(self)->expectedTti), sizeof(tAtSdhTti));

    return cAtOk;
    }

static eAtRet ExpectedTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    AtOsal osal;

    if (!ThaTtiProcessorIsSimulated(self))
        return cAtError;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, &(SimulationDatabase(self)->expectedTti), tti, sizeof(tAtSdhTti));

    return cAtOk;
    }

static eAtRet RxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    return cAtError;
    }

static eAtRet CompareEnable(ThaTtiProcessor self, eBool enable)
    {
    if (!ThaTtiProcessorIsSimulated(self))
        return cAtError;

    SimulationDatabase(self)->ttiCompareEnable = enable;

    return cAtOk;
    }

static eBool CompareIsEnabled(ThaTtiProcessor self)
    {
    if (!ThaTtiProcessorIsSimulated(self))
        return cAtError;

    return SimulationDatabase(self)->ttiCompareEnable;
    }

static uint32 AlarmGet(ThaTtiProcessor self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 AlarmInterruptGet(ThaTtiProcessor self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 AlarmInterruptClear(ThaTtiProcessor self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet TimAffectingEnable(ThaTtiProcessor self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtError;
    }

static eBool TimAffectingIsEnabled(ThaTtiProcessor self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet InterruptMaskSet(ThaTtiProcessor self, uint32 defectMask, uint32 enableMask)
    {
	AtUnused(enableMask);
	AtUnused(defectMask);
	AtUnused(self);
    return cAtError;
    }

static uint32 InterruptMaskGet(ThaTtiProcessor self)
    {
	AtUnused(self);
    return 0;
    }

static void Delete(AtObject self)
    {
    SimulationDatabaseDelete((ThaTtiProcessor)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaTtiProcessor object = (ThaTtiProcessor)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeChannelIdString(sdhChannel);
    mEncodeNone(simulation);
    }

static void OverrideAtObject(ThaTtiProcessor self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideAtObject(self);
    }

static eAtRet MonitorEnable(ThaTtiProcessor self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool MonitorIsEnabled(ThaTtiProcessor self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet WarmRestore(ThaTtiProcessor self)
    {
    /* Do nothing as default */
    AtUnused(self);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaTtiProcessor self)
    {
    /* Do nothing as default */
    AtUnused(self);
    return cAtOk;
    }

static eAtRet TtiBufferWrite(uint8 *msgBuffer, uint32 regAddr, AtChannel channel)
    {
    uint8 byte_i;
    uint8 byteIndexInDword;
    uint8 byteIndexInReg;
    eBool dwordIsFormed;
    eBool regContentIsFormed;
    uint8 endOfMessage;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 regIndex;
    uint8 dwordIndex = 1;
    uint8 numBytesInReg = cThaSonetNumBytesInDword * 2;
    uint8 *pCurrentByte = msgBuffer;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));

    for (byte_i = 0; byte_i < cAtSdhChannelMaxTtiLength; byte_i++)
        {
        byteIndexInDword = byte_i % cThaSonetNumBytesInDword;
        mFieldIns(&longRegVal[dwordIndex], cJnMsgMask(byteIndexInDword), cJnMsgShift(byteIndexInDword), *pCurrentByte);
        pCurrentByte++; /* Next byte */

        /* Change dword index when dword is formed */
        dwordIsFormed = (byteIndexInDword == (cThaSonetNumBytesInDword - 1));
        if (dwordIsFormed)
            dwordIndex = (dwordIndex == 0) ? 1 : 0;

        /* Only write hardware when register content is built or end of message */
        byteIndexInReg = byte_i % numBytesInReg;
        regContentIsFormed = (byteIndexInReg == (numBytesInReg - 1));
        endOfMessage = (byte_i == (cAtSdhChannelMaxTtiLength - 1));
        if ((!regContentIsFormed) && (!endOfMessage))
            continue;

        regIndex = byte_i / numBytesInReg;
        mChannelHwLongWrite(channel, regAddr + regIndex, longRegVal, cThaLongRegMaxSize, cThaModulePoh);
        AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
        }

    return cAtOk;
    }

static eBool HasAlarmForwarding(ThaTtiProcessor self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(ThaTtiProcessor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TxTtiGet);
        mMethodOverride(m_methods, TxTtiSet);
        mMethodOverride(m_methods, ExpectedTtiGet);
        mMethodOverride(m_methods, ExpectedTtiSet);
        mMethodOverride(m_methods, RxTtiGet);
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmInterruptGet);
        mMethodOverride(m_methods, AlarmInterruptClear);
        mMethodOverride(m_methods, TimAffectingEnable);
        mMethodOverride(m_methods, TimAffectingIsEnabled);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, MonitorEnable);
        mMethodOverride(m_methods, MonitorIsEnabled);
        mMethodOverride(m_methods, WarmRestore);
        mMethodOverride(m_methods, CompareEnable);
        mMethodOverride(m_methods, CompareIsEnabled);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, HasAlarmForwarding);
        }

    mMethodsSet(self, &m_methods);
    }

ThaTtiProcessor ThaTtiProcessorObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Setup private data */
    self->sdhChannel = sdhChannel;

    return self;
    }

uint32 ThaTtiProcessorRead(ThaTtiProcessor self, uint32 address, eAtModule moduleId)
    {
    if (self)
        return mChannelHwRead(ThaTtiProcessorChannelGet(self), address, moduleId);

    return 0;
    }

void ThaTtiProcessorWrite(ThaTtiProcessor self, uint32 address, uint32 value, eAtModule moduleId)
    {
    mChannelHwWrite(ThaTtiProcessorChannelGet(self), address, value, moduleId);
    }

eAtRet ThaTtiProcessorTxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    if (self)
        return mMethodsGet(self)->TxTtiGet(self, tti);

    return cAtErrorNullPointer;
    }

eAtRet ThaTtiProcessorTxTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    if (self)
        return mMethodsGet(self)->TxTtiSet(self, tti);

    return cAtErrorNullPointer;
    }

eAtRet ThaTtiProcessorExpectedTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    if (self)
        return mMethodsGet(self)->ExpectedTtiGet(self, tti);

    return cAtErrorNullPointer;
    }

eAtRet ThaTtiProcessorExpectedTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    if (self)
        return mMethodsGet(self)->ExpectedTtiSet(self, tti);

    return cAtErrorNullPointer;
    }

eAtRet ThaTtiProcessorRxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    if (self)
        return mMethodsGet(self)->RxTtiGet(self, tti);

    return cAtErrorNullPointer;
    }

AtSdhChannel ThaTtiProcessorChannelGet(ThaTtiProcessor self)
    {
    if (self)
        return self->sdhChannel;

    return NULL;
    }

uint32 ThaTtiProcessorAlarmGet(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->AlarmGet(self);

    return 0;
    }

uint32 ThaTtiProcessorAlarmInterruptGet(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->AlarmInterruptGet(self);

    return 0;
    }

uint32 ThaTtiProcessorAlarmInterruptClear(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->AlarmInterruptClear(self);

    return 0;
    }

eAtRet ThaTtiProcessorTimAffectingEnable(ThaTtiProcessor self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TimAffectingEnable(self, enable);

    return cAtErrorNullPointer;
    }

eBool ThaTtiProcessorTimAffectingIsEnabled(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->TimAffectingIsEnabled(self);

    return cAtFalse;
    }

eAtRet ThaTtiProcessorInterruptMaskSet(ThaTtiProcessor self, uint32 defectMask, uint32 enableMask)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaskSet(self, defectMask, enableMask);

    return cAtErrorNullPointer;
    }

uint32 ThaTtiProcessorInterruptMaskGet(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaskGet(self);

    return 0;
    }

eBool ThaTtiProcessorIsSimulated(ThaTtiProcessor self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet(self));
    return AtDeviceIsSimulated(device);
    }

eAtRet ThaTtiProcessorMonitorEnable(ThaTtiProcessor self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->MonitorEnable(self, enable);

    return cAtErrorNullPointer;
    }

eBool ThaTtiProcessorMonitorIsEnabled(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->MonitorIsEnabled(self);

    return cAtFalse;
    }

eAtRet ThaTtiProcessorCompareEnable(ThaTtiProcessor self, eBool enable)
    {
    return mMethodsGet(self)->CompareEnable(self, enable);
    }

eBool ThaTtiProcessorCompareIsEnabled(ThaTtiProcessor self)
    {
    return mMethodsGet(self)->CompareIsEnabled(self);
    }

uint32 ThaTtiProcessorPartOffset(ThaTtiProcessor self)
    {
    AtSdhChannel channel = ThaTtiProcessorChannelGet(self);
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)channel);
    return ThaDeviceModulePartOffset(device, cThaModulePoh, ThaModuleSdhPartOfChannel(channel));
    }

eAtRet ThaTtiProcessorWarmRestore(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->WarmRestore(self);

    return cAtErrorNullPointer;
    }

eAtRet ThaTtiProcessorLongWrite(const tAtSdhTti *tti, uint32 regAddr, AtChannel channel)
    {
    uint8 msgBuffer[cAtSdhChannelMaxTtiLength];
    eAtRet ret;
    uint8 i;
    uint8 msgLength;
    AtOsal osal = AtSharedDriverOsalGet();

    ret = ThaTtiMsgGet(tti, msgBuffer, NULL, &msgLength);
    if (ret != cAtOk)
        return ret;

    if (tti->mode == cAtSdhTtiMode1Byte)
        mMethodsGet(osal)->MemInit(osal, msgBuffer, tti->message[0], sizeof(msgBuffer));

    /* Make a 64-bytes buffer */
    if (tti->mode == cAtSdhTtiMode16Byte)
        {
        for (i = 1; i < (cAtSdhChannelMaxTtiLength / msgLength); i++)
            mMethodsGet(osal)->MemCpy(osal, &(msgBuffer[i * msgLength]), msgBuffer, msgLength);
        }

    return TtiBufferWrite(msgBuffer, regAddr, channel);
    }

eAtRet ThaTtiProcessorLongRead(tAtSdhTti *tti, uint32 regAddr, AtChannel channel)
    {
    uint8 byte_i;
    uint8 msgLength;
    uint8 byteIndexInDword;
    uint8 *pCurrentByte;
    eBool dwordIsFormed = 1;
    uint8 numBytesInReg = cThaSonetNumBytesInDword * 2;
    uint8 dwordIndex = 1;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 regIndex;
    uint8 position;

    pCurrentByte = tti->message;
    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));

    msgLength = AtSdhTtiLengthForTtiMode(tti->mode);
    for (byte_i = 0; byte_i < msgLength; byte_i++)
        {
        position = byte_i % numBytesInReg;

        /* Get 64-bits data from HW */
        if (position == 0)
            {
            regIndex = byte_i / numBytesInReg;
            mChannelHwLongRead(channel, regAddr + regIndex, longRegVal, cThaLongRegMaxSize, cThaModulePoh);
            }

        byteIndexInDword = byte_i % cThaSonetNumBytesInDword;

        /* If this is 16-byte message, save the first CRC byte */
        if ((tti->mode == cAtSdhTtiMode16Byte) && (byte_i == 0))
            {
            mFieldGet(longRegVal[dwordIndex], cJnMsgMask(byteIndexInDword), cJnMsgShift(byteIndexInDword), uint8, &(tti->crc));
            continue;
            }

        /* Put this byte to buffer */
        mFieldGet(longRegVal[dwordIndex], cJnMsgMask(byteIndexInDword), cJnMsgShift(byteIndexInDword), uint8, pCurrentByte);
        pCurrentByte++;

        dwordIsFormed = (byteIndexInDword == (cThaSonetNumBytesInDword - 1));
        if (dwordIsFormed)
            dwordIndex = (dwordIndex == 0) ? 1 : 0;
        }

    return cAtOk;
    }

uint8 ThaTtiProcessorTtiModeSw2Hw(eAtSdhTtiMode value)
    {
    if (value == cAtSdhTtiMode1Byte)
        return 0;
    if (value == cAtSdhTtiMode16Byte)
        return 1;
    if (value == cAtSdhTtiMode64Byte)
        return 2;
    return 0;
    }

eAtSdhTtiMode ThaTtiProcessorTtiModeHw2Sw(uint32 value)
    {
    if (value == 0)
        return cAtSdhTtiMode1Byte;
    if (value == 1)
        return cAtSdhTtiMode16Byte;
    if (value == 2)
        return cAtSdhTtiMode64Byte;
    return cAtSdhTtiMode1Byte;
    }

eAtRet ThaTtiProcessorDefaultSet(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->DefaultSet(self);
    return cAtErrorNullPointer;
    }

eBool ThaTtiProcessorHasAlarmForwarding(ThaTtiProcessor self)
    {
    if (self)
        return mMethodsGet(self)->HasAlarmForwarding(self);
    return cAtFalse;
    }

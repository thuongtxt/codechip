/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaPohProcessorInternal.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPOHPROCESSORINTERNAL_H_
#define _THAPOHPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/sdh/AtSdhPathInternal.h"
#include "../man/ThaDevice.h"
#include "../ocn/ThaModuleOcn.h"
#include "ThaPohProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* ERDI code */
#define cERDIPayloadValue      2
#define cERDIConnectivityValue 6
#define cERDIServerValue       5

/*--------------------------- Macros -----------------------------------------*/
#define mVcGet(self) ((AtSdhChannel)((ThaPohProcessor)self)->vc)
#define mModuleOcn(self) ThaPohProcessorModuleOcnGet((ThaPohProcessor)self)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPohProcessorCache
    {
    tAtSdhPathCache super;
    uint32 autoRdiAlarms;
    }tThaPohProcessorCache;

typedef struct tThaPohProcessorMethods
    {
    eAtRet (*VcAisAffectEnable)(ThaPohProcessor self, eBool enable);
    eAtRet (*AutoRdiEnable)(ThaPohProcessor self, uint32 alarmType, eBool enable);
    uint32 (*AutoRdiAlarms)(ThaPohProcessor self);
    eAtRet (*TxERdiEnable)(ThaPohProcessor self, eBool enable);
    eBool (*TxERdiIsEnabled)(ThaPohProcessor self);
    eAtRet (*ERdiMonitorEnable)(ThaPohProcessor self, eBool enable);
    eBool (*ERdiMonitorIsEnabled)(ThaPohProcessor self);
    eBool (*CanForceTxAlarms)(ThaPohProcessor self, uint32 alarmType);
    uint32 (*TxForceAbleErrors)(ThaPohProcessor self);
    void (*CounterAccumulate)(ThaPohProcessor self, uint16 counterType, uint32 value);
    }tThaPohProcessorMethods;

typedef struct tThaPohProcessorSimulation
    {
    tAtSdhTti txTti;
    tAtSdhTti expectedTti;
    eBool ttiCompareEnable;
    }tThaPohProcessorSimulation;

typedef struct tThaPohProcessor
    {
    tAtSdhPath super; /* To share the same interface */
    const tThaPohProcessorMethods* methods;

    /* Private data */
    AtSdhVc vc;
    uint32 partOffset;
    void *cache;
    tThaPohProcessorSimulation *simulation;
    }tThaPohProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor ThaPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc);
uint32 ThaPohProcessorPartOffset(ThaPohProcessor self);
ThaModuleOcn ThaPohProcessorModuleOcnGet(ThaPohProcessor self);

eBool ThaPohProcessorTimBackwardRdiShouldEnableWhenAisDownstream(ThaPohProcessor self, eBool timDownstreamAis);
eAtModuleSdhRet ThaPohProcessorTimAutoRdiUpdateWhenERdiEnable(ThaPohProcessor self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THAPOHPROCESSORINTERNAL_H_ */


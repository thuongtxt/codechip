/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaTtiProcessor.h
 * 
 * Created Date: Mar 25, 2013
 *
 * Description : TTI processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATTIPROCESSOR_H_
#define _THATTIPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Length of message in byte mode */
#define cThaOcnJnLenInByteMd            1

/* Length of message in 16 byte mode */
#define cThaOcnJnLenIn16ByteMd          16

/* Length of message in 64 byte mode */
#define cThaOcnJnLenIn64ByteMd          64

/* Number of Byte in Double Word */
#define cThaSonetNumBytesInDword        4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaTtiProcessor * ThaTtiProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* TTI APIs */
eAtRet ThaTtiProcessorTxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti);
eAtRet ThaTtiProcessorTxTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti);
eAtRet ThaTtiProcessorExpectedTtiGet(ThaTtiProcessor self, tAtSdhTti *tti);
eAtRet ThaTtiProcessorExpectedTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti);
eAtRet ThaTtiProcessorRxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti);

/* Alarm controlling */
uint32 ThaTtiProcessorAlarmGet(ThaTtiProcessor self);
uint32 ThaTtiProcessorAlarmInterruptGet(ThaTtiProcessor self);
uint32 ThaTtiProcessorAlarmInterruptClear(ThaTtiProcessor self);
eAtRet ThaTtiProcessorTimAffectingEnable(ThaTtiProcessor self, eBool enable);
eBool ThaTtiProcessorTimAffectingIsEnabled(ThaTtiProcessor self);
eAtRet ThaTtiProcessorInterruptMaskSet(ThaTtiProcessor self, uint32 defectMask, uint32 enableMask);
uint32 ThaTtiProcessorInterruptMaskGet(ThaTtiProcessor self);
eAtRet ThaTtiProcessorMonitorEnable(ThaTtiProcessor self, eBool enable);
eBool ThaTtiProcessorMonitorIsEnabled(ThaTtiProcessor self);
eAtRet ThaTtiProcessorCompareEnable(ThaTtiProcessor self, eBool enable);
eBool ThaTtiProcessorCompareIsEnabled(ThaTtiProcessor self);
eBool ThaTtiProcessorHasAlarmForwarding(ThaTtiProcessor self);

AtSdhChannel ThaTtiProcessorChannelGet(ThaTtiProcessor self);
uint32 ThaTtiProcessorPartOffset(ThaTtiProcessor self);
eAtRet ThaTtiProcessorWarmRestore(ThaTtiProcessor self);
eAtRet ThaTtiProcessorDefaultSet(ThaTtiProcessor self);

/* Default concretes */
ThaTtiProcessor ThaSdhLineTtiProcessorV1New(AtSdhChannel sdhChannel);
ThaTtiProcessor ThaSdhLineTtiProcessorV2New(AtSdhChannel sdhChannel);

/* Product concretes */
ThaTtiProcessor Tha60150011SdhLineTtiProcessorNew(AtSdhChannel sdhChannel);
ThaTtiProcessor Tha60290022FaceplateLineTtiProcessorNew(AtSdhChannel sdhChannel);
ThaTtiProcessor Tha60290081FaceplateLineTtiProcessorNew(AtSdhChannel sdhChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THATTIPROCESSOR_H_ */


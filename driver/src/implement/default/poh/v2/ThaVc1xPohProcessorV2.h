/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaVc1xPohProcessorV2.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : VC-1x POH processors
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVC1XPOHPROCESSORV2_H_
#define _THAVC1XPOHPROCESSORV2_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVc1xPohProcessorV2 * ThaVc1xPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAVC1XPOHPROCESSORV2_H_ */


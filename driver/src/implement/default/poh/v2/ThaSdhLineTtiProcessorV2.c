/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaSdhLineTtiProcessorV2.c
 *
 * Created Date: Mar 25, 2013
 *
 * Description : SDH Line TTI processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaTtiProcessorInternal.h"
#include "../../sdh/ThaModuleSdhReg.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../ThaModulePohInternal.h"
#include "ThaDefaultPohProcessorV2.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaSdhLineTtiProcessorV2Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaSdhLineTtiProcessorV2)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhLineTtiProcessorV2Methods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;
static tThaTtiProcessorMethods m_ThaTtiProcessorOverride;

/* Save super implementation */
static const tAtObjectMethods        *m_AtObjectMethods  = NULL;
static const tThaTtiProcessorMethods *m_ThaTtiProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePohV2 ModulePoh(ThaSdhLineTtiProcessorV2 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (ThaModulePohV2)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint8 StsMax(ThaSdhLineTtiProcessorV2 self)
    {
    return ThaModulePohV2NumSts(ModulePoh(self));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLineTtiProcessorV2);
    }

static uint8 HwLineIdGet(ThaSdhLineTtiProcessorV2 self)
    {
    return (uint8)ThaModuleSdhLineLocalId((AtSdhLine)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    }

static uint32 OcnJ0TxMsgBufDefaultOffset(ThaSdhLineTtiProcessorV2 self, uint8 bufId)
    {
    return (HwLineIdGet(self) * 16UL) + bufId + ThaTtiProcessorPartOffset((ThaTtiProcessor)self);
    }

static uint32 PohExpectedTtiAddressGet(ThaSdhLineTtiProcessorV2 self)
    {
    return (uint32)(0x0000 + mMethodsGet(self)->ExpTtiBufferRegOffset(self));
    }

static uint16 PohCapturedTtiBaseAddress(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return 0x2000;
    }

static uint32 PohCapturedTtiAddressGet(ThaSdhLineTtiProcessorV2 self)
    {
    uint8 STSMAX = StsMax(self);
    uint8 line = HwLineIdGet(self);
    uint32 captureBassAddr = mMethodsGet(self)->PohCapturedTtiBaseAddress(self);

    return (uint32)(captureBassAddr + (STSMAX * VTMAX * 16UL) + (STSMAX * 2UL * 16UL) + (line * 16UL) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self));
    }

/* Default implementation of register formula */
static uint32 DefaultOffset(ThaSdhLineTtiProcessorV2 self)
    {
    return HwLineIdGet(self) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self);
    }

static uint32 TxJ0MessageBufferRegAddress(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegOcnJ0MsgInsBuf;
    }

static eAtRet TxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    uint8    numByteInJ0Msg;
    uint8    byteId, position;
    uint8    *pMsg;
    uint32   address, regVal;
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;

    /* Get Tx TTI mode */
    tti->mode = ttiProcessor->ttiMode;
    numByteInJ0Msg = AtSdhTtiLengthForTtiMode(tti->mode);

    /* Get message */
    pMsg = tti->message;
    for (byteId = 0; byteId < numByteInJ0Msg; byteId++)
        {
        address = mMethodsGet(mThis(self))->TxJ0MessageBufferRegAddress(mThis(self)) +
                  mMethodsGet(ttiProcessor)->OcnJ0TxMsgBufDefaultOffset(ttiProcessor, byteId / cThaSonetNumBytesInDword);
        regVal  = ThaTtiProcessorRead(self, address, cThaModuleOcn);

        /* Ignore first CRC byte */
        if ((tti->mode == cAtSdhTtiMode16Byte) && (byteId == 0))
            {
            mFieldGet(regVal, cThaOcnTxJ0MsgMask(0), cThaOcnTxJ0MsgShift(0), uint8, &(tti->crc));
            continue;
            }

        /* Position of 1 character(1 byte) in 1 Dword register */
        position = byteId % cThaSonetNumBytesInDword;
        mFieldGet(regVal, cThaOcnTxJ0MsgMask(position), cThaOcnTxJ0MsgShift(position), uint8, pMsg);
        pMsg++;
        }

    return cAtOk;
    }

static void TxTtiWrite(ThaTtiProcessor self, uint8 start, uint8 *msgBuf, uint8 msgLen)
    {
    uint8 i, position, dwIndex;
    uint32 address, regVal;
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;

    position = 0;
    dwIndex  = 0;
    regVal   = 0;

    for (i = 0; i < msgLen; i++)
        {
        /* Build dword */
        mFieldIns(&regVal, cThaOcnTxJ0MsgMask(position), cThaOcnTxJ0MsgShift(position), msgBuf[i]);
        position++;

        if (msgLen == cThaOcnJnLenInByteMd)
            {
            mFieldIns(&regVal, cThaOcnTxJ0MsgMask(position), cThaOcnTxJ0MsgShift(position), msgBuf[i]); position++;
            mFieldIns(&regVal, cThaOcnTxJ0MsgMask(position), cThaOcnTxJ0MsgShift(position), msgBuf[i]); position++;
            mFieldIns(&regVal, cThaOcnTxJ0MsgMask(position), cThaOcnTxJ0MsgShift(position), msgBuf[i]); position++;
            }

        /* Write to HW after build 4 byte */
        if (position == cThaSonetNumBytesInDword)
            {
            address = mMethodsGet(mThis(self))->TxJ0MessageBufferRegAddress(mThis(self)) +
                      mMethodsGet(ttiProcessor)->OcnJ0TxMsgBufDefaultOffset(ttiProcessor, (uint8)(((start * msgLen) / 4) + dwIndex++));
            ThaTtiProcessorWrite(self, address, regVal, cThaModuleOcn);

            /* Reset reg val for next 4 bytes */
            regVal   = 0;
            position = 0;
            }
        }
    }

static eAtRet TxTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    uint8 i;
    uint8 hwJ0Md, msgLen;
    uint8 msgBuf[cAtSdhChannelMaxTtiLength];
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;
    AtOsal osal;

    /* Initialize message buffer */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, msgBuf, 0, sizeof(msgBuf));

    /* Store data to database */
    ttiProcessor->ttiMode = tti->mode;

    /* Get Tx TTI message */
    ThaTtiMsgGet(tti, msgBuf, &hwJ0Md, &msgLen);
    for (i = 0; i < (64 / msgLen); i++)
        TxTtiWrite(self, i, msgBuf, msgLen);

    return cAtOk;
    }

static eAtSdhTtiMode TtiModeGet(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 address, regVal;
    uint8 j0MsgHwMode;

    address = mMethodsGet(self)->PohJ0Control(self) + mMethodsGet(self)->DefaultOffset(self);
    regVal = ThaTtiProcessorRead((ThaTtiProcessor)self, address, cThaModulePoh);
    mFieldGet(regVal, cThaJ0FrmMdMask, cThaJ0FrmMdShift, uint8, &j0MsgHwMode);
    return ThaDefaultPohProcessorV2TtiModeHw2Sw(j0MsgHwMode);
    }

static eAtRet ExpectedTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    uint32 indirectAddr;

    if (ThaTtiProcessorIsSimulated(self))
        return m_ThaTtiProcessorMethods->ExpectedTtiGet(self, tti);

    /* Get message mode */
    tti->mode = TtiModeGet((ThaSdhLineTtiProcessorV2)self);

    /* Get expected message */
    indirectAddr = mMethodsGet((ThaSdhLineTtiProcessorV2)self)->PohExpectedTtiAddressGet((ThaSdhLineTtiProcessorV2)self);
    return ThaSdhTtiRead(ThaTtiProcessorChannelGet(self), tti, indirectAddr);
    }

static eAtRet ExpectedTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    uint8 hwJ0Md;
    uint32 regVal, address;
    uint32 indirectAddr;
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;

    if (ThaTtiProcessorIsSimulated(self))
        return m_ThaTtiProcessorMethods->ExpectedTtiSet(self, tti);

    /* Initialize memory */
    ThaTtiMsgGet(tti, NULL, &hwJ0Md, NULL);

    /* Write TTI message mode to hardware */
    address = mMethodsGet(ttiProcessor)->PohJ0Control(ttiProcessor) +
              mMethodsGet(ttiProcessor)->DefaultOffset(ttiProcessor);
    regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaJ0FrmMdMask, cThaJ0FrmMdShift, hwJ0Md);
    mFieldIns(&regVal, cThaJ0TimMonEnableMask, cThaJ0TimMonEnableShift, 1);
    mFieldIns(&regVal, cJ0TimL2AisEnbMask, cJ0TimL2AisEnbShift, 1);
    ThaTtiProcessorWrite(self, address, regVal, cThaModulePoh);

    /* Write message to HW */
    indirectAddr = mMethodsGet(ttiProcessor)->PohExpectedTtiAddressGet(ttiProcessor);
    return ThaSdhTtiWrite(ThaTtiProcessorChannelGet(self), tti, indirectAddr);
    }

static eAtRet RxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    uint32 indirectAddr;
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;

    tti->mode = TtiModeGet(ttiProcessor);

    /* Get captured message */
    indirectAddr = mMethodsGet(ttiProcessor)->PohCapturedTtiAddressGet(ttiProcessor);

    return ThaSdhTtiRead(ThaTtiProcessorChannelGet(self), tti, indirectAddr);
    }

static uint32 PohJ0StiMsgAlarmStatus(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cThaRegPohJ0StiMsgAlarmStatus;
    }

static uint32 PohJ0Control(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cThaRegPohSdhSonetCtrl;
    }

static uint32 AlarmGet(ThaTtiProcessor self)
    {
    uint32 address, regVal;
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;

    address = mMethodsGet(ttiProcessor)->PohJ0StiMsgAlarmStatus(ttiProcessor);
    address = address + mMethodsGet(ttiProcessor)->DefaultOffset(ttiProcessor);
    regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);

    return (regVal & cThaRegPohJ0StiTimMask) ? cAtSdhLineAlarmTim : 0;
    }

static uint32 PohJ0StiMsgAlarmIntrSticky(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cThaRegPohJ0StiMsgAlarmIntrSticky;
    }

static uint32 AlarmInterruptRead2Clear(ThaTtiProcessor self, eBool read2Clear)
    {
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;
    uint32 regVal, alarm;
    uint32 address = mMethodsGet(ttiProcessor)->PohJ0StiMsgAlarmIntrSticky(ttiProcessor);

    address = address + mMethodsGet(ttiProcessor)->DefaultOffset(ttiProcessor);
    regVal  = ThaTtiProcessorRead(self, address, cThaModulePoh);
    alarm   = (regVal & cThaRegPohJ0StiTimStbChgIntrMask) ? cAtSdhLineAlarmTim : 0;

    if (read2Clear)
        ThaTtiProcessorWrite(self, address, cThaRegPohJ0StiTimStbChgIntrMask, cThaModulePoh);

    return alarm;
    }

static uint32 AlarmInterruptGet(ThaTtiProcessor self)
    {
    return AlarmInterruptRead2Clear(self, cAtFalse);
    }

static uint32 AlarmInterruptClear(ThaTtiProcessor self)
    {
    return AlarmInterruptRead2Clear(self, cAtTrue);
    }

static eAtRet TimAffectingEnable(ThaTtiProcessor self, eBool enable)
    {
    uint32 regVal, address;
    uint8 hwLineId;

    hwLineId = HwLineIdGet((ThaSdhLineTtiProcessorV2)self);
    address = cThaRegOcnFrmrAlmTimLAffCtrl + ThaTtiProcessorPartOffset(self);
    regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaTimLAffAisLRdiLEnMask(hwLineId), cThaTimLAffAisLRdiLEnShift(hwLineId), enable ? 0x1 : 0x0);
    ThaTtiProcessorWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool TimAffectingIsEnabled(ThaTtiProcessor self)
    {
    uint32 regVal, address;
    uint8 hwLineId;

    hwLineId = HwLineIdGet((ThaSdhLineTtiProcessorV2)self);
    address = cThaRegOcnFrmrAlmTimLAffCtrl + ThaTtiProcessorPartOffset(self);
    regVal  = ThaTtiProcessorRead(self, address, cThaModulePoh);
    return (regVal & cThaTimLAffAisLRdiLEnMask(hwLineId)) ? cAtTrue : cAtFalse;
    }

static uint32 PohJ0StiMsgAlarmIntrEnable(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cThaRegPohJ0StiMsgAlarmIntrEnable;
    }

static eAtRet InterruptMaskSet(ThaTtiProcessor self, uint32 defectMask, uint32 enableMask)
    {
    uint32 address, regVal;
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;

    if ((defectMask & cAtSdhLineAlarmTim) == 0)
        return cAtOk;

    address = mMethodsGet(ttiProcessor)->PohJ0StiMsgAlarmIntrEnable(ttiProcessor);
    address = address + mMethodsGet(ttiProcessor)->DefaultOffset(ttiProcessor);
    regVal = ThaTtiProcessorRead(self, address, cThaModulePoh);
    mFieldIns(&regVal,
              cThaRegPohJ0StiTimStbChgIntrEnMask,
              cThaRegPohJ0StiTimStbChgIntrEnShift,
              (enableMask & cAtSdhLineAlarmTim) ? 1 : 0);
    ThaTtiProcessorWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(ThaTtiProcessor self)
    {
    uint32 address, regVal;
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;

    address = mMethodsGet(ttiProcessor)->PohJ0StiMsgAlarmIntrEnable(ttiProcessor);
    address = address + mMethodsGet(ttiProcessor)->DefaultOffset(ttiProcessor);

    regVal = ThaTtiProcessorRead(self, address, cThaModuleOcn);
    return (regVal & cThaRegPohJ0StiTimStbChgIntrEnMask) ? cAtSdhLineAlarmTim : 0;
    }

static eAtRet MonitorEnable(ThaTtiProcessor self, eBool enable)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->PohJ0Control(mThis(self)) +
                     mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cThaJ0TimMonEnable, enable ? 1 : 0);
    ThaTtiProcessorWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool MonitorIsEnabled(ThaTtiProcessor self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->PohJ0Control(mThis(self)) +
                     mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);

    return (regVal & cThaJ0TimMonEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxTtiModeWarmRestore(ThaTtiProcessor self)
    {
    eAtRet ret;
    tAtSdhTti tti;
    ThaSdhLineTtiProcessorV2 ttiProcessor = mThis(self);

    /* Get full 64 bytes */
    ttiProcessor->ttiMode = cAtSdhTtiMode64Byte;
    ret = ThaTtiProcessorTxTtiGet((ThaTtiProcessor)self, &tti);
    if (ret != cAtOk)
        return ret;

    /* Analyze real mode and correct database */
    ret = ThaSdhTtiFloatBufferAnalyze(&tti);
    if (ret == cAtOk)
        ttiProcessor->ttiMode = tti.mode;
    else
        ttiProcessor->ttiMode = cAtSdhTtiMode1Byte;

    return cAtOk;
    }

static eAtRet WarmRestore(ThaTtiProcessor self)
    {
    return TxTtiModeWarmRestore(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaSdhLineTtiProcessorV2 object = (ThaSdhLineTtiProcessorV2)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(ttiMode);
    }

static uint32 ExpTtiBufferRegOffset(ThaSdhLineTtiProcessorV2 self)
    {
    uint8 STSMAX = StsMax(self);
    uint8 line = HwLineIdGet(self);
    return (STSMAX * VTMAX * 16UL) + (STSMAX * 2UL * 16UL) + (line * 16UL) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self);
    }

static uint32 PohJ0Status(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PohJ0StatusOffset(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PohJ0Grabber(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PohJ0GrabberOffset(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void OverrideAtObject(ThaTtiProcessor self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaTtiProcessor(ThaTtiProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaTtiProcessorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaTtiProcessorOverride, m_ThaTtiProcessorMethods, sizeof(m_ThaTtiProcessorOverride));

        mMethodOverride(m_ThaTtiProcessorOverride, TxTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, TxTtiSet);
        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiSet);
        mMethodOverride(m_ThaTtiProcessorOverride, RxTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, MonitorEnable);
        mMethodOverride(m_ThaTtiProcessorOverride, MonitorIsEnabled);

        mMethodOverride(m_ThaTtiProcessorOverride, AlarmGet);
        mMethodOverride(m_ThaTtiProcessorOverride, AlarmInterruptGet);
        mMethodOverride(m_ThaTtiProcessorOverride, AlarmInterruptClear);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingEnable);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingIsEnabled);
        mMethodOverride(m_ThaTtiProcessorOverride, InterruptMaskSet);
        mMethodOverride(m_ThaTtiProcessorOverride, InterruptMaskGet);
        mMethodOverride(m_ThaTtiProcessorOverride, WarmRestore);
        }

    mMethodsSet(self, &m_ThaTtiProcessorOverride);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideAtObject(self);
    OverrideThaTtiProcessor(self);
    }

static void MethodsInit(ThaSdhLineTtiProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, OcnJ0TxMsgBufDefaultOffset);
        mMethodOverride(m_methods, PohExpectedTtiAddressGet);
        mMethodOverride(m_methods, PohCapturedTtiAddressGet);
        mMethodOverride(m_methods, PohJ0StiMsgAlarmStatus);
        mMethodOverride(m_methods, PohJ0StiMsgAlarmIntrSticky);
        mMethodOverride(m_methods, PohJ0StiMsgAlarmIntrEnable);
        mMethodOverride(m_methods, PohJ0Control);
        mMethodOverride(m_methods, PohCapturedTtiBaseAddress);
        mMethodOverride(m_methods, TxJ0MessageBufferRegAddress);
        mMethodOverride(m_methods, ExpTtiBufferRegOffset);
        mMethodOverride(m_methods, PohJ0Status);
        mMethodOverride(m_methods, PohJ0StatusOffset);
        mMethodOverride(m_methods, PohJ0Grabber);
        mMethodOverride(m_methods, PohJ0GrabberOffset);
        }

    mMethodsSet(self, &m_methods);
    }

ThaTtiProcessor ThaSdhLineTtiProcessorV2ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaTtiProcessorObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaSdhLineTtiProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor ThaSdhLineTtiProcessorV2New(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ThaSdhLineTtiProcessorV2ObjectInit(newProcessor, sdhChannel);
    }

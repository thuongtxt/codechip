/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaDefaultPohProcessorV2.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : Default POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEFAULTPOHPROCESSORV2_H_
#define _THADEFAULTPOHPROCESSORV2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaPohProcessor.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDefaultPohProcessorV2 * ThaDefaultPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Util functions to read/write TTI message */
eAtRet ThaSdhTtiRead(AtSdhChannel self, tAtSdhTti *tti, uint32 indirectAddr);
eAtRet ThaSdhTtiWrite(AtSdhChannel sdhChannel, const tAtSdhTti *tti, uint32 indirectAddr);
eAtRet ThaSdhTtiFloatBufferAnalyze(tAtSdhTti *tti);

#ifdef __cplusplus
}
#endif
#endif /* _THADEFAULTPOHPROCESSORV2_H_ */


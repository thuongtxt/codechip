/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaAuVcPohProcessorV2.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : AU's VC POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAAUVCPOHPROCESSORV2_H_
#define _THAAUVCPOHPROCESSORV2_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAuVcPohProcessorV2 * ThaAuVcPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAAUVCPOHPROCESSORV2_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaTu3VcPohProcessorV2.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : TU-3's VC POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATU3VCPOHPROCESSORV2_H_
#define _THATU3VCPOHPROCESSORV2_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaTu3VcPohProcessorV2 * ThaTu3VcPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THATU3VCPOHPROCESSORV2_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaVc1xPohProcessorV2.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : VC-1x POH processors
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaVc1xPohProcessorV2Internal.h"

/*--------------------------- Define -----------------------------------------*/
#define cVslBitMask  cBit3_1
#define cVslBitShift 1

#define cERdiK4ByteMask  cBit3_1
#define cERdiK4ByteShift 1

#define cRdiV5ByteMask  cBit0
#define cRdiV5ByteShift 0

/*--------------------------- Macros -----------------------------------------*/
#define IsOneBitRdiVc1x(v5, z7) ((v5 == 1) ? cAtTrue : cAtFalse)
#define mThis(self) (ThaVc1xPohProcessorV2)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVc1xPohProcessorV2Methods m_methods;

/* Override */
static tAtObjectMethods                 m_AtObjectOverride;
static tAtSdhChannelMethods             m_AtSdhChannelOverride;
static tAtChannelMethods                m_AtChannelOverride;
static tAtSdhPathMethods                m_AtSdhPathOverride;
static tThaPohProcessorMethods          m_ThaPohProcessorOverride;
static tThaDefaultPohProcessorV2Methods m_ThaDefaultPohProcessorV2Override;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaVc1xPohProcessorV2);
    }

static uint32 PohCpeVtCtrl(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPohRegCpeVtControl;
    }

static eAtModuleSdhRet ERdiEnable(AtSdhPath self, eBool enable)
    {
    ThaVc1xPohProcessorV2 vc1x = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(vc1x)->PohCpeVtCtrl(vc1x) +
                     mMethodsGet(vc1x)->PohCpeVtControlDefaultOffset(vc1x);

    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaPohVtERdiInsEnbMask, cThaPohVtERdiInsEnbShift, enable);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return ThaPohProcessorTimAutoRdiUpdateWhenERdiEnable((ThaPohProcessor)self, enable);
    }

static eBool ERdiIsEnabled(AtSdhPath self)
    {
    uint32 regVal, address, offset;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;

    offset  = mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    address = mMethodsGet(processor)->PohCpeVtCtrl(processor) + offset;
    regVal  = mChannelHwRead(self, address, cThaModulePoh);
    return (regVal & cThaPohVtERdiInsEnbMask) ? cAtTrue : cAtFalse;
    }

static eAtRet VcAisAffectEnable(ThaPohProcessor self, eBool enable)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);

    mFieldIns(&regVal, cThaPohVtVcAis2AisEnbMask, cThaPohVtVcAis2AisEnbShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    uint32 regVal;
    eBool hwEnable = enable ? 1 : 0;
    ThaPohProcessor pohProcessor = (ThaPohProcessor)self;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    if (!ThaPohProcessorCanEnableAlarmAffecting(pohProcessor, alarmType, enable))
        return cAtErrorModeNotSupport;

    regVal  = mChannelHwRead(self, address, cThaModulePoh);
    if (alarmType & cAtSdhPathAlarmTim)
        mFieldIns(&regVal, cThaPohVtTim2AisEnbMask, cThaPohVtTim2AisEnbShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal, cThaPohVtPlm2AisEnbMask, cThaPohVtPlm2AisEnbShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal, cThaPohVtUneq2AisEnbMask, cThaPohVtUneq2AisEnbShift, hwEnable);

    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    if (alarmType & cAtSdhPathAlarmTim)
        return ThaPohProcessorAutoRdiEnable(pohProcessor, cAtSdhPathAlarmTim, ThaPohProcessorTimBackwardRdiShouldEnableWhenAisDownstream(pohProcessor, enable));

    return cAtOk;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);
    uint32 mask = (cAtSdhPathAlarmAis | cAtSdhPathAlarmLop);

    if (regVal & cThaPohVtTim2AisEnbMask)
        mask |= cAtSdhPathAlarmTim;

    if (regVal & cThaPohVtPlm2AisEnbMask)
        mask |= cAtSdhPathAlarmPlm;

    if (regVal & cThaPohVtUneq2AisEnbMask)
        mask |= cAtSdhPathAlarmUneq;

    return mask;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);

    if ((alarmType & cAtSdhPathAlarmAis) || (alarmType & cAtSdhPathAlarmLop))
        return cAtTrue;

    if (alarmType & cAtSdhPathAlarmTim)
        return (regVal & cThaPohVtTim2AisEnbMask) ? cAtTrue : cAtFalse;

    if (alarmType & cAtSdhPathAlarmPlm)
        return (regVal & cThaPohVtPlm2AisEnbMask) ? cAtTrue : cAtFalse;

    if (alarmType & cAtSdhPathAlarmUneq)
        return (regVal & cThaPohVtUneq2AisEnbMask) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

/* Hardware returns value with format V5[b8],K4[b5:b7]. Where K4 is
 * corresponding with Z7. Refer GR 253, Table 6-5.  RDI-V Bit Settings and
 * Interpretation to understand how ERDI-V is calculated. Pseudocode is:
 * if v5[8] is 1
 *     if z7[5:7] is 6
 *         return ERDI Connectivity defect;
 *     else
 *         return ERDI Server defect;
 *
 * else
 *     if z7[5:7] is 2
 *         return ERDI Payload;
 *
 *
 *
Table VII.4/G.707/Y.1322 V5 b8 and K4 (b5-b7) coding and E-RDI interpretation
V5 b8    |K4 b5   |K4 b6    |K4 b7  |E-RDI Interpretation
--------------------------------------------------------------------------------------------
0        |0       |0        |0      |No remote defect (Note 1)
0        |0       |0        |1      |No remote defect
0        |0       |1        |0      |E-RDI Payload defect
0        |0       |1        |1      |No remote defect (Note 2)
0        |1       |0        |0      |No remote defect (Note 2)
0        |1       |0        |1      |No remote defect (Note 2)
0        |1       |1        |0      |No remote defect (Note 2)
0        |1       |1        |1      |No remote defect (Note 1)
1        |0       |0        |0      |E-RDI Server defect (Note 1)
1        |0       |0        |1      |E-RDI Server defect (Note 2)
1        |0       |1        |0      |E-RDI Server defect (Note 2)
1        |0       |1        |1      |E-RDI Server defect (Note 2)
1        |1       |0        |0      |E-RDI Server defect (Note 2)
1        |1       |0        |1      |E-RDI Server defect
1        |1       |1        |0      |E-RDI Connectivity defect
1        |1       |1        |1      |E-RDI Server defect (Note 1)
----------------------------------------------------------------------------------------------
NOTE 1 These codes are generated by RDI supporting equipment and are interpreted by E-RDI
supporting equipment as shown. For equipment supporting RDI (see 9.3.2.1) this code is triggered by
the presence or absence of one of the following defects: AIS, LOP, TIM, or UNEQ. Equipment
conforming to an earlier version of this Recommendation may include PLM as a trigger condition.
Note, for some national networks, this code was triggered only by an AIS or LOP defect.
NOTE 2 This code is not applicable to any known standards; it is included here for completeness.
----------------------------------------------------------------------------------------------*/
static uint32 ErdiTypeFromErdiValue(uint8 v5, uint8 z7, eBool erdiIsEnable)
    {
    v5 = (v5 & cBit0) ? 1 : 0;
    z7 = (uint8)((z7 & cBit3_1) >> 1);

    if (!erdiIsEnable)
        return IsOneBitRdiVc1x(v5, z7) ? cAtSdhPathAlarmRdi : 0;

    if (v5 == 1)
        return (z7 == 6) ? cAtSdhPathAlarmErdiC : cAtSdhPathAlarmErdiS;

    /* V5 == 0 */
    if (z7 == 2)
        return cAtSdhPathAlarmErdiP;

    return 0;
    }

static uint32 PohCpeVtPathOhGrabber1(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohSpeVtPathOhGrabber1;
    }

static uint32 Grabber1Read(AtSdhPath self)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtPathOhGrabber1(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    return mChannelHwRead(self, address, cThaModulePoh);
    }

static uint8 RxK4Get(AtSdhPath self)
    {
    return (uint8)mRegField(Grabber1Read(self), cThaRegPohCpeVtPathK4Grabber);
    }

static uint8 RxV5Get(AtSdhPath self)
    {
    return (uint8)mRegField(Grabber1Read(self), cThaRegPohCpeVtPathV5Grabber);
    }

static uint8 RxN2Get(AtSdhPath self)
    {
    return (uint8)mRegField(Grabber1Read(self), cThaRegPohCpeVtPathN2Grabber);
    }

static uint32 PohRegVtAlarmStatus(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohSpeVtAlarmStatus;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 alarmState = 0;
    AtBerController berController;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohRegVtAlarmStatus(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    /* Get UNEQ-P/V, PLM-P/V, TIM-P/V */
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Convert to SW alarm */
    if (regVal & cThaPohVtAlarmUneqMask)
        alarmState |= cAtSdhPathAlarmUneq;
    if (regVal & cThaPohVtAlarmTtiTimMask)
        alarmState |= cAtSdhPathAlarmTim;
    if (regVal & cThaPohVtAlarmPlmMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohVtAlarmVcAisMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohVtAlarmRfiStbMask)
        alarmState |= (RxV5Get((AtSdhPath)self) & cBit4) ? cAtSdhPathAlarmRfi : 0;

    /* SD SF defect */
    berController = SdhChannelBerControllerGet(mVcGet(self));
    if (AtBerControllerIsSd(berController))
        alarmState |= cAtSdhPathAlarmBerSd;
    if (AtBerControllerIsSf(berController))
        alarmState |= cAtSdhPathAlarmBerSf;
    if (AtBerControllerIsTca(berController))
        alarmState |= cAtSdhPathAlarmBerTca;

    /* RDI status */
    alarmState |= ErdiTypeFromErdiValue(RxV5Get((AtSdhPath)self),
                                        RxK4Get((AtSdhPath)self),
                                        AtSdhPathERdiIsEnabled((AtSdhPath)self));

    /* Payload UNEQ */
    address = cThaRegOcnRxVtTUperAlmCurrentStat + mMethodsGet(processor)->OcnDefaultOffset(processor);
    if (mChannelHwRead(self, address, cThaModuleOcn) & cThaVtPiCepUneqCurStatMask)
        alarmState |= cAtSdhPathAlarmPayloadUneq;

    return alarmState;
    }

static uint32 PohRegVtAlarmIntrSticky(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohCpeVtAlarmIntrSticky;
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 alarmState = 0;
    AtBerController berController;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;

    uint32 address = mMethodsGet(processor)->PohRegVtAlarmIntrSticky(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    /* Read hardware status */
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);
    if (regVal & cThaPohVtTtiTimStbChgIntMask)
        alarmState |= cAtSdhPathAlarmTim;
    if (regVal & cThaPohVtPlmStbChgIntMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohVtAlarmVcAisMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohVtAlarmUneqMask)
        alarmState |= cAtSdhPathAlarmUneq;
    if (regVal & cThaPohVtAlarmRfiStbMask)
        alarmState |= cAtSdhPathAlarmRfi;
    if (regVal & cThaPohVtAlarmRdiStbMask)
        alarmState |= cAtSdhPathAlarmRdi;

    if (read2Clear)
        mChannelHwWrite(self, address, regVal, cThaModulePoh);

    /* Get SD, SF */
    berController = SdhChannelBerControllerGet(mVcGet(self));
    if (AtBerControllerSdHistoryGet(berController, read2Clear))
       alarmState |= cAtSdhPathAlarmBerSd;
    if (AtBerControllerSfHistoryGet(berController, read2Clear))
       alarmState |= cAtSdhPathAlarmBerSf;

    /* Payload UNEQ */
    address = cThaRegOcnRxVtTUperAlmIntrStat + mMethodsGet(processor)->OcnDefaultOffset(processor);
    if (mChannelHwRead(self, address, cThaModuleOcn) & cThaVtPiCepUneqVStateChgIntrMask)
        alarmState |= cAtSdhPathAlarmPayloadUneq;
    if (read2Clear)
        mChannelHwWrite(self, address, cThaVtPiCepUneqVStateChgIntrMask, cThaModuleOcn);

    return alarmState;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 PohRegVtAlarmIntrEnable(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohCpeVtAlarmIntrEnable;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtSdhPathAlarmAis   |
            cAtSdhPathAlarmTim   |
            cAtSdhPathAlarmPlm   |
            cAtSdhPathAlarmUneq  |
            cAtSdhPathAlarmRfi   |
            cAtSdhPathAlarmRdi   |
            cAtSdhPathAlarmErdiS |
            cAtSdhPathAlarmErdiP |
            cAtSdhPathAlarmErdiC);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohRegVtAlarmIntrEnable(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    /* Get current configuration */
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Configure AIS mask */
    if (defectMask & cAtSdhPathAlarmAis)
        mFieldIns(&regVal,
                  cThaPohVtVcAisStbChgIntEnbMask,
                  cThaPohVtVcAisStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);

    /* Configure TTI mask */
    if (defectMask & cAtSdhPathAlarmTim)
        mFieldIns(&regVal,
                  cThaPohVtTtiTimStbChgIntEnbMask,
                  cThaPohVtTtiTimStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmTim) ? 1 : 0);

    /* Configure PLM mask */
    if (defectMask & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal,
                  cThaPohVtPlmStbChgIntEnbMask,
                  cThaPohVtPlmStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmPlm) ? 1 : 0);

    /* Configure UNEQ mask */
    if (defectMask & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal,
                  cThaPohVtUneqStbChgIntEnbMask,
                  cThaPohVtUneqStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmUneq) ? 1 : 0);

    /* Configure RDI mask */
    if (defectMask & (cAtSdhPathAlarmRdi | cAtSdhPathAlarmErdiS | cAtSdhPathAlarmErdiP | cAtSdhPathAlarmErdiC))
        mFieldIns(&regVal,
                  cThaPohVtRdiStbChgIntEnbMask,
                  cThaPohVtRdiStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmRdi) ? 1 : 0);

    /* Configure RFI mask */
    if (defectMask & cAtSdhPathAlarmRfi)
        mFieldIns(&regVal,
                  cThaPohVtRfiStbChgIntEnbMask,
                  cThaPohVtRfiStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmRfi) ? 1 : 0);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 swMask = 0;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohRegVtAlarmIntrEnable(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    /* Get current configuration */
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Get AIS mask */
    if (regVal & cThaPohVtVcAisStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmAis;

    /* Get TTI mask */
    if (regVal & cThaPohVtTtiTimStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmTim;

    /* Get PLM mask */
    if (regVal & cThaPohVtPlmStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmPlm;

    /* Get UNEQ mask */
    if (regVal & cThaPohVtUneqStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmUneq;

    /* Get RDI mask */
    if (regVal & cThaPohVtRdiStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmRdi;

    /* Get RFI mask */
    if (regVal & cThaPohVtRfiStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmRfi;

    return swMask;
    }

static eAtRet TxRdiForce(AtChannel self, eBool force)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);

    if (force)
        mFieldIns(&regVal, cThaPohVtRdiInsValMask, cThaPohVtRdiInsValShift, 0x1);

    mFieldIns(&regVal, cThaPohVtRdiInsEnbMask, cThaPohVtRdiInsEnbShift, mBoolToBin(force));
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtRet TxUneqForce(AtChannel self, eBool force)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address, regVal;

    /* Make PSL be zero */
    ThaDefaultPohProcessorV2TxUneqForceByPsl((ThaDefaultPohProcessorV2)self, force);

    /* Make payload (except PSL) be all-zero */
    address = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(processor)->OcnDefaultOffset(processor);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPgStsVtTuUneqPFrcMask, cThaTxPgStsVtTuUneqPFrcShift, mBoolToBin(force));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    if (!mMethodsGet((ThaPohProcessor)self)->CanForceTxAlarms((ThaPohProcessor)self, alarmType))
        return cAtErrorModeNotSupport;

    if (alarmType & cAtSdhPathAlarmRdi)
        TxRdiForce(self, force);

    if (alarmType & cAtSdhPathAlarmUneq)
        TxUneqForce(self, force);

    return cAtOk;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 forcedAlarms = 0;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    /* RDI */
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);
    if (regVal & cThaPohVtRdiInsEnbMask)
        forcedAlarms |= cAtSdhPathAlarmRdi;

    /* UNEQ */
    address = cThaRegOcnTxPGPerChnCtrl + mMethodsGet(processor)->OcnDefaultOffset(processor);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    if (regVal & cThaTxPgStsVtTuUneqPFrcMask)
        forcedAlarms |= cAtSdhPathAlarmUneq;

    return forcedAlarms;
    }

static eAtRet BipForce(AtChannel self, eBool force)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxPGPerChnCtrl + mMethodsGet((ThaVc1xPohProcessorV2)self)->OcnDefaultOffset((ThaVc1xPohProcessorV2)self);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaTxPgStsVtBipErrInsMask, cThaTxPgStsVtBipErrInsShift, force ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool BipForced(AtChannel self)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxPGPerChnCtrl + mMethodsGet((ThaVc1xPohProcessorV2)self)->OcnDefaultOffset((ThaVc1xPohProcessorV2)self);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    return (regVal & cThaTxPgStsVtBipErrInsMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    /* Release first */
    BipForce(self, cAtFalse);

    if (errorType == cAtSdhPathErrorBip) return BipForce(self, cAtTrue);

    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (errorType == cAtSdhPathErrorBip)
        return BipForce(self, cAtFalse);
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (BipForced(self))
        return cAtSdhPathErrorBip;
    return cAtSdhPathErrorNone;
    }

static eAtSdhTtiMode RxTtiModeGet(ThaDefaultPohProcessorV2 self)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    /* TU3 offset is same as STS offset, so use same function to get the offset */
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);
    uint8 hwMode;

    mFieldGet(regVal, cThaPohVtJ2FrmMdMask, cThaPohVtJ2FrmMdShift, uint8, &hwMode);
    return ThaDefaultPohProcessorV2TtiModeHw2Sw(hwMode);
    }

static eAtRet RxTtiModeSet(ThaDefaultPohProcessorV2 self, eAtSdhTtiMode ttiMode)
    {
    uint8 hwMode;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);

    hwMode = ThaDefaultPohProcessorV2TtiHwValueByModeGet(ttiMode);

    mFieldIns(&regVal, cThaPohVtJ2FrmMdMask, cThaPohVtJ2FrmMdShift, hwMode);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);
    return cAtOk;
    }

static uint32 PohCpeVtControlDefaultOffset(ThaVc1xPohProcessorV2 self)
    {
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (hwStsInSlice * VTMAX) + VtFlatId((ThaDefaultPohProcessorV2)self) + mPohPartOffset(self);

    return 0;
    }

static uint32 OcnDefaultOffset(ThaVc1xPohProcessorV2 self)
    {
    uint8 sliceId, stsId, tug2Id, tuId;

    ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &sliceId, &stsId);
    tug2Id = mVcGet(self)->tug2Id;
    tuId   = mVcGet(self)->tu1xId;

    /* (sliceId * 8192) + (stsId * 32) + (tug2Id * 4) + tuId */
    return (uint32)(sliceId << 13) + (uint32)(stsId << 5) + (uint32)(tug2Id << 2) + tuId + mPohPartOffset(self);
    }

static uint32 PohExpectedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (0x0000 + (((hwStsInSlice * VTMAX) +  VtFlatId(self)) * 16) + mPohPartOffset(self));

    return 0;
    }

static uint32 PohTxTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint32 baseAddress = mMethodsGet(self)->PohTxTtiBaseAddress(self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (baseAddress + (((hwStsInSlice * VTMAX) + VtFlatId(self)) * 16) + mPohPartOffset(self));

    return 0;
    }

static uint32 PohCapturedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint32 baseAddress = mMethodsGet(self)->PohCapturedTtiBaseAddress(self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (baseAddress + (((hwStsInSlice * VTMAX) + VtFlatId(self)) * 16) + mPohPartOffset(self));

    return 0;
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    return (uint8)((RxV5Get(self) & cBit3_1) >> 1);
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);

    mFieldIns(&regVal, cThaPohVtVslInsValMask, cThaPohVtVslInsValShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    uint8 psl;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaPohVtVslInsValMask, cThaPohVtVslInsValShift, uint8, &psl);

    return psl;
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);

    /* Get the expected mask/shift of Path/V Signal Label */
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);

    mFieldIns(&regVal, cThaPohVtVslExptValMask, cThaPohVtVslExptValShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    uint8 pslValue;
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);


    /* Get the expected mask/shift of Path/V Signal Label */
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaPohVtVslExptValMask, cThaPohVtVslExptValShift, uint8, &pslValue);

    return pslValue;
    }

static uint32 RxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 sts, slice;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &sts) == cAtOk)
        return (uint32)(slice << 10) + (uint32)(sts << 5) + VtFlatId((ThaDefaultPohProcessorV2)self) +
               (read2Clear ? 0 : 0x400) + (isDecrease ? 512 : 0) + mPohPartOffset(self);

    return 0;
    }

static uint32 RxPointerAdjustCounterAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegOcnRxVtPtrAdjPerChnCnt;
    }

static uint32 TxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    /* Same as RX */
    return RxPointerAdjustCounterOffset(self, read2Clear, isDecrease);
    }

static uint32 TxPointerAdjustCounterAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegOcnTxStsVtPtrAdjPerChnCnt;
    }

static uint32 PohRegVtBipErrCnt(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohCpeVtBipErrCnt;
    }

static uint32 PohRegVtReiPErrCnt(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohCpeVtReiVCnt;
    }

static uint32 ReadOnlyCntGet(AtChannel self, uint32 counterType)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 offset = mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 address;

    switch (counterType)
        {
        case cAtSdhPathCounterTypeBip:
            {
            address = mMethodsGet(processor)->PohRegVtBipErrCnt(processor) + offset;
            return mChannelHwRead(self, address, cThaModulePoh) & cThaRegPohCpeVtBipErrMask;
            }

        case cAtSdhPathCounterTypeRei:
            {
            address = mMethodsGet(processor)->PohRegVtReiPErrCnt(processor) + offset;
            return mChannelHwRead(self, address, cThaModulePoh) & cThaRegPohCpeVtReiVMask;
            }

        default:
            return 0;
        }
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);

    mFieldIns(&regVal, cThaPohVtJ2MonEnbMask, cThaPohVtJ2MonEnbShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModulePoh);
    return cAtOk;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);

    return (regVal & cThaPohVtJ2MonEnbMask) ? cAtTrue : cAtFalse;
    }

static uint32 OverheadByteInsertBufferRegister(uint32 overheadByte)
    {
    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteV5: return cThaRegPohVtV5InsBuffer;
        case cAtSdhPathOverheadByteN2: return cThaRegPohVtN2InsBuffer;
        case cAtSdhPathOverheadByteK4: return cThaRegPohVtK4InsBuffer;

        default:
            return 0x0;
        }
    }

static void TxByteK4BufferSet(ThaPohProcessor self, uint8 k4Value)
    {
    ThaVc1xPohProcessorV2 pohProcessor = (ThaVc1xPohProcessorV2)self;
    uint32 offset  = mMethodsGet(pohProcessor)->PohCpeVtControlDefaultOffset(pohProcessor);
    uint32 regAddr = OverheadByteInsertBufferRegister(cAtSdhPathOverheadByteK4) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cThaRegPohVtK4InsByte, k4Value);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
    }

static void TxByteV5BufferSet(ThaPohProcessor self, uint8 v5Value)
    {
    ThaVc1xPohProcessorV2 pohProcessor = (ThaVc1xPohProcessorV2)self;
    uint32 offset  = mMethodsGet(pohProcessor)->PohCpeVtControlDefaultOffset(pohProcessor);
    uint32 regAddr = OverheadByteInsertBufferRegister(cAtSdhPathOverheadByteV5) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cThaRegPohVtV5InsByte, v5Value);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
    }

static eAtRet AutoRdiEnable(ThaPohProcessor self, uint32 alarmType, eBool enable)
    {
    ThaVc1xPohProcessorV2 pohProcessor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(pohProcessor)->PohCpeVtCtrl(pohProcessor) +
                     mMethodsGet(pohProcessor)->PohCpeVtControlDefaultOffset(pohProcessor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);
    uint8 hwEnable = enable ? 0 : 1;
    AtSdhChannel sdhVc = (AtSdhChannel)ThaPohProcessorVcGet(self);
    AtSdhLine sdhLine  = AtSdhChannelLineObjectGet(sdhVc);

    /* When RX Line is disabled and auto RDI is also disabled, some applications
     * require no RDI is sent backward. To do this, software has to
     * - Catch all value K4,V5
     * - Clear RDI related fields to 0
     * - Disable Auto-RDI */
    if (!enable && !AtChannelRxIsEnabled((AtChannel)sdhLine))
        {
        uint32 rdiValue;

        pohProcessor->k4Value = AtSdhChannelTxOverheadByteGet(sdhVc, cAtSdhPathOverheadByteK4);
        rdiValue = pohProcessor->k4Value;
        mRegFieldSet(rdiValue, cERdiK4Byte, 0x0);
        TxByteK4BufferSet(self, (uint8)rdiValue);

        pohProcessor->v5Value = AtSdhChannelTxOverheadByteGet(sdhVc, cAtSdhPathOverheadByteV5);
        rdiValue = pohProcessor->v5Value;
        mRegFieldSet(rdiValue, cRdiV5Byte, 0x0);
        TxByteV5BufferSet(self, (uint8)rdiValue);

        pohProcessor->rdiFieldsCached = cAtTrue;
        }

    if (enable && pohProcessor->rdiFieldsCached)
        {
        TxByteK4BufferSet(self, pohProcessor->k4Value);
        TxByteV5BufferSet(self, pohProcessor->v5Value);
        pohProcessor->rdiFieldsCached = cAtFalse;
        }

    if (alarmType & cAtSdhPathAlarmAis)
        mFieldIns(&regVal, cThaPohVtAisMskMask, cThaPohVtAisMskShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmTim)
        mFieldIns(&regVal, cThaPohVtTimMskMask, cThaPohVtTimMskShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal, cThaPohVtPlmMskMask, cThaPohVtPlmMskShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal, cThaPohVtUneqMskMask, cThaPohVtUneqMskShift, hwEnable);

    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 AutoRdiAlarms(ThaPohProcessor self)
    {
    ThaVc1xPohProcessorV2 vc1xProcessor = (ThaVc1xPohProcessorV2)self;
    uint32 address = mMethodsGet(vc1xProcessor)->PohCpeVtCtrl(vc1xProcessor) +
                     mMethodsGet(vc1xProcessor)->PohCpeVtControlDefaultOffset(vc1xProcessor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);
    uint32 alarms  = 0;

    if ((regVal & cThaPohVtAisMskMask) == 0)
        alarms |= cAtSdhPathAlarmAis;
    if ((regVal & cThaPohVtTimMskMask) == 0)
        alarms |= cAtSdhPathAlarmTim;
    if ((regVal & cThaPohVtPlmMskMask) == 0)
        alarms |= cAtSdhPathAlarmPlm;
    if ((regVal & cThaPohVtUneqMskMask) == 0)
        alarms |= cAtSdhPathAlarmUneq;

    return alarms;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
	AtUnused(self);
    return cAtSdhPathAlarmUneq | cAtSdhPathAlarmRdi;
    }

static uint32 BlockErrorCounterOffset(ThaDefaultPohProcessorV2 self, uint32 counterType)
    {
	AtUnused(counterType);
    return mMethodsGet((ThaVc1xPohProcessorV2)self)->PohCpeVtControlDefaultOffset((ThaVc1xPohProcessorV2)self);
    }

static eBool OverheadByteIsValid(ThaDefaultPohProcessorV2 self, uint32 overheadByte)
    {
	AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteV5: return cAtTrue;
        case cAtSdhPathOverheadByteN2: return cAtTrue;
        case cAtSdhPathOverheadByteK4: return cAtTrue;

        default:
            return cAtFalse;
        }
    }

static eAtRet TxByteK4Insert(AtSdhChannel self, uint8 overheadByteValue)
    {
    ThaVc1xPohProcessorV2 processor = (ThaVc1xPohProcessorV2)self;
    uint32 regAddr = mMethodsGet(processor)->PohCpeVtCtrl(processor) +
                     mMethodsGet(processor)->PohCpeVtControlDefaultOffset(processor);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regVal, cThaPohVtK4b1b4InsVal, (overheadByteValue & cBit7_4) >> 4);
    mRegFieldSet(regVal, cThaPohVtK4b8InsVal  , (overheadByteValue & cBit0));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    ThaDefaultPohProcessorV2 pohProcessor = (ThaDefaultPohProcessorV2)self;

    if (!mMethodsGet(pohProcessor)->OverheadByteIsValid(pohProcessor, overheadByte))
        return cAtErrorInvlParm;

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteK4:
            return TxByteK4Insert(self, overheadByteValue);
        case cAtSdhPathOverheadByteV5:
            return AtSdhPathTxPslSet((AtSdhPath)self, (uint8)((overheadByteValue & cVslBitMask) >> cVslBitShift));
        default:
            {
            uint32 regAddr = OverheadByteInsertBufferRegister(overheadByte) +
                             mMethodsGet((ThaVc1xPohProcessorV2)self)->PohCpeVtControlDefaultOffset((ThaVc1xPohProcessorV2)self);
            uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
            mRegFieldSet(regVal, cThaRegPohStsTuVtInsByte, overheadByteValue);
            mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);
            return cAtOk;
            }
        }
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint32 regVal, regAddr;
    uint8 overheadByteVal;
    ThaDefaultPohProcessorV2 pohProcessor = (ThaDefaultPohProcessorV2)self;

    if (!mMethodsGet(pohProcessor)->OverheadByteIsValid(pohProcessor, overheadByte))
        return cAtErrorModeNotSupport;

    regAddr = OverheadByteInsertBufferRegister(overheadByte) +
              mMethodsGet((ThaVc1xPohProcessorV2)self)->PohCpeVtControlDefaultOffset((ThaVc1xPohProcessorV2)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    overheadByteVal = (uint8)mRegField(regVal, cThaRegPohStsTuVtInsByte);

    return overheadByteVal;
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    AtSdhPath sdhPath = (AtSdhPath)self;
    ThaDefaultPohProcessorV2 pohProcessor = (ThaDefaultPohProcessorV2)self;

    if (!mMethodsGet(pohProcessor)->OverheadByteIsValid(pohProcessor, overheadByte))
        return 0;

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteV5:
            return RxV5Get(sdhPath);
        case cAtSdhPathOverheadByteK4:
            return RxK4Get(sdhPath);
        case cAtSdhPathOverheadByteN2:
            return RxN2Get(sdhPath);

        /* Cannot be this */
        default:
            return 0;
        }
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaVc1xPohProcessorV2 object = (ThaVc1xPohProcessorV2)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(k4Value);
    mEncodeUInt(v5Value);
    mEncodeUInt(rdiFieldsCached);
    }

static void OverrideAtObject(ThaVc1xPohProcessorV2 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPohProcessor(ThaVc1xPohProcessorV2 self)
    {
    ThaPohProcessor processor = (ThaPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaPohProcessorOverride,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaPohProcessorOverride));
        mMethodOverride(m_ThaPohProcessorOverride, VcAisAffectEnable);
        mMethodOverride(m_ThaPohProcessorOverride, AutoRdiEnable);
        mMethodOverride(m_ThaPohProcessorOverride, AutoRdiAlarms);
        }

    mMethodsSet(processor, &m_ThaPohProcessorOverride);
    }

static void OverrideThaDefaultPohProcessorV2(ThaVc1xPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDefaultPohProcessorV2Override, mMethodsGet(processor), sizeof(m_ThaDefaultPohProcessorV2Override));
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeSet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohExpectedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, BlockErrorCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, OverheadByteIsValid);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterOffset);

        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void OverrideAtChannel(ThaVc1xPohProcessorV2 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, ReadOnlyCntGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(ThaVc1xPohProcessorV2 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(ThaVc1xPohProcessorV2 self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, ERdiEnable);
        mMethodOverride(m_AtSdhPathOverride, ERdiIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void Override(ThaVc1xPohProcessorV2 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaDefaultPohProcessorV2(self);
    OverrideThaPohProcessor(self);
    }

static void MethodsInit(ThaVc1xPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Initialize method */
        mMethodOverride(m_methods, PohCpeVtControlDefaultOffset);
        mMethodOverride(m_methods, OcnDefaultOffset);
        mMethodOverride(m_methods, PohCpeVtCtrl);
        mMethodOverride(m_methods, PohCpeVtPathOhGrabber1);
        mMethodOverride(m_methods, PohRegVtAlarmStatus);
        mMethodOverride(m_methods, PohRegVtAlarmIntrSticky);
        mMethodOverride(m_methods, PohRegVtAlarmIntrEnable);
        mMethodOverride(m_methods, PohRegVtBipErrCnt);
        mMethodOverride(m_methods, PohRegVtReiPErrCnt);
        }

    mMethodsSet(self, &m_methods);
    }

ThaVc1xPohProcessorV2 ThaVc1xPohProcessorV2ObjectInit(ThaVc1xPohProcessorV2 self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDefaultPohProcessorV2ObjectInit((ThaDefaultPohProcessorV2)self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->k4Value = 0x0;
    self->v5Value = 0x0;

    return self;
    }

ThaPohProcessor ThaVc1xPohProcessorV2New(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPohProcessor)ThaVc1xPohProcessorV2ObjectInit((ThaVc1xPohProcessorV2)newProcessor, vc);
    }

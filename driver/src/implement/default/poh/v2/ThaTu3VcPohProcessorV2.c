/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaTu3VcPohProcessorV2.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : TU-3's VC POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaTu3VcPohProcessorV2Internal.h"
#include "../ThaModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaDefaultPohProcessorV2Methods m_ThaDefaultPohProcessorV2Override;
static tThaAuVcPohProcessorV2Methods    m_ThaAuVcPohProcessorV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaTu3VcPohProcessorV2);
    }

static ThaModulePohV2 ModulePoh(ThaDefaultPohProcessorV2 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    return (ThaModulePohV2)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint8 StsMax(ThaDefaultPohProcessorV2 self)
    {
    return ThaModulePohV2NumSts(ModulePoh(self));
    }

static uint32 PohExpectedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax(self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (0x0000 + (STSMAX * VTMAX  * 16UL) + (STSMAX * 16UL)  + (hwStsInSlice * 16UL));

    return 0;
    }

static uint32 PohTxTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax(self);
    uint32 baseAddress = mMethodsGet(self)->PohTxTtiBaseAddress(self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return baseAddress + (STSMAX * VTMAX * 16UL) + (STSMAX * 16UL) + (hwStsInSlice * 16UL);

    return 0;
    }

static uint32 PohCapturedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax(self);
    uint32 baseAddress = mMethodsGet(self)->PohCapturedTtiBaseAddress(self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return baseAddress + (STSMAX * VTMAX * 16UL)  + (STSMAX * 16UL) + (hwStsInSlice * 16UL);

    return 0;
    }

static uint32 PohCpeStsTu3PohOffset(ThaAuVcPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax((ThaDefaultPohProcessorV2)self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (STSMAX * VTMAX) + STSMAX + hwStsInSlice + ThaPohProcessorPartOffset((ThaPohProcessor)self);

    return 0;
    }

static uint32 RxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 sts, slice;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &sts) == cAtOk)
        return (uint32)(slice << 10) + (uint32)(sts << 5) + (read2Clear ? 0 : 0x400) + (isDecrease ? 512 : 0) + mPohPartOffset(self);

    return 0;
    }

static uint32 TxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    /* Same as RX */
    return RxPointerAdjustCounterOffset(self, read2Clear, isDecrease);
    }

static uint32 RxPointerAdjustCounterAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegOcnRxVtPtrAdjPerChnCnt;
    }

static uint32 OverheadByteInsertBufferRegister(ThaAuVcPohProcessorV2 self, uint32 overheadByte)
    {
	AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteC2: return cThaRegPohTu3C2InsBuffer;
        case cAtSdhPathOverheadByteF2: return cThaRegPohTu3F2InsBuffer;
        case cAtSdhPathOverheadByteF3: return cThaRegPohTu3F3InsBuffer;
        case cAtSdhPathOverheadByteG1: return cThaRegPohTu3G1InsBuffer;
        case cAtSdhPathOverheadByteH4: return cThaRegPohTu3H4InsBuffer;
        case cAtSdhPathOverheadByteK3: return cThaRegPohTu3K3InsBuffer;
        case cAtSdhPathOverheadByteN1: return cThaRegPohTu3N1InsBuffer;

        default:
            return 0x0;
        }
    }

static void OverrideThaAuVcPohProcessorV2(ThaTu3VcPohProcessorV2 self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaAuVcPohProcessorV2Override,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaAuVcPohProcessorV2Override));
                                  
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PohOffset);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, OverheadByteInsertBufferRegister);
        }

    mMethodsSet(processor, &m_ThaAuVcPohProcessorV2Override);
    }

static void OverrideThaDefaultPohProcessorV2(ThaTu3VcPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaDefaultPohProcessorV2Override,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaDefaultPohProcessorV2Override));
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohExpectedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterOffset);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void Override(ThaTu3VcPohProcessorV2 self)
    {
    OverrideThaAuVcPohProcessorV2(self);
    OverrideThaDefaultPohProcessorV2(self);
    }

ThaTu3VcPohProcessorV2 ThaTu3VcPohProcessorV2ObjectInit(ThaTu3VcPohProcessorV2 self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaAuVcPohProcessorV2ObjectInit((ThaAuVcPohProcessorV2)self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor ThaTu3VcPohProcessorV2New(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return (ThaPohProcessor)ThaTu3VcPohProcessorV2ObjectInit((ThaTu3VcPohProcessorV2)newProcessor, vc);
    }


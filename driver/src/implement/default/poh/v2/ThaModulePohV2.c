/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaModulePohV2.c
 *
 * Created Date: Mar 20, 2013
 *
 * Description : POH module - work with latest hardware version
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModulePohInternal.h"
#include "../ThaTtiProcessor.h"
#include "../../sdh/ThaModuleSdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(module) (ThaModulePohV2)module

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePohV2Methods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModulePohMethods m_ThaModulePohOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePohV2);
    }

static uint8 NumParts(AtModule self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    return ThaDeviceNumPartsOfModule(device, cThaModulePoh);
    }

static uint32 PartOffset(AtModule self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    return ThaDeviceModulePartOffset(device, cThaModulePoh, partId);
    }

/* Set Counter mode for POH module, If read only, default to roll over */
static void CounterRead2ClearEnable(AtModule self, eBool read2Clear)
    {
    uint8 part_i;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        {
        uint32 regVal;
        uint32 partOffset = PartOffset(self, part_i);

        /* POH B3/REI-P Counter Mode Control */
        regVal = 0;
        mFieldIns(&regVal, cThaPohRegB3ReiCntMdMask, cThaPohRegB3ReiCntMdShift, mBoolToBin(read2Clear));
        mModuleHwWrite(self, cThaPohRegB3ReiCntMdCtrl + partOffset, regVal);

        /* POH BIP2/REI-V Counter Mode Control */
        regVal = 0;
        mFieldIns(&regVal, cThaPohRegBip2ReiVCntMdMask, cThaPohRegBip2ReiVCntMdShift, mBoolToBin(read2Clear));
        mModuleHwWrite(self, cThaPohRegBip2ReiVCntMdCtrl + partOffset, regVal);
        }
    }

static eAtRet PartDefaultSet(AtModule self, uint8 partId)
    {
    eBool raiseUneq = mMethodsGet(mThis(self))->RaiseUNEQwhenRxAndExpectedPslAreZero(mThis(self));
    uint32 regAddr  = cThaRegPohMastCtrl + PartOffset(self, partId);
    uint32 regVal   = mModuleHwRead(self, regAddr);

    /* POH engine will rise UNEQ signal when it received C2cur = C2exp = 0x0 */
    mFieldIns(&regVal,
              cThaUneqModeWhenC2ExpIs0Mask,
              cThaUneqModeWhenC2ExpIs0Shift,
              raiseUneq ? 1 : 0);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModulePohV2 self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;
    AtModule module = (AtModule)self;

    /* POH counter is always read only */
    CounterRead2ClearEnable(module, cAtFalse);

    for (part_i = 0; part_i < NumParts(module); part_i++)
        ret |= PartDefaultSet(module, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Set default */
    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModulePohV2RegisterIteratorNew(self);
    }

static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
	AtUnused(self);
    return ThaSdhLineTtiProcessorV2New((AtSdhChannel)line);
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(self);
    return (ThaPohProcessor)ThaAuVcPohProcessorV2New(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(self);
    return (ThaPohProcessor)ThaTu3VcPohProcessorV2New(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
	AtUnused(self);
    return (ThaPohProcessor)ThaVc1xPohProcessorV2New(vc);
    }

static eBool RaiseUNEQwhenRxAndExpectedPslAreZero(ThaModulePohV2 self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint8 NumSts(ThaModulePohV2 self)
    {
    AtUnused(self);
    return 12;
    }

static uint32 ThaPohIndirectAddressMask(ThaModulePohV2 self)
    {
    AtUnused(self);
    return cThaPohIndirectAddressMask;
    }

static uint8  ThaPohIndirectAddressShift(ThaModulePohV2 self)
    {
    AtUnused(self);
    return cThaPohIndirectAddressShift;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModulePoh(self);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Additional methods setup go here */
        mMethodOverride(m_methods, RaiseUNEQwhenRxAndExpectedPslAreZero);
        mMethodOverride(m_methods, NumSts);
        mMethodOverride(m_methods, DefaultSet);

        mBitFieldOverride(ThaModulePohV2, m_methods, ThaPohIndirectAddress)
        }

    mMethodsSet((ThaModulePohV2)self, &m_methods);
    }

AtModule ThaModulePohV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePohObjectInit(self, device) == NULL)
        return NULL;

    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePohV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePohV2ObjectInit(newModule, device);
    }

uint8 ThaModulePohV2NumSts(ThaModulePohV2 self)
    {
    if (self)
        return mMethodsGet(self)->NumSts(self);
    return 0;
    }

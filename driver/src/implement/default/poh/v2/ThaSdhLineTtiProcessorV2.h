/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : ThaSdhLineTtiProcessorV2.h
 * 
 * Created Date: Jul 11, 2019
 *
 * Description : SDH Line TTI processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHLINETTIPROCESSORV2_H_
#define _THASDHLINETTIPROCESSORV2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaTtiProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhLineTtiProcessorV2 * ThaSdhLineTtiProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THASDHLINETTIPROCESSORV2_H_ */


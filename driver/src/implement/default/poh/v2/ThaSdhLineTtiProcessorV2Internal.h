/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaSdhLineTtiProcessorV2Internal.h
 * 
 * Created Date: Jul 11, 2019
 *
 * Description : SDH Line TTI processor V2 representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHLINETTIPROCESSORV2INTERNAL_H_
#define _THASDHLINETTIPROCESSORV2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaTtiProcessorInternal.h" /* Superclass */
#include "ThaSdhLineTtiProcessorV2.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhLineTtiProcessorV2Methods
    {
    uint32 (*DefaultOffset)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*OcnJ0TxMsgBufDefaultOffset)(ThaSdhLineTtiProcessorV2 self, uint8 bufId);
    uint32 (*PohExpectedTtiAddressGet)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohCapturedTtiAddressGet)(ThaSdhLineTtiProcessorV2 self);
    uint16 (*PohCapturedTtiBaseAddress)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0StiMsgAlarmStatus)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0StiMsgAlarmIntrSticky)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0StiMsgAlarmIntrEnable)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0Control)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*TxJ0MessageBufferRegAddress)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*ExpTtiBufferRegOffset)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0Status)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0StatusOffset)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0Grabber)(ThaSdhLineTtiProcessorV2 self);
    uint32 (*PohJ0GrabberOffset)(ThaSdhLineTtiProcessorV2 self);
    }tThaSdhLineTtiProcessorV2Methods;

typedef struct tThaSdhLineTtiProcessorV2
    {
    tThaTtiProcessor super;
    const tThaSdhLineTtiProcessorV2Methods *methods;

    /* Private data */
    uint8 ttiMode;
    }tThaSdhLineTtiProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaTtiProcessor ThaSdhLineTtiProcessorV2ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHLINETTIPROCESSORV2INTERNAL_H_ */


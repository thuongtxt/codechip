/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaTu3VcPohProcessorV2Internal.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : TU-3's VC POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATU3VCPOHPROCESSORV2INTERNAL_H_
#define _THATU3VCPOHPROCESSORV2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaAuVcPohProcessorV2Internal.h"
#include "ThaTu3VcPohProcessorV2.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaTu3VcPohProcessorV2
    {
    tThaAuVcPohProcessorV2 super;

    /* Private data */
    }tThaTu3VcPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaTu3VcPohProcessorV2 ThaTu3VcPohProcessorV2ObjectInit(ThaTu3VcPohProcessorV2 self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THATU3VCPOHPROCESSORV2INTERNAL_H_ */


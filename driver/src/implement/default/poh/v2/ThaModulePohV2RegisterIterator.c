/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaModulePohV2RegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : POH register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"

#include "../../../../util/AtIteratorInternal.h"
#include "../../sdh/ThaModuleSdhReg.h"
#include "../../sdh/ThaModuleSdhRegisterIteratorInternal.h"
#include "../ThaModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModulePohV2RegisterIterator
    {
    tThaModuleSdhRegisterIterator super;
    }tThaModulePohV2RegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePohV2RegisterIterator);
    }

static uint8 CurrentRegType(AtModuleRegisterIterator  self)
    {
    return ThaModuleSdhRegisterIteratorCurrentRegType((ThaModuleSdhRegisterIterator)self);
    }

static void CurrentRegTypeSet(AtIterator self, uint8 type)
    {
    ThaModuleSdhRegisterIteratorCurrentRegTypeSet((ThaModuleSdhRegisterIterator)self, type);
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    ThaModuleSdhRegisterIterator iterator = (ThaModuleSdhRegisterIterator)self;

    if (CurrentRegType(self) == cSdhRegTypeLine)
        return ThaModuleSdhRegisterIteratorNextLine(iterator);

    if (CurrentRegType(self) == cSdhRegTypeSts)
        {
        uint8 slice, sts;
        ThaModuleSdhRegisterIteratorNextSts(iterator, &slice, &sts);

        return sts;
        }

    if (CurrentRegType(self) == cSdhRegTypeVt)
        {
        uint8 slice, sts, vtg, vt;
        ThaModuleSdhRegisterIteratorNextVt(iterator, &slice, &sts, &vtg, &vt);

        return (sts * VTMAX) + (vtg * 4UL) + vt;
        }

    return m_AtModuleRegisterIteratorMethods->NextOffset(self);
    }

static ThaModulePohV2 ModulePoh(AtIterator self)
    {
    return (ThaModulePohV2)AtModuleRegisterIteratorModuleGet((AtModuleRegisterIterator)self);
    }

static uint8 StsMax(AtIterator self)
    {
    return ThaModulePohV2NumSts(ModulePoh(self));
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;
    uint8 STSMAX = StsMax(self);

    mNextReg(iterator, cThaPohRegB3ReiCntMdCtrl, cThaRegPohSdhSonetCtrl);
    CurrentRegTypeSet(self, cSdhRegTypeLine);
    mNextChannelRegister(iterator, cThaRegPohSdhSonetCtrl, cThaRegPohCpeStsTu3Ctrl);
    CurrentRegTypeSet(self, cSdhRegTypeSts);
    mNextChannelRegister(iterator, cThaRegPohCpeStsTu3Ctrl, cThaPohRegCpeTu3Ctrl);
    mNextChannelRegister(iterator, cThaPohRegCpeTu3Ctrl, cThaPohRegCpeVtControl);
    CurrentRegTypeSet(self, cSdhRegTypeVt);
    mNextChannelRegister(iterator, cThaPohRegCpeVtControl, cThaRegPohStsC2InsBuffer);
    CurrentRegTypeSet(self, cSdhRegTypeSts);
    mNextChannelRegister(iterator, cThaRegPohStsC2InsBuffer, cThaRegPohTu3C2InsBuffer);
    mNextChannelRegister(iterator, cThaRegPohTu3C2InsBuffer, cThaRegPohIndAcsControl);
    mNextReg(iterator, cThaRegPohIndAcsControl, cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

/* TODO: Class setup functions look wrong */
static AtIterator ThaModulePohV2RegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (ThaModuleSdhRegisterIteratorObjectInit(self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;
    return self;
    }

AtIterator ThaModulePohV2RegisterIteratorNew(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());
    if (newIterator == NULL)
        return NULL;

    return ThaModulePohV2RegisterIteratorObjectInit(newIterator, module);
    }

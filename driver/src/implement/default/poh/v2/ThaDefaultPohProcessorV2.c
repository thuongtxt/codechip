/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Default POH processor
 *
 * File        : ThaDefaultPohProcessorV2.c
 *
 * Created Date: Mar 19, 2013
 *
 * Description : Default POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaDefaultPohProcessorV2Internal.h"
#include "../ThaModulePohInternal.h"
#include "atcrc7.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaDefaultPohProcessorV2)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaDefaultPohProcessorV2Methods m_methods;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtChannelMethods           m_AtChannelOverride;
static tAtSdhChannelMethods        m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtSdhTtiMode RxTtiModeGet(ThaDefaultPohProcessorV2 self)
    {
	AtUnused(self);
    /* Let's subclass do */
    return cAtSdhTtiMode1Byte;
    }

static eAtRet RxTtiModeSet(ThaDefaultPohProcessorV2 self, eAtSdhTtiMode ttiMode)
    {
	AtUnused(ttiMode);
	AtUnused(self);
    /* Let's subclass do */
    return cAtError;
    }

static uint32 PohExpectedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
	AtUnused(self);
    /* Let's subclass do */
    return 0;
    }

static uint32 PohTxTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
	AtUnused(self);
    /* Let subclass do */
    return 0;
    }

static uint32 PohCapturedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
	AtUnused(self);
    /* Let subclass do */
    return 0;
    }

static eAtRet TxTtiModeSet(ThaDefaultPohProcessorV2 self, eAtSdhTtiMode mode)
    {
    self->txTtiMode = (uint8)mode;
    return cAtOk;
    }

static eAtSdhTtiMode TxTtiModeGet(ThaDefaultPohProcessorV2 self)
    {
    return self->txTtiMode;
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    eAtRet thaRet;

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->TxTtiSet(self, tti);

    thaRet  = TxTtiModeSet(processor, tti->mode);
    thaRet |= ThaSdhTtiWrite((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self), tti, mMethodsGet(processor)->PohTxTtiAddressGet(processor));

    return thaRet;
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->TxTtiGet(self, tti);

    tti->mode = TxTtiModeGet(processor);
    return ThaSdhTtiRead((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self), tti, mMethodsGet(processor)->PohTxTtiAddressGet(processor));
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    tti->mode = mMethodsGet(processor)->RxTtiModeGet(processor);

    return ThaSdhTtiRead((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self), tti, mMethodsGet(processor)->PohCapturedTtiAddressGet(processor));
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    uint32 indirectAddr = mMethodsGet(processor)->PohExpectedTtiAddressGet(processor);

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->ExpectedTtiGet(self, tti);

    /* Get TTI expected mode */
    tti->mode = mMethodsGet(processor)->RxTtiModeGet(processor);

    /* Read message and return */
    return ThaSdhTtiRead((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self), tti, indirectAddr);
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    eAtRet thaRet;
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    uint32 indirectAddr = mMethodsGet(processor)->PohExpectedTtiAddressGet(processor);

    if (ThaPohProcessorIsSimulated((ThaPohProcessor)self))
        return m_AtSdhChannelMethods->ExpectedTtiSet(self, tti);

    /* Set expected mode */
    thaRet = mMethodsGet(processor)->RxTtiModeSet(processor, tti->mode);

    /* Set expected message */
    thaRet |= ThaSdhTtiWrite((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self), tti, indirectAddr);

    return thaRet;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDefaultPohProcessorV2);
    }

static void AllAlarmsUnforce(AtChannel self)
    {
    ThaPohProcessor processor = (ThaPohProcessor)self;
    uint32 forcableParam = AtChannelTxForcableAlarmsGet(self);
    if (forcableParam)
        AtChannelTxAlarmUnForce(self, forcableParam);

    forcableParam = mMethodsGet(processor)->TxForceAbleErrors(processor);
    if (forcableParam)
        AtChannelTxErrorUnForce(self, forcableParam);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AllAlarmsUnforce(self);

    /* Reset cache */
    mThis(self)->pslCached = cAtFalse;
    mThis(self)->txPsl     = 0;

    return cAtOk;
    }

static uint32 ChannelPartOffset(AtSdhChannel channel)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)channel);
    return ThaDeviceModulePartOffset(device, cThaModulePoh, ThaModuleSdhPartOfChannel(channel));
    }

static eAtRet TtiBufferWrite(ThaModulePohV2 pohModule,
                             AtSdhChannel channel,
                             uint8 *msgBuffer,
                             uint8 msgLength,
                             uint32 indirectAddr)
    {
    uint32 regVal;
    uint8 byte_i, byteIndexInDword;
    uint8 *pCurrentByte;
    eBool dwordIsFormed, endOfMessage;
    uint32 elapseTime;
    tAtOsalCurTime curTime, startTime;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 partOffset = ChannelPartOffset(channel);
    uint32 ctrlRegAddr = cThaPohIndirectAccessCtrl + partOffset;
    uint32 indirectAddressMask;
    uint8 indirectAddressShift;

    if (AtDeviceWarmRestoreIsStarted(AtModuleDeviceGet((AtModule)pohModule)))
        return cAtOk;

    indirectAddressMask = mMethodsGet(pohModule)->ThaPohIndirectAddressMask(pohModule);
    indirectAddressShift = mMethodsGet(pohModule)->ThaPohIndirectAddressShift(pohModule);

    pCurrentByte = msgBuffer;
    regVal = 0;
    for (byte_i = 0; byte_i < msgLength; byte_i++)
        {
        byteIndexInDword = byte_i % cThaSonetNumBytesInDword;
        mFieldIns(&regVal, cThaOcnJnMsgMask(byteIndexInDword), cThaOcnJnMsgShift(byteIndexInDword), *pCurrentByte);
        pCurrentByte++; /* Next byte */

        /* Only write hardware when one dword is built or end of message */
        dwordIsFormed = (byteIndexInDword == (cThaSonetNumBytesInDword - 1));
        endOfMessage  = (byte_i == (msgLength - 1));
        if ((!dwordIsFormed) && (!endOfMessage))
            continue;

        /* Its time to write message to hardware */
        mModuleHwWrite(pohModule, cThaPohIndirectAcsData + partOffset, regVal);

        /* Make a write command */
        regVal = 0;
        mRegFieldSet(regVal, indirectAddress, indirectAddr++);
        mFieldIns(&regVal, cThaPohIndirectRnWMask, cThaPohIndirectRnWShift, 0);
        mFieldIns(&regVal, cThaPohIndirectAcsEnMask, cThaPohIndirectAcsEnShift, 1);
        mModuleHwWrite(pohModule, ctrlRegAddr, regVal);

        /* Continue this job if hardware finished */
        regVal = mModuleHwRead(pohModule, ctrlRegAddr);
        if ((regVal & cThaPohIndirectAcsEnMask) == 0)
            continue;

        /* Or timeout waiting for it */
        elapseTime = 0;
        mMethodsGet(osal)->CurTimeGet(osal, &startTime);
        while (elapseTime < cThaPohIndirectAcsTimeOut)
            {
            /* Hardware finishes its job, exit this loop */
            regVal = mModuleHwRead(pohModule, ctrlRegAddr);
            if ((regVal & cThaPohIndirectAcsEnMask) == 0)
                break;

            /* Calculate elapse time */
            mMethodsGet(osal)->CurTimeGet(osal, &curTime);
            elapseTime = mTimeIntervalInMsGet(startTime, curTime);
            }

        /* Timeout */
        if (elapseTime >= cThaPohIndirectAcsTimeOut)
            return cAtErrorIndrAcsTimeOut;
        }

    return cAtOk;
    }

static uint32 BlockErrorCounterOffset(ThaDefaultPohProcessorV2 self, uint32 counterType)
    {
	AtUnused(counterType);
	AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 BlockCounterReadOnly(ThaDefaultPohProcessorV2 self, uint32 counterType)
    {
    uint32 offset = mMethodsGet(self)->BlockErrorCounterOffset(self, counterType);

    if (ThaModuleSdhHas24BitsBlockErrorCounter((ThaModuleSdh)AtChannelModuleGet((AtChannel)self)))
        {
        if (counterType == cAtSdhPathCounterTypeBip)
            return mChannelHwRead(self, cThaRegPohCpeBipBlockError24BitsCounter + offset, cThaModulePoh);
        if (counterType == cAtSdhPathCounterTypeRei)
            return mChannelHwRead(self, cThaRegPohCpeReiBlockError24BitsCounter + offset, cThaModulePoh);
        }
    else
        {
        uint32 regVal = mChannelHwRead(self, cThaRegPohCpeBip8ReiBlockCounter + offset, cThaModulePoh);
        if (counterType == cAtSdhPathCounterTypeBip)
            return mRegField(regVal, cThaPohCpeBip8Counter);
        if (counterType == cAtSdhPathCounterTypeRei)
            return mRegField(regVal, cThaPohCpeReiCounter);
        }

    return 0x0;
    }

static uint32 DefaultPohProcessorV2BlockCounterReadOnly(ThaDefaultPohProcessorV2 self, uint32 counterType)
    {
    AtDevice device;
    ThaModuleSdh sdhModule;

    if (self == NULL)
        return 0;

    device = AtChannelDeviceGet((AtChannel)self);
    sdhModule = (ThaModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    if (ThaModuleSdhBlockErrorCountersSupported(sdhModule))
        return mMethodsGet(self)->BlockCounterReadOnly(self, counterType);

    return 0;
    }

static uint32 BlockErrorCounterRead2Clear(AtSdhChannel self, uint16 counterType, eBool clear)
    {
    uint32 newCounter;
    uint32 *lastValue = NULL;
    uint32 maxValue;

    if (!ThaModuleSdhBlockErrorCountersSupported((ThaModuleSdh)AtChannelModuleGet((AtChannel)self)))
        return 0;

    newCounter = DefaultPohProcessorV2BlockCounterReadOnly(mThis(self), counterType);
    if (counterType == cAtSdhPathCounterTypeBip)
        lastValue = &(mThis(self)->bipBlockCnt);
    if (counterType == cAtSdhPathCounterTypeRei)
        lastValue = &(mThis(self)->reiBlockCnt);

    if (ThaModuleSdhHas24BitsBlockErrorCounter((ThaModuleSdh)AtChannelModuleGet((AtChannel)self)))
        maxValue = cBit23_0;
    else
        maxValue = cBit15_0;

    return ThaSdhChannelCounterFromReadOnlyCounterGet((AtChannel)self, newCounter, lastValue, maxValue, counterType, clear);
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
    return BlockErrorCounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
    return BlockErrorCounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool OverheadByteIsValid(ThaDefaultPohProcessorV2 self, uint32 overheadByte)
    {
	AtUnused(overheadByte);
	AtUnused(self);
    /* Concrete must do */
    return cAtFalse;
    }

static uint32 PohCapturedTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return 0x2000;
    }

static uint32 PohTxTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return 0x4000;
    }

static eBool TxPointerJustificationsExist(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    uint32 channelType = AtSdhChannelTypeGet(sdhChannel);

    switch (channelType)
        {
        /* Pointer Generation Justification Counters do not exist for the case that VC contains sub mapping
         * If vc contains sub mapping but it is bounded to pw,
         * that is in case of CEP fractional, Pointer generation counters exist */
        case cAtSdhChannelTypeVc4:
            if (AtSdhChannelMapTypeGet(sdhChannel) != cAtSdhVcMapTypeVc4Map3xTug3s)
                return cAtTrue;

            if (AtChannelBoundPwGet((AtChannel)sdhChannel))
                return cAtTrue;

            return cAtFalse;

        case cAtSdhChannelTypeVc3:
            if (AtSdhChannelMapTypeGet(sdhChannel) != cAtSdhVcMapTypeVc3Map7xTug2s)
                return cAtTrue;

            if (AtChannelBoundPwGet((AtChannel)sdhChannel))
                return cAtTrue;

            return cAtFalse;

        default:
            return cAtTrue;
        }
    }

static uint32 PointerAdjustCounterRead2Clear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    uint32 offset, address;
    const eBool cDecrease = cAtTrue;
    const eBool cIncrease = cAtFalse;

    /* Detection */
    if (counterType == cAtSdhPathCounterTypeRxPPJC)
        {
        offset = mMethodsGet(mThis(self))->RxPointerAdjustCounterOffset(mThis(self), read2Clear, cIncrease);
        address = mMethodsGet(mThis(self))->RxPointerAdjustCounterAddress(mThis(self));
        return mChannelHwRead(self, address + offset, cThaModuleOcn) & cThaRegOcnPtrAdjCntMask;
        }

    if (counterType == cAtSdhPathCounterTypeRxNPJC)
        {
        offset = mMethodsGet(mThis(self))->RxPointerAdjustCounterOffset(mThis(self), read2Clear, cDecrease);
        address = mMethodsGet(mThis(self))->RxPointerAdjustCounterAddress(mThis(self));
        return mChannelHwRead(self, address + offset, cThaModuleOcn) & cThaRegOcnPtrAdjCntMask;
        }

    /* Generation */
    if (TxPointerJustificationsExist(self))
        {
        if (counterType == cAtSdhPathCounterTypeTxPPJC)
            {
            offset = mMethodsGet(mThis(self))->TxPointerAdjustCounterOffset(mThis(self), read2Clear, cIncrease);
            address = mMethodsGet(mThis(self))->TxPointerAdjustCounterAddress(mThis(self));
            return mChannelHwRead(self, address + offset, cThaModuleOcn) & cThaRegOcnPtrAdjCntMask;
            }

        if (counterType == cAtSdhPathCounterTypeTxNPJC)
            {
            offset = mMethodsGet(mThis(self))->TxPointerAdjustCounterOffset(mThis(self), read2Clear, cDecrease);
            address = mMethodsGet(mThis(self))->TxPointerAdjustCounterAddress(mThis(self));
            return mChannelHwRead(self, address + offset, cThaModuleOcn) & cThaRegOcnPtrAdjCntMask;
            }
        }

    return 0;
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool read2Clear)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;
    uint32 newCounter;
    AtChannel channel = (AtChannel)self;

    /* BIP should be handled first to fast response BER monitoring task */
    if (counterType == cAtSdhPathCounterTypeBip)
        {
        uint32 resultCounterValue;
        newCounter = mMethodsGet(channel)->ReadOnlyCntGet(channel, cAtSdhPathCounterTypeBip);

        /* Calculate result counter needs to be returned */
        if (newCounter < processor->bipCnt)
            resultCounterValue = newCounter + (cBit23_0 - processor->bipCnt) + 1;
        else
            resultCounterValue = newCounter - processor->bipCnt;

        /* Only need to cache last value for next reading when reading the counter
         * in read to clear mode */
        if (read2Clear)
            processor->bipCnt = newCounter;

        return resultCounterValue;
        }

    if (counterType == cAtSdhPathCounterTypeRei)
        {
        newCounter = AtChannelReadOnlyCntGet(channel, cAtSdhPathCounterTypeRei);
        return ThaSdhChannelCounterFromReadOnlyCounterGet(channel, newCounter, &(processor->reiCnt), cBit23_0, cAtSdhPathCounterTypeRei, read2Clear);
        }

    return PointerAdjustCounterRead2Clear(self, counterType, read2Clear);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static uint32 RxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    AtUnused(self);
    AtUnused(read2Clear);
    AtUnused(isDecrease);
    return 0;
    }

static uint32 TxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    /* Sub-class will do */
    AtUnused(self);
    AtUnused(read2Clear);
    AtUnused(isDecrease);
    return 0;
    }

static uint32 RxPointerAdjustCounterAddress(ThaDefaultPohProcessorV2 self)
    {
    /* Sub-class will do */
    AtUnused(self);
    return 0;
    }

static uint32 TxPointerAdjustCounterAddress(ThaDefaultPohProcessorV2 self)
    {
    /* Sub-class will do */
    AtUnused(self);
    return 0;
    }

static eBool AllBytesInBufferAreIdentical(uint8* buffer)
    {
    uint8 byte_i;
    for (byte_i = 1 ; byte_i < cAtSdhChannelMaxTtiLength; byte_i++)
        {
        if (buffer[byte_i] != buffer[0])
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool BufferContainsCrLf(uint8* buffer, uint8* startMsg)
    {
    uint8 byte_i;
    const uint8 CR = 0xd;
    const uint8 LF = 0xa;

    *startMsg = 0;
    for (byte_i = 0 ; byte_i < cAtSdhChannelMaxTtiLength; byte_i++)
        {
        if (buffer[byte_i] != CR)
            continue;

        if (buffer[byte_i + 1] == LF)
            {
            *startMsg = (uint8)(byte_i + 2);
            return cAtTrue;
            }
        }

    return cAtFalse;
    }

static void CopyMsg(uint8* buffer, uint8* msg, uint8 startMsg, uint8 numBytes)
    {
    uint8 msgByte = startMsg;
    uint8 byte_i;

    for (byte_i = 0; byte_i < numBytes; byte_i++)
        {
        if (msgByte >= numBytes)
            msgByte = 0;

        msg[byte_i] = buffer[msgByte];
        msgByte++;
        }
    }

static void Copy64BytesMsg(uint8* buffer, uint8* msg, uint8 startMsg)
    {
    CopyMsg(buffer, msg, startMsg, cAtSdhChannelMaxTtiLength);
    }

static void Copy16BytesMsg(uint8* buffer, uint8* msg, uint8 startMsg)
    {
    CopyMsg(buffer, msg, startMsg, 16);
    }

static eBool FindCrcByte(uint8* buffer, uint8* startMsg)
    {
    uint8 byte_i;
    uint8 tmpBuffer[16];

    for (byte_i = 0; byte_i < 16; byte_i++)
        {
        Copy16BytesMsg(buffer, tmpBuffer, (uint8)(byte_i + 1));
        tmpBuffer[15] = 0;
        AtCrc7Ins(tmpBuffer, 16);
        if (buffer[byte_i] == tmpBuffer[0])
            {
            *startMsg = (uint8)(byte_i + 1);
            return cAtTrue;
            }
        }

    return cAtFalse;
    }

/* Analyze tti from buffer gotten by float mode */
eAtRet ThaSdhTtiFloatBufferAnalyze(tAtSdhTti *tti)
    {
    uint8 startMsg;
    uint8 buffer[cAtSdhChannelMaxTtiLength];

    AtOsalMemCpy(buffer, tti->message, cAtSdhChannelMaxTtiLength);
    AtOsalMemInit(tti->message, 0, cAtSdhChannelMaxTtiLength);
    if (AllBytesInBufferAreIdentical(buffer))
        {
        tti->mode = cAtSdhTtiMode1Byte;
        tti->message[0] = buffer[0];
        return cAtOk;
        }

    if (BufferContainsCrLf(buffer, &startMsg))
        {
        tti->mode = cAtSdhTtiMode64Byte;
        Copy64BytesMsg(buffer, tti->message, startMsg);
        return cAtOk;
        }

    if (FindCrcByte(buffer, &startMsg))
        {
        tti->mode = cAtSdhTtiMode16Byte;
        Copy16BytesMsg(buffer, tti->message, startMsg);
        return cAtOk;
        }

    return cAtError;
    }

static eAtRet TxTtiModeWarmRestore(ThaDefaultPohProcessorV2 self)
    {
    eAtRet ret;
    tAtSdhTti tti;

    tti.mode = cAtSdhTtiMode64Byte;
    ret = ThaSdhTtiRead((AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self), &tti, mMethodsGet(self)->PohTxTtiAddressGet(self));

    if (ret != cAtOk)
        return ret;

    ret = ThaSdhTtiFloatBufferAnalyze(&tti);
    if (ret == cAtOk)
        return TxTtiModeSet(self, tti.mode);

    return TxTtiModeSet(self, cAtSdhTtiMode1Byte);
    }

static eAtRet WarmRestore(AtChannel self)
    {
    /* There are some counters (BIP, REI) just support read-only mode,
     * to provide read-to-clear mode, software need to compute current value get from HW
     * with value saved in channel's database. When warm restoring, we need to get current value from HW and
     * and save to channel's database. With this approach, errors happened during
     * the time at which software was stopped to the time channel is restored will be unseen.
     *
     * Another solution to make error counters exact is saving counter value each time we get to HW
     * storage, performance will be reduced when BER is running and
     * current HW storage space is also not enough to that.
     */
    AtChannelCounterClear(self, cAtSdhPathCounterTypeBip);
    AtChannelCounterClear(self, cAtSdhPathCounterTypeRei);

    return mMethodsGet(mThis(self))->TxTtiModeWarmRestore(mThis(self));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaDefaultPohProcessorV2 object = (ThaDefaultPohProcessorV2)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(txTtiMode);
    mEncodeUInt(txPsl);
    mEncodeUInt(pslCached);
    mEncodeNone(bipCnt);
    mEncodeNone(reiCnt);
    mEncodeNone(bipBlockCnt);
    mEncodeNone(reiBlockCnt);
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    AtUnused(self);

    /* When LOP/AIS happen, AIS is always inserted downstream by standard.
     * LOP and AIS affecting can not be changed */
    if ((alarmType == cAtSdhPathAlarmAis) || (alarmType == cAtSdhPathAlarmLop))
        return cAtFalse;

    return cAtTrue;
    }

static void MethodsInit(ThaDefaultPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, RxTtiModeGet);
        mMethodOverride(m_methods, RxTtiModeSet);
        mMethodOverride(m_methods, PohExpectedTtiAddressGet);
        mMethodOverride(m_methods, PohCapturedTtiAddressGet);
        mMethodOverride(m_methods, PohTxTtiAddressGet);
        mMethodOverride(m_methods, BlockCounterReadOnly);
        mMethodOverride(m_methods, BlockErrorCounterOffset);
        mMethodOverride(m_methods, OverheadByteIsValid);
        mMethodOverride(m_methods, PohCapturedTtiBaseAddress);
        mMethodOverride(m_methods, PohTxTtiBaseAddress);
        mMethodOverride(m_methods, RxPointerAdjustCounterAddress);
        mMethodOverride(m_methods, TxPointerAdjustCounterAddress);
        mMethodOverride(m_methods, RxPointerAdjustCounterOffset);
        mMethodOverride(m_methods, TxPointerAdjustCounterOffset);
        mMethodOverride(m_methods, TxTtiModeWarmRestore);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(ThaDefaultPohProcessorV2 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhChannel(ThaDefaultPohProcessorV2 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeAlarmAffecting);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(ThaDefaultPohProcessorV2 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaDefaultPohProcessorV2 self)
    {
    OverrideAtObject(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    }

ThaDefaultPohProcessorV2 ThaDefaultPohProcessorV2ObjectInit(ThaDefaultPohProcessorV2 self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPohProcessorObjectInit((ThaPohProcessor)self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/* Helper function to write TTI message */
eAtRet ThaSdhTtiWrite(AtSdhChannel sdhChannel, const tAtSdhTti *tti, uint32 indirectAddr)
    {
    uint8 msgLength;
    uint8 msgBuffer[cAtSdhChannelMaxTtiLength];
    uint8 i;
    eAtRet ret;
    ThaModulePohV2 pohModule;
    AtOsal osal;

    ret = ThaTtiMsgGet(tti, msgBuffer, NULL, &msgLength);
    if (ret != cAtOk)
        return ret;

    /* Make a 64-bytes buffer */
    osal = AtSharedDriverOsalGet();
    if (tti->mode == cAtSdhTtiMode1Byte)
        mMethodsGet(osal)->MemInit(osal, msgBuffer, tti->message[0], sizeof(msgBuffer));
    else if (tti->mode == cAtSdhTtiMode16Byte)
        {
        for (i = 1; i < (cAtSdhChannelMaxTtiLength / msgLength); i++)
            mMethodsGet(osal)->MemCpy(osal, &(msgBuffer[i * msgLength]), msgBuffer, msgLength);
        }

    pohModule = (ThaModulePohV2)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhChannel), cThaModulePoh);
    return TtiBufferWrite(pohModule, sdhChannel, msgBuffer, cAtSdhChannelMaxTtiLength, indirectAddr);
    }

/* Helper function to read TTI message */
eAtRet ThaSdhTtiRead(AtSdhChannel self, tAtSdhTti *tti, uint32 indirectAddr)
    {
    uint32 regVal = 0;
    uint32 elapse;
    tAtOsalCurTime curTime, startTime;
    uint8 numByteInJ0Msg, byteId, position;
    uint8 *pMsg;
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint32 partOffset = ChannelPartOffset(self);
    uint32 ctrlRegAddr = cThaPohIndirectAccessCtrl + partOffset;
    uint32 indirectAddressMask;
    uint8 indirectAddressShift;
    ThaModulePohV2 pohModule = (ThaModulePohV2)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePoh);

    if (NULL == sdhModule)
        return cAtError;

    indirectAddressMask = mMethodsGet(pohModule)->ThaPohIndirectAddressMask(pohModule);
    indirectAddressShift = mMethodsGet(pohModule)->ThaPohIndirectAddressShift(pohModule);

    /* Get message */
    numByteInJ0Msg = AtSdhTtiLengthForTtiMode(tti->mode);
    pMsg = tti->message;
    for (byteId = 0; byteId < numByteInJ0Msg; byteId++)
        {
        position = byteId % cThaSonetNumBytesInDword;

        /* Get 32-bits data from HW */
        if (position == 0)
            {
            /* Make a read command */
            regVal = 0;
            mRegFieldSet(regVal, indirectAddress, indirectAddr++);
            mFieldIns(&regVal, cThaPohIndirectRnWMask, cThaPohIndirectRnWShift, 1);
            mFieldIns(&regVal, cThaPohIndirectAcsEnMask, cThaPohIndirectAcsEnShift, 1);
            mModuleHwWrite(sdhModule, ctrlRegAddr, regVal);

            /* Wait until HW done */
            regVal = mModuleHwRead(sdhModule, ctrlRegAddr);
            if ((regVal & cThaPohIndirectAcsEnMask) != 0)
                {
                /* Get starting time */
                elapse = 0;
                mMethodsGet(osal)->CurTimeGet(osal, &startTime);
                while (elapse < cThaPohIndirectAcsTimeOut)
                    {
                    regVal = mModuleHwRead(sdhModule, ctrlRegAddr);
                    if ((regVal & cThaPohIndirectAcsEnMask) == 0)
                        break;

                    /* Calculate elapse time */
                    mMethodsGet(osal)->CurTimeGet(osal, &curTime);
                    elapse = mTimeIntervalInMsGet(startTime, curTime);
                    }

                /* Timeout */
                if (elapse >= cThaPohIndirectAcsTimeOut)
                    return cAtFalse;
                }

            /* HW finished, get value */
            regVal = mModuleHwRead(sdhModule, cThaPohIndirectAcsData + partOffset);
            }

        /* If this is 16-byte message, save the first CRC byte */
        if ((tti->mode == cAtSdhTtiMode16Byte) && (byteId == 0))
            {
            mFieldGet(regVal, cThaOcnJnMsgMask(position), cThaOcnJnMsgShift(position), uint8, &(tti->crc));
            continue;
            }

        /* Put this byte to buffer */
        mFieldGet(regVal, cThaOcnJnMsgMask(position), cThaOcnJnMsgShift(position), uint8, pMsg);
        pMsg++;
        }

    return cAtOk;
    }

eAtSdhTtiMode ThaDefaultPohProcessorV2TtiModeHw2Sw(uint8 value)
    {
    if (value == 0) return cAtSdhTtiMode1Byte;
    if (value == 1) return cAtSdhTtiMode16Byte;
    if (value == 2) return cAtSdhTtiMode64Byte;

    return cAtSdhTtiMode1Byte;
    }

uint8 ThaDefaultPohProcessorV2TtiHwValueByModeGet(eAtSdhTtiMode ttiMode)
    {
    if (ttiMode == cAtSdhTtiMode1Byte)  return 0;
    if (ttiMode == cAtSdhTtiMode16Byte) return 1;
    if (ttiMode == cAtSdhTtiMode64Byte) return 2;

    return 0;
    }

eAtRet ThaDefaultPohProcessorV2TxUneqForceByPsl(ThaDefaultPohProcessorV2 self, eBool force)
    {
    eAtRet ret = cAtOk;
    AtSdhPath path = (AtSdhPath)self;

    /* Make TX PSL be zero */
    if (force)
        {
        const uint8 cUneqPsl = 0;

        /* Save application configuration PSL */
        if (!mThis(self)->pslCached)
            mThis(self)->txPsl = AtSdhPathTxPslGet(path);
        
        /* Force PSL to zero to indicate UNEQ */
        ret = AtSdhPathTxPslSet(path, cUneqPsl);
        if (ret == cAtOk)
            mThis(self)->pslCached = cAtTrue;

        return ret;
        }

    /* Restore application configured VSL */
    if (mThis(self)->pslCached)
        {
        ret = AtSdhPathTxPslSet(path, mThis(self)->txPsl);
        mThis(self)->pslCached = cAtFalse;
        return ret;
        }

    return cAtOk;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaDefaultPohProcessorV2Internal.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : Default POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADEFAULTPOHPROCESSORV2INTERNAL_H_
#define _THADEFAULTPOHPROCESSORV2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaPohProcessorInternal.h"
#include "ThaDefaultPohProcessorV2.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../sdh/ThaModuleSdhReg.h"
#include "../../man/ThaDeviceInternal.h"


/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define mPohPartOffset(self) (((ThaPohProcessor)self)->partOffset)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDefaultPohProcessorV2Methods
    {
    /* Internal method */
    eAtSdhTtiMode (*RxTtiModeGet)(ThaDefaultPohProcessorV2 self);
    eAtRet (*RxTtiModeSet)(ThaDefaultPohProcessorV2 self, eAtSdhTtiMode ttiMode);

    uint32 (*PohExpectedTtiAddressGet)(ThaDefaultPohProcessorV2 self);
    uint32 (*PohTxTtiAddressGet)(ThaDefaultPohProcessorV2 self);
    uint32 (*PohTxTtiBaseAddress)(ThaDefaultPohProcessorV2 self);
    uint32 (*PohCapturedTtiAddressGet)(ThaDefaultPohProcessorV2 self);
    uint32 (*PohCapturedTtiBaseAddress)(ThaDefaultPohProcessorV2 self);

    uint32 (*BlockErrorCounterOffset)(ThaDefaultPohProcessorV2 self, uint32 counterType);
    uint32 (*BlockCounterReadOnly)(ThaDefaultPohProcessorV2 self, uint32 counterType);
    eBool  (*OverheadByteIsValid)(ThaDefaultPohProcessorV2 self, uint32 overheadByte);

    uint32 (*RxPointerAdjustCounterAddress)(ThaDefaultPohProcessorV2 self);
    uint32 (*RxPointerAdjustCounterOffset)(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease);

    uint32 (*TxPointerAdjustCounterAddress)(ThaDefaultPohProcessorV2 self);
    uint32 (*TxPointerAdjustCounterOffset)(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease);

    eAtRet (*TxTtiModeWarmRestore)(ThaDefaultPohProcessorV2 self);
    }tThaDefaultPohProcessorV2Methods;

typedef struct tThaDefaultPohProcessorV2
    {
    tThaPohProcessor super;
    const tThaDefaultPohProcessorV2Methods *methods;

    /* Private data */
    uint8 txTtiMode;
    uint32 bipCnt;
    uint32 reiCnt;
    uint32 bipBlockCnt;
    uint32 reiBlockCnt;

    /* To save application configuration before forcing alarm */
    uint8 txPsl;
    eBool pslCached;
    }tThaDefaultPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/
/* TODO: Review and clean these declarations */
extern eAtSdhTtiMode ThaDefaultPohProcessorV2TtiModeHw2Sw(uint8 value);
extern uint8 ThaDefaultPohProcessorV2TtiHwValueByModeGet(eAtSdhTtiMode ttiMode);
extern uint32 ThaDefaultPohProcessorV2CounterGet(ThaDefaultPohProcessorV2 self, uint16 counterType, eBool read2Clear);
extern eAtRet ThaTtiMsgGet(const tAtSdhTti *tti, uint8 *pTtiMsg, uint8 *pHwJ0Md, uint8 *pJ0Len);

/*--------------------------- Entries ----------------------------------------*/
ThaDefaultPohProcessorV2 ThaDefaultPohProcessorV2ObjectInit(ThaDefaultPohProcessorV2 self, AtSdhVc vc);
eAtRet ThaDefaultPohProcessorV2TxUneqForceByPsl(ThaDefaultPohProcessorV2 self, eBool force);

#ifdef __cplusplus
}
#endif
#endif /* _THADEFAULTPOHPROCESSORV2INTERNAL_H_ */


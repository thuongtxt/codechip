/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaAuVcPohProcessorV2.c
 *
 * Created Date: Mar 18, 2013
 *
 * Description : AU's VC POH processor
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaAuVcPohProcessorV2Internal.h"
#include "../ThaModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* G1 sub fields */
#define cSpareBitMask  cBit0
#define cSpareBitShift 0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaAuVcPohProcessorV2)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAuVcPohProcessorV2Methods m_methods;

/* Override */
static tAtObjectMethods                 m_AtObjectOverride;
static tAtSdhChannelMethods             m_AtSdhChannelOverride;
static tAtChannelMethods                m_AtChannelOverride;
static tAtSdhPathMethods                m_AtSdhPathOverride;
static tThaDefaultPohProcessorV2Methods m_ThaDefaultPohProcessorV2Override;
static tThaPohProcessorMethods          m_ThaPohProcessorOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaAuVcPohProcessorV2);
    }

static uint32 PohCpeStsTu3DefaultOffset(ThaAuVcPohProcessorV2 self)
    {
    uint8 slice, hwStsInSlice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return hwStsInSlice + ThaPohProcessorPartOffset((ThaPohProcessor)self);

    return 0;
    }

static ThaModulePohV2 ModulePoh(ThaAuVcPohProcessorV2 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    return (ThaModulePohV2)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint8 StsMax(ThaAuVcPohProcessorV2 self)
    {
    return ThaModulePohV2NumSts(ModulePoh(self));
    }

static uint32 PohCpeStsTu3PohOffset(ThaAuVcPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax(self);
    uint8 slice, hwStsInSlice;
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (STSMAX * VTMAX) + hwStsInSlice + ThaPohProcessorPartOffset((ThaPohProcessor)self);

    return 0;
    }

static uint32 OcnStsDefaultOffset(ThaAuVcPohProcessorV2 self)
    {
    return ThaModuleOcnStsDefaultOffset(mModuleOcn(self), mVcGet(self), AtSdhChannelSts1Get(mVcGet(self)));
    }

static eBool IsOneBitRdi(uint8 erdiValue)
    {
    return (erdiValue & cBit2) ? cAtTrue : cAtFalse;
    }

static uint32 ErdiTypeFromErdiValue(uint8 g1Value, eBool erdiIsEnable)
    {
    uint8  erdiValue = (uint8)((g1Value & cBit3_1) >> 1);

    /* E-RDI is not enabled, just only check one-bit RDI */
    if (!erdiIsEnable)
        return IsOneBitRdi(erdiValue) ? cAtSdhPathAlarmRdi : 0;

    /* ERDI Payload defect */
    if (erdiValue == cERDIPayloadValue)
        return cAtSdhPathAlarmErdiP;

    /* ERDI Connectivity defect */
    if (erdiValue == cERDIConnectivityValue)
        return cAtSdhPathAlarmErdiC;

    /* ERDI Server defect */
    if ((erdiValue & cBit2) || (erdiValue == cERDIServerValue))
        return cAtSdhPathAlarmErdiS;

    return 0;
    }

static uint32 PohCpeStsTu3Ctrl(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohCpeStsTu3Ctrl;
    }

static uint32 Tim2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaTim2AisEnbMask;
    }

static uint32 Tim2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaTim2AisEnbShift;
    }

static uint32 Uneq2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaUneq2AisEnbMask;
    }

static uint32 Uneq2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaUneq2AisEnbShift;
    }

static uint32 Plm2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPlm2AisEnbMask;
    }

static uint32 Plm2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPlm2AisEnbShift;
    }

static ThaModuleSdh ModuleSdh(ThaAuVcPohProcessorV2 self)
    {
    return (ThaModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 auPohProcessor = (ThaAuVcPohProcessorV2)self;
    ThaPohProcessor pohProcessor = (ThaPohProcessor)self;
    eBool hwEnable = enable ? 1 : 0;

    if (!ThaPohProcessorCanEnableAlarmAffecting(pohProcessor, alarmType, enable))
        return cAtErrorModeNotSupport;

    /* Get current configuration */
    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);
    regVal  = mChannelHwRead(self, address, cThaModulePoh);

    if (alarmType & cAtSdhPathAlarmTim)
        mFieldIns(&regVal,
                  mMethodsGet(mThis(self))->Tim2AisEnbMask(mThis(self)),
                  mMethodsGet(mThis(self))->Tim2AisEnbShift(mThis(self)),
                  hwEnable);

    if (alarmType & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal,
                  mMethodsGet(mThis(self))->Plm2AisEnbMask(mThis(self)),
                  mMethodsGet(mThis(self))->Plm2AisEnbShift(mThis(self)),
                  hwEnable);

    if (alarmType & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal,
                  mMethodsGet(mThis(self))->Uneq2AisEnbMask(mThis(self)),
                  mMethodsGet(mThis(self))->Uneq2AisEnbShift(mThis(self)),
                  hwEnable);

    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    if (!ThaModuleSdhShouldUpdateTimRdiBackwardWhenAisDownstreamChange(ModuleSdh(auPohProcessor)))
        return cAtOk;

    if (alarmType & cAtSdhPathAlarmTim)
        return ThaPohProcessorAutoRdiEnable(pohProcessor, cAtSdhPathAlarmTim, ThaPohProcessorTimBackwardRdiShouldEnableWhenAisDownstream(pohProcessor, enable));

    return cAtOk;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 auPohProcessor = (ThaAuVcPohProcessorV2)self;
    uint32 mask = cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;

    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    /* Get current configuration */
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    if (regVal & mMethodsGet(mThis(self))->Tim2AisEnbMask(mThis(self)))
        mask |= cAtSdhPathAlarmTim;

    if (regVal & mMethodsGet(mThis(self))->Plm2AisEnbMask(mThis(self)))
        mask |= cAtSdhPathAlarmPlm;

    if (regVal & mMethodsGet(mThis(self))->Uneq2AisEnbMask(mThis(self)))
        mask |= cAtSdhPathAlarmUneq;

    return mask;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 auPohProcessor = (ThaAuVcPohProcessorV2)self;

    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    /* Get current configuration */
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    if ((alarmType & cAtSdhPathAlarmAis) || (alarmType & cAtSdhPathAlarmLop))
        return cAtTrue;

    if (alarmType & cAtSdhPathAlarmTim)
        return (regVal & mMethodsGet(mThis(self))->Tim2AisEnbMask(mThis(self))) ? cAtTrue : cAtFalse;

    if (alarmType & cAtSdhPathAlarmPlm)
        return (regVal & mMethodsGet(mThis(self))->Plm2AisEnbMask(mThis(self))) ? cAtTrue : cAtFalse;

    if (alarmType & cAtSdhPathAlarmUneq)
        return (regVal & mMethodsGet(mThis(self))->Uneq2AisEnbMask(mThis(self))) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint32 PohCpeStsTu3PathOhGrabber1(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegPohCpeStsTu3PathOhGrabber1;
    }

static uint32 Grabber1Read(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 vcProcessor = mThis(self);
    uint32 regAddr = mMethodsGet(vcProcessor)->PohCpeStsTu3PathOhGrabber1(vcProcessor) +
                     mMethodsGet(vcProcessor)->PohCpeStsTu3PohOffset(vcProcessor);

    return mChannelHwRead(self, regAddr, cThaModulePoh);
    }

static uint32 PohCpeStsTu3PathOhGrabber2(ThaAuVcPohProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cThaRegPohCpeStsTu3PathOhGrabber2;
    }

static uint32 Grabber2Read(AtSdhPath self)
    {
    ThaAuVcPohProcessorV2 vcProcessor = mThis(self);
    uint32 regAddr = mMethodsGet(vcProcessor)->PohCpeStsTu3PathOhGrabber2(vcProcessor) +
                     mMethodsGet(vcProcessor)->PohCpeStsTu3PohOffset(vcProcessor);
    return mChannelHwRead(self, regAddr, cThaModulePoh);
    }

static uint8 RxG1Get(AtSdhPath self)
    {
    return (uint8)mRegField(Grabber1Read(self), cThaRegPohCpeStsTu3PathG1Grabber);
    }

static uint32 PohCpeStsTu3AlarmStatus(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPohRegSpeStsTu3AlarmStatus;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 address, regVal;
    uint32 alarmState = 0;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    AtBerController berController;

    /* Read status */
    address = mMethodsGet(processor)->PohCpeStsTu3AlarmStatus(processor) +
              mMethodsGet(processor)->PohCpeStsTu3PohOffset(processor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Convert to software value */
    if (regVal & cThaPohAlarmUneqMask)
        alarmState |= cAtSdhPathAlarmUneq;
    if (regVal & cThaPohAlarmPlmMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohAlarmTtiTimMask)
        alarmState |= cAtSdhPathAlarmTim;
    if (regVal & cThaPohAlarmVcAisMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohAlarmRdiStbMask)
        alarmState |= ErdiTypeFromErdiValue(RxG1Get((AtSdhPath)self), AtSdhPathERdiIsEnabled((AtSdhPath)self));

    /* SD, SF */
    berController = SdhChannelBerControllerGet(mVcGet(self));
    if (AtBerControllerIsSd(berController))
        alarmState |= cAtSdhPathAlarmBerSd;
    if (AtBerControllerIsSf(berController))
        alarmState |= cAtSdhPathAlarmBerSf;
    if (AtBerControllerIsTca(berController))
        alarmState |= cAtSdhPathAlarmBerTca;

    /* Payload UNEQ */
    address = cThaRegOcnRxStsVCperAlmCurrentStat + mMethodsGet(processor)->OcnStsDefaultOffset(processor);
    if (mChannelHwRead(self, address, cThaModuleOcn) & cThaStsPiUneqPCurStatMask)
        alarmState |= cAtSdhPathAlarmPayloadUneq;

    return alarmState;
    }

static uint32 PohCpeStsTu3AlarmIntrSticky(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPohRegCpeStsTu3AlarmIntrSticky;
    }

static uint32 DefectInterruptRead2Clear(AtChannel self, eBool readToClear)
    {
    uint32 address, regVal;
    uint32 alarmState = 0;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    AtBerController berController;

    /* Get hardware status */
    address = mMethodsGet(processor)->PohCpeStsTu3AlarmIntrSticky(processor) +
              mMethodsGet(processor)->PohCpeStsTu3PohOffset(processor);

    regVal = mChannelHwRead(self, address, cThaModulePoh);
    if (readToClear)
        mChannelHwWrite(self, address, regVal, cThaModulePoh);

    /* Return software value */
    if (regVal & cThaPohTtiTimStbChgIntMask)
        alarmState |= cAtSdhPathAlarmTim;
    if (regVal & cThaPohPlmStbChgIntMask)
        alarmState |= cAtSdhPathAlarmPlm;
    if (regVal & cThaPohVcAisStbChgIntMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohUneqStbChgIntMask)
        alarmState |= cAtSdhPathAlarmUneq;
    if (regVal & cThaPohVcAisStbChgIntMask)
        alarmState |= cAtSdhPathAlarmAis;
    if (regVal & cThaPohRdiStbChgIntMask)
        alarmState |= cAtSdhPathAlarmRdi;

    /* Get SD, SF */
    berController = SdhChannelBerControllerGet((AtSdhChannel)self);
    if (AtBerControllerSdHistoryGet(berController, readToClear))
        alarmState |= cAtSdhPathAlarmBerSd;
    if (AtBerControllerSfHistoryGet(berController, readToClear))
        alarmState |= cAtSdhPathAlarmBerSf;

    /* Payload UNEQ */
    address = cThaRegOcnRxStsVCperAlmIntrStat + mMethodsGet(processor)->OcnStsDefaultOffset(processor);
    if (mChannelHwRead(self, address, cThaModuleOcn) & cThaStsPiUneqPStateChgIntrMask)
        alarmState |= cAtSdhPathAlarmPayloadUneq;
    if (readToClear)
        mChannelHwWrite(self, address, cThaStsPiUneqPStateChgIntrMask, cThaModuleOcn);

    return alarmState;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectInterruptRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectInterruptRead2Clear(self, cAtTrue);
    }

static uint32 PohCpeStsTu3AlarmIntrEnable(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPohRegCpeStsTu3AlarmIntrEnable;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtSdhPathAlarmAis   |
            cAtSdhPathAlarmTim   |
            cAtSdhPathAlarmPlm   |
            cAtSdhPathAlarmUneq  |
            cAtSdhPathAlarmRdi   |
            cAtSdhPathAlarmErdiS |
            cAtSdhPathAlarmErdiP |
            cAtSdhPathAlarmErdiC);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 vc = (ThaAuVcPohProcessorV2)self;

    /* Get current configuration */
    address = mMethodsGet(vc)->PohCpeStsTu3AlarmIntrEnable(vc) +
              mMethodsGet(vc)->PohCpeStsTu3PohOffset(vc);

    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Configure AIS mask */
    if (defectMask & cAtSdhPathAlarmAis)
        mFieldIns(&regVal,
                  cThaPohVcAisStbIntEnbMask,
                  cThaPohVcAisStbIntEnbShift,
                  (enableMask & cAtSdhPathAlarmAis) ? 1 : 0);

    /* Configure TTI mask */
    if (defectMask & cAtSdhPathAlarmTim)
        mFieldIns(&regVal,
                  cThaPohTtiTimStbIntEnbMask,
                  cThaPohTtiTimStbIntEnbShift,
                  (enableMask & cAtSdhPathAlarmTim) ? 1 : 0);

    /* Configure PLM mask */
    if (defectMask & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal,
                  cThaPohPlmStbIntEnbMask,
                  cThaPohPlmStbIntEnbShift,
                  (enableMask & cAtSdhPathAlarmPlm) ? 1 : 0);

    /* Configure UNEQ mask */
    if (defectMask & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal,
                  cThaPohUneqStbIntEnbMask,
                  cThaPohUneqStbIntEnbShift,
                  (enableMask & cAtSdhPathAlarmUneq) ? 1 : 0);

    /* Configure RDI mask */
    if (defectMask & (cAtSdhPathAlarmRdi | cAtSdhPathAlarmErdiS | cAtSdhPathAlarmErdiP | cAtSdhPathAlarmErdiC))
        mFieldIns(&regVal,
                  cThaPohRdiStbChgIntEnbMask,
                  cThaPohRdiStbChgIntEnbShift,
                  (enableMask & cAtSdhPathAlarmRdi) ? 1 : 0);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regVal, address, swMask = 0;
    ThaAuVcPohProcessorV2 vc = (ThaAuVcPohProcessorV2)self;

    /* Get current configuration */
    address = mMethodsGet(vc)->PohCpeStsTu3AlarmIntrEnable(vc) +
              mMethodsGet(vc)->PohCpeStsTu3PohOffset(vc);

    regVal = mChannelHwRead(self, address, cThaModulePoh);

    /* Get AIS mask */
    if (regVal & cThaPohVcAisStbIntEnbMask)
        swMask |= cAtSdhPathAlarmAis;

    /* Get TTI mask */
    if (regVal & cThaPohTtiTimStbIntEnbMask)
        swMask |= cAtSdhPathAlarmTim;

    /* Get PLM mask */
    if (regVal & cThaPohPlmStbIntEnbMask)
        swMask |= cAtSdhPathAlarmPlm;

    /* Get UNEQ mask */
    if (regVal & cThaPohUneqStbIntEnbMask)
        swMask |= cAtSdhPathAlarmUneq;

    /* Get RDI mask */
    if (regVal & cThaPohRdiStbChgIntEnbMask)
        swMask |= cAtSdhPathAlarmRdi;

    return swMask;
    }

static eAtRet TxRdiForce(AtChannel self, eBool enable)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) +
                     mMethodsGet(processor)->PohCpeStsTu3PohOffset(processor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);

    if (enable)
        mFieldIns(&regVal, cThaRdiInsValMask, cThaRdiInsValShift, 0x1);
    mFieldIns(&regVal, cThaRdiInsEnbMask, cThaRdiInsEnbShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool VcIsChannelized(AtChannel self)
    {
    AtSdhChannel vc = (AtSdhChannel)ThaPohProcessorVcGet((ThaPohProcessor)self);
    eAtSdhVcMapType mapType = AtSdhChannelMapTypeGet(vc);

    if ((mapType == cAtSdhVcMapTypeVc4Map3xTug3s) ||
        (mapType == cAtSdhVcMapTypeVc3Map7xTug2s))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ForceUneqOnStsRegister(AtChannel self, eBool enable)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;

    address = cThaRegOcnTxPohInsPerChnCtrl + mMethodsGet(processor)->OcnStsDefaultOffset(processor);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPg2StsUneqPFrcMask, cThaTxPg2StsUneqPFrcShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static uint32 OcnStsVtDefaultOffset(ThaAuVcPohProcessorV2 self)
    {
    const uint8 cDontCareVtg = 0;
    const uint8 cDontCareVt = 0;

    return ThaModuleOcnStsVtDefaultOffset(mModuleOcn(self), mVcGet(self), AtSdhChannelSts1Get(mVcGet(self)), cDontCareVtg, cDontCareVt);
    }

static eAtRet ForceUneqOnStsVtRegister(AtChannel self, eBool enable)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;

    address = cThaRegOcnTxPGPerChnCtrl + OcnStsVtDefaultOffset(processor);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    mFieldIns(&regVal, cThaTxPgStsVtTuUneqPFrcMask, cThaTxPgStsVtTuUneqPFrcShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet TxUneqForce(AtChannel self, eBool enable)
    {
    if (!VcIsChannelized(self))
        return ForceUneqOnStsVtRegister(self, enable);

    ThaDefaultPohProcessorV2TxUneqForceByPsl((ThaDefaultPohProcessorV2)self, enable);
    return ForceUneqOnStsRegister(self, enable);
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet((ThaPohProcessor)self)->CanForceTxAlarms((ThaPohProcessor)self, alarmType))
        return cAtErrorModeNotSupport;

    if (alarmType & cAtSdhPathAlarmRdi)
        ret |= TxRdiForce(self, enable);

    if (alarmType & cAtSdhPathAlarmUneq)
        ret |= TxUneqForce(self, enable);

    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;
    uint32 address = mMethodsGet(processor)->PohCpeStsTu3Ctrl(processor) +
                     mMethodsGet(processor)->PohCpeStsTu3PohOffset(processor);
    uint32 regVal  = mChannelHwRead(self, address, cThaModulePoh);
    uint32 forcedAlarms = 0;

    if (regVal & cThaRdiInsEnbMask)
        forcedAlarms |= cAtSdhPathAlarmRdi;

    /* UNEQ */
    address = cThaRegOcnTxPohInsPerChnCtrl + mMethodsGet(processor)->OcnStsDefaultOffset(processor);
    regVal  = mChannelHwRead(self, address, cThaModuleOcn);
    if (regVal & cThaTxPg2StsUneqPFrcMask)
        forcedAlarms |= cAtSdhPathAlarmUneq;

    return forcedAlarms;
    }

static eAtRet ForceBip(AtChannel self, eBool force)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxPohInsPerChnCtrl + mMethodsGet((ThaAuVcPohProcessorV2)self)->OcnStsDefaultOffset((ThaAuVcPohProcessorV2)self);
    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaTxPg2StsB3ErrInsMask, cThaTxPg2StsB3ErrInsShift, force ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static eBool BipForced(AtChannel self)
    {
    uint32 regVal, address;

    address = cThaRegOcnTxPohInsPerChnCtrl + mMethodsGet((ThaAuVcPohProcessorV2)self)->OcnStsDefaultOffset((ThaAuVcPohProcessorV2)self);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    return (regVal & cThaTxPg2StsB3ErrInsMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    /* Release all error forcing */
    ForceBip(self, cAtFalse);

    if (errorType == cAtSdhPathErrorBip)
        return ForceBip(self, cAtTrue);

    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (errorType == 0) /* Unforce nothing */
        return cAtOk;

    if (errorType == cAtSdhPathErrorBip)
        return ForceBip(self, cAtFalse);

    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (BipForced(self)) return cAtSdhPathErrorBip;

    return cAtSdhPathErrorNone;
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;

    /* Configure overhead byte (C2 byte) buffer using for transmit */
    address = mMethodsGet(processor)->OverheadByteInsertBufferRegister(processor, cAtSdhPathOverheadByteC2) +
              mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);

    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaRegPohStsC2InsByteMask, cThaRegPohStsC2InsByteShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 psl;
    ThaAuVcPohProcessorV2 processor = (ThaAuVcPohProcessorV2)self;

    /* Get overhead byte (C2 byte) buffer using for transmit */
    address = mMethodsGet(processor)->OverheadByteInsertBufferRegister(processor, cAtSdhPathOverheadByteC2) +
              mMethodsGet(processor)->PohCpeStsTu3DefaultOffset(processor);

    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaRegPohStsC2InsByteMask, cThaRegPohStsC2InsByteShift, uint8, &psl);

    return psl;
    }

static eAtSdhTtiMode RxTtiModeGet(ThaDefaultPohProcessorV2 self)
    {
    ThaAuVcPohProcessorV2 auPohProcessor = (ThaAuVcPohProcessorV2)self;
    uint32 address, regVal;
    uint8 hwMode;

    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);
    regVal = mChannelHwRead(self, address, cThaModulePoh);

    mFieldGet(regVal, cThaJ1FrmMdMask, cThaJ1FrmMdShift, uint8, &hwMode);
    return ThaDefaultPohProcessorV2TtiModeHw2Sw(hwMode);
    }

static eAtRet RxTtiModeSet(ThaDefaultPohProcessorV2 self, eAtSdhTtiMode ttiMode)
    {
    ThaAuVcPohProcessorV2 auPohProcessor = (ThaAuVcPohProcessorV2)self;
    uint32 address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
                     mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);
    uint32 regVal = mChannelHwRead(self, address, cThaModulePoh);
    uint8 hwMode = ThaDefaultPohProcessorV2TtiHwValueByModeGet(ttiMode);

    mFieldIns(&regVal, cThaJ1FrmMdMask, cThaJ1FrmMdShift, hwMode);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);
    return cAtOk;
    }

static uint32 PohExpectedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint8 slice, hwStsInSlice;
    uint8 STSMAX = StsMax(mThis(self));

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (0x0000 + (STSMAX * VTMAX * 16UL) + (hwStsInSlice * 16UL) + ThaPohProcessorPartOffset((ThaPohProcessor)self));

    return 0;
    }

static uint32 PohTxTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax(mThis(self));
    uint32 baseAddress = mMethodsGet(self)->PohTxTtiBaseAddress(self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (baseAddress + (STSMAX * VTMAX * 16UL) + (hwStsInSlice * 16UL) + ThaPohProcessorPartOffset((ThaPohProcessor)self));

    return 0;
    }

static uint32 PohCapturedTtiAddressGet(ThaDefaultPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax(mThis(self));
    uint32 baseAddress = mMethodsGet(self)->PohCapturedTtiBaseAddress(self);
    uint8 slice, hwStsInSlice;

    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return (baseAddress + (STSMAX * VTMAX * 16UL) + (hwStsInSlice * 16UL) + ThaPohProcessorPartOffset((ThaPohProcessor)self));

    return 0;
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    return (uint8)mRegField(Grabber1Read(self), cThaRegPohCpeStsTu3PathC2Grabber);
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    uint32 regVal, address;
    uint8 pslValue;
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);

    /* Get the expected mask/shift of Path/V Signal Label */
    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldGet(regVal, cThaPslExptValMask, cThaPslExptValShift, uint8, &pslValue);

    return pslValue;
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);

    /* Get the expected mask/shift of Path/V Signal Label */
    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    regVal = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaPslExptValMask, cThaPslExptValShift, psl);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 RxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 slice, sts;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &sts) == cAtOk)
        return (uint32)(slice << 10) + sts + (read2Clear ? 0 : 0x20) + (isDecrease ? 32 : 0) + mPohPartOffset(self);

    return 0;
    }

static uint32 TxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 slice, sts;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &sts) == cAtOk)
        return (uint32)(slice << 10) + (uint32)(sts << 5) + (read2Clear ? 0 : 0x400) + (isDecrease ? 512 : 0) + mPohPartOffset(self);

    return 0;
    }

static uint32 RxPointerAdjustCounterAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegOcnRxStsPtrAdjPerChnCnt;
    }

static uint32 TxPointerAdjustCounterAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaRegOcnTxStsVtPtrAdjPerChnCnt;
    }

static uint32 PohCpeStsTu3B3BipErrCnt(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPohRegCpeStsTu3B3BipErrCnt;
    }

static uint32 PohCpeStsTu3ReiPErrCnt(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaPohRegCpeStsTu3ReiPErrCnt;
    }

static uint32 ReadOnlyCntGet(AtChannel self, uint32 counterType)
    {
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);
    uint32 offset = mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);
    uint32 address;

    /* BIP */
    if (counterType == cAtSdhPathCounterTypeBip)
        {
        address = mMethodsGet(auPohProcessor)->PohCpeStsTu3B3BipErrCnt(auPohProcessor) + offset;
        return mChannelHwRead(self, address, cThaModulePoh) & cThaB3ErrCntMask;
        }

    /* REI */
    if (counterType == cAtSdhPathCounterTypeRei)
        {
        address = mMethodsGet(auPohProcessor)->PohCpeStsTu3ReiPErrCnt(auPohProcessor) + offset;
        return mChannelHwRead(self, address, cThaModulePoh) & cThaReiPErrCntMask;
        }

    return 0;
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    uint32 address, regVal;
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);

    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    regVal = mChannelHwRead(self, address, cThaModulePoh);

    mFieldIns(&regVal, cThaJ1MonEnbMask, cThaJ1MonEnbShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModulePoh);
    return cAtOk;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    uint32 address, regVal;
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);

    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    regVal = mChannelHwRead(self, address, cThaModulePoh);
    return (regVal & cThaJ1MonEnbMask) ? cAtTrue : cAtFalse;
    }

static eAtModuleSdhRet ERdiEnable(AtSdhPath self, eBool enable)
    {
    uint32 address, regVal;
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);

    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    regVal  = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal, cThaERdiEnMask, cThaERdiEnShift, enable);
    mFieldIns(&regVal, cThaERdiInsEnbMask, cThaERdiInsEnbShift, enable);
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return ThaPohProcessorTimAutoRdiUpdateWhenERdiEnable((ThaPohProcessor)self, enable);
    }

static eBool ERdiIsEnabled(AtSdhPath self)
    {
    uint32 address, regVal;
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);

    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    regVal  = mChannelHwRead(self, address, cThaModulePoh);

    return (regVal & cThaERdiEnMask) ? cAtTrue : cAtFalse;
    }

static uint32 VcAis2AisEnbMask(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaVcAis2AisEnbMask;
    }

static uint32 VcAis2AisEnbShift(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cThaVcAis2AisEnbShift;
    }

static eAtRet VcAisAffectEnable(ThaPohProcessor self, eBool enable)
    {
    uint32 regVal, address;
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);
    eBool enableActiveHigh = enable ? 1 : 0;

    /* Get current configuration */
    address = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
              mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);

    regVal  = mChannelHwRead(self, address, cThaModulePoh);
    mFieldIns(&regVal,
              mMethodsGet(mThis(self))->VcAis2AisEnbMask(mThis(self)),
              mMethodsGet(mThis(self))->VcAis2AisEnbShift(mThis(self)),
              enableActiveHigh);

    /* Apply */
    mChannelHwWrite(self, address, regVal, cThaModulePoh);

    return cAtOk;
    }

static void TxByteG1BufferSet(ThaPohProcessor self, uint32 g1Value)
    {
    ThaAuVcPohProcessorV2 pohProcessor = (ThaAuVcPohProcessorV2)self;
    uint32 offset   = mMethodsGet(pohProcessor)->PohCpeStsTu3DefaultOffset(pohProcessor);
    uint32 regAddr  = mMethodsGet(pohProcessor)->OverheadByteInsertBufferRegister(pohProcessor, cAtSdhPathOverheadByteG1) + offset;
    uint32 regValue = mChannelHwRead(self, regAddr, cThaModulePoh);

    mRegFieldSet(regValue, cThaRegPohStsG1InsByte, g1Value);
    mChannelHwWrite(self, regAddr, regValue, cThaModulePoh);
    }

static eAtRet AutoRdiEnable(ThaPohProcessor self, uint32 alarmType, eBool enable)
    {
    ThaAuVcPohProcessorV2 pohProcessor = (ThaAuVcPohProcessorV2)self;
    uint8 hwEnable = enable ? 0 : 1;
    uint32 offset  = mMethodsGet(pohProcessor)->PohCpeStsTu3PohOffset(pohProcessor);
    uint32 regAddr = mMethodsGet(pohProcessor)->PohCpeStsTu3Ctrl(pohProcessor) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    AtSdhChannel sdhVc = (AtSdhChannel)ThaPohProcessorVcGet(self);
    AtSdhLine sdhLine  = AtSdhChannelLineObjectGet(sdhVc);

    /* When RX Line is disabled and auto RDI is also disabled, some applications
     * require no RDI is sent backward. To do this, software has to
     * - Catch the current TX G1
     * - Change TX G1 to 0
     * - disable Auto-RDI */
    if (!enable && !AtChannelRxIsEnabled((AtChannel)sdhLine))
        {
        pohProcessor->g1Value  = AtSdhChannelTxOverheadByteGet(sdhVc, cAtSdhPathOverheadByteG1);
        pohProcessor->g1Cached = cAtTrue;
        TxByteG1BufferSet(self, 0x0);
        }

    /* Need to restore TX G1 if it was changed before */
    if (enable && pohProcessor->g1Cached)
        {
        TxByteG1BufferSet(self, pohProcessor->g1Value);
        pohProcessor->g1Cached = cAtFalse;
        }

    if ((alarmType & cAtSdhPathAlarmLop) || (alarmType & cAtSdhPathAlarmAis))
        mFieldIns(&regVal, cThaAisLopInsMskMask, cThaAisLopInsMskShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmTim)
        mFieldIns(&regVal, cThaTimInsMskMask, cThaTimInsMskShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmPlm)
        mFieldIns(&regVal, cThaPlmInsMskMask, cThaPlmInsMskShift, hwEnable);

    if (alarmType & cAtSdhPathAlarmUneq)
        mFieldIns(&regVal, cThaUneqInsMskMask, cThaUneqInsMskShift, hwEnable);

    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 AutoRdiAlarms(ThaPohProcessor self)
    {
    ThaAuVcPohProcessorV2 pohProcessor = (ThaAuVcPohProcessorV2)self;
    uint32 offset  = mMethodsGet(pohProcessor)->PohCpeStsTu3PohOffset(pohProcessor);
    uint32 regAddr = mMethodsGet(pohProcessor)->PohCpeStsTu3Ctrl(pohProcessor) + offset;
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    uint32 alarms = 0;

    if ((regVal & cThaAisLopInsMskMask) == 0)
        alarms |= (cAtSdhPathAlarmLop | cAtSdhPathAlarmAis);
    if ((regVal & cThaTimInsMskMask) == 0)
        alarms |= cAtSdhPathAlarmTim;
    if ((regVal & cThaPlmInsMskMask) == 0)
        alarms |= cAtSdhPathAlarmPlm;
    if ((regVal & cThaUneqInsMskMask) == 0)
        alarms |= cAtSdhPathAlarmUneq;

    return alarms;
    }

static uint32 BlockErrorCounterOffset(ThaDefaultPohProcessorV2 self, uint32 counterType)
    {
	AtUnused(counterType);
    return mMethodsGet((ThaAuVcPohProcessorV2)self)->PohCpeStsTu3PohOffset((ThaAuVcPohProcessorV2)self);
    }

static uint32 OverheadByteInsertBufferRegister(ThaAuVcPohProcessorV2 self, uint32 overheadByte)
    {
	AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteC2: return cThaRegPohStsC2InsBuffer;
        case cAtSdhPathOverheadByteF2: return cThaRegPohStsF2InsBuffer;
        case cAtSdhPathOverheadByteF3: return cThaRegPohStsF3InsBuffer;
        case cAtSdhPathOverheadByteG1: return cThaRegPohStsG1InsBuffer;
        case cAtSdhPathOverheadByteH4: return cThaRegPohStsH4InsBuffer;
        case cAtSdhPathOverheadByteK3: return cThaRegPohStsK3InsBuffer;
        case cAtSdhPathOverheadByteN1: return cThaRegPohStsN1InsBuffer;

        default:
            return 0xCAFECAFE;
        }
    }

static eBool OverheadByteIsValid(ThaDefaultPohProcessorV2 self, uint32 overheadByte)
    {
	AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteN1: return cAtTrue;
        case cAtSdhPathOverheadByteC2: return cAtTrue;
        case cAtSdhPathOverheadByteG1: return cAtTrue;
        case cAtSdhPathOverheadByteF2: return cAtTrue;
        case cAtSdhPathOverheadByteH4: return cAtTrue;
        case cAtSdhPathOverheadByteF3: return cAtTrue;
        case cAtSdhPathOverheadByteK3: return cAtTrue;

        default:
            return cAtFalse;
        }
    }

static eAtModuleSdhRet TxByteG1Insert(AtSdhChannel self, uint8 value)
    {
    ThaAuVcPohProcessorV2 auPohProcessor = mThis(self);
    uint32 regAddr = mMethodsGet(auPohProcessor)->PohCpeStsTu3Ctrl(auPohProcessor) +
                     mMethodsGet(auPohProcessor)->PohCpeStsTu3PohOffset(auPohProcessor);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cThaSpareInsVal, (value & cSpareBitMask) >> cSpareBitShift);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    uint32 regVal, regAddr;
    ThaDefaultPohProcessorV2 defaultPohProcessor = (ThaDefaultPohProcessorV2)self;
    ThaAuVcPohProcessorV2 vcPohProcessor = (ThaAuVcPohProcessorV2)self;

    if (!mMethodsGet(defaultPohProcessor)->OverheadByteIsValid(defaultPohProcessor, overheadByte))
        return cAtErrorModeNotSupport;

    if (overheadByte == cAtSdhPathOverheadByteG1)
        return TxByteG1Insert(self, overheadByteValue);

    /* Configure overhead byte buffer used for transmit */
    regAddr = mMethodsGet(vcPohProcessor)->OverheadByteInsertBufferRegister(vcPohProcessor, overheadByte) +
              mMethodsGet(vcPohProcessor)->PohCpeStsTu3DefaultOffset(vcPohProcessor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cThaRegPohStsTuVtInsByte, overheadByteValue);
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    uint32 regVal, regAddr;
    ThaDefaultPohProcessorV2 defaultPohProcessor = (ThaDefaultPohProcessorV2)self;
    ThaAuVcPohProcessorV2 vcPohProcessor = (ThaAuVcPohProcessorV2)self;

    if (!mMethodsGet(defaultPohProcessor)->OverheadByteIsValid(defaultPohProcessor, overheadByte))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(vcPohProcessor)->OverheadByteInsertBufferRegister(vcPohProcessor, overheadByte) +
              mMethodsGet(vcPohProcessor)->PohCpeStsTu3DefaultOffset(vcPohProcessor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    return (uint8)mRegField(regVal, cThaRegPohStsTuVtInsByte);
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    AtSdhPath path = (AtSdhPath)self;
    ThaDefaultPohProcessorV2 pohProcessor = (ThaDefaultPohProcessorV2)self;

    if (!mMethodsGet(pohProcessor)->OverheadByteIsValid(pohProcessor, overheadByte))
        return 0;

    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteC2:
            return AtSdhPathRxPslGet(path);
        case cAtSdhPathOverheadByteF2:
            return (uint8)mRegField(Grabber1Read(path), cThaRegPohCpeStsTu3PathF2Grabber);
        case cAtSdhPathOverheadByteF3:
            return (uint8)mRegField(Grabber2Read(path), cThaRegPohCpeStsTu3PathF3Grabber);
        case cAtSdhPathOverheadByteG1:
            return RxG1Get(path);
        case cAtSdhPathOverheadByteH4:
            return (uint8)mRegField(Grabber2Read(path), cThaRegPohCpeStsTu3PathH4Grabber);
        case cAtSdhPathOverheadByteK3:
            return (uint8)mRegField(Grabber2Read(path), cThaRegPohCpeStsTu3PathK3Grabber);
        case cAtSdhPathOverheadByteN1:
            return (uint8)mRegField(Grabber2Read(path), cThaRegPohCpeStsTu3PathN1Grabber);

        default:
            return 0;
        }
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmUneq | cAtSdhPathAlarmRdi;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaAuVcPohProcessorV2 object = (ThaAuVcPohProcessorV2)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(g1Value);
    mEncodeUInt(g1Cached);
    }

static void OverrideAtObject(ThaAuVcPohProcessorV2 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(ThaAuVcPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PohCpeStsTu3DefaultOffset);
        mMethodOverride(m_methods, PohCpeStsTu3PohOffset);
        mMethodOverride(m_methods, OcnStsDefaultOffset);
        mMethodOverride(m_methods, OverheadByteInsertBufferRegister);
        mMethodOverride(m_methods, PohCpeStsTu3Ctrl);
        mMethodOverride(m_methods, PohCpeStsTu3PathOhGrabber1);
        mMethodOverride(m_methods, PohCpeStsTu3PathOhGrabber2);
        mMethodOverride(m_methods, PohCpeStsTu3AlarmStatus);
        mMethodOverride(m_methods, PohCpeStsTu3AlarmIntrSticky);
        mMethodOverride(m_methods, PohCpeStsTu3AlarmIntrEnable);
        mMethodOverride(m_methods, PohCpeStsTu3B3BipErrCnt);
        mMethodOverride(m_methods, PohCpeStsTu3ReiPErrCnt);

        mMethodOverride(m_methods, Tim2AisEnbMask);
        mMethodOverride(m_methods, Tim2AisEnbShift);
        mMethodOverride(m_methods, Plm2AisEnbMask);
        mMethodOverride(m_methods, Plm2AisEnbShift);
        mMethodOverride(m_methods, Uneq2AisEnbMask);
        mMethodOverride(m_methods, Uneq2AisEnbShift);
        mMethodOverride(m_methods, VcAis2AisEnbMask);
        mMethodOverride(m_methods, VcAis2AisEnbShift);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaDefaultPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 defaultProcessor = (ThaDefaultPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaDefaultPohProcessorV2Override,
                                  mMethodsGet(defaultProcessor),
                                  sizeof(m_ThaDefaultPohProcessorV2Override));

        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohExpectedTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiAddressGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeGet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxTtiModeSet);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, BlockErrorCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, OverheadByteIsValid);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterAddress);
        }

    mMethodsSet(defaultProcessor, &m_ThaDefaultPohProcessorV2Override);
    }

static void OverrideThaPohProcessor(ThaAuVcPohProcessorV2 self)
    {
    ThaPohProcessor processor = (ThaPohProcessor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaPohProcessorOverride,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaPohProcessorOverride));
        mMethodOverride(m_ThaPohProcessorOverride, VcAisAffectEnable);
        mMethodOverride(m_ThaPohProcessorOverride, AutoRdiEnable);
        mMethodOverride(m_ThaPohProcessorOverride, AutoRdiAlarms);
        }

    mMethodsSet(processor, &m_ThaPohProcessorOverride);
    }

static void OverrideAtSdhPath(ThaAuVcPohProcessorV2 self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ERdiEnable);
        mMethodOverride(m_AtSdhPathOverride, ERdiIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtChannel(ThaAuVcPohProcessorV2 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, ReadOnlyCntGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(ThaAuVcPohProcessorV2 self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(ThaAuVcPohProcessorV2 self)
    {
    OverrideAtObject(self);
    OverrideAtSdhChannel(self);
    OverrideThaDefaultPohProcessorV2(self);
    OverrideAtSdhPath(self);
    OverrideAtChannel(self);
    OverrideThaPohProcessor(self);
    }

ThaAuVcPohProcessorV2 ThaAuVcPohProcessorV2ObjectInit(ThaAuVcPohProcessorV2 self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDefaultPohProcessorV2ObjectInit((ThaDefaultPohProcessorV2)self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->g1Value = 0x0;

    return self;
    }

ThaPohProcessor ThaAuVcPohProcessorV2New(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return (ThaPohProcessor)ThaAuVcPohProcessorV2ObjectInit((ThaAuVcPohProcessorV2)newProcessor, vc);
    }

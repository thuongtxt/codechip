/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : ThaVc1xPohProcessorV2Internal.h
 * 
 * Created Date: Mar 18, 2013
 *
 * Description : VC-1x POH processors
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVC1XPOHPROCESSORV2INTERNAL_H_
#define _THAVC1XPOHPROCESSORV2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaDefaultPohProcessorV2Internal.h"
#include "ThaVc1xPohProcessorV2.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define VtFlatId(self) (uint32)((mVcGet(self)->tug2Id << 2) + mVcGet(self)->tu1xId)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVc1xPohProcessorV2Methods
    {
    uint32 (*PohCpeVtControlDefaultOffset)(ThaVc1xPohProcessorV2 self);
    uint32 (*OcnDefaultOffset)(ThaVc1xPohProcessorV2 self);

    uint32 (*PohCpeVtCtrl)(ThaVc1xPohProcessorV2 self);
    uint32 (*PohCpeVtPathOhGrabber1)(ThaVc1xPohProcessorV2 self);
    uint32 (*PohRegVtAlarmStatus)(ThaVc1xPohProcessorV2 self);
    uint32 (*PohRegVtAlarmIntrSticky)(ThaVc1xPohProcessorV2 self);
    uint32 (*PohRegVtAlarmIntrEnable)(ThaVc1xPohProcessorV2 self);
    uint32 (*PohRegVtBipErrCnt)(ThaVc1xPohProcessorV2 self);
    uint32 (*PohRegVtReiPErrCnt)(ThaVc1xPohProcessorV2 self);
    }tThaVc1xPohProcessorV2Methods;

typedef struct tThaVc1xPohProcessorV2
    {
    tThaDefaultPohProcessorV2 super;
    const tThaVc1xPohProcessorV2Methods *methods;

    /* Private data */
    uint8 k4Value;
    uint8 v5Value;
    eBool rdiFieldsCached;
    }tThaVc1xPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaVc1xPohProcessorV2 ThaVc1xPohProcessorV2ObjectInit(ThaVc1xPohProcessorV2 self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THAVC1XPOHPROCESSORV2INTERNAL_H_ */


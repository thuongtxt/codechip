/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : ThaAuVcPohProcessorV2Internal.h
 *
 * Created Date: Mar 18, 2013
 *
 * Description : AU's VC POH processor
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAAUVCPOHPROCESSORV2INTERNAL_H_
#define _THAAUVCPOHPROCESSORV2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaDefaultPohProcessorV2Internal.h"
#include "ThaAuVcPohProcessorV2.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaAuVcPohProcessorV2Methods
    {
    uint32 (*PohCpeStsTu3DefaultOffset)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3PohOffset)(ThaAuVcPohProcessorV2 self);
    uint32 (*OcnStsDefaultOffset)(ThaAuVcPohProcessorV2 self);
    uint32 (*OverheadByteInsertBufferRegister)(ThaAuVcPohProcessorV2 self, uint32 overheadByte);
    uint32 (*PohCpeStsTu3Ctrl)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3PathOhGrabber1)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3PathOhGrabber2)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3AlarmStatus)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3AlarmIntrSticky)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3AlarmIntrEnable)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3B3BipErrCnt)(ThaAuVcPohProcessorV2 self);
    uint32 (*PohCpeStsTu3ReiPErrCnt)(ThaAuVcPohProcessorV2 self);

    /* Bit fields */
    uint32 (*Tim2AisEnbMask)(ThaAuVcPohProcessorV2 self);
    uint32 (*Tim2AisEnbShift)(ThaAuVcPohProcessorV2 self);
    uint32 (*Uneq2AisEnbMask)(ThaAuVcPohProcessorV2 self);
    uint32 (*Uneq2AisEnbShift)(ThaAuVcPohProcessorV2 self);
    uint32 (*Plm2AisEnbMask)(ThaAuVcPohProcessorV2 self);
    uint32 (*Plm2AisEnbShift)(ThaAuVcPohProcessorV2 self);
    uint32 (*VcAis2AisEnbMask)(ThaAuVcPohProcessorV2 self);
    uint32 (*VcAis2AisEnbShift)(ThaAuVcPohProcessorV2 self);
    }tThaAuVcPohProcessorV2Methods;

typedef struct tThaAuVcPohProcessorV2
    {
    tThaDefaultPohProcessorV2 super;
    const tThaAuVcPohProcessorV2Methods *methods;

    /* Private data */
    uint8 g1Value;
    eBool g1Cached;
    }tThaAuVcPohProcessorV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaAuVcPohProcessorV2 ThaAuVcPohProcessorV2ObjectInit(ThaAuVcPohProcessorV2 self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THAAUVCPOHPROCESSORV2INTERNAL_H_ */

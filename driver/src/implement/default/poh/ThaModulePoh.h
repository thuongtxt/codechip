/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH (internal module)
 * 
 * File        : ThaModulePoh.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPOH_H_
#define _THAMODULEPOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "ThaPohProcessor.h"
#include "ThaTtiProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePoh * ThaModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete modules */
AtModule ThaModulePohV1New(AtDevice device);
AtModule ThaModulePohV2New(AtDevice device);

/* POH processor factory */
ThaPohProcessor ThaModulePohAuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc);
ThaPohProcessor ThaModulePohTu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc);
ThaPohProcessor ThaModulePohVc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc);

/* TTI processor factory */
ThaTtiProcessor ThaModulePohLineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line);

eAtRet ThaModulePohAllVtsPohMonEnable(ThaModulePoh self, AtSdhVc vc3, eBool enabled);
eBool ThaModulePohPathPohMonitorEnablingIsSupported(ThaModulePoh self);
eBool ThaModulePohSoftStickyIsNeeded(ThaModulePoh self);
eAtRet ThaModulePohLinePohMonEnable(ThaModulePoh self, AtSdhLine line, eBool enabled);

/* Utils */
void ThaModulePohTtiStrip(eAtSdhTtiMode ttiMode, uint8 *message, uint8 *resultMessage);
eAtRet ThaModulePohV1TtiCapture(ThaModulePoh self,
                                AtSdhChannel sdhChannel,
                                uint8 bufId,
                                eAtSdhTtiMode ttiMode,
                                uint8 *pMsgBuf,
                                uint32 frameTimeInUs);

/* For debugging */
void ThaModulePohSdhChannelRegShow(AtSdhChannel sdhChannel);

/* Register base */
uint32 ThaModulePohBaseAddress(ThaModulePoh self);
uint32 ThaModulePohstspohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel);
uint32 ThaModulePohCpestsctrBase(ThaModulePoh self);
uint32 ThaModulePohCpeStsStatus(ThaModulePoh self);
uint32 ThaModulePohMsgstsexpBase(ThaModulePoh self);
uint32 ThaModulePohMsgstscurBase(ThaModulePoh self);
uint32 ThaModulePohMsgstsinsBase(ThaModulePoh self);
uint32 ThaModulePohIpmCnthiBase(ThaModulePoh self);
uint32 ThaModulePohAlmMskhiBase(ThaModulePoh self);
uint32 ThaModulePohAlmStahiBase(ThaModulePoh self);
uint32 ThaModulePohAlmChghiBase(ThaModulePoh self);
uint32 ThaModulePohVtpohgrbBase(ThaModulePoh self, AtSdhChannel sdhChannel);
uint32 ThaModulePohMsgvtinsBase(ThaModulePoh self);
uint32 ThaModulePohIpmCntloBase(ThaModulePoh self);
uint32 ThaModulePohAlmStaloBase(ThaModulePoh self);
uint32 ThaModulePohAlmChgloBase(ThaModulePoh self);
uint32 ThaModulePohMsgvtcurBase(ThaModulePoh self);

/* Product concretes */
AtModule Tha60030081ModulePohNew(AtDevice device);
AtModule Tha60150011ModulePohNew(AtDevice device);
AtModule Tha60210011ModulePohNew(AtDevice device);
AtModule Tha60210031ModulePohNew(AtDevice device);
AtModule Tha60210051ModulePohNew(AtDevice device);
AtModule Tha6A000010ModulePohNew(AtDevice device);
AtModule Tha6A210031ModulePohNew(AtDevice device);
AtModule Tha60210061ModulePohNew(AtDevice device);
AtModule Tha60290011ModulePohNew(AtDevice device);
AtModule Tha60290022ModulePohNew(AtDevice device);
AtModule Tha60290022ModulePohV2New(AtDevice device);
AtModule Tha6A290011ModulePohNew(AtDevice device);
AtModule Tha6A290021ModulePohNew(AtDevice device);
AtModule Tha60290081ModulePohNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPOH_H_ */


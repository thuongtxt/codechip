/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : ThaEncapBinder.c
 *
 * Created Date: Apr 5, 2015
 *
 * Description : ENCAP binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLink.h"
#include "../../../generic/encap/AtEncapChannelInternal.h"
#include "../map/ThaModuleAbstractMap.h"
#include "../man/ThaDeviceInternal.h"
#include "../encap/ThaModuleEncap.h"
#include "../pdh/ThaPdhDe1Internal.h"
#include "ThaEncapBinder.h"
#include "ThaEncapBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaEncapBinder)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEncapBinderMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtEncapBinderMethods m_AtEncapBinderOverride;

/* Save super implementation */
static const tAtEncapBinderMethods *m_AtEncapBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet BindVc1xToEncapChannel(AtEncapBinder self, AtChannel vc1x, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(vc1x);

    /* Configure module Map/Demap */
    ret |= ThaModuleAbstractMapBindVc1xToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtSdhVc)vc1x, encapChannel);
    ret |= ThaModuleAbstractMapBindVc1xToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)vc1x, encapChannel);

    if (encapChannel)
        ret |= ThaEncapBindDefaultStuffModeSet(self, encapChannel, cAtHdlcStuffByte);

    if (ret != cAtOk)
        return ret;

    /* Allocate hardware block resource */
    if (mMethodsGet(mThis(self))->ShouldAllocateHwBlockOnBinding(mThis(self)))
        ret |= ThaModuleEncapHardwareBlockAllocate(vc1x, encapChannel, cThaModuleEncapBlockTypeLowBandwidth);

    return ret;
    }

static eAtRet BindHoVcToEncapChannel(AtEncapBinder self, AtChannel hoVc, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(hoVc);

    /* Configure module Map/Demap */
    ThaModuleAbstractMapHoVcSignalTypeSet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)hoVc);
    ret |= ThaModuleAbstractMapBindHoVcToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtSdhVc)hoVc, encapChannel);
    ret |= ThaModuleAbstractMapBindHoVcToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)hoVc, encapChannel);
    if (ret != cAtOk)
        return ret;
	
	/* Default stuffing */ 
    if (encapChannel)
        ret |= ThaEncapBindDefaultStuffModeSet(self, encapChannel, cAtHdlcStuffByte);
    
    /* Allocate hardware block resource */
    if (mMethodsGet(mThis(self))->ShouldAllocateHwBlockOnBinding(mThis(self)))
        ret |= ThaModuleEncapHardwareBlockAllocate(hoVc, encapChannel, cThaModuleEncapBlockTypeHighBandwidth);

    return ret;
    }

static eAtRet BindTu3VcToEncapChannel(AtEncapBinder self, AtChannel tu3Vc, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(tu3Vc);

    /* Configure module Map/Demap */
    ThaModuleAbstractMapTu3VcSignalTypeSet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtSdhVc)tu3Vc);
    ret |= ThaModuleAbstractMapBindTu3VcToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtSdhVc)tu3Vc, encapChannel);
    ret |= ThaModuleAbstractMapBindTu3VcToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtSdhVc)tu3Vc, encapChannel);
    if (ret != cAtOk)
        return ret;

    /* Default stuffing */
    if (encapChannel)
        ret |= ThaEncapBindDefaultStuffModeSet(self, encapChannel, cAtHdlcStuffByte);

    /* Allocate hardware block resource */
    if (mMethodsGet(mThis(self))->ShouldAllocateHwBlockOnBinding(mThis(self)))
        ret |= ThaModuleEncapHardwareBlockAllocate(tu3Vc, encapChannel, cThaModuleEncapBlockTypeHighBandwidth);

    return ret;
    }

static eAtRet BindDe3ToEncapChannel(AtEncapBinder self, AtChannel de3, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(de3);

    ret |= ThaModuleAbstractMapBindDe3ToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhDe3)de3, encapChannel);
    ret |= ThaModuleAbstractMapBindDe3ToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe3)de3, encapChannel);
    if (ret != cAtOk)
        return ret;

    /* Default stuffing */ 
    if (encapChannel)
        ret |= ThaEncapBindDefaultStuffModeSet(self, encapChannel, cAtHdlcStuffBit);

    if (mMethodsGet(mThis(self))->ShouldAllocateHwBlockOnBinding(mThis(self)))
        ret |= ThaModuleEncapHardwareBlockAllocate(de3, encapChannel, cThaModuleEncapBlockTypeHighBandwidth);

    return ret;
    }

static eAtRet BindDe1ToEncapChannel(AtEncapBinder self, AtChannel de1, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(de1);

    /* MAP/DEMAP binding */
    ret |= ThaModuleAbstractMapBindDe1ToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhDe1)de1, encapChannel);
    ret |= ThaModuleAbstractMapBindDe1ToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe1)de1, encapChannel);
    if (ret != cAtOk)
        return ret;

    /* Default stuffing */ 
    if (encapChannel)
        ret |= ThaEncapBindDefaultStuffModeSet(self, encapChannel, cAtHdlcStuffBit);
    
    /* Allocate hardware block resource */
    if (mMethodsGet(mThis(self))->ShouldAllocateHwBlockOnBinding(mThis(self)))
        ret |= ThaModuleEncapHardwareBlockAllocate(de1, encapChannel, cThaModuleEncapBlockTypeLowBandwidth);

    return ret;
    }

static eAtRet BindNxDs0ToEncapChannel(AtEncapBinder self, AtChannel nxDs0, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(nxDs0);

    /* Bind/Unbind first */
    ret |= ThaModuleAbstractMapBindNxDs0ToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhNxDS0)nxDs0, encapChannel);
    ret |= ThaModuleAbstractMapBindNxDs0ToEncap((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhNxDS0)nxDs0, encapChannel);
    if (ret != cAtOk)
        return ret;

    /* Default stuffing */
    if (encapChannel)
        ret |= ThaEncapBindDefaultStuffModeSet(self, encapChannel, cAtHdlcStuffBit);

    /* Allocate hardware block */
    if (mMethodsGet(mThis(self))->ShouldAllocateHwBlockOnBinding(mThis(self)))
        ret |= ThaModuleEncapHardwareBlockAllocate(nxDs0, encapChannel, cThaModuleEncapBlockTypeLowBandwidth);

    return ret;
    }

static eAtRet BindConcateGroupToEncapChannel(AtEncapBinder self, AtChannel concateGroup, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(concateGroup);
    AtUnused(encapChannel);

    return cAtOk;
    }

static eAtRet AugEncapChannelRestore(AtEncapBinder self, AtChannel aug, AtEncapChannel encapChannel)
    {
    eAtRet ret;
    AtHdlcLink link = NULL;
    eBool txTrafficIsEn = cAtFalse;
    eBool rxTrafficIsEn = cAtFalse;

    AtUnused(self);

    if (AtEncapChannelEncapTypeGet(encapChannel) == cAtEncapHdlc)
        link = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);

    if (link)
        {
        txTrafficIsEn = AtHdlcLinkTxTrafficIsEnabled(link);
        rxTrafficIsEn = AtHdlcLinkRxTrafficIsEnabled(link);

        AtHdlcLinkTxTrafficEnable(link, cAtFalse);
        AtHdlcLinkRxTrafficEnable(link, cAtFalse);
        }

    ret = AtEncapChannelPhyBind(encapChannel, NULL);
    ret |= AtEncapChannelPhyBind(encapChannel, aug);

    if (link)
        {
        if (txTrafficIsEn)
            AtHdlcLinkTxTrafficEnable(link, cAtTrue);

        if (rxTrafficIsEn)
            AtHdlcLinkRxTrafficEnable(link, cAtTrue);
        }

    return ret;
    }

static uint8 BackupNxDs0BoundEncapChannels(AtPdhDe1 self, uint32 *masks, AtChannel *encapChannels)
    {
    uint8 numNxDs0s = 0;
    AtIterator nxDs0Iterator;
    AtPdhNxDS0 nxDs0;

    nxDs0Iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)self);
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(nxDs0Iterator)) != NULL)
        {
        masks[numNxDs0s] = AtPdhNxDS0BitmapGet(nxDs0);

        encapChannels[numNxDs0s] = (AtChannel)AtChannelBoundEncapChannel((AtChannel)nxDs0);
        if (encapChannels[numNxDs0s])
            AtEncapChannelPhyBind((AtEncapChannel)encapChannels[numNxDs0s], NULL);

        numNxDs0s = (uint8)(numNxDs0s + 1);
        }

    AtObjectDelete((AtObject)nxDs0Iterator);
    return numNxDs0s;
    }

static AtEncapChannel UnbindEncapChannel(AtPdhDe1 self)
    {
    AtEncapChannel encapChannel = AtChannelBoundEncapChannel((AtChannel)self);
    if (encapChannel)
        AtEncapChannelPhyBind(encapChannel, NULL);

    return encapChannel;
    }

static eAtRet RestoreNxDs0BoundEncapChannels(AtPdhChannel self, AtChannel *encapChannels, uint32 *masks, uint8 numNxDs0s)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    AtPdhNxDS0 nxds0;

    /* Have a list to store NxDS0s */
    if (de1->allNxDs0s == NULL)
        {
        de1->allNxDs0s = AtListCreate(cThaMaxE1TimeslotNum);
        if (de1->allNxDs0s == NULL)
            return cAtErrorRsrcNoAvail;
        }

    /* Restore all timeslots */
    for (i = 0; i < numNxDs0s; i++)
        {
        /* If E1 is signaling, nxds0 which contain timeslot 16 will NOT be re-created */
        if (ThaPdhDe1IsE1Signaling((AtPdhDe1)self) && (masks[i] & cBit16))
            continue;

        /* Restore and rebind encap channel */
        nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1)self, masks[i]);
        if (nxds0 == NULL)
            nxds0 = AtPdhDe1NxDs0Create((AtPdhDe1)self, masks[i]);

        if (nxds0 && encapChannels[i])
            ret |= AtEncapChannelPhyBind((AtEncapChannel)encapChannels[i], (AtChannel)nxds0);
        }

    return ret;
    }

static eAtRet De1EncapRestore(AtEncapBinder self, AtPdhChannel de1, tDe1Backup *backup)
    {
    eAtRet ret = cAtOk;

    AtUnused(self);

    /* Restore bound encap channel */
    if (backup->de1BoundChannel)
        ret |= AtEncapChannelPhyBind((AtEncapChannel)(backup->de1BoundChannel), (AtChannel)de1);

    /* Restore all NxDS0 when framing mode is changed, in case not un-framed mode */
    if (!AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet(de1)))
        ret |= RestoreNxDs0BoundEncapChannels(de1, backup->nxDs0BoundChannels, backup->masks, backup->numNxDs0s);

    AtOsalMemFree(backup);
    return ret;
    }

static eAtRet EncapChannelPhysicalChannelSet(AtEncapBinder self, AtEncapChannel encapChannel, AtChannel physicalChannel)
    {
    AtUnused(self);
    AtEncapChannelPhysicalChannelSet(encapChannel, physicalChannel);
    return cAtOk;
    }

static tDe1Backup* De1EncapBackup(AtEncapBinder self, AtPdhChannel de1)
    {
    tDe1Backup* backup = AtOsalMemAlloc(sizeof(tDe1Backup));

    AtUnused(self);

    if (backup == NULL)
        return NULL;
    AtOsalMemInit(backup, 0, sizeof(tDe1Backup));

    /* Backup encap channel */
    backup->de1BoundChannel = (AtChannel)UnbindEncapChannel((AtPdhDe1)de1);

    /* Backup timeslots */
    backup->numNxDs0s = BackupNxDs0BoundEncapChannels((AtPdhDe1)de1, backup->masks, backup->nxDs0BoundChannels);

    return backup;
    }

static void Delete(AtObject self)
    {
    AtUnused(self);
    /* Do nothing because of using static instance */
    }

static eBool ShouldAllocateHwBlockOnBinding(ThaEncapBinder self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModuleAbstractMap ModuleMap(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static uint32 HoVcBoundEncapHwIdGet(AtEncapBinder self, AtChannel hoVc)
    {
    AtUnused(self);
    return ThaModuleAbstractMapAuVcBoundEncapHwIdGet(ModuleMap(hoVc), (AtSdhVc)hoVc);
    }

static uint32 Vc1xBoundEncapHwIdGet(AtEncapBinder self, AtChannel vc1x)
    {
    AtUnused(self);
    return ThaModuleAbstractMapVc1xBoundEncapHwIdGet(ModuleMap(vc1x), (AtSdhVc)vc1x);
    }

static uint32 De1BoundEncapHwIdGet(AtEncapBinder self, AtChannel de1)
    {
    AtUnused(self);
    return ThaModuleAbstractMapDe1BoundEncapHwIdGet(ModuleMap(de1), (AtPdhDe1)de1);
    }

static uint32 De3BoundEncapHwIdGet(AtEncapBinder self, AtChannel de3)
    {
    AtUnused(self);
    return ThaModuleAbstractMapDe1BoundEncapHwIdGet(ModuleMap(de3), (AtPdhDe1)de3);
    }

static uint32 NxDs0BoundEncapHwIdGet(AtEncapBinder self, AtChannel nxDs0)
    {
    AtUnused(self);
    return ThaModuleAbstractMapNxDs0BoundEncapHwIdGet(ModuleMap(nxDs0), (AtPdhNxDS0)nxDs0);
    }

static void MethodsInit(ThaEncapBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ShouldAllocateHwBlockOnBinding);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtEncapBinder self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEncapBinder(AtEncapBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapBinderOverride, mMethodsGet(self), sizeof(m_AtEncapBinderOverride));

        mMethodOverride(m_AtEncapBinderOverride, BindVc1xToEncapChannel);
        mMethodOverride(m_AtEncapBinderOverride, BindHoVcToEncapChannel);
        mMethodOverride(m_AtEncapBinderOverride, BindTu3VcToEncapChannel);
        mMethodOverride(m_AtEncapBinderOverride, BindDe3ToEncapChannel);
        mMethodOverride(m_AtEncapBinderOverride, BindDe1ToEncapChannel);
        mMethodOverride(m_AtEncapBinderOverride, BindNxDs0ToEncapChannel);
        mMethodOverride(m_AtEncapBinderOverride, BindConcateGroupToEncapChannel);
        mMethodOverride(m_AtEncapBinderOverride, AugEncapChannelRestore);
        mMethodOverride(m_AtEncapBinderOverride, EncapChannelPhysicalChannelSet);
        mMethodOverride(m_AtEncapBinderOverride, De1EncapBackup);
        mMethodOverride(m_AtEncapBinderOverride, De1EncapRestore);
        mMethodOverride(m_AtEncapBinderOverride, HoVcBoundEncapHwIdGet);
        mMethodOverride(m_AtEncapBinderOverride, Vc1xBoundEncapHwIdGet);
        mMethodOverride(m_AtEncapBinderOverride, De1BoundEncapHwIdGet);
        mMethodOverride(m_AtEncapBinderOverride, De3BoundEncapHwIdGet);
        mMethodOverride(m_AtEncapBinderOverride, NxDs0BoundEncapHwIdGet);
        }

    mMethodsSet(self, &m_AtEncapBinderOverride);
    }

static void Override(AtEncapBinder self)
    {
    OverrideAtObject(self);
    OverrideAtEncapBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEncapBinder);
    }

AtEncapBinder ThaEncapBinderObjectInit(AtEncapBinder self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEncapBinderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtEncapBinder ThaEncapDefaultBinder(AtDevice device)
    {
    static tThaEncapBinder m_binder;
    static AtEncapBinder m_sharedBinder = NULL;
    if (m_sharedBinder == NULL)
        m_sharedBinder = ThaEncapBinderObjectInit((AtEncapBinder)&m_binder, device);
    return m_sharedBinder;
    }

eAtRet ThaEncapBindDefaultStuffModeSet(AtEncapBinder self, AtEncapChannel encapChannel, uint32 stuffMode)
    {
    AtUnused(self);
    if (AtEncapChannelEncapTypeGet(encapChannel) == cAtEncapHdlc)
        return AtHdlcChannelStuffModeSet((AtHdlcChannel)encapChannel, stuffMode);
    return cAtOk;
    }

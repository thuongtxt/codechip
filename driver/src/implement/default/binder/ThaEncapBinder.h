/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : ThaEncapBinder.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : ENCAP binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAENCAPBINDER_H_
#define _THAENCAPBINDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/binder/AtEncapBinder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEncapBinder * ThaEncapBinder;
typedef struct tThaEncapDynamicBinder * ThaEncapDynamicBinder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEncapBinder ThaEncapDefaultBinder(AtDevice device);
AtEncapBinder ThaEncapDynamicBinderNew(uint8 numPool, AtDevice device, uint32 numChannelPerPool);
AtEncapBinder Tha60290081EncapBinder(AtDevice device);

eAtRet ThaEncapBinderDe1EncapRestore(AtEncapBinder self, AtPdhChannel de1, tDe1Backup *backup);
eAtRet ThaEncapBindDefaultStuffModeSet(AtEncapBinder self, AtEncapChannel encapChannel, uint32 stuffMode);

#ifdef __cplusplus
}
#endif
#endif /* _THAENCAPBINDER_H_ */


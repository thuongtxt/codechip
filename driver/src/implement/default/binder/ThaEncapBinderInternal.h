/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaEncapBinderInternal.h
 * 
 * Created Date: Apr 4, 2016
 *
 * Description : Encap binder internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAENCAPBINDERINTERNAL_H_
#define _THAENCAPBINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/binder/AtEncapBinderInternal.h"
#include "../util/ThaBitMask.h"
#include "ThaEncapBinder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaEncapBinderMethods
    {
    eBool (*ShouldAllocateHwBlockOnBinding)(ThaEncapBinder self);
    }tThaEncapBinderMethods;

typedef struct tThaEncapBinder
    {
    tAtEncapBinder super;
    const tThaEncapBinderMethods *methods;
    }tThaEncapBinder;

typedef struct tThaEncapDynamicBinderMethods
    {
    ThaBitMask (*ChannelPoolCreate)(ThaEncapDynamicBinder self, uint8 slice);
    }tThaEncapDynamicBinderMethods;

typedef struct tThaEncapDynamicBinder
    {
    tThaEncapBinder super;
    const tThaEncapDynamicBinderMethods * methods;

    /* Private data */
    ThaBitMask *channelPools;
    uint8 numPool;
    uint32 numChannelPerPool;
    }tThaEncapDynamicBinder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEncapBinder ThaEncapBinderObjectInit(AtEncapBinder self, AtDevice device);
AtEncapBinder ThaEncapDynamicBinderObjectInit(AtEncapBinder self, AtDevice device, uint8 numPool, uint32 numChannelPerPool);

uint32 ThaEncapDynamicBinderLoVcEncapChannelHwIdGet(AtEncapBinder self, AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _THAENCAPBINDERINTERNAL_H_ */


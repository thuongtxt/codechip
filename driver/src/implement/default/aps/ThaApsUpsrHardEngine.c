/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaApsUprsHardEngine.c
 *
 * Created Date: Jul 13, 2016
 *
 * Description : UPSR Engine Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "../../../generic/sdh/AtSdhPathInternal.h"
#include "../ocn/ThaModuleOcn.h"
#include "../xc/ThaVcCrossConnect.h"
#include "../sdh/ThaSdhVc.h"
#include "ThaModuleHardAps.h"
#include "ThaApsUpsrHardEngineInternal.h"
#include "ThaModuleApsReg.h"

/*--------------------------- Define -----------------------------------------*/


/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaApsUpsrHardEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods       m_AtChannelOverride;
static tAtApsEngineMethods     m_AtApsEngineOverride;
static tAtApsUpsrEngineMethods m_AtApsUpsrEngineOverride;

/* Save super implementation */
static const tAtApsUpsrEngineMethods *m_AtApsUpsrEngineMethods = NULL;
static const tAtApsEngineMethods     *m_AtApsEngineMethods     = NULL;
static const tAtChannelMethods       *m_AtChannelMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 UpsrHwPathIdGet(AtSdhPath channel)
    {
    uint8 sts1Id = AtSdhChannelSts1Get((AtSdhChannel)channel);
    uint8  hwSlice, hwSts;
    uint32 pathId = 0;
    eAtRet ret = ThaSdhChannelHwStsGet((AtSdhChannel)channel, cAtModuleAps, sts1Id, &hwSlice, &hwSts);
    if (ret != cAtOk)
        return ret;

    pathId |= (hwSlice & cBit3_0) << 6;
    pathId |= (hwSts   & cBit5_0);

    return pathId;
    }

static uint32 EngineAddressWithLocalAddress(AtApsEngine self, uint32 localAddress)
    {
    ThaModuleHardAps module = (ThaModuleHardAps)AtChannelModuleGet((AtChannel)self);
    return ThaModuleHardApsEngineAddressWithLocalAddress(module, AtChannelIdGet((AtChannel)self), localAddress);
    }

static uint32 EngineControl1Register(AtApsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_upsrsmramctl1_Base);
    }

static uint32 EngineControl2Register(AtApsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_upsrsmramctl2_Base);
    }

static eAtModuleApsRet PathsAssign(AtChannel self, eBool enable)
    {
    static const uint32 cUnusedPathId = cBit9_0;
    AtApsUpsrEngine engine = (AtApsUpsrEngine)self;
    uint32 workingPathId = (enable) ? UpsrHwPathIdGet(AtApsUpsrEngineWorkingPathGet(engine)) : cUnusedPathId;
    uint32 protectionPathId = (enable) ? UpsrHwPathIdGet(AtApsUpsrEngineProtectionPathGet(engine)) : cUnusedPathId;
    uint32 regAddress = EngineControl1Register((AtApsEngine)self);
    uint32 regValue = mChannelHwRead(self, regAddress, cAtModuleAps);
    mRegFieldSet(regValue, cAf6_upsrsmramctl1_UpsrSmWokPathId_, workingPathId);
    mRegFieldSet(regValue, cAf6_upsrsmramctl1_UpsrSmProPathId_, protectionPathId);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);
    return cAtOk;
    }

static eAtModuleApsRet PathTimerSet(AtSdhChannel path, uint32 holdOff, uint32 holdOn)
    {
    eAtRet ret = cAtOk;
    ret |= AtSdhChannelHoldOffTimerSet(path, holdOff);
    ret |= AtSdhChannelHoldOnTimerSet(path, holdOn);
    return ret;
    }

static eAtModuleApsRet PathsDefaultTimerSet(AtChannel self)
    {
    const uint32 holdOff = 2;
    const uint32 holdOn = 100;
    AtApsUpsrEngine engine = (AtApsUpsrEngine)self;
    eAtRet ret = cAtOk;

    ret |= PathTimerSet((AtSdhChannel)AtApsUpsrEngineWorkingPathGet(engine), holdOff, holdOn);
    ret |= PathTimerSet((AtSdhChannel)AtApsUpsrEngineProtectionPathGet(engine), holdOff, holdOn);

    return ret;
    }

static ThaVcCrossConnect VcCrossConnect(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    return (ThaVcCrossConnect)AtModuleXcVcCrossConnectGet(xcModule);
    }

static AtList DropPathsListGet(AtChannel self)
    {
    AtChannel workingPath = (AtChannel)AtApsUpsrEngineWorkingPathGet((AtApsUpsrEngine)self);
    return AtChannelDestinationChannels(workingPath);
    }

static eAtRet DropPathsAssign(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    ThaVcCrossConnect vcXc = VcCrossConnect(self);
    AtList pathList = DropPathsListGet(self);
    uint32 numPaths = AtListLengthGet(pathList);
    uint32 path_i;
    AtSdhChannel protectionPath = (AtSdhChannel)AtApsUpsrEngineProtectionPathGet((AtApsUpsrEngine)self);

    for (path_i = 0; path_i < numPaths; path_i++)
        {
        AtSdhChannel dropPath = (AtSdhChannel)AtListObjectGet(pathList, path_i);

        if (enable)
            {
            ret |= ThaVcCrossConnectVcConnectAtIndex(vcXc, dropPath, protectionPath, cThaXcVcConnectionProtection);
            ret |= ThaVcCrossConnectVcSelectorSet(vcXc, dropPath, cThaXcVcSelectorUpsr, AtChannelIdGet(self));
            }
        else
            {
            ret |= ThaVcCrossConnectVcDisconnectAtIndex(vcXc, dropPath, cThaXcVcConnectionProtection);
            ret |= ThaVcCrossConnectVcSelectorSet(vcXc, dropPath, cThaXcVcSelectorDisable, 0);
            }
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= PathsAssign(self, cAtTrue);
    ret |= PathsDefaultTimerSet(self);

    return ret;
    }

static eAtRet HwEnable(AtChannel self, eBool enable)
    {
    uint32 regAddress = EngineControl1Register((AtApsEngine)self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    mRegFieldSet(regValue, cAf6_upsrsmramctl1_UpsrSmEnable_, (enable) ? 1 : 0);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);
    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    uint32 regAddress = EngineControl1Register((AtApsEngine)self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    return mRegField(regValue, cAf6_upsrsmramctl1_UpsrSmEnable_) ? cAtTrue : cAtFalse;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    /* Engine can be disabled any time. */
    if (enable == cAtFalse)
        return cAtTrue;

    /* At least one dropped path must be connected before allow to enable UPSR engine
     * to ensure correct operation. */
    return AtListLengthGet(DropPathsListGet(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    if (!enable)
        {
        eAtRet ret;

        ret  = DropPathsAssign((AtChannel)self, enable);
        ret |= HwEnable(self, enable);
        return ret;
        }

    if (AtChannelCanEnable(self, enable))
        {
        eAtRet ret = DropPathsAssign((AtChannel)self, enable);
        if (ret != cAtOk)
            return ret;

        return HwEnable(self, enable);
        }

    return cAtErrorNotReady;
    }

static eAtRet HardwareCleanup(AtChannel self)
    {
    AtApsUpsrEngine engine = (AtApsUpsrEngine)self;
    eAtRet ret = m_AtChannelMethods->HardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    if (AtApsUpsrEngineActivePathGet(engine) == AtApsUpsrEngineProtectionPathGet(engine))
        {
        if (!AtChannelIsEnabled(self))
            ret |= AtChannelEnable(self, cAtTrue);

        /* Enforce to switch back to working path before deletion */
        ret |= AtApsUpsrEngineExtCmdSet(engine, cAtApsUpsrExtCmdLp);
        AtOsalUSleep(125);
        ret |= AtApsUpsrEngineExtCmdSet(engine, cAtApsUpsrExtCmdClear);
        }

    ret |= AtChannelEnable(self, cAtFalse);
    ret |= PathsAssign(self, cAtFalse);

    return ret;
    }

static eAtApsEngineState StateGet(AtApsEngine self)
    {
    return AtChannelIsEnabled((AtChannel)self) ? cAtApsEngineStateStart : cAtApsEngineStateStop;
    }

static eAtModuleApsRet WtrSw2Hw(AtApsEngine self, uint32 wtrInSec, uint32 *wtrHwValue, uint32 *wtrHwResolution)
    {
    ThaModuleHardAps module = (ThaModuleHardAps)AtChannelModuleGet((AtChannel)self);

    return ThaModuleHardApsWtrSw2Hw(module, wtrInSec, wtrHwValue, wtrHwResolution);
    }

static uint32 WtrHw2Sw(AtApsEngine self, uint32 wtrHwValue, uint32 wtrHwResolution)
    {
    ThaModuleHardAps module = (ThaModuleHardAps)AtChannelModuleGet((AtChannel)self);
    return ThaModuleHardApsWtrHw2Sw(module, wtrHwValue, wtrHwResolution);
    }

static eAtModuleApsRet WtrSet(AtApsEngine self, uint32 wtrInSec)
    {
    uint32 regAddress, regValue;
    uint32 wtrValue, resolutionValue;
    eAtRet ret;

    ret = WtrSw2Hw(self, wtrInSec, &wtrValue, &resolutionValue);
    if (ret != cAtOk)
        return ret;

    regAddress = EngineControl1Register(self);
    regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    mRegFieldSet(regValue, cAf6_upsrsmramctl1_UpsrSmWtrTmUnit_, resolutionValue);
    mRegFieldSet(regValue, cAf6_upsrsmramctl1_UpsrSmWtrTmMax_, wtrValue);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static uint32 WtrGet(AtApsEngine self)
    {
    uint32 regAddress = EngineControl1Register(self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    uint32 resolution = mRegField(regValue, cAf6_upsrsmramctl1_UpsrSmWtrTmUnit_);
    uint32 wtrValue   = mRegField(regValue, cAf6_upsrsmramctl1_UpsrSmWtrTmMax_);
    return WtrHw2Sw(self, wtrValue, resolution);
    }

static eAtModuleApsRet SwitchTypeSet(AtApsEngine self, eAtApsSwitchType swType)
    {
    uint32 regAddress = EngineControl1Register(self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    mRegFieldSet(regValue, cAf6_upsrsmramctl1_UpsrSmRevertMode_, (swType == cAtApsSwitchTypeRev) ? 1 : 0);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);
    return cAtOk;
    }

static eAtApsSwitchType SwitchTypeGet(AtApsEngine self)
    {
    uint32 regAddress = EngineControl1Register(self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    return (mRegField(regValue, cAf6_upsrsmramctl1_UpsrSmRevertMode_) == 1) ? cAtApsSwitchTypeRev : cAtApsSwitchTypeNonRev;
    }

static eAtModuleApsRet SwitchingConditionSet(AtApsEngine self, uint32 defects, eAtApsSwitchingCondition condition)
    {
    AtApsUpsrEngine engine = (AtApsUpsrEngine)self;
    AtSdhPath working = AtApsUpsrEngineWorkingPathGet(engine);
    AtSdhPath protection = AtApsUpsrEngineProtectionPathGet(engine);
    eAtRet ret = cAtOk;

    if (AtSdhPathHasPohProcessor(working))
        ret |= AtSdhPathApsSwitchingConditionSet(working, defects, condition);
    if (AtSdhPathHasPohProcessor(protection))
        ret |= AtSdhPathApsSwitchingConditionSet(protection, defects, condition);

    return ret;
    }

static eAtApsSwitchingCondition SwitchingConditionGet(AtApsEngine self, uint32 defect)
    {
    AtApsUpsrEngine engine = (AtApsUpsrEngine)self;
    AtSdhPath working = AtApsUpsrEngineWorkingPathGet(engine);
    AtSdhPath protection = AtApsUpsrEngineProtectionPathGet(engine);

    if (AtSdhPathHasPohProcessor(working))
        return AtSdhPathApsSwitchingConditionGet(working, defect);

    if (AtSdhPathHasPohProcessor(protection))
        return AtSdhPathApsSwitchingConditionGet(protection, defect);

    return cAtApsSwitchingConditionNone;
    }

static uint32 EngineStatusRegister(AtApsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_upsrsmcursta_Base);
    }

static uint32 InterruptStatusRegister(AtApsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_upsrsmintint_Base);
    }

static uint32 ExtCmdSw2Hw(eAtApsUpsrExtCmd extlCmd)
    {
    switch (extlCmd)
        {
        case cAtApsUpsrExtCmdLp     : return 5;
        case cAtApsUpsrExtCmdFs2Wrk : return 4;
        case cAtApsUpsrExtCmdFs2Prt : return 3;
        case cAtApsUpsrExtCmdMs2Wrk : return 2;
        case cAtApsUpsrExtCmdMs2Prt : return 1;
        case cAtApsUpsrExtCmdClear  : return 0;
        case cAtApsUpsrExtCmdUnknown: return 6;
        default:
            return 6;
        }
    }

static eAtModuleApsRet ExtCmdHwSet(AtApsUpsrEngine self, eAtApsUpsrExtCmd extlCmd)
    {
    uint32 regAddress = EngineControl2Register((AtApsEngine)self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    mRegFieldSet(regValue, cAf6_upsrsmramctl2_UpsrSmManualCmd_, ExtCmdSw2Hw(extlCmd));
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);
    return cAtOk;
    }

static eAtModuleApsRet ExtCmdSet(AtApsUpsrEngine self, eAtApsUpsrExtCmd extlCmd)
    {
    if (extlCmd == AtApsUpsrEngineExtCmdGet(self))
        return cAtOk;

    return ExtCmdHwSet(self, extlCmd);
    }

static eAtApsUpsrExtCmd ExtCmdHw2Sw(uint32 extlValue)
    {
    switch (extlValue)
        {
        case 5: return cAtApsUpsrExtCmdLp;
        case 4: return cAtApsUpsrExtCmdFs2Wrk;
        case 3: return cAtApsUpsrExtCmdFs2Prt;
        case 2: return cAtApsUpsrExtCmdMs2Wrk;
        case 1: return cAtApsUpsrExtCmdMs2Prt;
        case 0: return cAtApsUpsrExtCmdClear;
        default: return cAtApsUpsrExtCmdUnknown;
        }
    }

static eAtApsUpsrExtCmd ExtCmdGet(AtApsUpsrEngine self)
    {
    uint32 regAddress = EngineControl2Register((AtApsEngine)self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    return ExtCmdHw2Sw(mRegField(regValue, cAf6_upsrsmramctl2_UpsrSmManualCmd_));
    }

static uint32 ExtCmd2LocalState(eAtApsUpsrExtCmd extlCmd)
    {
    switch ((uint32)extlCmd)
        {
        case cAtApsUpsrExtCmdLp    : return cAtApsRequestStateLp;
        case cAtApsUpsrExtCmdFs2Prt: return cAtApsRequestStateFs;
        case cAtApsUpsrExtCmdFs2Wrk: return cAtApsRequestStateFs;
        case cAtApsUpsrExtCmdMs2Prt: return cAtApsRequestStateMs;
        case cAtApsUpsrExtCmdMs2Wrk: return cAtApsRequestStateMs;
        default:                     return cAtApsRequestStateNr;
        }
    }

static eAtModuleApsRet ExtCmdCheck(AtApsUpsrEngine self, eAtApsUpsrExtCmd extCmd)
    {
    eAtRet ret = m_AtApsUpsrEngineMethods->ExtCmdCheck(self, extCmd);
    if (ret != cAtOk)
        return ret;

    /* Same command is applied, just does nothing */
    if (extCmd == AtApsUpsrEngineExtCmdGet(self))
        return cAtOk;

    /* Clear command is always applicable. */
    if (extCmd == cAtApsUpsrExtCmdClear)
        return cAtOk;

    if (ExtCmd2LocalState(extCmd) > AtApsEngineRequestStateGet((AtApsEngine)self))
        return cAtOk;

    return cAtModuleApsErrorExtCmdLowPriority;
    }

static AtSdhPath ActivePathGet(AtApsUpsrEngine self)
    {
    uint32 regAddress = EngineStatusRegister((AtApsEngine)self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    uint8  actStatus  = (uint8)mRegField(regValue, cAf6_upsrsmcursta_UpsrSmStaActPath_);
    return (actStatus == 0) ? AtApsUpsrEngineWorkingPathGet(self) : AtApsUpsrEngineProtectionPathGet(self);
    }

static uint32 InterruptMaskRegister(AtApsEngine self)
    {
    return EngineAddressWithLocalAddress(self, cAf6Reg_upsrsmintenb_Base);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtApsEngineEventSwitch | cAtApsEngineEventStateChange);
    }

static uint32 StateChangeMask(AtChannel self)
    {
    static const uint32 stateChgMask =
            cAf6_upsrsmintenb_UpsrSmSDSFtoNRIntrEn_Mask |
            cAf6_upsrsmintenb_UpsrSmWTRtoNRIntrEn_Mask  |
            cAf6_upsrsmintenb_UpsrSmSDSFtoWTRIntrEn_Mask|
            cAf6_upsrsmintenb_UpsrSmtoMSIntrEn_Mask     |
            cAf6_upsrsmintenb_UpsrSmtoSDIntrEn_Mask     |
            cAf6_upsrsmintenb_UpsrSmtoSFIntrEn_Mask     |
            cAf6_upsrsmintenb_UpsrSmtoFSIntrEn_Mask     |
            cAf6_upsrsmintenb_UpsrSmtoLPIntrEn_Mask     |
            cAf6_upsrsmintenb_UpsrSmCLRtoNRIntrEn_Mask;
    AtUnused(self);

    return stateChgMask;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddress = InterruptMaskRegister((AtApsEngine)self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);

    if (defectMask & cAtApsEngineEventSwitch)
        mRegFieldSet(regValue, cAf6_upsrsmintenb_UpsrSmActPathChgIntrEn_, (enableMask & cAtApsEngineEventSwitch) ? 1 : 0);

    if (defectMask & cAtApsEngineEventStateChange)
        {
        if (enableMask & cAtApsEngineEventStateChange)
            regValue |= StateChangeMask(self);
        else
            regValue &= (uint32)~StateChangeMask(self);
        }
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddress = InterruptMaskRegister((AtApsEngine)self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    uint32 valMask = 0;

    if (regValue & cAf6_upsrsmintenb_UpsrSmActPathChgIntrEn_Mask)
        valMask |= cAtApsEngineEventSwitch;

    if (regValue & StateChangeMask(self))
        valMask |= cAtApsEngineEventStateChange;

    return valMask;
    }

static eBool ManualSwitchShouldBePreempted(AtApsEngine self)
    {
    eAtApsSwitchingReason switchingReason;
    eAtApsUpsrExtCmd externalCommand;

    externalCommand = AtApsUpsrEngineExtCmdGet((AtApsUpsrEngine)self);
    if ((externalCommand != cAtApsUpsrExtCmdMs2Prt) &&
        (externalCommand != cAtApsUpsrExtCmdMs2Wrk))
        return cAtFalse;

    switchingReason = AtApsEngineSwitchingReasonGet(self);
    if ((switchingReason != cAtApsSwitchingReasonSd) &&
        (switchingReason != cAtApsSwitchingReasonSf))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrAddr = InterruptStatusRegister((AtApsEngine)self);
    uint32 maskAddr = InterruptMaskRegister((AtApsEngine)self);
    uint32 intrValue = mChannelHwRead(self, intrAddr, cAtModuleAps);
    uint32 maskValue = mChannelHwRead(self, maskAddr, cAtModuleAps);
    uint32 events = 0;
    AtUnused(offset);

    intrValue &= maskValue;
    /* Clear interrupt. */
    mChannelHwWrite(self, intrAddr, intrValue, cAtModuleAps);

    if (intrValue & cAf6_upsrsmintenb_UpsrSmActPathChgIntrEn_Mask)
        events |= cAtApsEngineEventSwitch;

    if (intrValue & StateChangeMask(self))
        events |= cAtApsEngineEventStateChange;

    AtChannelAllAlarmListenersCall(self, events, 0);

    if (ManualSwitchShouldBePreempted((AtApsEngine)self))
        AtApsUpsrEngineExtCmdSet((AtApsUpsrEngine)self, cAtApsUpsrExtCmdClear);

    return 0;
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    AtApsEngine engine = (AtApsEngine)self;
    uint32 regAddress = InterruptStatusRegister(engine);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    uint32 alarmMask = 0;

    if (regValue & cAf6_upsrsmintenb_UpsrSmActPathChgIntrEn_Mask)
        alarmMask |= cAtApsEngineEventSwitch;

    if (regValue & StateChangeMask(self))
        alarmMask |= cAtApsEngineEventStateChange;

    if (read2Clear)
        mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    return alarmMask;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 LocalStateGet(AtApsEngine self)
    {
    uint32 regAddress = EngineStatusRegister(self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    return mRegField(regValue, cAf6_upsrsmcursta_UpsrSmStaCurState_);
    }

static eAtApsRequestState RequestStateGet(AtApsEngine self)
    {
    uint32 localReq = LocalStateGet(self);
    switch (localReq)
        {
        case 0:  return cAtApsRequestStateNr;
        case 1:  return cAtApsRequestStateWtr;
        case 2:  return cAtApsRequestStateMs;
        case 3:  return cAtApsRequestStateSd;
        case 4:  return cAtApsRequestStateSf;
        case 5:  return cAtApsRequestStateFs;
        case 6:  return cAtApsRequestStateLp;
        default: return cAtApsRequestStateUnknown;
        }
    }

static void OverrideAtChannel(AtApsEngine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, HardwareCleanup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtApsEngine(AtApsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtApsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsEngineOverride, m_AtApsEngineMethods, sizeof(m_AtApsEngineOverride));

        mMethodOverride(m_AtApsEngineOverride, StateGet);
        mMethodOverride(m_AtApsEngineOverride, WtrGet);
        mMethodOverride(m_AtApsEngineOverride, WtrSet);
        mMethodOverride(m_AtApsEngineOverride, SwitchTypeGet);
        mMethodOverride(m_AtApsEngineOverride, SwitchTypeSet);
        mMethodOverride(m_AtApsEngineOverride, SwitchingConditionSet);
        mMethodOverride(m_AtApsEngineOverride, SwitchingConditionGet);
        mMethodOverride(m_AtApsEngineOverride, RequestStateGet);
        }

    mMethodsSet(self, &m_AtApsEngineOverride);
    }

static void OverrideAtApsUpsrEngine(AtApsEngine self)
    {
    AtApsUpsrEngine engine = (AtApsUpsrEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtApsUpsrEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsUpsrEngineOverride, mMethodsGet(engine), sizeof(m_AtApsUpsrEngineOverride));

        mMethodOverride(m_AtApsUpsrEngineOverride, ExtCmdSet);
        mMethodOverride(m_AtApsUpsrEngineOverride, ExtCmdGet);
        mMethodOverride(m_AtApsUpsrEngineOverride, ExtCmdCheck);
        mMethodOverride(m_AtApsUpsrEngineOverride, ActivePathGet);
        }

    mMethodsSet(engine, &m_AtApsUpsrEngineOverride);
    }

static void Override(AtApsEngine self)
    {
    OverrideAtChannel(self);
    OverrideAtApsEngine(self);
    OverrideAtApsUpsrEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaApsUpsrHardEngine);
    }

AtApsEngine ThaUpsrHardEngineObjectInit(AtApsEngine self,
                                        AtModuleAps module,
                                        uint32 engineId,
                                        AtSdhPath working,
                                        AtSdhPath protection)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsUpsrEngineObjectInit(self, module, engineId, working, protection) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtApsEngine ThaUpsrHardEngineNew(AtModuleAps module, uint32 engineId, AtSdhPath working, AtSdhPath protection)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtApsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaUpsrHardEngineObjectInit(newEngine, module, engineId, working, protection);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaApsUprsHardEngineInternal.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : Internal APS UPSR engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAAPSUPRSHARDENGINEINTERNAL_H_
#define _THAAPSUPRSHARDENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/aps/AtApsUpsrEngineInternal.h"
#include "ThaApsUpsrHardEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaApsUpsrHardEngine
    {
    tAtApsUpsrEngine super;
    }tThaApsUpsrHardEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine ThaUpsrHardEngineObjectInit(AtApsEngine       self,
                                        AtModuleAps       module,
                                        uint32            engineId,
                                        AtSdhPath         working,
                                        AtSdhPath         protection);

#ifdef __cplusplus
}
#endif
#endif /* _THAAPSUPRSHARDENGINEINTERNAL_H_ */


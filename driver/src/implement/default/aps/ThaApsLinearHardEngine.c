/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaApsLinearHardEngine.c
 *
 * Created Date: Jan 16, 2015
 *
 * Description : Linear APS hardware engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaApsLinearHardEngineInternal.h"
#include "ThaModuleApsReg.h"

#include "../sdh/ThaModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)            ((ThaApsLinearHardEngine)self)
#define mApsLinearEngine(self) ((AtApsLinearEngine)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods         m_AtChannelOverride;
static tAtApsLinearEngineMethods m_AtApsLinearEngineOverride;
static tAtApsEngineMethods       m_AtApsEngineOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods         = NULL;
static const tAtApsEngineMethods       *m_AtApsEngineMethods       = NULL;
static const tAtApsLinearEngineMethods *m_AtApsLinearEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Offset(AtApsLinearEngine self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static eAtModuleApsRet WorkingLineSet(AtChannel self, AtSdhLine workingLine)
    {
    uint32 regAddress = cThaRegModuleApsControl + AtChannelIdGet(self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    uint8  channelId  = ThaModuleSdhLineLocalId(workingLine);

    mRegFieldSet(regValue, cThaRegModuleApsWorkingChannnelId, channelId);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static eAtModuleApsRet ProtectionLineSet(AtChannel self, AtSdhLine protectionLine)
    {
    uint32 regAddress = cThaRegModuleApsControl + AtChannelIdGet(self);
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    uint8  channelId  = ThaModuleSdhLineLocalId(protectionLine);

    mRegFieldSet(regValue, cThaRegModuleApsProtectChannelId, channelId);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static eAtRet DefaultAlarmMonitoringSet(AtChannel self)
    {
    uint32 regAddress  = cThaRegModuleApsControl + AtChannelIdGet(self);
    uint32 regValue    = mChannelHwRead(self, regAddress, cAtModuleAps);
    uint32 defaultMask = cThaRegModuleApsAlarmDefLos | cThaRegModuleApsAlarmDefLof | cThaRegModuleApsAlarmDefAis;

    mRegFieldSet(regValue, cThaRegModuleApsAlarmDefMask, defaultMask);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    AtList lines;

    if (ret != cAtOk)
        return ret;

    lines = AtApsLinearEngineLinesGet((AtApsLinearEngine)self);
    if (AtListLengthGet(lines) < 2)
        return ret;

    ret |= ProtectionLineSet(self, (AtSdhLine)AtListObjectGet(lines, 0));
    ret |= WorkingLineSet(self, (AtSdhLine)AtListObjectGet(lines, 1));
    ret |= DefaultAlarmMonitoringSet(self);
    ret |= AtApsLinearEngineArchSet((AtApsLinearEngine)self, mThis(self)->arch);

    return ret;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    uint32 regAddress = cThaRegModuleApsControl + Offset(mApsLinearEngine(self));
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);

    mRegFieldSet(regValue, cThaRegModuleApsHwEngineEnable, enable ? 1 : 0);
    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    uint32 regAddress = cThaRegModuleApsControl + Offset(mApsLinearEngine(self));
    uint32 regValue  = mChannelHwRead(self, regAddress, cAtModuleAps);
    return (regValue & cThaRegModuleApsHwEngineEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtApsEngineState StateGet(AtApsEngine self)
    {
    return AtChannelIsEnabled((AtChannel)self) ? cAtApsEngineStateStart : cAtApsEngineStateStop;
    }

static uint32 WtrGet(AtApsEngine self)
    {
	AtUnused(self);
    return cBit31_0;
    }

static eAtModuleApsRet WtrSet(AtApsEngine self, uint32 wtrInSec)
    {
	AtUnused(wtrInSec);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleApsRet SwitchTypeSet(AtApsEngine self, eAtApsSwitchType swType)
    {
	AtUnused(self);
    return (swType == cAtApsSwitchTypeNonRev) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtApsSwitchType SwitchTypeGet(AtApsEngine self)
    {
	AtUnused(self);
    return cAtApsSwitchTypeNonRev;
    }

static eAtModuleApsRet OpMdSet(AtApsLinearEngine self, eAtApsLinearOpMode opMd)
    {
	AtUnused(self);
    return (opMd == cAtApsLinearOpModeUni) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtApsLinearOpMode OpMdGet(AtApsLinearEngine self)
    {
	AtUnused(self);
    return cAtApsLinearOpModeUni;
    }

static eAtModuleApsRet ArchSet(AtApsLinearEngine self, eAtApsLinearArch arch)
    {
    uint32 regAddress = cThaRegModuleApsControl + Offset(mApsLinearEngine(self));
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);

    mRegFieldSet(regValue, cThaRegModuleApsForceBrigde, (arch == cAtApsLinearArch11) ? 0x1 : 0x0);
    mRegFieldSet(regValue, cThaRegModuleApsEnableBrigde, 0x1);

    mChannelHwWrite(self, regAddress, regValue, cAtModuleAps);

    /* Updated database */
    mThis(self)->arch = arch;

    return cAtOk;
    }

static eAtApsLinearArch ArchGet(AtApsLinearEngine self)
    {
    uint32 regAddress = cThaRegModuleApsControl + Offset(mApsLinearEngine(self));
    uint32 regValue   = mChannelHwRead(self, regAddress, cAtModuleAps);
    return (mRegField(regValue, cThaRegModuleApsForceBrigde)) ? cAtApsLinearArch11 : cAtApsLinearArch1n;
    }

static eBool ExtCmdIsValid(eAtApsLinearExtCmd extlCmd)
    {
    if ((extlCmd == cAtApsLinearExtCmdFs) ||
        (extlCmd == cAtApsLinearExtCmdClear))
        return cAtTrue;
    return cAtFalse;
    }

static eAtModuleApsRet ExtCmdSet(AtApsLinearEngine engine, eAtApsLinearExtCmd extlCmd, AtSdhLine affectedLine)
    {
    uint32 regAddress, regValue;
    uint8 fs;

    if (!ExtCmdIsValid(extlCmd))
        return cAtErrorModeNotSupport;

    /* Do nothing If hardware engine do not start */
    if (AtApsEngineStateGet((AtApsEngine)engine) != cAtApsEngineStateStart)
        return cAtOk;

    regAddress = cThaRegModuleApsControl + Offset(engine);
    regValue   = mChannelHwRead(engine, regAddress, cAtModuleAps);

    /* 1: enable forces, 0 clear forces */
    fs = (extlCmd == cAtApsLinearExtCmdFs) ? 0x1 : 0x0;
    mRegFieldSet(regValue, cThaRegModuleApsForceSwitch, fs);

    /* Clear force = 1 : forces to working, Enable forces 0 forces to protect */
    fs = (extlCmd == cAtApsLinearExtCmdFs) ? 0x0 : 0x1;
    mRegFieldSet(regValue, cThaRegModuleApsForceSwitchMode, fs);
    mChannelHwWrite(engine, regAddress, regValue, cAtModuleAps);

    mThis(engine)->affectedLine = affectedLine;

    return cAtOk;
    }

static eAtApsLinearExtCmd ExtCmdGet(AtApsLinearEngine engine)
    {
    uint32 regAddress = cThaRegModuleApsControl + Offset(engine);
    uint32 regValue   = mChannelHwRead(engine, regAddress, cAtModuleAps);
    return mRegField(regValue, cThaRegModuleApsForceSwitch) ? cAtApsLinearExtCmdFs : cAtApsLinearExtCmdClear;
    }

static AtSdhLine ExtCmdAffectedLineGet(AtApsLinearEngine engine)
    {
    return mThis(engine)->affectedLine;
    }

static AtSdhLine SwitchLineGet(AtApsLinearEngine self)
    {
    uint32 regValues = mChannelHwRead(self, cThaRegModuleApsStatus, cAtModuleAps);
    uint8 switchedId;

    if (regValues & cThaRegModuleApsSwitchEnabledMask)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel)self);
        switchedId = (uint8)mRegField(regValues, cThaRegModuleApsSwitchedId);
        return AtModuleSdhLineGet((AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh), switchedId);
        }

    return NULL;
    }

static void OverrideAtChannel(AtApsEngine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtApsEngine(AtApsEngine self)
    {
    AtApsEngine engine = (AtApsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtApsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsEngineOverride, m_AtApsEngineMethods, sizeof(m_AtApsEngineOverride));
        mMethodOverride(m_AtApsEngineOverride, StateGet);
        mMethodOverride(m_AtApsEngineOverride, WtrGet);
        mMethodOverride(m_AtApsEngineOverride, WtrSet);
        mMethodOverride(m_AtApsEngineOverride, SwitchTypeGet);
        mMethodOverride(m_AtApsEngineOverride, SwitchTypeSet);
        }

    mMethodsSet(engine, &m_AtApsEngineOverride);
    }

static void OverrideAtApsLinearEngine(AtApsEngine self)
    {
    AtApsLinearEngine engine = (AtApsLinearEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtApsLinearEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsLinearEngineOverride, m_AtApsLinearEngineMethods, sizeof(m_AtApsLinearEngineOverride));
        mMethodOverride(m_AtApsLinearEngineOverride, OpMdGet);
        mMethodOverride(m_AtApsLinearEngineOverride, OpMdSet);
        mMethodOverride(m_AtApsLinearEngineOverride, ArchGet);
        mMethodOverride(m_AtApsLinearEngineOverride, ArchSet);
        mMethodOverride(m_AtApsLinearEngineOverride, ExtCmdSet);
        mMethodOverride(m_AtApsLinearEngineOverride, ExtCmdGet);
        mMethodOverride(m_AtApsLinearEngineOverride, ExtCmdAffectedLineGet);
        mMethodOverride(m_AtApsLinearEngineOverride, SwitchLineGet);
        }

    mMethodsSet(engine, &m_AtApsLinearEngineOverride);
    }

static void Override(AtApsEngine self)
    {
    OverrideAtChannel(self);
    OverrideAtApsEngine(self);
    OverrideAtApsLinearEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaApsLinearHardEngine);
    }

static AtApsEngine ObjectInit(AtApsEngine self,
                              AtModuleAps module,
                              uint32 engineId,
                              AtSdhLine lines[],
                              uint8 numLine,
                              uint8 arch)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsLinearEngineObjectInit(self, module, engineId, lines, numLine) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->arch = arch;

    return self;
    }

AtApsEngine ThaApsLinearHardEngineNew(AtModuleAps module, uint32 engineId, AtSdhLine lines[], uint8 numLine, uint8 arch)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtApsEngine newApsEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newApsEngine, module, engineId, lines, numLine, arch);
    }

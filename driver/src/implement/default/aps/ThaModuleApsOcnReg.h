/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaModuleApsOcnReg.h
 * 
 * Created Date: Dec 5, 2016
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEAPSOCNREG_H_
#define _THAMODULEAPSOCNREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : OCN Global APS Grouping SXC_Page Selection
Reg Addr   : 0xf0006
Reg Formula:
    Where  :
Reg Desc   :
This register use 16 bits to configure SXC_Page ID for 16 APS Group. Bit 0 for APS Group 0

------------------------------------------------------------------------------*/
#define cAf6Reg_glbapsgrppage_reg                                                                      0xf0006

/*--------------------------------------
BitField Name: ApsGrpPageId
BitField Type: RW
BitField Desc: Each bit is used to configure SXC_Page ID for 16 APS Group. Bit 0
for APS Group 0 1: SXC_Page ID 1 is selected 0: SXC_Page ID 0 is selected
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbapsgrppage_reg_ApsGrpPageId_Mask                                                      cBit15_0
#define cAf6_glbapsgrppage_reg_ApsGrpPageId_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Passthrough Grouping Enable
Reg Addr   : 0xf0007
Reg Formula:
    Where  :
Reg Desc   :
This register use 32 bits to configure for 16 Passthrough Groups at Rx direction and 16 Passthrough Groups at Tx direction.

------------------------------------------------------------------------------*/
#define cAf6Reg_glbpasgrpenb_reg                                                                       0xf0007

/*--------------------------------------
BitField Name: TxSpgPassThrGrpEnb
BitField Type: RW
BitField Desc: Each bit is used to configure enable/disable Passthrough mode at
Tx SPG for 1 Passthrough Group. Bit 16 for Passthrough Group Tx0 1: Enable 0:
Disable.
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_glbpasgrpenb_reg_TxSpgPassThrGrpEnb_Mask                                                cBit31_16
#define cAf6_glbpasgrpenb_reg_TxSpgPassThrGrpEnb_Shift                                                      16

/*--------------------------------------
BitField Name: RxSpiPassThrGrpEnb
BitField Type: RW
BitField Desc: Each bit is used to configure enable/disable Passthrough mode at
Rx SPI for 1 Passthrough Group. Bit 0 for Passthrough Group Rx0 1: Enable 0:
Disable.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_glbpasgrpenb_reg_RxSpiPassThrGrpEnb_Mask                                                 cBit15_0
#define cAf6_glbpasgrpenb_reg_RxSpiPassThrGrpEnb_Shift                                                       0



/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEAPSOCNREG_H_ */


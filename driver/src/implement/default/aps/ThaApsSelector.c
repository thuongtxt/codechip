/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaApsSelector.c
 *
 * Created Date: Aug 8, 2016
 *
 * Description : Default 2-to-1 APS selector.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/aps/AtApsSelectorInternal.h"
#include "AtApsSelector.h"
#include "AtApsGroup.h"
#include "AtSdhChannel.h"
#include "AtModuleXc.h"
#include "ThaApsSelector.h"
#include "../xc/ThaVcCrossConnect.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((tThaApsSelector*)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaApsSelector
    {
    tAtApsSelector super;

    /* TODO: remove when RTL ready */
    AtChannel selectedChannel;
    }tThaApsSelector;

typedef enum eThaApsSelectorState
    {
    cThaApsSelectorWorking      = 0,
    cThaApsSelectorProtection   = 1,
    cThaApsSelectorUnknown
    }eThaApsSelectorState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtApsSelectorMethods m_AtApsSelectorOverride;

/* Override */
static const tAtApsSelectorMethods *m_AtApsSelectorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaVcCrossConnect VcCrossConnect(AtApsSelector self)
    {
    AtModuleAps apsModule = AtApsSelectorModuleGet(self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)apsModule), cAtModuleXc);
    return (ThaVcCrossConnect)AtModuleXcVcCrossConnectGet(xcModule);
    }

static AtSdhChannel OutgoingVc(AtApsSelector self)
    {
    return (AtSdhChannel)AtApsSelectorOutgoingChannelGet(self);
    }

static eAtRet Init(AtApsSelector self)
    {
    eAtRet ret;
    ThaVcCrossConnect vcXc = VcCrossConnect(self);
    AtSdhChannel destVc = OutgoingVc(self);
    AtSdhChannel workingVc = (AtSdhChannel)AtApsSelectorWorkingChannelGet(self);
    AtSdhChannel protectVc = (AtSdhChannel)AtApsSelectorProtectionChannelGet(self);

    ret  = ThaVcCrossConnectVcConnectAtIndex(vcXc, destVc, workingVc, cThaApsSelectorWorking);
    ret |= ThaVcCrossConnectVcConnectAtIndex(vcXc, destVc, protectVc, cThaApsSelectorProtection);
    if (ret != cAtOk)
        return ret;

    return m_AtApsSelectorMethods->Init(self);
    }

static eAtRet HardwareCleanup(AtApsSelector self)
    {
    eAtRet ret;
    ThaVcCrossConnect vcXc = VcCrossConnect(self);
    AtSdhChannel destVc = OutgoingVc(self);

    ret  = ThaVcCrossConnectVcDisconnectAtIndex(vcXc, destVc, cThaApsSelectorWorking);
    ret |= ThaVcCrossConnectVcDisconnectAtIndex(vcXc, destVc, cThaApsSelectorProtection);
    ret |= AtApsSelectorEnable(self, cAtFalse);

    return ret;
    }

static uint32 SelectorType(AtApsSelector self, eBool enable)
    {
    if (!enable)
        return cThaXcVcSelectorDisable;

    if (AtApsSelectorGroupGet(self))
        return cThaXcVcSelectorApsGroup;

    return cThaXcVcSelectorDeviceRole;
    }

static uint32 SelectorID(AtApsSelector self, eBool enable)
    {
    if (enable)
        {
        AtApsGroup group = AtApsSelectorGroupGet(self);
        if (group != NULL)
            return AtApsGroupIdGet(group);
        }
    return 0;
    }

static eAtRet Enable(AtApsSelector self, eBool enable)
    {
    ThaVcCrossConnect vcXc = VcCrossConnect(self);
    AtSdhChannel destVc = OutgoingVc(self);
    return ThaVcCrossConnectVcSelectorSet(vcXc, destVc, SelectorType(self, enable), SelectorID(self, enable));
    }

static eBool IsEnabled(AtApsSelector self)
    {
    ThaVcCrossConnect vcXc = VcCrossConnect(self);
    uint32 selectorType = ThaVcCrossConnectVcSelectorTypeGet(vcXc, OutgoingVc(self));

    if ((selectorType == cThaXcVcSelectorDisable) || (selectorType == cThaXcVcSelectorUnknown))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleApsRet ChannelSelect(AtApsSelector self, AtChannel selectedChannel)
    {
    if (AtApsSelectorIsEnabled(self))
        return cAtErrorNotEditable;

    /* TODO: remove when RTL update. */
    mThis(self)->selectedChannel = selectedChannel;
    return cAtOk;
    }

static AtChannel State2SelectedChannel(AtApsSelector self)
    {
    uint32 xcSelectorState = ThaVcCrossConnectVcSelectorStateGet(VcCrossConnect(self), OutgoingVc(self));
    if (xcSelectorState == cThaXcVcSelectorStateActive)
        return AtApsSelectorWorkingChannelGet(self);
    if (xcSelectorState == cThaXcVcSelectorStateStandby)
        return AtApsSelectorProtectionChannelGet(self);

    return AtApsSelectorWorkingChannelGet(self);
    }

static AtChannel SelectedChannel(AtApsSelector self)
    {
    if (AtApsSelectorIsEnabled(self))
        return State2SelectedChannel(self);

    /* TODO: remove when RTL update. */
    return mThis(self)->selectedChannel;
    }

static eAtModuleApsRet GroupSet(AtApsSelector self, AtApsGroup group)
    {
    eAtRet ret;
    ret = m_AtApsSelectorMethods->GroupSet(self, group);
    if (ret != cAtOk)
        return ret;

    /* Calling enable() will update selectorType and selector ID as well. */
    return AtApsSelectorEnable(self, cAtTrue);
    }

static void OverrideAtApsSelector(AtApsSelector self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtApsSelectorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsSelectorOverride, mMethodsGet(self), sizeof(m_AtApsSelectorOverride));

        mMethodOverride(m_AtApsSelectorOverride, Init);
        mMethodOverride(m_AtApsSelectorOverride, Enable);
        mMethodOverride(m_AtApsSelectorOverride, IsEnabled);
        mMethodOverride(m_AtApsSelectorOverride, ChannelSelect);
        mMethodOverride(m_AtApsSelectorOverride, SelectedChannel);
        mMethodOverride(m_AtApsSelectorOverride, GroupSet);
        }

    mMethodsSet(self, &m_AtApsSelectorOverride);
    }

static void Override(AtApsSelector self)
    {
    OverrideAtApsSelector(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaApsSelector);
    }

static AtApsSelector ObjectInit(AtApsSelector   self,
                                AtModuleAps   module,
                                uint32        selectorId,
                                AtChannel     working,
                                AtChannel     protection,
                                AtChannel     outGoing)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsSelectorObjectInit(self, module, selectorId, working, protection, outGoing) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtApsSelector ThaApsSelectorNew(AtModuleAps module, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outGoing)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtApsSelector newSelector = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newSelector, module, selectorId, working, protection, outGoing);
    }

eAtRet ThaApsSelectorHardwareCleanup(AtApsSelector self)
    {
    if (self)
        return HardwareCleanup(self);
    return cAtErrorNullPointer;
    }


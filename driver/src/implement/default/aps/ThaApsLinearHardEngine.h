/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaApsLinearHardEngine.h
 * 
 * Created Date: Jan 16, 2015
 *
 * Description : Linear APS hardware engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAAPSLINEARHARDENGINE_H_
#define _THAAPSLINEARHARDENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsLinearEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaApsLinearHardEngine *ThaApsLinearHardEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine ThaApsLinearHardEngineNew(AtModuleAps module, uint32 engineId, AtSdhLine lines[], uint8 numLine, uint8 arch);

#endif /* _THAAPSLINEARHARDENGINE_H_ */


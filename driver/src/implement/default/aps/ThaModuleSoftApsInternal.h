/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaModuleApsInternal.h
 * 
 * Created Date: Sep 17, 2013
 *
 * Description : APS module of Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESOFTAPSINTERNAL_H_
#define _THAMODULESOFTAPSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/aps/AtModuleSoftApsInternal.h"
#include "ThaModuleSoftAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleSoftApsMethods
    {
    uint8 dummy;
    }tThaModuleSoftApsMethods;

typedef struct tThaModuleSoftAps
    {
    tAtModuleSoftAps super;
    const tThaModuleSoftApsMethods *methods;

    /* Private data */
    }tThaModuleSoftAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THAMODULESOFTAPSINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaApsGroup.c
 *
 * Created Date: Aug 8, 2016
 *
 * Description : APS group interface for Tha Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/aps/AtApsGroupInternal.h"
#include "../../../generic/man/AtModuleInternal.h"
#include "../man/ThaDevice.h"
#include "../ocn/ThaModuleOcn.h"
#include "ThaApsGroup.h"
#include "ThaModuleApsOcnReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaApsGroup
    {
    tAtApsGroup super;
    }tThaApsGroup;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtApsGroupMethods m_AtApsGroupOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtApsGroup self)
    {
    AtModuleAps apsModule = AtApsGroupModuleGet(self);
    return (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)apsModule), cThaModuleOcn);
    }

static uint32 StateMask(AtApsGroup self)
    {
    return cBit0 << AtApsGroupIdGet(self);
    }

static eAtModuleApsRet StateSet(AtApsGroup self, eAtApsGroupState state)
    {
    ThaModuleOcn ocnModule = OcnModule(self);
    uint32 regAddr = ThaModuleOcnBaseAddress(ocnModule) + cAf6Reg_glbapsgrppage_reg;
    uint32 regValue = mModuleHwRead(ocnModule, regAddr);
    uint32 shift = AtApsGroupIdGet(self);
    uint32 mask = StateMask(self);
    mFieldIns(&regValue, mask, shift, (state == cAtApsGroupStateWorking) ? 0 : 1);
    mModuleHwWrite(ocnModule, regAddr, regValue);
    return cAtOk;
    }

static eAtApsGroupState StateGet(AtApsGroup self)
    {
    ThaModuleOcn ocnModule = OcnModule(self);
    uint32 regAddr = ThaModuleOcnBaseAddress(ocnModule) + cAf6Reg_glbapsgrppage_reg;
    uint32 regValue = mModuleHwRead(ocnModule, regAddr);
    uint32 mask = StateMask(self);
    return (regValue & mask) ? cAtApsGroupStateProtection : cAtApsGroupStateWorking;
    }

static void OverrideAtApsGroup(AtApsGroup self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsGroupOverride, mMethodsGet(self), sizeof(m_AtApsGroupOverride));

        mMethodOverride(m_AtApsGroupOverride, StateSet);
        mMethodOverride(m_AtApsGroupOverride, StateGet);
        }

    mMethodsSet(self, &m_AtApsGroupOverride);
    }

static void Override(AtApsGroup self)
    {
    OverrideAtApsGroup(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaApsGroup);
    }

static AtApsGroup ObjectInit(AtApsGroup self, AtModuleAps module, uint32 groupId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsGroupObjectInit(self, module, groupId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtApsGroup ThaApsGroupNew(AtModuleAps module, uint32 groupId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtApsGroup newGroup = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newGroup, module, groupId);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaModuleAps.h
 * 
 * Created Date: Sep 17, 2013
 *
 * Description : APS module of Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEAPS_H_
#define _THAMODULEAPS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleAps * ThaModuleAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps ThaModuleSoftApsNew(AtDevice device);

#endif /* _THAMODULEAPS_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaModuleHardAps.c
 *
 * Created Date: Jan 16, 2015
 *
 * Description : APS module of Thalassa product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaStsGroup.h"
#include "ThaModuleHardApsInternal.h"
#include "ThaApsLinearHardEngine.h"
#include "ThaApsUpsrHardEngine.h"
#include "ThaApsGroup.h"
#include "ThaApsSelector.h"
#include "ThaModuleApsReg.h"
#include "ThaApsInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#define cHoldTimerResolution100Ms   5
#define cHoldTimerResolution50Ms    4
#define cHoldTimerResolution10Ms    3
#define cHoldTimerResolution5Ms     2
#define cHoldTimerResolution1Ms     1
/* WTR resolution */
#define cWtrResolutionMinute     1
#define cWtrResolution30Seconds  0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleHardAps)self)

#define mSwitchingConditionGet(regVal, defect)                                 \
    SwitchingConditionSw2Sw(regVal, cAf6_upsralrmmasksfsdramctl_Upsr##defect##MaskSf_Mask, cAf6_upsralrmmasksfsdramctl_Upsr##defect##MaskSd_Mask)

#define mSwitchingConditionSet(regVal, defect, sf, sd)                         \
    do {                                                                       \
    mRegFieldSet(regVal, cAf6_upsralrmmasksfsdramctl_Upsr##defect##MaskSf_, sf);\
    mRegFieldSet(regVal, cAf6_upsralrmmasksfsdramctl_Upsr##defect##MaskSd_, sd);\
    } while (0)

#define mForcedSwitchingConditionGet(regVal, _type)                             \
    mRegField(regVal, cAf6_upsralrmmasksfsdramctl_UpsrForce##_type##_)

#define mForcedSwitchingConditionSet(regVal, _type, _value)                     \
    mRegFieldSet(regVal, cAf6_upsralrmmasksfsdramctl_UpsrForce##_type##_, _value)

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleHardApsMethods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModuleApsMethods  m_AtModuleApsOverride;

/* To save super implementation */
static const tAtModuleMethods    *m_AtModuleMethods    = NULL;
static const tAtModuleApsMethods *m_AtModuleApsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static  eBool LinearIsSupported(ThaModuleHardAps self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static  eBool UpsrIsSupported(ThaModuleHardAps self)
    {
	AtUnused(self);
	return cAtFalse;
    }

static AtApsEngine LinearEngineCreate(AtModuleAps self,
                                      uint32 engineId,
                                      AtSdhLine lines[],
                                      uint8 numLines,
                                      eAtApsLinearArch arch)
    {
    return ThaApsLinearHardEngineNew(self, engineId, lines, numLines, arch);
    }

static uint32 MaxNumLinearEngines(AtModuleAps self)
    {
	if (mMethodsGet((ThaModuleHardAps)self)->LinearIsSupported((ThaModuleHardAps)self))
		return m_AtModuleApsMethods->MaxNumLinearEngines(self);
    return 0;
    }

static AtApsEngine UpsrEngineCreate(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection)
    {
    return ThaUpsrHardEngineNew(self, engineId, working, protection);
    }

static eAtModuleApsRet UpsrEngineDelete(AtModuleAps self, uint32 engineId)
    {
    eAtRet ret = cAtOk;
    AtApsEngine engine = AtModuleApsUpsrEngineGet(self, engineId);

    ret |= AtChannelHardwareCleanup((AtChannel)engine);
    ret |= m_AtModuleApsMethods->UpsrEngineDelete(self, engineId);

    return ret;
    }

static AtApsSelector SelectorCreate(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing)
    {
    return ThaApsSelectorNew(self, selectorId, working, protection, outgoing);
    }

static eAtModuleApsRet SelectorDelete(AtModuleAps self, uint32 selectorId)
    {
    eAtRet ret = cAtOk;
    AtApsSelector selector = AtModuleApsSelectorGet(self, selectorId);

    ret |= ThaApsSelectorHardwareCleanup(selector);
    ret |= m_AtModuleApsMethods->SelectorDelete(self, selectorId);

    return ret;
    }

static AtApsGroup GroupCreate(AtModuleAps self, uint32 groupId)
    {
    return ThaApsGroupNew(self, groupId);
    }

static AtStsGroup StsGroupCreate(AtModuleAps self, uint32 groupId)
    {
    return ThaStsGroupNew((AtModule)self, groupId);
    }

static uint32 BaseAddress(ThaModuleHardAps self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 PathHoldTimerRegister(ThaModuleHardAps self, AtSdhPath path)
    {
    uint32 offset = ThaModuleHardApsUpsrPathOffset(self, path);
    return cAf6Reg_upsrholdwtrtimerramctl_Base + offset + ThaModuleHardApsBaseAddress(self);
    }

static uint32 Timer2Resolution(uint32 timerValue, uint32 timerMax)
    {
    if (timerValue <= timerMax)
        return cHoldTimerResolution1Ms;

    if (timerValue <= (timerMax * 5))
        return cHoldTimerResolution5Ms;

    if (timerValue <= (timerMax * 10))
        return cHoldTimerResolution10Ms;

    if (timerValue <= (timerMax * 50))
        return cHoldTimerResolution50Ms;

    if (timerValue <= (timerMax * 100))
        return cHoldTimerResolution100Ms;

    return cInvalidUint32;
    }

static uint32 Resolution2Value(uint32 resolutionValue)
    {
    if (resolutionValue == cHoldTimerResolution1Ms)
        return 1;

    if (resolutionValue == cHoldTimerResolution5Ms)
        return 5;

    if (resolutionValue == cHoldTimerResolution10Ms)
        return 10;

    if (resolutionValue == cHoldTimerResolution50Ms)
        return 50;

    if (resolutionValue == cHoldTimerResolution100Ms)
        return 100;

    return 0;
    }

static eAtRet HwPathHoldTimerSet(ThaModuleHardAps self, AtSdhPath path,
                                 uint32 timerValue,
                                 uint32 timerValueMask, uint32 timerValueShift,
                                 uint32 resolutionMask, uint32 resolutionShift)
    {
    uint32 regValue;
    uint32 timerMax, resolution, resolutionValue, resolutionTimerValue;
    uint32 regAddress = PathHoldTimerRegister(self, path);

    if (regAddress == cInvalidUint32)
        return cAtErrorOutOfRangParm;

    timerMax = (timerValueMask >> timerValueShift);
    resolution = Timer2Resolution(timerValue, timerMax);
    if (resolution == cInvalidUint32)
        return cAtErrorOutOfRangParm;

    resolutionValue = Resolution2Value(resolution);
    resolutionTimerValue = resolutionValue ? (timerValue / resolutionValue) : 0;
    regValue = mChannelHwRead(path, regAddress, cAtModuleAps);
    mRegFieldSet(regValue, resolution, resolution);
    mRegFieldSet(regValue, timerValue, resolutionTimerValue);
    mChannelHwWrite(path, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static uint32 HwPathHoldTimerGet(ThaModuleHardAps self, AtSdhPath path,
                                 uint32 timerValueMask, uint32 timerValueShift,
                                 uint32 resolutionMask, uint32 resolutionShift)
    {
    uint32 regValue;
    uint32 resolution, timerValue, resolutionValue;
    uint32 regAddress = PathHoldTimerRegister(self, path);

    if (regAddress == cInvalidUint32)
        return cAtErrorOutOfRangParm;

    regValue = mChannelHwRead(path, regAddress, cAtModuleAps);
    resolution = mRegField(regValue, resolution);
    timerValue = mRegField(regValue, timerValue);
    resolutionValue = Resolution2Value(resolution);
    if (resolutionValue == cInvalidUint32)
        return 0;

    return timerValue * resolutionValue;
    }

static eAtRet PathHoldOffTimerSet(ThaModuleHardAps self, AtSdhPath path, uint32 holdOffTimer)
    {
    return HwPathHoldTimerSet(self, path, holdOffTimer,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmMax_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmMax_Shift,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmUnit_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmUnit_Shift);
    }

static uint32 PathHoldOffTimerGet(ThaModuleHardAps self, AtSdhPath path)
    {
    return HwPathHoldTimerGet(self, path,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmMax_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmMax_Shift,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmUnit_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmUnit_Shift);
    }

static eAtRet PathHoldOnTimerSet(ThaModuleHardAps self, AtSdhPath path, uint32 holdOnTimer)
    {
    return HwPathHoldTimerSet(self, path,
                              holdOnTimer,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmMax_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmMax_Shift,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmUnit_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmUnit_Shift);
    }

static uint32 PathHoldOnTimerGet(ThaModuleHardAps self, AtSdhPath path)
    {
    return HwPathHoldTimerGet(self, path,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmMax_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmMax_Shift,
                              cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmUnit_Mask, cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmUnit_Shift);
    }

static eBool DefectsIsSupported(uint32 defects)
    {
    const uint32 supportedDefects = (cAtSdhPathAlarmAis|cAtSdhPathAlarmLop|
                                     cAtSdhPathAlarmTim|cAtSdhPathAlarmUneq|cAtSdhPathAlarmPlm|
                                     cAtSdhPathAlarmBerSd|cAtSdhPathAlarmBerSf);
    return (defects & ~supportedDefects) ? cAtFalse : cAtTrue;
    }

static uint32 PathDefectsMaskRegister(ThaModuleHardAps self, AtSdhPath path)
    {
    uint32 pathOffset = ThaModuleHardApsUpsrPathDefectMaskOffset(self, path);
    return ThaModuleHardApsBaseAddress(self) + cAf6Reg_upsralrmmasksfsdramctl_Base + pathOffset;
    }

static eAtRet PathSwitchingConditionSet(ThaModuleHardAps self, AtSdhPath path, uint32 defects, eAtApsSwitchingCondition condition)
    {
    uint32 regAddress, regValue;
    uint8 sf = (condition == cAtApsSwitchingConditionSf) ? 1 : 0;
    uint8 sd = (condition == cAtApsSwitchingConditionSd) ? 1 : 0;

    if (!DefectsIsSupported(defects))
        return cAtErrorModeNotSupport;

    regAddress = PathDefectsMaskRegister(self, path);
    regValue = mChannelHwRead(path, regAddress, cAtModuleAps);

    if (defects & cAtSdhPathAlarmAis)
        mSwitchingConditionSet(regValue, Ais, sf, sd);

    if (defects & cAtSdhPathAlarmLop)
        mSwitchingConditionSet(regValue, Lop, sf, sd);

    if (defects & cAtSdhPathAlarmTim)
        mSwitchingConditionSet(regValue, Tim, sf, sd);

    if (defects & cAtSdhPathAlarmUneq)
        mSwitchingConditionSet(regValue, Uneq, sf, sd);

    if (defects & cAtSdhPathAlarmPlm)
        mSwitchingConditionSet(regValue, Plm, sf, sd);

    if (defects & cAtSdhPathAlarmBerSd)
        mSwitchingConditionSet(regValue, BerSD, sf, sd);

    if (defects & cAtSdhPathAlarmBerSf)
        mSwitchingConditionSet(regValue, BerSF, sf, sd);

    mChannelHwWrite(path, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static eAtApsSwitchingCondition SwitchingConditionSw2Sw(uint32 regValue, uint32 sfMask, uint32 sdMask)
    {
    if (regValue & sfMask)
        return cAtApsSwitchingConditionSf;
    if (regValue & sdMask)
        return cAtApsSwitchingConditionSd;
    return cAtApsSwitchingConditionNone;
    }

static eAtApsSwitchingCondition PathSwitchingConditionGet(ThaModuleHardAps self, AtSdhPath path, uint32 defect)
    {
    uint32 regAddress = PathDefectsMaskRegister(self, path);
    uint32 regValue = mChannelHwRead(path, regAddress, cAtModuleAps);

    if (defect & cAtSdhPathAlarmAis)
        return mSwitchingConditionGet(regValue, Ais);

    if (defect & cAtSdhPathAlarmLop)
        return mSwitchingConditionGet(regValue, Lop);

    if (defect & cAtSdhPathAlarmTim)
        return mSwitchingConditionGet(regValue, Tim);

    if (defect & cAtSdhPathAlarmUneq)
        return mSwitchingConditionGet(regValue, Uneq);

    if (defect & cAtSdhPathAlarmPlm)
        return mSwitchingConditionGet(regValue, Plm);

    if (defect & cAtSdhPathAlarmBerSd)
        return mSwitchingConditionGet(regValue, BerSD);

    if (defect & cAtSdhPathAlarmBerSf)
        return mSwitchingConditionGet(regValue, BerSF);

    return cAtApsSwitchingConditionNone;
    }

static uint32 UpsrPathOffset(ThaModuleHardAps self, AtSdhPath path)
    {
    AtSdhChannel channel = (AtSdhChannel)path;
    uint8  hwSlice, hwSts;
    eAtRet ret;
    AtUnused(self);

    ret = ThaSdhChannelHwStsGet(channel, cAtModuleAps, AtSdhChannelSts1Get(channel), &hwSlice, &hwSts);
    if (ret != cAtOk)
        return cInvalidUint32;

    return (64UL * hwSlice) + hwSts;
    }

static uint32 UpsrPathDefectMaskOffset(ThaModuleHardAps self, AtSdhPath path)
    {
    return UpsrPathOffset(self, path);
    }

static eBool InterruptIsSupported(ThaModuleHardAps self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);

    if (ThaModuleHardApsInterruptIsSupported(mThis(self)))
        return ThaApsInterruptManagerNew(self);

    return NULL;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 EngineOffset(ThaModuleHardAps self, uint32 engineId)
    {
    AtUnused(self);
    return engineId;
    }

static uint32 EngineAddressWithLocalAddress(ThaModuleHardAps self, uint32 engineId, uint32 localAddress)
    {
    return ThaModuleHardApsBaseAddress(self) + localAddress + EngineOffset(self, engineId);
    }

static AtModuleSdh SdhModule(ThaModuleHardAps self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint32 NumLines(ThaModuleHardAps self)
    {
    return AtModuleSdhMaxLinesGet(SdhModule(self));
    }

static eAtRet AllLinesInit(AtModule self)
    {
    uint32 baseAddress = ThaModuleHardApsBaseAddress((ThaModuleHardAps)self);
    uint32 lineId, stsId;

    for (lineId = 0; lineId < mMethodsGet(mThis(self))->NumLines(mThis(self)); lineId++)
        {
        uint32 lineOffset = 64UL * lineId;

        for (stsId = 0; stsId < 48; stsId++)
            {
            uint32 absoluteOffset = baseAddress + stsId + lineOffset;
            uint32 regAddr;

            regAddr = cAf6Reg_upsralrmmasksfsdramctl_Base + absoluteOffset;
            mModuleHwWrite(self, regAddr, 0x0);
            }
        }

    return cAtOk;
    }

static eAtRet AllUpsrEnginesInit(AtModule self)
    {
    uint32 engineId;
    uint32 baseAddress = ThaModuleHardApsBaseAddress((ThaModuleHardAps)self);
    uint32 defaultWtrInSec = 300; /* 300s = 5 minutes*/
    uint32 defaultHwResolution = 0, defaultHwWtrValue = 0;
    ThaModuleHardApsWtrSw2Hw((ThaModuleHardAps)self, defaultWtrInSec, &defaultHwWtrValue, &defaultHwResolution);

    for (engineId = 0; engineId < AtModuleApsMaxNumUpsrEngines((AtModuleAps)self); engineId++)
        {
        uint32 absoluteOffset = engineId + baseAddress;
        uint32 regAddr, regVal;

        regAddr = cAf6Reg_upsrsmramctl1_Base + absoluteOffset;
        regVal = mModuleHwRead(self, regAddr);
        mRegFieldSet(regVal, cAf6_upsrsmramctl1_UpsrSmWtrTmUnit_, defaultHwResolution);
        mRegFieldSet(regVal, cAf6_upsrsmramctl1_UpsrSmWtrTmMax_, defaultHwWtrValue);
        mRegFieldSet(regVal, cAf6_upsrsmramctl1_UpsrSmEnable_, 0);
        mRegFieldSet(regVal, cAf6_upsrsmramctl1_UpsrSmRevertMode_, 0);
        mRegFieldSet(regVal, cAf6_upsrsmramctl1_UpsrSmWokPathId_, cAf6_upsrsmramctl1_UpsrSmWokPathId_Mask >> cAf6_upsrsmramctl1_UpsrSmWokPathId_Shift);
        mRegFieldSet(regVal, cAf6_upsrsmramctl1_UpsrSmProPathId_, cAf6_upsrsmramctl1_UpsrSmProPathId_Mask >> cAf6_upsrsmramctl1_UpsrSmProPathId_Shift);
        mModuleHwWrite(self, regAddr, regVal);

        mModuleHwWrite(self, cAf6Reg_upsrsmramctl2_Base + absoluteOffset, 0);
        mModuleHwWrite(self, cAf6Reg_upsrsmintenb_Base + absoluteOffset, 0);
        }

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AllLinesInit(self);
    ret |= AllUpsrEnginesInit(self);

    return ret;
    }

static eAtRet PathForcedSwitchingConditionSet(ThaModuleHardAps self, AtSdhPath path, eAtApsSwitchingCondition condition)
    {
    uint32 regAddress = PathDefectsMaskRegister(self, path);
    uint32 regValue = mChannelHwRead(path, regAddress, cAtModuleAps);
    mForcedSwitchingConditionSet(regValue, Sf, (condition == cAtApsSwitchingConditionSf) ? 1 : 0);
    mForcedSwitchingConditionSet(regValue, Sd, (condition == cAtApsSwitchingConditionSd) ? 1 : 0);
    mChannelHwWrite(path, regAddress, regValue, cAtModuleAps);

    return cAtOk;
    }

static eAtApsSwitchingCondition PathForcedSwitchingConditionGet(ThaModuleHardAps self, AtSdhPath path)
    {
    uint32 regAddress = PathDefectsMaskRegister(self, path);
    uint32 regValue = mChannelHwRead(path, regAddress, cAtModuleAps);

    if (mForcedSwitchingConditionGet(regValue, Sf))
        return cAtApsSwitchingConditionSf;

    if (mForcedSwitchingConditionGet(regValue, Sd))
        return cAtApsSwitchingConditionSd;

    return cAtApsSwitchingConditionNone;
    }

static uint32 WtrTimer2Resolution(ThaModuleHardAps self, uint32 timerValue, uint32 timerMax)
    {
    AtUnused(self);
    if (timerValue <= (timerMax * 30))
        return cWtrResolution30Seconds;

    if (timerValue <= (timerMax * 60))
        return cWtrResolutionMinute;

    return cInvalidUint32;
    }

static uint32 WtrResolution2Value(ThaModuleHardAps self, uint32 resolution)
    {
    AtUnused(self);
    if (resolution == cWtrResolution30Seconds)
        return 30;

    if (resolution == cWtrResolutionMinute)
        return 60;

    return 0;
    }

static uint32 MaxWtrHwValue(uint32 wtrValueMask, uint32 wtrValueShift)
    {
    return (wtrValueMask >> wtrValueShift);
    }

static eAtModuleApsRet WtrSw2Hw(ThaModuleHardAps self, uint32 wtrInSec, uint32 *wtrHwValue, uint32 *wtrHwResolution)
    {
    uint32 maxWtrHwValue;
    uint32 resolution;

    maxWtrHwValue = MaxWtrHwValue(cAf6_upsrsmramctl1_UpsrSmWtrTmMax_Mask, cAf6_upsrsmramctl1_UpsrSmWtrTmMax_Shift);
    *wtrHwResolution = ThaModuleHardApsWtrTimer2Resolution(self, wtrInSec, maxWtrHwValue);
    if (*wtrHwResolution == cInvalidUint32)
        return cAtErrorOutOfRangParm;

    *wtrHwValue = 0;
    resolution = ThaModuleHardApsWtrResolution2Value(self, *wtrHwResolution);
    if (resolution)
        *wtrHwValue = wtrInSec / resolution;

    return cAtOk;
    }

static uint32 WtrHw2Sw(ThaModuleHardAps self, uint32 wtrHwValue, uint32 wtrHwResolution)
    {
    return wtrHwValue * ThaModuleHardApsWtrResolution2Value(self, wtrHwResolution);
    }

static void MethodsInit(ThaModuleHardAps self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, LinearIsSupported);
        mMethodOverride(m_methods, UpsrIsSupported);
        mMethodOverride(m_methods, PathHoldOffTimerSet);
        mMethodOverride(m_methods, PathHoldOffTimerGet);
        mMethodOverride(m_methods, PathHoldOnTimerSet);
        mMethodOverride(m_methods, PathHoldOnTimerGet);
        mMethodOverride(m_methods, PathSwitchingConditionSet);
        mMethodOverride(m_methods, PathSwitchingConditionGet);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, UpsrPathDefectMaskOffset);
        mMethodOverride(m_methods, InterruptIsSupported);
        mMethodOverride(m_methods, NumLines);
        mMethodOverride(m_methods, WtrTimer2Resolution);
        mMethodOverride(m_methods, WtrResolution2Value);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModuleAps(AtModuleAps self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleApsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleApsOverride, m_AtModuleApsMethods, sizeof(m_AtModuleApsOverride));

        mMethodOverride(m_AtModuleApsOverride, LinearEngineCreate);
        mMethodOverride(m_AtModuleApsOverride, MaxNumLinearEngines);
        mMethodOverride(m_AtModuleApsOverride, UpsrEngineCreate);
        mMethodOverride(m_AtModuleApsOverride, UpsrEngineDelete);
        mMethodOverride(m_AtModuleApsOverride, SelectorCreate);
        mMethodOverride(m_AtModuleApsOverride, SelectorDelete);
        mMethodOverride(m_AtModuleApsOverride, GroupCreate);
        mMethodOverride(m_AtModuleApsOverride, StsGroupCreate);
        }

    mMethodsSet(self, &m_AtModuleApsOverride);
    }

static void OverrideAtModule(AtModuleAps self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleAps self)
    {
    OverrideAtModuleAps(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleHardAps);
    }

AtModuleAps ThaModuleHardApsObjectInit(AtModuleAps self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleApsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleAps ThaModuleHardApsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleAps newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaModuleHardApsObjectInit(newModule, device);
    }

uint32 ThaModuleHardApsBaseAddress(ThaModuleHardAps self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return cInvalidUint32;
    }

eAtRet ThaModuleHardApsPathHoldOffTimerSet(ThaModuleHardAps self, AtSdhPath path, uint32 holdOffTimer)
    {
    if (self)
        return mMethodsGet(self)->PathHoldOffTimerSet(self, path, holdOffTimer);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleHardApsPathHoldOffTimerGet(ThaModuleHardAps self, AtSdhPath path)
    {
    if (self)
        return mMethodsGet(self)->PathHoldOffTimerGet(self, path);
    return 0;
    }

eAtRet ThaModuleHardApsPathHoldOnTimerSet(ThaModuleHardAps self, AtSdhPath path, uint32 holdOnTimer)
    {
    if (self)
        return mMethodsGet(self)->PathHoldOnTimerSet(self, path, holdOnTimer);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleHardApsPathHoldOnTimerGet(ThaModuleHardAps self, AtSdhPath path)
    {
    if (self)
        return mMethodsGet(self)->PathHoldOnTimerGet(self, path);
    return 0;
    }

uint32 ThaModuleHardApsUpsrPathOffset(ThaModuleHardAps self, AtSdhPath path)
    {
    if (self)
        return UpsrPathOffset(self, path);
    return cBit31_0;
    }

uint32 ThaModuleHardApsUpsrPathDefectMaskOffset(ThaModuleHardAps self, AtSdhPath path)
    {
    if (self)
        return mMethodsGet(self)->UpsrPathDefectMaskOffset(self, path);
    return cBit31_0;
    }

eBool ThaModuleHardApsInterruptIsSupported(ThaModuleHardAps self)
    {
    if (self)
        return mMethodsGet(self)->InterruptIsSupported(self);
    return cAtFalse;
    }

uint32 ThaModuleHardApsEngineAddressWithLocalAddress(ThaModuleHardAps self, uint32 engineId, uint32 localAddress)
    {
    if (self)
        return EngineAddressWithLocalAddress(self, engineId, localAddress);
    return cInvalidUint32;
    }

uint32 ThaModuleHardApsEngineOffset(ThaModuleHardAps self, uint32 engineId)
    {
    if (self)
        return EngineOffset(self, engineId);
    return cInvalidUint32;
    }

eAtRet ThaModuleHardApsPathSwitchingConditionSet(ThaModuleHardAps self, AtSdhPath path, uint32 defects, eAtApsSwitchingCondition condition)
    {
    if (self)
        return mMethodsGet(self)->PathSwitchingConditionSet(self, path, defects, condition);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleHardApsPathSwitchingConditionGet(ThaModuleHardAps self, AtSdhPath path, uint32 defect)
    {
    if (self)
        return mMethodsGet(self)->PathSwitchingConditionGet(self, path, defect);
    return 0;
    }

eAtRet ThaModuleHardApsPathForcedSwitchingConditionSet(ThaModuleHardAps self, AtSdhPath path, eAtApsSwitchingCondition condition)
    {
    if (self)
        return PathForcedSwitchingConditionSet(self, path, condition);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleHardApsPathForcedSwitchingConditionGet(ThaModuleHardAps self, AtSdhPath path)
    {
    if (self)
        return PathForcedSwitchingConditionGet(self, path);
    return 0;
    }

uint32 ThaModuleHardApsWtrTimer2Resolution(ThaModuleHardAps self, uint32 timerValue, uint32 timerMax)
    {
    if (self)
        return mMethodsGet(self)->WtrTimer2Resolution(self, timerValue, timerMax);

    return cInvalidUint32;
    }

uint32 ThaModuleHardApsWtrResolution2Value(ThaModuleHardAps self, uint32 resolution)
    {
    if (self)
        return mMethodsGet(self)->WtrResolution2Value(self, resolution);

    return 0;
    }

eAtModuleApsRet ThaModuleHardApsWtrSw2Hw(ThaModuleHardAps self, uint32 wtrInSec, uint32 *wtrHwValue, uint32 *wtrHwResolution)
    {
    return WtrSw2Hw(self, wtrInSec, wtrHwValue, wtrHwResolution);
    }

uint32 ThaModuleHardApsWtrHw2Sw(ThaModuleHardAps self, uint32 wtrHwValue, uint32 wtrHwResolution)
    {
    return WtrHw2Sw(self, wtrHwValue, wtrHwResolution);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaModuleHardAps.h
 * 
 * Created Date: Jan 16, 2015
 *
 * Description : APS module of Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEHARDAPS_H_
#define _THAMODULEHARDAPS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
typedef struct tThaModuleHardAps *ThaModuleHardAps;

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps ThaModuleHardApsNew(AtDevice device);

uint32 ThaModuleHardApsBaseAddress(ThaModuleHardAps self);

/* Path timers */
eAtRet ThaModuleHardApsPathHoldOffTimerSet(ThaModuleHardAps self, AtSdhPath path, uint32 holdOffTimer);
uint32 ThaModuleHardApsPathHoldOffTimerGet(ThaModuleHardAps self, AtSdhPath path);
eAtRet ThaModuleHardApsPathHoldOnTimerSet(ThaModuleHardAps self, AtSdhPath path, uint32 holdOnTimer);
uint32 ThaModuleHardApsPathHoldOnTimerGet(ThaModuleHardAps self, AtSdhPath path);

/* Path switching condition */
eAtRet ThaModuleHardApsPathSwitchingConditionSet(ThaModuleHardAps self, AtSdhPath path, uint32 defects, eAtApsSwitchingCondition condition);
uint32 ThaModuleHardApsPathSwitchingConditionGet(ThaModuleHardAps self, AtSdhPath path, uint32 defect);
eAtRet ThaModuleHardApsPathForcedSwitchingConditionSet(ThaModuleHardAps self, AtSdhPath path, eAtApsSwitchingCondition condition);
uint32 ThaModuleHardApsPathForcedSwitchingConditionGet(ThaModuleHardAps self, AtSdhPath path);

/* For registers */
uint32 ThaModuleHardApsEngineAddressWithLocalAddress(ThaModuleHardAps self, uint32 engineId, uint32 localAddress);
uint32 ThaModuleHardApsEngineOffset(ThaModuleHardAps self, uint32 engineId);
uint32 ThaModuleHardApsUpsrPathOffset(ThaModuleHardAps self, AtSdhPath path);
uint32 ThaModuleHardApsUpsrPathDefectMaskOffset(ThaModuleHardAps self, AtSdhPath path);

/* Features */
eBool ThaModuleHardApsInterruptIsSupported(ThaModuleHardAps self);

/* Product concretes */
AtModuleAps Tha6029ModuleApsNew(AtDevice device);
AtModuleAps Tha60290022ModuleApsNew(AtDevice device);
AtModuleAps Tha60290081ModuleApsNew(AtDevice device);

/* WTR resolution */
uint32 ThaModuleHardApsWtrResolution2Value(ThaModuleHardAps self, uint32 resolution);
uint32 ThaModuleHardApsWtrTimer2Resolution(ThaModuleHardAps self, uint32 timerValue, uint32 timerMax);
eAtModuleApsRet ThaModuleHardApsWtrSw2Hw(ThaModuleHardAps self, uint32 wtrInSec, uint32 *wtrHwValue, uint32 *wtrHwResolution);
uint32 ThaModuleHardApsWtrHw2Sw(ThaModuleHardAps self, uint32 wtrHwValue, uint32 wtrHwResolution);

#endif /* _THAMODULEHARDAPS_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaModuleHardApsInternal.h
 * 
 * Created Date: Jan 16, 2015
 *
 * Description : APS module of Thalassa product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEHARDAPSINTERNAL_H_
#define _THAMODULEHARDAPSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/aps/AtModuleApsInternal.h"
#include "ThaModuleHardAps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleHardApsMethods
    {
    /* Capacity */
    uint32 (*NumLines)(ThaModuleHardAps self);

    /* Supported features. */
    eBool (*LinearIsSupported)(ThaModuleHardAps self);
    eBool (*UpsrIsSupported)(ThaModuleHardAps self);
    eBool (*InterruptIsSupported)(ThaModuleHardAps self);

    /* Hold-on/hold-off timers */
    eAtRet (*PathHoldOffTimerSet)(ThaModuleHardAps self, AtSdhPath path, uint32 holdOffTimer);
    uint32 (*PathHoldOffTimerGet)(ThaModuleHardAps self, AtSdhPath path);
    eAtRet (*PathHoldOnTimerSet)(ThaModuleHardAps self, AtSdhPath path, uint32 holdonTimer);
    uint32 (*PathHoldOnTimerGet)(ThaModuleHardAps self, AtSdhPath path);

    /* Switching condition */
    eAtRet (*PathSwitchingConditionSet)(ThaModuleHardAps self, AtSdhPath path, uint32 defects, eAtApsSwitchingCondition condition);
    eAtApsSwitchingCondition (*PathSwitchingConditionGet)(ThaModuleHardAps self, AtSdhPath path, uint32 defects);

    /* To manage registers */
    uint32 (*BaseAddress)(ThaModuleHardAps self);
    uint32 (*UpsrPathDefectMaskOffset)(ThaModuleHardAps self, AtSdhPath path);

    /* WTR resolution */
    uint32 (*WtrTimer2Resolution)(ThaModuleHardAps self, uint32 timerValue, uint32 timerMax);
    uint32 (*WtrResolution2Value)(ThaModuleHardAps self, uint32 resolution);
    }tThaModuleHardApsMethods;

typedef struct tThaModuleHardAps
    {
    tAtModuleAps super;
    const tThaModuleHardApsMethods *methods;
    }tThaModuleHardAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps ThaModuleHardApsObjectInit(AtModuleAps self, AtDevice device);

#endif /* _THAMODULEHARDAPSINTERNAL_H_ */


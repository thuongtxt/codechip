/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaModuleApsReg.h
 * 
 * Created Date: Jan 16, 2015
 *
 * Description : APS Module Registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEAPSREG_H_
#define _THAMODULEAPSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: Register to control APS
Reg Addr: 0x40800 + engineId
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegModuleApsControl                 0x40800

/*--------------------------------------
BitField Name: HwEngineEnable
BitField Type: R/W
BitField Desc: 0:Disable, 1:Enable
--------------------------------------*/
#define cThaRegModuleApsHwEngineEnableMask      cBit0
#define cThaRegModuleApsHwEngineEnableShift     0

/*--------------------------------------
BitField Name: Enable Forces Switch
BitField Type: R/W
BitField Desc: 1:Enable, 0:Disable
--------------------------------------*/
#define cThaRegModuleApsForceSwitchMask         cBit1
#define cThaRegModuleApsForceSwitchShift        1

/*--------------------------------------
BitField Name: Forces Switching mode
BitField Type: R/W
BitField Desc: 1: Force to working, 0: Force to protection
--------------------------------------*/
#define cThaRegModuleApsForceSwitchModeMask     cBit2
#define cThaRegModuleApsForceSwitchModeShift    2

/*--------------------------------------
BitField Name: Enable Brigde
BitField Type: R/W
BitField Desc: 1: Enable, 0: Disable
--------------------------------------*/
#define cThaRegModuleApsEnableBrigdeMask        cBit3
#define cThaRegModuleApsEnableBrigdeShift       3

/*--------------------------------------
BitField Name: Working Channel
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegModuleApsWorkingChannnelIdMask   cBit5_4
#define cThaRegModuleApsWorkingChannnelIdShift  4

/*--------------------------------------
BitField Name: Protected Channel
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegModuleApsProtectChannelIdMask    cBit9_8
#define cThaRegModuleApsProtectChannelIdShift   8

/*--------------------------------------
BitField Name: DefMask
BitField Type: R/W
BitField Desc: 3:AIS-L, 2:TIM-S, 1:LOF, 0:LOS
--------------------------------------*/
#define cThaRegModuleApsAlarmDefMaskMask        cBit15_12
#define cThaRegModuleApsAlarmDefMaskShift       12
#define cThaRegModuleApsAlarmDefLos             cBit0
#define cThaRegModuleApsAlarmDefLof             cBit1
#define cThaRegModuleApsAlarmDefTim             cBit2
#define cThaRegModuleApsAlarmDefAis             cBit3

/*--------------------------------------
BitField Name: Enable/Disable forces brigde
BitField Type: R/W
BitField Desc: 1: Enable, 0: Disable
--------------------------------------*/
#define cThaRegModuleApsForceBrigdeMask         cBit16
#define cThaRegModuleApsForceBrigdeShift        16

/*------------------------------------------------------------------------------
Reg Name: Register to get status APS
Reg Addr: 0x40808
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegModuleApsStatus                 0x40808

/*--------------------------------------
BitField Name: Get status switch enabled
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegModuleApsSwitchEnabledMask        cBit29_28
#define cThaRegModuleApsSwitchEnabledShift       28

/*--------------------------------------
BitField Name: Get status bridge enabled
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegModuleApsBrigdeEnabledMask        cBit27_24
#define cThaRegModuleApsBrigdeEnabledShift       24

/*--------------------------------------
BitField Name: Get switched Id
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRegModuleApsSwitchedIdMask          cBit19_16
#define cThaRegModuleApsSwitchedIdShift         16


/*------------------------------------------------------------------------------
Reg Name   : UPSR SF/SD Alarm Mask Registers
Reg Addr   : 0x01000 - 0x0172f
Reg Formula: 0x01000 + 64*LineId + StsId
    Where  :
           + $LineId(0-7)
           + $StsId(0-47)
Reg Desc   :
These configuration registers are used to assign these alarms {plm,tim,ber_sd,ber_sf,uneq,lop,ais} into SF/SD events for 8 lines OCn_Path.
If each alarm is not assigned to SD and SF, this means that this alarm is disabled.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsralrmmasksfsdramctl_Base                                                            0x01000
#define cAf6Reg_upsralrmmasksfsdramctl(LineId, StsId)                            (0x01000+64*(LineId)+(StsId))

/*--------------------------------------
BitField Name: UpsrForceSf
BitField Type: RW
BitField Desc: Set 1 to enable SF forcing. The behavior when we have SF forcing
is that the engine will treat it with the same priority as a real SF coming in.
1: Enable. 0: Disable.
BitField Bits: [15]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrForceSf_Mask                                                    cBit15
#define cAf6_upsralrmmasksfsdramctl_UpsrForceSf_Shift                                                       15

/*--------------------------------------
BitField Name: UpsrPlmMaskSf
BitField Type: RW
BitField Desc: Assign PLM defect alarm to SF event, set 1 to choose. 1: Enable.
0: Disable.
BitField Bits: [14]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrPlmMaskSf_Mask                                                  cBit14
#define cAf6_upsralrmmasksfsdramctl_UpsrPlmMaskSf_Shift                                                     14

/*--------------------------------------
BitField Name: UpsrTimMaskSf
BitField Type: RW
BitField Desc: Assign TIM defect alarm to SF event, set 1 to choose. 1: Enable.
0: Disable.
BitField Bits: [13]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrTimMaskSf_Mask                                                  cBit13
#define cAf6_upsralrmmasksfsdramctl_UpsrTimMaskSf_Shift                                                     13

/*--------------------------------------
BitField Name: UpsrBerSDMaskSf
BitField Type: RW
BitField Desc: Assign Ber_SD to SF event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSDMaskSf_Mask                                                cBit12
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSDMaskSf_Shift                                                   12

/*--------------------------------------
BitField Name: UpsrBerSFMaskSf
BitField Type: RW
BitField Desc: Assign Ber_SF to SF event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSFMaskSf_Mask                                                cBit11
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSFMaskSf_Shift                                                   11

/*--------------------------------------
BitField Name: UpsrUneqMaskSf
BitField Type: RW
BitField Desc: Assign UNEQ defect to SF event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrUneqMaskSf_Mask                                                 cBit10
#define cAf6_upsralrmmasksfsdramctl_UpsrUneqMaskSf_Shift                                                    10

/*--------------------------------------
BitField Name: UpsrLopMaskSf
BitField Type: RW
BitField Desc: Assign LOP defect to SF event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [09]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrLopMaskSf_Mask                                                   cBit9
#define cAf6_upsralrmmasksfsdramctl_UpsrLopMaskSf_Shift                                                      9

/*--------------------------------------
BitField Name: UpsrAisMaskSf
BitField Type: RW
BitField Desc: Assign AIS defect to SF event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [08]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrAisMaskSf_Mask                                                   cBit8
#define cAf6_upsralrmmasksfsdramctl_UpsrAisMaskSf_Shift                                                      8

/*--------------------------------------
BitField Name: UpsrForceSd
BitField Type: RW
BitField Desc: Set 1 to enable SD forcing. The behavior when we have SD forcing
is that the engine will treat it with the same priority as a real SD coming in.
1: Enable. 0: Disable.
BitField Bits: [07]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrForceSd_Mask                                                     cBit7
#define cAf6_upsralrmmasksfsdramctl_UpsrForceSd_Shift                                                        7

/*--------------------------------------
BitField Name: UpsrPlmMaskSd
BitField Type: RW
BitField Desc: Assign PLM defect alarm to SD event, set 1 to choose. 1: Enable.
0: Disable.
BitField Bits: [06]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrPlmMaskSd_Mask                                                   cBit6
#define cAf6_upsralrmmasksfsdramctl_UpsrPlmMaskSd_Shift                                                      6

/*--------------------------------------
BitField Name: UpsrTimMaskSd
BitField Type: RW
BitField Desc: Assign TIM defect alarm to SD event, set 1 to choose. 1: Enable.
0: Disable.
BitField Bits: [05]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrTimMaskSd_Mask                                                   cBit5
#define cAf6_upsralrmmasksfsdramctl_UpsrTimMaskSd_Shift                                                      5

/*--------------------------------------
BitField Name: UpsrBerSDMaskSd
BitField Type: RW
BitField Desc: Assign Ber_SD to SD event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [04]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSDMaskSd_Mask                                                 cBit4
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSDMaskSd_Shift                                                    4

/*--------------------------------------
BitField Name: UpsrBerSFMaskSd
BitField Type: RW
BitField Desc: Assign Ber_SF to SD event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [03]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSFMaskSd_Mask                                                 cBit3
#define cAf6_upsralrmmasksfsdramctl_UpsrBerSFMaskSd_Shift                                                    3

/*--------------------------------------
BitField Name: UpsrUneqMaskSd
BitField Type: RW
BitField Desc: Assign UNEQ defect to SD event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [02]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrUneqMaskSd_Mask                                                  cBit2
#define cAf6_upsralrmmasksfsdramctl_UpsrUneqMaskSd_Shift                                                     2

/*--------------------------------------
BitField Name: UpsrLopMaskSd
BitField Type: RW
BitField Desc: Assign LOP defect to SD event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [01]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrLopMaskSd_Mask                                                   cBit1
#define cAf6_upsralrmmasksfsdramctl_UpsrLopMaskSd_Shift                                                      1

/*--------------------------------------
BitField Name: UpsrAisMaskSd
BitField Type: RW
BitField Desc: Assign AIS defect to SD event, set 1 to choose. 1: Enable. 0:
Disable.
BitField Bits: [00]
--------------------------------------*/
#define cAf6_upsralrmmasksfsdramctl_UpsrAisMaskSd_Mask                                                   cBit0
#define cAf6_upsralrmmasksfsdramctl_UpsrAisMaskSd_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : UPSR SF/SD Hold_Off/Hold_On timer Registers
Reg Addr   : 0x02000 - 0x02f2f
Reg Formula: 0x02000 + 64*LineId + StsId
    Where  :
           + $LineId(0-15)
           + $StsId(0-47)
Reg Desc   :
These configuration registers are used to configure hold_off/hold_On for 8 lines OCn_Path (LineId:0-7) and 8 lines MATE (LineId:8-15).
Hold_off Timer          = UpsrHoldOffTmUnit * UpsrHoldOffTmMax
Hold_on  Timer          = UpsrHoldOnnTmUnit * UpsrHoldOnnTmMax

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrholdwtrtimerramctl_Base                                                            0x02000
#define cAf6Reg_upsrholdwtrtimerramctl(LineId, StsId)                            (0x02000+64*(LineId)+(StsId))

/*--------------------------------------
BitField Name: UpsrHoldOnnTmUnit
BitField Type: RW
BitField Desc: Hold_on Timer Unit. (Scale down 5 times for simulation) 7,6:
Unused. 5: Timer Unit is 100 ms. 4: Timer Unit is 50  ms. 3: Timer Unit is 10
ms. 2: Timer Unit is 5   ms. 1: Timer Unit is 1       ms. 0: Timer Unit is 0.5
ms.
BitField Bits: [22:20]
--------------------------------------*/
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmUnit_Mask                                           cBit22_20
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmUnit_Shift                                                 20

/*--------------------------------------
BitField Name: UpsrHoldOffTmUnit
BitField Type: RW
BitField Desc: Hold_off Timer Unit. (Scale down 5 times for simulation) 7,6:
Unused. 5: Timer Unit is 100 ms. 4: Timer Unit is 50  ms. 3: Timer Unit is 10
ms. 2: Timer Unit is 5   ms. 1: Timer Unit is 1      ms. 0: Timer Unit is 0.5
ms.
BitField Bits: [18:16]
--------------------------------------*/
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmUnit_Mask                                           cBit18_16
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmUnit_Shift                                                 16

/*--------------------------------------
BitField Name: UpsrHoldOnnTmMax
BitField Type: RW
BitField Desc: Hold_on Unit_Timer Maximum Value.
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmMax_Mask                                             cBit15_8
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOnnTmMax_Shift                                                   8

/*--------------------------------------
BitField Name: UpsrHoldOffTmMax
BitField Type: RW
BitField Desc: Hold_off Unit_Timer Maximum Value.
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmMax_Mask                                              cBit7_0
#define cAf6_upsrholdwtrtimerramctl_UpsrHoldOffTmMax_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : UPSR State Machine Engine Registers 1
Reg Addr   : 0x03000 - 0x0317f
Reg Formula: 0x03000 + SelectorId
    Where  :
           + $SelectorId(0-383)
Reg Desc   :
These configuration registers are used to configure for UPSR state machine engine, They are configured per Path Selector
Wait_to_Restore Timer   = UpsrWtrTmUnit     * UpsrWtrTmMax

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmramctl1_Base                                                                     0x03000
#define cAf6Reg_upsrsmramctl1(SelectorId)                                               (0x03000+(SelectorId))

/*--------------------------------------
BitField Name: UpsrSmWtrTmUnit
BitField Type: RW
BitField Desc: Wait_to_Restore Timer Unit.(Scale down (10ms/5, or 5ms/5 for
simulation) 1: Timer Unit is 1 minute. 0: Timer Unit is 0.5 minute.
BitField Bits: [29]
--------------------------------------*/
#define cAf6_upsrsmramctl1_UpsrSmWtrTmUnit_Mask                                                         cBit29
#define cAf6_upsrsmramctl1_UpsrSmWtrTmUnit_Shift                                                            29

/*--------------------------------------
BitField Name: UpsrSmWtrTmMax
BitField Type: RW
BitField Desc: Wait_to_Restore Unit_Timer Maximum Value.
BitField Bits: [28:22]
--------------------------------------*/
#define cAf6_upsrsmramctl1_UpsrSmWtrTmMax_Mask                                                       cBit28_22
#define cAf6_upsrsmramctl1_UpsrSmWtrTmMax_Shift                                                             22

/*--------------------------------------
BitField Name: UpsrSmEnable
BitField Type: RW
BitField Desc: UPSR enable per selector. When any selector is disable, all off
status is reset to Reset value (0x0). and no interrupt is sent. 1: Enable. 0:
Disable.
BitField Bits: [21]
--------------------------------------*/
#define cAf6_upsrsmramctl1_UpsrSmEnable_Mask                                                            cBit21
#define cAf6_upsrsmramctl1_UpsrSmEnable_Shift                                                               21

/*--------------------------------------
BitField Name: UpsrSmRevertMode
BitField Type: RW
BitField Desc: Configure Path Selector is revertive or nonrevertive mode.
Default is nonrevertive mode 1: Revertive Mode. 0: Nonrevertive Mode.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_upsrsmramctl1_UpsrSmRevertMode_Mask                                                        cBit20
#define cAf6_upsrsmramctl1_UpsrSmRevertMode_Shift                                                           20

/*--------------------------------------
BitField Name: UpsrSmWokPathId
BitField Type: RW
BitField Desc: Working Path Sonet_ID
BitField Bits: [19:10]
--------------------------------------*/
#define cAf6_upsrsmramctl1_UpsrSmWokPathId_Mask                                                      cBit19_10
#define cAf6_upsrsmramctl1_UpsrSmWokPathId_Shift                                                            10

/*--------------------------------------
BitField Name: UpsrSmProPathId
BitField Type: RW
BitField Desc: Protection Path Sonet_ID
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_upsrsmramctl1_UpsrSmProPathId_Mask                                                        cBit9_0
#define cAf6_upsrsmramctl1_UpsrSmProPathId_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : UPSR State Machine Engine Registers 2
Reg Addr   : 0x03800 - 0x0397f
Reg Formula: 0x03800 + SelectorId
    Where  : 
           + $SelectorId(0-383)
Reg Desc   : 
These configuration registers are used to configure for UPSR state machine engine, They are configured per Path Selector
Wait_to_Restore Timer 	= UpsrWtrTmUnit 	* UpsrWtrTmMax

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmramctl2_Base                                                                     0x03800
#define cAf6Reg_upsrsmramctl2(SelectorId)                                               (0x03800+(SelectorId))

/*--------------------------------------
BitField Name: UpsrSmManualCmd
BitField Type: RW
BitField Desc: Manually Protection Switching Commands. 7,6: Unused. 5: Lockout
of Protection. 4: Forced Switch to Working. 3: Forced Switch to Protection. 2:
Manual Switch to Working. 1: Manual Switch to Protection. 0: Clear.
BitField Bits: [02:00]
--------------------------------------*/
#define cAf6_upsrsmramctl2_UpsrSmManualCmd_Mask                                                        cBit2_0
#define cAf6_upsrsmramctl2_UpsrSmManualCmd_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : UPSR per Alarm Interrupt Enable Control
Reg Addr   : 0x04000 - 0x041ff
Reg Formula: 0x04000 + SelectorId
    Where  :
           + $SelectorId(0-383)
Reg Desc   :
These are the per Alarm interrupt enable. Each register is used to store bits to enable interrupts when the related alarms/events in UPSR engine happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmintenb_Base                                                                      0x04000
#define cAf6Reg_upsrsmintenb(SelectorId)                                                (0x04000+(SelectorId))

/*--------------------------------------
BitField Name: UpsrSmIntEnb
BitField Type: RW
BitField Desc: Set 1 to enable alarm/event in UPSR engine to generate an
interrupt when having any local state or active path is changed. [12] : SDSF to
NRQ Condition: Current state changed from SDSF to NRQ. [11] : WTR to NRQ
Condition:  Current state changed from WTR to NRQ. [10] : WTR Condition: Current
state changed from NRQ to WTR OR from SDSF to WTR. [9]  : Manual Switch Command.
[8]  : SD Condition. [7]  : SF Condition. [6]  : Forced Switch Command. [5]  :
Lockout of Protection Command. [4]  : Clear command when current state at
External Command Switching. [3:1]: Unused. [0]  : Active path change from
working to protection or protection to working.
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_upsrsmintenb_UpsrSmIntEnb_Mask                                                           cBit12_0
#define cAf6_upsrsmintenb_UpsrSmIntEnb_Shift                                                                 0

/* UpsrSmIntEnb[12]  : Current state changed from SDSF to NRQ. */
#define cAf6_upsrsmintenb_UpsrSmSDSFtoNRIntrEn_Mask                     cBit12
#define cAf6_upsrsmintenb_UpsrSmSDSFtoNRIntrEn_Shift                    12

/* UpsrSmIntEnb[11]  : Current state changed from WTR to NRQ. */
#define cAf6_upsrsmintenb_UpsrSmWTRtoNRIntrEn_Mask                      cBit11
#define cAf6_upsrsmintenb_UpsrSmWTRtoNRIntrEn_Shift                     11

/* UpsrSmIntEnb[10]  : Current state changed from SDSF to WTR.*/
#define cAf6_upsrsmintenb_UpsrSmSDSFtoWTRIntrEn_Mask                    cBit10
#define cAf6_upsrsmintenb_UpsrSmSDSFtoWTRIntrEn_Shift                   10

/* UpsrSmIntEnb[9]  : Current state changed to MS.*/
#define cAf6_upsrsmintenb_UpsrSmtoMSIntrEn_Mask                         cBit9
#define cAf6_upsrsmintenb_UpsrSmtoMSIntrEn_Shift                        9

/* UpsrSmIntEnb[8]  : Current state changed to SD.*/
#define cAf6_upsrsmintenb_UpsrSmtoSDIntrEn_Mask                         cBit8
#define cAf6_upsrsmintenb_UpsrSmtoSDIntrEn_Shift                        8

/* UpsrSmIntEnb[7]  : Current state changed to SF.*/
#define cAf6_upsrsmintenb_UpsrSmtoSFIntrEn_Mask                         cBit7
#define cAf6_upsrsmintenb_UpsrSmtoSFIntrEn_Shift                        7

/* UpsrSmIntEnb[6]  : Current state changed to FS.*/
#define cAf6_upsrsmintenb_UpsrSmtoFSIntrEn_Mask                         cBit6
#define cAf6_upsrsmintenb_UpsrSmtoFSIntrEn_Shift                        6

/* UpsrSmIntEnb[5]  : Current state changed to LP.*/
#define cAf6_upsrsmintenb_UpsrSmtoLPIntrEn_Mask                         cBit5
#define cAf6_upsrsmintenb_UpsrSmtoLPIntrEn_Shift                        5

/* UpsrSmIntEnb[4]  : Clear command when current state at External Command Switching. */
#define cAf6_upsrsmintenb_UpsrSmCLRtoNRIntrEn_Mask                      cBit4
#define cAf6_upsrsmintenb_UpsrSmCLRtoNRIntrEn_Shift                     4

/* UpsrSmIntEnb[0]  : Active path change from working to protection or protection to working. */
#define cAf6_upsrsmintenb_UpsrSmActPathChgIntrEn_Mask                   cBit0
#define cAf6_upsrsmintenb_UpsrSmActPathChgIntrEn_Shift                  0


/*------------------------------------------------------------------------------
Reg Name   : UPSR per Alarm Interrupt Status
Reg Addr   : 0x04200 - 0x043ff
Reg Formula: 0x04200 + SelectorId
    Where  :
           + $SelectorId(0-383)
Reg Desc   :
These are the per Alarm interrupt status. Each register is used to store sticky bits for each alarms/events in UPSR engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmintint_Base                                                                      0x04200
#define cAf6Reg_upsrsmintint(SelectorId)                                                (0x04200+(SelectorId))

/*--------------------------------------
BitField Name: UpsrSmIntInt
BitField Type: W1C
BitField Desc: Set 1 while alarm/event detected in the related UPSR selector,
and it is generated an interrupt if it is enabled. [12] : SDSF to NRQ
Condition:Current state changed from SDSF to NRQ. [11] : WTR to NRQ
Condition:Current state changed from WTR to NRQ. [10] : WTR Condition: Current
state changed from NRQ to WTR OR from SDSF to WTR. [9]  : Manual Switch Command.
[8]  : SD Condition. [7]  : SF Condition. [6]  : Forced Switch Command. [5]  :
Lockout of Protection Command. [4]  : Clear command when current state at
External Command Switching. [3:1]: Unused. [0]  : Active path change from
working to protection or protection to working.
BitField Bits: [12:00]
--------------------------------------*/
#define cAf6_upsrsmintint_UpsrSmIntInt_Mask                                                           cBit12_0
#define cAf6_upsrsmintint_UpsrSmIntInt_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : UPSR per Alarm Current Status
Reg Addr   : 0x04400 - 0x045ff
Reg Formula: 0x04400 + SelectorId
    Where  :
           + $SelectorId(0-383)
Reg Desc   :
These are the per Alarm Current status. Each register include Local state and actived path indication.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmcursta_Base                                                                      0x04400
#define cAf6Reg_upsrsmcursta(SelectorId)                                                (0x04400+(SelectorId))

/*--------------------------------------
BitField Name: UpsrSmStaCurState
BitField Type: RO
BitField Desc: This field indicate Local State of Path Seletor. 7: Unused. 6:
Lockout of Protection. 5: Forced Switch. 4: SF. 3: SD. 2: Manual Switch. 1:
Wait_to_Restore. 0: No Request.
BitField Bits: [03:01]
--------------------------------------*/
#define cAf6_upsrsmcursta_UpsrSmStaCurState_Mask                                                       cBit3_1
#define cAf6_upsrsmcursta_UpsrSmStaCurState_Shift                                                            1

/*--------------------------------------
BitField Name: UpsrSmStaActPath
BitField Type: RO
BitField Desc: This field indicate Active Path is Working Path or Protection
Path. 1: Protection Path. 0: Working Path.
BitField Bits: [00]
--------------------------------------*/
#define cAf6_upsrsmcursta_UpsrSmStaActPath_Mask                                                          cBit0
#define cAf6_upsrsmcursta_UpsrSmStaActPath_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : UPSR per Alarm per Selector Interrupt_OR Status
Reg Addr   : 0x04600 - 0x0460f
Reg Formula: 0x04600 + GroupSelectorId
    Where  :
           + $GroupSelectorId(0-11)
Reg Desc   :
There are 12 groups(0-11) of selector, These group consists of 32 bits for 32 selectors. Group #0 for SelectorId from 0 to 31, respectively. Each bit is used to store Interrupt_OR status of the related Selector.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmselorint_Base                                                                    0x04600
#define cAf6Reg_upsrsmselorint(GroupSelectorId)                                    (0x04600+(GroupSelectorId))

/*--------------------------------------
BitField Name: UpsrSmStaIntSelOr
BitField Type: RO
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the "UPSR per Alarm Interrupt Status" registers of the related Selector to be //
set and they are enabled to raise interrupt. Bit 0 for Selector #0,
respectively.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upsrsmselorint_UpsrSmStaIntSelOr_Mask                                                    cBit31_0
#define cAf6_upsrsmselorint_UpsrSmStaIntSelOr_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : UPSR per Alarm Selector Group Interrupt_OR Status
Reg Addr   : 0x047ff - 0x047ff
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 12 bits for 12 Selector Groups. Each bit is used to store Interrupt_OR status of the related Selector Group.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmgrporint                                                                         0x047ff

/*--------------------------------------
BitField Name: UpsrSmStaIntGrpOr
BitField Type: RO
BitField Desc: Set to 1 to indicate that there is any interrupt status bit in
the "UPSR per Alarm per Selector Interrupt_OR Status" register of the related //
Selector Group to be set and they are enabled to raise interrupt Bit 0 for
Selector Group #0, respectively.
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upsrsmgrporint_UpsrSmStaIntGrpOr_Mask                                                    cBit11_0
#define cAf6_upsrsmgrporint_UpsrSmStaIntGrpOr_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : UPSR per Alarm Selector Group Interrupt_OR Enable Control
Reg Addr   : 0x047fe - 0x047fe
Reg Formula:
    Where  :
Reg Desc   :
The register consists of 12 bits for 12 Selector Groups to enable interrupts when alarms in related Selector Group happen.

------------------------------------------------------------------------------*/
#define cAf6Reg_upsrsmgrporintenb                                                                      0x047fe

/*--------------------------------------
BitField Name: UpsrSmStaIntGrpOrEn
BitField Type: RW
BitField Desc: Set to 1 to enable the related Selector Group to generate
interrupt. Bit 0 for Selector Group #0, respectively.
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upsrsmgrporintenb_UpsrSmStaIntGrpOrEn_Mask                                               cBit11_0
#define cAf6_upsrsmgrporintenb_UpsrSmStaIntGrpOrEn_Shift                                                     0


#endif /* _THAMODULEAPSREG_H_ */


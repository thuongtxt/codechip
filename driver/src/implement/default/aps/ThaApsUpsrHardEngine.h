/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaApsUprsHardEngine.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : APS UPSR engine management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAAPSUPRSHARDENGINE_H_
#define _THAAPSUPRSHARDENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsUpsrEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaApsUpsrHardEngine * ThaApsUpsrHardEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine ThaUpsrHardEngineNew(AtModuleAps    module,
                                 uint32         engineId,
                                 AtSdhPath      working,
                                 AtSdhPath      protection);

#ifdef __cplusplus
}
#endif
#endif /* _THAAPSUPRSHARDENGINE_H_ */


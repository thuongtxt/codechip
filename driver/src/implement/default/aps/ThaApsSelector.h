/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaApsSelector.h
 * 
 * Created Date: Aug 8, 2016
 *
 * Description : 2-to-1 APS selector.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THAAPSSELECTOR_H_
#define THAAPSSELECTOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsSelector ThaApsSelectorNew(AtModuleAps module,
                                uint32      selectorId,
                                AtChannel   working,
                                AtChannel   protection,
                                AtChannel   outGoing);

eAtRet ThaApsSelectorHardwareCleanup(AtApsSelector self);


#ifdef __cplusplus
}
#endif
#endif /* THAAPSSELECTOR_H_ */


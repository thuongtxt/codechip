/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaApsInterruptManager.h
 * 
 * Created Date: Apr 26, 2017
 *
 * Description : Default APS module interrupt manager.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAAPSINTERRUPTMANAGER_H_
#define _THAAPSINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaApsInterruptManager *ThaApsInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager ThaApsInterruptManagerNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAAPSINTERRUPTMANAGER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaApsInterruptManager.c
 *
 * Created Date: Apr 26, 2017
 *
 * Description : Default APS module UPSR interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/ThaDevice.h"
#include "../man/ThaIpCoreInternal.h"
#include "../man/intrcontroller/ThaInterruptManagerInternal.h"
#include "ThaApsInterruptManager.h"
#include "ThaModuleHardAps.h"
#include "ThaApsUpsrHardEngine.h"
#include "ThaModuleApsReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)             ((ThaApsInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaApsInterruptManager
    {
    tThaInterruptManager super;
    }tThaApsInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleAps ApsModule(AtInterruptManager self)
    {
    return (AtModuleAps)AtInterruptManagerModuleGet(self);
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    AtModuleAps apsModule = ApsModule(self);
    const uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    uint32 intrEnable, intrStatus;
    uint32 groupId;

    AtUnused(glbIntr);

    intrEnable = AtHalRead(hal, baseAddress + cAf6Reg_upsrsmgrporintenb);
    intrStatus = AtHalRead(hal, baseAddress + cAf6Reg_upsrsmgrporint);

    intrStatus &= intrEnable;

    for (groupId = 0; groupId < 12; groupId++)
        {
        if (intrStatus & cIteratorMask(groupId))
            {
            uint32 intrStatus32 = AtHalRead(hal, baseAddress + cAf6Reg_upsrsmselorint(groupId));
            uint32 engineInGroup;

            for (engineInGroup = 0; engineInGroup < 32; engineInGroup++)
                {
                if (intrStatus32 & cIteratorMask(engineInGroup))
                    {
                    uint32 engineId = (uint32)(groupId << 5) | engineInGroup;
                    AtApsEngine apsEngine = AtModuleApsUpsrEngineGet(apsModule, engineId);

                    AtChannelInterruptProcess((AtChannel)apsEngine, engineId);
                    }
                }
            }
        }
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cBit16) ? cAtTrue : cAtFalse;
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    return ThaModuleHardApsBaseAddress((ThaModuleHardAps)ApsModule((AtInterruptManager)self));
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_upsrsmcursta_Base;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_upsrsmintint_Base;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_upsrsmintenb_Base;
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);

    if (hal)
        AtHalWrite(hal, baseAddress + cAf6Reg_upsrsmgrporintenb, 0xFFF);

    ThaIpCoreApsHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaApsInterruptManager);
    }

static AtInterruptManager ObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager ThaApsInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newManager, module);
    }

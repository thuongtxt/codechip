/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaApsLinearHardEngineInternal.h
 * 
 * Created Date: Jan 16, 2015
 *
 * Description : Linear APS hardware engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAAPSLINEARHARDENGINEINTERNAL_H_
#define _THAAPSLINEARHARDENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/aps/AtApsLinearEngineInternal.h"

#include "ThaApsLinearHardEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaApsLinearHardEngine
    {
    tAtApsLinearEngine super;

    /* Private data */
    uint8 arch;
    AtSdhLine affectedLine;
    }tThaApsLinearHardEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine ThaApsLinearHardEngineObjectInit(AtApsEngine self,
                                             AtModuleAps module,
                                             uint8 engineId,
                                             AtSdhLine lines[],
                                             uint8 numLine,
                                             uint8 arch);

#endif /* _THAAPSLINEARHARDENGINEINTERNAL_H_ */


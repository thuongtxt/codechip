/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : ThaApsGroup.h
 * 
 * Created Date: Aug 8, 2016
 *
 * Description : APS group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THAAPSGROUP_H_
#define THAAPSGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../include/aps/AtApsGroup.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsGroup ThaApsGroupNew(AtModuleAps module, uint32 groupId);

#ifdef __cplusplus
}
#endif
#endif /* THAAPSGROUP_H_ */


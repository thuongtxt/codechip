/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR (internal)
 *
 * File        : ThaModuleCdr.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : CDR internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "../../../generic/man/AtModuleInternal.h"
#include "../../../generic/pdh/AtPdhDe1Internal.h"
#include "../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../../default/man/ThaDeviceInternal.h"
#include "../../default/man/intrcontroller/ThaInterruptManager.h"
#include "../sdh/ThaSdhVc.h"
#include "../pdh/ThaPdhVcDe1.h"
#include "../pdh/ThaPdhDe1.h"
#include "../pdh/ThaPdhDe3.h"
#include "controllers/ThaCdrControllerInternal.h"
#include "ThaCdrInterruptManager.h"
#include "ThaModuleCdrInternal.h"
#include "ThaModuleCdrReg.h"
#include "ThaModuleCdrIntrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleCdr)self)

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleCdrMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
	AtUnused(vc);
	AtUnused(self);
    return NULL;
    }

static ThaCdrController Tu3VcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    AtUnused(vc);
    AtUnused(self);
    return NULL;
    }

static ThaCdrController Vc1xCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
	AtUnused(vc);
	AtUnused(self);
    return NULL;
    }

static ThaCdrController VcDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    return NULL;
    }

static ThaCdrController De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    AtChannel channel = (AtChannel)de1;
	AtUnused(self);
    return ThaDe1CdrControllerNew(AtChannelIdGet(channel), channel);
    }

static ThaCdrController De2De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    uint32 engineId = ThaPdhDe1FlatId((ThaPdhDe1)de1);
    AtUnused(self);
    return ThaDe2De1CdrControllerNew(engineId, (AtChannel)de1);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    localAddress = localAddress & cBit23_0;
    if ((localAddress >= 0x280000) && (localAddress <= 0x2BFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x2B0000, 0x2B0001, 0x2B0002};
	AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleCdrRegisterIteratorCreate(self);
    }

static eAtRet Activate(ThaModuleCdr self)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    eBool activateRx = mMethodsGet(self)->NeedActivateRxEngine(self);
    eBool activateTx = mMethodsGet(self)->NeedActivateTxEngine(self);

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cThaModuleCdr); part_i++)
        {
        uint32 offset = ThaDeviceModulePartOffset(device, cThaModuleCdr, part_i);

        if (activateTx)
            mModuleHwWrite(self, cThaRegDCRTxEngActCtrl + offset, cAtTrue);
        if (activateRx)
            mModuleHwWrite(self, cThaRegDCRRxEngActCtrl + offset, cAtTrue);
        }

    return cAtOk;
    }

static eBool NeedToSetExtDefaultFrequency(ThaModuleCdr self)
    {
	AtUnused(self);
    /* Let's keep hardware default value and let concrete products determine if
     * default frequency need to be configured */
    return cAtFalse;
    }

static uint32 DefaultExtFrequency(ThaModuleCdr self)
    {
	AtUnused(self);
    /* Concrete class must know when ext frequency need to be configured. */
    return 0x0;
    }

static uint32 DefaultDcrClockFrequency(ThaModuleCdr self)
    {
    AtUnused(self);
    return 19440;
    }

static uint32 ExtRefAndPrcCtrlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cThaRegCdrTimingExtRefAndPrcCtrl;
    }

static eAtRet DefaultExtFrequencySet(ThaModuleCdr self)
    {
    uint8 part_i;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    uint32 ext1FrequencyDefault = mMethodsGet(self)->DefaultExtFrequency(self);

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cThaModuleCdr); part_i++)
        {
        uint32 offset = ThaDeviceModulePartOffset(device, cThaModuleCdr, part_i);
        uint32 regAddr = mMethodsGet(self)->ExtRefAndPrcCtrlRegister(self) + offset;
        uint32 regValue = mModuleHwRead(self,  regAddr);

        mRegFieldSet(regValue, cThaRegCdrTimingExt1N2k, ext1FrequencyDefault);
        mModuleHwWrite(self, regAddr, regValue);
        }

    return cAtOk;
    }

static eAtRet DefaultFrequencySet(ThaModuleCdr self)
    {
    if (mMethodsGet(self)->NeedToSetExtDefaultFrequency(self))
        return DefaultExtFrequencySet(self);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleCdr self)
    {
    eAtRet ret = cAtOk;

    ret |= Activate(self);
    ret |= DefaultFrequencySet(self);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    /* Super */
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);

    if (ThaModuleCdrInterruptIsSupported(mThis(self)))
        return ThaCdrInterruptManagerNew(self);

    return NULL;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 1;
    }

static void Delete(AtObject self)
    {
    /* Delete private data */
    AtObjectDelete((AtObject)(mThis(self)->debugger));
    mThis(self)->debugger = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleCdr object = (ThaModuleCdr)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(debugger);
    }

static uint8 HwSystemTimingMode(ThaModuleCdr self)
    {
	AtUnused(self);
    return 0;
    }

static eBool HasDcrTimingMode(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceModuleGet(device, cAtModuleAtm) ? cAtFalse : cAtTrue;
    }

static uint8 DcrClockSource2Hw(ThaModuleCdr self, eAtPwDcrClockSource clockSource)
    {
    AtUnused(self);
    if (clockSource == cAtPwDcrClockSourcePrc)    return 0;
    if (clockSource == cAtPwDcrClockSourceSystem) return 1;
    if (clockSource == cAtPwDcrClockSourceExt1)   return 2;
    if (clockSource == cAtPwDcrClockSourceExt2)   return 3;

    /* Default is system */
    return 1;
    }

static eBool OnlySupportPrcForDcr(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool DcrClockSourceIsSupported(ThaModuleCdr self, eAtPwDcrClockSource clockSource)
    {
    AtUnused(self);

    if (mMethodsGet(self)->OnlySupportPrcForDcr(self))
        return (clockSource == cAtPwDcrClockSourcePrc) ? cAtTrue : cAtFalse;

    if ((clockSource == cAtPwDcrClockSourcePrc)    ||
        (clockSource == cAtPwDcrClockSourceSystem) ||
        (clockSource == cAtPwDcrClockSourceExt1)   ||
        (clockSource == cAtPwDcrClockSourceExt2))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PartDcrClockSourceSet(ThaModuleCdr self, uint8 hwClockSource, uint8 partId)
    {
    uint32 regAddr = cThaRegDCRprCSourceSelectCfg + ThaModuleCdrPartOffset(self, partId);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cThaDcrPrcSourceSelMask, cThaDcrPrcSourceSelShift, hwClockSource);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint8 DcrClockSource2Sw(uint8 clockSource)
    {
    if (clockSource == 0 ) return cAtPwDcrClockSourcePrc;
    if (clockSource == 1 ) return cAtPwDcrClockSourceSystem;
    if (clockSource == 2 ) return cAtPwDcrClockSourceExt1;
    if (clockSource == 3 ) return cAtPwDcrClockSourceExt2;

    return cAtPwDcrClockSourceUnknown;
    }

static uint32 DcrClockFrequencyRegMask(ThaModuleCdr self)
    {
    AtUnused(self);
    return cThaDcrPrcFreqCfgMask;
    }

static eAtRet PartDcrClockFrequencySet(ThaModuleCdr self, uint32 khz, uint8 partId)
    {
    uint32 regAddr = cThaRegDCRPrcFreqCfg + ThaModuleCdrPartOffset(self, partId);
    uint32 regVal  = mModuleHwRead(self, regAddr);
    uint32 mask = mMethodsGet(self)->DcrClockFrequencyRegMask(self);

    mFieldIns(&regVal, mask, cThaDcrPrcFreqCfgShift, khz);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwDcrClockSourceSet(ThaModuleCdr self, eAtPwDcrClockSource clockSource)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;
    uint8 hwClockSource = DcrClockSource2Hw(self, clockSource);

    for (part_i = 0; part_i < ThaModuleCdrNumParts(self); part_i++)
        ret |= mMethodsGet(self)->PartDcrClockSourceSet(self, hwClockSource, part_i);

    return ret;
    }

static eAtRet DcrClockSourceSet(ThaModuleCdr self, eAtPwDcrClockSource clockSource)
    {
    eAtRet ret = cAtOk;

    if (!HasDcrTimingMode(self))
        return cAtOk;

    ret |= HwDcrClockSourceSet(self, clockSource);
    return ret;
    }

static uint8 HwDcrClockSourceGet(ThaModuleCdr self)
    {
    uint32 regVal = mModuleHwRead(self, cThaRegDCRprCSourceSelectCfg);
    return (uint8)mRegField(regVal, cThaDcrPrcSourceSel);
    }

static eAtPwDcrClockSource DcrClockSourceGet(ThaModuleCdr self)
    {
    if (mMethodsGet(self)->OnlySupportPrcForDcr(self))
        return cAtPwDcrClockSourcePrc;

    if (HasDcrTimingMode(self))
        return DcrClockSource2Sw(mMethodsGet(self)->HwDcrClockSourceGet(self));

    return cAtPwDcrClockSourceUnknown;
    }

static uint32 MaxDcrPrcFrequency(ThaModuleCdr self)
    {
    return mMethodsGet(self)->DcrClockFrequencyRegMask(self);
    }

static eAtRet DcrClockFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    if (!HasDcrTimingMode(self))
        return cAtOk;

    if (khz > mMethodsGet(self)->MaxDcrPrcFrequency(self))
        return cAtErrorOutOfRangParm;

    for (part_i = 0; part_i < ThaModuleCdrNumParts(self); part_i++)
        ret |= mMethodsGet(self)->PartDcrClockFrequencySet(self, khz, part_i);

    return ret;
    }

static uint32 DcrClockFrequencyGet(ThaModuleCdr self)
    {
    uint32 regVal, frequency;
    uint32 mask;

    if (!HasDcrTimingMode(self))
        return 0x0;

    mask = mMethodsGet(self)->DcrClockFrequencyRegMask(self);
    regVal = mModuleHwRead(self, cThaRegDCRPrcFreqCfg);
    mFieldGet(regVal, mask, cThaDcrPrcFreqCfgShift, uint32, &frequency);

    return frequency;
    }

static eBool HasLiuTimingMode(ThaModuleCdr self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtTimingMode OtherTimingSourceAsSystemTiming(ThaModuleCdr self)
    {
	AtUnused(self);
    return cAtTimingModeUnknown;
    }

static uint32 EngineTimingCtrlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cThaRegCDRTimingCtrl;
    }

static eBool NeedActivateRxEngine(ThaModuleCdr self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool NeedActivateTxEngine(ThaModuleCdr self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 DcrRtpTimestampFrequencyRegMask(ThaModuleCdr self)
    {
    AtUnused(self);
    return cThaDcrRtpFreqCfgMask;
    }

static eAtRet DcrRtpTimestampFrequencyRegSet(ThaModuleCdr self, uint32 khz)
    {
    uint8 part_i;
    uint32 mask = mMethodsGet(self)->DcrRtpTimestampFrequencyRegMask(self);

    for (part_i = 0; part_i < ThaModuleCdrNumParts(self); part_i++)
        {
        uint32 regVal, regAddr;

        regAddr = cThaRegDCRRtpFreqCfg + ThaModuleCdrPartOffset(self, part_i);
        regVal = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, mask, cThaDcrRtpFreqCfgShift, khz);
        mModuleHwWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static uint32 DcrRtpTimestampFrequencyFromRegGet(ThaModuleCdr self)
    {
    uint32 regVal, frequency;
    uint32 mask = mMethodsGet(self)->DcrRtpTimestampFrequencyRegMask(self);

    regVal = mModuleHwRead(self, cThaRegDCRRtpFreqCfg);
    mFieldGet(regVal, mask, cThaDcrRtpFreqCfgShift, uint32, &frequency);

    return frequency;
    }

static eAtRet DcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    if (!HasDcrTimingMode(self))
        return cAtOk;

    return DcrRtpTimestampFrequencyRegSet(self, khz);
    }

static uint32 DcrRtpTimestampFrequencyGet(ThaModuleCdr self)
    {
    if (!HasDcrTimingMode(self))
        return 0x0;

    return DcrRtpTimestampFrequencyFromRegGet(self);
    }

static uint32 ControllerDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return ThaCdrControllerIdGet(controller) + ThaCdrControllerPartOffset(controller);
    }

static uint32 ChannelDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return ThaCdrControllerChannelIdFlat(controller, ThaCdrControllerChannelGet(controller)) + ThaCdrControllerPartOffset(controller);
    }

static uint32 HoVcDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    return ChannelDefaultOffset(self, controller);
    }

static uint32 Vc1xDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    return ChannelDefaultOffset(self, controller);
    }

static uint32 De1DefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    return ChannelDefaultOffset(self, controller);
    }

static uint32 De3DefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    return ChannelDefaultOffset(self, controller);
    }

static uint32 De2De1DefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    return ChannelDefaultOffset(self, controller);
    }

static uint32 AdjustStateStatusRegister(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return 0x0280800;
    }

static eBool VtAsyncMapIsSupported(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 CDRLineModeControlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cThaRegCDRlineMdCtrl;
    }

static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    AtUnused(self);
    /* Let sub-class specify the version. */
    return cBit31_0;
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "cdr";
    }

static ThaModuleCdr ModuleCdr(AtChannel channel)
    {
    return (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleCdr);
    }

static void SdhChannelRegsShow(ThaModuleCdr self, AtSdhChannel sdhChannel)
    {
    ThaCdrController controller;
    AtUnused(self);

    if (!AtSdhChannelIsVc(sdhChannel))
        return;

    controller = ThaSdhVcCdrControllerGet((AtSdhVc)sdhChannel);
    if (controller == NULL)
        return;

    ThaCdrControllerRegsShow(controller);
    }

static void PdhChannelRegsShow(ThaModuleCdr self, AtPdhChannel pdhChannel)
    {
    ThaCdrController controller = NULL;
    eAtPdhChannelType type = AtPdhChannelTypeGet(pdhChannel);
    AtUnused(self);

    if ((type == cAtPdhChannelTypeDs1) || (type == cAtPdhChannelTypeE1))
        controller = ThaPdhDe1CdrControllerGet((ThaPdhDe1)pdhChannel);

    else if ((type == cAtPdhChannelTypeDs3) || (type == cAtPdhChannelTypeE3))
        controller = ThaPdhDe3CdrControllerGet((ThaPdhDe3)pdhChannel);

    ThaCdrControllerRegsShow(controller);
    }

static ThaCdrDebugger DebuggerObjectCreate(ThaModuleCdr self)
    {
    AtUnused(self);
    return ThaCdrDebuggerNew();
    }

static eBool DcrShaperIsSupported(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return cAtTrue;
    }

static eBool ClockSubStateIsSupported(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaCdrController VcDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static ThaCdrController De3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static eAtClockState ClockStateFromHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    AtUnused(self);
    
    if (hwState <= 2) return cAtClockStateInit;
    if (hwState == 7) return cAtClockStateHoldOver;
    if (hwState == 5) return cAtClockStateLocked;

    return cAtClockStateLearning;
    }

static eAtClockState ClockStateFromLoChannelHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    AtUnused(self);

    if ((hwState == 2) || (hwState == 6))
        return cAtClockStateInit;

    if ((hwState == 3) || (hwState == 4))
        return cAtClockStateLearning;

    if (hwState == 5)
        return cAtClockStateLocked;

    if ((hwState == 0) || (hwState == 1) || (hwState == 7))
        return cAtClockStateHoldOver;

    return cAtClockStateUnknown;
    }

static eAtClockState ClockStateFromHoChannelHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    return ClockStateFromLoChannelHwStateGet(self, hwState);
    }

static uint32 DcrTxEngineTimingControlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cThaRegDCRTxEngBitRateCfg;
    }

static eBool Vc1xCdrControllerShouldInitAfterCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    AtUnused(self);
    return AtChannelDeviceInWarmRestore((AtChannel)vc) ? cAtFalse : cAtTrue;
    }

static eBool ShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 BaseAddress(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0;
    }

static AtDevice Device(AtModule self)
    {
    return AtModuleDeviceGet((AtModule)self);
    }

static uint32 PwAcrDcrRestore(AtModule self, AtPw pw)
    {
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(Device(self), cThaModuleCla);
    ThaClaPwController pwController = ThaModuleClaPwControllerGet(claModule);

    if (!ThaClaPwControllerCdrIsEnabled(pwController, (AtPw)ThaPwAdapterGet(pw)))
        return 0;

    return AtChannelPwTimingRestore(AtPwBoundCircuitGet(pw), pw);
    }

static uint32 Restore(AtModule self)
    {
    AtModulePw modulePw = (AtModulePw)AtDeviceModuleGet(Device(self), cAtModulePw);
    AtIterator pwIterator;
    AtPw pw;
    uint32 remained = m_AtModuleMethods->Restore(self);

    pwIterator = AtModulePwIteratorCreate(modulePw);
    while ((pw = (AtPw)AtIteratorNext(pwIterator)) != NULL)
        remained = remained + PwAcrDcrRestore(self, pw);
    AtObjectDelete((AtObject)pwIterator);

    return remained;
    }

static eAtRet ChannelizeDe3(ThaModuleCdr self, AtPdhDe3 de3, eBool channelized)
    {
    AtUnused(self);
    AtUnused(de3);
    AtUnused(channelized);
    return cAtErrorNotImplemented;
    }

static eAtRet HoVcUnChannelize(ThaModuleCdr self, AtSdhChannel hoVc)
    {
    AtUnused(self);
    AtUnused(hoVc);
    return cAtErrorNotImplemented;
    }

static eAtRet HoVcChannelizeRestore(ThaModuleCdr self, AtSdhChannel hoVc)
    {
    AtUnused(self);
    AtUnused(hoVc);
    return cAtErrorNotImplemented;
    }

static uint32 TxEngineTimingControlOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return ThaCdrControllerDefaultOffset(controller);
    }

static uint32 CDREngineUnlockedIntrMask(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return cAf6_cdr_per_chn_intr_en_ctrl_CDRUnlokcedIntrEn_Mask;
    }

static uint32 TimingCtrlDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->DefaultOffset(controller);
    }

static uint32 TimingCtrlDe2De1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->DefaultOffset(controller);
    }

static uint32 TimingCtrlVcDe1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->DefaultOffset(controller);
    }

static uint32 TimingCtrlVcDe3Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->DefaultOffset(controller);
    }

static uint32 TimingCtrlVcOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->DefaultOffset(controller);
    }

static uint32 TimingCtrlDe3Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->DefaultOffset(controller);
    }

static uint32 TimingStateDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    return mMethodsGet(self)->TimingCtrlDefaultOffset(self, controller);
    }

static uint32 TimingStateDe2De1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    return mMethodsGet(self)->TimingCtrlDe2De1Offset(self, controller);
    }

static uint32 TimingStateVcDe1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    return mMethodsGet(self)->TimingCtrlVcDe1Offset(self, controller);
    }

static uint32 TimingStateVcDe3Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    return mMethodsGet(self)->TimingCtrlVcDe3Offset(self, controller);
    }

static uint32 TimingStateDe3Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    return mMethodsGet(self)->TimingCtrlDe3Offset(self, controller);
    }

static uint32 TimingStateVcOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    return mMethodsGet(self)->TimingCtrlVcOffset(self, controller);
    }

static uint32 AdjustHoldoverValueStatusRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0;
    }

static eBool InterruptIsSupported(ThaModuleCdr self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= mMethodsGet(self)->StartVersionSupportInterrupt(self))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, Restore);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, De1DefaultOffset);
        mMethodOverride(m_methods, De3DefaultOffset);
        mMethodOverride(m_methods, De2De1DefaultOffset);
        mMethodOverride(m_methods, HoVcCdrControllerCreate);
        mMethodOverride(m_methods, Tu3VcCdrControllerCreate);
        mMethodOverride(m_methods, Vc1xCdrControllerCreate);
        mMethodOverride(m_methods, VcDe1CdrControllerCreate);
        mMethodOverride(m_methods, De2De1CdrControllerCreate);
        mMethodOverride(m_methods, De1CdrControllerCreate);
        mMethodOverride(m_methods, VcDe3CdrControllerCreate);
        mMethodOverride(m_methods, De3CdrControllerCreate);
        mMethodOverride(m_methods, HwSystemTimingMode);
        mMethodOverride(m_methods, HasLiuTimingMode);
        mMethodOverride(m_methods, OtherTimingSourceAsSystemTiming);
        mMethodOverride(m_methods, NeedToSetExtDefaultFrequency);
        mMethodOverride(m_methods, DefaultExtFrequency);
        mMethodOverride(m_methods, EngineTimingCtrlRegister);
        mMethodOverride(m_methods, NeedActivateRxEngine);
        mMethodOverride(m_methods, NeedActivateTxEngine);
        mMethodOverride(m_methods, ControllerDefaultOffset);
        mMethodOverride(m_methods, AdjustStateStatusRegister);
        mMethodOverride(m_methods, CDRLineModeControlRegister);
        mMethodOverride(m_methods, HoVcDefaultOffset);
        mMethodOverride(m_methods, Vc1xDefaultOffset);
        mMethodOverride(m_methods, VtAsyncMapIsSupported);
        mMethodOverride(m_methods, OnlySupportPrcForDcr);
        mMethodOverride(m_methods, PartDcrClockFrequencySet);
        mMethodOverride(m_methods, PartDcrClockSourceSet);
        mMethodOverride(m_methods, SdhChannelRegsShow);
        mMethodOverride(m_methods, PdhChannelRegsShow);
        mMethodOverride(m_methods, DcrClockFrequencyGet);
        mMethodOverride(m_methods, DebuggerObjectCreate);
        mMethodOverride(m_methods, DefaultDcrClockFrequency);
        mMethodOverride(m_methods, ExtRefAndPrcCtrlRegister);
        mMethodOverride(m_methods, HwDcrClockSourceGet);
        mMethodOverride(m_methods, DcrShaperIsSupported);
        mMethodOverride(m_methods, ClockSubStateIsSupported);
        mMethodOverride(m_methods, StartVersionSupportInterrupt);
        mMethodOverride(m_methods, ClockStateFromHwStateGet);
        mMethodOverride(m_methods, ClockStateFromLoChannelHwStateGet);
        mMethodOverride(m_methods, ClockStateFromHoChannelHwStateGet);
        mMethodOverride(m_methods, DcrTxEngineTimingControlRegister);
        mMethodOverride(m_methods, Vc1xCdrControllerShouldInitAfterCreate);
        mMethodOverride(m_methods, ShouldEnableJitterAttenuator);
        mMethodOverride(m_methods, MaxDcrPrcFrequency);
        mMethodOverride(m_methods, DcrClockFrequencyRegMask);
        mMethodOverride(m_methods, DcrRtpTimestampFrequencyRegMask);
        mMethodOverride(m_methods, ChannelizeDe3);
        mMethodOverride(m_methods, HoVcUnChannelize);
        mMethodOverride(m_methods, HoVcChannelizeRestore);
        mMethodOverride(m_methods, TxEngineTimingControlOffset);
        mMethodOverride(m_methods, CDREngineUnlockedIntrMask);
        mMethodOverride(m_methods, TimingCtrlDefaultOffset);
        mMethodOverride(m_methods, TimingCtrlDe2De1Offset);
        mMethodOverride(m_methods, TimingCtrlVcDe1Offset);
        mMethodOverride(m_methods, TimingCtrlVcDe3Offset);
        mMethodOverride(m_methods, TimingCtrlVcOffset);
        mMethodOverride(m_methods, TimingCtrlDe3Offset);
        mMethodOverride(m_methods, TimingStateDefaultOffset);
        mMethodOverride(m_methods, TimingStateDe2De1Offset);
        mMethodOverride(m_methods, TimingStateVcDe1Offset);
        mMethodOverride(m_methods, TimingStateVcDe3Offset);
        mMethodOverride(m_methods, TimingStateVcOffset);
        mMethodOverride(m_methods, TimingStateDe3Offset);
        mMethodOverride(m_methods, AdjustHoldoverValueStatusRegister);
        }

    mMethodsSet((ThaModuleCdr)self, &m_methods);
    }

AtModule ThaModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModuleCdr));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModuleCdr, device) == NULL)
        return NULL;

    /* Override */
    Override(self);

    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModuleCdr));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleCdrObjectInit(newModule, device);
    }

ThaCdrController ThaModuleCdrHoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    ThaCdrController newController = mMethodsGet(self)->HoVcCdrControllerCreate(self, vc);
    if (!AtChannelDeviceInWarmRestore((AtChannel)vc))
        ThaCdrControllerInit(newController);

    return newController;
    }

ThaCdrController ThaModuleCdrTu3VcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    ThaCdrController newController = mMethodsGet(self)->Tu3VcCdrControllerCreate(self, vc);
    if (!AtChannelDeviceInWarmRestore((AtChannel)vc))
        ThaCdrControllerInit(newController);

    return newController;
    }

ThaCdrController ThaModuleCdrVc1xCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    ThaCdrController newController = mMethodsGet(self)->Vc1xCdrControllerCreate(self, vc);
    if (mMethodsGet(self)->Vc1xCdrControllerShouldInitAfterCreate(self, vc))
        ThaCdrControllerInit(newController);

    return newController;
    }

ThaCdrController ThaModuleCdrVcDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    ThaCdrController newController = mMethodsGet(self)->VcDe1CdrControllerCreate(self, de1);
    if (!AtChannelDeviceInWarmRestore((AtChannel)de1))
        ThaCdrControllerInit(newController);

    return newController;
    }

ThaCdrController ThaModuleCdrDe2De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    ThaCdrController newController = mMethodsGet(self)->De2De1CdrControllerCreate(self, de1);
	if (!AtChannelDeviceInWarmRestore((AtChannel)de1))
        ThaCdrControllerInit(newController);

	return newController;
    }

ThaCdrController ThaModuleCdrDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    ThaCdrController newController = mMethodsGet(self)->De1CdrControllerCreate(self, de1);
    if (!AtChannelDeviceInWarmRestore((AtChannel)de1))
        ThaCdrControllerInit(newController);

    return newController;
    }

ThaCdrController ThaModuleCdrVcDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    ThaCdrController newController = mMethodsGet(self)->VcDe3CdrControllerCreate(self, de3);
    if (!AtChannelDeviceInWarmRestore((AtChannel)de3))
        ThaCdrControllerInit(newController);

    return newController;
    }

ThaCdrController ThaModuleCdrDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    ThaCdrController newController = mMethodsGet(self)->De3CdrControllerCreate(self, de3);
    if (!AtChannelDeviceInWarmRestore((AtChannel)de3))
        ThaCdrControllerInit(newController);

    return newController;
    }

uint32 ThaModuleCdrPartOffset(ThaModuleCdr self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleCdr, partId);
    }

uint8 ThaModuleCdrHwSystemTimingMode(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->HwSystemTimingMode(self);
    return 0;
    }

uint32 ThaModuleCdrDcrClockFrequencyGet(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->DcrClockFrequencyGet(self);
    return 0x0;
    }

eAtRet ThaModuleCdrDcrClockFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    if (self)
        return DcrClockFrequencySet(self, khz);
    return cAtError;
    }

eAtRet ThaModuleCdrDcrClockSourceSet(ThaModuleCdr self, eAtPwDcrClockSource clockSource)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (DcrClockSourceIsSupported(self, clockSource))
        return DcrClockSourceSet(self, clockSource);

    return cAtErrorModeNotSupport;
    }

eAtPwDcrClockSource ThaModuleCdrDcrClockSourceGet(ThaModuleCdr self)
    {
    if (self)
        return DcrClockSourceGet(self);
    return cAtPwDcrClockSourceUnknown;
    }

uint8 ThaModuleCdrNumParts(ThaModuleCdr self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModuleCdr);
    }

eAtRet ThaModuleCdrDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    if (self)
        return DcrRtpTimestampFrequencySet(self, khz);
    return cAtErrorNotImplemented;
    }

uint32 ThaModuleCdrDcrRtpTimestampFrequencyGet(ThaModuleCdr self)
    {
    if (self)
        return DcrRtpTimestampFrequencyGet(self);
    return 0x0;
    }

eAtRet ThaModuleCdrDcrRtpTimestampFrequencyRegSet(ThaModuleCdr self, uint32 khz)
    {
    if (self)
        return DcrRtpTimestampFrequencyRegSet(self, khz);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(ThaModuleCdr self)
    {
    if (self)
        return DcrRtpTimestampFrequencyFromRegGet(self);
    return 0x0;
    }

eBool ThaModuleCdrTimestampFrequencyInRange(ThaModuleCdr self, uint32 khz)
    {
    if (self)
        return ((mMethodsGet(self)->DcrClockFrequencyRegMask(self) >> cThaDcrPrcFreqCfgShift) >= khz) ? cAtTrue : cAtFalse;
    return cAtFalse;
    }

eBool ThaModuleCdrHasLiuTimingMode(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->HasLiuTimingMode(self);
    return cAtFalse;
    }

eAtTimingMode ThaModuleCdrOtherTimingSourceAsSystemTiming(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->OtherTimingSourceAsSystemTiming(self);
    return cAtTimingModeUnknown;
    }

eBool ThaModuleCdrHasDcrTimingMode(ThaModuleCdr self)
    {
    return (eBool)(self ? HasDcrTimingMode(self): cAtFalse);
    }

uint32 ThaModuleCdrControllerDefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->ControllerDefaultOffset(self, controller);
    return 0;
    }

uint32 ThaModuleCdrChannelDefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return ChannelDefaultOffset(self, controller);
    return 0;
    }

uint32 ThaModuleCdrControllerHoVcDefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->HoVcDefaultOffset(self, controller);
    return 0;
    }

uint32 ThaModuleCdrControllerVc1xDefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->Vc1xDefaultOffset(self, controller);
    return 0;
    }

uint32 ThaModuleCdrControllerDe1DefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->De1DefaultOffset(self, controller);
    return 0;
    }

uint32 ThaModuleCdrControllerDe3DefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->De3DefaultOffset(self, controller);
    return 0;
    }

uint32 ThaModuleCdrControllerDe2De1DefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->De2De1DefaultOffset(self, controller);
    return 0;
    }

uint32 ThaModuleCdrAdjustStateStatusRegister(ThaModuleCdr self, ThaCdrController controller)
    {
    if (self)
        return mMethodsGet(self)->AdjustStateStatusRegister(self, controller);
    return cBit31_0;
    }

uint32 ThaModuleCdrEngineTimingCtrlRegister(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->EngineTimingCtrlRegister(self);
    return cBit31_0;
    }

eBool ThaModuleCdrVtAsyncMapIsSupported(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->VtAsyncMapIsSupported(self);
    return cAtFalse;
    }

void ThaModuleCdrSdhChannelRegsShow(AtSdhChannel sdhChannel)
    {
    ThaModuleCdr self;
    ThaModuleSdhStsMappingDisplay(sdhChannel);

    self = ModuleCdr((AtChannel)sdhChannel);
    AtPrintc(cSevInfo, "* CDR registers of channel '%s':\r\n", AtObjectToString((AtObject)sdhChannel));
    if (self)
        mMethodsGet(self)->SdhChannelRegsShow(self, sdhChannel);
    }

void ThaModuleCdrPdhChannelRegsShow(AtPdhChannel pdhChannel)
    {
    ThaModuleCdr self = ModuleCdr((AtChannel)pdhChannel);
    AtPrintc(cSevInfo, "* CDR registers of channel '%s':\r\n", AtObjectToString((AtObject)pdhChannel));
    if (self)
        mMethodsGet(self)->PdhChannelRegsShow(self, pdhChannel);
    }

ThaCdrDebugger ThaModuleCdrDebugger(ThaModuleCdr self)
    {
    if (self == NULL)
        return NULL;

    if (self->debugger == NULL)
        self->debugger = mMethodsGet(self)->DebuggerObjectCreate(self);
    return self->debugger;
    }

eBool ThaModuleCdrDcrShaperIsSupported(ThaModuleCdr self, ThaCdrController controller)
    {
    if (self)
        return mMethodsGet(self)->DcrShaperIsSupported(self, controller);
    return cAtFalse;
    }

eBool ThaModuleCdrClockSubStateIsSupported(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->ClockSubStateIsSupported(self);
    return cAtTrue;
    }

eBool ThaModuleCdrInterruptIsSupported(ThaModuleCdr self)
    {
    if (self)
        return InterruptIsSupported(self);
    return cAtFalse;
    }

eAtClockState ThaModuleCdrClockStateFromHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    if (self)
        return mMethodsGet(self)->ClockStateFromHwStateGet(self, hwState);
    return cAtClockStateUnknown;
    }

eAtClockState ThaModuleCdrClockStateFromLoChannelHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    if (self)
        return mMethodsGet(self)->ClockStateFromLoChannelHwStateGet(self, hwState);
    return cAtClockStateUnknown;
    }

eAtClockState ThaModuleCdrClockStateFromHoChannelHwStateGet(ThaModuleCdr self, uint8 hwState)
    {
    if (self)
        return mMethodsGet(self)->ClockStateFromHoChannelHwStateGet(self, hwState);
    return cAtClockStateUnknown;
    }

uint32 ThaModuleCdrDefaultDcrClockFrequency(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->DefaultDcrClockFrequency(self);

    return 0;
    }

uint32 ThaModuleCdrDcrTxEngineTimingControlRegister(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->DcrTxEngineTimingControlRegister(self);
    return cBit31_0;
    }

uint32 ThaModuleCDRLineModeControlRegister(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->CDRLineModeControlRegister(self);
    return cBit31_0;
    }

eBool ThaModuleCdrShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->ShouldEnableJitterAttenuator(self);
    return cAtFalse;
    }

uint32 ThaModuleCdrBaseAddress(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0;
    }

eAtRet ThaModuleCdrChannelizeDe3(ThaModuleCdr self, AtPdhDe3 de3, eBool channelized)
    {
    if (self)
        return mMethodsGet(self)->ChannelizeDe3(self, de3, channelized);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaModuleCdrHoVcUnChannelize(ThaModuleCdr self, AtSdhChannel hoVc)
    {
    if (self)
        return mMethodsGet(self)->HoVcUnChannelize(self, hoVc);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaModuleCdrHoVcChannelizeRestore(ThaModuleCdr self, AtSdhChannel hoVc)
    {
    if (self)
        return mMethodsGet(self)->HoVcChannelizeRestore(self, hoVc);
    return cAtErrorObjectNotExist;
    }

uint32 ThaModuleCdrTxEngineTimingControlOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TxEngineTimingControlOffset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrCDREngineUnlockedIntrMask(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->CDREngineUnlockedIntrMask(self, controller);
    return 0;
    }

uint32 ThaModuleCdrTimingCtrlDefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingCtrlDefaultOffset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingCtrlDe2De1Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingCtrlDe2De1Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingCtrlVcDe1Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingCtrlVcDe1Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingCtrlVcDe3Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingCtrlVcDe3Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingCtrlVcOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingCtrlVcOffset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingCtrlDe3Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingCtrlDe3Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingStateDefaultOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingStateDefaultOffset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingStateDe2De1Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingStateDe2De1Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingStateVcDe1Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingStateVcDe1Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingStateVcDe3Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingStateVcDe3Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingStateVcOffset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingStateVcOffset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrTimingStateDe3Offset(ThaCdrController controller)
    {
    ThaModuleCdr self = ThaCdrControllerModuleGet(controller);
    if (self)
        return mMethodsGet(self)->TimingStateDe3Offset(self, controller);
    return cInvalidUint32;
    }

uint32 ThaModuleCdrAdjustHoldoverValueStatusRegister(ThaModuleCdr self)
    {
    if (self)
        return mMethodsGet(self)->AdjustHoldoverValueStatusRegister(self);
    return cBit31_0;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaCdrInterruptManager.c
 *
 * Created Date: Dec 12, 2015
 *
 * Description : Default CDR module interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleCdr.h"
#include "ThaCdrInterruptManagerInternal.h"
#include "ThaModuleCdrIntrReg.h"
#include "controllers/ThaCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)             ((ThaCdrInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaCdrInterruptManagerMethods m_methods;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    const uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    uint32 intrEnable, intrStatus;
    uint8 stsvc;

    AtUnused(glbIntr);

    intrEnable = AtHalRead(hal, baseAddress + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base);
    intrStatus = AtHalRead(hal, baseAddress + cAf6Reg_cdr_per_stsvc_intr_or_stat_Base);

    intrStatus &= intrEnable;

    for (stsvc = 0; stsvc < 2; stsvc++)
        {
        if (intrStatus & cIteratorMask(stsvc))
            {
            uint32 intrStatus32 = AtHalRead(hal, baseAddress + (uint32)cAf6Reg_cdr_per_chn_intr_or_stat(stsvc));
            uint8 de1InVc;

            for (de1InVc = 0; de1InVc < 32; de1InVc++)
                {
                if (intrStatus32 & cIteratorMask(de1InVc))
                    {
                    ThaCdrController controller = ThaCdrInterruptManagerAcrDcrControllerFromHwIdGet(mThis(self), 0, stsvc, de1InVc);
                    uint32 cdrId = (uint32)(stsvc << 5) | de1InVc;

                    ThaCdrControllerInterruptProcess(controller, cdrId);
                    }
                }
            }
        }
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);

    if (hal)
        {
        uint32 baseAddres = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
        AtHalWrite(hal, baseAddres + cAf6Reg_cdr_per_stsvc_intr_en_ctrl_Base, (enable) ? cBit1_0 : 0);
        }
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x280000;
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x2080;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x2040;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x2000;
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cBit28) ? cAtTrue : cAtFalse;
    }

static ThaCdrController AcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt)
    {
    AtModule cdrModule = AtInterruptManagerModuleGet((AtInterruptManager)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtModuleDeviceGet(cdrModule), cAtModulePdh);
    uint32 de1Id = (uint32)(sts << 5) | vt;
    ThaPdhDe1 de1 = (ThaPdhDe1)AtModulePdhDe1Get(pdhModule, de1Id);

    AtUnused(slice);
    return ThaPdhDe1CdrControllerGet(de1);
    }

static uint32 InterruptOffset(ThaCdrInterruptManager self, ThaCdrController controller)
    {
    AtUnused(self);
    return mMethodsGet(controller)->DefaultOffset(controller);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static void MethodsInit(ThaCdrInterruptManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, AcrDcrControllerFromHwIdGet);
        mMethodOverride(m_methods, InterruptOffset);
        }
    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCdrInterruptManager);
    }

AtInterruptManager ThaCdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaCdrInterruptManager)self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager ThaCdrInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ThaCdrInterruptManagerObjectInit(newManager, module);
    }

ThaCdrController ThaCdrInterruptManagerAcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt)
    {
    if (self)
        return mMethodsGet(self)->AcrDcrControllerFromHwIdGet(self, slice, sts, vt);
    return NULL;
    }

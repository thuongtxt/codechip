/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaModuleCdrReg
 *
 * File        : ThaModuleCdrReg.h
 *
 * Created Date: Sep 12, 2012
 *
 * Author      : ntdung
 *
 * Description : This file contains registers of CDR module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#ifdef __cplusplus
}
#endif
#endif

#ifndef THA_MODULE_CDR_REG_HEADER
#define THA_MODULE_CDR_REG_HEADER
/*--------------------------- Define -----------------------------------------*/

/*==============================================================================
Reg Name: CDR Timing External reference  and PRC control
Address : 0x0280000
Description: Used for TDM PW, This register is used to configure for the 2Khz
             coefficient of external reference signal, PRC clock in order to
             generate the sync 125us timing. The mean that, if the reference
             signal is 8Khz, the  coefficient must be configured 4
             (I.e 8Khz  = 4x2Khz)
==============================================================================*/
#define cThaRegCdrTimingExtRefAndPrcCtrl    0x00280000

/*--------------------------------------
BitField Name: Ext1N2k [31:16]
BitField Type: R/W
BitField Desc: The 2Khz coefficient of the first external reference signal,
                            default 8khz input signal
--------------------------------------*/
#define cThaRegCdrTimingExt1N2kMask cBit31_16   /* bit[31:16]*/
#define cThaRegCdrTimingExt1N2kShift    16

/*==============================================================================
Reg Name: CDR Engine Timing control
Address : 0x0280200-0x028021F
          The address format for these registers is 0x0280200 + CHID
          Where:     CHID = [0,31]
Description: This register is used to configure timing mode for per line Id
==============================================================================*/
#define cThaRegCDRTimingCtrl                        0x0280200

/*--------------------------------------
BitField Name: HoldValMode
BitField Type: R/W
BitField Desc: Hold value mode of NCO, default value 0
--------------------------------------*/
#define cThaRegCDRTimingHoldValMdMask               cBit24
#define cThaRegCDRTimingHoldValMdShift              24

/*--------------------------------------
BitField Name: SeqMode
BitField Type: R/W
BitField Desc: Sequence mode, default value 0
--------------------------------------*/
#define cThaRegCDRTimingSeqMdMask                   cBit23
#define cThaRegCDRTimingSeqMdShift                  23

/*--------------------------------------
BitField Name: LineTypeMode
BitField Type: R/W
BitField Desc: Line type mode
--------------------------------------*/
#define cThaRegCDRTimingLineTypeMask                cBit22_20
#define cThaRegCDRTimingLineTypeShift               20

/*--------------------------------------
BitField Name: PktLen
BitField Type: R/W
BitField Desc: The payload packet  length parameter to create a packet. - SAToP
               mode: The number payload of bit - CESoPSN mode: The number of
               bit which converted to full DS1/E1/DS3/E3 rate mode. In CESoPSN
               mode, the payload is assembled by NxDS0 with M frame, the value
               configured to this register is Mx256 bits for E1 mode, Mx193
               bits for DS1 mode. - SAToP T1 Octet align: to be provided - AAL1
               raw mode: to be provided - AAL1 nDS0 mode: to be provided
--------------------------------------*/
#define cThaRegCDRTimingPktLenMask                  cBit19_4
#define cThaRegCDRTimingPktLenShift                 4

/*--------------------------------------
BitField Name: CDRTimeMode
BitField Type: R/W
BitField Desc: Timing mode. Supported value are:
    0: System   - system clock
    1: Loop     - Rx clock is looped back to Tx transparently
    2: LIU      - Rx LIU as CDR source generates timing for Tx clock
    3: PRC      - PRC as CDR source generates timing for Tx clock
    4: Ext#1    - External#1 as CDR source generates timing for Tx clock
    5: Ext#2    - External#2 as CDR source generates timing for Tx clock
    6: Ethernet - Rx Ethernet clock as CDR source generates timing for Tx clock
    7: Freerun  - System clock as CDR source generates timing for Tx clock
    8: ACR      - Adaptive Clock Recovery mode
    9: Reserve
    10: DCR     - Dynamic Clock Recovery mode
    11: Ext. CDR- External CDR timing mode
--------------------------------------*/
#define cThaRegCDRTimingMdMask                  cBit3_0
#define cThaRegCDRTimingMdShift                 0

#define cThaRegCDRTimingCtrlNoneEditableFieldsMask cBit31_25

/*==============================================================================
Reg. Name   : CDR Selection Control
Address     : 0x2B0000
Description : This register is used to select ACR or DCR mode for each PDH line.
==============================================================================*/
#define cThaRegCDRSelCtrl                       0x2B0000

/*==============================================================================
Reg. Name   : DCR Tx Engine Active Control
Address     : 0x0290000
Description : This register is used to activate the DCR TX Engine. Active high.
==============================================================================*/
#define cThaRegDCRTxEngActCtrl          0x0290000

/*==============================================================================
Reg. Name   : DCR Rx Engine Active Control
Address     : 0x0298000
Description : This register is used to activate the DCR RX Engine. Active high.
==============================================================================*/
#define cThaRegDCRRxEngActCtrl              0x0298000

/*==============================================================================
Reg. Name   : DCR Rx NCO Configuration
Address     : 0x0299000-0x02993FF
Format      : 0x0299000 + SLCID*256 + CHID, where SLCID=0, CHID=[0,31]
Description : This register is used to configure the NCO.
==============================================================================*/
#define cThaRegDCRRxNcoConf                0x0299000

/*------------------------------------------------------------------------------
Bit Field Name  : DcrRxPackLen
Bit Field Type  : R/W
Bit Field Desc  : Packet Length. Unit is byte.
------------------------------------------------------------------------------*/
#define cThaRegDCRRxNcoConfPackLenMask    cBit15_0
#define cThaRegDCRRxNcoConfPackLenShift   0

/*------------------------------------------------------------------------------
Reg Name: CDR STS Timing control
Reg Addr: 0x0280400 - 0x028040B
          The address format for these registers is 0x0280400 + STS
          Where: STS (0 - 11)
Reg Desc: This register is used to configure timing mode for per STS
------------------------------------------------------------------------------*/
#undef cThaRegCDRStsTimeCtrl
#define cThaRegCDRStsTimeCtrl                       (0x280400)

/*--------------------------------------
BitField Name: CDRChid[1:0]
BitField Type: R/W
BitField Desc: CDR channel ID in CDR mode (0)
--------------------------------------*/
#undef cThaVC3CDRChidMask
#undef cThaVC3CDRChidShift
#define cThaVC3CDRChidMask                                cBit28_20
#define cThaVC3CDRChidShift                               20

/*--------------------------------------
BitField Name: MapSTSmode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#undef cThaVC3EParEnMask
#undef cThaVC3EParEnShift
#define cThaVC3EParEnMask                               cBit17
#define cThaVC3EParEnShift                              17

/*--------------------------------------
BitField Name: MapSTSmode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#undef cThaMapStsMdMask
#undef cThaMapStsMdShift
#define cThaMapStsMdMask                               cBit16
#define cThaMapStsMdShift                              16

/*--------------------------------------
BitField Name: VC3TimeMode
BitField Type: R/W
BitField Desc: VC3 time mode
               - 0: System timing mode
               - 1: Loop timing mode
               - 2: Line OCN#1 timing mode
               - 3: Line OCN#2 timing mode
               - 4: Line OCN#3 timing mode
               - 5: Line OCN#4 timing mode
               - 6: Line EXT#1 timing mode
               - 7: Line EXT#2 timing mode
--------------------------------------*/
#undef cThaVC3TimeModeMask
#undef cThaVC3TimeModeShift
#define cThaVC3TimeModeMask                            cBit3_0
#define cThaVC3TimeModeShift                           0

/*------------------------------------------------------------------------------
Reg Name: CDR VT Timing control
Reg Addr: 0x0280600 - 0x028077B
          The address format for these registers is 0x0280600 + STS*32 + TUG*4
          + VT
          Where: STS (0 - 11)
          TUG (0 -6)
          VT (0 -2) in E1 and (0 - 3) in DS1
Reg Desc: This register is used to configure timing mode for per VT
------------------------------------------------------------------------------*/
#undef cThaRegCDRVtTimeCtrl
#define cThaRegCDRVtTimeCtrl                        (0x280600)

/*--------------------------------------
BitField Name: CDRChid[6:0]
BitField Type: R/W
BitField Desc: CDR channel ID in CDR mode (0)
--------------------------------------*/
#undef cThaVTCDRChidMask
#undef cThaVTCDRChidShift
#define cThaVTCDRChidMask                                cBit20_12
#define cThaVTCDRChidShift                               12

/*--------------------------------------
BitField Name: MapVTmode[0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#undef cThaVTEParEnMask
#undef cThaVTEParEnShift
#define cThaVTEParEnMask                                cBit9
#define cThaVTEParEnShift                               9

/*--------------------------------------
BitField Name: MapVTmode[0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#undef cThaMapVtMdMask
#undef cThaMapVtMdShift
#define cThaMapVtMdMask                                cBit8
#define cThaMapVtMdShift                               8

/*--------------------------------------
BitField Name: DE1TimeMode
BitField Type: R/W
BitField Desc: DE1 time mode
               - 0: System timing mode
               - 1: Loop timing mode
               - 2: Line OCN#1 timing mode
               - 3: Line OCN#2 timing mode
               - 4: Line OCN#3 timing mode
               - 5: Line OCN#4 timing mode
               - 6: Line EXT#1 timing mode
               - 7: Line EXT#2 timing mode
               - 8: CDR timing mode
--------------------------------------*/
#undef cThaDE1TimeModeMask
#undef cThaDE1TimeModeShift
#define cThaDE1TimeModeMask                            cBit7_4
#define cThaDE1TimeModeShift                           4

/*--------------------------------------
BitField Name: VTTimeMode
BitField Type: R/W
BitField Desc: VT time mode
               - 0: System timing mode
               - 1: Loop timing mode
               - 2: Line OCN#1 timing mode
               - 3: Line OCN#2 timing mode
               - 4: Line OCN#3 timing mode
               - 5: Line OCN#4 timing mode
               - 6: Line EXT#1 timing mode
               - 7: Line EXT#2 timing mode
--------------------------------------*/
#undef cThaVtTimeModeMask
#undef cThaVtTimeModeShift
#define cThaVtTimeModeMask                             cBit3_0
#define cThaVtTimeModeShift                            0

/*------------------------------------------------------------------------------
Reg Name: DCR PRC Source Select Configuration
Reg Addr: 0x0290001
Reg Desc: This register is used to configure to select PRC source for PRC
          timer. The PRC clock selected must be less than 19.44 Mhz.
------------------------------------------------------------------------------*/
#define cThaRegDCRprCSourceSelectCfg                  0x290001

/*--------------------------------------
BitField Name: DcrPrcSourceSel
BitField Type: R/W
BitField Desc: PRC source selection.
               - 0: PRC Reference Clock.
               - 1: System Clock 19Mhz.
               - 2: External Reference Clock 1.
               - 3: External Reference Clock 2.
               - 4: Ocn Line Clock Port 1
               - 5: Ocn Line Clock Port 2
               - 6: Ocn Line Clock Port 3
               - 7: Ocn Line Clock Port 4
BitField Bits: 2_0
--------------------------------------*/
#define cThaDcrPrcSourceSelMask                        cBit2_0
#define cThaDcrPrcSourceSelShift                       0

/*------------------------------------------------------------------------------
Reg Name: DCR PRC Frequency Configuration
Reg Addr: 0x029000b
Reg Desc: This register is used to configure the frequency of PRC clock.
------------------------------------------------------------------------------*/
#define cThaRegDCRPrcFreqCfg                     0x29000b

/*--------------------------------------
BitField Name: DcrPrcFrequency
BitField Type:
BitField Desc:
BitField Bits: 15_0
--------------------------------------*/
#define cThaDcrPrcFreqCfgMask                    cBit15_0
#define cThaDcrPrcFreqCfgShift                   0

/*------------------------------------------------------------------------------
Reg Name: DCR RTP Frequency Configuration
Reg Addr: 0x029000c
Reg Desc: This register is used to configure the frequency of PRC clock.
------------------------------------------------------------------------------*/
#define cThaRegDCRRtpFreqCfg                   0x29000c

/*--------------------------------------
BitField Name: DcrRtpFrequency
BitField Type:
BitField Desc:
BitField Bits: 15_0
--------------------------------------*/
#define cThaDcrRtpFreqCfgMask                    cBit15_0
#define cThaDcrRtpFreqCfgShift                   0

#endif /* THA_MODULE_CDR_REG_HEADER */

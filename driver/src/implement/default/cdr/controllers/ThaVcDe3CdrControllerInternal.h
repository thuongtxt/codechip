/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaVcDe3CdrControllerInternal.h
 * 
 * Created Date: Mar 4, 2015
 *
 * Description : VC DE3 CDR controller internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCDE3CDRCONTROLLERINTERNAL_H_
#define _THAVCDE3CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHoVcCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcDe3CdrController * ThaVcDe3CdrController;
typedef struct tThaVcDe3CdrController
    {
    tThaHoVcCdrController super;

    /* Private */
    }tThaVcDe3CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaVcDe3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
ThaCdrController ThaVcDe3CdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _THAVCDE3CDRCONTROLLERINTERNAL_H_ */


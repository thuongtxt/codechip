/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaHoVcCdrControllerInternal.h
 * 
 * Created Date: Apr 26, 2013
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHOVCCDRCONTROLLERINTERNAL_H_
#define _THAHOVCCDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaSdhChannelCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHoVcCdrController * ThaHoVcCdrController;

typedef struct tThaHoVcCdrControllerMethods
    {
    uint32 (*TimingSourceId)(ThaHoVcCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    eBool (*TimingSourceStsIdIsMaster)(ThaHoVcCdrController self, AtSdhChannel sdhChannel, uint8 sts1Id);
    }tThaHoVcCdrControllerMethods;

typedef struct tThaHoVcCdrController
    {
    tThaSdhChannelCdrController super;
    const tThaHoVcCdrControllerMethods *methods;

    /* Private data */
    uint8 fixedTimingMode;
    }tThaHoVcCdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaHoVcCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

uint32 ThaHoVcCdrControllerRegCDRStsTimeCtrl(ThaCdrController self);

#ifdef __cplusplus
}
#endif
#endif /* _THAHOVCCDRCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaVcDe3CdrController.c
 *
 * Created Date: Apr 26, 2013
 *
 * Description : CDR controller for DE3 that is mapped to VC-3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../map/ThaModuleStmMap.h"
#include "ThaVcDe3CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaVcDe3CdrController)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;

/* Save super implementation */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcDe3CdrController);
    }

static AtSdhChannel SdhChannel(ThaSdhChannelCdrController self, AtChannel channel)
    {
	AtUnused(self);
    return (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)channel);
    }

static uint8 HwDcrTimingMode(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtUnused(self);
    AtUnused(timingSource);

    if (timingMode == cAtTimingModeSys)        return 0;
    if (timingMode == cAtTimingModeLoop)       return 1;
    if (timingMode == cAtTimingModeAcr)        return 8;
    if (timingMode == cAtTimingModeDcr)        return 10;
    if (timingMode == cAtTimingModeExt1Ref)    return 4;
    if (timingMode == cAtTimingModeExt2Ref)    return 5;
    if (timingMode == cAtTimingModeSdhLineRef) return 6;
    if (timingMode == cAtTimingModeFreeRunning)return 7;

    return 0; /* System as default */
    }

static eBool HasPwFeature(ThaCdrController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaCdrControllerModuleGet(self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    return pwModule ? cAtTrue : cAtFalse;
    }

static eBool CDREngineTimingControlIsApplicable(ThaCdrController self, eAtTimingMode timingMode)
    {
    eBool timingModeIsApplicable = (timingMode == cAtTimingModeDcr || timingMode == cAtTimingModeAcr) ? cAtTrue : cAtFalse;
    eBool isPwFeature = HasPwFeature(self);
    if (mMethodsGet(self)->FreeRunningIsSupported(self))
        {
        if (timingMode == cAtTimingModeFreeRunning)
            timingModeIsApplicable = cAtTrue;
        }
    return timingModeIsApplicable && isPwFeature;
    }

static eAtRet TimingModeSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    uint32 regAddr, regVal;

    if (CDREngineTimingControlIsApplicable(self, timingMode))
        {
        AtPdhDe3 de3 = (AtPdhDe3)ThaCdrControllerChannelGet(self);
        regAddr = ThaCdrControllerCDREngineTimingCtrl(self) + mMethodsGet(self)->TimingCtrlOffset(self);
        regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
        mRegFieldSet(regVal, cThaHoldvalMode, 0);
        mRegFieldSet(regVal, cThaSeqMode, 0x0);
        mRegFieldSet(regVal, cThaCDRTimeMode, HwDcrTimingMode(self, timingMode, timingSource));

        mFieldIns(&regVal, cThaLineTypeMask, cThaLineTypeShift, AtPdhDe3IsE3(de3) ? 4 : 5);

        ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);
        }

    regAddr = ThaHoVcCdrControllerRegCDRStsTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaDE3TimeModeMask, cThaDE3TimeModeShift, ThaCdrControllerTimingModeSw2Hw(self, timingMode, timingSource));
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    uint32 regAddr, regVal;

    regAddr = ThaHoVcCdrControllerRegCDRStsTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaMapStsMdMask, cThaMapStsMdShift, 1);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static uint8 HwTimingModeGet(ThaCdrController self)
    {
    uint32 address, value;
    uint8 hwTimmingMode;

    address = ThaHoVcCdrControllerRegCDRStsTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    value = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldGet(value, cThaDE3TimeModeMask, cThaDE3TimeModeShift, uint8, &hwTimmingMode);

    return hwTimmingMode;
    }

static uint32 ChannelDefaultOffset(ThaCdrController self)
    {
    return ThaModuleCdrControllerDe3DefaultOffset(self);
    }

static int32 PpmCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtPdhDe3 de3 = (AtPdhDe3)ThaCdrControllerChannelGet(self);
    if (AtPdhDe3IsE3(de3))
        return ThaCdrDebuggerE3PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerDs3PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static int32 PpbCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtPdhDe3 de3 = (AtPdhDe3)ThaCdrControllerChannelGet(self);
    if (AtPdhDe3IsE3(de3))
        return ThaCdrDebuggerE3PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerDs3PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static uint8 ChannelSliceGet(ThaCdrController self, AtChannel channel)
    {
    uint8 slice, hwSts;

    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &hwSts);
    return slice;
    }

static uint32 ChannelIdFlat(ThaCdrController self, AtChannel channel)
    {
    uint8 slice, hwId;

    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &hwId);
    return hwId;
    }

static uint32 ChannelInterruptMask(ThaCdrController self)
    {
    AtUnused(self);
    return cAtPdhDe3AlarmClockStateChange;
    }

static uint8 TxBitRate(ThaCdrController self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)ThaCdrControllerChannelGet(self);
    return AtPdhDe3IsE3(de3) ? 3 : 2;
    }

static eBool CanOnlyUseFixedTiming(ThaCdrController self)
    {
    /* As this extend from ThaHoVcCdrController class and super implementation
     * is assuming channel is VC then VC methods are accessed. This may make
     * memory unsafe if not override this method */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlVcDe3Offset(self);
    }

static uint32 TimingStatusOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingStateVcDe3Offset(self);
    }

static ThaModuleMap ModuleMap(ThaCdrController self)
    {
    return (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet(ThaCdrControllerChannelGet(self)), cThaModuleMap);
    }

static uint32 MapLineCtrl(ThaCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;
    AtChannel de3 = ThaCdrControllerChannelGet(self);
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(controller, de3);
    ThaModuleStmMap mapModule = (ThaModuleStmMap)ModuleMap(self);
    uint32 offset;

    if (sdhChannel)
        offset = ThaSdhChannelCdrControllerMapLineOffset(controller, (AtChannel)sdhChannel, AtSdhChannelSts1Get(sdhChannel), 0, 0);
    else
        offset = ThaModuleStmMapPdhDe3MapLineOffset(mapModule, (AtPdhDe3)de3);

    return ThaModuleStmMapMapLineCtrl(mapModule, sdhChannel) + offset;
    }

static eAtRet TimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;
    AtChannel de3 = ThaCdrControllerChannelGet(self);
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(controller, de3);
    AtChannel source = mMethodsGet(self)->TimingSourceForConfiguration(self, timingMode, timingSource);
    ThaModuleMap mapModule = ModuleMap(self);
    ThaHoVcCdrController hoVcController = (ThaHoVcCdrController)self;
    uint32 regVal, address;
    eBool isMaster;
    uint32 mask, shift;
    uint32 timingSourceId;

    if (source == NULL)
        isMaster = cAtTrue;
    else
        isMaster = (source == de3) ? cAtTrue : cAtFalse;

    address = MapLineCtrl(self);
    regVal = ThaCdrControllerRead(self, address, cThaModuleMap);

    mask  = ThaModuleMapMapTimeSrcMastMask(mapModule, sdhChannel);
    shift = ThaModuleMapMapTimeSrcMastShift(mapModule, sdhChannel);
    mFieldIns(&regVal, mask, shift, isMaster ? 1 : 0);

    mask  = ThaModuleMapMapTimeSrcIdMask(mapModule, sdhChannel);
    shift = ThaModuleMapMapTimeSrcIdShift(mapModule, sdhChannel);
    timingSourceId = mMethodsGet(hoVcController)->TimingSourceId(hoVcController, timingMode, timingSource);
    mFieldIns(&regVal, mask, shift, timingSourceId);
    ThaCdrControllerWrite(self, address, regVal, cThaModuleMap);

    /* Save timing source */
    ThaCdrControllerTimingSourceSave(self, source);

    return cAtOk;
    }

static eBool TimingIsMaster(ThaCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(controller, ThaCdrControllerChannelGet(self));
    ThaModuleMap mapModule = ModuleMap(self);
    uint32 regAddr, regVal;

    regAddr = MapLineCtrl(self);
    regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleMap);

    return (regVal & ThaModuleMapMapTimeSrcMastMask(mapModule, sdhChannel)) ? cAtTrue : cAtFalse;
    }

static eAtTimingMode TimingModeGet(ThaCdrController self)
    {
    if ((!TimingIsMaster(self)) && (ThaCdrControllerChannelGet(self) != self->timingSource))
        return cAtTimingModeSlave;

    return m_ThaCdrControllerMethods->TimingModeGet(self);
    }

static void OverrideThaCdrController(ThaVcDe3CdrController self)
    {
    ThaCdrController controller = (ThaCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, TimingModeSet);
        mMethodOverride(m_ThaCdrControllerOverride, HwTimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelDefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelMapTypeSet);
        mMethodOverride(m_ThaCdrControllerOverride, PpmCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, PpbCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelSliceGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelIdFlat);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelInterruptMask);
        mMethodOverride(m_ThaCdrControllerOverride, TxBitRate);
        mMethodOverride(m_ThaCdrControllerOverride, CanOnlyUseFixedTiming);
        mMethodOverride(m_ThaCdrControllerOverride, TimingCtrlOffset);
        mMethodOverride(m_ThaCdrControllerOverride, TimingStatusOffset);
        mMethodOverride(m_ThaCdrControllerOverride, TimingSourceSet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeGet);
        }

    mMethodsSet(controller, &m_ThaCdrControllerOverride);
    }

static void OverrideThaSdhChannelCdrController(ThaVcDe3CdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, mMethodsGet(controller), sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, SdhChannel);
        }

    mMethodsSet(controller, &m_ThaSdhChannelCdrControllerOverride);
    }

static void Override(ThaVcDe3CdrController self)
    {
    OverrideThaCdrController(self);
    OverrideThaSdhChannelCdrController(self);
    }

ThaCdrController ThaVcDe3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHoVcCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaCdrController ThaVcDe3CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaVcDe3CdrControllerObjectInit(newController, engineId, channel);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaSdhChannelCdrController.c
 * 
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller for SDH channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "ThaSdhChannelCdrControllerInternal.h"
#include "../../sdh/ThaModuleSdh.h"
#include "../../map/ThaModuleStmMap.h"

/*--------------------------- Define -----------------------------------------*/
#define cLineTimingModeStartValue 2
#define cLineTimingModeStopValue  5

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaSdhChannelCdrController)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaSdhChannelCdrControllerMethods m_methods;

/* Override */
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/* Save super implementation */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhChannelCdrController);
    }

static uint8 PartGet(ThaCdrController self)
    {
    AtSdhChannel channel = mMethodsGet(mThis(self))->SdhChannel(mThis(self), ThaCdrControllerChannelGet(self));
    if (channel)
        return ThaModuleSdhPartOfChannel(channel);
    return 0;
    }
    
static eBool FreeRunningIsSupported(ThaCdrController self)
    {
    AtUnused(self);
    return cAtFalse;
    }
    
static eBool IsAdditionalSupportedTimingMode(ThaCdrController self, eAtTimingMode timingMode)
    {
	AtUnused(self);
    if ((timingMode == cAtTimingModeExt1Ref) ||
        (timingMode == cAtTimingModeExt2Ref) ||
        (timingMode == cAtTimingModeSdhSys)  ||
        (timingMode == cAtTimingModeSdhLineRef))
        return cAtTrue;

    if (mMethodsGet(self)->FreeRunningIsSupported(self))
        {
        if (timingMode == cAtTimingModeFreeRunning)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool TimingModeIsSupported(ThaCdrController self, eAtTimingMode timingMode)
    {
    if (IsAdditionalSupportedTimingMode(self, timingMode))
        return cAtTrue;

    return m_ThaCdrControllerMethods->TimingModeIsSupported(self, timingMode);
    }

static eBool TimingIsSupported(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (IsAdditionalSupportedTimingMode(self, timingMode))
        return cAtTrue;

    return m_ThaCdrControllerMethods->TimingIsSupported(self, timingMode, timingSource);
    }

static AtSdhChannel SdhChannel(ThaSdhChannelCdrController self, AtChannel channel)
    {
	AtUnused(self);
    return (AtSdhChannel)channel;
    }

static eAtRet HwTimingModeSet(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
	AtUnused(timingSource);
	AtUnused(timingMode);
	AtUnused(self);
    /* Concrete must do */
    return cAtErrorNotImplemented;
    }

static uint8 TimingModeSw2Hw(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (timingMode == cAtTimingModeExt1Ref) return 6;
    if (timingMode == cAtTimingModeExt2Ref) return 7;

    if (mMethodsGet(self)->FreeRunningIsSupported(self) && (timingMode == cAtTimingModeFreeRunning))
        return 8;

    if (timingMode == cAtTimingModeSdhSys)  return 9;

    if (timingMode == cAtTimingModeSdhLineRef)
        {
        uint8 localLineId = (uint8)ThaModuleSdhLineLocalId((AtSdhLine)timingSource);
        return (uint8)(cLineTimingModeStartValue + localLineId);
        }

    return m_ThaCdrControllerMethods->TimingModeSw2Hw(self, timingMode, timingSource);
    }

static eAtTimingMode TimingModeHw2Sw(ThaCdrController self, uint8 hwTimingMode)
    {
    if (hwTimingMode == 6)
        return cAtTimingModeExt1Ref;
    if (hwTimingMode == 7)
        return cAtTimingModeExt2Ref;

    if (mMethodsGet(self)->FreeRunningIsSupported(self) && (hwTimingMode == 8))
        return cAtTimingModeFreeRunning;

    if (hwTimingMode == 9)
        return cAtTimingModeSdhSys;
    if ((hwTimingMode >= cLineTimingModeStartValue) && (hwTimingMode <= cLineTimingModeStopValue))
        return cAtTimingModeSdhLineRef;

    return m_ThaCdrControllerMethods->TimingModeHw2Sw(self, hwTimingMode);
    }

static AtChannel TimingSourceGet(ThaCdrController self)
    {
    uint8 hwTimingMode = mMethodsGet(self)->HwTimingModeGet(self);
    eAtTimingMode timingMode = ThaCdrControllerTimingModeHw2Sw(self, hwTimingMode);

    if ((timingMode == cAtTimingModeExt1Ref) ||
        (timingMode == cAtTimingModeExt2Ref) ||
        (timingMode == cAtTimingModeSdhSys))
        return NULL;

    if (timingMode == cAtTimingModeSdhLineRef)
        {
        uint8 localLineId = (uint8)(hwTimingMode - cLineTimingModeStartValue);
        AtSdhChannel sdhChannel = mMethodsGet(mThis(self))->SdhChannel(mThis(self), ThaCdrControllerChannelGet(self));
        AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);
        uint8 startLine = (uint8)(ThaModuleSdhPartOfChannel(sdhChannel) * ThaModuleSdhNumLinesPerPart(sdhModule));

        return (AtChannel)AtModuleSdhLineGet(sdhModule, (uint8)(startLine + localLineId));
        }

    /* Ask super for other cases */
    return m_ThaCdrControllerMethods->TimingSourceGet(self);
    }

static uint8 TxBitRate(ThaCdrController self)
    {
    AtSdhChannel channel = (AtSdhChannel)ThaCdrControllerChannelGet(self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    if (channelType == cAtSdhChannelTypeVc11) return 4;
    if (channelType == cAtSdhChannelTypeVc12) return 5;
    if (channelType == cAtSdhChannelTypeVc3)  return 6;
    if (channelType == cAtSdhChannelTypeVc4)  return 7;     /*reserve for HW*/
    if (channelType == cAtSdhChannelTypeVc4_4c)    return 7;/*reserve for HW*/
    if (channelType == cAtSdhChannelTypeVc4_16c)   return 7;/*reserve for HW*/
    if (channelType == cAtSdhChannelTypeVc4_64c)   return 7;/*reserve for HW*/
    if (channelType == cAtSdhChannelTypeVc4_nc)    return 7;/*reserve for HW*/
    if (channelType == cAtSdhChannelTypeVc4_16nc)  return 7;/*reserve for HW*/

    AtChannelLog((AtChannel)channel, cAtLogLevelCritical, AtSourceLocation, "Unknown channel type\r\n");

    return 0;
    }

static eAtRet TimingPacketLenInBitsSet(ThaCdrController self, uint32 packetLengthInBits)
    {
    uint32 regAddr, regVal = 0;
    eAtRet ret;
    ThaModuleCdr cdrModule = ThaCdrControllerModuleGet(self);

    ret = m_ThaCdrControllerMethods->TimingPacketLenInBitsSet(self, packetLengthInBits);
    if (ret != cAtOk)
        return ret;

    if (!ThaModuleCdrDcrShaperIsSupported(cdrModule, self))
        return cAtOk;

    /* Register Full Name: DCR Tx Engine Timing control
       RTL Instant Name  : dcr_tx_eng_timing_ctrl*/
    regAddr = ThaModuleCdrDcrTxEngineTimingControlRegister(cdrModule) + ThaModuleCdrTxEngineTimingControlOffset(self);
    mRegFieldSet(regVal, cThaDcrTxPackLenExtCfg, (packetLengthInBits % 8)); /* Number of bits are left */
    mRegFieldSet(regVal, cThaDcrTxRateCfg, ThaCdrControllerTxBitRate(self));
    mRegFieldSet(regVal, cThaDcrTxPackLenCfg, packetLengthInBits / 8);      /* Number of bytes */
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet EparEnable(ThaSdhChannelCdrController self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtError;
    }

static eBool EparIsEnabled(ThaSdhChannelCdrController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MapLineOffset(ThaSdhChannelCdrController self, AtChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    ThaModuleStmMap mapModule = (ThaModuleStmMap)AtDeviceModuleGet(device, cThaModuleMap);
    AtUnused(self);
    return ThaModuleStmMapSdhChannelMapLineOffset(mapModule, (AtSdhChannel)channel, stsId, vtgId, vtId);
    }

static eAtRet StsTimingModeSet(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtUnused(self);
    AtUnused(timingMode);
    AtUnused(timingSource);
    return cAtOk;
    }

static uint8 ChannelSliceGet(ThaCdrController self, AtChannel channel)
    {
    uint8 slice, hwSts;
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(mThis(self), channel);
    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &hwSts);

    return slice;
    }

static uint32 ChannelInterruptMask(ThaCdrController self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmClockStateChange;
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlVcOffset(self);
    }

static uint32 TimingStatusOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingStateVcOffset(self);
    }

static void MethodsInit(ThaSdhChannelCdrController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SdhChannel);
        mMethodOverride(m_methods, HwTimingModeSet);
        mMethodOverride(m_methods, EparEnable);
        mMethodOverride(m_methods, EparIsEnabled);
        mMethodOverride(m_methods, MapLineOffset);
        mMethodOverride(m_methods, StsTimingModeSet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaCdrController(ThaSdhChannelCdrController self)
    {
    ThaCdrController controller = (ThaCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, PartGet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, FreeRunningIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, TimingIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeSw2Hw);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeHw2Sw);
        mMethodOverride(m_ThaCdrControllerOverride, TimingSourceGet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingPacketLenInBitsSet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelSliceGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelInterruptMask);
        mMethodOverride(m_ThaCdrControllerOverride, TxBitRate);
        mMethodOverride(m_ThaCdrControllerOverride, TimingCtrlOffset);
        mMethodOverride(m_ThaCdrControllerOverride, TimingStatusOffset);
        }

    mMethodsSet(controller, &m_ThaCdrControllerOverride);
    }

static void Override(ThaSdhChannelCdrController self)
    {
    OverrideThaCdrController(self);
    }

ThaCdrController ThaSdhChannelCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* No additional setup at this version */
    return self;
    }

eAtRet ThaSdhChannelCdrControllerEparEnable(ThaSdhChannelCdrController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->EparEnable(self, enable);
    return cAtError;
    }

eBool ThaSdhChannelCdrControllerEparIsEnabled(ThaSdhChannelCdrController self)
    {
    if (self)
        return mMethodsGet(self)->EparIsEnabled(self);
    return cAtFalse;
    }

AtSdhChannel ThaSdhChannelCdrControllerSdhChannel(ThaSdhChannelCdrController self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->SdhChannel(self, channel);
    return NULL;
    }

uint32 ThaSdhChannelCdrControllerMapLineOffset(ThaSdhChannelCdrController self, AtChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    if (self)
        return mMethodsGet(self)->MapLineOffset(self, channel, stsId, vtgId, vtId);
    return cBit31_0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaSdhChannelCdrControllerInternal.h
 * 
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller for SDH channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASDHCHANNELCDRCONTROLLERINTERNAL_H_
#define _THASDHCHANNELCDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaCdrControllerInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../sdh/ThaSdhVcInternal.h"
#include "../../map/ThaModuleStmMapReg.h"
#include "../../pdh/ThaModulePdhForStmReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaSdhChannelCdrControllerMethods
    {
    AtSdhChannel (*SdhChannel)(ThaSdhChannelCdrController self, AtChannel channel);
    eAtRet (*HwTimingModeSet)(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    eAtRet (*EparEnable)(ThaSdhChannelCdrController self, eBool enable);
    eBool (*EparIsEnabled)(ThaSdhChannelCdrController self);
    uint32 (*MapLineOffset)(ThaSdhChannelCdrController self, AtChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);
    eAtRet (*StsTimingModeSet)(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    }tThaSdhChannelCdrControllerMethods;

typedef struct tThaSdhChannelCdrController
    {
    tThaCdrController super;
    const tThaSdhChannelCdrControllerMethods *methods;

    /* Private data */
    }tThaSdhChannelCdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaSdhChannelCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THASDHCHANNELCDRCONTROLLERINTERNAL_H_ */


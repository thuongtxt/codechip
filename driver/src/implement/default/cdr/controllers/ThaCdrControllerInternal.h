/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaCdrControllerInternal.h
 * 
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACDRCONTROLLERINTERNAL_H_
#define _THACDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/man/interrupt/AtInterruptManager.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../cla/pw/ThaModuleClaPwInternal.h"
#include "../ThaModuleCdrStmReg.h"
#include "ThaCdrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCdrControllerMethods
    {
    eAtRet (*Init)(ThaCdrController self);
    AtChannel (*ChannelGet)(ThaCdrController self); /*Channel for interrupt/clock status report*/
    AtChannel (*ReportChannelGet)(ThaCdrController self);
    uint8 (*PartGet)(ThaCdrController self);
    eBool (*TimingModeIsSupported)(ThaCdrController self, eAtTimingMode timingMode);
    eBool (*FreeRunningIsSupported)(ThaCdrController self);
    eBool (*TimingIsSupported)(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    eAtRet (*TimingSet)(ThaCdrController, eAtTimingMode timingMode, AtChannel timingSource);
    eAtRet (*TimingModeSet)(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    eAtTimingMode (*TimingModeGet)(ThaCdrController self);
    AtChannel (*TimingSourceGet)(ThaCdrController self);
    eAtRet (*TimingSourceSet)(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    AtPw (*CurrentAcrDcrTimingSource)(ThaCdrController self);
    AtPw  (*AcrDcrTimingSource)(ThaCdrController self);
    AtChannel (*TimingSourceForConfiguration)(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    uint8 (*HwTimingModeGet)(ThaCdrController self);
    eAtClockState (*ClockStateGet)(ThaCdrController self);
    uint8 (*HwClockStateGet)(ThaCdrController self);
    uint8 (*TimingModeSw2Hw)(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    eAtTimingMode (*TimingModeHw2Sw)(ThaCdrController self, uint8 hwTimingMode);
    eAtRet (*RxNcoPackLenSet)(ThaCdrController self, uint32 packetLength);
    eAtRet (*TimingPacketLenInBitsSet)(ThaCdrController self, uint32 packetLengthInBits);
    eAtRet (*CwSequenceModeSet)(ThaCdrController self, eAtPwCwSequenceMode sequenceMode);
    eAtPwCwSequenceMode (*CwSequenceModeGet)(ThaCdrController self);
    void (*Debug)(ThaCdrController self);
    void (*PwHeaderDebug)(ThaCdrController self, AtPw pw);
    uint8 (*HwSystemTimingMode)(ThaCdrController self);
    eAtRet (*CdrIdAssign)(ThaCdrController self);
    eAtRet (*ChannelMapTypeSet)(ThaCdrController self);
    int32 (*PpmCalculate)(ThaCdrController self, uint32 ncoValue);
    uint8 (*TxBitRate)(ThaCdrController self);
    int32 (*PpbCalculate)(ThaCdrController self, uint32 ncoValue);

    /* For channel that cannot flexible configure timing mode */
    eBool (*CanOnlyUseFixedTiming)(ThaCdrController self);
    eAtTimingMode (*FixedTimingMode)(ThaCdrController self);
    eBool (*CanSpecifyFixedTimingMode)(ThaCdrController self, eAtTimingMode timingMode);

    /* Register offset */
    uint32 (*ChannelDefaultOffset)(ThaCdrController self);
    uint32 (*ChannelIdFlat)(ThaCdrController self, AtChannel channel);
    uint32 (*DefaultOffset)(ThaCdrController self);
    uint8 (*ChannelSliceGet)(ThaCdrController self, AtChannel channel);
    uint32 (*ControllerIdGet)(ThaCdrController self);
    uint32 (*TimingCtrlOffset)(ThaCdrController self);
    uint32 (*TimingStatusOffset)(ThaCdrController self);

    /* Interrupt */
    eAtRet (*InterruptMaskSet)(ThaCdrController self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(ThaCdrController self);
    uint32 (*InterruptRead2Clear)(ThaCdrController self, eBool read2Clear);
    void (*InterruptProcess)(ThaCdrController self, uint32 offset);
    uint32 (*ChannelInterruptMask)(ThaCdrController self); /* Return clock-state-change mask specified on channel type. */
    AtInterruptManager (*InterruptManager)(ThaCdrController self);

    /* Debug */
    void (*RegsShow)(ThaCdrController self);
    }tThaCdrControllerMethods;

typedef struct tThaCdrController
    {
    tAtObject super;
    const tThaCdrControllerMethods *methods;

    /* Private data */
    AtChannel channel;
    AtChannel timingSource;
    AtPw pwTimingSource;
    uint32 controllerId;
    eBool systemTiming;
    }tThaCdrController;

typedef struct tThaDe1CdrControllerMethods
    {
	eAtRet (*LiuTimingModeSet)(ThaDe1CdrController self, eAtTimingMode timingMode);
    }tThaDe1CdrControllerMethods;

typedef struct tThaDe1CdrController
    {
    tThaCdrController super;
    const tThaDe1CdrControllerMethods *methods;

    /* Private */
    }tThaDe1CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
ThaCdrController ThaDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

ThaModuleCdr ThaCdrControllerModuleGet(ThaCdrController self);
uint32 ThaCdrControllerRead(ThaCdrController self, uint32 address, eAtModule moduleId);
void ThaCdrControllerWrite(ThaCdrController self, uint32 address, uint32 value, eAtModule moduleId);
uint16 ThaCdrControllerLongRead(ThaCdrController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen, eAtModule moduleId);
void ThaCdrControllerLongWrite(ThaCdrController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen, eAtModule moduleId);

/* Util */
uint8 ThaCdrControllerTimingModeSw2Hw(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
eAtTimingMode ThaCdrControllerTimingModeHw2Sw(ThaCdrController self, uint8 hwTimingMode);
eAtTimingMode ThaCdrControllerCdrTimingModeGet(ThaCdrController self);

eAtRet ThaDe1CdrControllerTimingSourceSave(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
AtPw ThaDe1CdrControllerCurrentAcrDcrTimingSource(ThaCdrController self);
eBool ThaDe1CdrControllerAcrDcrTimingConfigurationIsValid(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);

#ifdef __cplusplus
}
#endif
#endif /* _THACDRCONTROLLERINTERNAL_H_ */


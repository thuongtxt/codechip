/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaVc1xCdrController.c
 *
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller for AU-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaVc1xCdrControllerInternal.h"
#include "../../map/ThaModuleStmMapInternal.h"
#include "../../pdh/ThaStmModulePdh.h"
#include "../ThaModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cFifoModeByteStuff   0
#define cFifoModeBitStuff    1
#define cByteFifoModeLoop    0
#define cByteFifoModeDefault 1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaVc1xCdrController)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVc1xCdrControllerMethods m_methods;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;

/* Save super implementation */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelIdFlat(ThaCdrController self, AtChannel channel)
    {
    uint8 slice, sts;
    ThaSdhChannelCdrController sdhCdrController = (ThaSdhChannelCdrController)self;
    AtSdhChannel sdhChannel = mMethodsGet(sdhCdrController)->SdhChannel(sdhCdrController, channel);

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);
    return (sts * 32UL) + (AtSdhChannelTug2Get(sdhChannel) * 4UL) + AtSdhChannelTu1xGet(sdhChannel);
    }

static uint8 MappingMode(ThaVc1xCdrController self)
    {
	AtUnused(self);
    return 0;
    }

static eBool IsAcrDcrTiming(eAtTimingMode timingMode)
    {
    return ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr)) ? cAtTrue : cAtFalse;
    }

static eBool AcrRunInByteMode(ThaSdhChannelCdrController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MakeAcrJitterConfiguration(ThaSdhChannelCdrController self, eAtTimingMode timingMode)
    {
    uint32 jitterConfigure = 0;
    eBool  isAcrByteMode   = AcrRunInByteMode(self);

    mRegFieldSet(jitterConfigure, cThaStsVtMapJitCfgBitFifoMode, (isAcrByteMode) ? cFifoModeByteStuff : cFifoModeBitStuff);

    if (isAcrByteMode == cAtTrue)
        mRegFieldSet(jitterConfigure, cThaStsVtMapJitCfgByteFifoStuffMode, cByteFifoModeDefault);
    else
        mRegFieldSet(jitterConfigure, cThaStsVtMapJitCfgMasterEnable, (timingMode == cAtTimingModeSlave) ? 0 : 1);

    return jitterConfigure >> cThaStsVtMapJitCfgShift;
    }

static uint32 MakeJitterConfiguration(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eBool  isAcrDcrTiming  = IsAcrDcrTiming(timingMode);

    /* Still need to make jitter configuration if the clock source is in ACR/DCR timing mode */
    if ((timingMode == cAtTimingModeSlave) && (IsAcrDcrTiming(AtChannelTimingModeGet(timingSource))))
        isAcrDcrTiming = cAtTrue;

    if (isAcrDcrTiming)
        return MakeAcrJitterConfiguration(self, timingMode);
    else
        {
        uint32 jitterConfigure = 0;
        mRegFieldSet(jitterConfigure, cThaStsVtMapJitCfgBitFifoMode, cFifoModeByteStuff);
        mRegFieldSet(jitterConfigure,
                     cThaStsVtMapJitCfgByteFifoStuffMode,
                     (timingMode == cAtTimingModeLoop) ? cByteFifoModeLoop : cByteFifoModeDefault);
        return jitterConfigure >> cThaStsVtMapJitCfgShift;
        }
    }

static uint8 VtHwTimingMode(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    return ThaCdrControllerTimingModeSw2Hw((ThaCdrController)self, timingMode, timingSource);
    }

static uint8 De1HwTimingMode(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
	AtUnused(timingSource);
	AtUnused(timingMode);
	AtUnused(self);
    return 0;
    }

static eAtRet AcrDcrTimingSet(ThaSdhChannelCdrController self, uint32 engineId)
    {
    ThaCdrController controller;
    uint32 longReg[cThaLongRegMaxSize];
    AtChannel channel;
    uint32 offset, address;

    if (!mMethodsGet(mThis(self))->VtAsyncMapIsSupported(mThis(self)))
        return cAtOk;

    controller = (ThaCdrController)self;
    channel = ThaCdrControllerChannelGet(controller);
    offset = mMethodsGet(controller)->ChannelDefaultOffset(controller);
    address = cThaRegVtAsyncMapCtrl + offset;

    mChannelHwLongRead(channel, address, longReg, cThaLongRegMaxSize, cAtModulePdh);
    mRegFieldSet(longReg[0], cThaVtAsyncMapJitIDCfgHwTail, mRegField(engineId, cThaVtAsyncMapJitIDCfgSwTail));
    mRegFieldSet(longReg[1], cThaVtAsyncMapJitIDCfgHwHead, mRegField(engineId, cThaVtAsyncMapJitIDCfgSwHead));
    mChannelHwLongWrite(channel, address, longReg, cThaLongRegMaxSize, cAtModulePdh);

    return cAtOk;
    }

uint32 ThaVc1xCdrControllerCDRVtTimeCtrl(ThaCdrController self)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)ThaCdrControllerModuleGet(self);
    if (moduleCdr)
        return mMethodsGet(moduleCdr)->VtTimingControlReg(moduleCdr);

    return 0;
    }

static uint32 ThaVTCDRChidMask(ThaCdrController self)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)ThaCdrControllerModuleGet(self);
    if (moduleCdr)
        return mMethodsGet(moduleCdr)->VtCdrChannelIdMask(moduleCdr);

    return 0;
    }

static uint32 ThaVTCDRChidShift(ThaCdrController self)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)ThaCdrControllerModuleGet(self);
    if (moduleCdr)
        return mMethodsGet(moduleCdr)->VtCdrChannelIdShift(moduleCdr);

    return 0;
    }

static uint32 HwTimingModeMask(ThaVc1xCdrController self)
    {
    AtUnused(self);
    return cThaVtTimeModeMask;
    }

static uint32 HwTimingModeShift(ThaVc1xCdrController self)
    {
    AtUnused(self);
    return cThaVtTimeModeShift;
    }

static eAtRet HwTimingModeSet(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaCdrController controller = (ThaCdrController)self;
    uint32 offset = mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 address = ThaVc1xCdrControllerCDRVtTimeCtrl((ThaCdrController)self) + offset;
    uint32 value = ThaCdrControllerRead(controller, address, cThaModuleCdr);
    AtSdhChannel channel;

    /* Set hardware timing mode */
    mFieldIns(&value,
              mMethodsGet(mThis(self))->HwTimingModeMask(mThis(self)),
              mMethodsGet(mThis(self))->HwTimingModeShift(mThis(self)),
              ThaCdrControllerTimingModeSw2Hw(controller, timingMode, timingSource));
    ThaCdrControllerWrite(controller, address, value, cThaModuleCdr);

    /* Configure jitter for VT asynce incase map DE1 in VT */
    channel = ThaSdhChannelCdrControllerSdhChannel(self, ThaCdrControllerChannelGet(controller));
    if (channel)
        ThaStmModulePdhStsVtMapJitterSet(channel, MakeJitterConfiguration(self, timingMode, timingSource));

    /* ACR/DCR specific configuration */
    if (channel && IsAcrDcrTiming(timingMode))
        AcrDcrTimingSet(self, ThaCdrControllerIdGet(controller));

    return cAtOk;
    }

static eAtRet TimingModeSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    uint32 address, value;
    eAtRet ret;
    ThaSdhChannelCdrController sdhCdrController = (ThaSdhChannelCdrController)self;

    /* Need to register CDR engine ID for this channel */
    address = ThaVc1xCdrControllerCDRVtTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    value = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldIns(&value, ThaVTCDRChidMask(self), ThaVTCDRChidShift(self), ThaCdrControllerIdGet(self));
    ThaCdrControllerWrite(self, address, value, cThaModuleCdr);

    /* And have the super do first */
    ret = m_ThaCdrControllerMethods->TimingModeSet(self, timingMode, timingSource);
    if (ret != cAtOk)
        return ret;

    /* Then the VC1x itself */
    mMethodsGet(sdhCdrController)->HwTimingModeSet(sdhCdrController, timingMode, timingSource);

    return cAtOk;
    }

static ThaCdrController TimingSourceCdrController(ThaVc1xCdrController self, AtChannel timingSource)
    {
	AtUnused(self);
    return ThaSdhVcCdrControllerGet((AtSdhVc)timingSource);
    }

static eAtRet SlaveAcrTimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaCdrController timingSourceCdrController;
	AtUnused(timingMode);

    if (!IsAcrDcrTiming(AtChannelTimingModeGet(timingSource)))
        return cAtOk;

    timingSourceCdrController = mMethodsGet(mThis(self))->TimingSourceCdrController(mThis(self), timingSource);
    if (timingSourceCdrController == NULL)
        return cAtOk;

    return AcrDcrTimingSet((ThaSdhChannelCdrController)self, ThaCdrControllerIdGet(timingSourceCdrController));
    }

static eAtRet TimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    AtChannel source = mMethodsGet(self)->TimingSourceForConfiguration(self, timingMode, timingSource);
    ThaModuleMap mapModule = (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet(ThaCdrControllerChannelGet(self)), cThaModuleMap);
    AtChannel controllerChannel = ThaCdrControllerChannelGet(self);
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel((ThaSdhChannelCdrController)self, controllerChannel);
    eBool isMaster = (source == controllerChannel) ? cAtTrue : cAtFalse;
    uint8 vtgId;
    uint8 vtId;
    uint32 address;
    uint32 regVal;
    uint32 offset;

    if (sdhChannel)
        {
        vtgId = AtSdhChannelTug2Get(sdhChannel);
        vtId = AtSdhChannelTu1xGet(sdhChannel);
        offset = ThaSdhChannelCdrControllerMapLineOffset((ThaSdhChannelCdrController)self, (AtChannel)sdhChannel, AtSdhChannelSts1Get(sdhChannel), vtgId, vtId);;
        }
    else
        offset = ThaModuleStmMapPdhDe2De1MapLineOffset((ThaModuleStmMap)mapModule, (AtPdhDe1)controllerChannel);

    address = ThaModuleStmMapMapLineCtrl((ThaModuleStmMap)mapModule, sdhChannel) + offset;
    regVal = ThaCdrControllerRead(self, address, cThaModuleMap);
    mFieldIns(&regVal, ThaModuleMapMapTimeSrcMastMask(mapModule, sdhChannel), ThaModuleMapMapTimeSrcMastShift(mapModule, sdhChannel), isMaster ? 1 : 0);
    mFieldIns(&regVal, ThaModuleMapMapTimeSrcIdMask(mapModule, sdhChannel), ThaModuleMapMapTimeSrcIdShift(mapModule, sdhChannel), mMethodsGet(self)->ChannelIdFlat(self, source));
    ThaCdrControllerWrite(self, address, regVal, cThaModuleMap);

    /* In case of slaving timing mode, if timing source is a channel that has
     * ACR/DCR timing, need to configure ACR/DCR information for this channel */
    if (timingMode == cAtTimingModeSlave)
        ret |= SlaveAcrTimingSourceSet(self, timingMode, timingSource);

    ret |= m_ThaCdrControllerMethods->TimingSourceSet(self, timingMode, timingSource);
    
    return ret;
    }

static uint8 HwTimingModeGet(ThaCdrController self)
    {
    uint32 address, value;
    uint8 hwTimmingMode;

    address = ThaVc1xCdrControllerCDRVtTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    value = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldGet(value, cThaVtTimeModeMask, cThaVtTimeModeShift, uint8, &hwTimmingMode);

    return hwTimmingMode;
    }

static eAtRet EparEnable(ThaSdhChannelCdrController self, eBool enable)
    {
    ThaCdrController controller = (ThaCdrController)self;
    uint32 regAddr = ThaVc1xCdrControllerCDRVtTimeCtrl((ThaCdrController)self) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 regVal  = ThaCdrControllerRead(controller, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaVTEParEnMask, cThaVTEParEnShift, mBoolToBin(enable));
    ThaCdrControllerWrite(controller, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static eBool EparIsEnabled(ThaSdhChannelCdrController self)
    {
    ThaCdrController controller = (ThaCdrController)self;
    uint32 regAddr = ThaVc1xCdrControllerCDRVtTimeCtrl((ThaCdrController)self) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 regVal  = ThaCdrControllerRead(controller, regAddr, cThaModuleCdr);
    return (regVal & cThaVTEParEnMask) ? cAtTrue : cAtFalse;
    }

static eBool VtAsyncMapIsSupported(ThaVc1xCdrController self)
    {
    return ThaModuleCdrVtAsyncMapIsSupported(ThaCdrControllerModuleGet((ThaCdrController)self));
    }

static uint32 ChannelDefaultOffset(ThaCdrController self)
    {
    return ThaModuleCdrControllerVc1xDefaultOffset(self);
    }

static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    uint32 address, value;
    ThaVc1xCdrController controller = mThis(self);

    address = ThaVc1xCdrControllerCDRVtTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    value = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldIns(&value, cThaMapVtMdMask, cThaMapVtMdShift, mMethodsGet(controller)->MappingMode(controller));
    ThaCdrControllerWrite(self, address, value, cThaModuleCdr);

    return cAtOk;
    }

static void RegDisplay(ThaCdrController self, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay(ThaCdrControllerChannelGet(self), address, cThaModuleCdr);
    }

static void RegsShow(ThaCdrController self)
    {
    uint32 address;
    ThaModuleCdr moduleCdr = ThaCdrControllerModuleGet(self);

    address = ThaVc1xCdrControllerCDRVtTimeCtrl(self) + ThaModuleCdrControllerVc1xDefaultOffset(self);
    RegDisplay(self, "CDR VT Timing control", address);

    address = ThaModuleCdrEngineTimingCtrlRegister(moduleCdr) + mMethodsGet(self)->TimingCtrlOffset(self);
    RegDisplay(self, "CDR Engine Timing control", address);

    address = ThaModuleCdrAdjustStateStatusRegister(moduleCdr, self) + mMethodsGet(self)->TimingStatusOffset(self);
    RegDisplay(self, "CDR Adjust State status", address);
    }

static int32 PpmCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtSdhChannel vc1x = (AtSdhChannel)ThaCdrControllerChannelGet(self);
    if (AtSdhChannelTypeGet(vc1x) == cAtSdhChannelTypeVc12)
        return ThaCdrDebuggerVc12PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerVc11PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static int32 PpbCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtSdhChannel vc1x = (AtSdhChannel)ThaCdrControllerChannelGet(self);
    if (AtSdhChannelTypeGet(vc1x) == cAtSdhChannelTypeVc12)
        return ThaCdrDebuggerVc12PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerVc11PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static void MethodsInit(ThaVc1xCdrController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, MappingMode);
        mMethodOverride(m_methods, VtHwTimingMode);
        mMethodOverride(m_methods, De1HwTimingMode);
        mMethodOverride(m_methods, TimingSourceCdrController);
        mMethodOverride(m_methods, VtAsyncMapIsSupported);
        mMethodOverride(m_methods, HwTimingModeMask);
        mMethodOverride(m_methods, HwTimingModeShift);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaCdrController(ThaVc1xCdrController self)
    {
    ThaCdrController controller = (ThaCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, TimingSourceSet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelIdFlat);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeSet);
        mMethodOverride(m_ThaCdrControllerOverride, HwTimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelDefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelMapTypeSet);
        mMethodOverride(m_ThaCdrControllerOverride, RegsShow);
        mMethodOverride(m_ThaCdrControllerOverride, PpmCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, PpbCalculate);
        }

    mMethodsSet(controller, &m_ThaCdrControllerOverride);
    }

static void OverrideThaSdhChannelCdrController(ThaVc1xCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, mMethodsGet(controller), sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, HwTimingModeSet);
        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, EparEnable);
        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, EparIsEnabled);
        }

    mMethodsSet(controller, &m_ThaSdhChannelCdrControllerOverride);
    }

static void Override(ThaVc1xCdrController self)
    {
    OverrideThaCdrController(self);
    OverrideThaSdhChannelCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVc1xCdrController);
    }

ThaCdrController ThaVc1xCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhChannelCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaCdrController ThaVc1xCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaVc1xCdrControllerObjectInit(newController, engineId, channel);
    }

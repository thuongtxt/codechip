/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaCdrController.c
 *
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCep.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../cla/controllers/ThaClaPwController.h"
#include "../../man/intrcontroller/ThaInterruptManager.h"
#include "../ThaCdrInterruptManagerInternal.h"
#include "../ThaModuleCdrInternal.h"
#include "../ThaModuleCdrIntrReg.h"
#include "ThaCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cClockMainStateLocked 5

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaCdrControllerMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(ThaCdrController self)
    {
    return AtChannelDeviceGet(ThaCdrControllerChannelGet(self));
    }

static uint32 ChannelIdFlat(ThaCdrController self, AtChannel channel)
    {
	AtUnused(self);
    return AtChannelIdGet(channel);
    }

static uint8 PartGet(ThaCdrController self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 ChannelDefaultOffset(ThaCdrController self)
    {
    return ThaModuleCdrChannelDefaultOffset(self);
    }

static uint32 DefaultOffset(ThaCdrController self)
    {
    return ThaModuleCdrControllerDefaultOffset(self);
    }

static eAtRet TimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    self->timingSource = mMethodsGet(self)->TimingSourceForConfiguration(self, timingMode, timingSource);
    return cAtOk;
    }

static uint32 CDREngineAdjustStateStatus(ThaCdrController self)
    {
    return ThaModuleCdrAdjustStateStatusRegister(ThaCdrControllerModuleGet(self), self);
    }

static void ClockStateReset(ThaCdrController self)
    {
    uint32 regVal, regAddr;

    regAddr = CDREngineAdjustStateStatus(self) + mMethodsGet(self)->TimingStatusOffset(self);
    regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaAdjStateMask2, cThaAdjStateShift2, 0);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);
    }

static eBool TimingModeIsSupported(ThaCdrController self, eAtTimingMode timingMode)
    {
	AtUnused(self);
    if ((timingMode == cAtTimingModeSys)  ||
        (timingMode == cAtTimingModeLoop) ||
        (timingMode == cAtTimingModeAcr)  ||
        (timingMode == cAtTimingModeDcr)  ||
        (timingMode == cAtTimingModeSlave))
        return cAtTrue;
    if (mMethodsGet(self)->FreeRunningIsSupported(self))
        {
        if (timingMode == cAtTimingModeFreeRunning)
            return cAtTrue;
        }
    return cAtFalse;
    }

static eBool TimingIsSupported(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (!ThaCdrControllerTimingModeIsSupported(self, timingMode))
        return cAtFalse;

    if (timingMode == cAtTimingModeSlave)
        {
        eAtTimingMode masterTimingMode = AtChannelTimingModeGet(timingSource);
        if ((masterTimingMode == cAtTimingModeSlave) || (masterTimingMode == cAtTimingModeUnknown))
            return cAtFalse;
        else
            return cAtTrue;
        }

    return cAtTrue;
    }

static AtChannel TimingSourceForConfiguration(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtChannel channel = ThaCdrControllerChannelGet(self);

    if (timingMode == cAtTimingModeSlave)
        return timingSource;

    if ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr))
        {
        if (AtChannelBoundPwGet(channel))
            return (AtChannel)channel;

        /* For CESoP, need a special case */
        if (timingSource)
            {
            AtPw pw = (AtPw)timingSource;
            if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
                return channel;

            return (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)AtPwBoundCircuitGet(pw));
            }
        }

    return channel;
    }

static AtPw CurrentAcrDcrTimingSource(ThaCdrController self)
    {
    return AtChannelBoundPwGet(ThaCdrControllerChannelGet(self));
    }

static eAtTimingMode OtherTimingSourceAsSystemTiming(ThaCdrController self)
    {
    ThaModuleCdr cdrModule = ThaCdrControllerModuleGet(self);
    return ThaModuleCdrOtherTimingSourceAsSystemTiming(cdrModule);
    }

static ThaModulePwe ModulePwe(ThaCdrController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaCdrControllerModuleGet(self));
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static AtPw  AcrDcrTimingSource(ThaCdrController self)
    {
    return AtChannelBoundPwGet(ThaCdrControllerChannelGet(self));
    }

static eAtRet TimingSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(DeviceGet(self), cThaModuleCla);
    eBool needCdrEngine = cAtFalse;
    AtPw acrDcrTimingSource = mMethodsGet(self)->AcrDcrTimingSource(self);
    AtPw currentAcrDcrTimingSource = mMethodsGet(self)->CurrentAcrDcrTimingSource(self);
    ThaClaPwController pwController;

    if (!mMethodsGet(self)->TimingIsSupported(self, timingMode, timingSource))
        return cAtErrorModeNotSupport;

    if (timingMode == cAtTimingModeSlave)
        {
        if (timingSource == NULL)
            return cAtErrorNullPointer;

        /* If timing source is the same, consider this as loop timing mode */
        if (timingSource == ThaCdrControllerChannelGet(self))
            timingMode = cAtTimingModeLoop;
        }

    /* In case of ACR/DCR, use input PW timing source if circuit has not been
     * bound to any PW */
    if ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr))
        {
        if (acrDcrTimingSource == NULL)
            acrDcrTimingSource = (AtPw)timingSource;
        if (acrDcrTimingSource == NULL)
            return cAtErrorNullPointer;
        }

    /* Need to disable CLA first */
    needCdrEngine = ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr)) ? cAtTrue : cAtFalse;
    pwController = ThaModuleClaPwControllerGet(claModule);
    if (currentAcrDcrTimingSource)
        {
        AtPw adapter = (AtPw)ThaPwAdapterGet(currentAcrDcrTimingSource);
        ret |= ThaClaPwControllerCdrEnable(pwController, adapter, 0, cAtFalse);
        }
    if (acrDcrTimingSource)
        {
        AtPw adapter = (AtPw)ThaPwAdapterGet(acrDcrTimingSource);
        ret |= ThaClaPwControllerCdrEnable(pwController, adapter, (uint16)ThaCdrControllerIdGet(self), needCdrEngine);
        }

    /* Apply timing mode and update packet length */
    if (needCdrEngine && acrDcrTimingSource)
        {
        ThaPwAdapter adapter = ThaPwAdapterGet(acrDcrTimingSource);
        eBool shouldEnableJA = ThaModuleCdrShouldEnableJitterAttenuator(ThaCdrControllerModuleGet(self));

        ret |= mMethodsGet(self)->TimingPacketLenInBitsSet(self, ThaPwAdapterCDRTimingPktLenInBits(adapter));
        ret |= ThaModulePwePwJitterAttenuatorEnable(ModulePwe(self), (AtPw)adapter, shouldEnableJA);
        }

    if (timingMode == cAtTimingModeFreeRunning)
        {
        mMethodsGet(self)->TimingModeSet(self, cAtTimingModeAcr, timingSource);
        AtOsalUSleep(1000);
        }

    ret |= mMethodsGet(self)->TimingModeSet(self, timingMode, timingSource);
    ret |= mMethodsGet(self)->TimingSourceSet(self, timingMode, timingSource);

    /* Configure the CDR engine */
    if (needCdrEngine)
        {
        uint8 i;
        const uint8 cResetCdrNumTimes = 5;

        for (i = 0; i < cResetCdrNumTimes; i++)
            ClockStateReset(self);
        }

    /* Some product use other timing source as system timing */
    if (OtherTimingSourceAsSystemTiming(self) != cAtTimingModeUnknown)
        self->systemTiming = (timingMode == cAtTimingModeSys) ? cAtTrue : cAtFalse;

    return ret;
    }

static eBool HasPwFeature(ThaCdrController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaCdrControllerModuleGet(self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    return pwModule ? cAtTrue : cAtFalse;
    }
    
static uint8 HwDcrTimingMode(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
	AtUnused(timingSource);
	AtUnused(self);
    if (timingMode == cAtTimingModeSys)        return 0;
    if (timingMode == cAtTimingModeLoop)       return 1;
    if (timingMode == cAtTimingModeAcr)        return 8;
    if (timingMode == cAtTimingModeDcr)        return 10;
    if (timingMode == cAtTimingModeExt1Ref)    return 4;
    if (timingMode == cAtTimingModeExt2Ref)    return 5;
    if (timingMode == cAtTimingModeSdhLineRef) return 6;
    if (timingMode == cAtTimingModeFreeRunning)return 7;

    return 0; /* System as default */
    }

static eAtTimingMode HwDcrTimingModeGet(ThaCdrController self, uint8 hwVal)
    {
    AtUnused(self);

    if (hwVal == 0)        return cAtTimingModeSys;
    if (hwVal == 1)        return cAtTimingModeLoop;
    if (hwVal == 8)        return cAtTimingModeAcr;
    if (hwVal == 10)       return cAtTimingModeDcr;
    if (hwVal == 4)        return cAtTimingModeExt1Ref;
    if (hwVal == 5)        return cAtTimingModeExt2Ref;
    if (hwVal == 6)        return cAtTimingModeSdhLineRef;
    if (hwVal == 7)        return cAtTimingModeFreeRunning;
    return cAtTimingModeUnknown; /* System as default */
    }
static uint32 CDREngineTimingCtrl(ThaCdrController self)
    {
    return ThaModuleCdrEngineTimingCtrlRegister(ThaCdrControllerModuleGet(self));
    }

static eAtRet CdrTimingModeSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    uint32 regAddr = CDREngineTimingCtrl(self) + mMethodsGet(self)->TimingCtrlOffset(self);
    uint32 regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);

    mRegFieldSet(regVal, cThaHoldvalMode, 0);
    mRegFieldSet(regVal, cThaSeqMode, 0x0);
    mRegFieldSet(regVal, cThaCDRTimeMode, HwDcrTimingMode(self, timingMode, timingSource));
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtTimingMode CdrTimingModeGet(ThaCdrController self)
    {
    eAtTimingMode timingMode;
    uint32 regAddr = CDREngineTimingCtrl(self) + mMethodsGet(self)->DefaultOffset(self);
    uint32 regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    uint8  mode =0;

    mode = mRegField(regVal, cThaCDRTimeMode);
    timingMode = HwDcrTimingModeGet(self, mode);
    return timingMode;
    }

static eAtRet TimingModeSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (HasPwFeature(self)||mMethodsGet(self)->FreeRunningIsSupported(self))
        {
        return CdrTimingModeSet(self, timingMode, timingSource);
        }
    return cAtOk;
    }

static AtChannel TimingSourceGet(ThaCdrController self)
    {
    eAtTimingMode timingMode = cAtTimingModeUnknown;

    timingMode = ThaCdrControllerTimingModeGet(self);

    if (timingMode == cAtTimingModeAcr)
        {
        if (mMethodsGet(self)->FreeRunningIsSupported(self))
            timingMode = ThaCdrControllerCdrTimingModeGet(self);
        }

    if (timingMode == cAtTimingModeSys)
        return NULL;

    if (timingMode == cAtTimingModeSlave)
        return self->timingSource;

    if (timingMode == cAtTimingModeLoop)
        return ThaCdrControllerChannelGet(self);

    if ((timingMode == cAtTimingModeAcr) ||
        (timingMode == cAtTimingModeDcr))
        {
        AtPw pw = AtChannelBoundPwGet(self->channel);
        return (AtChannel)(pw ? pw : self->pwTimingSource);
        }

    return self->timingSource;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCdrController);
    }

uint8 ThaCdrControllerHwClockMainStateGet(ThaCdrController self)
    {
    uint32 address, regVal;
    uint8 hwState;

    address = CDREngineAdjustStateStatus(self) + mMethodsGet(self)->TimingStatusOffset(self);
    regVal = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldGet(regVal, cThaAdjStateMask2, cThaAdjStateShift2, uint8, &hwState);

    return hwState;
    }

uint8 ThaCdrControllerHwClockSubStateGet(ThaCdrController self)
    {
    uint32 regVal, address;
    uint8 hwState;

    address = CDREngineAdjustStateStatus(self) + mMethodsGet(self)->TimingStatusOffset(self);
    regVal = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldGet(regVal, cThaAdjSubStateMask, cThaAdjSubStateShift, uint8, &hwState);

    return hwState;
    }

static eAtClockState ClockMainStateHw2Sw(ThaCdrController self, uint8 hwState)
    {
    AtChannel channel;

    /* Later on, on some products, hardware use different clock state transition
     * for each kind of service. So, let each kind of channel determine which
     * transition they should do */
    channel = ThaCdrControllerChannelGet(self);
    if (channel && ThaDeviceHwClockStateShouldUseNewTransition((ThaDevice)AtChannelDeviceGet(channel)))
        return AtChannelHwClockStateTranslate(channel, hwState);

    return ThaModuleCdrClockStateFromHwStateGet(ThaCdrControllerModuleGet(self), hwState);
    }

static eAtClockState ClockSubStateHw2Sw(ThaCdrController self, uint8 hwState)
    {
	AtUnused(self);
    if (hwState == 0)
        return cAtClockStateLocked;
    if ((hwState == 1) || (hwState == 3))
        return cAtClockStateHoldOver;

    return cAtClockStateLearning;
    }

static eAtClockState ClockStateGet(ThaCdrController self)
    {
    eAtTimingMode timingMode;
    eAtClockState state;

    timingMode = ThaCdrControllerTimingModeGet(self);
    if ((timingMode != cAtTimingModeAcr) && (timingMode != cAtTimingModeDcr))
        return cAtClockStateNotApplicable;

    state = ClockMainStateHw2Sw(self, ThaCdrControllerHwClockMainStateGet(self));
    if (!ThaModuleCdrClockSubStateIsSupported(ThaCdrControllerModuleGet(self)))
        return state;

    if (state == cAtClockStateLocked)
        state = ClockSubStateHw2Sw(self, ThaCdrControllerHwClockSubStateGet(self));

    return state;
    }

static uint8 HwClockStateGet(ThaCdrController self)
    {
    eAtTimingMode timingMode;

    timingMode = ThaCdrControllerTimingModeGet(self);
    if ((timingMode != cAtTimingModeAcr) && (timingMode != cAtTimingModeDcr))
        return 0;

    return ThaCdrControllerHwClockMainStateGet(self);
    }

static uint8 HwTimingModeGet(ThaCdrController self)
    {
	AtUnused(self);
    return 0;
    }

static eAtTimingMode TimingModeGet(ThaCdrController self)
    {    
    eAtTimingMode timingMode = cAtTimingModeUnknown;
    uint8 hwTimmingMode;

    hwTimmingMode = mMethodsGet(self)->HwTimingModeGet(self);
    timingMode = ThaCdrControllerTimingModeHw2Sw(self, hwTimmingMode);

    if (timingMode == cAtTimingModeAcr)
            {
            if (mMethodsGet(self)->FreeRunningIsSupported(self))
                timingMode =  ThaCdrControllerCdrTimingModeGet(self);
            }

    if ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr))
        return timingMode;

    if ((timingMode == cAtTimingModeSys) || (timingMode == cAtTimingModeLoop))
        return timingMode;

    if (ThaCdrControllerChannelGet(self) != self->timingSource)
        return cAtTimingModeSlave;

    /* Some products use other timing source as system timing */
    if ((OtherTimingSourceAsSystemTiming(self) != cAtTimingModeUnknown) &&
        (timingMode == OtherTimingSourceAsSystemTiming(self)) &&
        self->systemTiming)
        return cAtTimingModeSys;

    return timingMode;
    }

static eAtTimingMode TimingModeHw2Sw(ThaCdrController self, uint8  hwTimingMode)
    {
	AtUnused(self);
    if (hwTimingMode == 0)  return cAtTimingModeSys;
    if (hwTimingMode == 1)  return cAtTimingModeLoop;
    if (hwTimingMode == 8)  return cAtTimingModeAcr;
    if (hwTimingMode == 3)  return cAtTimingModePrc;
    if (hwTimingMode == 4)  return cAtTimingModeExt1Ref;
    if (hwTimingMode == 5)  return cAtTimingModeExt2Ref;
    if (hwTimingMode == 10) return cAtTimingModeDcr;
    if (hwTimingMode == 7)  return cAtTimingModeFreeRunning;
    return cAtTimingModeUnknown;
    }

static uint8 HwSystemTimingMode(ThaCdrController self)
    {
    ThaModuleCdr cdrModule = ThaCdrControllerModuleGet(self);
    return ThaModuleCdrHwSystemTimingMode(cdrModule);
    }

static uint8 TimingModeSw2Hw(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
	AtUnused(timingSource);
    if (timingMode == cAtTimingModeSys)     return mMethodsGet(self)->HwSystemTimingMode(self);
    if (timingMode == cAtTimingModeLoop)    return 1;
    if (timingMode == cAtTimingModeAcr)     return 8;
    if (timingMode == cAtTimingModeDcr)     return 10;
    if (timingMode == cAtTimingModeSlave)   return 1;
    if (timingMode == cAtTimingModePrc)     return 3;
    if (timingMode == cAtTimingModeExt1Ref) return 4;
    if (timingMode == cAtTimingModeExt2Ref) return 5;
    if (timingMode == cAtTimingModeFreeRunning) return 7;

    return 0;
    }

static eAtRet RxNcoPackLenSet(ThaCdrController self, uint32 packetLength)
    {
	AtUnused(packetLength);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet TimingPacketLenInBitsSet(ThaCdrController self, uint32 packetLengthInBits)
    {
    uint32 regAddr = CDREngineTimingCtrl(self) + mMethodsGet(self)->TimingCtrlOffset(self);
    uint32 regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);

    mFieldIns(&regVal, cThaPktLenExtenMask, cThaPktLenExtenShift, ((packetLengthInBits & cBit16) >> 16));
    mFieldIns(&regVal, cThaPktLenMask, cThaPktLenShift, packetLengthInBits);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet CwSequenceModeSet(ThaCdrController self, eAtPwCwSequenceMode sequenceMode)
    {
	AtUnused(sequenceMode);
	AtUnused(self);
    return cAtOk;
    }

static eAtPwCwSequenceMode CwSequenceModeGet(ThaCdrController self)
    {
	AtUnused(self);
    return cAtPwCwSequenceModeInvalid;
    }

static void Debug(ThaCdrController self)
    {
    ThaCdrDebuggerDebug(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), self);
    }

static eAtRet Init(ThaCdrController self)
    {
    return mMethodsGet(self)->TimingSet(self, cAtTimingModeSys, ThaCdrControllerChannelGet(self));
    }

static eAtRet CdrIdAssign(ThaCdrController self)
    {
    /* Some sub class may want to override this assignment */
    AtUnused(self);
    return cAtOk;
    }

static uint8 ChannelSliceGet(ThaCdrController self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return 0;
    }

static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    AtUnused(self);
    /* Concrete channel will handle this */
    return cAtOk;
    }

static const char *ToString(AtObject self)
    {
    static char buf[128];
    AtChannel channel = ThaCdrControllerChannelGet((ThaCdrController)self);
    AtDevice dev = AtChannelDeviceGet(channel);

    AtSnprintf(buf,
               sizeof(buf), "%scdr_controller_%d (%s.%s)",
               AtDeviceIdToString(dev),
               ThaCdrControllerIdGet((ThaCdrController)self),
               AtChannelTypeString(channel),
               AtChannelIdString(channel));

    return buf;
    }

static void RegsShow(ThaCdrController self)
    {
    AtUnused(self);
    /* Concrete channel will handle this */
    }

static int32 PpmCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return 0;
    }

static void PwHeaderDebug(ThaCdrController self, AtPw pw)
    {
    ThaCdrDebuggerPwHeaderDebug(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), self, pw);
    }

static uint32 InterruptEnableRegister(AtInterruptManager manager)
    {
    ThaInterruptManager thaManager = (ThaInterruptManager)manager;
    return ThaInterruptManagerInterruptEnableRegister(thaManager) + ThaInterruptManagerBaseAddress(thaManager);
    }

static eAtRet InterruptMaskSet(ThaCdrController self, uint32 defectMask, uint32 enableMask)
    {
    AtChannel tdmChannel = ThaCdrControllerChannelGet(self);
    uint32 moduleId = AtModuleTypeGet(AtChannelModuleGet(tdmChannel));
    uint32 tdmChannelDefectMask = mMethodsGet(self)->ChannelInterruptMask(self);
    AtInterruptManager intrManager = mMethodsGet(self)->InterruptManager(self);
    uint32 regAddr = InterruptEnableRegister(intrManager) + ThaCdrControllerInterruptOffsetGet(self);
    uint32 regVal;
    uint32 channelMask  = ThaModuleCdrCDREngineUnlockedIntrMask(self);

    if (defectMask & tdmChannelDefectMask)
        {
        regVal = mChannelHwRead(tdmChannel, regAddr, moduleId);
        if (enableMask & tdmChannelDefectMask)
            regVal |= channelMask;
        else
            regVal &= (uint32)(~channelMask);
        mChannelHwWrite(tdmChannel, regAddr, regVal, moduleId);
        }

    return cAtOk;
    }

static uint32 InterruptMaskGet(ThaCdrController self)
    {
    AtChannel tdmChannel = ThaCdrControllerChannelGet(self);
    uint32 moduleId = AtModuleTypeGet(AtChannelModuleGet(tdmChannel));
    AtInterruptManager intrManager = mMethodsGet(self)->InterruptManager(self);
    uint32 regAddr = InterruptEnableRegister(intrManager) + ThaCdrControllerInterruptOffsetGet(self);
    uint32 regVal  = mChannelHwRead(tdmChannel, regAddr, moduleId);
    uint32 tdmChannelDefectMask = 0;
    uint32 fieldMask  = ThaModuleCdrCDREngineUnlockedIntrMask(self);

    if (regVal & fieldMask)
        tdmChannelDefectMask |= mMethodsGet(self)->ChannelInterruptMask(self);

    return tdmChannelDefectMask;
    }

static uint32 InterruptStatusRegister(AtInterruptManager intrManager)
    {
    ThaInterruptManager thaManager = (ThaInterruptManager)intrManager;
    return ThaInterruptManagerInterruptStatusRegister(thaManager) + ThaInterruptManagerBaseAddress(thaManager);
    }

static uint32 InterruptRead2Clear(ThaCdrController self, eBool read2Clear)
    {
    AtChannel tdmChannel = ThaCdrControllerChannelGet(self);
    uint32 moduleId = AtModuleTypeGet(AtChannelModuleGet(tdmChannel));
    AtInterruptManager intrManager = mMethodsGet(self)->InterruptManager(self);
    uint32 regAddr = InterruptStatusRegister(intrManager) + ThaCdrControllerInterruptOffsetGet(self);
    uint32 regVal;
    uint32 tdmChannelDefectMask = 0;
    uint32 channelMask = ThaModuleCdrCDREngineUnlockedIntrMask(self);

    regVal  = mChannelHwRead(tdmChannel, regAddr, moduleId);
    if (regVal & channelMask)
        tdmChannelDefectMask |= mMethodsGet(self)->ChannelInterruptMask(self);

    if (read2Clear)
        mChannelHwWrite(tdmChannel, regAddr, channelMask, moduleId);

    return tdmChannelDefectMask;
    }

static void InterruptProcess(ThaCdrController self, uint32 offset)
    {
    AtChannel tdmChannel = ThaCdrControllerChannelGet(self);
    AtChannel reportChannel = ThaCdrControllerReportChannelGet(self);
    uint32 moduleId = AtModuleTypeGet(AtChannelModuleGet(tdmChannel));
    AtInterruptManager intrManager = mMethodsGet(self)->InterruptManager(self);
    uint32 regIntrAddr = InterruptStatusRegister(intrManager) + offset;
    uint32 regMaskAddr = InterruptEnableRegister(intrManager) + offset;
    uint32 regIntrVal  = mChannelHwRead(tdmChannel, regIntrAddr, moduleId);
    uint32 channelMask = ThaModuleCdrCDREngineUnlockedIntrMask(self);
    uint32 regMaskVal  = mChannelHwRead(tdmChannel, regMaskAddr, moduleId);
    uint32 events = 0;

    regIntrVal &= regMaskVal;

    if (regIntrVal & channelMask)
        {
        events |= mMethodsGet(self)->ChannelInterruptMask(self);

        /* Clear interrupt. */
        mChannelHwWrite(tdmChannel, regIntrAddr, channelMask, moduleId);

        AtChannelAllAlarmListenersCall(reportChannel, events, 0);
        }
    }

static uint32 ChannelInterruptMask(ThaCdrController self)
    {
    AtUnused(self);
    /* Return clock-state-change mask specified on channel type. */
    return 0;
    }

static AtModule CdrModule(ThaCdrController self)
    {
    return (AtModule)ThaCdrControllerModuleGet(self);
    }

static AtInterruptManager InterruptManager(ThaCdrController self)
    {
    return AtModuleInterruptManagerAtIndexGet(CdrModule(self), 0);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaCdrController object = (ThaCdrController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(channel);
    mEncodeObjectDescription(timingSource);
    mEncodeObjectDescription(pwTimingSource);
    mEncodeUInt(controllerId);
    mEncodeUInt(systemTiming);
    }

static eBool NeedToUpdateTiming(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtTimingMode currentTimingMode;
    AtChannel currentTimingSource;

    currentTimingMode = ThaCdrControllerTimingModeGet(self);
    currentTimingSource = ThaCdrControllerTimingSourceGet(self);

    if (currentTimingMode != timingMode)
        return cAtTrue;

    if ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr))
        {
        ThaModuleCla claModule = (ThaModuleCla)AtDeviceModuleGet(DeviceGet(self), cThaModuleCla);
        ThaClaPwController pwController = ThaModuleClaPwControllerGet(claModule);
        AtPw adapter;

        if (timingSource && (AtPwTypeGet((AtPw)timingSource) == cAtPwTypeCESoP))
            adapter = (AtPw)ThaPwAdapterGet((AtPw)timingSource);
        else
            adapter = (AtPw)ThaPwAdapterGet(AtChannelBoundPwGet(ThaCdrControllerChannelGet(self)));

        if (adapter == NULL)
            {
            AtDeviceLog(DeviceGet(self), cAtLogLevelCritical, AtSourceLocation, "CDR %s gets NULL PW adapter\r\n", AtObjectToString((AtObject)self));
            return cAtFalse;
            }

        if (!ThaClaPwControllerCdrIsEnabled(pwController, adapter))
            return cAtTrue;

        if (timingSource == NULL)
            return cAtFalse;
        }

    if (timingSource != currentTimingSource)
        return cAtTrue;

    return cAtFalse;
    }

static eBool NeedToDisableEpar(ThaCdrController self, eAtTimingMode timingMode)
    {
    ThaModulePw modulePw;
    eAtTimingMode currentTimingMode;
    AtChannel tdmChannel = ThaCdrControllerChannelGet(self);
    AtChannel pw = (AtChannel) AtChannelBoundPwGet(tdmChannel);

    if (pw == NULL)
        return cAtFalse;

    if (AtPwTypeGet((AtPw)pw) != cAtPwTypeCEP)
        return cAtFalse;

    /* Current timing mode is not applicable for EPAR, EPAR was not enabled in
     * this case */
    currentTimingMode = ThaCdrControllerTimingModeGet(self);
    modulePw = (ThaModulePw) AtChannelModuleGet(pw);
    if (!ThaModulePwTimingModeIsApplicableForEPAR(modulePw, currentTimingMode))
        return cAtFalse;

    /* If new timing mode is not applicable for EPAR while EPAR might be enabled
     * before, it should be disabled. */
    if (ThaModulePwTimingModeIsApplicableForEPAR(modulePw, timingMode))
        return cAtFalse;

    /* Also do not need to touch EPAR if it has not been enabled before */
    return AtPwCepEparIsEnabled((AtPwCep)pw);
    }

static eAtRet DisableEpar(ThaCdrController self)
    {
    AtChannel tdmChannel = ThaCdrControllerChannelGet(self);
    AtChannel pw = (AtChannel) AtChannelBoundPwGet(tdmChannel);
    return AtPwCepEparEnable((AtPwCep) pw, cAtFalse);
    }

static int32 PpbCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return 0;
    }

static eBool IsAcrDcrTimingMode(uint8 timingMode)
    {
    if (timingMode == cAtTimingModeAcr || timingMode == cAtTimingModeDcr)
        return cAtTrue;
    return cAtFalse;
    }

static eBool CanSupportInterrupt(ThaCdrController self)
    {
    eAtTimingMode timingMode = ThaCdrControllerTimingModeGet(self);

    if (!ThaModuleCdrInterruptIsSupported(ThaCdrControllerModuleGet(self)))
        return cAtFalse;

    if (IsAcrDcrTimingMode(timingMode))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanOnlyUseFixedTiming(ThaCdrController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtTimingMode FixedTimingMode(ThaCdrController self)
    {
    AtUnused(self);
    return cAtTimingModeSys;
    }

static eBool CanSpecifyFixedTimingMode(ThaCdrController self, eAtTimingMode timingMode)
    {
    return (timingMode == mMethodsGet(self)->FixedTimingMode(self)) ? cAtTrue : cAtFalse;
    }

static eBool FreeRunningIsSupported(ThaCdrController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ControllerIdGet(ThaCdrController self)
    {
    return self->controllerId;
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlDefaultOffset(self);
    }

static uint32 TimingStatusOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingStateDefaultOffset(self);
    }

static AtChannel ChannelGet(ThaCdrController self)
    {
    return self->channel;
    }

static AtChannel ReportChannelGet(ThaCdrController self)
    {
    return ThaCdrControllerChannelGet(self);
    }

static void OverrideAtObject(ThaCdrController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, ChannelGet);
        mMethodOverride(m_methods, ReportChannelGet);
        mMethodOverride(m_methods, PartGet);
        mMethodOverride(m_methods, TimingSourceSet);
        mMethodOverride(m_methods, TimingSourceGet);
        mMethodOverride(m_methods, TimingSet);
        mMethodOverride(m_methods, TimingModeGet);
        mMethodOverride(m_methods, TimingModeSet);
        mMethodOverride(m_methods, CurrentAcrDcrTimingSource);
        mMethodOverride(m_methods, AcrDcrTimingSource);
        mMethodOverride(m_methods, ClockStateGet);
        mMethodOverride(m_methods, HwClockStateGet);
        mMethodOverride(m_methods, HwTimingModeGet);
        mMethodOverride(m_methods, TimingModeIsSupported);
        mMethodOverride(m_methods, TimingIsSupported);
        mMethodOverride(m_methods, ChannelDefaultOffset);
        mMethodOverride(m_methods, ChannelIdFlat);
        mMethodOverride(m_methods, TimingModeSw2Hw);
        mMethodOverride(m_methods, TimingModeHw2Sw);
        mMethodOverride(m_methods, RxNcoPackLenSet);
        mMethodOverride(m_methods, TimingPacketLenInBitsSet);
        mMethodOverride(m_methods, CwSequenceModeSet);
        mMethodOverride(m_methods, CwSequenceModeGet);
        mMethodOverride(m_methods, TimingSourceForConfiguration);
        mMethodOverride(m_methods, TimingStatusOffset);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, HwSystemTimingMode);
        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, CdrIdAssign);
        mMethodOverride(m_methods, ChannelSliceGet);
        mMethodOverride(m_methods, ChannelMapTypeSet);
        mMethodOverride(m_methods, RegsShow);
        mMethodOverride(m_methods, PpmCalculate);
        mMethodOverride(m_methods, PpbCalculate);
        mMethodOverride(m_methods, PwHeaderDebug);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, InterruptRead2Clear);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, ChannelInterruptMask);
        mMethodOverride(m_methods, InterruptManager);
        mMethodOverride(m_methods, CanOnlyUseFixedTiming);
        mMethodOverride(m_methods, FixedTimingMode);
        mMethodOverride(m_methods, CanSpecifyFixedTimingMode);
        mMethodOverride(m_methods, FreeRunningIsSupported);
        mMethodOverride(m_methods, ControllerIdGet);
        mMethodOverride(m_methods, TimingCtrlOffset);
        }

    mMethodsSet(self, &m_methods);
    }

ThaCdrController ThaCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->controllerId = engineId;
    self->channel      = channel;

    return self;
    }

uint32 ThaCdrControllerRead(ThaCdrController self, uint32 address, eAtModule moduleId)
    {
    if (mControllerIsValid(self))
        return mChannelHwRead(ThaCdrControllerChannelGet(self), address, moduleId);

    return 0xdeadcafe;
    }

void ThaCdrControllerWrite(ThaCdrController self, uint32 address, uint32 value, eAtModule moduleId)
    {
    if (mControllerIsValid(self))
        mChannelHwWrite(ThaCdrControllerChannelGet(self), address, value, moduleId);
    }

uint8 ThaCdrControllerTimingModeSw2Hw(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->TimingModeSw2Hw(self, timingMode, timingSource);
    return 0;
    }

eAtTimingMode ThaCdrControllerTimingModeHw2Sw(ThaCdrController self, uint8 hwTimingMode)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->TimingModeHw2Sw(self, hwTimingMode);
    return cAtTimingModeUnknown;
    }

AtChannel ThaCdrControllerChannelGet(ThaCdrController self)
    {
    if (self)
        return mMethodsGet(self)->ChannelGet(self);
    return NULL;
    }

AtChannel ThaCdrControllerReportChannelGet(ThaCdrController self)
    {
    if (self)
        return mMethodsGet(self)->ReportChannelGet(self);
    return NULL;
    }

uint32 ThaCdrControllerIdGet(ThaCdrController self)
    {
    if (self)
        return mMethodsGet(self)->ControllerIdGet(self);
    return  0xdeadcafe;
    }

eBool ThaCdrControllerTimingModeIsSupported(ThaCdrController self, eAtTimingMode timingMode)
    {
    if (self)
        return mMethodsGet(self)->TimingModeIsSupported(self, timingMode);
    return cAtFalse;
    }

eAtRet ThaCdrControllerTimingSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    eBool needDisabledEpar;

    if (!mControllerIsValid(self))
        return cAtError;

    ret = ThaCdrControllerChannelMapTypeSet(self);
    if (ret != cAtOk)
        return ret;

    /* Clear clock state interrupt mask for safe operation. */
    if (IsAcrDcrTimingMode(ThaCdrControllerTimingModeGet(self)) && !IsAcrDcrTimingMode(timingMode))
        ThaCdrControllerInterruptMaskSet(self, mMethodsGet(self)->ChannelInterruptMask(self), 0);

    if (AtChannelShouldPreventReconfigure(ThaCdrControllerChannelGet(self)) && !NeedToUpdateTiming(self, timingMode, timingSource))
        return cAtOk;

    needDisabledEpar = NeedToDisableEpar(self, timingMode);
    ret |= mMethodsGet(self)->TimingSet(self, timingMode, timingSource);
    if (needDisabledEpar)
        ret |= DisableEpar(self);

    return ret;
    }

eAtTimingMode ThaCdrControllerTimingModeGet(ThaCdrController self)
    {
    if (!mControllerIsValid(self))
        return cAtTimingModeUnknown;

    /* Cannot access hardware in standby mode. Need help from the channel for
     * correct timing mode which is cached there */
    if (ThaCdrControllerInAccessible(self))
        {
        AtChannel channel = ThaCdrControllerChannelGet(self);
        return AtChannelTimingModeGet(channel);
        }

    return mMethodsGet(self)->TimingModeGet(self);
    }

AtChannel ThaCdrControllerTimingSourceGet(ThaCdrController self)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->TimingSourceGet(self);
    return NULL;
    }

void ThaCdrControllerTimingSourceSave(ThaCdrController self, AtChannel timingSource)
    {
    if (self)
        self->timingSource = timingSource;
    }

uint8 ThaCdrControllerHwClockStateGet(ThaCdrController self)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->HwClockStateGet(self);
    return 0;
    }

eAtClockState ThaCdrControllerClockStateGet(ThaCdrController self)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->ClockStateGet(self);
    return cAtClockStateUnknown;
    }

eAtRet ThaCdrControllerRxNcoPackLenSet(ThaCdrController self, uint32 packetLength)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->RxNcoPackLenSet(self, packetLength);
    return cAtError;
    }

eAtRet ThaCdrControllerTimingPacketLenSet(ThaCdrController self, uint32 packetLengthInBits)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->TimingPacketLenInBitsSet(self, packetLengthInBits);
    return cAtError;
    }

eAtRet ThaCdrControllerCwSequenceModeSet(ThaCdrController self, eAtPwCwSequenceMode sequenceMode)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->CwSequenceModeSet(self, sequenceMode);
    return cAtError;
    }

eAtPwCwSequenceMode ThaCdrControllerCwSequenceModeGet(ThaCdrController self)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->CwSequenceModeGet(self);
    return cAtPwCwSequenceModeDisable;
    }

ThaModuleCdr ThaCdrControllerModuleGet(ThaCdrController self)
    {
    AtDevice device = AtChannelDeviceGet(ThaCdrControllerChannelGet(self));
    return (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    }

uint32 ThaCdrControllerPartOffset(ThaCdrController self)
    {
    return ThaDeviceModulePartOffset((ThaDevice)DeviceGet(self), cThaModuleCdr, (uint8)(mMethodsGet(self)->PartGet(self)));
    }

void ThaCdrControllerDebug(ThaCdrController self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

void ThaCdrControllerPwHeaderDebug(ThaCdrController self, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->PwHeaderDebug(self, pw);
    }

uint32 ThaCdrControllerCDREngineTimingCtrl(ThaCdrController self)
    {
    return CDREngineTimingCtrl(self);
    }

uint32 ThaCdrControllerChannelIdFlat(ThaCdrController self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->ChannelIdFlat(self, channel);
    return 0;
    }

uint8 ThaCdrControllerChannelSliceGet(ThaCdrController self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->ChannelSliceGet(self, channel);
    return 0;
    }

uint16 ThaCdrControllerLongRead(ThaCdrController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen, eAtModule moduleId)
    {
    if (self)
        return mChannelHwLongRead(ThaCdrControllerChannelGet(self), address, dataBuffer, bufferLen, moduleId);

    return 0x0;
    }

void ThaCdrControllerLongWrite(ThaCdrController self, uint32 address, uint32 *dataBuffer, uint16 bufferLen, eAtModule moduleId)
    {
    if (self)
        mChannelHwLongWrite(ThaCdrControllerChannelGet(self), address, dataBuffer, bufferLen, moduleId);
    }

eAtRet ThaCdrControllerChannelMapTypeSet(ThaCdrController self)
    {
    if (self)
        return mMethodsGet(self)->ChannelMapTypeSet(self);

    return cAtErrorNullPointer;
    }

eAtRet ThaCdrControllerInit(ThaCdrController self)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }
    
void ThaCdrControllerRegsShow(ThaCdrController self)
    {
    if (mControllerIsValid(self))
        mMethodsGet(self)->RegsShow(self);
    }

int32 ThaCdrControllerPpmCalculate(ThaCdrController self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->PpmCalculate(self, ncoValue);
    return 0;
    }

int32 ThaCdrControllerPpbCalculate(ThaCdrController self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->PpbCalculate(self, ncoValue);
    return 0;
    }

eBool ThaCdrControllerInterruptMaskIsSupported(ThaCdrController self, uint32 defectMask)
    {
    AtUnused(defectMask);

    if (self == NULL)
        return cAtFalse;

    if (!ThaModuleCdrInterruptIsSupported(ThaCdrControllerModuleGet(self)))
        return cAtFalse;

    if (IsAcrDcrTimingMode(ThaCdrControllerTimingModeGet(self)))
        return cAtTrue;

    return cAtFalse;
    }

eAtRet ThaCdrControllerInterruptMaskSet(ThaCdrController self, uint32 defectMask, uint32 enableMask)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ThaCdrControllerInterruptMaskIsSupported(self, defectMask))
        return mMethodsGet(self)->InterruptMaskSet(self, defectMask, enableMask);

    return cAtErrorModeNotSupport;
    }

uint32 ThaCdrControllerInterruptMaskGet(ThaCdrController self)
    {
    if (!CanSupportInterrupt(self))
        return 0;

    if (self)
        return mMethodsGet(self)->InterruptMaskGet(self);
        
    return 0;
    }

uint32 ThaCdrControllerInterruptRead2Clear(ThaCdrController self, eBool read2Clear)
    {
    if (!CanSupportInterrupt(self))
        return 0;

    if (self)
        return mMethodsGet(self)->InterruptRead2Clear(self, read2Clear);
        
    return 0;
    }

void ThaCdrControllerInterruptProcess(ThaCdrController self, uint32 offset)
    {
    /* Do not need backward here because this code will be not executed in
     * interrupt process context if RTL does not support. */
    if (self)
        mMethodsGet(self)->InterruptProcess(self, offset);
    }

uint32 ThaCdrControllerInterruptOffsetGet(ThaCdrController self)
    {
    ThaCdrInterruptManager manager = (ThaCdrInterruptManager)mMethodsGet(self)->InterruptManager(self);
    if (manager)
        return mMethodsGet(manager)->InterruptOffset(manager, self);

    return cBit31_0;
    }

uint8 ThaCdrControllerTxBitRate(ThaCdrController self)
    {
    if (self)
        return mMethodsGet(self)->TxBitRate(self);
    return 0;
    }

uint32 ThaCdrControllerDefaultOffset(ThaCdrController self)
    {
    if (self)
        return mMethodsGet(self)->DefaultOffset(self);

    return cBit31_0;
    }

void ThaCdrControllerPwTimingSourceSet(ThaCdrController self, AtPw pw)
    {
    eAtTimingMode timingMode;
    if (self == NULL)
        return;

    timingMode =  mMethodsGet(self)->TimingModeGet(self);
    if (IsAcrDcrTimingMode(timingMode))
        {
        self->pwTimingSource = pw;
        ThaPwAdapterAcrDcrRefCircuitSet(ThaPwAdapterGet(pw), ThaCdrControllerChannelGet(self));
        }
    }

eAtTimingMode ThaCdrControllerCdrTimingModeGet(ThaCdrController self)
    {
    if (self != NULL)
        return CdrTimingModeGet(self);
    return cAtTimingModeUnknown;
    }

eBool ThaCdrControllerInAccessible(ThaCdrController self)
    {
    if (self)
        return AtModuleInAccessible((AtModule)ThaCdrControllerModuleGet(self));
    return cAtFalse;
    }

eAtRet ThaCdrControllerTimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (self)
        return mMethodsGet(self)->TimingSourceSet(self, timingMode, timingSource);
    return cAtErrorObjectNotExist;
    }

uint32 ThaCdrControllerTimingStatusOffset(ThaCdrController self)
    {
    if (self)
        return mMethodsGet(self)->TimingStatusOffset(self);
    return cBit31_0;
    }

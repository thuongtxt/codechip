/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaVcDe1CdrController.c
 *
 * Created Date: Apr 25, 2013
 *
 * Description : VC's DE1 CDR controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaVcDe1CdrControllerInternal.h"
#include "../../map/ThaModuleStmMapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaVcDe1CdrController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaVcDe1CdrControllerMethods m_methods;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;
static tThaVc1xCdrControllerMethods       m_ThaVc1xCdrControllerOverride;

/* Save super implementation */
static const tThaCdrControllerMethods     *m_ThaCdrControllerMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaVcDe1CdrController);
    }

static AtSdhChannel SdhChannel(ThaSdhChannelCdrController self, AtChannel channel)
    {
	AtUnused(self);
    return (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)channel);
    }

static uint8 MappingMode(ThaVc1xCdrController self)
    {
	AtUnused(self);
    return 1;
    }

static eAtRet TimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;

    ret |= m_ThaCdrControllerMethods->TimingSourceSet(self, timingMode, timingSource);
    ret |= ThaDe1CdrControllerTimingSourceSave(self, timingMode, timingSource);

    return ret;
    }

static uint8 Vc1xDefaultTiming(ThaVcDe1CdrController self)
    {
    AtChannel channel = ThaCdrControllerChannelGet((ThaCdrController)self);
    AtDevice device   = AtChannelDeviceGet(channel);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrHwSystemTimingMode(cdrModule);
    }

static uint8 VtHwTimingMode(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
	AtUnused(timingSource);
	AtUnused(timingMode);
    return mMethodsGet(mThis(self))->Vc1xDefaultTiming(mThis(self));
    }

static uint8 De1HwTimingMode(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    return ThaCdrControllerTimingModeSw2Hw((ThaCdrController)self, timingMode, timingSource);
    }

static uint8 HwTimingModeGet(ThaCdrController self)
    {
    uint32 address = ThaVc1xCdrControllerCDRVtTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    uint32 value = ThaCdrControllerRead(self, address, cThaModuleCdr);

    return (uint8)mRegField(value, cThaDE1TimeMode);
    }

static AtPw CurrentAcrDcrTimingSource(ThaCdrController self)
    {
    return ThaDe1CdrControllerCurrentAcrDcrTimingSource(self);
    }

static uint8 TxBitRate(ThaCdrController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    return AtPdhDe1IsE1(de1) ? 1 : 0;
    }

static ThaCdrController TimingSourceCdrController(ThaVc1xCdrController self, AtChannel timingSource)
    {
	AtUnused(self);
    return ThaPdhDe1CdrControllerGet((ThaPdhDe1)timingSource);
    }

static eBool TimingIsSupported(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (!ThaDe1CdrControllerAcrDcrTimingConfigurationIsValid(self, timingMode, timingSource))
        return cAtFalse;

    /* Let super do other cases */
    return m_ThaCdrControllerMethods->TimingIsSupported(self, timingMode, timingSource);
    }

static eBool TimingIsMaster(ThaCdrController self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet(ThaCdrControllerChannelGet(self)), cThaModuleMap);
    AtPdhChannel de1 = (AtPdhChannel)ThaCdrControllerChannelGet(self);
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);
    uint8 vtgId;
    uint8 vtId;
    uint32 offset;
    uint32 regAddr;
    uint32 regVal;

    if (sdhChannel)
        {
        vtgId = AtSdhChannelTug2Get(sdhChannel);
        vtId = AtSdhChannelTu1xGet(sdhChannel);
        offset = ThaSdhChannelCdrControllerMapLineOffset((ThaSdhChannelCdrController)self, (AtChannel)sdhChannel, AtSdhChannelSts1Get(sdhChannel), vtgId, vtId);
        }
    else
        offset = ThaModuleStmMapPdhDe2De1MapLineOffset((ThaModuleStmMap)mapModule, (AtPdhDe1)de1);


    regAddr = ThaModuleStmMapMapLineCtrl((ThaModuleStmMap)mapModule, sdhChannel) + offset;
    regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleMap);

    return (regVal & ThaModuleMapMapTimeSrcMastMask(mapModule, sdhChannel)) ? cAtTrue : cAtFalse;
    }

static eAtTimingMode TimingModeGet(ThaCdrController self)
    {
    if ((!TimingIsMaster(self)) && (ThaCdrControllerChannelGet(self) != self->timingSource))
        return cAtTimingModeSlave;

    return m_ThaCdrControllerMethods->TimingModeGet(self);
    }

static void RegsShow(ThaCdrController self)
    {
    AtSdhChannel vc1x = ThaSdhChannelCdrControllerSdhChannel((ThaSdhChannelCdrController)self, ThaCdrControllerChannelGet(self));
    if (vc1x)
        ThaModuleCdrSdhChannelRegsShow(vc1x);
    }

static int32 PpmCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    if (AtPdhDe1IsE1(de1))
        return ThaCdrDebuggerE1PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerDs1PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static int32 PpbCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    if (AtPdhDe1IsE1(de1))
        return ThaCdrDebuggerE1PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerDs1PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static uint32 ChannelInterruptMask(ThaCdrController self)
    {
    AtUnused(self);
    return cAtPdhDe1AlarmClockStateChange;
    }

static uint32 HwTimingModeMask(ThaVc1xCdrController self)
    {
    AtUnused(self);
    return cThaDE1TimeModeMask;
    }

static uint32 HwTimingModeShift(ThaVc1xCdrController self)
    {
    AtUnused(self);
    return cThaDE1TimeModeShift;
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlVcDe1Offset(self);
    }

static uint32 TimingStatusOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingStateVcDe1Offset(self);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, TimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingSourceSet);
        mMethodOverride(m_ThaCdrControllerOverride, HwTimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, CurrentAcrDcrTimingSource);
        mMethodOverride(m_ThaCdrControllerOverride, TimingIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, RegsShow);
        mMethodOverride(m_ThaCdrControllerOverride, PpmCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, PpbCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelInterruptMask);
        mMethodOverride(m_ThaCdrControllerOverride, TxBitRate);
        mMethodOverride(m_ThaCdrControllerOverride, TimingCtrlOffset);
        mMethodOverride(m_ThaCdrControllerOverride, TimingStatusOffset);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void OverrideThaVc1xCdrController(ThaCdrController self)
    {
    ThaVc1xCdrController controler = (ThaVc1xCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVc1xCdrControllerOverride, mMethodsGet(controler), sizeof(m_ThaVc1xCdrControllerOverride));

        mMethodOverride(m_ThaVc1xCdrControllerOverride, MappingMode);
        mMethodOverride(m_ThaVc1xCdrControllerOverride, VtHwTimingMode);
        mMethodOverride(m_ThaVc1xCdrControllerOverride, De1HwTimingMode);
        mMethodOverride(m_ThaVc1xCdrControllerOverride, TimingSourceCdrController);
        mMethodOverride(m_ThaVc1xCdrControllerOverride, HwTimingModeMask);
        mMethodOverride(m_ThaVc1xCdrControllerOverride, HwTimingModeShift);
        }

    mMethodsSet(controler, &m_ThaVc1xCdrControllerOverride);
    }

static void OverrideThaSdhChannelCdrController(ThaCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, mMethodsGet(controller), sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, SdhChannel);
        }

    mMethodsSet(controller, &m_ThaSdhChannelCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    OverrideThaSdhChannelCdrController(self);
    OverrideThaVc1xCdrController(self);
    }

static void MethodsInit(ThaVcDe1CdrController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Vc1xDefaultTiming);
        }

    mMethodsSet(self, &m_methods);
    }

ThaCdrController ThaVcDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVc1xCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaCdrController ThaVcDe1CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaVcDe1CdrControllerObjectInit(newController, engineId, channel);
    }

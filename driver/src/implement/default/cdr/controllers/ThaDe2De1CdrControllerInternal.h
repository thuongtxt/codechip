/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaDe2De1CdrControllerInternal.h
 * 
 * Created Date: May 24, 2017
 *
 * Description : DE2's DE1 CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADE2DE1CDRCONTROLLERINTERNAL_H_
#define _THADE2DE1CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaVcDe1CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDe2De1CdrController * ThaDe2De1CdrController;

typedef struct tThaDe2De1CdrController
    {
    tThaVcDe1CdrController super;
    }tThaDe2De1CdrController;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaDe2De1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THADE2DE1CDRCONTROLLERINTERNAL_H_ */


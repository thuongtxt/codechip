/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaVc1xCdrControllerInternal.h
 * 
 * Created Date: Apr 25, 2013
 *
 * Description : VC1x CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVC1XCDRCONTROLLERINTERNAL_H_
#define _THAVC1XCDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaSdhChannelCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVc1xCdrController * ThaVc1xCdrController;

typedef struct tThaVc1xCdrControllerMethods
    {
    uint8 (*MappingMode)(ThaVc1xCdrController self);
    uint8 (*VtHwTimingMode)(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    uint8 (*De1HwTimingMode)(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
    ThaCdrController (*TimingSourceCdrController)(ThaVc1xCdrController self, AtChannel timingSource);
    eBool (*VtAsyncMapIsSupported)(ThaVc1xCdrController self);
    uint32 (*HwTimingModeMask)(ThaVc1xCdrController self);
    uint32 (*HwTimingModeShift)(ThaVc1xCdrController self);
    }tThaVc1xCdrControllerMethods;

typedef struct tThaVc1xCdrController
    {
    tThaSdhChannelCdrController super;
    const tThaVc1xCdrControllerMethods *methods;

    /* Private data */
    }tThaVc1xCdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaVc1xCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
uint32 ThaVc1xCdrControllerCDRVtTimeCtrl(ThaCdrController self);

#ifdef __cplusplus
}
#endif
#endif /* _THAVC1XCDRCONTROLLERINTERNAL_H_ */


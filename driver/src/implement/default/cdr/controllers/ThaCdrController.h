/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaCdrController.h
 * 
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACDRCONTROLLER_H_
#define _THACDRCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

#define mControllerIsValid(self)    ((self == NULL) ? cAtFalse : cAtTrue)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCdrController           * ThaCdrController;
typedef struct tThaDe1CdrController        * ThaDe1CdrController;
typedef struct tThaSdhChannelCdrController * ThaSdhChannelCdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete controller */
ThaCdrController ThaHoVcCdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController ThaTu3VcCdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController ThaVc1xCdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController ThaDe1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController ThaDe2De1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController ThaVcDe1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController ThaVcDe3CdrControllerNew(uint32 engineId, AtChannel channel);

AtChannel ThaCdrControllerChannelGet(ThaCdrController self);
AtChannel ThaCdrControllerReportChannelGet(ThaCdrController self);
uint32 ThaCdrControllerIdGet(ThaCdrController self);
uint8 ThaCdrControllerTxBitRate(ThaCdrController self);
uint32 ThaCdrControllerDefaultOffset(ThaCdrController self);
void ThaCdrControllerPwTimingSourceSet(ThaCdrController self, AtPw pw);
eBool ThaCdrControllerInAccessible(ThaCdrController self);

eBool ThaCdrControllerTimingModeIsSupported(ThaCdrController self, eAtTimingMode timingMode);
eAtRet ThaCdrControllerTimingSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
eAtTimingMode ThaCdrControllerTimingModeGet(ThaCdrController self);
AtChannel ThaCdrControllerTimingSourceGet(ThaCdrController self);
void ThaCdrControllerTimingSourceSave(ThaCdrController self, AtChannel timingSource);
eAtClockState ThaCdrControllerClockStateGet(ThaCdrController self);
uint8 ThaCdrControllerHwClockStateGet(ThaCdrController self);
uint8 ThaCdrControllerHwClockMainStateGet(ThaCdrController self);
uint8 ThaCdrControllerHwClockSubStateGet(ThaCdrController self);

eAtRet ThaCdrControllerRxNcoPackLenSet(ThaCdrController self, uint32 packetLength);
eAtRet ThaCdrControllerTimingPacketLenSet(ThaCdrController self, uint32 packetLength);
eAtRet ThaCdrControllerCwSequenceModeSet(ThaCdrController self, eAtPwCwSequenceMode sequenceMode);
eAtPwCwSequenceMode ThaCdrControllerCwSequenceModeGet(ThaCdrController self);

/* CEP */
eAtRet ThaSdhChannelCdrControllerEparEnable(ThaSdhChannelCdrController self, eBool enable);
eBool ThaSdhChannelCdrControllerEparIsEnabled(ThaSdhChannelCdrController self);

uint32 ThaCdrControllerPartOffset(ThaCdrController self);
AtSdhChannel ThaSdhChannelCdrControllerSdhChannel(ThaSdhChannelCdrController self, AtChannel channel);
eAtRet ThaCdrControllerInit(ThaCdrController self);
uint32 ThaSdhChannelCdrControllerMapLineOffset(ThaSdhChannelCdrController self, AtChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId);
uint32 ThaCdrControllerCDREngineTimingCtrl(ThaCdrController self);
eAtRet ThaCdrControllerChannelMapTypeSet(ThaCdrController self);
int32 ThaCdrControllerPpmCalculate(ThaCdrController self, uint32 ncoValue);
int32 ThaCdrControllerPpbCalculate(ThaCdrController self, uint32 ncoValue);
eAtRet ThaCdrControllerTimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource);
uint32 ThaCdrControllerTimingStatusOffset(ThaCdrController self);

/* Interrupt */
eBool ThaCdrControllerInterruptMaskIsSupported(ThaCdrController self, uint32 defectMask);
eAtRet ThaCdrControllerInterruptMaskSet(ThaCdrController self, uint32 defectMask, uint32 enableMask);
uint32 ThaCdrControllerInterruptMaskGet(ThaCdrController self);
uint32 ThaCdrControllerInterruptRead2Clear(ThaCdrController self, eBool read2Clear);
void ThaCdrControllerInterruptProcess(ThaCdrController self, uint32 offset);
uint32 ThaCdrControllerInterruptOffsetGet(ThaCdrController self);

/* For debugging */
void ThaCdrControllerDebug(ThaCdrController self);
void ThaCdrControllerPwHeaderDebug(ThaCdrController self, AtPw pw);

/* For registers */
uint32 ThaCdrControllerChannelIdFlat(ThaCdrController self, AtChannel channel);
uint8 ThaCdrControllerChannelSliceGet(ThaCdrController self, AtChannel channel);
void ThaCdrControllerRegsShow(ThaCdrController self);

/* Product concretes */
ThaCdrController ThaPdhPwProductDe1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60070013De1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60210011HoLineVcCdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60210031De3CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60210021De1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60210011Tfi5LineHoVcCdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60210061De3CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60210061Ec1HoVcCdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60290011VcDe1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60290011De1CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha60290022HoVcCdrControllerNew(uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THACDRCONTROLLER_H_ */


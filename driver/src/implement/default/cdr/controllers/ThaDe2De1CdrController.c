/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaDe2De1CdrController.c
 *
 * Created Date: Feb 4, 2015
 *
 * Description : CDR controller for DE1 that is inside a DS2/E2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaDe2De1CdrControllerInternal.h"
#include "../../pdh/ThaPdhDe2De1.h"
#include "../../map/ThaModuleStmMap.h"
#include "../ThaModuleCdrStm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;

/* Save super implementation */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 SliceOffset(ThaCdrController self, uint8 slice)
    {
    ThaModuleCdr cdrModule = ThaCdrControllerModuleGet(self);
    AtPdhChannel de1 = (AtPdhChannel)ThaCdrControllerChannelGet(self);
    return ThaModuleCdrStmSliceOffset(cdrModule, (AtSdhChannel)AtPdhChannelVcInternalGet(de1), slice);
    }

static uint32 DefaultOffset(ThaCdrController self)
    {
    uint8 slice, sts;
    AtPdhChannel channel = (AtPdhChannel)ThaCdrControllerChannelGet(self);
    ThaPdhChannelHwIdGet(channel, cThaModuleCdr, &slice, &sts);
    return (SliceOffset(self, slice) + ThaCdrControllerIdGet(self));
    }

static uint8 PartGet(ThaCdrController self)
    {
    /* Let concrete determine this */
    AtUnused(self);
    return 0;
    }

static uint32 MapLineOffset(ThaSdhChannelCdrController self, AtChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    ThaModuleStmMap mapModule = (ThaModuleStmMap)AtDeviceModuleGet(device, cThaModuleMap);
    AtUnused(vtId);
    AtUnused(vtgId);
    AtUnused(stsId);
    return ThaModuleStmMapPdhDe2De1MapLineOffset(mapModule, (AtPdhDe1)ThaCdrControllerChannelGet((ThaCdrController)self));
    }

static uint32 ChannelIdFlat(ThaCdrController self, AtChannel channel)
    {
    ThaPdhDe1 de2de1 = (ThaPdhDe1)channel;
    AtUnused(self);
    return ThaPdhDe1FlatId(de2de1);
    }

static uint32 ChannelDefaultOffset(ThaCdrController controller)
    {
    return ThaModuleCdrControllerDe2De1DefaultOffset(controller);
    }

static uint8 ChannelSliceGet(ThaCdrController self, AtChannel channel)
    {
    AtPdhChannel de3 = AtPdhChannelParentChannelGet(AtPdhChannelParentChannelGet((AtPdhChannel)channel));
    uint8 slice, hwId;
    AtUnused(self);

    ThaPdhChannelHwIdGet(de3, cThaModuleCdr, &slice, &hwId);
    return slice;
    }

static AtSdhChannel SdhChannel(ThaSdhChannelCdrController self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return NULL;
    }

static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    ThaModuleCdr cdrModule = ThaCdrControllerModuleGet(self) ;

    uint32 regAddr = ThaCdrControllerCDREngineTimingCtrl(self) + mMethodsGet(self)->TimingCtrlOffset(self);
    uint32 regVal = mModuleHwRead(cdrModule, regAddr);
    mFieldIns(&regVal, cThaLineTypeMask, cThaLineTypeShift, AtPdhDe1IsE1(de1) ? 0x0 : 0x1);
    mModuleHwWrite(cdrModule, regAddr, regVal);

    return ThaCdrDe1TimingControlMapVtModeSet(cdrModule, (AtPdhChannel)de1);
    }

static void RegDisplay(ThaCdrController self, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay(ThaCdrControllerChannelGet(self), address, cThaModuleCdr);
    }

static void RegsShow(ThaCdrController self)
    {
    uint32          address;
    ThaModuleCdr    moduleCdr    = ThaCdrControllerModuleGet(self);

    address = ThaVc1xCdrControllerCDRVtTimeCtrl(self) + ThaModuleCdrControllerDe2De1DefaultOffset(self);
    RegDisplay(self, "CDR VT Timing control", address);

    address = ThaModuleCdrEngineTimingCtrlRegister(moduleCdr) + mMethodsGet(self)->TimingCtrlOffset(self);
    RegDisplay(self, "CDR Engine Timing control", address);

    address = ThaModuleCdrAdjustStateStatusRegister(moduleCdr, self) + mMethodsGet(self)->TimingStatusOffset(self);
    RegDisplay(self, "CDR Adjust State status", address);
    }

static uint32 ChannelInterruptMask(ThaCdrController self)
    {
    AtUnused(self);
    return cAtPdhDe1AlarmClockStateChange;
    }

static uint32 TimingCtrlOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingCtrlDe2De1Offset(self);
    }

static uint32 TimingStatusOffset(ThaCdrController self)
    {
    return ThaModuleCdrTimingStateDe2De1Offset(self);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, PartGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelIdFlat);
        mMethodOverride(m_ThaCdrControllerOverride, DefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelDefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelSliceGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelMapTypeSet);
        mMethodOverride(m_ThaCdrControllerOverride, RegsShow);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelInterruptMask);
        mMethodOverride(m_ThaCdrControllerOverride, TimingCtrlOffset);
        mMethodOverride(m_ThaCdrControllerOverride, TimingStatusOffset);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void OverrideThaSdhChannelCdrController(ThaCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, mMethodsGet(controller), sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, MapLineOffset);
        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, SdhChannel);
        }

    mMethodsSet(controller, &m_ThaSdhChannelCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    OverrideThaSdhChannelCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDe2De1CdrController);
    }

ThaCdrController ThaDe2De1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcDe1CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController ThaDe2De1CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaDe2De1CdrControllerObjectInit(newController, engineId, channel);
    }

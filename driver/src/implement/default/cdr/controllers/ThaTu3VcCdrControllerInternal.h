/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaTu3VcCdrControllerInternal.h
 * 
 * Created Date: Sep 27, 2016
 *
 * Description : TU3 VC3 CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THATU3VCCDRCONTROLLERINTERNAL_H_
#define _THATU3VCCDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHoVcCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaTu3VcCdrController * ThaTu3VcCdrController;

typedef struct tThaTu3VcCdrController
    {
    tThaHoVcCdrController super;

    /* Private */
    }tThaTu3VcCdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaTu3VcCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
ThaCdrController ThaTu3VcCdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _THATU3VCCDRCONTROLLERINTERNAL_H_ */


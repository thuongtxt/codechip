/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaVcDe1CdrControllerInternal.h
 * 
 * Created Date: Jul 18, 2013
 *
 * Description : VC's DE1 CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAVCDE1CDRCONTROLLERINTERNAL_H_
#define _THAVCDE1CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/pdh/AtPdhChannelInternal.h"
#include "ThaVc1xCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaVcDe1CdrController * ThaVcDe1CdrController;

typedef struct tThaVcDe1CdrControllerMethods
    {
    uint8 (*Vc1xDefaultTiming)(ThaVcDe1CdrController self);
    }tThaVcDe1CdrControllerMethods;

typedef struct tThaVcDe1CdrController
    {
    tThaVc1xCdrController super;
    const tThaVcDe1CdrControllerMethods *methods;

    /* Private data */
    }tThaVcDe1CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController ThaVcDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAVCDE1CDRCONTROLLERINTERNAL_H_ */


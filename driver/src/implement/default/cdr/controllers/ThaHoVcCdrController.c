/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaHoVcCdrController.c
 *
 * Created Date: Apr 20, 2013
 *
 * Description : CDR controller for AU-VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../map/ThaModuleStmMapInternal.h"
#include "../ThaModuleCdrInternal.h"
#include "ThaHoVcCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaHoVcCdrController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaHoVcCdrControllerMethods m_methods;

/* Override */
static tAtObjectMethods                   m_AtObjectOverride;
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;

/* Save super implementation */
static const tAtObjectMethods         *m_AtObjectMethods         = NULL;
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelIdFlat(ThaCdrController self, AtChannel channel)
    {
    uint8 slice, sts;
    ThaSdhChannelCdrController sdhCdrController = (ThaSdhChannelCdrController)self;
    AtSdhChannel sdhChannel = mMethodsGet(sdhCdrController)->SdhChannel(sdhCdrController, channel);

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);
    return sts;
    }

static uint32 ThaRegCDRStsTimeCtrl(ThaCdrController self, AtSdhChannel sdhChannel)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)ThaCdrControllerModuleGet(self);
    if (moduleCdr)
        return mMethodsGet(moduleCdr)->StsTimingControlReg(moduleCdr, sdhChannel);

    return 0;
    }

static uint32 ThaVC3CDRChidMask(ThaCdrController self)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)ThaCdrControllerModuleGet(self);
    if (moduleCdr)
        return mMethodsGet(moduleCdr)->StsCdrChannelIdMask(moduleCdr);

    return 0;
    }

static uint32 ThaVC3CDRChidShift(ThaCdrController self)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)ThaCdrControllerModuleGet(self);
    if (moduleCdr)
        return mMethodsGet(moduleCdr)->StsCdrChannelIdShift(moduleCdr);

    return 0;
    }

static eAtRet CdrIdAssign(ThaCdrController self)
    {
    uint32 regAddr = ThaRegCDRStsTimeCtrl(self, (AtSdhChannel)ThaCdrControllerChannelGet(self)) + mMethodsGet(self)->ChannelDefaultOffset(self);
    uint32 regVal = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, ThaVC3CDRChidMask(self), ThaVC3CDRChidShift(self), ThaCdrControllerIdGet(self));
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel((ThaSdhChannelCdrController)self, ThaCdrControllerChannelGet(self));
    uint32 regAddr = ThaRegCDRStsTimeCtrl(self, sdhChannel) + mMethodsGet(self)->ChannelDefaultOffset(self);
    uint32 regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaMapStsMdMask, cThaMapStsMdShift, 0);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet StsTimingModeSet(ThaSdhChannelCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaCdrController controller = (ThaCdrController)self;
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(controller);

    uint32 regAddr = ThaRegCDRStsTimeCtrl(controller, sdhChannel) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 regVal  = ThaCdrControllerRead(controller, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaVC3TimeModeMask, cThaVC3TimeModeShift, ThaCdrControllerTimingModeSw2Hw(controller, timingMode, timingSource));
    ThaCdrControllerWrite(controller, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static eAtRet TimingModeSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret;
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    mMethodsGet(self)->CdrIdAssign(self);

    /* And have the super do first */
    ret = m_ThaCdrControllerMethods->TimingModeSet(self, timingMode, timingSource);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(controller)->StsTimingModeSet(controller, timingMode, timingSource);
    }

static uint32 TimingSourceId(ThaHoVcCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaCdrController controller = (ThaCdrController)self;
    AtChannel source = mMethodsGet(controller)->TimingSourceForConfiguration(controller, timingMode, timingSource);
    return mMethodsGet(controller)->ChannelIdFlat(controller, source) << 5;
    }

static eBool TimingSourceStsIdIsMaster(ThaHoVcCdrController self, AtSdhChannel sdhChannel, uint8 sts1Id)
    {
    AtUnused(self);
    return (sts1Id == AtSdhChannelSts1Get(sdhChannel)) ? cAtTrue : cAtFalse;
    }

static eAtRet TimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    static const uint8 cVtgDontCare = 0;
    static const uint8 cVtDontCare = 0;

    uint32 regVal, address;
    eBool isMaster;
    uint8 numSts, i;
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;
    AtSdhChannel sdhChannel = mMethodsGet(controller)->SdhChannel(controller, ThaCdrControllerChannelGet(self));
    AtChannel source = mMethodsGet(self)->TimingSourceForConfiguration(self, timingMode, timingSource);
    ThaModuleMap mapModule = (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet(ThaCdrControllerChannelGet(self)), cThaModuleMap);
    ThaHoVcCdrController hoVcController = (ThaHoVcCdrController)self;

    numSts = AtSdhChannelNumSts(sdhChannel);
    if (source == NULL)
        isMaster = cAtTrue;
    else
        isMaster = (source == ThaCdrControllerChannelGet(self)) ? cAtTrue : cAtFalse;

    for (i = 0; i < numSts; i++)
        {
        uint32 mask, shift;
        uint32 timingSourceId;
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhChannel) + i);

        address = ThaModuleStmMapMapLineCtrl((ThaModuleStmMap)mapModule, sdhChannel) +
                  ThaSdhChannelCdrControllerMapLineOffset(controller, (AtChannel)sdhChannel, stsId, cVtgDontCare, cVtDontCare);
        regVal = ThaCdrControllerRead(self, address, cThaModuleMap);

        mask  = ThaModuleMapMapTimeSrcMastMask(mapModule, sdhChannel);
        shift = ThaModuleMapMapTimeSrcMastShift(mapModule, sdhChannel);
        if (mMethodsGet(hoVcController)->TimingSourceStsIdIsMaster(hoVcController, sdhChannel, stsId))
            mFieldIns(&regVal, mask, shift, isMaster ? 1 : 0);
        else
            mFieldIns(&regVal, mask, shift, 0);

        mask  = ThaModuleMapMapTimeSrcIdMask(mapModule, sdhChannel);
        shift = ThaModuleMapMapTimeSrcIdShift(mapModule, sdhChannel);
        timingSourceId = mMethodsGet(hoVcController)->TimingSourceId(hoVcController, timingMode, timingSource);
        mFieldIns(&regVal, mask, shift, timingSourceId);
        ThaCdrControllerWrite(self, address, regVal, cThaModuleMap);
        }

    return m_ThaCdrControllerMethods->TimingSourceSet(self, timingMode, timingSource);
    }

static uint8 HwTimingModeGet(ThaCdrController self)
    {
    uint32 address, value;
    uint8 hwTimmingMode;
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(self);

    address = ThaRegCDRStsTimeCtrl(self, sdhChannel) + mMethodsGet(self)->ChannelDefaultOffset(self);
    value = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldGet(value, cThaVC3TimeModeMask, cThaVC3TimeModeShift, uint8, &hwTimmingMode);

    return hwTimmingMode;
    }

static eAtRet EparEnable(ThaSdhChannelCdrController self, eBool enable)
    {
    ThaCdrController controller = (ThaCdrController)self;
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(controller);

    uint32 regAddr = ThaRegCDRStsTimeCtrl((ThaCdrController)self, sdhChannel) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 regVal  = ThaCdrControllerRead(controller, regAddr, cThaModuleCdr);
    mFieldIns(&regVal, cThaVC3EParEnMask, cThaVC3EParEnShift, mBoolToBin(enable));
    ThaCdrControllerWrite(controller, regAddr, regVal, cThaModuleCdr);
    return cAtOk;
    }

static eBool EparIsEnabled(ThaSdhChannelCdrController self)
    {
    ThaCdrController controller = (ThaCdrController)self;
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(controller);

    uint32 regAddr = ThaRegCDRStsTimeCtrl((ThaCdrController)self, sdhChannel) + mMethodsGet(controller)->ChannelDefaultOffset(controller);
    uint32 regVal  = ThaCdrControllerRead(controller, regAddr, cThaModuleCdr);
    return (regVal & cThaVC3EParEnMask) ? cAtTrue : cAtFalse;
    }

static uint32 ChannelDefaultOffset(ThaCdrController self)
    {
    return ThaModuleCdrControllerHoVcDefaultOffset(self);
    }

static void RegDisplay(ThaCdrController self, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay(ThaCdrControllerChannelGet(self), address, cThaModuleCdr);
    }

static void RegsShow(ThaCdrController self)
    {
    uint32 address;
    ThaModuleCdr    moduleCdr    = ThaCdrControllerModuleGet(self);

    address = ThaHoVcCdrControllerRegCDRStsTimeCtrl(self) + mMethodsGet(self)->ChannelDefaultOffset(self);
    RegDisplay(self, "CDR STS Timing control", address);

    address = ThaModuleCdrEngineTimingCtrlRegister(moduleCdr) + mMethodsGet(self)->TimingCtrlOffset(self);
    RegDisplay(self, "CDR Engine Timing control", address);

    address = ThaModuleCdrAdjustStateStatusRegister(moduleCdr, self) + mMethodsGet(self)->TimingStatusOffset(self);
    RegDisplay(self, "CDR Adjust State status", address);
    }

static int32 PpmCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(self);

    if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeTu3)
        return ThaCdrDebuggerTu3VcPpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerHoVcPpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static int32 PpbCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)ThaCdrControllerChannelGet(self);

    if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeTu3)
        return ThaCdrDebuggerTu3VcPpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerHoVcPpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static eBool CanOnlyUseFixedTiming(ThaCdrController self)
    {
    AtSdhChannel vc = (AtSdhChannel)ThaCdrControllerChannelGet(self);

    if ((AtSdhChannelTypeGet(vc)    == cAtSdhChannelTypeVc4) &&
        (AtSdhChannelMapTypeGet(vc) == cAtSdhVcMapTypeVc4Map3xTug3s))
        return cAtTrue;

    return m_ThaCdrControllerMethods->CanOnlyUseFixedTiming(self);
    }

static eAtTimingMode TimingModeGet(ThaCdrController self)
    {
    if (mMethodsGet(self)->CanOnlyUseFixedTiming(self))
        {
        /* Some channel will not be able to specify timing and they always use
         * its default fixed timing mode. */
        if (mThis(self)->fixedTimingMode == cAtTimingModeUnknown)
            mThis(self)->fixedTimingMode = mMethodsGet(self)->FixedTimingMode(self);

        return mThis(self)->fixedTimingMode;
        }

    return m_ThaCdrControllerMethods->TimingModeGet(self);
    }

static eAtRet TimingSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (!mMethodsGet(self)->CanOnlyUseFixedTiming(self))
        return m_ThaCdrControllerMethods->TimingSet(self, timingMode, timingSource);

    if (mMethodsGet(self)->CanSpecifyFixedTimingMode(self, timingMode))
        {
        mThis(self)->fixedTimingMode = timingMode;
        return cAtOk;
        }

    mChannelLog(ThaCdrControllerChannelGet(self), cAtLogLevelWarning, "Only fixed timing mode is supported\r\n");
    return cAtErrorModeNotSupport;
    }

static eBool TimingModeIsSupported(ThaCdrController self, eAtTimingMode timingMode)
    {
    if (mMethodsGet(self)->CanOnlyUseFixedTiming(self))
        return mMethodsGet(self)->CanSpecifyFixedTimingMode(self, timingMode);
    return m_ThaCdrControllerMethods->TimingModeIsSupported(self, timingMode);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHoVcCdrController object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(fixedTimingMode);
    }

static void OverrideAtObject(ThaHoVcCdrController self)
    {
    AtObject controller = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(controller, &m_AtObjectOverride);
    }

static void OverrideThaCdrController(ThaHoVcCdrController self)
    {
    ThaCdrController controller = (ThaCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, TimingSourceSet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeSet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelIdFlat);
        mMethodOverride(m_ThaCdrControllerOverride, HwTimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelDefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, CdrIdAssign);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelMapTypeSet);
        mMethodOverride(m_ThaCdrControllerOverride, RegsShow);
        mMethodOverride(m_ThaCdrControllerOverride, PpmCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, PpbCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, CanOnlyUseFixedTiming);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingSet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeIsSupported);
        }

    mMethodsSet(controller, &m_ThaCdrControllerOverride);
    }

static void OverrideThaSdhChannelCdrController(ThaHoVcCdrController self)
    {
    ThaSdhChannelCdrController controller = (ThaSdhChannelCdrController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, mMethodsGet(controller), sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, EparEnable);
        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, EparIsEnabled);
        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, StsTimingModeSet);
        }

    mMethodsSet(controller, &m_ThaSdhChannelCdrControllerOverride);
    }

static void Override(ThaHoVcCdrController self)
    {
    OverrideAtObject(self);
    OverrideThaCdrController(self);
    OverrideThaSdhChannelCdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHoVcCdrController);
    }

static void MethodsInit(ThaHoVcCdrController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TimingSourceId);
        mMethodOverride(m_methods, TimingSourceStsIdIsMaster);
        }

    mMethodsSet(self, &m_methods);
    }

ThaCdrController ThaHoVcCdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhChannelCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((ThaHoVcCdrController)self);
    Override((ThaHoVcCdrController)self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->fixedTimingMode = cAtTimingModeUnknown;

    return self;
    }

ThaCdrController ThaHoVcCdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaHoVcCdrControllerObjectInit(newController, engineId, channel);
    }

uint32 ThaHoVcCdrControllerRegCDRStsTimeCtrl(ThaCdrController self)
    {
    AtChannel channel = ThaCdrControllerChannelGet(self);
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel((ThaSdhChannelCdrController)self, channel);
    return ThaRegCDRStsTimeCtrl(self, sdhChannel);
    }

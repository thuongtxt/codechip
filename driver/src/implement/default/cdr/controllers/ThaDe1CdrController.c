/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaDe1CdrController.c
 *
 * Created Date: Apr 25, 2013
 *
 * Description : CDR controller for DS1/E1 that works with LIU
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaCdrControllerInternal.h"
#include "../../cdr/ThaModuleCdrReg.h"
#include "../../map/ThaModuleMapReg.h"
#include "../../map/ThaModuleMapInternal.h"
#include "../../pdh/ThaModulePdhReg.h"
#include "../../pw/adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaDe1CdrControllerMethods m_methods;

/* Override */
static tThaCdrControllerMethods m_ThaCdrControllerOverride;

/* Save super implementation */
static const tThaCdrControllerMethods *m_ThaCdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaDe1CdrController);
    }

static eBool IsExtTimingMode(ThaCdrController self, eAtTimingMode timingMode)
    {
	AtUnused(self);
    return ((timingMode == cAtTimingModeExt1Ref) || (timingMode == cAtTimingModeExt2Ref)) ? cAtTrue : cAtFalse;
    }

static eBool TimingModeIsSupported(ThaCdrController self, eAtTimingMode timingMode)
    {
    if (IsExtTimingMode(self, timingMode))
        return cAtTrue;
    return m_ThaCdrControllerMethods->TimingModeIsSupported(self, timingMode);
    }

static eBool TimingIsSupported(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (IsExtTimingMode(self, timingMode))
        return cAtTrue;

    if (!ThaDe1CdrControllerAcrDcrTimingConfigurationIsValid(self, timingMode, timingSource))
        return cAtFalse;

    /* Let super do other cases */
    return m_ThaCdrControllerMethods->TimingIsSupported(self, timingMode, timingSource);
    }

static uint8 TimingModeSw2Hw(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (timingMode == cAtTimingModeSlave)
        return 0;

    return m_ThaCdrControllerMethods->TimingModeSw2Hw(self, timingMode, timingSource);
    }

static eAtRet LiuTimingModeSet(ThaDe1CdrController self, eAtTimingMode timingMode)
    {
    uint32 regVal;
    ThaCdrController controller = (ThaCdrController)self;
    uint32 de1Id = AtChannelIdGet(ThaCdrControllerChannelGet(controller));

    regVal = ThaCdrControllerRead(controller, cThaRegPdhLIULooptimeCfg, cAtModulePdh);
    mFieldIns(&regVal, (cBit0 << de1Id) , de1Id, (timingMode == cAtTimingModeLoop) ? 0x1 : 0x0);
    ThaCdrControllerWrite(controller, cThaRegPdhLIULooptimeCfg, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet TimingModeSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)ThaCdrControllerChannelGet(self);
    uint8 hwTimingMode;
    uint32 address, regVal;
    uint32 de1Id = AtChannelIdGet((AtChannel)de1);
    ThaDe1CdrController controller = (ThaDe1CdrController)self;

    address = cThaRegCDRTimingCtrl + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal = ThaCdrControllerRead(self, address, cThaModuleCdr);
    hwTimingMode = mMethodsGet(self)->TimingModeSw2Hw(self, timingMode, timingSource);
    mFieldIns(&regVal, cThaRegCDRTimingHoldValMdMask, cThaRegCDRTimingHoldValMdShift, 0);
    mFieldIns(&regVal, cThaRegCDRTimingMdMask, cThaRegCDRTimingMdShift, hwTimingMode);
    ThaCdrControllerWrite(self, address, regVal, cThaModuleCdr);

    /* Set CDR Selection Control */
    regVal = ThaCdrControllerRead(self, cThaRegCDRSelCtrl, cThaModuleCdr);
    mFieldIns(&regVal, (cBit0 << de1Id), de1Id, (timingMode == cAtTimingModeDcr) ? 1 : 0);
    ThaCdrControllerWrite(self, cThaRegCDRSelCtrl, regVal, cThaModuleCdr);

    /* LIU timing mode */
    if (ThaModuleCdrHasLiuTimingMode(ThaCdrControllerModuleGet(self)))
        return mMethodsGet(controller)->LiuTimingModeSet(controller, timingMode);

    return cAtOk;
    }

static eBool IsAcrOrDcrTimingMode(eAtTimingMode timingMode)
    {
    if ((timingMode == cAtTimingModeAcr) || (timingMode == cAtTimingModeDcr))
        return cAtTrue;

    return cAtFalse;
    }

eAtRet ThaDe1CdrControllerTimingSourceSave(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtPw pw = (AtPw)timingSource;

    if (!IsAcrOrDcrTimingMode(timingMode))
        {
        self->pwTimingSource = NULL;
        return cAtOk;
        }

    /* If this DE1 is bound to PW, use its PW as timing source and
     * the ThaCdrController.TimingSourceGet will return correct timing
     * source. Do not need to cache timing source */
    if (AtChannelBoundPwGet(ThaCdrControllerChannelGet(self)))
        {
        self->timingSource   = NULL;
        self->pwTimingSource = NULL;
        return cAtOk;
        }

    /* In case of CESoP, DE1 timing source can be one of PWs that its NxDS0s are bound to */
    if (timingSource == NULL)
        return cAtOk;

    if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
        return cAtOk;

    self->timingSource   = NULL;
    self->pwTimingSource = pw;
    ThaPwAdapterAcrDcrRefCircuitSet(ThaPwAdapterGet((AtPw)timingSource), ThaCdrControllerChannelGet(self));

    return cAtOk;
    }

static ThaModuleAbstractMap ModuleMap(ThaCdrController self)
    {
    AtChannel channel = ThaCdrControllerChannelGet(self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModuleMap);
    }

static eAtRet TimingSourceSet(ThaCdrController self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    uint32  regVal, address, mask, shift;
    AtChannel source = mMethodsGet(self)->TimingSourceForConfiguration(self, timingMode, timingSource);
    ThaModuleMap mapModule = (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet(source), cThaModuleMap);

    eAtRet ret;

    address = ThaModuleMapTimingRefCtrlRegister(ModuleMap(self)) + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal  = ThaCdrControllerRead(self, address, cThaModuleMap);
    mask  = mFieldMask(mapModule, MapTmSrcId);
    shift = mFieldShift(mapModule, MapTmSrcId);
    mFieldIns(&regVal, mask, shift, AtChannelIdGet((AtChannel)source));
    ThaCdrControllerWrite(self, address, regVal, cThaModuleMap);

    /* Let super do remaining thing */
    ret  = m_ThaCdrControllerMethods->TimingSourceSet(self, timingMode, timingSource);
    ret |= ThaDe1CdrControllerTimingSourceSave(self, timingMode, timingSource);

    return ret;
    }

static AtPw CurrentAcrDcrTimingSource(ThaCdrController self)
    {
    return ThaDe1CdrControllerCurrentAcrDcrTimingSource(self);
    }

static uint8 HwTimingModeGet(ThaCdrController self)
    {
    uint32 address, regVal;
    uint8 hwTimingMode;

    address = cThaRegCDRTimingCtrl + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal  = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldGet(regVal, cThaRegCDRTimingMdMask, cThaRegCDRTimingMdShift, uint8, &hwTimingMode);

    return hwTimingMode;
    }

static eAtRet RxNcoPackLenSet(ThaCdrController self, uint32 packetLength)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegDCRRxNcoConf + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mRegFieldSet(regVal, cThaRegDCRRxNcoConfPackLen, packetLength);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet TimingPacketLenInBitsSet(ThaCdrController self, uint32 packetLengthInBits)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegCDRTimingCtrl + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    mRegFieldSet(regVal, cThaRegCDRTimingPktLen, packetLengthInBits);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eBool SequenceIsSupported(eAtPwCwSequenceMode sequenceMode)
    {
    if ((sequenceMode == cAtPwCwSequenceModeWrapZero) ||
        (sequenceMode == cAtPwCwSequenceModeSkipZero))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 SequenceModeSw2Hw(eAtPwCwSequenceMode sequenceMode)
    {
    if (sequenceMode == cAtPwCwSequenceModeWrapZero) return 0;
    if (sequenceMode == cAtPwCwSequenceModeSkipZero) return 1;

    return 0;
    }

static eAtPwCwSequenceMode SequenceModeHw2Sw(uint8 sequenceMode)
    {
    if (sequenceMode == 0) return cAtPwCwSequenceModeWrapZero;
    if (sequenceMode == 1) return cAtPwCwSequenceModeSkipZero;

    return cAtPwCwSequenceModeInvalid;
    }

static eAtRet CwSequenceModeSet(ThaCdrController self, eAtPwCwSequenceMode sequenceMode)
    {
    uint32 regAddr, regVal;
    uint16 value;

    if (!SequenceIsSupported(sequenceMode))
        return cAtErrorModeNotSupport;

    regAddr = cThaRegCDRTimingCtrl + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal  = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    value   = SequenceModeSw2Hw(sequenceMode);
    mRegFieldSet(regVal, cThaRegCDRTimingSeqMd, value);
    ThaCdrControllerWrite(self, regAddr, regVal, cThaModuleCdr);

    return cAtOk;
    }

static eAtPwCwSequenceMode CwSequenceModeGet(ThaCdrController self)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegCDRTimingCtrl + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal = ThaCdrControllerRead(self, regAddr, cThaModuleCdr);
    return SequenceModeHw2Sw((uint8)mRegField(regVal, cThaRegCDRTimingSeqMd));
    }

static eAtTimingMode TimingModeGet(ThaCdrController self)
    {
    uint8 hwTimingMode = mMethodsGet(self)->HwTimingModeGet(self);
    if (hwTimingMode != 0)
        return m_ThaCdrControllerMethods->TimingModeGet(self);

    /* Need to detect whether it is slave or system timing mode. These two modes
     * have the same hardware timing mode value but different in reference source */
    if ((self->timingSource == NULL) || (self->timingSource == ThaCdrControllerChannelGet(self)))
        return cAtTimingModeSys;

    return cAtTimingModeSlave;
    }

static uint32 ChannelDefaultOffset(ThaCdrController self)
    {
    return ThaModuleCdrControllerDe1DefaultOffset(self);
    }

static uint8 ChannelSliceGet(ThaCdrController self, AtChannel channel)
    {
    uint8 slice, hwId;
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &hwId);

    return slice;
    }

static eAtRet ChannelMapTypeSet(ThaCdrController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    uint32 address, regVal;

    address = cThaRegCDRTimingCtrl + mMethodsGet(self)->ChannelDefaultOffset(self);
    regVal = ThaCdrControllerRead(self, address, cThaModuleCdr);
    mFieldIns(&regVal, cThaRegCDRTimingLineTypeMask, cThaRegCDRTimingLineTypeShift, AtPdhDe1IsE1(de1) ? 0x0 : 0x1);
    ThaCdrControllerWrite(self, address, regVal, cThaModuleCdr);

    return cAtOk;
    }

static int32 PpmCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    if (AtPdhDe1IsE1(de1))
        return ThaCdrDebuggerE1PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerDs1PpmCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static uint32 ChannelInterruptMask(ThaCdrController self)
    {
    AtUnused(self);
    return cAtPdhDe1AlarmClockStateChange;
    }

static int32 PpbCalculate(ThaCdrController self, uint32 ncoValue)
    {
    AtPdhDe1 de1 = (AtPdhDe1)ThaCdrControllerChannelGet(self);
    if (AtPdhDe1IsE1(de1))
        return ThaCdrDebuggerE1PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);

    return ThaCdrDebuggerDs1PpbCalculate(ThaModuleCdrDebugger(ThaCdrControllerModuleGet(self)), ncoValue);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, m_ThaCdrControllerMethods, sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, TimingModeIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, TimingIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeSw2Hw);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeSet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingSourceSet);
        mMethodOverride(m_ThaCdrControllerOverride, CurrentAcrDcrTimingSource);
        mMethodOverride(m_ThaCdrControllerOverride, HwTimingModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, RxNcoPackLenSet);
        mMethodOverride(m_ThaCdrControllerOverride, TimingPacketLenInBitsSet);
        mMethodOverride(m_ThaCdrControllerOverride, CwSequenceModeGet);
        mMethodOverride(m_ThaCdrControllerOverride, CwSequenceModeSet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelDefaultOffset);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelSliceGet);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelMapTypeSet);
        mMethodOverride(m_ThaCdrControllerOverride, PpmCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, PpbCalculate);
        mMethodOverride(m_ThaCdrControllerOverride, ChannelInterruptMask);
        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    }

static void MethodsInit(ThaCdrController self)
    {
	ThaDe1CdrController controller = (ThaDe1CdrController)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LiuTimingModeSet);
        }

    mMethodsSet(controller, &m_methods);
    }

ThaCdrController ThaDe1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController ThaDe1CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaDe1CdrControllerObjectInit(newController, engineId, channel);
    }

AtPw ThaDe1CdrControllerCurrentAcrDcrTimingSource(ThaCdrController self)
    {
    eAtTimingMode timingMode = ThaCdrControllerTimingModeGet(self);
    if (IsAcrOrDcrTimingMode(timingMode))
        return (AtPw)ThaCdrControllerTimingSourceGet(self);

    return NULL;
    }

eBool ThaDe1CdrControllerAcrDcrTimingConfigurationIsValid(ThaCdrController self,
                                                          eAtTimingMode timingMode,
                                                          AtChannel timingSource)
    {
    eBool isAcrDcr;
    AtPw pwTimingSource;
    AtChannel circuit;

    isAcrDcr = IsAcrOrDcrTimingMode(timingMode);
    if (!isAcrDcr || (timingSource == NULL))
        return cAtTrue;

    /* If PW is specified for ACR/DCR timing, it must be bound to a circuit to
     * be a valid one */
    pwTimingSource = (AtPw)timingSource;
    circuit = AtPwBoundCircuitGet(pwTimingSource);
    if (circuit == NULL)
        {
        AtChannelLog(ThaCdrControllerChannelGet(self), cAtLogLevelWarning, AtSourceLocation,
                     "Cannot set ACR/DCR to PW that has not been bound to any circuit\r\n");
        return cAtFalse;
        }

    /* Cannot set ACR/DCR to PW that has NxDS0 of other DE1 */
    if (AtPwTypeGet(pwTimingSource) == cAtPwTypeCESoP)
        {
        AtPdhDe1 de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)circuit);
        if (de1 != (AtPdhDe1)ThaCdrControllerChannelGet(self))
            {
            AtChannelLog(ThaCdrControllerChannelGet(self), cAtLogLevelWarning, AtSourceLocation,
                         "Cannot set ACR/DCR to PW that has NxDS0 of other DE1\r\n");
            return cAtFalse;
            }
        }

    return cAtTrue;
    }

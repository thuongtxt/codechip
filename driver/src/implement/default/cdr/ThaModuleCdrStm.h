/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaModuleCdrStm.h
 * 
 * Created Date: Nov 16, 2012
 *
 * Description : STM CDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECDRSTM_H_
#define _THAMODULECDRSTM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ocn/ThaModuleOcn.h"
#include "ThaModuleCdr.h" /* Super class */
#include "AtPdhDe2.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleCdrStmNew(AtDevice device);

eAtRet ThaCdrStsPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, eThaOcnVc3PldType payloadType);
eAtRet ThaCdrVtgPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, eThaOcnVtgTug2PldType payloadType);
eAtRet ThaCdrVtPldSet(ThaModuleCdr self, AtSdhChannel vc1x, eAtSdhVcMapType vcType);
eAtRet ThaCdrDe1TimingControlMapVtModeSet(ThaModuleCdr self, AtPdhChannel de1);
eAtRet ThaCdrVC4xPldSet(ThaModuleCdr self, AtSdhChannel sdhChannel);
eAtRet ThaCdrVc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, eBool isTu3Path);
eAtRet ThaCdrVc3CepPldSet(ThaModuleCdr self, AtSdhChannel vc3);
eAtClockState ThaCdrEngineStateGet(ThaModuleCdr self, uint16 engineId);
eAtSdhVcMapType ThaCdrVtPldGet(ThaModuleCdr self, AtSdhChannel vc1x);
eAtRet ThaCdrDe3PldSet(ThaModuleCdr self, AtPdhChannel channel);
eAtRet ThaCdrDe3UnChannelizedModeSet(ThaModuleCdr self, AtPdhChannel channel, eBool isUnChannelized);
eAtRet ThaCdrDe2PldSet(ThaModuleCdr self, AtPdhChannel channel, eAtPdhDe2FrameType de2FrameType);
eAtRet ThaCdrDe1De2PldSet(ThaModuleCdr self, AtPdhChannel channel, uint32 e2ConvertedId, eAtPdhDe1FrameType de1FrameType);
eAtRet ThaCdrVc3PldSet(AtSdhChannel vc3, eThaOcnVc3PldType payloadType);
uint32 ThaModuleCdrStmVtTimingControlReg(ThaModuleCdrStm self);

/* Engine get */
uint32 ThaModuleCdrStmEngineIdOfVc1x(ThaModuleCdr self, AtSdhVc vc);
uint32 ThaModuleCdrStmEngineIdOfHoVc(ThaModuleCdr self, AtSdhVc vc);
uint32 ThaModuleCdrStmEngineIdOfDe1(ThaModuleCdr self, AtPdhDe1 de1);
uint32 ThaModuleCdrStmEngineIdOfDe3(ThaModuleCdr self, AtPdhDe3 de3);

/* By HW sts */
eAtRet ThaCdrHwStsVc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable);
eAtRet ThaCdrHwStsPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType);
uint32 ThaModuleCdrStmSliceOffset(ThaModuleCdr self, AtSdhChannel channel, uint8 slice);

/* Helper */
uint32 ThaModuleCdrStmSdhVcCdrIdCalculate(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId);
eAtRet ThaModuleCdrStmHoEngineLineTypeSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType);
eAtRet ThaModuleCdrStmDe2PldSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECDRSTM_H_ */

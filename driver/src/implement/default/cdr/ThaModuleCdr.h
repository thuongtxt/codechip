/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR (internal module)
 * 
 * File        : ThaModuleCdr.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECDR_H_
#define _THAMODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"

#include "../pdh/ThaPdhDe1.h"
#include "../pdh/ThaPdhDe3.h"
#include "controllers/ThaCdrController.h"
#include "debugger/ThaCdrDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleCdr    * ThaModuleCdr;
typedef struct tThaModuleCdrStm * ThaModuleCdrStm;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleCdrNew(AtDevice device);

uint32 ThaModuleCdrBaseAddress(ThaModuleCdr self);

/* For DS3/E3 */
eAtRet ThaModuleCdrDe3FrameModeSet(ThaPdhDe3 de3, eAtPdhDe3FrameType frameType);

/* CDR controller factory methods */
ThaCdrController ThaModuleCdrHoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc);
ThaCdrController ThaModuleCdrTu3VcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc);
ThaCdrController ThaModuleCdrVc1xCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc);
ThaCdrController ThaModuleCdrVcDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1);
ThaCdrController ThaModuleCdrVcDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3);
ThaCdrController ThaModuleCdrDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3);
ThaCdrController ThaModuleCdrDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1);
ThaCdrController ThaModuleCdrDe2De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1);

uint8 ThaModuleCdrNumParts(ThaModuleCdr self);
uint32 ThaModuleCdrPartOffset(ThaModuleCdr self, uint8 partId);

eAtRet ThaModuleCdrDe3TimingModeSet(ThaPdhDe3 de3, eAtTimingMode timingMode, AtChannel timingSource);
eAtTimingMode ThaModuleCdrDe3TimingModeGet(ThaPdhDe3 de3);

uint8 ThaModuleCdrHwSystemTimingMode(ThaModuleCdr self);
eAtTimingMode ThaModuleCdrOtherTimingSourceAsSystemTiming(ThaModuleCdr self);
eBool ThaModuleCdrVtAsyncMapIsSupported(ThaModuleCdr self);

/* DCR processing */
eAtRet ThaModuleCdrDcrClockSourceSet(ThaModuleCdr self, eAtPwDcrClockSource clockSource);
eAtPwDcrClockSource ThaModuleCdrDcrClockSourceGet(ThaModuleCdr self);
eAtRet ThaModuleCdrDcrClockFrequencySet(ThaModuleCdr self, uint32 khz);
uint32 ThaModuleCdrDcrClockFrequencyGet(ThaModuleCdr self);
eAtRet ThaModuleCdrDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz);
uint32 ThaModuleCdrDcrRtpTimestampFrequencyGet(ThaModuleCdr self);
uint32 ThaModuleCdrDefaultDcrClockFrequency(ThaModuleCdr self);
eAtRet ThaModuleCdrDcrRtpTimestampFrequencyRegSet(ThaModuleCdr self, uint32 khz);
uint32 ThaModuleCdrDcrRtpTimestampFrequencyFromRegGet(ThaModuleCdr self);
eBool ThaModuleCdrTimestampFrequencyInRange(ThaModuleCdr self, uint32 khz);

eBool ThaModuleCdrHasLiuTimingMode(ThaModuleCdr self);
eBool ThaModuleCdrHasDcrTimingMode(ThaModuleCdr self);
eBool ThaModuleCdrClockSubStateIsSupported(ThaModuleCdr self);
eAtClockState ThaModuleCdrClockStateFromHwStateGet(ThaModuleCdr self, uint8 hwState);
eAtClockState ThaModuleCdrClockStateFromLoChannelHwStateGet(ThaModuleCdr self, uint8 hwState);
eAtClockState ThaModuleCdrClockStateFromHoChannelHwStateGet(ThaModuleCdr self, uint8 hwState);

eBool ThaModuleCdrDcrShaperIsSupported(ThaModuleCdr self, ThaCdrController controller);
eBool ThaModuleCdrShouldEnableJitterAttenuator(ThaModuleCdr self);

/* For registers */
uint32 ThaModuleCdrAdjustStateStatusRegister(ThaModuleCdr self, ThaCdrController controller);
uint32 ThaModuleCdrEngineTimingCtrlRegister(ThaModuleCdr self);
uint32 ThaModuleCdrDcrTxEngineTimingControlRegister(ThaModuleCdr self);
uint32 ThaModuleCDRLineModeControlRegister(ThaModuleCdr self);
uint32 ThaModuleCdrAdjustHoldoverValueStatusRegister(ThaModuleCdr self);

/* Channel default offset */
uint32 ThaModuleCdrControllerDefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrChannelDefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrControllerHoVcDefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrControllerVc1xDefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrControllerDe1DefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrControllerDe3DefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrControllerDe2De1DefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrTxEngineTimingControlOffset(ThaCdrController controller);
uint32 ThaModuleCdrChannelTimingControlOffset(ThaModuleCdr self, ThaCdrController controller);
uint32 ThaModuleCdrTimingCtrlDefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrTimingCtrlDe2De1Offset(ThaCdrController controller);
uint32 ThaModuleCdrTimingCtrlVcDe1Offset(ThaCdrController controller);
uint32 ThaModuleCdrTimingCtrlVcDe3Offset(ThaCdrController controller);
uint32 ThaModuleCdrTimingCtrlVcOffset(ThaCdrController controller);
uint32 ThaModuleCdrTimingCtrlDe3Offset(ThaCdrController controller);
uint32 ThaModuleCdrTimingStateDefaultOffset(ThaCdrController controller);
uint32 ThaModuleCdrTimingStateDe2De1Offset(ThaCdrController controller);
uint32 ThaModuleCdrTimingStateVcDe1Offset(ThaCdrController controller);
uint32 ThaModuleCdrTimingStateVcDe3Offset(ThaCdrController controller);
uint32 ThaModuleCdrTimingStateVcOffset(ThaCdrController controller);
uint32 ThaModuleCdrTimingStateDe3Offset(ThaCdrController controller);

/* Bit fields */
uint32 ThaModuleCdrCDREngineUnlockedIntrMask(ThaCdrController controller);

/* For debugging */
void ThaModuleCdrSdhChannelRegsShow(AtSdhChannel sdhChannel);
void ThaModuleCdrPdhChannelRegsShow(AtPdhChannel pdhChannel);
ThaCdrDebugger ThaModuleCdrDebugger(ThaModuleCdr self);

/* Backward compatible */
eBool ThaModuleCdrInterruptIsSupported(ThaModuleCdr self);

/* Channelized BERT */
eAtRet ThaModuleCdrChannelizeDe3(ThaModuleCdr self, AtPdhDe3 de3, eBool channelized);
eAtRet ThaModuleCdrHoVcUnChannelize(ThaModuleCdr self, AtSdhChannel hoVc);
eAtRet ThaModuleCdrHoVcChannelizeRestore(ThaModuleCdr self, AtSdhChannel hoVc);

/* Product concretes */
AtModule ThaStmPwProductModuleCdrNew(AtDevice device);
AtModule ThaPdhPwProductModuleCdrNew(AtDevice device);
AtModule Tha60031021ModuleCdrNew(AtDevice device);
AtModule Tha60035011ModuleCdrNew(AtDevice device);
AtModule Tha60091023ModuleCdrNew(AtDevice device);
AtModule Tha60070013ModuleCdrNew(AtDevice device);
AtModule Tha60001031ModuleCdrNew(AtDevice device);
AtModule Tha60031031ModuleCdrNew(AtDevice device);
AtModule Tha60031033ModuleCdrNew(AtDevice device);
AtModule Tha60035021ModuleCdrNew(AtDevice device);
AtModule Tha60150011ModuleCdrNew(AtDevice device);
AtModule Tha60210031ModuleCdrNew(AtDevice device);
AtModule Tha60210021ModuleCdrNew(AtDevice device);
AtModule Tha60210011ModuleCdrNew(AtDevice device);
AtModule Tha61210011ModuleCdrNew(AtDevice device);
AtModule Tha60210051ModuleCdrNew(AtDevice device);
AtModule Tha60210061ModuleCdrNew(AtDevice device);
AtModule Tha60290021ModuleCdrNew(AtDevice device);
AtModule Tha60290022ModuleCdrNew(AtDevice device);
AtModule Tha60290011ModuleCdrNew(AtDevice device);
AtModule Tha6A210031ModuleCdrNew(AtDevice device);
AtModule Tha6A290011ModuleCdrNew(AtDevice device);
AtModule Tha6A290021ModuleCdrNew(AtDevice device);
AtModule Tha6A290022ModuleCdrNew(AtDevice device);
AtModule Tha60290022ModuleCdrV3New(AtDevice device);
AtModule Tha60291022ModuleCdrNew(AtDevice device);
AtModule Tha60290061ModuleCdrNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECDR_H_ */


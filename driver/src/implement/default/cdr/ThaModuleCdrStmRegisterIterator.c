/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaModuleCdrStmRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : CDR register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "../../../util/AtIteratorInternal.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "ThaModuleCdrInternal.h"
#include "ThaModuleCdrStmReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumVtsInVtg 4
#define cMaxNumVtgsInSts 7
#define cMaxNumStss 12

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModuleCdrStmRegisterIterator * ThaModuleCdrStmRegisterIterator;

typedef struct tThaModuleCdrStmRegisterIterator
    {
    tAtModuleRegisterIterator super;

    /* Private data */
    uint8 sts;
    uint8 vtg;
    uint8 vt;
    }tThaModuleCdrStmRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleCdrStmRegisterIterator);
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    return (((ThaModuleCdrStmRegisterIterator)self)->sts >= cMaxNumStss) ? cAtTrue : cAtFalse;
    }

static void NextVt(AtModuleRegisterIterator self)
    {
    ThaModuleCdrStmRegisterIterator iterator = (ThaModuleCdrStmRegisterIterator)self;

    iterator->vt = (uint8)(iterator->vt + 1);
    if (iterator->vt < cMaxNumVtsInVtg)
        return;
    iterator->vt = 0;

    iterator->vtg = (uint8)(iterator->vtg + 1);
    if (iterator->vtg < cMaxNumVtgsInSts)
        return;
    iterator->vtg = 0;

    iterator->sts = (uint8)(iterator->sts + 1);
    if (iterator->sts < cMaxNumStss)
        return;
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    ThaModuleCdrStmRegisterIterator iterator = (ThaModuleCdrStmRegisterIterator)self;

    NextVt(self);
    return (uint32)((iterator->sts * 32) + (iterator->vtg * 4) + iterator->vt);
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    mNextReg(iterator, 0, cThaRegCDRVtTimeCtrl);
    mNextChannelRegister(iterator, cThaRegCDRVtTimeCtrl, cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NoChannelLeft);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleCdrStmRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;
	
	/* Setup class */
    Override(self);
    m_methodsInit = 1;
    
    return self;
    }

AtIterator ThaModuleCdrStmRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return ThaModuleCdrStmRegisterIteratorObjectInit(newIterator, module);
    }

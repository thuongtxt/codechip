/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR internal module
 * 
 * File        : ThaModuleCdrInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECDRINTERNAL_H_
#define _THAMODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../../default/man/ThaDeviceInternal.h"
#include "../pdh/ThaPdhDe1.h"
#include "ThaModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleCdrMethods
    {
    uint32 (*BaseAddress)(ThaModuleCdr self);
    eAtRet (*DefaultSet)(ThaModuleCdr self);
    uint8 (*HwSystemTimingMode)(ThaModuleCdr self);
    eBool (*HasLiuTimingMode)(ThaModuleCdr self);
    eAtTimingMode (*OtherTimingSourceAsSystemTiming)(ThaModuleCdr self);
    eBool (*NeedToSetExtDefaultFrequency)(ThaModuleCdr self);
    uint32 (*DefaultExtFrequency)(ThaModuleCdr self);
    eBool (*VtAsyncMapIsSupported)(ThaModuleCdr self);
    eBool (*ClockSubStateIsSupported)(ThaModuleCdr self);
    uint32 (*MaxDcrPrcFrequency)(ThaModuleCdr self);

    eAtRet (*PartDcrClockFrequencySet)(ThaModuleCdr self, uint32 khz, uint8 partId);
    eAtRet (*PartDcrClockSourceSet)(ThaModuleCdr self, uint8 hwClockSource, uint8 partId);
    eBool (*OnlySupportPrcForDcr)(ThaModuleCdr self);
    uint32 (*DcrClockFrequencyGet)(ThaModuleCdr self);
    uint32 (*DefaultDcrClockFrequency)(ThaModuleCdr self);
    uint8 (*HwDcrClockSourceGet)(ThaModuleCdr self);
    eAtClockState (*ClockStateFromHwStateGet)(ThaModuleCdr self, uint8 hwState);
    eAtClockState (*ClockStateFromLoChannelHwStateGet)(ThaModuleCdr self, uint8 hwState);
    eAtClockState (*ClockStateFromHoChannelHwStateGet)(ThaModuleCdr self, uint8 hwState);

    /* CDR controller */
    ThaCdrController (*HoVcCdrControllerCreate)(ThaModuleCdr self, AtSdhVc vc);
    ThaCdrController (*Tu3VcCdrControllerCreate)(ThaModuleCdr self, AtSdhVc vc);
    ThaCdrController (*Vc1xCdrControllerCreate)(ThaModuleCdr self, AtSdhVc vc);
    ThaCdrController (*VcDe1CdrControllerCreate)(ThaModuleCdr self, AtPdhDe1 de1);
    ThaCdrController (*De1CdrControllerCreate)(ThaModuleCdr self, AtPdhDe1 de1);
    ThaCdrController (*De2De1CdrControllerCreate)(ThaModuleCdr self, AtPdhDe1 de1);
    ThaCdrController (*VcDe3CdrControllerCreate)(ThaModuleCdr self, AtPdhDe3 de3);
    ThaCdrController (*De3CdrControllerCreate)(ThaModuleCdr self, AtPdhDe3 de3);

    /* Registers */
    uint32 (*EngineTimingCtrlRegister)(ThaModuleCdr self);
    uint32 (*CDRLineModeControlRegister)(ThaModuleCdr self);
    uint32 (*AdjustStateStatusRegister)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*AdjustHoldoverValueStatusRegister)(ThaModuleCdr self);
    uint32 (*ExtRefAndPrcCtrlRegister)(ThaModuleCdr self);
    uint32 (*ControllerDefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*DcrTxEngineTimingControlRegister)(ThaModuleCdr self);
    uint32 (*DcrClockFrequencyRegMask)(ThaModuleCdr self);
    uint32 (*DcrRtpTimestampFrequencyRegMask)(ThaModuleCdr self);

    /* Channel default offset */
    uint32 (*HoVcDefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*Vc1xDefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*De1DefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*De3DefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*De2De1DefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TxEngineTimingControlOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*CDREngineUnlockedIntrMask)(ThaModuleCdr self, ThaCdrController controller);

    /* Offset for timing control registers are changed now */
    uint32 (*TimingCtrlDefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingCtrlDe2De1Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingCtrlVcDe1Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingCtrlVcDe3Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingCtrlDe3Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingCtrlVcOffset)(ThaModuleCdr self, ThaCdrController controller);

    /* Offset for timing status registers */
    uint32 (*TimingStateDefaultOffset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingStateDe2De1Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingStateVcDe1Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingStateVcDe3Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingStateDe3Offset)(ThaModuleCdr self, ThaCdrController controller);
    uint32 (*TimingStateVcOffset)(ThaModuleCdr self, ThaCdrController controller);

    /* Module activating (some products do not require engine activating) */
    eBool (*NeedActivateRxEngine)(ThaModuleCdr self);
    eBool (*NeedActivateTxEngine)(ThaModuleCdr self);

    /* For debugging */
    void (*SdhChannelRegsShow)(ThaModuleCdr self, AtSdhChannel sdhChannel);
    void (*PdhChannelRegsShow)(ThaModuleCdr self, AtPdhChannel pdhChannel);
    ThaCdrDebugger (*DebuggerObjectCreate)(ThaModuleCdr self);

    eBool (*DcrShaperIsSupported)(ThaModuleCdr self, ThaCdrController controller);
    eBool (*ShouldEnableJitterAttenuator)(ThaModuleCdr self);

    /* Backward compatible */
    uint32 (*StartVersionSupportInterrupt)(ThaModuleCdr self);

    eBool (*Vc1xCdrControllerShouldInitAfterCreate)(ThaModuleCdr self, AtSdhVc vc);

    /* For channelized BERT */
    eAtRet (*ChannelizeDe3)(ThaModuleCdr self, AtPdhDe3 de3, eBool channelized);
    eAtRet (*HoVcUnChannelize)(ThaModuleCdr self, AtSdhChannel hoVc);
    eAtRet (*HoVcChannelizeRestore)(ThaModuleCdr self, AtSdhChannel hoVc);
    }tThaModuleCdrMethods;

typedef struct tThaModuleCdr
    {
    tAtModule super;

    /* Methods of CDR module */
    const tThaModuleCdrMethods  *methods;
    ThaCdrDebugger debugger;
    }tThaModuleCdr;

typedef struct tThaModuleCdrStmMethods
    {
    AtIpCore (*CoreOfSts)(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId);
    eAtRet (*PartDefaultSet)(ThaModuleCdr self, uint8 partId);
    uint8 (*MaxNumSts)(ThaModuleCdr self);

    uint32 (*EngineTimingOffsetByHwSts)(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId);
    eAtRet (*Vc3CepModeEnable)(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable);
    eAtRet (*LineModePayloadSet)(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType);
    eAtRet (*StsPldSet)(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType);
    eAtRet (*De3PldSet)(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType);
    eAtRet (*De3UnChannelizedModeSet)(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwStsId, eBool isUnChannelized);
    eAtRet (*VtgPldSet)(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType);
    eAtRet (*De2PldSet)(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType);
    uint32 (*SliceOffset)(ThaModuleCdr self, AtSdhChannel channel, uint8 slice);

    /* Register */
    uint32 (*StsTimingControlReg)(ThaModuleCdrStm self, AtSdhChannel channel);
    uint32 (*StsCdrChannelIdMask)(ThaModuleCdrStm self);
    uint8  (*StsCdrChannelIdShift)(ThaModuleCdrStm self);
    uint32 (*VcEngineTimingCtrlRegister)(ThaModuleCdrStm self, AtSdhChannel vc);

    uint32 (*VtTimingControlReg)(ThaModuleCdrStm self);
    uint32 (*VtCdrChannelIdMask)(ThaModuleCdrStm self);
    uint8  (*VtCdrChannelIdShift)(ThaModuleCdrStm self);
    }tThaModuleCdrStmMethods;

typedef struct tThaModuleCdrStm
    {
    tThaModuleCdr super;
    const tThaModuleCdrStmMethods *methods;
    }tThaModuleCdrStm;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtModule ThaModuleCdrObjectInit(AtModule module, AtDevice device);
AtModule ThaModuleCdrStmObjectInit(AtModule self, AtDevice device);

AtIterator ThaModuleCdrRegisterIteratorCreate(AtModule module);
AtIterator ThaModuleCdrStmRegisterIteratorCreate(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECDRINTERNAL_H_ */


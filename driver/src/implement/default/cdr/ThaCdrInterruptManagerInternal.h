/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaCdrInterruptManagerInternal.h
 * 
 * Created Date: Dec 18, 2015
 *
 * Description : Default CDR interrupt management representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACDRINTERRUPTMANAGERINTERNAL_H_
#define _THACDRINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/intrcontroller/ThaInterruptManagerInternal.h"
#include "ThaCdrInterruptManager.h"
#include "controllers/ThaCdrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCdrInterruptManagerMethods
    {
    ThaCdrController (*AcrDcrControllerFromHwIdGet)(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt);
    uint32 (*InterruptOffset)(ThaCdrInterruptManager self, ThaCdrController controller);
    }tThaCdrInterruptManagerMethods;

typedef struct tThaCdrInterruptManager
    {
    tThaInterruptManager super;
    const tThaCdrInterruptManagerMethods * methods;
    }tThaCdrInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager ThaCdrInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THACDRINTERRUPTMANAGERINTERNAL_H_ */


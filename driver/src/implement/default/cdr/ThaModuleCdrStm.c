/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaModuleCdrStm.c
 *
 * Created Date: Nov 13, 2012
 *
 * Description : CDR concrete module for STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../generic/pdh/AtPdhDe3Internal.h"
#include "../man/ThaDeviceInternal.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "../sdh/ThaModuleSdh.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaModuleSdh.h"
#include "../pdh/ThaPdhDe2De1.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../generic/pdh/AtPdhDe3Internal.h"
#include "ThaModuleCdrInternal.h"
#include "ThaModuleCdrStmReg.h"
#include "ThaModuleCdrStm.h"
#include "ThaModuleCdrInternal.h"
#include "controllers/ThaCdrControllerInternal.h"
#include "AtPdhDe2.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaNumVtgInSts  7

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaModuleCdrStm)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleCdrStmMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;
static tThaModuleCdrMethods m_ThaModuleCdrOverride;

/* Save super implementations */
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tThaModuleCdrMethods *m_ThaModuleCdrMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet SdhVcChannelizeRestore(ThaModuleCdr self, AtSdhChannel channel);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ThaRegCDRStsTimeCtrl(ThaModuleCdr self, AtSdhChannel sdhChannel)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)self;
    return mMethodsGet(moduleCdr)->StsTimingControlReg(moduleCdr, sdhChannel);
    }

static uint32 ThaRegCDRVtTimeCtrl(ThaModuleCdr self)
    {
    ThaModuleCdrStm moduleCdr = (ThaModuleCdrStm)self;
    return mMethodsGet(moduleCdr)->VtTimingControlReg(moduleCdr);
    }

static uint8 NumStsInOneSlice(ThaModuleCdr self)
    {
    ThaModuleOcn moduleOcn = (ThaModuleOcn)AtDeviceModuleGet((AtModuleDeviceGet((AtModule)self)), cThaModuleOcn);
    return ThaModuleOcnNumStsInOneSlice(moduleOcn);
    }

static eAtRet PartDefaultSet(ThaModuleCdr self, uint8 partId)
    {
    uint16 stsId, vtId;
    uint32 partOffset = ThaModuleCdrPartOffset(self, partId);
    uint8 numSts = NumStsInOneSlice(self);
    uint32 numVt = numSts * 28UL;

    /* Reset CDR STS Timing control */
    for (stsId = 0; stsId < numSts; stsId++)
        mModuleHwWrite(self, ThaRegCDRStsTimeCtrl(self, NULL) + stsId + partOffset, 0);

    /* Reset CDR VT Timing control */
    for (vtId = 0; vtId < numVt; vtId++)
        mModuleHwWrite(self, ThaRegCDRVtTimeCtrl(self) + vtId + partOffset, 0);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleCdr self)
    {
    uint8 part_i;
    eAtRet ret = m_ThaModuleCdrMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    for (part_i = 0; part_i < ThaModuleCdrNumParts(self); part_i++)
        ret |= mMethodsGet(mThis(self))->PartDefaultSet(self, part_i);

    return ret;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleCdrStmRegisterIteratorCreate(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static AtIpCore CoreOfSts(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId)
    {
    AtUnused(channel);
	AtUnused(stsId);
    return AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet((AtModule)self));
    }

static uint32 SdhVcCdrIdCalculate(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(slice);
    return ((hwStsId * 32UL) + (vtgId * 4UL) + vtId);
    }

uint32 ThaModuleCdrStmEngineIdOfHoVc(ThaModuleCdr self, AtSdhVc vc)
    {
    AtSdhChannel channel = (AtSdhChannel)vc;
    uint8 slice, hwSts;

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleCdr, &slice, &hwSts);
    return ThaModuleCdrStmSdhVcCdrIdCalculate(self, channel, slice, hwSts, 0, 0);
    }

static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    return ThaHoVcCdrControllerNew(ThaModuleCdrStmEngineIdOfHoVc(self, vc), (AtChannel)vc);
    }

uint32 ThaModuleCdrStmEngineIdOfVc1x(ThaModuleCdr self, AtSdhVc vc)
    {
    AtSdhChannel channel = (AtSdhChannel)vc;
    uint8 slice, hwSts;

    ThaSdhChannel2HwMasterStsId(channel, cThaModuleCdr, &slice, &hwSts);
    return ThaModuleCdrStmSdhVcCdrIdCalculate(self, channel, slice, hwSts, AtSdhChannelTug2Get(channel), AtSdhChannelTu1xGet(channel));
    }

static ThaCdrController Tu3VcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    return ThaTu3VcCdrControllerNew(ThaModuleCdrStmEngineIdOfVc1x(self, vc), (AtChannel)vc);
    }

static ThaCdrController Vc1xCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    return ThaVc1xCdrControllerNew(ThaModuleCdrStmEngineIdOfVc1x(self, vc), (AtChannel)vc);
    }

static ThaCdrController VcDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    return ThaVcDe1CdrControllerNew(ThaModuleCdrStmEngineIdOfDe1(self, de1), (AtChannel)de1);
    }

static ThaCdrController VcDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    AtSdhVc vc = AtPdhChannelVcInternalGet((AtPdhChannel)de3);
    return ThaVcDe3CdrControllerNew(ThaModuleCdrStmEngineIdOfHoVc(self, vc), (AtChannel)de3);
    }

uint32 ThaModuleCdrStmEngineIdOfDe3(ThaModuleCdr self, AtPdhDe3 de3)
    {
    uint8 slice, hwSts;

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cThaModuleCdr, &slice, &hwSts);
    return ThaModuleCdrStmSdhVcCdrIdCalculate(self, NULL, slice, hwSts, 0, 0);
    }

static ThaCdrController De3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    return ThaVcDe3CdrControllerNew(ThaModuleCdrStmEngineIdOfDe3(self, de3), (AtChannel)de3);
    }

static uint8 MaxNumSts(ThaModuleCdr self)
    {
    AtUnused(self);
    return 12;
    }

static uint32 EngineTimingCtrlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return cThaRegCDREngTimeCtrl;
    }

static uint32 EngineTimingOffsetByHwSts(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    uint32 cdrId = ThaModuleCdrStmSdhVcCdrIdCalculate(self, channel, slice, hwStsId, vtgId, vtId);
    uint32 partOffset = ThaModuleCdrPartOffset(self, ThaModuleSdhPartOfChannel(channel));
    uint32 sliceOffset = ThaModuleCdrStmSliceOffset(self, channel, slice);

    return sliceOffset + cdrId + partOffset;
    }

static uint32 StsTimingControlReg(ThaModuleCdrStm self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaRegCDRStsTimeCtrl;
    }

static uint32 StsCdrChannelIdMask(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cThaVC3CDRChidMask;
    }

static uint8 StsCdrChannelIdShift(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cThaVC3CDRChidShift;
    }

static uint32 VtTimingControlReg(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cThaRegCDRVtTimeCtrl;
    }

static uint32 VtCdrChannelIdMask(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cThaVTCDRChidMask;
    }

static uint8 VtCdrChannelIdShift(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cThaVTCDRChidShift;
    }

static uint16 LongReadForSts(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtModule module = (AtModule)self;
    AtIpCore core = mMethodsGet((ThaModuleCdrStm)self)->CoreOfSts(self, channel, stsId);
    return mMethodsGet(module)->HwLongReadOnCore(module, localAddress, dataBuffer, bufferLen, core);
    }

static uint16 LongReadForDe3(ThaModuleCdr self, AtPdhChannel channel, uint8 stsId, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtModule module = (AtModule)self;
    AtIpCore core = mMethodsGet((ThaModuleCdrStm)self)->CoreOfSts(self, NULL, stsId);
    AtUnused(channel);
    return mMethodsGet(module)->HwLongReadOnCore(module, localAddress, dataBuffer, bufferLen, core);
    }

static uint16 LongWriteForDe3(ThaModuleCdr self, AtPdhChannel channel, uint8 stsId, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen)
    {
    AtModule module = (AtModule)self;
    AtIpCore core = mMethodsGet((ThaModuleCdrStm)self)->CoreOfSts(self, NULL, stsId);
    AtUnused(channel);
    return mMethodsGet(module)->HwLongWriteOnCore(module, localAddress, dataBuffer, bufferLen, core);
    }

static uint16 LongWriteForSts(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen)
    {
    AtModule module = (AtModule)self;
    AtIpCore core = mMethodsGet((ThaModuleCdrStm)self)->CoreOfSts(self, channel, stsId);
    return mMethodsGet(module)->HwLongWriteOnCore(module, localAddress, dataBuffer, bufferLen, core);
    }

static eAtRet Vc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    uint32 longRegValue[cThaLongRegMaxSize];
    uint32 regAddr;

    AtUnused(slice);

    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + ThaModuleCdrPartOffset(self, ThaModuleSdhPartOfChannel(channel));
    LongReadForSts(self, channel, hwSts, regAddr, longRegValue, cThaLongRegMaxSize);
    mFieldIns(&longRegValue[cThaStsVC3MdDwIndex],
              cThaStsVC3MdMask(hwSts),
              cThaStsVC3MdShift(hwSts),
              mBoolToBin(enable));
    LongWriteForSts(self, channel, hwSts, regAddr, longRegValue, cThaLongRegMaxSize);

    return cAtOk;
    }

static eBool StsIsValid(ThaModuleCdr self, uint8 stsId)
    {
    uint8 maxNumSts = mMethodsGet(mThis(self))->MaxNumSts(self);
    return (stsId < maxNumSts) ? cAtTrue : cAtFalse;
    }

static eAtRet LineModePayloadSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType)
    {
    uint32 longRegValue[cThaLongRegMaxSize];
    uint32 regAddr;
    uint8 hwPayloadType = (payloadType == cThaOcnVc3PldDs3) ? 0x1 : 0x0;
    AtPdhChannel pdhChannel = (AtPdhChannel)AtSdhChannelMapChannelGet(channel);
    if (pdhChannel != NULL)
        hwPayloadType = AtPdhDe3IsE3((AtPdhDe3)pdhChannel) ? 0 : 1;

    AtUnused(slice);

    if (!StsIsValid(self, hwStsId))
        return cAtErrorInvlParm;

    if (hwStsId / 4 >= cThaLongRegMaxSize)
        return cAtErrorInvlParm;

    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + ThaModuleCdrPartOffset(self, ThaModuleSdhPartOfChannel(channel));
    LongReadForSts(self, channel, hwStsId, regAddr, longRegValue, cThaLongRegMaxSize);
    mFieldIns(&longRegValue[hwStsId / 4],
              cThaStsStsMdMask(hwStsId),
              cThaStsStsMdShift(hwStsId),
              ((payloadType == cThaOcnVc3Pld7Tug2) ? 0x0 : 0x1));
    mFieldIns(&longRegValue[3],
              cThaStsDE3ModMask(hwStsId),
              cThaStsDE3ModShift(hwStsId),
              hwPayloadType);
    LongWriteForSts(self, channel, hwStsId, regAddr, longRegValue, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtRet De3PldSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType)
    {
    uint32 longRegValue[cThaLongRegMaxSize];
    uint32 regAddr;

    AtUnused(slice);

    if (!StsIsValid(self, hwStsId))
        return cAtErrorInvlParm;

    if (hwStsId / 4 >= cThaLongRegMaxSize)
        return cAtErrorInvlParm;

    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + ThaModuleCdrPartOffset(self, ThaPdhDe3PartId((ThaPdhDe3)channel));
    LongReadForDe3(self, channel, hwStsId, regAddr, longRegValue, cThaLongRegMaxSize);
    mFieldIns(&longRegValue[hwStsId / 4],
              cThaStsStsMdMask(hwStsId),
              cThaStsStsMdShift(hwStsId),
              0x1);
    mFieldIns(&longRegValue[3],
              cThaStsDE3ModMask(hwStsId),
              cThaStsDE3ModShift(hwStsId),
              (payloadType == cThaOcnVc3PldDs3) ? 0x1 : 0x0);
    LongWriteForDe3(self, channel, hwStsId, regAddr, longRegValue, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtRet De3UnChannelizedModeSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwStsId, eBool isUnChannelized)
    {
    uint32 longRegValue[cThaLongRegMaxSize];
    uint32 regAddr;
    uint32 hwVal = isUnChannelized ? 1 : 0;

    AtUnused(slice);

    if (!StsIsValid(self, hwStsId))
        return cAtErrorInvlParm;

    if (hwStsId / 4 >= cThaLongRegMaxSize)
        return cAtErrorInvlParm;

    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + ThaModuleCdrPartOffset(self, ThaPdhDe3PartId((ThaPdhDe3)channel));
    LongReadForDe3(self, channel, hwStsId, regAddr, longRegValue, cThaLongRegMaxSize);
    mFieldIns(&longRegValue[hwStsId / 4],
              cThaStsStsMdMask(hwStsId),
              cThaStsStsMdShift(hwStsId),
              hwVal);
    LongWriteForDe3(self, channel, hwStsId, regAddr, longRegValue, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtRet VtgPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr;

    AtUnused(slice);

    if (!StsIsValid(self, hwSts))
        return cAtErrorInvlParm;

    if (hwSts / 4 >= cThaLongRegMaxSize)
        return cAtErrorInvlParm;

    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) + ThaModuleCdrPartOffset(self, ThaModuleSdhPartOfChannel(channel));
    LongReadForSts(self, channel, hwSts, regAddr, pdRegVal, cThaLongRegMaxSize);
    mFieldIns(&pdRegVal[hwSts / 4],
              cThaStsVtTypeMask(hwSts, vtgId),
              cThaStsVtTypeShift(hwSts, vtgId),
              ((payloadType == cThaOcnVtgTug2PldVt15) ? 0x1 : 0x0));
    LongWriteForSts(self, channel, hwSts, regAddr, pdRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static uint32 AdjustStateStatusRegister(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return 0x2A0800;
    }

static uint32 SliceOffset(ThaModuleCdr self, AtSdhChannel channel, uint8 slice)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(slice);

    return 0;
    }

static uint32 De2De1DefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtPdhChannel de1 = (AtPdhChannel)ThaCdrControllerChannelGet(controller);
    AtPdhChannel de3 = AtPdhChannelParentChannelGet(AtPdhChannelParentChannelGet(de1));
    uint8 slice, sts;
    uint32 sliceOffset;
    uint32 flatId = ThaCdrControllerChannelIdFlat(controller, ThaCdrControllerChannelGet(controller));

    ThaPdhChannelHwIdGet(de3, cThaModuleCdr, &slice, &sts);
    sliceOffset = ThaModuleCdrStmSliceOffset(self, (AtSdhChannel)AtPdhChannelVcInternalGet(de3), slice);

    return sliceOffset + flatId + ThaCdrControllerPartOffset(controller);
    }

static ThaCdrDebugger DebuggerObjectCreate(ThaModuleCdr self)
    {
    AtUnused(self);
    return ThaStmCdrDebuggerNew();
    }

static eAtRet De2PldSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(slice);
    AtUnused(hwSts);
    AtUnused(vtgId);
    AtUnused(payloadType);

    return cAtOk;
    }

/* Default length should not JUMBO frame */
static uint16 LineTypeToDefaultLength(uint8 lineType)
    {
    switch(lineType)
        {
        case 0:
            /* E1 */
            return 256 * 8;
        case 1:
            /* DS1 */
            return 192 * 8;
        case 2:
            /* VT-2 */
            return 140 * 8;
        case 3:
            /* VT-1.5 */
            return 104 * 8;
        case 4:
            /* E3 */
            return 4296;
        case 5:
            /* DS3 */
            return 5592;
        case 6:
            /* STS1 */
            return 783 * 8;
        case 7:
            /* VC4 */
            return 783 * 8;
        case 8:
            /* VC4_4c */
            return 783 * 8;
        default:
            return 0;
        }
    }

/* This function set lineType & default length for CDR engine
 * Note:
 *    - Bit 3 of lineType indicate Vc4-4c or TU3 mode
 *    - Bit 2:0 of lineType indicate the line type */
static eAtRet EngineLineTypeSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId, uint8 lineType)
    {
    uint32 regAddr, regVal;
    uint16 defaultLenght = LineTypeToDefaultLength(lineType);
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    /* For products that do not have PW feature, this function just does nothing */
    if (AtDeviceModuleGet(device, cAtModulePw) == NULL)
        return cAtOk;

    regAddr = mMethodsGet(self)->EngineTimingCtrlRegister(self) +
              mMethodsGet(mThis(self))->EngineTimingOffsetByHwSts(self, channel, slice, hwStsId, vtgId, vtId);

    regVal = mModuleHwRead(self, regAddr);

    /* Bit 3 of lineType indicate Vc4-4c or TU3 mode
     * This bit is defined for vc4-4c but HW recommended, it is also used for TU3-VC3 */
    mFieldIns(&regVal, cThaPktLenExtenMask, cThaPktLenExtenShift, 0x0); /* Jumbo frame length extend bit */
    mFieldIns(&regVal, cThaVc4_4cMdMask, cThaVc4_4cMdShift, (lineType >> 3));

    /* Bit 2:0 of lineType indicate the line type */
    mFieldIns(&regVal, cThaLineTypeMask, cThaLineTypeShift, lineType & cBit2_0);

    /* For products that require channelized PRBS to be supported, do not need
     * to set default CDR packet length when timing mode is not ACR/DCR. Doing so
     * will be hard to make the channelized BERT feature be hard to implement and
     * it is not necessary indeed. */
    if (!AtModulePrbsChannelizedPrbsAllowed((AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs)))
        mFieldIns(&regVal, cThaPktLenMask, cThaPktLenShift, defaultLenght);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint8 VtgPayloadTypeToLineType(eThaOcnVtgTug2PldType payloadType)
    {
    if (payloadType == cThaOcnVtgTug2PldE1)   return 0;
    if (payloadType == cThaOcnVtgTug2PldDs1)  return 1;
    if (payloadType == cThaOcnVtgTug2PldVt15) return 3;
    if (payloadType == cThaOcnVtgTug2PldVt2)  return 2;

    return 0;
    }

static eAtRet HwStsCdrVtgPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    eAtRet ret;
    uint8  vtId;

    ret = mMethodsGet(mThis(self))->VtgPldSet(self, channel, slice, hwSts, vtgId, payloadType);
    if (ret != cAtOk)
        return ret;

    for (vtId = 0; vtId < 4; vtId++)
        {
        ret = EngineLineTypeSet(self, channel, slice, hwSts, vtgId, vtId, VtgPayloadTypeToLineType(payloadType));
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static uint8 Vc3PayloadTypeToLineType(eThaOcnVc3PldType payloadType)
    {
    if (payloadType == cThaOcnVc3PldC3)       return 6;
    if (payloadType == cThaOcnVc3PldDs3)      return 5;
    if (payloadType == cThaOcnVc3PldE3)       return 4;

    /* - Bit 3 of lineType indicate Vc4-4c or TU3 mode
       - Bit 2:0 of lineType indicate the line type */
    if (payloadType == cThaOcnTu3Vc3Pld)      return 0xe;

    return 0;
    }

static eAtRet StsPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType)
    {
    uint8  vtgId;
    eAtRet ret;
    AtPdhDe3 pdhChannel;
    eThaOcnVtgTug2PldType defaultTug2PldType;

    ret = mMethodsGet(mThis(self))->LineModePayloadSet(self, channel, slice, hwStsId, payloadType);
    if (ret != cAtOk)
        return ret;

    /* If mapping type is not TUG-2, registers of TUG-2s also need to be reset
     * to default value even when they are not used */
    if (payloadType == cThaOcnVc3Pld7Tug2)
        return cAtOk;

    pdhChannel = (AtPdhDe3)AtSdhChannelMapChannelGet(channel);
    defaultTug2PldType = cThaOcnVtgTug2PldVt15;

    if (pdhChannel && AtPdhDe3IsChannelized(pdhChannel))
        defaultTug2PldType = AtPdhDe3IsE1Channelized(pdhChannel) ? cThaOcnVtgTug2PldE1 : cThaOcnVtgTug2PldDs1;

    /* Reset to default */
    for (vtgId = 0; vtgId < cThaNumVtgInSts; vtgId++)
        HwStsCdrVtgPldSet(self, channel, slice, hwStsId, vtgId, defaultTug2PldType);

    return EngineLineTypeSet(self, channel, slice, hwStsId, 0, 0, Vc3PayloadTypeToLineType(payloadType));
    }

static eAtRet ChannelizeDe3(ThaModuleCdr self, AtPdhDe3 de3, eBool channelized)
    {
    AtSdhChannel channel = (AtSdhChannel)AtPdhChannelVcGet((AtPdhChannel)de3);
    uint8 hwSlice, hwSts, lineType;
    uint32 regAddr, regVal;

    if (channel == NULL)
        return cAtErrorNullPointer;

    ThaSdhChannelHwStsGet(channel, cThaModuleCdr, AtSdhChannelSts1Get(channel), &hwSlice, &hwSts);
    regAddr = mMethodsGet(self)->EngineTimingCtrlRegister(self) +
              mMethodsGet(mThis(self))->EngineTimingOffsetByHwSts(self, channel, hwSlice, hwSts, 0, 0);
    regVal = mModuleHwRead(self, regAddr);

    if (channelized)
        {
        uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de3);
        lineType = (frameType == cAtPdhDs3FrmCbitChnl28Ds1s) ? 1 : 0;
        }
    else
        lineType = Vc3PayloadTypeToLineType(AtPdhDe3IsDs3(de3) ? cThaOcnVc3PldDs3 : cThaOcnVc3PldE3);

    mFieldIns(&regVal, cThaLineTypeMask, cThaLineTypeShift, lineType);

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet De3ChannelizedRestore(ThaModuleCdr self, AtPdhChannel de3)
    {
    uint8 numDe2s = AtPdhChannelNumberOfSubChannelsGet(de3);
    uint8 de2_i;
    eAtRet ret = cAtOk;

    AtUnused(self);

    for (de2_i = 0; de2_i < numDe2s; de2_i++)
        {
        uint8 numDe1s, de1_i;
        AtPdhChannel de2;

        de2 = AtPdhChannelSubChannelGet(de3, de2_i);
        if (de2 == NULL)
            continue;

        numDe1s = AtPdhChannelNumberOfSubChannelsGet(de2);
        for (de1_i = 0; de1_i < numDe1s; de1_i++)
            {
            AtPdhChannel de1;
            ThaCdrController cdrController;

            de1 = AtPdhChannelSubChannelGet(de2, de1_i);
            if (de1 == NULL)
                continue;

            cdrController = ThaPdhDe1CdrControllerGet((ThaPdhDe1)de1);
            if (cdrController)
                ret |= ThaCdrControllerChannelMapTypeSet(cdrController);
            }
        }

    return ret;
    }

static eAtRet Vc3MappingRestore(ThaModuleCdr self, AtSdhChannel vc3)
    {
    eAtRet ret = cAtOk;
    uint8 mapType;

    ret |= ThaCdrStsPldSet(self, vc3, AtSdhChannelSts1Get(vc3), ThaSdhVcOcnVc3PldType(vc3, AtSdhChannelMapTypeGet(vc3)));
    ret |= SdhVcChannelizeRestore(self, vc3);

    mapType = AtSdhChannelMapTypeGet(vc3);
    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        {
        ThaPdhDe3 de3 = (ThaPdhDe3)AtSdhChannelMapChannelGet(vc3);
        ThaCdrController cdr = ThaPdhDe3CdrControllerGet(de3);
        eBool shouldChannelized = (eBool)((AtChannelBoundPwGet((AtChannel)de3) != NULL) ? cAtFalse : AtPdhDe3IsChannelized((AtPdhDe3)de3));
        if (cdr)
            ret |= ThaCdrControllerChannelMapTypeSet(cdr);

        ret |= De3ChannelizedRestore(self, (AtPdhChannel)de3);
        ret |= ThaModuleCdrChannelizeDe3(self, (AtPdhDe3)de3, shouldChannelized);
        }

    if (AtChannelBoundPwGet((AtChannel)vc3))
        ret |= ThaCdrVc3CepModeEnable(self, vc3, AtSdhChannelSts1Get(vc3), AtSdhVcIsLoVc((AtSdhVc)vc3));

    return ret;
    }

static eAtRet Tug3MappingRestore(ThaModuleCdr self, AtSdhChannel tug3)
    {
    eAtRet ret = cAtOk;

    ret |= ThaOcnTug3PldSet(tug3, AtSdhChannelMapTypeGet(tug3));

    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        {
        AtSdhChannel vc3 = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(tug3, 0), 0);
        ret |= Vc3MappingRestore(self, vc3);
        }
    else
        ret |= SdhVcChannelizeRestore(self, tug3);

    return ret;
    }

static eAtRet Vc1xChannelizeRestore(ThaModuleCdr self, AtSdhChannel vc1x)
    {
    uint8 mapType = AtSdhChannelMapTypeGet(vc1x);
    return ThaCdrVtPldSet(self, vc1x, mapType);
    }

static eAtRet SdhVcChannelizeRestore(ThaModuleCdr self, AtSdhChannel channel)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint8 subChannel_i;
    eAtRet ret = cAtOk;
    uint32 channelType = AtSdhChannelTypeGet(channel);

    if (AtSdhChannelIsVc(channel))
        {
        ThaCdrController controller = ThaSdhVcCdrControllerGet((AtSdhVc)channel);
        eAtTimingMode timingMode = AtChannelTimingModeGet((AtChannel)channel);
        AtChannel timingSource = AtChannelTimingSourceGet((AtChannel)channel);
        ret |= ThaCdrControllerTimingSourceSet(controller, timingMode, timingSource);
        }

    if ((channelType == cAtSdhChannelTypeVc11) || (channelType == cAtSdhChannelTypeVc12))
        ret |= Vc1xChannelizeRestore(self, channel);

    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, subChannel_i);

        if (subChannel == NULL)
            continue;

        channelType = AtSdhChannelTypeGet(subChannel);

        switch (channelType)
            {
            case cAtSdhChannelTypeVc3:
                ret |= Vc3MappingRestore(self, subChannel);
                break;

            case cAtSdhChannelTypeTug3:
                ret |= Tug3MappingRestore(self, subChannel);
                break;

            default:
                ret |= SdhVcChannelizeRestore(self, subChannel);
            }
        }

    if ((numSubChannels == 0) && (channelType == cAtSdhChannelTypeVc3) && (AtSdhChannelMapTypeGet(channel) == cAtSdhVcMapTypeVc3MapDe3))
        {
        ThaPdhDe3 de3 = (ThaPdhDe3)AtSdhChannelMapChannelGet(channel);
        eBool shouldChannelized = (eBool)((AtChannelBoundPwGet((AtChannel)de3) != NULL) ? cAtFalse : AtPdhDe3IsChannelized((AtPdhDe3)de3));
        ret |= ThaModuleCdrChannelizeDe3(self, (AtPdhDe3)de3, shouldChannelized);
        }

    return ret;
    }

static eAtRet HoVcChannelizeRestore(ThaModuleCdr self, AtSdhChannel hoVc)
    {
    return SdhVcChannelizeRestore(self, hoVc);
    }

static eAtRet HoVcUnChannelize(ThaModuleCdr self, AtSdhChannel hoVc)
    {
    uint32 vcType = AtSdhChannelTypeGet(hoVc);
    eAtRet ret = cAtOk;

    if (vcType == cAtSdhChannelTypeVc4)
        {
        AtChannel channel = (AtChannel)hoVc;
        ThaCdrController cdr = ThaSdhVcCdrControllerGet((AtSdhVc)hoVc);

        ret |= ThaCdrVC4xPldSet(self, hoVc);

        /* Re-apply timing configuration so that all of STSs will have the same
         * timing source configuration */
        ret |= ThaCdrControllerTimingSourceSet(cdr, AtChannelTimingModeGet(channel), AtChannelTimingSourceGet(channel));
        ret |= ThaCdrControllerChannelMapTypeSet(cdr);
        return ret;
        }

    if (vcType == cAtSdhChannelTypeVc3)
        {
        uint8 stsId = AtSdhChannelSts1Get(hoVc);
        ThaCdrController cdr = ThaSdhVcCdrControllerGet((AtSdhVc)hoVc);

        if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(hoVc)) == cAtSdhChannelTypeTu3)
            {
            ret |= ThaCdrStsPldSet(self, hoVc, stsId, cThaOcnTu3Vc3Pld);
            ret |= ThaCdrVc3CepModeEnable(self, hoVc, stsId, cAtFalse);
            ret |= ThaCdrControllerChannelMapTypeSet(cdr);
            }
        if (AtSdhChannelMapTypeGet(hoVc) == cAtSdhVcMapTypeVc3MapDe3)
            {
            ThaPdhDe3 de3 = (ThaPdhDe3)AtSdhChannelMapChannelGet(hoVc);
            if (!AtChannelBoundPwGet((AtChannel)de3))
                {
                ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)de3), cThaModuleCdr);
                ret |= ThaModuleCdrChannelizeDe3(moduleCdr, (AtPdhDe3)de3, cAtFalse);
                }
            }
        }

    return ret;
    }

static uint32 TimingCtrlDe2De1Offset(ThaModuleCdr self, ThaCdrController controller)
    {
    uint8 slice, hwStsId, de2Id, de1Id;
    AtPdhChannel de1 = (AtPdhChannel)ThaCdrControllerChannelGet(controller);
    uint32 sliceOffset;

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)de1, &slice, &hwStsId, &de2Id, &de1Id, cThaModuleCdr);
    sliceOffset = ThaModuleCdrStmSliceOffset(self, (AtSdhChannel)AtPdhChannelVcInternalGet(de1), slice);
    return sliceOffset + (uint32)(hwStsId << 5) + (uint32)(de2Id << 2) + (uint32)de1Id;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, DefaultSet);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, Tu3VcCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, Vc1xCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, VcDe1CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, VcDe3CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, De3CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, EngineTimingCtrlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, AdjustStateStatusRegister);
        mMethodOverride(m_ThaModuleCdrOverride, De2De1DefaultOffset);
        mMethodOverride(m_ThaModuleCdrOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModuleCdrOverride, ChannelizeDe3);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcChannelizeRestore);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcUnChannelize);
        mMethodOverride(m_ThaModuleCdrOverride, TimingCtrlDe2De1Offset);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    OverrideThaModuleCdr(self);
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, CoreOfSts);
        mMethodOverride(m_methods, PartDefaultSet);
        mMethodOverride(m_methods, MaxNumSts);
        mMethodOverride(m_methods, Vc3CepModeEnable);
        mMethodOverride(m_methods, EngineTimingOffsetByHwSts);
        mMethodOverride(m_methods, StsTimingControlReg);
        mMethodOverride(m_methods, StsCdrChannelIdMask);
        mMethodOverride(m_methods, StsCdrChannelIdShift);
        mMethodOverride(m_methods, VtTimingControlReg);
        mMethodOverride(m_methods, VtCdrChannelIdMask);
        mMethodOverride(m_methods, VtCdrChannelIdShift);
        mMethodOverride(m_methods, LineModePayloadSet);
        mMethodOverride(m_methods, StsPldSet);
        mMethodOverride(m_methods, De3PldSet);
        mMethodOverride(m_methods, De3UnChannelizedModeSet);
        mMethodOverride(m_methods, VtgPldSet);
        mMethodOverride(m_methods, SliceOffset);
        mMethodOverride(m_methods, De2PldSet);
        }

    mMethodsSet((ThaModuleCdrStm)self, &m_methods);
    }

static uint8 VtHwLineType(AtSdhChannel vc1x, eAtSdhVcMapType vcMapType)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(vc1x);

    if (vcMapType == cAtSdhVcMapTypeVc1xMapDe1)
        return (channelType == cAtSdhChannelTypeVc12) ? 0 : 1;

    return (channelType == cAtSdhChannelTypeVc12) ? 2 : 3;
    }

static eThaOcnVtgTug2PldType De2OcnPayloadType(eAtPdhDe2FrameType de2FrameType)
    {
    if (de2FrameType == cAtPdhDs2T1_107Carrying4Ds1s)
        return cThaOcnVtgTug2PldDs1;

    if ((de2FrameType == cAtPdhDs2G_747Carrying3E1s) ||
            (de2FrameType == cAtPdhE2G_742Carrying4E1s))
        return cThaOcnVtgTug2PldE1;

    return cThaOcnVtgTug2PldDs1;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleCdrStm);
    }

AtModule ThaModuleCdrStmObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleCdrStmNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleCdrStmObjectInit(newModule, device);
    }

eAtRet ThaCdrVtgPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    eAtRet ret;
    uint8  hwSts, slice;

    ret = ThaSdhChannelHwStsGet(channel, cThaModuleCdr, stsId, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    return HwStsCdrVtgPldSet(self, channel, slice, hwSts, vtgId, payloadType);
    }

eAtRet ThaCdrHwStsVc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eBool enable)
    {
    return mMethodsGet(mThis(self))->Vc3CepModeEnable(self, channel, slice, hwStsId, enable);
    }

eAtRet ThaCdrVc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, eBool enable)
    {
    uint8  hwSts, slice;
    eAtRet ret = ThaSdhChannelHwStsGet(channel, cThaModuleCdr, stsId, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    return ThaCdrHwStsVc3CepModeEnable(self, channel, slice, hwSts, enable);
    }

eAtRet ThaCdrHwStsPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType)
    {
    return mMethodsGet(mThis(self))->StsPldSet(self, channel, slice, hwStsId, payloadType);
    }

eAtRet ThaCdrDe3PldSet(ThaModuleCdr self, AtPdhChannel channel)
    {
    uint8 hwSts, slice;
    eThaOcnVc3PldType payloadType;
    AtSdhChannel sdhVc;
    eAtRet ret;

    ret = ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    if (AtPdhDe3IsE3((AtPdhDe3)channel))
        payloadType = cThaOcnVc3PldE3;
    else
        payloadType = cThaOcnVc3PldDs3;

    ret = mMethodsGet((ThaModuleCdrStm)self)->De3PldSet(self, channel, slice, hwSts, payloadType);
    if (ret != cAtOk)
        return ret;

    sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);
    return EngineLineTypeSet(self, sdhVc, slice, hwSts, 0, 0, Vc3PayloadTypeToLineType(payloadType));
    }

eAtRet ThaCdrDe3UnChannelizedModeSet(ThaModuleCdr self, AtPdhChannel channel, eBool isUnChannelized)
    {
    uint8 hwSts, slice;
    eAtRet ret;

    ret = ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    ret = mMethodsGet((ThaModuleCdrStm)self)->De3UnChannelizedModeSet(self, channel, slice, hwSts, isUnChannelized);

    return ret;
    }

eAtRet ThaCdrDe2PldSet(ThaModuleCdr self, AtPdhChannel channel, eAtPdhDe2FrameType de2FrameType)
    {
    uint8 hwSts, slice;
    uint8 vtgId = (uint8)AtChannelIdGet((AtChannel)channel);

    eAtRet ret = ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet((ThaModuleCdrStm)self)->De2PldSet(self, channel, slice, hwSts, vtgId, De2OcnPayloadType(de2FrameType));
    }

eAtRet ThaCdrDe1De2PldSet(ThaModuleCdr self, AtPdhChannel channel, uint32 e2ConvertedId, eAtPdhDe1FrameType de1FrameType)
    {
    uint8 hwSts, slice;
    eThaOcnVtgTug2PldType plType;
    eAtRet ret = ThaPdhChannelHwIdGet((AtPdhChannel)channel, cThaModuleCdr, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    if (AtPdhDe1FrameTypeIsE1(de1FrameType))
        plType = cThaOcnVtgTug2PldE1;
    else
        plType = cThaOcnVtgTug2PldDs1;

    return mMethodsGet((ThaModuleCdrStm)self)->De2PldSet(self, channel, slice, hwSts, (uint8)e2ConvertedId, plType);
    }

eAtRet ThaCdrStsPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 stsId, eThaOcnVc3PldType payloadType)
    {
    uint8 hwSts, slice;
    eAtRet ret = ThaSdhChannelHwStsGet(channel, cThaModuleCdr, stsId, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    return ThaCdrHwStsPldSet(self, channel, slice, hwSts, payloadType);
    }

static uint8 Vc4xCdrLineTypeGet(AtSdhChannel channel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);
    uint32 numSts;

    if ((channelType == cAtSdhChannelTypeVc4_4c)  ||
        (channelType == cAtSdhChannelTypeVc4_16c))
        return 0xF;

    if (channelType == cAtSdhChannelTypeVc4_64c)
        return 0xD; /* NxVC4-16c */

    /* VC-4nc still use same line type as standard concatenation */
    numSts = AtSdhChannelNumSts(channel);
    if ((channelType == cAtSdhChannelTypeVc4_nc) &&
        ((numSts == 12) || /* VC4-4c */
         (numSts == 48) || /* VC4-16c */
         (numSts == 24)))  /* VC4-8c */
        return 0xF;

    return 0x7;
    }

eAtRet ThaCdrVC4xPldSet(ThaModuleCdr self, AtSdhChannel sdhChannel)
    {
    eAtRet ret;
    uint8  sts_i;
    uint8 numSts;
    uint8 hwSts, hwSlice;

    numSts = AtSdhChannelNumSts(sdhChannel);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i);
        ret = ThaCdrStsPldSet(self, sdhChannel, stsId, cThaOcnVc3PldC3);
        if (ret != cAtOk)
            return ret;

        ThaCdrVc3CepModeEnable(self, sdhChannel, stsId, cAtFalse);
        }

    /* - Bit 3 of lineType indicate Vc4-4c or TU3 mode
       - Bit 2:0 of lineType indicate the line type */
    ret = ThaSdhChannelHwStsGet(sdhChannel, cThaModuleCdr, AtSdhChannelSts1Get(sdhChannel), &hwSlice, &hwSts);
    if (ret != cAtOk)
        return ret;

    ret = EngineLineTypeSet(self, sdhChannel, hwSlice, hwSts, 0, 0, Vc4xCdrLineTypeGet(sdhChannel));
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

eAtRet ThaCdrVc3CepPldSet(ThaModuleCdr self, AtSdhChannel vc3)
    {
    uint8 stsId = AtSdhChannelSts1Get(vc3);
    eBool isVc3Pld = AtSdhVcStuffIsEnabled((AtSdhVc)vc3);
    if (isVc3Pld == cAtFalse)
        return ThaCdrStsPldSet(self, vc3, stsId, cThaOcnTu3Vc3Pld);

    return ThaCdrStsPldSet(self, vc3, stsId, cThaOcnVc3PldC3);
    }

eAtSdhVcMapType ThaCdrVtPldGet(ThaModuleCdr self, AtSdhChannel vc1x)
    {
    uint32 address, value;
    uint8 hwPldMode;
    uint8 hwSts, hwSlice;
    ThaCdrController vcCdrController;

    ThaSdhChannel2HwMasterStsId(vc1x, cThaModuleCdr, &hwSlice, &hwSts);
    if (!StsIsValid(self, hwSts))
        return cAtErrorInvlParm;

    vcCdrController = ThaSdhVcCdrControllerGet((AtSdhVc)vc1x);
    address = ThaRegCDRVtTimeCtrl(self) + mMethodsGet(vcCdrController)->ChannelDefaultOffset(vcCdrController);
    value = mChannelHwRead(vc1x, address, cThaModuleCdr);
    hwPldMode = (uint8)mRegField(value, cThaMapVtMd);
    if (hwPldMode)
        return cAtSdhVcMapTypeVc1xMapDe1;

    return cAtSdhVcMapTypeVc1xMapC1x;
    }

eAtRet ThaCdrVtPldSet(ThaModuleCdr self, AtSdhChannel vc1x, eAtSdhVcMapType vcMapType)
    {
    uint32 address, value;
    uint8 hwPldMode;
    uint8 hwSts, slice;
    ThaCdrController vcCdrController;

    eAtRet ret = ThaSdhChannel2HwMasterStsId(vc1x, cThaModuleCdr, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    vcCdrController = ThaSdhVcCdrControllerGet((AtSdhVc)vc1x);
    address = ThaRegCDRVtTimeCtrl(self) + mMethodsGet(vcCdrController)->ChannelDefaultOffset(vcCdrController);
    value = mChannelHwRead(vc1x, address, cThaModuleCdr);
    hwPldMode = (vcMapType == cAtSdhVcMapTypeVc1xMapDe1) ? 1 : 0;
    mFieldIns(&value, cThaMapVtMdMask, cThaMapVtMdShift, hwPldMode);
    mChannelHwWrite(vc1x, address, value, cThaModuleCdr);

    return EngineLineTypeSet(self, vc1x, slice, hwSts, AtSdhChannelTug2Get(vc1x), AtSdhChannelTu1xGet(vc1x), VtHwLineType(vc1x, vcMapType));
    }

eAtRet ThaCdrDe1TimingControlMapVtModeSet(ThaModuleCdr self, AtPdhChannel de1)
    {
    uint32 address, value;
    uint8 hwPldMode;
    uint8 hwSts, slice;
    ThaCdrController cdrController;

    eAtRet ret = ThaPdhChannelHwIdGet(de1, cThaModuleCdr, &slice, &hwSts);
    if (ret != cAtOk)
        return ret;

    cdrController = ThaPdhDe1CdrControllerGet((ThaPdhDe1)de1);
    address = ThaRegCDRVtTimeCtrl(self) + mMethodsGet(cdrController)->ChannelDefaultOffset(cdrController);
    value = mChannelHwRead(de1, address, cThaModuleCdr);
    hwPldMode = 1;
    mFieldIns(&value, cThaMapVtMdMask, cThaMapVtMdShift, hwPldMode);
    mChannelHwWrite(de1, address, value, cThaModuleCdr);

    return cAtOk;
    }

uint32 ThaModuleCdrStmEngineIdOfDe1(ThaModuleCdr self, AtPdhDe1 de1)
    {
    AtSdhVc vc = AtPdhChannelVcInternalGet((AtPdhChannel)de1);
    return ThaModuleCdrStmEngineIdOfVc1x(self, vc);
    }

uint32 ThaModuleCdrStmSliceOffset(ThaModuleCdr self, AtSdhChannel channel, uint8 slice)
    {
    if (self)
        return mMethodsGet(mThis(self))->SliceOffset(self, channel, slice);

    return 0;
    }

uint32 ThaModuleCdrStmSdhVcCdrIdCalculate(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, uint8 vtgId, uint8 vtId)
    {
    if (self)
        return SdhVcCdrIdCalculate(self, channel, slice, hwStsId, vtgId, vtId);
    return cBit31_0;
    }

eAtRet ThaModuleCdrStmHoEngineLineTypeSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType)
    {
    return EngineLineTypeSet(self, channel, slice, hwStsId, 0, 0, Vc3PayloadTypeToLineType(payloadType));
    }

eAtRet ThaModuleCdrStmDe2PldSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    if (self)
        return mMethodsGet(mThis(self))->De2PldSet(self, channel, slice, hwSts, vtgId, payloadType);
    return cAtErrorNullPointer;
    }

uint32 ThaModuleCdrStmVtTimingControlReg(ThaModuleCdrStm self)
    {
    if (self)
        return mMethodsGet(mThis(self))->VtTimingControlReg(self);
    return cInvalidUint32;
    }

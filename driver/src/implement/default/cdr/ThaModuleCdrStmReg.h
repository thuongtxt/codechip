/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaModuleCdrStmReg.h
 * 
 * Created Date: Nov 13, 2012
 *
 * Description : STM CDR register definitions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECDRSTMREG_H_
#define _THAMODULECDRSTMREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------------
Reg Name: CDR line mode control
Reg Addr: 0x0280000
Reg Desc: This register is used to configure line mode for CDR engine.
------------------------------------------------------------------------------*/
#define cThaRegCDRlineMdCtrl                       0x280000

/*--------------------------------------
BitField Name: DE3mode
BitField Type: R/W
BitField Desc: 1: VC-3 CEP mapped to TU-3
               0: Other mode
--------------------------------------*/
#define cThaStsVC3MdMask(stsId)                         ((uint32)(cBit0 << (stsId + 16))) /* cBit123_112 */
#define cThaStsVC3MdShift(stsId)                        ((uint32)(stsId + 16))
#define cThaStsVC3MdDwIndex                             3

/*--------------------------------------
BitField Name: DE3Mode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsDE3ModMask(stsId)                        (cBit0 << (stsId)) /* cBit107_96 */
#define cThaStsDE3ModShift(stsId)                       (stsId)

/*--------------------------------------
BitField Name: STSSTSmode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsStsMdMask(stsId)                        (uint32)(cBit7 << (((stsId) % 4) * 8)) /* cBit95 */
#define cThaStsStsMdShift(stsId)                       (uint32)(7 + (((stsId) % 4) * 8))

/*--------------------------------------
BitField Name: STSVTType
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsVtTypeMask(stsId, vtgId)                ((uint32)(cBit0 << (((stsId) % 4)*8 + (vtgId)))) /* cBit94_88 */
#define cThaStsVtTypeShift(stsId, vtgId)               ((uint32)((stsId) % 4)*8 + (vtgId))

/*------------------------------------------------------------------------------
Reg Name: CDR VT Timing control
Reg Addr: 0x0280600 - 0x028077B
          The address format for these registers is 0x0280600 + STS*32 + TUG*4
          + VT
          Where: STS (0 - 11)
          TUG (0 -6)
          VT (0 -2) in E1 and (0 - 3) in DS1
Reg Desc: This register is used to configure timing mode for per VT
------------------------------------------------------------------------------*/
#undef cThaRegCDRVtTimeCtrl
#define cThaRegCDRVtTimeCtrl                             0x280600

/*--------------------------------------
BitField Name: CDRChid[6:0]
BitField Type: R/W
BitField Desc: CDR channel ID in CDR mode (0)
--------------------------------------*/
#define cThaVTCDRChidMask                                cBit20_12
#define cThaVTCDRChidShift                               12

/*--------------------------------------
BitField Name: MapVTmode[0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVTEParEnMask                                cBit9
#define cThaVTEParEnShift                               9

/*--------------------------------------
BitField Name: MapVTmode[0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaMapVtMdMask                                cBit8
#define cThaMapVtMdShift                               8

/*--------------------------------------
BitField Name: DE1TimeMode
BitField Type: R/W
BitField Desc: DE1 time mode
               - 0: System timing mode
               - 1: Loop timing mode
               - 2: Line OCN#1 timing mode
               - 3: Line OCN#2 timing mode
               - 4: Line OCN#3 timing mode
               - 5: Line OCN#4 timing mode
               - 6: Line EXT#1 timing mode
               - 7: Line EXT#2 timing mode
               - 8: CDR timing mode
--------------------------------------*/
#define cThaDE1TimeModeMask                            cBit7_4
#define cThaDE1TimeModeShift                           4

/*--------------------------------------
BitField Name: VTTimeMode
BitField Type: R/W
BitField Desc: VT time mode
               - 0: System timing mode
               - 1: Loop timing mode
               - 2: Line OCN#1 timing mode
               - 3: Line OCN#2 timing mode
               - 4: Line OCN#3 timing mode
               - 5: Line OCN#4 timing mode
               - 6: Line EXT#1 timing mode
               - 7: Line EXT#2 timing mode
--------------------------------------*/
#define cThaVtTimeModeMask                             cBit3_0
#define cThaVtTimeModeShift                            0

#define cThaRegCDRVtTimeCtrlNoneEditableFieldsMask (cBit31_20 | cBit11_10)

/*------------------------------------------------------------------------------
Reg Name: CDR STS Timing control
Reg Addr: 0x0280400 - 0x028040B
          The address format for these registers is 0x0280400 + STS
          Where: STS (0 - 11)
Reg Desc: This register is used to configure timing mode for per STS
------------------------------------------------------------------------------*/
#undef cThaRegCDRStsTimeCtrl
#define cThaRegCDRStsTimeCtrl                      0x280400

/*--------------------------------------
BitField Name: CDRChid[1:0]
BitField Type: R/W
BitField Desc: CDR channel ID in CDR mode (0)
--------------------------------------*/
#define cThaVC3CDRChidMask                                cBit28_20
#define cThaVC3CDRChidShift                               20

/*--------------------------------------
BitField Name: MapSTSmode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaVC3EParEnMask                               cBit17
#define cThaVC3EParEnShift                              17

/*--------------------------------------
BitField Name: MapSTSmode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaMapStsMdMask                               cBit16
#define cThaMapStsMdShift                              16

/*--------------------------------------
BitField Name: DE3TimeMode
BitField Type: R/W
BitField Desc: DE3 time mode
               - 0: System timing mode
               - 1: Loop timing mode
               - 2: Line OCN#1 timing mode
               - 3: Line OCN#2 timing mode
               - 4: Line OCN#3 timing mode
               - 5: Line OCN#4 timing mode
               - 6: Line EXT#1 timing mode
               - 7: Line EXT#2 timing mode
               - 8: CDR timing mode
--------------------------------------*/
#define cThaDE3TimeModeMask                            cBit7_4
#define cThaDE3TimeModeShift                           4

/*--------------------------------------
BitField Name: VC3TimeMode
BitField Type: R/W
BitField Desc: VC3 time mode
               - 0: System timing mode
               - 1: Loop timing mode
               - 2: Line OCN#1 timing mode
               - 3: Line OCN#2 timing mode
               - 4: Line OCN#3 timing mode
               - 5: Line OCN#4 timing mode
               - 6: Line EXT#1 timing mode
               - 7: Line EXT#2 timing mode
--------------------------------------*/
#define cThaVC3TimeModeMask                            cBit3_0
#define cThaVC3TimeModeShift                           0

/*------------------------------------------------------------------------------
Reg Name: CDR Global DS3E3 Frame mode
Reg Addr: 0x0280413
Reg Desc: This register is used to configure STS line type for ACR/DCR engine
------------------------------------------------------------------------------*/
#define cThaRegCDRGlbDs3E3FrmMd                       0x0280413

/*--------------------------------------
BitField Name: E3FrameMode[11:0]
BitField Type: R/W
BitField Desc: E3 Frame Mode enable Type.
BitField Bits: 0
--------------------------------------*/
#define cThaE3FrmMdMask(de3id)                       (cBit16 << de3id)
#define cThaE3FrmMdShift(de3id)                      (16 + de3id)

/*--------------------------------------
BitField Name: DE3FramerMode[11:0]
BitField Type: R/W
BitField Desc: E3 Frame Mode enable Type.
BitField Bits: 0
--------------------------------------*/
#define cThaDE3FrmMdMask(de3id)                       (cBit0 << de3id)
#define cThaDE3FrmMdShift(de3id)                      (de3id)

/*------------------------------------------------------------------------------
Reg Name: CDR Engine Timing control
Reg Addr: 0x02A0200 - 0x028021F
          The address format for these registers is 0x0280200 + CHID
          Where: CHID (0 - 31)
Reg Desc: This register is used to configure timing mode for per STS
------------------------------------------------------------------------------*/
#define cThaRegCDREngTimeCtrl                       (0x02A0200UL)


/*--------------------------------------
BitField Name: VC-4_4cMd
BitField Type: R/W
BitField Desc:
BitField Bits: 26
--------------------------------------*/
#define cThaVc4_4cMdMask                            cBit26
#define cThaVc4_4cMdShift                           26

/*--------------------------------------
BitField Name: Packet length extension
BitField Type: R/W
BitField Desc:
BitField Bits: 25
--------------------------------------*/
#define cThaPktLenExtenMask                            cBit25
#define cThaPktLenExtenShift                           25

/*--------------------------------------
BitField Name: HoldvalMode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 24
--------------------------------------*/
#define cThaHoldvalModeMask                            cBit24
#define cThaHoldvalModeShift                           24

/*--------------------------------------
BitField Name: SeqMode
BitField Type: R/W
BitField Desc: Sequence mode mode, default value 0
               - 0: Wrap zero
               - 1: Skip zero
BitField Bits: 23
--------------------------------------*/
#define cThaSeqModeMask                                cBit23
#define cThaSeqModeShift                               23

/*--------------------------------------
BitField Name: LineType
BitField Type: R/W
BitField Desc: Line type mode
               - 0: E1
               - 1: DS1
BitField Bits: 22_20
--------------------------------------*/
#define cThaLineTypeMask                               cBit22_20
#define cThaLineTypeShift                              20

/*--------------------------------------
BitField Name: PktLen
BitField Type: R/W
BitField Desc: The payload packet  length parameter to create a packet. - SAToP
               mode: The number payload of bit - CESoPSN mode: The number of
               bit which converted to full DS1/E1 rate mode. In CESoPSN mode,
               the payload is assembled by NxDS0 with M frame, the value
               configured to this register is Mx256 bits for E1 mode, Mx193
               bits for DS1 mode.
BitField Bits: 19_4
--------------------------------------*/
#define cThaPktLenMask                                 cBit19_4
#define cThaPktLenShift                                4

/*--------------------------------------
BitField Name: CDRTimeMode
BitField Type: R/W
BitField Desc: CDR time mode
               - 0: System mode
               - 1: Loop timing mode, transparency service Rx clock to service
                 Tx clock
               - 2: LIU timing mode, using service Rx clock for CDR source to
                 generate service Tx clock
               - 3: Prc timing mode, using Prc clock for CDR source to generate
                 service Tx clock
               - 4: Ext#1 timing mode, using Ext#1 clock for CDR source to
                 generate service Tx clock
               - 5: Ext#2 timing mode, using Ext#2 clock for CDR source to
                 generate service Tx clock
               - 6: Ethernet timing mode, using Rx Ethernet clock for CDR
                 source to generate service Tx clock
               - 7: Free timing mode, using system clock for CDR source to
                 generate service Tx clock
               - 8: ACR timing mode
               - 9: Reserve
               - 10: DCR timing mode
               - 11: External CDR timing mode
BitField Bits: 3_0
--------------------------------------*/
#define cThaCDRTimeModeMask                            cBit3_0
#define cThaCDRTimeModeShift                           0


/*------------------------------------------------------------------------------
Reg Name: CDR Adjust State status
Reg Addr: 0x02A0800 - 0x028081F
          The address format for these registers is 0x0280800 + CHID
          Where: CHID (0 - 31)
Reg Desc: This register is used to store status or configure  some parameter
          of per CDR engine
------------------------------------------------------------------------------*/

/*--------------------------------------
BitField Name: Adjstate
BitField Type: R/W
BitField Desc: Adjust state Arrive Technologies Inc. Register Description
               This controlled document is the proprietary of Arrive
               Technologies Inc.. Any duplication, reproduction, or
               transmission to unauthorized parties is prohibited. Copyright ?
               2009 Website: www.arrivetechnologies.com        THALASSA CDR
               Regiister Descriptions    Copyright ? 2009. Arrive Technologies
               Inc.      Page i Internal Doc. Subject to Change
               THALASSA CDR Register Descriptions    Copyright ? 2009. Arrive
               Technologies Inc.      Page 1 Internal Doc. Subject to Change
               - 0: Load state
               - 1: Holdover state
               - 2: Initialization state
               - 3: Learn State
               - 4: Rapid State
               - 5: Lock State
BitField Bits: 2_0
--------------------------------------*/
#define cThaAdjStateMask2                               cBit2_0
#define cThaAdjStateShift2                              0

#define cThaAdjSubStateMask                             cBit5_3
#define cThaAdjSubStateShift                            3

/*------------------------------------------------------------------------------
Reg Name: CDR Timing External reference control
Reg Addr: 0x0280003
Reg Desc:  This register is used to configure for the 2Khz coefficient of two
           external reference signal in order to generate timing. The mean that,
           if the reference signal is 8Khz, the  coefficient must be configured
           4 (I.e 8Khz = 4x2Khz)
------------------------------------------------------------------------------*/
#define cThaRegCDRTimingExternalReferenceControl 0x0280003


#define cThaDcrExt1N2kMask                      cBit31_16
#define cThaDcrExt1N2kShift                     16

#define cThaDcrExt2N2kMask                      cBit15_0
#define cThaDcrExt2N2kShift                     0

/*------------------------------------------------------------------------------
Reg Name: DCR TX Engine Bit Rate Configuration
Reg Addr: 0x0290400 - 0x02905FF
          The address format for these registers is 0x0290400 + STSID*32 +
          VTID
          Where: STSID (0 - 11)
          VTID (0-27)
Reg Desc: This register is used to configure packet length for DCR TX Engine.
------------------------------------------------------------------------------*/
#define cThaRegDCRTxEngBitRateCfg                   0x290400

/*--------------------------------------
BitField Name: DcrTxPackLen[2:0]:
BitField Type:
BitField Desc: Packet Length. Unit is bit extension
BitField Bits: 21_19
--------------------------------------*/
#define cThaDcrTxPackLenExtCfgMask                         cBit21_19
#define cThaDcrTxPackLenExtCfgShift                        19

/*--------------------------------------
BitField Name: DcrTxRateCfg
BitField Type:
BitField Desc: Force Dcr Tx Engine to synchronize with PLA RTP request. Dcr Tx
               Engine will clear this bit automatically when the
               synchronization is done.
               - 0: Disable
               - 1: Force to synchronize.
BitField Bits: 18_16
--------------------------------------*/
#define cThaDcrTxRateCfgMask                           cBit18_16
#define cThaDcrTxRateCfgShift                          16

/*--------------------------------------
BitField Name: DcrTxPackLenCfg
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 15_0
--------------------------------------*/
#define cThaDcrTxPackLenCfgMask                        cBit15_0
#define cThaDcrTxPackLenCfgShift                       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECDRSTMREG_H_ */


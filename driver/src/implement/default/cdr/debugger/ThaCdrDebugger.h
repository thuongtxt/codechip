/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaCdrDebugger.h
 * 
 * Created Date: Jun 13, 2015
 *
 * Description : CDR debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACDRDEBUGGER_H_
#define _THACDRDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../controllers/ThaCdrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCdrDebugger * ThaCdrDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void ThaCdrDebuggerDebug(ThaCdrDebugger self, ThaCdrController controller);
void ThaCdrDebuggerPwHeaderDebug(ThaCdrDebugger self, ThaCdrController controller, AtPw pw);

int32 ThaCdrDebuggerE1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerDs1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerE3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerDs3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerVc12PpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerVc11PpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerTu3VcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerHoVcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue);
uint32 ThaCdrDebuggerTimingBaseAddress(ThaCdrDebugger self, ThaCdrController controller);
uint32 ThaCdrDebuggerPwHeaderBaseAddress(ThaCdrDebugger self, ThaCdrController controller);

int32 ThaCdrDebuggerE1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerDs1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerE3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerDs3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerVc12PpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerVc11PpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerTu3VcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
int32 ThaCdrDebuggerHoVcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue);
uint32 ThaCdrDebuggerHoldOverPerChannelAddress(ThaCdrDebugger self, ThaCdrController controller);
eBool ThaCdrDebuggerHoldOverPerChannelIsSupported(ThaCdrDebugger self);

/* Debuggers */
ThaCdrDebugger ThaCdrDebuggerNew(void);
ThaCdrDebugger ThaStmCdrDebuggerNew(void);
ThaCdrDebugger Tha60150011CdrDebuggerNew(void);
ThaCdrDebugger Tha60210021CdrDebuggerNew(void);
ThaCdrDebugger Tha60210031CdrDebuggerNew(void);
ThaCdrDebugger Tha60210051CdrDebuggerNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _THACDRDEBUGGER_H_ */


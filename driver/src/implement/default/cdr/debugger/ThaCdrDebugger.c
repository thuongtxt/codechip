/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaCdrDebugger.c
 *
 * Created Date: Jun 13, 2015
 *
 * Description : CDR debugger implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaCdrDebuggerInternal.h"
#include "AtCommon.h"
#include "../controllers/ThaCdrController.h"

/*--------------------------- Define -----------------------------------------*/
#define cClockMainStateLocked 5
#define cInvalidPpb 1000000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaCdrDebuggerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char* HwClockSubState2String(ThaCdrDebugger self, uint8 hwState)
    {
    AtUnused(self);
    if (hwState == 0) return "Locked";
    if (hwState == 2) return "Rapid";
    if (hwState == 4) return "Learn";
    if (hwState == 5) return "Freeze";

    if ((hwState == 1) || (hwState == 3)) return "Holdover";

    return "none";
    }

static void ClockStateShow(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint8 hwState = ThaCdrControllerHwClockMainStateGet(controller);

    AtPrintc(cSevNormal, "* Main clock state: %s (%d)\r\n", mMethodsGet(self)->HwClockMainState2String(self, hwState), hwState);

    /* Lock's sub state */
    if (hwState == cClockMainStateLocked)
        {
        hwState = ThaCdrControllerHwClockSubStateGet(controller);
        AtPrintc(cSevNormal, "* Sub clock state: %s (%d)\r\n", HwClockSubState2String(self, hwState), hwState);
        }
    else
        AtPrintc(cSevNormal, "* Sub clock state: N/A\r\n");
    }

static void Debug(ThaCdrDebugger self, ThaCdrController controller)
    {
    eAtTimingMode timingMode = ThaCdrControllerTimingModeGet(controller);

    AtPrintc(cSevNormal, "* CDR engine ID: %d\r\n", ThaCdrControllerIdGet(controller));

    if ((timingMode != cAtTimingModeAcr) && (timingMode != cAtTimingModeDcr))
        return;

    ClockStateShow(self, controller);
    }

static void PwHeaderDebug(ThaCdrDebugger self, ThaCdrController controller, AtPw pw)
    {
    AtUnused(self);
    AtUnused(controller);
    AtUnused(pw);
    AtPrintc(cSevNormal, "No information\r\n");
    }

static const char* HwClockMainState2String(ThaCdrDebugger self, uint8 hwState)
    {
    AtUnused(self);
    if (hwState == 0) return "Load";
    if (hwState == 1) return "Wait";
    if (hwState == 2) return "Initialize";
    if (hwState == 3) return "Learn";
    if (hwState == 4) return "Rapid";
    if (hwState == 5) return "Locked";
    if (hwState == 6) return "Freeze";
    if (hwState == 7) return "Holdover";

    /* Return invalid state */
    return "N/A";
    }

static int32 E1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Ds1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Vc12PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Vc11PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 E3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Ds3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Tu3VcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 HoVcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static uint32 TimingBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return 0;
    }

static uint32 PwHeaderBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return 0;
    }

static int32 E1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Ds1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Vc12PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Vc11PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 E3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Ds3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 Tu3VcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static int32 HoVcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    AtUnused(self);
    AtUnused(ncoValue);
    return cInvalidPpb;
    }

static uint32 HoldOverPerChannelAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return cBit31_0;
    }

static eBool HoldOverPerChannelIsSupported(ThaCdrDebugger self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DebugPwHeaderId(ThaCdrDebugger self, ThaCdrController controller, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return ThaCdrControllerChannelIdFlat(controller, ThaCdrControllerChannelGet(controller));
    }

static void MethodsInit(ThaCdrDebugger self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, PwHeaderDebug);
        mMethodOverride(m_methods, HwClockMainState2String);
        mMethodOverride(m_methods, E1PpmCalculate);
        mMethodOverride(m_methods, Ds1PpmCalculate);
        mMethodOverride(m_methods, E3PpmCalculate);
        mMethodOverride(m_methods, Ds3PpmCalculate);
        mMethodOverride(m_methods, Vc12PpmCalculate);
        mMethodOverride(m_methods, Vc11PpmCalculate);
        mMethodOverride(m_methods, Tu3VcPpmCalculate);
        mMethodOverride(m_methods, HoVcPpmCalculate);
        mMethodOverride(m_methods, TimingBaseAddress);
        mMethodOverride(m_methods, PwHeaderBaseAddress);
        mMethodOverride(m_methods, E1PpbCalculate);
        mMethodOverride(m_methods, Ds1PpbCalculate);
        mMethodOverride(m_methods, E3PpbCalculate);
        mMethodOverride(m_methods, Ds3PpbCalculate);
        mMethodOverride(m_methods, Vc12PpbCalculate);
        mMethodOverride(m_methods, Vc11PpbCalculate);
        mMethodOverride(m_methods, Tu3VcPpbCalculate);
        mMethodOverride(m_methods, HoVcPpbCalculate);
        mMethodOverride(m_methods, HoldOverPerChannelAddress);
        mMethodOverride(m_methods, HoldOverPerChannelIsSupported);
        mMethodOverride(m_methods, DebugPwHeaderId);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCdrDebugger);
    }

ThaCdrDebugger ThaCdrDebuggerObjectInit(ThaCdrDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrDebugger ThaCdrDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrDebugger newDebugger = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDebugger == NULL)
        return NULL;

    /* Construct it */
    return ThaCdrDebuggerObjectInit(newDebugger);
    }

void ThaCdrDebuggerDebug(ThaCdrDebugger self, ThaCdrController controller)
    {
    if (self)
        mMethodsGet(self)->Debug(self, controller);
    }

void ThaCdrDebuggerPwHeaderDebug(ThaCdrDebugger self, ThaCdrController controller, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->PwHeaderDebug(self, controller, pw);
    }

int32 ThaCdrDebuggerE1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->E1PpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerDs1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Ds1PpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerVc12PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Vc12PpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerVc11PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Vc11PpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerE3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->E3PpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerDs3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Ds3PpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerTu3VcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Tu3VcPpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerHoVcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->HoVcPpmCalculate(self, ncoValue);
    return cInvalidPpb;
    }

uint32 ThaCdrDebuggerTimingBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    if (self)
        return mMethodsGet(self)->TimingBaseAddress(self, controller);
    return cBit31_0;
    }

uint32 ThaCdrDebuggerPwHeaderBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    if (self)
        return mMethodsGet(self)->PwHeaderBaseAddress(self, controller);
    return cBit31_0;
    }

uint32 ThaCdrDebuggerHoldOverPerChannelAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    if (self)
        return mMethodsGet(self)->HoldOverPerChannelAddress(self, controller);
    return cBit31_0;
    }

eBool ThaCdrDebuggerHoldOverPerChannelIsSupported(ThaCdrDebugger self)
    {
    if (self)
        return mMethodsGet(self)->HoldOverPerChannelIsSupported(self);
    return cAtFalse;
    }

int32 ThaCdrDebuggerE1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->E1PpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerDs1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Ds1PpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerVc12PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Vc12PpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerVc11PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Vc11PpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerE3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->E3PpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerDs3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Ds3PpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerTu3VcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->Tu3VcPpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

int32 ThaCdrDebuggerHoVcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    if (self)
        return mMethodsGet(self)->HoVcPpbCalculate(self, ncoValue);
    return cInvalidPpb;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaStmCdrDebugger.c
 *
 * Created Date: Jun 13, 2015
 *
 * Description : SDH channel cdr debugger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaCdrDebuggerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrDebuggerMethods m_ThaCdrDebuggerOverride;

/* Save super implementation */
static const tThaCdrDebuggerMethods *m_ThaCdrDebuggerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char* HwClockMainState2String(ThaCdrDebugger self, uint8 hwState)
    {
    if (hwState == 1) return "Holdover";

    return m_ThaCdrDebuggerMethods->HwClockMainState2String(self, hwState);
    }

static void OverrideThaCdrDebugger(ThaCdrDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaCdrDebuggerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrDebuggerOverride, m_ThaCdrDebuggerMethods, sizeof(m_ThaCdrDebuggerOverride));

        mMethodOverride(m_ThaCdrDebuggerOverride, HwClockMainState2String);
        }

    mMethodsSet(self, &m_ThaCdrDebuggerOverride);
    }

static void Override(ThaCdrDebugger self)
    {
    OverrideThaCdrDebugger(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmCdrDebugger);
    }

ThaCdrDebugger ThaStmCdrDebuggerObjectInit(ThaCdrDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCdrDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrDebugger ThaStmCdrDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrDebugger newDebugger = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDebugger == NULL)
        return NULL;

    /* Construct it */
    return ThaStmCdrDebuggerObjectInit(newDebugger);
    }

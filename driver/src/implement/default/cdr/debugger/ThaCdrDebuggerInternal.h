/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaCdrDebuggerInternal.h
 * 
 * Created Date: Jun 11, 2015
 *
 * Description : CDR debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACDRDEBUGGERINTERNAL_H_
#define _THACDRDEBUGGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "ThaCdrDebugger.h"
#include "../controllers/ThaCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCdrDebuggerMethods
    {
    void (*Debug)(ThaCdrDebugger self, ThaCdrController controller);
    void (*PwHeaderDebug)(ThaCdrDebugger self, ThaCdrController controller, AtPw pw);
    uint32 (*DebugPwHeaderId)(ThaCdrDebugger self, ThaCdrController controller, AtPw pw);
    uint32 (*TimingBaseAddress)(ThaCdrDebugger self, ThaCdrController controller);
    uint32 (*PwHeaderBaseAddress)(ThaCdrDebugger self, ThaCdrController controller);

    const char* (*HwClockMainState2String)(ThaCdrDebugger self, uint8 hwState);
    int32 (*E1PpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Ds1PpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Vc12PpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Vc11PpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*E3PpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Ds3PpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Tu3VcPpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*HoVcPpmCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*E1PpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Ds1PpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Vc12PpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Vc11PpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*E3PpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Ds3PpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*Tu3VcPpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    int32 (*HoVcPpbCalculate)(ThaCdrDebugger self, uint32 ncoValue);
    uint32 (*HoldOverPerChannelAddress)(ThaCdrDebugger self, ThaCdrController controller);
    eBool  (*HoldOverPerChannelIsSupported)(ThaCdrDebugger self);
    }tThaCdrDebuggerMethods;

typedef struct tThaCdrDebugger
    {
    tAtObject super;
    const tThaCdrDebuggerMethods * methods;

    }tThaCdrDebugger;

typedef struct tThaStmCdrDebugger
    {
    tThaCdrDebugger super;
    }tThaStmCdrDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrDebugger ThaCdrDebuggerObjectInit(ThaCdrDebugger self);
ThaCdrDebugger ThaStmCdrDebuggerObjectInit(ThaCdrDebugger self);

#ifdef __cplusplus
}
#endif
#endif /* _THACDRDEBUGGERINTERNAL_H_ */


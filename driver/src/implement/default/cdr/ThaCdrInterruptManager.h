/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaCdrInterruptManagerInternal.h
 * 
 * Created Date: Dec 12, 2015
 *
 * Description : Default CDR module interrupt manager.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACDRINTERRUPTMANAGER_H_
#define _THACDRINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/interrupt/AtInterruptManager.h"
#include "controllers/ThaCdrController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCdrInterruptManager * ThaCdrInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager ThaCdrInterruptManagerNew(AtModule module);

ThaCdrController ThaCdrInterruptManagerAcrDcrControllerFromHwIdGet(ThaCdrInterruptManager self, uint8 slice, uint8 sts, uint8 vt);

#ifdef __cplusplus
}
#endif
#endif /* _THACDRINTERRUPTMANAGER_H_ */


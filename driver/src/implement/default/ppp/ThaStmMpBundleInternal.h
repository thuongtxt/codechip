/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaStmMpBundleInternal.h
 * 
 * Created Date: Aug 20, 2013
 *
 * Description : MLPPP bundle of STM product Intefnal Interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMPBUNDLEINTERNAL_H_
#define _THASTMMPBUNDLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaMpBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* This class */
typedef struct tThaStmMpBundle * ThaStmMpBundle;

typedef struct tThaStmMpBundle
    {
    tThaMpBundle super;

    /* Private data */
    }tThaStmMpBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMpBundle ThaStmMpBundleObjectInit(AtMpBundle self, uint32 channelId, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMPBUNDLEINTERNAL_H_ */


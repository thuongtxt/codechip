/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtPppLinkInternal.h
 *
 * Created Date: Sep 2, 2012
 *
 * Description : PPP link
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPPPLINKINTERNAL_H_
#define _THAPPPLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ppp/AtPppLinkInternal.h"
#include "../eth/controller/ThaEthFlowController.h"
#include "../eth/ThaEthFlow.h"
#include "../encap/ThaModuleEncapInternal.h"

#include "ThaPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self) mMethodsGet((ThaPppLink)self)->DefaultOffset((ThaPppLink)self)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPppLinkMethods
    {
    uint32 (*DefaultOffset)(ThaPppLink self);
    uint32 (*DefaultBLockOffset)(ThaPppLink self, uint8 blockaddress);
    uint32 (*MpigLinkAlarmIntrEnCtrlOffset)(ThaPppLink self);
    uint32 (*MPIGDATLinkAlarmIntrStatOffset)(ThaPppLink self);
    uint32 (*IntrDefaultOffset)(ThaPppLink self);
    uint32 (*QueueDefaultOffset)(ThaPppLink self, uint16 queueId);

    eAtRet (*PppPacketModeSet)(ThaPppLink self);
    ThaEthFlowController (*EthFlowControllerGet)(ThaPppLink self);
    eBool (*PidIsByPass)(ThaPppLink self);
    AtPidTable (*PidTableObjectCreate)(ThaPppLink self);

    eAtRet (*PppPacketEnable)(ThaPppLink self, eBool enable);
    eBool (*PppPacketIsEnabled)(ThaPppLink self);

    eAtRet (*IgMlpppEnable)(ThaPppLink self, eBool enable);
    eAtRet (*EgMlpppEnable)(ThaPppLink self, eBool enable);
    eAtRet (*JoinBundle)(ThaPppLink self, ThaMpBundle bundle);
    eAtRet (*MemberLocalIdInBundleSet)(ThaPppLink self , uint16 localMemberId);
    }tThaPppLinkMethods;

typedef struct tThaPppLinkSimulation
    {
    uint8 phase;
    }tThaPppLinkSimulation;

typedef struct tThaPppLink
    {
    tAtPppLink super;

    const tThaPppLinkMethods *methods;

    /* Private data */
    uint32 lcpCounters;
    uint32 ipcpCounters;
    uint32 ipv6cpCounters;
    uint32 unknownPidCounters;
    uint32 otherOamCounters;
    uint8 txTrafficIsEnabled;

    /* To support feature that check if all of direct OAMs are sent */
    AtOsalSem oamSemaphore;
    uint32 numOamsRequested;
    uint32 numOamsSent;
    uint32 txOamCounter;

    AtPidTable pidTable;

    /* For simulation */
    tThaPppLinkSimulation *simulation;
    }tThaPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void ThaPppLinkHwTxTrafficEnable(AtPppLink self, eBool enable);

/* To handle the logic that checking all OAMs sent or not */
eBool ThaPppLinkCanCheckOamsSent(ThaPppLink self);
uint32 ThaPppLinkTxOamCounterLatch(ThaPppLink self, uint32 txOamCounter, eBool clearAfterLatch);
void ThaPppLinkOamLock(ThaPppLink self);
void ThaPppLinkOamUnlock(ThaPppLink self);

uint32 ThaModuleEncapLocalPppLinkId(ThaPppLink link);

eBool ThaPmcIndirectCounterIsValid(AtChannel self, eAtModule moduleId, uint32 timeoutMs, uint8  partId);
eBool ThaPmcIndirectCounterIsAccessible(AtChannel self, eAtModule moduleId, uint8 partId);
uint32 ThaPppLinkDefaultOffset(ThaPppLink self);
uint8 ThaPppLinkMaxNumQueues(ThaPppLink self);
eAtRet ThaPppLinkDebugCountersClear(ThaPppLink self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPPPLINKINTERNAL_H_ */


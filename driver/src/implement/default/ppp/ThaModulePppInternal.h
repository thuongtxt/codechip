/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaModulePppInternal.h
 * 
 * Created Date: Dec 3, 2012
 *
 * Description : PPP module class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPPPINTERNAL_H_
#define _THAMODULEPPPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ppp/AtModulePppInternal.h"
#include "../util/ThaUtil.h"
#include "ThaModulePpp.h"
#include "ThaModulePppInternal.h"
#include "ThaPidTable.h"
#include "ThaMpBundle.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaMaxOamSizeInBytes  1024

#define mDefineMaskShifts(field)                                                \
    static uint32 field##Mask(ThaModulePpp self)                                 \
        {                                                                      \
        AtUnused(self);                                                        \
        return cTha##field##Mask;                                              \
        }                                                                      \
                                                                               \
    static uint8  field##Shift(ThaModulePpp self)                                \
        {                                                                      \
        AtUnused(self);                                                        \
        return cTha##field##Shift;                                             \
        }

#define mBitFieldOverrides(methods, field)                                      \
    mMethodOverride(methods, field##Mask);                                     \
    mMethodOverride(methods, field##Shift);


#define mDefineMaskShiftFields(field)                                           \
     uint32 (*field##Mask)(ThaModulePpp self);                                   \
     uint8  (*field##Shift)(ThaModulePpp self);

#define mDefineRegAdresss(regName)                                              \
    static uint32 regName(ThaModulePpp self)                                    \
        {                                                                      \
		AtUnused(self);                                                        \
        return cThaReg##regName;                                               \
        }

#define mPppMask(ppp, field)                                                \
        mMethodsGet((ThaModulePpp)ppp)->field##Mask((ThaModulePpp)ppp)
#define mPppShift(ppp, field)                                             \
        mMethodsGet((ThaModulePpp)ppp)->field##Shift((ThaModulePpp)ppp)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePppBlockUsage
    {
    uint8 *blockIsUsed;
    uint32 numBlocks;
    uint32 numUsedBlocks;

    uint8 *highBwBlockIsUsed;
    uint32 numHighBwBlocks;
    uint32 numUsedHighBwBlocks;
    }tThaModulePppBlockUsage;

typedef struct tThaModulePppMethods
    {
    uint32 (*MpigMaxNumBlocks)(ThaModulePpp self);
    uint32 (*MpigHighBwMaxNumBlocks)(ThaModulePpp self);
    void   (*OamReceiveStickyClear)(ThaModulePpp self, AtIpCore ipCore);
    uint32 (*NumBlocksForRange)(ThaModulePpp self, uint8 rangeId);
    eBool  (*DirectOam)(ThaModulePpp self);
    eBool  (*NeedInitializePidTable)(ThaModulePpp self);
    uint8  (*MaxNumQueuesPerLink)(ThaModulePpp self);
    eAtRet (*DefaultSet)(ThaModulePpp self);

    /* MPIG Bit fields */
    mDefineMaskShiftFields(OamLinkId)
    mDefineMaskShiftFields(FlwEnb)
    mDefineMaskShiftFields(MPIGDATLookupFlwID)
    }tThaModulePppMethods;

typedef struct tThaModulePpp
    {
    tAtModulePpp super;
    const tThaModulePppMethods *methods;

    /* Private data */
    tThaModulePppBlockUsage *partBlockUsage;
    uint8 oamBuffer[cThaMaxOamSizeInBytes];
    uint8 interruptEnabled;
    uint8 oamDirectMode;

    uint32 oamNotReady;
    uint32 hwTimeOut;
    uint32 bufferNotEnough;
    uint32 invalidAddrCtrl;

    }tThaModulePpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePpp ThaModulePppObjectInit(AtModulePpp self, AtDevice device);

/* Helpers */
eBool ThaModuleHwFinished(ThaModulePpp self, uint32 address, uint32 bitMask, uint32 timeOut);
uint32 ThaModulePppReadOamOnCore(AtModulePpp self, AtIpCore core, uint8 *buffer, uint32 bufferLength, uint16 *pLinkId);
eAtRet ThaModulePppSendOamOnCore(AtModulePpp self, uint8 ipCore, uint8 *oamBuffer, uint32 bufferLength, uint16 linkId);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPPPINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPidTable.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : PID table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPIDTABLE_H_
#define _THAPIDTABLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPidTable.h" /* Super class */
#include "AtPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPidTable *ThaPidTable;
typedef struct tThaPidTablePppLink *ThaPidTablePppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaPidTablePppLinkGet(ThaPidTablePppLink self);

/* Product concretes */
AtPidTable Tha60031032PidTableNew(AtModule module);
AtPidTable Tha60031033PidTableNew(AtModule module);
AtPidTable Tha60060011PidTableNew(AtPppLink pppLink);
AtPidTable Tha60091023PidTableNew(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPIDTABLE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO module name
 *
 * File        : ThaMpigReg.h
 *
 * Created Date: Aug 13, 2012
 *
 * Author      : nguyennt
 *
 * Description : TODO Description
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAMPIGREG_H_
#define _THAMPIGREG_H_

#define cThaRegMPIGLongRegSize 4

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT Lookup Bundle control
Reg Addr: 0x853000-0x8533FF
          The address format for these registers is 0x853000 + BundleID
          Where: BundleID (0 -1023)
Reg Desc: This register is used to configure control mode for per Bundle for
          MIPG Lookup
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATLookupBundleCtrl   						0x853000
#define cThaRegMPIGDATLookupBundleCtrlNoneEditableFieldsMask   	cBit31_3

/*--------------------------------------
BitField Name: MLPLongMode
BitField Type: R/W
BitField Desc: MLPPP Long Sequence Number Fragment Format
               - 0: Short
               - 1: Long
--------------------------------------*/
#define cThaMLPSequenceModeMask                            cBit0
#define cThaMLPSequenceModeShift                           0
#define cThaMLPSequenceModeShort                           0

/*--------------------------------------
BitField Name: MLPMCMode
BitField Type: R/W
BitField Desc: Multi-class mode of MLP packet
               - 0: Multi-link RFC 1990. Use 3-bit class field to look up from
                 Bundle ID to Flow ID
               - 1: Multi-class RFC 2886. Use 4-bit class field to look up from
                 Bundle ID to Flow ID (not support Class #16 in case of there
                 are non-LFI packet)
--------------------------------------*/
#define cThaMLPMCModeMask                              cBit1
#define cThaMLPMCModeShift                             1

/*--------------------------------------
BitField Name: BndEnb
BitField Type: R/W
BitField Desc: Bundle enable
--------------------------------------*/
#define cThaBndEnbMask                                 cBit2
#define cThaBndEnbShift                                2

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Packet Assembler Control
Reg Addr: 0x864000-0x8647FF
          The address format for these registers is 0x864000 + flowid
          Where: flowid (0-2047) is flow ID
Reg Desc: These registers are used to Config Packet Assember Per Flow
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQPktAssemblerCtrl    0x864000

/*--------------------------------------
BitField Name: MPIGRsqPkaMsru
BitField Type: R/W
BitField Desc: Multilink Maximum Sequence Reconstructed Unit
--------------------------------------*/
#define cThaMPIGRsqPkaMsruMask                         cBit22_16
#define cThaMPIGRsqPkaMsruShift                        16

/*--------------------------------------
BitField Name: MPIGRsqPkaMrru
BitField Type: R/W
BitField Desc: Multilink Maximum Received Reconstructed Unit (RFC 1990)
--------------------------------------*/
#define cThaMPIGRsqPkaMrruMask                         cBit13_0
#define cThaMPIGRsqPkaMrruShift                        0

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Resequence Timeout
Reg Addr: 0x861000-0x8617FF
          The address format for these registers is 0x861000 + flowid
          Where: flowid (0-2047) is flow ID
Reg Desc: These registers are used to Config Packet Assember Per Flow
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQResequenceTimeout    						0x861000
#define cThaRegMPIGRSQResequenceTimeoutNoneEditableFieldsMask   cBit31_8

/*--------------------------------------
BitField Name: MPIGRsqTmoThshTimer
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaMPIGRsqTmoThshTimerMask                    cBit7_0
#define cThaMPIGRsqTmoThshTimerShift                   0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT Lookup Link control
Reg Addr: 0x851000-0x8513FF
          The address format for these registers is 0x851000 + LinkID
          Where: LinkID (0 -1023)
Reg Desc: This register is used to configure control mode for per Link for
          MIPG Lookup
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATLookupLinkCtrl                			0x851000
#define cThaRegMPIGDATLookupLinkCtrlNoneEditableFieldsMask		(cBit31 | cBit12)

/*--------------------------------------
BitField Name: PassMLPHdrField[32]
BitField Type: R/W
BitField Desc: By pass MLPPP header field
--------------------------------------*/
#define cThaMlppHdrModeMask                            cBit0
#define cThaMlppHdrModeShift                           0
#define cThaMlppHdrModeDwordIndex                      1

/*--------------------------------------
BitField Name: PassPPPPidField[29]
BitField Type: R/W
BitField Desc: By pass PPP protocol  field
--------------------------------------*/
#define cThaPidModeMask                                cBit31
#define cThaPidModeShift                               31

/*--------------------------------------
BitField Name: PassAdrCtlField[28]
BitField Type: R/W
BitField Desc: By pass Address Control field
--------------------------------------*/
#define cThaAddrCtrlModeMask                           cBit30
#define cThaAddrCtrlModeShift                          30

/*--------------------------------------
BitField Name: AdrComp[0]
BitField Type: R/W
BitField Desc: Address field compress mode
--------------------------------------*/
#define cThaAdrCompMask                                cBit29
#define cThaAdrCompShift                               29

/*--------------------------------------
BitField Name: BlkAloc[3:0]
BitField Type: R/W
BitField Desc: The number of block 256 segment 32-bytes is allocated for per
               link. Each block correspond with bandwidth of 2 DS0.
--------------------------------------*/
#define cThaBlkAlocMask                                cBit28_25
#define cThaBlkAlocShift                               25

/*--------------------------------------
BitField Name: FcsErrDropEn[22]
BitField Type: R/W
BitField Desc: FCS Error drop packet enable
--------------------------------------*/
#define cThaFcsErrDropEnMask                            cBit24
#define cThaFcsErrDropEnShift                           24

/*--------------------------------------
BitField Name: AdrCtlErrDropEn[21]
BitField Type: R/W
BitField Desc: Address Control Error drop packet enable
--------------------------------------*/
#define cThaAdrCtlErrDropEnMask                        cBit23
#define cThaAdrCtlErrDropEnShift                       23

/*--------------------------------------
BitField Name: NCPRule[1:0]
BitField Type: R/W
BitField Desc: The rule of NCP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#define cThaNCPRuleMask                                cBit22_19
#define cThaNCPRuleShift                               19

/*--------------------------------------
BitField Name: LCPRule[1:0]
BitField Type: R/W
BitField Desc: The rule of LCP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#define cThaLCPRuleMask                                cBit18_15
#define cThaLCPRuleShift                               15

/*--------------------------------------
BitField Name: RSQRule[1:0]
BitField Type: R/W
BitField Desc: The rule of packet which need re-sequence, such as MLPPP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#define cThaMlpRuleRuleMask                            cBit14_11
#define cThaMlpRuleRuleShift                           11

/*--------------------------------------
BitField Name: PppRule[10:7]
BitField Type: R/W
BitField Desc: The rule of packet which don't need re-sequence, such as
               HDLC/PPP PW packet or BCP/IP  non MLPPP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#define cThaPppRuleMask                                cBit10_7
#define cThaPppRuleShift                               7

/*--------------------------------------
BitField Name: PKtMode[6:4]
BitField Type: R/W
BitField Desc: Packet Encapsulation mode
    0: HDLC PW
    1: HDLC Ter (CISCO-HDLC), Base on PPP-PID to classify to packet type
    2: PPP PW
    3: PPP Ter (Termination). Base on PPP-PID to classify to packet type
    4: FR PW
    5: FR Ter
    6-7: Unused
--------------------------------------*/
#define cThaPKtModeMask                                 cBit6_4
#define cThaPKtModeShift                                4

/*--------------------------------------
BitField Name: BndID[3:0]
BitField Type: R/W
BitField Desc: Bundle ID. Look up from Link ID to Bundle ID
--------------------------------------*/
#define cThaBndIDMask                                  cBit3_0
#define cThaBndIDShift                                 0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT Lookup FlowID Lookup control
Reg Addr: 0x854000-0x857FFF
          The address format for these registers is 0x854000 + BundleID*16 +
          Class
          Where:
          BundleID (0 -1023)
          Class (0 -15). In case of
          * HDLC/PPP PW: must use class = 15
          * BCP/IP/LCP/NCP/Other packet non LFI forward to PSN: must use class
          = 15
          * MLPPP RFC 1990 : must use class = 0-7
          * MLPPP RFC 2686 : must use class = 0-15
Reg Desc: This register is used to configure Look up from Bundle and Class to
          Flow/PW ID
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATLookupFlowIdLookupCtrl     						0x854000
#define cThaRegMPIGDATLookupFlowIdLookupCtrlNoneEditableFieldsMask     	cBit31_8

/*--------------------------------------
BitField Name: FlwEnb[0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFlwEnbMask                                 cBit7
#define cThaFlwEnbShift                                7

/*--------------------------------------
BitField Name: FlwID[10:0]k
BitField Type: R/W
BitField Desc: Flow or Pseudo-wire ID
--------------------------------------*/
#define cThaMPIGDATLookupFlwIDMask                     cBit6_0
#define cThaMPIGDATLookupFlwIDShift                    0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT De-Queue PSN header control
Reg Addr: 0x843000-0x843FFF
          The address format for these registers is 0x843000 + FlowID
          Where: FlowID (0 -2047)
Reg Desc: This register is used to configure control mode for per Flow for
          MIPG De-Queue
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATDeQueOamLinkCtrl  						0x843000
#define cThaRegMPIGDATDeQuePppLinkCtrl  						(0x843000 + 0x400)
#define cThaRegMPIGDATDeQueMpFlowCtrl   						(0x843000 + 0x800)
#define cThaRegMPIGDATDeQueOamLinkCtrlNoneEditableFieldsMask  	cBit31_18
#define cThaRegMPIGDATDeQuePppLinkCtrlNoneEditableFieldsMask    cBit31_18
#define cThaRegMPIGDATDeQueMpFlowCtrlNoneEditableFieldsMask     cBit31_18
/*--------------------------------------
BitField Name: STMPort[1:0]
BitField Type: R/W
BitField Desc: STM port which this flow is belong.
               It is apply into Higig header.
--------------------------------------*/
#define cThaFlowInStmPortMask                          cBit16_14
#define cThaFlowInStmPortShift                         14

/*--------------------------------------
BitField Name: UDPChkSumMode[0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               0: UDP Check sum insert disable (0x0000)
               1: UDP Check sum insert enable
--------------------------------------*/
#define cThaOutEnableMask                          cBit13
#define cThaOutEnableShift                         13

/*--------------------------------------
BitField Name: PSNMode[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
                0: PSN Header insert disable
                1: MEF
                2: MPLS
                3: UDPoIPv4
                4: UDPoIPv6
                5: MPLSoIPv4
                6: MPLSoIPv6
                7: - See RD
                8: - See RD
                A: - See RD
                B: - See RD
--------------------------------------*/
#define cThaPSNModeMask                                cBit12_9
#define cThaPSNModeShift                               9

/*--------------------------------------
BitField Name: VlanMode[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               0: No VLAN
               1: 1 VLAN Tags
               2: 2 VLAN Tags
--------------------------------------*/
#define cThaVlanModeMask                                cBit8_7
#define cThaVlanModeShift                               7

/*--------------------------------------
BitField Name: HdrLen[6:0]
BitField Type: R/W
BitField Desc: PSN Header length. Valid from 1
--------------------------------------*/
#define cThaHdrLenMask                                 cBit6_0
#define cThaHdrLenShift                                0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT De-Queue OAM Info get
Reg Addr: 0x844000
Reg Desc: The register provides channel for CPU get OAM packet information
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATDeQueOamInfoget               0x844000

/*--------------------------------------
BitField Name: OAMReady
BitField Type: R/W
BitField Desc: OAM ID is ready. HW set to 1 when there a OAM packet ready and
               set to 0 when CPU read at end of packet.
--------------------------------------*/
#define cThaOamReadyMask                               cBit13

/*--------------------------------------
BitField Name: OAMID
BitField Type: R/W
BitField Desc: Thalassa support 2 queue OAM, each queue can store 2048 packet.
               Value from 0 to 2047 for queue #1 (high queue) and value from
               2048 to 4095 fro queue #2 (low queue)
--------------------------------------*/
#define cThaOamIDMask                                  cBit12_0
#define cThaOamIDShift                                 0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT De-Queue CPU Read Control
Reg Addr: 0x844001
Reg Desc: The register provides channel for CPU process to read  OAM packet
          from DDR. This is procedure to CPU get a OAM packet. - CPU read
          OAMRdy Info to know what OAM ID ready to get - CPU set OAM2CPU
          register to get first segment address by set Reqenb = 1 to know the
          packet length - Engine will request a cache ID - Wait DDR respond
          and return to CPU by set bit ReqEnb = 0 with Cache ID - HW base on
          Cache ID to read OAM Data from DDR send to CPU via MPIG De-Queue OAM
          Data cache register - Free Cache ID when Engine read to the end of
          segment (upa[1:0] == 2'd3) - After that CPU set OAM2CPU register to
          get next segment until end of packet with indicate eopbit = 1
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATDeQueCpuReadCtrl                			0x844001
#define cThaRegMPIGDATDeQueCpuReadCtrlNoneEditableFieldsMask	cBit31

/*--------------------------------------
BitField Name: ReqEnb
BitField Type: R/W
BitField Desc: Read DDR request enable. CPU se
--------------------------------------*/
#define cThaReqEnbMask                                 cBit31
#define cThaReqEnbShift                                31

/*--------------------------------------
BitField Name: SoP
BitField Type: R/W
BitField Desc: Start of packet
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
SoP in 4.10.2.9.MPIG DAT De-Queue CPU Read Control
 have declare in 4.10.2.7. MPIG DAT SegPro CPU write Control
*/
#define cThaMPIGDATDeQueCpuReadSoPMask                 cBit30
#define cThaMPIGDATDeQueCpuReadSoPShift                30

/*--------------------------------------
BitField Name: EoP
BitField Type:
BitField Desc: End of packet
--------------------------------------*/
/*_ERRORS___BIT_FIELD___RE_DEFINE
EoP in 4.10.2.9.MPIG DAT De-Queue CPU Read Control
 have declare in 4.10.2.7. MPIG DAT SegPro CPU write Control
*/
#define cThaMPIGDATDeQueCpuReadEoPMask                 cBit29
#define cThaMPIGDATDeQueCpuReadEoPShift                29

/*--------------------------------------
BitField Name: HdrEnb
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaHdrEnbMask                                 cBit28
#define cThaHdrEnbShift                                28

/*--------------------------------------
BitField Name: Adr
BitField Type:
BitField Desc: Address of segment 32-bytes. Address =
               {OAMID[12:0],PktLen[11:5]}
--------------------------------------*/
#define cThaBlkTypeMask                                cBit27_24
#define cThaBlkTypeShift                               24

/*--------------------------------------
BitField Name: Adr
BitField Type:
BitField Desc: Address of segment 32-bytes. Address =
               {OAMID[12:0],PktLen[11:5]}
--------------------------------------*/
#define cThaBlkIdMask                                  cBit23_8
#define cThaBlkIdShift                                 8

/*--------------------------------------
BitField Name: CacheID
BitField Type:
BitField Desc: CachID
--------------------------------------*/
#define cThaSegIDMask                                cBit7_0
#define cThaSegIDShift                               0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT Per Link Alarm Interrupt Enable Control
Reg Addr: 0x85C000 - 0x85C3FF
          The address format for these registers is 0x85C000 � 0x85C3FF
Reg Desc: This is the per Alarm interrupt enable of ppp link. Each register is
used to store 7 bits to enable interrupts when the related alarms in ppp link happen.
------------------------------------------------------------------------------*/
#define cThaMPIGDATLinkAlarmIntrEnCtrl                     		0x85C000

/*------------------------------------------------------------------------------
 Reg Name: MPIG RSQ Resequence FSM Access
 Reg Addr: 0x8600A1
 The address format for these registers is 0x8600A1
 Reg Desc: These registers are used to access FSM Resequence.
 ------------------------------------------------------------------------------*/
#define cThaMPIGRsqReqFsmAccess                			0x8600A1

#define cThaMPIGRsqAccDoneMask                 cBit19
#define cThaMPIGRsqAccDoneShift                19

#define cThaMPIGRsqAccReqMask                  cBit16
#define cThaMPIGRsqAccReqShift                 16

#define cThaMPIGRsqAccReqFsmMask               cBit15_12
#define cThaMPIGRsqAccReqFsmShift                12

#define cThaMPIGRsqAccReqFlowIdMask            cBit10_0
#define cThaMPIGRsqAccReqFlowIdShift             0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT De-Queue OAM Data cache
Reg Addr: 0x844010-0x844013
Reg Desc: The register provides channel for CPU read Data of OAM packet from
          Cache 32-bytes (64x4).
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATDeQueOamDatacache             0x844010UL

#define cThaDataLenMask                                cBit13_0
#define cThaDataLenShift                               0

#define cThaOamLinkIdMask                              cBit20_14
#define cThaOamLinkIdShift                             14

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Resequence Control
Reg Addr: 0x862000-0x8627FF
          The address format for these registers is 0x862000 + flowid
          Where: flowid (0-1023) is flow ID
Reg Desc: These registers are used to Config Resequence Per Flow * Maximum
          Link Delay <DL> (ms) * BandWidth <BW> (Kbps) * Fragment Length <FL>
          Bytes => Thsh = (BW*DL/1000)/(8*FL) Exam-1: DL = 100ms, BW = 32Mbps,
          FL = 64 => Thsh = (32*10^6*100/1000)/(8*64) = 6250 Exam-2: DL =
          32ms, BW = 64Kbps, FL = 64 => Thsh = (64*10^3*32/1000)/(8*64) = 4
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQResequenceCtrl    0x862000

/*--------------------------------------
BitField Name: MPIGRsqCtlWoff
BitField Type: R/W
BitField Desc: Window offset sequence, wind = thsh + Woff*2
--------------------------------------*/
#define cThaMPIGRsqCtlWoffMask                         cBit23_18
#define cThaMPIGRsqCtlWoffShift                        18

/*--------------------------------------
BitField Name: MPIGRsqCtlThsh
BitField Type: R/W
BitField Desc: Threshold Sequence that declaration lost sequence
--------------------------------------*/
#define cThaMPIGRsqCtlThshMask                         cBit17_4
#define cThaMPIGRsqCtlThshShift                        4

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT SegPro CPU write Control
Reg Addr: 0x841002
Reg Desc: The register provides channel for CPU write data to DDR for PSN
          header or OAM packet send to  PSN.
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATSegProCpuWrCtrl               			0x841002

/*--------------------------------------
BitField Name: WrReq
BitField Type: R/W
BitField Desc: Request write. CPU set 1 to request write and HW will set 0 when
               write done
--------------------------------------*/
#define cThaWrReqMask                                  cBit6 /* cBit102 */
#define cThaWrReqShift                                 6
#define cThaWrReqDwIndex                               3

/*--------------------------------------
BitField Name: SoP
BitField Type: R/W
BitField Desc: Start of packet
--------------------------------------*/
#define cThaSoPMask                                    cBit5 /* cBit101 */
#define cThaSoPShift                                   5
#define cThaSoPDwIndex                                 3

/*--------------------------------------
BitField Name: EoP
BitField Type: R/W
BitField Desc: End of packet
--------------------------------------*/
#define cThaEoPMask                                    cBit4 /* cBit100 */
#define cThaEoPShift                                   4
#define cThaEoPDwIndex                                 3

/*--------------------------------------
BitField Name: BlkTyp
BitField Type: R/W
BitField Desc: Type of packet Other: Resever
               - 0x0: PSN OAM packet, will send to PSN, BlkID[12:0]
               - 0x4: PPP Network Control PSN Header. Maximum 128 bytes, BlkID
                 is PPP link ID
               - 0x5: PPP Network Packet PSN Header. Maximum 128 bytes, BlkID
                 is PPP link ID
               - 0x8: MLPPP Network Control PSN Header. Maximum 128 bytes,
                 BlkID is mpflowid
               - 0x9: MLPPP Network Packet PSN Header. Maximum 128 bytes, BlkID
                 is mpflowid
--------------------------------------*/
#define cThaBlkTypMask                                 cBit3_0 /* cBit99_96 */
#define cThaBlkTypShift                                0
#define cThaBlkTypDwIndex                              3

/*--------------------------------------
BitField Name: FlwID
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaFlwIDMask                                  cBit31_16 /* cBit95_80 */
#define cThaFlwIDShift                                 16
#define cThaFlwIDDwIndex                               2

/*--------------------------------------
BitField Name: CurLen
BitField Type: R/W
BitField Desc: Current length of packet. Valid from 0, maximum 2048 Bytes
--------------------------------------*/
#define cThaCurLenMask                                 cBit15_0 /* cBit79_64 */
#define cThaCurLenShift                                0
#define cThaCurLenDwIndex                              2

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Global Control 1
Reg Addr: 0x860001-0x860001
          The address format for these registers is 0x860001
Reg Desc: These registers are global Control
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQGlbCtrl1                    		(0x860001)

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Global Control 2
Reg Addr: 0x860002-0x860002
          The address format for these registers is 0x860002
Reg Desc: These registers are global Control
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQGlbCtrl2                     		(0x860002)

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ EMI Access Control
Reg Addr: 0x860100
          The address format for these registers is 0x860100
Reg Desc: These registers are used to Emi access control
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQEMIAcsCtrl                    		(0x860100)

/*--------------------------------------
BitField Name: MPIGRsqEmiAccDone
BitField Type: R_O
BitField Desc: Indicate Request is Done
--------------------------------------*/
#define cThaMPIGRsqEmiAccDoneMask                      cBit31

/*--------------------------------------
BitField Name: MPIGRsqEmiAccBurst
BitField Type: R/W/SC
BitField Desc: Burst Request to EMI  begin start address MPIGRsqEmiAccAdd to
               MPIGRsqEmiEndAdd.
--------------------------------------*/
#define cThaMPIGRsqEmiAccBurstMask                     cBit30
#define cThaMPIGRsqEmiAccBurstShift                    30

/*--------------------------------------
BitField Name: MPIGRsqEmiAccReq
BitField Type: R/W/SC
BitField Desc: Request read or write data to EMI
--------------------------------------*/
#define cThaMPIGRsqEmiAccReqMask                       cBit29
#define cThaMPIGRsqEmiAccReqShift                      29

/*--------------------------------------
BitField Name: MPIGRsqEmiAccRnw
BitField Type: R/W
BitField Desc: read or write
               - 0x0: Write
               - 0x1: Read
--------------------------------------*/
#define cThaMPIGRsqEmiAccRnwMask                       cBit28
#define cThaMPIGRsqEmiAccRnwShift                      28

/*--------------------------------------
BitField Name: MPIGRsqEmiAccEcc
BitField Type: R/W
BitField Desc: ECC mode
               - 0x0: ECC disable
               - 0x1: ECC enable
--------------------------------------*/
#define cThaMPIGRsqEmiAccEccMask                       cBit27
#define cThaMPIGRsqEmiAccEccShift                      27

/*--------------------------------------
BitField Name: MPIGRsqEmiAccTyp
BitField Type: R/W
BitField Desc: Block mode
               - 0x0: One address MPIGRsqEmiAccAdd[22:0] is 8 Bytes
               - 0x1: One address MPIGRsqEmiAccAdd[22:1]is 16 Bytes
               - 0x2: One address MPIGRsqEmiAccAdd[22:2] is 32 Bytes
               - 0x3: One address MPIGRsqEmiAccAdd[22:3] is 64 Bytes
--------------------------------------*/
#define cThaMPIGRsqEmiAccTypMask                       cBit25_24
#define cThaMPIGRsqEmiAccTypShift                      24

/*--------------------------------------
BitField Name: MPIGRsqEmiAccAdd
BitField Type: R/W
BitField Desc: MPIGRsqEmiAccAdd to MPIGRsqEmiEndAdd.
--------------------------------------*/
#define cThaMPIGRsqEmiAccAddMask                       cBit22_0
#define cThaMPIGRsqEmiAccAddShift                      0

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ EMI Access Data Write
Reg Addr: 0x860108
          The address format for these registers is 0x860108
Reg Desc: These registers are used to Valid information.
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQEMIAcsDataWrite              				(0x860108)

#define cThaRegMPIGRSQEMIAcsDataWriteDwNum          4

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ EMI Access Address
Reg Addr: 0x860101
          The address format for these registers is 0x860101
Reg Desc: These registers are used to Emi access end address
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQEMIAcsAddress                 		(0x860101)

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ FrgBuf Init-Blk
Reg Addr: 0x860020-0x860020
          The address format for these registers is 0x860020
Reg Desc: These registers are used to Init Blk
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQFrgBufInitBlk                (0x860020)

/*--------------------------------------
BitField Name: MPIGRsqFrgbufInitBlkReq
BitField Type: R/W/SC
BitField Desc: SW set high Init Blk request, HW auto clear when request done
               (Init BlkId value from  MPIGRsqFrgbufInitBlkId to
               MPIGRsqFrgbufFreBlkId). Should seperate 3 step: Step1: Flush all
               fifo in Frgbuf by set and clear bit MPIGRsqFgbFsh at MPIG RSQ
               Control 2 register Step2: Write End BlkId at MPIG RSQ Frgbuf
               Free Blk register Step3: Write Start BlkId and init request to
               MPIG RSQ Frgbuf Init  Blk register
--------------------------------------*/
#define cThaMPIGRsqFrgbufInitBlkReqMask                cBit16
#define cThaMPIGRsqFrgbufInitBlkReqShift               16

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Frgbuf Free-Blk
Reg Addr: 0x860021-0x860021
          The address format for these registers is 0x860021
Reg Desc: These registers are used to Free Blk
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQFrgbufFreeBlk                 (0x860021)

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Oque Init-Blk
Reg Addr: 0x860030-0x860030
          The address format for these registers is 0x860030
Reg Desc: These registers are used to Init Blk
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQOqueInitBlk                  (0x860030)

/*--------------------------------------
BitField Name: MPIGRsqOqueInitBlkReq
BitField Type: R/W/SC
BitField Desc: SW set high Init Blk request, HW auto clear when request done
               (Init BlkId value from  MPIGRsqOqueInitBlkId to
               MPIGRsqOqueFreBlkId). Should seperate 3 step: Step1: Flush all
               fifo oque by set and clear bit MPIGRsqOquFsh at MPIG RSQ Control
               2 regiser Step2: Write End BlkId at MPIG RSQ Oque Free Blk
               register Step3: Write Start BlkId and request to MPIG RSQ Oque
               Init  Blk register
--------------------------------------*/
#define cThaMPIGRsqOqueInitBlkReqMask                  cBit16

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Oque Free Blk
Reg Addr: 0x860031-0x860031
          The address format for these registers is 0x860031
Reg Desc: These registers are used to Free Blk
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQOqueFreeBlk                  (0x860031)

/*--------------------------------------
BitField Name: MPIGRsqOqueFreBlkReq
BitField Type: R/W/SC
BitField Desc: SW set high Free Blk request, HW auto clear when request done
--------------------------------------*/
#define cThaMPIGRsqOqueFreBlkReqMask                   cBit16
#define cThaMPIGRsqOqueFreBlkReqShift                  16

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ MsbCtrl Init-Msb
Reg Addr: 0x860040-0x860040
          The address format for these registers is 0x860040
Reg Desc: These registers are used to Init Msb
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQMsbCtrlInitMsb               (0x860040)

/*--------------------------------------
BitField Name: MPIGRsqMsbCtrlInitMsbReq
BitField Type: R/W
BitField Desc: SW set high Init Msb request, HW auto clear when request done
               (Init MsbId value from  MPIGRsqMsbCtrlInitMsbId to
               MPIGRsqMsbCtrlFreMsbId). Init process include two steps:
               (<MPIGRsqMsbCtrlFreMsbId ; MPIGRsqMsbCtrlInitMsbId>) Step1:
               Write End MsbId at MPIG RSQ MsbCtrl Free-Msb register Step2:
               Write Start MsbId and init request to MPIG RSQ MsbCtrl Init  Msb
               register
--------------------------------------*/
#define cThaMPIGRsqMsbCtrlInitMsbReqMask               cBit16
#define cThaMPIGRsqMsbCtrlInitMsbReqShift              16

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ MsbCtrl Free-Msb
Reg Addr: 0x860041-0x860041
          The address format for these registers is 0x860041
Reg Desc: These registers are used to free one Msb ID
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQMsbCtrlFreeMsb                (0x860041)

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT Lookup Link state Control
Reg Addr: 0x850001
Reg Desc: The register provides channel for CPU control Link State of MPIG .
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATLookupLinkStateCtrl           					0x850001
#define cThaRegMPIGDATLookupLinkStateCtrlNoneEditableFieldsMask         (cBit31_13 | cBit9_7)

/*--------------------------------------
BitField Name: LinkState
BitField Type: R/W
BitField Desc: Current Link State. Updated by HW when HW changed state done
               - 0: START
               - 1: LINK INITIALIZE
               - 2: NETWORK INITIALIZE
               - 3: ACTIVE
--------------------------------------*/
#define cThaLinkStateMask                              cBit17_16
#define cThaLinkStateShift                             16

#define cThaRnwMask                                    cBit15
#define cThaRnwShift                                   15

/*--------------------------------------
BitField Name: Req[0]
BitField Type: R/W
BitField Desc: Request change state. CPU set to 1 to request change Link state
               and HW will set to 0 when change state done
--------------------------------------*/
#define cThaReqMask                                    cBit13
#define cThaReqShift                                   13

/*--------------------------------------
BitField Name: Cmd[2:0]
BitField Type: R/W
BitField Desc: Command request
               - 0: STOP     ? change to START state   from any states
               - 1: LINKUP   ? change to LINKINI state from STOP state
               - 2: LINKOPEN ? change to NETINI state  from LINKINI state
               - 3: RUN      ? change to ACTIVE state  from NETINI state
               - 4: ENABLE   ? change to ACTIVE state  from START state (for no
                 OAM protocol)
               - 5: LINKACT  ? change to ACTIVE state  from LINKINI state(for
                 NCP over MLPPP)
--------------------------------------*/
#define cThaCmdMask                                    cBit12_10
#define cThaCmdShift                                   10

/*--------------------------------------
BitField Name: LinkID[9:0]
BitField Type: R/W
BitField Desc: Link ID request
--------------------------------------*/
#define cThaLinkIDMask                                 cBit9_0
#define cThaLinkIDShift                                0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT De-Queue Link control
Address : 0x845800 � 0x845BFF
          The address format for these registers is 0x845800 + LinkID
Where   : LinkID (0 -1023)
Description: This register is used to configure control mode for per Link for MIPG De-Queue
------------------------------------------------------------------------------*/
#define cThaRegMPIGDeQueueLinkCtrl          					0x845800
#define cThaRegMPIGDeQueueLinkCtrlNoneEditableFieldsMask        cBit31_4

/*--------------------------------------
BitField Name: HighBw[4]
BitField Type: R/W
BitField Desc: High Bandwidth indicate, Set 1 for Ds3/E3/VC-4 HDLC channel.
--------------------------------------*/
#define cThaRegMPIGDeQueueHighBwCtrlMask  cBit4
#define cThaRegMPIGDeQueueHighBwCtrlShift 4

/*--------------------------------------
BitField Name: BlkAloc[3:0]
BitField Type: R/W
BitField Desc: The same value with BlkAloc[3:0] filed
                of MPIG DAT Lookup Link control  register
--------------------------------------*/
#define cThaRegMPIGDeQueueLinkCtrlMask  cBit3_0
#define cThaRegMPIGDeQueueLinkCtrlShift 0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT EMI Link Buffer Pool Address
Address : 0x848000 � 0x848BFF
          The address format for these registers is 0x848000 + LinkID*16 + BlkAdr
Where   : LinkID (0 -1023) is HDLC/PPP Link ID
          BlkAdr (0-15) is BlkAloc[3:0] at MPIG DAT Lookup Link control register.
Description: This register is used to configure address data buffer per Link
------------------------------------------------------------------------------*/
#define cThaRegMPIGDatEmiLinkBufPoolAddr     0x848000

#define cThaMPIGDatEmiLbpBlkVlMask  cBit9
#define cThaMPIGDatEmiLbpBlkVlShift 9

#define cThaMPIGDatEmiLbpBlkIDMask  cBit8_0
#define cThaMPIGDatEmiLbpBlkIDShift 0

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Per Alarm Interrupt Status
Reg Addr: 0x86C800-0x86CFFF
          The address format for these registers is 0x86C800 + GrpID*32 +
          BitID
          Where: GrpID: (0-63) Flow ID bits[10:5]
          BitsID: (0-31) Flow ID bits [4:0]
Reg Desc: This is the per Alarm interrupt status of flows. Each register is
          used to store 16 bits to status interrupts when the related alarms
          in flows happen.
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQPerAlmIntrStat                  0x86C800

/*------------------------------------------------------------------------------
Reg Name: MPIG RSQ Resequence Status
Reg Addr: 0x863000-0x8637FF
          The address format for these registers is 0x863000 + flowid
          Where: flowid (0-2047) is flow ID
Reg Desc: These registers are used to mon status resequence Per Flow, for HW
          Debug
------------------------------------------------------------------------------*/
#define cThaRegMPIGRSQResequenceStat                   0x863000

/*--------------------------------------
BitField Name: MPIGRsqCurrRsPktC
BitField Type: R/W
BitField Desc: Packet ready counter status
--------------------------------------*/
#define cThaMPIGRsqCurrRsPktCMask                      cBit23_20 /* cBit119_116 */
#define cThaMPIGRsqCurrRsPktCShift                     20
#define cThaMPIGRsqCurrRsPktCDwIndex                   3

/*--------------------------------------
BitField Name: MPIGRsqCurrRsBlkC
BitField Type: R/W
BitField Desc: Block counter status, each block store 32 fragment.
--------------------------------------*/
#define cThaMPIGRsqCurrRsBlkCMask                      cBit12_4 /* cBit108_100 */
#define cThaMPIGRsqCurrRsBlkCShift                     4
#define cThaMPIGRsqCurrRsBlkCDwIndex                   3

/*--------------------------------------
BitField Name: MPIGRsqCurrRsLong
BitField Type: R/W
BitField Desc: Long sequence status
--------------------------------------*/
#define cThaMPIGRsqCurrRsLongMask                      cBit24 /* cBit88 */
#define cThaMPIGRsqCurrRsLongShift                     24
#define cThaMPIGRsqCurrRsLongDwIndex                   2

/*--------------------------------------
BitField Name: MPIGRsqCurrRsMaxSta
BitField Type: R/W
BitField Desc: Maximum sequence receive status
--------------------------------------*/
#define cThaMPIGRsqCurrRsMaxStaMask                      cBit23_0 /* cBit87_64 */
#define cThaMPIGRsqCurrRsMaxStaShift                     0
#define cThaMPIGRsqCurrRsMaxStaDwIndex                   2

/*--------------------------------------
BitField Name: MPIGRsqCurrRsBuff
BitField Type: R/W
BitField Desc: Early Sequence Counter
--------------------------------------*/
#define cThaMPIGRsqCurrRsBuffMask                      cBit29_16 /* cBit61_48 */
#define cThaMPIGRsqCurrRsBuffShift                     16
#define cThaMPIGRsqCurrRsBuffDwIndex                   1

/*--------------------------------------
BitField Name: MPIGRsqCurrPaHead
BitField Type: R/W
BitField Desc: Current sequence to Packet assembler
--------------------------------------*/
#define cThaMPIGRsqCurrPaHeadMask                      cBit13_0 /* cBit45_32 */
#define cThaMPIGRsqCurrPaHeadShift                     0
#define cThaMPIGRsqCurrPaHeadDwIndex                   1

/*--------------------------------------
BitField Name: MPIGRsqCurrRsExpt
BitField Type: R/W
BitField Desc: Expected sequence, SW must write to Zero when flow initial
--------------------------------------*/
#define cThaMPIGRsqCurrRsExptMask                      cBit31_8
#define cThaMPIGRsqCurrRsExptShift                     8
#define cThaMPIGRsqCurrRsExptDwIndex                   0

/*--------------------------------------
BitField Name: MPIGRsqCurrRsCnt
BitField Type: R/W
BitField Desc: Resequence Good/Error counter, SW must write to Zero when flow
               initial
--------------------------------------*/
#define cThaMPIGRsqCurrRsCntMask                       cBit7_4
#define cThaMPIGRsqCurrRsCntShift                      4
#define cThaMPIGRsqCurrRsCntDwIndex                    0

/*--------------------------------------
BitField Name: MPIGRsqCurrRsFsm
BitField Type: R/W
BitField Desc: Resequence FSM, SW must write to Zero when flow initial
               - 0x0: RSFSM_INITIAL
               - 0x1: RSFSM_RESTART
               - 0x2: RSFSM_INIRST
               - 0x3: RSFSM_RESET
               - 0x4: RSFSM_RESYN_1
               - 0x8: RSFSM_SYNC_1
               - 0xC: RSFSM_DNRSQ
--------------------------------------*/
#define cThaMPIGRsqCurrRsFsmMask                       cBit3_0
#define cThaMPIGRsqCurrRsFsmShift                      0
#define cThaMPIGRsqCurrRsFsmDwIndex                    0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT Per Link Alarm Interrupt  Status
Reg Addr: 0x85C400 � 0x85C7FF
          The address format for these registers is 0x85C400 � 0x85C7FF
Reg Desc: This is the per Alarm interrupt status of ppp link.
Each register is used to store 16 bits to status interrupts when the related alarms in ppp link happen.
------------------------------------------------------------------------------*/
#define cThaMPIGDATLinkAlarmIntrStat                      0x85C400

/*------------------------------------------------------------------------------
Reg Name: ISIS
Reg Addr: 0x840110
Reg Desc:
------------------------------------------------------------------------------*/
/* MPIG DAT ISIS MAC Select Control */
#define cThaMPIG32LSB_ISISMacAddress 0x840110
#define cThaMPIG32MSB_ISISMacAddress 0x840111

#define cThaOamDirectReadControl    0x4
#define cThaOamDirectReadReadyMask  cBit15
#define cThaOamDirectReadEnd        cBit14
#define cThaOamDirectReadOper       cBit13
#define cThaOamDirectReadErr        cBit12
#define cThaOamDirectReadLengthMask cBit8_0
#define cThaOamDirectReadLengthShift 0

#define cThaOamDirectReadLinkId      0x5
#define cThaOamDirectReadLinkIdMask  cBit8_0
#define cThaOamDirectReadLinkIdShift 0
#define cThaOamDirectReadData        0x6

#define cThaOamDirectIntrControl     0x3
#define cThaOamDirectIntrMask        cBit0
#define cThaOamDirectIntrShift       0

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT PPP-PID Control
Reg Addr: 0x840120
          The address format for these registers is 0x840120 + PPP_TYPE
   Where:
   PPP_TYPE (0-5): Value to Insert ETH-TYPE  of MPIG Block, value 0 is lowest priority
Reg Desc: The register provides value PPP_PID of MPIG block or PPP/MLPPP Network packets
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATPppPIDCtrl                        0x840120

/*--------------------------------------
BitField Name: PPPPidValue
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaMPIGDatPPPPIDValueMask                    cBit15_0
#define cThaMPIGDatPPPPIDValueShift                   0

/*--------------------------------------
BitField Name: PPPPidMask
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaMPIGDatPPPPIDMaskMask                     cBit31_16
#define cThaMPIGDatPPPPIDMaskShift                    16

/*------------------------------------------------------------------------------
Reg Name: MPIG DAT ETH-TYPE Control
Reg Addr: 0x840130 + PPP_TYPE
   Where:
   PPP_TYPE (0-5): Value to Insert ETH-TYPE  of MPIG Block, value 0 is highest priority
Reg Desc: The register provides value PPP_PID of MPIG block or PPP/MLPPP Network packets
------------------------------------------------------------------------------*/
#define cThaRegMPIGDATEthTypeCtrl                      0x840130

/*--------------------------------------
BitField Name: DeqEthTypeValue
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaMPIGDatDeqEthTypeValueMask                 cBit15_0
#define cThaMPIGDatDeqEthTypeValueShift                0

#ifdef __cplusplus
}
#endif
#endif /* _THAMPIGREG_ */
#ifdef __cplusplus
extern "C" {
#endif

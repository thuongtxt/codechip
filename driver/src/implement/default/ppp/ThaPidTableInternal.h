/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPidTableInternal.h
 * 
 * Created Date: Sep 20, 2013
 *
 * Description : Table to translate between PID values and Ethernet type
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPIDTABLEINTERNAL_H_
#define _THAPIDTABLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ppp/AtPidTableInternal.h"
#include "ThaPidTable.h"
#include "ThaPidTableEntry.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPidTableMethods
    {
    uint32 (*EntryDefaultPid)(ThaPidTable self, uint32 entryIndex);
    uint32 (*EntryDefaultEthType)(ThaPidTable self, uint32 entryIndex);
    eBool  (*PartIsUnused)(ThaPidTable self, uint8 partId);
    }tThaPidTableMethods;

typedef struct tThaPidTable
    {
    tAtPidTable super;
    const tThaPidTableMethods *methods;
    }tThaPidTable;

typedef struct tThaPidTablePppLink
    {
    tThaPidTable super;

    /* Private data */
    AtPppLink pppLink;
    }tThaPidTablePppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTable ThaPidTableNew(AtModule module);
AtPidTable ThaPidTableObjectInit(AtPidTable self, AtModule module);
AtPidTable ThaPidTablePppLinkObjectInit(AtPidTable self, AtPppLink pppLink);

#ifdef __cplusplus
}
#endif
#endif /* _THAPIDTABLEINTERNAL_H_ */


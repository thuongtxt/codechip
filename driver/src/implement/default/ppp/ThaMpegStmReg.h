/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaMpegStmReg.h
 * 
 * Created Date: Dec 20, 2012
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMPEGSTMREG_H_
#define _THAMPEGSTMREG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaMpegReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------------------
BitField Name: MPEGUpOAMPktLen
BitField Type: R/W
BitField Desc: Length of  the CPU OAM  packet
--------------------------------------*/
#undef cThaMPEGUpOamPktLenMask
#undef cThaMPEGUpOamPktLenShift
#define cThaMPEGUpOamPktLenMask                        cBit21_10
#define cThaMPEGUpOamPktLenShift                       10

/*--------------------------------------
BitField Name: MPEGUpOAMLinkID
BitField Type: R/W
BitField Desc: Link ID that transmits the CPU OAM  packet
--------------------------------------*/
#undef cThaMPEGUpOamLinkIDMask
#undef cThaMPEGUpOamLinkIDShift
#define cThaMPEGUpOamLinkIDMask                        cBit9_0
#define cThaMPEGUpOamLinkIDShift                       0

/*--------------------------------------
BitField Name: MPEGBundleID
BitField Type: R/W
BitField Desc: Bundle ID that the PPP Link belonged to
--------------------------------------*/
#undef cThaMPEGPppLinkCtrl1BundleIDMask
#undef cThaMPEGPppLinkCtrl1BundleIDShift
#define cThaMPEGPppLinkCtrl1BundleIDMask               cBit7_1
#define cThaMPEGPppLinkCtrl1BundleIDShift              1

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress MLPPP Flow Sequence Status
Reg Addr: 0x253000 �  0x2533FF
          The address format for these registers is 0x253000 + MpFlowID
Where: MpFlowID (0 � 1023) is MLPPP Flow ID (Without Multi-Class Multilink, is BundleID)
Reg Desc: This register CPU access Mpflow sequence status
------------------------------------------------------------------------------*/
#define  cThaMPEGMlpppFlowSequenceStatus               0x253000

/*--------------------------------------
BitField Name: MPEGBundleID
BitField Type: R/W
BitField Desc: Bundle ID that the PPP Link belonged to
--------------------------------------*/
#define cThaMPEGMpflowCurrSequenceMaks                cBit23_0
#define cThaMPEGMpflowCurrSequenceShift               0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMPEGSTMREG_H_ */


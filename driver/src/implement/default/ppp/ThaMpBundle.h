/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP/MLPPP
 * 
 * File        : ThaMpBundle.h
 * 
 * Created Date: Sep 17, 2012
 *
 * Description : Thalassa MP Bundle
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMPBUNDLE_H_
#define _THAMPBUNDLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcBundle.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaMpBundle * ThaMpBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtMpBundle ThaMpBundleNew(uint32 channelId, AtModule module);
AtMpBundle ThaStmMpBundleNew(uint32 channelId, AtModule module);

uint32 ThaModulePppLocalBundleId(ThaMpBundle bundle);
uint32 ThaMpBundlePartOffset(ThaMpBundle self);
void ThaMlpppBundleQueueBlockSizeSet(ThaMpBundle self, uint8 queueId, uint8 rangeId);
eAtRet ThaMpBundleQueueEnable(ThaMpBundle self, uint8 queueId, eBool enable);
eAtRet ThaMpBundleNumberLinksSet(ThaMpBundle self, uint16 numLinkInBundle);
eAtRet ThaMpBundleHardwareBundleRemove(ThaMpBundle self);

/* For debugging */
void ThaModuleEncapHdlcBundleRegsShow(AtHdlcBundle channel);

AtEthFlow AtHdlcBundleOamFlowCreate(AtHdlcBundle self);
void AtHdlcBundleOamFlowDelete(AtHdlcBundle self);

/* Product concretes */
AtMpBundle Tha60031032MpBundleNew(uint32 channelId, AtModulePpp module);
AtMpBundle Tha60060011MpBundleNew(uint32 channelId, AtModulePpp module);
AtMpBundle Tha60091023MpBundleNew(uint32 channelId, AtModulePpp module);

#ifdef __cplusplus
}
#endif
#endif /* _THAMPBUNDLE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaMpigStmReg.h
 * 
 * Created Date: Dec 20, 2012
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMPIGSTMREG_H_
#define _THAMPIGSTMREG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaMpigReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*--------------------------------------
BitField Name: Data[23:14] is Link ID
BitField Type: R/W
BitField Desc: In case OAM packet infomation
--------------------------------------*/
#undef  cThaOamLinkIdMask
#undef  cThaOamLinkIdShift
#define cThaOamLinkIdMask                              cBit21_14
#define cThaOamLinkIdShift                             14

/*--------------------------------------
BitField Name: PassOamAdrCtlField[31]
BitField Type: R/W
BitField Desc: By pass PPP address/control field
--------------------------------------*/
#define cThaPassOamAdrCtlFieldMask                     cBit31
#define cThaPassOamAdrCtlFieldShift                    31


/*--------------------------------------
BitField Name: PassMLPHdrField[32]
BitField Type: R/W
BitField Desc: By pass MLPPP header field
--------------------------------------*/
#undef  cThaMlppHdrModeMask
#undef  cThaMlppHdrModeShift
#define cThaMlppHdrModeMask                            cBit30
#define cThaMlppHdrModeShift                           30

/*--------------------------------------
BitField Name: PassPPPPidField[29]
BitField Type: R/W
BitField Desc: By pass PPP protocol  field
--------------------------------------*/
#undef  cThaPidModeMask
#undef  cThaPidModeShift
#define cThaPidModeMask                                cBit29
#define cThaPidModeShift                               29

/*--------------------------------------
BitField Name: PassAdrCtlField[28]
BitField Type: R/W
BitField Desc: By pass Address Control field
--------------------------------------*/
#undef  cThaAddrCtrlModeMask
#undef  cThaAddrCtrlModeShift
#define cThaAddrCtrlModeMask                           cBit28
#define cThaAddrCtrlModeShift                          28

/*--------------------------------------
BitField Name: AdrComp[0]
BitField Type: R/W
BitField Desc: Address field compress mode
--------------------------------------*/
#undef  cThaAdrCompMask
#undef  cThaAdrCompShift
#define cThaAdrCompMask                                cBit27
#define cThaAdrCompShift                               27

/*--------------------------------------
BitField Name: BlkAloc[3:0]
BitField Type: R/W
BitField Desc: The number of block 256 segment 32-bytes is allocated for per
               link. Each block correspond with bandwidth of 2 DS0.
--------------------------------------*/
#undef  cThaBlkAlocMask
#undef  cThaBlkAlocShift
#define cThaBlkAlocMask                                cBit26_23
#define cThaBlkAlocShift                               23

/*--------------------------------------
BitField Name: FcsErrDropEn[22]
BitField Type: R/W
BitField Desc: FCS Error drop packet enable
--------------------------------------*/
#undef  cThaFcsErrDropEnMask
#undef  cThaFcsErrDropEnShift
#define cThaFcsErrDropEnMask                            cBit22
#define cThaFcsErrDropEnShift                           22

/*--------------------------------------
BitField Name: AdrCtlErrDropEn[21]
BitField Type: R/W
BitField Desc: Address Control Error drop packet enable
--------------------------------------*/
#undef  cThaAdrCtlErrDropEnMask
#undef  cThaAdrCtlErrDropEnShift
#define cThaAdrCtlErrDropEnMask                        cBit21
#define cThaAdrCtlErrDropEnShift                       21

/*--------------------------------------
BitField Name: NCPRule[1:0]
BitField Type: R/W
BitField Desc: The rule of NCP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaNCPRuleMask
#undef  cThaNCPRuleShift
#define cThaNCPRuleMask                                cBit20_19
#define cThaNCPRuleShift                               19

/*--------------------------------------
BitField Name: LCPRule[1:0]
BitField Type: R/W
BitField Desc: The rule of LCP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaLCPRuleMask
#undef  cThaLCPRuleShift
#define cThaLCPRuleMask                                cBit18_17
#define cThaLCPRuleShift                               17

/*--------------------------------------
BitField Name: RSQRule[1:0]
BitField Type: R/W
BitField Desc: The rule of packet which need re-sequence, such as MLPPP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaMlpRuleRuleMask
#undef  cThaMlpRuleRuleShift
#define cThaMlpRuleRuleMask                            cBit16_15
#define cThaMlpRuleRuleShift                           15

/*--------------------------------------
BitField Name: PppRule[10:7]
BitField Type: R/W
BitField Desc: The rule of packet which don't need re-sequence, such as
               HDLC/PPP PW packet or BCP/IP  non MLPPP packet
               - 0: Forward to PSN
               - 1: Forward to CPU Queue #1
               - 2: Forward to CPU Queue #2
               - 3: Discard
--------------------------------------*/
#undef  cThaPppRuleMask
#undef  cThaPppRuleShift
#define cThaPppRuleMask                                cBit14_13
#define cThaPppRuleShift                               13

/*--------------------------------------
BitField Name: PKtMode[6:4]
BitField Type: R/W
BitField Desc: Packet Encapsulation mode
    0: HDLC PW
    1: HDLC Ter (CISCO-HDLC), Base on PPP-PID to classify to packet type
    2: PPP PW
    3: PPP Ter (Termination). Base on PPP-PID to classify to packet type
    4: FR PW
    5: FR Ter
    6-7: Unused
--------------------------------------*/
#undef  cThaPKtModeMask
#undef  cThaPKtModeShift
#define cThaPKtModeMask                                 cBit12_10
#define cThaPKtModeShift                                10

/*--------------------------------------
BitField Name: BndID[3:0]
BitField Type: R/W
BitField Desc: Bundle ID. Look up from Link ID to Bundle ID
--------------------------------------*/
#undef  cThaBndIDMask
#undef  cThaBndIDShift
#define cThaBndIDMask                                  cBit9_0
#define cThaBndIDShift                                 0

/*--------------------------------------
BitField Name: FlwEnb[0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#undef  cThaFlwEnbMask
#undef  cThaFlwEnbShift
#define cThaFlwEnbMask                                 cBit11
#define cThaFlwEnbShift                                11

/*--------------------------------------
BitField Name: FlwID[10:0]k
BitField Type: R/W
BitField Desc: Flow or Pseudo-wire ID
--------------------------------------*/
#undef  cThaMPIGDATLookupFlwIDMask
#undef  cThaMPIGDATLookupFlwIDShift
#define cThaMPIGDATLookupFlwIDMask                     cBit10_0
#define cThaMPIGDATLookupFlwIDShift                    0

#undef  cThaMPIGDatEmiLbpBlkVlMask
#undef  cThaMPIGDatEmiLbpBlkVlShift
#define cThaMPIGDatEmiLbpBlkVlMask  cBit12
#define cThaMPIGDatEmiLbpBlkVlShift 12

#undef  cThaMPIGDatEmiLbpBlkIDMask
#undef  cThaMPIGDatEmiLbpBlkIDShift
#define cThaMPIGDatEmiLbpBlkIDMask  cBit11_0
#define cThaMPIGDatEmiLbpBlkIDShift 0

#ifdef __cplusplus
}
#endif
#endif /* _THAMPIGSTMREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPppLink.h
 * 
 * Created Date: Sep 17, 2012
 *
 * Description : Thalassa PPP Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPPPLINK_H_
#define _THAPPPLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPppLink.h" /* Its super class */
#include "ThaMpBundle.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cThaAddrOffsetCounterReadOnly   0x8000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPppLink * ThaPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel);
AtPppLink ThaPppLinkNew(AtHdlcChannel hdlcChannel);

eAtRet ThaPppLinkIgMlpppEnable(ThaPppLink self, eBool enable);
eAtRet ThaPppLinkEgMlpppEnable(ThaPppLink self, eBool enable);
eAtRet ThaPppLinkJoinBundle(ThaPppLink self, ThaMpBundle bundle);
eAtRet CounterMpbundleMakeReqst(AtChannel self, uint8 bValue,
                               uint32 counterAddr , eAtModule moduleId);
eAtRet ThaMpigDdrCounterGet(AtChannel self, uint32 cntAddr, eBool r2c, uint32 *pDdrVal);
eAtRet ThaMpegDdrCounterGet(AtChannel self, uint32 cntAddr, eBool r2c, uint32 *pDdrVal);
eBool ThaHwFinished(AtChannel self, uint32 address, uint32 bitMask, uint32 timeOut, eAtModule moduleId);
eAtRet ThaPppLinkBlockAllocate(AtPppLink self, uint16 numberBlock, eBool isHighBandwidth);
eAtRet ThaPppLinkBlockDeAllocate(AtPppLink self, uint16 numBlocks, eBool isHighBandwidth);
eAtRet ThaPppLinkAddressControlBypass(AtPppLink self, eBool addressControlByPass);
eBool ThaPppLinkAddressControlIsBypassed(AtPppLink self);
eAtRet ThaPppLinkAddressControlCompress(AtPppLink self, eBool compress);
eBool ThaPppLinkAddressControlIsCompressed(AtPppLink self);
eAtRet ThaPppLinkAddressControlErrorBypass(AtPppLink self, eBool bypass);
eBool ThaPppLinkAddressControlErrorIsBypassed(AtPppLink self);
eAtRet ThaPppLinkFcsErrorBypass(AtPppLink self, eBool bypass);
eBool ThaPppLinkFcsErrorIsBypassed(AtPppLink self);
void ThaPppLinkOamCountByPid(ThaPppLink self, uint16 pid);
eAtRet ThaPppLinkPppPacketEnable(ThaPppLink self, eBool enable);
eBool ThaPppLinkPppPacketIsEnabled(ThaPppLink self);

/* Type Header Vlan Lookup Type */
uint16 ThaPppLinkEthHeaderLen(AtHdlcLink self);
uint16 ThaPppLinkEthVlanLookupType(AtHdlcLink self);

/* Sync with hardware */
eBool ThaPppLinkHwIsReady(ThaPppLink self, uint32 address, uint32 bitMask, eAtModule moduleId);
eAtRet ThaPppLinkMemberLocalIdInBundleSet(ThaPppLink self , uint16 localMemberId);
eAtRet ThaPppLinkHardwareRemove(AtPppLink self);
eAtRet ThaPppLinkQueueBlockSizeSet(AtHdlcLink self, uint8 queueId, uint8 rangeId);
uint8 ThaPppLinkHwPsnType(ThaPppLink self);

uint32 ThaPppLinkOffset(ThaPppLink self);
uint32 ThaPppLinkPartOffset(ThaPppLink self);
void ThaPppLinkOamTxEnable(ThaPppLink self, eBool enable);

/* Product concretes */
AtPppLink Tha60031032PppLinkNew(AtHdlcChannel hdlcChannel);
AtPppLink Tha60060011PppLinkNew(AtHdlcChannel hdlcChannel);
AtPppLink Tha60091023PppLinkNew(AtHdlcChannel hdlcChannel);

eAtRet ThaHdlcLinkFlowCanBeBound(AtHdlcLink self, AtEthFlow flow);
AtEthFlow AtHdlcLinkOamFlowCreate(AtHdlcLink self);
void AtHdlcLinkOamFlowDelete(AtHdlcLink self);
#ifdef __cplusplus
}
#endif
#endif /* _THAPPPLINK_H_ */


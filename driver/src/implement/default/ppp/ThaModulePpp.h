/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaModulePpp.h
 * 
 * Created Date: Nov 24, 2012
 *
 * Description : PPP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPPP_H_
#define _THAMODULEPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePpp.h" /* Super class */
#include "ThaMpBundle.h"
#include "ThaPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePpp * ThaModulePpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePpp ThaModulePppNew(AtDevice device);
AtModulePpp ThaStmMlpppModuleStmPppNew(AtDevice device);

/* TODO: Need to move block management to classes */
/* Block management */
uint32 ThaModulePppFreeBlockGet(ThaModulePpp self, uint8 partId);
eAtRet ThaModulePppBlockUse(ThaModulePpp self, uint8 partId, uint32 blockId);
eAtRet ThaModulePppBlockUnuse(ThaModulePpp self, uint8 partId, uint32 blockId);
uint32 ThaModulePppNumFreeBlocks(ThaModulePpp self, uint8 partId);

uint32 ThaModulePppHighBandwidthFreeBlockGet(ThaModulePpp self, uint8 partId);
eAtRet ThaModulePppHighBandwidthBlockUse(ThaModulePpp self, uint8 partId, uint32 blockId);
eAtRet ThaModulePppHighBandwidthBlockUnuse(ThaModulePpp self, uint8 partId, uint32 blockId);
eBool ThaModulePppBlockHighBandwidthIsUsed(ThaModulePpp self, uint8 partId, uint32 blockId);
uint32 ThaModulePppMpigHighBwMaxNumBlocks(ThaModulePpp self);
uint32 ThaModulePppHighBandwidthNumFreeBlocks(ThaModulePpp self, uint8 partId);

uint8 ThaModulePppOamModeIsDirect(AtModulePpp self);

uint8 ThaModulePppNumParts(ThaModulePpp self);
uint32 ThaModulePppNumBundlePerPart(AtModulePpp self);
uint32 ThaModulePppPartOffset(ThaModulePpp self, uint8 partId);
uint8 ThaMpBundlePartOfPpp(ThaModulePpp self, ThaMpBundle bundle);

uint32 ThaModulePppNumBlocksForRange(ThaModulePpp self, uint8 rangeId);
uint8 ThaModulePppMaxNumQueuesPerLink(ThaModulePpp self);

/* Product concretes */
AtModulePpp Tha60031032ModulePppNew(AtDevice device);
AtModulePpp Tha60031033ModulePppNew(AtDevice device);
AtModulePpp Tha60070023ModulePppNew(AtDevice device);
AtModulePpp Tha60030022ModulePppNew(AtDevice device);
AtModulePpp Tha60030023ModulePppNew(AtDevice device);
AtModulePpp Tha60060011ModulePppNew(AtDevice device);
AtModulePpp Tha60070013ModulePppNew(AtDevice device);
AtModulePpp Tha60091023ModulePppNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPPP_H_ */


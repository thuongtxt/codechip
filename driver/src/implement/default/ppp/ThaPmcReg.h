/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO module name
 *
 * File        : ThaPmcReg.h
 *
 * Created Date: Aug 13, 2012
 *
 * Author      : nguyennt
 *
 * Description : TODO Description
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _THAPMCREG_H_
#define _THAPMCREG_H_

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Link Good Byte Counter
Reg Addr: 0x178000 � 0x17807F(R_O), 0x170000 � 0x17007F(R2C)
          The address format for these registers is 0x178000 + LinkId (R_O), 0x170000 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read good byte counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegLinkGoodByte(offsetCount)     (0x170000 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Link Good Packet Counter
Reg Addr: 0x178080 � 0x1780FF(R_O), 0x170080 � 0x1700FF(R2C)
          The address format for these registers is 0x178080 + LinkId (R_O), 0x170080 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read good packet counter
------------------------------------------------------------------------------*/
#define cThaRegPmcPmcMpegLinkGoodPkt(offsetCount)    (0x170080 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Link MLPPP Packet Counter
Reg Addr: 0x178100 � 0x17817F(R_O), 0x170100 � 0x17017F(R2C)
          The address format for these registers is 0x178100 + LinkId (R_O), 0x170100 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read MLPPP Packet counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegLinkMlpppPkt(offsetCount)  (0x170100 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Link PPP Packet Counter
Reg Addr: 0x178200 � 0x17827F(R_O), 0x170200 � 0x17027F(R2C)
          The address format for these registers is 0x178200 + LinkId (R_O), 0x170200 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read PPP Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegLinkPppPkt(offsetCount)       (0x170200 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Link OAM Packet Counter
Reg Addr: 0x178280 � 0x1782FF(R_O), 0x170280 � 0x1702FF(R2C)
          The address format for these registers is 0x178280 + LinkId (R_O), 0x170280 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read OAM Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegLinkOamPkt(offsetCount)   (0x170280 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Link Discarded Packet Counter
Reg Addr: 0x178300 � 0x17837F(R_O), 0x170300 � 0x17037F(R2C)
          The address format for these registers is 0x178300 + LinkId (R_O), 0x170370 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read OAM Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegLinkDiscardPkt(offsetCount)   (0x170300 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Link Good Byte Counter
Reg Addr: 0x178380 � 0x1783FF(R_O), 0x170380 � 0x1703FF(R2C)
          The address format for these registers is 0x178380 + LinkId (R_O), 0x170380 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read Good Byte Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigLinkGoodByte(offsetCount)     (0x170380 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Link Good Packet Counter
Reg Addr: 0x178400 � 0x17847F(R_O), 0x170400 � 0x17047F(R2C)
          The address format for these registers is 0x178400 + LinkId (R_O), 0x170400 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read Good Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigLinkGoodPkt(offsetCount)     (0x170400 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Link PPP Packet Counter
Reg Addr: 0x178480 � 0x1784FF(R_O), 0x170480 � 0x1704FF(R2C)
          The address format for these registers is 0x178480 + LinkId (R_O), 0x170480 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read PPP Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigLinkPppPkt(offsetCount)   (0x170480 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Link MLPPP Packet Counter
Reg Addr: 0x178500 � 0x17857F(R_O), 0x170500 � 0x17057F(R2C)
          The address format for these registers is 0x178500 + LinkId (R_O), 0x170500 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read MLPPP Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcPmcMpigLinkMlpppPkt(offsetCount)   (0x170500 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Link OAM Packet Counter
Reg Addr: 0x178580 � 0x1785FF(R_O), 0x170580 � 0x1705FF(R2C)
          The address format for these registers is 0x178580 + LinkId (R_O), 0x170580 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read OAM Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigLinkOamPkt(offsetCount)   (0x170580 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Link Discarded Packet Counter
Reg Addr: 0x178600 � 0x17867F(R_O), 0x170600 � 0x17067F(R2C)
          The address format for these registers is 0x178600 + LinkId (R_O), 0x170600 + LinkId (R2C)
          Where: LinkId (0 � 127) Link IDs
Reg Desc: Read Discarded Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigLinkDiscardPkt(offsetCount)   (0x170600 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Bundle Good Byte Counter
Reg Addr: 0x168000 � 0x16800F(R_O), 0x160000 � 0x16000F(R2C)
          The address format for these registers is 0x168000 + BundId (R_O), 0x160000 + Bundld (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Good Byte Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegBundleGoodByte(offsetCount)    (0x160000 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Bundle Fragment Counter
Reg Addr: 0x168010 � 0x16801F(R_O), 0x160010 � 0x16001F(R2C)
          The address format for these registers is 0x168010 + BundId (R_O), 0x160010 + BundId (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Fragment Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegBundleFragment(offsetCount)     (0x160010 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Bundle Good Packet Counter
Reg Addr: 0x168020 � 0x16802F(R_O), 0x160020 � 0x16002F(R2C)
          The address format for these registers is 0x168020 + BundId (R_O), 0x160020 + BundId (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Good Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegBundleGoodPkt(offsetCount)   (0x160020 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Bundle Discarded Packet Counter
Reg Addr: 0x168030 � 0x16803F(R_O), 0x160030 � 0x16003F(R2C)
          The address format for these registers is 0x168030 + BundId (R_O), 0x160030 + BundId (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Discarded Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegBundleDiscardPkt(offsetCount)  (0x160030 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Bundle Queue Good Packet Counter
Reg Addr: 0x168040 � 0x1680BF(R_O), 0x160040 � 0x1600BF(R2C)
          The address format for these registers is 0x168040 + QueId*16 + BundId (R_O), 0x160040 + QueId*16 + BundId (R2C)
          Where: BundId (0 � 15)  Bundle Ids, QueId (0 � 7) Queue ID per each bundle
Reg Desc: Read Bundle Queue Good Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegBundleQueGoodPkt(offsetCount)   (0x160040 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Bundle Queue Discarded Packet Counter
Reg Addr: 0x1680C0 � 0x16813F(R_O), 0x1600C0 � 0x16013F(R2C)
          The address format for these registers is 0x1680C0 + QueId*16 + BundId (R_O), 0x1600C0 + QueId*16 + BundId (R2C)
          Where: BundId (0 � 15)  Bundle Ids, QueId (0 � 7) Queue ID per each bundle
Reg Desc: Read Bundle Queue Discarded Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegBundleQueDisPkt(offsetCount)   (0x1600C0 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Bundle Good Byte Counter
Reg Addr: 0x168140 � 0x16814F(R_O), 0x160140 � 0x16014F(R2C)
          The address format for these registers is 0x168140 + BundId (R_O), 0x160140 + Bundld (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Good Byte Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigBundleGoodByte(offsetCount)   (0x160140 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Bundle Fragment Counter
Reg Addr: 0x168150 � 0x16815F(R_O), 0x160150 � 0x16015F(R2C)
          The address format for these registers is 0x168150 + BundId (R_O), 0x160150 + Bundld (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Fragment Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigBundleFragment(offsetCount)  (0x160150 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Bundle Good Packet Counter
Reg Addr: 0x168160 � 0x16816F(R_O), 0x160160 � 0x16016F(R2C)
          The address format for these registers is 0x168160 + BundId (R_O), 0x160160 + Bundld (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Good Byte Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigBundleGoodPkt(offsetCount)  (0x160160 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Bundle Discarded Packet Counter
Reg Addr: 0x168170 � 0x16817F(R_O), 0x160170 � 0x16017F(R2C)
          The address format for these registers is 0x168170 + BundId (R_O), 0x160170 + Bundld (R2C)
          Where: BundId (0 � 15)  Bundle IDs
Reg Desc: Read Bundle Discarded Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigBundleDiscardPkt(offsetCount)  (0x160170 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Flow Good Byte Counter
Reg Addr: 0x158000 � 0x15807F(R_O), 0x150000 � 0x15007F(R2C)
          The address format for these registers is 0x158000 + FlowId (R_O), 0x150000 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Good Byte Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegFlowGoodByte(offsetCount)   (0x150000 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Flow Fragment Counter
Reg Addr: 0x158080 � 0x1580FF(R_O), 0x150080 � 0x1500FF(R2C)
          The address format for these registers is 0x158080 + FlowId (R_O), 0x150080 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Fragment Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegFlowFragment(offsetCount)   (0x150080 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Flow Good Packet Counter
Reg Addr: 0x158100 � 0x15817F(R_O), 0x150100 � 0x15017F(R2C)
          The address format for these registers is 0x158100 + FlowId (R_O), 0x150100 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Good Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegFlowGoodPkt(offsetCount)   (0x150100 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Egress Flow Discarded Packet Counter
Reg Addr: 0x158180 � 0x1581FF(R_O), 0x150180 � 0x1501FF(R2C)
          The address format for these registers is 0x158180 + FlowId (R_O), 0x150180 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Discarded Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpegFlowDiscardPkt(offsetCount)   (0x150180 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Flow Good Byte Counter
Reg Addr: 0x158680 � 0x1586FF(R_O), 0x150680 � 0x1506FF(R2C)
          The address format for these registers is 0x158680 + FlowId (R_O), 0x150680 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Good byte Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigFlowGoodBytePkt(offsetCount)   (0x150680 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Flow Discarded Fragment Counter
Reg Addr: 0x158200 � 0x15827F(R_O), 0x150200 � 0x15027F(R2C)
          The address format for these registers is 0x158200 + FlowId (R_O), 0x150200 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Discarded Fragment Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigFlowDiscardFragmentPkt(offsetCount)   (0x150200 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Flow Good Packet Counter
Reg Addr: 0x158280 � 0x1582FF(R_O), 0x150280 � 0x1502FF(R2C)
          The address format for these registers is 0x158280 + FlowId (R_O), 0x150280 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Good Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigFlowGoodPkt(offsetCount)   (0x150280 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Flow MRRU Packet Counter
Reg Addr: 0x158300 � 0x15837F(R_O), 0x150300 � 0x15037F(R2C)
          The address format for these registers is 0x158300 + FlowId (R_O), 0x150300 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow MRRU Packet Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigFlowMRRUPkt(offsetCount)   (0x150300 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Flow Total Fragment Counter
Reg Addr: 0x158480 � 0x1584FF(R_O), 0x150480 � 0x1504FF(R2C)
          The address format for these registers is 0x158480 + FlowId (R_O), 0x150480 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Out Fragment Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigFlowTotalFragment(offsetCount)   (0x150480 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Flow Good Fragment Counter
Reg Addr: 0x158500 � 0x15857F(R_O), 0x150500 � 0x15057F(R2C)
          The address format for these registers is 0x158500 + FlowId (R_O), 0x150500 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow Out Fragment Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigFlowGoodFragment(offsetCount)   (0x150500 + (offsetCount))

/*------------------------------------------------------------------------------
Reg Name: MLPPP Ingress Flow M Fragment Violation Counter
Reg Addr: 0x158600 � 0x15867F(R_O), 0x150600 � 0x15067F(R2C)
          The address format for these registers is 0x158600 + FlowId (R_O), 0x150600 + Flowld (R2C)
          Where: FlowId (0 � 127)  Flow IDs
Reg Desc: Read Flow M Fragment Violation Counter
------------------------------------------------------------------------------*/
#define cThaRegPmcMpigFlowViolateMFragment(offsetCount)    (0x150600 + (offsetCount))

#endif /* _THAPMCREG_H_ */

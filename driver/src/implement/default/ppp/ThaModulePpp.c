/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP/MLPPP module
 *
 * File        : ThaModulePpp.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa PPP/MLPPP module default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtHdlcChannel.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/encap/AtHdlcBundleInternal.h"
#include "../../../generic/man/AtIpCoreInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../man/ThaIpCoreInternal.h"
#include "../eth/ThaModuleEth.h"
#include "../encap/ThaModuleEncap.h"
#include "ThaModulePppInternal.h"
#include "ThaPppLink.h"
#include "ThaMpBundleInternal.h"
#include "ThaMpigReg.h"
#include "ThaMpegReg.h"
#include "ThaModulePpp.h"
#include "ThaPidTableInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaWrRsqDdrInitTimeout          2000  /* ms */
#define cThaNumServedOamsAtOneTimeByIntr 1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePpp)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePppMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;
static tAtModulePppMethods m_AtModulePppOverride;

/* Cache super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShifts(OamLinkId)
mDefineMaskShifts(FlwEnb)
mDefineMaskShifts(MPIGDATLookupFlwID)

static eAtRet ReSequenceDdrInit(ThaModulePpp self, uint8 partId)
    {
    uint32 regVal[cThaLongRegMaxSize];
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModule mpigModule = AtDeviceModuleGet(device, cThaModuleMpig);
    AtIpCore ipcore = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)self), AtModuleDefaultCoreGet(mpigModule));
    uint32 partOffset = ThaModulePppPartOffset(self, partId);

    regVal[0] = mModuleHwRead(self, cThaRegMPIGRSQEMIAcsCtrl + partOffset);
    if ((regVal[0] & cThaMPIGRsqEmiAccDoneMask) == 0)
        return cAtErrorDevBusy;

    mMethodsGet(osal)->MemInit(osal, regVal, 0, sizeof(uint32) * cThaLongRegMaxSize);
    mModuleHwLongWrite((AtModule)self,
                       cThaRegMPIGRSQEMIAcsDataWrite + partOffset,
                       regVal,
                       cThaRegMPIGRSQEMIAcsDataWriteDwNum,
                       ipcore);

    /* Init full of DDR */
    regVal[0] = 0x1FFFFF;
    mModuleHwWrite(self, cThaRegMPIGRSQEMIAcsAddress + partOffset, regVal[0]);

    /* Write request */
    regVal[0] = 0;
    mFieldIns(&regVal[0], cThaMPIGRsqEmiAccBurstMask, cThaMPIGRsqEmiAccBurstShift, 1);
    mFieldIns(&regVal[0], cThaMPIGRsqEmiAccReqMask, cThaMPIGRsqEmiAccReqShift, 1);
    mFieldIns(&regVal[0], cThaMPIGRsqEmiAccRnwMask, cThaMPIGRsqEmiAccRnwShift, 0);
    mFieldIns(&regVal[0], cThaMPIGRsqEmiAccEccMask, cThaMPIGRsqEmiAccEccShift, 0);
    mFieldIns(&regVal[0], cThaMPIGRsqEmiAccTypMask, cThaMPIGRsqEmiAccTypShift, 0x3);
    mFieldIns(&regVal[0], cThaMPIGRsqEmiAccAddMask, cThaMPIGRsqEmiAccAddShift, 0x0);

    mModuleHwWrite(self, cThaRegMPIGRSQEMIAcsCtrl + partOffset, regVal[0]);

    /* Wait for request done */
    if(!ThaModuleHwFinished(self,
                            cThaRegMPIGRSQEMIAcsCtrl + partOffset,
                            cThaMPIGRsqEmiAccDoneMask,
                            cThaWrRsqDdrInitTimeout))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

/* This function is used to init all of buffer used by RSQ engine */
static eAtRet ReSequenceFragmentBufferInit(ThaModulePpp self, uint8 partId)
    {
    uint32 regVal;
    uint32 partOffset = ThaModulePppPartOffset(self, partId);
    uint32 regAddress = cThaRegMPIGRSQFrgBufInitBlk + partOffset;

    /* Fragment buffer */
    regVal = mModuleHwRead(self, regAddress);
    if ((regVal & cThaMPIGRsqFrgbufInitBlkReqMask) != 0)
        return cAtErrorDevBusy;

    mModuleHwWrite(self, cThaRegMPIGRSQFrgbufFreeBlk + partOffset, 0xFFFF);

    /* Write request */
    regVal = 0;
    mFieldIns(&regVal, cThaMPIGRsqFrgbufInitBlkReqMask, cThaMPIGRsqFrgbufInitBlkReqShift, 1);
    mModuleHwWrite(self, regAddress, regVal);

    /* Wait for request done */
    if(!ThaModuleHwFinished(self,
                            regAddress,
                            cThaMPIGRsqFrgbufInitBlkReqMask,
                            cThaWrRsqDdrInitTimeout))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

static eAtRet ReSequenceQueueInit(ThaModulePpp self, uint8 partId)
    {
    uint32 regVal;
    uint32 partOffset = ThaModulePppPartOffset(self, partId);
    uint32 regAddress = cThaRegMPIGRSQOqueInitBlk + partOffset;

    /* Queue */
    regVal = mModuleHwRead(self, regAddress);
    if ((regVal & cThaMPIGRsqOqueInitBlkReqMask) != 0)
        return cAtErrorDevBusy;

    mModuleHwWrite(self, cThaRegMPIGRSQOqueFreeBlk + partOffset, 0xFFFF);

    /* Write request */
    regVal = 0;
    mFieldIns(&regVal, cThaMPIGRsqOqueFreBlkReqMask, cThaMPIGRsqOqueFreBlkReqShift, 1);
    mModuleHwWrite(self, regAddress, regVal);

    /* Wait for request done */
    if(!ThaModuleHwFinished(self,
                            regAddress,
                            cThaMPIGRsqOqueFreBlkReqMask,
                            cThaWrRsqDdrInitTimeout))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

static eAtRet ReSequenceBufferInit(ThaModulePpp self, uint8 partId)
    {
    eAtRet ret;

    ret  = ReSequenceFragmentBufferInit(self, partId);
    ret |= ReSequenceQueueInit(self, partId);

    return ret;
    }

static void ReSequenceDefaultValSet(ThaModulePpp self, uint32 flowId, uint8 partId)
    {
    uint32 regVal = 0;
    uint32 regAddress = cThaRegMPIGRSQResequenceTimeout + flowId + ThaModulePppPartOffset(self, partId);

    mFieldIns(&regVal, cThaMPIGRsqCtlWoffMask, cThaMPIGRsqCtlWoffShift, 0x4);
    mFieldIns(&regVal, cThaMPIGRsqCtlThshMask, cThaMPIGRsqCtlThshShift, 64);
    mModuleHwWrite(self, cThaRegMPIGRSQResequenceCtrl + flowId, regVal);
    mModuleHwWrite(self, regAddress, 128);
    }

static eBool IsSimulated(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static void ResequenceEngineInit(ThaModulePpp self, uint8 partId)
    {
    uint32 i;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    uint32 partOffset = ThaModulePppPartOffset(self, partId);

    if (AtModulePppMaxBundlesGet((AtModulePpp)self) == 0)
        return;

    if (IsSimulated((AtModule)self))
        return;

    /* Disable RSQ engine */
    mModuleHwWrite(self, cThaRegMPIGRSQGlbCtrl1 + partOffset, 0);

    /* Flush FIFO */
    mModuleHwWrite(self, cThaRegMPIGRSQGlbCtrl2 + partOffset, 0xF);
    mModuleHwWrite(self, cThaRegMPIGRSQGlbCtrl2 + partOffset, 0x0);

    ReSequenceDdrInit(self, partId);
    ReSequenceBufferInit(self, partId);

    /* Enable RSQ engine */
    mModuleHwWrite(self, cThaRegMPIGRSQGlbCtrl1 + partOffset, 0x7);

    for (i = 0; i < ThaModuleEthMaxNumMpFlows(ethModule); i++)
        ReSequenceDefaultValSet((ThaModulePpp)self, i, partId);
    }

static void HeaderControlReset(ThaModulePpp self, uint8 partId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleEncap encapModule = (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    ThaModuleEth   ethModule   = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    uint32 i, partOffset = ThaModulePppPartOffset(self, partId);

    for (i = 0; i < ThaModuleEncapNumEncapPerPart(encapModule); i++)
        {
        mModuleHwWrite(self, cThaRegMPIGDATDeQueOamLinkCtrl + i + partOffset, 0);
        mModuleHwWrite(self, cThaRegMPIGDATDeQuePppLinkCtrl + i + partOffset, 0);
        }

    for (i = 0; i < ThaModuleEthNumMpFlowPerPart(ethModule); i++)
        mModuleHwWrite(self, cThaRegMPIGDATDeQueMpFlowCtrl + i + partOffset, 0);
    }

static uint32 NumBlocksForRange(ThaModulePpp self, uint8 rangeId)
    {
	AtUnused(rangeId);
	AtUnused(self);
    return 0x600;
    }

/* To be polymophism in the future */
static uint8 NumBlockRanges(ThaModulePpp self)
    {
	AtUnused(self);
    return 8;
    }

static void BlockRangesInit(ThaModulePpp self, uint8 partId)
    {
    uint8 range_i;

    /* Lookup rangeId to configure */
    for (range_i = 0; range_i < NumBlockRanges(self); range_i++)
        {
        uint32 regValue = 0;
        uint32 numBlocks = mMethodsGet(mThis(self))->NumBlocksForRange(self, range_i);
        mFieldIns(&regValue, cThaMpegBlockValueMask, cThaMpegBlockValueShift, numBlocks);
        mModuleHwWrite(self, cThaMpegBlockControl + range_i + ThaModulePppPartOffset(self, partId), regValue);
        }
    }

static void FragmentSizeDefault(ThaModulePpp self, uint8 partId)
    {
    uint32 i, regAddr, regVal;

    for (i = 0; i < ThaModulePppNumBundlePerPart((AtModulePpp)self); i++)
        {
        regAddr = cThaRegEgBundleScheduleCtrl + i + ThaModulePppPartOffset(self, partId);
        regVal = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, cThaRegMLPppEgrBundleFragSizeMsk, cThaRegMLPppEgrBundleFragSizeShift, 0x1);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static void SequenceModeDefault(ThaModulePpp self, uint8 partId)
    {
    uint32 i, regAddr, regVal;

    for (i = 0; i < AtModulePppMaxBundlesGet((AtModulePpp)self); i++)
        {
        regAddr = cThaRegEgBundleScheduleCtrl + i + ThaModulePppPartOffset(self, partId);
        regVal = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, cThaRegEgBundleScheduleSeqModMasks, cThaRegEgBundleScheduleSeqModShift, 0x1);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }
    
static uint8 NumParts(ThaModulePpp self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cAtModulePpp);
    }

static eAtRet PartDefaultSet(ThaModulePpp self, uint8 partId)
    {
    ResequenceEngineInit(self, partId);
    HeaderControlReset(self, partId);
    FragmentSizeDefault(self, partId);
    BlockRangesInit(self, partId);
    SequenceModeDefault(self, partId);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaModulePpp self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;
    ThaModulePpp modulePpp = (ThaModulePpp)self;

    /* Let the super do first */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    mMethodsGet(modulePpp)->DefaultSet(modulePpp);
    AtPidTableInit(AtModulePppPidTableGet((AtModulePpp)self));

    /* Interrupt should be disable as default. Enable it when listener is
     * registered on at least one channel */
    ret |= mMethodsGet(self)->InterruptEnable(self, cAtFalse);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eAtRet BlockUsageCreate(ThaModulePpp self)
    {
    uint32 memorySize;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    uint8 numParts   = ThaDeviceNumPartsOfModule(device, cAtModulePpp);
    AtOsal osal      = AtSharedDriverOsalGet();

    if (self->partBlockUsage)
        return cAtOk;

    memorySize = sizeof(tThaModulePppBlockUsage) * numParts;
    self->partBlockUsage = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (self->partBlockUsage == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, self->partBlockUsage, 0, memorySize);

    return cAtOk;
    }

static tThaModulePppBlockUsage *PartBlockUsage(ThaModulePpp self, uint8 partId)
    {
    if (self->partBlockUsage == NULL)
        return NULL;

    if (partId >= NumParts(self))
        return NULL;

    return &(self->partBlockUsage[partId]);
    }

static eAtRet LowBandwidthBlockUsageReset(ThaModulePpp self, uint8 partId)
    {
    uint32 memorySize;
    AtOsal osal = AtSharedDriverOsalGet();
    tThaModulePppBlockUsage *blockUsage = PartBlockUsage(self, partId);
    uint32 numBlocks;

    if (blockUsage == NULL)
        return cAtErrorRsrcNoAvail;

    numBlocks = mMethodsGet(self)->MpigMaxNumBlocks(self);
    memorySize = numBlocks * sizeof(uint8);
    if (blockUsage->blockIsUsed == NULL)
        {
        blockUsage->blockIsUsed = mMethodsGet(osal)->MemAlloc(osal, memorySize);
        if (blockUsage->blockIsUsed == NULL)
            return cAtErrorRsrcNoAvail;
        }
    mMethodsGet(osal)->MemInit(osal, blockUsage->blockIsUsed, 0, memorySize);

    blockUsage->numUsedBlocks = 0;
    blockUsage->numBlocks = numBlocks;

    return cAtOk;
    }

static eAtRet HighBandwidthBlockUsageReset(ThaModulePpp self, uint8 partId)
    {
    uint32 memorySize;
    AtOsal osal = AtSharedDriverOsalGet();
    tThaModulePppBlockUsage *blockUsage = PartBlockUsage(self, partId);
    uint32 numBlocks;

    if (blockUsage == NULL)
        return cAtErrorRsrcNoAvail;

    numBlocks = mMethodsGet(self)->MpigHighBwMaxNumBlocks(self);
    memorySize = numBlocks * sizeof(uint8);
    if (memorySize == 0)
        return cAtOk;

    if (blockUsage->highBwBlockIsUsed == NULL)
        {
        blockUsage->highBwBlockIsUsed = mMethodsGet(osal)->MemAlloc(osal, memorySize);
        if (blockUsage->highBwBlockIsUsed == NULL)
            return cAtErrorRsrcNoAvail;
        }
    mMethodsGet(osal)->MemInit(osal, blockUsage->highBwBlockIsUsed, 0, memorySize);

    blockUsage->numUsedHighBwBlocks = 0;
    blockUsage->numBlocks = numBlocks;

    return cAtOk;
    }

static eAtRet BlockUsageInit(ThaModulePpp self, uint8 partId)
    {
    eAtRet ret = cAtOk;

    ret |= LowBandwidthBlockUsageReset(self, partId);
    ret |= HighBandwidthBlockUsageReset(self, partId);

    return ret;
    }

static eAtRet Setup(AtModule self)
    {
    ThaModulePpp pppModule = (ThaModulePpp)self;
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    uint8 part_i, numParts = ThaDeviceNumPartsOfModule(device, cAtModulePpp);
    eAtRet ret;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Initialize block usage */
    ret = BlockUsageCreate(pppModule);
    if (ret != cAtOk)
        return ret;
    for (part_i = 0; part_i < numParts; part_i++)
        ret |= BlockUsageInit(pppModule, part_i);

    return ret;
    }

static eBool HwDidFinish(AtHal hal, uint32 address, uint32 bitMask, uint32 timeOut)
    {
    uint32 regValue;
    uint32 elapseTime;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Hardware finish its job */
    regValue = AtHalRead(hal, address);
    if ((regValue & bitMask) == 0)
        return cAtTrue;

    /* Timeout waiting for it */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < timeOut)
        {
        regValue = AtHalRead(hal, address);
        if ((regValue & bitMask) == 0)
            return cAtTrue;

        /* Get current time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);

        /* Calculate elapse time */
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static uint16 ReceivedOamFirstBlock(AtModulePpp self, AtIpCore core)
    {
    uint32 regValue;
    AtHal hal = AtIpCoreHalGet(core);
    uint16 firstBlock;
	AtUnused(self);

    regValue = AtHalRead(hal, cThaRegMPIGDATDeQueOamInfoget);
    mFieldGet(regValue, cThaOamIDMask, cThaOamIDShift, uint16, &firstBlock);

    return firstBlock;
    }

static AtPppLink PppLinkGet(AtModulePpp self, uint16 linkId)
    {
    AtHdlcChannel hdlcChannel;
    AtModuleEncap encapModule;
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    hdlcChannel = (AtHdlcChannel)AtModuleEncapChannelGet(encapModule, linkId);

    return (AtPppLink)AtHdlcChannelHdlcLinkGet(hdlcChannel);
    }

static uint32 IndirectReadOamOnCore(AtModulePpp self, AtIpCore core, uint8 *buffer, uint32 bufferLength, uint16 *pLinkId)
    {
    uint32 regValue[cThaLongRegMaxSize];
    uint32 address, pktLen;
    uint16 oamBlk;
    uint8  wIndex, *pBuffer, wIdx;
    eBool  discarded;
    static const uint32 cOamReadyTimeOut = 100;
    static const uint8 cThaMaxNumberReaddEngine = 4;
    AtHal hal = AtIpCoreHalGet(core);
    eBool hwIsReady;
    AtModule mpigModule;
    ThaModulePpp modulePpp = (ThaModulePpp)self;

    /* Check if OAM ready. If so, read the first block that OAM content is stored */
    address = cThaRegMPIGDATDeQueCpuReadCtrl;
    hwIsReady = (AtHalRead(hal, address) & cThaReqEnbMask) == 0;
    if (!hwIsReady)
        {
        modulePpp->oamNotReady++;
        return 0;
        }

    /* Make request to read this first block */
    regValue[0] = 0;
    oamBlk = ReceivedOamFirstBlock(self, core);
    mFieldIns(&regValue[0], cThaBlkIdMask, cThaBlkIdShift, oamBlk);
    mFieldIns(&regValue[0], cThaMPIGDATDeQueCpuReadSoPMask, cThaMPIGDATDeQueCpuReadSoPShift, 0x1);
    mFieldIns(&regValue[0], cThaReqEnbMask, cThaReqEnbShift, 0x1);
    AtHalWrite(hal, cThaRegMPIGDATDeQueCpuReadCtrl, regValue[0]);

    /* Wait hardware finish */
    if (!HwDidFinish(hal, address, cThaReqEnbMask, cOamReadyTimeOut))
        {
        modulePpp->hwTimeOut++;
        return 0;
        }

    /* HW finished, get message length and link */
    mpigModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleMpig);
    if (mpigModule == NULL)
        return 0;

    mMethodsGet(mpigModule)->HwLongReadOnCore(mpigModule,
                                              cThaRegMPIGDATDeQueOamDatacache,
                                              regValue,
                                              cThaNumDwordsInLongReg,
                                              core);
    mFieldGet(regValue[0], cThaDataLenMask, cThaDataLenShift, uint32, &pktLen);
    mFieldGet(regValue[0],
              mMethodsGet(modulePpp)->OamLinkIdMask(modulePpp),
              mMethodsGet(modulePpp)->OamLinkIdShift(modulePpp),
              uint16,
              pLinkId);
    for (wIndex = 1; wIndex < cThaMaxNumberReaddEngine; wIndex++)
        mMethodsGet(mpigModule)->HwLongReadOnCore(mpigModule,
                                                  cThaRegMPIGDATDeQueOamDatacache + wIndex,
                                                  regValue,
                                                  cThaNumDwordsInLongReg,
                                                  core);
    pktLen++;

    /* Get packet data, first 2 bytes will be used to store link Id */
    bufferLength -= 2;

    /* Not enough buffer to hold */
    discarded = cAtFalse;
    if (bufferLength < pktLen)
        {
        discarded = cAtTrue;
        pktLen = 31;
        }

    /* Get packet data, first 2 bytes will be used to store link Id */
    pBuffer = buffer + 2;
    for (wIndex = 1; wIndex<= ((pktLen / 32) + 1); wIndex++)
        {
        regValue[0] = 0;
        mFieldIns(&regValue[0], cThaSegIDMask, cThaSegIDShift, wIndex);
        mFieldIns(&regValue[0], cThaBlkIdMask, cThaBlkIdShift, oamBlk);

        if (wIndex == ((pktLen / 32) + 1))
            mFieldIns(&regValue[0], cThaMPIGDATDeQueCpuReadEoPMask, cThaMPIGDATDeQueCpuReadEoPShift, 1);
        else
            mFieldIns(&regValue[0], cThaMPIGDATDeQueCpuReadEoPMask, cThaMPIGDATDeQueCpuReadEoPShift, 0);

        mFieldIns(&regValue[0], cThaMPIGDATDeQueCpuReadSoPMask, cThaMPIGDATDeQueCpuReadSoPShift, 0);
        mFieldIns(&regValue[0], cThaReqEnbMask, cThaReqEnbShift, 1);
        AtHalWrite(hal, cThaRegMPIGDATDeQueCpuReadCtrl, regValue[0]);

        /* Wait for HW finish */
        if (!HwDidFinish(hal, address, cThaReqEnbMask, cOamReadyTimeOut))
            {
            modulePpp->hwTimeOut++;
            return 0;
            }

        for (wIdx = 0; wIdx < cThaMaxNumberReaddEngine; wIdx++)
            {
            mMethodsGet(mpigModule)->HwLongReadOnCore(mpigModule,
                                                      cThaRegMPIGDATDeQueOamDatacache + wIdx,
                                                      regValue,
                                                      cThaNumDwordsInLongReg,
                                                      core);

            *pBuffer = (uint8)(regValue[1] >> 24);
            pBuffer++;
            *pBuffer = (uint8)(regValue[1] >> 16);
            pBuffer++;
            *pBuffer = (uint8)(regValue[1] >> 8);
            pBuffer++;
            *pBuffer = (uint8)(regValue[1]);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0] >> 24);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0] >> 16);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0] >> 8);
            pBuffer++;
            *pBuffer = (uint8)(regValue[0]);
            pBuffer++;
            }
        }

    if (discarded == cAtTrue)
        {
        modulePpp->bufferNotEnough++;
        return 0;
        }

    /* First 2 bytes will be used to store link Id */
    *buffer = (uint8)((*pLinkId) >> 8);
    *(buffer + 1) = (uint8)(*pLinkId);
    pktLen += 2;

    return pktLen;
    }

static eBool OamIsReceivedOnCore(AtModulePpp self, AtIpCore core)
    {
    uint32 regValue;
    AtHal hal = AtIpCoreHalGet(core);

    AtUnused(self);

    regValue = AtHalRead(hal, cThaRegMPIGDATDeQueOamInfoget);
    if ((regValue & cThaOamReadyMask) == 0)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 OamReadIndirect(AtModulePpp self, AtIpCore ipCore, uint8 *oamBuffer, uint32 bufferLength, uint16 *linkId)
    {
    /* No OAM come */
    if (!OamIsReceivedOnCore((AtModulePpp)self, ipCore))
        return 0;

    /* Link id will be pushed into first 2 bytes of OAM packet */
    return IndirectReadOamOnCore((AtModulePpp)self, ipCore, oamBuffer, bufferLength, linkId);
    }

static uint32 OamLengthAndLinkIdGet(AtIpCore core, uint16 *linkId, uint8 *oamIsReliable)
    {
    AtHal hal;
    uint32 length;
    uint32 oamAddrOffset, regValue;

    hal = AtIpCoreHalGet(core);
    if (hal == 0)
        return 0;

    oamAddrOffset = mMethodsGet(hal)->DirectOamOffsetGet(hal);
    regValue = AtHalRead(hal, cThaOamDirectReadControl + oamAddrOffset) ;

    /* If OAM not ready, return 0 */
    if ((regValue & cThaOamDirectReadReadyMask) == 0)
        return 0;

    /* Otherwise regValue hold OAM length, get it and return */
    mFieldGet(regValue, cThaOamDirectReadLengthMask, cThaOamDirectReadLengthShift, uint32, &length);

    if (regValue & cThaOamDirectReadErr)
        *oamIsReliable = 0;
    else
        {
        *oamIsReliable = 1;

        /* Get link id */
        regValue = AtHalRead(hal, cThaOamDirectReadLinkId + oamAddrOffset);
        mFieldGet(regValue, cThaOamDirectReadLinkIdMask, cThaOamDirectReadLinkIdShift, uint16, linkId);
        }

    return length;
    }

static void OamDataOnCoreRead(AtIpCore ipCore, uint8 *oamBuffer, uint32 oamLength, uint8 oamIsValid)
    {
    uint32 i, oamOffset;
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint8 *pBuffer = oamBuffer;

    if (hal == NULL)
        return;

    oamOffset = mMethodsGet(hal)->DirectOamOffsetGet(hal);

    /* Start reading */
    AtHalWrite(hal, cThaOamDirectReadControl + oamOffset, cThaOamDirectReadOper);

    for (i = 0; i < oamLength/2 + 1; i++)
        {
        uint32 numReadBytes;
        uint32 regVal;

        /* Note, still read OAM to free hardware resource regardless OAM is valid or not */
        regVal = AtHalRead(hal, cThaOamDirectReadData + oamOffset);
        if (!oamIsValid)
            continue;

        /* Buffer is not enough */
        numReadBytes = (uint32)(pBuffer - oamBuffer);
        if (numReadBytes > oamLength)
            continue;

        /* Save first byte */
        *pBuffer = (uint8)(regVal >> 8);
        pBuffer++;

        /* And continue reading OAM regardless buffer is full or not. This is to
         * free hardware internal resource */
        numReadBytes = (uint32)(pBuffer - oamBuffer);
        if (numReadBytes > oamLength)
            continue;

        /* Save second byte */
        *pBuffer = (uint8)(regVal);
        pBuffer++;
        }

    /* Stop process */
    AtHalWrite(hal, cThaOamDirectReadControl + oamOffset, cThaOamDirectReadEnd);
    }

static uint32 OamReadDirect(AtModulePpp self, AtIpCore ipCore, uint8 *oamBuffer, uint32 bufferLength, uint16 *linkId)
    {
    uint32 oamLength;
    ThaModulePpp modulePpp = (ThaModulePpp)self;
    uint8 oamIsValid;

    if ((oamLength = OamLengthAndLinkIdGet(ipCore, linkId, &oamIsValid)) == 0)
        {
        modulePpp->oamNotReady++;
        return 0;
        }

    /* For safety */
    if(bufferLength < 2)
        return 0;

    /* LinkId will be push into first 2 bytes */
    *oamBuffer = (uint8)((*linkId) >> 8);
    *(oamBuffer + 1) = (uint8)(*linkId);
    oamBuffer += 2;
    bufferLength -= 2;

    if(oamLength > bufferLength)
        {
        modulePpp->bufferNotEnough++;
        oamIsValid = 0;
        }

    OamDataOnCoreRead(ipCore, oamBuffer, oamLength, oamIsValid);

    if (oamIsValid)
        {
        if ((oamLength < 4) || ((oamBuffer[0] != 0xFF) || (oamBuffer[1] != 0x03)))
            modulePpp->invalidAddrCtrl++;
        }

    /* If OAM is valid, return length of OAM + 2 byte contain link ID */
    return ((oamIsValid) ? oamLength + 2 : 0);
    }

static uint8 *SharedOamBuffer(ThaModulePpp self, uint32 *bufferSize)
    {
    *bufferSize = cThaMaxOamSizeInBytes;
    return self->oamBuffer;
    }

static void OamProcessOnCore(AtModule self, AtIpCore ipCore, uint16 numServedOamsAtOneTime)
    {
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleEncap);
    uint16 i;
    uint16 invalidLinkId = AtModuleEncapMaxChannelsGet(encapModule);

    for (i = 0; i < numServedOamsAtOneTime; i++)
        {
        uint16 linkId = invalidLinkId;
        AtPppLink pppLink;
        AtEncapChannel encapChannel;
        uint32 oamLength;
        uint8 *oamBuffer = SharedOamBuffer((ThaModulePpp)self, &oamLength);

        /* Link id will be pushed into first 2 bytes of OAM packet */
        oamLength = ThaModulePppReadOamOnCore((AtModulePpp)self, ipCore, oamBuffer, oamLength, &linkId);
        if (oamLength == 0)
            return;

        /* Notify all listeners about this OAM message */
        /* Grab link object first */
        encapChannel = AtModuleEncapChannelGet(encapModule, linkId);
        if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
            return;

        pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);

        /* Then notify */
        AtChannelOamReceivedNotify((AtChannel)pppLink, oamBuffer, oamLength);
        }
    }

static uint32 MpigMaxNumBlocks(ThaModulePpp self)
    {
	AtUnused(self);
    return 512;
    }

static void DeleteBlockUsage(ThaModulePpp self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    uint8 part_i;

    if (self->partBlockUsage == NULL)
        return;

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cAtModulePpp); part_i++)
        {
        tThaModulePppBlockUsage *partBlockUsage = PartBlockUsage(self, part_i);
        if (partBlockUsage == NULL)
            continue;

        mMethodsGet(osal)->MemFree(osal, partBlockUsage->blockIsUsed);
        partBlockUsage->blockIsUsed = NULL;
        mMethodsGet(osal)->MemFree(osal, partBlockUsage->highBwBlockIsUsed);
        partBlockUsage->highBwBlockIsUsed = NULL;
        }

    mMethodsGet(osal)->MemFree(osal, self->partBlockUsage);
    self->partBlockUsage = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteBlockUsage((ThaModulePpp)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void SerializeBlockUsage(AtObject blockUsage, AtCoder encoder)
    {
    tThaModulePppBlockUsage *object = (tThaModulePppBlockUsage *)blockUsage;

    mEncodeUInt8Array(blockIsUsed, object->numBlocks);
    mEncodeUInt(numUsedBlocks);
    mEncodeUInt8Array(highBwBlockIsUsed, object->numHighBwBlocks);
    mEncodeUInt(numUsedHighBwBlocks);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModulePpp object = mThis(self);
    uint32 maxNumParts = ThaDeviceNumPartsOfModule((ThaDevice)AtModuleDeviceGet((AtModule)self), cAtModulePpp);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectsWithHandler(partBlockUsage, maxNumParts, SerializeBlockUsage);
    mEncodeNone(oamBuffer);
    mEncodeUInt(interruptEnabled);
    mEncodeUInt(oamDirectMode);
    mEncodeUInt(oamNotReady);
    mEncodeNone(hwTimeOut);
    mEncodeUInt(bufferNotEnough);
    mEncodeUInt(invalidAddrCtrl);
    }

static AtMpBundle MpBundleObjectCreate(AtModulePpp self, uint32 bundleId)
    {
    return ThaMpBundleNew(bundleId, (AtModule)self);
    }

static AtPidTable PidTableObjectCreate(AtModulePpp self)
    {
    return ThaPidTableNew((AtModule)self);
    }

static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    return 16;
    }

static void IsrContextEnter(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);

    /* Itself */
    AtModuleIsrContextEnter(self);

    AtModuleIsrContextEnter(AtDeviceModuleGet(device, cThaModuleMpig));
    AtModuleIsrContextEnter(AtDeviceModuleGet(device, cThaModuleMpeg));
    }

static void IsrContextExit(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);

    AtModuleIsrContextExit(self);
    AtModuleIsrContextExit(AtDeviceModuleGet(device, cThaModuleMpig));
    AtModuleIsrContextExit(AtDeviceModuleGet(device, cThaModuleMpeg));
    }

static eAtRet Debug(AtModule self)
    {
    ThaModulePpp modulePpp = (ThaModulePpp)self;

    /* Super */
    eAtRet ret = m_AtModuleMethods->Debug(self);
    if (ret != cAtOk)
       return ret;

    /* Print PPP OAM counters  */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* OAM Counters:\r\n");
    AtPrintc(cSevNormal, "  OAM not ready           : %u\r\n", modulePpp->oamNotReady);
    AtPrintc(cSevNormal, "  HW time out             : %u\r\n", modulePpp->hwTimeOut);
    AtPrintc(cSevNormal, "  Buffer not enough       : %u\r\n", modulePpp->bufferNotEnough);
    AtPrintc(cSevNormal, "  Invalid address control : %u\r\n", modulePpp->invalidAddrCtrl);

    /* Clear counters */
    modulePpp->oamNotReady     = 0;
    modulePpp->hwTimeOut       = 0;
    modulePpp->bufferNotEnough = 0;
    modulePpp->invalidAddrCtrl = 0;
    return cAtOk;
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
	AtUnused(glbIntr);
    IsrContextEnter(self);

    /* Notify OAM received */
    OamProcessOnCore(self, ipCore, cThaNumServedOamsAtOneTimeByIntr);

    /* Clear OAM receive sticky */
    mMethodsGet((ThaModulePpp)self)->OamReceiveStickyClear((ThaModulePpp)self, ipCore);

    IsrContextExit(self);
    }

static void OamReceiveStickyClear(ThaModulePpp self, AtIpCore ipCore)
    {
	AtUnused(ipCore);
	AtUnused(self);
    return;
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCorePppHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = AtModuleDeviceGet(self);

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* Just write to HW if core is enabled */
        if (ipCore && (mMethodsGet(ipCore)->InterruptIsEnabled(ipCore)))
            InterruptHwEnable(ipCore, enable);

        /* Store to DB */
        ((ThaModulePpp)self)->interruptEnabled = enable;
        }

    return cAtOk;
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return ((ThaModulePpp)self)->interruptEnabled;
    }

/* Enable interrupt on one core */
static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    if (InterruptIsEnabled(self))
        InterruptHwEnable(ipCore, enable);
    }

static uint32 MpigHighBwMaxNumBlocks(ThaModulePpp self)
    {
	AtUnused(self);
    return 0;
    }

static eBool BlockIsValid(ThaModulePpp self, uint32 blockId)
    {
    return blockId < mMethodsGet(self)->MpigMaxNumBlocks(self);
    }

static eBool HighBandwidthBlockIsValid(ThaModulePpp self, uint32 blockId)
    {
    return blockId < mMethodsGet(self)->MpigHighBwMaxNumBlocks(self);
    }

static uint16 NumHighBwBlocksForOnePart(ThaModulePpp self)
    {
	AtUnused(self);
    return 16;
    }

static uint16 StartHighBandwidthFreeBlockIdForPart(ThaModulePpp self, uint8 partId)
    {
    return (uint16)(NumHighBwBlocksForOnePart(self) * partId);
    }

static eBool DirectOam(ThaModulePpp self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint8 MaxNumQueuesPerLink(ThaModulePpp self)
    {
	AtUnused(self);
    return 2;
    }

static void OverrideAtObject(ThaModulePpp self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(ThaModulePpp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, OamProcessOnCore);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePpp(ThaModulePpp self)
    {
    AtModulePpp pppModule = (AtModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, mMethodsGet(pppModule), sizeof(m_AtModulePppOverride));
        mMethodOverride(m_AtModulePppOverride, MpBundleObjectCreate);
        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        mMethodOverride(m_AtModulePppOverride, PidTableObjectCreate);
        }

    mMethodsSet(pppModule, &m_AtModulePppOverride);
    }

static void Override(ThaModulePpp self)
    {
    OverrideAtModulePpp(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaModulePpp self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, MpigMaxNumBlocks);
        mMethodOverride(m_methods, OamReceiveStickyClear);
        mMethodOverride(m_methods, MpigHighBwMaxNumBlocks);
        mMethodOverride(m_methods, NumBlocksForRange);
        mMethodOverride(m_methods, DirectOam);
        mMethodOverride(m_methods, MaxNumQueuesPerLink);
        mMethodOverride(m_methods, DefaultSet);

        mBitFieldOverrides(m_methods, OamLinkId);
        mBitFieldOverrides(m_methods, FlwEnb);
        mBitFieldOverrides(m_methods, MPIGDATLookupFlwID);
        }

    mMethodsSet((ThaModulePpp)self, &m_methods);
    }

AtModulePpp ThaModulePppObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModulePpp));

    /* Super constructor */
    if (AtModulePppObjectInit((AtModulePpp)self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModulePpp ThaModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModulePpp));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePppObjectInit(newModule, device);
    }

uint8 ThaModulePppOamModeIsDirect(AtModulePpp self)
    {
    return ((ThaModulePpp)self)->oamDirectMode;
    }

/*
 * Read OAM
 *
 * @param self This module
 * @param core Core want to read
 * @param [out] buffer Buffer to hold OAM message
 * @param bufferLength Buffer length
 * @param [out] pLinkId Link that OAM message come
 *
 * @return Size of OAM message. 0 if message is invalid
 */
uint32 ThaModulePppReadOamOnCore(AtModulePpp self, AtIpCore core, uint8 *oamBuffer, uint32 bufferLength, uint16 *linkId)
    {
    uint16 pid;
    ThaPppLink link;
    uint32 oamLength;

    if (ThaModulePppOamModeIsDirect(self))
        oamLength = OamReadDirect(self, core, oamBuffer, bufferLength, linkId);
    else
        oamLength = OamReadIndirect(self, core, oamBuffer, bufferLength, linkId);

    if (oamLength < 6)
        return oamLength;

    /* Counter OAM on link */
    pid = (uint16)((oamBuffer[4] << 8) | oamBuffer[5]);
    link = (ThaPppLink)PppLinkGet(self, *linkId);
    ThaPppLinkOamCountByPid(link, pid);

    return oamLength;
    }

uint32 ThaModulePppFreeBlockGet(ThaModulePpp self, uint8 partId)
    {
    tThaModulePppBlockUsage *partBlockUsage = PartBlockUsage(self, partId);
    uint32 maxNumBlock = mMethodsGet(self)->MpigMaxNumBlocks(self);

    if (partBlockUsage != NULL)
        {
        uint32 block_i;

        /* Find a free block */
        for (block_i = 0; block_i < maxNumBlock; block_i++)
            {
            if (!partBlockUsage->blockIsUsed[block_i])
                return block_i;
            }
        }

    /* Return invalid block */
    return maxNumBlock;
    }

eAtRet ThaModulePppBlockUse(ThaModulePpp self, uint8 partId, uint32 blockId)
    {
    tThaModulePppBlockUsage *blockUsage;

    if (!BlockIsValid(self, blockId))
        return cAtErrorInvlParm;

    blockUsage = PartBlockUsage(self, partId);
    if (blockUsage == NULL)
        return cAtErrorNullPointer;

    blockUsage->blockIsUsed[blockId] = cAtTrue;
    blockUsage->numUsedBlocks = blockUsage->numUsedBlocks + 1;

    return cAtOk;
    }

eAtRet ThaModulePppBlockUnuse(ThaModulePpp self, uint8 partId, uint32 blockId)
    {
    tThaModulePppBlockUsage *blockUsage;

    if (!BlockIsValid(self, blockId))
        return cAtErrorInvlParm;

    blockUsage = PartBlockUsage(self, partId);
    if (blockUsage == NULL)
        return cAtErrorNullPointer;

    blockUsage->blockIsUsed[blockId] = cAtFalse;
    blockUsage->numUsedBlocks = blockUsage->numUsedBlocks - 1;

    return cAtOk;
    }

uint32 ThaModulePppNumFreeBlocks(ThaModulePpp self, uint8 partId)
    {
    tThaModulePppBlockUsage *blockUsage = PartBlockUsage(self, partId);
    uint32 maxNumBlock = mMethodsGet(self)->MpigMaxNumBlocks(self);

    if (blockUsage)
        return (maxNumBlock - blockUsage->numUsedBlocks);

    return maxNumBlock;
    }

uint32 ThaModulePppHighBandwidthFreeBlockGet(ThaModulePpp self, uint8 partId)
    {
    tThaModulePppBlockUsage *blockUsage = PartBlockUsage(self, partId);
    uint32 maxNumBlocks = mMethodsGet(self)->MpigHighBwMaxNumBlocks(self);
    uint32 startBlockId, stopBlockId, block_i;

    if (blockUsage == NULL)
        return maxNumBlocks;

    startBlockId = StartHighBandwidthFreeBlockIdForPart(self, partId);
    stopBlockId  = startBlockId + NumHighBwBlocksForOnePart(self);
    for (block_i = startBlockId; block_i < stopBlockId; block_i++)
        {
        if (!blockUsage->highBwBlockIsUsed[block_i])
            return block_i;
        }

    /* Return invalid block */
    return maxNumBlocks;
    }

uint32 ThaModulePppPartOffset(ThaModulePpp self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cAtModulePpp, partId);
    }

eBool ThaModuleHwFinished(ThaModulePpp self, uint32 address, uint32 bitMask, uint32 timeOut)
    {
    uint32 regValue;
    uint32 elapseTime;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Hardware finish its job */
    regValue = mModuleHwRead(self, address);
    if ((regValue & bitMask) == 0)
        return cAtTrue;

    /* Timeout waiting for it */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < timeOut)
        {
        regValue = mModuleHwRead(self, address);
        if ((regValue & bitMask) == 0)
            return cAtTrue;

        /* Get current time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);

        /* Calculate elapse time */
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

eAtRet ThaModulePppHighBandwidthBlockUse(ThaModulePpp self, uint8 partId, uint32 blockId)
    {
    tThaModulePppBlockUsage *blockUsage;

    if (!HighBandwidthBlockIsValid(self, blockId))
        return cAtErrorInvlParm;

    blockUsage = PartBlockUsage(self, partId);
    if (blockUsage == NULL)
        return cAtErrorNullPointer;

    blockUsage->highBwBlockIsUsed[blockId] = cAtTrue;
    blockUsage->numUsedHighBwBlocks = blockUsage->numUsedHighBwBlocks + 1;

    return cAtOk;
    }

eAtRet ThaModulePppHighBandwidthBlockUnuse(ThaModulePpp self, uint8 partId, uint32 blockId)
    {
    tThaModulePppBlockUsage *blockUsage;

    if (!HighBandwidthBlockIsValid(self, blockId))
        return cAtErrorInvlParm;

    blockUsage = PartBlockUsage(self, partId);
    if (blockUsage == NULL)
        return cAtErrorNullPointer;

    blockUsage->highBwBlockIsUsed[blockId] = cAtFalse;
    blockUsage->numUsedHighBwBlocks = blockUsage->numUsedHighBwBlocks - 1;

    return cAtOk;
    }

uint32 ThaModulePppHighBandwidthNumFreeBlocks(ThaModulePpp self, uint8 partId)
    {
    tThaModulePppBlockUsage *blockUsage = PartBlockUsage(self, partId);
    uint32 maxNumBlocks = mMethodsGet(self)->MpigHighBwMaxNumBlocks(self);

    if (blockUsage)
        return maxNumBlocks - blockUsage->numUsedHighBwBlocks;

    return maxNumBlocks;
    }

uint32 ThaModulePppNumBundlePerPart(AtModulePpp self)
    {
    ThaDevice device;
    uint8 numParts;

    if (self == NULL)
        return 0;

    device   = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    numParts = ThaDeviceNumPartsOfModule(device, cAtModulePpp);
    return numParts ? (AtModulePppMaxBundlesGet(self) / numParts) : 0;
    }

uint32 ThaModulePppNumBlocksForRange(ThaModulePpp self, uint8 rangeId)
    {
    if (self)
        return mMethodsGet(self)->NumBlocksForRange(self, rangeId);
    return 0;
    }

uint8 ThaModulePppMaxNumQueuesPerLink(ThaModulePpp self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumQueuesPerLink(self);
    return 0;
    }

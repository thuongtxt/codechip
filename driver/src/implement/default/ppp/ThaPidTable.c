/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaPidTable.c
 *
 * Created Date: Sep 20, 2013
 *
 * Description : Table to translate between PID values and Ethernet type
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPidTableInternal.h"

#include "../man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPppPidIpV4Packet                   0x0021
#define cThaPppPidIpV6Packet                   0x0057
#define cThaPppPidMPLSUnicastPacket            0x0281
#define cThaPppPidMPLSMulticastPacket          0x0283
#define cThaPppPidIS_ISPacket                  0x0023
#define cThaPppPidBCPPacket                    0x0031

#define cThaPppEthernetTypeIpV4                0x0800
#define cThaPppEthernetTypeIpV6                0x86DD
#define cThaPppEthernetTypeIS_ISPacket         0xFEFE
#define cThaPppEthernetTypeMPLSUnicast         0x8847
#define cThaPppEthernetTypeMPLSMulticastPacket 0x8848
#define cThaPppEthernetTypeTransparentPacket   0xABCD

#define cInvalidValue 0xCAFE

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaPidTableMethods m_methods;

/* Override */
static tAtPidTableMethods m_AtPidTableOverride;

/* To save super implementation */
static const tAtPidTableMethods *m_AtPidTableMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPidTableEntry EntryObjectCreate(AtPidTable self, uint32 entryIndex)
    {
    return ThaPidTableEntryNew(self, entryIndex);
    }

static uint32 MaxNumEntries(AtPidTable self)
    {
	AtUnused(self);
    return 6;
    }

static uint32 EntryDefaultPid(ThaPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    if (entryIndex == 0) return cThaPppPidIpV4Packet;
    if (entryIndex == 1) return cThaPppPidIpV6Packet;
    if (entryIndex == 2) return cThaPppPidMPLSUnicastPacket;
    if (entryIndex == 3) return cThaPppPidMPLSMulticastPacket;
    if (entryIndex == 4) return cThaPppPidIS_ISPacket;
    if (entryIndex == 5) return cThaPppPidBCPPacket;

    return cInvalidValue;
    }

static uint32 EntryDefaultEthType(ThaPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    if (entryIndex == 0) return cThaPppEthernetTypeIpV4;
    if (entryIndex == 1) return cThaPppEthernetTypeIpV6;
    if (entryIndex == 2) return cThaPppEthernetTypeMPLSUnicast;
    if (entryIndex == 3) return cThaPppEthernetTypeMPLSMulticastPacket;
    if (entryIndex == 4) return cThaPppEthernetTypeIS_ISPacket;
    if (entryIndex == 5) return cThaPppEthernetTypeTransparentPacket;

    return cInvalidValue;
    }

static eAtRet EntryPsnToTdmInit(AtPidTableEntry entry, uint32 entryIndex)
    {
    eAtRet ret = cAtOk;
    ThaPidTable pidTable = (ThaPidTable)AtPidTableEntryTableGet(entry);
    uint32 ethTypeValue, pidValue;

    if (pidTable == NULL)
        return cAtOk;

    ethTypeValue = mMethodsGet(pidTable)->EntryDefaultEthType(pidTable, entryIndex);
    pidValue     = mMethodsGet(pidTable)->EntryDefaultPid(pidTable, entryIndex);
    ret |= AtPidTableEntryPsnToTdmPidSet(entry, pidValue);
    ret |= AtPidTableEntryPsnToTdmEthTypeSet(entry, ethTypeValue);
    ret |= AtPidTableEntryPsnToTdmEnable(entry, cAtTrue);

    return ret;
    }

static eAtRet EntryTdmToPsnInit(AtPidTableEntry entry, uint32 entryIndex)
    {
    eAtRet ret = cAtOk;
    ThaPidTable pidTable = (ThaPidTable)AtPidTableEntryTableGet(entry);
    uint32 ethTypeValue, pidValue;

    if (pidTable == NULL)
        return cAtOk;

    ethTypeValue = mMethodsGet(pidTable)->EntryDefaultEthType(pidTable, entryIndex);
    pidValue     = mMethodsGet(pidTable)->EntryDefaultPid(pidTable, entryIndex);
    ret |= AtPidTableEntryTdmToPsnPidSet(entry, pidValue);
    ret |= AtPidTableEntryTdmToPsnEthTypeSet(entry, ethTypeValue);
    ret |= AtPidTableEntryTdmToPsnEnable(entry, cAtTrue);

    return ret;
    }

static eAtRet EntryDefaultSet(AtPidTable self)
    {
    eAtRet ret = cAtOk;
    uint32 numEntries = AtPidTableMaxNumEntries(self);
    uint32 entry_i;

    for (entry_i = 0; entry_i < numEntries; entry_i++)
        {
        AtPidTableEntry entry = AtPidTableEntryGet(self, entry_i);
        if (!AtPidTableEntryIsEditable(self, entry_i))
            continue;

        ret |= EntryPsnToTdmInit(entry, entry_i);
        ret |= EntryTdmToPsnInit(entry, entry_i);
        }

    return ret;
    }

static eBool PartIsUnused(ThaPidTable self, uint8 partId)
    {
	AtUnused(partId);
	AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(ThaPidTable self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EntryDefaultPid);
        mMethodOverride(m_methods, EntryDefaultEthType);
        mMethodOverride(m_methods, PartIsUnused);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtPidTable(ThaPidTable self)
    {
    AtPidTable pidTable = (AtPidTable)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPidTableMethods = mMethodsGet(pidTable);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableOverride, m_AtPidTableMethods, sizeof(m_AtPidTableOverride));

        mMethodOverride(m_AtPidTableOverride, EntryObjectCreate);
        mMethodOverride(m_AtPidTableOverride, MaxNumEntries);
        mMethodOverride(m_AtPidTableOverride, EntryDefaultSet);
        }

    mMethodsSet(pidTable, &m_AtPidTableOverride);
    }

static void Override(ThaPidTable self)
    {
    OverrideAtPidTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPidTable);
    }

AtPidTable ThaPidTableObjectInit(AtPidTable self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPidTableObjectInit((AtPidTable)self, module) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaPidTable)self);
    MethodsInit((ThaPidTable)self);
    m_methodsInit = 1;

    return self;
    }

AtPidTable ThaPidTableNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTable table = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (table == NULL)
        return NULL;

    /* Construct it */
    return ThaPidTableObjectInit(table, module);
    }

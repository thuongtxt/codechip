/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaPidTableEntry.c
 *
 * Created Date: Sep 20, 2013
 *
 * Description : PID table entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPidTableEntryInternal.h"
#include "ThaPidTableInternal.h"
#include "ThaMpigReg.h"
#include "ThaMpegReg.h"

#include "../../../generic/ppp/AtPidTableInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../cla/ThaModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPidTableEntryMethods m_AtPidTableEntryOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModulePpp ModulePpp(AtPidTableEntry self)
    {
    return (AtModulePpp)AtPidTableModuleGet(AtPidTableEntryTableGet(self));
    }

static uint32 Read(AtPidTableEntry self, uint32 address, eAtModule moduleId)
    {
    uint8 part_i;
    ThaDevice device;
    AtModule moduleToRead;
    ThaPidTable pidTable = (ThaPidTable)AtPidTableEntryTableGet(self);

    if (pidTable == NULL)
        return 0;

    device = (ThaDevice)AtModuleDeviceGet((AtModule)ModulePpp(self));
    moduleToRead = AtDeviceModuleGet((AtDevice)device, moduleId);
    for (part_i = 0; part_i < ThaDeviceMaxNumParts(device); part_i++)
        {
        uint32 offset;

        if (mMethodsGet(pidTable)->PartIsUnused(pidTable, part_i))
            continue;

        offset = ThaDeviceModulePartOffset(device, moduleId, part_i);
        return mModuleHwRead(moduleToRead, address + offset);
        }

    return 0;
    }

static void Write(AtPidTableEntry self, uint32 address, uint32 value, eAtModule moduleId)
    {
    ThaPidTable pidTable   = (ThaPidTable)AtPidTableEntryTableGet(self);
    uint8 part_i;
    ThaDevice device;
    AtModule moduleToWrite;

    if (pidTable == NULL)
        return;

    device = (ThaDevice)AtModuleDeviceGet((AtModule)ModulePpp(self));
    moduleToWrite = AtDeviceModuleGet((AtDevice)device, moduleId);

    for (part_i = 0; part_i < ThaDeviceMaxNumParts(device); part_i++)
        {
        uint32 offset;

        if (mMethodsGet(pidTable)->PartIsUnused(pidTable, part_i))
            continue;

        offset = ThaDeviceModulePartOffset(device, moduleId, part_i);
        mModuleHwWrite(moduleToWrite, address + offset, value);
        }
    }

static eBool Editable(AtPidTableEntry self)
    {
    AtPidTable table = AtPidTableEntryTableGet(self);
    return AtPidTableEntryIsEditable(table, AtPidTableEntryIndexGet(self));
    }

static ThaModuleCla ClaModule(AtPidTableEntry self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ModulePpp(self));
    return (ThaModuleCla)AtDeviceModuleGet(device, cThaModuleCla);
    }

static eAtRet PsnToTdmPidSet(AtPidTableEntry self, uint32 pid)
    {
    uint32 regVal, regAddr;

    if (!Editable(self))
        return cAtErrorNotEditable;

    regAddr = cThaRegEgressPppProtocolCtrl + AtPidTableEntryIndexGet(self);
    regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    mRegFieldSet(regVal, cThaRegMlpppMPEGPPPID, pid);
    AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);

    return cAtOk;
    }

static eAtRet PsnToTdmEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    if (!Editable(self))
        return cAtErrorNotEditable;

    return ThaModuleClaPppPsnToTdmEthTypeSet(ClaModule(self), AtPidTableEntryIndexGet(self), ethType);
    }

static eAtRet PsnToTdmEnable(AtPidTableEntry self, eBool enable)
    {
    if (!Editable(self))
        return cAtErrorNotEditable;

    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 PsnToTdmPidGet(AtPidTableEntry self)
    {
    uint32 regAddr = cThaRegEgressPppProtocolCtrl + AtPidTableEntryIndexGet(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cThaRegMlpppMPEGPPPID);
    }

static uint32 PsnToTdmEthTypeGet(AtPidTableEntry self)
    {
    return ThaModuleClaPppPsnToTdmEthTypeGet(ClaModule(self), AtPidTableEntryIndexGet(self));
    }

static eBool PsnToTdmIsEnabled(AtPidTableEntry self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtRet TdmToPsnPidSet(AtPidTableEntry self, uint32 pid)
    {
    uint32 hwEntry = AtPidTableEntryIndexGet(self);
    uint32 regVal, regAddr;

    if (!Editable(self))
        return cAtErrorNotEditable;

    regAddr = cThaRegMPIGDATPppPIDCtrl + hwEntry;
    regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    mRegFieldSet(regVal, cThaMPIGDatPPPPIDValue, pid);
    AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);

    return cAtOk;
    }

static eAtRet TdmToPsnEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    uint32 regVal, regAddr;

    if (!Editable(self))
        return cAtErrorNotEditable;

    regAddr = cThaRegMPIGDATEthTypeCtrl + AtPidTableEntryIndexGet(self) ;
    regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    mRegFieldSet(regVal, cThaMPIGDatDeqEthTypeValue, ethType);
    AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);

    return cAtOk;
    }

static eAtRet TdmToPsnEnable(AtPidTableEntry self, eBool enable)
    {
    if (!Editable(self))
        return cAtErrorNotEditable;

    /* Hardware has not supported disabling */
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 TdmToPsnPidGet(AtPidTableEntry self)
    {
    uint32 regAddr = cThaRegMPIGDATPppPIDCtrl + AtPidTableEntryIndexGet(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cThaMPIGDatPPPPIDValue);
    }

static uint32 TdmToPsnEthTypeGet(AtPidTableEntry self)
    {
    uint32 regAddr = cThaRegMPIGDATEthTypeCtrl + AtPidTableEntryIndexGet(self);
    uint32 regVal = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cThaMPIGDatDeqEthTypeValue);
    }

static eBool TdmToPsnIsEnabled(AtPidTableEntry self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eAtRet TdmToPsnPidMaskSet(AtPidTableEntry self, uint16 pidMask)
    {
    uint32 regVal, regAddr;

    if (!Editable(self))
        return cAtErrorNotEditable;

    regAddr = cThaRegMPIGDATPppPIDCtrl + AtPidTableEntryIndexGet(self);
    regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    mRegFieldSet(regVal, cThaMPIGDatPPPPIDMask, pidMask);
    AtPidTableEntryWrite(self, regAddr, regVal, cAtModulePpp);

    return cAtOk;
    }

static uint32 TdmToPsnPidMaskGet(AtPidTableEntry self)
    {
    uint32 regAddr = cThaRegMPIGDATPppPIDCtrl + AtPidTableEntryIndexGet(self);
    uint32 regVal  = AtPidTableEntryRead(self, regAddr, cAtModulePpp);
    return mRegField(regVal, cThaMPIGDatPPPPIDMask);
    }

static void OverrideAtPidEntryTable(AtPidTableEntry self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableEntryOverride, mMethodsGet(self), sizeof(m_AtPidTableEntryOverride));

        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEthTypeSet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEnable);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmPidGet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEthTypeGet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmIsEnabled);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEthTypeSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEnable);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEthTypeGet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnIsEnabled);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidGet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidMaskSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidMaskGet);
        mMethodOverride(m_AtPidTableEntryOverride, Read);
        mMethodOverride(m_AtPidTableEntryOverride, Write);
        }

    mMethodsSet(self, &m_AtPidTableEntryOverride);
    }

static void Override(AtPidTableEntry self)
    {
    OverrideAtPidEntryTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPidTableEntry);
    }

AtPidTableEntry ThaPidTableEntryObjectInit(AtPidTableEntry self, uint32 entryIndex, AtPidTable table)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPidTableEntryObjectInit(self, entryIndex, table) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTableEntry ThaPidTableEntryNew(AtPidTable self, uint32 entryIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTableEntry entry = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (entry == NULL)
        return NULL;

    /* Construct it */
    return ThaPidTableEntryObjectInit(entry, entryIndex, self);
    }

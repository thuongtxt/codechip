/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaPidTablePppLinkPppLink.c
 *
 * Created Date: Jan 21, 2014
 *
 * Description : PID table for each PPP link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPidTableInternal.h"

#include "../man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPidTablePppLink)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPidTablePppLink);
    }

static AtModule PppModule(AtPidTable self, AtPppLink pppLink)
    {
	AtUnused(self);
    return AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pppLink), cAtModulePpp);
    }

AtPidTable ThaPidTablePppLinkObjectInit(AtPidTable self, AtPppLink pppLink)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPidTableObjectInit((AtPidTable)self, PppModule(self, pppLink)) == NULL)
        return NULL;

    mThis(self)->pppLink = pppLink;
    m_methodsInit = 1;

    return self;
    }

AtPppLink ThaPidTablePppLinkGet(ThaPidTablePppLink self)
    {
    return self ? self->pppLink : NULL;
    }

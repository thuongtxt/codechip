/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaStmPppLinkInternal.h
 * 
 * Created Date: Jan 23, 2013
 *
 * Description : PPP link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPPPLINKINTERNAL_H_
#define _THASTMPPPLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPppLinkInternal.h"
#include "ThaStmPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPppLinkMethods
    {
    /* Bypass OAM address/control */
    eBool (*OamAddrCtrlIsBypass)(ThaStmPppLink self);
    }tThaStmPppLinkMethods;

typedef struct tThaStmPppLink
    {
    tThaPppLink super;
    const tThaStmPppLinkMethods *methods;

    /* Private data */
    }tThaStmPppLink;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaStmPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel);
#ifdef __cplusplus
}
#endif
#endif /* _THASTMPPPLINKINTERNAL_H_ */


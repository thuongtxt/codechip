/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP/MLPPP
 * 
 * File        : ThaMpBundleInternal.h
 * 
 * Created Date: Sep 2, 2012
 *
 * Description : Thalassa MLPPP bundle class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMPBUNDLEINTERNAL_H_
#define _THAMPBUNDLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePpp.h"

#include "../../../generic/ppp/AtMpBundleInternal.h"
#include "../eth/controller/ThaEthFlowController.h"
#include "../util/ThaUtil.h"

#include "ThaModulePppInternal.h"
#include "ThaMpBundle.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cHardwareBundleRemoveTimeOut           30000 /* ms */

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaMpBundleMethods
    {
    uint32 (*DefaultOffset)(ThaMpBundle self);
    uint32 (*CounterAddress)(ThaMpBundle self, uint32 baseAddress);
    uint32 (*ClassDefaultOffset)(ThaMpBundle self, uint8 classId);
    uint32 (*QueueDefaultOffset)(ThaMpBundle self, uint8 queueId);
    uint32 (*QueueSchedulerOffset)(ThaMpBundle self, uint8 queueId);
    uint8  (*MaxNumQueues)(ThaMpBundle self);
    eAtRet (*QueueEnable)(ThaMpBundle self, uint8 queueId, eBool enable);
    eAtModulePppRet (*FlowClassAssign)(ThaMpBundle self, AtEthFlow flow, uint8 classNumber);
    eAtModulePppRet (*FlowClassDeassign)(ThaMpBundle self, AtEthFlow flow);
    eBool (*FlowCanJoin)(ThaMpBundle self, AtEthFlow flow, uint8 classNumber);
    eAtRet (*NumberLinksSet)(ThaMpBundle self, uint16 numLinkInBundle);
    void (*QueueBlockSizeSet)(ThaMpBundle self, uint8 queueId, uint8 rangeId);

    eAtRet (*HardwareBundleRemove)(ThaMpBundle self);
    eAtRet (*LinkHwRemove)(ThaMpBundle self, AtHdlcLink link);
    }tThaMpBundleMethods;

typedef struct tThaMpBundle
    {
    tAtMpBundle super;
    tThaMpBundleMethods *methods;

    /* Private data */
    uint8 maxDelay;
    uint32 mrru;

    uint32 maxMsWaitHw;
    uint32 curMsWaitHw;
    }tThaMpBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMpBundle ThaMpBundleObjectInit(AtMpBundle self, uint32 channelId, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAMPBUNDLEINTERNAL_H_ */


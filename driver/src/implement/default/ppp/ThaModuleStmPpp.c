/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaModuleStmPpp.c
 *
 * Created Date: Dec 3, 2012
 *
 * Description : PPP module of STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/ThaDevice.h"
#include "ThaModulePppInternal.h"
#include "ThaStmMpBundle.h"
#include "ThaMpigReg.h"
#include "ThaMpigStmReg.h"
#include "ThaModuleStmPpp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtModulePppMethods m_AtModulePppOverride;
static tThaModulePppMethods m_ThaModulePppOverride;

/* Cache super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShifts(OamLinkId)
mDefineMaskShifts(FlwEnb)
mDefineMaskShifts(MPIGDATLookupFlwID)

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleStmPpp);
    }

static AtMpBundle MpBundleObjectCreate(AtModulePpp self, uint32 bundleId)
    {
    return ThaStmMpBundleNew(bundleId, (AtModule)self);
    }

static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    return 128;
    }

static eBool IsSimulated(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static uint8 NumParts(ThaModulePpp self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cAtModulePpp);
    }

static eAtRet ReSequenceMsbInit(ThaModulePpp self, uint8 partId)
    {
    uint32 regVal;
    uint32 partAddr = cThaRegMPIGRSQMsbCtrlInitMsb + ThaModulePppPartOffset(self, partId);
    static const uint32 cThaWrRsqDdrInitTimeout = 2000;  /* ms */

    if (IsSimulated((AtModule)self))
        return cAtOk;

    /* MSB */
    regVal = mModuleHwRead(self, partAddr);
    if ((regVal & cThaMPIGRsqMsbCtrlInitMsbReqMask) != 0)
        return cAtErrorDevBusy;

    mModuleHwWrite(self, cThaRegMPIGRSQMsbCtrlFreeMsb, 0xFFFF);

    /* Write request */
    regVal = 0;
    mFieldIns(&regVal, cThaMPIGRsqMsbCtrlInitMsbReqMask, cThaMPIGRsqMsbCtrlInitMsbReqShift, 1);
    mModuleHwWrite(self, partAddr, regVal);

    /* Wait for request done */
    if(!ThaModuleHwFinished(self,
                            partAddr,
                            cThaMPIGRsqMsbCtrlInitMsbReqMask,
                            cThaWrRsqDdrInitTimeout))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

static void InitializeCounter(ThaModulePpp self, uint8 partId)
    {
    mModuleHwWrite(self, 0xc00104 + ThaModulePppPartOffset(self, partId), 1);
    mModuleHwWrite(self, 0xC00021 + ThaModulePppPartOffset(self, partId), 0x3fff);
    }

static eAtRet PartDefaultSet(ThaModulePpp self, uint8 partId)
    {
    InitializeCounter(self, partId);
    ReSequenceMsbInit(self, partId);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModulePpp self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    /* Super */
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Init re-sequence engine and counter */
    return DefaultSet((ThaModulePpp)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OverrideAtModule(ThaModulePpp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    AtModulePpp pppModule = (AtModulePpp)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, mMethodsGet(pppModule), sizeof(m_AtModulePppOverride));
        mMethodOverride(m_AtModulePppOverride, MpBundleObjectCreate);
        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        }

    mMethodsSet(pppModule, &m_AtModulePppOverride);
    }

static uint32 MpigMaxNumBlocks(ThaModulePpp self)
    {
	AtUnused(self);
    return 4096;
    }

static uint32 MpigHighBwMaxNumBlocks(ThaModulePpp self)
    {
	AtUnused(self);
    return 16;
    }

static uint32 NumBlocksForRange(ThaModulePpp self, uint8 rangeId)
    {
	AtUnused(self);
    if (rangeId == 0) return 0x100;
    if (rangeId == 1) return 0x200;
    if (rangeId == 2) return 0x400;
    if (rangeId == 3) return 0x800;
    if (rangeId == 4) return 0xFFF;
    if (rangeId == 7) return 0x3FFF;

    return 0x600;
    }

static void OverrideThaModulePpp(AtModulePpp self)
    {
    ThaModulePpp pppModule = (ThaModulePpp)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, mMethodsGet(pppModule), sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, MpigMaxNumBlocks);
        mMethodOverride(m_ThaModulePppOverride, MpigHighBwMaxNumBlocks);
        mMethodOverride(m_ThaModulePppOverride, NumBlocksForRange);
        mBitFieldOverrides(m_ThaModulePppOverride, OamLinkId)
        mBitFieldOverrides(m_ThaModulePppOverride, FlwEnb)
        mBitFieldOverrides(m_ThaModulePppOverride, MPIGDATLookupFlwID)
        }

    mMethodsSet(pppModule, &m_ThaModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtModule((ThaModulePpp)self);
    OverrideAtModulePpp(self);
    OverrideThaModulePpp(self);
    }

AtModulePpp ThaModuleStmPppObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

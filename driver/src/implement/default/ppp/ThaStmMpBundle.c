/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaStmMpBundle.c
 *
 * Created Date: Dec 3, 2012
 *
 * Description : MLPPP bundle of STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmMpBundleInternal.h"
#include "ThaMpegStmReg.h"
#include "ThaMpigStmReg.h"
#include "ThaStmPmcReg.h"
#include "ThaPppLink.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/

/* Define number counter bundle in Hw */
#define cThaPMCMpegBundRxByte    0
#define cThaPMCMpegBundRxPkt     1
#define cThaPMCMpegBundRxDisc    2
#define cThaPMCMpigBundTxPkt     11
#define cThaPMCMpigBundTxByte    12
#define cThaPMCMpigBundTxDisc    13

#define cThaMlpppIndrCounterCheckTime 8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaMpBundle)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods   m_AtChannelOverride;
static tAtMpBundleMethods  m_AtMpBundleOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtMpBundleMethods *m_AtMpBundleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool ThaPmcIndirectCounterIsAccessible(AtChannel self, eAtModule moduleId, uint8  partId);
extern eBool ThaPmcIndirectCounterIsValid(AtChannel self,eAtModule moduleId, uint32 timeoutMs, uint8  partId);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmMpBundle);
    }

static uint32 CounterAddress(AtChannel self)
    {
    return mMethodsGet(mThis(self))->DefaultOffset(mThis(self)) + 0x0400;
    }

static uint8 PppBundlePartIdGet(ThaMpBundle self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModulePpp modulePpp = (ThaModulePpp)AtDeviceModuleGet(device, cAtModulePpp);

    return ThaMpBundlePartOfPpp(modulePpp, self);
    }

static eAtRet MlpppCounterMpigGet(AtChannel self, tAtHdlcBundleCounters *pCounters, eBool r2c)
    {
    uint32 localCounterAddress, actualCounterAddress;
    uint8 counterPosition;
    AtOsal osal;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint8 i;
    static const uint32 cTimeoutMs = 20;
    uint32 counterOffset = ThaMpBundlePartOffset((ThaMpBundle)self);
    uint8 partId = PppBundlePartIdGet((ThaMpBundle)self);
    uint8 allCounters[] = {cThaPMCMpigBundTxPkt,
                           cThaPMCMpigBundTxByte,
                           cThaPMCMpigBundTxDisc};
    if (!ThaPmcIndirectCounterIsAccessible(self, cThaModulePmcMpig, partId))
        return cAtErrorDevBusy;

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    localCounterAddress = CounterAddress(self) & (~cBit24);
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, localCounterAddress);

    /* Mask of counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self, cThaRegThalassaPMMpxgCntsAddressRequestCtrl + counterOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpig);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpig, cTimeoutMs, partId))
        return cAtErrorDevBusy;

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpig, cTimeoutMs, partId))
            return cAtOk;

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self, cThaRegThalassaPMMpxgCntsIndrRegRptStat + counterOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpig) == 0)
            return cAtErrorDevFail;

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &actualCounterAddress);
        if (actualCounterAddress != localCounterAddress)
            return cAtErrorDevFail;

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);
        mPmcCounterGet(PMCMpigBundTxPkt , &(pCounters->rxPkt));
        mPmcCounterGet(PMCMpigBundTxByte, &(pCounters->rxByte));
        mPmcCounterGet(PMCMpigBundTxByte, &(pCounters->rxGoodByte));
        mPmcCounterGet(PMCMpigBundTxPkt , &(pCounters->rxGoodPkt));
        mPmcCounterGet(PMCMpigBundTxPkt , &(pCounters->rxQueueGoodPkt));
        mPmcCounterGet(PMCMpigBundTxDisc, &(pCounters->rxQueueDiscardPkt));
        }

    return cAtOk;
    }

static eAtRet MlpppCounterMpegGet(AtChannel self, tAtHdlcBundleCounters *pCounters, eBool r2c)
    {
    uint32 localCounterAddress, actualCounterAddress;
    uint8 counterPosition;
    AtOsal osal;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint8 i;
    static const uint32 cTimeoutMs = 20;
    uint32 counterOffset = ThaMpBundlePartOffset((ThaMpBundle)self);
    uint8 partId = PppBundlePartIdGet((ThaMpBundle)self);
    uint8 allCounters[] = {cThaPMCMpegBundRxByte,
                           cThaPMCMpegBundRxPkt,
                           cThaPMCMpegBundRxDisc};

    if (!ThaPmcIndirectCounterIsAccessible(self, cThaModulePmcMpeg, partId))
        return cAtErrorDevBusy;

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    localCounterAddress = CounterAddress(self) & (~cBit24);
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, localCounterAddress);

    /* Mask of counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self, cThaRegThalassaPMMpxgCntsAddressRequestCtrl + counterOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpeg);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpeg, cTimeoutMs, partId))
        return cAtErrorDevBusy;

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpeg, cTimeoutMs, partId))
            return cAtOk;

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self, cThaRegThalassaPMMpxgCntsIndrRegRptStat + counterOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpeg) == 0)
            return cAtErrorDevFail;

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &actualCounterAddress);
        if (actualCounterAddress != localCounterAddress)
            return cAtErrorDevFail;

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);
        mPmcCounterGet(PMCMpegBundRxPkt, &(pCounters->txPkt));
        mPmcCounterGet(PMCMpegBundRxByte, &(pCounters->txByte));
        mPmcCounterGet(PMCMpegBundRxByte, &(pCounters->txGoodByte));
        mPmcCounterGet(PMCMpegBundRxPkt, &(pCounters->txGoodPkt));
        mPmcCounterGet(PMCMpegBundRxDisc, &(pCounters->txDiscardPkt));
        }

    return cAtOk;
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;
    AtOsal osal;

    /* NULL counter structure has not been handled yet */
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    /* Init value counter */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, pAllCounters, 0, sizeof(tAtHdlcBundleCounters));

    /* MPIG */
    ret = MlpppCounterMpigGet(self, (tAtHdlcBundleCounters *)pAllCounters, clear);
    if (ret != cAtOk)
        return cAtError;

    /* MPEG */
    ret = MlpppCounterMpegGet(self, (tAtHdlcBundleCounters *)pAllCounters, clear);
    if (ret != cAtOk)
        return cAtError;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static void OverrideAtChannel(ThaStmMpBundle self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtMpBundle(ThaStmMpBundle self)
    {
    AtMpBundle bundle = (AtMpBundle)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtMpBundleMethods = mMethodsGet(bundle);
        mMethodsGet(osal)->MemCpy(osal, &m_AtMpBundleOverride, m_AtMpBundleMethods, sizeof(m_AtMpBundleOverride));
        }

    mMethodsSet(bundle, &m_AtMpBundleOverride);
    }

static void Override(ThaStmMpBundle self)
    {
    OverrideAtChannel(self);
    OverrideAtMpBundle(self);
    }

AtMpBundle ThaStmMpBundleObjectInit(AtMpBundle self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaMpBundleObjectInit((AtMpBundle)self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaStmMpBundle)self);

    m_methodsInit = 1;

    return self;
    }

AtMpBundle ThaStmMpBundleNew(uint32 channelId, AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtMpBundle newBundle = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBundle == NULL)
        return NULL;

    /* Construct it */
    return ThaStmMpBundleObjectInit(newBundle, channelId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPidTableEntry.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : PID table entry
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPIDTABLEENTRY_H_
#define _THAPIDTABLEENTRY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPidTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtPidTableEntry ThaPidTableEntryNew(AtPidTable self, uint32 entryIndex);

/* Product concretes */
AtPidTableEntry Tha60031032PidTableEntryNew(AtPidTable self, uint32 entryIndex);
AtPidTableEntry Tha60031032PidTableEntryNew(AtPidTable self, uint32 entryIndex);
AtPidTableEntry Tha60060011PidTableEntryNew(AtPidTable self, uint32 entryIndex);

#ifdef __cplusplus
}
#endif
#endif /* _THAPIDTABLEENTRY_H_ */


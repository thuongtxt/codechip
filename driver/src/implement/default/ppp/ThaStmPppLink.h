/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaStmPppLink.h
 * 
 * Created Date: Dec 3, 2012
 *
 * Description : PPP link of STM product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMPPPLINK_H_
#define _THASTMPPPLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPppLink.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmPppLink * ThaStmPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaStmPppLinkNew(AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMPPPLINK_H_ */


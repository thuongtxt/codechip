/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaStmPppLink.c
 *
 * Created Date: Dec 3, 2012
 *
 * Description : PPP link of STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPppLink.h"
#include "ThaStmPppLinkInternal.h"
#include "ThaMpigStmReg.h"
#include "ThaMpegStmReg.h"
#include "ThaStmPmcReg.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "../encap/ThaHdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Counter ID masks */
#define cThaPmcMpegLinkTxByte   1
#define cThaPmcMpegLinkTxPpp    4
#define cThaPmcMpegLinkTxMlPpp  6
#define cThaPmcMpegLinkTxLcp    3
#define cThaPmcMpegLinkTxFrame  2
#define cThaPmcMpigLinkRxPpp    15
#define cThaPmcMpigLinkRxMlPpp  16
#define cThaPmcMpigLinkRxLcp    14
#define cThaPmcMpigLinkRxMru    19
#define cThaPmcMpigLinkRxByte   12
#define cThaPmcMpigLinkRxFrame  13
#define cThaPmcMpigLinkRxErr    21

#define cThaMlpppIndrCounterCheckTime 8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmPppLinkMethods m_methods;

/* Override */
static tThaPppLinkMethods m_ThaPppLinkOverride;
static tAtChannelMethods  m_AtChannelOverride;

static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 QueueDefaultOffset(ThaPppLink self, uint16 queueId)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleEncap encapModule = (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    return (queueId * ThaModuleEncapNumEncapPerPart(encapModule)) + mMethodsGet(self)->DefaultOffset(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmPppLink);
    }

static uint32 ModulePartOffset(AtChannel self, uint8 partId)
    {
    eAtModule moduleType = AtModuleTypeGet(AtChannelModuleGet(self));
    AtDevice device = AtChannelDeviceGet(self);

    return ThaDeviceModulePartOffset((ThaDevice)device, moduleType, partId);
    }

static ThaModuleEncap EncapModule(ThaPppLink self)
    {
    AtHdlcChannel channel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    return (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

/*
 * FIXME: Counter engine may halt. Temporary reset it for further debugging.
 * This function be removed after hardware fix this issue.
 */
static eAtRet CounterReset(AtChannel self, eAtModule moduleId, uint8 partId)
    {
    uint32 address;
    uint32 offset = ModulePartOffset(self, partId);

    /* Reset */
    address = cThaRegThalassaPMCCntIndrRefresh + offset;
    mChannelHwWrite(self, address, 0x10000, moduleId);
    mChannelHwWrite(self, address, 0      , moduleId);

    /* Make sure that it works after reset */
    if (!ThaPmcIndirectCounterIsValid(self, moduleId, 0, partId))
        return cAtOk;

    return cAtErrorDevBusy;
    }

/*
 * Sometime, when checking "Counters Indirect Register Valid Status", hardware
 * reports invalid. To workaround, software need to read "Counters Indirect
 * Register Report Status" for several times. During reading the report
 * status register, the valid register needs to be check if it becomes
 * valid.
 */
static eAtRet IndirectCounterRefresh(AtChannel self, eAtModule moduleId, uint8 partId)
    {
    uint32 regVal[cThaLongRegMaxSize];
    uint32 address;
    uint8 wIndex;
    uint32 offset = ModulePartOffset(self, partId);

    /* Request is not made by the outside */
    if (!ThaPmcIndirectCounterIsValid(self, moduleId, 0, partId))
        return cAtOk;

    /* Otherwise, read for serveral times to make this counter invalid */
    address = cThaRegThalassaPMMpxgCntsIndrRegRptStat + offset;
    for (wIndex = 0; wIndex < cThaMlpppIndrCounterCheckTime; wIndex++)
        {
        /* Just read to flush counter */
        mChannelHwLongRead(self, address, regVal, cThaNumDwordsInLongReg, moduleId);

        /* Check if counter is invalid */
        if (!ThaPmcIndirectCounterIsValid(self, moduleId, 0, partId))
            return cAtOk;
        }

    return CounterReset(self, moduleId, partId);
    }

static uint8 PppLinkPartIdGet(ThaPppLink self)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    ThaModuleEncap moduleEncap = (ThaModuleEncap)AtChannelModuleGet((AtChannel)channel);

    return ThaHdlcChannelPartOfEncap(moduleEncap, channel);
    }

static eBool IndirectCounterIsReady(AtChannel self, eAtModule moduleId, uint8 partId)
    {
    uint32 address;
    AtOsal osal;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse;
    static const uint32 cTimeoutMs = 100;
    uint32 offset = ModulePartOffset(self, partId);

    /* Check if indirect counter is ready for reading */
    address = cThaRegThalassaPMMpxgCntsIndrRegReadyStat + offset;
    if ((mChannelHwRead(self, address, moduleId) & cThaPMCounterIndRdyMask) == 0)
        return cAtTrue;

    /* Timeout waiting for it */
    elapse = 0;
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapse < cTimeoutMs)
        {
        /* It is now ready */
        if ((mChannelHwRead(self, address, moduleId) & cThaPMCounterIndRdyMask) == 0)
            return cAtTrue;

        /* Continue waiting */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* It is really busy */
    return cAtFalse;
    }

static uint32 CounterAddress(AtChannel self)
    {
    return mMethodsGet((ThaPppLink)self)->DefaultOffset((ThaPppLink)self);
    }

static eBool IsSimulated(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static eAtRet CountersRxGet(AtChannel self, tAtPppLinkCounters *pCounter, eBool r2c)
    {
    AtOsal osal;
    uint8 counterPosition, i;
    uint32 localCounterAddress, actualCounterAddress;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    static const uint32 cTimeoutMs = 20;
    uint32 addrOffset = ThaPppLinkPartOffset((ThaPppLink)self);
    uint8 partId = PppLinkPartIdGet((ThaPppLink)self);
    uint8 allCounters[] = {cThaPmcMpigLinkRxPpp,
                           cThaPmcMpigLinkRxMlPpp,
                           cThaPmcMpigLinkRxLcp,
                           cThaPmcMpigLinkRxMru,
                           cThaPmcMpigLinkRxByte,
                           cThaPmcMpigLinkRxFrame,
                           cThaPmcMpigLinkRxErr};

    /* Counters are all read in indirect mode. And it is not available on
     * simulation mode */
    if (IsSimulated(self))
        return cAtOk;

    if (!ThaPmcIndirectCounterIsAccessible(self, cThaModulePmcMpig, partId))
        {
        AtPrintc(cSevCritical, "ERROR: Busy reading PPP counter at %s, line %d\r\n", AtSourceLocation);
        return cAtErrorDevBusy;
        }

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    localCounterAddress = CounterAddress(self) & (~cBit24);
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, localCounterAddress);

    /* Mask of counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self, cThaRegThalassaPMMpxgCntsAddressRequestCtrl + addrOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpig);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpig, cTimeoutMs, partId))
        {
        AtPrintc(cSevCritical, "ERROR: Busy reading PPP counter at %s, line %d\r\n", AtSourceLocation);
        return cAtErrorDevBusy;
        }

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpig, cTimeoutMs, partId))
            return cAtOk;

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self, cThaRegThalassaPMMpxgCntsIndrRegRptStat + addrOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpig) == 0)
            {
            AtPrintc(cSevCritical, "ERROR: Busy reading PPP counter at %s, line %d\r\n", AtSourceLocation);
            return cAtErrorDevFail;
            }

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &actualCounterAddress);
        if (actualCounterAddress != localCounterAddress)
            {
            AtPrintc(cSevCritical, "ERROR: Busy reading PPP counter at %s, line %d\r\n", AtSourceLocation);
            return cAtErrorDevFail;
            }

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);

        if (pCounter)
            {
            mPmcCounterGet(PMCMpigLinkRxPpp,          &(pCounter->rxPlain));
            mPmcCounterGet(PMCMpigLinkRxMlppp,        &(pCounter->rxFragment));
            mPmcCounterGetIn2Dwords(PMCMpigLinkRxLcp, &(pCounter->rxOam));
            mPmcCounterGet(PMCMpigLinkRxMRU,          &(pCounter->rxExceedMru));
            mPmcCounterGet(PMCMpigLinkRxByte,         &(pCounter->rxGoodByte));
            mPmcCounterGet(PMCMpigLinkRxFrame,        &(pCounter->rxGoodPkt));
            mPmcCounterGet(PMCMpigLinkRxErr,          &(pCounter->rxDiscardPkt));
            }
        }

    return cAtOk;
    }

static eAtRet CountersTxGet(AtChannel self, tAtPppLinkCounters *pCounter, eBool r2c)
    {
    uint8 i;
    AtOsal osal;
    uint8 counterPosition;
    uint32 localCounterAddress, counterAddress;
    uint32 longRegVal[cThaNumDwordsInLongReg];
    static const uint32 cTimeoutMs = 20;
    uint32 addrOffset = ThaPppLinkPartOffset((ThaPppLink)self);
    uint8 partId = PppLinkPartIdGet((ThaPppLink)self);
    uint8 allCounters[] = {cThaPmcMpegLinkTxByte,
                           cThaPmcMpegLinkTxPpp,
                           cThaPmcMpegLinkTxMlPpp,
                           cThaPmcMpegLinkTxLcp,
                           cThaPmcMpegLinkTxFrame};
	uint32 txOamPacket = 0;
    ThaPppLink link = (ThaPppLink)self;
    eBool canCheckOamSent = ThaPppLinkCanCheckOamsSent(link);

    if (!ThaPmcIndirectCounterIsAccessible(self, cThaModulePmcMpeg, partId))
        return cAtErrorDevBusy;

    /* Make request */
    /* The link that needs to read counters */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    localCounterAddress  = CounterAddress(self) & (~cBit24);
    mFieldIns(&(longRegVal[cThaPMCCounterCntDwIndex]), cThaPMCounterCntAddrMask, cThaPMCounterCntAddrShift, localCounterAddress);

    /* Need to handle TX OAM by special way because this counter is now controlled by
     * the SDK to determine whether all of OAMs are sent or not. If application clear
     * this counter, it will affect the algorithm */
    if (canCheckOamSent)
        {
        mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(cThaPmcMpegLinkTxLcp)]),
                  cThaPMCCounterR2CMask(cThaPmcMpegLinkTxLcp),
                  cThaPMCCounterR2CShift(cThaPmcMpegLinkTxLcp),
                  1);
        }

    /* Mask of other counters need to be clear */
    if (r2c)
        {
        for (i = 0; i < mCount(allCounters); i++)
            mFieldIns(&(longRegVal[cThaPMCCounterR2CDwIndex(allCounters[i])]),
                      cThaPMCCounterR2CMask(allCounters[i]),
                      cThaPMCCounterR2CShift(allCounters[i]),
                      1);
        }

    /* Write to make request */
    mChannelHwLongWrite(self, cThaRegThalassaPMMpxgCntsAddressRequestCtrl + addrOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpeg);

    /* Wait for hardware. Then read counters */
    if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpeg, cTimeoutMs, partId))
        return cAtErrorDevBusy;

    /* Read status until counter is invalid */
    for (i = 0; i < cThaMlpppIndrCounterCheckTime; i++)
        {
        if (!ThaPmcIndirectCounterIsValid(self, cThaModulePmcMpeg, cTimeoutMs, partId))
            {
            if (canCheckOamSent)
                {
                ThaPppLinkOamLock(link);
                pCounter->txOam = ThaPppLinkTxOamCounterLatch(link, txOamPacket, r2c);
                ThaPppLinkOamUnlock(link);
                }
            return cAtOk;
            }

        mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
        if (mChannelHwLongRead(self, cThaRegThalassaPMMpxgCntsIndrRegRptStat + addrOffset, longRegVal, cThaNumDwordsInLongReg, cThaModulePmcMpeg) == 0)
            return cAtErrorDevFail;

        /* Make sure that hardware return counter of this channel */
        mFieldGet(longRegVal[cThaPMCounterCntAddrRepDwIndex],
                  cThaPMCounterCntAddrRepMask,
                  cThaPMCounterCntAddrRepShift,
                  uint16,
                  &counterAddress);
        if (counterAddress != localCounterAddress)
            return cAtErrorDevFail;

        /* Get counter type that hardware returns */
        mFieldGet(longRegVal[cThaPMCounterCntPosDwIndex],
                  cThaPMCounterCntPosMask,
                  cThaPMCounterCntPosShift,
                  uint8,
                  &counterPosition);

        if (pCounter)
            {
            mPmcCounterGet(PMCMpegLinkTxByte          , &(pCounter->txByte));
            mPmcCounterGet(PMCMpegLinkTxPpp           , &(pCounter->txPlain));
            mPmcCounterGetIn2Dwords(PMCMpegLinkTxMlppp, &(pCounter->txFragment));
            mPmcCounterGetIn2Dwords(PMCMpegLinkTxLcp  , &(pCounter->txOam));
            mPmcCounterGet(PMCMpegLinkTxByte          , &(pCounter->txGoodByte));
            mPmcCounterGet(PMCMpegLinkTxFrame         , &(pCounter->txGoodPkt));
            }

        if (canCheckOamSent)
            mPmcCounterGetIn2Dwords(PMCMpegLinkTxLcp, &txOamPacket);
        }

    if (canCheckOamSent)
        {
        ThaPppLinkOamLock(link);
        pCounter->txOam = ThaPppLinkTxOamCounterLatch(link, txOamPacket, r2c);
        ThaPppLinkOamUnlock(link);
        }

    return cAtOk;
    }

static eAtRet LinkCountersGet(AtChannel self, void *pAllCounters, eBool clear)
    {
    eAtRet ret;

    /* Init value counter */
    if (pAllCounters)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, pAllCounters, 0, sizeof(tAtPppLinkCounters));
        }

    if (IsSimulated(self))
        return cAtOk;

    /* MPIG */
    ret = CountersRxGet(self, (tAtPppLinkCounters*)pAllCounters, clear);
    if (ret != cAtOk)
        return ret;

    /* MPEG */
    ret = CountersTxGet(self, (tAtPppLinkCounters*)pAllCounters, clear);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return LinkCountersGet(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet ret = LinkCountersGet(self, pAllCounters, cAtTrue);
    ThaPppLinkDebugCountersClear((ThaPppLink)self);
    return ret;
    }

static eBool OamAddrCtrlIsBypass(ThaStmPppLink self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OamFowardCpuAddresCtrl(ThaStmPppLink self)
    {
    uint32 regAddr, regVal;
    eBool oamAddrCtrlBypass = mMethodsGet(self)->OamAddrCtrlIsBypass(self);

    /* Default OAM forward CPU have Address-Control */
    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, cThaPassOamAdrCtlFieldMask, cThaPassOamAdrCtlFieldShift, oamAddrCtrlBypass);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);
    }

static eAtRet PppPacketModeSet(ThaPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule(self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, mLinkMask(encapModule, PKtMode), mLinkShift(encapModule, PKtMode), 3);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    regAddr = cThaRegThalassaMLPppEgrPppLinkCtrl1 + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldIns(&regVal, cThaMPEGEncTypeMask, cThaMPEGEncTypeShift, 3);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Default OAM forward CPU have Address-Control */
    OamFowardCpuAddresCtrl((ThaStmPppLink)self);

    return ret;
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaPppLink(AtPppLink self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPppLinkOverride));

        mMethodOverride(m_ThaPppLinkOverride, QueueDefaultOffset);
        mMethodOverride(m_ThaPppLinkOverride, PppPacketModeSet);
        }

    mMethodsSet(link, &m_ThaPppLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideThaPppLink(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(ThaStmPppLink self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, OamAddrCtrlIsBypass);
        }

    mMethodsSet(self, &m_methods);
    }

AtPppLink ThaStmPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppLinkObjectInit((AtPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaStmPppLink)self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink ThaStmPppLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ThaStmPppLinkObjectInit(newLink, hdlcChannel);
    }

eBool ThaPmcIndirectCounterIsValid(AtChannel self, eAtModule moduleId, uint32 timeoutMs, uint8  partId)
    {
    tAtOsalCurTime curTime, startTime;
    uint32 elapse;
    AtOsal osal;
    uint32 offset = ModulePartOffset(self, partId);

    /* Nothing to do if it is already valid */
    if ((mChannelHwRead(self, cThaRegThalassaPMMpxgCntsIndrRegValidStat + offset, moduleId) & cThaPMCounterIndVldMask))
        return cAtTrue;

    /* Timeout wait for it */
    if (timeoutMs == 0)
        return cAtFalse;

    /* Timeout waiting for it */
    elapse = 0;
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapse < timeoutMs)
        {
        /* It is now ready */
        if ((mChannelHwRead(self, cThaRegThalassaPMMpxgCntsIndrRegValidStat + offset, moduleId) & cThaPMCounterIndVldMask))
            return cAtTrue;

        /* Continue waiting */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

eBool ThaPmcIndirectCounterIsAccessible(AtChannel self, eAtModule moduleId, uint8 partId)
    {
    if (!IndirectCounterIsReady(self, moduleId, partId))
        return cAtFalse;

    if (IndirectCounterRefresh(self, moduleId, partId) != cAtOk)
        return cAtFalse;

    return cAtTrue;
    }

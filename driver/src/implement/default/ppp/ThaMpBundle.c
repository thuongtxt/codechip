/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP/MLPPP module
 *
 * File        : ThaMpBundle.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalsssa MLPPP bundle default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/ppp/AtMpBundleInternal.h"
#include "../../../generic/encap/AtHdlcLinkInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../eth/controller/ThaEthFlowControllerInternal.h"
#include "../eth/ThaEthFlowInternal.h"
#include "../eth/ThaModuleEthInternal.h"
#include "../util/ThaUtil.h"
#include "ThaPppLink.h"
#include "ThaMpBundleInternal.h"
#include "commacro.h"
#include "ThaPmcReg.h"
#include "ThaMpegReg.h"
#include "ThaMpigReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaTimeOutResequenceMax    240
#define cThaTimeOutResequenceMin    10
#define cThaMaxClassPerBundle       16
#define cThaNumberOfQueueBundle     8

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultOffset(self) mMethodsGet((ThaMpBundle)self)->DefaultOffset((ThaMpBundle)self)
#define mCounterAddress(self, address) mMethodsGet((ThaMpBundle)self)->CounterAddress((ThaMpBundle)self, address)
#define mClassDefaultOffset(self, classNumber) mMethodsGet(((ThaMpBundle)self))->ClassDefaultOffset((ThaMpBundle)self, classNumber)
#define mBundleId(mpBundle) AtChannelHwIdGet((AtChannel)mpBundle)
#define mThis(self) ((ThaMpBundle)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaMpBundleMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtMpBundleMethods m_AtMpBundleOverride;
static tAtHdlcBundleMethods m_AtHdlcBundleOverride;

/* Cache super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtHdlcBundleMethods *m_AtHdlcBundleMethods = NULL;
static const tAtMpBundleMethods *m_AtMpBundleMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePpp PppModule(ThaMpBundle self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModulePpp)AtDeviceModuleGet(device, cAtModulePpp);
    }

static uint32 DefaultOffset(ThaMpBundle self)
    {
    return mBundleId(self) + ThaMpBundlePartOffset(self);
    }

static uint32 CounterAddress(ThaMpBundle self, uint32 regAddress)
    {
    return (DefaultOffset(self) + regAddress);
    }

static uint32 QueueDefaultOffset(ThaMpBundle self, uint8 queueId)
    {
    return (uint32)(DefaultOffset(self) + (queueId * 16UL));
    }

static uint32 QueueSchedulerOffset(ThaMpBundle self, uint8 queueId)
    {
    return DefaultOffset(self) + (queueId * ThaModulePppNumBundlePerPart((AtModulePpp)PppModule(self)));
    }

static uint32 ClassDefaultOffset(ThaMpBundle self, uint8 classId)
    {
    return (mBundleId(self) * 16) + classId + ThaMpBundlePartOffset(self);
    }

static eBool IsSimulated(ThaMpBundle self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceIsSimulated(device);
    }

static eBool HwIsReadyForRemovingBundle(ThaMpBundle self)
    {
    uint32  regValue;

    if (IsSimulated(self))
        return cAtTrue;

    regValue = mChannelHwRead(self, cThaEgBundleLinkRemove + ThaMpBundlePartOffset(self), cThaModuleMpeg);
    if ((regValue & cThaEgBdlLinkRmReqMask) != 0)
        return cAtFalse;

    return cAtTrue;
    }

static eBool MaxDelayIsValid(uint8 maxDelay)
    {
    return (maxDelay >= cThaTimeOutResequenceMin) &&
           (maxDelay <= cThaTimeOutResequenceMax);
    }

static void Delete(AtObject self)
    {
    AtChannelBoundPwObjectDelete((AtChannel)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool SequenceModeIsValid(eAtMpSequenceMode sequenceMode)
    {
    eAtMpSequenceMode pSequenceMode[] = {cAtMpSequenceModeShort, cAtMpSequenceModeLong};
    uint8 wIndex;

    for (wIndex = 0; wIndex < mCount(pSequenceMode); wIndex++)
        {
        if (sequenceMode == pSequenceMode[wIndex])
           return cAtTrue;
        }
    return cAtFalse;
    }
    
static eAtModulePppRet RxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
    uint32 address, regVal;

    if (!SequenceModeIsValid(sequenceMode))
        return cAtErrorInvlParm;

    /* Configure MPIG direction */
    address = cThaRegMPIGDATLookupBundleCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regVal, cThaMLPSequenceModeMask, cThaMLPSequenceModeShift, sequenceMode);
    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    return cAtOk;
    }
    
static eAtModulePppRet TxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
    uint32 address, regVal;

    if (!SequenceModeIsValid(sequenceMode))
        return cAtErrorInvlParm;

    /* Configure MPEG direction */
    address = cThaRegEgBundleScheduleCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regVal, cThaRegEgBundleScheduleSeqModMasks, cThaRegEgBundleScheduleSeqModShift, sequenceMode);
    mChannelHwWrite(self, address, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eAtMpSequenceMode RxSequenceModeGet(AtMpBundle self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegMPIGDATLookupBundleCtrl + mDefaultOffset(self), cThaModuleMpig);
    return (regVal & cThaMLPSequenceModeMask) ? cAtMpSequenceModeLong : cAtMpSequenceModeShort;
    }

static eAtMpSequenceMode TxSequenceModeGet(AtMpBundle self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegEgBundleScheduleCtrl + mDefaultOffset(self), cThaModuleMpeg);
    return (regVal & cThaRegEgBundleScheduleSeqModMasks) ? cAtMpSequenceModeLong : cAtMpSequenceModeShort;
    }

static eBool WorkingModeIsValid(eAtMpWorkingMode sequence)
    {
    eAtMpWorkingMode pWorkingMode[] = {cAtMpWorkingModeLfi, cAtMpWorkingModeMcml};
    uint8 wIndex;

    for (wIndex = 0; wIndex < mCount(pWorkingMode); wIndex++)
        {
        if (sequence == pWorkingMode[wIndex])
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtModulePppRet WorkingModeSet(AtMpBundle self, eAtMpWorkingMode workingMode)
    {
    uint32 address, regVal;

    if (!WorkingModeIsValid(workingMode))
        return cAtErrorInvlParm;

    address = cThaRegMPIGDATLookupBundleCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleMpig);

    mFieldIns(&regVal,
              cThaMLPMCModeMask,
              cThaMLPMCModeShift,
              workingMode);

    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    return cAtOk;
    }

static eAtMpWorkingMode WorkingModeGet(AtMpBundle self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegMPIGDATLookupBundleCtrl +  mDefaultOffset(self), cThaModuleMpig);
    return (regVal & cThaMLPMCModeMask) ? cAtMpWorkingModeMcml : cAtMpWorkingModeLfi;
    }

static eAtModulePppRet MrruSet(AtMpBundle self, uint32 mrru)
    {
    ThaEthFlow flowInBundle;
    AtIterator flowIterator;
    const uint8 cMinMrru = 64;

    if (mrru < cMinMrru)
        return cAtErrorInvlParm;

    /* Apply the same MRRU to all flows */
    flowIterator = AtMpBundleFlowIteratorCreate(self);
    while ((flowInBundle = (ThaEthFlow)AtIteratorNext(flowIterator)) != NULL)
        ThaEthFlowMrruSet(flowInBundle, mrru);

    /* Update database */
    ((ThaMpBundle)(ThaMpBundle)self)->mrru = mrru;
    AtObjectDelete((AtObject)flowIterator);

    return cAtOk;
    }

static uint32 MrruGet(AtMpBundle self)
    {
    return ((ThaMpBundle)(ThaMpBundle)self)->mrru;
    }

static eBool BundleHasFlowWithClass(AtMpBundle self, uint8 classNumber)
    {
    AtEthFlow flowInBundle;
    AtIterator flowIterator = AtMpBundleFlowIteratorCreate(self);

    while ((flowInBundle = (AtEthFlow)AtIteratorNext(flowIterator)) != NULL)
        {
        if (AtMpBundleFlowClassNumberGet(self, flowInBundle) == classNumber)
            {
            AtObjectDelete((AtObject)flowIterator);
            return cAtTrue;
            }
        }

    AtObjectDelete((AtObject)flowIterator);
    return cAtFalse;
    }

static eAtRet FlowActivate(ThaMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    /* Get MP flow controller managed by Module Ethernet and let's it do the job */
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)flow);
    ThaEthFlowController flowController = ThaModuleEthMpFlowControllerGet(ethModule);

    return ThaEthFlowControllerFlowActivate(flowController, flow, (AtChannel)self, classNumber);
    }

static eBool FlowAdded(ThaMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    if (mMethodsGet(flow)->IsBusy(flow) &&
       (self        == (ThaMpBundle)AtEthFlowMpBundleGet(flow)) &&
       (classNumber == ThaEthFlowClassInMpBundleGet((ThaEthFlow)flow)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool FlowCanJoin(ThaMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    /* This flow is currently in this bundle with different class number,
     * or it is busy with other purpose (link or another bundle) */
    if (mMethodsGet(flow)->IsBusy(flow))
        return cAtFalse;

    /* Flow is free but need to check if this bundle has another flow with this class number */
    if (BundleHasFlowWithClass((AtMpBundle)self, classNumber))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet FlowDeActivate(ThaMpBundle self, AtEthFlow flow)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)flow);
    ThaEthFlowController flowController = ThaModuleEthMpFlowControllerGet(ethModule);
	AtUnused(self);

    return ThaEthFlowControllerFlowDeActivate(flowController, flow);
    }

static eAtModulePppRet FlowClassAssign(ThaMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    uint32 hwFlowId, currentHwFlowId;
    uint32 address, regVal;
    uint32 mask, shift;
    ThaModulePpp pppModule = PppModule(self);
    eBool flowEnabled;

    hwFlowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    address  = cThaRegMPIGDATLookupFlowIdLookupCtrl + mClassDefaultOffset(self, classNumber);
    regVal   = mChannelHwRead(self, address, cThaModuleMpig);

    mask  = mPppMask(pppModule, MPIGDATLookupFlwID);
    shift = mPppShift(pppModule, MPIGDATLookupFlwID);
    mFieldIns(&regVal, mask, shift, hwFlowId);

    mask  = mPppMask(pppModule, FlwEnb);
    shift = mPppShift(pppModule, FlwEnb);
    mFieldIns(&regVal, mask, shift, 1);
    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    regVal = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldGet(regVal, mPppMask(pppModule, MPIGDATLookupFlwID), mPppShift(pppModule, MPIGDATLookupFlwID), uint32, &currentHwFlowId);
    mFieldGet(regVal, mPppMask(pppModule, FlwEnb), mPppShift(pppModule, FlwEnb), eBool, &flowEnabled);
    if ((flowEnabled != 1) || (currentHwFlowId != hwFlowId))
        return cAtErrorDevFail;

    return cAtOk;
    }

static eAtModulePppRet FlowClassDeassign(ThaMpBundle self, AtEthFlow flow)
    {
    uint32 address, regVal;
    uint32 mask, shift;
    uint8 classNumber;
    ThaModulePpp pppModule = PppModule(self);

    classNumber = ThaEthFlowClassInMpBundleGet((ThaEthFlow)flow);
    address = cThaRegMPIGDATLookupFlowIdLookupCtrl + mClassDefaultOffset(self, classNumber);
    regVal = mChannelHwRead(self, address, cThaModuleMpig);

    mask  = mPppMask(pppModule, FlwEnb);
    shift = mPppShift(pppModule, FlwEnb);
    mFieldIns(&regVal, mask, shift, 0);
    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    return cAtOk;
    }

static eAtModulePppRet FlowAdd(AtMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    eAtRet ret;
    ThaMpBundle bundle = (ThaMpBundle)self;

    /* Check if class is valid */
    if (classNumber >= cThaMaxClassPerBundle)
        return cAtErrorOutOfRangParm;

    if (FlowAdded(bundle, flow, classNumber))
        return cAtOk;

    if (!mMethodsGet(mThis(self))->FlowCanJoin(mThis(self), flow, classNumber))
        return cAtErrorChannelBusy;

    /* Check if there is enough space for new flow */
    if (AtListLengthGet(self->flows) >= cThaMaxClassPerBundle)
        return cAtErrorRsrcNoAvail;

    /* Activate this flow in this bundle */
    ret = FlowActivate(bundle, flow, classNumber);
    if (ret != cAtOk)
        {
        ret |= FlowDeActivate(bundle, flow);
        return ret;
        }

    ret = mMethodsGet(bundle)->FlowClassAssign(bundle, flow, classNumber);
    if (ret != cAtOk)
        return ret;

    /* Enable this bundle */
    AtChannelTxEnable((AtChannel)self, cAtTrue);

    /* Update database */
    ret |= AtListObjectAdd(self->flows, (AtObject)flow);

    return ret;
    }

static uint8 FlowClassNumberGet(AtMpBundle self, AtEthFlow flow)
    {
	AtUnused(self);
    return ThaEthFlowClassInMpBundleGet((ThaEthFlow)flow);
    }

static eAtModulePppRet FlowRemove(AtMpBundle self, AtEthFlow flow)
    {
    eAtRet ret;

    if (flow == NULL)
        return cAtErrorNullPointer;

    if (AtEthFlowMpBundleGet(flow) != self)
        return cAtErrorRsrcNoAvail;

    /* Disable this channel before removing flow */
    ret = AtChannelEnable((AtChannel)flow, cAtFalse);
    if (ret != cAtOk)
        return ret;

    /* Disable a flow IG */
    ret = ThaEthFlowTxTrafficEnable((ThaEthFlow)flow, cAtFalse);
    if (ret != cAtOk)
        return ret;

    mMethodsGet(mThis(self))->FlowClassDeassign(mThis(self), flow);
    ret |= FlowDeActivate((ThaMpBundle)self, flow);

    /* Update database */
    ret |= AtListObjectRemove(self->flows, (AtObject)flow);

    return ret;
    }

static eAtRet MaxDelaySet(AtHdlcBundle self, uint8 maxDelay)
    {
    ThaEthFlow flowInBundle;
    AtIterator flowIterator;

    if (!MaxDelayIsValid(maxDelay))
        return cAtErrorOutOfRangParm;

    flowIterator = AtMpBundleFlowIteratorCreate((AtMpBundle)self);

    /* Apply the same to all flows */
    while ((flowInBundle = (ThaEthFlow)AtIteratorNext(flowIterator)) != NULL)
        ThaEthFlowMaxDelaySet(flowInBundle, maxDelay);

    AtObjectDelete((AtObject)flowIterator);

    /* Update database */
    ((ThaMpBundle)self)->maxDelay = maxDelay;

    return cAtOk;
    }

static uint8 MaxDelayGet(AtHdlcBundle self)
    {
    return ((ThaMpBundle)self)->maxDelay;
    }

static eBool FragmentSizeIsValid(eAtFragmentSize fragmentSize)
    {
    eAtFragmentSize validFragmentSizes[] = {
                                           cAtNoFragment,
                                           cAtFragmentSize64,
                                           cAtFragmentSize128,
                                           cAtFragmentSize256,
                                           cAtFragmentSize512
                                           };
    uint8 i;

    for (i = 0; i < mCount(validFragmentSizes); i++)
        {
        if (fragmentSize == validFragmentSizes[i])
            return cAtTrue;
        }

    return cAtFalse;
    }

static uint8 FragmentSizeSw2Hw(eAtFragmentSize fragmentSize)
    {
    if (fragmentSize == cAtFragmentSize64)  return 5;
    if (fragmentSize == cAtFragmentSize128) return 1;
    if (fragmentSize == cAtFragmentSize256) return 2;
    if (fragmentSize == cAtFragmentSize512) return 3;

    return 0;
    }

static eAtRet FragmentSizeSet(AtHdlcBundle self, eAtFragmentSize fragmentSize)
    {
    uint32 address, regVal;
    uint8  fragmentValue;

    if (!FragmentSizeIsValid(fragmentSize))
    	return cAtErrorOutOfRangParm;

    /* Convert fragmentsize to hw value */
    fragmentValue = FragmentSizeSw2Hw(fragmentSize);

    address = cThaRegEgBundleScheduleCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regVal,
              cThaRegMLPppEgrBundleFragSizeMsk,
              cThaRegMLPppEgrBundleFragSizeShift,
              fragmentValue);

    mChannelHwWrite(self, address, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eAtFragmentSize FragmentSizeHw2Sw(uint8 hwFragmentSize)
    {
    if (hwFragmentSize == 0) return cAtNoFragment;
    if (hwFragmentSize == 1) return cAtFragmentSize128;
    if (hwFragmentSize == 2) return cAtFragmentSize256;
    if (hwFragmentSize == 3) return cAtFragmentSize512;
    if (hwFragmentSize == 5) return cAtFragmentSize64;

    return cAtNoFragment;
    }

static eAtFragmentSize FragmentSizeGet(AtHdlcBundle self)
    {
    uint32 regVal;
    uint8  hwFragmentSize;

    regVal = mChannelHwRead(self, cThaRegEgBundleScheduleCtrl + mDefaultOffset(self), cThaModuleMpeg);
    mFieldGet(regVal,
              cThaRegMLPppEgrBundleFragSizeMsk,
              cThaRegMLPppEgrBundleFragSizeShift,
              uint8,
              &hwFragmentSize);

    return FragmentSizeHw2Sw(hwFragmentSize);
    }

static eBool BundleRemoveFinished(ThaMpBundle self)
    {
    uint32 regValue;
    uint32 elapseTime;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 partAddress = cThaEgBundleLinkRemove + ThaMpBundlePartOffset(self);

    if (IsSimulated(self))
        return cAtTrue;

    /* Hardware finish its job */
    regValue = mChannelHwRead(self, partAddress, cThaModuleMpeg);
    if ((regValue & cThaEgBdlLinkRmReqMask) == 0)
        return cAtTrue;

    /* Timeout waiting for it */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cHardwareBundleRemoveTimeOut)
        {
        regValue = mChannelHwRead(self, partAddress, cThaModuleMpeg);
        if ((regValue & cThaEgBdlLinkRmReqMask) == 0)
        	{
        	if (elapseTime > self->maxMsWaitHw)
		        self->maxMsWaitHw = elapseTime;
        	self->curMsWaitHw = elapseTime;
            return cAtTrue;
			}
			
        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static eAtRet HardwareBundleRemove(ThaMpBundle self)
    {
    uint32 regValue;

    /* Remove bundle HW sequence */
    if (!HwIsReadyForRemovingBundle(self))
        return cAtErrorDevBusy;

    /* Request HW to remove bundle */
    regValue = 0;
    mFieldIns(&regValue,
              cThaEgRemoveIdMask,
              cThaEgRemoveIdShift,
              AtChannelHwIdGet((AtChannel)self));
    mFieldIns(&regValue,
              cThaEgLinkIndicMask,
              cThaEgLinkIndicShift,
              0);
    mFieldIns(&regValue,
              cThaEgBdlLinkRmReqMask,
              cThaEgBdlLinkRmReqShift,
              1);
    mChannelHwWrite(self, cThaEgBundleLinkRemove + ThaMpBundlePartOffset(self), regValue, cThaModuleMpeg);

    /* wait for hardware finish */
    if (!BundleRemoveFinished(self))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

static eAtRet QueueEnable(ThaMpBundle self, uint8 queueId, eBool enable)
    {
    uint32 address, regValue;

    address = cThaRegEgBundleScheduleCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaRegEgBundleScheduleMPEGBunQueEnMask(queueId),
              cThaRegEgBundleScheduleMPEGBunQueEnShift(queueId),
              mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static eAtRet BundleTrafficActivate(AtMpBundle self, eBool activate)
    {
    AtIterator flowIterator;
    ThaEthFlow flow;
    eAtRet ret;

    /* Iterate all flows */
    ret = cAtOk;
    flowIterator = AtMpBundleFlowIteratorCreate(self);
    while ((flow = (ThaEthFlow)AtIteratorNext(flowIterator)) != NULL)
        ret |= ThaEthFlowRxTrafficEnable(flow, activate);

    AtObjectDelete((AtObject)flowIterator);

    return ret;
    }

static eAtRet NumberLinksSet(ThaMpBundle self, uint16 numLinkInBundle)
    {
    uint32 address, regValue;

    address = cThaRegEgBundleScheduleCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaRegMLPppEgrMPEGMaxLinkMsk,
              cThaRegMLPppEgrMPEGMaxLinkShift,
              numLinkInBundle);
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static eAtRet RearrangeMemberIds(AtHdlcBundle self, AtHdlcLink removedLink)
    {
    eAtRet ret = cAtOk;
    AtIterator linkIterator;
    AtHdlcLink currentLink;
    static const uint8 cMemberIdWhenRemoved = 0x1f;
    uint16 currentLinkIdInBundle = 0;

    /* Search link need to remove and set it's member id to 0x1F */
    linkIterator = AtHdlcBundleLinkIteratorCreate(self);
    while ((currentLink = (AtHdlcLink)AtIteratorNext(linkIterator)) != NULL)
        {
        if (currentLink == removedLink)
            {
            ret |= ThaPppLinkMemberLocalIdInBundleSet((ThaPppLink)removedLink, cMemberIdWhenRemoved);
            break;
            }

        currentLinkIdInBundle = (uint16)(currentLinkIdInBundle + 1);
        }

    /* Update member id for the links after removed link */
    while ((currentLink = (AtHdlcLink)AtIteratorNext(linkIterator)) != NULL)
        {
        ret |= ThaPppLinkMemberLocalIdInBundleSet((ThaPppLink)currentLink, (uint8)currentLinkIdInBundle);
        currentLinkIdInBundle = (uint16)(currentLinkIdInBundle + 1);
        }

    AtObjectDelete((AtObject)linkIterator);

    return ret;
    }

static eAtRet LinkHwRemove(ThaMpBundle self, AtHdlcLink link)
    {
    uint32 curNumLink;
    uint8  queueId;
    eAtRet ret = cAtOk;
    AtOsal osal;
    AtHdlcBundle hdlcBundle = (AtHdlcBundle)self;

    curNumLink = AtListLengthGet(hdlcBundle->links);
    if (curNumLink == 1)
        {
        /* Disable all flows to make traffic not come to hardware buffer */
        ret = BundleTrafficActivate((AtMpBundle)self, cAtFalse);
        if (ret != cAtOk)
            return ret;

        /* Disable all queue */
        for (queueId = 0; queueId < mMethodsGet(self)->MaxNumQueues(self); queueId++)
            ret |= ThaMpBundleQueueEnable(self, queueId, cAtFalse);

        /* The application may disable TX traffic before this function
         * runs and this cause hardware cannot release internal buffer and
         * the link removing sequence will fail. The trick is, just enable TX
         * data to give hardware a chance to release buffer */
        ret = ThaPppLinkIgMlpppEnable((ThaPppLink)link, cAtTrue);
        if (ret != cAtOk)
            return ret;

        /* Remove bundle in HW */
        ret = ThaMpBundleHardwareBundleRemove(self);
        if (ret != cAtOk)
            return ret;
        }

    /* Disable MLPPP on link at IG direction */
    ret = ThaPppLinkIgMlpppEnable((ThaPppLink)link, cAtFalse);
    if (ret != cAtOk)
        return ret;

    /* Sleep */
    if (!IsSimulated(self))
        {
        osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->USleep(osal, (uint32)(MaxDelayGet(hdlcBundle) * 1000));
        }

    /* Disable MLPPP on link at EG direction */
    ret = ThaPppLinkEgMlpppEnable((ThaPppLink)link, cAtFalse);
    if (ret != cAtOk)
        return ret;

    ret = RearrangeMemberIds(hdlcBundle, link);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static eAtRet LinkRemove(AtHdlcBundle self, AtHdlcLink link)
    {
    ThaMpBundle bundle = (ThaMpBundle)self;
    eAtRet ret;

    /* check if link not existing */
    if (link == NULL)
        return cAtErrorInvlParm;

    /* Check if link belong to bundle */
    if (link->hdlcBundle != self)
        return cAtErrorInvlParm;

    ret = mMethodsGet(bundle)->LinkHwRemove(bundle, link);
    if (ret != cAtOk)
        return ret;

    /* Call AtHdlcBundle method to manage DB */
    m_AtHdlcBundleMethods->LinkRemove(self, link);

    /* Update again number link in bundle */
    if (AtHdlcBundleNumberOfLinksGet(self) > 0)
        ThaMpBundleNumberLinksSet(bundle, (uint16)(AtHdlcBundleNumberOfLinksGet(self) - 1));

    return cAtOk;
    }

static void DefaultBlockRangeSet(AtMpBundle self, AtHdlcLink link)
    {
    uint8 queueId;
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)AtHdlcLinkHdlcChannelGet(link));
    uint8 rangeId = AtChannelMlpppBlockRange(phyChannel);

    for (queueId = 0; queueId < mMethodsGet((ThaMpBundle)self)->MaxNumQueues((ThaMpBundle)self); queueId++)
        ThaMlpppBundleQueueBlockSizeSet((ThaMpBundle)self, queueId, rangeId);
    }

static eAtRet LinkAdd(AtHdlcBundle self, AtHdlcLink link)
    {
    eAtRet ret = cAtOk;
    uint8  queueId;
    ThaMpBundle bundle = (ThaMpBundle)self;

    /* Let the super deal with database first */
    ret = m_AtHdlcBundleMethods->LinkAdd(self, link);
    if (ret != cAtOk)
        return ret;

    /* Additional actions to configure hardware */
    /* Join bundle */
    ret = ThaPppLinkJoinBundle((ThaPppLink)link, (ThaMpBundle)self);

    /* Enable this link on both directions */
    ret |= ThaPppLinkEgMlpppEnable((ThaPppLink)link, cAtTrue);
    ret |= ThaPppLinkIgMlpppEnable((ThaPppLink)link, cAtTrue);

    for (queueId = 0; queueId < mMethodsGet(bundle)->MaxNumQueues(bundle); queueId++)
        ret |= ThaMpBundleQueueEnable((ThaMpBundle)self, queueId, cAtTrue);

    /* If this is first link, enable flows which disabled before */
    if (AtHdlcBundleNumberOfLinksGet(self) == 1)
        {
        DefaultBlockRangeSet((AtMpBundle)self, link);
        ret |= BundleTrafficActivate((AtMpBundle)self, cAtTrue);

        if (mMethodsGet((AtMpBundle)self)->CanConfigureSequenceNumber((AtMpBundle)self))
            ret |= AtMpBundleSequenceNumberSet((AtMpBundle)self, 0, 0);
        }

    if (ret != cAtOk)
        return ret;

    /* Update number of links in bundle and link's member Id */
    ThaMpBundleNumberLinksSet((ThaMpBundle)self, (uint16)(AtHdlcBundleNumberOfLinksGet((AtHdlcBundle)self) - 1));

    /* Set Local ID for link in bundle */
    ret = ThaPppLinkMemberLocalIdInBundleSet((ThaPppLink)link, (uint16)(AtHdlcBundleNumberOfLinksGet(self) - 1));
    if (ret != cAtOk)
        return ret;

    /* Note, it is tempted to add code to enable bundle  when first link is
     * added to bundle. Doing this may affect the application from controlling
     * traffic on it. One bundle may have a minimum links to activate
     * traffic and it is only determined by application. So, do not try to
     * enable bundle traffic (by calling IgEnable) here, let it be done by
     * application */

    return cAtOk;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtChannelTxEnable(self, enable);
    ret |= AtChannelRxEnable(self, enable);

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return (AtChannelTxIsEnabled(self) && AtChannelRxIsEnabled(self)) ? cAtTrue : cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
	AtUnused(enableMask);
	AtUnused(defectMask);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet MlpppCounterMpigGet(AtChannel self, tAtHdlcBundleCounters *pCounters, eBool r2c)
    {
    uint32  address;
    uint32  offsetCount;

    /* Get Offset counter */
    offsetCount = (r2c == cAtTrue) ? 0 : cThaAddrOffsetCounterReadOnly;

    /* Get good byte count */
    address = mCounterAddress(self, cThaRegPmcMpigBundleGoodByte(offsetCount));
    pCounters->rxGoodByte = mChannelHwRead(self, address, cThaModuleMpig);

    /* For this direction, all of errors are checked and counted by HDLC and PPP
     * layers. So, when these packets come to bundle, they are always processed
     * with no trouble. So, total bytes is equal to total good bytes */
    pCounters->rxByte = pCounters->rxGoodByte;

    /* Get good packet count */
    address = mCounterAddress(self, cThaRegPmcMpigBundleGoodPkt(offsetCount));
    pCounters->rxGoodPkt = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get good packet count */
    address = mCounterAddress(self, cThaRegPmcMpigBundleFragment(offsetCount));
    pCounters->rxFragmentPkt = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get discard packet count */
    address = mCounterAddress(self, cThaRegPmcMpigBundleDiscardPkt(offsetCount));
    pCounters->rxDiscardPkt = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get all packet count */
    pCounters->rxPkt = pCounters->rxDiscardPkt + pCounters->rxGoodPkt;

    return cAtOk;
    }

static eAtRet MlpppCounterMpegGet(AtChannel self, tAtHdlcBundleCounters *pCounters, eBool r2c)
    {
    uint32  address;
    uint32  offsetCount;
    uint8   queueId;

    /* Get Offset counter */
    offsetCount = (r2c == cAtTrue) ? 0 : cThaAddrOffsetCounterReadOnly;

    /* Get good byte count */
    address = mCounterAddress(self, cThaRegPmcMpegBundleGoodByte(offsetCount));
    pCounters->txGoodByte = mChannelHwRead(self, address, cThaModuleMpeg);

    /* For this direction, all of errors are checked and counted by Ethernet flow.
     * When these packets come to bundle, they are always processed with no
     * trouble. So, total bytes is equal to total good bytes */
    pCounters->txByte = pCounters->txGoodByte;

    /* Get good packet count */
    address = mCounterAddress(self, cThaRegPmcMpegBundleGoodPkt(offsetCount));
    pCounters->txGoodPkt = mChannelHwRead(self, address, cThaModuleMpeg);

    /* Get fragment packet count */
    address = mCounterAddress(self, cThaRegPmcMpegBundleFragment(offsetCount));
    pCounters->txFragmentPkt = mChannelHwRead(self, address, cThaModuleMpeg);

    /* Get discard packet count */
    address = mCounterAddress(self, cThaRegPmcMpegBundleDiscardPkt(offsetCount));
    pCounters->txDiscardPkt = mChannelHwRead(self, address, cThaModuleMpeg);

    /* TODO: These counter should be renamed */
    for (queueId = 0; queueId < cThaNumberOfQueueBundle; queueId++)
        {
        /* Get queue good packet count */
        address = mCounterAddress(self, cThaRegPmcMpegBundleQueGoodPkt(offsetCount));
        pCounters->rxQueueGoodPkt += mChannelHwRead(self, address, cThaModuleMpeg);

        /* Get discard queue packet count */
        address = mCounterAddress(self, cThaRegPmcMpegBundleQueDisPkt(offsetCount));
        pCounters->rxQueueDiscardPkt += mChannelHwRead(self, address, cThaModuleMpeg);
        }

    /* Get all packet count */
    address = mCounterAddress(self, cThaRegPmcMpegBundleDiscardPkt(offsetCount));
    pCounters->txPkt = pCounters->txGoodPkt + mChannelHwRead(self, address, cThaModuleMpeg);

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    eAtRet     ret;

    /* NULL counter structure has not been handled yet */
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    /* mpig */
    ret = MlpppCounterMpigGet(self, (tAtHdlcBundleCounters *)pAllCounters, cAtFalse);
    if (ret != cAtOk)
        return cAtError;

    /* mpeg */
    ret = MlpppCounterMpegGet(self, (tAtHdlcBundleCounters *)pAllCounters, cAtFalse);
    if (ret != cAtOk)
        return cAtError;

    return cAtOk;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet  ret;

    /* NULL counter structure has not been handled yet */
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    /* mpig */
    ret = MlpppCounterMpigGet(self, (tAtHdlcBundleCounters *)pAllCounters, cAtTrue);
    if (ret != cAtOk)
        return cAtError;

    /* mpeg */
    ret = MlpppCounterMpegGet(self, (tAtHdlcBundleCounters *)pAllCounters, cAtTrue);
    if (ret != cAtOk)
        return cAtError;

    return cAtOk;
    }

static eAtRet AllConfigSet(AtChannel self, void *pAllConfig)
    {
    eAtRet ret;
    AtMpBundle bundle = (AtMpBundle)self;
    tAtMpBundleAllConfig *pBundleConfig =(tAtMpBundleAllConfig*)pAllConfig;

    ret  = cAtOk;
    ret |= AtMpBundleWorkingModeSet(bundle, pBundleConfig->workingMd);
    ret |= AtMpBundleSequenceModeSet(bundle, pBundleConfig->seqMd);
    ret |= AtHdlcBundleFragmentSizeSet((AtHdlcBundle)self, pBundleConfig->fragmentSize);
    ret |= AtMpBundleMrruSet(bundle, pBundleConfig->mrru);
    ret |= AtHdlcBundleMaxDelaySet((AtHdlcBundle)self, pBundleConfig->maxLinkDelay);

    /* Default disable bundle after create, it will be enabled when there is
     * a link inside it is enable EG side */
    ret |= AtChannelEnable(self, cAtFalse);

    return ret;
    }

static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
    tAtMpBundleAllConfig *pBundleConfig =(tAtMpBundleAllConfig*)pAllConfig;

    if (pBundleConfig == NULL)
        return cAtErrorNullPointer;

    /* Get configure bundle */
    pBundleConfig->workingMd    = AtMpBundleWorkingModeGet((AtMpBundle)self);
    pBundleConfig->seqMd        = AtMpBundleSequenceModeGet((AtMpBundle)self);
    pBundleConfig->fragmentSize = AtHdlcBundleFragmentSizeGet((AtHdlcBundle)self);
    pBundleConfig->maxLinkDelay = AtHdlcBundleMaxDelayGet((AtHdlcBundle)self);
    pBundleConfig->mrru         = AtMpBundleMrruGet((AtMpBundle)self);
    pBundleConfig->enable       = AtChannelIsEnabled(self);

    return cAtOk;
    }

static eAtRet MpegFlowSequenceNumberSet(AtEthFlow flow, uint32 sequenceNumber)
    {
    uint32 hwFlowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    uint32 address, regValue;

    address = cThaMPEGFlowSequenceStatus + hwFlowId;
    regValue = mChannelHwRead(flow, address, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaMPEGMpFlowCurrSequenceMask,
              cThaMPEGMpFlowCurrSequenceShift,
              sequenceNumber);
    mChannelHwWrite(flow, address, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static eAtRet MpigFlowSequenceNumberSet(AtEthFlow flow, uint32 sequenceNumber)
    {
    uint32 hwFlowId = ThaEthFlowHwFlowIdGet((ThaEthFlow)flow);
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 address, regValues[cThaLongRegMaxSize];
    eAtRet ret;

    ret = ThaEthFlowResequenceBufferRelease(flow);
    if (ret != cAtOk)
        return ret;

    address = cThaRegMPIGRSQResequenceStat + hwFlowId;
    mMethodsGet(osal)->MemInit(osal, regValues, 0, cThaLongRegMaxSize * sizeof(uint32));

    /* Set current sequence, head, max number to sequence number */
    mRegFieldSet(regValues[cThaMPIGRsqCurrRsExptDwIndex], cThaMPIGRsqCurrRsExpt, sequenceNumber);
    mRegFieldSet(regValues[cThaMPIGRsqCurrPaHeadDwIndex], cThaMPIGRsqCurrPaHead, sequenceNumber);
    mRegFieldSet(regValues[cThaMPIGRsqCurrRsMaxStaDwIndex], cThaMPIGRsqCurrRsMaxSta, sequenceNumber);

    /* Set FSM to state RE-SYNC */
    mRegFieldSet(regValues[cThaMPIGRsqCurrRsFsmDwIndex], cThaMPIGRsqCurrRsFsm, 0x4);
    mChannelHwLongWrite(flow, address, regValues, 4, cThaModuleMpig);

    return cAtOk;
    }

static eAtModulePppRet SequenceNumberSet(AtMpBundle self, uint8 classNumber, uint32 sequenceNumber)
    {
    AtIterator flowIterator;
    AtEthFlow flow;
    uint8 flowClass;
    eAtRet ret = cAtOk;

    /* check if bundle not existing */
    if (self == NULL)
        return cAtErrorInvlParm;

    flowIterator = AtMpBundleFlowIteratorCreate(self);
    while ((flow = (AtEthFlow)AtIteratorNext(flowIterator)) != NULL)
        {
        flowClass = AtMpBundleFlowClassNumberGet(self, flow);
        if (flowClass != classNumber)
            continue;

        ret |= MpegFlowSequenceNumberSet(flow, sequenceNumber);
        ret |= MpigFlowSequenceNumberSet(flow, sequenceNumber);
        break;
        }

    AtObjectDelete((AtObject)flowIterator);

    return ret;
    }

static uint8 MaxNumLinkInBundleGet(AtHdlcBundle self)
    {
	AtUnused(self);
    return 32;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    ThaMpBundle mpBundle = (ThaMpBundle)self;
    uint8 queue_i;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Additional initialization */
    for (queue_i = 0; queue_i < mMethodsGet(mpBundle)->MaxNumQueues(mpBundle); queue_i++)
        ThaMpBundleQueueEnable(mpBundle, queue_i, cAtTrue);

    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    ThaMpBundle bundle = (ThaMpBundle)self;

    /* Print PPP OAM counters  */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Debug information:\r\n");
    AtPrintc(cSevNormal, "  Maximum time to wait Hw     : %u(ms)\r\n", bundle->maxMsWaitHw);
    AtPrintc(cSevNormal, "  Current time to wait Hw     : %u(ms)\r\n", bundle->curMsWaitHw);
    return cAtOk;
    }

static eAtHdlcBundleSchedulingMode SchedulingModeGet(AtHdlcBundle self)
    {
    uint32 regAddr, regValue;
    uint8 hwFragVal;

    regAddr = cThaRegEgBundleScheduleCtrl +  mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldGet(regValue,
              cThaRegMLPppEgrMPEGFragModeMask,
              cThaRegMLPppEgrMPEGFragModeShift,
              uint8,
              &hwFragVal);

    if (hwFragVal == 0) return cAtMpSchedulingModeRandom;
    if (hwFragVal == 1) return cAtMpSchedulingModeFairRR;
    if (hwFragVal == 2) return cAtMpSchedulingModeStrictRR;
    if (hwFragVal == 3) return cAtMpSchedulingModeSmartRR;

    return cAtMpSchedulingModeRandom;
    }

static eAtRet SchedulingModeSet(AtHdlcBundle self, eAtHdlcBundleSchedulingMode schedulingMode)
    {
    uint32 regAddr, regValue;
    regAddr = cThaRegEgBundleScheduleCtrl +  mDefaultOffset(self);
    regValue = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaRegMLPppEgrMPEGFragModeMask,
              cThaRegMLPppEgrMPEGFragModeShift,
              schedulingMode);
    mChannelHwWrite(self, regAddr, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return ThaModulePppLocalBundleId((ThaMpBundle)self);
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtChannelHwIdGet(self);
    }

static uint8 MaxNumQueues(ThaMpBundle self)
    {
	AtUnused(self);
    return 2;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    uint32 address = cThaRegEgBundleScheduleCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, address, cThaModuleMpeg);

    mRegFieldSet(regValue, cThaRegMLPppEgrBundleEn, mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    uint32 address = cThaRegEgBundleScheduleCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    return mRegField(regValue, cThaRegMLPppEgrBundleEn) ? cAtTrue : cAtFalse;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 address = cThaRegMPIGDATLookupBundleCtrl +  mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, address, cThaModuleMpig);

    mRegFieldSet(regValue, cThaBndEnb, mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue,cThaModuleMpig);

    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 address = cThaRegMPIGDATLookupBundleCtrl +  mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, address, cThaModuleMpig);
    return mRegField(regValue, cThaBndEnb) ? cAtTrue : cAtFalse;
    }

static void QueueBlockSizeSet(ThaMpBundle self, uint8 queueId, uint8 rangeId)
    {
    uint32 regAddr = cThaRegEgBundlePerQueueCtrl + mMethodsGet(self)->QueueSchedulerOffset(self, queueId);
    uint32 regValue = mChannelHwRead(self, regAddr, cThaModuleMpeg);

    mFieldIns(&regValue, cThaMpegBundleMaxBlkTypeMask, cThaMpegBundleMaxBlkTypeShift, rangeId);
    mChannelHwWrite(self, regAddr, regValue, cThaModuleMpeg);
    }

static AtModuleEth ModuleEth(AtHdlcBundle self)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    }

static AtEthFlow OamFlowCreate(AtHdlcBundle self)
    {
    return AtHdlcBundleOamFlowCreate(self);
    }

static void OamFlowDelete(AtHdlcBundle self)
    {
    AtHdlcBundleOamFlowDelete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaMpBundle object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(maxDelay);
    mEncodeUInt(mrru);
    mEncodeNone(maxMsWaitHw);
    mEncodeNone(curMsWaitHw);
    }

static eBool CanConfigureSequenceNumber(AtMpBundle self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtChannel(ThaMpBundle self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllConfigSet);
        mMethodOverride(m_AtChannelOverride, AllConfigGet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtMpBundle(ThaMpBundle self)
    {
	AtMpBundle bundle = (AtMpBundle)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtMpBundleMethods = mMethodsGet(bundle);
        mMethodsGet(osal)->MemCpy(osal, &m_AtMpBundleOverride, m_AtMpBundleMethods, sizeof(m_AtMpBundleOverride));
        mMethodOverride(m_AtMpBundleOverride, TxSequenceModeSet);
        mMethodOverride(m_AtMpBundleOverride, RxSequenceModeSet);
        mMethodOverride(m_AtMpBundleOverride, TxSequenceModeGet);
        mMethodOverride(m_AtMpBundleOverride, RxSequenceModeGet);
        mMethodOverride(m_AtMpBundleOverride, WorkingModeSet);
        mMethodOverride(m_AtMpBundleOverride, WorkingModeGet);
        mMethodOverride(m_AtMpBundleOverride, MrruSet);
        mMethodOverride(m_AtMpBundleOverride, MrruGet);
        mMethodOverride(m_AtMpBundleOverride, FlowAdd);
        mMethodOverride(m_AtMpBundleOverride, FlowClassNumberGet);
        mMethodOverride(m_AtMpBundleOverride, FlowRemove);
        mMethodOverride(m_AtMpBundleOverride, SequenceNumberSet);
        mMethodOverride(m_AtMpBundleOverride, CanConfigureSequenceNumber);
        }

    mMethodsSet(bundle, &m_AtMpBundleOverride);
    }

static void OverrideAtHdlcBundle(ThaMpBundle self)
    {
	AtHdlcBundle bundle = (AtHdlcBundle)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_AtHdlcBundleMethods = mMethodsGet(bundle);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcBundleOverride, m_AtHdlcBundleMethods, sizeof(m_AtHdlcBundleOverride));

        mMethodOverride(m_AtHdlcBundleOverride, MaxDelaySet);
        mMethodOverride(m_AtHdlcBundleOverride, MaxDelayGet);
        mMethodOverride(m_AtHdlcBundleOverride, FragmentSizeSet);
        mMethodOverride(m_AtHdlcBundleOverride, FragmentSizeGet);
        mMethodOverride(m_AtHdlcBundleOverride, LinkRemove);
        mMethodOverride(m_AtHdlcBundleOverride, LinkAdd);
        mMethodOverride(m_AtHdlcBundleOverride, MaxNumLinkInBundleGet);
        mMethodOverride(m_AtHdlcBundleOverride, OamFlowCreate);
        mMethodOverride(m_AtHdlcBundleOverride, OamFlowDelete);
        mMethodOverride(m_AtHdlcBundleOverride, SchedulingModeSet);
        mMethodOverride(m_AtHdlcBundleOverride, SchedulingModeGet);
        }

    mMethodsSet(bundle, &m_AtHdlcBundleOverride);
    }

static void OverrideAtObject(ThaMpBundle self)
    {
	AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaMpBundle self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtHdlcBundle(self);
    OverrideAtMpBundle(self);
    }

static void MethodsInit(ThaMpBundle self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, CounterAddress);
        mMethodOverride(m_methods, ClassDefaultOffset);
        mMethodOverride(m_methods, QueueDefaultOffset);
        mMethodOverride(m_methods, QueueSchedulerOffset);
        mMethodOverride(m_methods, MaxNumQueues);
        mMethodOverride(m_methods, QueueEnable);
        mMethodOverride(m_methods, FlowClassAssign);
        mMethodOverride(m_methods, FlowClassDeassign);
        mMethodOverride(m_methods, FlowCanJoin);
        mMethodOverride(m_methods, LinkHwRemove);
        mMethodOverride(m_methods, NumberLinksSet);
        mMethodOverride(m_methods, QueueBlockSizeSet);
        mMethodOverride(m_methods, HardwareBundleRemove);
        }

    mMethodsSet(self, &m_methods);
    }

AtMpBundle ThaMpBundleObjectInit(AtMpBundle self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaMpBundle));

    /* Super constructor */
    if (AtMpBundleObjectInit((AtMpBundle)self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaMpBundle)self);

    /* Only initialize method structures one time */
    MethodsInit((ThaMpBundle)self);

    m_methodsInit = 1;

    return self;
    }

AtMpBundle ThaMpBundleNew(uint32 channelId, AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtMpBundle newBundle = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaMpBundle));
    if (newBundle == NULL)
        return NULL;

    /* Construct it */
    return ThaMpBundleObjectInit(newBundle, channelId, module);
    }

uint8 ThaMpBundlePartOfPpp(ThaModulePpp self, ThaMpBundle bundle)
    {
    uint32 numBundlePerPart = ThaModulePppNumBundlePerPart((AtModulePpp)self);

    if (numBundlePerPart == 0)
        return 0;

    return (uint8)(AtChannelIdGet((AtChannel)bundle) / numBundlePerPart);
    }

uint32 ThaMpBundlePartOffset(ThaMpBundle self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset((ThaDevice)device, cAtModulePpp, ThaMpBundlePartOfPpp(PppModule(self), self));
    }

uint32 ThaModulePppLocalBundleId(ThaMpBundle bundle)
    {
    uint32 numBundlePerPart = ThaModulePppNumBundlePerPart((AtModulePpp)PppModule(bundle));
    uint32 bundleId = AtChannelIdGet((AtChannel)bundle);
    return (numBundlePerPart == 0) ? bundleId : (bundleId % numBundlePerPart);
    }

void ThaMlpppBundleQueueBlockSizeSet(ThaMpBundle self, uint8 queueId, uint8 rangeId)
    {
    if (self)
        mMethodsGet(self)->QueueBlockSizeSet(self, queueId, rangeId);
    }

eAtRet ThaMpBundleQueueEnable(ThaMpBundle self, uint8 queueId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->QueueEnable(self, queueId, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaMpBundleNumberLinksSet(ThaMpBundle self, uint16 numLinkInBundle)
    {
    if (self)
        return mMethodsGet(mThis(self))->NumberLinksSet(self, numLinkInBundle);
    return cAtErrorNullPointer;
    }

eAtRet ThaMpBundleHardwareBundleRemove(ThaMpBundle self)
    {
    if (self)
        return mMethodsGet(self)->HardwareBundleRemove(self);
    return cAtErrorNullPointer;
    }

AtEthFlow AtHdlcBundleOamFlowCreate(AtHdlcBundle self)
    {
    AtEthFlow flow = ThaModuleEthMpOamFlowCreate(ModuleEth(self), self);
    if (flow == NULL)
        return NULL;

    AtChannelInit((AtChannel)flow);

    /* Catch this bundle */
    AtEthFlowHdlcBundleSet(flow, self);
    return flow;
    }

void AtHdlcBundleOamFlowDelete(AtHdlcBundle self)
    {
    AtEthFlow oamFlow = AtHdlcBundleOamFlowGet(self);
    if (oamFlow == NULL)
        return;

    ThaModuleEthMpOamFlowDelete(ModuleEth(self), oamFlow);
    }

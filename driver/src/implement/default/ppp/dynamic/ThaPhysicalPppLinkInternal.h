/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPhysicalPppLinkInternal.h
 * 
 * Created Date: Apr 8, 2016
 *
 * Description : Physical PPP Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPHYSICALPPPLINKINTERNAL_H_
#define _THAPHYSICALPPPLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaPppLinkInternal.h"
#include "ThaPhysicalPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPhysicalPppLinkMethods
    {
    eAtRet (*OamProfileSet)(ThaPhysicalPppLink self, ThaProfile profile);
    eAtRet (*NetworkProfileSet)(ThaPhysicalPppLink self, ThaProfile profile);
    eAtRet (*ErrorProfileSet)(ThaPhysicalPppLink self, ThaProfile profile);
    eAtRet (*BcpProfileSet)(ThaPhysicalPppLink self, ThaProfile profile);
    }tThaPhysicalPppLinkMethods;

typedef struct tThaPhysicalPppLink
    {
    tThaPppLink super;
    const tThaPhysicalPppLinkMethods *methods;

    /* Private data */
    ThaProfile oamProfile;
    ThaProfile networkProfile;
    ThaProfile errorProfile;
    ThaProfile bcpProfile;
    eAtHdlcPduType pduType;
    uint8 phase;
    }tThaPhysicalPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaPhysicalPppLinkObjectInit(AtPppLink self, AtHdlcChannel physicalChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THAPHYSICALPPPLINKINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaDynamicPppLink.h
 * 
 * Created Date: Apr 7, 2016
 *
 * Description : Dynamic PPP link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADYNAMICPPPLINK_H_
#define _THADYNAMICPPPLINK_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaDynamicPppLinkNew(AtHdlcChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THADYNAMICPPPLINK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaPhysicalPppLink.h
 * 
 * Created Date: Apr 1, 2016
 *
 * Description : Physical PPP Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPHYSICALPPPLINK_H_
#define _THAPHYSICALPPPLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../encap/profile/profilepool/ThaProfile.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPhysicalPppLink *ThaPhysicalPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaPhysicalPppLinkNew(AtHdlcChannel physicalChannel);

eAtRet ThaPhysicalPppLinkBcpProfileSet(ThaPhysicalPppLink self, ThaProfile profile);
ThaProfile ThaPhysicalPppLinkBcpProfileGet(ThaPhysicalPppLink self);

eAtRet ThaPhysicalPppLinkErrorProfileSet(ThaPhysicalPppLink self, ThaProfile profile);
ThaProfile ThaPhysicalPppLinkErrorProfileGet(ThaPhysicalPppLink self);

eAtRet ThaPhysicalPppLinkNetworkProfileSet(ThaPhysicalPppLink self, ThaProfile profile);
ThaProfile ThaPhysicalPppLinkNetworkProfileGet(ThaPhysicalPppLink self);

eAtRet ThaPhysicalPppLinkOamProfileSet(ThaPhysicalPppLink self, ThaProfile profile);
ThaProfile ThaPhysicalPppLinkOamProfileGet(ThaPhysicalPppLink self);

void ThaPhysicalPppLinkPduTypeCache(ThaPhysicalPppLink self, eAtHdlcPduType pduType);

#ifdef __cplusplus
}
#endif
#endif /* _THAPHYSICALPPPLINK_H_ */


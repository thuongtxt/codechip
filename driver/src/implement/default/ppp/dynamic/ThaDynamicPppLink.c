/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaDynamicPppLink.c
 *
 * Created Date: Apr 6, 2016
 *
 * Description : Dynamic PPP link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaPppLinkInternal.h"
#include "../../encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../eth/controller/ThaEthFlowController.h"
#include "../../eth/ThaModuleEth.h"
#include "ThaDynamicPppLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaDynamicPppLink)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaDynamicPppLink * ThaDynamicPppLink;
typedef struct tThaDynamicPppLink
    {
    tThaPppLink super;

    tThaDynamicHdlcLinkCache *hdlcCache;
    uint8 bcpLanFcsEnabled;
    }tThaDynamicPppLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPppLinkMethods  m_AtPppLinkOverride;
static tAtChannelMethods  m_AtChannelOverride;
static tAtHdlcLinkMethods m_AtHdlcLinkOverride;
static tAtObjectMethods   m_AtObjectOverride;
static tThaPppLinkMethods m_ThaPppLinkOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtObjectMethods   *m_AtObjectMethods   = NULL;
static const tAtHdlcLinkMethods *m_AtHdlcLinkMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth ModuleEth(AtHdlcLink self)
    {
    return (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    }

static ThaEthFlowController ControllerGet(AtHdlcLink self)
    {
    return ThaModuleEthPppFlowControllerGet(ModuleEth(self));
    }

#include "../../encap/dynamic/ThaDynamicHdlcLinkCommon.h"

static eAtModulePppRet PhaseSet(AtPppLink self, eAtPppLinkPhase phase)
    {
    if (IsLogical((AtHdlcLink)self))
        mHdlcCacheValueSet(phase);

    return AtPppLinkPhaseSet((AtPppLink)PhysicalLinkGet((AtHdlcLink)self), phase);
    }

static eAtPppLinkPhase PhaseGet(AtPppLink self)
    {
    if (IsLogical((AtHdlcLink)self))
        mHdlcCacheValueGet(phase, cAtPppLinkPhaseUnknown);

    return AtPppLinkPhaseGet((AtPppLink)PhysicalLinkGet((AtHdlcLink)self));
    }

static AtPidTable PidTableGet(AtPppLink self)
    {
    AtUnused(self);
    /* TODO: implement this */
    return NULL;
    }

static eAtModulePppRet TxProtocolFieldCompress(AtPppLink self, eBool compress)
    {
    if (IsLogical((AtHdlcLink)self))
        mHdlcCacheValueSet(compress);

    return AtPppLinkTxProtocolFieldCompress((AtPppLink)PhysicalLinkGet((AtHdlcLink)self), compress);
    }

static eBool TxProtocolFieldIsCompressed(AtPppLink self)
    {
    if (IsLogical((AtHdlcLink)self))
        mHdlcCacheValueGet(compress, cAtFalse);

    return AtPppLinkTxProtocolFieldIsCompressed((AtPppLink)PhysicalLinkGet((AtHdlcLink)self));
    }

static eAtRet JoinBundle(ThaPppLink self, ThaMpBundle bundle)
    {
    ThaPppLink phylink = (ThaPppLink)ThaDynamicHdlcLinkPhysicalHdlcLinkGet((AtHdlcLink)self);
    if (phylink == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(phylink)->JoinBundle(phylink, bundle);
    }

static eAtRet EgMlpppEnable(ThaPppLink self, eBool enable)
    {
    ThaPppLink phylink = (ThaPppLink)ThaDynamicHdlcLinkPhysicalHdlcLinkGet((AtHdlcLink)self);
    if (phylink == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(phylink)->EgMlpppEnable(phylink, enable);
    }

static eAtRet IgMlpppEnable(ThaPppLink self, eBool enable)
    {
    ThaPppLink phylink = (ThaPppLink)ThaDynamicHdlcLinkPhysicalHdlcLinkGet((AtHdlcLink)self);
    if (phylink == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(phylink)->IgMlpppEnable(phylink, enable);
    }

static eAtRet MemberLocalIdInBundleSet(ThaPppLink self , uint16 localMemberId)
    {
    ThaPppLink phylink = (ThaPppLink)ThaDynamicHdlcLinkPhysicalHdlcLinkGet((AtHdlcLink)self);
    if (phylink == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(phylink)->MemberLocalIdInBundleSet(phylink, localMemberId);
    }

static eBool BcpLanFcsCanApplyToHardware(AtPppLink self)
    {
    AtEthFlow flow = AtHdlcLinkBoundFlowGet((AtHdlcLink)self);
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);

    if (IsLogical((AtHdlcLink)self))
        return cAtFalse;

    if ((flow != NULL) || (pw != NULL))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModulePppRet BcpLanFcsInsertionEnable(AtPppLink self, eBool enable)
    {
    if (AtHdlcLinkPduTypeGet((AtHdlcLink)self) != cAtHdlcPduTypeEthernet)
        return cAtErrorModeNotSupport;

    if (!BcpLanFcsCanApplyToHardware(self))
        {
        mThis(self)->bcpLanFcsEnabled = enable;
        return cAtOk;
        }

    return AtPppLinkBcpLanFcsInsertionEnable((AtPppLink)PhysicalLinkGet((AtHdlcLink)self), enable);
    }

static eBool BcpLanFcsInsertionIsEnabled(AtPppLink self)
    {
    if (AtHdlcLinkPduTypeGet((AtHdlcLink)self) != cAtHdlcPduTypeEthernet)
        return cAtFalse;

    if (!BcpLanFcsCanApplyToHardware(self))
        return mThis(self)->bcpLanFcsEnabled;

    return AtPppLinkBcpLanFcsInsertionIsEnabled((AtPppLink)PhysicalLinkGet((AtHdlcLink)self));
    }

static void OverrideAtPppLink(AtPppLink self)
    {
    if (!m_methodsInit)
        {
        /* Reuse all super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, mMethodsGet(self), sizeof(m_AtPppLinkOverride));

        /* Override */
        mMethodOverride(m_AtPppLinkOverride, PhaseSet);
        mMethodOverride(m_AtPppLinkOverride, PhaseGet);
        mMethodOverride(m_AtPppLinkOverride, PidTableGet);
        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldCompress);
        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldIsCompressed);
        mMethodOverride(m_AtPppLinkOverride, BcpLanFcsInsertionEnable);
        mMethodOverride(m_AtPppLinkOverride, BcpLanFcsInsertionIsEnabled);
        }

    mMethodsSet(self, &m_AtPppLinkOverride);
    }

static void OverrideThaPppLink(AtPppLink self)
    {
    ThaPppLink thaLink = (ThaPppLink)self;
    if (!m_methodsInit)
        {
        /* Reuse all super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(thaLink), sizeof(m_ThaPppLinkOverride));

        /* Override */
        mMethodOverride(m_ThaPppLinkOverride, JoinBundle);
        mMethodOverride(m_ThaPppLinkOverride, EgMlpppEnable);
        mMethodOverride(m_ThaPppLinkOverride, IgMlpppEnable);
        mMethodOverride(m_ThaPppLinkOverride, MemberLocalIdInBundleSet);
        }

    mMethodsSet(thaLink, &m_ThaPppLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideAtChannel((AtHdlcLink)self);
    OverrideAtHdlcLink((AtHdlcLink)self);
    OverrideAtObject((AtHdlcLink)self);
    OverrideAtPppLink(self);
    OverrideThaPppLink(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDynamicPppLink);
    }

static AtPppLink ObjectInit(AtPppLink self, AtHdlcChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppLinkObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink ThaDynamicPppLinkNew(AtHdlcChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, channel);
    }

eAtRet ThaDynamicPppLinkActivateConfiguration(AtHdlcLink self)
    {
    eAtRet ret;
    tThaDynamicHdlcLinkCache *cache = HdlcCacheGet(self);

    ret = HdlcLinkActivateConfiguration(self);
    if (ret != cAtOk)
        return ret;

    ret  = AtPppLinkTxProtocolFieldCompress((AtPppLink)self, cache->compress);
    ret |= AtPppLinkPhaseSet((AtPppLink)self, cache->phase);

    return ret;
    }

eAtRet ThaDynamicPppLinkConfigureCache(AtHdlcLink self)
    {
    eAtRet ret;
    tThaDynamicHdlcLinkCache *cache = HdlcCacheGet(self);

    ret = HdlcLinkConfigureCache(self);
    if (ret != cAtOk)
        return ret;

    cache->phase    = AtPppLinkPhaseGet((AtPppLink)self);
    cache->compress = AtPppLinkTxProtocolFieldIsCompressed((AtPppLink)self);

    return cAtOk;
    }

eBool ThaDynamicPppLinkLanFcsCacheGet(AtHdlcLink logicLink)
    {
    if (logicLink)
        return mThis(logicLink)->bcpLanFcsEnabled;
    return cAtFalse;
    }

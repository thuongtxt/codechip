/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : ThaPhysicalPppLink.c
 *
 * Created Date: Apr 1, 2016
 *
 * Description : Default physical PPP Link implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../eth/controller/ThaEthFlowControllerInternal.h"
#include "../../eth/ThaModuleEthInternal.h"
#include "../../encap/dynamic/ThaModuleDynamicEncap.h"
#include "../../encap/dynamic/ThaPhysicalHdlcChannel.h"
#include "../../encap/dynamic/ThaDynamicHdlcLink.h"
#include "../../mpeg/ThaModuleMpeg.h"
#include "../ThaPppLink.h"
#include "ThaPhysicalPppLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaPhysicalPppLink)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPhysicalPppLinkMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;
static tAtHdlcLinkMethods       m_AtHdlcLinkOverride;
static tAtPppLinkMethods        m_AtPppLinkOverride;
static tThaPppLinkMethods       m_ThaPppLinkOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods  = NULL;
static const tAtChannelMethods  *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwIdGet(AtChannel self)
    {
    /* Once an HDLC channel has the physical binding, the logical ID of that HDLC
     * channel will be used as the physical link ID in advance. */
    AtHdlcChannel physicalChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    return AtChannelIdGet((AtChannel)ThaPhysicalHdlcChannelLogicalChannelGet(physicalChannel));
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    return (defectMask & enableMask) ? cAtErrorModeNotSupport : cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet Init(AtChannel self)
    {
    /* This kind of link is totally different with ThaPppLink, so do not call it's super initialization */
    /* Disable PFC as default */
    eAtRet ret = AtHdlcLinkProtocolCompress((AtHdlcLink)self, cAtFalse);
    ret |= AtPppLinkPhaseSet((AtPppLink)self, cAtPppLinkPhaseDead);

    return ret;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "physical_ppp_link";
    }

static eAtRet OamProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;

    ThaProfileHdlcLinkRemove(ThaPhysicalPppLinkOamProfileGet(self), hdlcLink);
    self->oamProfile = profile;
    ThaProfileHdlcLinkAdd(profile, hdlcLink);

    return cAtOk;
    }

static eAtRet NetworkProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;

    ThaProfileHdlcLinkRemove(ThaPhysicalPppLinkNetworkProfileGet(self), hdlcLink);
    self->networkProfile = profile;
    ThaProfileHdlcLinkAdd(profile, hdlcLink);

    return cAtOk;
    }

static eAtRet ErrorProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;

    ThaProfileHdlcLinkRemove(ThaPhysicalPppLinkErrorProfileGet(self), hdlcLink);
    self->errorProfile = profile;
    ThaProfileHdlcLinkAdd(profile, hdlcLink);

    return cAtOk;
    }

static eAtRet BcpProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;

    ThaProfileHdlcLinkRemove(ThaPhysicalPppLinkBcpProfileGet(self), hdlcLink);
    self->bcpProfile = profile;
    ThaProfileHdlcLinkAdd(profile, hdlcLink);

    return cAtOk;
    }

static eAtRet OamPacketModeSet(AtHdlcLink self, eAtHdlcLinkOamMode oamPacketMode)
    {
    AtUnused(self);
    return (oamPacketMode == cAtHdlcLinkOamModeToPsn) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcLinkOamMode OamPacketModeGet(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtHdlcLinkOamModeToPsn;
    }

static eAtRet OamSend(AtHdlcLink self, uint8 *buffer, uint32 oamLength)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(oamLength);
    return cAtErrorModeNotSupport;
    }

static uint32 OamReceive(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(bufferLength);
    return 0;
    }

static eBool AllOamsSent(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet PidByPass(AtHdlcLink self, eBool pidByPass)
    {
    AtUnused(self);
    AtUnused(pidByPass);
    return cAtOk;
    }

static eAtRet PppPacketEnable(ThaPppLink self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool PppPacketIsEnabled(ThaPppLink self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet PduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType)
    {
    eAtRet ret;
    ThaProfileManager manager;
    ThaProfileFinder networkFinder, bcpFinder;
    uint16 networkProfileType;

    /* Apply this when phase was network active */
    ThaPhysicalPppLinkPduTypeCache(mThis(self), pduType);
    if (AtPppLinkPhaseGet((AtPppLink)self) != cAtPppLinkPhaseNetworkActive)
        return cAtOk;

    networkProfileType = ThaModuleEncapPduType2NetworkProfileType(pduType);
    manager = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    networkFinder = ThaProfileManagerNetworkProfileFinderGet(manager);
    ThaProfileFinderRuleSet(networkFinder, networkProfileType, cThaProfileRuleData);
    ret = ThaPhysicalPppLinkNetworkProfileSet(mThis(self), ThaProfileFinderProfileForLinkFind(networkFinder, self));
    if (ret != cAtOk)
        return ret;

    if (networkProfileType == cThaProfileNetworkTypeBcp)
        {
        bcpFinder = ThaProfileManagerBcpProfileFinderGet(manager);
        ThaProfileFinderRuleSet(bcpFinder, cThaProfileBcpTypeAll, cThaProfileRuleData);
        ret = ThaPhysicalPppLinkBcpProfileSet(mThis(self), ThaProfileFinderProfileForLinkFind(bcpFinder, self));
        }

    return cAtOk;
    }

static eAtHdlcPduType PduTypeGet(AtHdlcLink self)
    {
    return mThis(self)->pduType;
    }

static eAtRet PhaseEstablishAndAuthenticateToProfile(ThaPhysicalPppLink self)
    {
    ThaProfileManager manager = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    ThaProfileFinder profileFinder = ThaProfileManagerOamProfileFinderGet(manager);

    ThaProfileFinderRuleSet(profileFinder, cThaProfileOamTypeLCP, cThaProfileRuleControl);
    return ThaPhysicalPppLinkOamProfileSet(self, ThaProfileFinderProfileForLinkFind(profileFinder, (AtHdlcLink)self));
    }

static eAtRet PhaseEnterNetworkToProfile(ThaPhysicalPppLink self)
    {
    ThaProfileManager manager = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    ThaProfileFinder profileFinder = ThaProfileManagerOamProfileFinderGet(manager);

    ThaProfileFinderRuleSet(profileFinder, cThaProfileOamTypeLCP, cThaProfileRuleControl);
    ThaProfileFinderRuleSet(profileFinder, cThaProfileOamTypeNCP, cThaProfileRuleControl);
    return ThaPhysicalPppLinkOamProfileSet(self, ThaProfileFinderProfileForLinkFind(profileFinder, (AtHdlcLink)self));
    }

static eAtRet PhaseNetworkActiveToProfile(ThaPhysicalPppLink self)
    {
    eAtRet ret;
    ThaProfileManager manager = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    ThaProfileFinder profileFinder = ThaProfileManagerOamProfileFinderGet(manager);
    uint16 networkProfileType;

    ThaProfileFinderRuleSet(profileFinder, cThaProfileOamTypeLCP, cThaProfileRuleControl);
    ThaProfileFinderRuleSet(profileFinder, cThaProfileOamTypeNCP, cThaProfileRuleControl);
    ret = ThaPhysicalPppLinkOamProfileSet(self, ThaProfileFinderProfileForLinkFind(profileFinder, (AtHdlcLink)self));
    if (ret != cAtOk)
        return ret;

    profileFinder = ThaProfileManagerNetworkProfileFinderGet(manager);
    networkProfileType = ThaModuleEncapPduType2NetworkProfileType(self->pduType);
    ThaProfileFinderRuleSet(profileFinder, networkProfileType, cThaProfileRuleData);
    ret = ThaPhysicalPppLinkNetworkProfileSet(self, ThaProfileFinderProfileForLinkFind(profileFinder, (AtHdlcLink)self));
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static eAtRet AllProfileDrop(ThaPhysicalPppLink phyLink)
    {
    eAtRet ret;
    ThaProfileManager manager;
    ThaProfileFinder networkFinder, oamFinder, errorFinder;

    manager = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)phyLink));
    networkFinder = ThaProfileManagerNetworkProfileFinderGet(manager);
    ThaProfileFinderRuleSet(networkFinder, cThaProfileNetworkTypeAll, cThaProfileRuleDrop);
    ret = ThaPhysicalPppLinkNetworkProfileSet(phyLink, ThaProfileFinderProfileForLinkFind(networkFinder, (AtHdlcLink)phyLink));
    if (ret != cAtOk)
        return ret;

    oamFinder = ThaProfileManagerOamProfileFinderGet(manager);
    ThaProfileFinderRuleSet(oamFinder, cThaProfileOamTypeAll, cThaProfileRuleDrop);
    ret = ThaPhysicalPppLinkOamProfileSet(phyLink, ThaProfileFinderProfileForLinkFind(oamFinder, (AtHdlcLink)phyLink));
    if (ret != cAtOk)
        return ret;

    errorFinder = ThaProfileManagerErrorProfileFinderGet(manager);
    ThaProfileFinderRuleSet(errorFinder, cThaProfileErrorTypeAll, cThaProfileRuleDrop);
    return ThaPhysicalPppLinkErrorProfileSet(phyLink, ThaProfileFinderProfileForLinkFind(errorFinder, (AtHdlcLink)phyLink));
    }

static eAtModulePppRet PhaseSet(AtPppLink self, eAtPppLinkPhase phase)
    {
    ThaPhysicalPppLink phyLink = (ThaPhysicalPppLink)self;

    if (phase == cAtPppLinkPhaseUnknown)
        return cAtErrorModeNotSupport;

    mThis(self)->phase = phase;
    if (phase == cAtPppLinkPhaseDead)
        return AllProfileDrop(phyLink);

    if ((phase == cAtPppLinkPhaseEstablish) || (phase == cAtPppLinkPhaseAuthenticate))
        return PhaseEstablishAndAuthenticateToProfile(phyLink);

    if (phase == cAtPppLinkPhaseEnterNetwork)
        return PhaseEnterNetworkToProfile(phyLink);

    return PhaseNetworkActiveToProfile(phyLink);
    }

static eAtPppLinkPhase PhaseGet(AtPppLink self)
    {
    return mThis(self)->phase;
    }

static AtPw BoundPseudowireGet(AtChannel self)
    {
    return AtChannelBoundPwGet((AtChannel)ThaPhysicalHdlcLinkLogicLinkGet((AtHdlcLink)self));
    }

static AtEthFlow BoundFlowGet(AtHdlcLink self)
    {
    return AtHdlcLinkBoundFlowGet(ThaPhysicalHdlcLinkLogicLinkGet(self));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPhysicalPppLink object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(oamProfile);
    mEncodeObjectDescription(networkProfile);
    mEncodeObjectDescription(errorProfile);
    mEncodeObjectDescription(bcpProfile);
    mEncodeUInt(pduType);
    mEncodeUInt(phase);
    }

static void PrintProfile(const char* prefix, ThaProfile profile)
    {
    AtPrintc(cSevNormal, "   - %-30s:", prefix);
    if (profile)
        AtPrintc(cSevInfo, " %u\r\n", ThaProfileProfileIdGet(profile) + 1);
    else
        AtPrintc(cSevWarning, " None\r\n");
    }

static eAtRet Debug(AtChannel self)
    {
    AtPrintc(cSevNormal, "\r\n* Link profiles: \r\n");
    PrintProfile("Bcp profile ID", mThis(self)->bcpProfile);
    PrintProfile("Error profile ID", mThis(self)->errorProfile);
    PrintProfile("Network profile ID", mThis(self)->networkProfile);
    PrintProfile("OAM profile ID", mThis(self)->oamProfile);

    return cAtOk;
    }

static void OverrideAtObject(AtPppLink self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, BoundPseudowireGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcLink(AtPppLink self)
    {
    AtHdlcLink link = (AtHdlcLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(link), sizeof(m_AtHdlcLinkOverride));

        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeSet);
        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeGet);
        mMethodOverride(m_AtHdlcLinkOverride, OamSend);
        mMethodOverride(m_AtHdlcLinkOverride, OamReceive);
        mMethodOverride(m_AtHdlcLinkOverride, AllOamsSent);
        mMethodOverride(m_AtHdlcLinkOverride, PidByPass);
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeSet);
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeGet);
        mMethodOverride(m_AtHdlcLinkOverride, BoundFlowGet);
        }

    mMethodsSet(link, &m_AtHdlcLinkOverride);
    }

static void OverrideAtPppLink(AtPppLink self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, mMethodsGet(self), sizeof(m_AtPppLinkOverride));

        mMethodOverride(m_AtPppLinkOverride, PhaseGet);
        mMethodOverride(m_AtPppLinkOverride, PhaseSet);
        }

    mMethodsSet(self, &m_AtPppLinkOverride);
    }

static void OverrideThaPppLink(AtPppLink self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPppLinkOverride));

        mMethodOverride(m_ThaPppLinkOverride, PppPacketEnable);
        mMethodOverride(m_ThaPppLinkOverride, PppPacketIsEnabled);
        }

    mMethodsSet(link, &m_ThaPppLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtHdlcLink(self);
    OverrideAtPppLink(self);
    OverrideThaPppLink(self);
    }

static void MethodsInit(ThaPhysicalPppLink self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BcpProfileSet);
        mMethodOverride(m_methods, NetworkProfileSet);
        mMethodOverride(m_methods, ErrorProfileSet);
        mMethodOverride(m_methods, OamProfileSet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPhysicalPppLink);
    }

AtPppLink ThaPhysicalPppLinkObjectInit(AtPppLink self, AtHdlcChannel physicalChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPppLinkObjectInit(self, physicalChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtPppLink ThaPhysicalPppLinkNew(AtHdlcChannel physicalChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ThaPhysicalPppLinkObjectInit(newLink, physicalChannel);
    }

eAtRet ThaPhysicalPppLinkOamProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    if (self)
        return mMethodsGet(self)->OamProfileSet(self, profile);
    return cAtErrorNullPointer;
    }

ThaProfile ThaPhysicalPppLinkOamProfileGet(ThaPhysicalPppLink self)
    {
    if (self)
        return self->oamProfile;
    return NULL;
    }

eAtRet ThaPhysicalPppLinkNetworkProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    if (self)
        return mMethodsGet(self)->NetworkProfileSet(self, profile);
    return cAtErrorNullPointer;
    }

ThaProfile ThaPhysicalPppLinkNetworkProfileGet(ThaPhysicalPppLink self)
    {
    if (self)
        return self->networkProfile;
    return NULL;
    }

eAtRet ThaPhysicalPppLinkErrorProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    if (self)
        return mMethodsGet(self)->ErrorProfileSet(self, profile);
    return cAtErrorNullPointer;
    }

ThaProfile ThaPhysicalPppLinkErrorProfileGet(ThaPhysicalPppLink self)
    {
    if (self)
        return self->errorProfile;
    return NULL;
    }

eAtRet ThaPhysicalPppLinkBcpProfileSet(ThaPhysicalPppLink self, ThaProfile profile)
    {
    if (self)
        return mMethodsGet(self)->BcpProfileSet(self, profile);
    return cAtErrorNullPointer;
    }

ThaProfile ThaPhysicalPppLinkBcpProfileGet(ThaPhysicalPppLink self)
    {
    if (self)
        return self->bcpProfile;
    return NULL;
    }

void ThaPhysicalPppLinkPduTypeCache(ThaPhysicalPppLink self, eAtHdlcPduType pduType)
    {
    if (self)
        self->pduType = pduType;
    }

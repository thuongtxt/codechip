/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP/MLPPP module
 *
 * File        : ThaPppLink.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa PPP link default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtHdlcChannel.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/encap/AtHdlcChannelInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../eth/ThaEthFlow.h"
#include "../encap/ThaHdlcChannelInternal.h"
#include "../util/ThaUtil.h"
#include "ThaPppLinkInternal.h"
#include "ThaModulePppInternal.h"
#include "ThaMpegReg.h"
#include "ThaMpigReg.h"
#include "ThaPmcReg.h"
#include "ThaModulePpp.h"
#include "../eth/ThaEthFlowInternal.h"
#include "../eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cOamReadyTimeOut         100  /* ms*/
#define cLinkStatChangeTimeOut   1000 /*1000 ms*/
#define cThaMaxBlkPerLink        16

/* Interrupt bitmap */
#define cThaDatLinkBuffOverIntr cBit12
#define cThaDatlLinkOtherIntr   cBit11
#define cThaDatLinkMissSopIntr  cBit10
#define cThaDatLinkBcpIntr      cBit9
#define cThaDatLinkMlpppIntr    cBit8
#define cThaDatLinkMcmpIntr     cBit7
#define cThaDatLinkMpflowIntr   cBit6
#define cThaDatLinkPppPidIntr   cBit5
#define cThaDatLinkAddCtlIntr   cBit4
#define cThaDatLinkDdrCrcIntr   cBit3
#define cThaDatDecFcsErrIntr    cBit2
#define cThaDatDecRxErrIntr     cBit1
#define cThaDatLinkOutErrIntr   cBit0

/* Traffic modes */
#define cForwardToPsn 0
#define cDiscard      15

#define cInvalidValue 0xFF

/*--------------------------- Macros -----------------------------------------*/
#define mDefaultBlockOffset(self, blockAddress) mMethodsGet((ThaPppLink)self)->DefaultBLockOffset((ThaPppLink)self, blockAddress)
#define mIntrDefaultOffset(self) mMethodsGet((ThaPppLink)self)->IntrDefaultOffset((ThaPppLink)self)
#define mLinkId(pppLink) AtChannelHwIdGet((AtChannel)pppLink)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPppLinkMethods m_methods;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tAtHdlcLinkMethods m_AtHdlcLinkOverride;
static tAtPppLinkMethods  m_AtPppLinkOverride;
static tAtObjectMethods   m_AtObjectOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtObjectMethods   *m_AtObjectMethods   = NULL;
static const tAtHdlcLinkMethods *m_AtHdlcLinkMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tThaPppLinkSimulation *SimulationDatabaseCreate(AtPppLink self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaPppLinkSimulation);
    tThaPppLinkSimulation *database = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    AtUnused(self);

    if (database)
        mMethodsGet(osal)->MemInit(osal, database, 0, memorySize);

    return database;
    }

static void SimulationDatabaseDelete(AtPppLink self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, ((ThaPppLink)self)->simulation);
    ((ThaPppLink)self)->simulation = NULL;
    }

static tThaPppLinkSimulation *SimulationDatabase(AtPppLink self)
    {
    if (((ThaPppLink)self)->simulation == NULL)
        ((ThaPppLink)self)->simulation = SimulationDatabaseCreate(self);
    return ((ThaPppLink)self)->simulation;
    }

static uint32 IntrDefaultOffset(ThaPppLink self)
    {
    uint32 linkId;
    uint32 groupId, bitId;

    linkId = mLinkId(self);

    groupId = (linkId & cBit10_5) >> 5;
    bitId   =  linkId & cBit4_0;

    return (groupId * 32) + bitId + ThaPppLinkPartOffset(self);
    }

static uint32 QueueDefaultOffset(ThaPppLink self, uint16 queueId)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleEncap encapModule = (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    return (queueId * ThaModuleEncapNumEncapPerPart(encapModule)) + ThaPppLinkOffset(self);
    }

static ThaModuleEncap EncapModule(ThaPppLink link)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)link));
    return (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static void HardwareInterruptShow(AtChannel self, uint32 intrBitmap)
    {
	AtUnused(self);
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Internal status: ");
    if (intrBitmap == 0)
        {
        AtPrintc(cSevInfo, "None\r\n");
        return;
        }

    if ((intrBitmap & cThaDatLinkOutErrIntr))
        AtPrintc(cSevCritical, " OutErr");
    if ((intrBitmap & cThaDatDecRxErrIntr))
        AtPrintc(cSevCritical, " RxErr");
    if ((intrBitmap & cThaDatDecFcsErrIntr))
        AtPrintc(cSevCritical, " FcsErr");
    if ((intrBitmap & cThaDatLinkDdrCrcIntr))
        AtPrintc(cSevCritical, " DdrCrc");
    if ((intrBitmap & cThaDatLinkAddCtlIntr))
        AtPrintc(cSevCritical, " AddCtl");
    if ((intrBitmap & cThaDatLinkPppPidIntr))
        AtPrintc(cSevCritical, " PppPid");
    if ((intrBitmap & cThaDatLinkMpflowIntr))
        AtPrintc(cSevCritical, " Mpflow");
    if ((intrBitmap & cThaDatLinkMcmpIntr))
        AtPrintc(cSevCritical, " Mcmp");
    if ((intrBitmap & cThaDatLinkMlpppIntr))
        AtPrintc(cSevCritical, " Mlppp");
    if ((intrBitmap & cThaDatLinkBcpIntr))
        AtPrintc(cSevCritical, " Bcp");
    if ((intrBitmap & cThaDatLinkMissSopIntr))
        AtPrintc(cSevCritical, " MissSop");
    if ((intrBitmap & cThaDatlLinkOtherIntr))
        AtPrintc(cSevCritical, " Other");
    if ((intrBitmap & cThaDatLinkBuffOverIntr))
        AtPrintc(cSevCritical, " BuffOver");

    AtPrintc(cSevNormal, "\r\n");
    }

static uint32 NumBlockGet(ThaPppLink self)
    {
    uint32 regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleMpig);
    uint32 numBlocks;
    ThaModuleEncap encapModule = EncapModule(self);

    mFieldGet(regVal, mLinkMask(encapModule, BlkAloc), mLinkShift(encapModule, BlkAloc), uint32, &numBlocks);

    return numBlocks + 1;
    }

static eBool BlockIsValid(ThaPppLink self)
    {
    uint32 regAddr = cThaRegMPIGDatEmiLinkBufPoolAddr + mDefaultBlockOffset(self, 0);
    uint32 regVal;
    eBool  blockValid;
    ThaModuleEncap encapModule = EncapModule(self);

    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldGet(regVal, mLinkMask(encapModule, MPIGDatEmiLbpBlkVl), mLinkShift(encapModule, MPIGDatEmiLbpBlkVl), eBool, &blockValid);

    return (blockValid) ? cAtTrue : cAtFalse;
    }

static uint32* BlockIdListGet(ThaPppLink self)
    {
    uint8  i;
    uint32 regAddr, regVal;
    static uint32 blockIdListBuf[cThaMaxBlkPerLink];
    AtOsal osal = AtSharedDriverOsalGet();
    ThaModuleEncap encapModule = EncapModule(self);

    mMethodsGet(osal)->MemInit(osal, blockIdListBuf, 0, cThaMaxBlkPerLink);
    if (!BlockIsValid(self))
        return NULL;

    for (i = 0; i < NumBlockGet(self); i++)
        {
        regAddr = cThaRegMPIGDatEmiLinkBufPoolAddr + mDefaultBlockOffset(self, i);
        regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
        mFieldGet(regVal, mLinkMask(encapModule, MPIGDatEmiLbpBlkID), mLinkShift(encapModule, MPIGDatEmiLbpBlkID), uint32, &blockIdListBuf[i]);
        }

    return blockIdListBuf;
    }

static char *BlockIdList2Str(ThaPppLink self, const uint32 *blockIdList)
    {
    static char blockIdListBuf[100];
    char        blockIdBuf[16];
    uint8       i;
    AtOsal      osal = AtSharedDriverOsalGet();

    mMethodsGet(osal)->MemInit(osal, blockIdListBuf, 0, cThaMaxBlkPerLink);

    if (blockIdList == NULL)
        {
        AtSprintf(blockIdListBuf, "None");
        return blockIdListBuf;
        }

    for (i = 0; i < NumBlockGet(self); i++)
        {
        AtSprintf(blockIdBuf, ",%u", blockIdList[i]);
        AtStrcat(blockIdListBuf, blockIdBuf);
        }

    return &blockIdListBuf[1];
    }

static eAtRet Debug(AtChannel self)
    {
    uint32 address, regValue;
    uint16 intrBitmap;
    ThaPppLink link = (ThaPppLink)self;

    /* Super */
    eAtRet ret = m_AtChannelMethods->Debug(self);
    if (ret != cAtOk)
        return ret;

    /* Print PPP OAM counters  */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Link Id         : %d\r\n", AtChannelIdGet(self) + 1);
    AtPrintc(cSevNormal, "* OAM Counters    :\r\n");
    AtPrintc(cSevNormal, "  LCP             : %d\r\n", link->lcpCounters);
    AtPrintc(cSevNormal, "  IPCP            : %d\r\n", link->ipcpCounters);
    AtPrintc(cSevNormal, "  IPv6CP          : %d\r\n", link->ipv6cpCounters);
    AtPrintc(cSevNormal, "  Other           : %d\r\n", link->otherOamCounters);
    AtPrintc(link->unknownPidCounters ? cSevCritical : cSevNormal, "  Unknown PID     : %d\r\n", link->unknownPidCounters);

    if (ThaPppLinkCanCheckOamsSent(link))
        {
        AtPrintc(cSevNormal, "  NumOamsRequested: %d\r\n", link->numOamsRequested);
        AtPrintc(cSevNormal, "  NumOamsSent     : %d\r\n", link->numOamsSent);
        AtPrintc(cSevNormal, "  All OAMs sent   : %s\r\n", AtHdlcLinkAllOamsSent((AtHdlcLink)self) ? "YES" : "NO");
        }

    /* Get Link interrupt status */
    address = cThaMPIGDATLinkAlarmIntrStat + mIntrDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    intrBitmap = regValue & cBit15_0;

    /* print Link interrupt status */
    HardwareInterruptShow(self, intrBitmap);

    /* Clear Interrupt status */
    address = cThaMPIGDATLinkAlarmIntrStat + mIntrDefaultOffset(self);
    mChannelHwWrite(self, address, cBit15_0, cThaModuleMpig);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Block Id List  : %s\r\n", BlockIdList2Str(link, BlockIdListGet(link)));

    return cAtOk;
    }

static eAtRet MlppHdrByPass(AtHdlcLink self, eBool mlppHdrByPass)
    {
    uint32 regVal[cThaRegMPIGLongRegSize];
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);
    uint32 mask    = mLinkMask(encapModule, MlppHdrMode);
    uint32 shift   = mLinkShift(encapModule, MlppHdrMode);
    uint32 regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);

    mChannelHwLongRead(self, regAddr, regVal, cThaRegMPIGLongRegSize, cThaModuleMpig);
    mFieldIns(&regVal[cThaMlppHdrModeDwordIndex], mask, shift, mlppHdrByPass ? 1 : 0);
    mChannelHwLongWrite(self, regAddr, regVal, cThaRegMPIGLongRegSize, cThaModuleMpig);

    return cAtOk;
    }

static eAtRet PidByPass(AtHdlcLink self, eBool pidByPass)
    {
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);
    uint32 mask    = mLinkMask(encapModule, PidMode);
    uint32 shift   = mLinkShift(encapModule, PidMode);
    uint32 regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleMpig);

    mFieldIns(&regVal, mask, shift, mBoolToBin(pidByPass));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

static uint32 DefaultOffset(ThaPppLink self)
    {
    return ThaPppLinkOffset(self);
    }

static uint32 DefaultBLockOffset(ThaPppLink self, uint8 blockaddress)
    {
    return (mLinkId(self) * 16) + blockaddress +  + ThaPppLinkPartOffset(self);
    }

static ThaEthFlowController EthFlowControllerGet(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return ThaModuleEthPppFlowControllerGet(ethModule);
    }

static eAtRet PppFlowActivate(ThaPppLink self, AtEthFlow flow)
    {
    ThaEthFlowController flowController = mMethodsGet(self)->EthFlowControllerGet(self);

    return ThaEthFlowControllerFlowActivate(flowController, flow, (AtChannel)self, 0);
    }

static eAtRet PppFlowDeActivate(ThaPppLink self, AtEthFlow flow)
    {
    ThaEthFlowController flowController = mMethodsGet(self)->EthFlowControllerGet(self);

    return ThaEthFlowControllerFlowDeActivate(flowController, flow);
    }

static eAtRet OamFlowActivate(AtHdlcLink self, AtEthFlow flow)
    {
    /* Get OAM flow controller managed by module Ethernet and let it does the job */
    ThaEthFlowController flowController = ThaModuleEthOamFlowControllerGet((ThaModuleEth)AtChannelModuleGet((AtChannel)flow));

    return ThaEthFlowControllerFlowActivate(flowController, flow, (AtChannel)self, 0);
    }

static eAtRet OamFlowDeActivate(AtHdlcLink self, AtEthFlow flow)
    {
    /* Get OAM flow controller managed by module Ethernet and let it does the job */
    ThaEthFlowController flowController = ThaModuleEthOamFlowControllerGet((ThaModuleEth)AtChannelModuleGet((AtChannel)flow));
	AtUnused(self);

    return ThaEthFlowControllerFlowDeActivate(flowController, flow);
    }

static uint8 OamModeSw2Hw(eAtHdlcLinkOamMode oamPacketMode)
    {
    if (oamPacketMode == cAtHdlcLinkOamModeToPsn)   return 0;
    if (oamPacketMode == cAtHdlcLinkOamModeToCpu)   return 1;
    if (oamPacketMode == cAtHdlcLinkOamModeDiscard) return 3;

    return cInvalidValue;
    }

static eAtRet OamPacketModeSet(AtHdlcLink self, eAtHdlcLinkOamMode oamPacketMode)
    {
    AtEthFlow oamFlow;
    uint32 address  = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, address, cThaModuleMpig);
    uint8 oamHwMode = OamModeSw2Hw(oamPacketMode);
    ThaPppLink pppLink = (ThaPppLink)self;
    eAtHdlcLinkOamMode oldOamPacketMode = AtHdlcLinkOamPacketModeGet(self);
    ThaModuleEncap encapModule = EncapModule(pppLink);
    uint32 mask, shift;

    mask  = mLinkMask(encapModule, NCPRule);
    shift = mLinkShift(encapModule, NCPRule);
    mFieldIns(&regValue, mask, shift, oamHwMode);

    mask  = mLinkMask(encapModule, LCPRule);
    shift = mLinkShift(encapModule, LCPRule);
    mFieldIns(&regValue, mask, shift, oamHwMode);

    mChannelHwWrite(self, address, regValue, cThaModuleMpig);

    /* If OAM flow if it is existing , activate it when OAM mode is to PSN,
     * de-activate it when OAM mode is to CPU */
    oamFlow = AtHdlcLinkOamFlowGet(self);
    if (oamFlow == NULL)
        return cAtOk;

    /* Need to activate OAM flow when there is a change from OAM CPU to OAM PSN */
    if ((oldOamPacketMode == cAtHdlcLinkOamModeToCpu) && (oamPacketMode == cAtHdlcLinkOamModeToPsn))
        {
        eAtRet ret = OamFlowActivate(self, oamFlow);
        if (ret != cAtOk)
            ret |= OamFlowDeActivate(self, oamFlow);

        return ret;
        }

    /* If OAM PSN mode is not used, need to deactivate its flow for hardware safe */
    if (oamPacketMode == cAtHdlcLinkOamModeToCpu)
        return OamFlowDeActivate(self, oamFlow);

    return cAtOk;
    }

static eAtHdlcLinkOamMode OamPacketModeGet(AtHdlcLink self)
    {
    uint32 regValue;
    uint8 value;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    regValue = mChannelHwRead(self, cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self), cThaModuleMpig);
    mFieldGet(regValue, mLinkMask(encapModule, NCPRule), mLinkShift(encapModule, NCPRule), uint8, &value);

    if (value == 0) return cAtHdlcLinkOamModeToPsn;
    if (value == 1) return cAtHdlcLinkOamModeToCpu;
    if (value == 3) return cAtHdlcLinkOamModeDiscard;

    return cAtHdlcLinkOamModeInvalid;
    }

static AtOsalSem OamLocker(ThaPppLink self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if (self->oamSemaphore == NULL)
        self->oamSemaphore = mMethodsGet(osal)->SemCreate(osal, cAtFalse, 1);
    return self->oamSemaphore;
    }

static eAtRet OamSendIndirect(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
    uint16  entryVal, wIndex;
    uint32  regValue, address;
    uint32  bitMask, numWrittenBytes;
    uint32  partOffset = ThaPppLinkPartOffset((ThaPppLink)self);
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);
    uint8   *pCurrentByte;

    address = cThaRegThalassaMLPppEgrOamCpuPktInformationReadOnly + partOffset;
    bitMask = cThaMPEGUpOamBusyMask;
    if(!ThaPppLinkHwIsReady((ThaPppLink)self, address, bitMask, cThaModuleMpeg))
        return cAtErrorDevBusy;

    /* Write packet data */
    pCurrentByte = buffer;
    for (entryVal = 0; entryVal < (bufferLength / 4); entryVal++)
        {
        regValue = 0;
        regValue = *pCurrentByte;
        pCurrentByte++;
        regValue <<= 8;
        regValue |= *pCurrentByte;
        pCurrentByte++;
        regValue <<= 8;
        regValue |= *pCurrentByte;
        pCurrentByte++;
        regValue <<= 8;
        regValue |= *pCurrentByte;
        pCurrentByte++;

        address = (uint32)(cThaRegThalassaMLPppEgrOamCpuPktDataCtrl + entryVal + partOffset);
        mChannelHwWrite(self, address, regValue, cThaModuleMpeg);
        }

    /* Write the remaining of packet */
    numWrittenBytes = (uint32)(pCurrentByte - buffer);
    if (numWrittenBytes < bufferLength)
        {
        regValue = 0;
        for (wIndex = 0; wIndex < 3; wIndex++)
            {
            numWrittenBytes = (uint32)(pCurrentByte - buffer);
            if (numWrittenBytes < bufferLength)
                {
                regValue |= *pCurrentByte;
                regValue <<= 8;
                pCurrentByte++;
                }
            else
                regValue <<= 8;
            }
        address = (uint32)(cThaRegThalassaMLPppEgrOamCpuPktDataCtrl + entryVal + partOffset);
        mChannelHwWrite(self, address, regValue,cThaModuleMpeg);
        }

    /* Write packet information */
    regValue = 0;
    mFieldIns(&regValue,
              mLinkMask(encapModule, MPEGUpOamPktLen),
              mLinkShift(encapModule, MPEGUpOamPktLen),
              bufferLength);
    mFieldIns(&regValue,
              mLinkMask(encapModule, MPEGUpOamLinkID),
              mLinkShift(encapModule, MPEGUpOamLinkID),
              AtChannelHwIdGet((AtChannel)self));

    mChannelHwWrite(self,
                    cThaRegThalassaMLPppEgrOamCpuPktInformationCtrl + partOffset,
                    regValue,
                    cThaModuleMpeg);

    address = cThaRegThalassaMLPppEgrOamCpuPktInformationReadOnly + partOffset;
    bitMask = cThaMPEGUpOamBusyMask;

    /* wait hardware finish */
    if (!ThaHwFinished((AtChannel)self, address, bitMask, cOamReadyTimeOut, cThaModuleMpeg))
        return cAtErrorIndrAcsTimeOut;

    /* Catch number packet user request */
    if (ThaPppLinkCanCheckOamsSent((ThaPppLink)self))
        {
        ThaPppLinkOamLock((ThaPppLink)self);
        ((ThaPppLink)self)->numOamsRequested = ((ThaPppLink)self)->numOamsRequested + 1;
        ThaPppLinkOamUnlock((ThaPppLink)self);
        }

    return cAtOk;
    }

static uint8 IpCoreIsReadyToSendOam(AtIpCore ipCore)
    {
    uint32 oamOffset, regVal;
    AtHal hal = AtIpCoreHalGet(ipCore);

    if (hal == NULL)
        return cAtFalse;

    oamOffset = mMethodsGet(hal)->DirectOamOffsetGet(hal);
    regVal    = AtHalRead(hal, cThaOamDirectWriteControl + oamOffset);
    return (regVal & cThaOamDirectWriteReadyMask) ? cAtTrue : cAtFalse;
    }

static eAtRet OamDataWriteToCore(AtIpCore ipCore, uint8 *buffer, uint32 oamLength, uint16 linkId)
    {
    uint32 regVal = 0;
    uint32 i, numWrittenBytes;
    uint8 *scanPntr = buffer;
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 oamOffset;
    static const uint16 cHwMaxOamPacketLength = 1023;

    if (hal == NULL)
        return cAtErrorNullPointer;

    if (oamLength > cHwMaxOamPacketLength)
        return cAtErrorOutOfRangParm;

    /* Start writing */
    oamOffset = mMethodsGet(hal)->DirectOamOffsetGet(hal);
    regVal = cThaOamDirectWriteOper;
    mFieldIns(&regVal, cThaOamDirectWriteLengthMask, cThaOamDirectWriteLengthShift, oamLength);
    AtHalWrite(hal, cThaOamDirectWriteControl + oamOffset, regVal);
    AtHalWrite(hal, cThaOamDirectWriteLinkId + oamOffset, linkId);

    /* Each time write 16 bits */
    for (i = 0; i < oamLength / 2; i++)
        {
        regVal = *scanPntr;
        regVal <<= 8;
        scanPntr++;

        regVal |= *scanPntr;
        scanPntr++;

        AtHalWrite(hal, cThaOamDirectWriteData + oamOffset, regVal);
        }

    /* Maybe remain one byte of buffer */
    numWrittenBytes = (uint32)(scanPntr - buffer);
    if (numWrittenBytes < oamLength)
        {
        regVal = *scanPntr;
        regVal <<= 8;
        AtHalWrite(hal, cThaOamDirectWriteData + oamOffset, regVal);
        }

    /* Stop writing */
    AtHalWrite(hal, cThaOamDirectWriteControl + oamOffset, cThaOamDirectWriteEnd);
    return cAtOk;
    }

static eAtRet OamSendDirect(AtHdlcLink self, uint8 *buffer, uint32 oamLength)
    {
    eAtRet ret;
    AtModule module = AtChannelModuleGet((AtChannel)self);
    AtIpCore ipCore = AtDeviceIpCoreGet(AtChannelDeviceGet((AtChannel)self), AtModuleDefaultCoreGet(module));

    if (ipCore == NULL)
        return cAtError;

    if (!IpCoreIsReadyToSendOam(ipCore))
        return cAtErrorDevBusy;

    /* Make request */
    ret = OamDataWriteToCore(ipCore, buffer, oamLength, (uint16)AtChannelHwIdGet((AtChannel)self));
    if ((ret == cAtOk) && ThaPppLinkCanCheckOamsSent((ThaPppLink)self))
        {
        ThaPppLinkOamLock((ThaPppLink)self);
        ((ThaPppLink)self)->numOamsRequested = ((ThaPppLink)self)->numOamsRequested + 1;
        ThaPppLinkOamUnlock((ThaPppLink)self);
        }

    return ret;
    }

static eAtRet OamSend(AtHdlcLink self, uint8 *buffer, uint32 oamLength)
    {
    AtModulePpp modulePpp = (AtModulePpp)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePpp);

    if (ThaModulePppOamModeIsDirect(modulePpp))
        return OamSendDirect(self, buffer, oamLength);

    return OamSendIndirect(self, buffer, oamLength);
    }

static uint32 OamReceive(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModulePpp modulePpp = (AtModulePpp)AtDeviceModuleGet(device, cAtModulePpp);
    AtModule mpigModule = AtDeviceModuleGet(device, cThaModuleMpig);
    AtIpCore ipCore = AtDeviceIpCoreGet(AtChannelDeviceGet((AtChannel)self), AtModuleDefaultCoreGet(mpigModule));
    uint16 linkId;

    if ((modulePpp == NULL) || (ipCore == NULL))
        return 0;

    /* Invalidate link ID first */
    linkId = AtModuleEncapMaxChannelsGet((AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap));
    return ThaModulePppReadOamOnCore(modulePpp, ipCore, buffer, bufferLength, &linkId);
    }

static eBool FlowIsValidToBindLink(AtHdlcLink self, AtEthFlow flow)
    {
    AtHdlcLink currentBoundLinkOfFlow = AtEthFlowHdlcLinkGet(flow);

    /* Flow is free */
    if (!mMethodsGet(flow)->IsBusy(flow))
        return cAtTrue;

    /* It is ok if this flow is already bound to this link */
    if (self == currentBoundLinkOfFlow)
        return cAtTrue;

    /* Otherwise, flow is busy with other purpose or other link */
    return cAtFalse;
    }

/* Traffic binding */
static eAtRet FlowBind(AtHdlcLink self, AtEthFlow flow)
    {
    eAtRet ret;
    AtEthFlow currentFlow;
    ThaPppLink thaPppLink = (ThaPppLink)self;

    /* Get current bound flow */
    currentFlow = AtHdlcLinkBoundFlowGet(self);
    if (currentFlow == flow)
        return cAtOk;

    /* Bind if flow is not NULL */
    if (flow)
        {
        ret = ThaHdlcLinkFlowCanBeBound(self, flow);
        if (ret != cAtOk)
            return ret;

        ret = PppFlowActivate(thaPppLink, flow);
        if (ret != cAtOk)
            ret |= PppFlowDeActivate(thaPppLink, flow);
        }

    /* Unbind */
    else
        {
        /* If this link is already free, do nothing */
        if (currentFlow == NULL)
            return cAtOk;

        /* Disable link traffic */
        ret  = AtHdlcLinkTxTrafficEnable(self, cAtFalse);
        ret |= AtHdlcLinkRxTrafficEnable(self, cAtFalse);

        /* And deactivate flow */
        ret |= PppFlowDeActivate(thaPppLink, currentFlow);
        }

    /* Update bound flow of this HDLC link if binding is successful */
    if (ret == cAtOk)
        ret = m_AtHdlcLinkMethods->FlowBind(self, flow);

    return ret;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    ThaPppLinkHwTxTrafficEnable((AtPppLink)self, enable);
    ((ThaPppLink)self)->txTrafficIsEnabled = enable;
    return cAtOk;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    return ((ThaPppLink)self)->txTrafficIsEnabled;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtEthFlow flow;

    /* Enable LCP/NCP packets */
    if (enable)
        ThaPppLinkPppPacketEnable((ThaPppLink)self, enable);

    /* Enable flow this is bound to this link */
    flow = AtHdlcLinkBoundFlowGet((AtHdlcLink)self);
    if (flow)
        return ThaEthFlowTxTrafficEnable((ThaEthFlow)flow, enable);

    return cAtOk;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    ThaEthFlow flow = (ThaEthFlow)AtHdlcLinkBoundFlowGet((AtHdlcLink)self);
    if (flow)
        return ThaEthFlowTxTrafficIsEnabled(flow);
    else
        return ThaPppLinkPppPacketIsEnabled((ThaPppLink)self);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    uint32 address, regValue;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    /* Enable link direct Ethernet to TDM  */
    address = cThaRegEgLinkScheduleCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaRegEgLinkScheduleMlpEnMasks,
              cThaRegEgLinkScheduleMlpEnShift,
              mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    address = cThaRegThalassaMLPppEgrPppLinkCtrl1 + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaMPEGPppLinkCtrl1MlpEnMasks,
              cThaMPEGPppLinkCtrl1MlpEnShift,
              mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    /* TDM to Ethernet */
    address = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regValue,
              mLinkMask(encapModule, MlpRuleRule),
              mLinkShift(encapModule, MlpRuleRule),
              (enable == cAtTrue) ? cForwardToPsn : cDiscard);
    if(enable == cAtTrue)
        mFieldIns(&regValue, mLinkMask(encapModule, PppRule), mLinkShift(encapModule, PppRule), 0x0);

    mChannelHwWrite(self, address, regValue, cThaModuleMpig);

    return cAtOk;
    }

static eAtRet LinkQueueEnable(ThaPppLink self, uint8 queueId, eBool enable)
    {
    uint32 address, regValue;

    address = cThaRegEgLinkScheduleCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaRegEgLinkScheduleMPEGLinkQueEnMask(queueId),
              cThaRegEgLinkScheduleMPEGLinkQueEnShift(queueId),
              mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    uint32 regValue = mChannelHwRead(self, cThaRegEgLinkScheduleCtrl + mDefaultOffset(self), cThaModuleMpeg);
    return (regValue & cThaRegEgLinkScheduleMlpEnMasks) ? cAtTrue : cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self,
                               uint32 defectMask,
                               uint32 enableMask)
    {
    uint32 address;

    address = cThaMPIGDATLinkAlarmIntrEnCtrl +
            mMethodsGet((ThaPppLink)self)->MpigLinkAlarmIntrEnCtrlOffset((ThaPppLink)self);
    if (enableMask == cAtTrue)
        mChannelHwWrite(self, address, defectMask, cThaModuleMpig);
    else
        mChannelHwWrite(self, address, 0, cThaModuleMpig);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 address, regValue;

    address = cThaMPIGDATLinkAlarmIntrStat
              + mMethodsGet((ThaPppLink)self)->MPIGDATLinkAlarmIntrStatOffset((ThaPppLink)self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);

    return (regValue & cBit15_0);
    }

static eAtRet EventListenerAdd(AtChannel self, tAtChannelEventListener *listener)
    {
    AtModule module;

    /* Let the super deal with database */
    eAtRet ret = m_AtChannelMethods->EventListenerAdd(self, listener);
    if (ret != cAtOk)
        return ret;

    /* Enable module interrupt if it has not */
    module = AtChannelModuleGet(self);
    if (module == NULL)
        return cAtErrorNullPointer;

    if (!AtModuleInterruptIsEnabled(module))
        return AtModuleInterruptEnable(module, cAtTrue);

    return cAtOk;
    }

static eAtRet CountersRxGet(AtChannel self, tAtPppLinkCounters *pCounter, eBool r2c)
    {
    uint32  address;
    uint32  offsetCount;

    /* Get Offset counter */
    offsetCount = (r2c == cAtTrue) ? 0 : cThaAddrOffsetCounterReadOnly;

    /* Get good byte count */
    address = cThaRegPmcMpigLinkGoodByte(offsetCount) + mDefaultOffset(self);
    pCounter->rxGoodByte = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get good packet count */
    address = cThaRegPmcMpigLinkGoodPkt(offsetCount) + mDefaultOffset(self);
    pCounter->rxGoodPkt = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get ppp packet count */
    address = cThaRegPmcMpigLinkPppPkt(offsetCount) + mDefaultOffset(self);
    pCounter->rxPlain = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get ppp packet count */
    address = cThaRegPmcPmcMpigLinkMlpppPkt(offsetCount) + mDefaultOffset(self);
    pCounter->rxFragment = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get oam packet count */
    address = cThaRegPmcMpigLinkOamPkt(offsetCount) + mDefaultOffset(self);
    pCounter->rxOam = mChannelHwRead(self, address, cThaModuleMpig);

    /* Get oam packet count */
    address = cThaRegPmcMpigLinkDiscardPkt(offsetCount) + mDefaultOffset(self);
    pCounter->rxDiscardPkt = mChannelHwRead(self, address, cThaModuleMpig);

    return cAtOk;
    }

/* Need to handle TX OAM by special way because this counter is now controlled by
 * the SDK to determine whether all of OAMs are sent or not. If application clear
 * this counter, it will affect the algorithm */
static uint32 TxOamCounterRead2Clear(ThaPppLink self, eBool r2c)
    {
    const uint32 cRead2ClearOffset = 0;
    uint32 address = cThaRegPmcMpegLinkOamPkt(cRead2ClearOffset) + mDefaultOffset(self);
    return ThaPppLinkTxOamCounterLatch(self, mChannelHwRead(self, address, cThaModuleMpeg), r2c);
    }

static eAtRet CountersTxGet(AtChannel self, tAtPppLinkCounters *pCounter, eBool r2c)
    {
    uint32 address;
    uint32 read2ClearOffset = r2c ? 0 : cThaAddrOffsetCounterReadOnly;

    /* Get good byte count */
    address = cThaRegPmcMpegLinkGoodByte(read2ClearOffset) + mDefaultOffset(self);
    pCounter->txGoodByte = mChannelHwRead(self, address, cThaModuleMpeg);

    /* Get good packet count */
    address = cThaRegPmcPmcMpegLinkGoodPkt(read2ClearOffset) + mDefaultOffset(self);
    pCounter->txGoodPkt = mChannelHwRead(self, address, cThaModuleMpeg);

    /* Get ppp packet count */
    address = cThaRegPmcMpegLinkPppPkt(read2ClearOffset) + mDefaultOffset(self);
    pCounter->txPlain = mChannelHwRead(self, address, cThaModuleMpeg);

    /* Get Mlppp packet count */
    address = cThaRegPmcMpegLinkMlpppPkt(read2ClearOffset) + mDefaultOffset(self);
    pCounter->txFragment = mChannelHwRead(self, address, cThaModuleMpeg);

    /* Get Discard packet count */
    address = cThaRegPmcMpegLinkDiscardPkt(read2ClearOffset) + mDefaultOffset(self);
    pCounter->txDiscardPkt = mChannelHwRead(self, address, cThaModuleMpeg);

    /* Get oam packet count */
    if (ThaPppLinkCanCheckOamsSent((ThaPppLink)self))
        {
        ThaPppLinkOamLock((ThaPppLink)self);
        pCounter->txOam = TxOamCounterRead2Clear((ThaPppLink)self, r2c);
        ThaPppLinkOamUnlock((ThaPppLink)self);
        }
    else
        {
        address = cThaRegPmcMpegLinkOamPkt(read2ClearOffset) + mDefaultOffset(self);
        pCounter->txOam = mChannelHwRead(self, address, cThaModuleMpeg);
        }

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* NULL counter structure has not been handled yet */
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    /* mpig */
    ret = CountersRxGet(self, (tAtPppLinkCounters*)pAllCounters, cAtFalse);
    if (ret != cAtOk)
        return cAtErrorDevFail;

    /* mpeg */
    ret = CountersTxGet(self, (tAtPppLinkCounters*)pAllCounters, cAtFalse);
    if (ret != cAtOk)
        return cAtErrorDevFail;

    return cAtOk;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    eAtRet ret;

    /* NULL counter structure has not been handled yet */
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    /* mpig */
    ret = CountersRxGet(self, (tAtPppLinkCounters*)pAllCounters, cAtTrue);
    if (ret != cAtOk)
        return cAtErrorDevFail;

    /* mpeg */
    ret = CountersTxGet(self, (tAtPppLinkCounters*)pAllCounters, cAtTrue);
    if (ret != cAtOk)
        return cAtErrorDevFail;

    ThaPppLinkDebugCountersClear((ThaPppLink)self);

    return cAtOk;
    }

static eAtRet PppPacketModeSet(ThaPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule(self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, mLinkMask(encapModule, PKtMode), mLinkShift(encapModule, PKtMode), 3);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    regAddr = cThaRegThalassaMLPppEgrPppLinkCtrl1 + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldIns(&regVal, cThaMPEGEncTypeMask, cThaMPEGEncTypeShift, 3);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eBool IsSimulated(AtPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceIsSimulated(device);
    }

static eAtPppLinkPhase PhaseGet(AtPppLink self)
    {
    uint32 address, regValue;
    uint8  phaseValue;

    if (IsSimulated(self))
        return SimulationDatabase(self)->phase;

    /* Change link state */
    address = cThaRegMPIGDATLookupLinkStateCtrl + ThaPppLinkPartOffset((ThaPppLink)self);
    if (!ThaPppLinkHwIsReady((ThaPppLink)self, address, cThaReqMask, cThaModuleMpig))
        return cAtPppLinkPhaseUnknown;

    regValue = 0;
    mFieldIns(&regValue, cThaRnwMask, cThaRnwShift, 1);
    mFieldIns(&regValue, cThaLinkIDMask, cThaLinkIDShift, ThaModuleEncapLocalPppLinkId((ThaPppLink)self));
    mFieldIns(&regValue, cThaReqMask, cThaReqShift, 1);
    mChannelHwWrite(self, address, regValue, cThaModuleMpig);

    if (!ThaHwFinished((AtChannel)self, address, cThaReqMask, cLinkStatChangeTimeOut, cThaModuleMpig))
        return cAtPppLinkPhaseUnknown;

    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldGet(regValue, cThaLinkStateMask, cThaLinkStateShift, uint8, &phaseValue);

    if (phaseValue == 0) return cAtPppLinkPhaseDead;
    if (phaseValue == 1) return cAtPppLinkPhaseEstablish;
    if (phaseValue == 2) return cAtPppLinkPhaseEnterNetwork;
    if (phaseValue == 3) return cAtPppLinkPhaseNetworkActive;

    return cAtPppLinkPhaseUnknown;
    }

static eAtRet HwPhaseSet(AtPppLink self, eAtPppLinkPhase phase)
    {
    eAtPppLinkPhase currentPhase;
    uint8 changeCmd;
    uint32 address, regValue;
    static const uint8 cCmdStop       = 0;
    static const uint8 cCmdLinkUp     = 1;
    static const uint8 cCmdLinkOpen   = 2;
    static const uint8 cCmdRun        = 3;
    static const uint8 cCmdEnable     = 4;
    static const uint8 cCmdLinkActive = 5;

    /* Do nothing if there is no change on phase */
    currentPhase  = PhaseGet(self);
    if (currentPhase == phase)
        return cAtOk;

    /* Check if hardware is ready */
    address = cThaRegMPIGDATLookupLinkStateCtrl + ThaPppLinkPartOffset((ThaPppLink)self);
    if (!ThaPppLinkHwIsReady((ThaPppLink)self, address, cThaReqMask, cThaModuleMpig))
        return cAtErrorDevBusy;

    changeCmd = cCmdStop;
    switch (phase)
        {
        case cAtPppLinkPhaseDead:
            changeCmd = cCmdStop;
            break;

        case cAtPppLinkPhaseEstablish:
            if (currentPhase != cAtPppLinkPhaseDead)
                HwPhaseSet(self, cAtPppLinkPhaseDead);
            changeCmd = cCmdLinkUp;
            break;

        case cAtPppLinkPhaseEnterNetwork:
            if (currentPhase != cAtPppLinkPhaseEnterNetwork)
                {
                HwPhaseSet(self,cAtPppLinkPhaseDead);
                HwPhaseSet(self, cAtPppLinkPhaseEstablish);
                }
            changeCmd = cCmdLinkOpen;
            break;

        case cAtPppLinkPhaseNetworkActive:
            if (currentPhase == cAtPppLinkPhaseDead)
                changeCmd = cCmdEnable;
            else if (currentPhase == cAtPppLinkPhaseEstablish)
                changeCmd = cCmdLinkActive;
            else if (currentPhase == cAtPppLinkPhaseEnterNetwork)
                changeCmd = cCmdRun;
            else
                {
                HwPhaseSet(self, cAtPppLinkPhaseDead);
                changeCmd = cCmdEnable;
                }

            break;

        case cAtPppLinkPhaseUnknown:
        case cAtPppLinkPhaseAuthenticate:
        default:
            return cAtErrorModeNotSupport;
        }

    regValue = 0;
    mFieldIns(&regValue, cThaRnwMask, cThaRnwShift, 0);
    mFieldIns(&regValue, cThaCmdMask, cThaCmdShift, changeCmd);
    mFieldIns(&regValue, cThaLinkIDMask, cThaLinkIDShift, ThaModuleEncapLocalPppLinkId((ThaPppLink)self));
    mFieldIns(&regValue, cThaReqMask, cThaReqShift, 1);
    mChannelHwWrite(self, address, regValue, cThaModuleMpig);

    if (!ThaHwFinished((AtChannel)self, address, cThaReqMask, cLinkStatChangeTimeOut, cThaModuleMpig))
        return cAtErrorIndrAcsTimeOut;

    return cAtOk;
    }

static eAtModulePppRet PhaseSet(AtPppLink self, eAtPppLinkPhase phase)
    {
    uint8 i;
    static const uint8 cPppLinkPhaseRetryTimes = 10;

    if (IsSimulated(self))
        {
        SimulationDatabase(self)->phase = phase;
        return cAtOk;
        }

    for (i = 0; i < cPppLinkPhaseRetryTimes; i++)
        {
        HwPhaseSet(self, phase);
        if (AtPppLinkPhaseGet(self) == phase)
            return cAtOk;
        }

    return cAtErrorDevFail;
    }

static void PidTableDelete(ThaPppLink self)
    {
    AtObjectDelete((AtObject)(self->pidTable));
    self->pidTable = NULL;
    }

static void Delete(AtObject self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (link->oamSemaphore)
        AtOsalSemDestroy(link->oamSemaphore);
    link->oamSemaphore = NULL;

    PidTableDelete(link);
    SimulationDatabaseDelete((AtPppLink)self);

    m_AtObjectMethods->Delete(self);
    }

static void OamTxEnable(ThaPppLink self, eBool enable)
    {
    uint32 regAddr, regValue;

    regAddr  = cThaRegEgLinkScheduleCtrl + ThaPppLinkOffset(self);
    regValue = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldIns(&regValue, cThaMPEGTrafficOAMEnMask, cThaMPEGTrafficOAMEnShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regValue, cThaModuleMpeg);
    }

static eBool PidIsByPass(ThaPppLink self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet AddressControlDefaultSet(AtHdlcLink self)
    {
    uint8 address = 0xFF;
    uint8 control = 0x03;
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet(self);

    AtHdlcChannelTxAddressSet(hdlcChannel, &address);
    AtHdlcChannelTxControlSet(hdlcChannel, &control);
    AtHdlcChannelAddressInsertionEnable(hdlcChannel, cAtTrue);
    AtHdlcChannelControlInsertionEnable(hdlcChannel, cAtTrue);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtPppLink pppLink   = (AtPppLink)self;
    ThaPppLink thaLink  = (ThaPppLink)self;
    AtHdlcLink hdlcLink = (AtHdlcLink)self;
    AtHdlcChannel hdlcChannel = NULL;
    eBool pidByPass;
    uint8 queue_i;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Additional initialization */
    ret |= MlppHdrByPass(hdlcLink, cAtFalse);
    ret |= AddressControlDefaultSet(hdlcLink);
    ret |= ThaPppLinkAddressControlCompress(pppLink, cAtFalse);

    for (queue_i = 0; queue_i < ThaPppLinkMaxNumQueues(thaLink); queue_i++)
        ret |= LinkQueueEnable(thaLink, queue_i, cAtTrue);

    ret |= TxTrafficEnable(self, cAtFalse);
    ret |= RxTrafficEnable(self, cAtFalse);

    /* FCS checking */
    hdlcChannel = AtHdlcLinkHdlcChannelGet(hdlcLink);
    if (AtHdlcChannelFcsModeGet(hdlcChannel) == cAtHdlcFcsModeNoFcs)
        ret |= ThaPppLinkFcsErrorBypass(pppLink, cAtTrue);
    else
        ret |= ThaPppLinkFcsErrorBypass(pppLink, cAtFalse);

    /* Address/control checking */
    if (AtHdlcChannelAddressInsertionIsEnabled(hdlcChannel))
        ret |= ThaPppLinkAddressControlBypass(pppLink, cAtFalse);
    else
        ret |= ThaPppLinkAddressControlBypass(pppLink, cAtTrue);

    pidByPass = mMethodsGet(thaLink)->PidIsByPass(thaLink);
    ret |= mMethodsGet(thaLink)->PppPacketModeSet(thaLink);
    ret |= mMethodsGet(hdlcLink)->PidByPass(hdlcLink, pidByPass);

    OamTxEnable(thaLink, cAtTrue);

    /* Init member id */
    ret |= ThaPppLinkMemberLocalIdInBundleSet(thaLink , 0x1F);

    /* Reset private database */
    thaLink->numOamsRequested = 0;
    thaLink->numOamsSent      = 0;
    thaLink->txOamCounter     = 0;

    /* Disable PFC as default, ignore error code because not all of products support this */
    AtPppLinkTxProtocolFieldCompress(pppLink, cAtFalse);

    return ret;
    }

static AtModuleEth EthModule(AtHdlcLink self)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    }

static AtEthFlow OamFlowCreate(AtHdlcLink self)
    {
    return AtHdlcLinkOamFlowCreate(self);
    }

static void OamFlowDelete(AtHdlcLink self)
    {
    AtHdlcLinkOamFlowDelete(self);
    }

/* Get PPP module from a channel. Note, we cannot say PPP link belongs to PPP module
 * link belong to HDLC channel which is created by Encap module. So Link object
 * will belong to Encap module, not PPP module. The PPP module just use PPP link
 * as its input, it has no right to create a link */
static ThaModulePpp PppModuleFromChannel(AtChannel channel)
    {
    return (ThaModulePpp)AtDeviceModuleGet(AtChannelDeviceGet(channel), cAtModulePpp);
    }

static eBool AllOamsSent(AtHdlcLink self)
    {
    ThaPppLink pppLink = (ThaPppLink)self;
    eBool sent;

    if (!ThaPppLinkCanCheckOamsSent(pppLink))
        return m_AtHdlcLinkMethods->AllOamsSent(self);

    /* If all of OAMs are sent, need to reset database for next detection */
    ThaPppLinkOamLock(pppLink);
    sent = (pppLink->numOamsSent == pppLink->numOamsRequested) ? cAtTrue : cAtFalse;
    if (sent)
        {
        pppLink->numOamsRequested = 0;
        pppLink->numOamsSent      = 0;
        }
    ThaPppLinkOamUnlock(pppLink);

    return sent;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return ThaModuleEncapLocalPppLinkId((ThaPppLink)self);
    }

static AtPidTable PidTableObjectCreate(ThaPppLink self)
    {
	AtUnused(self);
    return NULL;
    }

static AtPidTable PidTableGet(AtPppLink self)
    {
    ThaPppLink pppLink = (ThaPppLink)self;
    AtPidTable newTable;

    /* Already create */
    if (pppLink->pidTable)
        return pppLink->pidTable;

    /* Just create one. Note, not all of products can support PID table for each
     * link, so NULL object should not be checked */
    newTable = mMethodsGet(pppLink)->PidTableObjectCreate(pppLink);
    if (newTable)
        AtPidTableInit(newTable);
    pppLink->pidTable = newTable;

    return pppLink->pidTable;
    }

static eAtModulePppRet TxProtocolFieldCompress(AtPppLink self, eBool compress)
    {
    uint32 regAddr = cThaRegEgressCompressControl + ThaPppLinkOffset((ThaPppLink)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mRegFieldSet(regVal, cThaRegMlpppMPEGLinkPFCEn, compress ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eBool TxProtocolFieldIsCompressed(AtPppLink self)
    {
    uint32 regAddr = cThaRegEgressCompressControl + ThaPppLinkOffset((ThaPppLink)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    return (regVal & cThaRegMlpppMPEGLinkPFCEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PppPacketEnable(ThaPppLink self, eBool enable)
    {
    uint32 address, regValue;
    ThaModuleEncap encapModule;

    encapModule = EncapModule(self);
    address  = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regValue, mLinkMask(encapModule, PppRule), mLinkShift(encapModule, PppRule), enable ? cForwardToPsn : cDiscard);
    mChannelHwWrite(self, address, regValue, cThaModuleMpig);

    return cAtOk;
    }

static eBool PppPacketIsEnabled(ThaPppLink self)
    {
    uint32 address, regValue;
    ThaModuleEncap encapModule;
    uint8 enVal;

    encapModule = EncapModule(self);
    address  = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldGet(regValue, mLinkMask(encapModule, PppRule), mLinkShift(encapModule, PppRule), uint8, &enVal);
    return (enVal == cForwardToPsn) ? cAtTrue : cAtFalse;
    }

static eAtRet IgMlpppEnable(ThaPppLink self, eBool enable)
    {
    uint32 address, regVal;

    /* Enable link direct Ethernet to TDM  */
    address = cThaRegEgLinkScheduleCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regVal,
              cThaRegEgLinkScheduleMlpEnMasks,
              cThaRegEgLinkScheduleMlpEnShift,
              mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModuleMpeg);

    address = cThaRegThalassaMLPppEgrPppLinkCtrl1 + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regVal,
              cThaMPEGPppLinkCtrl1MlpEnMasks,
              cThaMPEGPppLinkCtrl1MlpEnShift,
              mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eAtRet EgMlpppEnable(ThaPppLink self, eBool enable)
    {
    uint32 address, regVal;
    uint8 mlpRule = enable ? cForwardToPsn : cDiscard;
    ThaModuleEncap encapModule = EncapModule(self);

    address = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regVal, mLinkMask(encapModule, MlpRuleRule), mLinkShift(encapModule, MlpRuleRule), mlpRule);
    if (enable)
       mFieldIns(&regVal, mLinkMask(encapModule, PppRule), mLinkShift(encapModule, PppRule), cForwardToPsn);

    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    return cAtOk;
    }

static eAtRet JoinBundle(ThaPppLink self, ThaMpBundle bundle)
    {
    uint32 address, regVal, offset;
    uint32 bundleId = ThaModulePppLocalBundleId(bundle);
    ThaModuleEncap encapModule = EncapModule(self);

    /* Set bundle ID at MPEG */
    offset = mDefaultOffset(self);
    address = cThaRegThalassaMLPppEgrPppLinkCtrl1 + offset;
    regVal = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regVal,
              mLinkMask(encapModule, MPEGPppLinkCtrl1BundleID),
              mLinkShift(encapModule, MPEGPppLinkCtrl1BundleID),
              bundleId);
    mChannelHwWrite(self, address, regVal, cThaModuleMpeg);

    /* Set bundle ID at MPIG */
    address = cThaRegMPIGDATLookupLinkCtrl + offset;
    regVal = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regVal, mLinkMask(encapModule, BndID), mLinkShift(encapModule, BndID), bundleId);
    mChannelHwWrite(self, address, regVal, cThaModuleMpig);

    return cAtOk;
    }

static eAtRet MemberLocalIdInBundleSet(ThaPppLink self , uint16 localMemberId)
    {
    uint32 address, regValue;

    if (self == NULL)
        return cAtErrorNullPointer;

    address  = cThaRegThalassaMLPppEgrPppLinkCtrl1 + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regValue,cThaMPEGMemIdMask, cThaMPEGMemIdShift, localMemberId);
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);

    return cAtOk;
    }

static void SerializeSimulation(AtObject simulation, AtCoder encoder)
    {
    tThaPppLinkSimulation *object = (tThaPppLinkSimulation *)simulation;
    mEncodeUInt(phase);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPppLink object = (ThaPppLink)(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(txTrafficIsEnabled);
    mEncodeObjectDescription(pidTable);
    AtCoderEncodeObjectWithHandler(encoder, (AtObjectAny)&(object->simulation), "simulation", SerializeSimulation);

    mEncodeNone(lcpCounters);
    mEncodeNone(ipcpCounters);
    mEncodeNone(ipv6cpCounters);
    mEncodeNone(unknownPidCounters);
    mEncodeNone(otherOamCounters);
    mEncodeNone(oamSemaphore);
    mEncodeNone(numOamsRequested);
    mEncodeNone(numOamsSent);
    mEncodeNone(txOamCounter);
    }

static void OverrideAtPppLink(ThaPppLink self)
    {
    AtPppLink link = (AtPppLink)self;

    if (!m_methodsInit)
        {
        /* Reuse all super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, mMethodsGet(link), sizeof(m_AtPppLinkOverride));

        /* Override */
        mMethodOverride(m_AtPppLinkOverride, PhaseSet);
        mMethodOverride(m_AtPppLinkOverride, PhaseGet);
        mMethodOverride(m_AtPppLinkOverride, PidTableGet);
        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldCompress);
        mMethodOverride(m_AtPppLinkOverride, TxProtocolFieldIsCompressed);
        }

    mMethodsSet(link, &m_AtPppLinkOverride);
    }

static void OverrideAtChannel(ThaPppLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, EventListenerAdd);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcLink(ThaPppLink self)
    {
    AtHdlcLink link = (AtHdlcLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcLinkMethods = mMethodsGet(link);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(link), sizeof(m_AtHdlcLinkOverride));
        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeSet);
        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeGet);
        mMethodOverride(m_AtHdlcLinkOverride, FlowBind);
        mMethodOverride(m_AtHdlcLinkOverride, OamSend);
        mMethodOverride(m_AtHdlcLinkOverride, OamReceive);
        mMethodOverride(m_AtHdlcLinkOverride, OamFlowCreate);
        mMethodOverride(m_AtHdlcLinkOverride, AllOamsSent);
        mMethodOverride(m_AtHdlcLinkOverride, OamFlowDelete);
        mMethodOverride(m_AtHdlcLinkOverride, PidByPass);
        }

    mMethodsSet(link, &m_AtHdlcLinkOverride);
    }

static void OverrideAtObject(ThaPppLink self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPppLink self)
    {
    OverrideAtHdlcLink(self);
    OverrideAtChannel(self);
    OverrideAtPppLink(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaPppLink self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, DefaultBLockOffset);
        mMethodOverride(m_methods, IntrDefaultOffset);
        mMethodOverride(m_methods, QueueDefaultOffset);
        mMethodOverride(m_methods, PppPacketModeSet);
        mMethodOverride(m_methods, EthFlowControllerGet);
        mMethodOverride(m_methods, PidIsByPass);
        mMethodOverride(m_methods, PidTableObjectCreate);
        mMethodOverride(m_methods, PppPacketEnable);
        mMethodOverride(m_methods, PppPacketIsEnabled);
        mMethodOverride(m_methods, IgMlpppEnable);
        mMethodOverride(m_methods, EgMlpppEnable);
        mMethodOverride(m_methods, JoinBundle);
        mMethodOverride(m_methods, MemberLocalIdInBundleSet);
        }

    mMethodsSet(self, &m_methods);
    }

AtPppLink ThaPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPppLink));

    /* Super constructor */
    if (AtPppLinkObjectInit((AtPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Override */
    Override((ThaPppLink)self);

    /* Initialize implementation */
    MethodsInit((ThaPppLink)self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtPppLink ThaPppLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPppLink));
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ThaPppLinkObjectInit(newLink, hdlcChannel);
    }

eBool ThaPppLinkHwIsReady(ThaPppLink self, uint32 address, uint32 bitMask, eAtModule moduleId)
    {
    uint32  regValue;

    regValue = mChannelHwRead(self, address, moduleId);
    if ((regValue & bitMask) != 0)
        return cAtFalse;

    return cAtTrue;
    }

eBool ThaHwFinished(AtChannel self, uint32 address, uint32 bitMask, uint32 timeOut, eAtModule moduleId)
    {
    uint32 regValue;
    uint32 elapseTime;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Hardware finish its job */
    regValue = mChannelHwRead(self, address, moduleId);
    if ((regValue & bitMask) == 0)
        return cAtTrue;

    /* Timeout waiting for it */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < timeOut)
        {
        regValue = mChannelHwRead(self, address, moduleId);
        if ((regValue & bitMask) == 0)
            return cAtTrue;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

eAtRet ThaPppLinkIgMlpppEnable(ThaPppLink self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->IgMlpppEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaPppLinkEgMlpppEnable(ThaPppLink self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->EgMlpppEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaPppLinkJoinBundle(ThaPppLink self, ThaMpBundle bundle)
    {
    if (self)
        return mMethodsGet(self)->JoinBundle(self, bundle);

    return cAtErrorNullPointer;
    }

static void NumBlockSet(ThaPppLink self, uint16 numBlocks)
    {
    uint32 regValue, address;
    ThaModuleEncap encapModule = EncapModule(self);

    address = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regValue, mLinkMask(encapModule, BlkAloc), mLinkShift(encapModule, BlkAloc), numBlocks);
    mChannelHwWrite(self, address, regValue, cThaModuleMpig);
    }

static uint8 LinkPartId(ThaPppLink self)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    return ThaHdlcChannelPartOfEncap(EncapModule(self), channel);
    }

static eAtRet HighBandwidthAllocate(ThaPppLink self, uint16 numBlocks)
    {
    uint32 regValue, address;
    uint8 i;
    ThaModulePpp pppModule = PppModuleFromChannel((AtChannel)self);
    uint8 partId = LinkPartId(self);
    ThaModuleEncap encapModule = EncapModule(self);

    if (ThaModulePppHighBandwidthNumFreeBlocks(pppModule, partId) < numBlocks)
        return cAtErrorRsrcNoAvail;

    NumBlockSet(self, numBlocks);

    /* De-queue link control */
    address = cThaRegMPIGDeQueueLinkCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regValue, cThaRegMPIGDeQueueHighBwCtrlMask, cThaRegMPIGDeQueueHighBwCtrlShift, 1);
    mFieldIns(&regValue, cThaRegMPIGDeQueueLinkCtrlMask, cThaRegMPIGDeQueueLinkCtrlShift, numBlocks);
    mChannelHwWrite(self, address, regValue, cThaModuleMpig);

    /* MPIG DAT EMI Link Buffer Pool Address */
    for (i = 0; i < numBlocks + 1; i++)
        {
        uint32 freeBlockId;
        uint8 hwFreeBlockId;

        address = cThaRegMPIGDatEmiLinkBufPoolAddr + mDefaultBlockOffset(self, i);
        freeBlockId = ThaModulePppHighBandwidthFreeBlockGet(pppModule, partId);
        ThaModulePppHighBandwidthBlockUse(pppModule, partId, freeBlockId);

        regValue = 0;
        hwFreeBlockId = freeBlockId % 16;
        mFieldIns(&regValue, mLinkMask(encapModule, MPIGDatEmiLbpBlkID), mLinkShift(encapModule, MPIGDatEmiLbpBlkID), hwFreeBlockId);
        mFieldIns(&regValue, mLinkMask(encapModule, MPIGDatEmiLbpBlkVl), mLinkShift(encapModule, MPIGDatEmiLbpBlkVl), 1);
        mChannelHwWrite(self, address, regValue, cThaModuleMpig);
        }

    /* Set remaining blocks to unused */
    for (; i < cThaMaxBlkPerLink; i++)
        {
        address = cThaRegMPIGDatEmiLinkBufPoolAddr + mDefaultBlockOffset(self, i);
        mChannelHwWrite(self, address, 0, cThaModuleMpig);
        }

    return cAtOk;
    }

static eAtRet LowBandwidthAllocate(ThaPppLink self, uint16 numBlocks)
    {
    uint32 regValue, address;
    uint8 i;
    uint16 hwNumBlocks = (uint16)(numBlocks - 1);
    ThaModulePpp pppModule = PppModuleFromChannel((AtChannel)self);
    uint8 partId = LinkPartId(self);
    ThaModuleEncap encapModule = EncapModule(self);

    if (ThaModulePppNumFreeBlocks(pppModule, partId) < numBlocks)
        return cAtErrorRsrcNoAvail;

    NumBlockSet(self, hwNumBlocks);

    /* De-queue link control */
    address = cThaRegMPIGDeQueueLinkCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpig);
    mFieldIns(&regValue, cThaRegMPIGDeQueueHighBwCtrlMask, cThaRegMPIGDeQueueHighBwCtrlShift, 0);
    mFieldIns(&regValue, cThaRegMPIGDeQueueLinkCtrlMask, cThaRegMPIGDeQueueLinkCtrlShift, hwNumBlocks);
    mChannelHwWrite(self, address, regValue, cThaModuleMpig);

    /* MPIG DAT EMI Link Buffer Pool Address */
    for(i = 0; i < numBlocks; i++)
        {
        uint32 freeBlockId;

        address = cThaRegMPIGDatEmiLinkBufPoolAddr + mDefaultBlockOffset(self, i);
        freeBlockId = ThaModulePppFreeBlockGet(pppModule, partId);
        ThaModulePppBlockUse(pppModule, partId, freeBlockId);

        regValue = 0;
        mFieldIns(&regValue, mLinkMask(encapModule, MPIGDatEmiLbpBlkID), mLinkShift(encapModule, MPIGDatEmiLbpBlkID), freeBlockId);
        mFieldIns(&regValue, mLinkMask(encapModule, MPIGDatEmiLbpBlkVl), mLinkShift(encapModule, MPIGDatEmiLbpBlkVl), 1);
        mChannelHwWrite(self, address, regValue, cThaModuleMpig);
        }

    /* Set remaining blocks to unused */
    for (; i < cThaMaxBlkPerLink; i++)
        {
        address = cThaRegMPIGDatEmiLinkBufPoolAddr + mDefaultBlockOffset(self, i);
        mChannelHwWrite(self, address, 0, cThaModuleMpig);
        }

    return cAtOk;
    }

eAtRet ThaPppLinkBlockAllocate(AtPppLink self, uint16 numBlocks, eBool isHighBandwidth)
    {
    ThaPppLink pppLink = (ThaPppLink)self;
    eAtRet ret;

    /* If High bandwidth link DS3/E3 */
    if (isHighBandwidth)
        ret = HighBandwidthAllocate(pppLink, numBlocks);

    /* Low bandwidth link DS1/E1 */
    else
        ret = LowBandwidthAllocate(pppLink, numBlocks);

    return ret;
    }

eAtRet ThaPppLinkBlockDeAllocate(AtPppLink self, uint16 numBlocks, eBool isHighBandwidth)
    {
    uint32 address, regValue;
    uint16 blockId;
    uint8  i, partId = LinkPartId((ThaPppLink)self);
    ThaModulePpp pppModule = PppModuleFromChannel((AtChannel)self);
    eAtRet (*PppBlockUnuse)(ThaModulePpp, uint8, uint32) = ThaModulePppBlockUnuse;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);
    uint16 startBlock = 0;
	AtUnused(numBlocks);

    if (isHighBandwidth)
        {
        startBlock = (uint16)(partId * cThaMaxBlkPerLink);
        PppBlockUnuse = ThaModulePppHighBandwidthBlockUnuse;
        }

    /* MPIG DAT EMI Link Buffer Pool Address */
    for(i = 0; i < cThaMaxBlkPerLink; i++)
        {
        address = cThaRegMPIGDatEmiLinkBufPoolAddr + mDefaultBlockOffset(self, i);
        regValue = mChannelHwRead(self, address, cThaModuleMpig);

        if ((regValue & mLinkMask(encapModule, MPIGDatEmiLbpBlkVl)) == 0)
            continue;

        mFieldGet(regValue, mLinkMask(encapModule, MPIGDatEmiLbpBlkID), mLinkShift(encapModule, MPIGDatEmiLbpBlkID), uint16, &blockId);
        mChannelHwWrite(self, address, 0, cThaModuleMpig);

        /* Release this block */
        PppBlockUnuse(pppModule, partId, (uint32)(blockId + startBlock));
        }

    return cAtOk;
    }

static const char *Pid2String(uint16 pid)
    {
    switch (pid)
        {
        case 0x0001: return "Padding Protocol";
        case 0x0003: return "ROHC small-CID";
        case 0x0005: return "ROHC large-CID";
        case 0x0021: return "Internet Protocol version 4";
        case 0x0023: return "OSI Network Layer";
        case 0x0025: return "Xerox NS IDP";
        case 0x0027: return "DECnet Phase IV";
        case 0x0029: return "Appletalk";
        case 0x002b: return "Novell IPX";
        case 0x002d: return "Van Jacobson Compressed TCP/IP";
        case 0x002f: return "Van Jacobson Uncompressed TCP/IP";
        case 0x0031: return "Bridging PDU";
        case 0x0033: return "Stream Protocol";
        case 0x0035: return "Banyan Vines";
        case 0x0039: return "AppleTalk EDDP";
        case 0x003b: return "AppleTalk SmartBuffered";
        case 0x003d: return "Multi-Link";
        case 0x003f: return "NETBIOS Framing";
        case 0x0041: return "Cisco Systems";
        case 0x0043: return "Ascom Timeplex";
        case 0x0045: return "Fujitsu Link Backup and Load Balancing (LBLB)";
        case 0x0047: return "DCA Remote Lan";
        case 0x0049: return "Serial Data Transport Protocol (PPP-SDTP)";
        case 0x004b: return "SNA over 802.2";
        case 0x004d: return "SNA";
        case 0x004f: return "IPv6 Header Compression";
        case 0x0051: return "KNX Bridging Data";
        case 0x0053: return "Encryption";
        case 0x0055: return "Individual Link Encryption";
        case 0x0057: return "Internet Protocol version 6";
        case 0x0059: return "PPP Muxing";
        case 0x005b: return "Vendor-Specific Network Protocol (VSNP)";
        case 0x005d: return "TRILL Network Protocol (TNP)";
        case 0x0061: return "RTP IPHC Full Header";
        case 0x0063: return "RTP IPHC Compressed TCP";
        case 0x0065: return "RTP IPHC Compressed Non TCP";
        case 0x0067: return "RTP IPHC Compressed UDP 8";
        case 0x0069: return "RTP IPHC Compressed RTP 8";
        case 0x006f: return "Stampede Bridging";
        case 0x0073: return "MP+ Protocol";
        case 0x007d: return "Reserved (Control Escape)";
        case 0x007f: return "Reserved (compression inefficient)";
        case 0x00c1: return "NTCITS IPI";
        case 0x00cf: return "Reserved (PPP NLPID)";
        case 0x00fb: return "Single link compression in multilink";
        case 0x00fd: return "Compressed datagram";
        case 0x00ff: return "Reserved (compression inefficient)";
        case 0x0201: return "802.1d Hello Packets";
        case 0x0203: return "IBM Source Routing BPDU";
        case 0x0205: return "DEC LANBridge100 Spanning Tree";
        case 0x0207: return "Cisco Discovery Protocol";
        case 0x0209: return "Netcs Twin Routing";
        case 0x020b: return "STP - Scheduled Transfer Protocol";
        case 0x020d: return "EDP - Extreme Discovery Protocol";
        case 0x0211: return "Optical Supervisory Channel Protocol (OSCP)";
        case 0x0213: return "Optical Supervisory Channel Protocol (OSCP)";
        case 0x0231: return "Luxcom";
        case 0x0233: return "Sigma Network Systems";
        case 0x0235: return "Apple Client Server Protocol";
        case 0x0281: return "MPLS Unicast";
        case 0x0283: return "MPLS Multicast";
        case 0x0285: return "IEEE p1284.4 standard - data packets";
        case 0x0287: return "ETSI TETRA Network Protocol Type 1";
        case 0x0289: return "Multichannel Flow Treatment Protocol";
        case 0x2063: return "RTP IPHC Compressed TCP No Delta";
        case 0x2065: return "RTP IPHC Context State";
        case 0x2067: return "RTP IPHC Compressed UDP 16";
        case 0x2069: return "RTP IPHC Compressed RTP 16";
        case 0x4001: return "Cray Communications Control Protocol";
        case 0x4003: return "CDPD Mobile Network Registration Protocol";
        case 0x4005: return "Expand accelerator protocol";
        case 0x4007: return "ODSICP NCP";
        case 0x4009: return "DOCSIS DLL";
        case 0x400B: return "Cetacean Network Detection Protocol";
        case 0x4021: return "Stacker LZS";
        case 0x4023: return "RefTek Protocol";
        case 0x4025: return "Fibre Channel";
        case 0x4027: return "EMIT Protocols";
        case 0x405b: return "Vendor-Specific Protocol (VSP)";
        case 0x405d: return "TRILL Link State Protocol (TLSP)";
        case 0x8021: return "Internet Protocol Control Protocol";
        case 0x8023: return "OSI Network Layer Control Protocol";
        case 0x8025: return "Xerox NS IDP Control Protocol";
        case 0x8027: return "DECnet Phase IV Control Protocol";
        case 0x8029: return "Appletalk Control Protocol";
        case 0x802b: return "Novell IPX Control Protocol";
        case 0x8031: return "Bridging NCP";
        case 0x8033: return "Stream Protocol Control Protocol";
        case 0x8035: return "Banyan Vines Control Protocol";
        case 0x803d: return "Multi-Link Control Protocol";
        case 0x803f: return "NETBIOS Framing Control Protocol";
        case 0x8041: return "Cisco Systems Control Protocol";
        case 0x8043: return "Ascom Timeplex";
        case 0x8045: return "Fujitsu LBLB Control Protocol";
        case 0x8047: return "DCA Remote Lan Network Control Protocol (RLNCP)";
        case 0x8049: return "Serial Data Control Protocol (PPP-SDCP)";
        case 0x804b: return "SNA over 802.2 Control Protocol";
        case 0x804d: return "SNA Control Protocol";
        case 0x804f: return "IP6 Header Compression Control Protocol";
        case 0x8051: return "KNX Bridging Control Protocol";
        case 0x8053: return "Encryption Control Protocol";
        case 0x8055: return "Individual Link Encryption Control Protocol";
        case 0x8057: return "IPv6 Control Protocol";
        case 0x8059: return "PPP Muxing Control Protocol";
        case 0x805b: return "Vendor-Specific Network Control Protocol (VSNCP)";
        case 0x805d: return "TRILL Network Control Protocol (TNCP)";
        case 0x806f: return "Stampede Bridging Control Protocol";
        case 0x8073: return "MP+ Control Protocol";
        case 0x80c1: return "NTCITS IPI Control Protocol";
        case 0x80fb: return "single link compression in multilink control";
        case 0x80fd: return "Compression Control Protocol";
        case 0x8207: return "Cisco Discovery Protocol Control";
        case 0x8209: return "Netcs Twin Routing";
        case 0x820b: return "STP - Control Protocol";
        case 0x820d: return "EDPCP - Extreme Discovery Protocol Ctrl Prtcl";
        case 0x8235: return "Apple Client Server Protocol Control";
        case 0x8281: return "MPLSCP";
        case 0x8285: return "IEEE p1284.4 standard - Protocol Control";
        case 0x8287: return "ETSI TETRA TNP1 Control Protocol";
        case 0x8289: return "Multichannel Flow Treatment Protocol";
        case 0xc021: return "Link Control Protocol";
        case 0xc023: return "Password Authentication Protocol";
        case 0xc025: return "Link Quality Report";
        case 0xc027: return "Shiva Password Authentication Protocol";
        case 0xc029: return "CallBack Control Protocol (CBCP)";
        case 0xc02b: return "BACP Bandwidth Allocation Control Protocol";
        case 0xc02d: return "BAP";
        case 0xc05b: return "Vendor-Specific Authentication Protocol (VSAP)";
        case 0xc081: return "Container Control Protocol";
        case 0xc223: return "Challenge Handshake Authentication Protocol";
        case 0xc225: return "RSA Authentication Protocol";
        case 0xc227: return "Extensible Authentication Protocol";
        case 0xc229: return "Mitsubishi Security Info Exch Ptcl (SIEP)";
        case 0xc26f: return "Stampede Bridging Authorization Protocol";
        case 0xc281: return "Proprietary Authentication Protocol";
        case 0xc283: return "Proprietary Authentication Protocol";
        case 0xc481: return "Proprietary Node ID Authentication Protocol";
        default    : return NULL;
        }
    }

void ThaPppLinkOamCountByPid(ThaPppLink self, uint16 pid)
    {
    if (self == NULL)
        return;

    switch (pid)
        {
        case 0xc021:
            self->lcpCounters++;
            return;

        case 0x8021:
            self->ipcpCounters++;
            return;

        case 0x8057:
            self->ipv6cpCounters++;
            return;

        default:
            if (Pid2String(pid))
                self->otherOamCounters++;
            else
                self->unknownPidCounters++;
            return;
        }
    }

eAtRet ThaPppLinkAddressControlErrorBypass(AtPppLink self, eBool bypass)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, mLinkMask(encapModule, AdrCtlErrDropEn), mLinkShift(encapModule, AdrCtlErrDropEn), bypass ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

eBool ThaPppLinkAddressControlErrorIsBypassed(AtPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    return (regVal & mLinkMask(encapModule, AdrCtlErrDropEn)) ? cAtFalse : cAtTrue;
    }

eAtRet ThaPppLinkAddressControlBypass(AtPppLink self, eBool addressControlByPass)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal,
              mLinkMask(encapModule, AddrCtrlMode),
              mLinkShift(encapModule, AddrCtrlMode),
              addressControlByPass ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return ThaPppLinkAddressControlErrorBypass(self, addressControlByPass);
    }

eBool ThaPppLinkAddressControlIsBypassed(AtPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    return (regVal & mLinkMask(encapModule, AddrCtrlMode)) ? cAtTrue : cAtFalse;
    }

eAtRet ThaPppLinkAddressControlCompress(AtPppLink self, eBool compress)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule;

    if (self == NULL)
        return cAtErrorNullPointer;

    encapModule = EncapModule((ThaPppLink)self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, mLinkMask(encapModule, AdrComp), mLinkShift(encapModule, AdrComp), compress ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

eBool ThaPppLinkAddressControlIsCompressed(AtPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    if (self == NULL)
        return cAtFalse;

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleMpig);
    return (regVal & mLinkMask(encapModule, AdrComp)) ? cAtTrue : cAtFalse;
    }

eAtRet ThaPppLinkFcsErrorBypass(AtPppLink self, eBool bypass)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal,
              mLinkMask(encapModule, FcsErrDropEn),
              mLinkShift(encapModule, FcsErrDropEn),
              bypass ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

eBool ThaPppLinkFcsErrorIsBypassed(AtPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule((ThaPppLink)self);

    regAddr = cThaRegMPIGDATLookupLinkCtrl + mDefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    return (regVal & mLinkMask(encapModule, FcsErrDropEn)) ? cAtFalse : cAtTrue;
    }

static eBool LinkRemoveFinished(AtPppLink self, uint32 address, uint32 bitMask, uint32 timeOut, eAtModule moduleId)
    {
    uint32 regValue;
    uint32 elapseTime;
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Hardware finish its job */
    regValue = mChannelHwRead(self, address, moduleId);
    if ((regValue & bitMask) == 0)
        return cAtTrue;

    if (IsSimulated(self))
        return cAtTrue;

    /* Timeout waiting for it */
    elapseTime = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < timeOut)
        {
        regValue = mChannelHwRead(self, address, moduleId);
        if ((regValue & bitMask) == 0)
            return cAtTrue;

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static eBool CheckStatusHwRdy(AtPppLink self, uint32 address, uint32 bitMask, eAtModule moduleId)
    {
    uint32  regValue;

    if (IsSimulated(self))
        return cAtTrue;

    regValue = mChannelHwRead(self, address, moduleId);
    if ((regValue & bitMask) != 0)
        return cAtFalse;

    return cAtTrue;
    }

eAtRet ThaPppLinkHardwareRemove(AtPppLink self)
    {
    uint32 regValue;
    AtOsal osal;
    const uint32 cHardwareLinkRemoveTimeOutInMs = 5000;
    uint32 partAddr = cThaEgBundleLinkRemove + ThaPppLinkPartOffset((ThaPppLink)self);

    /* Remove bundle HW sequence */
    if (!CheckStatusHwRdy(self, partAddr, cThaEgBdlLinkRmReqMask, cThaModuleMpeg))
        return cAtErrorDevBusy;

    /* Request HW to remove bundle */
    regValue = 0;
    mFieldIns(&regValue,
              cThaEgRemoveIdMask,
              cThaEgRemoveIdShift,
              ThaModuleEncapLocalPppLinkId((ThaPppLink)self));
    mFieldIns(&regValue,
              cThaEgLinkIndicMask,
              cThaEgLinkIndicShift,
              1);
    mFieldIns(&regValue,
              cThaEgBdlLinkRmReqMask,
              cThaEgBdlLinkRmReqShift,
              1);
    mChannelHwWrite(self, partAddr, regValue, cThaModuleMpeg);

    /* wait for hardware finish */
    if(!LinkRemoveFinished(self,
                           partAddr,
                           cThaEgBdlLinkRmReqMask,
                           cHardwareLinkRemoveTimeOutInMs,
                           cThaModuleMpeg))
        return cAtErrorIndrAcsTimeOut;

    /* Give hardware a moment */
    if (!IsSimulated(self))
        {
        osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->USleep(osal, 1000);
        }

    return cAtOk;
    }

void ThaPppLinkHwTxTrafficEnable(AtPppLink self, eBool enable)
    {
    uint32 address, regValue;

    address = cThaRegEgLinkScheduleCtrl + mDefaultOffset(self);
    regValue = mChannelHwRead(self, address, cThaModuleMpeg);
    mFieldIns(&regValue,
              cThaMPEGTrafficPppEnMask,
              cThaMPEGTrafficPppEnShift,
              mBoolToBin(enable));
    mChannelHwWrite(self, address, regValue, cThaModuleMpeg);
    }

eAtRet ThaPppLinkMemberLocalIdInBundleSet(ThaPppLink self , uint16 localMemberId)
    {
    if (self)
        return mMethodsGet(self)->MemberLocalIdInBundleSet(self, localMemberId);
    return cAtErrorNullPointer;
    }

eAtRet ThaPppLinkQueueBlockSizeSet(AtHdlcLink link, uint8 queueId, uint8 rangeId)
    {
    uint32 regAddr, regValue;

    if (link == NULL)
        return cAtErrorNullPointer;

    regAddr  = cThaRegEgLinkPerQueueCtrl + mMethodsGet((ThaPppLink)link)->QueueDefaultOffset((ThaPppLink)link, queueId);
    regValue = mChannelHwRead(link, regAddr, cThaModuleMpeg);
    mFieldIns(&regValue, cThaMpegLinkMaxBlkTypeMask, cThaMpegLinkMaxBlkTypeShift, rangeId);
    mChannelHwWrite(link, regAddr, regValue, cThaModuleMpeg);

    return cAtOk;
    }

eAtRet ThaPppLinkPppPacketEnable(ThaPppLink self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PppPacketEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPppLinkPppPacketIsEnabled(ThaPppLink self)
    {
    if (self)
        return mMethodsGet(self)->PppPacketIsEnabled(self);
    return cAtFalse;
    }

uint32 ThaPppLinkTxOamCounterLatch(ThaPppLink self, uint32 txOamCounter, eBool clearAfterLatch)
    {
    uint32 totalOam;

    self->txOamCounter = self->txOamCounter + txOamCounter;
    self->numOamsSent  = self->numOamsSent  + txOamCounter;
    totalOam = self->txOamCounter;

    if (clearAfterLatch)
        self->txOamCounter = 0;

    return totalOam;
    }

void ThaPppLinkOamLock(ThaPppLink self)
    {
    AtOsalSemTake(OamLocker(self));
    }

void ThaPppLinkOamUnlock(ThaPppLink self)
    {
    AtOsalSemGive(OamLocker(self));
    }

eBool ThaPppLinkCanCheckOamsSent(ThaPppLink self)
    {
    /* Turn this to cAtTrue if AtHdlcLinkAllOamsSent needs to be tested */
    AtUnused(self);
    return cAtTrue;
    }

uint32 ThaPppLinkPartOffset(ThaPppLink self)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    return ThaDeviceModulePartOffset((ThaDevice)device, cAtModuleEncap, ThaHdlcChannelPartOfEncap(EncapModule(self), channel));
    }

uint32 ThaModuleEncapLocalPppLinkId(ThaPppLink link)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)link);
    uint32 numChannelsPerPart = ThaModuleEncapNumEncapPerPart(EncapModule(link));
    uint32 channelId = AtChannelIdGet((AtChannel)channel);
    return numChannelsPerPart ? (channelId % numChannelsPerPart) : channelId;
    }

uint32 ThaPppLinkDefaultOffset(ThaPppLink self)
    {
    return mMethodsGet(self)->DefaultOffset(self);
    }

void ThaPppLinkOamTxEnable(ThaPppLink self, eBool enable)
    {
    OamTxEnable(self, enable);
    }

uint8 ThaPppLinkMaxNumQueues(ThaPppLink self)
    {
    AtDevice device = (AtDevice)AtChannelDeviceGet((AtChannel)self);
    ThaModulePpp pppModule = (ThaModulePpp)AtDeviceModuleGet(device, cAtModulePpp);
    return (uint8)ThaModulePppMaxNumQueuesPerLink(pppModule);
    }

uint32 ThaPppLinkOffset(ThaPppLink self)
    {
    return mLinkId(self) + ThaPppLinkPartOffset(self);
    }

eAtRet ThaPppLinkDebugCountersClear(ThaPppLink self)
    {
    self->lcpCounters        = 0;
    self->ipcpCounters       = 0;
    self->ipv6cpCounters     = 0;
    self->otherOamCounters   = 0;
    self->unknownPidCounters = 0;
    return cAtOk;
    }

eAtRet ThaHdlcLinkFlowCanBeBound(AtHdlcLink self, AtEthFlow flow)
    {
    AtEthFlow currentFlow = AtHdlcLinkBoundFlowGet(self);

    /* If this link is already bound to this flow, do nothing */
    if (currentFlow == flow)
        return cAtOk;

    /* Do not allow to bind to another flow if this link is busy */
    if (currentFlow != NULL)
        return cAtErrorChannelBusy;

    if (!FlowIsValidToBindLink(self, flow))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

AtEthFlow AtHdlcLinkOamFlowCreate(AtHdlcLink self)
    {
    AtModuleEth ethModule = EthModule(self);
    AtEthFlow flow = ThaModuleEthOamFlowCreate(ethModule, self);
    if (flow == NULL)
        return NULL;

    AtChannelInit((AtChannel)flow);

    /* OAM flow is always activated after being created */
    if (OamFlowActivate(self, flow) != cAtOk)
        {
        ThaModuleEthOamFlowDelete(ethModule, flow);
        return NULL;
        }

    /* Catch this link */
    AtEthFlowHdlcLinkSet(flow, self);

    return flow;
    }

void AtHdlcLinkOamFlowDelete(AtHdlcLink self)
    {
    AtEthFlow oamFlow = AtHdlcLinkOamFlowGet(self);
    if (oamFlow == NULL)
        return;

    OamFlowDeActivate(self, oamFlow);
    ThaModuleEthOamFlowDelete(EthModule(self), oamFlow);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO module name
 *
 * File        : ThaStmPmcReg.h
 *
 * Created Date: Dec 3, 2012
 *
 * Author      : thanhnc
 *
 * Description : STM product
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef THASTMPMCREG_H_
#define THASTMPMCREG_H_

#define mPmcCounterGet(hwCounterName, pSwCounterField)                         \
    do                                                                         \
        {                                                                      \
        if (counterPosition == cTha##hwCounterName##Position)                  \
            mFieldGet(longRegVal[cTha##hwCounterName##DwIndex],                \
                      cTha##hwCounterName##Mask,                               \
                      cTha##hwCounterName##Shift,                              \
                      uint32,                                                  \
                      pSwCounterField);                                        \
        }while(0)

#define mPmcCounterGetIn2Dwords(hwCounterName, pSwCounterField)                \
    do                                                                         \
        {                                                                      \
        uint32 fieldValue;                                                     \
        if (counterPosition == cTha##hwCounterName##Position)                  \
            {                                                                  \
            /* Low field */                                                    \
            mFieldGet(longRegVal[cTha##hwCounterName##LowDwIndex],             \
                      cTha##hwCounterName##HwLowMask,                          \
                      cTha##hwCounterName##HwLowShift,                         \
                      uint32,                                                  \
                      &fieldValue);                                            \
            mFieldIns(pSwCounterField,                                         \
                      cTha##hwCounterName##SwLowMask,                          \
                      cTha##hwCounterName##SwLowShift,                         \
                      fieldValue);                                             \
                                                                               \
            /* High field */                                                   \
            mFieldGet(longRegVal[cTha##hwCounterName##HighDwIndex],            \
                      cTha##hwCounterName##HwHighMask,                         \
                      cTha##hwCounterName##HwHighShift,                        \
                      uint32,                                                  \
                      &fieldValue);                                            \
            mFieldIns(pSwCounterField,                                         \
                      cTha##hwCounterName##SwHighMask,                         \
                      cTha##hwCounterName##SwHighShift,                        \
                      fieldValue);                                             \
            }                                                                  \
        }while(0)

/* Register Refresh when counter invalid */
#define cThaRegThalassaPMCCntIndrRefresh     0xC00020

/*------------------------------------------------------------------------------
Reg Name: Thalassa PM MLPPP Counters Indirect Register Ready Status
Reg Addr: 0x0100
          The address format of these registers is 0x0100 + MPXGPMCPrjOffset
          Where: HoldId: (0 - 2) Hold register
          MPXGPMCPrjOffset: is MPEGPMCPrjOffset or MPIGPMCPrjOffset, has
          description at Thalassa PM MLPPP Counters register
Reg Desc: The register provides the status of indirect register is ready to
          receive a reading request from CPU.
------------------------------------------------------------------------------*/
#define cThaRegThalassaPMMpxgCntsIndrRegReadyStat       0xC00100

/*--------------------------------------
BitField Name: PMCounterIndRdy
BitField Type: R
BitField Desc: This is the status of indirect register. If this bit assert the
               indirect register is full and  not ready receive command from
               CPU else ready receive command.
--------------------------------------*/
#define cThaPMCounterIndRdyMask                         cBit0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PM MLPPP Counters Indirect Register Valid Status
Reg Addr: 0x0102
          The address format of these registers is 0x0102 + MPXGPMCPrjOffset
          Where: MPXGPMCPrjOffset: is MPEGPMCPrjOffset or MPIGPMCPrjOffset,
          has description at Thalassa PM MLPPP Counters register
Reg Desc: The register provides the valid status of counters in the indirect
          register.
------------------------------------------------------------------------------*/
#define cThaRegThalassaPMMpxgCntsIndrRegValidStat       0xC00102

/*--------------------------------------
BitField Name: PMCounterIndVld
BitField Type: R
BitField Desc: This is status of counter values in the indirect register. If
               this bit assert the counters is read from DDR memory according
               to address which CPU request is valid and CPU can get values
               from the Thalassa PM MLPPP Counters  Indirect Register Report
               Status register now.
--------------------------------------*/
#define cThaPMCounterIndVldMask                        cBit0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PM MLPPP Counters  Indirect Register Report Status
Reg Addr: 0x0103
          The address format of these registers is 0x0103 + MPXGPMCPrjOffset
          Where: MPXGPMCPrjOffset: is MPEGPMCPrjOffset or MPIGPMCPrjOffset,
          has description at Thalassa PM MLPPP Counters register
Reg Desc: The register provides the valid status of counters in the indirect
          register.
------------------------------------------------------------------------------*/
#define cThaRegThalassaPMMpxgCntsIndrRegRptStat          0xC00103

/*--------------------------------------
BitField Name: PMCounterCntPos[1:0]
BitField Type: R
BitField Desc: These are 4x64 bits of 256 bits counter report according to one
               address.
               - 3:  The PMCounterVal[63:0] get from 255 to 192 of counter
                 report.
               - 2:  The PMCounterVal[63:0] get from 191 to 128 of counter
                 report.
               - 1:  The PMCounterVal[63:0] get from 127 to 64 of counter
                 report.
               - 0:  The PMCounterVal[63:0] get from 63 to 0 of counter report.
--------------------------------------*/
#define cThaPMCounterCntPosMask                        cBit18_16
#define cThaPMCounterCntPosShift                       16
#define cThaPMCounterCntPosDwIndex                     3

/*--------------------------------------
BitField Name: PMCounterCntAddr[13:0]
BitField Type: R
BitField Desc: There are 14 bits LSB of address of counter report.
--------------------------------------*/
#define cThaPMCounterCntAddrRepMask                       cBit13_0
#define cThaPMCounterCntAddrRepShift                      0
#define cThaPMCounterCntAddrRepDwIndex                    2

/*------------------------------------------------------------------------------
Reg Name: Thalassa PM PPP Flow Counters Status Group
Reg Addr: refer to Thalassa PM Counters Address Request Control register
          The address format for these registers is 0x1800 + linkid
          Where: Flowid (0 - 2047) Flow resequence engine IDs
Reg Desc: The register provides the value of MLPPP Flow Counters Status which
          are accessed through the indirect register.
------------------------------------------------------------------------------*/










/*------------------------------------------------------------------------------
Reg Name: Thalassa PM MLPPP Counters Address Request Control
Reg Addr: 0x0101
          The address format of these registers is 0x0101 + MPXGPMCPrjOffset
          Where: MPXGPMCPrjOffset: is MPEGPMCPrjOffset or MPIGPMCPrjOffset,
          has description at Thalassa PM MLPPP Counters register
Reg Desc: The register used to write address of counters which CPU need
          reading when it's ready.
------------------------------------------------------------------------------*/
#define cThaRegThalassaPMMpxgCntsAddressRequestCtrl     0xC00101

/*--------------------------------------
BitField Name: PMCounterCntMod[47_16]
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaPMCCounterR2CMask(counterId)    (uint32)((((counterId) < 16) ? cBit16 : cBit0) << ((counterId) % 16))
#define cThaPMCCounterR2CShift(counterId)   (uint32)(((counterId) < 16) ? ((counterId) + 16) : ((counterId) % 16))
#define cThaPMCCounterR2CDwIndex(counterId) (((counterId) < 16) ? 0 : 1)

/*--------------------------------------
BitField Name: PMCounterCntAddr[13:0]
BitField Type: R/W
BitField Desc: These are 14 bits LSB address of counter which CPU need reading
from DDR memory.
    0x0000 � 0x03FF: for Thalassa PM MLPPP Link Counters Status 0
    0x0400 � 0x07FF: for Thalassa PM MLPPP Bundle Counters Status 0
    0x0800 � 0x0FFF: for Thalassa PM MLPPP MpFlow Counters Status 0
--------------------------------------*/
#define cThaPMCounterCntAddrMask                       cBit13_0
#define cThaPMCounterCntAddrShift                      0
#define cThaPMCCounterCntDwIndex                       0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PM MLPPP Link Counters Status Group 0
Reg Addr: refer to Thalassa PM Counters Address Request Control register
          The address format for these registers is 0x0000 + Linkid
          Where: Linkid (0 - 1023) Link connection IDs
Reg Desc: The register provides the value of MLPPP Link Counters Status which
          are accessed through the indirect register.
------------------------------------------------------------------------------*/

#define cThaPMCMpigLinkRxErrMask cBit31_8
#define cThaPMCMpigLinkRxErrShift 8
#define cThaPMCMpigLinkRxErrDwIndex 1
#define cThaPMCMpigLinkRxErrPosition 7

#define cThaPMCMpigLinkRxMRUMask cBit19_0
#define cThaPMCMpigLinkRxMRUShift 0
#define cThaPMCMpigLinkRxMRUDwIndex 0
#define cThaPMCMpigLinkRxMRUPosition 7

#define cThaPMCMpigLinkRxMlpppMask cBit23_0
#define cThaPMCMpigLinkRxMlpppShift 0
#define cThaPMCMpigLinkRxMlpppDwIndex 0
#define cThaPMCMpigLinkRxMlpppPosition 6

#define cThaPMCMpigLinkRxPppMask cBit31_12
#define cThaPMCMpigLinkRxPppShift 12
#define cThaPMCMpigLinkRxPppDwIndex 1
#define cThaPMCMpigLinkRxPppPosition 5

#define cThaPMCMpigLinkRxLcpHwLowMask cBit31_24
#define cThaPMCMpigLinkRxLcpHwLowShift 24
#define cThaPMCMpigLinkRxLcpLowDwIndex 0
#define cThaPMCMpigLinkRxLcpHwHighMask cBit11_0
#define cThaPMCMpigLinkRxLcpHwHighShift 0
#define cThaPMCMpigLinkRxLcpHighDwIndex 1

#define cThaPMCMpigLinkRxLcpSwLowMask cBit7_0
#define cThaPMCMpigLinkRxLcpSwLowShift 0
#define cThaPMCMpigLinkRxLcpSwHighMask cBit19_8
#define cThaPMCMpigLinkRxLcpSwHighShift 8
#define cThaPMCMpigLinkRxLcpPosition 5

#define cThaPMCMpigLinkRxFrameMask cBit23_0
#define cThaPMCMpigLinkRxFrameShift 0
#define cThaPMCMpigLinkRxFrameDwIndex 0
#define cThaPMCMpigLinkRxFramePosition 5

#define cThaPMCMpigLinkRxByteMask cBit31_0
#define cThaPMCMpigLinkRxByteShift 0
#define cThaPMCMpigLinkRxByteDwIndex 1
#define cThaPMCMpigLinkRxBytePosition 4

#define cThaPMCMpigLinkTxByteMask cBit31_0
#define cThaPMCMpigLinkTxByteShift 0
#define cThaPMCMpigLinkTxByteDwIndex 0
#define cThaPMCMpigLinkTxBytePosition 4

#define cThaPMCMpigLinkTxPktMask cBit31_8
#define cThaPMCMpigLinkTxPktShift 8
#define cThaPMCMpigLinkTxPktDwIndex 1
#define cThaPMCMpigLinkTxPktPosition 3

#define cThaPMCMpegLinkRxDiscMask cBit31_12
#define cThaPMCMpegLinkRxDiscShift 12
#define cThaPMCMpegLinkRxDiscDwIndex 1
#define cThaPMCMpegLinkRxDiscPosition 2

#define cThaPMCMpegLinkTxMlpppHwLowMask cBit31_24
#define cThaPMCMpegLinkTxMlpppHwLowShift 24
#define cThaPMCMpegLinkTxMlpppLowDwIndex 0
#define cThaPMCMpegLinkTxMlpppHwHighMask cBit11_0
#define cThaPMCMpegLinkTxMlpppHwHighShift 0
#define cThaPMCMpegLinkTxMlpppHighDwIndex 1

#define cThaPMCMpegLinkTxMlpppSwLowMask cBit7_0
#define cThaPMCMpegLinkTxMlpppSwLowShift 0
#define cThaPMCMpegLinkTxMlpppSwHighMask cBit19_8
#define cThaPMCMpegLinkTxMlpppSwHighShift 8
#define cThaPMCMpegLinkTxMlpppPosition 2

#define cThaPMCMpegLinkRxPktMask cBit23_0
#define cThaPMCMpegLinkRxPktShift 0
#define cThaPMCMpegLinkRxPktDwIndex 0
#define cThaPMCMpegLinkRxPktPosition 2

#define cThaPMCMpegLinkTxPppMask cBit31_12
#define cThaPMCMpegLinkTxPppShift 12
#define cThaPMCMpegLinkTxPppDwIndex 1
#define cThaPMCMpegLinkTxPppPosition 1

#define cThaPMCMpegLinkTxLcpHwLowMask cBit31_24
#define cThaPMCMpegLinkTxLcpHwLowShift 24
#define cThaPMCMpegLinkTxLcpLowDwIndex 0
#define cThaPMCMpegLinkTxLcpHwHighMask cBit11_0
#define cThaPMCMpegLinkTxLcpHwHighShift 0
#define cThaPMCMpegLinkTxLcpHighDwIndex 1

#define cThaPMCMpegLinkTxLcpSwLowMask cBit7_0
#define cThaPMCMpegLinkTxLcpSwLowShift 0
#define cThaPMCMpegLinkTxLcpSwHighMask cBit19_8
#define cThaPMCMpegLinkTxLcpSwHighShift 8
#define cThaPMCMpegLinkTxLcpPosition 1

#define cThaPMCMpegLinkTxFrameMask cBit23_0
#define cThaPMCMpegLinkTxFrameShift 0
#define cThaPMCMpegLinkTxFrameDwIndex 0
#define cThaPMCMpegLinkTxFramePosition 1

#define cThaPMCMpegLinkTxByteMask cBit31_0
#define cThaPMCMpegLinkTxByteShift 0
#define cThaPMCMpegLinkTxByteDwIndex 1
#define cThaPMCMpegLinkTxBytePosition 0

#define cThaPMCMpegLinkRxByteMask cBit31_0
#define cThaPMCMpegLinkRxByteShift 0
#define cThaPMCMpegLinkRxByteDwIndex 0
#define cThaPMCMpegLinkRxBytePosition 0

/*------------------------------------------------------------------------------
 Reg Name: Thalassa PM MLPPP Bundle Counters Status Group 0
Reg Addr: refer to Thalassa PM Counters Address Request Control register
 The address format for these registers is 0x0400 + Bundid
 Where: Bundid (0 - 1023) Bund connection IDs
 Reg Desc: The register provides the value of MLPPP Bund Counters Status which
          are accessed through the indirect register.
------------------------------------------------------------------------------*/

#define cThaPMCMpigBundTxDiscMask cBit23_0
#define cThaPMCMpigBundTxDiscShift 0
#define cThaPMCMpigBundTxDiscDwIndex 0
#define cThaPMCMpigBundTxDiscPosition 5

#define cThaPMCMpigBundTxByteMask cBit31_0
#define cThaPMCMpigBundTxByteShift 0
#define cThaPMCMpigBundTxByteDwIndex 1
#define cThaPMCMpigBundTxBytePosition 4

#define cThaPMCMpigBundTxPktMask cBit31_0
#define cThaPMCMpigBundTxPktShift 0
#define cThaPMCMpigBundTxPktDwIndex 0
#define cThaPMCMpigBundTxPktPosition 4

#define cThaPMCMpegBundRxDiscMask cBit23_0
#define cThaPMCMpegBundRxDiscShift 0
#define cThaPMCMpegBundRxDiscDwIndex 0
#define cThaPMCMpegBundRxDiscPosition 1

#define cThaPMCMpegBundRxPktMask cBit31_0
#define cThaPMCMpegBundRxPktShift 0
#define cThaPMCMpegBundRxPktDwIndex 1
#define cThaPMCMpegBundRxPktPosition 0

#define cThaPMCMpegBundRxByteMask cBit31_0
#define cThaPMCMpegBundRxByteShift 0
#define cThaPMCMpegBundRxByteDwIndex 0
#define cThaPMCMpegBundRxBytePosition 0


/*------------------------------------------------------------------------------
Reg Name: Thalassa PM MLPPP MpFlow Counters Status  0
Address: Refer to Thalassa PM Counters Address Request Control register
         The address format for these registers is 0x0800 +  MpFlowid
Where:   MpFlowid (0 � 1023) MpFlow IDs
Reg Desc:The register provides the value of MLPPP MpFlowID Counters Status
         which are accessed through the indirect register.
------------------------------------------------------------------------------*/

#define cThaPMCMpigMpFlowRsqDiscFragMask cBit31_8
#define cThaPMCMpigMpFlowRsqDiscFragShift 8
#define cThaPMCMpigMpFlowRsqDiscFragDwIndex 1
#define cThaPMCMpigMpFlowRsqDiscFragPosition 7

#define cThaPMCMpigMpFlowRsqViolMHwLowMask cBit31_20
#define cThaPMCMpigMpFlowRsqViolMHwLowShift 20
#define cThaPMCMpigMpFlowRsqViolMLowDwIndex 0
#define cThaPMCMpigMpFlowRsqViolMHwHighMask cBit7_0
#define cThaPMCMpigMpFlowRsqViolMHwHighShift 0
#define cThaPMCMpigMpFlowRsqViolMHighDwIndex 1
#define cThaPMCMpigMpFlowRsqViolMSwLowMask cBit11_0
#define cThaPMCMpigMpFlowRsqViolMSwLowShift 0
#define cThaPMCMpigMpFlowRsqViolMSwHighMask cBit19_12
#define cThaPMCMpigMpFlowRsqViolMSwHighShift 12
#define cThaPMCMpigMpFlowRsqViolMPosition 7

#define cThaPMCMpigMpFlowGoodByteMask cBit31_0
#define cThaPMCMpigMpFlowGoodByteShift 0
#define cThaPMCMpigMpFlowGoodByteDwIndex 1
#define cThaPMCMpigMpFlowGoodBytePosition 5

#define cThaPMCMpigMpFlowNullFragMask cBit31_0
#define cThaPMCMpigMpFlowNullFragShift 0
#define cThaPMCMpigMpFlowNullFragDwIndex 0
#define cThaPMCMpigMpFlowNullFragPosition 5

#define cThaPMCMpigMpFlowFragMask cBit31_0
#define cThaPMCMpigMpFlowFragShift 0
#define cThaPMCMpigMpFlowFragDwIndex 1
#define cThaPMCMpigMpFlowFragPosition 4

#define cThaPMCMpigMpFlowGoodPktMask cBit31_0
#define cThaPMCMpigMpFlowGoodPktShift 0
#define cThaPMCMpigMpFlowGoodPktDwIndex 0
#define cThaPMCMpigMpFlowGoodPktPosition 4

#define cThaPMCMpigMpFlowMrruPktMask cBit31_0
#define cThaPMCMpigMpFlowMrruPktShift 0
#define cThaPMCMpigMpFlowMrruPktDwIndex 0
#define cThaPMCMpigMpFlowMrruPktPosition 3

#define cThaPMCMpegMpFlowDiscbyteMask cBit31_0
#define cThaPMCMpegMpFlowDiscbyteShift 0
#define cThaPMCMpegMpFlowDiscbyteDwIndex 1
#define cThaPMCMpegMpFlowDiscbytePosition 2

#define cThaPMCMpegMpFlowDiscPktMask cBit31_0
#define cThaPMCMpegMpFlowDiscPktShift 0
#define cThaPMCMpegMpFlowDiscPktDwIndex 0
#define cThaPMCMpegMpFlowDiscPktPosition 2

#define cThaPMCMpegMpFlowGoodByteMask cBit31_0
#define cThaPMCMpegMpFlowGoodByteShift 0
#define cThaPMCMpegMpFlowGoodByteDwIndex 1
#define cThaPMCMpegMpFlowGoodBytePosition 1

#define cThaPMCMpegMpFlowNullFragMask cBit31_0
#define cThaPMCMpegMpFlowNullFragShift 0
#define cThaPMCMpegMpFlowNullFragDwIndex 0
#define cThaPMCMpegMpFlowNullFragPosition 1

#define cThaPMCMpegMpFlowFragMask cBit31_0
#define cThaPMCMpegMpFlowFragShift 0
#define cThaPMCMpegMpFlowFragDwIndex 1
#define cThaPMCMpegMpFlowFragPosition 0

#define cThaPMCMpegMpFlowGoodPktMask cBit31_0
#define cThaPMCMpegMpFlowGoodPktShift 0
#define cThaPMCMpegMpFlowGoodPktDwIndex 0
#define cThaPMCMpegMpFlowGoodPktPosition 0

#ifdef __cplusplus
}
#endif
#endif /* THASTMPMCREG_H_ */
#ifdef __cplusplus
extern "C" {
#endif

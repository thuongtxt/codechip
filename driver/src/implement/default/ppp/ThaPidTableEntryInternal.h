/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaPidTableEntryInternal.h
 * 
 * Created Date: Sep 20, 2013
 *
 * Description : PID table entry
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPIDTABLEENTRYINTERNAL_H_
#define _THAPIDTABLEENTRYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ppp/AtPidTableEntryInternal.h"
#include "../../../generic/man/AtModuleInternal.h"
#include "ThaPidTableEntry.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPidTableEntry
    {
    tAtPidTableEntry super;
    }tThaPidTableEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTableEntry ThaPidTableEntryObjectInit(AtPidTableEntry entry, uint32 entryIndex, AtPidTable self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPIDTABLEENTRYINTERNAL_H_ */


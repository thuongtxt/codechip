/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaStmMpBundle.h
 * 
 * Created Date: Dec 3, 2012
 *
 * Description : MLPPP bundle of STM product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMPBUNDLE_H_
#define _THASTMMPBUNDLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaMpBundle.h" /* It super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMpBundle ThaStmMpBundleNew(uint32 channelId, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMPBUNDLE_H_ */


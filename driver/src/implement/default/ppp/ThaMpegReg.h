/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO module name
 *
 * File        : AtModulePpp.h
 *
 * Created Date: Aug 13, 2012
 *
 * Author      : nguyennt
 *
 * Description : TODO Description
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAMPEGREG_H_
#define _THAMPEGREG_H_
/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress Bundle Scheduler Control
Address: 0x278000 � 0x27807F
The address format for these registers is 0x278000 + BundleID
Where: BundleID (0 � 127) is MLPPP bundle ID
Description: This register configures the scheduling parameters of bundle
------------------------------------------------------------------------------*/
#define cThaRegEgBundleScheduleCtrl 						0x278000
#define cThaRegEgBundleScheduleCtrlNoneEditableFieldsMask 	cBit31_13

#define cThaRegMLPppEgrMPEGFragModeMask     cBit25_24
#define cThaRegMLPppEgrMPEGFragModeShift   24

#define cThaRegMLPppEgrMPEGMaxLinkMsk      cBit20_16
#define cThaRegMLPppEgrMPEGMaxLinkShift    16

#define cThaRegEgBundleScheduleMPEGBunQueEnMask(queueId)   (cBit5 << (queueId))
#define cThaRegEgBundleScheduleMPEGBunQueEnShift(queueId)  (5 + (queueId))

#define cThaRegMLPppEgrBundleFragSizeMsk   cBit4_2
#define cThaRegMLPppEgrBundleFragSizeShift 2

#define cThaRegMLPppEgrBundleEnMask       cBit1
#define cThaRegMLPppEgrBundleEnShift       1

#define cThaRegEgBundleScheduleSeqModMasks cBit0
#define cThaRegEgBundleScheduleSeqModShift 0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress OAM CPU Packet Information Read Only
Reg Addr: 0x25A001
Reg Desc: CPU will monitor this register to get HW engine busy status before
          originating a new OAM
------------------------------------------------------------------------------*/
#define cThaRegThalassaMLPppEgrOamCpuPktInformationReadOnly  0x25A001

/*--------------------------------------
BitField Name: MPEGUpOAMBusy
BitField Type: R_O
BitField Desc: - 0: HW Engine is not busy and ready for a new CPU OAM packet
               - 1: HW Engine is busy sending the current CPU OAM packet
--------------------------------------*/
#define cThaMPEGUpOamBusyMask                          cBit0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress OAM CPU Packet Data Control
Reg Addr: 0x25A100-0x25A1FF
          The address format for these registers is 0x25A100 + Entry
          Where: Entry (0-255) is 4-byte position from SOP to EOP of the OAM
          packet
Reg Desc: CPU write OAM packet data to this register
------------------------------------------------------------------------------*/
#define cThaRegThalassaMLPppEgrOamCpuPktDataCtrl     						0x25A100UL
#define cThaRegThalassaMLPppEgrOamCpuPktDataCtrlNoneEditableFieldsMask     	0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress OAM CPU Packet Information Control
Reg Addr: 0x25A000
Reg Desc: CPU write OAM packet information to this register to inform HW
          engine that a new CPU OAM packet has been written into CPU Packet
          Data buffer and ready to be transmitted
------------------------------------------------------------------------------*/
#define cThaRegThalassaMLPppEgrOamCpuPktInformationCtrl  0x25A000

/*--------------------------------------
BitField Name: MPEGUpOAMPktLen
BitField Type: R/W
BitField Desc: Length of  the CPU OAM  packet
--------------------------------------*/
#define cThaMPEGUpOamPktLenMask                        cBit17_7
#define cThaMPEGUpOamPktLenShift                       7

/*--------------------------------------
BitField Name: MPEGUpOAMLinkID
BitField Type: R/W
BitField Desc: Link ID that transmits the CPU OAM  packet
--------------------------------------*/
#define cThaMPEGUpOamLinkIDMask                        cBit6_0
#define cThaMPEGUpOamLinkIDShift                       0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress PPP Link Control 1
Reg Addr: 0x248000-0x2483FF
          The address format for these registers is 0x248000 + LinkID
          Where: LinkID (0-1023) is PPP Link ID
Reg Desc: This register configures bundle ID that includes the PPP Link
------------------------------------------------------------------------------*/
#define cThaRegThalassaMLPppEgrPppLinkCtrl1    							0x248000
#define cThaRegThalassaMLPppEgrPppLinkCtrl1NoneEditableFieldsMask    	cBit31_5

/*--------------------------------------
BitField Name: MPEGMemID
BitField Type: R/W
BitField Desc:Member ID on bundle, The first Link set zero, second Link set one, and so on
--------------------------------------*/
#define cThaMPEGMemIdMask                                cBit20_16
#define cThaMPEGMemIdShift                               16
/*--------------------------------------
BitField Name: PktMode
BitField Type: R/W
BitField Desc:
            0: HDLC PW
            1: HDLC Ter  Base on PPP-PID to classify to packet type
            2: PPP PW
            3: PPP Ter (Termination). Base on PPP-PID to classify to packet type
            4: FR PW
            5: FR Ter
            6: Cisco-HDLC
            7: Cisco-FramRelay
--------------------------------------*/
#define cThaMPEGEncTypeMask                               cBit14_12
#define cThaMPEGEncTypeShift                              12

/*--------------------------------------
BitField Name: MPEGBundleID
BitField Type: R/W
BitField Desc: Bundle ID that the PPP Link belonged to
--------------------------------------*/
#define cThaMPEGPppLinkCtrl1BundleIDMask               cBit4_1
#define cThaMPEGPppLinkCtrl1BundleIDShift              1

/*--------------------------------------
BitField Name: MPEGLinkMlpEn
BitField Type: R/W
BitField Desc:
    1: The link enable MLPPP packets (when the link is belong to a MLPPP bundle)
    0: The link disable MLPPP packets (when the link is not belong to any MLPPP bundle)
--------------------------------------*/
#define cThaMPEGPppLinkCtrl1MlpEnMasks     cBit0
#define cThaMPEGPppLinkCtrl1MlpEnShift     0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress Link Scheduler Control
Address:  0x268000 � 0x2681FF
The address format for these registers is 0x268000 + LinkID
Where: LinkID (0 � 511) is PPP Link ID
Description: This register configures the scheduling parameters of link
------------------------------------------------------------------------------*/
#define cThaRegEgLinkScheduleCtrl   						0x268000
#define cThaRegEgLinkScheduleCtrlNoneEditableFieldsMask   	cBit31_5

/*--------------------------------------
BitField Name: MPEGLinkQueEn
BitField Type: R/W
BitField Desc:
    Each bit set will enable the corresponding queue of link, otherwise, the will is disabled.
    Eg: MPEGLinkQueEn[0] set will enable the queue#0 of link
--------------------------------------*/
#define cThaRegEgLinkScheduleMPEGLinkQueEnMask(queueId)   (cBit3 << (queueId)) /* cBit1_0 */
#define cThaRegEgLinkScheduleMPEGLinkQueEnShift(queueId)  (3 + (queueId))

/*--------------------------------------
BitField Name: MPEGTrafficOAMEn
BitField Type: R/W
BitField Desc:
    1: Link enable sending OAM (LCP,NCP) traffic from GE
    0: Link disable sending OAM (LCP,NCP) traffic from GE
--------------------------------------*/
#define cThaMPEGTrafficOAMEnMask              cBit2
#define cThaMPEGTrafficOAMEnShift             2

/*--------------------------------------
BitField Name: MPEGTrafficPPPEn
BitField Type: R/W
BitField Desc:
    1: Link enable sending PPP traffic from GE
    0: Link disable sending PPP traffic from GE
--------------------------------------*/
#define cThaMPEGTrafficPppEnMask              cBit1
#define cThaMPEGTrafficPppEnShift             1

/*--------------------------------------
BitField Name: MPEGLinkMlpEn
BitField Type: R/W
BitField Desc:
    1: The link enable MLPPP packets (when the link is belong to a MLPPP bundle)
    0: The link disable MLPPP packets (when the link is not belong to any MLPPP bundle)
--------------------------------------*/
#define cThaRegEgLinkScheduleMlpEnMasks       cBit0
#define cThaRegEgLinkScheduleMlpEnShift       0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress OAM CPU Packet Information Control
Reg Addr: 0x25A000
Reg Desc: This register configures process to remove bundle or link
------------------------------------------------------------------------------*/
#define  cThaEgBundleLinkRemove             						0x27A000

/*--------------------------------------
BitField Name: MPEGRemoveReq
BitField Type: R/W
BitField Desc:
    1: Set by SW to request HW MPEG to remove bundle or link
    0: Clear by HW MPEG to indicate the remove process finish
--------------------------------------*/
#define cThaEgBdlLinkRmReqMask              cBit0
#define cThaEgBdlLinkRmReqShift             0

/*--------------------------------------
BitField Name: MPEGRemoveLinkInd
BitField Type: R/W
BitField Desc:
    1: Indicate request remove link
    0: Indicate request remove bundle
--------------------------------------*/
#define cThaEgLinkIndicMask                 cBit1
#define cThaEgLinkIndicShift                1

/*--------------------------------------
BitField Name: MPEGRemoveID
BitField Type: R/W
BitField Desc: Bundle ID or Link ID to be removed
--------------------------------------*/
#define cThaEgRemoveIdMask                  cBit11_2
#define cThaEgRemoveIdShift                 2

/*------------------------------------------------------------------------------
Reg Name: 3.23.8. Thalassa MLPPP Egress MLPPP Flow Sequence Status
Address: 0x253000 �  0x2533FF
The address format for these registers is 0x253000 + MpFlowID
Where: MpFlowID (0 � 1023) is MLPPP Flow ID (Without Multi-Class Multilink, is BundleID)
Description: This register CPU access Mpflow sequence status
------------------------------------------------------------------------------*/
#define cThaMPEGFlowSequenceStatus       0x253000

#define cThaMPEGMpFlowCurrSequenceMask   cBit23_0
#define cThaMPEGMpFlowCurrSequenceShift  0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress Block Control
Reg Addr: 0x27B000-0x27B007
          The address format for these registers is 0x27B000 + (typeId)
Reg Desc: The register configure maximum block value for queue block type
------------------------------------------------------------------------------*/
#define cThaMpegBlockControl            0x27B000

/*--------------------------------------
BitField Name: MPEGMaxBlkTyp
BitField Type: R/W
BitField Desc: Number of block
--------------------------------------*/
#define cThaMpegBlockValueMask  cBit15_0
#define cThaMpegBlockValueShift 0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress Queue Per Bundle Scheduler Control
Address: 0x270000 - 0x270F67
The address format for these registers is 0x270000 + QueueID*128 + BundleID
Where: BundleID (0 - 127) is MLPPP bundle ID, QueueID (0 - 9) is MLPPP Queue ID in a bundle
Description: This register configures the scheduling parameters of queues in bundle
------------------------------------------------------------------------------*/
#define cThaRegEgBundlePerQueueCtrl 0x270000

#define cThaMpegBundleMaxBlkTypeMask  cBit16_14
#define cThaMpegBundleMaxBlkTypeShift 14

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress Queue Per Link Scheduler Control
Address: 0x260000 - 0x2613FF
The address format for these registers is 0x260000 + QueueID*512 + LinkID
Where: LinkID (0 - 511) is PPP Link ID, QueueID (0 - 9) is  Queue ID in a PPP link
Description: This register configures the scheduling parameters of queues in PPP link
------------------------------------------------------------------------------*/
#define cThaRegEgLinkPerQueueCtrl                    0x260000

#define cThaMpegLinkMaxBlkTypeMask  cBit16_14
#define cThaMpegLinkMaxBlkTypeShift 14

#define cThaOamDirectWriteControl    0x0
#define cThaOamDirectWriteReadyMask  cBit15
#define cThaOamDirectWriteEnd        cBit14
#define cThaOamDirectWriteOper       cBit13
#define cThaOamDirectWriteLengthMask cBit9_0
#define cThaOamDirectWriteLengthShift 0

#define cThaOamDirectWriteLinkId      0x1
#define cThaOamDirectWriteData    0x2

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress PPP Protocol Control
Address: 0x25C000-0x25C005
The address format for these registers is 0x25C000 + PppIdx
Where: PppIdx (0 � 5) is 6 PPP protocol valude
Description: The register configure PPP protocol ID #1 encapsulation. MPEG will use this
PPP protocol value when the PPP protocol type value indication by classification equal to ZERO
------------------------------------------------------------------------------*/
#define cThaRegEgressPppProtocolCtrl 0x25C000

#define cThaRegMlpppMPEGPPPIDMask    cBit15_0
#define cThaRegMlpppMPEGPPPIDShift   0

/*------------------------------------------------------------------------------
Reg Name: Thalassa MLPPP Egress Compress Control
Address: 0x25f000 � 0x25f3FF
The address format for these registers is 0x25f000 + LinkID
Where: LinkID (0 � 1023) is PPP Link ID
Description: This register configures compression mode in PPP Link
------------------------------------------------------------------------------*/
#define cThaRegEgressCompressControl  0x25f000

#define cThaRegMlpppMPEGHdlcAddCtrlInsertEnMask  cBit0
#define cThaRegMlpppMPEGHdlcAddCtrlInsertEnShift 0

#define cThaRegMlpppMPEGLinkPFCEnMask    cBit1
#define cThaRegMlpppMPEGLinkPFCEnShift   1

#define cThaRegMlpppMPEGHdlcAcfcEnMask   cBit2
#define cThaRegMlpppMPEGHdlcAcfcEnShift  2

#ifdef __cplusplus
}
#endif
#endif /* _THAMPEGREG_H_ */

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : ThaModuleStmPpp.h
 * 
 * Created Date: Dec 3, 2012
 *
 * Description : PPP module for STM product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESTMPPP_H_
#define _THAMODULESTMPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModulePpp.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Class representation */
typedef struct tThaModuleStmPpp
    {
    tThaModulePpp super;

    /* Private data */
    }tThaModuleStmPpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePpp ThaModuleStmPppObjectInit(AtModulePpp self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESTMPPP_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaResequenceManager.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : Re-sequence engine manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THARESEQUENCEMANAGER_H_
#define _THARESEQUENCEMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "../../../../generic/encap/resequence/AtResequenceManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtResequenceManager ThaResequenceManagerNew(AtDevice device);

AtResequenceEngine Tha60210012ResequenceEngineNew(AtResequenceManager manager, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THARESEQUENCEMANAGER_H_ */


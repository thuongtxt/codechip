/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaResequenceManagerInternal.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THARESEQUENCEMANAGERINTERNAL_H_
#define _THARESEQUENCEMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/encap/resequence/AtResequenceManagerInternal.h"
#include "../../../default/util/ThaBitMask.h"
#include "ThaResequenceManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaResequenceManager
    {
    tAtResequenceManager super;

    /* Private data */
    ThaBitMask engineMask;
    }tThaResequenceManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtResequenceManager ThaResequenceManagerObjectInit(AtResequenceManager self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THARESEQUENCEMANAGERINTERNAL_H_ */


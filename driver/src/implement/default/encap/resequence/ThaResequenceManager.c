/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaResequenceManager.c
 *
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaResequenceManagerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/encap/resequence/AtResequenceEngineInternal.h"
#include "../../../../generic/man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaResequenceManager *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtResequenceManagerMethods  m_AtResequenceManagerOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaBitMask AllEnginesBitMask(AtResequenceManager self)
    {
    if (mThis(self)->engineMask == NULL)
        mThis(self)->engineMask = ThaBitMaskNew(AtResequenceManagerNumEngines(self));
    return mThis(self)->engineMask;
    }

static eAtRet EngineUse(AtResequenceManager self, AtResequenceEngine engine)
    {
    uint32 engineId = AtResequenceEngineIdGet(engine);
    ThaBitMaskSetBit(AllEnginesBitMask(self), engineId);

    return cAtOk;
    }

static AtResequenceEngine FreeEngineGet(AtResequenceManager self)
    {
    AtResequenceEngine engine = NULL;
    ThaBitMask allEnginesMask = AllEnginesBitMask(self);
    uint32 freeEngineId = ThaBitMaskFirstZeroBit(allEnginesMask);

    if (!ThaBitMaskBitPositionIsValid(allEnginesMask, freeEngineId))
        {
        AtDeviceLog(AtResequenceManagerDevice(self), cAtLogLevelCritical,
                    __FILE__, __LINE__, "Cannot found any free engineId \r\n");
        return NULL;
        }

    engine = AtResequenceManagerEngineGet(self, freeEngineId);
    if (engine == NULL)
        {
        AtDeviceLog(AtResequenceManagerDevice(self), cAtLogLevelCritical,
                    __FILE__, __LINE__, "Cannot get engine from ID = %u \r\n", freeEngineId);;
        return NULL;
        }

    AtResequenceManagerEngineUse(self, engine);
    return engine;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->engineMask);
    mThis(self)->engineMask = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tThaResequenceManager* object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(engineMask);
    }

static eAtRet EngineUnuse(AtResequenceManager self, AtResequenceEngine engine)
    {
    ThaBitMask mask = AllEnginesBitMask(self);
    uint32 engineId = AtResequenceEngineIdGet(engine);

    ThaBitMaskClearBit(mask, engineId);
    return cAtOk;
    }

static void Debug(AtResequenceManager self)
    {
    AtPrintc(cSevInfo, "* Engines allocation\r\n");
    AtPrintc(cSevNormal, "    Total engines     : "); AtPrintc(cSevInfo, "%u\r\n", AtResequenceManagerNumEngines(self));
    AtPrintc(cSevNormal, "    Next free engines : "); AtPrintc(cSevInfo, "%u\r\n", ThaBitMaskFirstZeroBit(AllEnginesBitMask(self)));
    AtPrintc(cSevNormal, "    Engines bit mask  : \r\n"); ThaBitMaskDebug(AllEnginesBitMask(self));
    }

static void OverrideAtResequenceManager(AtResequenceManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtResequenceManagerOverride, mMethodsGet(self), sizeof(tAtResequenceManagerMethods));

        mMethodOverride(m_AtResequenceManagerOverride, EngineUse);
        mMethodOverride(m_AtResequenceManagerOverride, FreeEngineGet);
        mMethodOverride(m_AtResequenceManagerOverride, EngineUnuse);
        mMethodOverride(m_AtResequenceManagerOverride, Debug);
        }

    mMethodsSet(self, &m_AtResequenceManagerOverride);
    }

static void OverrideAtObject(AtResequenceManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtResequenceManager self)
    {
    OverrideAtResequenceManager(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaResequenceManager);
    }

AtResequenceManager ThaResequenceManagerObjectInit(AtResequenceManager self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtResequenceManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtResequenceManager ThaResequenceManagerNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtResequenceManager manager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (manager == NULL)
        return NULL;

    return ThaResequenceManagerObjectInit(manager, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaHdlcChannel.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : HDLC channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHDLCCHANNEL_H_
#define _THAHDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcChannel.h" /* Super class */
#include "AtPwHdlc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHdlcChannel * ThaHdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtHdlcChannel ThaHdlcChannelNew(uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module);
uint8 ThaHdlcChannelCircuitSliceGet(ThaHdlcChannel self);
eBool ThaHdlcChannelCircuitIsHo(ThaHdlcChannel self);
eAtRet ThaHdlcChannelHdlcPwPayloadTypeSet(ThaHdlcChannel self, eAtPwHdlcPayloadType payloadType);
AtHdlcChannel ThaHdlcChannelPhysicalChannelGet(AtHdlcChannel self);
uint32 ThaHdlcChannelSurCounterId(ThaHdlcChannel self);

/* Product concretes */
AtHdlcChannel Tha60031032HdlcChannelNew(uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module);

#ifdef __cplusplus
}
#endif
#endif /* _THAHDLCCHANNEL_H_ */


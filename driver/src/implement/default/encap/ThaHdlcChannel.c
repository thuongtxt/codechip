/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encapsulation
 *
 * File        : ThaHdlcChannel.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa HDLC channel implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../../generic/encap/AtModuleEncapInternal.h"
#include "../../../generic/eth/AtEthFlowInternal.h"
#include "../ppp/ThaPppLinkInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../eth/ThaEthFlow.h"
#include "../eth/controller/ThaEthFlowController.h"
#include "ThaModuleEncapReg.h"
#include "ThaHdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSupportedMtu 9600
#define cThaPosIdle7EPattern 0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaHdlcChannel)self)
#define mChannelCtrlOffset(self) mMethodsGet((ThaHdlcChannel)self)->ChannelCtrlOffset((ThaHdlcChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/
enum eThaHdlcChannelCounterType
    {
    cThaHdlcChannelTxGoodPkt,        /**< Encap good packet */
    cThaHdlcChannelTxAbortPkt,       /**< Encap Abort packet */
    cThaHdlcChannelRxGoodPkt,        /**< Decap good packet*/
    cThaHdlcChannelRxAbortPkt,       /**< Decap Abort packet*/
    cThaHdlcChannelRxFcsErrPkt,      /**< Decap Fcs error packet*/
    cThaHdlcChannelRxAddrCtrlErrPkt, /**< Decap Address Control error packet*/
    cThaHdlcChannelRxSapiErrPkt,     /**< Decap Sapi error packet*/
    cThaHdlcChannelRxErrPkt          /**< Decap error packet*/
    };

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaHdlcChannelMethods m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;
static tAtHdlcChannelMethods  m_AtHdlcChannelOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FcsModeIsValid(eAtHdlcFcsMode fcsMode)
    {
    return (fcsMode == cAtHdlcFcsModeNoFcs) ||
           (fcsMode == cAtHdlcFcsModeFcs16) ||
           (fcsMode == cAtHdlcFcsModeFcs32);
    }

static eBool SapiModeIsValid(eThaPosSapiMd sapiMode)
    {
    return (sapiMode == cThaPosSapi1byte) ||
           (sapiMode == cThaPosSapi2byte) ||
           (sapiMode == cThaPosSapiDisable);
    }

static eAtRet SapiModeSet(ThaHdlcChannel self, eThaPosSapiMd sapiMode)
    {
    uint32 regAddr;
    uint32 regVal;
    eBool sapiIns;

    /* Check FCS mode */
    if (!SapiModeIsValid(sapiMode))
        return cAtErrorModeNotSupport;

    /* Configure Flag mode to insert */
    regAddr = cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    /* Sapi insertion enable*/
    sapiIns = (sapiMode == cThaPosSapiDisable) ? cAtFalse : cAtTrue;
    mFieldIns(&regVal,
              cThaRegDatEncapPosSapiInsMask,
              cThaRegDatEncapPosSapiInsShift,
              sapiIns);
    mFieldIns(&regVal,
              cThaRegDatEncapPosSapiModeMask,
              cThaRegDatEncapPosSapiModeShift,
              sapiMode);

    /* Apply */
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet FlagModeSet(ThaHdlcChannel self, uint8 flagMode)
    {
    uint32 address  = cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self);
    uint32 regValue = mChannelHwRead(self, address, cAtModuleEncap);

    mRegFieldSet(regValue, cThaRegDatEncapPosFlagMode, flagMode);
    mChannelHwWrite(self, address, regValue, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)self;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);

    /* Additional initialization */
    ret |= mMethodsGet(mThis(self))->SapiModeSet(mThis(self), cThaPosSapiDisable);

    /* Disable ACFC as default, ignore error code because not all of products
     * support this */
    AtHdlcChannelTxAddressControlCompress(hdlcChannel, cAtFalse);
    AtHdlcChannelRxAddressControlCompress(hdlcChannel, cAtFalse);

    mMethodsGet(mThis(self))->FlagModeSet(mThis(self), cThaRegDatEncapPosFlagModeOneFlag);

    return ret;
    }

static ThaModuleEncap EncapModule(ThaHdlcChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static uint32 ChannelPartOffset(ThaHdlcChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset((ThaDevice)device, cAtModuleEncap, ThaHdlcChannelPartOfEncap(EncapModule(self), self));
    }

static uint32 DefaultOffset(ThaHdlcChannel channel)
    {
    return AtChannelHwIdGet((AtChannel)channel) + ChannelPartOffset(channel);
    }

static uint32 ChannelCtrlOffset(ThaHdlcChannel channel)
    {
    return DefaultOffset(channel);
    }

/* Default implementation of deleting an object */
static void Delete(AtObject self)
    {

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if ((phyChannel == NULL) && enable)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool txTrafficEnable)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if (phyChannel == NULL)
        return (txTrafficEnable) ? cAtModuleEncapErrorNoBoundChannel : cAtOk;

    return mMethodsGet(phyChannel)->TxEncapConnectionEnable(phyChannel, txTrafficEnable);
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if (phyChannel == NULL)
        return cAtFalse;

    return mMethodsGet(phyChannel)->TxEncapConnectionIsEnabled(phyChannel);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool rxTrafficEnable)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if (phyChannel == NULL)
        return (rxTrafficEnable) ? cAtModuleEncapErrorNoBoundChannel : cAtOk;

    return mMethodsGet(phyChannel)->RxEncapConnectionEnable(phyChannel, rxTrafficEnable);
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if (phyChannel == NULL)
        return cAtFalse;

    return mMethodsGet(phyChannel)->RxEncapConnectionIsEnabled(phyChannel);
    }

static eBool IsEnabled(AtChannel self)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    if (phyChannel == NULL)
        return cAtFalse;

    return mMethodsGet(phyChannel)->RxEncapConnectionIsEnabled(phyChannel) &&
           mMethodsGet(phyChannel)->TxEncapConnectionIsEnabled(phyChannel);
    }

static uint32 DefectGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
	AtUnused(enableMask);
	AtUnused(defectMask);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    uint32 regAddr;
    uint32 partOffset    = ChannelPartOffset((ThaHdlcChannel)self);
    uint32 counterOffset = DefaultOffset((ThaHdlcChannel)self);

    switch (counterType)
        {
        /* Get values counter */
        case cThaHdlcChannelTxGoodPkt:
            regAddr = cThaRegDatEncPerCntRO1 + counterOffset;
            break;
        case cThaHdlcChannelTxAbortPkt:
            regAddr = cThaRegDatEncPerCntRO2 + counterOffset;
            break;
        case cThaHdlcChannelRxGoodPkt:
            regAddr = cThaRegDatDecPerCntRo + counterOffset;
            break;

        /* Other counters will be valid after reading cThaRegDatDecPerCnt at first cycle */
        case cThaHdlcChannelRxAbortPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntRo + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt2 + partOffset;
            break;
        case cThaHdlcChannelRxFcsErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntRo + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt3 + partOffset;
            break;
        case cThaHdlcChannelRxAddrCtrlErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntRo + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt4 + partOffset;
            break;
        case cThaHdlcChannelRxSapiErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntRo + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt5 + partOffset;
            break;
        case cThaHdlcChannelRxErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntRo + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt6 + partOffset;
            break;

        /* Not supported counter */
        default:
            return 0;
        }

    return mChannelHwRead(self, regAddr, cAtModuleEncap);
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    uint32 addrGoodPkt, addrAbortPkt, addrDecCount;
    tAtHdlcChannelCounters *counters = ((tAtHdlcChannelCounters *)pAllCounters);
    ThaModuleEncap encapModule;
    uint32 partOffset, channelOffset;
    AtOsal osal;

    /* NULL counter structure has not been handled yet */
    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(tAtHdlcChannelCounters));
    partOffset    = ChannelPartOffset(mThis(self));
    channelOffset = DefaultOffset(mThis(self));

    addrGoodPkt  = clear ? cThaRegDatEncPerCntR2C1 : cThaRegDatEncPerCntRO1;
    addrAbortPkt = clear ? cThaRegDatEncPerCntR2C2 : cThaRegDatEncPerCntRO2;
    addrDecCount = clear ? cThaRegDatDecPerCntR2c  : cThaRegDatDecPerCntRo;
    
    counters->txGoodPkt        = mChannelHwRead(self, addrGoodPkt  + channelOffset, cAtModuleEncap);
    counters->txAbortPkt       = mChannelHwRead(self, addrAbortPkt + channelOffset, cAtModuleEncap);
    counters->rxGoodPkt        = mChannelHwRead(self, addrDecCount + channelOffset, cAtModuleEncap);
    counters->rxAbortPkt       = mChannelHwRead(self, cThaRegDatDecPerCnt2 + partOffset, cAtModuleEncap);
    counters->rxFcsErrPkt      = mChannelHwRead(self, cThaRegDatDecPerCnt3 + partOffset, cAtModuleEncap);
    counters->rxAddrCtrlErrPkt = mChannelHwRead(self, cThaRegDatDecPerCnt4 + partOffset, cAtModuleEncap);
    counters->rxSapiErrPkt     = mChannelHwRead(self, cThaRegDatDecPerCnt5 + partOffset, cAtModuleEncap);
    counters->rxErrPkt         = mChannelHwRead(self, cThaRegDatDecPerCnt6 + partOffset, cAtModuleEncap);

    encapModule = (ThaModuleEncap)AtChannelModuleGet(self);
    if (ThaModuleEncapHdlcByteCountersAreSupported(encapModule))
        {
        counters->txByte     = mChannelHwRead(self, ThaModuleEncapDataEncapPerByteCounterAddress(encapModule, clear) + channelOffset, cAtModuleEncap);
        counters->rxByte     = mChannelHwRead(self, ThaModuleEncapDataDecapPerByteCounterAddress(encapModule, clear) + channelOffset, cAtModuleEncap);
        }

    if (ThaModuleEncapHdlcIdleByteCountersAreSupported(encapModule))
        {
        counters->txIdleByte = mChannelHwRead(self, ThaModuleEncapDataEncapPerIdleByteCounterAddress(encapModule, clear) + channelOffset, cAtModuleEncap);
        counters->rxIdleByte = mChannelHwRead(self, ThaModuleEncapDataDecapPerIdleByteCounterAddress(encapModule, clear) + channelOffset, cAtModuleEncap);
        }

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    uint32 regAddr;
    uint32 partOffset    = ChannelPartOffset((ThaHdlcChannel)self);
    uint32 counterOffset = DefaultOffset((ThaHdlcChannel)self);

    switch (counterType)
        {
        /* Get values counter */
        case cThaHdlcChannelTxGoodPkt:
            regAddr = cThaRegDatEncPerCntR2C1 + counterOffset;
            break;
        case cThaHdlcChannelTxAbortPkt:
            regAddr = cThaRegDatEncPerCntR2C2 + counterOffset;
            break;
        case cThaHdlcChannelRxGoodPkt:
            regAddr = cThaRegDatDecPerCntR2c + counterOffset;
            break;
        case cThaHdlcChannelRxAbortPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntR2c + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt2 + partOffset;
            break;
        case cThaHdlcChannelRxFcsErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntR2c + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt3 + partOffset;
            break;
        case cThaHdlcChannelRxAddrCtrlErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntR2c + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt4 + partOffset;
            break;
        case cThaHdlcChannelRxSapiErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntR2c + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt5 + partOffset;
            break;
        case cThaHdlcChannelRxErrPkt:
            mChannelHwRead(self, cThaRegDatDecPerCntR2c + counterOffset, cAtModuleEncap);
            regAddr = cThaRegDatDecPerCnt6 + partOffset;
            break;

            /* Invalid counter */
        default:
            return 0;
        }

    return mChannelHwRead(self, regAddr, cAtModuleEncap);
    }

static eBool IsHighBandwidth(ThaHdlcChannel self)
    {
    return self->isHighBandwidth;
    }

static eAtRet HardwareLinkRemove(AtEncapChannel self)
    {
    eAtRet ret;
    AtPppLink pppLink;
    uint16 numBlocks;
    AtChannel boundPhy = AtEncapChannelBoundPhyGet(self);

    pppLink  = (AtPppLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    if (pppLink == NULL)
        return cAtOk;

    ThaPppLinkHwTxTrafficEnable(pppLink, cAtFalse);

    ret = AtPppLinkPhaseSet(pppLink, cAtPppLinkPhaseDead);

    /* Remove in hardware */
    ret |= ThaPppLinkHardwareRemove(pppLink);

    /* De-allocate block */
    if (boundPhy)
        {
        numBlocks = mMethodsGet(boundPhy)->NumBlocksNeedToBindToEncapChannel(boundPhy, self);
        ret |= ThaPppLinkBlockDeAllocate(pppLink, numBlocks, IsHighBandwidth(mThis(self)));
        }

    return ret;
    }

static void LinkTxTrafficEnable(AtEncapChannel self)
    {
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    if (link == NULL)
        return;

    if (AtHdlcLinkTxTrafficIsEnabled(link))
        ThaPppLinkHwTxTrafficEnable((AtPppLink)link, cAtTrue);
    }

static eAtRet BlockDefaultRangeSet(AtEncapChannel self, AtChannel phyChannel)
    {
    uint8 queueId;
    eAtRet ret      = cAtOk;
    uint8 rangId    = AtChannelPppBlockRange(phyChannel);
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);

    for (queueId = 0; queueId < ThaPppLinkMaxNumQueues((ThaPppLink)link); queueId++)
        ret |= ThaPppLinkQueueBlockSizeSet(link, queueId, rangId);

    return ret;
    }

static eBool ContainsPppLink(AtHdlcChannel self)
    {
    eAtHdlcFrameType frameType;

    frameType = AtHdlcChannelFrameTypeGet(self);
    if (frameType == cAtHdlcFrmPpp)
        return cAtTrue;

    return cAtFalse;
    }

static AtPppLink PppLink(AtHdlcChannel self)
    {
    if (!ContainsPppLink(self))
        return NULL;

    return (AtPppLink)AtHdlcChannelHdlcLinkGet(self);
    }

static eAtRet FlowControlThresholdInit(AtEncapChannel self)
    {
    AtHdlcLink hdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    ThaEthFlow flow     = (ThaEthFlow)AtHdlcLinkBoundFlowGet(hdlcLink);
    ThaEthFlowController flowController = ThaEthFlowControllerGet(flow);

    if (flow)
        ThaEthFlowControllerFlowControlThresholdInit(flowController, (AtEthFlow)flow, (AtChannel)hdlcLink);

    return cAtOk;
    }

static eAtRet PhyBind(AtEncapChannel self, AtChannel phyChannel)
    {
    eAtRet ret;
    AtChannel boundPhyChannel;

    /* Get current bound physical channel */
    boundPhyChannel = AtEncapChannelBoundPhyGet(self);

    /* Additional binding */
    /* Bind */
    if (phyChannel)
        {
        /* If this channel is already bound to this physical interface, do nothing */
        if (boundPhyChannel == phyChannel)
            return cAtOk;

        /* This encap channel is busy with another physical channel */
        if (boundPhyChannel != NULL)
            return cAtErrorChannelBusy;

        /* Update to private data */
        AtEncapChannelPhysicalChannelSet(self, phyChannel);

        ret = mMethodsGet((AtChannel)phyChannel)->BindToEncapChannel(phyChannel, self);

        /* Enable Tx and Rx traffic if it is enabled before */
        if (ret == cAtOk)
            {
            AtHdlcLink link;
            LinkTxTrafficEnable(self);

            /* IMPORTANT: cHDLC link also needs to set to Network Active. The
             * previous version just activate for PPP link */
            link = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
            if (link != NULL)
                ret |= AtPppLinkPhaseSet((AtPppLink)link, cAtPppLinkPhaseNetworkActive);
            }

        ret |= BlockDefaultRangeSet(self, phyChannel);
        ret |= FlowControlThresholdInit(self);

        return ret;
        }

    /* Unbind */
    else
        {
        /* This encap channel is currently free, do nothing */
        if (boundPhyChannel == NULL)
            return cAtOk;

        ret = HardwareLinkRemove(self);
        if (ret != cAtOk)
            return ret;

        /* Update to private data */
        AtEncapChannelPhysicalChannelSet(self, NULL);

        return mMethodsGet(boundPhyChannel)->BindToEncapChannel(boundPhyChannel, NULL);
        }
    }

static eAtEncapType EncapTypeGet(AtEncapChannel self)
    {
	AtUnused(self);
    return cAtEncapHdlc;
    }

static eAtRet AddressSizeSet(AtHdlcChannel self, uint8 addressSize)
    {
	AtUnused(addressSize);
	AtUnused(self);
    /* Do nothing, HW just support 1 byte address */
    return cAtOk;
    }

static uint8 AddressSizeGet(AtHdlcChannel self)
    {
	AtUnused(self);
    return 1;
    }

static eAtRet AddressControlCompareEnable(AtHdlcChannel self, eBool compareEnable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegDatDecOptCtrl1 + mChannelCtrlOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal,
              cThaRegDatDecPosAdrCtrlCompEnMask,
              cThaRegDatDecPosAdrCtrlCompEnShift,
              mBoolToBin(compareEnable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet ExpectedAddressSet(AtHdlcChannel self, uint8 *expectedAddress)
    {
    uint32 regAddr, regVal;

    /* The current hardware only support one byte address */
    if (expectedAddress != NULL)
        {
        regAddr = cThaRegDatDecOptCtrl2 + mChannelCtrlOffset(self);
        regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
        mFieldIns(&regVal, cThaRegDatDecAddValMask, cThaRegDatDecAddValShift, expectedAddress[0]);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
        }

    return cAtOk;
    }

static eAtRet AddressMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    /* Enable/disable comparison */
    return AddressControlCompareEnable(self, compareEnable);
    }

static eBool AddressControlCompareIsEnabled(AtHdlcChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegDatDecOptCtrl1 + mChannelCtrlOffset(self), cAtModuleEncap);
    return (regVal & cThaRegDatDecPosAdrCtrlCompEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet ExpectedAddressGet(AtHdlcChannel self, uint8 *address)
    {
    uint32 regVal;

    /* Get address */
    if (address != NULL)
        {
        regVal = mChannelHwRead(self, cThaRegDatDecOptCtrl2 + mChannelCtrlOffset(self), cAtModuleEncap);
        mFieldGet(regVal, cThaRegDatDecAddValMask, cThaRegDatDecAddValShift, uint8, &(address[0]));
        }

    return cAtOk;
    }

static eBool AddressMonitorIsEnabled(AtHdlcChannel self)
    {
    /* Comparison enabling */
    return AddressControlCompareIsEnabled(self);
    }

static eAtRet AddressControlInsertionEnable(ThaHdlcChannel self, eBool insertEnable)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cThaRegDatEncapPosCtrlInsMask, cThaRegDatEncapPosCtrlInsShift, mBoolToBin(insertEnable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool AddressControlInsertionIsEnabled(ThaHdlcChannel self)
    {
    uint32 regVal;

    regVal = mChannelHwRead(self, cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self), cAtModuleEncap);
    return (regVal & cThaRegDatEncapPosCtrlInsMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxAddressSet(AtHdlcChannel self, uint8 *address)
    {
    uint32 regAddr;
    uint32 regVal;

    /* Configure TX address. Hardware only supports one byte address */
    if (address != NULL)
        {
        regAddr = cThaRegDatEncOptCtrl2 + mChannelCtrlOffset(self);
        regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
        mFieldIns(&regVal,
                  cThaRegDatEncapAddValMask,
                  cThaRegDatEncapAddValShift,
                  address[0]);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
        }

    return cAtOk;
    }

static eAtRet AddressInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    AtPppLink link;

    /* Enable/disable Address insertion */
    mMethodsGet(mThis(self))->AddressControlInsertionEnable(mThis(self), insertEnable);

    /* Configure its PPP link */
    link = PppLink(self);
    if (link)
        ThaPppLinkAddressControlBypass(link, insertEnable ? cAtFalse : cAtTrue);

    return cAtOk;
    }

static eAtRet TxAddressGet(AtHdlcChannel self, uint8 *insertedAddress)
    {
    uint32 regAddr;
    uint32 regVal;

    /* Get transmitted address value. Note, hardware only supports one byte address */
    if (insertedAddress != NULL)
        {
        regAddr = cThaRegDatEncOptCtrl2 + mChannelCtrlOffset(self);
        regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
        mFieldGet(regVal,
                  cThaRegDatEncapAddValMask,
                  cThaRegDatEncapAddValShift,
                  uint8,
                  &(insertedAddress[0]));
        }

    return cAtOk;
    }

static eBool AddressInsertionIsEnabled(AtHdlcChannel self)
    {
    /* Enabling */
    return mMethodsGet(mThis(self))->AddressControlInsertionIsEnabled(mThis(self));
    }

static eAtRet ControlSizeSet(AtHdlcChannel self, uint8 controlSize)
    {
	AtUnused(controlSize);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8 ControlSizeGet(AtHdlcChannel self)
    {
	AtUnused(self);
    return 1;
    }

static eAtRet ExpectedControlSet(AtHdlcChannel self, uint8 * expectedControl)
    {
    uint32 regAddr;
    uint32 regVal;

    /* The current hardware only support one byte control */
    if (expectedControl != NULL)
        {
        regAddr = cThaRegDatDecOptCtrl2 + mChannelCtrlOffset(self);
        regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
        mFieldIns(&regVal,
                  cThaRegDatDecCtrlValMask,
                  cThaRegDatDecCtrlValShift,
                  expectedControl[0]);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
        }

    return cAtOk;
    }

static eAtRet ControlMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    /* Enable/disable comparison */
    return AddressControlCompareEnable(self, compareEnable);
    }

static eAtRet ExpectedControlGet(AtHdlcChannel self, uint8 *control)
    {
    uint32 regAddr;
    uint32 regVal;

    /* Get expected control */
    if (control != NULL)
        {
        regAddr = cThaRegDatDecOptCtrl2 + mChannelCtrlOffset(self);
        regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

        /* The current hardware only support one byte control */
        mFieldGet(regVal,
                  cThaRegDatDecCtrlValMask,
                  cThaRegDatDecCtrlValShift,
                  uint8,
                  &(control[0]));
        }

    return cAtOk;
    }

static eBool ControlMonitorIsEnabled(AtHdlcChannel self)
    {
    /* Enabling */
    return AddressControlCompareIsEnabled(self);
    }

static eAtRet TxControlSet(AtHdlcChannel self, uint8 *control)
    {
    uint32 regAddr;
    uint32 regVal;

    /* Set transmitted control value. The current hardware only support one byte control */
    if (control != NULL)
        {
        regAddr = cThaRegDatEncOptCtrl2 + mChannelCtrlOffset(self);
        regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
        mFieldIns(&regVal,
                  cThaRegDatEncapCtrlValMask,
                  cThaRegDatEncapCtrlValShift,
                  control[0]);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
        }

    return cAtOk;
    }

static eAtRet ControlInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    /* Enable/disable insertion */
    return mMethodsGet(mThis(self))->AddressControlInsertionEnable(mThis(self), insertEnable);
    }

static eAtRet TxControlGet(AtHdlcChannel self, uint8 *insertedControl)
    {
    uint32 regAddr;
    uint32 regVal;

    /* Get transmitted control value */
    if (insertedControl != NULL)
        {
        regAddr = cThaRegDatEncOptCtrl2 + mChannelCtrlOffset(self);
        regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

        /* The current hardware only support one byte control */
        mFieldGet(regVal,
                  cThaRegDatEncapCtrlValMask,
                  cThaRegDatEncapCtrlValShift,
                  uint8,
                  &(insertedControl[0]));
        }

    return cAtOk;
    }

static eBool ControlInsertionIsEnabled(AtHdlcChannel self)
    {
    /* Enabling */
    return mMethodsGet(mThis(self))->AddressControlInsertionIsEnabled(mThis(self));
    }

static eAtRet FlagSet(AtHdlcChannel self, uint8 flag)
    {
    uint32 regAddr;
    uint32 regVal;

    switch (flag)
        {
        case 0xFF:
            flag = 1;
            break;
        case 0x7E:
            flag = 0;
            break;
        default:
            return cAtErrorModeNotSupport;
        }

    /* Configure Flag mode to insert */
    regAddr = cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mRegFieldSet(regVal, cThaRegDatEncapPosIdleMode, flag);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint8 FlagGet(AtHdlcChannel self)
    {
    uint32 regAddr = cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    uint8 flag = (uint8)mRegField(regVal, cThaRegDatEncapPosIdleMode);

    return ((flag == 1) ? 0xFF : 0X7E);
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint32 regAddr;
    uint32 regVal;
    uint8 hwFcsMode, fcsEnabled;
    AtPppLink link;

    /* Check FCS mode */
    if (!FcsModeIsValid(fcsMode))
        return cAtErrorModeNotSupport;

    /* Configure ENC */
    regAddr = cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    fcsEnabled = (fcsMode == cAtHdlcFcsModeNoFcs) ? 0 : 1;
    mFieldIns(&regVal, cThaRegDatEncapPosFcsInsMask, cThaRegDatEncapPosFcsInsShift, fcsEnabled);
    hwFcsMode = (fcsMode == cAtHdlcFcsModeFcs32) ? 1 : 0;
    mFieldIns(&regVal, cThaRegDatEncapPosFcsMdMask, cThaRegDatEncapPosFcsMdShift, hwFcsMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* Configure DEC */
    regAddr = cThaRegDatDecOptCtrl1 + mChannelCtrlOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cThaRegDatDecPosFcsChkMask, cThaRegDatDecPosFcsChkShift, fcsEnabled);
    mFieldIns(&regVal, cThaRegDatDecPosFcsMdMask, cThaRegDatDecPosFcsMdShift, hwFcsMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* Configure its PPP link */
    link = PppLink(self);
    if (link)
        {
        eBool bypass = (fcsMode == cAtHdlcFcsModeNoFcs) ? cAtTrue : cAtFalse;
        return ThaPppLinkFcsErrorBypass(link, bypass);
        }

    return cAtOk;
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
    uint32 regVal;
    uint8 fcsMode;

    /* Check if FCS is enabled */
    regVal = mChannelHwRead(self, cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self), cAtModuleEncap);
    if ((regVal & cThaRegDatEncapPosFcsInsMask) == 0)
        return cAtHdlcFcsModeNoFcs;

    /* Get FCS mode */
    mFieldGet(regVal,
              cThaRegDatEncapPosFcsMdMask,
              cThaRegDatEncapPosFcsMdShift,
              uint8,
              &fcsMode);

    return (fcsMode == 1) ? cAtHdlcFcsModeFcs32 : cAtHdlcFcsModeFcs16;
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 offset;
    uint8 hwStuffing;

    if ((stuffMode != cAtHdlcStuffBit) && (stuffMode != cAtHdlcStuffByte))
        return cAtErrorModeNotSupport;

    /* Configure Stuff mode for ENC direction */
    offset = mChannelCtrlOffset(self);
    hwStuffing = (stuffMode == cAtHdlcStuffBit) ? 1 : 0;
    regAddr = cThaRegDatEncOptCtrl1 + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cThaRegDatEncapStuffingModeMask, cThaRegDatEncapStuffingModeShift, hwStuffing);

    /* Additional configuration for bit stuffing */
    if (stuffMode == cAtHdlcStuffBit)
        {
        mFieldIns(&regVal, cThaRegDatEncapPosRate56KMask, cThaRegDatEncapPosRate56KShift, 0);
        mFieldIns(&regVal, cThaRegDatEncapPosIdleModeMask, cThaRegDatEncapPosIdleModeShift, cThaPosIdle7EPattern);
        }

    /* Apply for ENC direction */
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* Configure stuffing mode for DEC direction */
    regAddr = cThaRegDatDecOptCtrl1 + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cThaRegDatDecStuffingModeMask, cThaRegDatDecStuffingModeShift, hwStuffing);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
    uint32 regAddr;
    uint32 regVal;
    uint8 stuffMode;

    regAddr = cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);

    mFieldGet(regVal,
              cThaRegDatEncapStuffingModeMask,
              cThaRegDatEncapStuffingModeShift,
              uint8,
              &stuffMode);
    return (stuffMode == 1) ? cAtHdlcStuffBit : cAtHdlcStuffByte;
    }

static eAtRet MtuSet(AtHdlcChannel self, uint32 mtu)
    {
	AtUnused(self);
    /* Only MTU 9600 is supported */
    if (mtu == cSupportedMtu)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint32 MtuGet(AtHdlcChannel self)
    {
	AtUnused(self);
    return cSupportedMtu;
    }

static void LinkOamFlowBackup(ThaHdlcChannel self)
    {
    AtOsal osal;
    uint8 mac[cAtMacAddressLen];
    tAtEthVlanDesc vlanDesc;
    AtEthFlow oamFlow = AtHdlcLinkOamFlowGet(AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self));

    if (oamFlow == NULL)
        {
        self->oamFlowIsCreated = cAtFalse;
        return;
        }

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, mac, 0, sizeof(mac));
    AtEthFlowEgressDestMacGet(oamFlow, mac);
    mMethodsGet(osal)->MemCpy(osal, self->oamDestMac, mac, sizeof(mac));

    mMethodsGet(osal)->MemInit(osal, mac, 0, sizeof(mac));
    AtEthFlowEgressSourceMacGet(oamFlow, mac);
    mMethodsGet(osal)->MemCpy(osal, self->oamSourceMac, mac, sizeof(mac));

    mMethodsGet(osal)->MemInit(osal, &vlanDesc, 0, sizeof(vlanDesc));
    AtEthFlowEgressVlanGet(oamFlow, &vlanDesc);
    mMethodsGet(osal)->MemCpy(osal, &(self->oamVlanDesc), &vlanDesc, sizeof(vlanDesc));

    self->oamFlowIsCreated = cAtTrue;
    }

static eAtRet LinkOamFlowRestore(ThaHdlcChannel self)
    {
    eAtRet ret = cAtOk;
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    ThaHdlcChannel thisChannel = mThis(self);
    AtEthFlow oamFlow;

    if (!thisChannel->oamFlowIsCreated)
        return cAtOk;

    ret |= AtHdlcLinkOamPacketModeSet(link, thisChannel->packetMode);
    
    /* Restore configuration for this OAM flow */
    oamFlow = AtHdlcLinkOamFlowGet(link);
    ret |= AtEthFlowEgressDestMacSet(oamFlow, thisChannel->oamDestMac);
    ret |= AtEthFlowEgressSourceMacSet(oamFlow, thisChannel->oamSourceMac);
    ret |= AtEthFlowEgressVlanSet(oamFlow, &(thisChannel->oamVlanDesc));

    return ret;
    }

static void LinkBackup(AtHdlcChannel self)
    {
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet(self);
    ThaHdlcChannel thisChannel = mThis(self);

    thisChannel->flow        = AtHdlcLinkBoundFlowGet(link);
    thisChannel->linkPhase   = AtPppLinkPhaseGet((AtPppLink)link);
    thisChannel->txTrafficEn = AtHdlcLinkTxTrafficIsEnabled(link);
    thisChannel->rxTrafficEn = AtHdlcLinkRxTrafficIsEnabled(link);
    thisChannel->packetMode  = AtHdlcLinkOamPacketModeGet(link);

    LinkOamFlowBackup(thisChannel);
    }

static eAtRet LinkRestore(AtHdlcChannel self)
    {
    eAtRet ret = cAtOk;
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet(self);
    ThaHdlcChannel thisChannel = mThis(self);

    if (AtHdlcChannelFrameTypeGet(AtHdlcLinkHdlcChannelGet(link)) == cAtHdlcFrmPpp)
        ret |= AtPppLinkPhaseSet((AtPppLink)link, thisChannel->linkPhase);

    ret |= AtHdlcLinkFlowBind(link, thisChannel->flow);
    ret |= AtHdlcLinkTxTrafficEnable(link, thisChannel->txTrafficEn);
    ret |= AtHdlcLinkRxTrafficEnable(link, thisChannel->rxTrafficEn);
    ret |= LinkOamFlowRestore(thisChannel);

    return ret;
    }

static AtHdlcLink CreateLinkObject(AtHdlcChannel self, uint8 frameType)
    {
    AtHdlcLink link;
    AtModuleEncap moduleEncap = (AtModuleEncap)EncapModule((ThaHdlcChannel)self);

    if ((link = AtModuleEncapLinkObjectCreate(moduleEncap, self, frameType)) == NULL)
        return NULL;

    /* Save to database */
    AtHdlcChannelHdlcLinkSet(self, link);
    self->frameType = frameType;

    return link;
    }

static eAtRet LinkDelete(AtHdlcChannel self)
    {
    eAtRet ret;
    AtHdlcLink link = AtHdlcChannelHdlcLinkGet(self);

    /* Need to backup link configure */
    LinkBackup(self);

    /* Need disable traffic */
    ret = AtHdlcLinkTxTrafficEnable(link, cAtFalse);
    ret |= AtHdlcLinkRxTrafficEnable(link, cAtFalse);
    ret |= AtHdlcLinkFlowBind(link, NULL);

    /* Delete this link */
    AtObjectDelete((AtObject)link);
    AtHdlcChannelHdlcLinkSet(self, NULL);

    return ret;
    }

static eBool FrameTypeIsSupported(AtHdlcChannel self, uint8 frameType)
    {
	AtUnused(self);
    if ((frameType == cAtHdlcFrmPpp) || (frameType == cAtHdlcFrmCiscoHdlc))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    eAtRet ret;
    AtHdlcLink link;

    if (!FrameTypeIsSupported(self, frameType))
        return cAtErrorModeNotSupport;

    if (frameType == AtHdlcChannelFrameTypeGet(self))
        return cAtOk;

    link = AtHdlcChannelHdlcLinkGet(self);
    if (link == NULL)
        return cAtErrorInvlParm;

    /* Delete internal link object */
    ret = LinkDelete(self);
    if (ret != cAtOk)
        return ret;

    /* Create link accordance with frame type */
    link = CreateLinkObject(self, frameType);
    if (link == NULL)
        return cAtErrorNullPointer;

    /* Configure new link */
    AtChannelInit((AtChannel)link);
    ret = LinkRestore(self);
    return ret;
    }

static eAtRet AddressControlBypass(AtHdlcChannel self, eBool bypass)
    {
    AtPppLink link = PppLink(self);
    if (link == NULL)
        return cAtErrorModeNotSupport;

    return ThaPppLinkAddressControlBypass(link, bypass);
    }

static eBool AddressControlIsBypassed(AtHdlcChannel self)
    {
    AtPppLink link = PppLink(self);

    if (link == NULL)
        return cAtFalse;

    return ThaPppLinkAddressControlIsBypassed(link);
    }

static eAtRet AddressControlErrorBypass(AtHdlcChannel self, eBool bypass)
    {
    AtPppLink link = PppLink(self);

    if (link == NULL)
        return cAtErrorModeNotSupport;

    return ThaPppLinkAddressControlErrorBypass(link, bypass);
    }

static eBool AddressControlErrorIsBypassed(AtHdlcChannel self)
    {
    AtPppLink link = PppLink(self);
    if (link == NULL)
        return cAtFalse;

    return ThaPppLinkAddressControlErrorIsBypassed(link);
    }

static eAtRet FcsErrorBypass(AtHdlcChannel self, eBool bypass)
    {
    AtPppLink link = PppLink(self);
    if (link == NULL)
        return cAtErrorModeNotSupport;

    return ThaPppLinkFcsErrorBypass(link, bypass);
    }

static eBool FcsErrorIsBypassed(AtHdlcChannel self)
    {
    AtPppLink link = PppLink(self);
    if (link == NULL)
        return cAtFalse;

    return ThaPppLinkFcsErrorIsBypassed(link);
    }

static eBool CanChangePacketDroppingCondition(AtEncapChannel self, uint32 conditionMask)
    {
    AtUnused(self);
    if ((conditionMask == cAtHdlcChannelDropConditionPktAddrCtrlError) ||
        (conditionMask == cAtHdlcChannelDropConditionPktFcsError))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet DropPacketConditionMaskSet(AtEncapChannel self, uint32 conditionMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;
    AtHdlcChannel channel = (AtHdlcChannel)self;
    eBool isNotDropped;

    if (conditionMask & cAtHdlcChannelDropConditionPktAddrCtrlError)
        {
        isNotDropped = (enableMask & cAtHdlcChannelDropConditionPktAddrCtrlError) ? cAtFalse : cAtTrue;
        ret |= AddressControlErrorBypass(channel, isNotDropped);
        }

    if (conditionMask & cAtHdlcChannelDropConditionPktFcsError)
        {
        isNotDropped = (enableMask & cAtHdlcChannelDropConditionPktFcsError) ? cAtFalse : cAtTrue;
        ret |= FcsErrorBypass(channel, isNotDropped);
        }

    return ret;
    }

static uint32 DropPacketConditionMaskGet(AtEncapChannel self)
    {
    AtHdlcChannel channel = (AtHdlcChannel)self;
    uint32 conditions = 0;

    if (AddressControlErrorIsBypassed(channel) == cAtFalse)
        conditions |= cAtHdlcChannelDropConditionPktAddrCtrlError;

    if (FcsErrorIsBypassed(channel) == cAtFalse)
        conditions |= cAtHdlcChannelDropConditionPktFcsError;

    return conditions;
    }

static eAtRet ScrambleEnable(AtEncapChannel self, eBool scrambleEnable)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 offset;

    /* Encapsulation */
    offset = mChannelCtrlOffset(self);
    regAddr = cThaRegDatEncOptCtrl1 + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal,
              cThaRegDatEncapPosScrEnbMask,
              cThaRegDatEncapPosScrEnbShift,
              mBoolToBin(scrambleEnable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    /* Decapsulation */
    regAddr = cThaRegDatDecOptCtrl1 + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal,
              cThaRegDatDecPosScrEnbMask,
              cThaRegDatDecPosScrEnbShift,
              mBoolToBin(scrambleEnable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool ScrambleIsEnabled(AtEncapChannel self)
    {
    uint32 regVal;

    regVal = mChannelHwRead(self, cThaRegDatEncOptCtrl1 + mChannelCtrlOffset(self), cAtModuleEncap);
    return (regVal & cThaRegDatEncapPosScrEnbMask) ? cAtTrue : cAtFalse;
    }

static eAtRet AllConfigSet(AtChannel self, void *pAllConfig)
    {
    eAtRet ret;
    tAtHdlcChannelAllConfig *configs = (tAtHdlcChannelAllConfig *) pAllConfig;
    AtHdlcChannel channel = (AtHdlcChannel)self;

    ret = cAtOk;
    ret |= AddressMonitorEnable(channel, configs->addrCmpEn);
    ret |= ExpectedAddressSet(channel, &(configs->addrExpVal));
    ret |= AddressInsertionEnable(channel, configs->addrInsEn);
    ret |= TxAddressSet(channel, &(configs->addrInsVal));
    ret |= ControlMonitorEnable(channel, configs->ctrlCmpEn);
    ret |= ExpectedControlSet(channel, &(configs->ctrlExpVal));
    ret |= ControlInsertionEnable(channel, configs->ctrlInsEn);
    ret |= TxControlSet(channel, &(configs->ctrlInsVal));
    ret |= FlagSet(channel, configs->flag);
    ret |= FcsModeSet(channel, configs->fcsMode);
    ret |= StuffModeSet(channel, configs->stuffingMd);
    ret |= ScrambleEnable((AtEncapChannel)channel, configs->scrEn);

    return ret;
    }

static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
    eAtRet ret;
    tAtHdlcChannelAllConfig *configs = (tAtHdlcChannelAllConfig *) pAllConfig;
    AtHdlcChannel channel = (AtHdlcChannel)self;

    if (pAllConfig == NULL)
        return cAtErrorNullPointer;

    ret = ExpectedAddressGet(channel, &configs->addrExpVal);
    ret |= TxAddressGet(channel, &configs->addrInsVal);
    ret |= ExpectedControlGet(channel, &configs->ctrlExpVal);
    ret |= TxControlGet(channel, &configs->ctrlInsVal);
    configs->flag = FlagGet(channel);
    configs->fcsMode = FcsModeGet(channel);
    configs->stuffingMd = StuffModeGet(channel);
    configs->scrEn = ScrambleIsEnabled((AtEncapChannel)channel);
    return ret;
    }

static uint32 LocalHdlcChannelId(ThaModuleEncap self, ThaHdlcChannel channel)
    {
    uint32 numEncapsPerPart = ThaModuleEncapNumEncapPerPart(self);
    uint32 channelId = AtChannelIdGet((AtChannel)channel);
    return numEncapsPerPart ? (channelId % numEncapsPerPart) : channelId;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return LocalHdlcChannelId(EncapModule((ThaHdlcChannel)self), (ThaHdlcChannel)self);
    }

static eAtRet RxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet(self);
    if ((frameType == cAtHdlcFrmCiscoHdlc) || (frameType == cAtHdlcFrmFr))
        return cAtErrorModeNotSupport;

    return ThaPppLinkAddressControlCompress(PppLink(self), compress);
    }

static eBool RxAddressControlIsCompressed(AtHdlcChannel self)
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet(self);
    if ((frameType == cAtHdlcFrmCiscoHdlc) || (frameType == cAtHdlcFrmFr))
        return cAtFalse;

    return ThaPppLinkAddressControlIsCompressed(PppLink(self));
    }

static uint8 CircuitSliceGet(ThaHdlcChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eBool CircuitIsHo(ThaHdlcChannel self)
    {
    AtUnused(self);
    /* Concrete classes will know how to decide */
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHdlcChannel object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(isHighBandwidth);
    mEncodeUInt(linkPhase);
    mEncodeUInt(packetMode);
    mEncodeUInt(txTrafficEn);
    mEncodeUInt(rxTrafficEn);
    mEncodeObjectDescription(flow);
    mEncodeUInt(oamFlowIsCreated);
    mEncodeUInt8Array(oamSourceMac, cAtMacAddressLen);
    mEncodeUInt8Array(oamDestMac, cAtMacAddressLen);

    AtCoderEncodeObjectWithHandler(encoder, (AtObjectAny)&(object->oamVlanDesc), "oamVlanDesc", AtUtilVlanDescriptorSerialize);
    }

static eAtRet HdlcPwPayloadTypeSet(ThaHdlcChannel self, eAtPwHdlcPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(payloadType);
    return cAtErrorNotImplemented;
    }

static ThaHdlcChannel PhysicalChannelGet(ThaHdlcChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 SurCounterId(ThaHdlcChannel self)
    {
    return AtChannelHwIdGet((AtChannel)self);
    }

static void OverrideAtObject(AtHdlcChannel self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllConfigSet);
        mMethodOverride(m_AtChannelOverride, AllConfigGet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel encapChannel = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(encapChannel) ;
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(m_AtEncapChannelOverride));
        mMethodOverride(m_AtEncapChannelOverride, PhyBind);
        mMethodOverride(m_AtEncapChannelOverride, EncapTypeGet);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleEnable);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtEncapChannelOverride, CanChangePacketDroppingCondition);
        mMethodOverride(m_AtEncapChannelOverride, DropPacketConditionMaskSet);
        mMethodOverride(m_AtEncapChannelOverride, DropPacketConditionMaskGet);
        }

    mMethodsSet(encapChannel, &m_AtEncapChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, mMethodsGet(self), sizeof(tAtHdlcChannelMethods));
        mMethodOverride(m_AtHdlcChannelOverride, AddressSizeSet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressSizeGet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressGet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressGet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeSet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeGet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlGet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlGet);
        mMethodOverride(m_AtHdlcChannelOverride, FlagSet);
        mMethodOverride(m_AtHdlcChannelOverride, FlagGet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, MtuSet);
        mMethodOverride(m_AtHdlcChannelOverride, MtuGet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressControlBypass);
        mMethodOverride(m_AtHdlcChannelOverride, AddressControlIsBypassed);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlCompress);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlIsCompressed);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtHdlcChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ChannelCtrlOffset);
        mMethodOverride(m_methods, AddressControlInsertionEnable);
        mMethodOverride(m_methods, AddressControlInsertionIsEnabled);
        mMethodOverride(m_methods, SapiModeSet);
        mMethodOverride(m_methods, FlagModeSet);
        mMethodOverride(m_methods, CircuitSliceGet);
        mMethodOverride(m_methods, CircuitIsHo);
        mMethodOverride(m_methods, HdlcPwPayloadTypeSet);
        mMethodOverride(m_methods, PhysicalChannelGet);
        mMethodOverride(m_methods, SurCounterId);
        }

    mMethodsSet(self, &m_methods);
    }

AtHdlcChannel ThaHdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaHdlcChannel));

    /* Super constructor */
    if (AtHdlcChannelObjectInit((AtHdlcChannel) self, channelId, frameType, (AtModule) module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel ThaHdlcChannelNew(uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaHdlcChannel));
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ThaHdlcChannelObjectInit(newChannel, channelId, frameType, module);
    }

eAtRet ThaHdlcChannelHighBandwidthSet(ThaHdlcChannel self, eBool isHighBandwidth)
    {
    self->isHighBandwidth = isHighBandwidth;
    return cAtOk;
    }

uint8 ThaHdlcChannelPartOfEncap(ThaModuleEncap self, ThaHdlcChannel channel)
    {
    uint32 numEncaps = ThaModuleEncapNumEncapPerPart(self);
    if (numEncaps == 0)
        return 0;
    return (uint8)(AtChannelIdGet((AtChannel)channel) / numEncaps);
    }

uint8 ThaHdlcChannelCircuitSliceGet(ThaHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->CircuitSliceGet(self);
    return 0;
    }

eBool ThaHdlcChannelCircuitIsHo(ThaHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->CircuitIsHo(self);
    return cAtFalse;
    }

eAtRet ThaHdlcChannelHdlcPwPayloadTypeSet(ThaHdlcChannel self, eAtPwHdlcPayloadType payloadType)
    {
    if (self)
        return mMethodsGet(self)->HdlcPwPayloadTypeSet(self, payloadType);
    return cAtErrorNullPointer;
    }

AtHdlcChannel ThaHdlcChannelPhysicalChannelGet(AtHdlcChannel self)
    {
    if (self)
        return (AtHdlcChannel)mMethodsGet(mThis(self))->PhysicalChannelGet(mThis(self));
    return NULL;
    }

uint32 ThaHdlcChannelSurCounterId(ThaHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->SurCounterId(self);
    return cInvalidUint32;
    }

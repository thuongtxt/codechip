/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaCiscoHdlcLinkInternal.h
 * 
 * Created Date: Jan 23, 2013
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACISCOHDLCLINKINTERNAL_H_
#define _THACISCOHDLCLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ppp/ThaPppLinkInternal.h"
#include "../ppp/ThaStmPppLinkInternal.h"
#include "ThaCiscoHdlcLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCiscoHdlcLink * ThaCiscoHdlcLink;

typedef struct tThaCiscoHdlcLink
    {
    tThaPppLink super;

    /* Private data */
    }tThaCiscoHdlcLink;

/* This class */
typedef struct tThaStmCiscoHdlcLink * ThaStmCiscoHdlcLink;
typedef struct tThaStmCiscoHdlcLink
    {
    tThaStmPppLink super;

    /* Private data */
    }tThaStmCiscoHdlcLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPppLink ThaCiscoHdlcLinkObjectInit(ThaPppLink self, AtHdlcChannel hdlcChannel);
ThaStmPppLink ThaStmCiscoHdlcLinkObjectInit(ThaStmPppLink self, AtHdlcChannel hdlcChannel);
eAtRet ThaCiscoHdlcInit(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THACISCOHDLCLINKINTERNAL_H_ */


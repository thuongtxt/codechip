/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaCiscoHdlcLink.h
 * 
 * Created Date: Feb 28, 2015
 *
 * Description : cHDLC link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACISCOHDLCLINK_H_
#define _THACISCOHDLCLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ppp/ThaStmPppLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
ThaPppLink ThaCiscoHdlcLinkNew(AtHdlcChannel hdlcChannel);
ThaStmPppLink ThaStmCiscoHdlcLinkNew(AtHdlcChannel hdlcChannel);

/* Product concretes */
ThaPppLink Tha60091023CiscoHdlcLinkNew(AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THACISCOHDLCLINK_H_ */


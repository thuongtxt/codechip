/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaProfileManager.c
 *
 * Created Date: Jun 8, 2016
 *
 * Description : Traffic manager interface
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaProfileManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaProfileManagerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaProfileFinder ErrorProfileFinderObjectCreate(ThaProfileManager self)
    {
    return ThaErrorProfileFinderNew(self);
    }

static ThaProfileFinder NetworkProfileFinderObjectCreate(ThaProfileManager self)
    {
    return ThaNetworkProfileFinderNew(self);
    }

static ThaProfileFinder OamProfileFinderObjectCreate(ThaProfileManager self)
    {
    return ThaOamProfileFinderNew(self);
    }

static ThaProfileFinder BcpProfileFinderObjectCreate(ThaProfileManager self)
    {
    return ThaBcpProfileFinderNew(self);
    }

static ThaProfileFinder FcnProfileFinderObjectCreate(ThaProfileManager self)
    {
    return ThaFcnProfileFinderNew(self);
    }

static void FinderDelete(ThaProfileFinder *finder)
    {
    AtObjectDelete((AtObject)*finder);
    *finder = NULL;
    }

static void AllProfileFindersDelete(ThaProfileManager self)
    {
    FinderDelete(&self->errorProfileFinder);
    FinderDelete(&self->networkProfileFinder);
    FinderDelete(&self->oamProfileFinder);
    FinderDelete(&self->bcpProfileFinder);
    FinderDelete(&self->fcnProfileFinder);
    }

static eAtRet AllProfileFindersCreate(ThaProfileManager self)
    {
    if (self->errorProfileFinder   == NULL)
        self->errorProfileFinder   = mMethodsGet(self)->ErrorProfileFinderObjectCreate(self);
    if (self->networkProfileFinder == NULL)
        self->networkProfileFinder = mMethodsGet(self)->NetworkProfileFinderObjectCreate(self);
    if (self->oamProfileFinder     == NULL)
        self->oamProfileFinder     = mMethodsGet(self)->OamProfileFinderObjectCreate(self);
    if (self->bcpProfileFinder     == NULL)
        self->bcpProfileFinder     = mMethodsGet(self)->BcpProfileFinderObjectCreate(self);
    if (self->fcnProfileFinder     == NULL)
        self->fcnProfileFinder     = mMethodsGet(self)->FcnProfileFinderObjectCreate(self);

    if ((self->errorProfileFinder   == NULL) ||
        (self->networkProfileFinder == NULL) ||
        (self->oamProfileFinder     == NULL) ||
        (self->bcpProfileFinder     == NULL) ||
        (self->fcnProfileFinder     == NULL))
        {
        AllProfileFindersDelete(self);
        return cAtErrorRsrcNoAvail;
        }

    return cAtOk;
    }

static ThaProfilePool ProfilePoolCreate(ThaProfileManager self)
    {
    if (self->profilePool == NULL)
        {
        ThaProfilePool pool = mMethodsGet(self)->ProfilePoolObjectCreate(self);
        if (ThaProfilePoolSetup(pool) != cAtOk)
            {
            AtObjectDelete((AtObject)pool);
            return NULL;
            }

        self->profilePool = pool;
        }

    return self->profilePool;
    }

static void ProfilePoolDelete(ThaProfileManager self)
    {
    if (self->profilePool == NULL)
        return;

    AtObjectDelete((AtObject)self->profilePool);
    self->profilePool = NULL;
    }

static ThaProfilePool ProfilePoolObjectCreate(ThaProfileManager self)
    {
    return ThaProfilePoolNew(self);
    }

static eAtRet Init(ThaProfileManager self)
    {
    return ThaProfilePoolInit(self->profilePool);
    }

static eAtRet Setup(ThaProfileManager self)
    {
    eAtRet ret;

    if (ProfilePoolCreate(self) == NULL)
        return cAtErrorRsrcNoAvail;

    ret = AllProfileFindersCreate(self);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    ThaProfileManager manager = (ThaProfileManager)self;

    AllProfileFindersDelete(manager);
    ProfilePoolDelete(manager);

    m_AtObjectMethods->Delete(self);
    }

static uint8 RuleToHw(ThaProfileManager self, eThaProfileRule rule)
    {
    AtUnused(self);
    AtUnused(rule);
    return 0;
    }

static eThaProfileRule HwToRule(ThaProfileManager self, uint8 hwValue)
    {
    AtUnused(self);
    AtUnused(hwValue);
    return cThaProfileRuleUnknown;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "profile_manager";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaProfileManager object = (ThaProfileManager)(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    mEncodeObject(profilePool);
    mEncodeObject(errorProfileFinder);
    mEncodeObject(networkProfileFinder);
    mEncodeObject(oamProfileFinder);
    mEncodeObject(bcpProfileFinder);
    mEncodeObject(fcnProfileFinder);
    }

static void OverrideAtObject(ThaProfileManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaProfileManager self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaProfileManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, ProfilePoolObjectCreate);
        mMethodOverride(m_methods, OamProfileFinderObjectCreate);
        mMethodOverride(m_methods, ErrorProfileFinderObjectCreate);
        mMethodOverride(m_methods, NetworkProfileFinderObjectCreate);
        mMethodOverride(m_methods, BcpProfileFinderObjectCreate);
        mMethodOverride(m_methods, FcnProfileFinderObjectCreate);
        mMethodOverride(m_methods, RuleToHw);
        mMethodOverride(m_methods, HwToRule);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaProfileManager);
    }

ThaProfileManager ThaProfileManagerObjectInit(ThaProfileManager self, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->module = module;

    return self;
    }

ThaProfileManager ThaProfileManagerNew(AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaProfileManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    return ThaProfileManagerObjectInit(newManager, module);
    }

AtModuleEncap ThaProfileManagerEncapModuleGet(ThaProfileManager self)
    {
    if (self)
        return self->module;
    return NULL;
    }

eAtRet ThaProfileManagerInit(ThaProfileManager self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNotImplemented;
    }

eAtRet ThaProfileManagerSetup(ThaProfileManager self)
    {
    if (self)
        return mMethodsGet(self)->Setup(self);
    return cAtErrorNullPointer;
    }

ThaProfileFinder ThaProfileManagerErrorProfileFinderGet(ThaProfileManager self)
    {
    if (self)
        return ThaProfileFinderReset(self->errorProfileFinder);
    return NULL;
    }

ThaProfileFinder ThaProfileManagerNetworkProfileFinderGet(ThaProfileManager self)
    {
    if (self)
        return ThaProfileFinderReset(self->networkProfileFinder);
    return NULL;
    }

ThaProfileFinder ThaProfileManagerOamProfileFinderGet(ThaProfileManager self)
    {
    if (self)
        return ThaProfileFinderReset(self->oamProfileFinder);
    return NULL;
    }

ThaProfileFinder ThaProfileManagerBcpProfileFinderGet(ThaProfileManager self)
    {
    if (self)
        return ThaProfileFinderReset(self->bcpProfileFinder);
    return NULL;
    }

ThaProfileFinder ThaProfileManagerFcnProfileFinderGet(ThaProfileManager self)
    {
    if (self)
        return ThaProfileFinderReset(self->fcnProfileFinder);
    return NULL;
    }

ThaProfilePool ThaProfileManagerProfilePoolGet(ThaProfileManager self)
    {
    if (self)
        return self->profilePool;
    return NULL;
    }

uint8 ThaProfileManagerRuleToHw(ThaProfileManager self, eThaProfileRule rule)
    {
    if (self)
        return mMethodsGet(self)->RuleToHw(self, rule);
    return 0;
    }

eThaProfileRule ThaProfileManagerHwToRule(ThaProfileManager self, uint8 hwValue)
    {
    if (self)
        return mMethodsGet(self)->HwToRule(self, hwValue);
    return cThaProfileRuleUnknown;
    }

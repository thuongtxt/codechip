/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaProfileManager.h
 * 
 * Created Date: Jun 7, 2016
 *
 * Description : Traffic manager interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPROFILEMANAGER_H_
#define _THAPROFILEMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtRet.h"
#include "AtModuleEncap.h"
#include "profilepool/ThaProfile.h"
#include "profilefinder/ThaProfileFinder.h"
#include "profilepool/ThaProfilePool.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaProfileManager ThaProfileManagerNew(AtModuleEncap module);
eAtRet ThaProfileManagerInit(ThaProfileManager self);
eAtRet ThaProfileManagerSetup(ThaProfileManager self);

uint8 ThaProfileManagerRuleToHw(ThaProfileManager self, eThaProfileRule rule);
eThaProfileRule ThaProfileManagerHwToRule(ThaProfileManager self, uint8 hwValue);

ThaProfileFinder ThaProfileManagerErrorProfileFinderGet(ThaProfileManager self);
ThaProfileFinder ThaProfileManagerNetworkProfileFinderGet(ThaProfileManager self);
ThaProfileFinder ThaProfileManagerOamProfileFinderGet(ThaProfileManager self);
ThaProfileFinder ThaProfileManagerBcpProfileFinderGet(ThaProfileManager self);
ThaProfileFinder ThaProfileManagerFcnProfileFinderGet(ThaProfileManager self);

ThaProfilePool ThaProfileManagerProfilePoolGet(ThaProfileManager self);
AtModuleEncap ThaProfileManagerEncapModuleGet(ThaProfileManager self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPROFILEMANAGER_H_ */


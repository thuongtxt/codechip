/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaFcnProfile.c
 *
 * Created Date: Jun 28, 2016
 *
 * Description : FCN profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaProfileInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaProfileMethods m_ThaProfileOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16* AllTypesGet(ThaProfile self, uint8* numType)
    {
    static uint16 cAllFcnTypesVal[] = {cThaProfileFcnTypeFECN,
                                      cThaProfileFcnTypeBECN,
                                      cThaProfileFcnTypeDE,
                                      cThaProfileFcnTypeC};
    AtUnused(self);
    if (numType)
        *numType = mCount(cAllFcnTypesVal);
    return cAllFcnTypesVal;
    }

static const char** AllTypesStringGet(ThaProfile self, uint8* numType)
    {
    static const char* cAllFcnTypesString[] = {"FECN bit", "BECN bit", "DE bit", "C bit"};
    AtUnused(self);
    if (numType)
        *numType = mCount(cAllFcnTypesString);
    return cAllFcnTypesString;
    }

static void OverrideThaProfile(ThaProfile self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfileOverride, mMethodsGet(self), sizeof(m_ThaProfileOverride));

        mMethodOverride(m_ThaProfileOverride, AllTypesGet);
        mMethodOverride(m_ThaProfileOverride, AllTypesStringGet);
        }

    mMethodsSet(self, &m_ThaProfileOverride);
    }

static void Override(ThaProfile self)
    {
    OverrideThaProfile(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaFcnProfile);
    }

ThaProfile ThaFcnProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaProfileObjectInit(self, pool, profileId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }


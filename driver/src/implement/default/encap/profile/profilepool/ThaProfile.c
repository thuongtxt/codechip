/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaProfile.c
 *
 * Created Date: Jun 9, 2016
 *
 * Description : Profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcLink.h"
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../../generic/man/AtModuleInternal.h"
#include "ThaProfileInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaProfile)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaProfileMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet KeyValueSet(ThaProfile self, uint32 hwValue)
    {
    AtUnused(self);
    AtUnused(hwValue);
    return cAtErrorNotImplemented;
    }

static uint32 KeyValueGet(ThaProfile self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void DeleteLinkList(ThaProfile self)
    {
    AtHdlcLink link;

    if (self->hdlcLinkList == NULL)
        return;

    while ((link = (AtHdlcLink)AtListObjectRemoveAtIndex(self->hdlcLinkList, 0)) != NULL)
        {
        /* Do nothing */
        }

    AtObjectDelete((AtObject)self->hdlcLinkList);
    self->hdlcLinkList = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteLinkList((ThaProfile)self);
    m_AtObjectMethods->Delete(self);
    }

static ThaProfileManager Manager(ThaProfile self)
    {
    return ThaProfilePoolManagerGet(self->pool);
    }

static AtModuleEncap ModuleEncap(ThaProfile self)
    {
    return ThaProfileManagerEncapModuleGet(Manager(self));
    }

static uint32 TypeBitMask(ThaProfile self, uint16 type)
    {
    return (cBit1_0 << mMethodsGet(self)->TypeBitShift(self, type));
    }

static uint8 TypeBitShift(ThaProfile self, uint16 type)
    {
    AtUnused(self);
    AtUnused(type);
    return 0;
    }

static uint16* AllTypesGet(ThaProfile self, uint8* numType)
    {
    AtUnused(self);
    if (numType) *numType = 0;
    return NULL;
    }

static const char** AllTypesStringGet(ThaProfile self, uint8* numType)
    {
    AtUnused(self);
    if (numType) numType = 0;
    return NULL;
    }

static eThaProfileRule RuleOfTypeGet(ThaProfile self, uint16 type)
    {
    uint8 numType, type_i;
    uint32 profileHwValue = mMethodsGet(self)->KeyValueGet(self);
    uint16* allTypes = mMethodsGet(self)->AllTypesGet(self, &numType);
    uint8 ruleHwValue, typeShift;
    uint32 typeMask;

    for (type_i = 0; type_i < numType; type_i++)
        {
        if (type != allTypes[type_i])
            continue;

        typeMask = ThaProfileTypeBitMask(self, allTypes[type_i]);
        typeShift = ThaProfileTypeBitShift(self, allTypes[type_i]);
        ruleHwValue = (uint8)mRegField(profileHwValue, type);

        return ThaProfileManagerHwToRule(Manager(self), ruleHwValue);
        }

    return cThaProfileRuleUnknown;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "encap_profile";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaProfile object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescriptionList(hdlcLinkList);
    mEncodeUInt(profileId);
    mEncodeObjectDescription(pool);
    }

static void OverrideAtObject(ThaProfile self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaProfile self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaProfile);
    }

static void MethodsInit(ThaProfile self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, KeyValueSet);
        mMethodOverride(m_methods, KeyValueGet);
        mMethodOverride(m_methods, TypeBitMask);
        mMethodOverride(m_methods, TypeBitShift);
        mMethodOverride(m_methods, AllTypesGet);
        mMethodOverride(m_methods, AllTypesStringGet);
        mMethodOverride(m_methods, RuleOfTypeGet);
        }

    mMethodsSet(self, &m_methods);
    }

ThaProfile ThaProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->pool         = pool;
    self->profileId    = profileId;
    self->hdlcLinkList = AtListCreate(0);

    return self;
    }

uint32 ThaProfileProfileIdGet(ThaProfile self)
    {
    if (self)
        return self->profileId;

    return cInvalidUint32;
    }

uint32 ThaProfileHwRead(ThaProfile self, uint32 address)
    {
    AtModuleEncap moduleEncap = ModuleEncap(self);
    return mModuleHwRead(moduleEncap, address);
    }

void ThaProfileHwWrite(ThaProfile self, uint32 regAddr, uint32 regValue)
    {
    AtModuleEncap moduleEncap = ModuleEncap(self);
    mModuleHwWrite(moduleEncap, regAddr, regValue);
    }

ThaProfilePool ThaProfileProfilePoolGet(ThaProfile self)
    {
    if (self)
        return self->pool;

    return NULL;
    }

AtList ThaProfileListHdlcLinkGet(ThaProfile self)
    {
    if (self)
        return self->hdlcLinkList;

    return NULL;
    }

eAtRet ThaProfileHdlcLinkAdd(ThaProfile self, AtHdlcLink phyLink)
    {
    if (self)
        {
        if (!AtListContainsObject(self->hdlcLinkList, (AtObject)phyLink))
            AtListObjectAdd(self->hdlcLinkList, (AtObject)phyLink);
        }

    return cAtErrorNullPointer;
    }

eAtRet ThaProfileHdlcLinkRemove(ThaProfile self, AtHdlcLink phyLink)
    {
    if (self)
        AtListObjectRemove(self->hdlcLinkList, (AtObject)phyLink);

    return cAtErrorNullPointer;
    }

eAtRet ThaProfileKeyValueSet(ThaProfile self, uint32 value)
    {
    if (self)
        return mMethodsGet(self)->KeyValueSet(self, value);

    return cAtErrorNullPointer;
    }

uint32 ThaProfileKeyValueGet(ThaProfile self)
    {
    if (self)
        return mMethodsGet(self)->KeyValueGet(self);
    return cInvalidUint32;
    }

eBool ThaProfileIsFree(ThaProfile self)
    {
    if (self)
        return (AtListLengthGet(self->hdlcLinkList) > 0) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

eBool ThaProfileIsOnlyUsedByLink(ThaProfile self, AtHdlcLink link)
    {
    if (self == NULL)
        return cAtFalse;

    /* None or more than 1 links are using this profile */
    if (AtListLengthGet(self->hdlcLinkList) != 1)
        return cAtFalse;

    if (AtListContainsObject(self->hdlcLinkList, (AtObject)link))
        return cAtTrue;

    return cAtFalse;
    }

uint16* ThaProfileAllTypesGet(ThaProfile self, uint8 *numRule)
    {
    if (numRule)
        *numRule = 0;

    if (self == NULL)
        return NULL;

    return mMethodsGet(self)->AllTypesGet(self, numRule);
    }

const char** ThaProfileAllTypesStringGet(ThaProfile self, uint8 *numRule)
    {
    if (numRule)
        *numRule = 0;

    if (self == NULL)
        return NULL;

    return mMethodsGet(self)->AllTypesStringGet(self, numRule);
    }

eThaProfileRule ThaProfileRuleOfTypeGet(ThaProfile self, uint16 type)
    {
    if (self == NULL)
        return cThaProfileRuleUnknown;

    return mMethodsGet(self)->RuleOfTypeGet(self, type);
    }

uint32 ThaProfileTypeBitMask(ThaProfile self, uint16 type)
    {
    if (self)
        return mMethodsGet(self)->TypeBitMask(self, type);
    return 0;
    }

uint8 ThaProfileTypeBitShift(ThaProfile self, uint16 type)
    {
    if (self)
        return mMethodsGet(self)->TypeBitShift(self, type);
    return 0;
    }

/*
 * Set rule for multiple types for profile
 *
 * @param self This profile
 * @param maskTypes Multiple types mask
 * @param rule Rule
 * @return At return code
 */
eAtRet ThaProfileTypesRuleSet(ThaProfile self, uint32 maskTypes, eThaProfileRule rule)
    {
    eAtRet ret = cAtOk;
    uint8 numRules, type_i;
    uint32 hwValue, typeMask, typeShift;
    uint16 *allProfileTypes = ThaProfileAllTypesGet(self, &numRules);

    if (self == NULL)
        return cAtErrorNullPointer;

    for (type_i = 0; type_i < numRules; type_i++)
        {
        if ((allProfileTypes[type_i] & maskTypes) == 0)
            continue;

        hwValue   = ThaProfileKeyValueGet(self);
        typeMask  = ThaProfileTypeBitMask(self, allProfileTypes[type_i]);
        typeShift = ThaProfileTypeBitShift(self, allProfileTypes[type_i]);
        mRegFieldSet(hwValue, type, ThaProfileManagerRuleToHw(Manager(self), rule));
        ret |= ThaProfileKeyValueSet(self, hwValue);
        }

    return ret;
    }

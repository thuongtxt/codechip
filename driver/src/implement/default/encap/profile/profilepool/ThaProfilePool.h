/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaProfilePool.h
 *
 * Created Date: Jun 8, 2016
 *
 * Description : Profile Pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "ThaProfile.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
ThaProfilePool ThaProfilePoolNew(ThaProfileManager manager);
eAtRet ThaProfilePoolInit(ThaProfilePool self);
eAtRet ThaProfilePoolSetup(ThaProfilePool self);
ThaProfileManager ThaProfilePoolManagerGet(ThaProfilePool self);
AtList ThaProfilePoolNetworkProfileListGet(ThaProfilePool self);
AtList ThaProfilePoolOamProfileListGet(ThaProfilePool self);
AtList ThaProfilePoolErrorProfileListGet(ThaProfilePool self);
AtList ThaProfilePoolBcpProfileListGet(ThaProfilePool self);
AtList ThaProfilePoolFcnProfileListGet(ThaProfilePool self);

ThaProfile ThaProfilePoolNetworkProfileGet(ThaProfilePool self, uint32 profileId);
ThaProfile ThaProfilePoolOamProfileGet(ThaProfilePool self, uint32 profileId);
ThaProfile ThaProfilePoolErrorProfileGet(ThaProfilePool self, uint32 profileId);
ThaProfile ThaProfilePoolBcpProfileGet(ThaProfilePool self, uint32 profileId);
ThaProfile ThaProfilePoolFcnProfileGet(ThaProfilePool self, uint32 profileId);


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaProfile.h
 * 
 * Created Date: Jun 9, 2016
 *
 * Description : Profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPROFILE_H_
#define _THAPROFILE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaProfileManager      *ThaProfileManager;
typedef struct tThaProfilePool         *ThaProfilePool;
typedef struct tThaProfile             *ThaProfile;

typedef struct tThaProfileHandler      *ThaProfileHandler;
typedef struct tThaPppProfileHandler   *ThaPppProfileHandler;
typedef struct tThaFrProfileHandler    *ThaFrProfileHandler;
typedef struct tThaCHdlcProfileHandler *ThaCHdlcProfileHandler;

typedef struct tThaProfileFinder       *ThaProfileFinder;

typedef enum eThaProfileRule
    {
    cThaProfileRuleUnknown,
    cThaProfileRuleDrop,
    cThaProfileRuleData,
    cThaProfileRuleControl,
    cThaProfileRuleCorrupt
    }eThaProfileRule;

typedef enum eThaProfileErrorType
    {
    cThaProfileErrorTypeFcs       = cBit0,
    cThaProfileErrorTypeDecAbort  = cBit1,
    cThaProfileErrorTypeAddress   = cBit2,
    cThaProfileErrorTypeControl   = cBit3,
    cThaProfileErrorTypeProtocol  = cBit4,
    cThaProfileErrorTypeMultilink = cBit5,
    cThaProfileErrorTypeMac       = cBit6,
    cThaProfileErrorTypeLookup    = cBit7,
    cThaProfileErrorTypeAll       = cBit7_0
    }eThaProfileErrorType;

typedef enum eThaProfileNetworkType
    {
    cThaProfileNetworkTypeIpv4                   = cBit0,
    cThaProfileNetworkTypeIpv6                   = cBit1,
    cThaProfileNetworkTypeUniMpls                = cBit2,
    cThaProfileNetworkTypeMultiMpls              = cBit3,
    cThaProfileNetworkTypeOSINetwork             = cBit4,
    cThaProfileNetworkTypeAppleTalk              = cBit5,
    cThaProfileNetworkTypeNovellIPX              = cBit6,
    cThaProfileNetworkTypeXeroxNSIDP             = cBit7,
    cThaProfileNetworkTypeCiscoSystem            = cBit8,
    cThaProfileNetworkTypeCiscoDiscoveryProtocol = cBit9,
    cThaProfileNetworkTypeNetbiosFraming         = cBit10,
    cThaProfileNetworkTypeIPv6HeaderCompression  = cBit11,
    cThaProfileNetworkType8021DPacket            = cBit12,
    cThaProfileNetworkTypeIBMBDPU                = cBit13,
    cThaProfileNetworkTypeDecLanBrigde           = cBit14,
    cThaProfileNetworkTypeBcp                    = cBit15,
    cThaProfileNetworkTypeAll                    = cBit15_0
    }eThaProfileNetworkType;

typedef enum eThaProfileOamType
    {
    cThaProfileOamTypeLCP      = cBit0,
    cThaProfileOamTypeNCP      = cBit1,
    cThaProfileOamTypeQ933     = cBit2,
    cThaProfileOamTypeCiscoLMI = cBit3,
    cThaProfileOamTypeCLNP     = cBit4,
    cThaProfileOamTypeESIS     = cBit5,
    cThaProfileOamTypeISIS     = cBit6,
    cThaProfileOamTypeLIPCM    = cBit7,
    cThaProfileOamTypeAll      = cBit7_0
    }eThaProfileOamType;

typedef enum eThaProfileFcnType
    {
    cThaProfileFcnTypeFECN      = cBit0,
    cThaProfileFcnTypeBECN      = cBit1,
    cThaProfileFcnTypeDE        = cBit2,
    cThaProfileFcnTypeC         = cBit3,
    cThaProfileFcnTypeAll       = cBit3_0
    }eThaProfileFcnType;

typedef enum eThaProfileBcpType
    {
    cThaProfileBcpTypeFbit      = cBit0,
    cThaProfileBcpTypeZbit      = cBit1,
    cThaProfileBcpTypeBbit      = cBit2,
    cThaProfileBcpTypePbit      = cBit3,
    cThaProfileBcpTypePADsField = cBit4,
    cThaProfileBcpTypeAll       = cBit4_0
    }eThaProfileBcpType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaProfileKeyValueSet(ThaProfile self, uint32 value);
uint32 ThaProfileKeyValueGet(ThaProfile self);
uint32 ThaProfileHwRead(ThaProfile self, uint32 regAddr);
void ThaProfileHwWrite(ThaProfile self, uint32 regAddr, uint32 regValue);

uint32 ThaProfileProfileIdGet(ThaProfile self);
eBool ThaProfileIsFree(ThaProfile self);
eBool ThaProfileIsOnlyUsedByLink(ThaProfile self, AtHdlcLink link);
eAtRet ThaProfileHdlcLinkAdd(ThaProfile self, AtHdlcLink phyLink);
eAtRet ThaProfileHdlcLinkRemove(ThaProfile self, AtHdlcLink phyLink);

AtList ThaProfileListHdlcLinkGet(ThaProfile self);
ThaProfilePool ThaProfileProfilePoolGet(ThaProfile self);
uint16* ThaProfileAllTypesGet(ThaProfile self, uint8 *numRule);
const char** ThaProfileAllTypesStringGet(ThaProfile self, uint8 *numRule);
eThaProfileRule ThaProfileRuleOfTypeGet(ThaProfile self, uint16 type);
uint32 ThaProfileTypeBitMask(ThaProfile self, uint16 type);
uint8 ThaProfileTypeBitShift(ThaProfile self, uint16 type);
eAtRet ThaProfileTypesRuleSet(ThaProfile self, uint32 maskTypes, eThaProfileRule rule);

#ifdef __cplusplus
}
#endif
#endif /* _THAPROFILE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ThaOamProfile.c
 *
 * Created Date: Jun 27, 2016
 *
 * Description : OAM profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaProfileInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaProfileMethods m_ThaProfileOverride;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16* AllTypesGet(ThaProfile self, uint8* numType)
    {
    static uint16 cOamProfileTypes[] = {cThaProfileOamTypeLCP,
                                        cThaProfileOamTypeNCP,
                                        cThaProfileOamTypeQ933,
                                        cThaProfileOamTypeCiscoLMI,
                                        cThaProfileOamTypeCLNP,
                                        cThaProfileOamTypeESIS,
                                        cThaProfileOamTypeISIS,
                                        cThaProfileOamTypeLIPCM};
    AtUnused(self);
    if (numType)
        *numType = mCount(cOamProfileTypes);
    return cOamProfileTypes;
    }

static const char** AllTypesStringGet(ThaProfile self, uint8* numType)
    {
    static const char* cOamProfileString[] = {"SLARP/LCP/RARP", "NCP/ARP", "Q933", "CISCO LMI",
                                              "CLNP", "ESIS", "ISIS", "LIPCM"};
    AtUnused(self);
    if (numType)
        *numType = mCount(cOamProfileString);
    return cOamProfileString;
    }

static void OverrideThaProfile(ThaProfile self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfileOverride, mMethodsGet(self), sizeof(m_ThaProfileOverride));

        mMethodOverride(m_ThaProfileOverride, AllTypesGet);
        mMethodOverride(m_ThaProfileOverride, AllTypesStringGet);
        }

    mMethodsSet(self, &m_ThaProfileOverride);
    }

static void Override(ThaProfile self)
    {
    OverrideThaProfile(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaOamProfile);
    }

ThaProfile ThaOamProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaProfileObjectInit(self, pool, profileId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaProfilePoolInternal.h
 * 
 * Created Date: Jun 8, 2016
 *
 * Description : Profile Pool
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPROFILEPOOLINTERNAL_H_
#define _THAPROFILEPOOLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/common/AtObjectInternal.h"
#include "../ThaProfileManager.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaProfilePoolMethods
    {
    eAtRet (*Init)(ThaProfilePool self);
    eAtRet (*Setup)(ThaProfilePool self);

    uint8 (*MaxNumNetworkProfile)(ThaProfilePool self);
    uint8 (*MaxNumErrorProfile)(ThaProfilePool self);
    uint8 (*MaxNumOamProfile)(ThaProfilePool self);
    uint8 (*MaxNumBcpProfile)(ThaProfilePool self);
    uint8 (*MaxNumFcnProfile)(ThaProfilePool self);

    ThaProfile (*ErrorProfileObjectCreate)(ThaProfilePool self, uint32 profileId);
    ThaProfile (*NetworkProfileObjectCreate)(ThaProfilePool self, uint32 profileId);
    ThaProfile (*OamProfileObjectCreate)(ThaProfilePool self, uint32 profileId);
    ThaProfile (*BcpProfileObjectCreate)(ThaProfilePool self, uint32 profileId);
    ThaProfile (*FcnProfileObjectCreate)(ThaProfilePool self, uint32 profileId);
    }tThaProfilePoolMethods;

typedef struct tThaProfilePool
    {
    tAtObject super;
    const tThaProfilePoolMethods *methods;

    AtList networkProfiles;
    AtList fcnProfiles;
    AtList bcpProfiles;
    AtList errorProfiles;
    AtList oamProfiles;
    ThaProfileManager manager;
    }tThaProfilePool;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaProfilePool ThaProfilePoolObjectInit(ThaProfilePool self, ThaProfileManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _THAPROFILEPOOLINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaProfileInternal.h
 * 
 * Created Date: Jun 9, 2016
 *
 * Description : Profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPROFILEINTERNAL_H_
#define _THAPROFILEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/common/AtObjectInternal.h"
#include "../ThaProfileManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaProfileMethods
    {
    eAtRet (*KeyValueSet)(ThaProfile self, uint32 hwValue);
    uint32 (*KeyValueGet)(ThaProfile self);

    uint16* (*AllTypesGet)(ThaProfile self, uint8* numType);
    const char** (*AllTypesStringGet)(ThaProfile self, uint8* numType);
    eThaProfileRule (*RuleOfTypeGet)(ThaProfile self, uint16 type);
    uint32 (*TypeBitMask)(ThaProfile self, uint16 type);
    uint8 (*TypeBitShift)(ThaProfile self, uint16 type);
    }tThaProfileMethods;

typedef struct tThaProfile
    {
    tAtObject super;
    const tThaProfileMethods *methods;

    /* Private data */
    AtList hdlcLinkList;
    uint32 profileId;
    ThaProfilePool pool;
    }tThaProfile;

typedef struct tThaErrorProfile
    {
    tThaProfile super;
    }tThaErrorProfile;

typedef struct tThaOamProfile
    {
    tThaProfile super;
    }tThaOamProfile;

typedef struct tThaNetworkProfile
    {
    tThaProfile super;
    }tThaNetworkProfile;

typedef struct tThaBcpProfile
    {
    tThaProfile super;
    }tThaBcpProfile;

typedef struct tThaFcnProfile
    {
    tThaProfile super;
    }tThaFcnProfile;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaProfile ThaProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId);
ThaProfile ThaErrorProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId);
ThaProfile ThaOamProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId);
ThaProfile ThaNetworkProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId);
ThaProfile ThaBcpProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId);
ThaProfile ThaFcnProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId);

#ifdef __cplusplus
}
#endif
#endif /* _THAPROFILEINTERNAL_H_ */


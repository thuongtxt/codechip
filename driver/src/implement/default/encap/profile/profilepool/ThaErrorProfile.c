/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaErrorProfile.c
 *
 * Created Date: Jun 27, 2016
 *
 * Description : Error profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaProfileInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaProfileMethods m_ThaProfileOverride;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16* AllTypesGet(ThaProfile self, uint8* numType)
    {
    static uint16 profileTypes[] = {cThaProfileErrorTypeFcs, cThaProfileErrorTypeDecAbort, cThaProfileErrorTypeAddress,
                                    cThaProfileErrorTypeControl, cThaProfileErrorTypeProtocol, cThaProfileErrorTypeMultilink,
                                    cThaProfileErrorTypeMac, cThaProfileErrorTypeLookup};

    AtUnused(self);
    if (numType)
        *numType = mCount(profileTypes);

    return profileTypes;
    }

static const char** AllTypesStringGet(ThaProfile self, uint8* numType)
    {
    static const char* cErrorProfileString[] = {"FCS_error", "Dec_abort", "Address_error", "Control_error",
                                                "Protocol_error", "Multilink_error",
                                                "MAC_error", "Lookup_fail"};
    AtUnused(self);
    if (numType)
        *numType = mCount(cErrorProfileString);

    return cErrorProfileString;
    }

static void OverrideThaProfile(ThaProfile self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfileOverride, mMethodsGet(self), sizeof(m_ThaProfileOverride));

        mMethodOverride(m_ThaProfileOverride, AllTypesGet);
        mMethodOverride(m_ThaProfileOverride, AllTypesStringGet);
        }

    mMethodsSet(self, &m_ThaProfileOverride);
    }

static void Override(ThaProfile self)
    {
    OverrideThaProfile(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaErrorProfile);
    }

ThaProfile ThaErrorProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaProfileObjectInit(self, pool, profileId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }


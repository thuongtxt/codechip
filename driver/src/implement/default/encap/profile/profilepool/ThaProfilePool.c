/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaProfilePool.c
 *
 * Created Date: Jun 8, 2016
 *
 * Description : r
 *
 * Notes       : Profile Pool
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "ThaProfilePoolInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAllDrop 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef uint8 (*MaxNumProfileFunc)(ThaProfilePool self);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaProfilePoolMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList ProfileCreate(ThaProfilePool self, MaxNumProfileFunc maxNumProfileFunc,
                            ThaProfile (*ProfileObjectCreate)(ThaProfilePool self, uint32 profileId))
    {
    AtList networkProfiles;
    uint8 numProfile = maxNumProfileFunc(self);
    uint8 profileId;

    networkProfiles = AtListCreate(numProfile);
    if (networkProfiles == NULL)
        return NULL;

    for (profileId = 0; profileId < numProfile; profileId++)
        {
        ThaProfile profile = ProfileObjectCreate(self, profileId);
        if (profile)
            AtListObjectAdd(networkProfiles, (AtObject)profile);
        }

    if (AtListLengthGet(networkProfiles) == 0)
        return NULL;

    return networkProfiles;
    }

static void NetworkProfileDelete(ThaProfilePool self)
    {
    ThaProfile profile;

    if (self->networkProfiles == NULL)
        return;

    while ((profile = (ThaProfile)AtListObjectRemoveAtIndex(self->networkProfiles, 0)) != NULL)
        AtObjectDelete((AtObject)profile);

    AtObjectDelete((AtObject)self->networkProfiles);
    self->networkProfiles = NULL;
    }

static eAtRet NetworkProfileCreate(ThaProfilePool self)
    {
    self->networkProfiles = ProfileCreate(self, mMethodsGet(self)->MaxNumNetworkProfile,
                                          mMethodsGet(self)->NetworkProfileObjectCreate);
    if (AtListLengthGet(self->networkProfiles) == 0)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static void ErrorProfileDelete(ThaProfilePool self)
    {
    ThaProfile profile;

    if (self->errorProfiles == NULL)
        return;

    while ((profile = (ThaProfile)AtListObjectRemoveAtIndex(self->errorProfiles, 0)) != NULL)
        AtObjectDelete((AtObject)profile);

    AtObjectDelete((AtObject)self->errorProfiles);
    self->errorProfiles = NULL;
    }

static eAtRet ErrorProfileCreate(ThaProfilePool self)
    {
    self->errorProfiles = ProfileCreate(self, mMethodsGet(self)->MaxNumErrorProfile,
                                        mMethodsGet(self)->ErrorProfileObjectCreate);
    if (AtListLengthGet(self->errorProfiles) == 0)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static void BcpProfileDelete(ThaProfilePool self)
    {
    ThaProfile profile;

    if (self->bcpProfiles == NULL)
        return;

    while ((profile = (ThaProfile)AtListObjectRemoveAtIndex(self->bcpProfiles, 0)) != NULL)
        AtObjectDelete((AtObject)profile);

    AtObjectDelete((AtObject)self->bcpProfiles);
    self->bcpProfiles = NULL;
    }

static eAtRet BcpProfileCreate(ThaProfilePool self)
    {
    self->bcpProfiles = ProfileCreate(self, mMethodsGet(self)->MaxNumBcpProfile,
                                      mMethodsGet(self)->BcpProfileObjectCreate);
    if (AtListLengthGet(self->bcpProfiles) == 0)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static void FcnProfileDelete(ThaProfilePool self)
    {
    ThaProfile profile;

    if (self->fcnProfiles == NULL)
        return;

    while ((profile = (ThaProfile)AtListObjectRemoveAtIndex(self->fcnProfiles, 0)) != NULL)
        AtObjectDelete((AtObject)profile);

    AtObjectDelete((AtObject)self->fcnProfiles);
    self->fcnProfiles = NULL;
    }

static eAtRet FcnProfileCreate(ThaProfilePool self)
    {
    self->fcnProfiles = ProfileCreate(self, mMethodsGet(self)->MaxNumFcnProfile,
                                      mMethodsGet(self)->FcnProfileObjectCreate);
    if (AtListLengthGet(self->fcnProfiles) == 0)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static void OamProfileDelete(ThaProfilePool self)
    {
    ThaProfile profile;

    if (self->oamProfiles == NULL)
        return;

    while ((profile = (ThaProfile)AtListObjectRemoveAtIndex(self->oamProfiles, 0)) != NULL)
        AtObjectDelete((AtObject)profile);

    AtObjectDelete((AtObject)self->oamProfiles);
    self->oamProfiles = NULL;
    }

static eAtRet OamProfileCreate(ThaProfilePool self)
    {
    OamProfileDelete(self);

    self->oamProfiles = ProfileCreate(self, mMethodsGet(self)->MaxNumOamProfile,
                                      mMethodsGet(self)->OamProfileObjectCreate);
    if (AtListLengthGet(self->oamProfiles) == 0)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static eAtRet AllProfileSetup(ThaProfilePool self)
    {
    eAtRet ret;

    ret = NetworkProfileCreate(self);
    ret |= ErrorProfileCreate(self);
    ret |= OamProfileCreate(self);
    ret |= BcpProfileCreate(self);
    ret |= FcnProfileCreate(self);

    return ret;
    }

static void ProfileAllLinksRemove(ThaProfile self)
    {
    AtHdlcLink link;
    AtList listLink = ThaProfileListHdlcLinkGet(self);

    while ((link = (AtHdlcLink)AtListObjectGet(listLink, 0)) != NULL)
        ThaProfileHdlcLinkRemove(self, link);
    }

static void ProfilesDefaultSet(ThaProfilePool self, AtList profiles)
    {
    AtIterator iterator = AtListIteratorCreate(profiles);
    ThaProfile profile;
    AtUnused(self);

    while ((profile = (ThaProfile)AtIteratorNext(iterator)) != NULL)
        {
        ThaProfileKeyValueSet(profile, cAllDrop);
        ProfileAllLinksRemove(profile);
        }

    AtObjectDelete((AtObject)iterator);
    }

static void NetworkProfileDefaultSet(ThaProfilePool self)
    {
    ProfilesDefaultSet(self, self->networkProfiles);
    }

static void ErrorProfileDefaultSet(ThaProfilePool self)
    {
    ProfilesDefaultSet(self, self->errorProfiles);
    }

static void OamProfileDefaultSet(ThaProfilePool self)
    {
    ProfilesDefaultSet(self, self->oamProfiles);
    }

static void BcpProfileDefaultSet(ThaProfilePool self)
    {
    ProfilesDefaultSet(self, self->bcpProfiles);
    }

static void FcnProfileDefaultSet(ThaProfilePool self)
    {
    ProfilesDefaultSet(self, self->fcnProfiles);
    }

static eAtRet Init(ThaProfilePool self)
    {
    NetworkProfileDefaultSet(self);
    ErrorProfileDefaultSet(self);
    OamProfileDefaultSet(self);
    FcnProfileDefaultSet(self);
    BcpProfileDefaultSet(self);

    return cAtOk;
    }

static uint8 MaxNumNetworkProfile(ThaProfilePool self)
    {
    /* Let concrete class know that */
    AtUnused(self);
    return 0;
    }

static uint8 MaxNumErrorProfile(ThaProfilePool self)
    {
    /* Let concrete class know that */
    AtUnused(self);
    return 0;
    }

static uint8 MaxNumOamProfile(ThaProfilePool self)
    {
    /* Let concrete class know that */
    AtUnused(self);
    return 0;
    }

static uint8 MaxNumBcpProfile(ThaProfilePool self)
    {
    /* Let concrete class know that */
    AtUnused(self);
    return 0;
    }

static uint8 MaxNumFcnProfile(ThaProfilePool self)
    {
    /* Let concrete class know that */
    AtUnused(self);
    return 0;
    }

static eAtRet Setup(ThaProfilePool self)
    {
    return AllProfileSetup(self);
    }

static void AllProfileDelete(ThaProfilePool self)
    {
    NetworkProfileDelete(self);
    ErrorProfileDelete(self);
    OamProfileDelete(self);
    BcpProfileDelete(self);
    FcnProfileDelete(self);
    }

static void Delete(AtObject self)
    {
    AllProfileDelete((ThaProfilePool)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaProfilePool object = (ThaProfilePool)(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeList(networkProfiles);
    mEncodeList(fcnProfiles);
    mEncodeList(bcpProfiles);
    mEncodeList(errorProfiles);
    mEncodeList(oamProfiles);
    mEncodeObjectDescription(manager);
    }

static ThaProfile ErrorProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    AtUnused(self);
    AtUnused(profileId);
    return NULL;
    }

static ThaProfile NetworkProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    AtUnused(self);
    AtUnused(profileId);
    return NULL;
    }

static ThaProfile OamProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    AtUnused(self);
    AtUnused(profileId);
    return NULL;
    }

static ThaProfile BcpProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    AtUnused(self);
    AtUnused(profileId);
    return NULL;
    }

static ThaProfile FcnProfileObjectCreate(ThaProfilePool self, uint32 profileId)
    {
    AtUnused(self);
    AtUnused(profileId);
    return NULL;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "profile_pool";
    }

static void OverrideAtObject(ThaProfilePool self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(ThaProfilePool self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, ErrorProfileObjectCreate);
        mMethodOverride(m_methods, NetworkProfileObjectCreate);
        mMethodOverride(m_methods, OamProfileObjectCreate);
        mMethodOverride(m_methods, BcpProfileObjectCreate);
        mMethodOverride(m_methods, FcnProfileObjectCreate);
        mMethodOverride(m_methods, MaxNumNetworkProfile);
        mMethodOverride(m_methods, MaxNumErrorProfile);
        mMethodOverride(m_methods, MaxNumOamProfile);
        mMethodOverride(m_methods, MaxNumBcpProfile);
        mMethodOverride(m_methods, MaxNumFcnProfile);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(ThaProfilePool self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaProfilePool);
    }

ThaProfilePool ThaProfilePoolObjectInit(ThaProfilePool self, ThaProfileManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->manager = manager;

    return self;
    }

ThaProfilePool ThaProfilePoolNew(ThaProfileManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaProfilePool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    return ThaProfilePoolObjectInit(newPool, manager);
    }

AtList ThaProfilePoolNetworkProfileListGet(ThaProfilePool self)
    {
    if (self)
        return self->networkProfiles;
    return NULL;
    }

AtList ThaProfilePoolOamProfileListGet(ThaProfilePool self)
    {
    if (self)
        return self->oamProfiles;
    return NULL;
    }

AtList ThaProfilePoolErrorProfileListGet(ThaProfilePool self)
    {
    if (self)
        return self->errorProfiles;
    return NULL;
    }

AtList ThaProfilePoolBcpProfileListGet(ThaProfilePool self)
    {
    if (self)
        return self->bcpProfiles;
    return NULL;
    }

AtList ThaProfilePoolFcnProfileListGet(ThaProfilePool self)
    {
    if (self)
        return self->fcnProfiles;
    return NULL;
    }

ThaProfileManager ThaProfilePoolManagerGet(ThaProfilePool self)
    {
    if (self)
        return self->manager;
    return NULL;
    }

eAtRet ThaProfilePoolInit(ThaProfilePool self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNotImplemented;
    }

eAtRet ThaProfilePoolSetup(ThaProfilePool self)
    {
    if (self)
        return mMethodsGet(self)->Setup(self);
    return cAtErrorNotImplemented;
    }

ThaProfile ThaProfilePoolNetworkProfileGet(ThaProfilePool self, uint32 profileId)
    {
    if (self == NULL)
        return NULL;

    return (ThaProfile)AtListObjectGet(self->networkProfiles, profileId);
    }

ThaProfile ThaProfilePoolOamProfileGet(ThaProfilePool self, uint32 profileId)
    {
    if (self == NULL)
        return NULL;

    return (ThaProfile)AtListObjectGet(self->oamProfiles, profileId);
    }

ThaProfile ThaProfilePoolErrorProfileGet(ThaProfilePool self, uint32 profileId)
    {
    if (self == NULL)
        return NULL;

    return (ThaProfile)AtListObjectGet(self->errorProfiles, profileId);
    }

ThaProfile ThaProfilePoolBcpProfileGet(ThaProfilePool self, uint32 profileId)
    {
    if (self == NULL)
        return NULL;

    return (ThaProfile)AtListObjectGet(self->bcpProfiles, profileId);
    }

ThaProfile ThaProfilePoolFcnProfileGet(ThaProfilePool self, uint32 profileId)
    {
    if (self == NULL)
        return NULL;

    return (ThaProfile)AtListObjectGet(self->fcnProfiles, profileId);
    }

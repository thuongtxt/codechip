/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaNetworkProfile.c
 *
 * Created Date: Jun 28, 2016
 *
 * Description : Network profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaProfileInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaProfileMethods m_ThaProfileOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16* AllTypesGet(ThaProfile self, uint8* numType)
    {
    static uint16 cAllNetworkTypes[] = {cThaProfileNetworkTypeIpv4,
                                        cThaProfileNetworkTypeIpv6,
                                        cThaProfileNetworkTypeUniMpls,
                                        cThaProfileNetworkTypeMultiMpls,
                                        cThaProfileNetworkTypeOSINetwork,
                                        cThaProfileNetworkTypeAppleTalk,
                                        cThaProfileNetworkTypeNovellIPX,
                                        cThaProfileNetworkTypeXeroxNSIDP,
                                        cThaProfileNetworkTypeCiscoSystem,
                                        cThaProfileNetworkTypeCiscoDiscoveryProtocol,
                                        cThaProfileNetworkTypeNetbiosFraming,
                                        cThaProfileNetworkTypeIPv6HeaderCompression,
                                        cThaProfileNetworkType8021DPacket,
                                        cThaProfileNetworkTypeIBMBDPU,
                                        cThaProfileNetworkTypeDecLanBrigde,
                                        cThaProfileNetworkTypeBcp};
    AtUnused(self);
    if (numType)
        *numType = mCount(cAllNetworkTypes);
    return cAllNetworkTypes;
    }

static const char** AllTypesStringGet(ThaProfile self, uint8* numType)
    {
    static const char* cNetworkProfileTypeString[] = {"IPv4", "IPv6", "uMPLS", "mMPLS",
                                                      "OSI Network layer", "Appletalk",
                                                      "Novell IPX", "Xerox NS IDP", "Cisco Systems",
                                                      "Cisco Discovery Protocol", "NETBIOS Framing",
                                                      "IPv6 Header Compression", "802.1d Hello Packets",
                                                      "IBM Source Routing BPDU", "DEC LANBridge Spanning Tree",
                                                      "ETH/BCP"};
    AtUnused(self);
    if (numType)
        *numType = mCount(cNetworkProfileTypeString);
    return cNetworkProfileTypeString;
    }

static void OverrideThaProfile(ThaProfile self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfileOverride, mMethodsGet(self), sizeof(m_ThaProfileOverride));

        mMethodOverride(m_ThaProfileOverride, AllTypesGet);
        mMethodOverride(m_ThaProfileOverride, AllTypesStringGet);
        }

    mMethodsSet(self, &m_ThaProfileOverride);
    }

static void Override(ThaProfile self)
    {
    OverrideThaProfile(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaNetworkProfile);
    }

ThaProfile ThaNetworkProfileObjectInit(ThaProfile self, ThaProfilePool pool, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaProfileObjectInit(self, pool, profileId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaProfileManagerInternal.h
 * 
 * Created Date: Jun 7, 2016
 *
 * Description : Profile manager internal defintion
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPROFILEMANAGERINTERNAL_H_
#define _THAPROFILEMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "ThaProfileManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaProfileManagerMethods
    {
    eAtRet (*Init)(ThaProfileManager self);
    eAtRet (*Setup)(ThaProfileManager self);

    uint8 (*RuleToHw)(ThaProfileManager self, eThaProfileRule rule);
    eThaProfileRule (*HwToRule)(ThaProfileManager self, uint8 hwValue);

    ThaProfilePool (*ProfilePoolObjectCreate)(ThaProfileManager self);
    ThaProfileFinder (*ErrorProfileFinderObjectCreate)(ThaProfileManager self);
    ThaProfileFinder (*NetworkProfileFinderObjectCreate)(ThaProfileManager self);
    ThaProfileFinder (*OamProfileFinderObjectCreate)(ThaProfileManager self);
    ThaProfileFinder (*BcpProfileFinderObjectCreate)(ThaProfileManager self);
    ThaProfileFinder (*FcnProfileFinderObjectCreate)(ThaProfileManager self);

    }tThaProfileManagerMethods;

typedef struct tThaProfileManager
    {
    tAtObject super;
    const tThaProfileManagerMethods *methods;

    AtModuleEncap module;
    ThaProfilePool profilePool;

    ThaProfileFinder errorProfileFinder;
    ThaProfileFinder networkProfileFinder;
    ThaProfileFinder oamProfileFinder;
    ThaProfileFinder bcpProfileFinder;
    ThaProfileFinder fcnProfileFinder;

    }tThaProfileManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaProfileManager ThaProfileManagerObjectInit(ThaProfileManager self, AtModuleEncap module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPROFILEMANAGERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaProfileFinderInternal.h
 * 
 * Created Date: Jun 7, 2016
 *
 * Description : Profile finder internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPROFILEFINDERINTERNAL_H_
#define _THAPROFILEFINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/common/AtObjectInternal.h"
#include "ThaProfileFinder.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaProfileFinderMethods
    {
    eAtRet (*RuleSet)(ThaProfileFinder self, uint32 profileType, eThaProfileRule rule);
    ThaProfile (*ProfileFind)(ThaProfileFinder self, AtHdlcLink link);
    ThaProfile (*ProfileSearchByKey)(ThaProfileFinder self, uint32 key);
    AtList (*ProfileList)(ThaProfileFinder self);
    }tThaProfileFinderMethods;

typedef struct tThaProfileFinder
    {
    tAtObject super;
    const tThaProfileFinderMethods * methods;

    /* Private data */
    ThaProfileManager manager;
    uint32 key; /* Key to compare while finding a profile */
    }tThaProfileFinder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaProfileFinder ThaProfileFinderObjectInit(ThaProfileFinder self, ThaProfileManager manager);
ThaProfilePool ThaProfileFinderPoolGet(ThaProfileFinder self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPROFILEFINDERINTERNAL_H_ */


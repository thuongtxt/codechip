/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaProfileFinder.h
 * 
 * Created Date: Jun 8, 2016
 *
 * Description : Profile finder interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPROFILEFINDER_H_
#define _THAPROFILEFINDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaProfileManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaProfileFinder ThaOamProfileFinderNew(ThaProfileManager manager);
ThaProfileFinder ThaNetworkProfileFinderNew(ThaProfileManager manager);
ThaProfileFinder ThaErrorProfileFinderNew(ThaProfileManager manager);
ThaProfileFinder ThaBcpProfileFinderNew(ThaProfileManager manager);
ThaProfileFinder ThaFcnProfileFinderNew(ThaProfileManager manager);

/* Internal interface */
ThaProfileFinder ThaProfileFinderReset(ThaProfileFinder self);

/* Public interface */
eAtRet ThaProfileFinderRuleSet(ThaProfileFinder self, uint32 profileType, eThaProfileRule rule);
ThaProfile ThaProfileFinderProfileForLinkFind(ThaProfileFinder self, AtHdlcLink link);
eAtRet ThaProfileFinderKeySet(ThaProfileFinder self, uint32 key);

#ifdef __cplusplus
}
#endif
#endif /* _THAPROFILEFINDER_H_ */


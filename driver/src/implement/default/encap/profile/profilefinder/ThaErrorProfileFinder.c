/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaErrorProfileFinder.c
 *
 * Created Date: Jun 14, 2016
 *
 * Description : Error profile finder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaProfileFinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaErrorProfileFinder
    {
    tThaProfileFinder super;
    }tThaErrorProfileFinder;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaProfileFinderMethods m_ThaProfileFinderOverride;

/* Save super implementation */
static const tThaProfileFinderMethods *m_ThaProfileFinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList ProfileList(ThaProfileFinder self)
    {
    return ThaProfilePoolErrorProfileListGet(ThaProfileFinderPoolGet(self));
    }

static void OverrideThaProfileFinder(ThaProfileFinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaProfileFinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaProfileFinderOverride, mMethodsGet(self), sizeof(m_ThaProfileFinderOverride));

        mMethodOverride(m_ThaProfileFinderOverride, ProfileList);
        }

    mMethodsSet(self, &m_ThaProfileFinderOverride);
    }

static void Override(ThaProfileFinder self)
    {
    OverrideThaProfileFinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaErrorProfileFinder);
    }

static ThaProfileFinder ObjectInit(ThaProfileFinder self, ThaProfileManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaProfileFinderObjectInit(self, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaProfileFinder ThaErrorProfileFinderNew(ThaProfileManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaProfileFinder newFinder = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFinder == NULL)
        return NULL;

    return ObjectInit(newFinder, manager);
    }

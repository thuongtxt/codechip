/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ThaProfileFinder.c
 *
 * Created Date: Jun 7, 2016
 *
 * Description : Profile finder implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaProfileFinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaProfileFinderMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaProfileManager Manager(ThaProfileFinder self)
    {
    return self->manager;
    }

static eAtRet RuleSet(ThaProfileFinder self, uint32 profileType, eThaProfileRule rule)
    {
    AtList listProfile = mMethodsGet(self)->ProfileList(self);
    ThaProfile aProfile = (ThaProfile)AtListObjectGet(listProfile, 0);
    uint8 numProfileType, profileType_i;
    uint16* profileTypes = ThaProfileAllTypesGet(aProfile, &numProfileType);
    uint32 typeMask;
    uint8 typeShift;

    /* All profile */
    for (profileType_i = 0; profileType_i < numProfileType; profileType_i++)
        {
        if ((profileType & profileTypes[profileType_i]) == 0)
            continue;

        typeMask = ThaProfileTypeBitMask(aProfile, profileTypes[profileType_i]);
        typeShift = ThaProfileTypeBitShift(aProfile, profileTypes[profileType_i]);
        mRegFieldSet(self->key, type, ThaProfileManagerRuleToHw(Manager(self), rule));
        }

    return cAtOk;
    }

static ThaProfile ProfileFind(ThaProfileFinder self, AtHdlcLink link)
    {
    AtList listProfile = mMethodsGet(self)->ProfileList(self);
    AtIterator iterator = AtListIteratorCreate(listProfile);
    ThaProfile profile, willUseProfile = NULL;

    while ((profile = (ThaProfile)AtIteratorNext(iterator)) != NULL)
        {
        uint32 hwValue = ThaProfileKeyValueGet(profile);
        if (hwValue == self->key)
            {
            AtObjectDelete((AtObject)iterator);
            return profile;
            }

        /* If profile is un-used, cache it, may be we need to use it if could not find match profile */
        if (ThaProfileIsFree(profile) || ThaProfileIsOnlyUsedByLink(profile, link))
            {
            if (willUseProfile == NULL)
                willUseProfile = profile;
            }
        }

    AtObjectDelete((AtObject)iterator);

    /* Can not find match profile */
    /* There is no un-used profile */
    if (willUseProfile == NULL)
        return NULL;

    ThaProfileKeyValueSet(willUseProfile, self->key);
    return willUseProfile;
    }

static AtList ProfileList(ThaProfileFinder self)
    {
    AtUnused(self);
    return NULL;
    }

static void MethodsInit(ThaProfileFinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, RuleSet);
        mMethodOverride(m_methods, ProfileFind);
        mMethodOverride(m_methods, ProfileList);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaProfileFinder);
    }

ThaProfileFinder ThaProfileFinderObjectInit(ThaProfileFinder self, ThaProfileManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->manager = manager;

    return self;
    }

eAtRet ThaProfileFinderRuleSet(ThaProfileFinder self, uint32 profileType, eThaProfileRule rule)
    {
    if (self)
        return mMethodsGet(self)->RuleSet(self, profileType, rule);

    return cAtErrorNullPointer;
    }

eAtRet ThaProfileFinderKeySet(ThaProfileFinder self, uint32 key)
    {
    if (self)
        self->key = key;
    return cAtErrorNullPointer;
    }

ThaProfile ThaProfileFinderProfileForLinkFind(ThaProfileFinder self, AtHdlcLink link)
    {
    if (self)
        return mMethodsGet(self)->ProfileFind(self, link);

    return NULL;
    }

ThaProfileFinder ThaProfileFinderReset(ThaProfileFinder self)
    {
    if (self == NULL)
        return NULL;

    self->key = 0;
    return self;
    }

ThaProfilePool ThaProfileFinderPoolGet(ThaProfileFinder self)
    {
    if (self == NULL)
        return NULL;

    return ThaProfileManagerProfilePoolGet(self->manager);
    }

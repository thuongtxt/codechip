/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encapsulation
 * 
 * File        : ThaHdlcChannelInternal.h
 * 
 * Created Date: Sep 12, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHDLCCHANNELINTERNAL_H_
#define _THAHDLCCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/encap/AtHdlcChannelInternal.h"
#include "ThaHdlcChannel.h"
#include "ThaModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*
 * Sapi/Protocol Field mode
 */
typedef enum eThaPosSapiMd
    {
    cThaPosSapi1byte = 0,   /* one byte to indicate the SAPI/Protocol field */
    cThaPosSapi2byte = 1,   /* two bytes to indicate the SAPI/Protocol field */
    cThaPosSapiDisable      /* Insertion/ comparing */
    } eThaPosSapiMd;

typedef struct tThaHdlcChannelMethods
    {
    uint32 (*ChannelCtrlOffset)(ThaHdlcChannel self);
    eAtRet (*AddressControlInsertionEnable)(ThaHdlcChannel self, eBool insertEnable);
    eBool  (*AddressControlInsertionIsEnabled)(ThaHdlcChannel self);
    eAtRet (*SapiModeSet)(ThaHdlcChannel self, eThaPosSapiMd sapiMode);
    eAtRet (*FlagModeSet)(ThaHdlcChannel self, uint8 flagMode);
    uint8  (*CircuitSliceGet)(ThaHdlcChannel self);
    eBool  (*CircuitIsHo)(ThaHdlcChannel self);
    eAtRet (*HdlcPwPayloadTypeSet)(ThaHdlcChannel self, eAtPwHdlcPayloadType payloadType);
    ThaHdlcChannel (*PhysicalChannelGet)(ThaHdlcChannel self);
    uint32 (*SurCounterId)(ThaHdlcChannel self);
    }tThaHdlcChannelMethods;

typedef struct tThaHdlcChannel
    {
    tAtHdlcChannel super;
    const tThaHdlcChannelMethods *methods;

    /* Private data */
    eBool isHighBandwidth;

    /* Link information, used to backup when hot change HDLC frame */
    uint8 linkPhase;
    uint8 packetMode;
    uint8 txTrafficEn;
    uint8 rxTrafficEn;
    AtEthFlow flow;
    
    /* To backup OAM flow configuration */
    eBool oamFlowIsCreated;
    uint8 oamSourceMac[cAtMacAddressLen];
    uint8 oamDestMac[cAtMacAddressLen];
    tAtEthVlanDesc oamVlanDesc;
    }tThaHdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel ThaHdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId,eAtHdlcFrameType frameType, AtModuleEncap module);
AtHdlcChannel ThaHdlcChannelNew(uint32 channelId,eAtHdlcFrameType frameType, AtModuleEncap module);

eAtRet ThaHdlcChannelHighBandwidthSet(ThaHdlcChannel self, eBool isHighBandwidth);
uint8 ThaHdlcChannelPartOfEncap(ThaModuleEncap self, ThaHdlcChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAHDLCCHANNELINTERNAL_H_ */


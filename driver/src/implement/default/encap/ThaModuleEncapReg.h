/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaModuleEncapReg.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Author      : huandh
 *
 * Description : This file contains constant definition for ENCAP/DECAPSULATION register.
 * 
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEENCAPREG_H_
#define _THAMODULEENCAPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Master Control
Reg Addr: 0x001C0003
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegDatEncMstCtrl                0x001C0003
#define cThaRegDatEnc56KStuffBitCtrMask     cBit2
#define cThaRegDatEnc56KStuffBitCtrShift    2

#define cThaRegDatEnc56KStuffvalMask        cBit1
#define cThaRegDatEnc56KStuffvalShift       1

#define cThaRegDatEncPosLsbFirstMask        cBit0
#define cThaRegDatEncPosLsbFirstShift       0

#define cThaRegDatEncMstCtrlNoneEditableFieldsMask cBit31_3

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Operation Control 1
Reg Addr: 0x001C8000-0x001C83FF
The address format for these registers is 0x001C8000 + CHID
Where,  CHID is a ENC channel ID. It is from 0 to 1023.
Description: The register controls the operations of the data encapsulation for
HDLC protocol.
------------------------------------------------------------------------------*/
#define cThaRegDatEncOptCtrl1               0x001C8000

#define cThaRegDatEncapPosScrEnbMask     cBit9
#define cThaRegDatEncapPosScrEnbShift    9

/* cBit7_cBit8 only Bit stuffing mode */
#define cThaRegDatEncapPosRate56KMask     cBit8
#define cThaRegDatEncapPosRate56KShift    8

#define cThaRegDatEncapPosIdleModeMask     cBit7
#define cThaRegDatEncapPosIdleModeShift    7

#define cThaRegDatEncapPosSapiModeMask   cBit6
#define cThaRegDatEncapPosSapiModeShift  6

#define cThaRegDatEncapPosSapiInsMask    cBit5
#define cThaRegDatEncapPosSapiInsShift   5

#define cThaRegDatEncapPosCtrlInsMask    cBit4
#define cThaRegDatEncapPosCtrlInsShift   4

#define cThaRegDatEncapPosFcsMdMask      cBit3
#define cThaRegDatEncapPosFcsMdShift     3

#define cThaRegDatEncapPosFcsInsMask      cBit2
#define cThaRegDatEncapPosFcsInsShift     2

#define cThaRegDatEncapPosFlagModeMask    cBit1
#define cThaRegDatEncapPosFlagModeShift   1
#define cThaRegDatEncapPosFlagModeOneFlag 0

#define cThaRegDatEncapStuffingModeMask    cBit0
#define cThaRegDatEncapStuffingModeShift   0

#define cThaRegDatEncOptCtrl1NoneEditableFieldsMask (cBit31_10 | cBit8_7)

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Operation Control 2
Reg Addr: 0x001C9000-0x001C93FF
The address format for these registers is 0x001C9000 + CHID
Where, CHID indicates ENC channel ID. It is from 0 to 1023
Description: The register controls the operations of the data encapsulation for
HDLC/PPP/LAPS.
------------------------------------------------------------------------------*/
#define cThaRegDatEncOptCtrl2           0x001C9000

#define cThaRegDatEncapAddValMask     cBit31_24
#define cThaRegDatEncapAddValShift    24

#define cThaRegDatEncapCtrlValMask     cBit23_16
#define cThaRegDatEncapCtrlValShift    16

#define cThaRegDatEncOptCtrl2NoneEditableFieldsMask 0

/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Master Control
Reg Addr: 0x00200003
Description: Description: The register controls for drop FCS error packet and the
order of bit/byte for LAPS/PPP/HDLC protocol transparent.
------------------------------------------------------------------------------*/
#define cThaRegDatDecMasterCtrl            0x00200003

#define cThaRegDatDecapPosLsbFirstMask     cBit1
#define cThaRegDatDecapPosLsbFirstShift    1

#define cThaRegDatDec56KStuffBitCtrMask     cBit0
#define cThaRegDatDec56KStuffBitCtrShift    0

#define cThaRegDatDecMasterCtrlNoneEditableFieldsMask cBit31_2

/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Operation Control 1
Reg Addr: 0x00208000 � 0x0020803FF
The address format for these registers is 0x00208000 + CHID
Where,  CHID is a Dec channel ID. It is from 0 to 1023.
Description: The register controls the operations of the data encapsulation
for ATM and HDLC/PPP
------------------------------------------------------------------------------*/
#define cThaRegDatDecOptCtrl1       0x00208000

#define cThaRegDatDecPosScrEnbMask     cBit7
#define cThaRegDatDecPosScrEnbShift    7

#define cThaRegDatDecPosFcsMdMask      cBit5
#define cThaRegDatDecPosFcsMdShift     5

#define cThaRegDatDecPosFcsChkMask      cBit4
#define cThaRegDatDecPosFcsChkShift     4

#define cThaRegDatDecPosAdrCtrlCompEnMask    cBit1
#define cThaRegDatDecPosAdrCtrlCompEnShift   1

#define cThaRegDatDecStuffingModeMask    cBit0
#define cThaRegDatDecStuffingModeShift   0

#define cThaRegDatDecOptCtrl1NoneEditableFieldsMask cBit31_8

/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Operation Control 2
Reg Addr:  0x00209000 � 0x002093FF
The address format for these registers is 0x00209000 + CHID
Where,  CHID is a DEC channel ID. It is from 0 to 1023.
Description: The register controls the operations of the data encapsulation for
HDLC/PPP/LAPS.
------------------------------------------------------------------------------*/
#define cThaRegDatDecOptCtrl2       0x00209000

#define cThaRegDatDecAddValMask     cBit31_24
#define cThaRegDatDecAddValShift    24

#define cThaRegDatDecCtrlValMask     cBit23_16
#define cThaRegDatDecCtrlValShift    16

#define cThaRegDatDecOptCtrl2NoneEditableFieldsMask 0

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Performance Counter 1 Read Only
Address: 0x001C2000-0x001C23FF
The address format for these registers is 0x001C2000 + CHID
Where,	CHID is a ENC channel ID. It is from 0 to 1023.
Description: The data encapsulation performance counter 1 is used for HDLC/PPP/LAPS
protocols. It is the good package counter. It indicates a number of good packages
which have been transmitted successfully.
------------------------------------------------------------------------------*/
#define cThaRegDatEncPerCntRO1        0x001C3000

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Performance Counter 2 Read Only
Address: 0x001C2800-0x001C2BFF
The address format for these registers is 0x001C2800 + CHID
Where,	CHID is a ENC channel ID. It is from 0 to 1023.
Description: The data encapsulation performance counter 2 is used for HDLC/PPP/LAPS protocols.
It is the abort package counter. It indicates a number of abort packets which have been transmitted.
-------------------------------------------------------------*/
#define cThaRegDatEncPerCntRO2        0x001C3800

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Performance Counter 1 Read-to-Clear
Address: 0x001C3000-0x001C33FF
The address format for these registers is 0x001C3000 + CHID
Where,  CHID is a ENC channel ID. It is from 0 to 1023.
Description: The data encapsulation performance counter 1 is used for HDLC/PPP/LAPS protocols.
It is the good package counter. It indicates a number of good packages which
have been transmitted successfully.
-------------------------------------------------------------*/
#define cThaRegDatEncPerCntR2C1        0x001C2000

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Performance Counter 2 Read-to-Clear
Address: 0x001C3800-0x001C3BFF
The address format for these registers is 0x001C3800 + CHID
Where,  CHID is a ENC channel ID. It is from 0 to 1023.
Description: The data encapsulation performance counter 2 is used for
HDLC/PPP/LAPS protocols. It is the abort package counter.
It indicates a number of abort packets which have been transmitted.
-------------------------------------------------------------*/
#define cThaRegDatEncPerCntR2C2        0x001C2800

/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Performance Counter 2 Latching
Address: 0x00200008
-------------------------------------------------------------*/
#define cThaRegDatDecPerCnt2        (0x00200008)

/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Performance Counter 3 Latching
Address: 0x00200009
-------------------------------------------------------------*/
#define cThaRegDatDecPerCnt3        (0x00200009)
/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Performance Counter 4 Latching
Address: 0x0020000A
-------------------------------------------------------------*/
#define cThaRegDatDecPerCnt4        (0x0020000A)
/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Performance Counter 5 Latching
Address: 0x0020000B
-------------------------------------------------------------*/
#define cThaRegDatDecPerCnt5       (0x0020000B)
/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Performance Counter 6 Latching
Address: 0x0020000D
-------------------------------------------------------------*/
#define cThaRegDatDecPerCnt6       (0x0020000D)

/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Performance Counters
Address: 0x00204000 � 0x002043FF
The address format for these registers is 0x00204000 + CHID
Where,	CHID is a DEC channel ID. It is from 0 to 1023.
------------------------------------------------------------------------------*/
#define cThaRegDatDecPerCntRo         0x00206000
#define cThaRegDatDecPerCntR2c        0x00204000

/*------------------------------------------------------------------------------
Reg Name: Thalassa Map Idle Pattern Control
Address: 0x1A1800 - 0x1A1800
The address format for these registers is 0x1A1800
Description: The registers provide idle pattern.
------------------------------------------------------------------------------*/
#define cThaMapIdlePatternControl 0x1A0C00
#define cThaMapIdlePatternMask    cBit7_0
#define cThaMapIdlePatternShift   0

/*------------------------------------------------------------------------------
Reg Name: Data Encapsulation Performance Byte Counter
Address: 0x001C1400 � 0x001C17FF (R_O), 0x001C1000 � 0x1C13FF(R2C),
The address format for these registers is 0x001c1400 + LID (R_O), 0x001C1000 + LID (R2C)
Where: LID (0 � 1024) Channel IDs.
Description: The data encapsulation performance Byte Counter 2 is used for
HDLC/PPP/LAPS protocols. It is the byte counter.
-------------------------------------------------------------*/
#define cThaRegDatEncPerByteCounter(r2c)   ((r2c) ? 0x001C1000 : 0x001C1400)

/*------------------------------------------------------------------------------
Reg Name: Data Decapsulation Performance Byte Counter
Address: 0x00201400 � 0x002017FF (R_O), 0x00201000 � 0x002013FF(R2C),
The address format for these registers is 0x00201400 + LID (R_O), 0x00201000 + LID (R2C)
Where: LID (0 � 1024) Channel IDs
Description: The data encapsulation performance Byte Counter is used for
HDLC/PPP/LAPS protocols. It is the byte counter.
-------------------------------------------------------------*/
#define cThaRegDatDecPerByteCounter(r2c)   ((r2c) ? 0x00201000 : 0x00201400)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEENCAPREG_H_ */


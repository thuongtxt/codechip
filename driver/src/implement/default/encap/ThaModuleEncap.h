/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaModuleEncap.h
 * 
 * Created Date: Sep 11, 2012
 *
 * Description : Encapsulation module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEENCAP_H_
#define _THAMODULEENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleEncap * ThaModuleEncap;

typedef enum eThaModuleEncapBlockType
    {
    cThaModuleEncapBlockTypeLowBandwidth,
    cThaModuleEncapBlockTypeHighBandwidth
    }eThaModuleEncapBlockType;

/* TODO: Rename these classes */
typedef struct tAf4ModuleEncap * Af4ModuleEncap;
typedef struct tThaGfpChannel  * ThaGfpChannel;
typedef struct tAf4HdlcChannel * Af4HdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap ThaModuleEncapNew(AtDevice device);

eBool ThaModuleEncapHdlcByteCountersAreSupported(ThaModuleEncap self);
eBool ThaModuleEncapHdlcIdleByteCountersAreSupported(ThaModuleEncap self);
eBool ThaModuleEncapAcfcSupported(ThaModuleEncap self);
eBool ThaModuleEncapFrameRelayIsSupported(ThaModuleEncap self);

/* Util */
eAtRet ThaModuleEncapHardwareBlockAllocate(AtChannel self, AtEncapChannel encapChannel, eThaModuleEncapBlockType blockType);
uint32 ThaModuleEncapNumEncapPerPart(ThaModuleEncap self);

/* Counter address */
uint32 ThaModuleEncapDataEncapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear);
uint32 ThaModuleEncapDataDecapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear);
uint32 ThaModuleEncapDataEncapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear);
uint32 ThaModuleEncapDataDecapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear);

/* For debugging */
void ThaModuleEncapEncapChannelRegsShow(AtEncapChannel encapChannel);
void ThaModuleEncapHdlcBundleRegsShow(AtHdlcBundle channel);

/* Product concretes */
AtModuleEncap Tha60060011ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60091023ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60031032ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60031033ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60031035ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60091135ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60091132ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60210012ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60210061ModuleEncapNew(AtDevice device);
AtModuleEncap Tha60290081ModuleEncapNew(AtDevice device);
AtModuleEncap Tha6A290081ModuleEncapNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEENCAP_H_ */


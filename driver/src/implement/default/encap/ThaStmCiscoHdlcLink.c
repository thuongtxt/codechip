/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : ThaStmCiscoHdlcLink.c
 *
 * Created Date: Jan 23, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../man/ThaDeviceInternal.h"
#include "../ppp/ThaStmPppLink.h"
#include "../ppp/ThaStmPppLinkInternal.h"
#include "../ppp/ThaPppLink.h"
#include "../ppp/ThaPppLinkInternal.h"
#include "../ppp/ThaMpigReg.h"
#include "../ppp/ThaMpegReg.h"
#include "ThaCiscoHdlcLinkInternal.h"
#include "../eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tThaPppLinkMethods m_ThaPppLinkOverride;
static tAtPppLinkMethods m_AtPppLinkOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPppLinkMethods *m_AtPppLinkMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmCiscoHdlcLink);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtChannelMethods->RxTrafficEnable((AtChannel)self, enable);
    ret |= m_AtPppLinkMethods->PhaseSet((AtPppLink)self, cAtPppLinkPhaseNetworkActive);

    return ret;
    }

static uint32 LinkOffSet(ThaPppLink self)
    {
    return mMethodsGet(self)->DefaultOffset(self);
    }

static ThaModuleEncap EncapModule(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self));
    return (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eAtRet PppPacketModeSet(ThaPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule(self);
    uint32 pktModeMask = mMethodsGet(encapModule)->PKtModeMask(encapModule);
    uint32 pktModeShift = mMethodsGet(encapModule)->PKtModeShift(encapModule);
    regAddr = cThaRegMPIGDATLookupLinkCtrl + LinkOffSet(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, pktModeMask, pktModeShift, 0x6);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    regAddr = cThaRegThalassaMLPppEgrPppLinkCtrl1 + LinkOffSet(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldIns(&regVal, cThaMPEGEncTypeMask, cThaMPEGEncTypeShift, 0x6);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ThaCiscoHdlcInit(self);
    }

static ThaEthFlowController EthFlowControllerGet(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return ThaModuleEthCiscoHdlcFlowControllerGet(ethModule);
    }

static void OverrideAtPppLink(ThaStmCiscoHdlcLink self)
    {
    AtPppLink pppLink = (AtPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPppLinkMethods = mMethodsGet(pppLink);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, mMethodsGet(pppLink), sizeof(m_AtPppLinkOverride));
        }

    mMethodsSet(pppLink, &m_AtPppLinkOverride);
    }

static void OverrideThaPppLink(ThaStmCiscoHdlcLink self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPppLinkOverride));

        mMethodOverride(m_ThaPppLinkOverride, EthFlowControllerGet);
        mMethodOverride(m_ThaPppLinkOverride, PppPacketModeSet);
        }

    mMethodsSet(link, &m_ThaPppLinkOverride);
    }

static void OverrideAtChannel(ThaStmCiscoHdlcLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaStmCiscoHdlcLink self)
    {
    OverrideAtChannel(self);
    OverrideAtPppLink(self);
    OverrideThaPppLink(self);
    }

ThaStmPppLink ThaStmCiscoHdlcLinkObjectInit(ThaStmPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPppLinkObjectInit((AtPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaStmCiscoHdlcLink)self);
    m_methodsInit = 1;

    return self;
    }

ThaStmPppLink ThaStmCiscoHdlcLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaStmPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ThaStmCiscoHdlcLinkObjectInit(newLink, hdlcChannel);
    }

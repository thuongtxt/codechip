/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaGfpInterruptManager.c
 *
 * Created Date: Sep 20, 2019
 *
 * Description : Default GFP interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaGfpInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)             ((ThaGfpInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaGfpInterruptManagerMethods m_methods;

/* Override */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEncap ModuleEncap(AtInterruptManager self)
    {
    return (AtModuleEncap)AtInterruptManagerModuleGet(self);
    }

static AtGfpChannel GfpChannelFromHwIdGet(ThaGfpInterruptManager self, uint32 hwLocalId)
    {
    return (AtGfpChannel)AtModuleEncapChannelGet(ModuleEncap((AtInterruptManager)self), (uint16)hwLocalId);
    }

static void Override(AtInterruptManager self)
    {
    AtUnused(self);
    }

static void MethodsInit(ThaGfpInterruptManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, GfpChannelFromHwIdGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaGfpInterruptManager);
    }

AtInterruptManager ThaGfpInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaGfpInterruptManager)self);
    m_methodsInit = 1;

    return self;
    }

AtGfpChannel ThaGfpInterruptManagerGfpChannelFromHwIdGet(ThaGfpInterruptManager self, uint32 hwLocalId)
    {
    if (self)
        return mMethodsGet(self)->GfpChannelFromHwIdGet(self, hwLocalId);
    return NULL;
    }

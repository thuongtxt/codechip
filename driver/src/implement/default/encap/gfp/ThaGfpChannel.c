/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaGfpChannel.c
 *
 * Created Date: May 19, 2014
 *
 * Description : GFP channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModuleEncap.h"
#include "ThaGfpChannelInternal.h"
#include "ThaModuleEncapGfpReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf4DecDir                      0
#define cAf4EncDir                      1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaGfpChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaGfpChannelMethods m_methods;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;
static tAtGfpChannelMethods   m_AtGfpChannelOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEncapRet ExpectedPtiSet(AtGfpChannel self, uint8 expectedPti)
    {
    AtUnused(self);
    AtUnused(expectedPti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEncapRet ExpectedExiSet(AtGfpChannel self, uint8 expectedExi)
    {
    AtUnused(self);
    AtUnused(expectedExi);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEncapRet ExpectedUpiSet(AtGfpChannel self, uint8 expectedUpi)
    {
    AtUnused(self);
    AtUnused(expectedUpi);
    return cAtErrorModeNotSupport;
    }

static uint8 ExpectedPtiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 ExpectedExiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 ExpectedUpiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x1;
    }

static eAtModuleEncapRet FcsMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool FcsMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEncapRet PtiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool PtiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet UpiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool UpiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultOffset(AtGfpChannel self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static uint32 GfpHeaderCfgRegister(ThaGfpChannel self)
    {
    return cAf4RegGfpHeaderCfg + DefaultOffset((AtGfpChannel)self);
    }

static eAtModuleEncapRet TxPtiSet(AtGfpChannel self, uint8 txPti)
    {
	uint32 regAddr =  mMethodsGet((ThaGfpChannel)self)->GfpHeaderCfgRegister((ThaGfpChannel)self);
	uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf4RegGfpPtiMask, cAf4RegGfpPtiShift, txPti);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint8 TxPtiGet(AtGfpChannel self)
    {
	uint32 regVal = mChannelHwRead(self, mMethodsGet((ThaGfpChannel)self)->GfpHeaderCfgRegister((ThaGfpChannel)self), cAtModuleEncap);
	uint8 txPti  = 0;
	mFieldGet(regVal, cAf4RegGfpPtiMask, cAf4RegGfpPtiShift, uint8, &txPti);

    return txPti;
    }

static eAtModuleEncapRet TxExiSet(AtGfpChannel self, uint8 txExi)
    {
	uint32 regAddr = mMethodsGet((ThaGfpChannel)self)->GfpHeaderCfgRegister((ThaGfpChannel)self);
	uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf4RegGfpExiMask, cAf4RegGfpExiShift, txExi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint8 TxExiGet(AtGfpChannel self)
    {
	uint32 regAddr = mMethodsGet((ThaGfpChannel)self)->GfpHeaderCfgRegister((ThaGfpChannel)self);
	uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
	uint8 txExi = 0;
	mFieldGet(regVal, cAf4RegGfpExiMask, cAf4RegGfpExiShift, uint8, &txExi);

    return txExi;
    }

static eAtModuleEncapRet TxUpiSet(AtGfpChannel self, uint8 txUpi)
    {
	uint32 regAddr = mMethodsGet((ThaGfpChannel)self)->GfpHeaderCfgRegister((ThaGfpChannel)self);
	uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf4RegGfpUpiMask, cAf4RegGfpUpiShift, txUpi);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint8 TxUpiGet(AtGfpChannel self)
    {
	uint32 regAddr = mMethodsGet((ThaGfpChannel)self)->GfpHeaderCfgRegister((ThaGfpChannel)self);
	uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
	uint8 txUpi = 0;
	mFieldGet(regVal, cAf4RegGfpUpiMask, cAf4RegGfpUpiShift, uint8, &txUpi);

    return txUpi;
    }

static eAtModuleEncapRet TxPfiSet(AtGfpChannel self, uint8 txPfi)
    {
    return AtGfpChannelTxFcsEnable(self, (txPfi > 0) ? cAtTrue : cAtFalse);
    }

static uint8 TxPfiGet(AtGfpChannel self)
    {
	return AtGfpChannelTxFcsIsEnabled(self) ? 0x1 : 0x0;
    }

static eBool GfptIsSupported(AtGfpChannel self)
    {
    return AtChannelHwIdGet((AtChannel)self) < 2;
    }

static eBool FrameModeIsSupported(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    if (frameMappingMode == cAtGfpFrameModeGfpF)
        return cAtTrue;

    if (frameMappingMode == cAtGfpFrameModeGfpT)
        return GfptIsSupported(self);

    return cAtFalse;
    }

static eAtModuleEncapRet FrameModeSet(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
	uint32 channelId = AtChannelHwIdGet((AtChannel)self);
	uint32 regAddr;
	uint32 regVal = 0;
	uint8 gfptEnable = (frameMappingMode == cAtGfpFrameModeGfpT) ? 1 : 0;

	/* Just supports 2 channels for GFP-T, user must choose to use GFP-T or GFPF for application, not mix */
	if ((channelId > 1) && (frameMappingMode == cAtGfpFrameModeGfpT))
        return cAtErrorModeNotSupport;

    /* Enable/disable GFPF at DECAP direction */
	regVal = mChannelHwRead(self, cAf4DecGfpComCfgReg, cAtModuleEncap);
    mFieldIns(&regVal, cAf4DecGfpEnMask, cAf4DecGfpEnShift, gfptEnable);
    mChannelHwWrite(self, cAf4DecGfpComCfgReg, regVal, cAtModuleEncap);

	/* Enable/disable GFPT at DECAP direction */
    regVal = mChannelHwRead(self, cGfpTEnableReg, cAtModuleEncap);
	mFieldIns(&regVal, cGfpTEnableMask(channelId), cGfpTEnableShift(channelId), gfptEnable);
	mFieldIns(&regVal, cGfpTEnChnMask(channelId), cGfpTEnChnShift(channelId), gfptEnable);
	mChannelHwWrite(self, cGfpTEnableReg, regVal, cAtModuleEncap);

	/* Select channel is GFP */
	regAddr = mMethodsGet(mThis(self))->DecModeSel(mThis(self), channelId);
	regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
	mFieldIns(&regVal, cAf4HdlcGfpSelMask(channelId), cAf4HdlcGfpSelShift(channelId), 1);
	mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
	mChannelHwWrite(self, mMethodsGet(mThis(self))->EncModeSel(mThis(self), channelId), regVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtGfpFrameMode FrameModeGet(AtGfpChannel self)
    {
	uint32 channelId   = AtChannelHwIdGet((AtChannel)self);
	uint32 regAddr = cGfpTEnableReg;
	uint32 regVal = 0;
	uint8 enableGfp = 0;
	uint8 enableGfpT = 0;

	regAddr = mMethodsGet(mThis(self))->EncModeSel(mThis(self), channelId);
	regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
	mFieldGet(regVal, cAf4HdlcGfpSelMask(channelId), cAf4HdlcGfpSelShift(channelId), uint8, &enableGfp);
	if (enableGfp == 0)
		return cAtErrorModeNotSupport;

	/* HW just support 2 channels for GFPT */
	if (channelId > 1)
		return cAtGfpFrameModeGfpF;

	regAddr = cGfpTEnableReg;
	regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
	mFieldGet(regVal, cGfpTEnChnMask(channelId), cGfpTEnChnShift(channelId), uint8, &enableGfpT);
	if (enableGfpT)
		return cAtGfpFrameModeGfpT;

	return cAtGfpFrameModeGfpF;
    }

static eAtModuleEncapRet TxFcsEnable(AtGfpChannel self, eBool enable)
    {
    uint32 channelId = AtChannelHwIdGet((AtChannel)self);
    uint32 regAddr   = mMethodsGet(mThis(self))->AggCfgGfpFcsRx(mThis(self), channelId);
    uint32 regVal    = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf4AggGfpFcsMask(channelId), cAf4AggGfpFcsShift(channelId), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);
    return cAtOk;
    }

static eBool TxFcsIsEnabled(AtGfpChannel self)
    {
    uint32 channelId = AtChannelHwIdGet((AtChannel)self);
    uint32 regAddr = mMethodsGet(mThis(self))->AggCfgGfpFcsRx(mThis(self), channelId);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    return ((regVal & cAf4AggGfpFcsMask(channelId)) ? cAtTrue : cAtFalse);
    }

static eAtModuleEncapRet FlowBind(AtEncapChannel self, AtEthFlow flow)
    {
    /* This product use static allocation, so do not support unbinding */
    if (flow == NULL)
        return cAtErrorModeNotSupport;

    /* And the flow ID must match the GFP channel */
    if (AtChannelIdGet((AtChannel)flow) != AtChannelIdGet((AtChannel)self))
        return cAtErrorModeNotSupport;

    /* They are internally bound, just do nothing */
    return cAtOk;
    }

static AtEthFlow BoundFlowGet(AtEncapChannel self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    return AtModuleEthFlowGet(ethModule, (uint16)AtChannelIdGet((AtChannel)self));
    }

static eAtModuleEncapRet TxCsfEnable(AtGfpChannel self, eBool enable)
    {
	uint32 channelId = AtChannelHwIdGet((AtChannel)self);
	uint32 regAddr;
	uint32 regVal;

	regAddr = cAf4RegAggCfgGfpCsfTx(channelId);
	regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf4AggGfpCsfMask(channelId), cAf4AggGfpCsfShift(channelId), enable);
	mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static eBool TxCsfIsEnabled(AtGfpChannel self)
    {
	uint32 channelId = AtChannelHwIdGet((AtChannel)self);
	uint32 regAddr, regVal;

	regAddr = cAf4RegAggCfgGfpCsfTx(channelId);
	regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
	return (regVal & cAf4AggGfpCsfMask(channelId)) ? cAtTrue : cAtFalse;
    }

static eAtModuleEncapRet TxCsfUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    uint32 channelId = AtChannelHwIdGet((AtChannel)self);
    uint32 regAddr;
    uint32 regVal;
    uint8 hwType;

    if (txUpi == 0x1)
        hwType = 0x0;
    else if (txUpi == 0x2)
        hwType = 0x1;
    else
        return cAtErrorModeNotSupport;

    regAddr = cAf4RegAggCfgGfpCsfTypeTx(channelId);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf4AggGfpCsfTypeMask(channelId), cAf4AggGfpCsfTypeShift(channelId), hwType);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEncap);

    return cAtOk;
    }

static uint8 TxCsfUpiGet(AtGfpChannel self)
    {
    uint32 channelId = AtChannelHwIdGet((AtChannel)self);
    uint32 regAddr, regVal;

    regAddr = cAf4RegAggCfgGfpCsfTypeTx(channelId);
    regVal  = mChannelHwRead(self, regAddr, cAtModuleEncap);
    return (regVal & cAf4AggGfpCsfTypeMask(channelId)) ? 0x1 : 0x2;
    }

static AtConcateGroup AssignedVcg(AtGfpChannel self)
    {
    AtModuleConcate moduleConcate = (AtModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleConcate);
    return AtModuleConcateGroupGet(moduleConcate, AtChannelIdGet((AtChannel)self));
    }

static eAtRet VcgUnBind(AtEncapChannel self)
    {
    if (AssignedVcg((AtGfpChannel)self) == NULL)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eAtRet VcgBind(AtEncapChannel self, AtConcateGroup vcg)
    {
    if (vcg == AssignedVcg((AtGfpChannel)self))
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

/* Note: this product line does not support flexible binding */
static eAtRet PhyBind(AtEncapChannel self, AtChannel phyChannel)
    {
    eAtRet ret = cAtOk;

    if (phyChannel == NULL)
        ret |= VcgUnBind(self);
    else
        ret |= VcgBind(self, (AtConcateGroup)phyChannel);

    /* Let super deal with database */
    if (ret == cAtOk)
        ret = m_AtEncapChannelMethods->PhyBind(self, phyChannel);

    return ret;
    }

static AtChannel BoundPhyGet(AtEncapChannel self)
    {
    return (AtChannel)AssignedVcg((AtGfpChannel)self);
    }

static eAtModuleEncapRet PayloadScrambleEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PayloadScrambleIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEncapRet CoreHeaderScrambleEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool CoreHeaderScrambleIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 AlarmGet(AtChannel self)
    {
    uint32 channelId = AtChannelIdGet(self);
    uint32 regVal = mChannelHwRead(self,
                                   mMethodsGet(mThis(self))->RxGfpCsfStat(mThis(self), channelId),
                                   cAtModuleEncap);

    if (regVal & cAf4AggGfpCsfMask(channelId))
        return cAtGfpChannelAlarmCsf;

    return 0;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 CounterIndirectRead2Clear(ThaGfpChannel self, uint8 counterType, uint32 controlAddr, uint32 dataAddr)
    {
    uint32 regVal;
    tAtOsalCurTime  startTime, currentTime;
    uint32 elapseTime = 0;
    const uint32 cTimeoutMs = 500;
    AtOsal osal = AtSharedDriverOsalGet();
    const uint32 cInvalidCounter = 0xCAFECAFE;

    if (AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self)))
        return 0;

    /* Clear bit done and bit fail */
    mChannelHwWrite(self, controlAddr, (cAf4EncDecDoneMask|cAf4EncDecFailMask), cAtModuleEncap);
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cTimeoutMs)
        {
        if ((mChannelHwRead(self, controlAddr, cAtModuleEncap) & (cAf4EncDecDoneMask|cAf4EncDecFailMask)) == 0)
            break;

        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &currentTime, &startTime);
        }

    if (elapseTime >= cTimeoutMs)
        return cInvalidCounter;

    /* Create request */
    regVal = 0x0;
    mFieldIns(&regVal, cAf4EncDecTrigMask, cAf4EncDecTrigShift, 0);
    mFieldIns(&regVal, cAf4EncDecTypeMask, cAf4EncDecTypeShift, counterType);
    mFieldIns(&regVal, cAf4EncDecVcgMask, cAf4EncDecVcgShift, AtChannelIdGet((AtChannel)self));
    mChannelHwWrite(self, controlAddr, regVal, cAtModuleEncap);

    /* Make request */
    regVal = mChannelHwRead(self, controlAddr, cAtModuleEncap);
    mFieldIns(&regVal, cAf4EncDecTrigMask, cAf4EncDecTrigShift, 1);
    mChannelHwWrite(self, controlAddr, regVal, cAtModuleEncap);

    /* Wait for status */
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cTimeoutMs)
        {
        regVal = mChannelHwRead(self, controlAddr, cAtModuleEncap);
        if (regVal & cAf4EncDecDoneMask)
            break;

        /* Wait */
        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapseTime = mMethodsGet(osal)->DifferenceTimeInMs(osal, &currentTime, &startTime);
        }

    if (elapseTime >= cTimeoutMs)
        return cInvalidCounter;

    if (regVal & cAf4EncDecFailMask)
        return cInvalidCounter;

    return mChannelHwRead(self, dataAddr, cAtModuleEncap);
    }

uint32 ThaGfpChannelEncapCounterRead2Clear(ThaGfpChannel self, uint8 counterType)
    {
    return CounterIndirectRead2Clear(self,
                                     counterType,
                                     mMethodsGet(self)->EncPktHold0(self),
                                     mMethodsGet(self)->EncPktHold1(self));
    }

uint32 ThaGfpChannelDecapCounterRead2Clear(ThaGfpChannel self, uint8 counterType)
    {
    return CounterIndirectRead2Clear(self,
                                     counterType,
                                     mMethodsGet(self)->DecPktHold0(self),
                                     mMethodsGet(self)->DecPktHold1(self));
    }

static void ReadAndCacheAllCounters(ThaGfpChannel self)
    {
    self->counters.txidle    += ThaGfpChannelEncapCounterRead2Clear(self, cAf4EncapIdleFrmCntMd);
    self->counters.txgdFrm   += ThaGfpChannelEncapCounterRead2Clear(self, cAf4EncapGoodFrmCntMd);

    self->counters.rxidleFrm += ThaGfpChannelDecapCounterRead2Clear(self, cAf4EncapIdleFrmCntMd);
    self->counters.rxgdFrm   += ThaGfpChannelDecapCounterRead2Clear(self, cAf4EncapGoodFrmCntMd);
    self->counters.rxfcsErr  += ThaGfpChannelDecapCounterRead2Clear(self, cAf4EncapErrFcsFrmCntMd);
    self->counters.rxcsf     += ThaGfpChannelDecapCounterRead2Clear(self, cAf4EncapCsfFrmCntMd);
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool clear)
    {
    AtOsal osal;

    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    osal = AtSharedDriverOsalGet();
    ReadAndCacheAllCounters(mThis(self));
    mMethodsGet(osal)->MemCpy(osal, pAllCounters, &(mThis(self)->counters), sizeof(tAtGfpChannelCounters));

    if (clear)
        mMethodsGet(osal)->MemInit(osal, &(mThis(self)->counters), 0, sizeof(tAtGfpChannelCounters));

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static void FifoStatusShow(AtChannel self)
    {
    uint32 regVal;
    uint32 channelId = AtChannelIdGet(self);
    eBool empty, full;

    /* Get FIFO status */
    regVal = mChannelHwRead(self, mMethodsGet(mThis(self))->EncPktFifoEpt(mThis(self), channelId), cAtModuleEncap);
    empty  = (regVal & cAf4EncFifoEptMask(channelId)) ? cAtTrue : cAtFalse;
    regVal = mChannelHwRead(self, mMethodsGet(mThis(self))->EncPktFifoFull(mThis(self), channelId), cAtModuleEncap);
    full   = (regVal & cAf4EncFifoFullMask(channelId)) ? cAtTrue : cAtFalse;
    AtPrintc(cSevMajor, "* Channel = %d: \n", channelId + 1);
    if (!full && !empty)
        return;

    /* And display them */
    AtPrintc(cSevInfo, "\t- FIFO status: \n");
    if (full)
        AtPrintc(cSevCritical, "\t\t Status: full");
    if (empty)
        AtPrintc(cSevCritical, "\t\t Status: empty");

    AtPrintc(cSevNormal, "\r\n");
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);
    FifoStatusShow(self);
    return cAtOk;
    }

static uint32 DecPktHold0(ThaGfpChannel self)
    {
    AtUnused(self);
    return cAf4RegDecPktHold0;
    }

static uint32 DecPktHold1(ThaGfpChannel self)
    {
    AtUnused(self);
    return cAf4RegDecPktHold1;
    }

static uint32 RxGfpCsfStat(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    return cAf4RxGfpCsfStat(channelId);
    }

static uint32 AggCfgGfpFcsRx(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    return cAf4RegAggCfgGfpFcsRx(channelId);
    }

static uint32 AggGfpSrcRx(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    return cAf4RegAggCfgGfpSrcRx(channelId);
    }

static uint32 EncPktHold0(ThaGfpChannel self)
    {
    AtUnused(self);
    return cAf4RegEncPktHold0;
    }

static uint32 EncPktHold1(ThaGfpChannel self)
    {
    AtUnused(self);
    return cAf4RegEncPktHold1;
    }

static uint32 EncModeSel(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    AtUnused(channelId);
    return cAf4RegEncModeSel(channelId);
    }

static uint32 DecModeSel(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    return cAf4RegDecModeSel(channelId);
    }

static uint32 EncPktFifoEpt(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    return cAf4RegEncPktFifoEpt(channelId);
    }

static uint32 EncPktFifoFull(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    return cAf4RegEncPktFifoFull(channelId);
    }

static uint32 AggCfgGfpSrcTx(ThaGfpChannel self, uint32 channelId)
    {
    AtUnused(self);
    return cAf4RegAggCfgGfpSrcTx(channelId);
    }

static eAtRet DecapDefaultSet(ThaGfpChannel self)
    {
    uint32 dRegVal = 0;
    uint32 regAddr;
    uint32 channelId = AtChannelIdGet((AtChannel)self);

    /* DEFAULT FCS have no interface ??? */
    regAddr = cAf4RegDecModeSel(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4HdlcGfpSelMask(channelId), cAf4HdlcGfpSelShift(channelId), 1);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    /* SCRAMBLE */
    regAddr = mMethodsGet(self)->AggGfpSrcRx(self, channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4AggGfpSrcRxMask(channelId), cAf4AggGfpSrcRxShift(channelId), 1);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    /* CSF */
    regAddr = cAf4RegAggCfgGfpCsfRx(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4AggGfpCsfMask(channelId), cAf4AggGfpCsfShift(channelId), 1);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    /* CSF TYPE */
    regAddr = cAf4RegAggCfgGfpCsfTypeRx(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4AggGfpCsfMask(channelId), cAf4AggGfpCsfShift(channelId), 0);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    return cAtOk;
    }

static eAtRet EncapDefaultSet(ThaGfpChannel self)
    {
    uint32 dRegVal = 0;
    uint32 regAddr;
    uint32 channelId = AtChannelIdGet((AtChannel)self);

    regAddr = cAf4RegEncModeSel(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4HdlcGfpSelMask(channelId), cAf4HdlcGfpSelShift(channelId), 1);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    /* DEFAULT FCS SETTING FOR ENCAP-CHANNEL */
    regAddr = cAf4RegAggCfgGfpFcsRx(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4AggGfpFcsMask(channelId), cAf4AggGfpFcsShift(channelId), 1);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    /* SCRAMBLE */
    regAddr = cAf4RegAggCfgGfpSrcTx(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4AggGfpSrcTxMask(channelId), cAf4AggGfpSrcTxShift(channelId), 1);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    /* CSF */
    regAddr = cAf4RegAggCfgGfpCsfTx(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4AggGfpCsfMask(channelId), cAf4AggGfpCsfShift(channelId), 1);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);

    /* CSF TYPE */
    regAddr = cAf4RegAggCfgGfpCsfTypeTx(channelId);
    dRegVal = mChannelHwRead(self, regAddr, cAtModuleEncap);
    mFieldIns(&dRegVal, cAf4AggGfpCsfMask(channelId), cAf4AggGfpCsfShift(channelId), 0);
    mChannelHwWrite(self, regAddr, dRegVal, cAtModuleEncap);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaGfpChannel self)
    {
    eAtRet ret = cAtOk;

    ret |= EncapDefaultSet(self);
    ret |= DecapDefaultSet(self);
    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static void OverrideAtChannel(AtGfpChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtGfpChannel self)
    {
    AtEncapChannel encap = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(encap);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, PhyBind);
        mMethodOverride(m_AtEncapChannelOverride, BoundPhyGet);
        mMethodOverride(m_AtEncapChannelOverride, FlowBind);
        mMethodOverride(m_AtEncapChannelOverride, BoundFlowGet);
        }

    mMethodsSet(encap, &m_AtEncapChannelOverride);
    }

static void OverrideAtGfpChannel(AtGfpChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtGfpChannelOverride, mMethodsGet(self), sizeof(m_AtGfpChannelOverride));

        mMethodOverride(m_AtGfpChannelOverride, ExpectedPtiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedExiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedPtiGet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedExiGet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, FcsMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, FcsMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, PtiMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, PtiMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, UpiMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, UpiMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxPtiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxPtiGet);
        mMethodOverride(m_AtGfpChannelOverride, TxExiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxExiGet);
        mMethodOverride(m_AtGfpChannelOverride, TxUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, TxPfiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxPfiGet);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeIsSupported);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeSet);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeGet);
        mMethodOverride(m_AtGfpChannelOverride, TxFcsEnable);
        mMethodOverride(m_AtGfpChannelOverride, TxFcsIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfEnable);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxCsfUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleEnable);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, CoreHeaderScrambleEnable);
        mMethodOverride(m_AtGfpChannelOverride, CoreHeaderScrambleIsEnabled);
        }

    mMethodsSet(self, &m_AtGfpChannelOverride);
    }

static void Override(AtGfpChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtGfpChannel(self);
    OverrideAtEncapChannel(self);
    }

static void MethodInit(ThaGfpChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, DecPktHold1);
        mMethodOverride(m_methods, DecPktHold0);
        mMethodOverride(m_methods, DecPktHold1);
        mMethodOverride(m_methods, RxGfpCsfStat);
        mMethodOverride(m_methods, AggCfgGfpFcsRx);
        mMethodOverride(m_methods, AggGfpSrcRx);
        mMethodOverride(m_methods, EncPktHold0);
        mMethodOverride(m_methods, EncPktHold1);
        mMethodOverride(m_methods, EncModeSel);
        mMethodOverride(m_methods, DecModeSel);
        mMethodOverride(m_methods, EncPktFifoEpt);
        mMethodOverride(m_methods, EncPktFifoFull);
        mMethodOverride(m_methods, AggCfgGfpSrcTx);
        mMethodOverride(m_methods, GfpHeaderCfgRegister);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaGfpChannel);
    }

AtGfpChannel ThaGfpChannelObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtGfpChannelObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtGfpChannel ThaGfpChannelNew(uint32 channelId, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtGfpChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ThaGfpChannelObjectInit(newChannel, channelId, module);
    }

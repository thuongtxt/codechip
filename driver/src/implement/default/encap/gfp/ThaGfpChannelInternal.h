/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaGfpChannelInternal.h
 * 
 * Created Date: Oct 7, 2014
 *
 * Description : GFP channel of AF4 product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF4GFPCHANNELINTERNAL_H_
#define _AF4GFPCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/encap/AtGfpChannelInternal.h"
#include "../ThaModuleEncap.h"
#include "ThaGfpChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaGfpChannelV2 * ThaGfpChannelV2;

typedef struct tThaGfpChannelMethods
    {
    eAtRet (*DefaultSet)(ThaGfpChannel self);

    /* Register polymorphism */
    uint32 (*DecPktHold0)(ThaGfpChannel self);
    uint32 (*DecPktHold1)(ThaGfpChannel self);
    uint32 (*EncPktHold0)(ThaGfpChannel self);
    uint32 (*EncPktHold1)(ThaGfpChannel self);
    uint32 (*RxGfpCsfStat)(ThaGfpChannel self, uint32 channelId);
    uint32 (*AggCfgGfpFcsRx)(ThaGfpChannel self, uint32 channelId);
    uint32 (*AggCfgGfpSrcTx)(ThaGfpChannel self, uint32 channelId);
    uint32 (*AggGfpSrcRx)(ThaGfpChannel self, uint32 channelId);
    uint32 (*EncModeSel)(ThaGfpChannel self, uint32 channelId);
    uint32 (*DecModeSel)(ThaGfpChannel self, uint32 channelId);
    uint32 (*EncPktFifoEpt)(ThaGfpChannel self, uint32 channelId);
    uint32 (*EncPktFifoFull)(ThaGfpChannel self, uint32 channelId);
    uint32 (*GfpHeaderCfgRegister)(ThaGfpChannel self);
    }tThaGfpChannelMethods;

typedef struct tThaGfpChannel
    {
    tAtGfpChannel super;
    const tThaGfpChannelMethods *methods;

    /* Private data */
    tAtGfpChannelCounters counters;
    }tThaGfpChannel;

typedef struct tThaGfpChannelV2
    {
    tAtGfpChannel super;
    }tThaGfpChannelV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtGfpChannel ThaGfpChannelObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module);
AtGfpChannel ThaGfpChannelV2ObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module);

uint32 ThaGfpChannelEncapCounterRead2Clear(ThaGfpChannel self, uint8 counterType);
uint32 ThaGfpChannelDecapCounterRead2Clear(ThaGfpChannel self, uint8 counterType);

#endif /* _AF4GFPCHANNELINTERNAL_H_ */


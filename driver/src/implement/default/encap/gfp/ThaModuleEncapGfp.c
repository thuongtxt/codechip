/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaModuleEncapGfp.c
 *
 * Created Date: Sep 7, 2015
 *
 * Description : Implement GFP specific things
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModuleEncapInternal.h"
#include "ThaModuleEncapGfpReg.h"
#include "ThaModuleEncapGfp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eAtRet ThaModuleEncapGfpDefaultSet(ThaModuleEncap self)
    {
    if (!AtModuleEncapGfpIsSupported((AtModuleEncap)self))
        return cAtOk;

    mModuleHwWrite(self, cAf4RxGfpCsfThresCfg, cAf4RxGfpCsfThresRst);
    mModuleHwWrite(self, cAf4DecLengthCfgVioReg, cAf4DecLengthCfgVioRstVal);
    mModuleHwWrite(self, cAf4DecRateAdptReg, cAf4DecRateAdptRstVal);

    return cAtOk;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaGfpInterruptManager.h
 * 
 * Created Date: Sep 20, 2019
 *
 * Description : Default GFP interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAGFPINTERRUPTMANAGER_H_
#define _THAGFPINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaGfpInterruptManager * ThaGfpInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtGfpChannel ThaGfpInterruptManagerGfpChannelFromHwIdGet(ThaGfpInterruptManager self, uint32 hwLocalId);

#ifdef __cplusplus
}
#endif
#endif /* _THAGFPINTERRUPTMANAGER_H_ */


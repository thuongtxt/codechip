/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaGfpInterruptManagerInternal.h
 * 
 * Created Date: Sep 20, 2019
 *
 * Description : Default GFP interrupt manager representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAGFPINTERRUPTMANAGERINTERNAL_H_
#define _THAGFPINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/intrcontroller/ThaInterruptManagerInternal.h"
#include "ThaGfpInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaGfpInterruptManagerMethods
    {
    AtGfpChannel (*GfpChannelFromHwIdGet)(ThaGfpInterruptManager self, uint32 hwLocalId);
    }tThaGfpInterruptManagerMethods;

typedef struct tThaGfpInterruptManager
    {
    tThaInterruptManager super;
    const tThaGfpInterruptManagerMethods *methods;
    }tThaGfpInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager ThaGfpInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAGFPINTERRUPTMANAGERINTERNAL_H_ */


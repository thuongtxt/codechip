/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaGfpChannel.h
 * 
 * Created Date: Sep 7, 2015
 *
 * Description : GFP channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THAGFPCHANNEL_H_
#define THAGFPCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtGfpChannel.h"
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtGfpChannel ThaGfpChannelNew(uint32 channelId, AtModuleEncap module);

/* Concrete products. */
AtGfpChannel Tha60210012VcgGfpChannelNew(uint32 channelId, AtModuleEncap module);
AtGfpChannel Tha60290081GfpChannelNew(uint32 channelId, AtModuleEncap module);

#ifdef __cplusplus
}
#endif
#endif /* THAGFPCHANNEL_H_ */


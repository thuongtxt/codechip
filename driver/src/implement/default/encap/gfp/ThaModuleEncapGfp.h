/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaModuleEncapGfp.h
 * 
 * Created Date: Sep 7, 2015
 *
 * Description : GFP related things
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEENCAPGFP_H_
#define _THAMODULEENCAPGFP_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaModuleEncapGfpDefaultSet(ThaModuleEncap self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEENCAPGFP_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Af4ModuleEncapReg.h
 * 
 * Created Date: Jun 10, 2014
 *
 * Description : ENCAP registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AF4MODULEENCAPREG_H_
#define _AF4MODULEENCAPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: Agg_gfpfcs
Reg Addr: 0x011002-0x011003
Reg Desc: configure scramble for GFP frame
------------------------------------------------------------------------------*/
#define cAf4RegAggCfgGfpFcsRx(vcgId)                ((vcgId<32) ? 0x40007 : 0x40008)

#define cAf4GfptThresDefReg                         0x40008
#define cAf4GfptThresDefMask                        cBit31_16
#define cAf4GfptThresDefShift                       16
#define cAf4GfptThresDefVal                         0x4003
/*--------------------------------------
BitField Name: gfpfcs
BitField Type: R/W
BitField Desc:
    1 : insert FCS32 for GFP frame
    0 : otherwise
BitField Bits: 16
--------------------------------------*/
#define cAf4AggGfpFcsMask(vcgId)                (cBit0 << ((vcgId) % 32))
#define cAf4AggGfpFcsShift(vcgId)               ((vcgId) % 32)
#define cAf4AggGfpFcsMaxVal                     0x1
#define cAf4AggGfpFcsMinVal                     0x0
#define cAf4AggGfpFcsRstVal                     0x0



/*------------------------------------------------------------------------------
Reg Name: Decpkt_hold0
Reg Addr: 0x030000
Reg Desc: hold register for indirect access
------------------------------------------------------------------------------*/
#define cAf4RegDecPktHold0                             0x030000



/*------------------------------------------------------------------------------
Reg Name: Decpkt_hold1
Reg Addr: 0x030001
Reg Desc: hold register for indirect access
------------------------------------------------------------------------------*/
#define cAf4RegDecPktHold1                             0x030001


/*--------------------------------------
0 : hold1[15:0] contain idle frame
--------------------------------------*/
#define cAf4DecIdleFrmCntMask                       cBit15_0
#define cAf4DecIdleFrmCntShift                      0


/*--------------------------------------
1 : hold1[31:0] contain good frame
--------------------------------------*/
#define cAf4DecGoodFrmCntMask                       cBit15_0
#define cAf4DecGoodFrmCntShift                      0

/*--------------------------------------
2 : hold1[31:0] contain idle frame
--------------------------------------*/
#define cAf4DecErrFcsFrmCntMask                     cBit31_0
#define cAf4DecErrFcsFrmCntShift                    0

/*--------------------------------------
3 : hold1[2:0]
         hold1[2] : sticky error LFD
         hold1[1] : sticky multi-error cHec
         hold1[0] : sticky correct single-error cHec
--------------------------------------*/
#define cAf4DecErrLfdStkMask                           cBit2
#define cAf4DecErrLfdStkShift                          2

#define cAf4DecMulErrCHecStkMask                       cBit1
#define cAf4DecMulErrCHecStkShift                      1

#define cAf4DecSingErrCHecStkMask                      cBit0
#define cAf4DecSingErrCHecStkShift                     0

/*--------------------------------------
4 : hold1[3:0] contain CSF frame
--------------------------------------*/
#define cAf4DecCsfFrmCntMask                        cBit31_0
#define cAf4DecCsfFrmCntShift                       0


/*--------------------------------------
5 : hold1[1:0]
         hold1[1] : stiky multi-error tHec
         hold1[0] : sticky correct single-error tHec
--------------------------------------*/
#define cAf4DecMulErrTHecStkMask                       cBit1
#define cAf4DecMulErrTHecStkShift                      1

#define cAf4DecSingErrTHecStkMask                      cBit0
#define cAf4DecSingErrTHecStkShift                     0


/*------------------------------------------------------------------------------
Reg Name: Decpkt_sta
Reg Addr: 0x030003
Reg Desc: configure scramble for GFP
------------------------------------------------------------------------------*/
#define cAf4RegDecPktStat                             0x030003


#define cAf4DecBufFullMask                       cBit5
#define cAf4DecBufFullShift                      5

#define cAf4DecFfCvEptMask                       cBit4
#define cAf4DecFfCvEptShift                      4

#define cAf4DecFfCvFullMask                      cBit3
#define cAf4DecFfCvFullShift                     3

#define cAf4DecLosSyn2Mask                      cBit2
#define cAf4DecLosSyn2Shift                     2

#define cAf4DecLosSyn1Mask                      cBit1
#define cAf4DecLosSyn1Shift                     1

#define cAf4DecLosSyn0Mask                      cBit0
#define cAf4DecLosSyn0Shift                     0

/*------------------------------------------------------------------------------
Reg Name: Encpkt_hold0
Reg Addr: 0x040000
Reg Desc: hold register for indirect access
------------------------------------------------------------------------------*/
#define cAf4RegEncPktHold0                             0x040000




/*------------------------------------------------------------------------------
Reg Name: Encpkt_hold1
Reg Addr: 0x040001
Reg Desc: hold register for indirect access
------------------------------------------------------------------------------*/
#define cAf4RegEncPktHold1                             0x040001


/*--------------------------------------
0 : hold1[15:0] contain idle frame
--------------------------------------*/
#define cAf4EncIdleFrmCntMask                       cBit15_0
#define cAf4EncIdleFrmCntShift                      0

/*--------------------------------------
1 : hold1[31:0] contain good frame
--------------------------------------*/
#define cAf4EncGoodFrmCntMask                     cBit31_0
#define cAf4EncGoodFrmCntShift                    0


/*------------------------------------------------------------------------------
Reg Name: Encpkt_ept
Reg Addr: 0x040002-0x040003
Reg Desc: empty fifo status, bit per VCG
------------------------------------------------------------------------------*/
#define cAf4RegEncPktFifoEpt(vcgId)                ((vcgId<32) ? 0x040002 : 0x040003)


/*--------------------------------------
BitField Name: Ept
BitField Type: R/W
BitField Desc:
    1 : empty fifo
    0 : normal
BitField Bits: 16
--------------------------------------*/
#define cAf4EncFifoEptMask(vcgId)                (cBit0 << ((vcgId) % 32))
#define cAf4EncFifoEptShift(vcgId)               ((vcgId) % 32)
#define cAf4EncFifoEptMaxVal                     0x1
#define cAf4EncFifoEptMinVal                     0x0
#define cAf4EncFifoEptRstVal                     0x0

/*------------------------------------------------------------------------------
Reg Name: Encpkt_full
Reg Addr: 0x040004-0x040005
Reg Desc: Full fifo status, bit per VCG
------------------------------------------------------------------------------*/
#define cAf4RegEncPktFifoFull(vcgId)                ((vcgId<32) ? 0x040004 : 0x040005)


/*--------------------------------------
BitField Name: Full
BitField Type: R/W
BitField Desc:
    1 : full fifo
    0 : normal
BitField Bits: 16
--------------------------------------*/
#define cAf4EncFifoFullMask(vcgId)                (cBit0 << ((vcgId) % 32))
#define cAf4EncFifoFullShift(vcgId)               ((vcgId) % 32)
#define cAf4EncFifoFullMaxVal                     0x1
#define cAf4EncFifoFullMinVal                     0x0
#define cAf4EncFifoFullRstVal                     0x0


/*--------------------------------------
BitField Name: fail
BitField Type: W2C
BitField Desc: sticky status, report access indirect register is fail
    1 : fail
    0 : ok
BitField Bits: 16
--------------------------------------*/
#define cAf4EncDecFailMask                       cBit14
#define cAf4EncDecFailShift                      14
#define cAf4EncDecFailMaxVal                     0x1
#define cAf4EncDecFailMinVal                     0x0
#define cAf4EncDecFailRstVal                     0x0

/*--------------------------------------
BitField Name: done
BitField Type: W2C
BitField Desc: stiky status, report access indirect register is done
    1 : done
    0 : waiting
BitField Bits: 13
--------------------------------------*/
#define cAf4EncDecDoneMask                       cBit13
#define cAf4EncDecDoneShift                      13
#define cAf4EncDecDoneMaxVal                     0x1
#define cAf4EncDecDoneMinVal                     0x0
#define cAf4EncDecDoneRstVal                     0x0


/*--------------------------------------
BitField Name: trig
BitField Type: W2C
BitField Desc: trigger 0 - 1 to HW start read indirect
BitField Bits: 13
--------------------------------------*/
#define cAf4EncDecTrigMask                       cBit12
#define cAf4EncDecTrigShift                      12
#define cAf4EncDecTrigMaxVal                     0x1
#define cAf4EncDecTrigMinVal                     0x0
#define cAf4EncDecTrigRstVal                     0x0

/*--------------------------------------
BitField Name: type
BitField Type: W2C
BitField Desc: the kind of indirect registers that you want to access
    0 : idle frame counter of GFP
    1 : good frame counter of GFP
    2 : error FCS frame of GFP
    3 : sticky error cHec of GFP
    4 : CSF counter frame (include CSF1 and CSF2)
    5 : sticky error tHec of GFP
    6,7 : Unused
BitField Bits: 13
--------------------------------------*/
#define cAf4EncDecTypeMask                       cBit11_8
#define cAf4EncDecTypeShift                      8
#define cAf4EncDecTypeMaxVal                     0x1
#define cAf4EncDecTypeMinVal                     0x0
#define cAf4EncDecTypeRstVal                     0x0
#define cAf4EncapIdleFrmCntMd           0
#define cAf4EncapGoodFrmCntMd           1
#define cAf4EncapErrFcsFrmCntMd         2
#define cAf4EncapErrcHecStkCntMd        3
#define cAf4EncapCsfFrmCntMd            4
#define cAf4EncapErrtHecStkMd           5

/*--------------------------------------
BitField Name: vcg
BitField Type:
BitField Desc: value VCG that you want to read, from 0 - 47
BitField Bits: 13
--------------------------------------*/
#define cAf4EncDecVcgMask                       cBit7_0
#define cAf4EncDecVcgShift                      0
#define cAf4EncDecVcgMaxVal                     0x1
#define cAf4EncDecVcgMinVal                     0x0
#define cAf4EncDecVcgRstVal                     0x0

#define cAf4RegEthCntOffset                       0x10



/*------------------------------------------------------------------------------
Reg Name: ENCAP-SCRAMBLE
Reg Addr: 0x4000d-0x4000e
Reg Desc: configure scramble for GFP frame at ENC
------------------------------------------------------------------------------*/
#define cAf4RegAggCfgGfpSrcTx(vcgId)                ((vcgId<32) ? 0x4000d  : 0x4000e)


/*--------------------------------------
BitField Name: gfpSrcTx
BitField Type: R/W
BitField Desc:
    1 : Enable Scramble
    0 : otherwise
BitField Bits: 16
--------------------------------------*/
#define cAf4AggGfpSrcTxMask(vcgId)                (cBit0 << ((vcgId) % 32))
#define cAf4AggGfpSrcTxShift(vcgId)               ((vcgId) % 32)
/*------------------------------------------------------------------------------
Reg Name: DEC-SCRAMBLE
Reg Addr: 0x4000d-0x4000e
Reg Desc: configure scramble for GFP frame at ENC
------------------------------------------------------------------------------*/
#define cAf4RegAggCfgGfpSrcRx(vcgId)                ((vcgId<32) ?  0x30005 : 0x30006)


/*--------------------------------------
BitField Name: gfpSrcRx
BitField Type: R/W
BitField Desc:
    1 : Enable Scramble
    0 : otherwise
BitField Bits: 16
--------------------------------------*/
#define cAf4AggGfpSrcRxMask(vcgId)                (cBit0 << ((vcgId) % 32))
#define cAf4AggGfpSrcRxShift(vcgId)               ((vcgId) % 32)


/*------------------------------------------------------------------------------
Reg Name: GFP Header configuration
Reg Addr: 0x040100-0x4012f
Description: configure Type header for GFP frame, VCG 1 to 47
------------------------------------------------------------------------------*/
#define cAf4RegGfpHeaderCfg              0x040100

#define cAf4RegGfpPtiMask                cBit15_13
#define cAf4RegGfpPtiShift               13

#define cAf4RegGfpExiMask                cBit11_8
#define cAf4RegGfpExiShift               8

#define cAf4RegGfpUpiMask                cBit7_0
#define cAf4RegGfpUpiShift               0



/*------------------------------------------------------------------------------
Reg Name: Agg_gfpcsf
Reg Addr: 0x011004-0x011005
Reg Desc: configure CSF for GFP frame
------------------------------------------------------------------------------*/
#define cAf4RegAggCfgGfpCsfRx(vcgId)                ((vcgId<32) ? 0x011004 : 0x011005)
#define cAf4RegAggCfgGfpCsfTx(vcgId)                ((vcgId<32) ? 0x40011 :  0x40012)

/*--------------------------------------
BitField Name: gfpfcs
BitField Type: R/W
BitField Desc:
    1 : insert FCS32 for GFP frame
    0 : otherwise
BitField Bits: 16
--------------------------------------*/
#define cAf4AggGfpCsfMask(vcgId)                (cBit0 << ((vcgId) % 32))
#define cAf4AggGfpCsfShift(vcgId)               ((vcgId) % 32)
#define cAf4AggGfpCsfMaxVal                     0x1
#define cAf4AggGfpCsfMinVal                     0x0
#define cAf4AggGfpCsfRstVal                     0x0


/*------------------------------------------------------------------------------
Reg Name: Agg_csftype
Reg Addr: 0x011006-0x011007
Reg Desc: configure type CSF for GFP frame
------------------------------------------------------------------------------*/
#define cAf4RegAggCfgGfpCsfTypeRx(vcgId)                ((vcgId<32) ? 0x011006 : 0x011007)
#define cAf4RegAggCfgGfpCsfTypeTx(vcgId)                ((vcgId<32) ? 0x40013  : 0x40014)

/*--------------------------------------
BitField Name: csftype
BitField Type: R/W
BitField Desc:
    1 : CSF is type 2
    0 : CSF is type 1
+ 0 :  Loss of Client Signal (UPI = 0000 0001) ,
+ 1 :  Loss of Client Character Synchronization (UPI = 0000 0010).
BitField Bits: 16
--------------------------------------*/
#define cAf4AggGfpCsfTypeMask(vcgId)                (cBit0 << ((vcgId) % 32))
#define cAf4AggGfpCsfTypeShift(vcgId)               ((vcgId) % 32)
#define cAf4AggGfpCsfTypeMaxVal                     0x1
#define cAf4AggGfpCsfTypeMinVal                     0x0
#define cAf4AggGfpCsfTypeRstVal                     0x0
/*------------------------------------------------------------------------------
Reg Name: Rx CSF threshold
Reg Addr: 0x300002
Description:
------------------------------------------------------------------------------*/
#define cAf4RxGfpCsfThresCfg                        0x30002
#define cAf4RxGfpCsfThresRst                        0x4A2860
#define cAf4RxGfpCsfStat(vcgId)                     ((vcgId < 32) ? 0x3000a  : 0x3000b)

#define cAf4TxGfpCsfThresCfg                        0x40012
#define cAf4TxGfpCsfMask                            cBit31_16
#define cAf4TxGfpCsfShift                           16
#define cAf4TxGfpCsfThresRst                        0x1615

/*--------------------------- PPP/LAPS ---------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: Encap 4 bytes first header check
Reg Addr:  0x030140 + VCGID  (VCGID: 0-47)
Description:
------------------------------------------------------------------------------*/
#define cAf4Enc4ByteFistHeaderCheckReg(vcg)     (0x040340+(vcg))
#define cAf4Enc4ByteFistHeaderCheckRstVal       0x0

/*------------------------------------------------------------------------------
Reg Name: Encap 2 bytes Last header check
Reg Addr:  0x030140 + VCGID  (VCGID: 0-47)
Description:
------------------------------------------------------------------------------*/
#define cAf4Enc2ByteLastHeaderCheckReg(vcg)     (0x040300+(vcg))
#define cAf4Enc2ByteLastHeaderCheckRstVal       0x23

#define cAf4Enc2ByteLastHeaderMask      cBit22_8
#define cAf4Enc2ByteLastHeaderShift     8

#define cAf4EncHdlcSrbEnMask    cBit7
#define cAf4EncHdlcSrbEnShift   7

#define cAf4EncFlagMdMask       cBit6
#define cAf4EncFlagMdShift      6

#define cAf4EncHdlcMdMask       cBit5_3
#define cAf4EncHdlcMdShift      3


#define cAf4EncMsbMdMask        cBit2
#define cAf4EncMSbMdShift       2

#define cAf4EncFcsMdMask        cBit1
#define cAf4EncFcsMdShift       1

#define cAf4EncFcsEnMask        cBit0
#define cAf4EncFcsEnShift       0
/*------------------------------------------------------------------------------
Reg Name: Decap length config violation
Reg Addr: 0x030004
Description:
------------------------------------------------------------------------------*/
#define cAf4DecLengthCfgVioReg                      0x030400
#define cAf4DecLengthCfgVioRstVal                   0xFFFF003C

/*------------------------------------------------------------------------------
Reg Name: Decap rate adaptation
Reg Addr: 0x030005
Description:
------------------------------------------------------------------------------*/
#define cAf4DecRateAdptReg                      0x030401
#define cAf4DecRateAdptRstVal                   0x0

/*------------------------------------------------------------------------------
Reg Name: Decap 4 bytes first header check
Reg Addr:  0x030140 + VCGID  (VCGID: 0-47)
Description:
------------------------------------------------------------------------------*/
#define cAf4Dec4ByteFistHeaderCheckReg(vcg)     (0x030540+(vcg))
#define cAf4Dec4ByteFistHeaderCheckRstVal       0x0
/*------------------------------------------------------------------------------
Reg Name: Decap 4 bytes first header check
Reg Addr:  0x030140 + VCGID  (VCGID: 0-47)
Description:
------------------------------------------------------------------------------*/
#define cAf4Dec2ByteLastHeaderCheckReg(vcg)     (0x030500+(vcg))
#define cAf4Dec2ByteLastHeaderCheckRstVal       0x23


#define cAf4Dec2ByteLastHeaderMask      cBit21_6
#define cAf4Dec2ByteLastHeaderShift     6


#define cAf4DecHdlcMdMask       cBit5_3
#define cAf4DecHdlcMdShift      3


#define cAf4DecMsbMdMask        cBit2
#define cAf4DecMSbMdShift       2

#define cAf4DecFcsMdMask        cBit1
#define cAf4DecFcsMdShift       1

#define cAf4DecFcsEnMask        cBit0
#define cAf4DecFcsEnShift       0


/*------------------------------------------------------------------------------
Reg Name: Deccap counter good/error
Reg Addr:  0x030140 + VCGID  (VCGID: 0-47)
Description:
------------------------------------------------------------------------------*/
#define cAf4DecGoodCntReg(vcg, r2c)         ((r2c == 0) ? (0x030680+(vcg)) : (0x030600+(vcg)))

#define cAf4DecAbrtCntReg               0x30480
#define cAf4DecFcsErrCntReg             0x30481
#define cAf4DecHeadErrCntReg            0x30482
#define cAf4DecComErrCntReg             0x30483


/*------------------------------------------------------------------------------
Reg Name: PAUSE FRAME
------------------------------------------------------------------------------*/
#define cAf4RegEthPortFcReg                 0x20009
#define cAf4RegEthPortFcRstVal              0x0

#define cAf4EthPortFcMask(portId)                       (cBit0 << (4+(portId)))
#define cAf4EthPortFcShift(portId)                      (4+(portId))


/*------------------------------------------------------------------------------
Reg Name:ENCAP MODE SELECT
#define cAf4RxGfpCsfStat(vcgId)
------------------------------------------------------------------------------*/
#define cAf4RegDecModeSel(vcgId)         ((vcgId < 32) ? 0x40020  : 0x40021)
#define cAf4RegDecModeSelRstVal          0x0

#define cAf4RegEncModeSel(vcgId)         ((vcgId < 32) ? 0x30020  : 0x30021)
#define cAf4RegEncModeSelRstVal          0x0

#define cAf4HdlcGfpSelMask(vcgId)       (cBit0 << ((vcgId) % 32))
#define cAf4HdlcGfpSelShift(vcgId)      ((vcgId) % 32)

/*------------------------------------------------------------------------------
Reg Name: HDLC ENGINE ENABLE
------------------------------------------------------------------------------*/
#define cAf4RegHdlcEngineEnableReg                 0x30022
#define cAf4RegHdlcEngineEnableRst                 0x0
#define cAf4RegHdlcEngineEnableSet                 0x1
#define cAf4RegHdlcEngineEnableClr                 0x0

/*------------------------------------------------------------------------------
GFP-T Configurations:
# disable ETH ko cho gen len AGG :

pktwr 28006 0
2. enable GFP-T
pktwr 30004 7
pktwr 40008 1010000
pktwr 30320 1
------------------------------------------------------------------------------*/
#define cGfpTEnableReg                          0x30320
#define cGfpTEnableMask(vcgId)                  (cBit0 << ((vcgId) % 32))
#define cGfpTEnableShift(vcgId)                 ((vcgId) % 32)

#define cAf4DecGfpComCfgReg                     0x030004
#define cAf4DecGfpComCfgRegRstVal               0x7

#define cGfpTEnChnMask(vcgId)                  (cBit0 << ((vcgId + 1) % 32))
#define cGfpTEnChnShift(vcgId)                 ((vcgId + 1) % 32)
#define cAf4DecGfpEnMask                        cBit0
#define cAf4DecGfpEnShift                       0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _AF4MODULEENCAPREG_H_ */


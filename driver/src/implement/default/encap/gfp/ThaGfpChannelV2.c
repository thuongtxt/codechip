/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaGfpChannelV2.c
 *
 * Created Date: Mar 18, 2016
 *
 * Description : Default GFP encapsulation channel version 2.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaGfpChannelInternal.h"
#include "../../eth/ThaModuleEthInternal.h"
#include "../../eth/ThaEthFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((ThaGfpChannelV2)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;
static tAtGfpChannelMethods   m_AtGfpChannelOverride;

/* Save super implementation */
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;
static const tAtGfpChannelMethods   *m_AtGfpChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwIdGet(AtChannel self)
    {
    return AtChannelIdGet(AtEncapChannelBoundPhyGet((AtEncapChannel)self));
    }

static eAtModuleEncapRet ExpectedPtiSet(AtGfpChannel self, uint8 expectedPti)
    {
    AtUnused(self);

    if (expectedPti == 0)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint8 ExpectedPtiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEncapRet PtiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    if(enable)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool PtiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEncapRet TxPtiSet(AtGfpChannel self, uint8 txPti)
    {
    AtUnused(self);
    if (txPti == 0)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint8 TxPtiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEncapRet TxExiSet(AtGfpChannel self, uint8 txExi)
    {
    AtUnused(self);
    return  (txExi == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 TxExiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEncapRet ExpectedExiSet(AtGfpChannel self, uint8 expectedExi)
    {
    AtUnused(self);
    return  (expectedExi == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 ExpectedExiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModuleEncapRet ExiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtFalse) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool ExiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet ExpectedUpiSet(AtGfpChannel self, uint8 expectedUpi)
    {
    AtUnused(self);
    AtUnused(expectedUpi);
    return cAtOk;
    }

static uint8 ExpectedUpiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEncapRet UpiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    if (enable)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool UpiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEncapRet TxFcsEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    if(enable)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool TxFcsIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEncapRet TxPfiSet(AtGfpChannel self, uint8 txPfi)
    {
    AtUnused(self);
    if (txPfi > 0x1)
        return cAtErrorOutOfRangParm;

    return AtGfpChannelTxFcsEnable(self, txPfi ? cAtTrue : cAtFalse);
    }

static uint8 TxPfiGet(AtGfpChannel self)
    {
    return AtGfpChannelTxFcsIsEnabled(self) ? 0x1 : 0x0;
    }

static eAtModuleEncapRet FrameModeSet(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    AtUnused(self);
    if (frameMappingMode == cAtGfpFrameModeGfpF)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtGfpFrameMode FrameModeGet(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtGfpFrameModeGfpF;
    }

static eBool FrameModeIsSupported(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    AtUnused(self);
    if (frameMappingMode == cAtGfpFrameModeGfpF)
        return cAtTrue;
    return cAtFalse;
    }

static uint8 RxCsfUpiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x1;
    }

static eAtModuleEncapRet PayloadScrambleEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    if(enable)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool PayloadScrambleIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEncapRet CoreHeaderScrambleEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    if (enable)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool CoreHeaderScrambleIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet FlowActivate(AtEncapChannel self, AtEthFlow flow)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)flow);
    ThaEthFlowController flowController = ThaModuleEthEncapFlowControllerGet(ethModule);
    return ThaEthFlowControllerFlowActivate(flowController, flow, (AtChannel)self, 0);
    }

static eAtRet FlowDeActivate(AtEncapChannel self, AtEthFlow flow)
    {
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)flow);
    ThaEthFlowController flowController = ThaModuleEthEncapFlowControllerGet(ethModule);
    AtUnused(self);
    return ThaEthFlowControllerFlowDeActivate(flowController, flow);
    }

static eAtModuleEncapRet FlowBinding(AtEncapChannel self, AtEthFlow flow)
    {
    uint32 ret;
    AtEncapChannel boundEncapChannel = AtEthFlowBoundEncapChannelGet(flow);

    /* Just do nothing when bind the same channel */
    if (self == boundEncapChannel)
        return cAtOk;

    /* This Flow is used */
    if (boundEncapChannel)
        return cAtErrorChannelBusy;

    /* Let supper do his job */
    ret = m_AtEncapChannelMethods->FlowBind(self, flow);
    if (ret != cAtOk)
        return ret;

    return FlowActivate(self, flow);
    }

static eAtModuleEncapRet FlowUnBinding(AtEncapChannel self)
    {
    uint32 ret;
    AtEthFlow currentFlow = AtEncapChannelBoundFlowGet(self);
    if (currentFlow)
        {
        ret = FlowDeActivate(self, currentFlow);
        if (ret != cAtOk)
            return ret;

        return m_AtEncapChannelMethods->FlowBind(self, NULL);
        }

    return cAtOk;
    }

static eAtModuleEncapRet FlowBind(AtEncapChannel self, AtEthFlow flow)
    {
    if (flow)
        return FlowBinding(self, flow);

    return FlowUnBinding(self);
    }

static void OverrideAtChannel(AtGfpChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtGfpChannel self)
    {
    AtEncapChannel encap = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(encap);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, FlowBind);
        }

    mMethodsSet(encap, &m_AtEncapChannelOverride);
    }

static void OverrideAtGfpChannel(AtGfpChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtGfpChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtGfpChannelOverride, m_AtGfpChannelMethods, sizeof(m_AtGfpChannelOverride));

        mMethodOverride(m_AtGfpChannelOverride, UpiMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, UpiMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedPtiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedPtiGet);
        mMethodOverride(m_AtGfpChannelOverride, PtiMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, PtiMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxPtiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxPtiGet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedExiSet);
        mMethodOverride(m_AtGfpChannelOverride, ExpectedExiGet);
        mMethodOverride(m_AtGfpChannelOverride, ExiMonitorEnable);
        mMethodOverride(m_AtGfpChannelOverride, ExiMonitorIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxExiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxExiGet);
        mMethodOverride(m_AtGfpChannelOverride, TxFcsEnable);
        mMethodOverride(m_AtGfpChannelOverride, TxFcsIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, TxPfiSet);
        mMethodOverride(m_AtGfpChannelOverride, TxPfiGet);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeIsSupported);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeSet);
        mMethodOverride(m_AtGfpChannelOverride, FrameModeGet);
        mMethodOverride(m_AtGfpChannelOverride, RxCsfUpiGet);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleEnable);
        mMethodOverride(m_AtGfpChannelOverride, PayloadScrambleIsEnabled);
        mMethodOverride(m_AtGfpChannelOverride, CoreHeaderScrambleEnable);
        mMethodOverride(m_AtGfpChannelOverride, CoreHeaderScrambleIsEnabled);
        }

    mMethodsSet(self, &m_AtGfpChannelOverride);
    }

static void Override(AtGfpChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtGfpChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaGfpChannelV2);
    }

AtGfpChannel ThaGfpChannelV2ObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtGfpChannelObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

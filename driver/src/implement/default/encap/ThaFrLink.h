/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaFrLink.h
 * 
 * Created Date: Feb 28, 2015
 *
 * Description : FR link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAFRLINK_H_
#define _THAFRLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtFrLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
AtHdlcLink ThaFrLinkNew(AtHdlcChannel hdlcChannel);
AtHdlcLink ThaStmFrLinkNew(AtHdlcChannel hdlcChannel);

/* Product concretes */
AtHdlcLink Tha60091023FrLinkNew(AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THAFRLINK_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaModuleStmEncap.c
 *
 * Created Date: Dec 3, 2012
 *
 * Description : Encapsulation module of STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaStmModuleEncapInternal.h"

#include "../ppp/ThaMpigStmReg.h"
#include "../ppp/ThaMpegStmReg.h"
#include "ThaCiscoHdlcLink.h"
#include "ThaFrLink.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaMapIdlePatternControl 0x1B4200
#define cThaMapIdlePatternMask    cBit7_0
#define cThaMapIdlePatternShift   0

/*--------------------------- Macros -----------------------------------------*/
/* MPEG Bit fields */
mDefineMaskShift(ThaModuleEncap, MPEGUpOamPktLen)
mDefineMaskShift(ThaModuleEncap, MPEGUpOamLinkID)
mDefineMaskShift(ThaModuleEncap, MPEGPppLinkCtrl1BundleID)

/* MPIG Bit fields */
mDefineMaskShift(ThaModuleEncap, MlppHdrMode)
mDefineMaskShift(ThaModuleEncap, PidMode)
mDefineMaskShift(ThaModuleEncap, AddrCtrlMode)
mDefineMaskShift(ThaModuleEncap, AdrComp)
mDefineMaskShift(ThaModuleEncap, BlkAloc)
mDefineMaskShift(ThaModuleEncap, FcsErrDropEn)
mDefineMaskShift(ThaModuleEncap, AdrCtlErrDropEn)
mDefineMaskShift(ThaModuleEncap, NCPRule)
mDefineMaskShift(ThaModuleEncap, LCPRule)
mDefineMaskShift(ThaModuleEncap, MlpRuleRule)
mDefineMaskShift(ThaModuleEncap, PppRule)
mDefineMaskShift(ThaModuleEncap, PKtMode)
mDefineMaskShift(ThaModuleEncap, BndID)
mDefineMaskShift(ThaModuleEncap, MPIGDatEmiLbpBlkVl)
mDefineMaskShift(ThaModuleEncap, MPIGDatEmiLbpBlkID)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapMethods m_AtModuleEncapOverride;
static tThaModuleEncapMethods m_ThaModuleEncapOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleStmEncap);
    }

static AtPppLink PppLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(self);
    return ThaStmPppLinkNew(hdlcChannel);
    }

static uint16 MaxChannelsGet(AtModuleEncap self)
    {
	AtUnused(self);
    return 512;
    }

static AtHdlcLink CiscoHdlcLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(self);
    return (AtHdlcLink)ThaStmCiscoHdlcLinkNew(hdlcChannel);
    }

static AtHdlcLink FrLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(self);
    return ThaStmFrLinkNew(hdlcChannel);
    }

/* Idle pattern */
static eAtRet IdlePatternSet(AtModuleEncap self, uint8 pattern)
    {
    uint32 regVal = 0;

    mRegFieldSet(regVal, cThaMapIdlePattern, pattern);
    mModuleHwWrite(self, cThaMapIdlePatternControl, regVal);
    return cAtOk;
    }

static uint8 IdlePatternGet(AtModuleEncap self)
    {
    uint32 regVal = mModuleHwRead(self, cThaMapIdlePatternControl);
    return (uint8)mRegField(regVal, cThaMapIdlePattern);
    }

static uint32 DataEncapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    uint32 readOnlyCounterAddress = ThaModuleEncapHdlcIdleByteCountersAreSupported(self) ? 0x001C1200 : 0x001C1400;
    return isReadToClear ? 0x001C1000 : readOnlyCounterAddress;
    }

static uint32 DataDecapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    uint32 readOnlyCounterAddress = ThaModuleEncapHdlcIdleByteCountersAreSupported(self) ? 0x00201200 : 0x00201400;
    return isReadToClear ? 0x00201000 : readOnlyCounterAddress;
    }

static uint32 DataEncapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
	AtUnused(self);
    return isReadToClear ? 0x001C1800 : 0x001C1A00;
    }

static uint32 DataDecapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
	AtUnused(self);
    return isReadToClear ? 0x00201800 : 0x00201A00;
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    AtModuleEncap encapModule = (AtModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, mMethodsGet(encapModule), sizeof(m_AtModuleEncapOverride));
        mMethodOverride(m_AtModuleEncapOverride, PppLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, MaxChannelsGet);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, IdlePatternSet);
        mMethodOverride(m_AtModuleEncapOverride, IdlePatternGet);
        mMethodOverride(m_AtModuleEncapOverride, FrLinkObjectCreate);
        }

    mMethodsSet(encapModule, &m_AtModuleEncapOverride);
    }

static void OverrideThaModuleEncap(AtModuleEncap self)
    {
    ThaModuleEncap encapModule = (ThaModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEncapOverride, mMethodsGet(encapModule), sizeof(m_ThaModuleEncapOverride));

        mMethodOverride(m_ThaModuleEncapOverride, DataEncapPerByteCounterAddress);
        mMethodOverride(m_ThaModuleEncapOverride, DataDecapPerByteCounterAddress);
        mMethodOverride(m_ThaModuleEncapOverride, DataEncapPerIdleByteCounterAddress);
        mMethodOverride(m_ThaModuleEncapOverride, DataDecapPerIdleByteCounterAddress);

        /* MPEG Bit fields */
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPEGUpOamPktLen)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPEGUpOamLinkID)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPEGPppLinkCtrl1BundleID)

        /* MPIG Bit fields */
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MlppHdrMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PidMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AddrCtrlMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AdrComp)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, BlkAloc)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, FcsErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, AdrCtlErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, NCPRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, LCPRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MlpRuleRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PppRule)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, PKtMode)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, BndID)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPIGDatEmiLbpBlkVl)
        mBitFieldOverride(ThaModuleEncap, m_ThaModuleEncapOverride, MPIGDatEmiLbpBlkID)
        }

    mMethodsSet(encapModule, &m_ThaModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtModuleEncap(self);
    OverrideThaModuleEncap(self);
    }

AtModuleEncap ThaModuleStmEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEncapObjectInit((AtModuleEncap)self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap ThaModuleStmEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return ThaModuleStmEncapObjectInit(newModule, device);
    }

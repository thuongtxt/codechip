/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encapsulation
 *
 * File        : ThaCiscoHdlcLink.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa Cisco HDLC link default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaCiscoHdlcLinkInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../ppp/ThaMpigReg.h"
#include "../ppp/ThaMpegReg.h"
#include "../encap/ThaModuleEncapReg.h"
#include "../eth/controller/ThaEthFlowControllerInternal.h"
#include "../eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tAtPppLinkMethods  m_AtPppLinkOverride;
static tThaPppLinkMethods m_ThaPppLinkOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods = NULL;
static const tAtPppLinkMethods  *m_AtPppLinkMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LinkOffSet(ThaPppLink self)
    {
    return mMethodsGet(self)->DefaultOffset(self);
    }

static ThaModuleEncap EncapModule(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self));
    return (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eAtRet PppPacketModeSet(ThaPppLink self)
    {
    uint32 regAddr, regVal;
    ThaModuleEncap encapModule = EncapModule(self);
    uint32 pktModeMask  = mMethodsGet(encapModule)->PKtModeMask(encapModule);
    uint32 pktModeShift = mMethodsGet(encapModule)->PKtModeShift(encapModule);
    regAddr = cThaRegMPIGDATLookupLinkCtrl + LinkOffSet(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, pktModeMask, pktModeShift, 0x6);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    regAddr = cThaRegThalassaMLPppEgrPppLinkCtrl1 + LinkOffSet(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldIns(&regVal, cThaMPEGEncTypeMask, cThaMPEGEncTypeShift, 0x6);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static void PidInsertionEnable(AtHdlcLink self, eBool insertEnable)
    {
    uint32 address, regValue;

    address  = cThaRegDatEncOptCtrl1 + LinkOffSet((ThaPppLink)self);
    regValue = mChannelHwRead(self, address, cAtModuleEncap);
    mFieldIns(&regValue,
              cThaRegDatEncapPosSapiInsMask,
              cThaRegDatEncapPosSapiInsShift,
              mBoolToBin(insertEnable));
    mChannelHwWrite(self, address, regValue, cAtModuleEncap);
    }

eAtRet ThaCiscoHdlcInit(AtChannel self)
    {
    eAtRet ret;
    AtHdlcChannel hdlcChannel;
    AtHdlcLink hdlcLink = (AtHdlcLink)self;

    /* Additional initialization */
    PidInsertionEnable(hdlcLink, cAtFalse);
    ret = ThaPppLinkAddressControlBypass((AtPppLink)self, cAtFalse);
    if (ret != cAtOk)
        return ret;

    hdlcChannel = AtHdlcLinkHdlcChannelGet(hdlcLink);
    AtHdlcChannelAddressMonitorEnable(hdlcChannel, cAtFalse);
    AtHdlcChannelAddressInsertionEnable(hdlcChannel, cAtFalse);
    AtHdlcChannelControlMonitorEnable(hdlcChannel, cAtFalse);
    AtHdlcChannelAddressInsertionEnable(hdlcChannel, cAtFalse);

    return mMethodsGet(hdlcLink)->PidByPass(hdlcLink, cAtTrue);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtChannelMethods->RxTrafficEnable((AtChannel)self, enable);
    ret |= m_AtPppLinkMethods->PhaseSet((AtPppLink)self, cAtPppLinkPhaseNetworkActive);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ThaCiscoHdlcInit(self);
    }

static ThaEthFlowController EthFlowControllerGet(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return ThaModuleEthCiscoHdlcFlowControllerGet(ethModule);
    }

static void OverrideAtChannel(ThaCiscoHdlcLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPppLink(ThaCiscoHdlcLink self)
    {
    AtPppLink pppLink = (AtPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPppLinkMethods = mMethodsGet(pppLink);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, mMethodsGet(pppLink), sizeof(m_AtPppLinkOverride));
        }

    mMethodsSet(pppLink, &m_AtPppLinkOverride);
    }

static void OverrideThaPppLink(ThaCiscoHdlcLink self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPppLinkOverride));
        mMethodOverride(m_ThaPppLinkOverride, PppPacketModeSet);
        mMethodOverride(m_ThaPppLinkOverride, EthFlowControllerGet);
        }

    mMethodsSet(link, &m_ThaPppLinkOverride);
    }

static void Override(ThaCiscoHdlcLink self)
    {
    OverrideAtChannel(self);
    OverrideAtPppLink(self);
    OverrideThaPppLink(self);
    }

ThaPppLink ThaCiscoHdlcLinkObjectInit(ThaPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaCiscoHdlcLink));

    /* Super constructor */
    if (ThaPppLinkObjectInit((AtPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaCiscoHdlcLink)self);
    m_methodsInit = 1;

    return self;
    }

ThaPppLink ThaCiscoHdlcLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaCiscoHdlcLink));
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ThaCiscoHdlcLinkObjectInit(newLink, hdlcChannel);
    }

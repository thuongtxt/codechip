/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ThaFrLink.c
 *
 * Created Date: Jun 5, 2014
 *
 * Description : Frame relay link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaFrLinkInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../ppp/ThaMpigReg.h"
#include "../ppp/ThaMpegReg.h"
#include "../eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPppLinkMethods m_ThaPppLinkOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 LinkOffSet(ThaPppLink self)
    {
    return mMethodsGet(self)->DefaultOffset(self);
    }

static ThaModuleEncap EncapModule(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)AtHdlcLinkHdlcChannelGet((AtHdlcLink)self));
    return (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    }

static eAtRet PppPacketModeSet(ThaPppLink self)
    {
    uint32 regAddr, regVal;
    static const uint8 cHdlcPwMode = 0x0;
    ThaModuleEncap encapModule = EncapModule(self);
    uint32 pktModeMask  = mMethodsGet(encapModule)->PKtModeMask(encapModule);
    uint32 pktModeShift = mMethodsGet(encapModule)->PKtModeShift(encapModule);
    regAddr = cThaRegMPIGDATLookupLinkCtrl + LinkOffSet(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpig);
    mFieldIns(&regVal, pktModeMask, pktModeShift, cHdlcPwMode);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpig);

    regAddr = cThaRegThalassaMLPppEgrPppLinkCtrl1 + LinkOffSet(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mFieldIns(&regVal, cThaMPEGEncTypeMask, cThaMPEGEncTypeShift, cHdlcPwMode);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static ThaEthFlowController EthFlowControllerGet(ThaPppLink self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return ThaModuleEthFrFlowControllerGet(ethModule);
    }

static void OverrideThaPppLink(AtHdlcLink self)
    {
    ThaPppLink link = (ThaPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPppLinkOverride, mMethodsGet(link), sizeof(m_ThaPppLinkOverride));
        mMethodOverride(m_ThaPppLinkOverride, PppPacketModeSet);
        mMethodOverride(m_ThaPppLinkOverride, EthFlowControllerGet);
        }

    mMethodsSet(link, &m_ThaPppLinkOverride);
    }

static void Override(AtHdlcLink self)
    {
    OverrideThaPppLink(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaFrLink);
    }

AtHdlcLink ThaFrLinkObjectInit(AtHdlcLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCiscoHdlcLinkObjectInit((ThaPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcLink ThaFrLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ThaFrLinkObjectInit(newLink, hdlcChannel);
    }

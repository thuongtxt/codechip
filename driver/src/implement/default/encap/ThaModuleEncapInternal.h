/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaModuleEncapInternal.h
 * 
 * Created Date: Dec 3, 2012
 *
 * Description : Encapsulation module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEENCAPINTERNAL_H_
#define _THAMODULEENCAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/encap/AtModuleEncapInternal.h"
#include "../util/ThaUtil.h"
#include "../ppp/ThaPppLink.h"
#include "ThaModuleEncap.h"
#include "ThaHdlcChannel.h"
#include "ThaCiscoHdlcLink.h"
#include "ThaFrLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mLinkMask(encapModule, field)                                           \
        mMethodsGet((ThaModuleEncap)encapModule)->field##Mask((ThaModuleEncap)encapModule)
#define mLinkShift(pppLink, field)                                             \
        mMethodsGet((ThaModuleEncap)encapModule)->field##Shift((ThaModuleEncap)encapModule)

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tThaModuleEncapMethods
    {
    eAtRet (*DefaultSet)(ThaModuleEncap self);
    eBool (*HdlcByteCountersAreSupported)(ThaModuleEncap self);
    eBool (*AcfcSupported)(ThaModuleEncap self);
    uint32 (*DataEncapPerByteCounterAddress)(ThaModuleEncap self, eBool isReadToClear);
    uint32 (*DataDecapPerByteCounterAddress)(ThaModuleEncap self, eBool isReadToClear);
    uint32 (*DataEncapPerIdleByteCounterAddress)(ThaModuleEncap self, eBool isReadToClear);
    uint32 (*DataDecapPerIdleByteCounterAddress)(ThaModuleEncap self, eBool isReadToClear);

    /* For backward compatible */
    uint32 (*StartDeviceVersionThatSupportsFrameRelay)(ThaModuleEncap self);
    uint32 (*StartDeviceVersionThatSupportsIdleByteCounter)(ThaModuleEncap self);

    /* MPEG Bit fields */
    mDefineMaskShiftField(ThaModuleEncap, MPEGUpOamPktLen)
    mDefineMaskShiftField(ThaModuleEncap, MPEGUpOamLinkID)
    mDefineMaskShiftField(ThaModuleEncap, MPEGPppLinkCtrl1BundleID)

    /* MPIG Bit fields */
    mDefineMaskShiftField(ThaModuleEncap, MlppHdrMode)
    mDefineMaskShiftField(ThaModuleEncap, PidMode)
    mDefineMaskShiftField(ThaModuleEncap, AddrCtrlMode)
    mDefineMaskShiftField(ThaModuleEncap, AdrComp)
    mDefineMaskShiftField(ThaModuleEncap, BlkAloc)
    mDefineMaskShiftField(ThaModuleEncap, FcsErrDropEn)
    mDefineMaskShiftField(ThaModuleEncap, AdrCtlErrDropEn)
    mDefineMaskShiftField(ThaModuleEncap, NCPRule)
    mDefineMaskShiftField(ThaModuleEncap, LCPRule)
    mDefineMaskShiftField(ThaModuleEncap, MlpRuleRule)
    mDefineMaskShiftField(ThaModuleEncap, PppRule)
    mDefineMaskShiftField(ThaModuleEncap, PKtMode)
    mDefineMaskShiftField(ThaModuleEncap, BndID)
    mDefineMaskShiftField(ThaModuleEncap, MPIGDatEmiLbpBlkVl)
    mDefineMaskShiftField(ThaModuleEncap, MPIGDatEmiLbpBlkID)
    mDefineMaskShiftField(ThaModuleEncap, FlwEnb)
    mDefineMaskShiftField(ThaModuleEncap, MPIGDATLookupFlwID)
    }tThaModuleEncapMethods;

/* Class representation */
typedef struct tThaModuleEncap
    {
    tAtModuleEncap super;
    const tThaModuleEncapMethods *methods;
    }tThaModuleEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap ThaModuleEncapObjectInit(AtModuleEncap self, AtDevice device);

AtIterator ThaModuleEncapRegisterIteratorCreate(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEENCAPINTERNAL_H_ */


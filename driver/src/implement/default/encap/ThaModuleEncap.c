/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encapsulation
 *
 * File        : ThaModuleEncap.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Implementation of Thalassa Encapsulation module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../encap/ThaHdlcChannelInternal.h"
#include "../ppp/ThaPppLink.h"
#include "../man/ThaDeviceInternal.h"
#include "../ppp/ThaMpigReg.h"
#include "../ppp/ThaMpegReg.h"
#include "../pdh/ThaStmModulePdh.h"
#include "../pdh/ThaPdhVcDe1.h"
#include "../sdh/ThaModuleSdh.h"

#include "ThaModuleEncapInternal.h"
#include "ThaModuleEncapReg.h"
#include "gfp/ThaGfpChannel.h"
#include "gfp/ThaModuleEncapGfp.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaLsbTransFirst 1
#define cThaPosStuffLsb 0
#define cAtHdlcFrmFr 3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleEncap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleEncapMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtModuleEncapMethods m_AtModuleEncapOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleEncapMethods *m_AtModuleEncapMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* MPEG Bit fields */
mDefineMaskShift(ThaModuleEncap, MPEGUpOamPktLen)
mDefineMaskShift(ThaModuleEncap, MPEGUpOamLinkID)
mDefineMaskShift(ThaModuleEncap, MPEGPppLinkCtrl1BundleID)

/* MPIG Bit fields */
mDefineMaskShift(ThaModuleEncap, MlppHdrMode)
mDefineMaskShift(ThaModuleEncap, PidMode)
mDefineMaskShift(ThaModuleEncap, AddrCtrlMode)
mDefineMaskShift(ThaModuleEncap, AdrComp)
mDefineMaskShift(ThaModuleEncap, BlkAloc)
mDefineMaskShift(ThaModuleEncap, FcsErrDropEn)
mDefineMaskShift(ThaModuleEncap, AdrCtlErrDropEn)
mDefineMaskShift(ThaModuleEncap, NCPRule)
mDefineMaskShift(ThaModuleEncap, LCPRule)
mDefineMaskShift(ThaModuleEncap, MlpRuleRule)
mDefineMaskShift(ThaModuleEncap, PppRule)
mDefineMaskShift(ThaModuleEncap, PKtMode)
mDefineMaskShift(ThaModuleEncap, BndID)
mDefineMaskShift(ThaModuleEncap, MPIGDatEmiLbpBlkVl)
mDefineMaskShift(ThaModuleEncap, MPIGDatEmiLbpBlkID)

static AtHdlcChannel HdlcPppChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return (AtHdlcChannel)ThaHdlcChannelNew(channelId, cAtHdlcFrmPpp, self);
    }

static AtPppLink PppLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    return ThaPppLinkNew(hdlcChannel);
    }

static uint32 StartDeviceVersionThatSupportsFrameRelay(ThaModuleEncap self)
    {
    AtUnused(self);
    /* Let concrete product determine. Return the maximum version value to
     * indicate not support for all of FPGA versions so far. */
    return cBit31_0;
    }

static uint32 StartDeviceVersionThatSupportsIdleByteCounter(ThaModuleEncap self)
    {
    AtUnused(self);
    /* Let concrete product determine. Return the maximum version value to
     * indicate not support for all of FPGA versions so far. */
    return cBit31_0;
    }

static AtHdlcLink CiscoHdlcLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    return (AtHdlcLink)ThaCiscoHdlcLinkNew(hdlcChannel);
    }

static AtHdlcLink FrLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    return (AtHdlcLink)ThaFrLinkNew(hdlcChannel);
    }

static AtHdlcChannel FrChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    if (ThaModuleEncapFrameRelayIsSupported((ThaModuleEncap)self))
        return (AtHdlcChannel)ThaHdlcChannelNew(channelId, cAtHdlcFrmFr, self);
    return NULL;
    }

static AtHdlcChannel CiscoHdlcChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return (AtHdlcChannel)ThaHdlcChannelNew(channelId, cAtHdlcFrmCiscoHdlc, self);
    }

/* Get maximum of channels */
static uint16 MaxChannelsGet(AtModuleEncap self)
    {
    AtUnused(self);
    return 128;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if ((localAddress >= 0x1C0000) && (localAddress <= 0x1CFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 NumParts(ThaModuleEncap self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cAtModuleEncap);
    }

static uint32 PartOffset(ThaModuleEncap self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cAtModuleEncap, partId);
    }

static void EncapDefaultSet(ThaModuleEncap self, uint8 partId)
    {
    uint32 dRegVal = 0;
    uint32 regAddr;

    regAddr = cThaRegDatEncMstCtrl + PartOffset(self, partId);
    mFieldIns(&dRegVal, cThaRegDatEnc56KStuffBitCtrMask, cThaRegDatEnc56KStuffBitCtrShift, cThaPosStuffLsb);
    mFieldIns(&dRegVal, cThaRegDatEnc56KStuffvalMask, cThaRegDatEnc56KStuffvalShift, 0x1);
    mFieldIns(&dRegVal, cThaRegDatEncPosLsbFirstMask, cThaRegDatEncPosLsbFirstShift, cThaLsbTransFirst);
    mModuleHwWrite(self, regAddr, dRegVal);
    }

static void DecapDefaultSet(ThaModuleEncap self, uint8 partId)
    {
    uint32 dRegVal = 0;
    uint32 regAddr;

    regAddr = cThaRegDatDecMasterCtrl + PartOffset(self, partId);
    mFieldIns(&dRegVal, cThaRegDatDecapPosLsbFirstMask, cThaRegDatDecapPosLsbFirstShift, cThaLsbTransFirst);
    mFieldIns(&dRegVal, cThaRegDatDec56KStuffBitCtrMask, cThaRegDatDec56KStuffBitCtrShift, cThaPosStuffLsb);
    mModuleHwWrite(self, regAddr, dRegVal);
    }

static eAtRet PartDefaultSet(ThaModuleEncap self, uint8 partId)
    {
    EncapDefaultSet(self, partId);
    DecapDefaultSet(self, partId);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleEncap self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    ret |= ThaModuleEncapGfpDefaultSet(self);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    /* Super initialize */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->DefaultSet((ThaModuleEncap)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eBool ChannelIsBusy(AtModuleEncap self, uint32 channelId)
    {
    AtHdlcLink hdlcLink;
    AtEncapChannel encapChannel = AtModuleEncapChannelGet(self, (uint16)channelId);
    if (encapChannel == NULL)
        return cAtFalse;

    /* The current version just support HDLC encapsulation */
    if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
        return cAtFalse;

    /* Check if this HDLC channel is carrying traffic. An encapsulation channel
     * is busy if its link is added to a bundle or it is carrying traffic of
     * Ethernet flow */
    hdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);
    if (AtHdlcLinkBundleGet(hdlcLink) || AtHdlcLinkBoundFlowGet(hdlcLink))
        return cAtTrue;

    return cAtFalse;
    }

static ThaPppLink PppLink(AtModuleEncap self, uint16 channelId)
    {
    AtEncapChannel channel = AtModuleEncapChannelGet(self, channelId);

    if (AtEncapChannelEncapTypeGet(channel) != cAtEncapHdlc)
        return NULL;
    if (AtHdlcChannelFrameTypeGet((AtHdlcChannel)channel) != cAtHdlcFrmPpp)
        return NULL;

    return (ThaPppLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)channel);
    }

static eAtRet ChannelDelete(AtModuleEncap self, uint32 channelId)
    {
    eAtRet ret = cAtOk;

    if (ChannelIsBusy(self, channelId))
        return cAtErrorChannelBusy;

    ret |= ThaPppLinkPppPacketEnable(PppLink(self, (uint16)channelId), cAtFalse);
    ret |= m_AtModuleEncapMethods->ChannelDelete(self, channelId);

    return ret;
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleEncapRegisterIteratorCreate(self);
    }

/* Idle pattern */
static eAtRet IdlePatternSet(AtModuleEncap self, uint8 pattern)
    {
    uint32 regVal = 0;

    mRegFieldSet(regVal, cThaMapIdlePattern, pattern);
    mModuleHwWrite(self, cThaMapIdlePatternControl, regVal);
    return cAtOk;
    }

static uint8 IdlePatternGet(AtModuleEncap self)
    {
    uint32 regVal = mModuleHwRead(self, cThaMapIdlePatternControl);
    return (uint8)mRegField(regVal, cThaMapIdlePattern);
    }

static eBool HdlcByteCountersAreSupported(ThaModuleEncap self)
    {
    AtUnused(self);
    /* Not all products support these counters. Let concrete products determine */
    return cAtFalse;
    }

static eBool AcfcSupported(ThaModuleEncap self)
    {
    AtUnused(self);
    /* Not all products support ACFC. Let concrete products determine */
    return cAtFalse;
    }

static uint32 DataEncapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    AtUnused(self);
    return (isReadToClear == cAtTrue) ? 0x001C1000 : 0x001C1080;
    }

static uint32 DataDecapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    AtUnused(self);
    return (isReadToClear == cAtTrue) ? 0x00201000 : 0x00201080;
    }

static uint32 DataEncapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    AtUnused(self);
    return (isReadToClear == cAtTrue) ? 0x001C1800 : 0x001C1880;
    }

static uint32 DataDecapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    AtUnused(self);
    return (isReadToClear == cAtTrue) ? 0x00201800 : 0x00201880;
    }

static void MethodsInit(ThaModuleEncap self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HdlcByteCountersAreSupported);
        mMethodOverride(m_methods, AcfcSupported);
        mMethodOverride(m_methods, StartDeviceVersionThatSupportsFrameRelay);
        mMethodOverride(m_methods, StartDeviceVersionThatSupportsIdleByteCounter);
        mMethodOverride(m_methods, DataEncapPerByteCounterAddress);
        mMethodOverride(m_methods, DataDecapPerByteCounterAddress);
        mMethodOverride(m_methods, DataEncapPerIdleByteCounterAddress);
        mMethodOverride(m_methods, DataDecapPerIdleByteCounterAddress);
        mMethodOverride(m_methods, DefaultSet);

        /* MPEG Bit fields */
        mBitFieldOverride(ThaModuleEncap, m_methods, MPEGUpOamPktLen)
        mBitFieldOverride(ThaModuleEncap, m_methods, MPEGUpOamLinkID)
        mBitFieldOverride(ThaModuleEncap, m_methods, MPEGPppLinkCtrl1BundleID)

        /* MPIG Bit fields */
        mBitFieldOverride(ThaModuleEncap, m_methods, MlppHdrMode)
        mBitFieldOverride(ThaModuleEncap, m_methods, PidMode)
        mBitFieldOverride(ThaModuleEncap, m_methods, AddrCtrlMode)
        mBitFieldOverride(ThaModuleEncap, m_methods, AdrComp)
        mBitFieldOverride(ThaModuleEncap, m_methods, BlkAloc)
        mBitFieldOverride(ThaModuleEncap, m_methods, FcsErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_methods, AdrCtlErrDropEn)
        mBitFieldOverride(ThaModuleEncap, m_methods, NCPRule)
        mBitFieldOverride(ThaModuleEncap, m_methods, LCPRule)
        mBitFieldOverride(ThaModuleEncap, m_methods, MlpRuleRule)
        mBitFieldOverride(ThaModuleEncap, m_methods, PppRule)
        mBitFieldOverride(ThaModuleEncap, m_methods, PKtMode)
        mBitFieldOverride(ThaModuleEncap, m_methods, BndID)
        mBitFieldOverride(ThaModuleEncap, m_methods, MPIGDatEmiLbpBlkVl)
        mBitFieldOverride(ThaModuleEncap, m_methods, MPIGDatEmiLbpBlkID)
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModuleEncap(ThaModuleEncap self)
    {
    AtModuleEncap encapModule = (AtModuleEncap)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEncapMethods = mMethodsGet(encapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, m_AtModuleEncapMethods, sizeof(m_AtModuleEncapOverride));
        mMethodOverride(m_AtModuleEncapOverride, MaxChannelsGet);
        mMethodOverride(m_AtModuleEncapOverride, HdlcPppChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, FrChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, PppLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, ChannelDelete);
        mMethodOverride(m_AtModuleEncapOverride, IdlePatternGet);
        mMethodOverride(m_AtModuleEncapOverride, IdlePatternSet);
        mMethodOverride(m_AtModuleEncapOverride, FrLinkObjectCreate);
        }

    mMethodsSet(encapModule, &m_AtModuleEncapOverride);
    }

static void OverrideAtModule(ThaModuleEncap self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(ThaModuleEncap self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaModuleEncap self)
    {
    OverrideAtModuleEncap(self);
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModuleEncap ThaModuleEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModuleEncap));

    /* Super constructor */
    if (AtModuleEncapObjectInit((AtModuleEncap)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap ThaModuleEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModuleEncap));
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return ThaModuleEncapObjectInit(newModule, device);
    }

void ThaModuleEncapEncapChannelRegsShow(AtEncapChannel channel)
    {
    AtModuleEncap moduleEncap = (AtModuleEncap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cAtModuleEncap);
    AtPrintc(cSevInfo, "* ENCAP registers of '%s':\r\n", AtObjectToString((AtObject)channel));
    
    if (moduleEncap)
        mMethodsGet(moduleEncap)->EncapChannelRegsShow(moduleEncap, channel);
    }

void ThaModuleEncapHdlcBundleRegsShow(AtHdlcBundle bundle)
    {
    AtModuleEncap moduleEncap = (AtModuleEncap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)bundle), cAtModuleEncap);
    AtPrintc(cSevInfo, "* ENCAP registers of '%s':\r\n", AtObjectToString((AtObject)bundle));
    
    if (moduleEncap)
        mMethodsGet(moduleEncap)->HdlcBundleRegsShow(moduleEncap, bundle);
    }

eAtRet ThaModuleEncapHardwareBlockAllocate(AtChannel physical, AtEncapChannel encapChannel, eThaModuleEncapBlockType blockType)
    {
    eAtRet ret = cAtOk;
    AtPppLink pppLink;
    eBool isHighBandwidth = (blockType == cThaModuleEncapBlockTypeHighBandwidth) ? cAtTrue :
    cAtFalse;

    /* Do not need to handle resource when physical is unbound */
    if (encapChannel == NULL)
        return cAtOk;

    /* Do not allocate MPIG block if this link is not PPP link */
    if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
        return cAtOk;

    pppLink = (AtPppLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);
    if (pppLink)
        {
        uint16 numBlocks = mMethodsGet(physical)->NumBlocksNeedToBindToEncapChannel(physical, encapChannel);
        ret |= ThaPppLinkBlockAllocate(pppLink, numBlocks, isHighBandwidth);
        ThaHdlcChannelHighBandwidthSet((ThaHdlcChannel)encapChannel, isHighBandwidth);
        }

    return ret;
    }

uint32 ThaModuleEncapNumEncapPerPart(ThaModuleEncap self)
    {
    ThaDevice device;

    if (self == NULL)
        return 0;

    device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return (uint32)(AtModuleEncapMaxChannelsGet((AtModuleEncap)self) / ThaDeviceNumPartsOfModule(device, cAtModuleEncap));
    }

eBool ThaModuleEncapHdlcByteCountersAreSupported(ThaModuleEncap self)
    {
    if (self)
        return mMethodsGet(self)->HdlcByteCountersAreSupported(self);
    return cAtFalse;
    }

eBool ThaModuleEncapHdlcIdleByteCountersAreSupported(ThaModuleEncap self)
    {
    uint32 supportedVersion = mMethodsGet(self)->StartDeviceVersionThatSupportsIdleByteCounter(self);
    return AtDeviceVersionNumber(AtModuleDeviceGet((AtModule)self)) >= supportedVersion;
    }

eBool ThaModuleEncapAcfcSupported(ThaModuleEncap self)
    {
    if (self)
        return mMethodsGet(self)->AcfcSupported(self);
    return cAtFalse;
    }

eBool ThaModuleEncapFrameRelayIsSupported(ThaModuleEncap self)
    {
    uint32 supportedVersion;

    if (self == NULL)
        return cAtFalse;

    supportedVersion = mMethodsGet(self)->StartDeviceVersionThatSupportsFrameRelay(self);
    return AtDeviceVersionNumber(AtModuleDeviceGet((AtModule)self)) >= supportedVersion;
    }

uint32 ThaModuleEncapDataEncapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    if (self)
        return mMethodsGet(self)->DataEncapPerByteCounterAddress(self, isReadToClear);
    return 0;
    }

uint32 ThaModuleEncapDataDecapPerByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    if (self)
        return mMethodsGet(self)->DataDecapPerByteCounterAddress(self, isReadToClear);
    return 0;
    }

uint32 ThaModuleEncapDataEncapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    if (self)
        return mMethodsGet(self)->DataEncapPerIdleByteCounterAddress(self, isReadToClear);
    return 0;
    }

uint32 ThaModuleEncapDataDecapPerIdleByteCounterAddress(ThaModuleEncap self, eBool isReadToClear)
    {
    if (self)
        return mMethodsGet(self)->DataDecapPerIdleByteCounterAddress(self, isReadToClear);
    return 0;
    }

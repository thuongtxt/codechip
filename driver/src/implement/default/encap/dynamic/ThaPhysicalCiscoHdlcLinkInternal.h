/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encapsulation
 * 
 * File        : ThaPhysicalCiscoHdlcLinkInternal.h
 * 
 * Created Date: Apr 11, 2016
 *
 * Description : Physical Cisco HDLC link internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPHYSICALCISCOHDLCLINKINTERNAL_H_
#define _THAPHYSICALCISCOHDLCLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ppp/dynamic/ThaPhysicalPppLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPhysicalCiscoHdlcLink
    {
    tThaPhysicalPppLink super;
    }tThaPhysicalCiscoHdlcLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaPhysicalCiscoHdlcLinkObjectInit(AtPppLink self, AtHdlcChannel physicalChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THAPHYSICALCISCOHDLCLINKINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ThaPhysicalCiscoHdlcLink.c
 *
 * Created Date: Apr 6, 2016
 *
 * Description : Physical Cisco HDLC link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPhysicalHdlcChannel.h"
#include "ThaModuleDynamicEncap.h"
#include "ThaPhysicalCiscoHdlcLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcLinkMethods m_AtHdlcLinkOverride;
static tAtChannelMethods  m_AtChannelOverride;
static tAtPppLinkMethods  m_AtPppLinkOverride;

/* Save super implementation */
static const tAtHdlcLinkMethods *m_AtHdlcLinkMethods = NULL;
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtPppLinkMethods  *m_AtPppLinkMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ProtocolCompress(AtHdlcLink self, eBool compress)
    {
    AtUnused(self);
    return (compress) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool ProtocolIsCompressed(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AllProfileDefault(ThaPhysicalPppLink self)
    {
    ThaProfileManager manager      = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    ThaProfileFinder errorFinder   = ThaProfileManagerErrorProfileFinderGet(manager);
    ThaProfileFinder oamFinder     = ThaProfileManagerOamProfileFinderGet(manager);
    ThaProfileFinder networkFinder = ThaProfileManagerNetworkProfileFinderGet(manager);

    ThaProfileFinderRuleSet(networkFinder, cThaProfileNetworkTypeAll, cThaProfileRuleData);
    ThaPhysicalPppLinkNetworkProfileSet(self, ThaProfileFinderProfileForLinkFind(networkFinder, (AtHdlcLink)self));

    ThaProfileFinderRuleSet(oamFinder, cThaProfileOamTypeAll, cThaProfileRuleControl);
    ThaPhysicalPppLinkOamProfileSet(self, ThaProfileFinderProfileForLinkFind(oamFinder, (AtHdlcLink)self));

    ThaProfileFinderRuleSet(errorFinder, cThaProfileErrorTypeAll, cThaProfileRuleDrop);
    ThaPhysicalPppLinkErrorProfileSet(self, ThaProfileFinderProfileForLinkFind(errorFinder, (AtHdlcLink)self));

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    return AllProfileDefault((ThaPhysicalPppLink)self);
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "physical_chdlc_link";
    }

static eAtModulePwRet PduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType)
    {
    eAtRet ret;
    ThaProfileManager manager;
    ThaProfileFinder networkFinder;
    uint16 networkProfileType;
    ThaPhysicalPppLink physicalLink = (ThaPhysicalPppLink)self;

    /* C-HDLC do not support bridge mode */
    if (pduType == cAtHdlcPduTypeEthernet)
        return cAtErrorModeNotSupport;

    networkProfileType = ThaModuleEncapPduType2NetworkProfileType(pduType);
    manager = ThaModuleEncapProfileManagerGet((AtModuleEncap)AtChannelModuleGet((AtChannel)self));
    networkFinder = ThaProfileManagerNetworkProfileFinderGet(manager);
    ThaProfileFinderRuleSet(networkFinder, networkProfileType, cThaProfileRuleData);
    ret = ThaPhysicalPppLinkNetworkProfileSet(physicalLink, ThaProfileFinderProfileForLinkFind(networkFinder, self));
    if (ret != cAtOk)
        return ret;

    ThaPhysicalPppLinkPduTypeCache(physicalLink, pduType);
    return ret;
    }

static eAtPppLinkPhase PhaseGet(AtPppLink self)
    {
    AtUnused(self);
    return cAtPppLinkPhaseUnknown;
    }

static eAtModulePppRet PhaseSet(AtPppLink self, eAtPppLinkPhase phase)
    {
    AtUnused(self);
    AtUnused(phase);
    return cAtErrorModeNotSupport;
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcLink(AtPppLink self)
    {
    AtHdlcLink hdlcLink = (AtHdlcLink)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcLinkMethods = mMethodsGet(hdlcLink) ;
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, m_AtHdlcLinkMethods, sizeof(m_AtHdlcLinkOverride));

        mMethodOverride(m_AtHdlcLinkOverride, ProtocolCompress);
        mMethodOverride(m_AtHdlcLinkOverride, ProtocolIsCompressed);
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeSet);
        }

    mMethodsSet(hdlcLink, &m_AtHdlcLinkOverride);
    }

static void OverrideAtPppLink(AtPppLink self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPppLinkMethods = mMethodsGet(self) ;
        mMethodsGet(osal)->MemCpy(osal, &m_AtPppLinkOverride, m_AtPppLinkMethods, sizeof(m_AtPppLinkOverride));

        mMethodOverride(m_AtPppLinkOverride, PhaseGet);
        mMethodOverride(m_AtPppLinkOverride, PhaseSet);
        }

    mMethodsSet(self, &m_AtPppLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideAtHdlcLink(self);
    OverrideAtChannel(self);
    OverrideAtPppLink(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPhysicalCiscoHdlcLink);
    }

AtPppLink ThaPhysicalCiscoHdlcLinkObjectInit(AtPppLink self, AtHdlcChannel physicalChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPhysicalPppLinkObjectInit(self, physicalChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink ThaPhysicalCiscoHdlcLinkNew(AtHdlcChannel physicalChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ThaPhysicalCiscoHdlcLinkObjectInit(newLink, physicalChannel);
    }


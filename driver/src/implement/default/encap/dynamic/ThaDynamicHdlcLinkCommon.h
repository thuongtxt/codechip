/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaDynamicHdlcLinkCommon.h
 * 
 * Created Date: Apr 6, 2016
 *
 * Description : HDLC link common setup
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwHdlc.h"
#include "../../../../generic/eth/AtEthFlowInternal.h"
#include "../../ppp/ThaPppLink.h"
#include "../../eth/ThaEthFlow.h"
#include "ThaDynamicHdlcLink.h"
#include "ThaDynamicHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
static eBool IsLogical(AtHdlcLink self)
    {
    AtHdlcChannel channel = AtHdlcLinkHdlcChannelGet(self);
    return AtEncapChannelBoundPhyGet((AtEncapChannel)channel) ? cAtFalse : cAtTrue;
    }

static AtHdlcLink PhysicalLinkGet(AtHdlcLink self)
    {
    AtHdlcChannel physicalChannel;
    if (IsLogical(self))
        return NULL;

    physicalChannel = ThaHdlcChannelPhysicalChannelGet(AtHdlcLinkHdlcChannelGet(self));
    return AtHdlcChannelHdlcLinkGet(physicalChannel);
    }

static uint32 HdlcCacheSize(void)
    {
    return sizeof(tThaDynamicHdlcLinkCache);
    }

static tThaDynamicHdlcLinkCache *HdlcCacheGet(AtHdlcLink self)
    {
    if (mThis(self)->hdlcCache == NULL)
        {
        tThaDynamicHdlcLinkCache *cache = AtOsalMemAlloc(HdlcCacheSize());
        if (cache == NULL)
            return NULL;

        AtOsalMemInit(cache, 0, HdlcCacheSize());
        cache->phase = cAtPppLinkPhaseDead;
        mThis(self)->hdlcCache = cache;
        }

    return mThis(self)->hdlcCache;
    }

static eAtRet FlowUnbind(AtHdlcLink self, AtEthFlow currentFlow)
    {
    eAtRet ret;
    ThaEthFlowController flowController;

    if (currentFlow == NULL)
        return cAtOk;

    if (IsLogical(self))
        {
        tThaDynamicHdlcLinkCache* cache = HdlcCacheGet((AtHdlcLink)self);
        if (cache == NULL)
            return cAtErrorNullPointer;

        cache->flow = NULL;
        AtEthFlowHdlcLinkSet(currentFlow, NULL);
        return cAtOk;
        }

    flowController = ControllerGet(self);
    ret = ThaEthFlowControllerFlowDeActivate(flowController, currentFlow);
    if (ret == cAtOk)
    	{
        AtHdlcLinkFlowSet(self, NULL);
        AtEthFlowHdlcLinkSet(currentFlow, NULL);
        }

    return ret;
    }

static eAtRet FlowBind(AtHdlcLink self, AtEthFlow flow)
    {
    eAtRet ret;
    AtEthFlow currentFlow = AtHdlcLinkBoundFlowGet(self);
    ThaEthFlowController flowController;

    if (currentFlow == flow)
        return cAtOk;

    if (flow == NULL)
        return FlowUnbind(self, currentFlow);

    ret = ThaHdlcLinkFlowCanBeBound(self, flow);
    if (ret != cAtOk)
        return ret;

    if (IsLogical(self))
        {
        mHdlcCacheValueSet(flow);
        AtEthFlowHdlcLinkSet(flow, self);

        return cAtOk;
        }

    /* Updated database */
    AtHdlcLinkFlowSet(self, flow);

    flowController = ControllerGet(self);
    return ThaEthFlowControllerFlowActivate(flowController, flow, (AtChannel)self, 0);
    }

static eAtRet OamPacketModeSet(AtHdlcLink self, eAtHdlcLinkOamMode oamPacketMode)
    {
    AtUnused(self);
    return (oamPacketMode == cAtHdlcLinkOamModeToPsn) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcLinkOamMode OamPacketModeGet(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtHdlcLinkOamModeToPsn;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    if (IsLogical((AtHdlcLink)self))
        return cAtModuleEncapErrorNoBoundFlow;

    return AtChannelAllCountersGet((AtChannel)PhysicalLinkGet((AtHdlcLink)self), pAllCounters);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    if (IsLogical((AtHdlcLink)self))
        return cAtModuleEncapErrorNoBoundFlow;

    return AtChannelAllCountersClear((AtChannel)PhysicalLinkGet((AtHdlcLink)self), pAllCounters);
    }

static AtEthFlow OamFlowCreate(AtHdlcLink self)
    {
    return AtHdlcLinkOamFlowCreate(self);
    }

static void OamFlowDelete(AtHdlcLink self)
    {
    AtHdlcLinkOamFlowDelete(self);
    }

static eAtRet HdlcLinkActivateConfiguration(AtHdlcLink self)
    {
    eAtRet ret = cAtOk;
    tThaDynamicHdlcLinkCache *cache = HdlcCacheGet(self);

    if (mThis(self)->hdlcCache == NULL)
        return cAtErrorNullPointer;

    ret |= AtHdlcLinkFlowBind(self, cache->flow);
    ret |= AtHdlcLinkPduTypeSet(self, cache->pduType);

    return ret;
    }

static eAtRet HdlcLinkConfigureCache(AtHdlcLink self)
    {
    tThaDynamicHdlcLinkCache *cache = HdlcCacheGet(self);

    if (mThis(self)->hdlcCache == NULL)
        return cAtErrorNullPointer;

    cache->flow     = AtHdlcLinkBoundFlowGet(self);
    cache->pduType  = AtHdlcLinkPduTypeGet(self);

    return cAtOk;
    }

static eAtRet HwResourceAllocate(AtChannel circuit)
    {
    AtHdlcChannel channel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)circuit);
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)channel);

    if (phyChannel == NULL)
        mChannelError(circuit, cAtErrorNullPointer);

    return AtChannelHwResourceAllocate(phyChannel);
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    return AtChannelHwIdGet((AtChannel)hdlcChannel);
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    AtHdlcLink physicalLink;

    if (ret != cAtOk)
        return ret;

    physicalLink = ThaDynamicHdlcLinkPhysicalHdlcLinkGet((AtHdlcLink)self);
    return AtChannelBindToPseudowire((AtChannel)physicalLink, pseudowire);
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtHdlcChannel logicChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    AtHdlcChannel physicalHdlcChannel = ThaHdlcChannelPhysicalChannelGet(logicChannel);

    if (physicalHdlcChannel == NULL)
        return cAtOk;

    return AtChannelTxTrafficEnable((AtChannel)physicalHdlcChannel, enable);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    ThaEthFlow flow;
    eAtRet ret = cAtOk;
    AtHdlcChannel logicChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    AtHdlcChannel physicalHdlcChannel = ThaHdlcChannelPhysicalChannelGet(logicChannel);

    if (physicalHdlcChannel == NULL)
        return cAtOk;

    flow = (ThaEthFlow)AtHdlcLinkBoundFlowGet((AtHdlcLink)self);
    if (flow)
        ret = ThaEthFlowRxTrafficEnable(flow, enable);

    if (ret != cAtOk)
        return ret;

    return AtChannelRxTrafficEnable((AtChannel)physicalHdlcChannel, enable);
    }

static eAtRet Debug(AtChannel self)
    {
    AtHdlcChannel physicalChannel = ThaHdlcChannelPhysicalChannelGet(AtHdlcLinkHdlcChannelGet((AtHdlcLink)self));
    if (physicalChannel == NULL)
        {
        AtPrintc(cSevInfo, "Link is logical\r\n");
        return cAtOk;
        }

    /* Debugging on physical link. */
    AtChannelDebug((AtChannel)AtHdlcChannelHdlcLinkGet(physicalChannel));
    return cAtOk;
    }

static void CacheDelete(AtChannel self)
    {
    AtOsalMemFree(mThis(self)->hdlcCache);
    mThis(self)->hdlcCache = NULL;
    }

static void Delete(AtObject self)
    {
    CacheDelete((AtChannel)self);
    AtChannelBoundPwObjectDelete((AtChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static eAtRet Init(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtHdlcChannel logicChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    AtHdlcChannel physicalHdlcChannel = ThaHdlcChannelPhysicalChannelGet(logicChannel);

    if (physicalHdlcChannel == NULL)
        return cAtOk;

    return AtChannelRxTrafficIsEnabled((AtChannel)physicalHdlcChannel);
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtHdlcChannel logicChannel = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    AtHdlcChannel physicalHdlcChannel = ThaHdlcChannelPhysicalChannelGet(logicChannel);

    if (physicalHdlcChannel == NULL)
        return cAtOk;

    return AtChannelTxTrafficIsEnabled((AtChannel)physicalHdlcChannel);
    }

static AtPw BoundPwGet(AtHdlcLink self)
    {
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);

    if (pw == NULL)
        {
        AtHdlcChannel channel = AtHdlcLinkHdlcChannelGet(self);
        pw = AtChannelBoundPwGet((AtChannel)channel);
        }

    return pw;
    }

static eAtModulePwRet PduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType)
    {
    eAtRet ret;
    AtPwHdlc pw = (AtPwHdlc)BoundPwGet(self);
    AtEthFlow flow;

    if (IsLogical(self))
        mHdlcCacheValueSet(pduType);

    ret = AtHdlcLinkPduTypeSet(PhysicalLinkGet(self), pduType);
    if (ret != cAtOk)
        return ret;

    /* Re-updated pdu payload type */
    pw = (AtPwHdlc)BoundPwGet(self);
    if (pw && (AtPwHdlcPayloadTypeGet(pw) == cAtPwHdlcPayloadTypePdu))
        AtPwHdlcPayloadTypeSet(pw, cAtPwHdlcPayloadTypePdu);

    flow = AtHdlcLinkBoundFlowGet(self);
    if (flow)
        ret = ThaEthFlowRxTrafficEnable((ThaEthFlow)flow, AtHdlcLinkRxTrafficIsEnabled(self));

    return cAtOk;
    }

static eAtHdlcPduType PduTypeGet(AtHdlcLink self)
    {
    if (IsLogical(self))
        mHdlcCacheValueGet(pduType, cAtHdlcPduTypeAny);
        
    return AtHdlcLinkPduTypeGet(PhysicalLinkGet(self));
    }

static void OverrideAtChannel(AtHdlcLink self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcLink(AtHdlcLink self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcLinkMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcLinkOverride, mMethodsGet(self), sizeof(tAtHdlcLinkMethods));

        mMethodOverride(m_AtHdlcLinkOverride, FlowBind);
        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeSet);
        mMethodOverride(m_AtHdlcLinkOverride, OamPacketModeGet);
        mMethodOverride(m_AtHdlcLinkOverride, OamFlowCreate);
        mMethodOverride(m_AtHdlcLinkOverride, OamFlowDelete);
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeSet);
        mMethodOverride(m_AtHdlcLinkOverride, PduTypeGet);
        }

    mMethodsSet(self, &m_AtHdlcLinkOverride);
    }

static void OverrideAtObject(AtHdlcLink self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaEncapDynamicBinder.h
 * 
 * Created Date: Apr 6, 2016
 *
 * Description : Dynamic encap binder utilities
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAENCAPDYNAMICBINDER_H_
#define _THAENCAPDYNAMICBINDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "../../binder/ThaEncapBinder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 ThaEncapDynamicBinderHwSlice(ThaEncapDynamicBinder self, uint32 hwId);
eBool ThaEncapDynamicBinderHwIsHo(ThaEncapDynamicBinder self, uint32 hwId);
uint32 ThaEncapDynamicBinderHwChannelId(ThaEncapDynamicBinder self, uint32 hwId);
uint32 ThaEncapDynamicBinderCompressedHwId(ThaEncapDynamicBinder self, uint32 channelId, uint8 slice, eBool isHo);
void ThaEncapDynamicBinderChannelUse(ThaEncapDynamicBinder self, uint8 slice, uint32 channelId);

#ifdef __cplusplus
}
#endif
#endif /* _THAENCAPDYNAMICBINDER_H_ */


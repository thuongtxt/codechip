/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaModuleDynamicEncapInternal.h
 * 
 * Created Date: Mar 18, 2016
 *
 * Description : Default dynamic ENCAP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEDYNAMICENCAPINTERNAL_H_
#define _THAMODULEDYNAMICENCAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/encap/AtModuleEncapInternal.h"
#include "ThaModuleDynamicEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tThaModuleDynamicEncapMethods
    {
    AtEncapChannel (*PhysicalEncapChannelCreate)(ThaModuleDynamicEncap self, uint32 physicalEncapId, AtEncapChannel logicalChannel);
    ThaProfileManager (*ProfileManagerObjectCreate)(ThaModuleDynamicEncap self);
    eBool (*ProfileManagerIsSupported)(ThaModuleDynamicEncap self);
    uint32 (*MaxHdlcLinksGet)(ThaModuleDynamicEncap self);

    /* Debug */
    void (*EncapChannelRegsShow)(ThaModuleDynamicEncap self, AtEncapChannel channel);
    void (*HdlcBundleRegsShow)(ThaModuleDynamicEncap self, AtHdlcBundle bundle);
    }tThaModuleDynamicEncapMethods;

typedef struct tThaModuleDynamicEncap
    {
    tAtModuleEncap super;
    const tThaModuleDynamicEncapMethods * methods;

    /* Private data */
    ThaProfileManager profileManager;
    }tThaModuleDynamicEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap ThaModuleDynamicEncapObjectInit(AtModuleEncap self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEDYNAMICENCAPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ThaDynamicCiscoHdlcLink.c
 *
 * Created Date: Apr 6, 2016
 *
 * Description : Dynamic Cisco HDLC link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/ppp/AtPppLinkInternal.h"
#include "../../eth/controller/ThaEthFlowController.h"
#include "../../eth/ThaModuleEth.h"
#include "ThaDynamicHdlcLink.h"
#include "ThaPhysicalHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaDynamicCiscoHdlcLink)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaDynamicCiscoHdlcLink * ThaDynamicCiscoHdlcLink;
typedef struct tThaDynamicCiscoHdlcLink
    {
    tAtPppLink super;

    tThaDynamicHdlcLinkCache *hdlcCache;
    }tThaDynamicCiscoHdlcLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tAtHdlcLinkMethods m_AtHdlcLinkOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods = NULL;
static const tAtObjectMethods   *m_AtObjectMethods  = NULL;
static const tAtHdlcLinkMethods *m_AtHdlcLinkMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth ModuleEth(AtHdlcLink self)
    {
    return (ThaModuleEth)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEth);
    }

static ThaEthFlowController ControllerGet(AtHdlcLink self)
    {
    return ThaModuleEthCiscoHdlcFlowControllerGet(ModuleEth(self));
    }

#include "ThaDynamicHdlcLinkCommon.h"

static void Override(AtPppLink self)
    {
    OverrideAtChannel((AtHdlcLink)self);
    OverrideAtHdlcLink((AtHdlcLink)self);
    OverrideAtObject((AtHdlcLink)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDynamicCiscoHdlcLink);
    }

static AtPppLink ObjectInit(AtPppLink self, AtHdlcChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPppLinkObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink ThaDynamicCiscoHdlcLinkNew(AtHdlcChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, channel);
    }

eAtRet ThaDynamicCiscoHdlcLinkActivateConfiguration(AtHdlcLink self)
    {
    return HdlcLinkActivateConfiguration(self);
    }

eAtRet ThaDynamicCiscoHdlcLinkConfigureCache(AtHdlcLink self)
    {
    return HdlcLinkConfigureCache(self);
    }

AtHdlcLink ThaDynamicHdlcLinkPhysicalHdlcLinkGet(AtHdlcLink logicalLink)
    {
    AtHdlcChannel logicChannel = AtHdlcLinkHdlcChannelGet(logicalLink);
    AtHdlcChannel physicalChannel = ThaHdlcChannelPhysicalChannelGet(logicChannel);

    return AtHdlcChannelHdlcLinkGet(physicalChannel);
    }

AtHdlcLink ThaPhysicalHdlcLinkLogicLinkGet(AtHdlcLink physicalLink)
    {
    AtHdlcChannel physicalChannel = AtHdlcLinkHdlcChannelGet(physicalLink);
    AtHdlcChannel logicChannel = ThaPhysicalHdlcChannelLogicalChannelGet(physicalChannel);

    return AtHdlcChannelHdlcLinkGet(logicChannel);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaModuleDynamicEncap.h
 * 
 * Created Date: Mar 18, 2016
 *
 * Description : Module dynamic encap.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEDYNAMICENCAP_H_
#define _THAMODULEDYNAMICENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcLink.h"

#include "../profile/ThaProfileManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleDynamicEncap * ThaModuleDynamicEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap ThaModuleDynamicEncapNew(AtDevice device);

/* Helper functions */
ThaProfileManager ThaModuleEncapProfileManagerGet(AtModuleEncap self);
uint16 ThaModuleEncapPduType2NetworkProfileType(eAtHdlcPduType pduType);

/* Physical ENCAP channels */
AtHdlcChannel ThaPhysicalHdlcChannelNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);

/* Physical link channels */
AtPppLink ThaPhysicalCiscoHdlcLinkNew(AtHdlcChannel physicalChannel);

/* Concrete products */
AtPppLink Tha60210012PhysicalCiscoHdlcLinkNew(AtHdlcChannel physicalChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEDYNAMICENCAP_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ThaPhysicalHdlcChannel.c
 *
 * Created Date: Mar 29, 2016
 *
 * Description : Physical HDLC channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/encap/AtHdlcChannelInternal.h"
#include "../../man/ThaDevice.h"
#include "../../cla/controllers/ThaClaPwController.h"
#include "../../ppp/dynamic/ThaPhysicalPppLink.h"
#include "../../framerelay/dynamic/ThaPhysicalFrLink.h"
#include "ThaModuleDynamicEncap.h"
#include "ThaPhysicalHdlcChannelInternal.h"
#include "ThaEncapDynamicBinder.h"
#include "ThaDynamicHdlcLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPhysicalHdlcChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPhysicalHdlcChannelMethods m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;
static tAtHdlcChannelMethods  m_AtHdlcChannelOverride;
static tThaHdlcChannelMethods m_ThaHdlcChannelOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;
static const tAtHdlcChannelMethods  *m_AtHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtHdlcChannel channel = (AtHdlcChannel)self;

    AtObjectDelete((AtObject)AtHdlcChannelHdlcLinkGet(channel));
    AtHdlcChannelHdlcLinkSet(channel, NULL);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPhysicalHdlcChannel object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(logicalChannel);
    }

static ThaEncapDynamicBinder EncapBinder(AtHdlcChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaEncapDynamicBinder)AtDeviceEncapBinder(device);
    }

static AtChannel BoundPhyGet(AtEncapChannel self)
    {
    return AtEncapChannelBoundPhyGet((AtEncapChannel)mThis(self)->logicalChannel);
    }

static AtPw BoundPseudowireGet(AtChannel self)
    {
    return AtChannelBoundPwGet((AtChannel)mThis(self)->logicalChannel);
    }

static eAtRet AddressMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(self);
    return compareEnable ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet ExpectedAddressSet(AtHdlcChannel self, uint8 * expectedAddress)
    {
    AtUnused(self);
    AtUnused(expectedAddress);
    return cAtOk;
    }

static eBool AddressMonitorIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ExpectedAddressGet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtOk;
    }

static eAtRet AddressInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    AtUnused(self);
    return insertEnable ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet TxAddressSet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtOk;
    }

static eBool AddressInsertionIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TxAddressGet(AtHdlcChannel self, uint8 *insertedAddress)
    {
    AtUnused(self);

    if (insertedAddress != NULL)
        *insertedAddress = 0xFF;

    return cAtOk;
    }

static eAtRet ControlMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(self);
    return compareEnable ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet ExpectedControlSet(AtHdlcChannel self, uint8 * expectedControl)
    {
    AtUnused(self);
    AtUnused(expectedControl);
    return cAtOk;
    }

static eBool ControlMonitorIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ExpectedControlGet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(self);
    AtUnused(control);
    return cAtOk;
    }

static eAtRet ControlInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    AtUnused(self);
    return insertEnable ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet TxControlSet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(self);
    AtUnused(control);
    return cAtOk;
    }

static eBool ControlInsertionIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TxControlGet(AtHdlcChannel self, uint8 *insertedControl)
    {
    AtUnused(self);

    if (insertedControl != NULL)
        *insertedControl = 0x03;

    return cAtOk;
    }

static eAtRet FlagSet(AtHdlcChannel self, uint8 flag)
    {
    AtUnused(self);
    AtUnused(self);
    if (flag == 0x7E)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint8 FlagGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 0x7E;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return ThaEncapDynamicBinderHwChannelId(EncapBinder((AtHdlcChannel)self), AtChannelIdGet(self));
    }

static AtHdlcChannel LogicalChannel(AtHdlcChannel physicalChannel)
    {
    return mThis(physicalChannel)->logicalChannel;
    }

static eAtRet LinkActivate(ThaPhysicalHdlcChannel self)
    {
    eAtRet ret;
    AtHdlcLink logicalLink;
    AtHdlcChannel physicalChannel = (AtHdlcChannel)self;
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet(physicalChannel);

    AtHdlcLink physicalLink = mMethodsGet(self)->LinkCreate(self);
    if (physicalLink == NULL)
        return cAtOk;

    ret = AtChannelInit((AtChannel)physicalLink);
    if (ret != cAtOk)
        return ret;

    AtHdlcChannelHdlcLinkSet(physicalChannel, physicalLink);
    logicalLink = AtHdlcChannelHdlcLinkGet(LogicalChannel(physicalChannel));

    if (frameType == cAtHdlcFrmPpp)
        return ThaDynamicPppLinkActivateConfiguration(logicalLink);

    if (frameType == cAtHdlcFrmCiscoHdlc)
        return ThaDynamicCiscoHdlcLinkActivateConfiguration(logicalLink);

    if (frameType == cAtHdlcFrmFr)
        return ThaDynamicFrLinkActivateConfiguration(logicalLink);

    return cAtErrorInvlParm;
    }

static eAtRet LinkDeactivate(ThaPhysicalHdlcChannel self)
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet((AtHdlcChannel)self);
    AtHdlcLink logicalLink = AtHdlcChannelHdlcLinkGet(LogicalChannel((AtHdlcChannel)self));

    if (frameType == cAtHdlcFrmCiscoHdlc)
        return ThaDynamicCiscoHdlcLinkConfigureCache(logicalLink);

    else if (frameType == cAtHdlcFrmPpp)
        return ThaDynamicPppLinkConfigureCache(logicalLink);

    else if (frameType == cAtHdlcFrmFr)
        return ThaDynamicFrLinkConfigureCache(logicalLink);

    return cAtErrorInvlParm;
    }

static AtHdlcLink LinkCreate(ThaPhysicalHdlcChannel self)
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet((AtHdlcChannel)self);
    if (frameType == cAtHdlcFrmPpp)
        return (AtHdlcLink)ThaPhysicalPppLinkNew((AtHdlcChannel)self);

    if (frameType == cAtHdlcFrmCiscoHdlc)
        return (AtHdlcLink)ThaPhysicalCiscoHdlcLinkNew((AtHdlcChannel)self);

    if (frameType == cAtHdlcFrmFr)
        return (AtHdlcLink)ThaPhysicalFrLinkNew((AtHdlcChannel)self);

    return NULL;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "physical_hdlc";
    }

static uint32 LoSliceHwChannelIdGet(ThaPhysicalHdlcChannel self)
    {
    AtChannel circuit = AtEncapChannelBoundPhyGet((AtEncapChannel)self);
    return AtChannelBoundEncapHwIdGet(circuit);
    }

static uint32 HaRestore(ThaPhysicalHdlcChannel self)
    {
    uint32 compressId = AtChannelIdGet((AtChannel)self);
    uint8 loSlice;
    uint32 localId;
    ThaEncapDynamicBinder encapBinder = EncapBinder((AtHdlcChannel)self);

    if (ThaEncapDynamicBinderHwIsHo(encapBinder, compressId))
        return 0;

    loSlice = ThaEncapDynamicBinderHwSlice(encapBinder, compressId);
    localId = LoSliceHwChannelIdGet(self);

    AtChannelIdSet((AtChannel)self, ThaEncapDynamicBinderCompressedHwId(encapBinder, localId, loSlice, cAtFalse));
    ThaEncapDynamicBinderChannelUse(encapBinder, loSlice, localId);

    return 0;
    }

static uint8 CircuitSliceGet(ThaHdlcChannel self)
    {
    return ThaEncapDynamicBinderHwSlice(EncapBinder((AtHdlcChannel)self), AtChannelIdGet((AtChannel)self));
    }

static eBool CircuitIsHo(ThaHdlcChannel self)
    {
    return ThaEncapDynamicBinderHwIsHo(EncapBinder((AtHdlcChannel)self), AtChannelIdGet((AtChannel)self));
    }

static eBool RxAddressControlIsCompressed(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool TxAddressControlIsCompressed(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool AddressControlIsBypassed(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DropPacketConditionMaskGet(AtEncapChannel self)
    {
    AtUnused(self);
    return (cAtHdlcChannelDropConditionPktAddrCtrlError|
            cAtHdlcChannelDropConditionPktFcsError);
    }

static void MethodsInit(ThaPhysicalHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LinkActivate);
        mMethodOverride(m_methods, LinkDeactivate);
        mMethodOverride(m_methods, LinkCreate);
        mMethodOverride(m_methods, HaRestore);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, BoundPseudowireGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel encap = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(encap);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, BoundPhyGet);
        mMethodOverride(m_AtEncapChannelOverride, DropPacketConditionMaskGet);
        }

    mMethodsSet(encap, &m_AtEncapChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, FlagSet);
        mMethodOverride(m_AtHdlcChannelOverride, FlagGet);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlIsCompressed);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressControlIsCompressed);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressGet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressGet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlGet);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlGet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressControlIsBypassed);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideThaHdlcChannel(AtHdlcChannel self)
    {
    ThaHdlcChannel hdlcChannel = (ThaHdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHdlcChannelOverride, mMethodsGet(hdlcChannel), sizeof(m_ThaHdlcChannelOverride));
        mMethodOverride(m_ThaHdlcChannelOverride, CircuitSliceGet);
        mMethodOverride(m_ThaHdlcChannelOverride, CircuitIsHo);
        }

    mMethodsSet(hdlcChannel, &m_ThaHdlcChannelOverride);
    }

static void OverrideAtObject(AtHdlcChannel self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideThaHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPhysicalHdlcChannel);
    }

AtHdlcChannel ThaPhysicalHdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    eAtHdlcFrameType initFrameType = (logicalChannel) ? AtHdlcChannelFrameTypeGet(logicalChannel) : cAtHdlcFrmUnknown;

    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHdlcChannelObjectInit(self, channelId, initFrameType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaPhysicalHdlcChannel)self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->logicalChannel = logicalChannel;

    return self;
    }

AtHdlcChannel ThaPhysicalHdlcChannelNew(uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ThaPhysicalHdlcChannelObjectInit(newChannel, channelId, logicalChannel, module);
    }

AtHdlcChannel ThaPhysicalHdlcChannelLogicalChannelGet(AtHdlcChannel physicalChannel)
    {
    if (physicalChannel)
        return LogicalChannel(physicalChannel);

    return NULL;
    }

eAtRet ThaPhysicalHdlcChannelLinkActivate(ThaPhysicalHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->LinkActivate(self);

    return cAtErrorNullPointer;
    }

eAtRet ThaPhysicalHdlcChannelLinkDeactivate(ThaPhysicalHdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->LinkDeactivate(self);

    return cAtErrorNullPointer;
    }

uint32 ThaPhysicalHdlcChannelHaRestore(AtHdlcChannel self)
    {
    if (self)
        return mMethodsGet(mThis(self))->HaRestore(mThis(self));

    return 0;
    }

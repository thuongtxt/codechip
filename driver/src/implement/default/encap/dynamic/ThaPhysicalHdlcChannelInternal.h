/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaPhysicalHdlcChannelInternal.h
 * 
 * Created Date: Apr 7, 2016
 *
 * Description : Physical HDLC channel internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPHYSICALHDLCCHANNELINTERNAL_H_
#define _THAPHYSICALHDLCCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaHdlcChannelInternal.h"
#include "ThaPhysicalHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPhysicalHdlcChannelMethods
    {
    AtHdlcLink (*LinkCreate)(ThaPhysicalHdlcChannel self);
    eAtRet (*LinkActivate)(ThaPhysicalHdlcChannel self);
    eAtRet (*LinkDeactivate)(ThaPhysicalHdlcChannel self);
    uint32 (*HaRestore)(ThaPhysicalHdlcChannel self);
    }tThaPhysicalHdlcChannelMethods;

typedef struct tThaPhysicalHdlcChannel
    {
    tThaHdlcChannel super;
    const tThaPhysicalHdlcChannelMethods * methods;

    /* Private data */
    AtHdlcChannel logicalChannel;
    }tThaPhysicalHdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel ThaPhysicalHdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtHdlcChannel logicalChannel, AtModuleEncap module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPHYSICALHDLCCHANNELINTERNAL_H_ */


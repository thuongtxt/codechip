/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaDynamicHdlcLink.h
 * 
 * Created Date: Mar 29, 2016
 *
 * Description : Default dynamic HDLC link implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADYNAMICHDLCLINK_H_
#define _THADYNAMICHDLCLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mHdlcCacheValueSet(value)                              \
    do {                                                       \
        tThaDynamicHdlcLinkCache* cache = HdlcCacheGet((AtHdlcLink)self);\
        if (cache == NULL)                                     \
            return cAtErrorNullPointer;                        \
                                                               \
        cache->value = value;                                  \
        return cAtOk;                                          \
    }while(0)                                                  \

#define mHdlcCacheValueGet(value, invalidValue)                \
    do{                                                        \
        tThaDynamicHdlcLinkCache* cache = HdlcCacheGet((AtHdlcLink)self); \
        if (cache == NULL)                                     \
            return invalidValue;                               \
                                                               \
        return cache->value;                                   \
    }while(0)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaDynamicHdlcLinkCache
    {
    eBool compress;
    AtEthFlow flow;
    uint8 phase;
    uint16 pduType;
    }tThaDynamicHdlcLinkCache;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink ThaDynamicCiscoHdlcLinkNew(AtHdlcChannel channel);

eAtRet ThaDynamicPppLinkActivateConfiguration(AtHdlcLink self);
eAtRet ThaDynamicPppLinkConfigureCache(AtHdlcLink self);

eAtRet ThaDynamicCiscoHdlcLinkActivateConfiguration(AtHdlcLink self);
eAtRet ThaDynamicCiscoHdlcLinkConfigureCache(AtHdlcLink self);

eAtRet ThaDynamicFrLinkActivateConfiguration(AtHdlcLink self);
eAtRet ThaDynamicFrLinkConfigureCache(AtHdlcLink self);

AtHdlcLink ThaDynamicHdlcLinkPhysicalHdlcLinkGet(AtHdlcLink logicalLink);
AtHdlcLink ThaPhysicalHdlcLinkLogicLinkGet(AtHdlcLink physicalLink);

eBool ThaDynamicPppLinkLanFcsCacheGet(AtHdlcLink logicLink);

#ifdef __cplusplus
}
#endif
#endif /* _THADYNAMICHDLCLINK_H_ */


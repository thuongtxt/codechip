/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaPhysicalHdlcChannel.h
 * 
 * Created Date: Apr 1, 2016
 *
 * Description : Physical HDLC channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPHYSICALHDLCCHANNEL_H_
#define _THAPHYSICALHDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPhysicalHdlcChannel * ThaPhysicalHdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel ThaPhysicalHdlcChannelLogicalChannelGet(AtHdlcChannel physicalChannel);

uint32 ThaPhysicalHdlcChannelHaRestore(AtHdlcChannel self);
eAtRet ThaPhysicalHdlcChannelLinkActivate(ThaPhysicalHdlcChannel self);
eAtRet ThaPhysicalHdlcChannelLinkDeactivate(ThaPhysicalHdlcChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPHYSICALHDLCCHANNEL_H_ */


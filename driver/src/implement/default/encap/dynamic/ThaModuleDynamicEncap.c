/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaModuleEncapV2.c
 *
 * Created Date: Mar 18, 2016
 *
 * Description : Default ENCAP module V2 implementation. This version supports 
 *               dynamic ENCAP channel configuration.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEncapChannel.h"
#include "AtHdlcChannel.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../ppp/dynamic/ThaDynamicPppLink.h"
#include "../../framerelay/dynamic/ThaDynamicFrLink.h"
#include "../../framerelay/ThaMfrBundle.h"
#include "../profile/ThaProfileManager.h"
#include "../gfp/ThaGfpChannel.h"
#include "ThaModuleDynamicEncapInternal.h"
#include "ThaDynamicHdlcChannel.h"
#include "ThaDynamicHdlcLink.h"
#include "ThaPhysicalHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidLinkID      cBit31_0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleDynamicEncap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleDynamicEncapMethods m_methods;

/* Override */
static tAtModuleMethods         m_AtModuleOverride;
static tAtModuleEncapMethods    m_AtModuleEncapOverride;
static tAtObjectMethods         m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleEncapMethods  *m_AtModuleEncapMethods = NULL;
static const tAtModuleMethods       *m_AtModuleMethods = NULL;
static const tAtObjectMethods       *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHdlcChannel HdlcPppChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return ThaDynamicHdlcChannelNew(channelId, cAtHdlcFrmPpp, self);
    }

static AtPppLink PppLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    return (AtPppLink)ThaDynamicPppLinkNew(hdlcChannel);
    }

static AtHdlcLink FrLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    return (AtHdlcLink)ThaDynamicFrLinkNew(hdlcChannel);
    }

static AtHdlcChannel FrChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return ThaDynamicHdlcChannelNew(channelId, cAtHdlcFrmFr, self);
    }

static eAtRet ProfileManagerInit(AtModule self)
    {
    if (mMethodsGet(mThis(self))->ProfileManagerIsSupported(mThis(self)))
        return ThaProfileManagerInit(mThis(self)->profileManager);
    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    /* Super initialize */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ProfileManagerInit(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eBool ChannelIsBusy(AtModuleEncap self, uint32 channelId)
    {
    AtHdlcLink hdlcLink;
    AtEncapChannel encapChannel = AtModuleEncapChannelGet(self, (uint16)channelId);
    if (encapChannel == NULL)
        return cAtFalse;

    /* The current version just support HDLC encapsulation */
    if (AtEncapChannelEncapTypeGet(encapChannel) != cAtEncapHdlc)
        return cAtFalse;

    /* Check if this HDLC channel is carrying traffic. An encapsulation channel
     * is busy if its link is added to a bundle or it is carrying traffic of
     * Ethernet flow */
    hdlcLink = AtHdlcChannelHdlcLinkGet((AtHdlcChannel)encapChannel);
    if (AtHdlcLinkBundleGet(hdlcLink) || AtHdlcLinkBoundFlowGet(hdlcLink))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ChannelDelete(AtModuleEncap self, uint32 channelId)
    {
    if (ChannelIsBusy(self, channelId))
        return cAtErrorChannelBusy;

    return m_AtModuleEncapMethods->ChannelDelete(self, channelId);
    }

static void ProfileManagerDelete(AtModuleEncap self)
    {
    AtObjectDelete((AtObject)mThis(self)->profileManager);
    mThis(self)->profileManager = NULL;
    }

static void Delete(AtObject self)
    {
    ProfileManagerDelete((AtModuleEncap)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleDynamicEncap object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(profileManager);
    }

static AtEncapChannel PhysicalEncapChannelCreate(ThaModuleDynamicEncap self, uint32 channelId, AtEncapChannel logicalChannel)
    {
    uint8 encapType = AtEncapChannelEncapTypeGet(logicalChannel);

    if (encapType == cAtEncapHdlc)
        return (AtEncapChannel)ThaPhysicalHdlcChannelNew(channelId, (AtHdlcChannel)logicalChannel, (AtModuleEncap)self);

    return NULL;
    }

static AtHdlcChannel CiscoHdlcChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    return ThaDynamicHdlcChannelNew(channelId, cAtHdlcFrmCiscoHdlc, self);
    }

static AtHdlcLink CiscoHdlcLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    return (AtHdlcLink)ThaDynamicCiscoHdlcLinkNew(hdlcChannel);
    }

static uint32 EncapChannelHaRestore(AtEncapChannel encapChannel)
    {
    eAtEncapType encapType = AtEncapChannelEncapTypeGet(encapChannel);
    if (encapType == cAtEncapHdlc)
        return ThaDynamicHdlcChannelHaRestore((AtHdlcChannel)encapChannel);

    return 0;
    }

static uint32 Restore(AtModule self)
    {
    AtIterator iterator;
    AtEncapChannel encapChannel;
    uint32 remained = m_AtModuleMethods->Restore(self);

    iterator = AtModuleEncapChannelIteratorCreate((AtModuleEncap)self);
    while ((encapChannel = (AtEncapChannel)AtIteratorNext(iterator)) != NULL)
        remained = remained + EncapChannelHaRestore(encapChannel);

    AtObjectDelete((AtObject)iterator);

    return remained;
    }

static ThaProfileManager ProfileManagerCreate(AtModuleEncap self)
    {
    if (mThis(self)->profileManager)
        return mThis(self)->profileManager;

    mThis(self)->profileManager = mMethodsGet(mThis(self))->ProfileManagerObjectCreate(mThis(self));
    ThaProfileManagerSetup(mThis(self)->profileManager);

    return mThis(self)->profileManager;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if ((ret != cAtOk) || (!mMethodsGet(mThis(self))->ProfileManagerIsSupported(mThis(self))))
        return ret;

    if (ProfileManagerCreate((AtModuleEncap)self) == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static ThaProfileManager ProfileManagerObjectCreate(ThaModuleDynamicEncap self)
    {
    return ThaProfileManagerNew((AtModuleEncap)self);
    }

static eBool ProfileManagerIsSupported(ThaModuleDynamicEncap self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(ThaModuleDynamicEncap self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PhysicalEncapChannelCreate);
        mMethodOverride(m_methods, ProfileManagerObjectCreate);
        mMethodOverride(m_methods, ProfileManagerIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static AtMpBundle MpBundleObjectCreate(AtModuleEncap self, uint32 bundleId)
    {
    return ThaMpBundleNew(bundleId, (AtModule)self);
    }

static AtMfrBundle MfrBundleObjectCreate(AtModuleEncap self, uint32 bundleId)
    {
    return ThaMfrBundleNew(bundleId, self);
    }

static uint32 MaxBundlesGet(AtModuleEncap self)
    {
    AtUnused(self);
    return 32;
    }

static void OverrideAtModuleEncap(ThaModuleDynamicEncap self)
    {
    AtModuleEncap encapModule = (AtModuleEncap)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEncapMethods = mMethodsGet(encapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, m_AtModuleEncapMethods, sizeof(m_AtModuleEncapOverride));
        mMethodOverride(m_AtModuleEncapOverride, HdlcPppChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, FrChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, PppLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, ChannelDelete);
        mMethodOverride(m_AtModuleEncapOverride, FrLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, MfrBundleObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, MpBundleObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, MaxBundlesGet);
        }

    mMethodsSet(encapModule, &m_AtModuleEncapOverride);
    }

static void OverrideAtModule(ThaModuleDynamicEncap self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Restore);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(ThaModuleDynamicEncap self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaModuleDynamicEncap self)
    {
    OverrideAtObject(self);	
    OverrideAtModule(self);
    OverrideAtModuleEncap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleDynamicEncap);
    }

AtModuleEncap ThaModuleDynamicEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap ThaModuleDynamicEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return ThaModuleDynamicEncapObjectInit(newModule, device);
    }

ThaProfileManager ThaModuleEncapProfileManagerGet(AtModuleEncap self)
    {
    if (self)
        return mThis(self)->profileManager;
    return NULL;
    }

uint16 ThaModuleEncapPduType2NetworkProfileType(eAtHdlcPduType pduType)
    {
    if (pduType == cAtHdlcPduTypeEthernet) return cThaProfileNetworkTypeBcp;
    if (pduType == cAtHdlcPduTypeIPv4)     return cThaProfileNetworkTypeIpv4;
    if (pduType == cAtHdlcPduTypeIPv6)     return cThaProfileNetworkTypeIpv6;

    return cThaProfileNetworkTypeAll;
    }

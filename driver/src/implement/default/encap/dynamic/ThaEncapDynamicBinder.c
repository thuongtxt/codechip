/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : ThaEncapDynamicBinder.c
 *
 * Created Date: Apr 4, 2016
 *
 * Description : Dynamic encap binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtPdhNxDs0.h"
#include "AtHdlcChannel.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../binder/ThaEncapBinderInternal.h"
#include "../../ocn/ThaModuleOcn.h"
#include "../../pdh/ThaPdhDe2De1.h"
#include "../../map/ThaModuleAbstractMap.h"
#include "../../man/ThaDevice.h"
#include "ThaEncapDynamicBinder.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaEncapDynamicBinder)self)
#define cChannelIdMask  cBit28_0
#define cChannelIdShift 0
#define cIsHoMask       cBit29
#define cIsHoShift      29
#define cSliceIdMask    cBit31_30
#define cSliceIdShift   30

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaEncapDynamicBinderMethods m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtEncapBinderMethods  m_AtEncapBinderOverride;
static tThaEncapBinderMethods m_ThaEncapBinderOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;
static const tAtEncapBinderMethods *m_AtEncapBinderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DeletePools(AtEncapBinder self)
    {
    AtOsal osal;
    uint8 pool_i;
    uint8 numPool = mThis(self)->numPool;

    if (mThis(self)->channelPools == NULL)
        return;

    /* Delete mask for each part */
    for (pool_i = 0; pool_i < numPool; pool_i++)
        AtObjectDelete((AtObject)mThis(self)->channelPools[pool_i]);

    /* And the memory that hold all masks of all part */
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, mThis(self)->channelPools);
    mThis(self)->channelPools = NULL;
    }

static ThaBitMask* AllPools(AtEncapBinder self)
    {
    uint32 memSize;
    uint8 numPool = mThis(self)->numPool;

    if (mThis(self)->channelPools)
        return mThis(self)->channelPools;

    memSize = numPool * sizeof(ThaBitMask);
    mThis(self)->channelPools = AtOsalMemAlloc(memSize);
    if (mThis(self)->channelPools == NULL)
        return NULL;

    AtOsalMemInit(mThis(self)->channelPools, 0, memSize);
    return mThis(self)->channelPools;
    }

static eBool SliceIsValid(AtEncapBinder self, uint8 slice)
    {
    return (slice < mThis(self)->numPool) ? cAtTrue : cAtFalse;
    }

static ThaBitMask ChannelPoolCreate(ThaEncapDynamicBinder self, uint8 slice)
    {
    AtUnused(slice);
    return ThaBitMaskNew(mThis(self)->numChannelPerPool);
    }

static ThaBitMask PoolBySlice(AtEncapBinder self, uint8 slice)
    {
    ThaBitMask* allPools = AllPools(self);
    if ((allPools == NULL) || (!SliceIsValid(self, slice)))
        return NULL;

    if (allPools[slice] == NULL)
        allPools[slice] = mMethodsGet(mThis(self))->ChannelPoolCreate(mThis(self), slice);

    return allPools[slice];
    }

static uint32 LoLineVcFlatId(AtChannel channel)
    {
    uint8 hwSlice, hwSts;
    AtSdhChannel sdhChannel = (AtSdhChannel)channel;
    if (ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModuleEncap, &hwSlice, &hwSts) == cAtOk)
        return (hwSts * 28UL) + (AtSdhChannelTug2Get(sdhChannel) * 4UL) + AtSdhChannelTu1xGet(sdhChannel);

    return cInvalidUint32;
    }

static eBool IsDe2(AtPdhChannel channel)
    {
    eAtPdhChannelType channelType;

    if (channel == NULL)
        return cAtFalse;

    channelType = AtPdhChannelTypeGet(channel);
    if ((channelType == cAtPdhChannelTypeE2) || (channelType == cAtPdhChannelTypeDs2))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PdhChannelFlatId(AtPdhChannel pdhChannel)
    {
    uint8 slice, hwId, hwDe2Id, hwDe1Id;
    AtChannel sdhChannel = (AtChannel)AtPdhChannelVcInternalGet(pdhChannel);

    if (sdhChannel)
        return LoLineVcFlatId(sdhChannel);

    /* Should be DE2 DE1 */
    if (!IsDe2(AtPdhChannelParentChannelGet(pdhChannel)))
        return cInvalidUint32;

    if (ThaPdhDe2De1HwIdGet((ThaPdhDe1)pdhChannel, &slice, &hwId, &hwDe2Id, &hwDe1Id, cAtModuleEncap) == cAtOk)
        return ((hwId * 28UL) + (hwDe2Id * 4UL) + hwDe1Id);

    return cInvalidUint32;
    }

static void EncapHwIdDeallocate(AtEncapBinder self, uint32 hwId)
    {
    uint32 channelId = mRegField(hwId, cChannelId);
    uint8 slice = (uint8)mRegField(hwId, cSliceId);
    ThaBitMask pool;

    if (mRegField(hwId, cIsHo))
        return;

    pool = PoolBySlice(self, slice);
    if (pool == NULL)
        return;

    if (ThaBitMaskBitVal(pool, channelId) == 1)
        ThaBitMaskClearBit(pool, channelId);
    }

static uint32 LoEncapChannelHwIdGet(AtEncapBinder self, uint32 selectedValue, uint8 slice)
    {
    uint32 freeId;

    if (AtDeviceAccessible(AtEncapBinderDeviceGet(self)))
        {
        ThaBitMask pool = PoolBySlice(self, slice);
        if (pool == NULL)
            return cInvalidUint32;

        if ((selectedValue != cInvalidUint32) && (ThaBitMaskBitVal(pool, selectedValue) == 0))
            freeId = selectedValue;
        else
            freeId = ThaBitMaskFirstZeroBit(pool);

        /* Use a valid one */
        if (!ThaBitMaskBitPositionIsValid(pool, freeId))
            return cInvalidUint32;

        ThaBitMaskSetBit(pool, freeId);
        }
    else
        {
        freeId = mThis(self)->numChannelPerPool;
        }

    /* Embed slice and isHo to return value */
    mRegFieldSet(freeId, cIsHo, 0);
    mRegFieldSet(freeId, cSliceId, slice);

    return freeId;
    }

static uint32 De1EncapHwIdAllocate(AtEncapBinder self, AtPdhDe1 de1)
    {
    uint8 hwId, slice;
    uint32 selectedValue;
    eAtRet ret = ThaPdhChannelHwIdGet((AtPdhChannel)de1, cAtModuleEncap, &slice, &hwId);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)de1, cAtLogLevelCritical, __FILE__, __LINE__, "Can not get HW ID\r\n");
        return cInvalidUint32;
        }

    selectedValue = PdhChannelFlatId((AtPdhChannel)de1);
    return LoEncapChannelHwIdGet(self, selectedValue, slice);
    }

static uint32 De3EncapHwIdAllocate(AtEncapBinder self, AtPdhDe3 de3)
    {
    uint8 hwId, slice;
    uint32 selectedValue;
    eAtRet ret = ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModuleEncap, &slice, &hwId);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)de3, cAtLogLevelCritical, __FILE__, __LINE__, "Can not get HW ID\r\n");
        return cInvalidUint32;
        }

    selectedValue = PdhChannelFlatId((AtPdhChannel)de3);
    return LoEncapChannelHwIdGet(self, selectedValue, slice);
    }

static uint32 NxDS0EncapHwIdAllocate(AtEncapBinder self, AtPdhNxDS0 ds0)
    {
    uint8 hwId, slice;
    eAtRet ret = ThaPdhChannelHwIdGet((AtPdhChannel)AtPdhNxDS0De1Get(ds0), cAtModuleEncap, &slice, &hwId);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)ds0, cAtLogLevelCritical, __FILE__, __LINE__, "Can not get HW ID\r\n");
        return cInvalidUint32;
        }

    return LoEncapChannelHwIdGet(self, cInvalidUint32, slice);
    }

static uint32 AuVcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc auvc)
    {
    uint8 hwSts, slice;
    uint32 hwId = 0;

    AtUnused(self);
    ThaSdhChannel2HwMasterStsId((AtSdhChannel)auvc, cAtModuleEncap, &slice, &hwSts);

    /* Embed slice and isHo to return value */
    mRegFieldSet(hwId, cChannelId, hwSts);
    mRegFieldSet(hwId, cIsHo, 1);
    mRegFieldSet(hwId, cSliceId, slice);

    return hwId;
    }

static uint32 LoVcEncapChannelHwIdGet(AtEncapBinder self, AtSdhVc vc)
    {
    uint8 hwSlice, hwSts;
    AtSdhChannel sdhChannel = (AtSdhChannel)vc;
    uint32 selectedValue;

    eAtRet ret = ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModuleEncap, &hwSlice, &hwSts);
    if (ret != cAtOk)
        {
        AtChannelLog((AtChannel)vc, cAtLogLevelCritical, __FILE__, __LINE__, "Can not get HW ID\r\n");
        return cInvalidUint32;
        }

    selectedValue = (hwSts * 28UL) + (AtSdhChannelTug2Get(sdhChannel) * 4UL) + AtSdhChannelTu1xGet(sdhChannel);
    return LoEncapChannelHwIdGet(self, selectedValue, hwSlice);
    }

static uint32 Vc1xEncapHwIdAllocate(AtEncapBinder self, AtSdhVc vc1x)
    {
    return LoVcEncapChannelHwIdGet(self, vc1x);
    }

static uint32 Tu3VcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc tu3vc)
    {
    return LoVcEncapChannelHwIdGet(self, tu3vc);
    }

static void Delete(AtObject self)
    {
    DeletePools((AtEncapBinder)self);
    AtOsalMemFree(self);
    }

static eBool ShouldAllocateHwBlockOnBinding(ThaEncapBinder self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaEncapDynamicBinder object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(channelPools, object->numPool);
    mEncodeUInt(numChannelPerPool);
    }

static void MethodsInit(AtEncapBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ChannelPoolCreate);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtObject(AtEncapBinder self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(tAtObjectMethods));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEncapBinder(AtEncapBinder self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapBinderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapBinderOverride, mMethodsGet(self), sizeof(m_AtEncapBinderOverride));

        mMethodOverride(m_AtEncapBinderOverride, De1EncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, De3EncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, NxDS0EncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, Vc1xEncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, Tu3VcEncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, AuVcEncapHwIdAllocate);
        mMethodOverride(m_AtEncapBinderOverride, EncapHwIdDeallocate);
        }

    mMethodsSet(self, &m_AtEncapBinderOverride);
    }

static void OverrideThaEncapBinder(AtEncapBinder self)
    {
    ThaEncapBinder binder = (ThaEncapBinder)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEncapBinderOverride, mMethodsGet(binder), sizeof(m_ThaEncapBinderOverride));

        mMethodOverride(m_ThaEncapBinderOverride, ShouldAllocateHwBlockOnBinding);
        }

    mMethodsSet(binder, &m_ThaEncapBinderOverride);
    }

static void Override(AtEncapBinder self)
    {
    OverrideAtObject(self);
    OverrideAtEncapBinder(self);
    OverrideThaEncapBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaEncapDynamicBinder);
    }

AtEncapBinder ThaEncapDynamicBinderObjectInit(AtEncapBinder self, AtDevice device, uint8 numPool, uint32 numChannelPerPool)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEncapBinderObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    mThis(self)->numPool = numPool;
    mThis(self)->numChannelPerPool = numChannelPerPool;
    return self;
    }

AtEncapBinder ThaEncapDynamicBinderNew(uint8 numPool, AtDevice device, uint32 numChannelPerPool)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEncapBinder newBinder = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBinder == NULL)
        return NULL;

    /* Construct it */
    return ThaEncapDynamicBinderObjectInit(newBinder, device, numPool, numChannelPerPool);
    }

uint8 ThaEncapDynamicBinderHwSlice(ThaEncapDynamicBinder self, uint32 hwId)
    {
    AtUnused(self);
    return (uint8)mRegField(hwId, cSliceId);
    }

eBool ThaEncapDynamicBinderHwIsHo(ThaEncapDynamicBinder self, uint32 hwId)
    {
    AtUnused(self);
    return (eBool)(mRegField(hwId, cIsHo) ? cAtTrue : cAtFalse);
    }

uint32 ThaEncapDynamicBinderHwChannelId(ThaEncapDynamicBinder self, uint32 hwId)
    {
    AtUnused(self);
    return mRegField(hwId, cChannelId);
    }

uint32 ThaEncapDynamicBinderCompressedHwId(ThaEncapDynamicBinder self, uint32 channelId, uint8 slice, eBool isHo)
    {
    uint32 hwId = 0;
    AtUnused(self);
    mRegFieldSet(hwId, cChannelId, channelId);
    mRegFieldSet(hwId, cIsHo, isHo);
    mRegFieldSet(hwId, cSliceId, slice);
    return hwId;
    }

void ThaEncapDynamicBinderChannelUse(ThaEncapDynamicBinder self, uint8 slice, uint32 channelId)
    {
    ThaBitMask pool = PoolBySlice((AtEncapBinder)self, slice);
    ThaBitMaskSetBit(pool, channelId);
    }

uint32 ThaEncapDynamicBinderLoVcEncapChannelHwIdGet(AtEncapBinder self, AtSdhVc vc)
    {
    if (self && vc)
        return LoVcEncapChannelHwIdGet(self, vc);
    return cInvalidUint32;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : ThaDynamicHdlcChannel.c
 *
 * Created Date: Mar 23, 2016
 *
 * Description : Default dynamic HDLC channel implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../pw/adapters/ThaPwAdapterInternal.h"
#include "../../pw/adapters/ThaPwAdapterInternal.h"
#include "../../pda/ThaModulePda.h"
#include "../ThaHdlcChannelInternal.h"
#include "ThaDynamicHdlcChannel.h"
#include "ThaModuleDynamicEncapInternal.h"
#include "ThaDynamicHdlcLink.h"
#include "ThaPhysicalHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#define cSupportedMtu 9600

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaDynamicHdlcChannel*)self)

#define mCacheValueSet(value)                                  \
    do {                                                       \
        tThaDynamicHdlcChannelCache* cache = CacheGet((AtHdlcChannel)self);   \
        if (cache == NULL)                                     \
            return cAtErrorNullPointer;                        \
                                                               \
        cache->value = value;                                  \
        return cAtOk;                                          \
    }while(0)                                                  \

#define mCacheValueGet(value, invalidValue)                    \
    do{                                                        \
        tThaDynamicHdlcChannelCache* cache = CacheGet((AtHdlcChannel)self); \
        if (cache == NULL)                                     \
            return invalidValue;                               \
                                                               \
        return cache->value;                                   \
    }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaDynamicHdlcChannelCache
    {
    /* Control */
    eBool controlInsertEnable;
    uint8 insertedControl;
    eBool controlCompareEnable;
    uint8 expectedControl;

    /* Address */
    eBool addressInsertEnable;
    uint8 insertedAddress;
    eBool addressCompareEnable;
    uint8 expectedAddress;
    eBool rxAddrCtrlCompress;

    /* FCS */
    eAtHdlcFcsMode fcsMode;
    eAtHdlcStuffMode stuffMode;
    eAtHdlcFcsCalculationMode fcsCalculationMode;

    eBool scrambleEnable;
    uint8 idlePattern;
    uint8 addressSize;
    uint8 frameType;
    }tThaDynamicHdlcChannelCache;

typedef struct tThaDynamicHdlcChannel
    {
    tThaHdlcChannel super;

    /* Private data. */
    tThaDynamicHdlcChannelCache *cache;
    AtHdlcChannel physicalChannel;
    }tThaDynamicHdlcChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;
static tAtHdlcChannelMethods  m_AtHdlcChannelOverride;
static tThaHdlcChannelMethods m_ThaHdlcChannelOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;

static const char * cThaDynamicHdlcFcsModeStr[] ={"No-FCS", "FCS-16", "FCS-16"};
static const eAtHdlcFcsMode cThaDynamicHdlcFcsModeVal[]={cAtHdlcFcsModeNoFcs, cAtHdlcFcsModeFcs16, cAtHdlcFcsModeFcs32};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CacheSize(void)
    {
    return sizeof(tThaDynamicHdlcChannelCache);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)self;
    uint8 defaulAddress = 0x4;
    uint8 defaulControl = 0x3;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);

    ret |= AtHdlcChannelTxAddressSet(hdlcChannel, &defaulAddress);
    ret |= AtHdlcChannelExpectedAddressSet(hdlcChannel, &defaulAddress);
    ret |= AtHdlcChannelAddressInsertionEnable(hdlcChannel, cAtTrue);
    ret |= AtHdlcChannelAddressMonitorEnable(hdlcChannel, cAtTrue);

    ret |= AtHdlcChannelTxControlSet(hdlcChannel, &defaulControl);
    ret |= AtHdlcChannelExpectedControlSet(hdlcChannel, &defaulControl);
    ret |= AtHdlcChannelControlInsertionEnable(hdlcChannel, cAtTrue);
    ret |= AtHdlcChannelControlMonitorEnable(hdlcChannel, cAtTrue);

    ret |= AtHdlcChannelScrambleEnable(hdlcChannel, cAtTrue);
    ret |= AtHdlcChannelIdlePatternSet(hdlcChannel, 0x7E);
    ret |= AtHdlcChannelStuffModeSet(hdlcChannel, cAtHdlcStuffByte);

    return ret;
    }

static tThaDynamicHdlcChannelCache * CacheGet(AtHdlcChannel self)
    {
    if (mThis(self)->cache == NULL)
        {
        tThaDynamicHdlcChannelCache *cache = AtOsalMemAlloc(CacheSize());
        if (cache == NULL)
            return NULL;

        AtOsalMemInit(cache, 0, CacheSize());
        mThis(self)->cache = cache;
        }

    return mThis(self)->cache;
    }

static void CacheDelete(AtHdlcChannel self)
    {
    AtOsalMemFree(mThis(self)->cache);
    mThis(self)->cache = NULL;
    }

static eBool IsLogical(AtHdlcChannel self)
    {
    return AtEncapChannelBoundPhyGet((AtEncapChannel)self) ? cAtFalse : cAtTrue;
    }

static AtHdlcChannel PhysicalEncapChannelGet(AtHdlcChannel self)
    {
    ThaHdlcChannel hdlcChannel = (ThaHdlcChannel)self;
    return (AtHdlcChannel)mMethodsGet(hdlcChannel)->PhysicalChannelGet(hdlcChannel);
    }

static AtEncapBinder EncapBinder(AtHdlcChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceEncapBinder(device);
    }

static AtHdlcChannel PhysicalEncapChannelCreate(AtHdlcChannel self, AtChannel phyChannel)
    {
    ThaModuleDynamicEncap encapModule;
    uint32 phyEncapId;

    if (mThis(self)->physicalChannel)
        return mThis(self)->physicalChannel;

    encapModule = (ThaModuleDynamicEncap)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleEncap);
    phyEncapId = AtChannelEncapHwIdAllocate(phyChannel);

    mThis(self)->physicalChannel = (AtHdlcChannel)mMethodsGet(encapModule)->PhysicalEncapChannelCreate(encapModule, phyEncapId, (AtEncapChannel)self);
    return mThis(self)->physicalChannel;
    }

static void PhysicalEncapChannelDelete(AtHdlcChannel self)
    {
    if (mThis(self)->physicalChannel == NULL)
        return;

    AtEncapBinderEncapHwIdDeallocate(EncapBinder(self), AtChannelIdGet((AtChannel)(mThis(self)->physicalChannel)));
    AtObjectDelete((AtObject)(mThis(self)->physicalChannel));
    mThis(self)->physicalChannel = NULL;
    }

static void Delete(AtObject self)
    {
    CacheDelete((AtHdlcChannel)self);
    PhysicalEncapChannelDelete((AtHdlcChannel)self);
    AtChannelBoundPwObjectDelete((AtChannel)self);

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        return 0;

    return AtChannelDefectGet((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        return 0;

    return AtChannelAlarmInterruptGet((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        return 0;

    return AtChannelAlarmInterruptClear((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (IsLogical((AtHdlcChannel)self))
        return cAtOk;

    return AtChannelInterruptMaskSet((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self), defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        return 0;

    return AtChannelInterruptMaskGet((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (IsLogical((AtHdlcChannel)self))
        return 0;

    return AtChannelCounterGet((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self), counterType);
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    if (IsLogical((AtHdlcChannel)self))
        return cAtModuleEncapErrorNoBoundChannel;

    return AtChannelAllCountersGet((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self), pAllCounters);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    if (IsLogical((AtHdlcChannel)self))
        return cAtModuleEncapErrorNoBoundChannel;

    return AtChannelAllCountersClear((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self), pAllCounters);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (IsLogical((AtHdlcChannel)self))
        return 0;

    return AtChannelCounterClear((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self), counterType);
    }

static eBool ShouldApplyHwConfiguration(AtEncapChannel self)
    {
    /* If cache is existent, it means user has some configuration that needs to
     * be applied to hardware right now. */
    return (mThis(self)->cache) ? cAtTrue : cAtFalse;
    }

static eAtRet HwConfigurationApply(AtHdlcChannel channel)
    {
    eAtRet ret = cAtOk;
    tThaDynamicHdlcChannelCache* cache = CacheGet(channel);

    if (cache == NULL)
        return cAtErrorNullPointer;

    /* Address */
    ret |= AtHdlcChannelExpectedAddressSet(channel, &cache->expectedAddress);
    ret |= AtHdlcChannelAddressMonitorEnable(channel, cache->addressCompareEnable);
    ret |= AtHdlcChannelTxAddressSet(channel, &cache->insertedAddress);
    ret |= AtHdlcChannelAddressInsertionEnable(channel, cache->addressInsertEnable);
    ret |= AtHdlcChannelRxAddressControlCompress(channel, cache->rxAddrCtrlCompress);

    /* Control */
    ret |= AtHdlcChannelExpectedControlSet(channel, &cache->expectedControl);
    ret |= AtHdlcChannelControlMonitorEnable(channel, cache->controlCompareEnable);
    ret |= AtHdlcChannelTxControlSet(channel, &cache->insertedControl);
    ret |= AtHdlcChannelControlInsertionEnable(channel, cache->controlInsertEnable);

    /* Others */
    ret |= AtHdlcChannelIdlePatternSet(channel, cache->idlePattern);
    ret |= AtHdlcChannelStuffModeSet(channel, cache->stuffMode);
    ret |= AtHdlcChannelFcsModeSet(channel, cache->fcsMode);
    ret |= AtHdlcChannelScrambleEnable(channel, cache->scrambleEnable);

    return ret;
    }

static eBool ShouldRecoverHwConfiguration(AtEncapChannel self)
    {
    /* If physical ENCAP channel is existent, it means user has some configuration
     * in hardware that needs to be saved in cache before unbinding process. */
    return (mThis(self)->physicalChannel) ? cAtTrue : cAtFalse;
    }

static eAtRet HwConfigurationCache(AtHdlcChannel physicalChannel)
    {
    tThaDynamicHdlcChannelCache* cache = CacheGet(physicalChannel);

    if (cache == NULL)
        return cAtErrorNullPointer;

    /* Address */
    cache->addressCompareEnable = AtHdlcChannelAddressMonitorIsEnabled(physicalChannel);
    cache->addressInsertEnable  = AtHdlcChannelAddressInsertionIsEnabled(physicalChannel);
    AtHdlcChannelExpectedAddressGet(physicalChannel, &cache->expectedAddress);
    AtHdlcChannelTxAddressGet(physicalChannel, &cache->insertedAddress);

    /* Control */
    cache->controlCompareEnable = AtHdlcChannelAddressMonitorIsEnabled(physicalChannel);
    cache->controlInsertEnable  = AtHdlcChannelAddressInsertionIsEnabled(physicalChannel);
    AtHdlcChannelExpectedControlGet(physicalChannel, &cache->expectedControl);
    AtHdlcChannelTxControlGet(physicalChannel, &cache->insertedControl);

    /* Others */
    cache->idlePattern    = AtHdlcChannelIdlePatternGet(physicalChannel);
    cache->stuffMode      = AtHdlcChannelStuffModeGet(physicalChannel);
    cache->fcsMode        = AtHdlcChannelFcsModeGet(physicalChannel);
    cache->scrambleEnable = AtHdlcChannelScrambleIsEnabled(physicalChannel);
    cache->rxAddrCtrlCompress = AtHdlcChannelRxAddressControlIsCompressed(physicalChannel);

    return cAtOk;
    }

static eAtRet PhyBind(AtEncapChannel self, AtChannel phyChannel)
    {
    eAtRet ret = cAtOk;
    AtChannel boundPhyChannel;
    AtHdlcChannel physicalHdlc;

    /* Get current bound physical channel */
    boundPhyChannel = AtEncapChannelBoundPhyGet(self);

    if (AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    /* Additional binding */
    /* Bind */
    if (phyChannel)
        {
        /* If this channel is already bound to this physical interface, do nothing */
        if (boundPhyChannel == phyChannel)
            return cAtOk;

        /* This encap channel is busy with another physical channel */
        if (boundPhyChannel != NULL)
            return cAtErrorChannelBusy;

        physicalHdlc = PhysicalEncapChannelCreate((AtHdlcChannel)self, phyChannel);
        if (physicalHdlc == NULL)
            return cAtErrorRsrcNoAvail;

        AtEncapChannelPhysicalChannelSet(self, phyChannel);
        if (ShouldApplyHwConfiguration(self))
            {
            /* Get configuration cache and apply them into hardware. */
            ret |= HwConfigurationApply((AtHdlcChannel)self);
            CacheDelete((AtHdlcChannel)self);
            }

        /* Call super to deal with database */
        ret |= mMethodsGet((AtChannel)phyChannel)->BindToEncapChannel(phyChannel, self);
        ret |= ThaPhysicalHdlcChannelLinkActivate((ThaPhysicalHdlcChannel)physicalHdlc);
        ret |= AtChannelHwResourceAllocate(phyChannel);
        }

    /* Unbind */
    else
        {
        /* This encap channel is currently free, do nothing */
        if (boundPhyChannel == NULL)
            return cAtOk;

        /* Deactivate link of channel */
        ret = ThaPhysicalHdlcChannelLinkDeactivate((ThaPhysicalHdlcChannel)mThis(self)->physicalChannel);
        if (ret != cAtOk)
            return ret;

        if (ShouldRecoverHwConfiguration(self))
            {
            /* Unbinding physical channel soon to recover hardware configuration
             * to cache. */
            ret = HwConfigurationCache((AtHdlcChannel)self);
            AtChannelHwResourceDeallocate((AtChannel)boundPhyChannel);
            PhysicalEncapChannelDelete((AtHdlcChannel)self);

            AtEncapChannelPhysicalChannelSet(self, NULL);
            }

        ret |= mMethodsGet(boundPhyChannel)->BindToEncapChannel(boundPhyChannel, NULL);
        }

    return ret;
    }

static eAtEncapType EncapTypeGet(AtEncapChannel self)
    {
	AtUnused(self);
    return cAtEncapHdlc;
    }

static eAtRet AddressSizeSet(AtHdlcChannel self, uint8 addressSize)
    {
    tThaDynamicHdlcChannelCache *cache;

    if (AtHdlcChannelFrameTypeGet(self) != cAtHdlcFrmFr)
        return (addressSize == 1) ? cAtOk : cAtErrorModeNotSupport; /* Do nothing, HW just support 1 byte address */

    cache = CacheGet(self);
    if (!cache)
        return cAtErrorNullPointer;

    /* Just updated for pass simulated, when hw available need remove it */
    cache->addressSize = addressSize;
    return cAtOk;
    }

static uint8 AddressSizeGet(AtHdlcChannel self)
    {
    tThaDynamicHdlcChannelCache *cache;

    if (AtHdlcChannelFrameTypeGet(self) != cAtHdlcFrmFr)
        return 1;

    cache = CacheGet(self);
    if (!cache)
        return 0;

    return cache->addressSize;
    }

static eAtRet AddressMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        cache->addressCompareEnable = compareEnable;

        return cAtOk;
        }

    return AtHdlcChannelAddressMonitorEnable(PhysicalEncapChannelGet(self), compareEnable);
    }

static eAtRet ExpectedAddressSet(AtHdlcChannel self, uint8 * expectedAddress)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        if (expectedAddress != NULL)
            cache->expectedAddress = expectedAddress[0];
        return cAtOk;
        }

    return AtHdlcChannelExpectedAddressSet(PhysicalEncapChannelGet(self), expectedAddress);
    }

static eBool AddressMonitorIsEnabled(AtHdlcChannel self)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        return (eBool)((cache) ? cache->addressCompareEnable : cAtFalse);
        }

    return AtHdlcChannelAddressMonitorIsEnabled(PhysicalEncapChannelGet(self));
    }

static eAtRet ExpectedAddressGet(AtHdlcChannel self, uint8 *address)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        if (address != NULL)
            address[0] = (uint8)((cache) ? cache->expectedAddress : 0);

        return cAtOk;
        }

    return AtHdlcChannelExpectedAddressGet(PhysicalEncapChannelGet(self), address);
    }

static eAtRet AddressInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        cache->addressInsertEnable = insertEnable;

        return cAtOk;
        }

    return AtHdlcChannelAddressInsertionEnable(PhysicalEncapChannelGet(self), insertEnable);
    }

static eAtRet TxAddressSet(AtHdlcChannel self, uint8 *address)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        if (address != NULL)
            cache->insertedAddress = address[0];
        return cAtOk;
        }

    return AtHdlcChannelTxAddressSet(PhysicalEncapChannelGet(self), address);
    }

static eBool AddressInsertionIsEnabled(AtHdlcChannel self)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        return (eBool)((cache) ? cache->addressInsertEnable : cAtFalse);
        }

    return AtHdlcChannelAddressInsertionIsEnabled(PhysicalEncapChannelGet(self));
    }

static eAtRet TxAddressGet(AtHdlcChannel self, uint8 *insertedAddress)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        if (insertedAddress != NULL)
            insertedAddress[0] = (uint8)((cache) ? cache->insertedAddress : 0);

        return cAtOk;
        }

    return AtHdlcChannelTxAddressGet(PhysicalEncapChannelGet(self), insertedAddress);
    }

static eAtRet ControlSizeSet(AtHdlcChannel self, uint8 controlSize)
    {
	AtUnused(self);
    return (controlSize == 1) ?  cAtOk : cAtErrorModeNotSupport;
    }

static uint8 ControlSizeGet(AtHdlcChannel self)
    {
	AtUnused(self);
    return 1;
    }

static eAtRet ControlMonitorEnable(AtHdlcChannel self, eBool compareEnable)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        cache->controlCompareEnable = compareEnable;

        return cAtOk;
        }

    return AtHdlcChannelControlMonitorEnable(PhysicalEncapChannelGet(self), compareEnable);
    }

static eAtRet ExpectedControlSet(AtHdlcChannel self, uint8 *expectedControl)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        if (expectedControl != NULL)
            cache->expectedControl = expectedControl[0];

        return cAtOk;
        }

    return AtHdlcChannelExpectedControlSet(PhysicalEncapChannelGet(self), expectedControl);
    }

static eBool ControlMonitorIsEnabled(AtHdlcChannel self)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        return (eBool)((cache) ? cache->controlCompareEnable : cAtFalse);
        }

    return AtHdlcChannelControlMonitorIsEnabled(PhysicalEncapChannelGet(self));
    }

static eAtRet ExpectedControlGet(AtHdlcChannel self, uint8 *control)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        if (control != NULL)
            control[0] = (uint8)((cache) ? cache->expectedControl : 0);

        return cAtOk;
        }

    return AtHdlcChannelExpectedControlGet(PhysicalEncapChannelGet(self), control);
    }

static eAtRet ControlInsertionEnable(AtHdlcChannel self, eBool insertEnable)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        cache->controlInsertEnable = insertEnable;

        return cAtOk;
        }

    return AtHdlcChannelControlInsertionEnable(PhysicalEncapChannelGet(self), insertEnable);
    }

static eAtRet TxControlSet(AtHdlcChannel self, uint8 *control)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);
        if (!cache)
            return cAtErrorNullPointer;

        if (control != NULL)
            cache->insertedControl = control[0];

        return cAtOk;
        }

    return AtHdlcChannelTxControlSet(PhysicalEncapChannelGet(self), control);
    }

static eBool ControlInsertionIsEnabled(AtHdlcChannel self)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        return (uint8)((cache) ? cache->controlInsertEnable : cAtFalse);
        }

    return AtHdlcChannelControlInsertionIsEnabled(PhysicalEncapChannelGet(self));
    }

static eAtRet TxControlGet(AtHdlcChannel self, uint8 *insertedControl)
    {
    if (IsLogical(self))
        {
        tThaDynamicHdlcChannelCache *cache = CacheGet(self);

        if (insertedControl != NULL)
            insertedControl[0] = (uint8)((cache) ? cache->insertedControl : 0);

        return cAtOk;
        }

    return AtHdlcChannelTxControlGet(PhysicalEncapChannelGet(self), insertedControl);
    }

static eAtRet FlagSet(AtHdlcChannel self, uint8 flag)
    {
    if (flag != 0x7E)
        return cAtErrorModeNotSupport;

    AtUnused(self);
    return cAtOk;
    }

static uint8 FlagGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 0x7E;
    }

static eBool FcsModeIsValid(eAtHdlcFcsMode fcsMode)
    {
    return (fcsMode == cAtHdlcFcsModeNoFcs) ||
           (fcsMode == cAtHdlcFcsModeFcs16) ||
           (fcsMode == cAtHdlcFcsModeFcs32);
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    if (!FcsModeIsValid(fcsMode))
        return cAtErrorModeNotSupport;

    if (IsLogical(self))
        mCacheValueSet(fcsMode);

    return AtHdlcChannelFcsModeSet(PhysicalEncapChannelGet(self), fcsMode);
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
    if (IsLogical(self))
        mCacheValueGet(fcsMode, cAtHdlcFcsModeNoFcs);

    return AtHdlcChannelFcsModeGet(PhysicalEncapChannelGet(self));
    }

static eBool StuffModeIsValid(eAtHdlcStuffMode stuffMode)
    {
    return ((stuffMode == cAtHdlcStuffBit) ||
           (stuffMode == cAtHdlcStuffByte));
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    if (!StuffModeIsValid(stuffMode))
        return cAtErrorModeNotSupport;

    if (IsLogical(self))
        mCacheValueSet(stuffMode);

    return AtHdlcChannelStuffModeSet(PhysicalEncapChannelGet(self), stuffMode);
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
    if (IsLogical(self))
        mCacheValueGet(stuffMode, cAtHdlcStuffBit);

    return AtHdlcChannelStuffModeGet(PhysicalEncapChannelGet(self));
    }

static eAtRet MtuSet(AtHdlcChannel self, uint32 mtu)
    {
    AtUnused(self);
    /* Only MTU 9600 is supported */
    if (mtu == cSupportedMtu)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint32 MtuGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return cSupportedMtu;
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    AtUnused(self);
    AtUnused(frameType);

    /* Frame type is decided when channel is created, do not support to change */
    return cAtErrorModeNotSupport;
    }

static eAtRet AddressControlBypass(AtHdlcChannel self, eBool bypass)
    {
    AtUnused(self);
    return bypass ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AddressControlIsBypassed(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AddressControlErrorBypass(AtHdlcChannel self, eBool bypass)
    {
    AtUnused(self);
    return bypass ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AddressControlErrorIsBypassed(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet FcsErrorBypass(AtHdlcChannel self, eBool bypass)
    {
    AtUnused(self);
    return bypass ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool FcsErrorIsBypassed(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DropPacketConditionMaskSet(AtEncapChannel self, uint32 conditionMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;
    AtHdlcChannel channel = (AtHdlcChannel)self;
    eBool isNotDropped;

    if (conditionMask & cAtHdlcChannelDropConditionPktAddrCtrlError)
        {
        isNotDropped = (enableMask & cAtHdlcChannelDropConditionPktAddrCtrlError) ? cAtFalse : cAtTrue;
        ret |= AddressControlErrorBypass(channel, isNotDropped);
        }

    if (conditionMask & cAtHdlcChannelDropConditionPktFcsError)
        {
        isNotDropped = (enableMask & cAtHdlcChannelDropConditionPktFcsError) ? cAtFalse : cAtTrue;
        ret |= FcsErrorBypass(channel, isNotDropped);
        }

    return ret;
    }

static uint32 DropPacketConditionMaskGet(AtEncapChannel self)
    {
    AtHdlcChannel channel = (AtHdlcChannel)self;
    uint32 conditions = 0;

    if (AddressControlErrorIsBypassed(channel))
        conditions |= cAtHdlcChannelDropConditionPktAddrCtrlError;

    if (FcsErrorIsBypassed(channel))
        conditions |= cAtHdlcChannelDropConditionPktFcsError;

    return conditions;
    }

static eAtRet ScrambleEnable(AtEncapChannel self, eBool scrambleEnable)
    {
    if (IsLogical((AtHdlcChannel)self))
        mCacheValueSet(scrambleEnable);

    return AtEncapChannelScrambleEnable((AtEncapChannel)PhysicalEncapChannelGet((AtHdlcChannel)self), scrambleEnable);
    }

static eBool ScrambleIsEnabled(AtEncapChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        mCacheValueGet(scrambleEnable, cAtFalse);

    return AtEncapChannelScrambleIsEnabled((AtEncapChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static eAtRet AllConfigSet(AtChannel self, void *pAllConfig)
    {
    eAtRet ret;
    tAtHdlcChannelAllConfig *configs = (tAtHdlcChannelAllConfig *) pAllConfig;
    AtHdlcChannel channel = (AtHdlcChannel)self;

    ret = cAtOk;
    ret |= AddressMonitorEnable(channel, configs->addrCmpEn);
    ret |= ExpectedAddressSet(channel, &(configs->addrExpVal));
    ret |= AddressInsertionEnable(channel, configs->addrInsEn);
    ret |= TxAddressSet(channel, &(configs->addrInsVal));
    ret |= ControlMonitorEnable(channel, configs->ctrlCmpEn);
    ret |= ExpectedControlSet(channel, &(configs->ctrlExpVal));
    ret |= ControlInsertionEnable(channel, configs->ctrlInsEn);
    ret |= TxControlSet(channel, &(configs->ctrlInsVal));
    ret |= FlagSet(channel, configs->flag);
    ret |= FcsModeSet(channel, configs->fcsMode);
    ret |= StuffModeSet(channel, configs->stuffingMd);
    ret |= ScrambleEnable((AtEncapChannel)channel, configs->scrEn);

    return ret;
    }

static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
    eAtRet ret = cAtOk;
    tAtHdlcChannelAllConfig *configs = (tAtHdlcChannelAllConfig *) pAllConfig;
    AtHdlcChannel channel = (AtHdlcChannel)self;

    if (pAllConfig == NULL)
        return cAtErrorNullPointer;

    configs->addrCmpEn = AddressMonitorIsEnabled(channel);
    configs->addrInsEn = AddressInsertionIsEnabled(channel);
    configs->ctrlCmpEn = ControlMonitorIsEnabled(channel);
    configs->ctrlInsEn = ControlInsertionIsEnabled(channel);
    ret |= ExpectedAddressGet(channel, &configs->addrExpVal);
    ret |= TxAddressGet(channel, &configs->addrInsVal);
    ret |= ExpectedControlGet(channel, &configs->ctrlExpVal);
    ret |= TxControlGet(channel, &configs->ctrlInsVal);
    configs->flag = FlagGet(channel);
    configs->fcsMode = FcsModeGet(channel);
    configs->stuffingMd = StuffModeGet(channel);
    configs->scrEn = ScrambleIsEnabled((AtEncapChannel)channel);
    return ret;
    }

static eAtRet RxAddressControlCompress(AtHdlcChannel self, eBool rxAddrCtrlCompress)
    {
    if (IsLogical((AtHdlcChannel)self))
        mCacheValueSet(rxAddrCtrlCompress);

    return AtHdlcChannelRxAddressControlCompress(PhysicalEncapChannelGet((AtHdlcChannel)self), rxAddrCtrlCompress);
    }

static eBool RxAddressControlIsCompressed(AtHdlcChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        mCacheValueGet(rxAddrCtrlCompress, cAtFalse);

    return AtHdlcChannelRxAddressControlIsCompressed(PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static eAtRet TxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
    AtUnused(self);

    /* Compression is not supported so far */
    return (compress) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxAddressControlIsCompressed(AtHdlcChannel self)
    {
    AtUnused(self);

    /* Compression is not supported so far */
    return cAtFalse;
    }

static uint32 HwIdGet(AtChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        {
        AtChannelLog(self, cAtLogLevelCritical, __FILE__, __LINE__, "Get HW ID while channel is logical\r\n");
        return cInvalidUint32;
        }

    return AtChannelHwIdGet((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static eAtRet Debug(AtChannel self)
    {
    AtHdlcChannel hwencap;
    static char str[16];
    eBool convertSuccess;
    eAtHdlcFcsMode fcsMode;
    uint8 controlInsert;
    uint8 addressInsert;
    uint8 expectedControl;
    uint8 expectedAddress;
    eBool en;
    uint32 dropConditions;

    if (IsLogical((AtHdlcChannel)self))
        AtPrintc(cSevInfo, "Encapsulation Channel is logical\r\n");
    else
        {
        hwencap = mThis(self)->physicalChannel;

        AtPrintc(cSevNormal, "=================================================\r\n");
        AtPrintc(cSevNormal, "= dynamic_hdlc.%s\r\n", AtChannelIdString(self));
        AtPrintc(cSevNormal, "=================================================\r\n");
        AtPrintc(cSevNormal, "* HwHdlcChannel: ");
        AtPrintc(cSevInfo, "%u\r\n", AtChannelHwIdGet((AtChannel)hwencap));

        AtPrintc(cSevNormal, "* Physical Configuration: \r\n");

        AtPrintc(cSevNormal, "   - Address size (bytes)          : ");
        AtPrintc(cSevInfo, "%u\r\n", AtHdlcChannelAddressSizeGet(hwencap));

        AtPrintc(cSevNormal, "   - Control size (bytes)          : ");
        AtPrintc(cSevInfo, "%u\r\n", AtHdlcChannelControlSizeGet(hwencap));

        en = AtHdlcChannelAddressControlIsCompressed(hwencap);
        AtPrintc(cSevNormal, "   - Address Control Compress      : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        en = AtHdlcChannelTxAddressControlIsCompressed(hwencap);
        AtPrintc(cSevNormal, "   - Tx Address Control Compress   : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        en = AtHdlcChannelRxAddressControlIsCompressed(hwencap);
        AtPrintc(cSevNormal, "   - Rx Address Control Compress   : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        en = AtHdlcChannelAddressInsertionIsEnabled(hwencap);
        AtPrintc(cSevNormal, "   - Address insert enable         : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        AtHdlcChannelTxAddressGet(hwencap, &addressInsert);
        AtPrintc(cSevNormal, "   - Address insert                : ");
        AtPrintc(cSevInfo, "%u\r\n", addressInsert);

        en = AtHdlcChannelControlInsertionIsEnabled(hwencap);
        AtPrintc(cSevNormal, "   - Control insert enable         : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        AtHdlcChannelTxControlGet(hwencap, &controlInsert);
        AtPrintc(cSevNormal, "   - Control insert                : ");
        AtPrintc(cSevInfo, "%u\r\n", controlInsert);

        en = AtHdlcChannelAddressMonitorIsEnabled(hwencap);
        AtPrintc(cSevNormal, "   - Address Compare enable        : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        AtHdlcChannelExpectedAddressGet(hwencap, &expectedAddress);
        AtPrintc(cSevNormal, "   - Expected Address              : ");
        AtPrintc(cSevInfo, "%u\r\n", expectedAddress);

        en = AtHdlcChannelControlMonitorIsEnabled(hwencap);
        AtPrintc(cSevNormal, "   - Control Compare enable        : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        AtHdlcChannelExpectedControlGet(hwencap, &expectedControl);
        AtPrintc(cSevNormal, "   - Expected Control              : ");
        AtPrintc(cSevInfo, "%u\r\n", expectedControl);

        en = AtHdlcChannelAddressControlIsBypassed(hwencap);
        AtPrintc(cSevNormal, "   - Address Control Bypass        : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        dropConditions = AtEncapChannelDropPacketConditionMaskGet((AtEncapChannel)self);
        en = (dropConditions & cAtHdlcChannelDropConditionPktAddrCtrlError) ? cAtFalse : cAtTrue;
        AtPrintc(cSevNormal, "   - Address Control Error Bypass  : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        AtPrintc(cSevNormal, "   - Flag                          : ");
        AtPrintc(cSevInfo, "0x%x\r\n", AtHdlcChannelFlagGet(hwencap));

        fcsMode = AtHdlcChannelFcsModeGet(hwencap);
        mAtEnumToStr(cThaDynamicHdlcFcsModeStr, cThaDynamicHdlcFcsModeVal, fcsMode, str, convertSuccess);
        AtPrintc(cSevNormal, "   - FCS Mode                      : ");
        AtPrintc(cSevInfo, "%s\r\n", (convertSuccess) ? str : "Unknown");

        en = (dropConditions & cAtHdlcChannelDropConditionPktFcsError) ? cAtFalse : cAtTrue;
        AtPrintc(cSevNormal, "   - FCS Error Bypass              : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        en = AtHdlcChannelScrambleIsEnabled(hwencap);
        AtPrintc(cSevNormal, "   - Scramble                      : ");
        AtPrintc(cSevInfo, "%s\r\n", (en) ? "en" : "dis");

        AtPrintc(cSevNormal, "   - Stuffing Mode                 : ");
        AtPrintc(cSevInfo, "%s\r\n", (AtHdlcChannelStuffModeGet(hwencap) == cAtHdlcStuffBit) ? "Stuff-Bit" : "Stuff-Byte");

        AtPrintc(cSevNormal, "   - MTU                           : ");
        AtPrintc(cSevInfo, "%u\r\n", AtHdlcChannelMtuGet(hwencap));

        AtPrintc(cSevNormal, "\r\n");
        ThaModulePdaHdlcChannelRegsShow((AtHdlcChannel)self);

        AtPrintc(cSevNormal, "\r\n");
        AtChannelDebug((AtChannel)hwencap);
        }

    return cAtOk;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtHdlcChannel physicalChannel;
    eAtRet ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    physicalChannel = ThaHdlcChannelPhysicalChannelGet((AtHdlcChannel)self);
    return AtChannelBindToPseudowire((AtChannel)physicalChannel, pseudowire);
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    AtChannel physicalChannel = (AtChannel)ThaHdlcChannelPhysicalChannelGet((AtHdlcChannel)self);

    /* Only Handle when have Physical */
    if (physicalChannel)
        return AtChannelQueueEnable(physicalChannel, enable);

    return cAtOk;
    }

static eAtRet HwResourceAllocate(AtChannel circuit)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)circuit);

    if (phyChannel == NULL)
        mChannelError(circuit, cAtErrorNullPointer);

    return AtChannelHwResourceAllocate(phyChannel);
    }

static eAtRet HwResourceDeallocate(AtChannel circuit)
    {
    AtChannel phyChannel = AtEncapChannelBoundPhyGet((AtEncapChannel)circuit);

    if (phyChannel == NULL)
        mChannelError(circuit, cAtErrorNullPointer);

    return AtChannelHwResourceDeallocate(phyChannel);
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtChannelHwIdGet(self);
    }

static eAtRet SapiModeSet(ThaHdlcChannel self, eThaPosSapiMd sapiMode)
    {
    AtUnused(self);
    AtUnused(sapiMode);
    return cAtOk;
    }

static eAtRet FlagModeSet(ThaHdlcChannel self, uint8 flagMode)
    {
    AtUnused(self);
    AtUnused(flagMode);
    return cAtOk;
    }

static uint8 CircuitSliceGet(ThaHdlcChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "Get slice ID while channel is logical\r\n");
        return 0;
        }

    return ThaHdlcChannelCircuitSliceGet((ThaHdlcChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static eBool CircuitIsHo(ThaHdlcChannel self)
    {
    if (IsLogical((AtHdlcChannel)self))
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "Channel has not been bound circuit\r\n");
        return 0;
        }

    return ThaHdlcChannelCircuitIsHo((ThaHdlcChannel)PhysicalEncapChannelGet((AtHdlcChannel)self));
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (IsLogical((AtHdlcChannel)self))
        return cAtFalse;

    return AtChannelCounterIsSupported((AtChannel)PhysicalEncapChannelGet((AtHdlcChannel)self), counterType);
    }

static eAtModuleEncapRet IdlePatternSet(AtHdlcChannel self, uint8 idlePattern)
    {
    if (IsLogical(self))
        mCacheValueSet(idlePattern);

    return AtHdlcChannelIdlePatternSet(PhysicalEncapChannelGet(self), idlePattern);
    }

static uint8 IdlePatternGet(AtHdlcChannel self)
    {
    if (IsLogical(self))
        mCacheValueGet(idlePattern, cAtHdlcFcsModeNoFcs);

    return AtHdlcChannelIdlePatternGet(PhysicalEncapChannelGet(self));
    }

static eAtRet FcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode fcsCalculationMode)
    {
    if (IsLogical(self))
        mCacheValueSet(fcsCalculationMode);

    return AtHdlcChannelFcsCalculationModeSet(PhysicalEncapChannelGet(self), fcsCalculationMode);
    }

static eAtHdlcFcsCalculationMode FcsCalculationModeGet(AtHdlcChannel self)
    {
    if (IsLogical(self))
        mCacheValueGet(fcsCalculationMode, cAtHdlcFcsCalculationModeUnknown);

    return AtHdlcChannelFcsCalculationModeGet(PhysicalEncapChannelGet(self));
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool rxTrafficEnable)
    {
    AtChannel phyChannel = (AtChannel)ThaHdlcChannelPhysicalChannelGet((AtHdlcChannel)self);
    if (phyChannel == NULL)
        return cAtOk;

    return AtChannelRxTrafficEnable(phyChannel, rxTrafficEnable);
    }

static eAtRet HdlcPwPayloadTypeSet(ThaHdlcChannel self, eAtPwHdlcPayloadType payloadType)
    {
    ThaHdlcChannel phyChannel = (ThaHdlcChannel)ThaHdlcChannelPhysicalChannelGet((AtHdlcChannel)self);
    if (phyChannel == NULL)
        return cAtOk;

    return mMethodsGet(phyChannel)->HdlcPwPayloadTypeSet(phyChannel, payloadType);
    }

static ThaHdlcChannel PhysicalChannelGet(ThaHdlcChannel self)
    {
    return (ThaHdlcChannel)(mThis(self)->physicalChannel);
    }

static void OverrideAtObject(AtHdlcChannel self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllConfigSet);
        mMethodOverride(m_AtChannelOverride, AllConfigGet);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, HwResourceAllocate);
        mMethodOverride(m_AtChannelOverride, HwResourceDeallocate);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, QueueEnable);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel encapChannel = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(encapChannel) ;
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(m_AtEncapChannelOverride));
        mMethodOverride(m_AtEncapChannelOverride, PhyBind);
        mMethodOverride(m_AtEncapChannelOverride, EncapTypeGet);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleEnable);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtEncapChannelOverride, DropPacketConditionMaskSet);
        mMethodOverride(m_AtEncapChannelOverride, DropPacketConditionMaskGet);
        }

    mMethodsSet(encapChannel, &m_AtEncapChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, mMethodsGet(self), sizeof(tAtHdlcChannelMethods));
        mMethodOverride(m_AtHdlcChannelOverride, AddressSizeSet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressSizeGet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, AddressInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeSet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlSizeGet);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlMonitorIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionEnable);
        mMethodOverride(m_AtHdlcChannelOverride, ControlInsertionIsEnabled);
        mMethodOverride(m_AtHdlcChannelOverride, FlagSet);
        mMethodOverride(m_AtHdlcChannelOverride, FlagGet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, MtuSet);
        mMethodOverride(m_AtHdlcChannelOverride, MtuGet);
        mMethodOverride(m_AtHdlcChannelOverride, AddressControlBypass);
        mMethodOverride(m_AtHdlcChannelOverride, AddressControlIsBypassed);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlCompress);
        mMethodOverride(m_AtHdlcChannelOverride, RxAddressControlIsCompressed);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressControlCompress);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressControlIsCompressed);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternSet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternGet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedAddressGet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, ExpectedControlGet);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxControlGet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressSet);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressGet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideThaHdlcChannel(AtHdlcChannel self)
    {
    ThaHdlcChannel hdlcChannel = (ThaHdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHdlcChannelOverride, mMethodsGet(hdlcChannel), sizeof(m_ThaHdlcChannelOverride));
        mMethodOverride(m_ThaHdlcChannelOverride, SapiModeSet);
        mMethodOverride(m_ThaHdlcChannelOverride, FlagModeSet);
        mMethodOverride(m_ThaHdlcChannelOverride, CircuitSliceGet);
        mMethodOverride(m_ThaHdlcChannelOverride, CircuitIsHo);
        mMethodOverride(m_ThaHdlcChannelOverride, HdlcPwPayloadTypeSet);
        mMethodOverride(m_ThaHdlcChannelOverride, PhysicalChannelGet);
        }

    mMethodsSet(hdlcChannel, &m_ThaHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideThaHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaDynamicHdlcChannel);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHdlcChannelObjectInit(self, channelId, frameType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel ThaDynamicHdlcChannelNew(uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, frameType, module);
    }

uint32 ThaDynamicHdlcChannelHaRestore(AtHdlcChannel self)
    {
    AtHdlcChannel physicalChannel = ThaHdlcChannelPhysicalChannelGet(self);
    if (physicalChannel == NULL)
        return 0;

    return ThaPhysicalHdlcChannelHaRestore(physicalChannel);
    }


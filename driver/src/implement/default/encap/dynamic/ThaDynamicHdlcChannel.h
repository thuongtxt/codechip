/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaDynamicHdlcChannel.h
 * 
 * Created Date: Mar 23, 2016
 *
 * Description : Dynamic HDLC channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THADYNAMICHDLCCHANNEL_H_
#define _THADYNAMICHDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcChannel.h"
#include "../ThaHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel ThaDynamicHdlcChannelNew(uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module);
uint32 ThaDynamicHdlcChannelHaRestore(AtHdlcChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THADYNAMICHDLCCHANNEL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaModuleStmEncap.h
 * 
 * Created Date: Dec 22, 2012
 *
 * Description : Encapsulation module of STM product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULESTMENCAP_H_
#define _THAMODULESTMENCAP_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap ThaModuleStmEncapNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULESTMENCAP_H_ */


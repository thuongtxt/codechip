/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : ThaFrLinkInternal.h
 * 
 * Created Date: Dec 31, 2014
 *
 * Description : Frame relay link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAFRLINKINTERNAL_H_
#define _THAFRLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaCiscoHdlcLinkInternal.h"
#include "ThaFrLink.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaFrLink
    {
    tThaCiscoHdlcLink super;
    }tThaFrLink;

typedef struct tThaStmFrLink
    {
    tThaStmCiscoHdlcLink super;
    }tThaStmFrLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcLink ThaFrLinkObjectInit(AtHdlcLink self, AtHdlcChannel hdlcChannel);
AtHdlcLink ThaStmFrLinkObjectInit(AtHdlcLink self, AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THAFRLINKINTERNAL_H_ */


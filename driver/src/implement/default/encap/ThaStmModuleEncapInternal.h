/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : ThaStmModuleEncapInternal.h
 * 
 * Created Date: Aug 14, 2013
 *
 * Description : Encap module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMODULEENCAPINTERNAL_H_
#define _THASTMMODULEENCAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ppp/ThaStmPppLink.h"
#include "ThaModuleEncapInternal.h"
#include "ThaModuleStmEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleStmEncap
    {
    tThaModuleEncap super;

    /* Private data */
    }tThaModuleStmEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap ThaModuleStmEncapObjectInit(AtModuleEncap self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMODULEENCAPINTERNAL_H_ */


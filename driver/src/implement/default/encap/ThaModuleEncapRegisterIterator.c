/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENC
 *
 * File        : ThaModuleEncapRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : ENC register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "AtModuleEncap.h"

#include "../../../util/AtIteratorInternal.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "ThaModuleEncapReg.h"
#include "ThaModuleEncapInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModuleEncapRegisterIterator
    {
    tAtModuleRegisterIterator super;
    }tThaModuleEncapRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleEncapRegisterIterator);
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
    AtModuleEncap encapModule = (AtModuleEncap)AtModuleRegisterIteratorModuleGet(self);
    return AtModuleEncapMaxChannelsGet(encapModule);
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    mNextReg(iterator, 0, cThaRegDatEncMstCtrl);
    mNextReg(iterator, cThaRegDatEncMstCtrl   , cThaRegDatEncOptCtrl1);
    mNextReg(iterator, cThaRegDatDecMasterCtrl, cThaRegDatDecOptCtrl1);
    mNextChannelRegister(iterator, cThaRegDatEncOptCtrl1, cThaRegDatEncOptCtrl2);
    mNextChannelRegister(iterator, cThaRegDatEncOptCtrl2, cThaRegDatDecMasterCtrl);
    mNextChannelRegister(iterator, cThaRegDatDecOptCtrl1, cThaRegDatDecOptCtrl2);
    mNextChannelRegister(iterator, cThaRegDatDecOptCtrl2, cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, MaxNumChannels);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleEncapRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator ThaModuleEncapRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return ThaModuleEncapRegisterIteratorObjectInit(newIterator, module);
    }

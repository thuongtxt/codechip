/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG (internal module)
 * 
 * File        : ThaModuleMpeg.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMPEG_H_
#define _THAMODULEMPEG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleMpegInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleMpegNew(AtDevice device);

/* For debugging */
void ThaModuleMpegEncapChannelRegsShow(AtEncapChannel channel);
void ThaModuleMpegHdlcBundleRegsShow(AtHdlcBundle channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMPEG_H_ */


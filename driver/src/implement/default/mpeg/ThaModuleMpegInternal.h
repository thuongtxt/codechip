/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG internal module
 * 
 * File        : ThaModuleMpegInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMPEGINTERNAL_H_
#define _THAMODULEMPEGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../man/ThaModuleClasses.h"
#include "ThaModuleMpeg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tThaModuleMpegMethods
    {
    /* Debug */
    void (*EncapChannelRegsShow)(ThaModuleMpeg self, AtEncapChannel channel);
    void (*HdlcBundleRegsShow)(ThaModuleMpeg self, AtHdlcBundle bundle);
    }tThaModuleMpegMethods;

typedef struct tThaModuleMpeg
    {
    tAtModule super;
    const tThaModuleMpegMethods *methods;
    }tThaModuleMpeg;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleMpegObjectInit(AtModule self, AtDevice device);
AtIterator ThaModuleMpegRegisterIteratorCreate(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMPEGINTERNAL_H_ */


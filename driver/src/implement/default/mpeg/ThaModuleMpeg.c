/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPEG (internal)
 *
 * File        : ThaModuleMpeg.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : MPEG internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "ThaModuleMpegDebugReg.h"
#include "ThaModuleMpegInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleMpegMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x240000) &&
        (localAddress <= 0x27A000))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet Init(AtModule self)
    {
    return m_AtModuleMethods->Init(self);
    }

static void EncapChannelRegsShow(ThaModuleMpeg self, AtEncapChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    }

static void HdlcBundleRegsShow(ThaModuleMpeg self, AtHdlcBundle channel)
    {
    AtUnused(self);
    AtUnused(channel);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool MemoryCanBeTested(AtModule self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleMpegRegisterIteratorCreate(self);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x240000, 0x240001, 0x240002, 0x240003};
	AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 4;

    return holdRegisters;
    }

static void StickiesClear(AtModule self)
    {
    static const uint32 cAllOne = 0xFFFFFFFF;

    mModuleHwWrite(self, cThaDebugMpegSticky0, cAllOne);
    mModuleHwWrite(self, cThaDebugMpegSticky1, cAllOne);
    }

static eAtRet Debug(AtModule self)
    {
    /* Information of super */
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    mModuleDebugRegPrint(self, MpegSticky0);
    mModuleDebugRegPrint(self, MpegSticky1);

    /* Clear for next time */
    StickiesClear(self);

    return cAtOk;
    }

void ThaModuleMpegEncapChannelRegsShow(AtEncapChannel channel)
    {
    ThaModuleMpeg self = (ThaModuleMpeg)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleMpeg);
    AtPrintc(cSevInfo, "* MPEG registers of '%s':\r\n", AtObjectToString((AtObject)channel));
    
    if (self)
        mMethodsGet(self)->EncapChannelRegsShow(self, channel);
    }

void ThaModuleMpegHdlcBundleRegsShow(AtHdlcBundle bundle)
    {
    ThaModuleMpeg moduleMpeg = (ThaModuleMpeg)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)bundle), cThaModuleMpeg);
    AtPrintc(cSevInfo, "* MPEG registers of '%s':\r\n", AtObjectToString((AtObject)bundle));
    
    if (moduleMpeg)
        mMethodsGet(moduleMpeg)->HdlcBundleRegsShow(moduleMpeg, (AtHdlcBundle)bundle);
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static void MethodsInit(ThaModuleMpeg self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EncapChannelRegsShow);
        mMethodOverride(m_methods, HdlcBundleRegsShow);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, MemoryCanBeTested);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModule ThaModuleMpegObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModuleMpeg));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModuleMpeg, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaModuleMpeg)self);
    m_methodsInit = 1;
    MethodsInit((ThaModuleMpeg)self);

    return self;
    }

AtModule ThaModuleMpegNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModuleMpeg));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleMpegObjectInit(newModule, device);
    }

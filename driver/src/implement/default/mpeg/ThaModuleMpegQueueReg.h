/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPEG
 * 
 * File        : ThaModuleMpegQueueReg.h
 * 
 * Created Date: Apr 27, 2016
 *
 * Description : MPEG Queue registers.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMPEGQUEUEREG_H_
#define _THAMODULEMPEGQUEUEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Bundle Queue Threshold Control
Reg Addr   : 0x20000-0x207FF
Reg Formula: 0x20000+$qid
    Where  :
           + qid(0-2047) : Queue ID
Reg Desc   :
Used to configure threshold for Bundle Queue
: {qid(0000-2047)} -> For Bundle Queue,512 Bundles, 4 Queues per Bundle
------------------------------------------------------------------------------*/
#define cAf6Reg_cfgqthsh_pen_Base                                                                     0x20000
#define cAf6Reg_cfgqthsh_pen(qid)                                                             (0x20000+(qid))
#define cAf6Reg_cfgqthsh_pen_WidthVal                                                                      32
#define cAf6Reg_cfgqthsh_pen_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: Queue_Enb
BitField Type: R/W
BitField Desc: Queue Enable 0: Disable Queue and flush Queue 1: Enable Queue
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_cfgqthsh_pen_Queue_Enb_Bit_Start                                                              20
#define cAf6_cfgqthsh_pen_Queue_Enb_Bit_End                                                                20
#define cAf6_cfgqthsh_pen_Queue_Enb_Mask                                                               cBit20
#define cAf6_cfgqthsh_pen_Queue_Enb_Shift                                                                  20
#define cAf6_cfgqthsh_pen_Queue_Enb_MaxVal                                                                0x1
#define cAf6_cfgqthsh_pen_Queue_Enb_MinVal                                                                0x0
#define cAf6_cfgqthsh_pen_Queue_Enb_RstVal                                                                0x0

/*--------------------------------------
BitField Name: EFFQthsh_max
BitField Type: R/W
BitField Desc: External FiFo maximun queue threshold, step 128 pkts
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_cfgqthsh_pen_EFFQthsh_max_Bit_Start                                                           15
#define cAf6_cfgqthsh_pen_EFFQthsh_max_Bit_End                                                             19
#define cAf6_cfgqthsh_pen_EFFQthsh_max_Mask                                                         cBit19_15
#define cAf6_cfgqthsh_pen_EFFQthsh_max_Shift                                                               15
#define cAf6_cfgqthsh_pen_EFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfgqthsh_pen_EFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfgqthsh_pen_EFFQthsh_max_RstVal                                                             0x8

/*--------------------------------------
BitField Name: EFFQthsh_min
BitField Type: R/W
BitField Desc: External FiFo minimun queue threshold, step 128 pkts
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_cfgqthsh_pen_EFFQthsh_min_Bit_Start                                                           10
#define cAf6_cfgqthsh_pen_EFFQthsh_min_Bit_End                                                             14
#define cAf6_cfgqthsh_pen_EFFQthsh_min_Mask                                                         cBit14_10
#define cAf6_cfgqthsh_pen_EFFQthsh_min_Shift                                                               10
#define cAf6_cfgqthsh_pen_EFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfgqthsh_pen_EFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfgqthsh_pen_EFFQthsh_min_RstVal                                                             0x2

/*--------------------------------------
BitField Name: IFFQthsh_max
BitField Type: R/W
BitField Desc: Internal FiFo maximun queue threshold, step 1 pkts
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_cfgqthsh_pen_IFFQthsh_max_Bit_Start                                                            5
#define cAf6_cfgqthsh_pen_IFFQthsh_max_Bit_End                                                              9
#define cAf6_cfgqthsh_pen_IFFQthsh_max_Mask                                                           cBit9_5
#define cAf6_cfgqthsh_pen_IFFQthsh_max_Shift                                                                5
#define cAf6_cfgqthsh_pen_IFFQthsh_max_MaxVal                                                            0x1f
#define cAf6_cfgqthsh_pen_IFFQthsh_max_MinVal                                                             0x0
#define cAf6_cfgqthsh_pen_IFFQthsh_max_RstVal                                                            0x31

/*--------------------------------------
BitField Name: IFFQthsh_min
BitField Type: R/W
BitField Desc: Internal FiFo minimun queue threshold, step 1 pkts
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_cfgqthsh_pen_IFFQthsh_min_Bit_Start                                                            0
#define cAf6_cfgqthsh_pen_IFFQthsh_min_Bit_End                                                              4
#define cAf6_cfgqthsh_pen_IFFQthsh_min_Mask                                                           cBit4_0
#define cAf6_cfgqthsh_pen_IFFQthsh_min_Shift                                                                0
#define cAf6_cfgqthsh_pen_IFFQthsh_min_MaxVal                                                            0x1f
#define cAf6_cfgqthsh_pen_IFFQthsh_min_MinVal                                                             0x0
#define cAf6_cfgqthsh_pen_IFFQthsh_min_RstVal                                                             0x7

/*------------------------------------------------------------------------------
Reg Name   : Scheduler: Group priority Configuration Per Bundle
Reg Addr   : 0x06000-0x061FF
Reg Formula: 0x06000 + $bid
    Where  :
           + bid(0-511) : Bundle ID
Reg Desc   :
Used to configure the priority queue per Link.
- Support 3 group: Low Priority(LSP), High Priority(HSP), DWRR, mix mode.
- HSP: Higest Priority
- LSP: Lowest Priority
- DWRR: Medium Priority

------------------------------------------------------------------------------*/
#define cAf6Reg_schebundicfg0_pen_Base                                                                 0x06000
#define cAf6Reg_schebundicfg0_pen(bid)                                                         (0x06000+(bid))
#define cAf6Reg_schebundicfg0_pen_WidthVal                                                                  32
#define cAf6Reg_schebundicfg0_pen_WriteMask                                                                0x0

/*--------------------------------------
BitField Name: Hsp_grp
BitField Type: R/W
BitField Desc: HSP Queue
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_schebundicfg0_pen_Hsp_grp_Bit_Start                                                             8
#define cAf6_schebundicfg0_pen_Hsp_grp_Bit_End                                                              11
#define cAf6_schebundicfg0_pen_Hsp_grp_Mask                                                           cBit11_8
#define cAf6_schebundicfg0_pen_Hsp_grp_Shift                                                                 8
#define cAf6_schebundicfg0_pen_Hsp_grp_MaxVal                                                              0xf
#define cAf6_schebundicfg0_pen_Hsp_grp_MinVal                                                              0x0
#define cAf6_schebundicfg0_pen_Hsp_grp_RstVal                                                              0x0

/*--------------------------------------
BitField Name: DWRR_grp
BitField Type: R/W
BitField Desc: DWRR Queue
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_schebundicfg0_pen_DWRR_grp_Bit_Start                                                            4
#define cAf6_schebundicfg0_pen_DWRR_grp_Bit_End                                                              7
#define cAf6_schebundicfg0_pen_DWRR_grp_Mask                                                           cBit7_4
#define cAf6_schebundicfg0_pen_DWRR_grp_Shift                                                                4
#define cAf6_schebundicfg0_pen_DWRR_grp_MaxVal                                                             0xf
#define cAf6_schebundicfg0_pen_DWRR_grp_MinVal                                                             0x0
#define cAf6_schebundicfg0_pen_DWRR_grp_RstVal                                                             0x0

/*--------------------------------------
BitField Name: Lsp_grp
BitField Type: R/W
BitField Desc: LSP Queue
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_schebundicfg0_pen_Lsp_grp_Bit_Start                                                             0
#define cAf6_schebundicfg0_pen_Lsp_grp_Bit_End                                                               3
#define cAf6_schebundicfg0_pen_Lsp_grp_Mask                                                            cBit3_0
#define cAf6_schebundicfg0_pen_Lsp_grp_Shift                                                                 0
#define cAf6_schebundicfg0_pen_Lsp_grp_MaxVal                                                              0xf
#define cAf6_schebundicfg0_pen_Lsp_grp_MinVal                                                              0x0
#define cAf6_schebundicfg0_pen_Lsp_grp_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : DWRR Quantum Configuration
Reg Addr   : 0x04000-0x051FF
Reg Formula: 0x04000+$bid
    Where  :
           + bid(0-2047) : Bundle Queue ID
Reg Desc   :
Used to configure quantum for per Bundle in DWRR scheduling mode

------------------------------------------------------------------------------*/
#define cAf6Reg_schebundcfg1_pen_Base                                                                  0x04000
#define cAf6Reg_schebundcfg1_pen(bid)                                                          (0x04000+(bid))
#define cAf6Reg_schebundcfg1_pen_WidthVal                                                                   32
#define cAf6Reg_schebundcfg1_pen_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: Bundshce_DWRR_QTum
BitField Type: R/W
BitField Desc: DWRR Quantum,Minximun MTU
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Bit_Start                                                   0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Bit_End                                                    19
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Mask                                                 cBit19_0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_Shift                                                       0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_MaxVal                                                0xfffff
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_MinVal                                                    0x0
#define cAf6_schebundcfg1_pen_Bundshce_DWRR_QTum_RstVal                                                    0x0


/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMPEGQUEUEREG_H_ */


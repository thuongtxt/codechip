/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPEG
 *
 * File        : ThaModuleMpegRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : MPEG register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "AtModulePpp.h"
#include "AtModuleEncap.h"

#include "../../../util/AtIteratorInternal.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "../ppp/ThaMpegReg.h"
#include "ThaModuleMpegInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eMpegRegType
    {
    cMpegRegTypeBundle,
    cMpegRegTypeLink,
    cMpegRegTypeEntry
    }eMpegRegType;

typedef struct tThaModuleMpegRegisterIterator * ThaModuleMpegRegisterIterator;

typedef struct tThaModuleMpegRegisterIterator
    {
    tAtModuleRegisterIterator super;
    eMpegRegType regType;
    }tThaModuleMpegRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleMpegRegisterIterator);
    }

static uint8 RegType(AtModuleRegisterIterator self)
    {
    return ((ThaModuleMpegRegisterIterator)self)->regType;
    }

static void RegTypeSet(AtModuleRegisterIterator self, uint8 regType)
    {
    ((ThaModuleMpegRegisterIterator)self)->regType = regType;
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
    AtDevice device = AtModuleDeviceGet(AtModuleRegisterIteratorModuleGet(self));
    AtModulePpp pppModule = (AtModulePpp)AtDeviceModuleGet(device, cAtModulePpp);
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    if (RegType(self) == cMpegRegTypeBundle)
        return AtModulePppMaxBundlesGet(pppModule);

    if (RegType(self) == cMpegRegTypeLink)
        return AtModuleEncapMaxChannelsGet(encapModule);

    if (RegType(self) == cMpegRegTypeEntry)
        return 256;

    return 0;
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

	mNextReg(iterator, 0, cThaRegThalassaMLPppEgrOamCpuPktDataCtrl);
	RegTypeSet(iterator, cMpegRegTypeEntry);
	mNextChannelRegister(iterator, cThaRegThalassaMLPppEgrOamCpuPktDataCtrl, cThaRegEgBundleScheduleCtrl);
	RegTypeSet(iterator, cMpegRegTypeBundle);
	mNextChannelRegister(iterator, cThaRegEgBundleScheduleCtrl, cThaRegEgLinkScheduleCtrl);
	RegTypeSet(iterator, cMpegRegTypeLink);
	mNextChannelRegister(iterator, cThaRegEgLinkScheduleCtrl, cThaRegThalassaMLPppEgrPppLinkCtrl1);
	mNextChannelRegister(iterator, cThaRegThalassaMLPppEgrPppLinkCtrl1, cAtModuleRegisterIteratorEnd);

	return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_AtModuleRegisterIteratorOverride,
                                  mMethodsGet(iterator),
                                  sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, MaxNumChannels);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleMpegRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator ThaModuleMpegRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return ThaModuleMpegRegisterIteratorObjectInit(newIterator, module);
    }

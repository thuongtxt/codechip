/*
 * ThaModuleMpegDebugReg.h
 *
 *  Created on: May 11, 2013
 *      Author: nguyennt
 */

#ifndef THAMODULEMPEGDEBUGREG_H_
#define THAMODULEMPEGDEBUGREG_H_

#define cThaDebugMpegSticky0 0x00254000
#define cThaDebugMpegSticky1 0x00254001

#ifdef __cplusplus
}
#endif
#endif /* THAMODULEMPEGDEBUGREG_H_ */
#ifdef __cplusplus
extern "C" {
#endif

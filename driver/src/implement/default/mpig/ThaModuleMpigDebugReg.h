/*
 * ThaModuleMpigDebugReg.h
 *
 *  Created on: May 11, 2013
 *      Author: nguyennt
 */

#ifndef THAMODULEMPIGDEBUGREG_H_
#define THAMODULEMPIGDEBUGREG_H_

#define cThaDebugMpigDataSticky0        0x840150
#define cThaDebugMpigDataSticky1        0x840151

#define cThaDebugMpigRsqSticky0         0x860010
#define cThaDebugMpigRsqSticky1         0x860011
#define cThaDebugMpigRsqSticky2         0x860013

#ifdef __cplusplus
}
#endif
#endif /* THAMODULEMPIGDEBUGREG_H_ */
#ifdef __cplusplus
extern "C" {
#endif

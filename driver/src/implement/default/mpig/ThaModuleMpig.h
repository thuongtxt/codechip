/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPIG (internal module)
 * 
 * File        : ThaModuleMpig.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMPIG_H_
#define _THAMODULEMPIG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/ThaModuleClasses.h"
#include "ThaModuleMpigInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Methods of MPIG go here */
AtModule ThaModuleMpigNew(AtDevice device);
eAtRet ThaModuleMpigISISMacGet(ThaModuleMpig self, uint8 *mac);
eAtRet ThaModuleMpigISISMacSet(ThaModuleMpig self, const uint8 *mac);
void ThaModuleMpigEthFlowRegsShow(AtEthFlow flow);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMPIG_H_ */


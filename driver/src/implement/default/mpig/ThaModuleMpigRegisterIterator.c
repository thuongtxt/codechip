/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPIG
 *
 * File        : ThaModuleMpigRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : MPIG register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "AtModuleEncap.h"
#include "AtModuleEth.h"
#include "AtModulePpp.h"

#include "../../../util/AtIteratorInternal.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "../ppp/ThaMpigReg.h"
#include "ThaModuleMpigInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumClassesInBundle 16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eMpigRegType
    {
    cMpigRegTypeLink,
    cMpigRegTypeBundle,
    cMpigRegTypeBundleClass,
    cMpigRegTypeFlow
    }eMpigRegType;

typedef struct tThaModuleMpigRegisterIterator * ThaModuleMpigRegisterIterator;

typedef struct tThaModuleMpigRegisterIterator
    {
    tAtModuleRegisterIterator super;

    /* Private data */
    eMpigRegType regType;
    uint32 bundleId;
    uint8  classId;
    }tThaModuleMpigRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleMpigRegisterIterator);
    }

static uint8 RegType(AtModuleRegisterIterator self)
    {
    return ((ThaModuleMpigRegisterIterator)self)->regType;
    }

static void RegTypeSet(AtModuleRegisterIterator self, uint8 regType)
    {
    ((ThaModuleMpigRegisterIterator)self)->regType = regType;
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
    AtDevice device = AtModuleDeviceGet(AtModuleRegisterIteratorModuleGet(self));
    uint32 maxBundles;

    if (RegType(self) == cMpigRegTypeLink)
        return AtModuleEncapMaxChannelsGet((AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap));

    maxBundles = (uint16)AtModulePppMaxBundlesGet((AtModulePpp)AtDeviceModuleGet(device, cAtModulePpp));
    if (RegType(self) == cMpigRegTypeBundle)
        return maxBundles;

    if (RegType(self) == cMpigRegTypeBundleClass)
        return maxBundles * cMaxNumClassesInBundle;

    if (RegType(self) == cMpigRegTypeFlow)
        {
        AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
        return AtModuleEthMaxFlowsGet(ethModule);
        }

    return m_AtModuleRegisterIteratorMethods->MaxNumChannels(self);
    }

static uint32 CurrentChannel(AtModuleRegisterIterator self)
    {
    if (RegType(self) == cMpigRegTypeBundleClass)
        {
        ThaModuleMpigRegisterIterator iterator = (ThaModuleMpigRegisterIterator)self;
        return (iterator->bundleId * cMaxNumClassesInBundle) + iterator->classId;
        }

    return m_AtModuleRegisterIteratorMethods->CurrentChannel(self);
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    if (RegType(self) == cMpigRegTypeBundleClass)
        {
        ThaModuleMpigRegisterIterator iterator = (ThaModuleMpigRegisterIterator)self;

        iterator->classId = (uint8)(iterator->classId + 1);

        /* Next bundle */
        if (iterator->classId == cMaxNumClassesInBundle)
            {
            iterator->classId  = 0;
            iterator->bundleId = iterator->bundleId + 1;
            }

        return (iterator->bundleId * cMaxNumClassesInBundle) + iterator->classId;
        }

    /* Super may know */
    return m_AtModuleRegisterIteratorMethods->NextOffset(self);
    }

static void ResetChannel(AtModuleRegisterIterator self)
    {
    ThaModuleMpigRegisterIterator iterator = (ThaModuleMpigRegisterIterator)self;

    /* Let its super do first */
    m_AtModuleRegisterIteratorMethods->ResetChannel(self);

    iterator->bundleId = 0;
    iterator->classId  = 0;
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

	mNextReg(iterator, 0, cThaRegMPIGDATLookupLinkStateCtrl);
	mNextReg(iterator, cThaRegMPIGDATLookupLinkStateCtrl, cThaRegMPIGDATLookupLinkCtrl);
	RegTypeSet(iterator, cMpigRegTypeLink);
	mNextChannelRegister(iterator, cThaRegMPIGDATLookupLinkCtrl, cThaRegMPIGDATLookupBundleCtrl);
	RegTypeSet(iterator, cMpigRegTypeBundle);
	mNextChannelRegister(iterator, cThaRegMPIGDATLookupBundleCtrl, cThaRegMPIGDATLookupFlowIdLookupCtrl);
	RegTypeSet(iterator, cMpigRegTypeBundleClass);
	mNextChannelRegister(iterator, cThaRegMPIGDATLookupFlowIdLookupCtrl, cThaRegMPIGDATDeQueCpuReadCtrl);
	mNextReg(iterator, cThaRegMPIGDATDeQueCpuReadCtrl, cThaRegMPIGDATDeQueOamLinkCtrl);
	RegTypeSet(iterator, cMpigRegTypeLink);
	mNextChannelRegister(iterator, cThaRegMPIGDATDeQueOamLinkCtrl, cThaRegMPIGDATDeQuePppLinkCtrl);
	mNextChannelRegister(iterator, cThaRegMPIGDATDeQuePppLinkCtrl, cThaRegMPIGDATDeQueMpFlowCtrl);
	RegTypeSet(iterator, cMpigRegTypeFlow);
	mNextChannelRegister(iterator, cThaRegMPIGDATDeQueMpFlowCtrl, cThaRegMPIGDeQueueLinkCtrl);
	RegTypeSet(iterator, cMpigRegTypeLink);
	mNextChannelRegister(iterator, cThaRegMPIGDeQueueLinkCtrl, cThaRegMPIGRSQResequenceTimeout);
	mNextReg(iterator, cThaRegMPIGRSQResequenceTimeout, cAtModuleRegisterIteratorEnd);

	return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, ResetChannel);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, MaxNumChannels);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, CurrentChannel);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleMpigRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator ThaModuleMpigRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return ThaModuleMpigRegisterIteratorObjectInit(newIterator, module);
    }

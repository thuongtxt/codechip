/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MPIG internal module
 * 
 * File        : ThaModuleMpigInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEMPIGINTERNAL_H_
#define _THAMODULEMPIGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../../default/man/ThaDeviceInternal.h"

#include "ThaModuleMpig.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleMpigMethods
    {
    void (*EthFlowRegsShow)(ThaModuleMpig self, AtEthFlow ethFlow);
    }tThaModuleMpigMethods;

typedef struct tThaModuleMpig
    {
    tAtModule super;
    const tThaModuleMpigMethods *methods;
    }tThaModuleMpig;

/*--------------------------- Forward declarations ---------------------------*/
AtModule ThaModuleMpigObjectInit(AtModule self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
AtIterator ThaModuleMpigRegisterIteratorCreate(AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEMPIGINTERNAL_H_ */


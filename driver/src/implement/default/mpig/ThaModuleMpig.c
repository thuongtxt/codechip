/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MPIG (internal)
 *
 * File        : ThaModuleMpig.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : MPIG internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleMpigInternal.h"
#include "ThaModuleMpigDebugReg.h"

#include "../../../implement/default/ppp/ThaMpigReg.h"
#include "../util/ThaUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleMpigMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtModule self)
    {
    return m_AtModuleMethods->Init(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x841002) && (localAddress <= 0x87FFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x840010, 0x840011, 0x840012};
	AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 3;

    return holdRegisters;
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleMpigRegisterIteratorCreate(self);
    }

static eBool MemoryCanBeTested(AtModule self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "mpig";
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void StickiesClear(AtModule self)
    {
    static const uint32 cAllOne = 0xFFFFFFFF;

    mModuleHwWrite(self, cThaDebugMpigDataSticky0, cAllOne);
    mModuleHwWrite(self, cThaDebugMpigDataSticky1, cAllOne);
    mModuleHwWrite(self, cThaDebugMpigRsqSticky0, cAllOne);
    mModuleHwWrite(self, cThaDebugMpigRsqSticky1, cAllOne);
    mModuleHwWrite(self, cThaDebugMpigRsqSticky2, cAllOne);
    }

static eAtRet Debug(AtModule self)
    {
    /* Information of super */
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    mModuleDebugRegPrint(self, MpigDataSticky0);
    mModuleDebugRegPrint(self, MpigDataSticky1);
    mModuleDebugRegPrint(self, MpigRsqSticky0);
    mModuleDebugRegPrint(self, MpigRsqSticky1);
    mModuleDebugRegPrint(self, MpigRsqSticky2);

    /* Clear for next time */
    StickiesClear(self);

    return cAtOk;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static void EthFlowRegsShow(ThaModuleMpig self, AtEthFlow flowList)
    {
    AtUnused(self);
    AtUnused(flowList);
    }

static void MethodsInit(ThaModuleMpig self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EthFlowRegsShow);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, MemoryCanBeTested);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, TypeString);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModule ThaModuleMpigObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModuleMpig));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModuleMpig, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaModuleMpig)self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleMpigNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModuleMpig));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleMpigObjectInit(newModule, device);
    }

/* ISIS MAC */
eAtRet ThaModuleMpigISISMacSet(ThaModuleMpig self, const uint8 *mac)
    {
    uint32 regVal;

    /* 32LSB */
    regVal  = 0;
    regVal |= mac[2]; regVal <<= 8;
    regVal |= mac[3]; regVal <<= 8;
    regVal |= mac[4]; regVal <<= 8;
    regVal |= mac[5];
    mModuleHwWrite(self, cThaMPIG32LSB_ISISMacAddress, regVal);

    /* 16MSB */
    regVal  = 0;
    regVal |= mac[0]; regVal <<= 8;
    regVal |= mac[1];
    mModuleHwWrite(self, cThaMPIG32MSB_ISISMacAddress, regVal);

    return cAtOk;
    }

eAtRet ThaModuleMpigISISMacGet(ThaModuleMpig self, uint8 *mac)
    {
    uint32 regVal;

    /* 32LSB */
    regVal = mModuleHwRead(self, cThaMPIG32LSB_ISISMacAddress);
    mac[5] = regVal & cBit7_0; regVal >>= 8;
    mac[4] = regVal & cBit7_0; regVal >>= 8;
    mac[3] = regVal & cBit7_0; regVal >>= 8;
    mac[2] = regVal & cBit7_0;

    /* 16MSB */
    regVal = mModuleHwRead(self, cThaMPIG32MSB_ISISMacAddress);
    mac[1] = regVal & cBit7_0; regVal >>= 8;
    mac[0] = regVal & cBit7_0;

    return cAtOk;
    }

void ThaModuleMpigEthFlowRegsShow(AtEthFlow flow)
    {
    ThaModuleMpig moduleMpig = (ThaModuleMpig)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModuleMpig);
    AtPrintc(cSevInfo, "* Mpig registers of '%s':\r\n", AtObjectToString((AtObject)flow));

    if (moduleMpig)
        mMethodsGet(moduleMpig)->EthFlowRegsShow(moduleMpig, flow);
    }

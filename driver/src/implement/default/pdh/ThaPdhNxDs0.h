/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaPdhNxDs0
 *
 * File        : ThaPdhNxDs0.h
 *
 * Created Date: Sep 21, 2012
 *
 * Author      : ntdung
 *
 * Description : This file to store declarations of NxDS0 timeslots.
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _THAPDHNxDS0_HEADER_
#define _THAPDHNxDS0_HEADER_

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/pdh/AtPdhNxDs0Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
typedef struct tThaPdhNxDs0 * ThaPdhNxDs0;

/* Additional methods of this class */
typedef struct tThaPdhNxDs0Methods
    {
    eAtRet (*HwIdFactorGet)(ThaPdhNxDs0 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1, uint32 *hwDs0);
    uint16 (*NxDs0DefaultOffset)(ThaPdhNxDs0 self);
    eAtRet (*TxFramerSignalingIDConversionSet)(ThaPdhNxDs0 self, AtPw pw);
    eAtRet (*PwCasIdleCode1Set)(ThaPdhNxDs0 self, AtPw pw, uint8 abcd1);
    uint8 (*PwCasIdleCode1Get)(ThaPdhNxDs0 self, AtPw pw);
    eAtRet (*PwCasIdleCode2Set)(ThaPdhNxDs0 self, AtPw pw, uint8 abcd2);
    uint8 (*PwCasIdleCode2Get)(ThaPdhNxDs0 self, AtPw pw);
    eAtRet (*PwCasAutoIdleEnable)(ThaPdhNxDs0 self, AtPw pw, eBool enable);
    eBool (*PwCasAutoIdleIsEnabled)(ThaPdhNxDs0 self, AtPw pw);
    }tThaPdhNxDs0Methods;

typedef struct tThaPdhNxDs0
    {
    tAtPdhNxDS0 super;
    const tThaPdhNxDs0Methods *methods;

    /* Private data */
    AtPrbsEngine prbsEngine;
    }tThaPdhNxDs0;

/*--------------------------- Forward declarations ---------------------------*/
AtPdhNxDS0 ThaPdhNxDs0ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask);
AtPdhNxDS0 ThaPdhNxDs0New(AtPdhDe1 de1, uint32 mask);
eAtRet ThaPdhNxDs0Enable(AtPdhNxDS0 self, eBool enable);
uint32 ThaPdhNxDs0MaskGet(AtPdhNxDS0 self);
uint8 ThaPdhNumberOfDs0(AtPdhNxDS0 self);
AtPrbsEngine ThaPdhNxDs0CachePrbsEngine(ThaPdhNxDs0 self, AtPrbsEngine engine);
eAtRet ThaPdhNxDs0HwIdFactorGet(ThaPdhNxDs0 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1, uint32 *hwDs0);

/* CAS IDLE pattern */
eAtRet ThaPdhNxDs0PwCasIdleCode1Set(ThaPdhNxDs0 self, AtPw pwAdapter, uint8 abcd1);
uint8 ThaPdhNxDs0PwCasIdleCode1Get(ThaPdhNxDs0 self, AtPw pwAdapter);
eAtRet ThaPdhNxDs0PwCasIdleCode2Set(ThaPdhNxDs0 self, AtPw pwAdapter, uint8 abcd2);
uint8 ThaPdhNxDs0PwCasIdleCode2Get(ThaPdhNxDs0 self, AtPw pwAdapter);
eAtRet ThaPdhNxDs0PwCasAutoIdleEnable(ThaPdhNxDs0 self, AtPw pwAdapter, eBool enable);
eBool ThaPdhNxDs0PwCasAutoIdleIsEnabled(ThaPdhNxDs0 self, AtPw pwAdapter);

/* Product concretes */
AtPdhNxDS0 Tha60060011PdhNxDs0New(AtPdhDe1 de1, uint32 mask);

#ifdef __cplusplus
}
#endif
#endif /*_THAPDHNxDS0_HEADER_ */

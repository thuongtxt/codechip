/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe2Internal.h
 * 
 * Created Date: May 03, 2018
 *
 * Description : PDH DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE2INTERNAL_H_
#define _THAPDHDE2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pdh/AtPdhDe2Internal.h"
#include "ThaPdhDe2.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Additional methods of this class */
typedef struct tThaPdhDe2Methods
    {
    eAtRet (*HwIdFactorGet)(ThaPdhDe2 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice, uint32 *hwDe2);
    } tThaPdhDe2Methods;

typedef struct tThaPdhDe2
    {
    tAtPdhDe2 super;
    const tThaPdhDe2Methods *methods;

    /* Private data */
    uint16 frameType;
    AtObjectAny        attController;
    }tThaPdhDe2;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe2 ThaPdhDe2ObjectInit(AtPdhDe2 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE2INTERNAL_H_ */


/*------------------------------------------------------------------------------
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaPdhDe1
 *
 * File        : ThaPdhDe1.h
 *
 * Created Date: Sep 10, 2012
 *
 * Author      : ntdung
 *
 * Description : This file contains common prototypes of DS1/E1/J1 module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _THAPDHDE1_H
#define _THAPDHDE1_H

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe1.h"
#include "AtClockExtractor.h"
#include "AtList.h"

#include "../../../generic/pdh/AtPdhDe1Internal.h"
#include "../cdr/controllers/ThaCdrController.h"
#include "retimingcontrollers/ThaPdhRetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tThaPdhDe1 * ThaPdhDe1;

typedef struct tThaCfgPdhDs1E1FrmRxThres
    {
    uint32                       losCntThres;    /* [0..7] Threshold of LOS
                                                    monitoring block */
    uint32                       lofThres;       /* [0..15] Thresholds of FAS
                                                error to declare LOF in
                                                DS1/E1/J1 mode */
    uint32                       aisThres;       /* [0..7] Threshold of AIS
                                                monitoring block. */
    uint32                       raiThres;       /* [0..7] Threshold of RAI
                                                monitoring block. */
    }tThaCfgPdhDs1E1FrmRxThres;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*-------------------------------- Entries -----------------------------------*/
/* Concretes */
AtPdhDe1 ThaPdhDe1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 ThaPdhDe1V2New(uint32 channelId, AtModulePdh module);
AtPdhDe1 ThaPdhDe2De1New(uint32 channelId, AtModulePdh module);

uint32 ThaPdhDe1FlatId(ThaPdhDe1 self);
uint32 ThaPdhDe1FlatIdInPhyModule(ThaPdhDe1 self, uint32 phyModule);
uint8 ThaModulePdhFrameTypeGet(ThaPdhDe1 self);
eAtRet ThaPdhDe1TxFramerBypass(ThaPdhDe1 self, eBool bypass);
eAtRet ThaPdhDe1MapBypass(ThaPdhDe1 self, eBool bypass);
eAtRet ThaPdhDe1RxFramerMonitorOnlyEnable(ThaPdhDe1 self, eBool enable);
eAtRet  ThaPdhDe1BeforeChangeFrameTypeCheck(AtPdhChannel self, uint16 frameType);

eAtTimingMode ThaModulePdhTimingModeHw2Sw(uint8 hwTimingMode);
ThaCdrController ThaPdhDe1CdrControllerGet(ThaPdhDe1 self);

eBool  ThaPdhDe1AutoTxAisEnabledByDefault(ThaPdhDe1 self, AtPw pw);

void ThaPdhDe1SignalingTransparent(ThaPdhDe1 self);
eAtRet ThaPdhDe1AutoInsertFBit(ThaPdhDe1 self, eBool autoInsert);
eBool ThaPdhDe1IsE1Signaling(AtPdhDe1 self);

uint8 ThaPdhDe1PartId(ThaPdhDe1 self);
uint32 ThaPdhDe1DefaultOffset(ThaPdhDe1 de1);
uint32 ThaPdhDe1EncapDs0BitMaskGet(ThaPdhDe1 de1);
uint8 ThaPdhDe1PartId(ThaPdhDe1 self);
uint32 ThaPdhDe1SatopDs0BitMaskGet(ThaPdhDe1 self);

void ThaPdhDe1ClockExtractorSet(ThaPdhDe1 self, AtClockExtractor clockExtractor);
AtClockExtractor ThaPdhDe1ClockExtractorGet(ThaPdhDe1 self);
uint32 ThaPdhDe1ErrorGeneratorHwId(ThaPdhDe1 self);

/* ID conversion */
uint8 ThaPdhDe1SliceGet(ThaPdhDe1 self);
eAtRet ThaPdhDe1HwIdGet(ThaPdhDe1 self, uint32 phyModule, uint8 *hwSlice, uint8 *hwSts,  uint8 *hwVtg, uint8 *hwVt);
eAtRet ThaPdhDe1HwSlice48AndSts48IdGet(ThaPdhDe1 self, uint8 *hwSlice48Id, uint8 *hwSts48Id,  uint8 *hwVtg, uint8 *hwVt);

AtSurEngine ThaPdhDe1SurEngine(ThaPdhDe1 self);
eAtRet ThaPdhDe1SwFrameTypeSet(ThaPdhDe1 self, uint16 frameType);
uint8 ThaPdhDe1FrameModeSw2Hw(ThaPdhDe1 self, eAtPdhDe1FrameType swFrameMode);

/* Retiming */
eBool ThaPdhDe1RetimingIsSupported(ThaPdhDe1 self);

/*For Map/Demap use */
uint32 ThaPdhDe1MapLineOffset(ThaPdhDe1 self);
uint8 ThaPdhDe1NumDs0Timeslots(AtPdhDe1 self);
AtSdhChannel ThaPdhDe1ShortestVcGet(ThaPdhDe1 self);
void ThaPdhDe1Ds0OffsetFactorForMap(ThaPdhDe1 self, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwVt);
eAtRet ThaPdhDe1HwIdFactorGet(ThaPdhDe1 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice, uint32 *hwVtg, uint32 *hwDe1);

/* SSM */
uint8 ThaPdhDe1RxSaBitSw2Hw(uint8 saBitsMask);
uint8 ThaPdhDe1RxSaBitHw2Sw(uint8 hwValue);
eAtRet ThaPdhDe1HwSsmSaBitsMaskSet(ThaPdhDe1 self, uint8 saBit);
uint8 ThaPdhDe1TxHwSaBitMaskGet(ThaPdhDe1 self);
eBool ThaPdhDe1SsmIsSupported(ThaPdhDe1 self);

/* DS1/J1 fast framing search */
eAtRet ThaPdhDe1FastFrameSearchEnable(ThaPdhDe1 self, eBool enable);
eBool ThaPdhDe1FastFrameSearchIsEnabled(ThaPdhDe1 self);

/* CDR controller */
void ThaPdhDe1CdrControllerDelete(ThaPdhDe1 self);

/* Timeslot DS0 and signalling */
uint32 ThaPdhDe1TimeslotDefaultOffset(ThaPdhDe1 self);
eAtRet ThaPdhDe1HwCasIdleCodeSet(ThaPdhDe1 self, uint32 ts, uint8 idle);
uint8 ThaPdhDe1HwCasIdleCodeGet(ThaPdhDe1 self, uint32 ts);
eAtRet ThaPdhDe1HwRxSlipBufferFrameTypeSet(AtPdhChannel self, uint16 frameType);
uint32 ThaPdhDe1RxSlipBuferCounter(AtPdhChannel self, eBool r2c);

/* Product concretes */
AtPdhDe1 Tha60030022PdhDe1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60070023De1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhDe1 Tha60150011PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60210021PdhDe1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60210031PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60290011PdhDe1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha62290011PdhDe1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60210061PdhDe1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60290022PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60290022PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /*_THAPDHDE1_H*/

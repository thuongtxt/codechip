/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhUtil.c
 *
 * Created Date: Jul 1, 2017
 *
 * Description : PDH util functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FramingShouldBypassOnSatopBinding(AtPdhChannel self, AtPw pseudowire)
    {
    AtPrbsEngine prbsEngine;

    if (pseudowire == NULL)
        return cAtFalse;

    prbsEngine = AtChannelPrbsEngineGet((AtChannel)self);
    if (prbsEngine && AtPrbsEngineIsEnabled(prbsEngine))
        return cAtFalse;

    return cAtTrue;
    }

eBool ThaPdhChannelFramingShouldBypassOnSatopBinding(AtPdhChannel self, AtPw pseudowire)
    {
    if (self)
        return FramingShouldBypassOnSatopBinding(self, pseudowire);
    return cAtFalse;
    }

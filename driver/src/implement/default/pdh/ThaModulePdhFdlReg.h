/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaModulePdhFdlReg.h
 * 
 * Created Date: Nov 30, 2015
 *
 * Description : DS1 FDL registers.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPDHFDLREG_H_
#define _THAMODULEPDHFDLREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer DLK Insertion BOM Control
Reg Addr   : 0x097000-0x0973FF
Reg Formula: 0x097000 +  de3id*32 + de2id*4 + de1id
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This memory is used to control transmitting Bit-Oriented Message (BOM). This memory is accessed indirectly via indirect Access registers

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_Base                                                  0x797000
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl(de3id, de2id, de1id)   (0x097000+(de3id)*32+(de2id)*4+(de1id))
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_WidthVal                                                    32
#define cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_WriteMask                                                  0x0

/*--------------------------------------
BitField Name: DlkBOMEn
BitField Type: RW
BitField Desc: enable transmitting BOM. By setting this field to 1, DLK engine
will rt to insert 6-bit into the BOM pattern 111111110xxxxxx0 with the MSB is
transmitted first. When transmitting finishes, engine automatically clears this
bit to inform to CPU that the BOM message has been already transmitted.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Bit_Start                                             10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Bit_End                                               10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Mask                                              cBit10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_Shift                                                 10
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_MaxVal                                               0x1
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_MinVal                                               0x0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_RstVal                                               0x0

/*--------------------------------------
BitField Name: DlkBOMRptTime
BitField Type: RW
BitField Desc: indicate the number of time that the BOM is transmitted
repeatedly.
BitField Bits: [9:6]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Bit_Start                                         6
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Bit_End                                           9
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Mask                                        cBit9_6
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_Shift                                             6
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_MaxVal                                          0xf
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_MinVal                                          0x0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_RstVal                                          0x0

/*--------------------------------------
BitField Name: DlkBOMMsg
BitField Type: RW
BitField Desc: 6-bit BOM message in pattern BOM 111111110xxxxxx0 in which the
MSB is transmitted first.
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Bit_Start                                             0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Bit_End                                               5
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Mask                                            cBit5_0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_Shift                                                 0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_MaxVal                                             0x3f
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_MinVal                                              0x0
#define cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_RstVal                                              0x0

/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer Outband Loopcode Status
Reg Addr   : 0xE_1800 - 0xE_1BFF
Reg Formula: 0xE_1800 + $ds1
    Where  :
           + $ds1 (0-1023):
Reg Desc   :
These registers are used to report the current captured FDL loopcode status per channel.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta_Base                                                             0x7E1800
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta(ds1)                                                     (0xE1800+(ds1))
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta_WidthVal                                                              32
#define cAf6Reg_ds1_rxfrm_fdl_lpc_sta_WriteMask                                                            0x0

#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_6bitpat_Mask                                                   cBit14_9
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_6bitpat_Shift                                                         9

#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_idle_Mask                                                   cBit5
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_idle_Shift                                                      5
/*--------------------------------------
BitField Name: bom_pat
BitField Type: RO
BitField Desc: captured 8-bit BOM pattern 0xxxxxx0
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Bit_Start                                                         8
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Bit_End                                                          15
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Mask                                                       cBit15_8
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_Shift                                                             8
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_MaxVal                                                         0xff
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_MinVal                                                          0x0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_pat_RstVal                                                          0x0

/*--------------------------------------
BitField Name: bom_ind
BitField Type: RO
BitField Desc: BOM message indication if it is 1
BitField Bits: [4]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Bit_Start                                                         4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Bit_End                                                           4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Mask                                                          cBit4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_Shift                                                             4
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_MaxVal                                                          0x1
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_MinVal                                                          0x0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_bom_ind_RstVal                                                          0x0

/*--------------------------------------
BitField Name: loopcode
BitField Type: RO
BitField Desc: Captured loopcode pattern 0000: 11111111_01111110 0001: line loop
up 0010: line loop down 0011: payload loop up 0100: payload loop down 0101:
smartjack activation 0110: smartjact deactivation 0111: idle
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Bit_Start                                                        0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Bit_End                                                          3
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Mask                                                       cBit3_0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_Shift                                                            0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_MaxVal                                                         0xf
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_MinVal                                                         0x0
#define cAf6_ds1_rxfrm_fdl_lpc_sta_loopcode_RstVal                                                         0x0

/*------------------------------------------------------------------------------
Reg Name   : DS1 Rx Framer Inband Loopcode Status
Reg Addr   : 0xE_2100 - 0xE_24FF
Reg Formula: 0xE_2100 + ds1
    Where  :
           + $ds1 (0-1023):
Reg Desc   :
These registers are used to report the current inband loopcode status per channel.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta_Base                                                          0x7E2100
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta(ds1)                                                  (0xE2100+(ds1))
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta_WidthVal                                                           32
#define cAf6Reg_ds1_rxfrm_inband_lpc_sta_WriteMask                                                         0x0

/*--------------------------------------
BitField Name: iblpc_sta
BitField Type: RO
BitField Desc: bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] ==
0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] ==
0x5  FAC2 DOWN bit [2:0] == 0x7  Change from LoopCode to normal
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Bit_Start                                                    0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Bit_End                                                      2
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Mask                                                   cBit2_0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Shift                                                        0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_MaxVal                                                     0x7
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_MinVal                                                     0x0
#define cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_RstVal                                                     0x0

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPDHFDLREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhNxDs0.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa NxDS0 default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"

#include "../../../generic/pdh/AtPdhNxDs0Internal.h"
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../util/coder/AtCoderUtil.h"

#include "../man/ThaDeviceInternal.h"
#include "../map/ThaModuleMap.h"
#include "../pw/ThaModulePw.h"
#include "../pw/adapters/ThaPwAdapter.h"
#include "../prbs/ThaPrbsEngine.h"

#include "ThaPdhDe1Internal.h"
#include "ThaPdhNxDs0.h"
#include "ThaModulePdh.h"
#include "ThaModulePdhReg.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhNxDs0)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhNxDs0Methods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwIdFactorGet(ThaPdhNxDs0 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1, uint32 *hwDs0)
    {
    eAtRet ret = cAtOk;
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhChannelParentChannelGet((AtPdhChannel)self);

    ret = ThaPdhDe1HwIdFactorGet(de1, phyModule, slice, hwStsInSlice, hwVtg, hwDe1);
    *hwDs0 = AtChannelIdGet((AtChannel)self);
    return ret;
    }

/* Default implementation of register formula */
static uint16 NxDs0DefaultOffset(ThaPdhNxDs0 self)
    {
    return (uint16)AtChannelIdGet((AtChannel)self);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)self;

    m_AtChannelMethods->Enable(self, enable);
    nxDs0->isEnable = enable;

    return cAtOk;
    }

static eBool IsEnabled(AtChannel self)
    {
    return ((AtPdhNxDS0)self)->isEnable;
    }

static eAtRet MapEnable(AtChannel self, ThaModuleAbstractMap mapModule, eBool enable)
    {
    uint32 mask = AtPdhNxDS0BitmapGet((AtPdhNxDS0)self);
    eAtRet ret = cAtOk;
    uint8 slotId = 0;
    AtPdhDe1 de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)self);

    while (mask)
        {
        if (mask & cBit0)
            ret |= ThaModuleAbstractMapDs0Enable(mapModule, de1, slotId, enable);

        /* Net timeslot */
        mask   = mask >> 1;
        slotId = (uint8)(slotId + 1);
        }

    return ret;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return MapEnable(self, mapModule, enable);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return MapEnable(self, mapModule, enable);
    }

static uint8 NumBlocksNeedToBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    uint8 numBlocks;
    uint8 numDs0s;
	AtUnused(encapChannel);

    numDs0s = ThaPdhNumberOfDs0((AtPdhNxDS0)self);
    numBlocks = (uint8)((numDs0s % 2) ? (numDs0s / 2) + 1 : (numDs0s / 2));

    /* Limit range of number of block */
    if (numBlocks > 16)
        numBlocks = 16;

    return numBlocks;
    }

static uint32 TxFramerSignalingIDConversionRegisterAddress(AtPw pw)
    {
    AtDevice device        = (AtDevice)AtChannelDeviceGet((AtChannel)pw);
    ThaModulePw pwModule   = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint8 pwPart           = ThaModulePwPartOfPw(pwModule, pw);
    uint32 offset          = AtChannelHwIdGet((AtChannel)pw) + ThaModulePdhPartOffset((AtModule)pdhModule, pwPart);

    return cThaRegPdhTxFramerSignalingIDConversionControl + offset;
    }

static eAtRet TxFramerSignalingIDConversionSet(ThaPdhNxDs0 self, AtPw pw)
    {
    ThaPdhDe1 de1  = (ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 regAddr = TxFramerSignalingIDConversionRegisterAddress(pw);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 mask;
    uint32 shift;

    mChannelHwLongRead(self, regAddr, longReg, cThaLongRegMaxSize, cAtModulePdh);

    /* DS1/E1 indication */
    mask  = ThaModulePdhTxDE1SigDS1Mask(pdhModule);
    shift = ThaModulePdhTxDE1SigDS1Shift(pdhModule);
    mFieldIns(&longReg[cThaRegPdhTxDE1SigDS1DwordIndex], mask, shift, AtPdhDe1IsE1((AtPdhDe1)de1) ? 0 : 1);

    /* DE1 ID */
    mask  = ThaModulePdhTxDE1SigLineIDMask(pdhModule);
    shift = ThaModulePdhTxDE1SigLineIDShift(pdhModule);
    mFieldIns(&longReg[cThaRegPdhTxDE1SigLineIDDwordIndex], mask, shift, ThaPdhDe1FlatId(de1));

    /* Signaling mask */
    mFieldIns(&longReg[cThaRegPdhTxDE1SigEnbDwordIndex],
              cThaRegPdhTxDE1SigEnbMask,
              cThaRegPdhTxDE1SigEnbShift,
              AtPdhNxDS0BitmapGet((AtPdhNxDS0)self));

    mChannelHwLongWrite(self, regAddr, longReg, cThaLongRegMaxSize, cAtModulePdh);

    return cAtOk;
    }

static void Timeslot16DidBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtPdhDe1 de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)self);

    if (!ThaModulePdhNeedCarryTimeslot16OnPw((ThaModulePdh)AtChannelModuleGet(self)))
        return;

    /* Bind */
    if (pseudowire)
        {
        if (AtPdhDe1SignalingIsEnabled(de1))
            ThaPdhDe1SignalingTransparent((ThaPdhDe1)de1);
        return;
        }

    /* Unbind: re-configure signaling to remove work around point */
    AtPdhDe1SignalingDs0MaskSet(de1, AtPdhDe1SignalingDs0MaskGet(de1));
    AtPdhDe1SignalingEnable(de1, AtPdhDe1SignalingIsEnabled(de1));
    }

static void Timeslot0DidBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    ThaPdhDe1AutoInsertFBit((ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)self), pseudowire ? cAtFalse : cAtTrue);
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);
    ThaPdhDe1 de1;
    ThaModulePdh modulePdh = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePdh);
    m_AtChannelMethods->BindToPseudowire(self, pseudowire);

    /* MAP */
    ret |= ThaModuleAbstractMapBindNxDs0ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhNxDS0)self, pseudowire);
    ret |= ThaModuleAbstractMapBindNxDs0ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhNxDS0)self, pseudowire);

    /* PDH */
    if (pseudowire && ThaModulePdhCasSupported(modulePdh))
        ret |= mMethodsGet(mThis(self))->TxFramerSignalingIDConversionSet(mThis(self), pseudowire);

    /* Check if TX auto AIS for DS1/E1 layer should be enabled or disabled */
    de1 = (ThaPdhDe1)AtPdhNxDS0De1Get((AtPdhNxDS0)self);
    if (pseudowire)
        AtPdhDe1AutoTxAisEnable((AtPdhDe1)de1, ThaPdhDe1AutoTxAisEnabledByDefault(de1, ThaPwAdapterPwGet((ThaPwAdapter)pseudowire)));

    /* In case of DS1, it is not necessary to control timeslot 0 and 16 */
    if (!AtPdhDe1IsE1((AtPdhDe1)de1))
        return ret;

    if (AtPdhNxDS0BitmapGet((AtPdhNxDS0)self) & cBit16)
        Timeslot16DidBindToPseudowire(self, pseudowire);

    if (AtPdhNxDS0BitmapGet((AtPdhNxDS0)self) & cBit0)
        Timeslot0DidBindToPseudowire(self, pseudowire);

    return ret;
    }

static AtEncapBinder EncapBinder(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceEncapBinder(device);
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtPdhDe1 de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)self);

    /* Check if the DE1 which this NxDs0 belong to is bound to an encap channel, return error */
    if (AtChannelBoundEncapChannelGet((AtChannel)de1) != NULL)
        return cAtError;

    /* Hardware binding */
    ret = AtEncapBinderBindNxDs0ToEncapChannel(EncapBinder(self), self, encapChannel);
    if (ret != cAtOk)
        return ret;

    /* Let the super deal with database */
    return m_AtChannelMethods->BindToEncapChannel(self, encapChannel);
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtEncapBinderNxDS0EncapHwIdAllocate(EncapBinder(self), (AtPdhNxDS0)self);
    }

static char *Ds0Bitmap2String(uint32 sigBitmap)
    {
    static const uint8 cMaxNumDs0s = 32;
    static char idItem[8];
    static char bitMapString[150];
    uint8 numContinuousBit1 = 0;
    uint8 bit1FirstAppearPosition = cMaxNumDs0s;
    uint8 i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Clear buffer */
    mMethodsGet(osal)->MemInit(osal, bitMapString, 0, sizeof(bitMapString));

    for (i = 0; i < cMaxNumDs0s; i++)
        {
        /* Continue finding continuous bit 1s */
        if (sigBitmap & (cBit0 << i))
            {
            if (bit1FirstAppearPosition == cMaxNumDs0s)
                bit1FirstAppearPosition = i;
            numContinuousBit1 = (uint8)(numContinuousBit1 + 1);

            /* Continue when there are still some bits */
            if (i < (cMaxNumDs0s - 1))
                continue;
            }

        /* Meet bit 0 or end of bit, time to put bit 1s to list */
        if (numContinuousBit1 == 0)
            continue;

        /* Build ID item */
        mMethodsGet(osal)->MemInit(osal, idItem, 0, sizeof(idItem));
        if (numContinuousBit1 == 1)
            AtSprintf(idItem, "%d", bit1FirstAppearPosition);
        else
            AtSprintf(idItem, "%d-%d", bit1FirstAppearPosition, bit1FirstAppearPosition + numContinuousBit1 - 1);

        /* Put it to the list */
        if (AtStrlen(bitMapString) == 0)
            AtSprintf(bitMapString, "%s", idItem);
        else
            AtSprintf(bitMapString, "%s,%s", bitMapString, idItem);

        /* To start the finding sequence again */
        bit1FirstAppearPosition = cMaxNumDs0s;
        numContinuousBit1 = 0;
        }

    return bitMapString;
    }

static const char *IdString(AtChannel self)
    {
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)self;
    AtPdhDe1 de1;
    static char idString[128];

    de1 = AtPdhNxDS0De1Get(nxds0);
    AtSprintf(idString, "%s.%s", AtChannelIdString((AtChannel)de1), Ds0Bitmap2String(AtPdhNxDS0BitmapGet(nxds0)));

    return idString;
    }

static eAtRet EncapConnectionEnable(AtChannel self, eBool enable)
    {
    uint32 mask;
    AtDevice device = AtChannelDeviceGet(self);
    eAtRet ret = cAtOk;

    mask = AtPdhNxDS0BitmapGet((AtPdhNxDS0)self);

    ret |= ThaModuleAbstractMapDs0EncapConnectionEnable(
                                (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap),
                                (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)self),
                                mask, enable);
    ret |= ThaModuleAbstractMapDs0EncapConnectionEnable(
                                (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap),
                                (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)self),
                                mask, enable);

    return ret;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "nxds0";
    }

static uint8 PppBlockRange(AtChannel self)
    {
    uint8 numDs0 = AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)self);

    if (numDs0 <= 10) return 0;
    if (numDs0 <= 20) return 1;

    return 2;
    }

static ThaModulePdh PdhModule(AtChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet(self);
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (mThis(self)->prbsEngine == NULL)
        mThis(self)->prbsEngine = ThaModulePdhNxDs0PrbsEngineCreate(PdhModule(self), (AtPdhNxDS0)self);
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

static AtModulePrbs PrbsModule(AtChannel self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePrbs);
    }

static void Delete(AtObject self)
    {
    AtModulePrbsEngineObjectDelete(PrbsModule((AtChannel)self), mThis(self)->prbsEngine);
    mThis(self)->prbsEngine = NULL;
    m_AtObjectMethods->Delete(self);
    }

static uint8 MlpppBlockRange(AtChannel self)
    {
	AtUnused(self);
    return 6;
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPdhNxDs0 object = (ThaPdhNxDs0)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(prbsEngine);
    }

static uint32 BoundPwHwIdGet(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    AtPdhNxDS0 nxds0 = (AtPdhNxDS0)self;

    return ThaModuleAbstractMapNxDs0BoundPwHwIdGet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), nxds0);
    }

static uint32 BoundEncapHwIdGet(AtChannel self)
    {
    return AtEncapBinderNxDs0BoundEncapHwIdGet(EncapBinder(self), self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = AtPdhNxDS0BitmapGet((AtPdhNxDS0)self);
    return ThaModuleAbstractMapDs0EncapConnectionEnable(
                                (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap),
                                (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)self),
                                mask, enable);
    }

static eBool TxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = AtPdhNxDS0BitmapGet((AtPdhNxDS0)self);
    return ThaModuleAbstractMapDs0EncapConnectionIsEnabled(
                                (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap),
                                (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)self),
                                mask);
    }

static eAtRet RxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = AtPdhNxDS0BitmapGet((AtPdhNxDS0)self);
    return ThaModuleAbstractMapDs0EncapConnectionEnable(
                                (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap),
                                (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)self),
                                mask, enable);
    }

static eBool RxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = AtPdhNxDS0BitmapGet((AtPdhNxDS0)self);
    return ThaModuleAbstractMapDs0EncapConnectionIsEnabled(
                                (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap),
                                (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)self),
                                mask);
    }

static eAtRet CascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5)
    {
    return ThaPdhNxDs0HwIdFactorGet((ThaPdhNxDs0)self, phyModule, layer1, layer2, layer3, layer4, layer5);
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    if (AtModulePrbsAllChannelsSupported(PrbsModule(self)))
        return (AtChannelPrbsEngineGet(self)) ? cAtTrue : cAtFalse;

    return m_AtChannelMethods->ServiceIsRunning(self);
    }

static uint32 PwTimingRestore(AtChannel self, AtPw pw)
    {
    AtChannel de1 = (AtChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)self);
    return AtChannelPwTimingRestore(de1, pw);
    }

static eAtRet DidBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwDidBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static eAtRet WillBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwWillBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static eAtRet PwCasIdleCode1Set(ThaPdhNxDs0 self, AtPw pw, uint8 abcd1)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(abcd1);
    return cAtErrorNotImplemented;
    }

static uint8 PwCasIdleCode1Get(ThaPdhNxDs0 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static eAtRet PwCasIdleCode2Set(ThaPdhNxDs0 self, AtPw pw, uint8 abcd2)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(abcd2);
    return cAtErrorNotImplemented;
    }

static uint8 PwCasIdleCode2Get(ThaPdhNxDs0 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static eAtRet PwCasAutoIdleEnable(ThaPdhNxDs0 self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PwCasAutoIdleIsEnabled(ThaPdhNxDs0 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static void OverrideAtObject(ThaPdhNxDs0 self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(ThaPdhNxDs0 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, EncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, NumBlocksNeedToBindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, PppBlockRange);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, MlpppBlockRange);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, BoundPwHwIdGet);
        mMethodOverride(m_AtChannelOverride, BoundEncapHwIdGet);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, PwTimingRestore);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, DidBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, WillBindToPseudowire);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(ThaPdhNxDs0 self)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, mMethodsGet(pdhChannel), sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, CascadeHwIdGet);
        }

    mMethodsSet(pdhChannel, &m_AtPdhChannelOverride);
    }

static void Override(ThaPdhNxDs0 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

static void MethodsInit(ThaPdhNxDs0 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NxDs0DefaultOffset);
        mMethodOverride(m_methods, HwIdFactorGet);
        mMethodOverride(m_methods, TxFramerSignalingIDConversionSet);
        mMethodOverride(m_methods, PwCasIdleCode1Set);
        mMethodOverride(m_methods, PwCasIdleCode1Get);
        mMethodOverride(m_methods, PwCasIdleCode2Set);
        mMethodOverride(m_methods, PwCasIdleCode2Get);
        mMethodOverride(m_methods, PwCasAutoIdleEnable);
        mMethodOverride(m_methods, PwCasAutoIdleIsEnabled);
        }
    mMethodsSet(self, &m_methods);
    }

AtPdhNxDS0 ThaPdhNxDs0ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPdhNxDs0));

    /* Super constructor */
    if (AtPdhNxDS0ObjectInit((AtPdhNxDS0)self, de1, mask) == NULL)
        return NULL;

    /* Override */
    Override((ThaPdhNxDs0)self);

    /* Setup methods */
    MethodsInit((ThaPdhNxDs0)self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtPdhNxDS0 ThaPdhNxDs0New(AtPdhDe1 de1, uint32 mask)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhNxDS0 newDs0 = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPdhNxDs0));
    if (newDs0 == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhNxDs0ObjectInit(newDs0, de1, mask);
    }

uint32 ThaPdhNxDs0MaskGet(AtPdhNxDS0 self)
    {
    if (self == NULL)
        return 0;

    return self->mask;
    }

uint8 ThaPdhNumberOfDs0(AtPdhNxDS0 self)
    {
    uint32 mask    = AtPdhNxDS0BitmapGet(self);
    uint32 bitMask = cBit0;
    uint8 numDs0s  = 0;

    while (mask > 0)
        {
        /* This bit is set */
        if (mask & bitMask)
            numDs0s = (uint8)(numDs0s + 1);

        /* Clear this bit to early exit the while loop when no bit is set */
        mask = (mask & ~(bitMask));

        /* To check the next bit */
        bitMask = (bitMask << 1);
        }

    return numDs0s;
    }

AtPrbsEngine ThaPdhNxDs0CachePrbsEngine(ThaPdhNxDs0 self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    return engine;
    }

eAtRet ThaPdhNxDs0HwIdFactorGet(ThaPdhNxDs0 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1, uint32 *hwDs0)
    {
    if (self)
        return mMethodsGet(self)->HwIdFactorGet(self, phyModule, slice, hwStsInSlice, hwVtg, hwDe1, hwDs0);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhNxDs0PwCasIdleCode1Set(ThaPdhNxDs0 self, AtPw pw, uint8 abcd1)
    {
    if (self)
        return mMethodsGet(self)->PwCasIdleCode1Set(self, pw, abcd1);
    return cAtErrorNullPointer;
    }

uint8 ThaPdhNxDs0PwCasIdleCode1Get(ThaPdhNxDs0 self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCasIdleCode1Get(self, pw);
    return 0;
    }

eAtRet ThaPdhNxDs0PwCasIdleCode2Set(ThaPdhNxDs0 self, AtPw pw, uint8 abcd2)
    {
    if (self)
        return mMethodsGet(self)->PwCasIdleCode2Set(self, pw, abcd2);
    return cAtErrorNullPointer;
    }

uint8 ThaPdhNxDs0PwCasIdleCode2Get(ThaPdhNxDs0 self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCasIdleCode2Get(self, pw);
    return 0;
    }

eAtRet ThaPdhNxDs0PwCasAutoIdleEnable(ThaPdhNxDs0 self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCasAutoIdleEnable(self, pw, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhNxDs0PwCasAutoIdleIsEnabled(ThaPdhNxDs0 self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCasAutoIdleIsEnabled(self, pw);
    return cAtFalse;
    }

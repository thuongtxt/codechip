/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe2.c
 *
 * Created Date: Sep 30, 2014
 *
 * Description : PDH DE2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhDe2Internal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaModulePdhForStmReg.h"
#include "../man/ThaDevice.h"
#include "../man/ThaDeviceInternal.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "ThaPdhDe3.h"
#include "ThaPdhDe2.h"
#include "ThaModulePdhInternal.h"
#include "ThaStmModulePdh.h"
#include "../../include/att/AtAttController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhDe2)self)
#define mRead(self, address) mChannelHwRead(self, address, cAtModulePdh)
#define mWrite(self, address, value) mChannelHwWrite(self, address, value, cAtModulePdh)

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhDe2Methods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwIdFactorGet(ThaPdhDe2 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice, uint32 *hwDe2)
    {
    eAtRet ret = cAtOk;
    ThaPdhDe3 de3 = (ThaPdhDe3)AtPdhChannelParentChannelGet((AtPdhChannel)self);

    ret = ThaPdhDe3HwIdFactorGet(de3, phyModule, slice, hwIdInSlice);
    *hwDe2 = AtChannelIdGet((AtChannel)self);
    return ret;
    }

static uint32 PartOffset(ThaPdhDe2 self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)AtPdhChannelParentChannelGet((AtPdhChannel)self);
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cAtModulePdh, (uint8)ThaPdhDe3PartId(de3));
    }

static uint32 M12E12ControlOffset(ThaPdhDe2 self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    return ThaModulePdhM12E12ControlOffset(module, self);
    }

static eAtPdhDe2FrameType DefaultFrameType(ThaPdhDe2 self)
    {
    AtPdhChannel de3 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    eAtPdhDe3FrameType de3FrameType = AtPdhChannelFrameTypeGet(de3);
    
    if ((de3FrameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (de3FrameType == cAtPdhDs3FrmM13Chnl28Ds1s))
        return cAtPdhDs2T1_107Carrying4Ds1s;

    if ((de3FrameType == cAtPdhDs3FrmCbitChnl21E1s) ||
        (de3FrameType == cAtPdhDs3FrmM13Chnl21E1s))
        return cAtPdhDs2G_747Carrying3E1s;

    if (de3FrameType == cAtPdhE3FrmG751Chnl16E1s)
        return cAtPdhE2G_742Carrying4E1s;

    return cAtPdhDe2FrameUnknown;
    }

static eAtRet LofUnforce(ThaPdhDe2 self)
    {
    uint32 regAddr = cThaRegRxM12E12Ctrl + M12E12ControlOffset(mThis(self));
    uint32 regVal  = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRegRxM12E12CtrlLofFrcMask, cThaRegRxM12E12CtrlLofFrcShift, 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet AlarmForwardingEnable(ThaPdhDe2 self, eBool enable)
    {
    uint32 regAddr = cThaRegRxM12E12Ctrl + M12E12ControlOffset(self);
    uint32 regVal = mRead(self, regAddr);

    mRegFieldSet(regVal, cThaRegRxM12E12AisFwdDisCtrl, enable ? 0 : 1); /* HW default 0 to enable. */
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool AlarmForwardingIsEnabled(ThaPdhDe2 self)
    {
    uint32 regAddr = cThaRegRxM12E12Ctrl + M12E12ControlOffset(self);
    uint32 regVal = mRead(self, regAddr);
    return (regVal & cThaRegRxM12E12AisFwdDisCtrlMask) ? cAtFalse : cAtTrue;
    }

static uint32 DefaultAlarmAffecting(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtPdhDe2AlarmLof;
    }

static eAtRet AlarmAffectingEnable(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    if (!AtPdhChannelCanChangeAlarmAffecting(self, alarmType, enable))
        return cAtErrorModeNotSupport;

    if (alarmType & cAtPdhDe2AlarmLof)
        return AlarmForwardingEnable(mThis(self), enable);

    return cAtOk;
    }

static eBool AlarmAffectingIsEnabled(AtPdhChannel self, uint32 alarmType)
    {
    if (alarmType & cAtPdhDe2AlarmLof)
        return AlarmForwardingIsEnabled(mThis(self));

    return cAtFalse;
    }

static eBool CanChangeAlarmAffecting(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    AtUnused(enable);
    if (alarmType & DefaultAlarmAffecting(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AlarmAffectingMaskGet(AtPdhChannel self)
    {
    if (AlarmForwardingIsEnabled(mThis(self)))
        return cAtPdhDe2AlarmLof;

    return 0;
    }

static eAtRet DefaultSet(ThaPdhDe2 self)
    {
    eAtRet ret = cAtOk;
    
    ret |= LofUnforce(self);
    ret |= AtPdhChannelFrameTypeNoLockSet((AtPdhChannel)self, DefaultFrameType(self));

    /* Clear all interrupt mask */
    AtChannelInterruptMaskSet((AtChannel)self, AtChannelInterruptMaskGet((AtChannel)self), 0x0);

    /* Disable AIS forwarding from DS2/E2 toward DS1/E1 */
    ret |= AtPdhChannelAlarmAffectingEnable((AtPdhChannel)self, cAtPdhDe2AlarmLof, cAtFalse);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet((ThaPdhDe2)self);
    }

static eBool FrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
    AtUnused(self);
    return (mOutOfRange(frameType, cAtPdhDs2T1_107Carrying4Ds1s, cAtPdhE2G_742Carrying4E1s)) ? cAtFalse : cAtTrue;
    }

static uint8 FrameTypeSw2Hw(uint16 frameType)
    {
    if (frameType == cAtPdhDs2T1_107Carrying4Ds1s) return 0;
    if (frameType == cAtPdhDs2G_747Carrying3E1s)   return 1;
    if (frameType == cAtPdhE2G_742Carrying4E1s)    return 2;

    return 0;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = cAtOk;
    uint32 regAddr, regVal;
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCdr);

    if (AtChannelShouldPreventReconfigure((AtChannel)self) && (AtPdhChannelFrameTypeGet(self) == frameType))
        return cAtOk;

    if (!AtPdhChannelFrameTypeIsSupported(self, frameType))
        return cAtErrorInvlParm;
        
    AtPdhChannelSubChannelsInterruptDisable(self, cAtTrue);

    /* Clean up HW before change to new frame */
    AtPdhChannelSubChannelsHardwareCleanup(self);

    ret |= ThaCdrDe2PldSet(cdrModule, self, frameType);
    mThis(self)->frameType = frameType;

    regAddr = cThaRegRxM12E12Ctrl + M12E12ControlOffset(mThis(self));
    regVal  = mRead(self, regAddr);
    mRegFieldSet(regVal, cThaRxDS2E2Md, FrameTypeSw2Hw(frameType));
    mWrite(self, regAddr, regVal);

    regAddr = cThaRegTxM12E12Ctrl + M12E12ControlOffset(mThis(self));
    regVal  = mRead(self, regAddr);
    mRegFieldSet(regVal, cThaRxDS2E2Md, FrameTypeSw2Hw(frameType));

    mWrite(self, regAddr, regVal);

    /* NOTE: Set M12E12 Mux must be done before configure RX DE1 framer:
     * The De2De1 will handle this because it E13IDconversion need De1ID */
    ret |= m_AtPdhChannelMethods->FrameTypeSet(self, frameType);

    return ret;
    }

static uint16 FrameTypeGet(AtPdhChannel self)
    {
	if (AtChannelInAccessible((AtChannel)self))
        return m_AtPdhChannelMethods->FrameTypeGet(self);

    return mThis(self)->frameType;
    }

static eAtRet RxLofForce(AtChannel self, eBool force)
    {
    uint32 regAddr = cThaRegRxM12E12Ctrl + M12E12ControlOffset(mThis(self));
    uint32 regVal  = mRead(self, regAddr);

    mFieldIns(&regVal, cThaRegRxM12E12CtrlLofFrcMask, cThaRegRxM12E12CtrlLofFrcShift, force ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool RxLofForced(AtChannel self)
    {
    uint32 regAddr = cThaRegRxM12E12Ctrl + M12E12ControlOffset(mThis(self));
    return (mRead(self, regAddr) & cThaRegRxM12E12CtrlLofFrcMask) ? cAtTrue : cAtFalse;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (alarmType & cAtPdhDe2AlarmLof)
        return RxLofForce(self, cAtTrue);

    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (alarmType & cAtPdhDe2AlarmLof)
        return RxLofForce(self, cAtFalse);

    return cAtErrorModeNotSupport;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 alarms = 0;

    if (RxLofForced(self))
        alarms |= cAtPdhDe2AlarmLof;

    return alarms;
    }

static uint32 DefaultOffset(ThaPdhDe2 self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    uint8 slice, hwIdInSlice;
    AtPdhChannel de3 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    int32 ret;

    ThaPdhChannelHwIdGet(de3, cAtModulePdh, &slice, &hwIdInSlice);
    ret = ((int32)PartOffset((ThaPdhDe2)self) + (int32)hwIdInSlice + ThaModulePdhSliceOffset(pdhModule, (uint32)slice));
    return (uint32)ret;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint32 swAlarms = 0;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    /* Read alarm from hardware */
    regAddr = ThaModulePdhRxM12E12FrmChnAlarmCurStatusRegister(pdhModule) + DefaultOffset((ThaPdhDe2)self);
    regVal  = mRead(self, regAddr);

    /* Common alarms */
    if (regVal & cThaRegRxM12E12FrmChnAlarmCurStatusMask(AtChannelIdGet(self)))
        swAlarms |= cAtPdhDe2AlarmLof;

    return swAlarms;
    }

static uint32 InterruptRead2Clear(AtChannel self, eBool clear)
    {
    uint32 swStatus = 0;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    uint32 regAddr = ThaModulePdhRxM12E12FrmChnIntrChngStatusRegister(pdhModule) + DefaultOffset((ThaPdhDe2)self);
    uint32 regVal  = mRead(self, regAddr);

    /* Common alarms */
    if (regVal & cThaRegRxM12E12FrmChnIntrChngStatusMask(AtChannelIdGet(self)))
        swStatus |= cAtPdhDe2AlarmLof;

    /* Clear */
    if (clear)
        mWrite(self, regAddr, cThaRegRxM12E12FrmChnIntrChngStatusMask(AtChannelIdGet(self)));

    return swStatus;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return InterruptRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return InterruptRead2Clear(self, cAtTrue);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtPdhDe2AlarmLof;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr, regVal;
    uint32 de2Idx = AtChannelIdGet(self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    regAddr = ThaModulePdhRxM12E12FrmChnIntrEnCtrlRegister(pdhModule) + DefaultOffset((ThaPdhDe2)self);
    regVal  = mRead(self, regAddr);

    if (defectMask & cAtPdhDe2AlarmLof)
        mFieldIns(&regVal,
                  cThaRegRxM12E12FrmChnIntrEnCtrlMask(de2Idx),
                  cThaRegRxM12E12FrmChnIntrEnCtrlShift(de2Idx),
                  (enableMask & cAtPdhDe2AlarmLof) ? 1 : 0);

    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint32 swMasks = 0;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    uint32 de2Idx = AtChannelIdGet(self);

    regAddr = ThaModulePdhRxM12E12FrmChnIntrEnCtrlRegister(pdhModule) + DefaultOffset((ThaPdhDe2)self);
    regVal  = mRead(self, regAddr);

    if (regVal & cThaRegRxM12E12FrmChnIntrEnCtrlMask(de2Idx))
        swMasks |= cAtPdhDe2AlarmLof;

    return swMasks;
    }

static uint32 DefectsCauseAlarmForwarding(AtPdhChannel self)
    {
    if (AlarmForwardingIsEnabled(mThis(self)))
        return m_AtPdhChannelMethods->DefectsCauseAlarmForwarding(self);
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPdhDe2 object = (ThaPdhDe2)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(frameType);
    mEncodeNone(attController);
    }

static eAtRet CascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5)
    {
    *layer4 = 0;
    *layer5 = 0;
    return ThaPdhDe2HwIdFactorGet((ThaPdhDe2)self, phyModule, layer1, layer2, layer3);
    }

static AtObjectAny AttController(AtChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        mThis(self)->attController =mMethodsGet(module)->De2AttControllerCreate(module, self);
        if (mThis(self)->attController)
            {
            AtAttControllerSetUp(mThis(self)->attController);
            AtAttControllerInit(mThis(self)->attController);
            }
        }
    return mThis(self)->attController;
    }

static void DeleteAttController(AtObject self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);
    mThis(self)->attController = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteAttController(self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void MethodsInit(ThaPdhDe2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwIdFactorGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtPdhDe2 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPdhDe2 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, AttController);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe2 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeGet);
        mMethodOverride(m_AtPdhChannelOverride, CascadeHwIdGet);
        mMethodOverride(m_AtPdhChannelOverride, DefectsCauseAlarmForwarding);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingMaskGet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void Override(AtPdhDe2 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhDe2);
    }

AtPdhDe2 ThaPdhDe2ObjectInit(AtPdhDe2 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhDe2ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup */
    MethodsInit((ThaPdhDe2)self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe2 ThaPdhDe2New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe2 newDe2 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe2 == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhDe2ObjectInit(newDe2, channelId, module);
    }

eAtRet ThaPdhDe2HwIdFactorGet(ThaPdhDe2 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice, uint32 *hwDe2)
    {
    if (self)
        return mMethodsGet(self)->HwIdFactorGet(self, phyModule, slice, hwIdInSlice, hwDe2);
    return cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe2De1.c
 *
 * Created Date: Feb 4, 2015
 *
 * Description : DE1 inside a DE2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/pdh/AtPdhDe2Internal.h"
#include "ThaPdhVcDe1Internal.h"
#include "../sdh/ThaSdhVc.h"
#include "../cdr/ThaModuleCdr.h"
#include "../man/ThaDeviceInternal.h"
#include "../map/ThaModuleStmMap.h"
#include "AtPdhDe1.h"
#include "ThaPdhDe2De1.h"
#include "ThaStmModulePdh.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "ThaModulePdhForStmReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tThaPdhDe1Methods    m_ThaPdhDe1Override;
static tThaPdhVcDe1Methods  m_ThaPdhVcDe1Override;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;
static const tThaPdhVcDe1Methods  *m_ThaPdhVcDe1Methods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwIdFactorGet(ThaPdhDe1 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1)
    {
    uint8 hwIdSlice, hwDe3InSlice, hwDe2Id, hwDe1Id;
    ThaPdhDe2De1HwIdGet(self, &hwIdSlice, &hwDe3InSlice, &hwDe2Id, &hwDe1Id, phyModule);
    *slice = (uint32)hwIdSlice;
    *hwStsInSlice = (uint32)hwDe3InSlice;
    *hwVtg = (uint32)hwDe2Id;
    *hwDe1 = (uint32)hwDe1Id;
    return cAtOk;
    }

static AtSdhChannel ShortestVcGet(ThaPdhDe1 self)
    {
    AtPdhChannel de2 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    return (AtSdhChannel)AtPdhChannelVcInternalGet(AtPdhChannelParentChannelGet(de2));
    }

static eAtRet HwIdConvert(ThaPdhDe1 self, uint32 phyModule, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    return ThaPdhDe2De1HwIdGet((ThaPdhDe1)self, hwSlice, hwSts, hwVtg, hwVt, phyModule);
    }

static eBool FrameModeCompatible(AtPdhChannel self, uint16 frameType)
    {
    AtPdhChannel de2 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    eAtPdhDe2FrameType de2FrameType = AtPdhChannelFrameTypeGet(de2);

    if (AtPdhDe1FrameTypeIsE1(frameType))
        {
        if ((de2FrameType == cAtPdhDs2G_747Carrying3E1s) || (de2FrameType == cAtPdhE2G_742Carrying4E1s))
            return cAtTrue;
        return cAtFalse;
        }

    return (de2FrameType == cAtPdhDs2T1_107Carrying4Ds1s) ? cAtTrue : cAtFalse;
    }

static eBool FrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
    if (mOutOfRange(frameType, cAtPdhDs1J1UnFrm, cAtPdhE1MFCrc))
        return cAtFalse;

    return FrameModeCompatible(self, frameType);
    }

static const char *IdString(AtChannel self)
    {
    static char idString[16];
    AtChannel de2 = (AtChannel)AtPdhChannelParentChannelGet((AtPdhChannel)self);
    AtSnprintf(idString, sizeof(idString), "%s.%d", AtChannelIdString(de2), AtChannelIdGet(self) + 1);
    return idString;
    }

static uint8 PartId(ThaPdhDe1 self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet VtMapModeSet(ThaPdhVcDe1 self, uint16 frameType)
    {
    AtUnused(self);
    AtUnused(frameType);
    return cAtOk;
    }

static uint8 DefaultFrameMode(AtPdhChannel self)
    {
    AtPdhChannel de2 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    eAtPdhDe2FrameType de2FrameType = AtPdhChannelFrameTypeGet(de2);

    if (de2FrameType == cAtPdhDs2T1_107Carrying4Ds1s)
        return cAtPdhDs1J1UnFrm;

    if ((de2FrameType == cAtPdhDs2G_747Carrying3E1s) ||
        (de2FrameType == cAtPdhE2G_742Carrying4E1s))
        return cAtPdhE1UnFrm;

    return cAtPdhDe1FrameUnknown;
    }

static eBool FrameTypeIsSupportForVc1xMapping(ThaPdhVcDe1 self, uint16 frameType)
    {
    AtUnused(self);
    AtUnused(frameType);
    return cAtTrue;
    }

static ThaCdrController CdrControllerCreate(ThaPdhDe1 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrDe2De1CdrControllerCreate(cdrModule, (AtPdhDe1)self);
    }

static uint32 MapLineOffset(ThaPdhDe1 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleStmMap mapModule = (ThaModuleStmMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleStmMapPdhDe2De1MapLineOffset(mapModule, (AtPdhDe1)self);
    }

static uint32 M12E12MuxOffset(AtPdhChannel self)
    {
    uint8 slice, hwIdInSlice;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);

    ThaPdhChannelHwIdGet(self, cAtModulePdh, &slice, &hwIdInSlice);
    return (uint32)((int32)hwIdInSlice + ThaModulePdhSliceOffset(pdhModule, slice));
    }

static void RxM12E12MuxSet(AtPdhChannel self, uint16 frameType)
    {
    uint32 regAddr, regVal, fieldVal;
    uint8 hwSlice, hwId, de2Id, de1Id;
    AtPdhChannel de2 = AtPdhChannelParentChannelGet(self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)self, &hwSlice, &hwId, &de2Id, &de1Id, cAtModulePdh);

    if (AtPdhChannelTypeGet(de2) == cAtPdhChannelTypeE2)
        {
        ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleCdr);
        ThaCdrDe1De2PldSet(cdrModule, self, de2Id, frameType);
        }

    fieldVal = AtPdhDe1FrameTypeIsE1(frameType) ? 1:0;
    regAddr  = ThaModulePdhRxM12E12MuxRegister(pdhModule) + M12E12MuxOffset(self);
    regVal   = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal, (cBit1_0 << (de2Id * 2)), (de2Id * 2), fieldVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    /* Check that this frameType must be compatible with its DE2*/
    if (!FrameModeCompatible(self, frameType))
        return cAtErrorInvlParm;

    RxM12E12MuxSet(self, frameType);

    return m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, IdString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe1 self)
    {
    AtPdhChannel de1 = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(de1);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, DefaultFrameMode);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeIsSupported);
        }

    mMethodsSet(de1, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhVcDe1(AtPdhDe1 self)
    {
    ThaPdhVcDe1 de1 = (ThaPdhVcDe1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhVcDe1Methods = mMethodsGet(de1);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhVcDe1Override, de1, sizeof(m_ThaPdhVcDe1Override));

        mMethodOverride(m_ThaPdhVcDe1Override, FrameTypeIsSupportForVc1xMapping);
        mMethodOverride(m_ThaPdhVcDe1Override, VtMapModeSet);
        }

    mMethodsSet(de1, &m_ThaPdhVcDe1Override);
    }

static void OverrideThaPdhDe1(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, mMethodsGet(de1), sizeof(tThaPdhDe1Methods));

        mMethodOverride(m_ThaPdhDe1Override, PartId);
        mMethodOverride(m_ThaPdhDe1Override, CdrControllerCreate);
        mMethodOverride(m_ThaPdhDe1Override, MapLineOffset);
        mMethodOverride(m_ThaPdhDe1Override, ShortestVcGet);
        mMethodOverride(m_ThaPdhDe1Override, HwIdFactorGet);
        mMethodOverride(m_ThaPdhDe1Override, HwIdConvert);
        }

    mMethodsSet(de1, &m_ThaPdhDe1Override);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    OverrideThaPdhDe1(self);
    OverrideThaPdhVcDe1(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhDe2De1);
    }

AtPdhDe1 ThaPdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (ThaPdhVcDe1ObjectInit(self, NULL, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    AtChannelIdSet((AtChannel)self, channelId);

    return self;
    }

AtPdhDe1 ThaPdhDe2De1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return ThaPdhDe2De1ObjectInit(newDe1, channelId, module);
    }

eAtRet ThaPdhDe2De1HwIdGet(ThaPdhDe1 self, uint8 *hwSlice, uint8 *hwStsInSlice, uint8 *hwDe2Id, uint8 *hwDe1Id, eAtModule phyModule)
    {
    AtPdhChannel de2 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    AtPdhChannel de3 = AtPdhChannelParentChannelGet(de2);
    uint32 de2Id = AtChannelIdGet((AtChannel)de2);
    uint32 de1Id = AtChannelIdGet((AtChannel)self);
    uint32 u32Slice, u32Sts;
    eAtRet ret = ThaPdhDe3HwIdFactorGet((ThaPdhDe3)de3, phyModule, &u32Slice, &u32Sts);

    /* Initialize */
    *hwDe2Id = 0;
    *hwDe1Id = 0;
    *hwSlice = (uint8)u32Slice;
    *hwStsInSlice = (uint8)u32Sts;

    if (ret != cAtOk)
        return ret;

    if (AtPdhChannelTypeGet(de2) == cAtPdhChannelTypeE2)
        {
        uint32 flatId = (de2Id * 4) + de1Id;
        de2Id = flatId / 3;
        de1Id = flatId % 3;
        }

    *hwDe2Id = (uint8)de2Id;
    *hwDe1Id = (uint8)de1Id;

    return cAtOk;
    }

uint8 ThaPdhDe2De1HwDe2IdGet(ThaPdhDe1 self)
    {
    AtPdhChannel de2 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    uint32 de2Id = AtChannelIdGet((AtChannel)de2);
    uint32 de1Id = AtChannelIdGet((AtChannel)self);

    if (AtPdhChannelTypeGet(de2) == cAtPdhChannelTypeE2)
        {
        uint32 flatId = (de2Id * 4) + de1Id;
        return (uint8)(flatId / 3);
        }

    return (uint8)de2Id;
    }

uint8 ThaPdhDe2De1HwDe1IdGet(ThaPdhDe1 self)
    {
    AtPdhChannel de2 = AtPdhChannelParentChannelGet((AtPdhChannel)self);
    uint32 de2Id = AtChannelIdGet((AtChannel)de2);
    uint32 de1Id = AtChannelIdGet((AtChannel)self);

    if (AtPdhChannelTypeGet(de2) == cAtPdhChannelTypeE2)
        {
        uint32 flatId = (de2Id * 4) + de1Id;
        return (uint8)(flatId % 3);
        }

    return (uint8)de1Id;
    }

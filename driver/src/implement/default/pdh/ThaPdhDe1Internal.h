/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe1Internal.h
 * 
 * Created Date: Jun 21, 2013
 *
 * Description : PDH DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE1INTERNAL_H_
#define _THAPDHDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSurEngine.h"
#include "../../../generic/pdh/AtPdhDe1Internal.h"
#include "ThaPdhDe1.h"
#include "AtSdhChannel.h"
#include "retimingcontrollers/ThaPdhRetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaMaxE1TimeslotNum  32
#define cThaMaxDS1TimeslotNum 24

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Additional methods of this class */
typedef struct tThaPdhDe1Methods
    {
    eAtRet (*HwIdFactorGet)(ThaPdhDe1 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice, uint32 *hwVtg, uint32 *hwDe1);
    uint32 (*FlatId)(ThaPdhDe1 self, uint32 phyModule);
    uint32 (*RxForceAbleAlarms)(ThaPdhDe1 self);
    uint32 (*TxForceAbleAlarms)(ThaPdhDe1 self);
    eAtRet (*AutoTxAisEnable)(ThaPdhDe1 self, eBool enable);
    eBool  (*AutoTxAisIsEnabled)(ThaPdhDe1 self);
    eBool  (*AutoTxAisEnabledByDefault)(ThaPdhDe1 self, AtPw pw);
    uint8 (*SliceGet)(ThaPdhDe1 self);
    eAtRet (*HwIdConvert)(ThaPdhDe1 self, uint32 phyModule, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt);
    eAtRet (*HwFrameTypeAtModuleMapSet)(ThaPdhDe1 self, uint16 frameType);
    eBool (*ShouldConsiderLofWhenLos)(ThaPdhDe1 self);
    eBool (*ShouldPreserveService)(ThaPdhDe1 self);

    /*for MAP/DEMAP*/
    uint32 (*MapLineOffset)(ThaPdhDe1 self);
    AtSdhChannel (*ShortestVcGet)(ThaPdhDe1 self);
    void (*Ds0OffsetFactorForMap)(ThaPdhDe1 self, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwVt);

    /* Get offsets */
    uint32 (*Ds1E1J1DefaultOffset)(ThaPdhDe1 de1);
    uint32 (*De1ErrCounterOffset)(ThaPdhDe1 de1, eBool r2cEn);
    uint32 (*EncapDs0BitMaskGet)(ThaPdhDe1 self);
    uint32 (*SatopDs0BitMaskGet)(ThaPdhDe1 self);
    uint8 (*PartId)(ThaPdhDe1 self);
    ThaCdrController (*CdrControllerCreate)(ThaPdhDe1 de1);
    eAtRet (*DefaultThreshold)(ThaPdhDe1 self, tThaCfgPdhDs1E1FrmRxThres *threshold);
    uint32 (*TimeslotDefaultOffset)(ThaPdhDe1 self);

    /* FDL BOM. */
    eAtRet (*TxHwBomSet)(ThaPdhDe1 self, uint8 message, uint32 threshold);
    uint8 (*TxHwBomGet)(ThaPdhDe1 self);
    uint8 (*RxHwBomGet)(ThaPdhDe1 self);
    uint8 (*RxHwCurrentBomGet)(ThaPdhDe1 self);
    uint32 (*TxHwBomThresGet)(ThaPdhDe1 self);

    /* Inband loopcode */
    uint8 (*RxHwLoopcodeGet)(ThaPdhDe1 self);

    /* Retiming */
    ThaPdhRetimingController (*RetimingControllerCreate)(ThaPdhDe1 self);
    eBool (*RetimingIsSupported)(ThaPdhDe1 self);

    /* E1 CRC SSM */
    eAtRet (*SsmHwEnable)(ThaPdhDe1 self, eBool enable);
    eBool  (*SsmHwIsEnabled)(ThaPdhDe1 self);

    /* CAS IDLE */
    eAtRet (*HwCasIdleCodeSet)(ThaPdhDe1 self, uint32 ts, uint8 idle);
    uint8 (*HwCasIdleCodeGet)(ThaPdhDe1 self, uint32 ts);

    /* Bit fields polymorphism for "DS1/E1/J1 Rx Framer HW Status"*/
    uint32 (*RxFrmrStatE1SsmMask)(ThaPdhDe1 self);
    uint8  (*RxFrmrStatE1SsmShift)(ThaPdhDe1 self);

    /* Bit fields polymorphism for "DS1/E1/J1 Tx Framer Control"*/
    uint32 (*TxFrmrE1SaCfgMask)(ThaPdhDe1 self);
    uint8  (*TxFrmrE1SaCfgShift)(ThaPdhDe1 self);
    }tThaPdhDe1Methods;

typedef struct tThaPdhDe1
    {
    tAtPdhDe1 super;
    const tThaPdhDe1Methods *methods;

    /* Private data */
    uint32 timeslotBitmap;  /* Store number of TS of De1 enable */
    AtList allNxDs0s;  /* This pointer is to store all NxDs0 groups */
    ThaCdrController cdrController;
    AtPrbsEngine prbsEngine;
    AtClockExtractor clockExtractor; /* Save to update clock extractor when timing mode is changed */
    AtPrbsEngine linePrbsEngine;

    /* For signaling */
    uint32 signalingBitMap;          /* Store TS bit map which TS16 carry signaling */
    eBool signalingEnabled;

    AtSurEngine surEngine;

    /* Store frame type to compare when re-configure frame type,
     * can not get from HW because default value in HW may coincide with a frame type */
    uint16 frameType;

    /* Private data */
    AtPdhPrmController prmController;
    AtObjectAny attController;
    ThaPdhRetimingController retimingController;
    uint8 txAisExplicitlyForced;
    }tThaPdhDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 ThaPdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);
AtPdhDe1 ThaPdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);
eAtRet ThaPdhDe1DbSignalingEnabled(ThaPdhDe1 self, eBool en);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE1INTERNAL_H_ */


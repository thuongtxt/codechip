/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaModulePdhReg.h
 *
 * Created Date: Sep 7, 2012
 *
 * Author      : ntdung
 *
 * Description : This file contains declarations of PDH registers.
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

#ifndef THAMODULEPDHREG_HEADER
#define THAMODULEPDHREG_HEADER

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*============================================================================
*                       GLOBAL REGISTERS (MODULE REGISTERS)                   *
* ============================================================================*/
/*------------------------------------------------------------------------------
Reg Name: PDH Global Control
Reg Addr: 0x00700000 - 0x00700000
          The address format for these registers is 0x00700000
Reg Desc: This is the global configuration register for the PDH
------------------------------------------------------------------------------*/
#define cThaRegPdhGlbCtrl                           0x700000

#define cThaRegPdhGlbCtrlRstVal                     0x00000000
#define cThaRegPdhGlbCtrlNoneEditableFieldsMask		(cBit31_8 | cBit5 | cBit4 | cBit0)

/*============================================================================
*                       CHANNEL REGISTERS (MODULE REGISTERS)                  *
* ============================================================================*/
/*------------------------------------------------------------------------------
 *                           CONFIGURATION
 *------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Tx Framer Signaling Insertion Control
Reg Addr: 0x00794800 - 0x0079497B (for 336)
          The address format for these registers is 0x00794800 + 32*de3id +
          4*de2id + de1id
          Where: de3id: 0 - 11
          de2id: 0 - 6
          de1id: 0 - 3
Reg Desc: DS1/E1/J1 Rx framer signaling insertion control is used to configure
          for signaling operation modes at the DS1/E1 framer.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1TxFrmrSigingInsCtrl                                  0x794800

#define cThaRegDS1E1J1TxFrmrSigingInsCtrlNoneEditableFieldsMask            cBit31

/*--------------------------------------
BitField Name: TxDE1SigMfrmEn
BitField Type: R/W
BitField Desc: Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling
               multiframe to the incoming data flow. No applicable for DS1
--------------------------------------*/
#define cThaTxDE1SigMfrmEnMask                         cBit30
#define cThaTxDE1SigMfrmEnShift                        30

/*--------------------------------------
BitField Name: TXDE1SigBypass[29:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaTXDE1SigBypassMask                         cBit29_0
#define cThaTXDE1SigBypassShift                        0
#define cThaTXDE1SigBypassMaxVal                       0x3FFFFFFF

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer Control
Reg Addr: 0x00754000 - 0x0075401F (for 32 lines)
          The address format for these registers is 0x609800 + 32*de3id +
          4*de2id + de1id
          Where: de3id: 0 - 11
          de2id: 0 - 6
          de1id: 0 - 3
Reg Desc: DS1/E1/J1 Rx framer control is used to configure for Frame mode
          (DS1, E1, J1) at the DS1/E1 framer.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrCtrl                              0x00754000
#define cThaRegDS1E1J1RxFrmrCtrlNoneEditableFieldsMask  	  cBit31_26

#define cThaRxDE1LOFFrwDisMask                      cBit31
#define cThaRxDE1LOFFrwDisShift                     31

/*--------------------------------------
BitField Name: RxDe1SsmThreshold
BitField Type: R/W
BitField Desc: Set threshold to detect SSM at Rx side
Note: this RxDe1SsmThreshold field becasem obsolete since fpga 3.4.0.0 (ds1 card) and 3.3.0.2 (ds3 card)
--------------------------------------*/
#define cThaRxDE1SsmThresholdMask                   cBit31_28
#define cThaRxDE1SsmThresholdShift                  28

/*--------------------------------------
BitField Name: RxDE1FrcAISLbit
BitField Type: R/W
BitField Desc: Force AIS alarm here,L-bit, data all one to PSN when Line/Payload Remote Loopback
--------------------------------------*/
#define cThaRxDE1FrcAISLbitMask                     cBit30
#define cThaRxDE1FrcAISLbitShift                    30

/*--------------------------------------
BitField Name: RxDE1FrcAISLineDwn
BitField Type: R/W
BitField Desc: Set 1 to force AIS Line all-ones downstream
--------------------------------------*/
#define cThaRxDE1FrcAISLineDwnMask                  cBit29
#define cThaRxDE1FrcAISLineDwnShift                 29

/*--------------------------------------
BitField Name: RxDE1FrcAISPldDwn
BitField Type: R/W
BitField Desc: Set 1 to force AIS Payload all-ones downstream
--------------------------------------*/
#define cThaRxDE1FrcAISPldDwnMask                   cBit28
#define cThaRxDE1FrcAISPldDwnShift                  28

/*--------------------------------------
BitField Name: RxDe1FrcLos
BitField Type: R/W
BitField Desc: Set 1 to force LOS on RX direction
--------------------------------------*/
#define cThaRxDE1FrcLosMask                         cBit27
#define cThaRxDE1FrcLosShift                        27

/*--------------------------------------
BitField Name: RxDE1MFASChkEn
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDE1MFASChkEnMask                     cBit26
#define cThaRxDE1MFASChkEnShift                    26

/*--------------------------------------
BitField Name: RxDE1LomfDstrEn
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDE1LomfDstrEnMask                    cBit26
#define cThaRxDE1LomfDstrEnShift                   26

/*--------------------------------------
BitField Name: RxDE1LosCntThres
BitField Type: R/W
BitField Desc: Threshold of LOS monitoring block
--------------------------------------*/
#define cThaRxDE1LosCntThresMask                     cBit25_23
#define cThaRxDE1LosCntThresShift                    23

/*--------------------------------------
BitField Name: RxDE1MonOnly
BitField Type: R/W
BitField Desc: Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer
--------------------------------------*/
#define cThaRxDE1MonOnlyMask                     cBit22
#define cThaRxDE1MonOnlyShift                    22

/*--------------------------------------
BitField Name: RxDE1LocalLineLoop
BitField Type: R/W
BitField Desc: Set 1 to enable Local Line loop back
--------------------------------------*/
#define cThaRxDE1LocalLineLoopMask                     cBit21
#define cThaRxDE1LocalLineLoopShift                    21

/*--------------------------------------
BitField Name: RxDE1LocalPayLoop
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxDE1LocalPayLoopMask                      cBit20
#define cThaRxDE1LocalPayLoopShift                     20

/*--------------------------------------
BitField Name: RxDE1E1SaCfg[2:0]
BitField Type: R/W
BitField Desc: In case E1 Sa used for FDL configuration
                0x0: No Sa bit used
                0x3: Sa4 is used for SSM
                0x4: Sa5 is used for SSM
                0x5: Sa6 is used for SSM
                0x6: Sa7 is used for SSM
                0x7: Sa8 is used for SSM
--------------------------------------*/
#define cThaRxDE1SaCfgMask                           cBit19_17
#define cThaRxDE1SaCfgShift                          17

/*--------------------------------------
BitField Name: RxDS1J1FastSrchDis
BitField Type: R/W
BitField Desc: Disable DS1/J1 fast searching
--------------------------------------*/
#define cThaRxDs1J1FastSrchDisMask                    cBit18
#define cThaRxDs1J1FastSrchDisShift                   18

/*--------------------------------------
BitField Name: RxDS1DlkEn
BitField Type: R/W
BitField Desc: Set 1 to enable Rx DLK
--------------------------------------*/
#define cThaRxDs1DlkEnMask                             cBit17
#define cThaRxDs1DlkEnShift                            17

/*--------------------------------------
BitField Name: RxDE1LofThres[3:0]
BitField Type: R/W
BitField Desc: Threshold for FAS error to declare LOF in DS1/E1/J1 mode
--------------------------------------*/
#define cThaRxDE1LofThresMask                          cBit16_13
#define cThaRxDE1LofThresShift                         13

/*--------------------------------------
BitField Name: RxDE1AisCntThres[2:0]
BitField Type: R/W
BitField Desc: Threshold of AIS monitoring block. This function always monitors
               the number of bit �0� of the incoming signal in each of two
               consecutive frame periods (256*2 bits in E1, 193*3 bits in DS1).
               If it is less than the desire number, AIS will be set then.
--------------------------------------*/
#define cThaRxDE1AisCntThresMask                       cBit12_10
#define cThaRxDE1AisCntThresShift                      10

/*--------------------------------------
BitField Name: RxDE1RaiCntThres[2:0]
BitField Type: R/W
BitField Desc: Threshold of RAI monitoring block. This function always monitors
               the number of bit �0� of the incoming signal in each of two
               consecutive frame periods (256*2 bits in E1, 193*3 bits in DS1).
               If it is less than the desire number, RAI will be set then.
--------------------------------------*/
#define cThaRxDE1RaiCntThresMask                       cBit9_7
#define cThaRxDE1RaiCntThresShift                      7

/*--------------------------------------
BitField Name: RxDE1AisMaskEn
BitField Type: R/W
BitField Desc: Set 1 to enable output data of the DS1/E1/J1 framer to be set to
               all one (1) during LOF
--------------------------------------*/
#define cThaRxDE1AisMaskEnMask                         cBit6
#define cThaRxDE1AisMaskEnShift                        6

/*--------------------------------------
BitField Name: RxDE1En
BitField Type: R/W
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
--------------------------------------*/
#define cThaRxDE1EnMask                                cBit5
#define cThaRxDE1EnShift                               5

/*--------------------------------------
BitField Name: RxDE1FrcLof
BitField Type: R/W
BitField Desc: Set 1 to force Re-frame
--------------------------------------*/
#define cThaRxDE1FrcLofMask                            cBit4
#define cThaRxDE1FrcLofShift                           4

/*--------------------------------------
BitField Name: RxDE1Md[2:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaRxDE1MdMask                                cBit3_0
#define cThaRxDE1MdShift                               0

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Tx Framer Control
Reg Addr: 0x609800 - 0x64997B (for 336)
          The address format for these registers is 0x609800 + 32*de3id +
          4*de2id + de1id
          Where: de3id: 0 - 11
          de2id: 0 - 6
          de1id: 0 - 3
Reg Desc: DS1/E1/J1 Rx framer control is used to configure for Frame mode
          (DS1, E1, J1) at the DS1/E1 framer.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1TxFrmrCtrl                           0x00794000

#define cThaRegDS1E1J1TxFrmrCtrlNoneEditableFieldsMask     0

/* only apply for DS1 support SSM */
#define cThaTxDE1TxSsmThresMask                        cBit31_27
#define cThaTxDE1TxSsmThresShift                       27

#define cThaTxDE1TxSaCfgMask                           cBit26_24
#define cThaTxDE1TxSaCfgShift                          24

/*--------------------------------------
BitField Name: TxDE1LineAisCorrectionIns
BitField Type: R/W
BitField Desc: Set 1 to enable Line AIS Insert
--------------------------------------*/
#define cThaTxDE1LineAisCorrectionInsMask              cBit24
#define cThaTxDE1LineAisCorrectionInsShift             24

/*--------------------------------------
BitField Name: TxDE1MdlEn
BitField Type: R/W
BitField Desc: Enable mdl ,set 1 to enable, clr 0 to disable
--------------------------------------*/
#define cThaTxDE1MdlEnMask                             cBit23
#define cThaTxDE1MdlEnShift                            23

/*--------------------------------------
BitField Name: TxDE1TxSaBit
BitField Type: R/W
BitField Desc: Transmitted unused Sa bit
--------------------------------------*/
#define cThaTxDE1TxSaBitMask                           cBit22
#define cThaTxDE1TxSaBitShift                          22

/*--------------------------------------
BitField Name: TxDE1FbitBypass
BitField Type: R/W
BitField Desc: Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer
--------------------------------------*/
#define cThaTxDE1FbitBypassMask                        cBit21
#define cThaTxDE1FbitBypassShift                       21

/*--------------------------------------
BitField Name: TxDE1LineAisIns
BitField Type: R/W
BitField Desc: Set 1 to enable Line AIS Insert
--------------------------------------*/
#define cThaTxDE1LineAisInsMask                        cBit20
#define cThaTxDE1LineAisInsShift                       20

/*--------------------------------------
BitField Name: TxDE1PayAisIns
BitField Type: R/W
BitField Desc: Set 1 to enable Payload AIS Insert
--------------------------------------*/
#define cThaTxDE1PayAisInsMask                         cBit19
#define cThaTxDE1PayAisInsShift                        19

/*--------------------------------------
BitField Name: TxDE1RmtLineLoop
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaTxDE1RmtLineLoopMask                       cBit18
#define cThaTxDE1RmtLineLoopShift                      18

/*--------------------------------------
BitField Name: TxDE1RmtPayLoop
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaTxDE1RmtPayLoopMask                        cBit17
#define cThaTxDE1RmtPayLoopShift                       17

/*--------------------------------------
BitField Name: TxDE1AutoAis
BitField Type: R/W
BitField Desc: Auto Yellow generation enable
               - 1: Yellow alarm detected from Rx Framer will be automatically
                 transmitted in Tx Framer
               - 0: No automatically Yellow alarm transmitted
--------------------------------------*/
#define cThaTxDE1AutoAisMask                           cBit16
#define cThaTxDE1AutoAisShift                          16


/*--------------------------------------
BitField Name: TxDE1DLBypass
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaTxDE1DLBypassMask                          cBit15
#define cThaTxDE1DLBypassShift                         15

/*--------------------------------------
BitField Name: TxDE1DLSwap
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaTxDE1DLSwapMask                            cBit14
#define cThaTxDE1DLSwapShift                           14

/*--------------------------------------
BitField Name: TxDE1DLCfg[4:0]
BitField Type: R/W
BitField Desc: Configured for transmit 5 datalink bits (Sa4 --> Sa8): 1:
               enable, otherwise disable.
--------------------------------------*/
#define cThaTxDE1DLCfgSsmEnMask                        cBit13
#define cThaTxDE1DLCfgSsmEnShift                       13
#define cThaTxDE1DLCfgSsmValMask                       cBit12_9
#define cThaTxDE1DLCfgSsmValShift                      9

#define cThaTxDE1DLCfgMask                             cBit13_9
#define cThaTxDE1DLCfgShift                            9

#define cThaTxDs1DlkEnMask                             cBit12
#define cThaTxDs1DlkEnShift                            12

/*--------------------------------------
BitField Name: TxDE1AutoYel
BitField Type: R/W
BitField Desc: Auto Yellow generation enable
               - 1: Yellow alarm detected from Rx Framer will be automatically
                 transmitted in Tx Framer
               - 0: No automatically Yellow alarm transmitted
--------------------------------------*/
#define cThaTxDE1AutoYelMask                           cBit8
#define cThaTxDE1AutoYelShift                          8

/*--------------------------------------
BitField Name: TxDE1FrcYel
BitField Type: R/W
BitField Desc: SW force to Tx Yellow alarm
--------------------------------------*/
#define cThaTxDE1FrcYelMask                            cBit7
#define cThaTxDE1FrcYelShift                           7

/*--------------------------------------
BitField Name: TxDE1AutoCrcErr
BitField Type: R/W
BitField Desc: Auto CRC error enable
               - 1: CRC Error detected from Rx Framer will be automatically
                 transmitted in REI bit of Tx Framer
               - 0: No automatically CRC Error alarm transmitted
--------------------------------------*/
#define cThaTxDE1AutoCrcErrMask                        cBit6
#define cThaTxDE1AutoCrcErrShift                       6

/*--------------------------------------
BitField Name: TxDE1FrcCrcErr
BitField Type: R/W
BitField Desc: SW force to Tx CRC Error alarm (REI bit)
--------------------------------------*/
#define cThaTxDE1FrcCrcErrMask                         cBit5
#define cThaTxDE1FrcCrcErrShift                        5

/*--------------------------------------
BitField Name: TxDE1En
BitField Type: R/W
BitField Desc: Set 1 to enable the DS1/E1/J1 framer.
--------------------------------------*/
#define cThaTxDE1EnMask                                cBit4
#define cThaTxDE1EnShift                               4

#define cThaTxDE1MdMask                                cBit3_0
#define cThaTxDE1MdShift                               0

/*------------------------------------------------------------------------------
Reg Name: PDH LIU Looptime Configuration
            (This is a channel register because of depending on LIU ID)
Reg Addr: 0x00700302 - 0x00700302
Reg Desc: This is the configuration for the looptime mode in the LIU interface
------------------------------------------------------------------------------*/
#define cThaRegPdhLIULooptimeCfg                         0x700302

#define cThaRegPdhLIULooptimeCfgNoneEditableFieldsMask   0

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Tx Framer Signaling CPU Insert Enable Control
Reg Addr: 0x007C8001 � 0x007C8001
Reg Desc: This is the CPU signaling insert global enable bit
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1TxFrmrSigingCpuInsEnCtrl      					  0x007C8001

#define cThaRegDS1E1J1TxFrmrSigingCpuInsEnCtrlNoneEditableFieldsMask      cBit31_1

/*--------------------------------------
BitField Name: TxDE1SigCPUMd
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaTxDE1SigCpuMdMask                          cBit0
#define cThaTxDE1SigCpuMdShift                         0

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control
Reg Addr: 0x00755000-0x0075501F
The address format for these registers is 0x00755000 + LiuID
Where:  LiuID: 0 � 31
Description: This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrperChnIntrEnCtrl                         0x755000

#define cThaRegDS1E1J1RxFrmrperChnIntrEnCtrlNoneEditableFieldsMask   0

#define cThaDE1BerTcaIntrEnMask                       cBit14
#define cThaDE1BerSdIntrEnMask                        cBit13
#define cThaDE1BerSfIntrEnMask                        cBit12

#define cThaDE1BomSsmIntrEnMask                       cBit11
#define cThaDE1InbandLoopCodeIntrEnMask               cBit10

/*--------------------------------------
BitField Name: cThaDE1SigRaiIntrEnMask
BitField Type: R/W
BitField Desc: Set 1 to enable change E1 Signaling RAI event to generate an interrupt.
--------------------------------------*/
#define cThaDE1SigRaiIntrEnMask                      cBit9

/*--------------------------------------
BitField Name: cThaDE1SigLofIntrEnMask
BitField Type: R/W
BitField Desc: Set 1 to enable change E1 Signaling LOF state event to generate an interrupt.
--------------------------------------*/
#define cThaDE1SigLofIntrEnMask                      cBit8

/*--------------------------------------
BitField Name: DE1FbeIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change FBE state event to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1CrcIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change CRC state event to generate an interrupt.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1ReiIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change REI state event to generate an interrupt.
--------------------------------------*/
#define cThaDE1ReiIntrEnMask                           cBit5
#define cThaDE1ReiIntrEnShift                          5

/*--------------------------------------
BitField Name: cThaDE1LomfIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change LOMF state event to generate an interrupt.
--------------------------------------*/
#define cThaDE1LomfIntrEnMask                           cBit4

/*--------------------------------------
BitField Name: DE1LosIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change LOS state event to generate an interrupt.
--------------------------------------*/
#define cThaDE1LosIntrEnMask                           cBit3

/*--------------------------------------
BitField Name: DE1AisIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change AIS state event to generate an interrupt.
--------------------------------------*/
#define cThaDE1AisIntrEnMask                           cBit2

/*--------------------------------------
BitField Name: DE1RaiIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change RAI state event to generate an interrupt.
--------------------------------------*/
#define cThaDE1RaiIntrEnMask                           cBit1

/*--------------------------------------
BitField Name: DE1LofIntrEn
BitField Type: R/W
BitField Desc: Set 1 to enable change LOF state event to generate an interrupt.
--------------------------------------*/
#define cThaDE1LofIntrEnMask                           cBit0

/*=============================================================================
 *                           COUNTER AND STATUS
 *============================================================================= */
/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer per Channel Current Status
Reg Addr: 0x00755040-0x0075557B
          The address format for these registers is 0x00755040 + StsID*32 +
          Tug2ID*4 + VtnID
          Where: StsID: (0-11) STS-1/VC-3 ID
          Tug2ID: (0-6) TUG-2/VTG ID
          VtnID: (0-3) VT/TU number ID in the TUG-2/VTG
Reg Desc: This is the per Channel Current status of DS1/E1/J1 Rx Framer.
------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer per Channel Interrupt Status
Reg Addr: 0x00755020-0x0075503F
          The address format for these registers is 0x00755020 + LIU ID [0-31]
Reg Desc: This is the per Channel interrupt status of DS1/E1/J1 Rx Framer.
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Type: R/W
BitField Desc: Set 1 if there is a the Signaling RAI detected.
--------------------------------------*/

/*--------------------------------------
BitField Type: R/W
BitField Desc: Set 1 if there is a the Signaling LOF detected.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1FbeIntr
BitField Type: R/W
BitField Desc: Set 1 if there is a change in FBE exceed threshold state event.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1CrcIntr
BitField Type: R/W
BitField Desc: Set 1 if there is a change in CRC exceed threshold state event.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1ReiIntr
BitField Type: R/W
BitField Desc: Set 1 if there is a change in REI exceed threshold state event.
--------------------------------------*/

/*--------------------------------------
BitField Type: R/W
BitField Desc: Set 1 if there is a change in LOMF state event.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1LosIntr
BitField Type: R/W
BitField Desc: Set 1 if there is a change in LOS state event.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1AisIntr
BitField Type: R/W
BitField Desc: Set 1 if there is a change in AIS state event.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1RaiIntr
BitField Type: R/W
BitField Desc: Set 1 if there is a change in RAI state event.
--------------------------------------*/

/*--------------------------------------
BitField Name: DE1LofIntr
BitField Type: R/W
BitField Desc: Set 1 if there is a change in LOF state event.
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: PDH LIU BPVP Error Counter
Address: 0x500000 � 0x50001F(R2C), 0x500100 � 0x50011F(R_O),
The address format for these registers is 0x500000 + LiuId (R2C),
                                          0x500100 + LiuId (R_O)
Where: LiuId (0 � 31) PDH LIU IDs
Description: Count number of  BPVP errors detected on each LIU port
------------------------------------------------------------------------------*/
#define cThaRegLiuBpvpErrCnt              0xC00000
#define cThaRegLiuBpvpErrCntV2            0x3F0000

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer CRC Error Counter
Reg Addr: 0x00756800 - 0x60A37B (for 336 lines).
          The address format for these registers is 0x60A000 + 512*r2c +
          32*de3id + 4*de2id + de1id + 262144*core
          Where: r2c: 0 - 1 r2c = 0: Read to clear. Otherwise, Read
          Only.
          de3id: 0 - 11
          de2id: 0 - 6
          de1id: 0 - 3
          core: 0 - 1
Reg Desc: This is the per channel CRC error counter for DS1/E1/J1 receive
          framer
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrCrcErrCnt                           0x00756800

/*-----------
BitField Name: DE1CrcErrCnt
BitField Type: R2C
BitField Desc: DS1/E1/J1 CRC Error Accumulator
------------*/
#define cThaDE1CrcErrCntMask                          cBit7_0
#define cThaDE1CrcErrCntShift                         0

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer REI Counter
Reg Addr: 0x60B000 - 0x64B37B (for 336x2 lines).
          The address format for these registers is 0x60B000 + 512*r2c +
          32*de3id + 4*de2id + de1id + 262144*core
          Where: r2c: 0 - 1 r2c = 0: Read to clear. Otherwise, Read
          Only.
          de3id: 0 - 11
          de2id: 0 - 6
          de1id: 0 - 3
          core: 0 - 1
Reg Desc: This is the per channel REI counter for DS1/E1/J1 receive framer
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrReiCnt                      0x00757000

/*-----------
BitField Name: DE1ReiCnt
BitField Type: R2C
BitField Desc: DS1/E1/J1 REi error accumulator
------------*/
#define cThaDE1ReiCntMask                             cBit7_0
#define cThaDE1ReiCntShift                            0

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer FBE Counter
Reg Addr: 0x60B000 - 0x64B37B (for 336x2 lines).
          The address format for these registers is 0x60B000 + 512*r2c +
          32*de3id + 4*de2id + de1id + 262144*core
          Where: r2c: 0 - 1 r2c = 0: Read to clear. Otherwise, Read
          Only.
          de3id: 0 - 11
          de2id: 0 - 6
          de1id: 0 - 3
          core: 0 - 1
Reg Desc: This is the per channel REI counter for DS1/E1/J1 receive framer
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrFBECnt                    0x00756000

#define cThaDE1FbitErrCntMask                         cBit7_0
#define cThaDE1FbitErrCntShift                        0

/*==============================================================================
*                       DATA LINK CHANNEL REGISTER
*==============================================================================*/
/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Tx Framer DLK Insertion Control
Reg Addr: 0x796020-0x79602F (For 16 engines)
Reg Desc: These registers are used to control DLK insertion engines.
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: DlkMsgLength
BitField Type: R/W
BitField Desc: length of message to transmit in byte. The maximum length is 128
               in byte.
BitField Bits: 15_9
--------------------------------------*/

/*--------------------------------------
BitField Name: DlkDE1ID
BitField Type: R/W
BitField Desc: To configure which DS1/E1 ID is selected to insert DLK. At a
               time, user can choose 16 DS1/E1s to carry DLK message.
BitField Bits: 8_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Tx Framer DLK Insertion Message Memory
Reg Addr: 0x796800-0x796FFF
          The address format for these registers is 0x796800 + msgid*128 +
          byteid
          Where: msgid: 0-15; byteid: 0-127
Reg Desc: This memory contains data messages; each DLK insertion engine has a
          128-byte message.
------------------------------------------------------------------------------*/


/*--------------------------------------
BitField Name: DlkInsMsg
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 7_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status
Reg Addr: 0x007557FF - 0x007557FF
Reg Desc: The register consists of 12 bits for 12 STS/VCs of the Rx DS1/E1/J1
          Framer. Each bit is used to store Interrupt OR status of the related
          STS/VC.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrperStsVCIntrOrStat           (0x7557FF)
#define cThaRegDS1E1J1RxFrmrperStsVCIntrStsIdMask(stsId) (cBit0 << stsId)

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer per Channel Interrupt OR Status
Reg Addr: 0x00755600 - 0x0075560B
          The address format for these registers is 0x00755600 + Stsid
          Where: StsID: (0-11) STS-1/VC-3 ID
Reg Desc: The register consists of 28 bits for 28 VT/TUs of the related STS/VC
          in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR
          status of the related DS1/E1/J1.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrperChnIntrOrStat (0x755600)
#define cThaRegDS1E1J1RxFrmrperChnIntrMask(vtId) (cBit0 << vtId)

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control
Reg Addr: 0x007557FE - 0x007557FE
Reg Desc: The register consists of 12 interrupt enable bits for 12 STS/VCs in
          the Rx DS1/E1/J1 Framer.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrperStsVCIntrEnCtrl      (0x7557FE)

/*------------------------------------------------------------------------------
Reg Name:
Reg Addr:
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegDS1E1LedStateCtrl                (0xFD)
#define cThaRegDS1E1LedStateForceMask(portId)   (cBit0 << (portId * 2))
#define cThaRegDS1E1LedStateForceShift(portId)  (portId * 2)
#define cThaRegDS1E1LedStateStatMask(portId)    (cBit0 << ((portId * 2) + 1))
#define cThaRegDS1E1LedStateStatShift(portId)  ((portId * 2) + 1)

/*------------------------------------------------------------------------------
Reg Name: Register description for out reference clocl 8k
Reg Addr: 0xF00043
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPdhOutRefClk8k             (0xF00043)

#define cThaRegPdhOutRefClkMask(clockId)    cBit3_0 << cThaRegPdhOutRefClkShift(clockId)
#define cThaRegPdhOutRefClkShift(clockId)  ((clockId) << 2)

#define cThaRegPdhOutRefClkEnableMask(clockId)   cBit8 << (clockId)
#define cThaRegPdhOutRefClkEnableShift(clockId)  (8 + (clockId))

/*------------------------------------------------------------------------------
Reg Name: PDH LIU Loop Out Configuration
Address: 0x00700303 - 0x00700303
Description: This is the configuration for the loopback out from Rx to Tx in the LIU interface
------------------------------------------------------------------------------*/
#define cThaRegPDHLiuLoopOutCfg               0x00700303

#define cThaRegPDHLiuLoopOutCfgMask(liuId)    cBit0 << (liuId)
#define cThaRegPDHLiuLoopOutCfgShift(liuId)   (liuId)

/*------------------------------------------------------------------------------
Reg Name: PDH LIU Loop In Configuration
Address: 0x00700305 � 0x00700305
Description: This is the configuration for the loopback in from Tx to Rx in the LIU interface
------------------------------------------------------------------------------*/
#define cThaRegPDHLiuLoopInCfg               0x00700305

#define cThaRegPDHLiuLoopInCfgMask(liuId)    cBit0 << (liuId)
#define cThaRegPDHLiuLoopInCfgShift(liuId)   (liuId)

/*------------------------------------------------------------------------------
Reg Name: PDH LIU Payload Loop In  Configuration
Address: 0x00700313 � 0x00700313
Description: This is the configuration for the payload loopback in from Tx PDH to Rx PDH in the LIU interface
------------------------------------------------------------------------------*/
#define cThaRegPDHLiuPayloadLoopInCfg               0x00700313

#define cThaRegPDHLiuPayloadLoopInCfgMask(liuId)    cBit0 << (liuId)
#define cThaRegPDHLiuPayloadLoopInCfgShift(liuId)   (liuId)

/*------------------------------------------------------------------------------
Reg Name: PDH LIU Payload Loop In  Configuration
Address: 0x00700314 � 0x00700314
Description: This is the configuration for the payload loopback out from Rx PDH to Tx PDH in the LIU interface
------------------------------------------------------------------------------*/
#define cThaRegPDHLiuPayloadLoopOutCfg               0x00700314

#define cThaRegPDHLiuPayloadLoopOutCfgMask(liuId)    cBit0 << (liuId)
#define cThaRegPDHLiuPayloadLoopOutCfgShift(liuId)   (liuId)

/*------------------------------------------------------------------------------
Reg Name: PDH LIU Tx Clock Invert Configuration
Address: 0x00700304 - 0x00700304
Description: This is the configuration for the transmit clock edge in the LIU interface
------------------------------------------------------------------------------*/
#define cThaRegPDHLIUTxClockInvertConfiguration      0x00700304
#define cThaRegPDHLIURxClockInvertConfiguration      0x00700306

/*------------------------------------------------------------------------------
Reg Name: PDH Global Interrupt Enable Configuration
Address: 0x00700001 � 0x00700001
Description: This is the global interrupt enable configuration for the PDH block
------------------------------------------------------------------------------*/
#define cThaRegPdhGlbIntrEnableCtrl       0x00700001



#define cThaRegPdhRxDe1FdlIntrEnableMask     cBit5
#define cThaRegPdhRxDe1FdlIntrEnableShift    5

#define cThaRegPdhRxDe1LoopCodeDetIntrEnableMask     cBit4
#define cThaRegPdhRxDe1LoopCodeDetIntrEnableShift    4

#define cThaRegPdhRxDe1IntrEnableMask     cBit1
#define cThaRegPdhRxDe1IntrEnableShift    1



/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer Master Interrupt Enable Control
Address: 0x00750001 - 0x00750001
The address format for these registers is 0x00750001
Description: This is the master interrupt enable configuration register for the PDH DS1/E1/J1 Rx framer
------------------------------------------------------------------------------*/
#define cThaRegPdhRxDe1FrameMasterIntrEnableCtrl    0x00750001





#define cThaRegPdhRxDe1LOSIntrEnableMask            cBit3
#define cThaRegPdhRxDe1LOSIntrEnableShift           3

#define cThaRegPdhRxDe1InbandLoopcodeIntrEnableMask           cBit11
#define cThaRegPdhRxDe1InbandLoopcodeIntrEnableShift          11
#define cThaRegPdhRxDe1OutbandLoopcodeIntrEnableMask          cBit10
#define cThaRegPdhRxDe1OutbandLoopcodeIntrEnableShift         10


/*------------------------------------------------------------------------------
Reg Name:DS1/E1/J1 Rx Framer Channel Interrupt Enable Control
Address: 0x0075507E
Description: This is the Channel interrupt enable of DS1/E1/J1 Rx Framer.
------------------------------------------------------------------------------*/
#define cThaRegPdhRxFrameChannelIntrEnableCtrl             0x0075507E

#define cThaRegPdhRxFrameChannelIntrEnableMask(de1Id)      cBit0 << (de1Id)
#define cThaRegPdhRxFrameChannelIntrEnableShift(de1Id)     (de1Id)

/*------------------------------------------------------------------------------
Reg Name:DS1/E1/J1 Rx Framer Channel Interrupt Status
Address:  0x0075507F
Description: This is the Channel interrupt status of DS1/E1/J1 Rx Framer.
------------------------------------------------------------------------------*/
#define cThaRegPdhRxFrameChannelIntrStatus                 0x0075507F

#define cThaRegPdhRxFrameChannelIntrStatusMask(de1Id)      cBit0 << (de1Id)

/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling ID Conversion Control
Address    : 0x007C2000 - 0x007C207F
             The address format for these registers is 0x007C2000 + pwid
Description: DS1/E1/J1 Rx framer signaling ID conversion control is used to
             convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer.
------------------------------------------------------------------------------*/
#define cThaRegPdhTxFramerSignalingIDConversionControl 0x007C2000

#define cThaRegPdhTxDE1SigDS1DwordIndex    1

#define cThaRegPdhTxDE1SigLineIDDwordIndex 1

#define cThaRegPdhTxDE1SigEnbMask          cBit31_0
#define cThaRegPdhTxDE1SigEnbShift         0
#define cThaRegPdhTxDE1SigEnbDwordIndex    0

/*------------------------------------------------------------------------------
Reg Name: 4.6.12.7. DS1/E1/J1 Tx Framer Signaling Buffer
Reg Addr: 0x007CC000 - 0x007CDFFF (for 8064 DS0 channels)
          The address format for these registers is 0x7CC000 + 1024*de3id + 128*de2id + 32*de1id + tscnt
          Where: de3id: 0 - 11
          de2id: 0 - 6
          de1id: 0 - 3
          tscnt: 0 - 23 (ds1) or 0 - 31 (e1)
Reg Desc: DS1/E1/J1 Rx framer signaling ABCD bit store for each DS0 at the DS1/E1 framer.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1TxFrmrSignalingBuffer            0x7CC000

/*--------------------------------------
BitField Name: TxDE1SigABCD
BitField Type: R/W
BitField Desc: 4-bit ABCD signaling for each DS0 (Note: for DS1/J1, must write the 0x007C8002 value 1 before
writing this ABCD, for E1  must write the 0x007C8002 value 0 before writing this ABCD)
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer HW Status
Reg Addr   : 0x00054400 - 0x0005402F
Reg Formula: 0x00054400 + liuid
    Where  :
           + $liuid(0-47):
Reg Desc   :
These bit[2:0] of registers are used for Hardware status only, bit[19:16] of registers are indicate SSM E1 value

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rxfrm_hw_stat_Base                                                             0x00754400
#define cAf6Reg_dej1_rxfrm_hw_stat(liuid)                                                 (0x00054400+(liuid))
#define cAf6Reg_dej1_rxfrm_hw_stat_WidthVal                                                                 32
#define cAf6Reg_dej1_rxfrm_hw_stat_WriteMask                                                               0x0

/*--------------------------------------
BitField Name: RXE1SSMSta
BitField Type: RO
BitField Desc: Current value of SSM message
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Bit_Start                                                        16
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Bit_End                                                          19
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Mask                                                      cBit19_16
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Shift                                                            16
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_MaxVal                                                          0xf
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_MinVal                                                          0x0
#define cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_RstVal                                                          0x0

/*--------------------------------------
BitField Name: RxDS1E1_CrrAISDownStr
BitField Type: RO
BitField Desc: Current value of AIS downstream
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dej1_rxfrm_hw_stat_RxDS1E1_CrrAISDownStr_Mask                                               cBit6
#define cAf6_dej1_rxfrm_hw_stat_RxDS1E1_CrrAISDownStr_Shift                                                  6

/*--------------------------------------
BitField Name: RxDE1Sta
BitField Type: RO
BitField Desc: 5 In Frame
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Bit_Start                                                           0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Bit_End                                                             2
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Mask                                                          cBit2_0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_Shift                                                               0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_MaxVal                                                            0x7
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_MinVal                                                            0x0
#define cAf6_dej1_rxfrm_hw_stat_RxDE1Sta_RstVal                                                            0x0

/*------------------------------------------------------------------------------
Reg Name   : Tx SSM Framer Control
Reg Addr   : 0x000F0000 - 0x000F002F
Reg Formula: 0x000F0000 + LiuID
    Where  :
           + $LiuID(0-47):
Reg Desc   :
This is used to configure the SSM transmitter

------------------------------------------------------------------------------*/
#define cAf6Reg_txctrl_pen_Base                                                                     0x000F0000
#define cAf6Reg_txctrl_pen(LiuID)                                                         (0x000F0000+(LiuID))
#define cAf6Reg_txctrl_pen_WidthVal                                                                         32
#define cAf6Reg_txctrl_pen_WriteMask                                                                       0x0

#define cAf6_txctrl_pen_E2_Bypass_Enable_Mask                                                             cBit2
#define cAf6_txctrl_pen_E2_Bypass_Enable_Shift                                                                2
#define cAf6_txctrl_pen_E1_Bypass_Enable_Mask                                                             cBit1
#define cAf6_txctrl_pen_E1_Bypass_Enable_Shift                                                                1

#define cAf6_txctrl_pen_SSM_Enable_Mask                                                                     cBit24
#define cAf6_txctrl_pen_SSM_Enable_Shift                                                                    24

#define cAf6_txctrl_pen_SSM_G832SSMEna_Mask                                                                 cBit4
#define cAf6_txctrl_pen_SSM_G832SSMEna_Shift                                                                4

/*SSM Engine Status*/
#define cAf6_txtrl_ssmsta_pen                                                                        0x000F0200

/*--------------------------------------
BitField Name: SSMCount
BitField Type: RW
BitField Desc: SSM Message Counter. These bits are used to store the amount of
repetitions the Transmit SSM message will be sent - 0 : the transmit BOC will be
set continuously - other: the amount of repetitions the Transmit SSM message
will be sent before an all ones
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_txctrl_pen_SSMCount_Bit_Start                                                                  16
#define cAf6_txctrl_pen_SSMCount_Bit_End                                                                    23
#define cAf6_txctrl_pen_SSMCount_Mask                                                                cBit23_16
#define cAf6_txctrl_pen_SSMCount_Shift                                                                      16
#define cAf6_txctrl_pen_SSMCount_MaxVal                                                                   0xff
#define cAf6_txctrl_pen_SSMCount_MinVal                                                                    0x0
#define cAf6_txctrl_pen_SSMCount_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: SSMMess
BitField Type: RW
BitField Desc: SSM Message. These bits are used to store the SSM message to be
transmitted out the National bits or Si International bit or FDL - E1: [13:10]
for SSM Message - T1: [15:10] for BOC code
BitField Bits: [15:10]
--------------------------------------*/
#define cAf6_txctrl_pen_SSMMess_Bit_Start                                                                   10
#define cAf6_txctrl_pen_SSMMess_Bit_End                                                                     15
#define cAf6_txctrl_pen_SSMMess_Mask                                                                 cBit15_10
#define cAf6_txctrl_pen_SSMMess_Shift                                                                       10
#define cAf6_txctrl_pen_SSMMess_MaxVal                                                                    0x3f
#define cAf6_txctrl_pen_SSMMess_MinVal                                                                     0x0
#define cAf6_txctrl_pen_SSMMess_RstVal                                                                     0x0

#define cAf6_txctrl_pen_E3G832_SSMMess_Mask                                                                 cBit10_5
#define cAf6_txctrl_pen_E3G832_SSMMess_Shift                                                                      5

#define cAf6_txctrl_pen_Ds3Cbit_SSMInvert_Mask                                                              cBit11
#define cAf6_txctrl_pen_Ds3Cbit_SSMInvert_Shift                                                              11
/*--------------------------------------
BitField Name: SSMSasel
BitField Type: RW
BitField Desc: Only one of the five Sa bits can be chosen for SSM transmission
at a time. Don't care in T1 case - Bit9: for Sa8 - Bit8: for Sa7 - Bit7: for Sa6
- Bit6: for Sa5 - Bit5: for Sa4
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_txctrl_pen_SSMSasel_Bit_Start                                                                   5
#define cAf6_txctrl_pen_SSMSasel_Bit_End                                                                     9
#define cAf6_txctrl_pen_SSMSasel_Mask                                                                  cBit9_5
#define cAf6_txctrl_pen_SSMSasel_Shift                                                                       5
#define cAf6_txctrl_pen_SSMSasel_MaxVal                                                                   0x1f
#define cAf6_txctrl_pen_SSMSasel_MinVal                                                                    0x0
#define cAf6_txctrl_pen_SSMSasel_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: SSMSiSaSel
BitField Type: RW
BitField Desc: Select Si or Sa to transmit SSM. Don't care in T1 case - 0: Sa
National Bits (Only one of the five Sa bits can be chosen for SSM transmission
at a time) - 1: Si International Bits
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_txctrl_pen_SSMSiSaSel_Bit_Start                                                                 4
#define cAf6_txctrl_pen_SSMSiSaSel_Bit_End                                                                   4
#define cAf6_txctrl_pen_SSMSiSaSel_Mask                                                                  cBit4
#define cAf6_txctrl_pen_SSMSiSaSel_Shift                                                                     4
#define cAf6_txctrl_pen_SSMSiSaSel_MaxVal                                                                  0x1
#define cAf6_txctrl_pen_SSMSiSaSel_MinVal                                                                  0x0
#define cAf6_txctrl_pen_SSMSiSaSel_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: TxDE1Md
BitField Type: RW
BitField Desc: Transmit DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX
Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For
bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other:
Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_txctrl_pen_TxDE1Md_Bit_Start                                                                    0
#define cAf6_txctrl_pen_TxDE1Md_Bit_End                                                                      3
#define cAf6_txctrl_pen_TxDE1Md_Mask                                                                   cBit3_0
#define cAf6_txctrl_pen_TxDE1Md_Shift                                                                        0
#define cAf6_txctrl_pen_TxDE1Md_MaxVal                                                                     0xf
#define cAf6_txctrl_pen_TxDE1Md_MinVal                                                                     0x0
#define cAf6_txctrl_pen_TxDE1Md_RstVal                                                                     0x0

/* SSM enable/disable*/
#define cAf6Reg_ssm_enable 0x000F4000

/*------------------------------------------------------------------------------
Reg Name   : Rx SSM Framer Control
Reg Addr   : 0x000FC000 - 0x000FC02F
Reg Formula: 0x000FC000 + LiuID
    Where  :
           + $LiuID(0-47):
Reg Desc   :
This is used to configure the SSM transmitter

------------------------------------------------------------------------------*/
#define cAf6Reg_rxctrl_pen_Base                                                                     0x000FC000
#define cAf6Reg_rxctrl_pen(LiuID)                                                         (0x000FC000+(LiuID))
#define cAf6Reg_rxctrl_pen_WidthVal                                                                         32
#define cAf6Reg_rxctrl_pen_WriteMask                                                                       0x0

#define cAf6_rxctrl_pen_RxDE3RxDE3Enb_Mask                                                                  cBit8
#define cAf6_rxctrl_pen_RxDE3RxDE3Enb_Shift                                                                     8

/*--------------------------------------
BitField Name: RxDE1LofThres
BitField Type: RW
BitField Desc: Threshold for declare LOF
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1LofThres_Bit_Start                                                             6
#define cAf6_rxctrl_pen_RxDE1LofThres_Bit_End                                                               9
#define cAf6_rxctrl_pen_RxDE1LofThres_Mask                                                           cBit9_6
#define cAf6_rxctrl_pen_RxDE1LofThres_Shift                                                                6
#define cAf6_rxctrl_pen_RxDE1LofThres_MaxVal                                                               0xf
#define cAf6_rxctrl_pen_RxDE1LofThres_MinVal                                                               0x0
#define cAf6_rxctrl_pen_RxDE1LofThres_RstVal                                                               0x0

#define cAf6_rxctrl_pen_RxDE3LofThres_Mask                                                                 cBit7_5
#define cAf6_rxctrl_pen_RxDE3LofThres_Shift                                                                 5
/*--------------------------------------
BitField Name: RxDE1FLofmod
BitField Type: RW
BitField Desc: Select mode Slide Window or Continous to declare LOF - 1: Slide
Window detection (window 8, threshold RxDE1LofThres) - 0: Continous detection
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1FLofmod_Bit_Start                                                               5
#define cAf6_rxctrl_pen_RxDE1FLofmod_Bit_End                                                                 5
#define cAf6_rxctrl_pen_RxDE1FLofmod_Mask                                                                cBit5
#define cAf6_rxctrl_pen_RxDE1FLofmod_Shift                                                                   5
#define cAf6_rxctrl_pen_RxDE1FLofmod_MaxVal                                                                0x1
#define cAf6_rxctrl_pen_RxDE1FLofmod_MinVal                                                                0x0
#define cAf6_rxctrl_pen_RxDE1FLofmod_RstVal                                                                0x0

/*--------------------------------------
BitField Name: RxDE1FrcLof
BitField Type: RW
BitField Desc: Set 1 to force Re-frame (default 0)
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1FrcLof_Bit_Start                                                                4
#define cAf6_rxctrl_pen_RxDE1FrcLof_Bit_End                                                                  4
#define cAf6_rxctrl_pen_RxDE1FrcLof_Mask                                                                 cBit4
#define cAf6_rxctrl_pen_RxDE1FrcLof_Shift                                                                    4
#define cAf6_rxctrl_pen_RxDE1FrcLof_MaxVal                                                                 0x1
#define cAf6_rxctrl_pen_RxDE1FrcLof_MinVal                                                                 0x0
#define cAf6_rxctrl_pen_RxDE1FrcLof_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: RxDE1Md
BitField Type: RW
BitField Desc: Receive DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX
Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For
bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other:
Unused
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_rxctrl_pen_RxDE1Md_Bit_Start                                                                    0
#define cAf6_rxctrl_pen_RxDE1Md_Bit_End                                                                      3
#define cAf6_rxctrl_pen_RxDE1Md_Mask                                                                   cBit3_0
#define cAf6_rxctrl_pen_RxDE1Md_Shift                                                                        0
#define cAf6_rxctrl_pen_RxDE1Md_MaxVal                                                                     0xf
#define cAf6_rxctrl_pen_RxDE1Md_MinVal                                                                     0x0
#define cAf6_rxctrl_pen_RxDE1Md_RstVal                                                                     0x0

/* Debug register */
#define cAf6_txslip_cnt0_pen  0x000F4100
#define cAf6_txslip_stk0_upen 0x000F4001
#define cAf6_txslip_stk1_upen 0x000F4002
#define cAf6_rxfrm_status_pen 0x000FC400
#define cAf6_rxfrm_crc_err_cnt 0x000FE800
#define cAf6_rxfrm_rei_cnt 0x000FF000
#define cAf6_rxfrm_fbe_cnt 0x000FE000

/*
#*********************
#4.1.5.1. Rx DS3E3 SSM status
#*********************
// Begin:
// Register Full Name: Rx DS3E3 SSM status
// RTL Instant Name  : upen_sta_ssm
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x040760-0x04077F
// Formula: Address +  de3id
// Where:  {$de3id(0-23):}
*/
#define cAf6_Reg_upen_sta_ssm   0x740760
#define cAf6_Reg_upen_sta_ssm_RxSsm_Mask  cBit7_0
#define cAf6_Reg_upen_sta_ssm_RxSsm_Shift 0
#define cAf6_Reg_upen_sta_ssm_RxMsgCount_Mask  cBit11_8
#define cAf6_Reg_upen_sta_ssm_RxMsgCount_Shift 8
#define cAf6_Reg_upen_sta_ssm_RxMsgStat_Mask  cBit15_12
#define cAf6_Reg_upen_sta_ssm_RxMsgStat_Shift 12

/* LIU TX force AIS, long register */
#define cAf6_Reg_Ds1_TxAis_Correction  0x00700307
#define cAf6_Reg_Ds1_TxAis_Correction1 0x00700308

/*------------------------------------------------------------------------------
Reg Name   : Rx Slip Enable
Reg Addr   : 0x00760000-0x007603FF
Reg Formula: 0x00760000 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
These registers are used to control Rx Slip

------------------------------------------------------------------------------*/
#define cAf6Reg_rxslipbuf_pen_Base                                                                  0x00760000
#define cAf6Reg_rxslipbuf_pen_WidthVal                                                                      32

/*--------------------------------------
BitField Name: rxslpmd
BitField Type:
BitField Desc: RX Slip Mode - 2'b00,2'b10: E1 - 2'b01: SF - 2'b11: ESF
BitField Bits: [2:1]
--------------------------------------*/
#define cAf6_rxslipbuf_pen_rxslpmd_Mask                                                                cBit2_1
#define cAf6_rxslipbuf_pen_rxslpmd_Shift                                                                     1

/*--------------------------------------
BitField Name: rxslpen
BitField Type:
BitField Desc: Rx Slip Enable - 1'b1 : enable, 1'b0 : disalbe End: Begin:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rxslipbuf_pen_rxslpen_Mask                                                                  cBit0
#define cAf6_rxslipbuf_pen_rxslpen_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Rx Slip Counter
Reg Addr   : 0x00761000-0x007617FF
Reg Formula: 0x00761000 + 1024*Rd2Clr + 32*de3id + 4*de2id + de1id
    Where  :
           + $Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This is the per channel Rx Slip event occur

------------------------------------------------------------------------------*/
#define cAf6Reg_rxfrm_rxslip_cnt_Base                                                                  0x00761000
#define cAf6Reg_rxfrm_rxslip_cnt_WidthVal                                                                      32

/*--------------------------------------
BitField Name: RxSlipCnt
BitField Type: RW
BitField Desc: Rx Slip Counter
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_rxfrm_rei_cnt_RxSlipCnt_Mask                                                             cBit15_0
#define cAf6_rxfrm_rei_cnt_RxSlipCnt_Shift                                                                   0

#ifdef __cplusplus
}
#endif
#endif /* THAMODULEPDHREG_HEADER */



/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe3.c
 *
 * Created Date: Feb 2, 2013
 *
 * Description : Thalassa DS3/E3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../generic/sur/AtModuleSurInternal.h"
#include "../../../generic/ber/AtBerControllerInternal.h"
#include "../../../generic/diag/AtErrorGeneratorInternal.h"
#include "../../../generic/pdh/AtPdhMdlControllerInternal.h"
#include "../../../generic/pdh/AtPdhDe2Internal.h"
#include "../../../generic/sur/AtSurEngineInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "../map/ThaModuleMap.h"
#include "../cdr/ThaModuleCdr.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "../sdh/ThaModuleSdh.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "../prbs/ThaPrbsEngine.h"
#include "ThaPdhDe3Internal.h"
#include "ThaModulePdhForStmReg.h"
#include "atcrc7.h"
#include "ThaModulePdhInternal.h"
#include "ThaPdhDe3.h"
#include "ThaStmModulePdh.h"
#include "../../include/att/AtAttController.h"
#include "retimingcontrollers/ThaPdhRetimingController.h"
#include "retimingcontrollers/ThaPdhDe3RetimingController.h"
#include "ThaPdhUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegRxM12E12Ctrl 0x00741000
#define cThaRxDS2E2MdMask   cBit1_0
#define cThaRxDS2E2MdShift  0
#define cThaRegTxM12E12Ctrl 0x00781000

#define cDs3CbitFeacHwThresholdSentStop 0
#define cDs3CbitFeacHwThresholdSentOneShot 10
#define cDs3CbitFeacHwThresholdSentContinuous 0xF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhDe3)self)
#define mRead(self, address) mChannelHwRead(self, (uint32)address, cAtModulePdh)
#define mWrite(self, address, value) mChannelHwWrite(self, (uint32)address, value, cAtModulePdh)
#define mRxDs3E3OHProCtrl(self) RxDs3E3OHProCtrl((ThaPdhDe3)self)
#define mRxDe3FeacBuffer(self) ThaModulePdhRxDe3FeacBufferRegister((ThaModulePdh)PdhModule((AtChannel)self))

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhDe3Methods m_methods;

/* Override */
static tAtPdhDe3Methods     m_AtPdhDe3Override;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtObjectMethods     m_AtObjectOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet MapFrameModeDefaultSet(AtPdhChannel self, uint16 frameType);

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhDe3);
    }

static eAtRet HwIdFactorGet(ThaPdhDe3 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice)
    {
    eAtRet ret = cAtOk;
    uint32 de3Id = 0;
    uint8 u8sliceId, u8hwIdInSlice;
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);

    if (sdhChannel)
        {
        ret = ThaSdhChannel2HwMasterStsId(sdhChannel, phyModule, &u8sliceId, &u8hwIdInSlice);
        *slice = (uint32)u8sliceId;
        *hwIdInSlice = (uint32)u8hwIdInSlice;
        return ret;
        }

    de3Id = AtChannelIdGet((AtChannel)self);
    *slice = 0;
    *hwIdInSlice = de3Id;
    return cAtOk;
    }

static uint32 PartOffset(ThaPdhDe3 self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cAtModulePdh, (uint8)ThaPdhDe3PartId(self));
    }

static uint32 DefaultOffset(ThaPdhDe3 self)
    {
    uint8 sliceId, hwIdInSlice;

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &hwIdInSlice);
    return hwIdInSlice + PartOffset(self) + (uint32)ThaModulePdhSliceOffset(PdhModule((AtChannel)self), sliceId);
    }

static uint32 DemapDefaultOffset(ThaPdhDe3 self)
    {
    uint8 sliceId, hwIdInSlice;

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &hwIdInSlice);
    return (hwIdInSlice * 32UL) + PartOffset(self) + (uint32)ThaModulePdhSliceOffset(PdhModule((AtChannel)self), sliceId);
    }

static eBool IsStmProduct(ThaPdhDe3 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceModuleGet(device, cAtModuleSdh) ? cAtTrue : cAtFalse;
    }

static uint8 DefaultLosThreshold(ThaPdhDe3 self)
    {
    if (ThaModulePdhDetectDe3LosByAllZeroPattern(PdhModule((AtChannel)self)))
        return 3;

    return IsStmProduct(self) ? 0 : 1;
    }

static uint8 DefaultAisAllOnesThreshold(ThaPdhDe3 self)
    {
    return (AtPdhDe3IsDs3((AtPdhDe3)self)) ? 0 : 1;
    }

static void DefaultThresholdSet(ThaPdhDe3 self)
    {
    uint8 losThreshold = mMethodsGet(self)->DefaultLosThreshold(self);
    uint8 aisAllOnesThreshold = mMethodsGet(self)->DefaultAisAllOnesThreshold(self);
    uint32 regAddr = cThaRegRxM23E23Ctrl + mMethodsGet(self)->DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldIns(&regVal, cThaRxDS3E3LosThresMask, cThaRxDS3E3LosThresShift, losThreshold);
    mFieldIns(&regVal, cThaRxDS3E3AisThresMask, cThaRxDS3E3AisThresShift, aisAllOnesThreshold);
    mFieldIns(&regVal, cThaRxDS3E3LofThresMask, cThaRxDS3E3LofThresShift, 2);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    }

static void RxFeacBufferReset(ThaPdhDe3 self)
    {
    uint32 regAddr = mRxDe3FeacBuffer(self) + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    mWrite(self, regAddr, 0);
    }

static uint32 RxDs3E3OHProCtrl(ThaPdhDe3 self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    return ThaModulePdhRxDs3E3OHProCtrlRegister(pdhModule);
    }

static eAtRet FeacEnable(ThaPdhDe3 self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);

    RxFeacBufferReset(self);

    regAddr = mRxDs3E3OHProCtrl(self) + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRegRxDs3FEACEnMask, cThaRegRxDs3FEACEnShift, enable ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet FBitEnable(ThaPdhDe3 self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);

    regAddr = mRxDs3E3OHProCtrl(self) + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRegRxDs3FbitCntEnMask, cThaRegRxDs3FbitCntEnShift, enable ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet MbitEnable(ThaPdhDe3 self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);

    regAddr = mRxDs3E3OHProCtrl(self) + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRegRxDs3MbitCntEnMask, cThaRegRxDs3MbitCntEnShift, enable ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 InCorrectionTxAisForceMask(AtChannel self)
    {
    eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if (AtPdhDe3FrameTypeIsE3(frameType))
        return cThaTxDS3E3LineAllOneMask;

    if ((frameType == cAtPdhDs3Unfrm) || (frameType == cAtPdhDe3FrameUnknown))
        return 0;

    return cThaTxDS3FrameAisMask;
    }

static uint32 CorrectionTxAisForceMask(AtChannel self)
    {
    AtUnused(self);
    return cThaTxDS3E3LineAisSelectMask;
    }

static uint32 TxAisForceMask(AtChannel self)
    {
    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        return CorrectionTxAisForceMask(self);

    return InCorrectionTxAisForceMask(self);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    uint32 alarms = 0;
    AtPw pw;

    if (TxAisForceMask(self))
        alarms = cAtPdhDe3AlarmAis;

    if (AtPdhDe3FrameTypeIsUnframed(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        return alarms;

    pw = AtChannelBoundPwGet(self);
    if ((AtPwTypeGet(pw) == cAtPwTypeSAToP) && !AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
        return alarms;

    alarms |= cAtPdhDe3AlarmRai;

    return alarms;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forcibleAlarms = cAtPdhDe3AlarmLof;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    if (ThaModulePdhAisDownStreamIsSupported(pdhModule))
        forcibleAlarms = forcibleAlarms | cAtPdhDe3AlarmAis;

    return forcibleAlarms;
    }

static eBool IsDs3CbitFormat(ThaPdhDe3 self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    switch (frameType)
        {
        case cAtPdhDs3FrmCbitUnChn:         return cAtTrue;
        case cAtPdhDs3FrmCbitChnl28Ds1s:    return cAtTrue;
        case cAtPdhDs3FrmCbitChnl21E1s:     return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool IsDs3FrameHasMbit(ThaPdhDe3 self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    switch (frameType)
        {
        case cAtPdhDs3FrmCbitUnChn:         return cAtTrue;
        case cAtPdhDs3FrmCbitChnl28Ds1s:    return cAtTrue;
        case cAtPdhDs3FrmCbitChnl21E1s:     return cAtTrue;
        case cAtPdhDs3FrmM23:               return cAtTrue;
        case cAtPdhDs3FrmM13Chnl28Ds1s:     return cAtTrue;
        case cAtPdhDs3FrmM13Chnl21E1s:      return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool IsDe3FrameFormat(ThaPdhDe3 self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    switch (frameType)
        {
        case cAtPdhDe3FrameUnknown:         return cAtFalse;
        case cAtPdhDs3Unfrm:                return cAtFalse;
        case cAtPdhE3Unfrm:                 return cAtFalse;
        default: return cAtTrue;
        }
    }

static void OverHeadControlDefault(ThaPdhDe3 self)
    {
    /* Enable FEAC */
    FeacEnable(self, IsDs3CbitFormat(self));

    /* Enable F-bit */
    FBitEnable(self, IsDe3FrameFormat(self));

    /* Enable MBit */
    MbitEnable(self, IsDs3FrameHasMbit(self));
    }

static void DefaultSet(ThaPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;
    ThaModulePdh pdhModule;
    eAtRet ret;

    DefaultThresholdSet(self);

    /* Auto yellow */
    ret = AtPdhChannelAutoRaiEnable((AtPdhChannel)self, cAtTrue);

    /*  OH Pro Control default */
    OverHeadControlDefault(self);

    /* Clear all interrupt mask */
    ret |= AtChannelInterruptMaskSet(channel, AtChannelInterruptMaskGet(channel), 0x0);

    /* Disable all of alarms that are being forced */
    ret |= AtChannelRxAlarmUnForce(channel, AtChannelRxForcableAlarmsGet(channel));
    ret |= AtChannelTxAlarmUnForce(channel, AtChannelTxForcableAlarmsGet(channel));
    ret |= AtChannelLoopbackSet(channel, cAtPdhLoopbackModeRelease);

    /* Use DL mode as default for datalink */
    pdhModule = (ThaModulePdh)AtChannelModuleGet(channel);
    if (ThaModulePdhMdlIsSupported(pdhModule))
        {
        ret |= AtPdhDe3DataLinkOptionSet((AtPdhDe3)self, cAtPdhDe3DataLinkOptionDlBits);
        ret |= AtPdhChannelDataLinkEnable((AtPdhChannel) self, cAtFalse);
        }

    if (ret != cAtOk)
        mChannelLog(self, cAtLogLevelCritical, "Default set fail");
    }

static eAtRet AutoRaiEnable(AtPdhChannel self, eBool enable)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 offset = mMethodsGet(de3)->DefaultOffset(de3);
    uint32 regAddr, regVal;

    /* Auto yellow */
    regAddr = cThaRegTxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaTxDS3E3AutoYelMask, cThaTxDS3E3AutoYelShift, enable ? 0x1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool AutoRaiIsEnabled(AtPdhChannel self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 offset = mMethodsGet(de3)->DefaultOffset(de3);
    uint32 regAddr, regVal;
    uint8 enabled;

    /* Auto yellow */
    regAddr = cThaRegTxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    mFieldGet(regVal, cThaTxDS3E3AutoYelMask, cThaTxDS3E3AutoYelShift, uint8, &enabled);

    return enabled ? cAtTrue : cAtFalse;
    }

static eBool CanChangeTxSignal(AtPdhDe3 self)
    {
    AtPw pseudowire = AtChannelBoundPwGet((AtChannel)self);
    AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel)self);

    if (pseudowire == NULL)
        return cAtTrue;

    if ((engine != NULL) && AtPrbsEngineIsEnabled(engine))
        return cAtTrue;

    if (AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
        return cAtTrue;

    if (ThaPdhChannelShouldRejectTxChangeWhenBoundPw((AtPdhChannel)self))
        return cAtFalse;

    return cAtTrue;
    }

static eBool IdleSignalIsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet IdleEnable(AtPdhChannel self, eBool enable)
    {
    uint32 regAddr, regVal;

    if (enable && !CanChangeTxSignal((AtPdhDe3)self))
        return cAtErrorNotApplicable;

    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regVal  = mRead(self, regAddr);
    mRegFieldSet(regVal, cThaTxDS3E3IdleSet, enable ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool  IdleIsEnabled(AtPdhChannel self)
    {
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mRead(self, regAddr);

    return (regVal & cThaTxDS3E3IdleSetMask) ? cAtTrue : cAtFalse;
    }

static uint8 FeacSignalTypeSw2Hw(eAtPdhDe3FeacType signalType)
    {
    switch (signalType)
        {
        case cAtPdhDe3FeacTypeAlarm       : return 3;
        case cAtPdhDe3FeacTypeLoopDeactive: return 2;
        case cAtPdhDe3FeacTypeLoopActive  : return 1;
        case cAtPdhDe3FeacTypeHistoryAlarm       :
        case cAtPdhDe3FeacTypeHistoryLoopDeactive:
        case cAtPdhDe3FeacTypeHistoryLoopActive  :
        case cAtPdhDe3FeacTypeUnknown     : return 0;
        default: return 0;
        }
    }

static eAtPdhDe3FeacType FeacSignalTypeHw2Sw(uint8 hwSignalType)
    {
    switch (hwSignalType)
        {
        case 3: return cAtPdhDe3FeacTypeAlarm;
        case 2: return cAtPdhDe3FeacTypeLoopDeactive;
        case 1: return cAtPdhDe3FeacTypeLoopActive;
        default: return cAtPdhDe3FeacTypeUnknown;
        }
    }

static eAtPdhDe3FeacType RxFeacSignalTypeHw2Sw(uint8 hwSignalType)
    {
    switch (hwSignalType)
        {
        case 7: return cAtPdhDe3FeacTypeHistoryAlarm;
        case 6: return cAtPdhDe3FeacTypeHistoryLoopDeactive;
        case 5: return cAtPdhDe3FeacTypeHistoryLoopActive;
        case 3: return cAtPdhDe3FeacTypeAlarm;
        case 2: return cAtPdhDe3FeacTypeLoopDeactive;
        case 1: return cAtPdhDe3FeacTypeLoopActive;

        default: return cAtPdhDe3FeacTypeUnknown;
        }
    }

static uint32 FeacTypeToSentThreshold(eAtPdhDe3FeacType signalType)
    {
    if (signalType == cAtPdhDe3FeacTypeUnknown)
        return cDs3CbitFeacHwThresholdSentStop;

    if (signalType == cAtPdhDe3FeacTypeAlarm)
        return cDs3CbitFeacHwThresholdSentContinuous;

    return cDs3CbitFeacHwThresholdSentOneShot;
    }

static uint32 FeacSentModeToSentThreshold(eAtPdhBomSentMode sentMode)
    {
    if (sentMode == cAtPdhBomSentModeInvalid)
        return cDs3CbitFeacHwThresholdSentStop;

    if (sentMode == cAtPdhBomSentModeContinuous)
        return cDs3CbitFeacHwThresholdSentContinuous;

    return cDs3CbitFeacHwThresholdSentOneShot;
    }

static uint32 FeacSentModeFromSentThreshold(uint32 sentThreshold)
    {
    if (sentThreshold == cDs3CbitFeacHwThresholdSentStop)
        return cAtPdhBomSentModeInvalid;

    if (sentThreshold == cDs3CbitFeacHwThresholdSentContinuous)
        return cAtPdhBomSentModeContinuous;

    return cAtPdhBomSentModeOneShot;
    }

static eAtRet HwTxFeacSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 code, uint32 sentThreshold)
    {
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mRead(self, regAddr);

    mRegFieldSet(regVal, cThaTxDs3FeacThres, sentThreshold);
    mRegFieldSet(regVal, cThaTxDs3FeacSig, FeacSignalTypeSw2Hw(signalType));
    mRegFieldSet(regVal, cThaTxDs3Feac, code);
    mWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 HwTxFeacThresholdGet(AtPdhDe3 self)
    {
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mRead(self, regAddr);
    uint32 sentThreshold = 0;

    sentThreshold = mRegField(regVal, cThaTxDs3FeacThres);
    return sentThreshold;
    }

static eAtRet TxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *code)
    {
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mRead(self, regAddr);
    uint8 hwCode   = (uint8)mRegField(regVal, cThaTxDs3Feac);
    uint8 hwSig    = (uint8)mRegField(regVal, cThaTxDs3FeacSig);

    *code = hwCode;
    *signalType = FeacSignalTypeHw2Sw(hwSig);

    return cAtOk;
    }

static eAtRet TxFeacSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 code)
    {
    eAtPdhDe3FeacType currentSignalType;
    uint8 currentCode;
    uint32 sentThreshold = FeacTypeToSentThreshold(signalType);

    if (!CanChangeTxSignal(self))
        return cAtErrorNotApplicable;

    TxFeacGet(self, &currentSignalType, &currentCode);
    if ((signalType != currentSignalType) || (code != currentCode))
        {
        HwTxFeacSet(self, cAtPdhDe3FeacTypeUnknown, currentCode, 0);
        AtOsalUSleep(4000);/*4ms for HW to finish sending*/
        }

    HwTxFeacSet(self, signalType, code, sentThreshold);
    return cAtOk;
    }

static eAtRet TxFeacWithModeSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 code, eAtPdhBomSentMode sentMode)
    {
    eAtPdhDe3FeacType currentSignalType;
    uint8 currentCode;
    uint32 sentThreshold = FeacSentModeToSentThreshold(sentMode);
    eAtPdhBomSentMode currentSentMode = AtPdhDe3TxFeacModeGet(self);

    if (!CanChangeTxSignal(self))
        return cAtErrorNotApplicable;

    TxFeacGet(self, &currentSignalType, &currentCode);
    if ((signalType != currentSignalType) || (code != currentCode) || (sentMode != currentSentMode))
        {
        HwTxFeacSet(self, cAtPdhDe3FeacTypeUnknown, currentCode, 0);
        AtOsalUSleep(1000);/*1ms for HW to finish sending*/
        }

    HwTxFeacSet(self, signalType, code, sentThreshold);
    return cAtOk;
    }

static eAtPdhBomSentMode TxFeacModeGet(AtPdhDe3 self)
    {
    uint32 sentThreshold = HwTxFeacThresholdGet(self);
    return FeacSentModeFromSentThreshold(sentThreshold);
    }

static eBool RxHw3BitFeacSignalIsSupported(ThaPdhDe3 self)
    {
    return ThaModulePdhRxHw3BitFeacSignalIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static void RxFeacMaskShift(ThaPdhDe3 self, uint32 *mask, uint32 *shift)
    {
    if (RxHw3BitFeacSignalIsSupported(self))
        {
        *mask = cBit8_6;
        *shift = 6;
        return;
        }

    *mask = cBit7_6;
    *shift = 6;
    }

static eAtRet RxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *code)
    {
    uint32 regAddr = mRxDe3FeacBuffer(self) + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 hwSigType, hwCode;
    uint32 stateMask, stateShift;

    RxFeacMaskShift(mThis(self), &stateMask, &stateShift);
    mFieldGet(regVal, cTha_rx_de3_feac_buffer_6xMask, cTha_rx_de3_feac_buffer_6xShift, uint8, &hwCode);
    mFieldGet(regVal, stateMask, stateShift, uint8, &hwSigType);

    *code = hwCode;
    *signalType = RxFeacSignalTypeHw2Sw(hwSigType);

    return cAtOk;
    }

static eAtRet TxTtiSet(AtPdhDe3 self, const tAtPdhTti *tti)
    {
    AtUnused(self);
    AtUnused(tti);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
    AtUnused(self);
    AtUnused(tti);
    return cAtErrorModeNotSupport;
    }

static eAtRet ExpectedTtiSet(AtPdhDe3 self, const tAtPdhTti *tti)
    {
    AtUnused(tti);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet ExpectedTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
    AtUnused(tti);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet RxTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
    AtUnused(tti);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet TmEnable(AtPdhDe3 self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool TmIsEnabled(AtPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxTmCodeSet(AtPdhDe3 self, uint8 tmCode)
    {
    AtUnused(tmCode);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8  TxTmCodeGet(AtPdhDe3 self)
    {
    AtUnused(self);
    return 0;
    }

static uint8  RxTmCodeGet(AtPdhDe3 self)
    {
    uint32 regAddr;
    uint32 offset;
    uint32 longRegVal[cThaNumDwordsInLongReg];

    if (mMethodsGet(self)->TmIsEnabled(self) == cAtFalse)
        return 0;

    offset  = mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regAddr = cThaRegRxM23E23OHCurStatus + offset;
    mChannelHwLongRead(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);

    return (uint8)mRegField(longRegVal[0], cThaG832TM);
    }

static eAtRet TxPldTypeSet(AtPdhDe3 self, uint8 pldType)
    {
    uint32 regAddr, regVal;

    if (!CanChangeTxSignal(self))
        return cAtErrorNotApplicable;

    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regVal  = mRead(self, regAddr);
    mRegFieldSet(regVal, cThaTxE3G832PldType, pldType);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint8  TxPldTypeGet(AtPdhDe3 self)
    {
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal  = mRead(self, regAddr);

    return (uint8)mRegField(regVal, cThaTxE3G832PldType);
    }

static uint8  RxPldTypeGet(AtPdhDe3 self)
    {
    uint32 regAddr = cThaRegRxM23E23OHCurStatus + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 longRegVal[cThaNumDwordsInLongReg];

    mChannelHwLongRead(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);

    return (uint8)mRegField(longRegVal[0], cThaG832PT);
    }

static eBool FrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
    AtUnused(self);
    return mOutOfRange(frameType, cAtPdhDs3Unfrm, cAtPdhE3FrmG751Chnl16E1s) ? cAtFalse : cAtTrue;
    }

static uint8 FrameTypeSw2Hw(uint16 frameType)
    {
    if (frameType == cAtPdhDs3Unfrm)             return 0;
    if (frameType == cAtPdhDs3FrmCbitUnChn)      return 3;
    if (frameType == cAtPdhDs3FrmCbitChnl28Ds1s) return 1;
    if (frameType == cAtPdhDs3FrmCbitChnl21E1s)  return 1;
    if (frameType == cAtPdhDs3FrmM23)            return 2;
    if (frameType == cAtPdhDs3FrmM13Chnl28Ds1s)  return 2;
    if (frameType == cAtPdhDs3FrmM13Chnl21E1s)   return 2;
    if (frameType == cAtPdhE3Unfrm)              return 4;
    if (frameType == cAtPdhE3Frmg832)            return 5;
    if (frameType == cAtPdhE3FrmG751)            return 7;
    if (frameType == cAtPdhE3FrmG751Chnl16E1s)   return 6;

    return 0;
    }

static eBool MappedToTu3(AtPdhChannel self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(self);
    AtSdhChannel parent;

    if (vc == NULL)
        return cAtFalse;

    parent = AtSdhChannelParentChannelGet(vc);
    return (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3) ? cAtTrue : cAtFalse;
    }

void PdhDe3SdhParentMappedTypeSet(AtPdhChannel self)
    {
    uint32 regAddr, regVal;
    ThaPdhDe3 thaDe3 = (ThaPdhDe3)self;
    regAddr = cThaRegStsVtDmapCtrl + mMethodsGet(thaDe3)->DemapDefaultOffset(thaDe3);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal, cThaStsVtDemapCepMdMask, cThaStsVtDemapCepMdShift, MappedToTu3(self) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    }

eAtRet ThaPdhDe3MapSet(AtPdhChannel self)
    {
    eAtRet ret = 0;
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    AtModulePdh pdhModule  = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);

    ret |= ThaCdrDe3PldSet(cdrModule, self);
    ret |= ThaPdhDe3MapMdSet(pdhModule, self);
    ret |= MapFrameModeDefaultSet((AtPdhChannel)self, AtPdhChannelFrameTypeGet(self));

    return ret;
    }

static uint8 NumDe1InDe2(uint32 frameType)
    {
    if ((frameType == cAtPdhDs3FrmCbitChnl21E1s) || (frameType == cAtPdhDs3FrmM13Chnl21E1s))
        return 3;
    if (frameType == cAtPdhE3FrmG751Chnl16E1s)
        return 3;
    if ((frameType == cAtPdhDs3FrmM13Chnl28Ds1s) || (frameType == cAtPdhDs3FrmCbitChnl28Ds1s))
        return 4;
    return 0;
    }

static uint32 De2De1RxFrmrCtrlOffset(ThaModulePdh self, uint8 sliceId, uint8 idInSlice, uint8 de2Id, uint8 de1Id)
    {
    int32 sliceOffset = mMethodsGet(self)->SliceOffset(self, (uint32)sliceId);
    uint32 offset;

    offset = 32UL * idInSlice + 4UL * de2Id + de1Id;
    return (uint32)((int32)offset + sliceOffset);
    }

static eBool IsTerminatedDe3(ThaPdhDe3 self)
    {
    AtSdhVc vc3 = AtPdhChannelVcInternalGet((AtPdhChannel)self);

    if (AtChannelBoundPwGet((AtChannel)self))
        return cAtFalse;

    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)vc3))
        return cAtFalse;

    return cAtTrue;
    }

static void DE3MuxToDE1(AtPdhChannel de3, uint16 frameType)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)de3), cAtModulePdh);
    uint32 address = ThaModulePdhRxM12E12MuxRegister(pdhModule) + mMethodsGet((ThaPdhDe3)de3)->DefaultOffset((ThaPdhDe3)de3);
    uint32 regVal = 0, rxde1frm = 0;
    AtSdhVc vc3 = AtPdhChannelVcInternalGet(de3);
    AtPw pseudowire = AtChannelBoundPwGet((AtChannel)de3);

    if ((vc3 == NULL) && (pseudowire == NULL))
        ThaPdhDe3MapSet(de3);

    if (AtPdhDe3IsChannelizedToDs1(frameType))
        {
        regVal = 0;/*DS1*/
        rxde1frm = 0;
        }
    else
        {
        regVal = 0x1555; /* E1 */
        rxde1frm = 8;
        }

    mWrite(de3, address, regVal);

    /* If DS3/E3 is non-terminated, just return */
    if (!IsTerminatedDe3((ThaPdhDe3)de3))
        return;

    /* Set all RX De1 framers to the same either DS1 or E1*/
    if (AtPdhDe3IsChannelizedFrameType(frameType))
        {
        uint8 de2Num = 7;
        uint8 de1Num = NumDe1InDe2(frameType);
        uint8 de2Id, de1Id;
        uint8 slice = 0, idInSlice = 0;

        ThaPdhChannelHwIdGet(de3, cAtModulePdh, &slice, &idInSlice);
        for (de2Id = 0; de2Id < de2Num; de2Id++)
            {
            for (de1Id = 0; de1Id < de1Num; de1Id++)
                {
                address = cThaRegDS1E1J1RxFrmrCtrl + De2De1RxFrmrCtrlOffset(pdhModule, slice, idInSlice, de2Id, de1Id);
                regVal = mModuleHwRead(pdhModule, address);
                mFieldIns(&regVal, cThaRxDE1MdMask, cThaRxDE1MdShift, rxde1frm);
                mModuleHwWrite(pdhModule, address, regVal);
                }
            }
        }
    }

static eBool De3AisPatternSupported(AtPdhChannel self)
    {
    return ThaModulePdhDe3AisSelectablePatternIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static eAtRet HwRxFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    ThaPdhDe3 thaDe3 = (ThaPdhDe3)self;
    uint32 offset = mMethodsGet(thaDe3)->DefaultOffset(thaDe3);
    uint32 regAddr = cThaRegRxM23E23Ctrl + offset;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 hwFrameMode = FrameTypeSw2Hw(frameType);
    

    mFieldIns(&regVal, cThaRxDS3E3MdMask, cThaRxDS3E3MdShift, hwFrameMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static uint32 AisPatternTypeValueOnFrameType(uint32 swFrameType)
    {
    if (AtPdhDe3FrameTypeIsE3(swFrameType))
        return 0;

    if (AtPdhDe3FrameTypeIsUnframed(swFrameType))
        return 0;

    return 1;
    }

static uint8 TxUnframedFrameType(uint16 frameType)
    {
    return AtPdhDe3FrameTypeIsE3(frameType) ? 4 : 0;
    }

static eAtRet HwTxFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    ThaPdhDe3 thaDe3 = (ThaPdhDe3)self;
    uint8 hwFrameMode = FrameTypeSw2Hw(frameType);
    uint32 offset = mMethodsGet(thaDe3)->DefaultOffset(thaDe3);
    uint32 regAddr = cThaRegTxM23E23Ctrl + offset;
    uint32 regVal = mRead(self, regAddr);

    if (De3AisPatternSupported(self))
        {
        uint32 hwAisPatternTypeValue = AisPatternTypeValueOnFrameType(frameType);
        mRegFieldSet(regVal, cThaTxDS3E3LineAisPatternSelect, hwAisPatternTypeValue);
        }

    if (!IsTerminatedDe3(thaDe3))
        hwFrameMode = TxUnframedFrameType(frameType);

    mFieldIns(&regVal, cThaTxDS3E3MdMask, cThaTxDS3E3MdShift, hwFrameMode);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet HwFrameTypeSet(ThaPdhDe3 self, uint16 frameType)
    {
    eAtRet ret = cAtOk;
    AtPdhChannel de3 = (AtPdhChannel)self;

    ret |= HwRxFrameTypeSet(de3, frameType);
    ret |= HwTxFrameTypeSet(de3, frameType);

    return ret;
    }

static eAtRet HwFrameTypeSetAndResetAllDe1s(AtPdhChannel self, uint16 frameType, eBool resetAllDe1s)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);

    HwFrameTypeSet((ThaPdhDe3)self, frameType);

    if (resetAllDe1s)
        DE3MuxToDE1(self, frameType);

    if (ThaModulePdhDatalinkIsSupported(modulePdh))
        {
        AtPdhChannelDataLinkEnable(self, cAtFalse);

        if (AtPdhDs3DataLinkIsApplicable((AtPdhDe3) self))
            AtPdhChannelDataLinkFcsBitOrderSet(self, cAtBitOrderLsb);
        }

    return cAtOk;
    }

static eAtRet NotApplicableAlarmsNotify(AtPdhChannel self, uint16 newFrameType)
    {
    /* Only change to unframe will have not applicable alarms */
    if (!AtPdhDe3FrameTypeIsUnframed(newFrameType))
        return cAtOk;

    /* And only if previous frame type is framed */
    if (AtPdhDe3FrameTypeIsUnframed(mThis(self)->frameType))
        return cAtOk;

    AtChannelAllAlarmListenersCall((AtChannel)self, (cAtPdhDe3AlarmLof | cAtPdhDe3AlarmRai), 0);

    return cAtOk;
    }

static eAtRet HandleAisAllOneDetection(AtPdhChannel self, ThaModulePdh pdhModule, uint16 frameType)
    {
    eBool enabled;

    /* Do nothing because current logic so far already handles this */
    if (ThaModulePdhShouldDetectAisAllOnesWithUnframedMode(pdhModule))
        return cAtOk;

    /* Only explicitly handle this for products that require this */
    enabled = AtPdhDe3FrameTypeIsUnframed(frameType) ? cAtFalse : cAtTrue;
    return AtPdhChannelAisAllOnesDetectionEnable(self, enabled);
    }

static AtModulePrbs PrbsModule(AtChannel self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePrbs);
    }

static eBool ShouldUpdateAisInterruptMaskOnNewFrameType(uint16 currentFrameType, uint16 newFrameType)
    {
    if (mBoolToBin(AtPdhDe3FrameTypeIsE3(currentFrameType)) == mBoolToBin(AtPdhDe3FrameTypeIsE3(newFrameType)))
        return cAtFalse;
    return cAtTrue;
    }

static uint32 NumDs1sForFrameType(uint16 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs3FrmCbitChnl28Ds1s: return 28;
        case cAtPdhDs3FrmM13Chnl28Ds1s : return 28;
        case cAtPdhDs3FrmCbitChnl21E1s : return 21;
        case cAtPdhDs3FrmM13Chnl21E1s  : return 21;
        case cAtPdhE3FrmG751Chnl16E1s  : return 16;
        default:
            return 0;
        }
    }

static eBool NeedRetainDe1s(uint16 oldFrameType, uint16 newFrameType)
    {
    uint32 numDe1sForOldFrameType = NumDs1sForFrameType(oldFrameType);
    uint32 numDe1sForNewFrameType = NumDs1sForFrameType(newFrameType);

    if ((numDe1sForNewFrameType == numDe1sForOldFrameType) && (numDe1sForNewFrameType > 0))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet RetainDe1s(AtPdhChannel self, uint16 frameType)
    {
    HwFrameTypeSetAndResetAllDe1s(self, frameType, cAtFalse);
    mThis(self)->frameType = frameType;
    OverHeadControlDefault((ThaPdhDe3)self);
    return cAtOk;
    }

static eBool ChangingBetweenDs3AndE3(uint16 currentFrameType, uint16 newFrameType)
    {
    if (currentFrameType == cAtPdhDe3FrameUnknown)
        return cAtFalse;

    if (mBoolToBin(AtPdhDe3FrameTypeIsE3(currentFrameType)) == mBoolToBin(AtPdhDe3FrameTypeIsE3(newFrameType)))
        return cAtFalse;

    return cAtTrue;
    }

static eBool CanChangeFrameType(AtPdhChannel self, uint16 currentFrameType, uint16 newFrameType)
    {
    /* Allow to hot change frame type while being bound SAToP PW with assumption that sub-channels are all free as SDK
     * has prevented binding PW while any sub channel is busy */
    if (AtChannelBoundPwGet((AtChannel)self))
        {
        if (!ThaModulePdhDe3FramedSatopIsSupported(PdhModule((AtChannel)self)))
            return cAtFalse;

        if (!ChangingBetweenDs3AndE3(currentFrameType, newFrameType))
            return cAtTrue;
        }

    if (AtChannelServiceIsRunning((AtChannel)self))
        {
        if (NeedRetainDe1s(currentFrameType, newFrameType))
            return cAtTrue;

        return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet FramedSatopApply(AtPdhChannel self, ThaModulePdh module, uint16 frameType)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);
    AtPw adapter = (AtPw)ThaPwAdapterGet(pw);
    eAtRet ret;
    AtUnused(frameType);

    /* Framed SAToP is not being used or it is not supported */
    if ((adapter == NULL) || !ThaModulePdhDe3FramedSatopIsSupported(module))
        return cAtOk;

    ret  = ThaModuleAbstractMapBindDe3ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhDe3)self, adapter);
    ret |= ThaModuleAbstractMapBindDe3ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe3)self, adapter);
    ret |= mMethodsGet(mThis(self))->De3FramingBindToPwSatop(mThis(self), adapter);

    return ret;
    }

static eAtPdhDe3FrameType UnframedMode(AtPdhChannel self)
    {
    eAtPdhDe3FrameType frameMode = AtPdhChannelFrameTypeGet(self);

    if (AtPdhDe3FrameTypeIsE3(frameMode))
        return cAtPdhE3Unfrm;
    else
        return cAtPdhDs3Unfrm;
    }

static eAtRet TcaNotificationMaskUpdate(AtPdhChannel self, uint16 frameType)
    {
    AtSurEngine surEngine = AtChannelSurEngineGet((AtChannel)self);
    uint32 tcaMask, ds3CbitParams;

    if (surEngine == NULL)
        return cAtOk;

    if (AtPdhDe3IsDs3CbitFrameType(frameType))
        return cAtOk;

    if (!AtSurEngineTcaIsSupported(surEngine))
        return cAtOk;

    tcaMask = AtSurEngineTcaNotificationMaskGet(surEngine);
    ds3CbitParams = AtSurEngineDs3CbitParams();
    if (tcaMask & ds3CbitParams)
        {
        AtModule sur = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleSur);
        if (AtModuleSurTcaIsSupported((AtModuleSur)sur))
            return AtSurEngineTcaNotificationMaskSet(surEngine, ds3CbitParams, 0);
        }

    return cAtOk;
    }

static eAtRet MapFrameModeDefaultSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    uint16 mapFrameType;
    AtSdhVc vc3 = AtPdhChannelVcInternalGet(self);

    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)vc3))
        return cAtOk;

    if (AtChannelBoundPwGet((AtChannel)self))
        mapFrameType = UnframedMode(self);
    else
        mapFrameType = frameType;
    ret |= ThaModuleAbstractMapDe3FrameModeSet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhDe3)self, mapFrameType);
    ret |= ThaModuleAbstractMapDe3FrameModeSet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe3)self, frameType);

    return ret;
    }

static eAtRet CanControlDe3Timing(AtPdhChannel self)
    {
    AtSdhVc vc3 = AtPdhChannelVcInternalGet(self);

    if (vc3 == NULL)
        return cAtTrue;

    /* If VC-3 mapping DS3/E3 Line is active, we should not configure DS3/E3
     * timing. Timing resource is now used for VC-3 instead. */
    if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)vc3))
        return cAtFalse;

    return cAtTrue;
    }

static uint16 FrameTypeForGeneric(AtPdhChannel self, uint16 frameType)
    {
    if (IsTerminatedDe3((ThaPdhDe3)self))
        return frameType;

    if ((frameType == cAtPdhDs3FrmCbitChnl21E1s) ||
        (frameType == cAtPdhDs3FrmCbitChnl28Ds1s)||
        (frameType == cAtPdhDs3FrmM13Chnl21E1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl28Ds1s))
        return cAtPdhDs3FrmCbitUnChn;

    if (frameType == cAtPdhE3FrmG751Chnl16E1s)
        return cAtPdhE3FrmG751;

    return frameType;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    eAtRet ret = 0;
    AtSdhVc vc3 = AtPdhChannelVcInternalGet(self);
    eAtTimingMode timingMode;
    AtChannel timingSource;
    AtBerController controller;
    AtPrbsEngine prbsEngine;
    eBool prbsEnabled = cAtFalse;
    uint32 interruptMask;
    eBool shouldUpdateInterruptMask;
    AtPw pseudowire = AtChannelBoundPwGet((AtChannel)self);

    if (!AtPdhChannelFrameTypeIsSupported(self, frameType))
        return cAtErrorInvlParm;

    if (mThis(self)->frameType == frameType)
        return cAtOk;

    if (!CanChangeFrameType(self, mThis(self)->frameType, frameType))
        return cAtErrorResourceBusy;

    /* Need to disable all of not applicable TCA mask on new frame type */
    TcaNotificationMaskUpdate(self, frameType);

    if (NeedRetainDe1s(mThis(self)->frameType, frameType))
        {
        ret = RetainDe1s(self, frameType);
        ret |= FramedSatopApply(self, (ThaModulePdh)pdhModule, frameType);
        return ret;
        }

    shouldUpdateInterruptMask = ShouldUpdateAisInterruptMaskOnNewFrameType(mThis(self)->frameType, frameType);
    interruptMask = AtChannelInterruptMaskGet((AtChannel)self);

    /* Disable PRBS to re-activate it later */
    prbsEngine = AtChannelPrbsEngineGet((AtChannel)self);
    if (prbsEngine)
        {
        /* PRBS only associate with DS3/E3 when their frame type is Unchannelized
         * and then application want set frame type to Channelized SDK should not
         * allow to this frame-type changing. Because PRBS not yet support when
         * DS3/E3 Channelized. */
        if (AtPdhDe3IsChannelizedFrameType(frameType) &&
            !AtModulePrbsAllChannelsSupported(PrbsModule((AtChannel)self)))
            {
            AtChannelLog((AtChannel) self,
                         cAtLogLevelWarning,
                         AtSourceLocation,
                         "Can not change frame type of DS3/E3 %s from Unchannelized to channelized when it has PRBS Engine \r\n",
                         AtChannelIdString((AtChannel) self));
            return cAtErrorResourceBusy;
            }

        prbsEnabled = AtPrbsEngineIsEnabled(prbsEngine);
        if (prbsEnabled)
            AtPrbsEngineEnable(prbsEngine, cAtFalse);
        }

    AtPdhChannelSubChannelsInterruptDisable(self, cAtTrue);
    AtPdhChannelSubChannelsHardwareCleanup(self);

    /* Apply hardware Frame Type */
    HwFrameTypeSetAndResetAllDe1s(self, frameType, cAtTrue);
    NotApplicableAlarmsNotify(self, frameType);
    mThis(self)->frameType = frameType;

    /* Update Channel type for DE3 Path BER Controller */
    controller = AtPdhChannelPathBerControllerGet(self);
    if (controller && AtBerControllerIsEnabled(controller))
		ret |= AtBerControllerHwChannelTypeUpdate(controller);

    /* Configure CDR */
    if (CanControlDe3Timing(self))
        {
        timingMode = AtChannelTimingModeGet((AtChannel)self);
        timingSource = AtChannelTimingSourceGet((AtChannel)self);
        ret |= AtChannelTimingSet((AtChannel)self, timingMode, timingSource);
        }

    /* re-configure map vc if frame type is ds3 and e3 */
    if (vc3)
        {
        /* Do not re-configure VC-3 mapping if either SAToP bound or DS3/E3 line
         * over VC-3 bound CEP */
        if ((pseudowire == NULL) && AtChannelBoundPwGet((AtChannel)vc3) == NULL)
            ret |= ThaSdhAuVcDs3Map((ThaSdhAuVc)vc3);
        ret |= ThaStmModulePdhRxDe3VtgTypeSet(pdhModule, self, cThaPdhStsVc);
        }
    else
        {
        if (pseudowire == NULL)
            ret |= ThaPdhDe3MapSet(self);
        }

    /* Configure MAP/DEMAP */
    ret |= MapFrameModeDefaultSet(self, frameType);

    /* Let super deal with database */
    ret |= m_AtPdhChannelMethods->FrameTypeSet(self, FrameTypeForGeneric(self, frameType));

    /* OH control default */
    OverHeadControlDefault((ThaPdhDe3)self);
    DefaultThresholdSet((ThaPdhDe3)self);

    /* Activate PRBS engine if it is enabled before */
    if (prbsEngine && prbsEnabled)
        ret |= AtPrbsEngineEnable(prbsEngine, cAtTrue);

    /* Handle AIS All One Patterns */
    ret |= HandleAisAllOneDetection(self, (ThaModulePdh)pdhModule, frameType);

    /* Update AIS interrupt mask because HW is using different AIS interrupt bit
     * in case of DS3 and E3 */
    if (shouldUpdateInterruptMask && (interruptMask & cAtPdhDe3AlarmAis))
        ret |= AtChannelInterruptMaskSet((AtChannel)self, interruptMask & cAtPdhDe3AlarmAis, cAtPdhDe3AlarmAis);

    ret |= FramedSatopApply(self, (ThaModulePdh)pdhModule, frameType);

    /* Reset FEAC */
    HwTxFeacSet((AtPdhDe3)self, cAtPdhDe3FeacTypeUnknown, 0, 0);

    return ret;
    }

static uint16 FrameTypeGet(AtPdhChannel self)
    {
    /* Must use data-base because there are some SW frame-types have the same HW value */
    return mThis(self)->frameType;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtChannelTxEnable(self, enable);
    ret |= AtChannelRxEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtChannel self)
    {
    return (AtChannelTxIsEnabled(self) || AtChannelRxIsEnabled(self));
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    if (enabled)
        return cAtTrue;

    return ThaModulePdhCanDisableDe3((ThaModulePdh)AtChannelModuleGet(self));
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    if (enabled)
        return cAtTrue;
    return ThaModulePdhCanDisableDe3((ThaModulePdh)AtChannelModuleGet(self));
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    mRegFieldSet(regVal, cThaTxDS3E3ChEnb, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 regAddr, regVal;

    /* Most of product cannot disable */
    if (!AtChannelTxCanEnable(self, cAtFalse))
        return cAtTrue;

    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    return mRegField(regVal, cThaTxDS3E3ChEnb) ? cAtTrue : cAtFalse;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 regAddr = cThaRegRxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    mRegFieldSet(regVal, cThaRxDS3E3ChEnb, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 regAddr, regVal;

    /* Most of products cannot disable */
    if (!AtChannelRxCanEnable(self, cAtFalse))
        return cAtTrue;

    regAddr = cThaRegRxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    return mRegField(regVal, cThaRxDS3E3ChEnb) ? cAtTrue : cAtFalse;
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    return ThaCdrControllerTimingModeIsSupported(ThaPdhDe3CdrControllerGet((ThaPdhDe3)self), timingMode);
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    return ThaCdrControllerTimingSet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)self), timingMode, timingSource);
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
    return ThaCdrControllerTimingModeGet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)self));
    }

static AtChannel TimingSourceGet(AtChannel self)
    {
    return ThaCdrControllerTimingSourceGet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)self));
    }

static uint32 PwTimingRestore(AtChannel self, AtPw pw)
    {
    ThaCdrControllerPwTimingSourceSet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)self), pw);
    return 0;
    }

static eAtClockState ClockStateGet(AtChannel self)
    {
    return ThaCdrControllerClockStateGet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)self));
    }

static uint8 HwClockStateGet(AtChannel self)
    {
    return ThaCdrControllerHwClockStateGet(ThaPdhDe3CdrControllerGet((ThaPdhDe3)self));
    }

static uint32 HwDefectGet(AtChannel self)
    {
    uint32 regAddr;
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 offset = mMethodsGet(de3)->DefaultOffset(de3);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    /* Read alarm from hardware */
    regAddr = ThaModulePdhDS3E3RxFrmrperChnCurStatCtrlRegister(pdhModule) + offset;
    return mRead(de3, regAddr);
    }

static eBool ShouldReportBerViaBerController(AtChannel self)
    {
    return ThaModulePdhShouldReportBerViaBerController(PdhModule(self));
    }

static AtBerController BerController(AtChannel self)
    {
    AtPdhChannel de3 = (AtPdhChannel)self;
    if (de3->lineBerController)     return de3->lineBerController;
    if (de3->pathBerController)     return de3->pathBerController;
    return NULL;
    }

static uint32 BerDefectGet(AtChannel self, uint32 regVal, uint32 (*DefectGet)(AtBerController))
    {
    uint32 swAlarms;

    if (ShouldReportBerViaBerController(self))
        return DefectGet(BerController(self));

    swAlarms = 0;
    if (regVal & cThaDE3BerTcaCurStatMask)
        swAlarms |= cAtPdhDe3AlarmBerTca;
    if (regVal & cThaDE3BerSfCurStatMask)
        swAlarms |= cAtPdhDe3AlarmSfBer;
    if (regVal & cThaDE3BerSdCurStatMask)
        swAlarms |= cAtPdhDe3AlarmSdBer;

    return swAlarms;
    }

static uint32 DefectGetAndFilter(AtChannel self, uint32 regVal)
    {
    uint32 swAlarms = 0;
    eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    /* For E3, AIS (all-ones) defect takes a higher severity level than LOF defect. */
    if (AtPdhDe3IsE3((AtPdhDe3)self))
        {
        if (regVal & cThaDE3AisCurStatMask)
            swAlarms |= cAtPdhDe3AlarmAis;
        else if (regVal & cThaDE3LofCurStatMask)
            swAlarms |= cAtPdhDe3AlarmLof;
        }
    /* For DS3, AIS (patterns) defect takes a lower severity level than LOF defect. */
    else
        {
        if (regVal & cThaDE3LofCurStatMask)
            swAlarms |= cAtPdhDe3AlarmLof;
        if (regVal & cThaDE3AisBitCurStatMask) /*GR-499 framed DS3 LOF then framed AIS is generated */
            swAlarms |= cAtPdhDe3AlarmAis;
        }

    swAlarms |= BerDefectGet(self, regVal, AtBerControllerDefectGet);

    if (AtPdhDe3FrameTypeIsUnframed(frameType))
        return swAlarms;

    /* RDI/RAI alarms */
    if (regVal & cThaDE3RdiCurStatMask)
        swAlarms |= cAtPdhDe3AlarmRai;

    /* DS3 alarms */
    if (AtPdhDe3IsDs3((AtPdhDe3)self))
        {
        if (regVal & cThaDE3IdlePtCurStatMask)
            swAlarms |= cAtPdhDs3AlarmIdle;
        }
    /* E3 alarms */
    else
        {
        if (frameType == cAtPdhE3Frmg832)
            {
            if (regVal & cThaDE3CbitPar_TraceCurStatMask)
                swAlarms |= cAtPdhE3AlarmTim;
            }
        }

    return swAlarms;
    }

static uint32 De3RxFramerHwStatusRegister(AtPdhChannel self)
    {
    AtUnused(self);
    return cThaRegRxM23E23HwStatus;
    }

static eBool HasParentAlarmForwarding(AtPdhChannel self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 regAddr = 0;
    uint32 regVal = 0;
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    if (!AtDeviceAllCoresAreActivated(device))
        return cAtFalse;

    regAddr = De3RxFramerHwStatusRegister(self) + mMethodsGet(de3)->DefaultOffset(de3);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    return (regVal & cThaRegRxDS3E3CrrAISDownStrMask) ? cAtTrue : cAtFalse;
    }

static uint32 LineDefectGet(AtChannel self, uint32 regVal)
    {
    AtUnused(self);
    if (regVal & cThaDE3LosCurStatMask)
        return cAtPdhDe3AlarmLos;
    return 0;
    }

static uint32 PathDefectGet(AtChannel self, uint32 regVal)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);

    if (!ThaModulePdhAlarmForwardingStatusIsSupported(module))
        return DefectGetAndFilter(self, regVal);

    if (AtPdhChannelHasParentAlarmForwarding((AtPdhChannel)self))
        return cAtPdhDe3AlarmAis;

    return DefectGetAndFilter(self, regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regVal = HwDefectGet(self);
    uint32 swAlarms = 0;

    swAlarms |= LineDefectGet(self, regVal);
    swAlarms |= PathDefectGet(self, regVal);

    return swAlarms;
    }

static eBool PreventToGetClockStateInterruptOfSubChannel(AtChannel self)
    {
    if (AtPdhDe3IsChannelized((AtPdhDe3)self) && (AtChannelBoundPwGet(self) == NULL))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 AisInterruptMask(ThaPdhDe3 self)
    {
    return AtPdhDe3IsE3((AtPdhDe3)self) ? cThaDE3AisIntrEnMask : cThaDE3AisBitIntrEnMask;
    }

static uint32 InterruptRead2Clear(ThaPdhDe3 self, eBool clear)
    {
    uint32 regAddr, regVal;
    uint32 swStatus = 0;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    ThaCdrController cdrController = ThaPdhDe3CdrControllerGet(self);

    regAddr = ThaModulePdhDS3E3RxFrmrperChnIntrStatCtrlRegister(pdhModule) + mMethodsGet(self)->DefaultOffset(self);
    regVal = mRead(self, regAddr);

    /* Common alarms */
    if (regVal & cThaDE3LofIntrStatMask)
        swStatus |= cAtPdhDe3AlarmLof;
    if (regVal & AisInterruptMask(self))
        swStatus |= cAtPdhDe3AlarmAis;
    if (regVal & cThaDE3LosIntrStatMask)
        swStatus |= cAtPdhDe3AlarmLos;
    if (regVal & cThaDE3RdiDetIntrStatMask)
        swStatus |= cAtPdhDe3AlarmRai;

    swStatus |= BerDefectGet((AtChannel)self, regVal, (clear) ? AtBerControllerDefectHistoryClear : AtBerControllerDefectHistoryGet);

    if (regVal & cThaDe3NewSsmIntrStatMask)
        swStatus |= cAtPdhDe3AlarmSsmChange;

    /* DS3 alarms */
    if (AtPdhDe3IsDs3((AtPdhDe3)self))
        {
        if (regVal & cThaDE3IdlePtNewIntrStatMask)
            swStatus |= cAtPdhDs3AlarmIdle;
        if (regVal & cThaDE3NewAicTmIntrStatMask)
            swStatus |= cAtPdhDs3AlarmAicChange;
        /* FEAC is only applicable for DS3 CBit */
        if ((regVal & cThaDE3NewFeacSsmIntrStatMask) && AtPdhDe3IsDs3CbitFrameType(frameType))
            swStatus |= cAtPdhDs3AlarmFeacChange;
        }
    /* E3 alarms */
    else
        {
        if (frameType == cAtPdhE3Frmg832)
            {
            if (regVal & cThaDE3NewAicTmIntrStatMask)
                swStatus |= cAtPdhE3AlarmTmChange;
            if (regVal & cThaDE3IdlePtNewIntrStatMask)
                swStatus |= cAtPdhE3AlarmPldTypeChange;
            }
        }

    /* Clear */
    if (clear)
        mWrite(self, regAddr, regVal);

    if (cdrController && !PreventToGetClockStateInterruptOfSubChannel((AtChannel)self))
        swStatus |= ThaCdrControllerInterruptRead2Clear(cdrController, clear);

    return swStatus;
    }

static uint32 SpecificDefectInterruptClear(AtChannel self, uint32 defectTypes)
    {
    /* Only support clock state change interrupt for now */
    if (defectTypes & cAtPdhDe3AlarmClockStateChange)
        {
        ThaCdrController cdrController = ThaPdhDe3CdrControllerGet((ThaPdhDe3)self);
        return ThaCdrControllerInterruptRead2Clear(cdrController, cAtTrue);
        }

    return 0;
    }

static AtPdhMdlController MdlControllerGet(ThaPdhDe3 self)
    {
    /* Actually we have only single hardware MDL controller */
    if (self->pathMessage)   return self->pathMessage;
    if (self->testSignal)    return self->testSignal;
    if (self->idleSignal)    return self->idleSignal;
    return NULL;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 defectMask = mMethodsGet(mThis(self))-> InterruptRead2Clear(mThis(self), cAtFalse);
    AtPdhMdlController controller = ThaPdhDe3MdlControllerGet((ThaPdhDe3)self);
    if (controller)
        defectMask |= AtPdhMdlControllerDefectHistoryGet(controller);

    return defectMask;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 defectMask = mMethodsGet(mThis(self))-> InterruptRead2Clear(mThis(self), cAtTrue);
    AtPdhMdlController controller = ThaPdhDe3MdlControllerGet((ThaPdhDe3)self);
    if (controller)
        defectMask |= AtPdhMdlControllerDefectHistoryClear(controller);

    return defectMask;
    }

static ThaCdrController CdrController(AtChannel self)
    {
    return ThaPdhDe3CdrControllerGet((ThaPdhDe3)self);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    ThaCdrController cdrController = CdrController(self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    uint32 supportedMask = 0;

    if (ThaCdrControllerInterruptMaskIsSupported(cdrController, cAtPdhDe3AlarmClockStateChange))
        supportedMask |= cAtPdhDe3AlarmClockStateChange;

    /* Common masks */
    supportedMask |= cAtPdhDe3AlarmLos;
    supportedMask |= cAtPdhDe3AlarmAis;
    supportedMask |= cAtPdhDe3AlarmLof;
    supportedMask |= cAtPdhDe3AlarmRai;

    supportedMask |= cAtPdhDe3AlarmSdBer;
    supportedMask |= cAtPdhDe3AlarmSfBer;
    supportedMask |= cAtPdhDe3AlarmBerTca;

    /* E3 */
    if (AtPdhDe3FrameTypeIsE3(frameType))
        {
        if (frameType == cAtPdhE3Frmg832)
            supportedMask |= cAtPdhDe3AlarmSsmChange;
        }
    /* DS3 */
    else
        {
        if (IsDs3CbitFormat((ThaPdhDe3)self))
            {
            supportedMask |= cAtPdhDs3AlarmFeacChange;
            supportedMask |= cAtPdhDe3AlarmSsmChange;
            }

        if (IsDs3FrameHasMbit((ThaPdhDe3)self))
            supportedMask |= cAtPdhDs3AlarmIdle;
        }

    return supportedMask;
    }

static eAtRet BerInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    static const uint32 cDe3BerDefects = cAtPdhDe3AlarmSdBer|cAtPdhDe3AlarmSfBer|cAtPdhDe3AlarmBerTca;
    uint32 regAddr, regVal;
    ThaPdhDe3 de3 = (ThaPdhDe3)self;

    if ((defectMask & cDe3BerDefects) == 0)
        return cAtOk;

    if (ShouldReportBerViaBerController(self))
        return AtBerControllerInterruptMaskSet(BerController(self), defectMask & cDe3BerDefects, enableMask & cDe3BerDefects);

    regAddr = ThaModulePdhDS3E3RxFrmrperChnIntrEnCtrlRegister(PdhModule(self)) + mMethodsGet(de3)->DefaultOffset(de3);
    regVal = mRead(self, regAddr);
    mSetIntrBitMask(cAtPdhDe3AlarmSdBer, defectMask, enableMask, cThaDE3BerSdIntrEnMask);
    mSetIntrBitMask(cAtPdhDe3AlarmSfBer, defectMask, enableMask, cThaDE3BerSfIntrEnMask);
    mSetIntrBitMask(cAtPdhDe3AlarmBerTca, defectMask, enableMask, cThaDE3BerTcaIntrEnMask);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;
    uint32 regAddr, regVal;
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    ThaCdrController cdrController = CdrController(self);
    const uint32 cThaDe3AisIntrMask = AisInterruptMask(de3);
    AtPdhMdlController mdlController = ThaPdhDe3MdlControllerGet((ThaPdhDe3)self);

    regAddr = ThaModulePdhDS3E3RxFrmrperChnIntrEnCtrlRegister(pdhModule) + mMethodsGet(de3)->DefaultOffset(de3);
    regVal = mRead(self, regAddr);

    /* Reset AIS bit mask */
    if (defectMask & cAtPdhDe3AlarmAis)
        regVal = regVal & (~(cThaDE3AisIntrEnMask | cThaDE3AisBitIntrEnMask));

    mSetIntrBitMask(cAtPdhDs3AlarmFeacChange, defectMask, enableMask, cThaDE3NewFeacIntrEnMask);
    mSetIntrBitMask(cAtPdhDs3AlarmIdle, defectMask, enableMask, cThaDE3IdlePtNewIntrEnMask);
    mSetIntrBitMask(cAtPdhDe3AlarmAis, defectMask, enableMask, cThaDe3AisIntrMask);
    mSetIntrBitMask(cAtPdhDe3AlarmLos, defectMask, enableMask, cThaDE3LosIntrEnMask);
    mSetIntrBitMask(cAtPdhDe3AlarmRai, defectMask, enableMask, cThaDE3RdiDetIntrEnMask);
    mSetIntrBitMask(cAtPdhDe3AlarmLof, defectMask, enableMask, cThaDE3LofIntrEnMask);
    mSetIntrBitMask(cAtPdhDe3AlarmSsmChange, defectMask, enableMask, cThaDe3NewSsmIntrEnMask);
    mWrite(self, regAddr, regVal);

    if (regVal)
        AtChannelInterruptEnable(self);
    else
        AtChannelInterruptDisable(self);

    ret |= BerInterruptMaskSet(self, defectMask, enableMask);

    if (cdrController && (defectMask & cAtPdhDe3AlarmClockStateChange))
        ret |= ThaCdrControllerInterruptMaskSet(cdrController, defectMask, enableMask);

    if (mdlController && (defectMask & cAtPdhDs3AlarmMdlChange))
        ret |= AtPdhMdlControllerInterruptMaskSet(mdlController, defectMask, enableMask);

    return ret;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint32 swMasks = 0;
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    ThaCdrController cdrController = mThis(self)->cdrController; /* When CDR controller don't exist
                                                                   it mean the interrupt mask was 0 */
    AtPdhMdlController mdlController = ThaPdhDe3MdlControllerGet((ThaPdhDe3)self);

    regAddr = ThaModulePdhDS3E3RxFrmrperChnIntrEnCtrlRegister(pdhModule) + mMethodsGet(de3)->DefaultOffset(de3);
    regVal = mRead(self, regAddr);
    if (regVal & (cThaDE3AisIntrEnMask | cThaDE3AisBitIntrEnMask))
        swMasks |= cAtPdhDe3AlarmAis;
    if (regVal & cThaDE3LosIntrEnMask)
        swMasks |= cAtPdhDe3AlarmLos;
    if (regVal & cThaDE3RdiDetIntrEnMask)
        swMasks |= cAtPdhDe3AlarmRai;
    if (regVal & cThaDE3LofIntrEnMask)
        swMasks |= cAtPdhDe3AlarmLof;

    swMasks |= BerDefectGet(self, regVal, AtBerControllerInterruptMaskGet);

    if (regVal & cThaDe3NewSsmIntrEnMask)
        swMasks |= cAtPdhDe3AlarmSsmChange;

    /* DS3 */
    if (AtPdhDe3IsDs3((AtPdhDe3)self))
        {
        if (regVal & cThaDE3IdlePtNewIntrEnMask)
            swMasks |= cAtPdhDs3AlarmIdle;
        if (regVal & cThaDE3NewAicTmIntrEnMask)
            swMasks |= cAtPdhDs3AlarmAicChange;
        if (regVal & cThaDE3NewFeacIntrEnMask)
            swMasks |= cAtPdhDs3AlarmFeacChange;
        }
    /* E3 */
    else
        {
        if (regVal & cThaDE3NewAicTmIntrEnMask)
            swMasks |= cAtPdhE3AlarmTmChange;
        if (regVal & cThaDE3IdlePtNewIntrEnMask)
            swMasks |= cAtPdhE3AlarmPldTypeChange;
        }

    if (cdrController)
        swMasks |= ThaCdrControllerInterruptMaskGet(cdrController);

    if (mdlController)
        swMasks |= AtPdhMdlControllerInterruptMaskGet(mdlController);

    return swMasks;
    }

static eBool ShouldConsiderLofWhenLos(ThaPdhDe3 self)
    {
    /* To avoid context switching on products that do not require this. And
     * products that require this will have their own channels override this
     * method. */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldReportLofWhenLos(AtChannel self, uint32 regMaskVal)
    {
    if (!ThaModulePdhDe3LiuShouldReportLofWhenLos((ThaModulePdh)AtChannelModuleGet(self)))
        return cAtFalse;

    if (AtPdhDe3FrameTypeIsUnframed(AtPdhChannelFrameTypeGet((AtPdhChannel)self)) || ((regMaskVal & cThaDE3LosIntrEnMask) == 0))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    const uint32 cIgnoredDefectsInUnframed = cThaDE3LofIntrEnMask | cThaDE3RdiDetIntrEnMask;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    uint32 regIntrAddr = ThaModulePdhDS3E3RxFrmrperChnIntrStatCtrlRegister(pdhModule) + offset;
    uint32 regIntrVal = mChannelHwRead(self, regIntrAddr, cAtModulePdh);
    uint32 regMaskVal = mChannelHwRead(self, ThaModulePdhDS3E3RxFrmrperChnIntrEnCtrlRegister(pdhModule) + offset, cAtModulePdh);
    uint32 regCurrVal;
    uint32 events = 0;
    uint32 states = 0;

    /* Filter non-masked interrupt bits. */
    regIntrVal &= regMaskVal;

    /* Get current defect after clearing interrupt to avoid missed event. */
    mChannelHwWrite(self, regIntrAddr, regIntrVal, cAtModulePdh);
    regCurrVal = mChannelHwRead(self, ThaModulePdhDS3E3RxFrmrperChnCurStatCtrlRegister(pdhModule) + offset, cAtModulePdh);

    /* Filter out not applicable alarms */
    if (AtPdhDe3FrameTypeIsUnframed(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        regCurrVal &= ~cIgnoredDefectsInUnframed;

    /* Products that have LIU and it does not use LOS from the codechip. See
     * commit detail to understand why this should be done.
     * Note: the purpose of ShouldConsiderLofWhenLos method is to avoid much
     * switching context for products that do not require this */
    if (mMethodsGet(mThis(self))->ShouldConsiderLofWhenLos(mThis(self)) && ShouldReportLofWhenLos(self, regIntrVal))
        {
        if (regCurrVal & cThaDE3LosIntrEnMask)
            {
            regCurrVal |= cThaDE3LofIntrEnMask;
            regIntrVal |= cThaDE3LofIntrEnMask;
            }
        else if ((regCurrVal & cThaDE3LofIntrEnMask) == 0)
            regIntrVal |= cThaDE3LofIntrEnMask;
        }

    /* LOS */
    if (regIntrVal & cThaDE3LosIntrEnMask)
        {
        events |= cAtPdhDe3AlarmLos;
        if (regCurrVal & cThaDE3LosIntrEnMask)
            states |= cAtPdhDe3AlarmLos;
        }

    /* LOF */
    if (regIntrVal & cThaDE3LofIntrEnMask)
        {
        events |= cAtPdhDe3AlarmLof;
        if (regCurrVal & cThaDE3LofIntrEnMask)
            states |= cAtPdhDe3AlarmLof;
        }

    /* RAI */
    if (regIntrVal & cThaDE3RdiDetIntrEnMask)
        {
        events |= cAtPdhDe3AlarmRai;
        if (regCurrVal & cThaDE3RdiDetIntrEnMask)
            states |= cAtPdhDe3AlarmRai;
        }

    /* AIS Signaling */
    if (regIntrVal & cThaDE3AisBitIntrEnMask)
        {
        events |= cAtPdhDe3AlarmAis;
        if (regCurrVal & cThaDE3AisBitIntrEnMask)
            states |= cAtPdhDe3AlarmAis;
        }
    /* AIS all '1's */
    if (regIntrVal & cThaDE3AisIntrEnMask)
        {
        events |= cAtPdhDe3AlarmAis;
        if (regCurrVal & cThaDE3AisIntrEnMask)
            states |= cAtPdhDe3AlarmAis;
        }

    /* Old version has this issue and need this quick fix */
    if (!ThaModulePdhDs3AisInterruptIsFixed(pdhModule))
        {
        if (events & cAtPdhDe3AlarmAis)
            {
            if (regCurrVal & (cThaDE3AisBitIntrEnMask | cThaDE3AisIntrEnMask))
                states |= cAtPdhDe3AlarmAis;
            }
        }

    /* SSM */
    if (regIntrVal & cThaDe3NewSsmIntrEnMask)
        events |= cAtPdhDe3AlarmSsmChange;

    /* DS3 C-bit IDLE signaling. */
    if (AtPdhDe3IsDs3((AtPdhDe3)self) == cAtTrue)
        {
        if (regIntrVal & cThaDE3IdlePtNewIntrEnMask)
            {
            events |= cAtPdhDs3AlarmIdle;
            if (regCurrVal & cThaDE3IdlePtNewIntrEnMask)
                states |= cAtPdhDs3AlarmIdle;
            }

        if (regIntrVal & cThaDE3NewFeacIntrEnMask)
            events |= cAtPdhDs3AlarmFeacChange;
        }

    /* BER-TCA */
    if (regIntrVal & cThaDE3BerTcaIntrEnMask)
        {
        events |= cAtPdhDe3AlarmBerTca;
        if (regCurrVal & cThaDE3BerTcaIntrEnMask)
            states |= cAtPdhDe3AlarmBerTca;
        }

    /* BER-SD */
    if (regIntrVal & cThaDE3BerSdIntrEnMask)
        {
        events |= cAtPdhDe3AlarmSdBer;
        if (regCurrVal & cThaDE3BerSdIntrEnMask)
            states |= cAtPdhDe3AlarmSdBer;
        }

    /* BER-SF */
    if (regIntrVal & cThaDE3BerSfIntrEnMask)
        {
        events |= cAtPdhDe3AlarmSfBer;
        if (regCurrVal & cThaDE3BerSfIntrEnMask)
            states |= cAtPdhDe3AlarmSfBer;
        }

    AtChannelAllAlarmListenersCall(self, events, states);

    return states;
    }

static uint32 InCorrectionTxAisForceShift(AtChannel self)
    {
    eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if (AtPdhDe3FrameTypeIsE3(frameType))
        return cThaTxDS3E3LineAllOneShift;

    if ((frameType == cAtPdhDs3Unfrm) || (frameType == cAtPdhDe3FrameUnknown))
        return 0;

    return cThaTxDS3FrameAisShift;
    }

static uint32 CorrectionTxAisForceShift(AtChannel self)
    {
    AtUnused(self);
    return cThaTxDS3E3LineAisSelectShift;
    }

static uint32 TxAisForceShift(AtChannel self)
    {
    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        return CorrectionTxAisForceShift(self);

    return InCorrectionTxAisForceShift(self);
    }

static eBool TxDs3IsForcedAis(AtChannel self)
    {
    uint32 mask0 = 0, mask1 = 0, shift0 = 0, shift1 = 0;
    uint32 regAddr, regVal;
    uint8 forced1, forced2;

    mask1 = cThaTxDS3E3LineAisSelectMask;
    shift1 = cThaTxDS3E3LineAisSelectShift;
    mask0 = cThaTxDS3FrameAisMask;
    shift0 = cThaTxDS3FrameAisShift;
    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regVal  = mRead(self, regAddr);
    mFieldGet(regVal, mask1, shift1, uint8, &forced1);
    mFieldGet(regVal, mask0, shift0, uint8, &forced2);

    return (forced1 | forced2) ? cAtTrue : cAtFalse;
    }

static eAtRet TxAisForce(AtChannel self, eBool force)
    {
    uint32 regAddr, regVal;
    uint32 mask  = TxAisForceMask(self);
    uint32 shift = TxAisForceShift(self);

    if (mask == 0)
        return cAtErrorModeNotSupport;

    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regVal  = mRead(self, regAddr);
    mFieldIns(&regVal, mask, shift, force ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool TxAisForced(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint32 mask  = TxAisForceMask(self);
    eBool isDs3TxAis = cAtFalse;

    if (mask == 0)
        return cAtFalse;

    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    regVal  = mRead(self, regAddr);

    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        {
        eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
        if (!AtPdhDe3FrameTypeIsE3(frameType))
            isDs3TxAis = TxDs3IsForcedAis(self);
        }

    return ((regVal & mask) || isDs3TxAis) ? cAtTrue : cAtFalse;
    }

static eAtRet TxRaiForce(AtChannel self, eBool force)
    {
    uint32 regAddr, regVal;
    ThaPdhDe3 de3 = (ThaPdhDe3)self;

    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaTxDS3E3FrcYelMask, cThaTxDS3E3FrcYelShift, force ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool TxRaiForced(AtChannel self)
    {
    uint32 regAddr, regVal;
    ThaPdhDe3 de3 = (ThaPdhDe3)self;

    regAddr = cThaRegTxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    regVal = mRead(self, regAddr);
    return (regVal & cThaTxDS3E3FrcYelMask) ? cAtTrue : cAtFalse;
    }

static eBool CanForceTxAlarms(AtChannel self, uint32 alarms)
    {
    return (alarms & AtChannelTxForcableAlarmsGet(self)) ? cAtTrue : cAtFalse;
    }

static eBool CanForceRxAlarms(AtChannel self, uint32 alarms)
    {
    return (alarms & AtChannelRxForcableAlarmsGet(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    eAtRet ret = cAtOk;

    if (!CanForceTxAlarms(self, alarmType))
        return cAtErrorModeNotSupport;

    if (alarmType & cAtPdhDe3AlarmAis)
        ret |= TxAisForce(self, force);

    if (alarmType & cAtPdhDe3AlarmRai)
        ret |= TxRaiForce(self, force);

    return ret;
    }

static eAtRet CacheTxAisAlarmForce(AtChannel self, uint32 alarmType, eBool forced)
    {
    if (alarmType & cAtPdhDe3AlarmAis)
        mThis(self)->txAisExplicitlyForced = forced;
    return cAtOk;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = cAtOk;

    ret |= HelperTxAlarmForce(self, alarmType, cAtTrue);
    ret |= CacheTxAisAlarmForce(self, alarmType, cAtTrue);

    return ret;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = cAtOk;

    ret |= HelperTxAlarmForce(self, alarmType, cAtFalse);
    ret |= CacheTxAisAlarmForce(self, alarmType, cAtFalse);

    return ret;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 alarms = 0;

    if (TxAisForced(self)) alarms |= cAtPdhDe3AlarmAis;
    if (TxRaiForced(self)) alarms |= cAtPdhDe3AlarmRai;

    return alarms;
    }

static void RxLofForce(AtChannel self, eBool force)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);

    regAddr = cThaRegRxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRxDS3E3FrcLofMask, cThaRxDS3E3FrcLofShift, force ? 1 : 0);
    mWrite(self, regAddr, regVal);
    }

static eBool RxLofForced(AtChannel self)
    {
    uint32 regAddr;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);

    regAddr = cThaRegRxM23E23Ctrl + offset;
    return (mRead(self, regAddr) & cThaRxDS3E3FrcLofMask) ? cAtTrue : cAtFalse;
    }

static uint32 InCorrectionAisDownStreamToRegVal(AtChannel self, uint32 regVal, eBool enable)
    {
    eAtPdhLoopbackMode loopMode = AtChannelLoopbackGet(self);
    uint8 pldAis = 0, lineAis = 0;

    switch (loopMode)
        {
        case cAtPdhLoopbackModeRemotePayload:
            if (enable)
                {
                pldAis  = 1;
                lineAis = 0;
                }
            break;
        case cAtPdhLoopbackModeRemoteLine:
        case cAtPdhLoopbackModeRelease:
        case cAtPdhLoopbackModeLocalPayload:
        case cAtPdhLoopbackModeLocalLine:
        default:
            if (enable)
                {
                pldAis  = 0;
                lineAis = 1;
                }
            break;
        }

    mFieldIns(&regVal, cThaRxDS3E3PayAllOneMask, cThaRxDS3E3PayAllOneShift, pldAis);
    mFieldIns(&regVal, cThaRxDS3E3LineAllOneMask, cThaRxDS3E3LineAllOneShift, lineAis);
    return regVal;
    }

static eBool InCorrectionRegValToAisDownStream(uint32 regVal)
    {
    if (regVal & (cThaRxDS3E3PayAllOneMask | cThaRxDS3E3LineAllOneMask))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 CorrectionAisDownStreamToRegVal(AtChannel self, uint32 regVal, eBool enable)
    {
    AtUnused(self);

    mFieldIns(&regVal, cThaRxDS3E3PayAllOneMask, cThaRxDS3E3PayAllOneShift, 0);
    mFieldIns(&regVal, cThaRxDE3ForceAISLbitMask, cThaRxDE3ForceAISLbitShift, enable ? 1:0);
    mFieldIns(&regVal, cThaRxDS3E3LineAllOneMask, cThaRxDS3E3LineAllOneShift, 0);

    return regVal;
    }

static eBool CorrectionRegValToAisDownStream(uint32 regVal)
    {
    if (regVal & (cThaRxDE3ForceAISLbitMask))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AisDownStreamToRegVal(AtChannel self, uint32 regVal, eBool enable)
    {
    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        return CorrectionAisDownStreamToRegVal(self, regVal, enable);
    return InCorrectionAisDownStreamToRegVal(self, regVal, enable);
    }

static eBool RegValToAisDownStream(AtChannel self, uint32 regVal)
    {
    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
            return CorrectionRegValToAisDownStream(regVal);
    return InCorrectionRegValToAisDownStream(regVal);
    }

static void AisDownStreamForce(AtChannel self, eBool force)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);

    regAddr = cThaRegRxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    regVal = AisDownStreamToRegVal(self, regVal, force);
    mWrite(self, regAddr, regVal);
    }

static eBool AisDownStreamForced(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);

    regAddr = cThaRegRxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    return RegValToAisDownStream(self, regVal) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperRxAlarmForce(AtChannel self, uint32 alarmType, eBool force)
    {
    if (!CanForceRxAlarms(self, alarmType))
        return cAtErrorModeNotSupport;

    /* LOF */
    if (alarmType & cAtPdhDe3AlarmLof)
        RxLofForce(self, force);

    /* AIS */
    if (alarmType & cAtPdhDe3AlarmAis)
        AisDownStreamForce(self, force);

    return cAtOk;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 alarms = 0;

    if (RxLofForced(self))
        alarms |= cAtPdhDe3AlarmLof;

    if (AisDownStreamForced(self))
        alarms |= cAtPdhDe3AlarmAis;

    return alarms;
    }

static eAtRet HwLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);

    /* Loop in */
    regAddr = cThaRegRxM23E23Ctrl + offset;
    regVal  = mRead(self, regAddr);
    mFieldIns(&regVal,
              cThaRxDS3E3LineLoopMask,
              cThaRxDS3E3LineLoopShift,
              (loopbackMode == cAtPdhLoopbackModeLocalLine) ? 1 : 0);
    mWrite(self, regAddr, regVal);

    /* Loop out */
    regAddr = cThaRegTxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal,
              cThaTxDE3RmtLineLoopMask,
              cThaTxDE3RmtLineLoopShift,
              (loopbackMode == cAtPdhLoopbackModeRemoteLine) ? 1 : 0);
    mFieldIns(&regVal,
              cThaTxDE3RmtPayLoopMask,
              cThaTxDE3RmtPayLoopShift,
              (loopbackMode == cAtPdhLoopbackModeRemotePayload) ? 1 : 0);
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool ShouldNotifyAisClearance(AtChannel self)
    {
    /* AIS clear should be notified when framer AIS clear and no sticky change */
    if (((DefectGetAndFilter(self, HwDefectGet(self)) & cAtPdhDe3AlarmAis) == 0) &&
        ((mMethodsGet(mThis(self))-> InterruptRead2Clear(mThis(self), cAtFalse) & cAtPdhDe3AlarmAis) == 0))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LocalLineLoopbackSet(AtChannel self)
    {
    eAtRet ret = HwLoopbackSet(self, cAtPdhLoopbackModeLocalLine);

    if (ShouldNotifyAisClearance(self))
        AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly((AtPdhChannel)self, cAtFalse);
    return ret;
    }

static eBool ShouldNotifyAisRaise(AtChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);

    /* If the DS3/E3 channel has currently AIS sticky, let interrupt
     * service notify AIS clearance. */
    if (DefectHistoryGet(self) & cAtPdhDe3AlarmAis)
        return cAtFalse;

    if (!ThaModulePdhAlarmForwardingStatusIsSupported(module))
        return cAtFalse;

    /* AIS declaration should be notified when AIS downstream is restored from loop-back. */
    if (AtPdhChannelHasParentAlarmForwarding((AtPdhChannel)self))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ReleaseLocalLineLoopback(AtChannel self)
    {
    eAtRet ret;
    AtModule modulePdh = AtChannelModuleGet(self);

    AtModuleLock(modulePdh);

    ret = HwLoopbackSet(self, cAtPdhLoopbackModeRelease);
    if (ShouldNotifyAisRaise(self))
        AtChannelAllAlarmListenersCall(self, cAtPdhDe3AlarmAis, cAtPdhDe3AlarmAis);

    AtModuleUnLock(modulePdh);

    return ret;
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    uint8 de2Id, de1Id;
    /* Although high-layer stopped its alarm forwarding, but HW sometimes relays
     * the alarm forwarding status of DS3/E3 channels for a moment so that DS3/E3
     * framer can be re-aligned. Therefore, we cannot check this status bit
     * for alarm forwarding clearance reporting. */
    uint32 defect = DefectGetAndFilter((AtChannel)self, HwDefectGet((AtChannel)self));
    AtUnused(subChannelsOnly);

    if (defect & AtPdhChannelDefectsCauseAlarmForwarding(self))
        return;

    if (subChannelsOnly == cAtFalse)
        AtChannelAllAlarmListenersCall((AtChannel)self, cAtPdhDe3AlarmAis, 0);

    if (!AtPdhDe3IsChannelized((AtPdhDe3)self))
        return;

    for (de2Id = 0; de2Id < 7; de2Id++)
        {
        for (de1Id = 0; de1Id < 4; de1Id++)
            {
            AtPdhChannel de1 = (AtPdhChannel)AtPdhDe3De1Get((AtPdhDe3)self, de2Id, de1Id);
            AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(de1, cAtFalse);
            }
        }
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret;

    if (loopbackMode == cAtPdhLoopbackModeLocalLine)
        ret = LocalLineLoopbackSet(self);

    if ((loopbackMode == cAtPdhLoopbackModeRelease) &&
        (AtChannelLoopbackGet(self) == cAtPdhLoopbackModeLocalLine))
        ret = ReleaseLocalLineLoopback(self);

    else
        ret = HwLoopbackSet(self, loopbackMode);

    /* Update AIS downstream location which depends on remote loopback or line loopback */
    if (AtChannelRxForcedAlarmGet(self) & cAtPdhDe3AlarmAis)
        (void)AtChannelRxAlarmForce(self, cAtPdhDe3AlarmAis);

    return ret;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);

    /* Loop in */
    regAddr = cThaRegRxM23E23Ctrl + offset;
    regVal  = mRead(self, regAddr);
    if (regVal & cThaRxDS3E3LineLoopMask) return cAtPdhLoopbackModeLocalLine;

    /* Loop out */
    regAddr = cThaRegTxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    if (regVal & cThaTxDE3RmtLineLoopMask) return cAtPdhLoopbackModeRemoteLine;
    if (regVal & cThaTxDE3RmtPayLoopMask)  return cAtPdhLoopbackModeRemotePayload;

    return cAtOk;
    }

static uint32 BpvCounterRead2Clear(ThaPdhDe3 self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return 0;
    }

static eBool RetimingIsSupported(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaPdhRetimingController RetimingControllerCreate(ThaPdhDe3 self)
    {
    AtUnused(self);
    return NULL;
    }

static ThaPdhRetimingController RetimingControllerGet(ThaPdhDe3 self)
    {
    if (self->retimingController)
        return self->retimingController;

    if (ThaPdhDe3RetimingIsSupported(self))
        self->retimingController = mMethodsGet(self)->RetimingControllerCreate(self);

    return self->retimingController;
    }

static uint32 TxCsCounter(ThaPdhDe3 self, eBool r2c)
    {
    return ThaPdhRetimingControllerTxCsCounter(RetimingControllerGet(self), r2c);
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool clear)
    {
    ThaModulePdh module = (ThaModulePdh) AtChannelModuleGet(self);
    uint32 counterOffSet =  mMethodsGet(module)->De3CounterOffset(module, clear);
    uint32 offset = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);
    if (counterType == cAtPdhDe3CounterRei)
        return mRead(self,  ThaModulePdhRxM23E23FrmrReiCntRegister(module) + counterOffSet + offset) & cThaRxM23E23ReiCntMask;

    if (counterType == cAtPdhDe3CounterFBit)
        return mRead(self, ThaModulePdhRxM23E23FrmrFBECntRegister(module)  + counterOffSet + offset) & cThaRxM23E23FbeCntMask;

    if (counterType == cAtPdhDe3CounterPBit)
        return mRead(self, ThaModulePdhRxM23E23FrmrParCntRegister(module) + counterOffSet + offset)  & cThaRxM23E23ParCntMask;

    if (counterType == cAtPdhDe3CounterCPBit)
        return mRead(self, ThaModulePdhRxM23E23FrmrCbitParCntRegister(module) + counterOffSet + offset) & cThaRxM23E23CParCntMask;

    if (counterType == cAtPdhDe3CounterBpvExz)
        return mMethodsGet(mThis(self))->BpvCounterRead2Clear(mThis(self), clear);

    if (counterType == cAtPdhDe3CounterTxCs)
        {
        if (AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
            return TxCsCounter((ThaPdhDe3)self, clear);
        }

    return 0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static uint8 NumBlocksNeedToBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    AtUnused(encapChannel);
    AtUnused(self);
    return 0;
    }

static AtEncapBinder EncapBinder(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceEncapBinder(device);
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = AtEncapBinderBindDe3ToEncapChannel(EncapBinder(self), self, encapChannel);

    if (AtPdhDe3IsDs3((AtPdhDe3)self))
        ret |= ThaOcnVc3PldSet((AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self), cThaOcnVc3PldDs3);

    /* Let super deal with database */
    ret |= m_AtChannelMethods->BindToEncapChannel(self, encapChannel);

    return ret;
    }

static eBool NationalValueIsValid(uint8 pattern)
    {
    return (pattern > 1) ? cAtFalse : cAtTrue;
    }

static eAtRet NationalBitsSet(AtPdhChannel self, uint8 pattern)
    {
    uint32 regAddr;
    ThaPdhDe3 thaDe3 = (ThaPdhDe3)self;
    uint32 offset = mMethodsGet(thaDe3)->DefaultOffset(thaDe3);
    uint32 longRegVal[cThaNumDwordsInLongReg];

    if (!CanChangeTxSignal((AtPdhDe3)self))
        return cAtErrorNotApplicable;

    /* Check Support base on Frame Type  */
    if (!AtPdhChannelNationalBitsSupported(self))
        return cAtErrorModeNotSupport;

    /* Pattern only one bit value */
    if (!NationalValueIsValid(pattern))
        return cAtErrorOutOfRangParm;

    regAddr = cThaRegTxM23E23Ctrl + offset;
    mChannelHwLongRead(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);
    mFieldIns(&longRegVal[cThaTxDS3E3OHValTailDwIndex],
              cThaTxDS3E3OHInsertMask,
              cThaTxDS3E3OHInsertShift,
              1);
    mFieldIns(&longRegVal[cThaTxDS3E3OHValTailDwIndex],
              cThaTxDS3E3OHValTailMask,
              cThaTxDS3E3OHValTailShift,
              (pattern & cBit0));
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);

    return cAtOk;
    }

static uint8 NationalBitsGet(AtPdhChannel self)
    {
    uint32 regAddr;
    ThaPdhDe3 thaDe3 = (ThaPdhDe3)self;
    uint32 offset = mMethodsGet(thaDe3)->DefaultOffset(thaDe3);
    uint32 longRegVal[cThaNumDwordsInLongReg];
    uint8 nBitValue;

    regAddr = cThaRegTxM23E23Ctrl + offset;
    mChannelHwLongRead(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);
    mFieldGet(longRegVal[cThaTxDS3E3OHValTailDwIndex],
              cThaTxDS3E3OHValTailMask,
              cThaTxDS3E3OHValTailShift,
              uint8,
              &nBitValue);

    return (nBitValue & cBit0);
    }

static uint8 RxNationalBitsGet(AtPdhChannel self)
    {
    uint32 regAddr, ohFieldValue;
    ThaPdhDe3 thaDe3 = (ThaPdhDe3)self;
    uint32 offset = mMethodsGet(thaDe3)->DefaultOffset(thaDe3);
    uint32 longRegVal[cThaNumDwordsInLongReg];

    regAddr = cThaRegRxM23E23OHCurStatus + offset;
    mChannelHwLongRead(self, regAddr, longRegVal, cThaNumDwordsInLongReg, cAtModulePdh);
    mFieldGet(longRegVal[cThaDE3OHCurrStatusTailDwIndex],
              cThaDE3OHCurrStatusTailMask,
              cThaDE3OHCurrStatusTailShift,
              uint32,
              &ohFieldValue);

    return (uint8)((ohFieldValue & cBit8) >> 8);
    }

static ThaCdrController CdrControllerCreate(ThaPdhDe3 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrVcDe3CdrControllerCreate(cdrModule, (AtPdhDe3)self);
    }

static eAtRet EncapConnectionEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);

    ret |= ThaModuleAbstractMapDe3EncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhDe3)self, enable);
    ret |= ThaModuleAbstractMapDe3EncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe3)self, enable);

    return ret;
    }

static AtObjectAny AttController(AtChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        mThis(self)->attController =mMethodsGet(module)->De3AttControllerCreate(module, self);
        AtAttControllerSetUp(mThis(self)->attController);
        AtAttControllerInit(mThis(self)->attController);
        }
    return mThis(self)->attController;
    }

static void DeleteAttController(AtObject self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);
    mThis(self)->attController = NULL;
    }

static void DeleteMdlController(AtPdhMdlController *controller)
    {
    AtObjectDelete((AtObject)*controller);
    *controller = NULL;
    }

static void DeleteAllMdlControllers(AtChannel self)
    {
    DeleteMdlController(&mThis(self)->pathMessage);
    DeleteMdlController(&mThis(self)->testSignal);
    DeleteMdlController(&mThis(self)->idleSignal);
    }

static void Delete(AtObject self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    AtSdhVc vc;
    DeleteAllMdlControllers((AtChannel)self);
    AtModulePrbsEngineObjectDelete(PrbsModule((AtChannel)self), de3->prbsEngine);
    de3->prbsEngine = NULL;

    AtObjectDelete((AtObject)de3->cdrController);
    de3->cdrController = NULL;
    AtObjectDelete((AtObject)de3->linePrbsEngine);
    de3->linePrbsEngine = NULL;
    DeleteAttController(self);

    /*
     * There are cases when a DE3 is managed by module PDH but there is a VC refers to it (EC1 is an example of such case)
     * module PDH may delete DE3 object while VC is still referring to this object. Need to unbind VC before object is deleted.
     */
    vc = AtPdhChannelVcGet((AtPdhChannel)de3);
    if (vc)
        SdhVcPdhChannelSet(vc, NULL);

    AtObjectDelete((AtObject)de3->retimingController);
    de3->retimingController = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static uint8 PppBlockRange(AtChannel self)
    {
    AtUnused(self);
    return 3;
    }

static AtSurEngine *SurEngineAddress(AtChannel self)
    {
    return &(mThis(self)->surEngine);
    }

static ThaModuleAbstractMap MapModule(AtPdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static ThaModuleAbstractMap DemapModule(AtPdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    }

static eAtRet TxFramerBypass(ThaPdhDe3 self, eBool bypass)
    {
    uint32 regAddr, regVal;
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);
    uint32 rxFrameType, newFrameType;
    
    regAddr = cThaRegRxM23E23Ctrl + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    rxFrameType = mRegField(regVal, cThaRxDS3E3Md);
    newFrameType = bypass ? TxUnframedFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)self)) : rxFrameType;

    regAddr = cThaRegTxM23E23Ctrl + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    if (newFrameType == mRegField(regVal, cThaTxDS3E3Md)) /* To save HW access */
        return cAtOk;

    mFieldIns(&regVal, cThaTxDS3E3MdMask, cThaTxDS3E3MdShift, newFrameType);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet MapBypassForPwBinding(ThaPdhDe3 self, eBool bypass)
    {
    AtPdhChannel de3PdhChannel = (AtPdhChannel)self;
    eAtPdhDe3FrameType frameType;

    frameType = AtPdhChannelFrameTypeGet(de3PdhChannel);
    if (bypass)
        frameType = UnframedMode(de3PdhChannel);

    return ThaModuleAbstractMapDe3FrameModeSet(MapModule(de3PdhChannel), (AtPdhDe3)self, frameType);
    }

static eAtRet MapBypassForChannelizedBert(ThaPdhDe3 self, eBool bypass)
    {
    AtPdhChannel de3PdhChannel = (AtPdhChannel)self;
    eAtPdhDe3FrameType frameType;

    frameType = AtPdhChannelFrameTypeGet(de3PdhChannel);
    if (bypass)
        frameType = UnframedMode(de3PdhChannel);
    else
        {
        if ((frameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
            (frameType == cAtPdhDs3FrmCbitChnl21E1s))
            frameType = cAtPdhDs3FrmCbitUnChn;
        }

    return ThaModuleAbstractMapDe3FrameModeSet(MapModule(de3PdhChannel), (AtPdhDe3)self, frameType);
    }

static eAtRet RxFramerMonitorOnlyEnable(ThaPdhDe3 self, eBool enable)
    {
    uint32 offset = mMethodsGet(self)->DefaultOffset(self);
    uint32 address = cThaRegRxM23E23Ctrl + offset;
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE3MonOnlyMask, cThaRxDE3MonOnlyShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static eBool ShouldEnableMonOnlyWhenBindingToSatop(ThaPdhDe3 self, AtPw pseudowire)
    {
    if (pseudowire == NULL)
        return cAtFalse;

    if (AtChannelPrbsEngineGet((AtChannel)self))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet MappingDisable(ThaPdhDe3 self)
    {
    /* Delete all DS2/E2s to make it be consistent to the hot framing change
     * while DS3/E3 is bound to PW */
    AtPdhChannelAllSubChannelsDelete((AtPdhChannel)self);

    return cAtOk;
    }

static eAtRet MappingRestore(ThaPdhDe3 self)
    {
    AtPdhChannel de3 = (AtPdhChannel)self;
    /* Call super to restore the database */
    return m_AtPdhChannelMethods->FrameTypeSet(de3, AtPdhChannelFrameTypeGet(de3));
    }

static eAtRet De3FramingBindToPwSatop(ThaPdhDe3 self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    eBool isUnChannelized = AtPdhDe3IsChannelized((AtPdhDe3)self) ? cAtFalse : cAtTrue;
    AtPdhChannel de3PdhChannel = (AtPdhChannel)self;
    eBool shouldBypass = ThaPdhChannelFramingShouldBypassOnSatopBinding(de3PdhChannel, pseudowire);

    if (pseudowire)
        AtPdhChannelSubChannelsHardwareCleanup((AtPdhChannel)self);

    ret |= RxFramerMonitorOnlyEnable(self, ShouldEnableMonOnlyWhenBindingToSatop(self, pseudowire));
    ret |= TxFramerBypass((ThaPdhDe3)self, shouldBypass);
    ret |= ThaCdrDe3UnChannelizedModeSet(cdrModule, de3PdhChannel, (eBool)(pseudowire ? cAtTrue : isUnChannelized));

    ret |= MapBypassForPwBinding(self, shouldBypass);

    if (shouldBypass)
        ret |= AtPdhChannelAlarmAffectingDisable(de3PdhChannel);
    else
        ret |= AtPdhChannelAlarmAffectingRestore(de3PdhChannel);

    if (pseudowire)
        ret |= MappingDisable(self);
    else
        ret |= MappingRestore(self);

    return ret;
    }

static eAtRet De3FramingBypass(ThaPdhDe3 self, eBool isBypass)
    {
    eAtRet ret = cAtOk;

    ret |= RxFramerMonitorOnlyEnable(self, isBypass);
    ret |= TxFramerBypass((ThaPdhDe3)self, isBypass);

    return ret;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);
    ThaModulePdh module = PdhModule(self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    ret |= ThaModuleAbstractMapBindDe3ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap)  , (AtPdhDe3)self, pseudowire);
    ret |= ThaModuleAbstractMapBindDe3ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe3)self, pseudowire);
    if (ret != cAtOk)
        return ret;

    /* In case of unbinding, need to update timing configuration */
    if (pseudowire == NULL)
        {
        /* Disable MonOnly mode */
        ret |= RxFramerMonitorOnlyEnable((ThaPdhDe3)self, cAtFalse);

        if ((AtChannelTimingModeGet(self) == cAtTimingModeAcr) ||
            (AtChannelTimingModeGet(self) == cAtTimingModeDcr))
            ret |= AtChannelTimingSet(self, cAtTimingModeSys, NULL);
        }

    if (ThaModulePdhDe3FramedSatopIsSupported(module) && !AtPdhDe3FrameTypeIsUnframed(frameType))
        ret |= mMethodsGet(mThis(self))->De3FramingBindToPwSatop(mThis(self), pseudowire);

    return ret;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleAbstractMapDe3EncapConnectionEnable(mapModule, (AtPdhDe3)self, enable);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return ThaModuleAbstractMapDe3EncapConnectionEnable(mapModule, (AtPdhDe3)self, enable);
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleAbstractMapDe3EncapConnectionIsEnabled(mapModule, (AtPdhDe3)self);
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return ThaModuleAbstractMapDe3EncapConnectionIsEnabled(mapModule, (AtPdhDe3)self);
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (mThis(self)->prbsEngine == NULL)
        mThis(self)->prbsEngine = ThaModulePdhDe3PrbsEngineCreate(PdhModule(self), (AtPdhDe3)self);
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    }

static uint8 PartId(ThaPdhDe3 self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    if (vc)
        return ThaModuleSdhPartOfChannel(vc);
    return 0;
    }

static eAtRet WorkingSideSet(ThaPdhDe3 self, eThaPdhDe3WorkingSide workingSide)
    {
    uint32 offset  = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);
    uint32 regAddr = cThaRegDS1E1J1RxFrmrMuxCtrl2 + offset;
    uint32 regVal  = (workingSide == cThaPdhDe3WorkingSideSdh) ? 0 : 0xFFFFFFF;
    mWrite(self, regAddr, regVal);

    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPdhDe3 object = (ThaPdhDe3)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(cdrController);
    mEncodeObjectDescription(prbsEngine);
    mEncodeObjectDescription(linePrbsEngine);
    mEncodeObjectDescription(clockExtractor);
    mEncodeObject(pathMessage);
    mEncodeObject(testSignal);
    mEncodeObject(idleSignal);
    mEncodeUInt(frameType);
    mEncodeObject(surEngine);
    mEncodeNone(rxCurrentMdlController);
    mEncodeObject(retimingController);
    mEncodeUInt(enablePrbsDe3Level);
    mEncodeUInt(txAisExplicitlyForced);
    mEncodeNone(attController);
    }

static AtPrbsEngine LinePrbsEngineGet(AtPdhChannel self)
    {
    if (mThis(self)->linePrbsEngine == NULL)
        mThis(self)->linePrbsEngine = ThaModulePdhDe3LinePrbsEngineCreate(PdhModule((AtChannel)self), (AtPdhDe3)self);
    return mThis(self)->linePrbsEngine;
    }

static eBool MdlSupported(AtPdhDe3 self)
    {
    uint16 currentFrameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if (currentFrameType == cAtPdhDs3FrmCbitUnChn)
        return cAtTrue;

    if (currentFrameType == cAtPdhDs3FrmCbitChnl28Ds1s)
        return cAtTrue;

    if (currentFrameType == cAtPdhDs3FrmCbitChnl21E1s)
        return cAtTrue;

    return cAtFalse;
    }

static AtPdhMdlController PathMessageMdlController(AtPdhDe3 self)
    {
    if (MdlSupported(self))
        return mThis(self)->pathMessage;
    return NULL;
    }

static AtPdhMdlController TestSignalMdlController(AtPdhDe3 self)
    {
    if (MdlSupported(self))
        return mThis(self)->testSignal;
    return NULL;
    }

static AtPdhMdlController IdleSignalMdlController(AtPdhDe3 self)
    {
    if (MdlSupported(self))
        return mThis(self)->idleSignal;
    return NULL;
    }

static void MdlControlerCreate(AtPdhDe3 self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);

    if (!ThaModulePdhMdlIsSupported(pdhModule))
        return;

    mThis(self)->pathMessage = ThaModulePdhMdlControllerCreate(pdhModule, self, cAtPdhMdlControllerTypePathMessage);
    mThis(self)->testSignal = ThaModulePdhMdlControllerCreate(pdhModule, self,  cAtPdhMdlControllerTypeTestSignal);
    mThis(self)->idleSignal = ThaModulePdhMdlControllerCreate(pdhModule, self,  cAtPdhMdlControllerTypeIdleSignal);
    }

static eAtRet DataLinkEnable(AtPdhChannel self, eBool enable)
    {
    uint32 offset, regAddr, regVal;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    if (!ThaModulePdhMdlIsSupported(pdhModule))
        return cAtErrorModeNotSupport;

    /* Rx Enable */
    offset  = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);
    regAddr = mRxDs3E3OHProCtrl(self) + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRegRxDs3DlkEnMask, cThaRegRxDs3DlkEnShift, enable ? 1 : 0);
    mWrite(self, regAddr, regVal);

    /* Tx Enable */
    regAddr = cThaRegTxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRegTxDs3DlkEnMask, cThaRegTxDs3DlkEnShift, enable ? 1 : 0);
    mWrite(self, regAddr, regVal);

    /* Clear the RX MDL controller to NULL */
    if (!enable)
        mThis(self)->rxCurrentMdlController = NULL;

    return cAtOk;
    }

static eBool DataLinkIsEnabled(AtPdhChannel self)
    {
    uint32 offset, regAddr, regRxVal;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    if (!ThaModulePdhMdlIsSupported(pdhModule))
        return cAtFalse;

    offset  = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);
    regAddr = mRxDs3E3OHProCtrl(self) + offset;
    regRxVal = mRead(self, regAddr);
    return (regRxVal & cThaRegRxDs3DlkEnMask) ? cAtTrue : cAtFalse;
    }

static eBool DataLinkOptionIsSupported(uint32 option)
    {
    switch(option)
        {
        case cAtPdhDe3DataLinkOptionDlBits:  return cAtTrue;
        case cAtPdhDe3DataLinkOptionUdlBits: return cAtFalse;
        default: return cAtFalse;
        }
    }

static uint32 DataLinkOptionSw2Hw(uint32 option)
    {
    switch(option)
        {
        case cAtPdhDe3DataLinkOptionDlBits:  return 0;
        case cAtPdhDe3DataLinkOptionUdlBits: return 1;
        default: return 0;
        }
    }

static eAtRet DataLinkOptionSet(AtPdhDe3 self, eAtPdhDe3DataLinkOption option)
    {
    uint32 offset, regAddr, regVal;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    if ((!ThaModulePdhMdlIsSupported(pdhModule)) || (!DataLinkOptionIsSupported(option)))
        return cAtErrorModeNotSupport;

    /* Rx Enable */
    offset  = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);
    regAddr = mRxDs3E3OHProCtrl(self) + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaRegRxDs3E3DlSelMask, cThaRegRxDs3E3DlSelShift, DataLinkOptionSw2Hw(option));
    mWrite(self, regAddr, regVal);

    /* Tx Enable */
    regAddr = cThaRegTxM23E23Ctrl + offset;
    regVal = mRead(self, regAddr);
    mFieldIns(&regVal, cThaTxDs3DlSelMask, cThaTxDs3DlSelShift, DataLinkOptionSw2Hw(option));
    mWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 DataLinkOptionHw2Sw(uint32 option)
    {
    switch(option)
        {
        case 0 : return cAtPdhDe3DataLinkOptionDlBits;
        case 1 : return cAtPdhDe3DataLinkOptionUdlBits;
        default: return cAtPdhDe3DataLinkOptionUnknown;
        }
    }

static eAtPdhDe3DataLinkOption DataLinkOptionGet(AtPdhDe3 self)
    {
    uint32 offset, regAddr, regRxVal, hwValue;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    if ((!ThaModulePdhMdlIsSupported(pdhModule))||(!AtPdhChannelDataLinkIsEnabled((AtPdhChannel)self)))
        return cAtPdhDe3DataLinkOptionUnknown;

    offset  = mMethodsGet((ThaPdhDe3)self)->DefaultOffset((ThaPdhDe3)self);
    regAddr = mRxDs3E3OHProCtrl(self) + offset;
    regRxVal = mRead(self, regAddr);
    hwValue = ((regRxVal & cThaRegRxDs3E3DlSelMask) >> cThaRegRxDs3E3DlSelShift);
    
    return DataLinkOptionHw2Sw(hwValue);
    }

static eAtRet Init(AtChannel self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleMap);
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    DefaultSet((ThaPdhDe3)self);

    if (AtPdhChannelLinePrbsEngineGet((AtPdhChannel)self))
        AtPrbsEngineEnable(AtPdhChannelLinePrbsEngineGet((AtPdhChannel)self), cAtFalse);

    ThaModuleMapMapLineControlDe3DefaultSet(mapModule, (AtPdhChannel)self);

    /* Default timing */
    if (CanControlDe3Timing((AtPdhChannel)self))
        ret |= AtChannelTimingSet(self, cAtTimingModeSys, self);

    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    /* Need to show CDR internal state */
    if (CdrController(self))
        ThaCdrControllerDebug(CdrController(self));

    return cAtOk;
    }

static AtPdhMdlController RxHwCurrentMdlControllerGet(AtPdhDe3 self)
    {
    if (AtPdhMdlControllerMessageReceived(AtPdhDe3PathMessageMdlController(self)))
        return AtPdhDe3PathMessageMdlController(self);

    if (AtPdhMdlControllerMessageReceived(AtPdhDe3TestSignalMdlController(self)))
        return AtPdhDe3TestSignalMdlController(self);

    if (AtPdhMdlControllerMessageReceived(AtPdhDe3IdleSignalMdlController(self)))
        return AtPdhDe3IdleSignalMdlController(self);

    return NULL;
    }

static AtPdhMdlController RxCurrentMdlControllerGet(AtPdhDe3 self)
    {
    AtPdhMdlController currentController = RxHwCurrentMdlControllerGet(self);

    if (currentController)
        mThis(self)->rxCurrentMdlController = currentController;

    return mThis(self)->rxCurrentMdlController;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtPdhLoopbackModeLocalPayload)
        return cAtFalse;

    return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);
    }

static eAtRet LosDetectionByAllZeroPatternEnable(AtPdhChannel self, eBool enable)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint8 losThreshold = (uint8)(enable ? mMethodsGet(de3)->DefaultLosThreshold(de3) : 0);
    uint32 regAddr = cThaRegRxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    mFieldIns(&regVal, cThaRxDS3E3LosThresMask, cThaRxDS3E3LosThresShift, losThreshold);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet LosDetectionEnable(AtPdhChannel self, eBool enable)
    {
    if (ThaModulePdhDetectDe3LosByAllZeroPattern(PdhModule((AtChannel)self)))
        return LosDetectionByAllZeroPatternEnable(self, enable);

    return m_AtPdhChannelMethods->LosDetectionEnable(self, enable);
    }

static eBool LosDetectionIsEnabled(AtPdhChannel self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 regAddr = cThaRegRxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 losThreshold = (uint8)mRegField(regVal, cThaRxDS3E3LosThres);
    return (losThreshold == 0) ? cAtFalse : cAtTrue;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtEncapBinderDe3EncapHwIdAllocate(EncapBinder(self), (AtPdhDe3)self);
    }

static eAtRet AisAllOnesDetectionEnable(AtPdhChannel self, eBool enable)
   {
   ThaPdhDe3 de3 = (ThaPdhDe3)self;
   uint8 aisAllOnesThreshold = (uint8)(enable ? mMethodsGet(de3)->DefaultAisAllOnesThreshold(de3) : 0);
   uint32 regAddr = cThaRegRxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
   uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

   mFieldIns(&regVal, cThaRxDS3E3AisThresMask, cThaRxDS3E3AisThresShift, aisAllOnesThreshold);
   mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

   return cAtOk;
   }

static eBool AisAllOnesDetectionIsEnabled(AtPdhChannel self)
   {
   ThaPdhDe3 de3 = (ThaPdhDe3)self;
   uint32 regAddr = cThaRegRxM23E23Ctrl + mMethodsGet(de3)->DefaultOffset(de3);
   uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
   uint8 aisAllOnesThreshold = (uint8)mRegField(regVal, cThaRxDS3E3AisThres);

   return (aisAllOnesThreshold == 0) ? cAtFalse : cAtTrue;
   }

static AtErrorGenerator ValidErrorGeneratorGet(AtPdhChannel self, AtErrorGenerator generator,
                                               void (*PdhChannelErrorGeneratorRefSet)(AtPdhChannel channel, AtErrorGenerator generator))
    {
    AtPdhChannel source = NULL;

    if (generator == NULL)
        return generator;

    source = (AtPdhChannel)AtErrorGeneratorSourceGet(generator);
    if (source == self)
        return generator;

    if (source != NULL)
        PdhChannelErrorGeneratorRefSet(source, NULL);

    if (source == NULL || !AtErrorGeneratorIsStarted(generator))
        {
        PdhChannelErrorGeneratorRefSet(self, generator);
        AtErrorGeneratorSourceSet(generator, (AtObject)self);
        AtErrorGeneratorInit(generator);
        return generator;
        }

    return NULL;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtPdhChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtErrorGenerator generator = ThaModulePdhDe3TxErrorGeneratorGet(module);

    return ValidErrorGeneratorGet(self, generator, AtPdhChannelTxErrorGeneratorRefSet);
    }

static AtErrorGenerator RxErrorGeneratorGet(AtPdhChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtErrorGenerator generator = ThaModulePdhDe3RxErrorGeneratorGet(module);

    return ValidErrorGeneratorGet(self, generator, AtPdhChannelRxErrorGeneratorRefSet);
    }

static uint32 BoundPwHwIdGet(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    AtPdhDe3 de3 = (AtPdhDe3)self;

    return ThaModuleAbstractMapDe3BoundPwHwIdGet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), de3);
    }

static uint32 BoundEncapHwIdGet(AtChannel self)
    {
    return AtEncapBinderDe3BoundEncapHwIdGet(EncapBinder(self), self);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapDe3EncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtPdhDe3)self, enable);
    }

static eBool TxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapDe3EncapConnectionIsEnabled((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtPdhDe3)self);
    }

static eAtRet RxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapDe3EncapConnectionEnable((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe3)self, enable);
    }

static eBool RxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaModuleAbstractMapDe3EncapConnectionIsEnabled((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe3)self);
    }

static eAtRet CascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5)
    {
    *layer3 = 0;
    *layer4 = 0;
    *layer5 = 0;
    return ThaPdhDe3HwIdFactorGet((ThaPdhDe3)self, phyModule, layer1, layer2);
    }

static eBool IsManagedByModulePdh(ThaPdhDe3 self)
    {
    /* At default layer, DE3 is created for VC mapping, so it is not managed by module PDH */
    AtUnused(self);
    return cAtFalse;
    }

static eAtClockState HwClockStateTranslate(AtChannel self, uint8 hwState)
    {
    ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleCdr);

    return ThaModuleCdrClockStateFromLoChannelHwStateGet(moduleCdr, hwState);
    }

static uint8 RxAicGet(AtPdhDe3 self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    uint32 regAddr = De3RxFramerHwStatusRegister((AtPdhChannel)self) + mMethodsGet(de3)->DefaultOffset(de3);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    return (uint8)mRegField(regVal, cThaRegRxDS3E3CurAIC);
    }

static eAtRet AlarmAffectingHwEnable(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    uint32 address = cThaRegRxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);

    /* Although LOF and AIS-signal affecting can be disabled only in case
     * SAToP Framing monitoring, but we do not include the constraint here for
     * freely configuration sequence. */
    if (alarmType & cAtPdhDe3AlarmLof)
        mRegFieldSet(regVal, cThaRxDS3LOFFrwDis, (enable) ? 0 : 1);

    if (alarmType & cAtPdhDe3AlarmAis)
        {
        if (AtPdhDe3IsE3((AtPdhDe3)self))
            {
            mRegFieldSet(regVal, cThaRxDE3AISAll1sFrwDis, (enable) ? 0 : 1);
            mRegFieldSet(regVal, cThaRxDS3AISSigFrwEnb, 0);
            }
        else
            {
        mRegFieldSet(regVal, cThaRxDS3AISSigFrwEnb, (enable) ? 1 : 0);
            mRegFieldSet(regVal, cThaRxDE3AISAll1sFrwDis, 1);
            }
        }

    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet AlarmAffectingEnable(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    if (AtPdhChannelCanChangeAlarmAffecting(self, alarmType, enable))
        return AlarmAffectingHwEnable(self, alarmType, enable);

    return cAtErrorModeNotSupport;
    }

static eBool AlarmAffectingIsEnabled(AtPdhChannel self, uint32 alarmType)
    {
        uint32 address = cThaRegRxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
        uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);

    if (alarmType & cAtPdhDe3AlarmLos)
        return cAtTrue;

    /* Although LOF and AIS-signal affecting can be disabled only in case
     * SAToP Framing monitoring, but we do not include the constraint here for
     * freely configuration sequence. */
        if (alarmType & cAtPdhDe3AlarmLof)
            return mRegField(regVal, cThaRxDS3LOFFrwDis) ? cAtFalse : cAtTrue;

        if (alarmType & cAtPdhDe3AlarmAis)
        {
        if (AtPdhDe3IsE3((AtPdhDe3)self))
            return mRegField(regVal, cThaRxDE3AISAll1sFrwDis) ? cAtFalse : cAtTrue;
        else
            return mRegField(regVal, cThaRxDS3AISSigFrwEnb) ? cAtTrue : cAtFalse;
        }

    return cAtFalse;
    }

static eBool CanChangeAlarmAffecting(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    static const uint32 alarmsAffecting = cAtPdhDe3AlarmLof|cAtPdhDe3AlarmAis;
    AtUnused(self);

    /* LOS always causes AIS forwarding. */
    if (alarmType & cAtPdhDe3AlarmLos)
        return enable;

    if (alarmType & alarmsAffecting)
            return cAtTrue;

    return enable ? cAtFalse : cAtTrue;
    }

static uint32 AlarmAffectingMaskGet(AtPdhChannel self)
    {
    uint32 alarms  = cAtPdhDe3AlarmLos;
        uint32 address = cThaRegRxM23E23Ctrl + mMethodsGet(mThis(self))->DefaultOffset(mThis(self));
        uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);

    /* Although LOF and AIS-signal affecting can be disabled only in case
     * SAToP Framing monitoring, but we do not include the constraint here for
     * freely configuration sequence. */
        if (mRegField(regVal, cThaRxDS3LOFFrwDis) == 0)
            alarms |= cAtPdhDe3AlarmLof;

    if (AtPdhDe3IsE3((AtPdhDe3)self))
        {
        if (mRegField(regVal, cThaRxDE3AISAll1sFrwDis) == 0)
            alarms |= cAtPdhDe3AlarmAis;
        }
    else
        {
        if (regVal & cThaRxDS3AISSigFrwEnbMask)
            alarms |= cAtPdhDe3AlarmAis;
        }

    return alarms;
    }

static eAtRet TxSquelch(AtChannel self, eBool squelched)
    {
    if (squelched)
        return HelperTxAlarmForce(self, cAtPdhDe3AlarmAis, cAtTrue);
    return HelperTxAlarmForce(self, cAtPdhDe3AlarmAis, mThis(self)->txAisExplicitlyForced);
    }

static eAtRet DidBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwDidBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static eAtRet WillBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwWillBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static ThaModuleConcate ModuleConcate(AtChannel self)
    {
    return (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleConcateBindDe3ToSourceConcateGroup(ModuleConcate(self), self, group);
    ret |= ThaModuleAbstractMapBindDe3ToConcateGroup(MapModule((AtPdhChannel)self), (AtPdhDe3)self, group);

    return ret;
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleConcateBindDe3ToSinkConcateGroup(ModuleConcate(self), self, group);
    ret |= ThaModuleAbstractMapBindDe3ToConcateGroup(DemapModule((AtPdhChannel)self), (AtPdhDe3)self, group);

    return ret;
    }

static uint32 BoundConcateHwIdGet(AtChannel self)
    {
    return ThaModuleAbstractMapDe3BoundConcateHwIdGet(MapModule((AtPdhChannel)self), (AtPdhDe3)self);
    }

static uint8 NumberOfSubChannelsGet(AtPdhChannel self)
    {
    if (IsTerminatedDe3((ThaPdhDe3)self))
        return m_AtPdhChannelMethods->NumberOfSubChannelsGet(self);

    /* To eliminate to access sub-channels in case non-terminated DS3/E3 path */
    return 0;
    }

static void MethodsInit(ThaPdhDe3 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, DefaultLosThreshold);
        mMethodOverride(m_methods, DemapDefaultOffset);
        mMethodOverride(m_methods, CdrControllerCreate);
        mMethodOverride(m_methods, BpvCounterRead2Clear);
        mMethodOverride(m_methods, InterruptRead2Clear);
        mMethodOverride(m_methods, HwIdFactorGet);
        mMethodOverride(m_methods, De3FramingBindToPwSatop);
        mMethodOverride(m_methods, IsManagedByModulePdh);
        mMethodOverride(m_methods, ShouldConsiderLofWhenLos);
        mMethodOverride(m_methods, RetimingIsSupported);
        mMethodOverride(m_methods, RetimingControllerCreate);
        mMethodOverride(m_methods, DefaultAisAllOnesThreshold);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtPdhDe3 self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPdhDe3(AtPdhDe3 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhDe3Override, mMethodsGet(self), sizeof(m_AtPdhDe3Override));

        /* Setup methods */
        mMethodOverride(m_AtPdhDe3Override, TxFeacSet);
        mMethodOverride(m_AtPdhDe3Override, TxFeacGet);
        mMethodOverride(m_AtPdhDe3Override, RxFeacGet);
        mMethodOverride(m_AtPdhDe3Override, TxFeacWithModeSet);
        mMethodOverride(m_AtPdhDe3Override, TxFeacModeGet);

        mMethodOverride(m_AtPdhDe3Override, TxTtiSet);
        mMethodOverride(m_AtPdhDe3Override, TxTtiGet);
        mMethodOverride(m_AtPdhDe3Override, ExpectedTtiSet);
        mMethodOverride(m_AtPdhDe3Override, ExpectedTtiGet);
        mMethodOverride(m_AtPdhDe3Override, RxTtiGet);
        mMethodOverride(m_AtPdhDe3Override, TmEnable);
        mMethodOverride(m_AtPdhDe3Override, TmIsEnabled);
        mMethodOverride(m_AtPdhDe3Override, TxTmCodeSet);
        mMethodOverride(m_AtPdhDe3Override, TxTmCodeGet);
        mMethodOverride(m_AtPdhDe3Override, RxTmCodeGet);
        mMethodOverride(m_AtPdhDe3Override, TxPldTypeSet);
        mMethodOverride(m_AtPdhDe3Override, TxPldTypeGet);
        mMethodOverride(m_AtPdhDe3Override, RxPldTypeGet);
        mMethodOverride(m_AtPdhDe3Override, PathMessageMdlController);
        mMethodOverride(m_AtPdhDe3Override, TestSignalMdlController);
        mMethodOverride(m_AtPdhDe3Override, IdleSignalMdlController);
        mMethodOverride(m_AtPdhDe3Override, RxCurrentMdlControllerGet);
        mMethodOverride(m_AtPdhDe3Override, DataLinkOptionSet);
        mMethodOverride(m_AtPdhDe3Override, DataLinkOptionGet);
        mMethodOverride(m_AtPdhDe3Override, RxAicGet);
        }

    mMethodsSet(self, &m_AtPdhDe3Override);
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeGet);
        mMethodOverride(m_AtPdhChannelOverride, NationalBitsSet);
        mMethodOverride(m_AtPdhChannelOverride, NationalBitsGet);
        mMethodOverride(m_AtPdhChannelOverride, RxNationalBitsGet);
        mMethodOverride(m_AtPdhChannelOverride, IdleSignalIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, IdleEnable);
        mMethodOverride(m_AtPdhChannelOverride, IdleIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, LinePrbsEngineGet);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkEnable);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, AutoRaiEnable);
        mMethodOverride(m_AtPdhChannelOverride, AutoRaiIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, LosDetectionEnable);
        mMethodOverride(m_AtPdhChannelOverride, LosDetectionIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, TxErrorGeneratorGet);
        mMethodOverride(m_AtPdhChannelOverride, RxErrorGeneratorGet);
        mMethodOverride(m_AtPdhChannelOverride, HasParentAlarmForwarding);
        mMethodOverride(m_AtPdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtPdhChannelOverride, CascadeHwIdGet);
    	mMethodOverride(m_AtPdhChannelOverride, AisAllOnesDetectionEnable);
        mMethodOverride(m_AtPdhChannelOverride, AisAllOnesDetectionIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtPdhChannelOverride, NumberOfSubChannelsGet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, TimingModeIsSupported);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, TimingSourceGet);
        mMethodOverride(m_AtChannelOverride, ClockStateGet);
        mMethodOverride(m_AtChannelOverride, HwClockStateGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SpecificDefectInterruptClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, NumBlocksNeedToBindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, EncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, PppBlockRange);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, SurEngineAddress);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, BoundPwHwIdGet);
        mMethodOverride(m_AtChannelOverride, BoundEncapHwIdGet);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, AttController);
        mMethodOverride(m_AtChannelOverride, HwClockStateTranslate);
        mMethodOverride(m_AtChannelOverride, PwTimingRestore);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, TxSquelch);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, DidBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, WillBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        mMethodOverride(m_AtChannelOverride, BindToSinkGroup);
        mMethodOverride(m_AtChannelOverride, BoundConcateHwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtPdhDe3(self);
    OverrideAtPdhChannel(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

AtPdhDe3 ThaPdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((ThaPdhDe3)self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    MdlControlerCreate(self);

    return self;
    }

AtPdhDe3 ThaPdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhDe3ObjectInit(newDe3, channelId, module);
    }

ThaCdrController ThaPdhDe3CdrControllerGet(ThaPdhDe3 self)
    {
    if (self->cdrController == NULL)
        self->cdrController = mMethodsGet(self)->CdrControllerCreate(self);

    return self->cdrController;
    }

uint8 ThaPdhDe3PartId(ThaPdhDe3 self)
    {
    return (uint8)(self ? PartId(self) : 0);
    }

AtPrbsEngine ThaPdhDe3CachePrbsEngine(ThaPdhDe3 self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    return engine;
    }

eAtRet ThaPdhDe3WorkingSideSet(ThaPdhDe3 self, eThaPdhDe3WorkingSide workingSide)
    {
    if (self)
        return WorkingSideSet(self, workingSide);
    return cAtErrorNullPointer;
    }

uint32 ThaPdhDe3DefaultOffset(ThaPdhDe3 self)
    {
    if (self)
        return mMethodsGet(self)->DefaultOffset(self);
    return 0x0;
    }

void ThaPdhDe3ClockExtractorSet(ThaPdhDe3 self, AtClockExtractor clockExtractor)
    {
    if (self)
        mThis(self)->clockExtractor = clockExtractor;
    }

AtClockExtractor ThaPdhDe3ClockExtractorGet(ThaPdhDe3 self)
    {
    if (self)
        return mThis(self)->clockExtractor;
    return NULL;
    }

eAtRet ThaPdhDe3HwIdFactorGet(ThaPdhDe3 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice)
    {
    if (self)
        return mMethodsGet(self)->HwIdFactorGet(self, phyModule, slice, hwIdInSlice);
    return cAtErrorNullPointer;
    }

eBool ThaPdhDe3IsManagedByModulePdh(ThaPdhDe3 self)
    {
    if (self)
        return mMethodsGet(self)->IsManagedByModulePdh(self);

    return cAtTrue;
    }

eAtRet ThaPdhDe3De1Hw2SwIdGet(ThaPdhDe3 de3, uint8 hwDe2Id, uint8 hwDe1Id, uint8* de2Id, uint8* de1Id)
    {
    if (de3 == NULL)
        return cAtErrorNullPointer;

    if (AtPdhChannelFrameTypeGet((AtPdhChannel)de3) == cAtPdhE3FrmG751Chnl16E1s)
        {
        uint32 flatId = (uint32)(hwDe2Id * 3 + hwDe1Id);
        *de2Id = (uint8)(flatId / 4);
        *de1Id = (uint8)(flatId % 4);
        }

    return cAtOk;
    }

eBool ThaPdhDe3RetimingIsSupported(ThaPdhDe3 self)
    {
    if (self)
        return mMethodsGet(self)->RetimingIsSupported(self);
    return cAtFalse;
    }

ThaPdhRetimingController ThaPdhDe3RetimingControllerGet(ThaPdhDe3 self)
    {
    if (self)
        return RetimingControllerGet(self);

    return NULL;
    }

eAtRet ThaPdhDe3RxFramingMonOnlyEnable(ThaPdhDe3 self, eBool enable)
    {
    if (self)
        return RxFramerMonitorOnlyEnable(self, enable);

    return cAtErrorNullPointer;
    }

eAtRet ThaPdhDe3TxFramerBypass(ThaPdhDe3 self, eBool bypass)
    {
    if (self)
        return TxFramerBypass(self, bypass);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaPdhDe3MapBypass(ThaPdhDe3 self, eBool bypass)
    {
    if (self)
        return MapBypassForChannelizedBert(self, bypass);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaPdhDe3FramingBypass(ThaPdhDe3 self, eBool isBypass)
    {
    if (self)
        return De3FramingBypass(self, isBypass);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhDe3PrbsDe3LevelSet(ThaPdhDe3 self, eBool en)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    self->enablePrbsDe3Level = en;
    return cAtOk;
    }

eBool ThaPdhDe3PrbsDe3LevelGet(ThaPdhDe3 self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    return  self->enablePrbsDe3Level;
    }

AtPdhMdlController ThaPdhDe3MdlControllerGet(ThaPdhDe3 self)
    {
    if (self)
        return MdlControllerGet(self);
    return NULL;
    }

eAtRet ThaPdhDe3HwFrameTypeSet(ThaPdhDe3 self, uint16 frameType)
    {
    if (self)
        return HwFrameTypeSet(self, frameType);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaPdhDe3MappingDisable(ThaPdhDe3 self)
    {
    if (self)
        return MappingDisable(self);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhDe3MappingRestore(ThaPdhDe3 self)
    {
    if (self)
        return MappingRestore(self);
    return cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaModulePdhRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : PDH register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "../../../util/AtIteratorInternal.h"
#include "../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "ThaModulePdh.h"
#include "ThaModulePdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModulePdhRegisterIterator
    {
    tAtModuleRegisterIterator super;
    }tThaModulePdhRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePdhRegisterIterator);
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
    return AtModulePdhNumberOfDe1sGet((AtModulePdh)AtModuleRegisterIteratorModuleGet(self));
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

	mNextReg(iterator, 0, cThaRegPdhGlbCtrl);
	mNextReg(iterator, cThaRegPdhGlbCtrl, cThaRegPdhLIULooptimeCfg);
	mNextReg(iterator, cThaRegPdhLIULooptimeCfg, cThaRegDS1E1J1RxFrmrCtrl);
	mNextChannelRegister(iterator, cThaRegDS1E1J1RxFrmrCtrl, cThaRegDS1E1J1RxFrmrperChnIntrEnCtrl);
	mNextChannelRegister(iterator, cThaRegDS1E1J1RxFrmrperChnIntrEnCtrl, cThaRegDS1E1J1TxFrmrCtrl);
	mNextChannelRegister(iterator, cThaRegDS1E1J1TxFrmrCtrl, cThaRegDS1E1J1TxFrmrSigingInsCtrl);
	mNextChannelRegister(iterator, cThaRegDS1E1J1TxFrmrSigingInsCtrl, cThaRegDS1E1J1TxFrmrSigingCpuInsEnCtrl);
	mNextChannelRegister(iterator, cThaRegDS1E1J1TxFrmrSigingCpuInsEnCtrl, cAtModuleRegisterIteratorEnd);

	return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, MaxNumChannels);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModulePdhRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;
    return self;
    }

AtIterator ThaModulePdhRegisterIteratorCreate(AtModule module)
    {
    return ThaModulePdhRegisterIteratorObjectInit(AtOsalMemAlloc(ObjectSize()), module);
    }

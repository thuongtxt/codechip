/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhPrmAnalyzer.c
 *
 * Created Date: Nov 7, 2017
 *
 * Description : PRM analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPrmMessage.h"
#include "../../../generic/common/AtChannelInternal.h" /* For read/write */
#include "ThaModulePdh.h"
#include "ThaPdhPrmAnalyzerInternal.h"
#include "ThaPdhPrmAnalyzerReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumBytes      128
#define cFlag             0x7E

/* State */
#define cStateFindFlag    0
#define cStateReadMessage 1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhPrmAnalyzer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhPrmAnalyzerMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(ThaPdhPrmAnalyzer self)
    {
    AtChannel de1 = AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    return (ThaModulePdh)AtChannelModuleGet(de1);
    }

static uint32 AddressWithLocalAddress(ThaPdhPrmAnalyzer self, uint32 localAddress)
    {
    return ThaModulePdhPrmBaseAddress(PdhModule(self)) + localAddress;
    }

static uint32 HwMaxId(void)
    {
    return cAf6_upen_prm_ctrl_prmid_cfg_Mask >> cAf6_upen_prm_ctrl_prmid_cfg_Shift;
    }

static uint32 ChannelPrmId(ThaPdhPrmAnalyzer self)
    {
    ThaPdhDe1 channel = (ThaPdhDe1)AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    ThaModulePdh pdhModule = PdhModule(self);
    uint32 prmOffset = ThaModulePdhDe1PrmOffset(pdhModule, channel);
    return prmOffset - ThaModulePdhPrmBaseAddress(pdhModule);
    }

static uint32 UpenRxPrmCtrlBase(ThaPdhPrmAnalyzer self)
    {
    AtUnused(self);
    return cAf6Reg_upen_prm_ctrl_Base;
    }

static uint32 UpenRxPrmCtrlAddress(ThaPdhPrmAnalyzer self)
    {
    return AddressWithLocalAddress(self, mMethodsGet(self)->UpenRxPrmCtrlBase(self));
    }

static eAtRet ChannelHwSet(ThaPdhPrmAnalyzer self, AtChannel channel)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_upen_prm_ctrl_Base);
    uint32 regVal;
    uint32 hwId = ChannelPrmId(self);

    if (hwId > HwMaxId())
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                                AtSourceLocation, "de1.%s has out-of-range PRM HW ID\r\n", AtChannelIdString(channel));
        return cAtErrorOutOfRangParm;
        }

    regVal = mChannelHwRead(channel, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_ctrl_prmid_cfg_, hwId);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet ChannelSet(AtPktAnalyzer self, AtChannel channel)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtPktAnalyzerMethods->ChannelSet(self, channel);
    ret |= mMethodsGet(mThis(self))->ChannelHwSet(mThis(self), channel);

    return ret;
    }

static eAtRet CaptureEnable(ThaPdhPrmAnalyzer self, eBool enabled)
    {
    AtChannel channel = AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    uint32 regAddr = ThaPdhPrmAnalyzerUpenRxPrmCtrlAddress(self);
    uint32 regVal = mChannelHwRead(channel, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_upen_prm_ctrl_prm_mon_en_, enabled ? 1 : 0);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static AtPacket CreatePacket(AtPktAnalyzer self, uint8 *buffer, uint32 numBytes)
    {
    AtUnused(self);
    return AtPrmMessageNew(cAtPrmMessageTypePRM, buffer, numBytes, cAtPacketCacheModeNoCache);
    }

static uint32 UpenRxprmMonBase(ThaPdhPrmAnalyzer self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxprm_mon_Base;
    }

static uint32 UpenRxprmMonAddress(ThaPdhPrmAnalyzer self)
    {
    return AddressWithLocalAddress(mThis(self), mMethodsGet(self)->UpenRxprmMonBase(self));
    }

static void AllMessagesRead(AtPktAnalyzer self)
    {
    uint32 regBase = UpenRxprmMonAddress(mThis(self));
    uint32 byte_i;
    AtChannel channel = AtPktAnalyzerChannelGet(self);
    uint32 state = cStateFindFlag;
    uint32 bufferSize;
    uint8 *buffer = AtPktAnalyzerBufferGet(self, &bufferSize);
    uint32 messageLength = 0;
    AtList packets = AtPktAnalyzerPacketListGet(self);

    for (byte_i = 0; byte_i < cMaxNumBytes; byte_i++)
        {
        uint32 regAddr = regBase + byte_i;
        uint32 regVal = mChannelHwRead(channel, regAddr, cAtModulePdh);

        switch (state)
            {
            case cStateFindFlag:
                if (regVal == cFlag)
                    state = cStateReadMessage;
                break;

            case cStateReadMessage:
                /* End of current message */
                if (regVal == cFlag)
                    {
                    if (messageLength > 0)
                        {
                        AtPacket newPacket = CreatePacket(self, buffer, messageLength);
                        AtListObjectAdd(packets, (AtObject)newPacket);

                        /* For next packet */
                        buffer        = buffer + messageLength;
                        bufferSize    = bufferSize - messageLength;
                        messageLength = 0;
                        }
                    }

                /* Still reading */
                else
                    {
                    if (messageLength >= bufferSize)
                        break;

                    buffer[messageLength] = (uint8)regVal;
                    messageLength = messageLength + 1;
                    }

                break;

            default:
                break;
            }
        }
    }

static eAtRet HwBufferFlush(AtPktAnalyzer self)
    {
    uint32 regAddr = UpenRxprmMonAddress(mThis(self));
    uint32 byte_i;
    AtChannel channel = AtPktAnalyzerChannelGet(self);

    for (byte_i = 0; byte_i < cMaxNumBytes; byte_i++)
        mChannelHwWrite(channel, regAddr + byte_i, 0, cAtModulePdh);

    return cAtOk;
    }

static AtList CaptureRxPackets(AtPktAnalyzer self)
    {
    AtList packets;

    if (!AtPktAnalyzerIsStarted(self))
        return NULL;

    AtPktAnalyzerPacketFlush(self);
    AllMessagesRead(self);
    packets = AtPktAnalyzerPacketListGet(self);

    /* Need clean buffer state for next analyzing */
    HwBufferFlush(self);

    return packets;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "prm_analyzer";
    }

static eAtRet Start(AtPktAnalyzer self)
    {
    return CaptureEnable(mThis(self), cAtTrue);
    }

static eAtRet Stop(AtPktAnalyzer self)
    {
    eAtRet ret = cAtOk;

    ret |= CaptureEnable(mThis(self), cAtFalse);
    ret |= HwBufferFlush(self);

    return ret;
    }

static eBool IsStarted(AtPktAnalyzer self)
    {
    AtChannel channel = AtPktAnalyzerChannelGet((AtPktAnalyzer)self);
    uint32 regAddr = ThaPdhPrmAnalyzerUpenRxPrmCtrlAddress(mThis(self));
    uint32 regVal = mChannelHwRead(channel, regAddr, cAtModulePdh);
    return (regVal & cAf6_upen_prm_ctrl_prm_mon_en_Mask) ? cAtTrue : cAtFalse;
    }

static void OverrideAtObject(AtPktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    AtPktAnalyzer analyzer = (AtPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(analyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, CaptureRxPackets);
        mMethodOverride(m_AtPktAnalyzerOverride, Start);
        mMethodOverride(m_AtPktAnalyzerOverride, Stop);
        mMethodOverride(m_AtPktAnalyzerOverride, IsStarted);
        mMethodOverride(m_AtPktAnalyzerOverride, ChannelSet);
        }

    mMethodsSet(analyzer, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtObject(self);
    OverrideAtPktAnalyzer(self);
    }

static void MethodsInit(ThaPdhPrmAnalyzer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ChannelHwSet);
        mMethodOverride(m_methods, UpenRxPrmCtrlBase);
        mMethodOverride(m_methods, UpenRxprmMonBase);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPrmAnalyzer);
    }

AtPktAnalyzer ThaPdhPrmAnalyzerObjectInit(AtPktAnalyzer self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPktAnalyzerObjectInit(self, (AtChannel)channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer ThaPdhPrmAnalyzerNew(AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPrmAnalyzerObjectInit(newAnalyzer, channel);
    }

uint32 ThaPdhPrmAnalyzerUpenRxPrmCtrlAddress(ThaPdhPrmAnalyzer self)
    {
    if (self)
        return UpenRxPrmCtrlAddress(self);
    return cInvalidUint32;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhPrmController.h
 *
 * Created Date: Oct 27, 2015
 *
 * Description : PRM controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPRMCONTROLLER_H_
#define _THAPDHPRMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pdh/AtPdhPrmController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPrmController *ThaPdhPrmController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhPrmController ThaPdhPrmControllerNew(AtPdhDe1 de1);

AtPdhPrmController Tha60210021PdhPrmControllerNew(AtPdhDe1 de1);
AtPdhPrmController Tha60210031PdhPrmControllerNew(AtPdhDe1 de1);
AtPdhPrmController Tha60210011PdhPrmControllerNew(AtPdhDe1 de1);
AtPdhPrmController Tha60210061PdhPrmControllerNew(AtPdhDe1 de1);

/* For Fcs Bit-order */
eAtModulePdhRet Tha60210021PdhDe1PrmFcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order);
eAtBitOrder Tha60210021PdhDe1PrmFcsBitOrderGet(AtPdhPrmController self);

eBool ThaPdhPrmControllerHasRxEnabledCfgRegister(ThaPdhPrmController self);

#endif /* _THAPDHPRMCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhUtil.h
 * 
 * Created Date: Jul 1, 2017
 *
 * Description : PDH util functions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHUTIL_H_
#define _THAPDHUTIL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool ThaPdhChannelFramingShouldBypassOnSatopBinding(AtPdhChannel self, AtPw pseudowire);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHUTIL_H_ */


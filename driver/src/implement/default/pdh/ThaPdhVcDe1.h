/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaPdhDe1
 *
 * File        : ThaPdhVcDe1.h
 *
 * Created Date: Sep 10, 2012
 *
 * Author      : ntdung
 *
 * Description : This file contains common prototypes of DS1/E1/J1 module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _THAPDHVCDE1_H
#define _THAPDHVCDE1_H

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhDe1Internal.h"
#include "ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tThaPdhVcDe1 * ThaPdhVcDe1;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*-------------------------------- Entries -----------------------------------*/
AtPdhDe1 ThaPdhVcDe1ObjectNew(AtSdhVc vc1x, AtModulePdh module);
eAtRet ThaSdhVcPdhFrameBypass(AtSdhVc sdhVc);


#ifdef __cplusplus
}
#endif
#endif /* _THAPDHVCDE1_H */

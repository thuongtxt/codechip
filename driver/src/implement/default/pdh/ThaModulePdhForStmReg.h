/*
 * ThaModulePdhForStmReg.h
 *
 *  Created on: Oct 5, 2012
 *      Author: dungnt
 */

#ifndef THAMODULEPDHFORSTMREG_H_
#define THAMODULEPDHFORSTMREG_H_

#include "ThaModulePdhReg.h"

/*===============================================================================
*                    Additional fields of PDH's registers for STM module
*================================================================================*/
/*------------------------------------------------------------------------------
Reg Name: STS/VT Map Control
Reg Addr: 0x609800 - 0x64997B (for 336 lines)
          The address format for these registers is 0x609800 + 32*stsid +
          4*vtgid + vtid
          Where: stsid: 0 - 11
          vtgid: 0 - 6
          vtid: 0 - 3
Reg Desc: The STS/VT Map Control is use to configure for per channel STS/VT
          Map operation.
------------------------------------------------------------------------------*/
#define cThaRegStsVtMapCtrl 0x00776000


#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Mask     cBit20
#define cAf6_stsvt_map_ctrl_Pdhoversdhmd_Shift    20
/*--------------------------------------
BitField Name: StsVtPdhOverSdhMd
BitField Type: RW
BitField Desc: Set 1 to PDH LIU over OCN
BitField Bits: [20]
--------------------------------------*/
#define cThaStsVtMapPdhOverSdhMdMask                cBit20
#define cThaStsVtMapPdhOverSdhMdShift               20

/*--------------------------------------
BitField Name: StsVtMapJitIDCfg
BitField Type: R/W
BitField Desc: This bit is used to set STS/VT Mapping Jitter ID configure
--------------------------------------*/
#define cThaStsVtMapAismaskMask                     cBit19
#define cThaStsVtMapAismaskShift                    19

/*--------------------------------------
BitField Name: StsVtMapJitIDCfg
BitField Type: R/W
BitField Desc: This bit is used to set STS/VT Mapping Jitter ID configure
--------------------------------------*/
#define cThaStsVtMapJitIDCfgMask                     cBit18_15
#define cThaStsVtMapJitIDCfgShift                    15

/*--------------------------------------
BitField Name: StsVtMapThr
BitField Type: R/W
BitField Desc: This bit is used to set STS/VT Mapping Stuff Threshold Configure
--------------------------------------*/
#define cThaStsVtMapThrMask                     cBit14_10
#define cThaStsVtMapThrShift                    10

/*--------------------------------------
BitField Name: StsVtMapJitCfg
BitField Type: R/W
BitField Desc: This bit is used to set STS/VT Mapping Jitter configure
--------------------------------------*/
#define cThaStsVtMapJitCfgMask                     cBit9_6
#define cThaStsVtMapJitCfgShift                    6
#define cThaStsVtMapJitCfgBitFifoModeMask          cBit6
#define cThaStsVtMapJitCfgBitFifoModeShift         6
#define cThaStsVtMapJitCfgMasterEnableMask         cBit7
#define cThaStsVtMapJitCfgMasterEnableShift        7
#define cThaStsVtMapJitCfgByteFifoStuffModeMask    cBit9
#define cThaStsVtMapJitCfgByteFifoStuffModeShift   9

/*--------------------------------------
BitField Name: StsVtMapCenterFifo
BitField Type: R/W
BitField Desc: This bit is set to 1 to force center the mapping FIFO.
--------------------------------------*/
#define cThaStsVtMapCenterFifoMask                     cBit5
#define cThaStsVtMapCenterFifoShift                    5

/*--------------------------------------
BitField Name: StsVtMapBypass
BitField Type: R/W
BitField Desc: This bit is set to 1 to bypass the STS/VT map
--------------------------------------*/
#define cThaStsVtMapBypassMask                         cBit4
#define cThaStsVtMapBypassShift                        4

/*--------------------------------------
BitField Name: StsVtMapMd[2:0]
BitField Type: R/W
BitField Desc: STS/VT Mapping mode
               - 0000: DS3 to VC3 Map
               - 0010: E3 to VC3 Map
               - 0100: DS3 to TU3 Map
               - 0110: E3 to TU3 Map
               - 1001: DS1 to VT1.5 Map
               - 1000: E1 to VT2 Map
--------------------------------------*/
#define cThaStsVtMapMdMask                             cBit3_0
#define cThaStsVtMapMdShift                            0

/*------------------------------------------------------------------------------
Reg Name: STS/VT Demap Control
Reg Addr: 0x00724000
          The address format for these registers is 0x00724000 + 32*stsid +
          4*vtgid + vtid
          Where: stsid: 0 - 11
          vtgid: 0 - 6
          vtid: 0 - 3
Reg Desc: The STS/VT Demap Control is use to configure for per channel STS/VT
          demap operation.
------------------------------------------------------------------------------*/
#define cThaRegStsVtDmapCtrl 0x00724000


/*--------------------------------------
Field: [9] %%  Pdhoversdhmd %% Set 1 to PDH LIU over OCN %% RW %% 0x0 %% 0x0
--------------------------------------*/
#define cAf6_stsvt_demap_ctrl_Pdhoversdhmd_Mask                                                         cBit9
#define cAf6_stsvt_demap_ctrl_Pdhoversdhmd_Shift                                                            9


/*--------------------------------------
BitField Name: StsVtDmapPdhOverSdhMd
BitField Type: R/W
BitField Desc: Set 1 to PDH LIU over OCN
--------------------------------------*/
#define cThaStsVtDmapPdhOverSdhMdMask                  cBit9
#define cThaStsVtDmapPdhOverSdhMdShift                 9

/*--------------------------------------
BitField Name: StsVtDemapFrcAis
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsVtDmapFrcAisMask                        cBit8
#define cThaStsVtDmapFrcAisShift                       8

/*--------------------------------------
BitField Name: StsVtDemapAutoAis
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsVtDmapAutoAisMask                       cBit7
#define cThaStsVtDmapAutoAisShift                      7

/*--------------------------------------
BitField Name: StsVtDemapAutoAis
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsVtDemapCepMdMask                       cBit6
#define cThaStsVtDemapCepMdShift                      6

/*--------------------------------------
BitField Name: StsVtDemapAutoAis
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsVtDemapFrmtypeMask                       cBit5_4
#define cThaStsVtDemapFrmtypeShift                      4

/*--------------------------------------
BitField Name: StsVtDemapAutoAis
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaStsVtDemapSigtypeMask                       cBit3_0
#define cThaStsVtDemapSigtypeShift                      0

/*------------------------------------------------------------------------------
Reg Name: DS1/E1/J1 Rx Framer Mux Control
Reg Addr: 0x00730000 - 0x0073000B (for 12 lines)
          The address format for these registers is 0x00730000 + stsid
          Where: sts: 0 - 11
Reg Desc: These registers are used to configure for the source data of Ds1/E1
          which may be get from Serial PDH interface or from SONET/SDH.
------------------------------------------------------------------------------*/
#define cThaRegDS1E1J1RxFrmrMuxCtrl2 0x730000

/*-----------
BitField Name: DE1MuxSel
BitField Type: R/W
BitField Desc: 336 Selector bit for 336 DS1/E1/J1 signals
               - 1: source selected from txde1,
               - 0 (default): source selected from m13 demux.
------------*/
#define cThaDE1MuxSelMask2(de2, de1)                   (cBit0 << (uint32)(4 * (de2) + de1))
#define cThaDE1MuxSelShift2(de2, de1)                  (4 * (de2) + (de1))

#define cThaRegRxM12E12Mux  0x00741300

/*------------------------------------------------------------------------------
Reg Name: PDH Rx Per STS/VC Payload Control
Reg Addr: 0x00730020 - 0x0073002B (for 12 STSs)
          The address format for these registers is 0x00730000+ stsid
          Where: stsid: 0 - 11
Reg Desc: The STS configuration registers are used to configure kinds of
          STS-1s and the kinds of VT/TUs in each STS-1.
------------------------------------------------------------------------------*/
#define cThaRegPdhRxPerStsVCPldCtrl 0x00730020

/*-----------
BitField Name: PdhVtRxMapTug2Type
BitField Type: R/W
BitField Desc: Define types of VT/Tus in TUG-2.
#ifdef __cplusplus
extern "C" {
#endif
               - 0: TU11 type
               - 1: TU12 type
               - 2: VT3 type
               - 3: TU2 type
------------*/
#define cThaPdhVtRxMapTug2TypeMask2(vtg)              (cBit1_0 << (uint32)(2 * (vtg)))
#define cThaPdhVtRxMapTug2TypeShift2(vtg)             (2 * (vtg))

/*--------------------------------------
BitField Name: PDHVtRxMapTug26Type[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: PDHVtRxMapTug25Type[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: PDHVtRxMapTug24Type[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: PDHVtRxMapTug23Type[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: PDHVtRxMapTug22Type[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: PDHVtRxMapTug21Type[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: PDHVtRxMapTug20Type[1:0]
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: RxM23E23 Control
Reg Addr: 0x609800 - 0x64997B (for 12 lines)
          The address format for these registers is 0x609800 + de3id
          Where: de3id: 0 - 11
Reg Desc: The RxM23E23 Control is use to configure for per channel Rx DS3/E3
          operation.
------------------------------------------------------------------------------*/
#define cThaRegRxM23E23Ctrl                          0x00740000

#define cThaRxDE3AISAll1sFrwDisMask                    cBit22
#define cThaRxDE3AISAll1sFrwDisShift                   22

#define cThaRxDS3LOFFrwDisMask                         cBit21
#define cThaRxDS3LOFFrwDisShift                        21

#define cThaRxDS3AISSigFrwEnbMask                      cBit20
#define cThaRxDS3AISSigFrwEnbShift                     20

#define cThaRxDE3ForceAISLbitMask                      cBit19
#define cThaRxDE3ForceAISLbitShift                     19

#define cThaRxDE3MonOnlyMask                           cBit18
#define cThaRxDE3MonOnlyShift                          18

/*--------------------------------------
BitField Name: RxDS3E3ChEnb
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDS3E3ChEnbMask                           cBit17
#define cThaRxDS3E3ChEnbShift                          17

/*--------------------------------------
BitField Name: RxDS3E3LosThres
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDS3E3LosThresMask                          cBit16_14
#define cThaRxDS3E3LosThresShift                         14

/*--------------------------------------
BitField Name: RxDS3E3AisThres
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDS3E3AisThresMask                          cBit13_11
#define cThaRxDS3E3AisThresShift                         11

/*--------------------------------------
BitField Name: RxDS3E3LofThres
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDS3E3LofThresMask                          cBit10_8
#define cThaRxDS3E3LofThresShift                         8

/*--------------------------------------
BitField Name: RxDS3E3PayAllOne
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDS3E3PayAllOneMask                         cBit7
#define cThaRxDS3E3PayAllOneShift                        7

/*--------------------------------------
BitField Name: RxDS3E3LineAllOne
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDS3E3LineAllOneMask                        cBit6
#define cThaRxDS3E3LineAllOneShift                       6

/*--------------------------------------
BitField Name: RxDS3E3LineLoop
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaRxDS3E3LineLoopMask                          cBit5
#define cThaRxDS3E3LineLoopShift                         5

/*--------------------------------------
BitField Name: RxDS3E3FrcLof
BitField Type: R/W
BitField Desc: This bit is set to 1 to force the Rx DS3/E3 framer into LOF
               status.
               - 1: force LOF.
               - 0: not force LOF.
--------------------------------------*/
#define cThaRxDS3E3FrcLofMask                          cBit4
#define cThaRxDS3E3FrcLofShift                         4

/*--------------------------------------
BitField Name: RxDS3E3Md[2:0]
BitField Type: R/W
BitField Desc: Rx DS3/E3 framing mode
               - 000: DS3 Unframed
               - 001: DS3 C-bit parity carrying 28 DS1s or 21 E1s
               - 010: DS3 M23 carrying 28 DS1s or 21 E1s
               - 011: Reserved
               - 100: E3 Unframed
               - 101: Reserved
               - 110: E3 G.751 carrying 16 E1s
               - 111: Bypass RxM13E13 (DS1/E1 mode)
--------------------------------------*/
#define cThaRxDS3E3MdMask                              cBit3_0
#define cThaRxDS3E3MdShift                             0

/*------------------------------------------------------------------------------
Reg Name: TxM23E23 Control
Reg Addr: 0x00780000 - 0x0078000B
          The address format for these registers is 0x00780000 + de3id
          Where: de3id: 0-11
Reg Desc: The TxM23E23 Control is use to configure for per channel Tx DS3/E3
          operation.
------------------------------------------------------------------------------*/
#define cThaRegTxM23E23Ctrl                               0x780000

#define cThaTxDS3E3LineAisPatternSelectMask               cBit30
#define cThaTxDS3E3LineAisPatternSelectShift              30

#define cThaTxDS3E3LineAisSelectMask               cBit29
#define cThaTxDS3E3LineAisSelectShift              29

#define cThaTxDS3E3ChEnbMask                              cBit28
#define cThaTxDS3E3ChEnbShift                             28

/*--------------------------------------
BitField Name: TxDS3E3OHVal
BitField Type:
BitField Desc:
DS3Cbit:
    TxDS3E3OHVal[0]: DL bit
    TxDS3E3OHVal[1]: UDL bit
G751:
    TxDS3E3OHVal[0]: N bit
G832:
    TxDS3E3OHVal[7:0]: NR and GC byte
--------------------------------------*/
#define cThaTxDS3E3OHValTailMask                           cBit31_26
#define cThaTxDS3E3OHValTailShift                          26

#define cThaTxDs3DlSelMask                                cBit27_26
#define cThaTxDs3DlSelShift                               26

#define cThaTxDS3E3OHValTailDwIndex                        0

/*--------------------------------------
BitField Name: TxDS3E3OHInsert
BitField Type:
BitField Desc:
    0: HW auto generate value (0x7E)
    1: SW insert value via  TxDS3E3OHValue
--------------------------------------*/
#define cThaTxDS3E3OHInsertMask                       cBit25
#define cThaTxDS3E3OHInsertShift                      25

/*--------------------------------------
BitField Name: TxDS3E3LineAllOne
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaTxDS3E3LineAllOneMask                      cBit20
#define cThaTxDS3E3LineAllOneShift                     20

/*--------------------------------------
BitField Name: TxDS3E3PayLoop
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaTxDE3RmtPayLoopMask                              cBit19
#define cThaTxDE3RmtPayLoopShift                             19

/*--------------------------------------
BitField Name: TxDS3E3LineLoop
BitField Type: R/W
BitField Desc:
--------------------------------------*/
#define cThaTxDE3RmtLineLoopMask                              cBit18
#define cThaTxDE3RmtLineLoopShift                             18

/*--------------------------------------
BitField Name: TxDS3E3FrcYel
BitField Type:
BitField Desc: TxDS3E3FrcYel
--------------------------------------*/
#define cThaTxDS3E3FrcYelMask                          cBit17
#define cThaTxDS3E3FrcYelShift                         17

/*--------------------------------------
BitField Name: TxDS3E3AutoYel
BitField Type:
BitField Desc: TxDS3E3AutoYel
--------------------------------------*/
#define cThaTxDS3E3AutoYelMask                         cBit16
#define cThaTxDS3E3AutoYelShift                        16

/*--------------------------------------
BitField Name: TxDS3E3LoopMd
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: TxDS3E3LoopEn
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/

/*--------------------------------------
BitField Name: TxDS3E3Ohbypass
BitField Type:
BitField Desc: TxDS3E3Ohbypass
--------------------------------------*/

/*--------------------------------------
BitField Name: TxDS3E3IdleSet
BitField Type:
BitField Desc: TxDS3E3IdleSet
--------------------------------------*/
#define cThaTxDS3E3IdleSetMask                         cBit5
#define cThaTxDS3E3IdleSetShift                        5

/*--------------------------------------
BitField Name: TxDS3E3AisSet
BitField Type:
BitField Desc: TxDS3E3AisSet
--------------------------------------*/
#define cThaTxDS3FrameAisMask                      cBit4
#define cThaTxDS3FrameAisShift                     4

/*--------------------------------------
BitField Name: TxDS3E3Md
BitField Type:
BitField Desc: Tx DS3/E3 framing mode
               - 000: DS3 Unframed
               - 001: DS3 C-bit parity carrying 28 DS1s or 21 E1s
               - 010: DS3 M23 carrying 28 DS1s or 21 E1s
               - 011: DS3 C-bit map
               - 100: E3 Unframed
               - 101: E3 G832
               - 110: E3 G.751 carrying 16 E1s
               - 111: Bypass RxM13E13 (DS1/E1 mode)
--------------------------------------*/
#define cThaTxDS3E3MdMask                              cBit3_0
#define cThaTxDS3E3MdShift                             0

#define cThaTxDs3FeacThresMask  cBit24_21
#define cThaTxDs3FeacThresShift 21

#define cThaTxDs3FeacSigMask  cBit15_14
#define cThaTxDs3FeacSigShift 14

#define cThaTxDs3FeacMask  cBit12_7
#define cThaTxDs3FeacShift 7

#define cThaTxE3G832SsmMask  cBit10_7
#define cThaTxE3G832SsmShift 7

#define cThaTxE3G832PldTypeMask  cBit13_11
#define cThaTxE3G832PldTypeShift 11

#define cThaRegTxDs3DlkEnMask  cBit25
#define cThaRegTxDs3DlkEnShift 25

/*----------------------------------------------------------------------------------
Reg Name: DS3 C-Bit SSM transmition
Reg Addr: 0x00080020 - 0x0008003F
Formula: Address +  de3id
Where:  {$de3id(0-26):}
Description: Control to transmit DS3 C-Bit SSM
------------------------------------------------------------------------------------*/
#define cThaRegTxM23Ds3CbitSsmCtrl                  0x780020

#define cThaRegTxDs3CbitSsmEnMask                   cBit7
#define cThaRegTxDs3CbitSsmEnShift                  7
/*-----------------------------------------------------------------------------------
Reg field: DS3SSMInv
  - 1:invert mode , SSM format 11111111_11_0xxxxxx0
  - 1:non_invert mode , SSM format 0xxxxxx0_11_11111111
-------------------------------------------------------------------------------------*/
#define cThaRegTxDs3CbitSsmInvMask                  cBit6
#define cThaRegTxDs3CbitSsmInvShift                 6

#define cThaRegTxDs3CbitSsmMessMask                 cBit5_0
#define cThaRegTxDs3CbitSsmMessShift                0


/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 RX SSM invert control
Reg Addr   : 0x04040B
Reg Formula:
    Where  :
Reg Desc   :

------------------------------------------------------------------------------*/
#define cAf6Reg_rxde3_SSM_Inv_Base                                                               0x74040B

/*------------------------------------------------------------------------------
Reg Name   : RxDS3E3 OH Pro Threshold
Reg Addr   : 0x040401
Reg Formula:
    Where  :
Reg Desc   :

------------------------------------------------------------------------------*/
#define cAf6Reg_rxde3_oh_thrsh_cfg_Base                                                               0x740401
#define cAf6Reg_rxde3_oh_thrsh_cfg                                                                    0x040401
#define cAf6Reg_rxde3_oh_thrsh_cfg_WidthVal                                                                 32
#define cAf6Reg_rxde3_oh_thrsh_cfg_WriteMask                                                               0x0

#define cAf6_rxde3_oh_thrsh_cfg_RxSsm_Mask                                                    cBit27_24
#define cAf6_rxde3_oh_thrsh_cfg_RxSsm_Shift                                                          24

/*--------------------------------------
BitField Name: IdleThresDS3
BitField Type: RW
BitField Desc:
BitField Bits: [23:21]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_Bit_Start                                                      21
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_Bit_End                                                        23
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_Mask                                                    cBit23_21
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_Shift                                                          21
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_MaxVal                                                        0x7
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_MinVal                                                        0x0
#define cAf6_rxde3_oh_thrsh_cfg_IdleThresDS3_RstVal                                                        0x0

/*--------------------------------------
BitField Name: AisThresDS3
BitField Type: RW
BitField Desc:
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_Bit_Start                                                       18
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_Bit_End                                                         20
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_Mask                                                     cBit20_18
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_Shift                                                           18
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_MaxVal                                                         0x7
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_MinVal                                                         0x0
#define cAf6_rxde3_oh_thrsh_cfg_AisThresDS3_RstVal                                                         0x0

/*--------------------------------------
BitField Name: TimingMakerE3G832
BitField Type: RW
BitField Desc:
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_Bit_Start                                                 15
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_Bit_End                                                   17
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_Mask                                               cBit17_15
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_Shift                                                     15
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_MaxVal                                                   0x7
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_MinVal                                                   0x0
#define cAf6_rxde3_oh_thrsh_cfg_TimingMakerE3G832_RstVal                                                   0x0

/*--------------------------------------
BitField Name: NewPayTypeE3G832
BitField Type: RW
BitField Desc:
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_Bit_Start                                                  12
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_Bit_End                                                    14
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_Mask                                                cBit14_12
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_Shift                                                      12
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_MaxVal                                                    0x7
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_MinVal                                                    0x0
#define cAf6_rxde3_oh_thrsh_cfg_NewPayTypeE3G832_RstVal                                                    0x0

/*--------------------------------------
BitField Name: AICThres
BitField Type: RW
BitField Desc:
BitField Bits: [11:9]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_Bit_Start                                                           9
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_Bit_End                                                            11
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_Mask                                                         cBit11_9
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_Shift                                                               9
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_MaxVal                                                            0x7
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_MinVal                                                            0x0
#define cAf6_rxde3_oh_thrsh_cfg_AICThres_RstVal                                                            0x0

/*--------------------------------------
BitField Name: RdiE3G832Thres
BitField Type: RW
BitField Desc:
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_Bit_Start                                                     6
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_Bit_End                                                       8
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_Mask                                                    cBit8_6
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_Shift                                                         6
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_MaxVal                                                      0x7
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_MinVal                                                      0x0
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G832Thres_RstVal                                                      0x0

/*--------------------------------------
BitField Name: RdiE3G751Thres
BitField Type: RW
BitField Desc:
BitField Bits: [5:3]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_Bit_Start                                                     3
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_Bit_End                                                       5
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_Mask                                                    cBit5_3
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_Shift                                                         3
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_MaxVal                                                      0x7
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_MinVal                                                      0x0
#define cAf6_rxde3_oh_thrsh_cfg_RdiE3G751Thres_RstVal                                                      0x0

/*--------------------------------------
BitField Name: RdiDS3Thres
BitField Type: RW
BitField Desc:
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_Bit_Start                                                        0
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_Bit_End                                                          2
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_Mask                                                       cBit2_0
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_Shift                                                            0
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_MaxVal                                                         0x7
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_MinVal                                                         0x0
#define cAf6_rxde3_oh_thrsh_cfg_RdiDS3Thres_RstVal                                                         0x0

/*------------------------------------------------------------------------------
Reg Name: RxM23E23 HW Status
Reg Addr: 0x00740080 - 0x00740097
          The address format for these registers is 0x00780000 + de3id
          Where: de3id: 0-23
Reg Desc: for HW debug only
------------------------------------------------------------------------------*/
#define cThaRegRxM23E23HwStatus                     0x740080

#define cThaRegRxDS3E3CurAICMask                    cBit6
#define cThaRegRxDS3E3CurAICShift                   6

#define cThaRegRxDS3E3CrrAISDownStrMask             cBit5
#define cThaRegRxDS3E3CrrAISDownStrShift            5

/*------------------------------------------------------------------------------
Reg Name: RxDS3E3 OH Pro Control
Reg Addr: 0x740420 - 0x74042B (for 12 lines)
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegRxDs3E3OHProCtrl                     0x740420

#define cThaRegRxE3SSMIntrStatChangeEnMask          cBit2
#define cThaRegRxE3SSMIntrStatChangeEnShift         2

#define cThaRegRxDs3FEACEnMask                      cBit2
#define cThaRegRxDs3FEACEnShift                     2

#define cThaRegRxDs3FbitCntEnMask                   cBit1
#define cThaRegRxDs3FbitCntEnShift                  1

#define cThaRegRxDs3MbitCntEnMask                   cBit0
#define cThaRegRxDs3MbitCntEnShift                  0

#define cThaRegRxDs3E3DlSelMask                     cBit4_3
#define cThaRegRxDs3E3DlSelShift                    3

#define cThaRegRxDs3DlkEnMask                       cBit5
#define cThaRegRxDs3DlkEnShift                      5


/*------------------------------------------------------------------------------
Reg Name: DS3/E3 Rx Framer per Channel Current Status
Reg Addr: 0x00755000-0x0075517B
          The address format for these registers is 0x7405e0 + StsID
          Where: StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Channel interrupt enable of DS3/E3 Rx Framer.
------------------------------------------------------------------------------*/
#define cThaRegDS3E3RxFrmrperChnCurStatCtrl            0x7405E0

#define cThaDE3BerTcaCurStatMask                       cBit15
#define cThaDE3BerSdCurStatMask                        cBit14
#define cThaDE3BerSfCurStatMask                        cBit13

#define cThaDE3CbitPar_TraceCurStatMask                cBit11
#define cThaDE3FeacSSMCurStatMask                      cBit7
#define cThaDE3AicTMCurStatMask                        cBit6
#define cThaDE3IdlePtCurStatMask                       cBit5
#define cThaDE3AisCurStatMask                          cBit4
#define cThaDE3LosCurStatMask                          cBit3
#define cThaDE3AisBitCurStatMask                       cBit2
#define cThaDE3RdiCurStatMask                          cBit1
#define cThaDE3LofCurStatMask                          cBit0

/*------------------------------------------------------------------------------
Reg Name: DS3/E3 Rx Framer per Channel Interrupt Status
Reg Addr: 0x00755000-0x0075517B
          The address format for these registers is 0x7405d0 + StsID
          Where: StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Channel interrupt enable of DS3/E3 Rx Framer.
------------------------------------------------------------------------------*/
#define cThaRegDS3E3RxFrmrperChnIntrStatCtrl       0x7405D0

#define cThaDe3NewSsmIntrStatMask                  cBit16
#define cThaDE3BerTcaIntrStatMask                  cBit15
#define cThaDE3BerSdIntrStatMask                   cBit14
#define cThaDE3BerSfIntrStatMask                   cBit13

#define cThaDE3NewFeacSsmIntrStatMask              cBit7
#define cThaDE3NewAicTmIntrStatMask                cBit6
#define cThaDE3IdlePtNewIntrStatMask               cBit5
#define cThaDE3AisIntrStatMask                     cBit4
#define cThaDE3LosIntrStatMask                     cBit3
#define cThaDE3AisBitIntrStatMask                  cBit2
#define cThaDE3RdiDetIntrStatMask                  cBit1
#define cThaDE3LofIntrStatMask                     cBit0

/*------------------------------------------------------------------------------
Reg Name: DS3/E3 Rx Framer per Channel Interrupt Enable Control
Reg Addr: 0x00755000-0x0075517B
          The address format for these registers is 0x7405c0 + StsID
          Where: StsID: (0-11) STS-1/VC-3 ID
Reg Desc: This is the per Channel interrupt enable of DS3/E3 Rx Framer.
------------------------------------------------------------------------------*/
#define cThaRegDS3E3RxFrmrperChnIntrEnCtrl           0x7405c0

#define cThaDe3NewSsmIntrEnMask                       cBit16
#define cThaDE3BerTcaIntrEnMask                       cBit15
#define cThaDE3BerSdIntrEnMask                        cBit14
#define cThaDE3BerSfIntrEnMask                        cBit13

#define cThaDE3NewFeacIntrEnMask                      cBit7
#define cThaDE3NewAicTmIntrEnMask                     cBit6
#define cThaDE3IdlePtNewIntrEnMask                    cBit5
#define cThaDE3AisIntrEnMask                          cBit4
#define cThaDE3LosIntrEnMask                          cBit3
#define cThaDE3AisBitIntrEnMask                       cBit2
#define cThaDE3RdiDetIntrEnMask                       cBit1
#define cThaDE3LofIntrEnMask                          cBit0

/*
RxM12E12 Control
Address: 0x00741000 - 0x007410FF (for 84x2 lines)
The address format for these registers is 0x00741000 + 8*de3id + de2id
Where: de3id: 0  23
de2id: 0  6
Description: The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation.
#define cThaRegRxM12E12Ctrl(de3, de2)                (0x00741000 + 8*(de3id) + (de2id))
*/
#define cThaRegRxM12E12Ctrl 0x00741000

#define cThaRxDS2E2MdMask   cBit1_0
#define cThaRxDS2E2MdShift  0

#define cThaRegRxM12E12CtrlLofFrcMask                cBit2
#define cThaRegRxM12E12CtrlLofFrcShift                2

#define cThaRegRxM12E12AisFwdDisCtrlMask             cBit3
#define cThaRegRxM12E12AisFwdDisCtrlShift            3

/*
RxM12E12 Framer Channel Interrupt Status
Address: 0x007413FF
Description: This is the Channel interrupt status of DS2/E2 Rx Framer.
*/
#define cThaRegRxM12E12FrmChnIntrStatus              0x7413FF
#define cThaRegRxM12E12FrmChnIntrStatusMask(de3)     (cBit0 << (de3))
#define cThaRegRxM12E12FrmChnIntrStatusShift(de3)    (de3)

/*
RxM12E12 Framer per Channel Interrupt Enable Control
Address: 0x00741380  0x0074139F (0x007405C0)
The address format for these registers is 0x00741380 + de3id
Where: de3id: 0  23
Description: This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.
*/
#define cThaRegRxM12E12FrmChnIntrEnCtrl               0x741380
#define cThaRegRxM12E12FrmChnIntrEnCtrlMask(de2)      (cBit0 << (de2))
#define cThaRegRxM12E12FrmChnIntrEnCtrlShift(de2)     (de2)

/*
RxM12E12 Framer per Channel Interrupt Change Status
Address: 0x007413A0  0x007413BF (0x007405D0)
The address format for these registers is 0x007413A0 + de3id
Where: de3id: 0  23
Description: This is the per Channel interrupt status of DS2/E2 Rx Framer.
*/
#define cThaRegRxM12E12FrmChnIntrChngStatus               0x7413A0
#define cThaRegRxM12E12FrmChnIntrChngStatusMask(de2)      (cBit0 << (de2))
#define cThaRegRxM12E12FrmChnIntrChngStatusShift(de2)     (de2)

/*
RxM12E12 Framer per Channel Current Status
Address: 0x007413C0  0x007413DF (0x007405E0)
The address format for these registers is 0x007413E0 + de3id
Where: de3id: 0  232
Description: This is the per Channel Current status of DS2/E2 Rx Framer.
*/
#define cThaRegRxM12E12FrmChnAlarmCurStatus               0x7413C0
#define cThaRegRxM12E12FrmChnAlarmCurStatusMask(de2)      (cBit0 << (de2))
#define cThaRegRxM12E12FrmChnAlarmCurStatusShift(de2)     (de2)

/*------------------------------------------------------------------------------
Reg Name: RxM23E23 Framer REI Counter
Reg Addr: 0x00740440 - 0x0074045B (for 12x2 channels).
          The address format for these registers is 0x00740440 + 16*Rd2Clr +
          de3id
          Where: Rd2Clr: 0 - 1 Rd2Clr = 0: Read to clear. Otherwise, Read
          Only.
          de3id: 0 - 11
Reg Desc: This is the per channel REI counter for DS3/E3 receive framer
------------------------------------------------------------------------------*/
#define cThaRegRxM23E23FrmrReiCnt(r2c)                 (0x740440UL + (((r2c) ? 0 : 1) * 16UL))
#define cThaRegRxM23E23FrmrReiCntBase                   0x740440UL
/*--------------------------------------
BitField Name: RxM23E23ReiCnt[15:0]
BitField Type: R2C
BitField Desc: DS3/E3 REI error accumulator
BitField Bits: 15_0
--------------------------------------*/
#define cThaRxM23E23ReiCntMask                         cBit15_0

/*------------------------------------------------------------------------------
Reg Name: RxM23E23 Framer FBE Counter
Reg Addr: 0x00740480 - 0x0074049B (for 12x2 channels).
          The address format for these registers is 0x00740480 + 16*Rd2Clr +
          de3id
          Where: Rd2Clr: 0 - 1 Rd2Clr = 0: Read to clear. Otherwise, Read
          Only.
          de3id: 0 - 11
Reg Desc: This is the per channel FBE counter for DS3/E3 receive framer
------------------------------------------------------------------------------*/
#define cThaRegRxM23E23FrmrFBECnt(r2c)                (0x740480UL + (((r2c) ? 0 : 1) * 16UL))
#define cThaRegRxM23E23FrmrFBECntBase                  0x740480UL

/*--------------------------------------
BitField Name: RxM23E23FbeCnt[15:0]
BitField Type: R2C
BitField Desc: DS3/E3 FBE error accumulator
BitField Bits: 15_0
--------------------------------------*/
#define cThaRxM23E23FbeCntMask                         cBit15_0

/*------------------------------------------------------------------------------
Reg Name: RxM23E23 Framer Parity Counter
Reg Addr: 0x007404C0 - 0x007404DB (for 12x2 channels).
          The address format for these registers is 0x007404C0 + 16*Rd2Clr +
          de3id
          Where: Rd2Clr: 0 - 1 Rd2Clr = 0: Read to clear. Otherwise, Read
          Only.
          de3id: 0 - 11
Reg Desc: This is the per channel Parity counter for DS3/E3 receive framer
------------------------------------------------------------------------------*/
#define cThaRegRxM23E23FrmrParCnt(r2c)              (0x7404C0UL + (((r2c) ? 0 : 1) * 16UL))
#define cThaRegRxM23E23FrmrParCntBase                0x7404C0UL

/*--------------------------------------
BitField Name: RxM23E23ParCnt[15:0]
BitField Type: R2C
BitField Desc: DS3/E3 Parity error accumulator
BitField Bits: 15_0
--------------------------------------*/
#define cThaRxM23E23ParCntMask                         cBit15_0

/*------------------------------------------------------------------------------
Reg Name: RxM23E23 Framer Cbit Parity Counter
Reg Addr: 0x00740500 - 0x0074051B (for 12x2 channels).
          The address format for these registers is 0x00740500 + 16*Rd2Clr +
          de3id
          Where: Rd2Clr: 0 - 1 Rd2Clr = 0: Read to clear. Otherwise, Read
          Only.
          de3id: 0 - 11
Reg Desc: This is the per channel Cbit Parity counter for DS3/E3 receive
          framer
------------------------------------------------------------------------------*/
#define cThaRegRxM23E23FrmrCbitParCnt(r2c)          (0x740500UL + (((r2c) ? 0 : 1) * 16UL))
#define cThaRegRxM23E23FrmrCbitParCntBase            0x740500UL

/*--------------------------------------
BitField Name: RxM23E23CParCnt[15:0]
BitField Type: R2C
BitField Desc: DS3/E3 Cbit Parity error accumulator
BitField Bits: 15_0
--------------------------------------*/
#define cThaRxM23E23CParCntMask                        cBit15_0

/*------------------------------------------------------------------------------
Reg Name: TxM23E23 E3g832 Trace Byte
Reg Addr: 0x00780100 - 0x7801BF
          The address format for these registers is 0x00780100 + de3id
Reg Desc: Description:
------------------------------------------------------------------------------*/

/*--------------------------------------
BitField Name:
BitField Type:
BitField Desc:
BitField Bits:
--------------------------------------*/

#define cThaReg_rx_de3_feac_buffer      0x00740560
#define cTha_rx_de3_feac_buffer_StateMask   cBit7_6
#define cTha_rx_de3_feac_buffer_StateShift  6
#define cTha_rx_de3_feac_buffer_6xMask   cBit5_0
#define cTha_rx_de3_feac_buffer_6xShift  0

/*------------------------------------------------------------------------------
Reg Name: RxM23E23 OH per Channel Current Status
Reg Address:  0x00740540 � 0x0074054B
        The address format for these registers is 0x00740540 + de3id
Where:  de3id: 0 � 11
Description: This is the per Channel Current status of DS3/E3 Rx OH.
------------------------------------------------------------------------------*/
#define cThaRegRxM23E23OHCurStatus          0x00740540

/*--------------------------------------
BitField Name:DE3OHCurrSta
BitField Type:
BitField Desc:
        DS3Cbit:
        [4] DS3DLbit: Current Value of DLbit in G751 mode.
        [5] DS3UDLbit: Current Value of UDLbit in G751 mode.
        [6] DS3NAbit: Current Value of NAbit in G751 mode.

        G751:
        [8] G751Nbit: Current Value of Nbit in G751 mode.

        G832:
        [28:21] G832NRByte: Current Value of NR byte in G832 mode.
        [42:35] G832GCByte: Current Value of GC byte in G832 mode.
--------------------------------------*/

#define cThaDE3OHCurrStatusTailMask             cBit31_0
#define cThaDE3OHCurrStatusTailShift            0
#define cThaDE3OHCurrStatusTailDwIndex          0

/* DS3Cbit: */



#define cThaDS3FEACMask   cBit15_10 /*cBit47_42*/
#define cThaDS3FEACShift  10

/* G832 */
#define cThaG832SSMMask    cBit8_5
#define cThaG832SSMShift   5

#define cThaG832TMMask     cBit5
#define cThaG832TMShift    5

#define cThaG832PTMask     cBit14_12
#define cThaG832PTShift    12



/*------------------------------------------------------------------------------
Reg Name: VT Async Map Control
Reg Addr: 0x776800 - 0x77697B (for 336 lines)
          The address format for these registers is 0x776800 + 32*stsid +
          4*vtgid + vtid
          Where: stsid: 0 - 11
          vtgid: 0 - 6
          vtid: 0 - 3
Reg Desc: The VT Async Map Control is use to configure for per channel VT Async
          Map operation.
------------------------------------------------------------------------------*/
#define cThaRegVtAsyncMapCtrl                   0x776800

#define cThaVtAsyncMapJitIDCfgHwHeadMask        cBit0
#define cThaVtAsyncMapJitIDCfgHwHeadShift       0
#define cThaVtAsyncMapJitIDCfgHwTailMask        cBit31_24
#define cThaVtAsyncMapJitIDCfgHwTailShift       24

#define cThaVtAsyncMapJitIDCfgSwHeadMask        cBit8
#define cThaVtAsyncMapJitIDCfgSwHeadShift       8
#define cThaVtAsyncMapJitIDCfgSwTailMask        cBit7_0
#define cThaVtAsyncMapJitIDCfgSwTailShift       0

/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling ID Conversion Control
------------------------------------------------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name: STS/VT Demap CEP VC4 fractional VC3 Control
Reg Addr: 0x007251FF
Reg Desc: The STS/VT Demap CEP VC4 fractional VC3 Control is use to configure
          for per STS1/VC3 Concatenation operation.
------------------------------------------------------------------------------*/
#define cThaRegStsVtDemapCepVc4FractionalVc3 0x007251FF
#define cThaRegStsVtDemapCepVc4FractionalVc3Mask(stsId) (cBit0 << (stsId))
#define cThaRegStsVtDemapCepVc4FractionalVc3Shift(stsId) (stsId)

/*------------------------------------------------------------------------------
Reg Name: STS/VT Demap Concatenation Control
Address : 0x00725000 - 0x0072500B (for 12 STS1/VC3)
          The address format for these registers is 0x00725000 + stsid
          Where: stsid: 0 - 11
Reg Desc: The STS/VT Demap Concatenation Control is use to configure for per STS1/VC3
          Concatenation operation.
------------------------------------------------------------------------------*/
#define cThaRegStsVtDemapConcatControl 0x00725000
#define cThaRegStsVtDemapCcatEnMask cBit4
#define cThaRegStsVtDemapCcatEnShift 4

#define cThaRegStsVTDemapCcatMasterMask cBit3_0
#define cThaRegStsVTDemapCcatMasterShift 0

/*------------------------------------------------------------------------------
The TxM12E12 Control is use to configure for per channel Tx DS2/E2 operation.
RTL Instant Name :  txm12e12_ctrl
Address :  0x00781000 - 0x007810FF
Formula :  0x00781000 + 8*de3id + de2id

Where :
    $de3id(0-11):
    $de2id(0-6):
------------------------------------------------------------------------------*/
#define cThaRegTxM12E12Ctrl 0x00781000

#ifdef __cplusplus
}
#endif
#endif /* THAMODULEPDHFORSTMREG_H_ */

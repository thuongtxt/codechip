/*------------------------------------------------------------------------------
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe1.c
 *
 * Created Date: Sep 5, 2012
 *
 * Description : Thalassa DS1/E1 default implementation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "commacro.h"
#include "ThaModulePdhReg.h"
#include "ThaPdhDe1.h"
#include "AtPdhNxDs0.h"
#include "AtPwCESoP.h"
#include "AtObject.h"

#include "../../../generic/pdh/AtModulePdhInternal.h"
#include "../../../generic/sur/AtModuleSurInternal.h"
#include "../../../generic/sur/AtSurEngineInternal.h"
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../generic/ber/AtBerControllerInternal.h"
#include "../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../../../generic/diag/AtErrorGeneratorInternal.h"
#include "../../../util/AtIteratorInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDeviceInternal.h"
#include "../map/ThaModuleAbstractMap.h"
#include "../map/ThaModuleMapReg.h"
#include "../cdr/ThaModuleCdr.h"
#include "../util/ThaUtil.h"
#include "../sdh/ThaSdhVc.h"
#include "../clock/ThaClockExtractor.h"
#include "../prbs/ThaPrbsEngine.h"
#include "ThaModulePdhInternal.h"
#include "ThaPdhNxDs0.h"
#include "ThaPdhDe1Internal.h"
#include "ThaModulePdhFdlReg.h"
#include "../../include/att/AtAttController.h"
#include "retimingcontrollers/ThaPdhRetimingController.h"
#include "retimingcontrollers/ThaPdhDe1RetimingController.h"
#include "ThaPdhUtil.h"

/*--------------------------- Define -----------------------------------------*/
/* TX_INBAND_LOOPCODE */
#define cHwTxInbandLoopCodeNone            0
#define cHwTxInbandCsuLoopUp               1
#define cHwTxInbandCsuLoopDown             2
#define cHwTxInbandFacility1LoopUp         3
#define cHwTxInbandFacility1LoopDown       4
#define cHwTxInbandFacility2LoopUp         5
#define cHwTxInbandFacility2LoopDown       6

/* RX_INBAND_LOOPCODE */
#define cHwRxInbandCsuLoopUp                 0
#define cHwRxInbandCsuLoopDown               1
#define cHwRxInbandFacility1LoopUp           2
#define cHwRxInbandFacility1LoopDown         3
#define cHwRxInbandFacility2LoopUp           4
#define cHwRxInbandFacility2LoopDown         5
#define cHwRxInbandLoopCodeNone              7

#define cThaPdhMinBomThreshold              0
#define cThaPdhStdBomThreshold              10
#define cThaPdhMaxBomThreshold              15

#define cThaTxFrmrE1SaCfgMask               cThaTxDE1TxSaCfgMask
#define cThaTxFrmrE1SaCfgShift              cThaTxDE1TxSaCfgShift

/*--------------------------- Macros -----------------------------------------*/
#define mDs1E1J1DefaultOffset(self) mMethodsGet((ThaPdhDe1)self)->Ds1E1J1DefaultOffset((ThaPdhDe1)self)

#define mSetIntrBitMask(alarmType_, defectMask_, enableMask_, hwMask_)         \
    do                                                                         \
        {                                                                      \
        if (defectMask_ & alarmType_)                                          \
            regVal = (enableMask_ & alarmType_) ? (regVal | hwMask_) : (regVal & (~hwMask_)); \
        }while(0)

#define mThis(self) ((ThaPdhDe1)self)
#define mPortId(self) (uint8)AtChannelIdGet((AtChannel)self)

#define mDe1RetimingController(self)    \
    (ThaPdhDe1RetimingController)RetimingControllerGet((ThaPdhDe1)self)

#define mFieldMask(channel, field)                                             \
        mMethodsGet(channel)->field##Mask(channel)
#define mFieldShift(channel, field)                                            \
        mMethodsGet(channel)->field##Shift(channel)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhDe1Methods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tAtPdhDe1Methods     m_AtPdhDe1Override;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaPdhDe1, TxFrmrE1SaCfg)

static eAtRet HwIdFactorGet(ThaPdhDe1 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1)
    {
    AtUnused(self);
    AtUnused(phyModule);
    *slice = 0;
    *hwStsInSlice = AtChannelIdGet((AtChannel)self);
    *hwVtg = 0;
    *hwDe1 = 0;
    return cAtOk;
    }

static uint32 MapLineOffset(ThaPdhDe1 self)
    {
    AtUnused(self);
    return 0;
    }

static AtSdhChannel ShortestVcGet(ThaPdhDe1 self)
    {
    AtUnused(self);
    return NULL;
    }

static void Ds0OffsetFactorForMap(ThaPdhDe1 self, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwVt)
    {
    ThaPdhDe1HwIdFactorGet(self, cThaModuleMap, slice, hwStsInSlice, hwVtg, hwVt);
    }

/* Default implementation of register formula */
static uint32 Ds1E1J1DefaultOffset(ThaPdhDe1 self)
    {
    uint8 slice, hwIdInSlice;

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &slice, &hwIdInSlice);
    return hwIdInSlice;
    }

/* This function is to get CRC/REI/FE Error counter Ds1/E1 offset */
static uint32 De1ErrCounterOffset(ThaPdhDe1 de1, eBool r2cEn)
    {
    if (r2cEn == cAtTrue)
        return mMethodsGet(de1)->Ds1E1J1DefaultOffset(de1);
    else
        return (32 + mMethodsGet(de1)->Ds1E1J1DefaultOffset(de1));
    }

static uint32 De1RxFrmrPerChnCurrentAlmRegAddrGet(ThaPdhDe1 de1)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)de1);
    return ThaModulePdhDe1RxFrmrPerChnCurrentAlmRegister(pdhModule);
    }

static uint32 De1RxFrmrPerChnInterruptAlmRegAddrGet(ThaPdhDe1 de1)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)de1);
    return ThaModulePdhDe1RxFrmrPerChnInterruptAlmRegister(pdhModule);
    }

static eAtRet SignalingMultiframeEnable(ThaPdhDe1 self, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrSigingInsCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self , address, cAtModulePdh);

    mFieldIns(&regVal, cThaTxDE1SigMfrmEnMask, cThaTxDE1SigMfrmEnShift, enable ? 1  : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool TxSigCpuMdIsApplicable(ThaPdhDe1 self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 PartOffset(ThaPdhDe1 self)
    {
    return ThaModulePdhPartOffset(AtChannelModuleGet((AtChannel)self), mMethodsGet(self)->PartId(self));
    }

/* This function is used to set the CPU signaling insert global enable bit*/
static eAtRet ThaCfgDs1E1FrmTxSigCpuMdEnSet(ThaPdhDe1 self, eBool  enable)
    {
    uint32 regVal, regAddr;

    /* At the current version, hardware disable this mode */
    if (!TxSigCpuMdIsApplicable(self))
        return cAtOk;

    /* Configure this mode */
    regAddr = cThaRegDS1E1J1TxFrmrSigingCpuInsEnCtrl + PartOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal,
              cThaTxDE1SigCpuMdMask,
              cThaTxDE1SigCpuMdShift,
              mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

/*----------------------------------------------------------------------------*/
static void DeleteAllNxDs0(ThaPdhDe1 self)
    {
    AtObject nxds0;

    /* Delete all objects */
    while ((nxds0 = AtListObjectRemoveAtIndex(self->allNxDs0s, 0)) != NULL)
        AtObjectDelete(nxds0);
    AtObjectDelete((AtObject)(self->allNxDs0s));
    self->allNxDs0s = NULL;

    /* Reset all private data */
    self->timeslotBitmap = 0x0;
    AtPdhDe1Ds0MaskSet((AtPdhDe1)self, 0x0);
    }

static void NxDs0ListDelete(ThaPdhDe1 self)
    {
    DeleteAllNxDs0(self);
    AtObjectDelete((AtObject)(self->allNxDs0s));
    self->allNxDs0s = NULL;
    }

static AtModulePrbs PrbsModule(AtChannel self)
    {
    return (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePrbs);
    }

static AtSurEngine *SurEngineAddress(AtChannel self)
    {
    return &(mThis(self)->surEngine);
    }
static AtObjectAny AttController(AtChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);
    if (mThis(self)->attController == NULL)
        {
        mThis(self)->attController = mMethodsGet(module)->De1AttControllerCreate(module, self);
        if (mThis(self)->attController)
            {
            AtAttControllerSetUp(mThis(self)->attController);
            AtAttControllerInit(mThis(self)->attController);
            }
        }
    return mThis(self)->attController;
    }

static void DeleteAttController(AtObject self)
    {
    if (mThis(self)->attController)
        AtObjectDelete((AtObject)mThis(self)->attController);
    mThis(self)->attController = NULL;
    }

static void Delete(AtObject self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    AtClockExtractor clockExtractor;

    AtModulePrbsEngineObjectDelete(PrbsModule((AtChannel)self), de1->prbsEngine);
    AtObjectDelete((AtObject)(de1->linePrbsEngine));
    de1->prbsEngine = NULL;
    de1->linePrbsEngine = NULL;
    NxDs0ListDelete(de1);
    ThaPdhDe1CdrControllerDelete(de1);

    clockExtractor = ThaPdhDe1ClockExtractorGet(de1);
    if (clockExtractor)
    	ThaClockExtractorSourceReset(clockExtractor);

    /* PRM */
    AtObjectDelete((AtObject)mThis(self)->prmController);
    mThis(self)->prmController = NULL;
    DeleteAttController(self);

    AtObjectDelete((AtObject)mThis(self)->retimingController);
    mThis(self)->retimingController = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static ThaModulePdh PdhModule(AtChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet(self);
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (mThis(self)->prbsEngine == NULL)
        mThis(self)->prbsEngine = ThaModulePdhDe1PrbsEngineCreate(PdhModule(self), (AtPdhDe1)self);
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

/* This function is to set enable/disable DS1/E1 channel */
static eAtRet Enable(AtChannel self, eBool enable)
    {
    uint32  regVal = 0;
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);

    /* Enable Rx framer block */
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1EnMask, cThaRxDE1EnShift, enable);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    /* Enable Tx framer block */
    address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1EnMask, cThaTxDE1EnShift, enable);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

/* This function is to get state of DS1/E1 channel */
static eBool IsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self), cAtModulePdh);
    return (regVal & cThaRxDE1EnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet MapEnable(AtChannel self, ThaModuleAbstractMap mapModule, eBool enable)
    {
    uint8 timeslot_i;
    AtPdhDe1 de1 = (AtPdhDe1)self;
    eAtRet ret = cAtOk;
    uint8 numTimeSlots = ThaPdhDe1NumDs0Timeslots(de1);

    for (timeslot_i = 0; timeslot_i < numTimeSlots; timeslot_i++)
        ret |= ThaModuleAbstractMapDs0Enable(mapModule, de1, timeslot_i, enable);

    return ret;
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return MapEnable(self, mapModule, enable);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return MapEnable(self, mapModule, enable);
    }

static eBool MapIsEnabled(AtChannel self, ThaModuleAbstractMap mapModule)
    {
    uint8 timeslot_i;
    AtPdhDe1 de1 = (AtPdhDe1)self;
    uint8 numTimeSlots = ThaPdhDe1NumDs0Timeslots(de1);

    for (timeslot_i = 0; timeslot_i < numTimeSlots; timeslot_i++)
        {
        if (!ThaModuleAbstractMapDs0IsEnabled(mapModule, de1, timeslot_i))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    return MapIsEnabled(self, mapModule);
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    return MapIsEnabled(self, mapModule);
    }

static ThaCdrController CdrController(AtChannel self)
    {
    return ThaPdhDe1CdrControllerGet((ThaPdhDe1)self);
    }

static eAtRet ClockExtractorUpdate(AtChannel self)
    {
    ThaPdhDe1 de1 = mThis(self);
    AtClockExtractor clockExtractor;

    clockExtractor = ThaPdhDe1ClockExtractorGet(de1);
    if (clockExtractor == NULL)
        return cAtOk;

    if (AtClockExtractorPdhDe1Get(clockExtractor) != (AtPdhDe1)self)
        return cAtOk;

    if (AtClockExtractorPdhDe1LiuClockIsExtracted(clockExtractor))
        return AtClockExtractorPdhDe1LiuClockExtract(clockExtractor, (AtPdhDe1)self);
    else
        return AtClockExtractorPdhDe1ClockExtract(clockExtractor, (AtPdhDe1)self);
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    return ThaCdrControllerTimingModeIsSupported(CdrController(self), timingMode);
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = ThaCdrControllerTimingSet(CdrController(self), timingMode, timingSource);
    if (ret != cAtOk)
        return ret;

    return ClockExtractorUpdate(self);
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
    return ThaCdrControllerTimingModeGet(CdrController(self));
    }

static AtChannel TimingSourceGet(AtChannel self)
    {
    if (self)
        return ThaCdrControllerTimingSourceGet(CdrController(self));

    return NULL;
    }

static eAtClockState ClockStateGet(AtChannel self)
    {
    if (self)
        return ThaCdrControllerClockStateGet(CdrController(self));

    return cAtClockStateUnknown;
    }

static uint8 HwClockStateGet(AtChannel self)
    {
    return ThaCdrControllerHwClockStateGet(CdrController(self));
    }

static uint32 PwTimingRestore(AtChannel self, AtPw pw)
    {
    ThaCdrControllerPwTimingSourceSet(CdrController(self), pw);
    return 0;
    }

static AtPdhPrmController PrmController(AtChannel self)
    {
    return AtPdhDe1PrmControllerGet((AtPdhDe1)self);
    }

static eAtRet DataLinkFcsBitOrderSet(AtPdhChannel self, eAtBitOrder order)
    {
    AtPdhPrmController prmController = PrmController((AtChannel) self);
    if (prmController)
        return AtPdhPrmFcsBitOrderSet(prmController, order);

    return m_AtPdhChannelMethods->DataLinkFcsBitOrderSet(self, order);
    }

static eAtBitOrder DataLinkFcsBitOrderGet(AtPdhChannel self)
    {
    AtPdhPrmController prmController = PrmController((AtChannel) self);
    if (prmController)
        return AtPdhPrmFcsBitOrderGet(prmController);

    return m_AtPdhChannelMethods->DataLinkFcsBitOrderGet(self);
    }

static eBool ShouldReportBerViaBerController(AtChannel self)
    {
    return ThaModulePdhShouldReportBerViaBerController(PdhModule(self));
    }

static AtBerController BerController(AtChannel self)
    {
    AtPdhChannel de1 = (AtPdhChannel)self;
    if (de1->lineBerController)     return de1->lineBerController;
    if (de1->pathBerController)     return de1->pathBerController;
    return NULL;
    }

static uint32 BerDefectGet(AtChannel self, uint32 regVal, uint32 (*DefectGet)(AtBerController))
    {
    uint32 swAlarms;

    if (ShouldReportBerViaBerController(self))
        return DefectGet(BerController(self));

    swAlarms = 0;
    if (regVal & cThaDE1BerTcaIntrEnMask)
        swAlarms |= cAtPdhDe1AlarmBerTca;
    if (regVal & cThaDE1BerSfIntrEnMask)
        swAlarms |= cAtPdhDe1AlarmSfBer;
    if (regVal & cThaDE1BerSdIntrEnMask)
        swAlarms |= cAtPdhDe1AlarmSdBer;

    return swAlarms;
    }

static uint32 AlarmHw2Sw(AtChannel self, uint32 hwIntrBitMap)
    {
    uint32 swIntrBitMap = 0;

    if (hwIntrBitMap & cThaDE1LosIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmLos;
    if (hwIntrBitMap & cThaDE1LofIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmLof;
    if (hwIntrBitMap & cThaDE1SigLofIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmSigLof;
    if (hwIntrBitMap & cThaDE1AisIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmAis;
    if (hwIntrBitMap & cThaDE1RaiIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmRai;
    if (hwIntrBitMap & cThaDE1LomfIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmLomf;
    if (hwIntrBitMap & cThaDE1SigRaiIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmSigRai;
    if (hwIntrBitMap & cThaDE1ReiIntrEnMask)
        swIntrBitMap |= cAtPdhDe1AlarmEbit;
    if (hwIntrBitMap & cThaDE1InbandLoopCodeIntrEnMask)
        swIntrBitMap |= cAtPdhDs1AlarmInbandLoopCodeChange;
    if (hwIntrBitMap & cThaDE1BomSsmIntrEnMask)
        {
        if (AtPdhChannelTypeGet((AtPdhChannel)self) == cAtPdhChannelTypeE1)
            swIntrBitMap |= cAtPdhE1AlarmSsmChange;
        else
            swIntrBitMap |= cAtPdhDs1AlarmBomChange;
        }

    return swIntrBitMap;
    }

static uint32 CorrectAlarm(uint32 realAlarms)
    {
    uint32 alarms = realAlarms;

    if (alarms & cAtPdhDe1AlarmAis)
        alarms = (uint32)((int32)alarms & (~cAtPdhDe1AlarmLof));

    return alarms;
    }

static uint32 FilterOutNotApplicableAlarms(AtPdhDe1 self, uint32 alarms)
    {
    /* Only filter out alarms when signaling is disabled on E1 */
    if (ThaPdhDe1IsE1Signaling(self))
        return alarms;

    return (uint32)((int32)alarms & (~(cAtPdhDe1AlarmSigLof | cAtPdhDe1AlarmSigRai)));
    }

/* This function is to get current alarm bitmap of DS1/E1 channel */
static uint32 HwDefectGet(AtChannel self)
    {
    ThaPdhDe1 de1  = (ThaPdhDe1)self;
    uint32 address = De1RxFrmrPerChnCurrentAlmRegAddrGet(de1) + mDs1E1J1DefaultOffset(de1);
    return mChannelHwRead(self, address, cAtModulePdh);
    }

static uint32 DefectGetAndFilter(AtChannel self, uint32 regVal)
    {
    uint32 alarms  = FilterOutNotApplicableAlarms((AtPdhDe1)self, AlarmHw2Sw(self, regVal));
    alarms |= BerDefectGet(self, regVal, AtBerControllerDefectGet);

    return CorrectAlarm(alarms);
    }

static uint32 De1RxFramerHwStatusRegister(AtPdhChannel self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    return ThaModulePdhDe1RxFramerHwStatusRegister(pdhModule);
    }

static eBool HasParentAlarmForwarding(AtPdhChannel self)
    {
    ThaPdhDe1 de1  = (ThaPdhDe1)self;
    uint32 address = 0;
    uint32 regVal = 0;

    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    if (!AtDeviceAllCoresAreActivated(device))
        return cAtFalse;

    address = De1RxFramerHwStatusRegister(self) + mDs1E1J1DefaultOffset(de1);
    regVal  = mChannelHwRead(self, address, cAtModulePdh);
    return (regVal & cAf6_dej1_rxfrm_hw_stat_RxDS1E1_CrrAISDownStr_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 LineDefectGet(AtChannel self, uint32 regVal)
    {
    AtUnused(self);
    if (regVal & cThaDE1LosIntrEnMask)
        return cAtPdhDe1AlarmLos;
    return 0;
    }

static uint32 PathDefectGet(AtChannel self, uint32 regVal)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);

    if (!ThaModulePdhAlarmForwardingStatusIsSupported(module))
        return DefectGetAndFilter(self, regVal);

    if (AtPdhChannelHasParentAlarmForwarding((AtPdhChannel)self))
        return cAtPdhDe1AlarmAis;

    return DefectGetAndFilter(self, regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regVal = HwDefectGet(self);
    uint32 swAlarms = 0;

    swAlarms |= LineDefectGet(self, regVal);
    swAlarms |= PathDefectGet(self, regVal);

    return swAlarms;
    }

static uint32 DefectHistoryGetAndFilter(AtChannel self, eBool r2c)
    {
    ThaPdhDe1 de1  = (ThaPdhDe1)self;
    uint32 address = De1RxFrmrPerChnInterruptAlmRegAddrGet(de1) + mDs1E1J1DefaultOffset(de1);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    uint32 alarms;
    if (r2c)
        mChannelHwWrite(self, address, regVal, cAtModulePdh);

    alarms  = FilterOutNotApplicableAlarms((AtPdhDe1)self, AlarmHw2Sw(self, regVal));
    alarms |= BerDefectGet(self, regVal, (r2c) ? AtBerControllerDefectHistoryClear : AtBerControllerDefectHistoryGet);

    return alarms;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 curAlarm = DefectHistoryGetAndFilter(self, cAtFalse);
    AtPdhPrmController prmController = PrmController(self);
    ThaCdrController cdrController = CdrController(self);

    if (prmController)
        curAlarm |= AtPdhPrmControllerDefectHistoryGet(prmController);

    if (cdrController)
        curAlarm |= ThaCdrControllerInterruptRead2Clear(cdrController, cAtFalse);

    return curAlarm;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 curAlarm = DefectHistoryGetAndFilter(self, cAtTrue);
    AtPdhPrmController prmController = PrmController(self);
    ThaCdrController cdrController = CdrController(self);

    if (prmController)
        curAlarm |= AtPdhPrmControllerDefectHistoryClear(prmController);

    if (cdrController)
        curAlarm |= ThaCdrControllerInterruptRead2Clear(cdrController, cAtTrue);

    return curAlarm;
    }

static uint32 SpecificDefectInterruptClear(AtChannel self, uint32 defectTypes)
    {
    /* Only support clock state change interrupt for now */
    if (defectTypes & cAtPdhDe1AlarmClockStateChange)
        {
        ThaCdrController cdrController = CdrController(self);
        return ThaCdrControllerInterruptRead2Clear(cdrController, cAtTrue);
        }

    return 0;
    }

static eAtRet HelperInterruptEnable(ThaPdhDe1 self, eBool enable)
    {
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    uint32 address = cThaRegPdhRxFrameChannelIntrEnableCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regValue  = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regValue,
              cThaRegPdhRxFrameChannelIntrEnableMask(channelId),
              cThaRegPdhRxFrameChannelIntrEnableShift(channelId), enable ? 1 : 0);
    mChannelHwWrite(self, address, regValue, cAtModulePdh);
    return cAtOk;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return HelperInterruptEnable((ThaPdhDe1)self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return HelperInterruptEnable((ThaPdhDe1)self, cAtFalse);
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint32 channelId = AtChannelIdGet(self);
    uint32 address = cThaRegPdhRxFrameChannelIntrEnableCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regValue = mChannelHwRead(self, address, cAtModulePdh);
    return (regValue & cThaRegPdhRxFrameChannelIntrEnableMask(channelId)) ? cAtTrue : cAtFalse;
    }

static eBool ShouldReportLofWhenLos(AtChannel self, uint32 regMaskVal)
    {
    if (!ThaModulePdhDe1LiuShouldReportLofWhenLos((ThaModulePdh)AtChannelModuleGet(self)))
        return cAtFalse;

    if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)self)) || ((regMaskVal & cThaDE1LosIntrEnMask) == 0))
        return cAtFalse;

    return cAtTrue;
    }

static eBool ShouldConsiderLofWhenLos(ThaPdhDe1 self)
    {
    /* To avoid context switching on products that do not require this. And
     * products that require this will have their own channels override this
     * method. */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    const uint32 cIgnoredDefectsInUnframed = cThaDE1LofIntrEnMask | cThaDE1RaiIntrEnMask;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    uint32 regIntrAddr = ThaModulePdhDe1RxFrmrPerChnInterruptAlmRegister(pdhModule) + offset;
    uint32 regIntrVal = mChannelHwRead(self, regIntrAddr, cAtModulePdh);
    uint32 regMaskVal = mChannelHwRead(self, ThaModulePdhDS1E1J1RxFrmrperChnIntrEnCtrlRegister(pdhModule) + offset, cAtModulePdh);
    uint32 regCurrVal;
    uint32 events = 0;
    uint32 states = 0;

    /* Filter non-masked interrupt bits. */
    regIntrVal &= regMaskVal;

    /* Get current defect after clearing interrupt to avoid missed event. */
    mChannelHwWrite(self, regIntrAddr, regIntrVal, cAtModulePdh);
    regCurrVal = mChannelHwRead(self, ThaModulePdhDe1RxFrmrPerChnCurrentAlmRegister(pdhModule) + offset, cAtModulePdh);

    /* Filter out not applicable alarms */
    if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        regCurrVal &= ~cIgnoredDefectsInUnframed;

    /* Products that have LIU and it does not use LOS from the codechip. See
     * commit detail to understand why this should be done.
     * Note: the purpose of ShouldConsiderLofWhenLos method is to avoid much
     * switching context for products that do not require this */
    if (mMethodsGet(mThis(self))->ShouldConsiderLofWhenLos(mThis(self)) && ShouldReportLofWhenLos(self, regIntrVal))
        {
        if (regCurrVal & cThaDE1LosIntrEnMask)
            {
            regCurrVal |= cThaDE1LofIntrEnMask;
            regIntrVal |= cThaDE1LofIntrEnMask;
            }
        else if ((regCurrVal & cThaDE1LofIntrEnMask) == 0)
            regIntrVal |= cThaDE1LofIntrEnMask;
        }

    /* LOF */
    if (regIntrVal & cThaDE1LofIntrEnMask)
        {
        events |= cAtPdhDe1AlarmLof;
        if (regCurrVal & cThaDE1LofIntrEnMask)
            states |= cAtPdhDe1AlarmLof;
        }

    /* RAI */
    if (regIntrVal & cThaDE1RaiIntrEnMask)
        {
        events |= cAtPdhDe1AlarmRai;
        if (regCurrVal & cThaDE1RaiIntrEnMask)
            states |= cAtPdhDe1AlarmRai;
        }

    /* AIS */
    if (regIntrVal & cThaDE1AisIntrEnMask)
        {
        events |= cAtPdhDe1AlarmAis;
        if (regCurrVal & cThaDE1AisIntrEnMask)
            states |= cAtPdhDe1AlarmAis;
        }

    /* LOS */
    if (regIntrVal & cThaDE1LosIntrEnMask)
        {
        events |= cAtPdhDe1AlarmLos;
        if (regCurrVal & cThaDE1LosIntrEnMask)
            states |= cAtPdhDe1AlarmLos;
        }

    /* BER-SD */
    if (regIntrVal & cThaDE1BerSdIntrEnMask)
        {
        events |= cAtPdhDe1AlarmSdBer;
        if (regCurrVal & cThaDE1BerSdIntrEnMask)
            states |= cAtPdhDe1AlarmSdBer;
        }

    /* BER-SF */
    if (regIntrVal & cThaDE1BerSfIntrEnMask)
        {
        events |= cAtPdhDe1AlarmSfBer;
        if (regCurrVal & cThaDE1BerSfIntrEnMask)
            states |= cAtPdhDe1AlarmSfBer;
        }

    /* BER-TCA */
    if (regIntrVal & cThaDE1BerTcaIntrEnMask)
        {
        events |= cAtPdhDe1AlarmBerTca;
        if (regCurrVal & cThaDE1BerTcaIntrEnMask)
            states |= cAtPdhDe1AlarmBerTca;
        }

    /* Inband loopcode */
    if (regIntrVal & cThaDE1InbandLoopCodeIntrEnMask)
        events |= cAtPdhDs1AlarmInbandLoopCodeChange;

    /* BOM/SSM */
    if (regIntrVal & cThaDE1BomSsmIntrEnMask)
        {
        if (AtPdhChannelTypeGet((AtPdhChannel)self) == cAtPdhChannelTypeE1)
            events |= cAtPdhE1AlarmSsmChange;
        else
            events |= cAtPdhDs1AlarmBomChange;
        }

    /* LOMF/LMFA */
    if (regIntrVal & cThaDE1LomfIntrEnMask)
        {
        events |= cAtPdhDe1AlarmLomf;
        if (regCurrVal & cThaDE1LomfIntrEnMask)
            states |= cAtPdhDe1AlarmLomf;
        }

    /* Signalling CAS LOF */
    if (regIntrVal & cThaDE1SigLofIntrEnMask)
        {
        events |= cAtPdhDe1AlarmSigLof;
        if (regCurrVal & cThaDE1SigLofIntrEnMask)
            states |= cAtPdhDe1AlarmSigLof;
        }

    /* Signalling CAS RAI */
    if (regIntrVal & cThaDE1SigRaiIntrEnMask)
        {
        events |= cAtPdhDe1AlarmSigRai;
        if (regCurrVal & cThaDE1SigRaiIntrEnMask)
            states |= cAtPdhDe1AlarmSigRai;
        }

    /* E-bit */
    if (regIntrVal & cThaDE1ReiIntrEnMask)
        {
        events |= cAtPdhDe1AlarmEbit;
        if (regCurrVal & cThaDE1ReiIntrEnMask)
            states |= cAtPdhDe1AlarmEbit;
        }

    AtChannelAllAlarmListenersCall(self, events, states);

    return states;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtPdhPrmController prmController = PrmController(self);
    ThaCdrController cdrController = CdrController(self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    uint32 supportedMask = 0;
    ThaModulePdh pdhModule = PdhModule(self);

    if (ThaCdrControllerInterruptMaskIsSupported(cdrController, cAtPdhDe1AlarmClockStateChange))
        supportedMask |= cAtPdhDe1AlarmClockStateChange;

    /* Common masks */
    supportedMask |= cAtPdhDe1AlarmLos;
    supportedMask |= cAtPdhDe1AlarmAis;
    supportedMask |= cAtPdhDe1AlarmLof;
    supportedMask |= cAtPdhDe1AlarmRai;

    supportedMask |= cAtPdhDe1AlarmSdBer;
    supportedMask |= cAtPdhDe1AlarmSfBer;
    supportedMask |= cAtPdhDe1AlarmBerTca;

    /* E1 */
    if (AtPdhDe1FrameTypeIsE1(frameType))
        {
        if (frameType == cAtPdhE1MFCrc)
            {
            supportedMask |= cAtPdhE1AlarmSsmChange;
            supportedMask |= cAtPdhDe1AlarmLomf;
            }

        if (ThaPdhDe1IsE1Signaling((AtPdhDe1)self))
            {
            supportedMask |= cAtPdhDe1AlarmSigLof;
            supportedMask |= cAtPdhDe1AlarmSigRai;
            }
        }
    /* DS1 */
    else
        {
        if (ThaModulePdhInbandLoopcodeIsSupported(pdhModule))
            supportedMask |= cAtPdhDs1AlarmInbandLoopCodeChange;

        if (frameType == cAtPdhDs1FrmEsf)
            supportedMask |= cAtPdhDs1AlarmBomChange;

        if (prmController)
            supportedMask |= cAtPdhDs1AlarmPrmLBBitChange;
        }

    return supportedMask;
    }

static eAtRet BerInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    static const uint32 cDe1BerDefects = cAtPdhDe1AlarmSdBer|cAtPdhDe1AlarmSfBer|cAtPdhDe1AlarmBerTca;
    uint32 regAddr, regVal;

    if ((defectMask & cDe1BerDefects) == 0)
        return cAtOk;

    if (ShouldReportBerViaBerController(self))
        return AtBerControllerInterruptMaskSet(BerController(self), defectMask & cDe1BerDefects, enableMask & cDe1BerDefects);

    regAddr = ThaModulePdhDS1E1J1RxFrmrperChnIntrEnCtrlRegister(PdhModule(self)) + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mSetIntrBitMask(cAtPdhDe1AlarmSdBer, defectMask, enableMask, cThaDE1BerSdIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmSfBer, defectMask, enableMask, cThaDE1BerSfIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmBerTca, defectMask, enableMask, cThaDE1BerTcaIntrEnMask);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtPdhPrmController prmController = PrmController(self);
    ThaCdrController cdrController = CdrController(self);
    uint32 regVal, address;
    eAtRet ret = cAtOk;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    address = ThaModulePdhDS1E1J1RxFrmrperChnIntrEnCtrlRegister(pdhModule) + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, address, cAtModulePdh);

    mSetIntrBitMask(cAtPdhDe1AlarmLos,    defectMask, enableMask, cThaDE1LosIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmLof,    defectMask, enableMask, cThaDE1LofIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmAis,    defectMask, enableMask, cThaDE1AisIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmRai,    defectMask, enableMask, cThaDE1RaiIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmLomf,   defectMask, enableMask, cThaDE1LomfIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmSigLof, defectMask, enableMask, cThaDE1SigLofIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmSigRai, defectMask, enableMask, cThaDE1SigRaiIntrEnMask);
    mSetIntrBitMask(cAtPdhDe1AlarmEbit,   defectMask, enableMask, cThaDE1ReiIntrEnMask);
    mSetIntrBitMask(cAtPdhDs1AlarmInbandLoopCodeChange, defectMask, enableMask, cThaDE1InbandLoopCodeIntrEnMask);
    mSetIntrBitMask((cAtPdhE1AlarmSsmChange | cAtPdhDs1AlarmBomChange), defectMask, enableMask, cThaDE1BomSsmIntrEnMask);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    if (regVal)
        AtChannelInterruptEnable(self);
    else
        AtChannelInterruptDisable(self);

    ret |= BerInterruptMaskSet(self, defectMask, enableMask);

    if (prmController && (defectMask & cAtPdhDs1AlarmPrmLBBitChange))
        ret |= AtPdhPrmControllerInterruptMaskSet(prmController, defectMask, enableMask);

    if (cdrController && (defectMask & cAtPdhDe1AlarmClockStateChange))
        ret |= ThaCdrControllerInterruptMaskSet(cdrController, defectMask, enableMask);

    return ret;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtPdhPrmController prmController = mThis(self)->prmController; /* When CDR controller or PRM controller don't exits */
    ThaCdrController cdrController = mThis(self)->cdrController;   /* it mean the interrupt mask was 0 */
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);
    uint32 regVal = mChannelHwRead(self, ThaModulePdhDS1E1J1RxFrmrperChnIntrEnCtrlRegister(pdhModule) + mDs1E1J1DefaultOffset(self), cAtModulePdh);
    uint32 swIntrMask = AlarmHw2Sw(self, regVal);

    swIntrMask |= BerDefectGet(self, regVal, AtBerControllerInterruptMaskGet);

    if (prmController)
        swIntrMask |= AtPdhPrmControllerInterruptMaskGet(prmController);

    if (cdrController)
        swIntrMask |= ThaCdrControllerInterruptMaskGet(cdrController);

    return swIntrMask;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
	AtPw pw;
	uint32 alarms = cAtPdhDe1AlarmAis;

	if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        return alarms;

	pw = AtChannelBoundPwGet(self);
    if ((AtPwTypeGet(pw) == cAtPwTypeSAToP) && !AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
        return alarms;

    alarms |= cAtPdhDe1AlarmRai;

    return alarms;
    }

static uint32 DwordIndexInLongReg(AtChannel self)
    {
    return AtChannelIdGet(self) / 32;
    }

static uint32 MaskInLongReg(AtChannel self)
    {
    return cBit0 << (AtChannelIdGet(self) % 32);
    }

static uint32 ShiftInLongReg(AtChannel self)
    {
    return (AtChannelIdGet(self) % 32);
    }

static eAtRet HelperTxLiuAisForce(AtChannel self, eBool enable)
    {
    uint32 address, dwordIndex, mask, shift;
    uint32 longReg[cThaNumDwordsInLongReg];

    dwordIndex = DwordIndexInLongReg(self);
    shift = ShiftInLongReg(self);
    mask = MaskInLongReg(self);
    address = cAf6_Reg_Ds1_TxAis_Correction;
    mChannelHwLongRead(self, address, longReg, cThaNumDwordsInLongReg, cAtModulePdh);
    mFieldIns(&longReg[dwordIndex], mask, shift, enable ? 1:0);
    mChannelHwLongWrite(self, address, longReg, cThaNumDwordsInLongReg, cAtModulePdh);

    return cAtOk;
    }

static eBool HelperTxLiuAisIsForced(AtChannel self)
    {
    uint32 address, dwordIndex, mask, shift;
    uint32 longReg[cThaNumDwordsInLongReg];
    uint8 enable;

    dwordIndex = DwordIndexInLongReg(self);
    shift = ShiftInLongReg(self);
    mask = MaskInLongReg(self);
    address = cAf6_Reg_Ds1_TxAis_Correction;
    mChannelHwLongRead(self, address, longReg, cThaNumDwordsInLongReg, cAtModulePdh);
    mFieldGet(longReg[dwordIndex], mask, shift, uint8, &enable);

    return enable == 1 ? cAtTrue : cAtFalse;
    }

static uint32 TxFramerAisMask(AtChannel self)
    {
    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        return cThaTxDE1LineAisCorrectionInsMask;
    return cThaTxDE1LineAisInsMask;
    }

static eAtRet HelperTxFramerAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    /* Set suitable value to Tx register */
    uint32 address;
    uint32 regVal;
    uint32 alarmMask = 0;

    if (alarmType & cAtPdhDe1AlarmAis)
        alarmMask |= TxFramerAisMask(self);

    if (alarmType & cAtPdhDe1AlarmRai)
        alarmMask |= cThaTxDE1FrcYelMask;

    address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModulePdh);

    regVal = enable ? (regVal | alarmMask) : (regVal & (~alarmMask));

    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    /* Check if alarm is supported */
    if ((alarmType & AtChannelTxForcableAlarmsGet(self)) == 0)
        return cAtErrorModeNotSupport;

    if (ThaModulePdhLiuForcedAisIsSupported((ThaModulePdh)AtChannelModuleGet(self))
            && (alarmType & cAtPdhDe1AlarmAis))
        {
        HelperTxLiuAisForce(self, enable);
        alarmType = alarmType & (uint32)(~cAtPdhDe1AlarmAis);
        }

    HelperTxFramerAlarmForce(self, alarmType, enable);

    return cAtOk;
    }

static eAtRet CacheTxAisForcedAlarm(AtChannel self, uint32 alarmType, eBool forced)
    {
    if (alarmType & cAtPdhDe1AlarmAis)
        mThis(self)->txAisExplicitlyForced = forced;
    return cAtOk;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = cAtOk;

    ret |= HelperTxAlarmForce(self, alarmType, cAtTrue);
    ret |= CacheTxAisForcedAlarm(self, alarmType, cAtTrue);

    return ret;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    eAtRet ret = cAtOk;

    ret |= HelperTxAlarmForce(self, alarmType, cAtFalse);
    ret |= CacheTxAisForcedAlarm(self, alarmType, cAtFalse);

    return ret;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 alarmMask = 0;
    uint32 txFramerReg = mChannelHwRead(self, cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self), cAtModulePdh);

    if (txFramerReg & cThaTxDE1FrcYelMask)
        alarmMask |= cAtPdhDe1AlarmRai;

    if (ThaModulePdhLiuForcedAisIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        alarmMask |= HelperTxLiuAisIsForced(self) ? cAtPdhDe1AlarmAis : 0;
    else if (txFramerReg & TxFramerAisMask(self))
        alarmMask |= cAtPdhDe1AlarmAis;

    return alarmMask;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forcibleAlarms = cAtPdhDe1AlarmLof;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    if (ThaModulePdhCanForceDe1Los(pdhModule))
        forcibleAlarms = forcibleAlarms | cAtPdhDe1AlarmLos;

    if (ThaModulePdhAisDownStreamIsSupported(pdhModule))
        forcibleAlarms = forcibleAlarms | cAtPdhDe1AlarmAis;

    return forcibleAlarms;
    }

static eBool CanForceRxAlarms(ThaPdhDe1 self, uint32 alarmType)
    {
    return (alarmType & AtChannelRxForcableAlarmsGet((AtChannel)self)) ? cAtTrue : cAtFalse;
    }

static uint32 InCorrectionAisDownStreamToRegVal(AtChannel self, uint32 regVal, eBool enable)
    {
    eAtPdhLoopbackMode loopMode = AtChannelLoopbackGet(self);
    uint8 payloadAis = 0, lineAis = 0;

    switch (loopMode)
        {
        case cAtPdhLoopbackModeRemotePayload:
            if (enable)
                {
                payloadAis = 1;
                lineAis    = 0;
                }
            break;

        case cAtPdhLoopbackModeRemoteLine:
        case cAtPdhLoopbackModeRelease:
        case cAtPdhLoopbackModeLocalPayload:
        case cAtPdhLoopbackModeLocalLine:
        default:
            if (enable)
                {
                payloadAis  = 0;
                lineAis     = 1;
                }
            break;
        }

    mFieldIns(&regVal, cThaRxDE1FrcAISPldDwnMask, cThaRxDE1FrcAISPldDwnShift, payloadAis);
    mFieldIns(&regVal, cThaRxDE1FrcAISLineDwnMask, cThaRxDE1FrcAISLineDwnShift, lineAis);

    return regVal;
    }

static uint32 CorrectionAisDownStreamToRegVal(AtChannel self, uint32 regVal, eBool enable)
    {
    AtUnused(self);
    mFieldIns(&regVal, cThaRxDE1FrcAISLineDwnMask, cThaRxDE1FrcAISLineDwnShift, 0);

    mFieldIns(&regVal, cThaRxDE1FrcAISPldDwnMask, cThaRxDE1FrcAISPldDwnShift, enable ? 1:0);
    mFieldIns(&regVal, cThaRxDE1FrcAISLbitMask, cThaRxDE1FrcAISLbitShift, enable ? 1:0);

    return regVal;
    }

static uint32 AisDownStreamToRegVal(AtChannel self, uint32 regVal, eBool enable)
    {
    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        return CorrectionAisDownStreamToRegVal(self, regVal, enable);

    return InCorrectionAisDownStreamToRegVal(self, regVal, enable);
    }

static eBool AisDownStream(AtChannel self, uint32 regVal)
    {
    if (ThaModulePdhFramerForcedAisCorrectionIsSupported((ThaModulePdh)AtChannelModuleGet(self)))
        {
        return (regVal & cThaRxDE1FrcAISPldDwnMask) ? cAtTrue : cAtFalse;
        }
    else
        return (regVal & (cThaRxDE1FrcAISPldDwnMask | cThaRxDE1FrcAISLineDwnMask)) ? cAtTrue:cAtFalse;
   }

static eAtRet HelperRxAlarmForce(AtChannel self, uint32 alarmType, eBool enable)
    {
    uint32 regAddr, regVal;

    if (!CanForceRxAlarms(mThis(self), alarmType))
        return cAtErrorModeNotSupport;

    regAddr = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    if (alarmType & cAtPdhDe1AlarmLof)
        mRegFieldSet(regVal, cThaRxDE1FrcLof, enable ? 1 : 0);
    if (alarmType & cAtPdhDe1AlarmLos)
        mRegFieldSet(regVal, cThaRxDE1FrcLos, enable ? 1 : 0);
    if (alarmType & cAtPdhDe1AlarmAis)
        regVal = AisDownStreamToRegVal(self, regVal, enable);

    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

/* This function is to force alarm types in Rx direction
 *                  - LOF                                 */
static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtTrue);
    }

/* This function is to un-force alarm types in Rx direction */
static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtFalse);
    }

/* This function is to get Rx alarm type forced */
static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint32 alarmMask = 0;

    if (regVal & cThaRxDE1FrcLofMask)
        alarmMask |= cAtPdhDe1AlarmLof;
    if (regVal & cThaRxDE1FrcLosMask)
        alarmMask |= cAtPdhDe1AlarmLos;
    if (AisDownStream(self, regVal))
        alarmMask |= cAtPdhDe1AlarmAis;

    return alarmMask;
    }

static eAtRet LiuLineLoopinEnable(AtChannel self, eBool enable)
    {
    uint8 portId  = mPortId(self);
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuLoopInCfg, cAtModulePdh);
    uint32 mask   = cThaRegPDHLiuLoopInCfgMask(portId);
    uint32 shift  = cThaRegPDHLiuLoopInCfgShift(portId);
    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    mChannelHwWrite(self, cThaRegPDHLiuLoopInCfg, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LiuLineLoopinIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuLoopInCfg, cAtModulePdh);
    return (regVal & cThaRegPDHLiuLoopInCfgMask(AtChannelIdGet(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet LiuLineLoopoutEnable(AtChannel self, eBool enable)
    {
    uint8 portId  = mPortId(self);
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuLoopOutCfg, cAtModulePdh);
    uint32 mask   = cThaRegPDHLiuLoopOutCfgMask(portId);
    uint32 shift  = cThaRegPDHLiuLoopOutCfgShift(portId);
    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    mChannelHwWrite(self, cThaRegPDHLiuLoopOutCfg, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LiuLineLoopoutIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuLoopOutCfg, cAtModulePdh);
    return (regVal & cThaRegPDHLiuLoopOutCfgMask(AtChannelIdGet(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet LiuPayloadLoopinEnable(AtChannel self, eBool enable)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuPayloadLoopInCfg, cAtModulePdh);
    uint8 portId  = mPortId(self);
    uint32 mask   = cThaRegPDHLiuPayloadLoopInCfgMask(portId);
    uint32 shift  = cThaRegPDHLiuPayloadLoopInCfgShift(portId);
    mFieldIns(&regVal, mask, shift, (enable ? 1 : 0));
    mChannelHwWrite(self, cThaRegPDHLiuPayloadLoopInCfg, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LiuPayloadLoopinIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuPayloadLoopInCfg, cAtModulePdh);
    return (regVal & cThaRegPDHLiuPayloadLoopInCfgMask(AtChannelIdGet(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet LiuPayloadLoopoutEnable(AtChannel self, eBool enable)
    {
    uint8 portId = mPortId(self);
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuPayloadLoopOutCfg, cAtModulePdh);
    uint32 mask = cThaRegPDHLiuPayloadLoopOutCfgMask(portId);
    uint32 shift = cThaRegPDHLiuPayloadLoopOutCfgShift(portId);
    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    mChannelHwWrite(self, cThaRegPDHLiuPayloadLoopOutCfg, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool LiuPayloadLoopoutIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegPDHLiuPayloadLoopOutCfg, cAtModulePdh);
    return (regVal & cThaRegPDHLiuPayloadLoopOutCfgMask(AtChannelIdGet(self))) ? cAtTrue : cAtFalse;
    }

static eAtRet LiuLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;

    /* Local loopback */
    ret |= LiuLineLoopinEnable(self, (loopbackMode == cAtPdhLoopbackModeLocalLine) ? cAtTrue : cAtFalse);
    ret |= LiuPayloadLoopinEnable(self, (loopbackMode == cAtPdhLoopbackModeLocalPayload) ? cAtTrue : cAtFalse);

    /* Remote loopback */
    ret |= LiuLineLoopoutEnable(self, (loopbackMode == cAtPdhLoopbackModeRemoteLine) ? cAtTrue : cAtFalse);
    ret |= LiuPayloadLoopoutEnable(self, (loopbackMode == cAtPdhLoopbackModeRemotePayload) ? cAtTrue : cAtFalse);

    return ret;
    }

static uint8 LiuLoopbackGet(AtChannel self)
    {
    if (LiuLineLoopinIsEnabled(self))     return cAtPdhLoopbackModeLocalLine;
    if (LiuPayloadLoopinIsEnabled(self))  return cAtPdhLoopbackModeLocalPayload;
    if (LiuLineLoopoutIsEnabled(self))    return cAtPdhLoopbackModeRemoteLine;
    if (LiuPayloadLoopoutIsEnabled(self)) return cAtPdhLoopbackModeRemotePayload;

    return cAtPdhLoopbackModeRelease;
    }

static eAtRet FpgaLineLoopinEnable(AtChannel self, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1LocalLineLoopMask, cThaRxDE1LocalLineLoopShift, (enable ? 1 : 0));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool FpgaLineLoopinIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self), cAtModulePdh);
    return (regVal & cThaRxDE1LocalLineLoopMask) ? cAtTrue : cAtFalse;
    }

static eAtRet FpgaLineLoopoutEnable(AtChannel self, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1RmtLineLoopMask, cThaTxDE1RmtLineLoopShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool FpgaLineLoopoutIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self), cAtModulePdh);
    return (regVal & cThaTxDE1RmtLineLoopMask) ? cAtTrue : cAtFalse;
    }

static eAtRet FpgaPayloadLoopinEnable(AtChannel self, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1LocalPayLoopMask, cThaRxDE1LocalPayLoopShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool FpgaPayloadLoopinIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self), cAtModulePdh);
    return (regVal & cThaRxDE1LocalPayLoopMask) ? cAtTrue : cAtFalse;
    }

static eAtRet FpgaPayloadLoopoutEnable(AtChannel self, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1RmtPayLoopMask, cThaTxDE1RmtPayLoopShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool FpgaPayloadLoopoutIsEnabled(AtChannel self)
    {
    uint32 regVal = mChannelHwRead(self, cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self), cAtModulePdh);
    return (regVal & cThaTxDE1RmtPayLoopMask) ? cAtTrue : cAtFalse;
    }

static eAtRet FpgaLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret = cAtOk;

    /* Local loopback */
    ret |= FpgaLineLoopinEnable(self, (loopbackMode == cAtPdhLoopbackModeLocalLine) ? cAtTrue : cAtFalse);
    ret |= FpgaPayloadLoopinEnable(self, (loopbackMode == cAtPdhLoopbackModeLocalPayload) ? cAtTrue : cAtFalse);

    /* Remote loopback */
    ret |= FpgaLineLoopoutEnable(self, (loopbackMode == cAtPdhLoopbackModeRemoteLine) ? cAtTrue : cAtFalse);
    ret |= FpgaPayloadLoopoutEnable(self, (loopbackMode == cAtPdhLoopbackModeRemotePayload) ? cAtTrue : cAtFalse);

    return ret;
    }

static uint8 FpgaLoopbackGet(AtChannel self)
    {
    /* Check local loopback */
    if (FpgaLineLoopinIsEnabled(self))     return cAtPdhLoopbackModeLocalLine;
    if (FpgaPayloadLoopinIsEnabled(self))  return cAtPdhLoopbackModeLocalPayload;

    /* Check remote loopback */
    if (FpgaLineLoopoutIsEnabled(self))    return cAtPdhLoopbackModeRemoteLine;
    if (FpgaPayloadLoopoutIsEnabled(self)) return cAtPdhLoopbackModeRemotePayload;

    return cAtPdhLoopbackModeRelease;
    }

static eBool HasLiuLoopback(AtChannel self)
    {
    return ThaModulePdhHasLiuLoopback((ThaModulePdh)AtChannelModuleGet(self));
    }

static eAtRet HwLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if (HasLiuLoopback(self))
        return LiuLoopbackSet(self, loopbackMode);

    return FpgaLoopbackSet(self, loopbackMode);
    }

static eBool ShouldNotifyAisClear(AtChannel self)
	{
	/* AIS clear should be notified when framer AIS clear and no sticky change */
    if (((DefectGetAndFilter(self, HwDefectGet(self)) & cAtPdhDe1AlarmAis) == 0) &&
        ((DefectHistoryGetAndFilter(self, cAtFalse) & cAtPdhDe1AlarmAis) == 0))
        return cAtTrue;

    return cAtFalse;
	}

static eAtRet LocalLineLoopbackSet(AtChannel self)
    {
    eAtRet ret;

    ret = HwLoopbackSet(self, cAtPdhLoopbackModeLocalLine);
    if (ShouldNotifyAisClear(self))
        AtChannelAllAlarmListenersCall(self, cAtPdhDe1AlarmAis, 0);

    return ret;
    }

static eBool ShouldNotifyAisRaise(AtChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet(self);

    /* AIS raise should be notified when downstream AIS raise and no sticky change */
    if (!ThaModulePdhAlarmForwardingStatusIsSupported(module))
        return cAtFalse;

    /* Just handle the case that interrupt routine cannot handle */
    if (AtPdhChannelHasAlarmForwarding((AtPdhChannel)self) &&
        ((DefectHistoryGetAndFilter(self, cAtFalse) & cAtPdhDe1AlarmAis) == 0))
        return cAtTrue;

    /* Interrupt routine would be able to handle */
    return cAtFalse;
    }

static eAtRet LocalLineLoopbackRelease(AtChannel self)
    {
    eAtRet ret;

    ret = HwLoopbackSet(self, cAtPdhLoopbackModeRelease);
    if (ShouldNotifyAisRaise(self))
        AtChannelAllAlarmListenersCall(self, cAtPdhDe1AlarmAis, cAtPdhDe1AlarmAis);

    return ret;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret;

    if (loopbackMode == cAtPdhLoopbackModeLocalLine)
        ret = LocalLineLoopbackSet(self);

    if ((loopbackMode == cAtPdhLoopbackModeRelease) &&
        (AtChannelLoopbackGet(self) == cAtPdhLoopbackModeLocalLine))
        ret = LocalLineLoopbackRelease(self);

    else
    ret = HwLoopbackSet(self, loopbackMode);

    if (ret != cAtOk)
        return ret;

    /* Update AIS downstream location which depends on remote loopback or line
     * loopback */
    if (AtChannelRxForcedAlarmGet(self) & cAtPdhDe1AlarmAis)
        (void)AtChannelRxAlarmForce(self, cAtPdhDe1AlarmAis);

    return cAtOk;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (HasLiuLoopback(self))
        return LiuLoopbackGet(self);
    return FpgaLoopbackGet(self);
    }

static uint32 BpvErrorCounterRegister(ThaPdhDe1 self, eBool r2c)
    {
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    uint32 offset = r2c ? channelId : (0x100 + channelId);
    uint32 baseAddress = ThaModulePdhBpvErrorCounterRegister((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    return baseAddress + offset;
    }

static eBool RetimingIsSupported(ThaPdhDe1 self)
    {
    return ThaModulePdhDe1SsmIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static ThaPdhRetimingController RetimingControllerCreate(ThaPdhDe1 self)
    {
    return (ThaPdhRetimingController)ThaPdhDe1RetimingControllerNew((AtPdhChannel)self);
    }

static ThaPdhRetimingController RetimingControllerGet(ThaPdhDe1 self)
    {
    if (self->retimingController)
        return self->retimingController;

    if (ThaPdhDe1RetimingIsSupported(self))
        self->retimingController = mMethodsGet(self)->RetimingControllerCreate(self);

    return self->retimingController;
    }

static uint32 TxCsCounter(ThaPdhDe1 self, eBool r2c)
    {
    return ThaPdhRetimingControllerTxCsCounter(RetimingControllerGet(self), r2c);
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool clear)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    uint32 address, regVal, value;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    switch (counterType)
        {
        case cAtPdhDe1CounterBpvExz:
            return mChannelHwRead(self, BpvErrorCounterRegister(de1, clear), cAtModulePdh);

        case cAtPdhDe1CounterCrc:
            address = ThaModulePdhDS1E1J1RxFrmrCrcErrCntRegister(pdhModule) + mMethodsGet(de1)->De1ErrCounterOffset(de1, clear);
            regVal  = mChannelHwRead(self, address, cAtModulePdh);
            mFieldGet(regVal, cThaDE1CrcErrCntMask, cThaDE1CrcErrCntShift, uint32, &value);
            return value;

        case cAtPdhDe1CounterFe:
            address = ThaModulePdhDS1E1J1RxFrmrFBECntRegister(pdhModule) + mMethodsGet(de1)->De1ErrCounterOffset(de1, clear);
            regVal  = mChannelHwRead(self, address, cAtModulePdh);
            mFieldGet(regVal, cThaDE1FbitErrCntMask, cThaDE1FbitErrCntShift, uint32, &value);
            return value;

        case cAtPdhDe1CounterRei:
            address = ThaModulePdhDS1E1J1RxFrmrReiCntRegister(pdhModule) + mMethodsGet(de1)->De1ErrCounterOffset(de1, clear);
            regVal  = mChannelHwRead(self, address, cAtModulePdh);
            mFieldGet(regVal, cThaDE1ReiCntMask, cThaDE1ReiCntShift, uint32, &value);
            return value;

        case cAtPdhDe1CounterTxCs:
            if (AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
                return TxCsCounter((ThaPdhDe1)self, clear);
            return 0;

        case cAtPdhDe1CounterRxCs:
            return ThaPdhDe1RxSlipBuferCounter((AtPdhChannel)self, clear);
        default:
            return 0;
        }
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

/* This function is to set all configurations of a DS1/E1 channel */
static eAtRet AllConfigSet(AtChannel self, void *pAllConfig)
    {
	AtUnused(pAllConfig);
	AtUnused(self);
    /* Don't use */
    return cAtErrorModeNotSupport;
    }

/* This function is to get all configurations of a DS1/E1 channel */
static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
	AtUnused(pAllConfig);
	AtUnused(self);
    /* Don't use */
    return cAtErrorModeNotSupport;
    }

static eAtRet Ds0SignalingDisable(ThaPdhDe1 self, uint32 mask)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrSigingInsCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self , address, cAtModulePdh);
    mFieldIns(&regVal, cThaTXDE1SigBypassMask, cThaTXDE1SigBypassShift, mask);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet AllDs0SignalingDisable(ThaPdhDe1 self)
    {
    return Ds0SignalingDisable(self, cThaTXDE1SigBypassMaxVal);
    }

static eAtRet DisableSignalingInUnframeMode(ThaPdhDe1 self)
    {
    ThaPdhDe1 de1 = mThis(self);
    eAtRet ret = cAtOk;

    ret |= AllDs0SignalingDisable(de1);
    ret |= SignalingMultiframeEnable(de1, cAtFalse);
    ret |= ThaCfgDs1E1FrmTxSigCpuMdEnSet(de1, cAtFalse);

    /* Update software database */
    self->signalingEnabled = cAtFalse;

    return ret;
    }

static eAtRet OnePwRestore(AtPw pw, AtChannel circuit, uint32 boundChannelsEnMask)
    {
    eBool pwIsEnabled = (boundChannelsEnMask & cBit0) ? cAtTrue : cAtFalse;
    eAtRet ret = AtPwCircuitBind(pw, circuit);

    /* Re-enable pw if it is enabled before */
    ret |= AtChannelEnable((AtChannel)pw, pwIsEnabled);
    return ret;
    }

static eAtRet OnePrbsEngineRestore(AtChannel prbsChannel, uint32 prbsEnableMask)
    {
    eBool prbsEnabled = (prbsEnableMask & cBit0) ? cAtTrue : cAtFalse;
    AtPrbsEngine prbsEngine = AtChannelPrbsEngineGet(prbsChannel);

    /* Not all products support PRBS feature */
    return prbsEngine ? AtPrbsEngineEnable(prbsEngine, prbsEnabled) : cAtOk;
    }

/* This function is to restore all Nxds0 belong to a DE1 when it framing has been changed */
static eAtRet RestoreNxDs0BoundPws(AtPdhChannel self, tDe1Backup *backup)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    AtPdhNxDS0 nxds0;
    AtChannel *pws = backup->nxDs0BoundChannels;
    uint32 *masks  = backup->masks;

    /* Have a list to store NxDS0s */
    if (de1->allNxDs0s == NULL)
        {
        de1->allNxDs0s = AtListCreate(cThaMaxE1TimeslotNum);
        if (de1->allNxDs0s == NULL)
            return cAtErrorRsrcNoAvail;
        }

    /* Restore all timeslots */
    for (i = 0; i < backup->numNxDs0s; i++)
        {
        /* If E1 is signaling, nxds0 which contain timeslot 16 will NOT be re-created */
        if (ThaPdhDe1IsE1Signaling((AtPdhDe1)self) && (masks[i] & cBit16))
            continue;

        /* Restore and rebind encap channel */
        nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1)self, masks[i]);
        if (nxds0 == NULL)
            nxds0 = AtPdhDe1NxDs0Create((AtPdhDe1)self, masks[i]);

        if((nxds0 != NULL) && (pws[i] != NULL))
            {
            ret |= OnePwRestore((AtPw)pws[i], (AtChannel)nxds0, (backup->enabledPwsMask >> i) & cBit0);
            ret |= OnePrbsEngineRestore((AtChannel)nxds0, (backup->enabledPrbsMask >> i) & cBit0);
            }
        }

    return ret;
    }

static uint8 ByPassedFrameType(AtPdhDe1 self)
    {
    return AtPdhDe1IsE1(self) ? 8 : 0;
    }

static ThaModuleAbstractMap MapModule(AtPdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap);
    }

static ThaModuleAbstractMap DemapModule(AtPdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap);
    }

static eAtPdhDe1FrameType UnframedMode(AtPdhChannel self)
    {
    eAtPdhDe1FrameType frameMode = AtPdhChannelFrameTypeGet(self);

    switch (frameMode)
        {
        case cAtPdhDs1J1UnFrm: return cAtPdhDs1J1UnFrm;
        case cAtPdhDs1FrmSf  : return cAtPdhDs1J1UnFrm;
        case cAtPdhDs1FrmEsf : return cAtPdhDs1J1UnFrm;
        case cAtPdhDs1FrmDDS : return cAtPdhDs1J1UnFrm;
        case cAtPdhDs1FrmSLC : return cAtPdhDs1J1UnFrm;
        case cAtPdhJ1FrmSf   : return cAtPdhDs1J1UnFrm;
        case cAtPdhJ1FrmEsf  : return cAtPdhDs1J1UnFrm;
        case cAtPdhE1UnFrm   : return cAtPdhE1UnFrm;
        case cAtPdhE1Frm     : return cAtPdhE1UnFrm;
        case cAtPdhE1MFCrc   : return cAtPdhE1UnFrm;
        case cAtPdhDe1FrameUnknown:
        default:
            return cAtPdhDe1FrameUnknown;
        }
    }

static eAtRet TxFramerBypass(ThaPdhDe1 self, eBool bypass)
    {
    uint32 regAddr, regVal;
    uint32 rxFrameType, newFrameType;

    regAddr = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    rxFrameType = mRegField(regVal, cThaRxDE1Md);
    newFrameType = bypass ? ByPassedFrameType((AtPdhDe1)self) : rxFrameType;

    regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    if (newFrameType == mRegField(regVal, cThaTxDE1Md)) /* To save HW access */
        return cAtOk;

    mFieldIns(&regVal, cThaTxDE1MdMask, cThaTxDE1MdShift, newFrameType);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet MapBypass(ThaPdhDe1 self, eBool bypass)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    uint16 mapFrameType;

    if (bypass)
        mapFrameType = UnframedMode(pdhChannel);
    else
        mapFrameType = AtPdhChannelFrameTypeGet(pdhChannel);

    return ThaModuleAbstractMapDe1FrmModeSet(MapModule(pdhChannel), (AtPdhDe1)self, mapFrameType);
    }

static eAtRet RxFramerMonitorOnlyEnable(ThaPdhDe1 self, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1MonOnlyMask, cThaRxDE1MonOnlyShift, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static eBool ShouldEnableMonOnlyWhenBindingToSatop(ThaPdhDe1 self, AtPw pseudowire)
    {
    if (pseudowire == NULL)
        return cAtFalse;

    if (AtChannelPrbsEngineGet((AtChannel)self))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet HandleSignalingWhenBindToPwSatop(AtPdhChannel self, AtPw pseudowire)
    {
    AtPdhDe1 de1 = (AtPdhDe1)self;
    eAtRet ret = cAtOk;

    if (!ThaModulePdhNeedCarryTimeslot16OnPw((ThaModulePdh)AtChannelModuleGet((AtChannel)self)))
        return cAtOk;

    /* Bind */
    if (pseudowire)
        {
        if (AtPdhDe1SignalingIsEnabled(de1))
            ThaPdhDe1SignalingTransparent((ThaPdhDe1)de1);
        return cAtOk;
        }

    /* Unbind: Reconfigure E1 signaling to remove all of work around point */
    ret |= AtPdhDe1SignalingDs0MaskSet(de1, AtPdhDe1SignalingDs0MaskGet(de1));
    ret |= AtPdhDe1SignalingEnable(de1, AtPdhDe1SignalingIsEnabled(de1));
    return ret;
    }

static eAtRet De1FramingBindToPwSatop(AtPdhChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    eBool shouldBypass = ThaPdhChannelFramingShouldBypassOnSatopBinding(self, pseudowire);

    ret |= RxFramerMonitorOnlyEnable(de1, ShouldEnableMonOnlyWhenBindingToSatop(de1, pseudowire));
    ret |= TxFramerBypass(de1, shouldBypass);
    ret |= MapBypass(de1, shouldBypass);
    ret |= HandleSignalingWhenBindToPwSatop(self, pseudowire);

    return ret;
    }

static eAtRet FrameSatopApply(AtPdhChannel self, uint16 frameType)
    {
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);
    AtPw adapter = (AtPw)ThaPwAdapterGet(pw);

    /* If framed SAToP is not being used, just return */
    if ((adapter == NULL) || AtPdhDe1IsUnframeMode(frameType))
        return cAtOk;

    return De1FramingBindToPwSatop(self, adapter);
    }

static eAtRet AutoTxAisEnable(AtPdhDe1 self, eBool enable)
    {
    uint32 regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1AutoAisMask, cThaTxDE1AutoAisShift, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool AutoTxAisIsEnabled(AtPdhDe1 self)
    {
    uint32 regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    return mBinToBool(mRegField(regVal, cThaTxDE1AutoAis));
    }

static eBool ShouldEnableFastFramingSearch(uint16 frameType)
    {
    if ((frameType == cAtPdhDs1FrmSf)  ||
        (frameType == cAtPdhJ1FrmSf)   ||
        (frameType == cAtPdhDs1FrmEsf) ||
        (frameType == cAtPdhJ1FrmEsf))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet FastFrameSearchEnable(ThaPdhDe1 self, eBool enable)
    {
    uint32 regAddr, regVal;

    if (!ThaModulePdhDe1FastSearchIsSupported(PdhModule((AtChannel)self)))
        return (enable) ? cAtErrorModeNotSupport : cAtOk;

    if (!ShouldEnableFastFramingSearch(AtPdhChannelFrameTypeGet((AtPdhChannel)self)) && enable)
        return cAtErrorModeNotSupport;

    regAddr = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cThaRxDs1J1FastSrchDis, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool FastFrameSearchIsEnabled(ThaPdhDe1 self)
    {
    uint32 regAddr, regVal;

    if (!ThaModulePdhDe1FastSearchIsSupported(PdhModule((AtChannel)self)))
        return cAtFalse;

    regAddr = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    if (mRegField(regVal, cThaRxDs1J1FastSrchDis))
        return cAtFalse;

    return cAtTrue;
    }

static eBool AutoTxAisEnabledByDefault(ThaPdhDe1 self, AtPw pw)
    {
    uint32 fullTsBitMap = cBit23_0;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);

    /* Application specific */
    if (!ThaModulePdhTxDe1AutoAisSwAutoControl(pdhModule))
        return ThaModulePdhTxDe1AutoAisByDefault(pdhModule);

    /* SW auto control */
    if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
        return cAtTrue;

    if (AtPdhDe1IsE1((AtPdhDe1)self))
        fullTsBitMap = (AtPwCESoPModeGet((AtPwCESoP)pw) == cAtPwCESoPModeWithCas) ? cBit31_1 & (~cBit16) : cBit31_1;
    return AtPdhDe1NxDs0Get((AtPdhDe1)self, fullTsBitMap) ? cAtTrue : cAtFalse;
    }

static uint8 FrameModeSw2Hw(eAtPdhDe1FrameType swFrameMode)
    {
    static uint8 cInvalidValue = 11;

    switch (swFrameMode)
        {
        case cAtPdhDs1J1UnFrm: return 0;
        case cAtPdhDs1FrmSf  : return 1;
        case cAtPdhDs1FrmEsf : return 2;
        case cAtPdhDs1FrmDDS : return 3;
        case cAtPdhDs1FrmSLC : return 4;
        case cAtPdhJ1FrmSf   : return 5;
        case cAtPdhJ1FrmEsf  : return 6;
        case cAtPdhE1UnFrm   : return 8;
        case cAtPdhE1Frm     : return 9;
        case cAtPdhE1MFCrc   : return 10;
        case cAtPdhDe1FrameUnknown: return cInvalidValue;

        /* Invalid */
        default:
            return cInvalidValue;
        }
    }

static eAtRet HwFrameTypeAtModuleMapSet(ThaPdhDe1 self, uint16 frameType)
    {
    eAtRet mapError = cAtOk, demapError = cAtOk;
    AtDevice device = AtChannelDeviceGet((AtChannel)self);

    mapError = ThaModuleAbstractMapDe1FrmModeSet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), (AtPdhDe1)self, frameType);
    if (mapError != cAtOk)
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "error code at MAP = %d\r\n", mapError);

    demapError = ThaModuleAbstractMapDe1FrmModeSet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe1)self, frameType);
    if (demapError != cAtOk)
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, __FILE__, __LINE__, "error code at DEMAP = %d\r\n", demapError);

    return (mapError | demapError);
    }

static eAtRet ShouldEnableAutoTxAis(ThaPdhDe1 self)
    {
    AtIterator nxds0Iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)self);
    AtChannel nxDs0;
    eBool ret = cAtTrue;

    while ((nxDs0 = (AtChannel)AtIteratorNext(nxds0Iterator)) != NULL)
        {
        AtPw pw = AtChannelBoundPwGet(nxDs0);
        if (!ThaPdhDe1AutoTxAisEnabledByDefault(self, pw))
            {
            ret = cAtFalse;
            break;
            }
        }

    AtObjectDelete((AtObject)nxds0Iterator);
    return ret;
    }

static eAtRet HwFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    uint32 address, regVal;
    eAtRet ret = cAtOk;
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    uint8 hwFrameType = FrameModeSw2Hw((eAtPdhDe1FrameType)frameType);

    /* Configure the MAP/DEMAP */
    ret = mMethodsGet(de1)->HwFrameTypeAtModuleMapSet(de1, frameType);

    /* Set framing information to 2 registers Tx and Rx */
    /* Rx */
    address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1MdMask, cThaRxDE1MdShift, hwFrameType);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    /* Tx */
    address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1MdMask, cThaTxDE1MdShift, hwFrameType);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    /* Set framer for Slip buffer */
    if (ThaPdhDe1RetimingIsSupported(de1))
        ThaPdhRetimingControllerSlipBufferTxHwSsmFrameTypeUpdate(RetimingControllerGet(de1), frameType);
    if (AtPdhChannelRxSlipBufferIsSupported(self))
        ThaPdhDe1HwRxSlipBufferFrameTypeSet(self, frameType);

    AtPdhDe1AutoTxAisEnable((AtPdhDe1)de1, ThaPdhDe1AutoTxAisEnabledByDefault(de1, NULL));

    return ret;
    }

static uint8 BackupNxDs0BoundPws(AtPdhDe1 self, tDe1Backup* backup)
    {
    uint32 numNxDs0s = 0;
    AtIterator nxDs0Iterator;
    AtPdhNxDS0 nxDs0;

    nxDs0Iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)self);
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(nxDs0Iterator)) != NULL)
        {
        AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel)nxDs0);
        uint32 prbsEnabled = 0;

        if (engine)
            prbsEnabled = AtPrbsEngineIsEnabled(engine) ? 1 : 0;

        backup->masks[numNxDs0s] = AtPdhNxDS0BitmapGet(nxDs0);

        /* Need backup and disable PRBS NxDs0 engine */
        backup->enabledPrbsMask |= (prbsEnabled << numNxDs0s);
        if (engine)
            AtPrbsEngineEnable(engine, cAtFalse);

        backup->nxDs0BoundChannels[numNxDs0s] = (AtChannel)AtChannelBoundPwGet((AtChannel)nxDs0);
        if (backup->nxDs0BoundChannels[numNxDs0s])
            {
            uint32 pwEnabled = AtChannelIsEnabled((AtChannel)(backup->nxDs0BoundChannels[numNxDs0s])) ? 1 : 0;
            backup->enabledPwsMask |= (pwEnabled << numNxDs0s);
            AtPwCircuitUnbind((AtPw)(backup->nxDs0BoundChannels[numNxDs0s]));
            }

        numNxDs0s = numNxDs0s + 1;
        }

    AtObjectDelete((AtObject)nxDs0Iterator);

    return (uint8)numNxDs0s;
    }

static eBool FrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
	AtUnused(self);
    return (mOutOfRange(frameType, cAtPdhDs1J1UnFrm, cAtPdhE1MFCrc)) ? cAtFalse : cAtTrue;
    }

static tDe1Backup* EncapBackup(AtPdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtEncapBinder binder = AtDeviceEncapBinder(device);
    if (binder)
        return AtEncapBinderDe1EncapBackup(binder, self);
    return NULL;
    }

static eAtRet EncapRestore(AtPdhChannel self, tDe1Backup *backup)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtEncapBinder binder = AtDeviceEncapBinder(device);
    if (binder)
        return AtEncapBinderDe1EncapRestore(binder, self, backup);
    return cAtOk;
    }

static AtPw UnbindPw(AtPdhDe1 self, uint32 *boundChannelsEnMask)
    {
    AtPw pw = AtChannelBoundPwGet((AtChannel)self);
    if (pw)
        {
        uint8 pwEnabled = AtChannelIsEnabled((AtChannel)pw) ? 1 : 0;
        *boundChannelsEnMask = pwEnabled;
        AtPwCircuitUnbind(pw);
        }

    return pw;
    }

static tDe1Backup* PwsBackup(AtPdhChannel self)
    {
    tDe1Backup* backup = AtOsalMemAlloc(sizeof(tDe1Backup));

    if (backup == NULL)
        return NULL;
    AtOsalMemInit(backup, 0, sizeof(tDe1Backup));

    /* Backup encap channel */
    backup->de1BoundChannel = (AtChannel)UnbindPw((AtPdhDe1)self, &(backup->enabledPwsMask));

    /* Backup timeslots */
    backup->numNxDs0s = BackupNxDs0BoundPws((AtPdhDe1)self, backup);

    return backup;
    }

static eAtRet PwsRestore(AtPdhChannel self, tDe1Backup *backup)
    {
    eAtRet ret = cAtOk;

    if (backup == NULL)
        return cAtError;

    /* Restore bound encap channel */
    if (backup->de1BoundChannel)
        ret |= OnePwRestore((AtPw)(backup->de1BoundChannel), (AtChannel)self, backup->enabledPwsMask);

    /* Restore all NxDS0 when framing mode is changed, in case not un-framed mode */
    if (!AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet(self)))
        ret |= RestoreNxDs0BoundPws(self, backup);

    AtOsalMemFree(backup);
    return ret;
    }

static AtList Backup(AtPdhChannel self)
    {
    AtList backupList = AtListCreate(2);
    if(backupList == NULL)
        return NULL;

    AtListObjectAdd(backupList, (AtObject)EncapBackup(self));
    AtListObjectAdd(backupList, (AtObject)PwsBackup(self));

    /* Delete all NxDs0 */
    NxDs0ListDelete((ThaPdhDe1)self);

    return backupList;
    }

static eAtRet Restore(AtPdhChannel self, AtList backupList)
    {
    eAtRet ret = cAtOk;

    if(backupList == NULL)
        return cAtOk;

    ret |= EncapRestore(self, (tDe1Backup *)((void *)AtListObjectGet(backupList, 0)));
    ret |= PwsRestore(self, (tDe1Backup *)((void *)AtListObjectGet(backupList, 1)));

    return ret;
    }

static eAtRet NotApplicableAlarmsNotify(AtPdhChannel self, uint16 newFrameType)
    {
    /* Only change to unframe will have not applicable alarms */
    if (!AtPdhDe1IsUnframeMode(newFrameType))
        return cAtOk;

    /* And only if previous frame type is framed */
    if (AtPdhDe1IsUnframeMode(mThis(self)->frameType))
        return cAtOk;

    AtChannelAllAlarmListenersCall((AtChannel)self, (cAtPdhDe1AlarmLof | cAtPdhDe1AlarmRai), 0);

    return cAtOk;
    }

static eAtRet HandleAisAllOneDetection(AtPdhChannel self, ThaModulePdh pdhModule, eBool isUnframed)
    {
    /* Just because the current logic already handle this */
    if (ThaModulePdhShouldDetectAisAllOnesWithUnframedMode(pdhModule))
        return cAtOk;

    /* And only explicitly touch this attribute for products require this */
    return AtPdhChannelAisAllOnesDetectionEnable(self, isUnframed ? cAtFalse : cAtTrue);
    }

static eAtRet PrmDefaultSet(AtPdhDe1 self)
    {
    AtPdhPrmController controller = AtPdhDe1PrmControllerGet(self);
    if (controller)
        AtPdhPrmControllerInit(controller);

    return cAtOk;
    }

static eBool ShouldPreserveService(ThaPdhDe1 self)
    {
    /* Keeping services when changing framing type is causing much effort, this
     * method is to give sub classes chance to disable this logic. When this is
     * disabled, application must de-provision all of services before changing
     * framing type */
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TcaNotificationMaskUpdate(AtPdhChannel self, uint16 frameType)
    {
    AtSurEngine surEngine = AtChannelSurEngineGet((AtChannel)self);
    uint32 tcaMask, farEndParams;

    if (surEngine == NULL)
        return cAtOk;

    if (AtPdhDe1HasFarEndPerformance((AtPdhDe1)self, frameType))
        return cAtOk;

    if (!AtSurEngineTcaIsSupported(surEngine))
        return cAtOk;

    tcaMask = AtSurEngineTcaNotificationMaskGet(surEngine);
    farEndParams = AtSurEngineDe1FarEndParams();
    if (tcaMask & farEndParams)
        {
        AtModule sur = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleSur);
        if (AtModuleSurTcaIsSupported((AtModuleSur)sur))
            return AtSurEngineTcaNotificationMaskSet(surEngine, farEndParams, 0);
        }

    return cAtOk;
    }

static eBool TimeslotsBusyOnChangingFrameType(AtIterator iterator, uint16 currentFrameType, uint16 newFrameType)
    {
    AtPdhNxDS0 nxDs0;

    /* If change from E1 frame to DS1 frame, need to check
     * if there is any current NxDs0 has bit mask over bit23 */
    if ((AtPdhDe1FrameTypeIsE1(currentFrameType) == cAtTrue) && (AtPdhDe1FrameTypeIsE1(newFrameType) == cAtFalse))
        {
        while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(iterator)) != NULL)
            {
            if (AtPdhNxDS0BitmapGet(nxDs0) & cBit31_24)
                return cAtTrue;
            }

        return cAtFalse;
        }

    /* If change from DS1 frame to E1 frame, need to check
     * if there is any current NxDs0 has bit0 */
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(iterator)) != NULL)
        {
        if (AtPdhNxDS0BitmapGet(nxDs0) & cBit0)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool CanChangeToFrameType(AtPdhChannel self, uint16 frameType)
    {
    eAtPdhDe1FrameType currentFrameType = mThis(self)->frameType;
    AtIterator iterator;
    eBool timeslotBusy;

    if (mBoolToBin(AtPdhDe1FrameTypeIsE1(currentFrameType)) == mBoolToBin(AtPdhDe1FrameTypeIsE1(frameType)))
        return cAtTrue;

    iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)self);
    timeslotBusy = TimeslotsBusyOnChangingFrameType(iterator, currentFrameType, frameType);
    AtObjectDelete((AtObject)iterator);

    return timeslotBusy ? cAtFalse : cAtTrue;
    }

static eBool CanChangeToUnframedType(AtPdhChannel self)
    {
    AtPdhNxDS0 nxDs0;
    AtIterator iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)self);

    /* Do not let user change frame-type if this DE1
     * contains NxDs0 binding to PW */
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(iterator)) != NULL)
        {
        if (AtChannelBoundPwGet((AtChannel)nxDs0))
            {
            AtObjectDelete((AtObject)iterator);
            return cAtFalse;
            }
        }

    /* If this DE1 does not contain any NxDs0 or
     * its NxDs0s do not bind to any PW, just
     * simply remove its NxDs0s to change frame-type then */
    NxDs0ListDelete((ThaPdhDe1)self);
    AtObjectDelete((AtObject)iterator);
    return cAtTrue;
    }

static eBool CanChangeFrameType(AtPdhChannel self, uint16 frameType)
    {
    eAtPdhDe1FrameType currentFrameType = mThis(self)->frameType;
    eBool isUnframed = AtPdhDe1IsUnframeMode(frameType);

    if (currentFrameType == cAtPdhDe1FrameUnknown)
        return cAtTrue;

    /* When it is mapped to VC, cannot change between DS1 and E1 */
    if (AtPdhChannelVcGet(self) &&
        (mBoolToBin(AtPdhDe1FrameTypeIsE1(currentFrameType)) != mBoolToBin(AtPdhDe1FrameTypeIsE1(frameType))))
        return cAtFalse;

    if (isUnframed)
        return CanChangeToUnframedType(self);

    return CanChangeToFrameType(self, frameType);
    }

static eAtRet  BeforeChangeFrameTypeCheck(AtPdhChannel self, uint16 frameType)
    {
    /* Should compare with frame type cache in object because HW reset value may
     * coincide with a frame type */
    if (AtChannelShouldPreventReconfigure((AtChannel)self) && (mThis(self)->frameType == frameType))
        return cAtOk;

    /* Check frameType whether is invalid */
    if (!mMethodsGet(self)->FrameTypeIsSupported(self, frameType))
        return cAtErrorModeNotSupport;

    /* If services preserving is not required, enforce application remove all
     * of services before changing framing type */
    if (!mMethodsGet(mThis(self))->ShouldPreserveService(mThis(self)))
        {
        /* Super logic will check bound PW, but incase of frame satop, it is
         * allowed to hot change framing */
        if (AtChannelServiceIsRunning((AtChannel)self))
            return AtChannelBoundPwGet((AtChannel)self) ? cAtRetMoveNext : cAtErrorResourceBusy;
        }

    /* Check frame type can be changed under current dependencies */
    if (CanChangeFrameType(self, frameType) == cAtFalse)
        return cAtErrorModeNotSupport;

    return cAtRetMoveNext;
    }

static eAtRet ThresholdSet(AtChannel self, const tThaCfgPdhDs1E1FrmRxThres *pThres)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);

    mFieldIns(&regVal, cThaRxDE1RaiCntThresMask, cThaRxDE1RaiCntThresShift, pThres->raiThres);
    mFieldIns(&regVal, cThaRxDE1AisCntThresMask, cThaRxDE1AisCntThresShift, pThres->aisThres);
    mFieldIns(&regVal, cThaRxDE1LofThresMask, cThaRxDE1LofThresShift, pThres->lofThres);
    mFieldIns(&regVal, cThaRxDE1LosCntThresMask, cThaRxDE1LosCntThresShift, pThres->losCntThres);

    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet ThresholdUpdate(AtPdhChannel self)
    {
    tThaCfgPdhDs1E1FrmRxThres threshold;

    /* T-REC-G.706 specify 3 frames for E1 */
    mMethodsGet(mThis(self))->DefaultThreshold(mThis(self), &threshold);
    if (AtPdhDe1IsE1((AtPdhDe1)self))
        threshold.lofThres = 3;

    return ThresholdSet((AtChannel)self, &threshold);
    }

static eBool SsmIsApplicable(AtPdhChannel self)
    {
    eAtPdhDe1FrameType frameType = AtPdhChannelFrameTypeGet(self);

    if ((frameType == cAtPdhE1MFCrc)   ||
        (frameType == cAtPdhDs1FrmEsf) ||
        (frameType == cAtPdhJ1FrmEsf))
        return cAtTrue;

    return cAtFalse;
    }

static eBool SsmIsSupported(AtPdhChannel self)
    {
    AtPw pw;

    if (!SsmIsApplicable(self))
        return cAtFalse;

    /* If it is bound to SAToP PW, slip buffer must be enabled first */
    pw = AtChannelBoundPwGet((AtChannel)self);
    if (AtPwTypeGet(pw) == cAtPwTypeSAToP)
        return AtPdhChannelTxSlipBufferIsEnabled(self);

    /* SSM can be inserted for all of other cases */
    return cAtTrue;
    }

static eBool BomIsSupported(ThaPdhDe1 self)
    {
    if (!ThaModulePdhBomIsSupported(PdhModule((AtChannel)self)))
        return cAtFalse;
    return (AtPdhChannelFrameTypeGet((AtPdhChannel)self) == cAtPdhDs1FrmEsf) ? cAtTrue : cAtFalse;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = cAtOk;
    AtList backup = NULL;
	ThaModulePdh pdhModule = PdhModule((AtChannel) self);
    eBool isUnframed = AtPdhDe1IsUnframeMode(frameType);
    eAtTimingMode timingMode;
    AtChannel timingSource;
    eBool prbsEnabled = cAtFalse;
    AtPrbsEngine prbsEngine;
    AtBerController controller;
    eBool canHotChangeFrameType;
    eAtRet dryRunRet = BeforeChangeFrameTypeCheck(self, frameType);

    if (dryRunRet != cAtRetMoveNext)
        return dryRunRet;

    /* Disable PRBS to re-activate it later */
    prbsEngine = AtChannelPrbsEngineGet((AtChannel)self);
    if (prbsEngine)
        {
        prbsEnabled = AtPrbsEngineIsEnabled(prbsEngine);
        if (prbsEnabled)
            AtPrbsEngineEnable(prbsEngine, cAtFalse);
        }

    /* Need to disable all of not applicable TCA mask on new frame type */
    TcaNotificationMaskUpdate(self, frameType);

    canHotChangeFrameType = ThaModulePdhCanHotChangeFrameType(pdhModule);
    if (canHotChangeFrameType == cAtFalse)
        {
        /* Notify mapping channel my frame mode is going to be changed */
        backup = Backup(self);
        }

    /* Disable ssm for old frame type */
    if (SsmIsSupported(self) && AtPdhChannelSsmIsEnabled(self))
        AtPdhChannelSsmEnable(self, cAtFalse);

    /* Set Frame type to HW */
    ret |= HwFrameTypeSet(self, frameType);
    if (ret != cAtOk)
        return ret;

    /* Cache frame type to avoid accessing every time from other context.
     * Below code need to know new frame type, must update cache here */
    mThis(self)->frameType = frameType;

    /* Default Disable Data-Link when change frame type */
    if (ThaModulePdhDatalinkIsSupported(PdhModule((AtChannel)self)))
        {
        AtPdhChannelDataLinkEnable(self, cAtFalse);
        AtPdhChannelDataLinkFcsBitOrderSet((AtPdhChannel) self, cAtBitOrderLsb);
        }

    if (isUnframed)
        ret |= DisableSignalingInUnframeMode((ThaPdhDe1)self);
    
    /* Update channel type for BER Controller */
	controller = AtPdhChannelPathBerControllerGet(self);
	if (controller && AtBerControllerIsEnabled(controller))
		ret |= AtBerControllerHwChannelTypeUpdate(controller);

    if (canHotChangeFrameType == cAtFalse)
        {
    /* Notify mapping channel my frame mode has been changed */
    ret |= Restore(self, backup);
    AtObjectDelete((AtObject)backup);
        }

    /* Need to re-configure timing mode */
    timingMode = AtChannelTimingModeGet((AtChannel)self);
    timingSource = AtChannelTimingSourceGet((AtChannel)self);
    ret |= AtChannelTimingSet((AtChannel)self, timingMode, timingSource);

    /* Activate PRBS engine if it is enabled before */
    if (prbsEngine && prbsEnabled)
        ret |= AtPrbsEngineEnable(prbsEngine, cAtTrue);

    /* Reset signaling mask in case of un-framed mode */
    if (isUnframed)
        mThis(self)->signalingBitMap = 0x0;

    /* Handle AIS All One Patterns */
    ret |= HandleAisAllOneDetection(self, pdhModule, isUnframed);

    NotApplicableAlarmsNotify(self, frameType);

    if (frameType == cAtPdhDs1FrmEsf)
        ret |= PrmDefaultSet((AtPdhDe1)self);

    if (ThaModulePdhDe1FastSearchIsSupported(pdhModule))
        ret |= FastFrameSearchEnable((ThaPdhDe1)self, ShouldEnableFastFramingSearch(frameType));

    /* Update frame satop hot change */
    ret |= FrameSatopApply(self, frameType);

    /* For FAS threshold */
    ret |= ThresholdUpdate(self);
    AtPdhDe1AutoTxAisEnable((AtPdhDe1)self, ShouldEnableAutoTxAis((ThaPdhDe1)self));

    /* Reset BOM */
    if (BomIsSupported(mThis(self)))
        AtPdhDe1TxBomWithModeSet((AtPdhDe1)self, 0, cAtPdhBomSentModeInvalid);

    return ret;
    }

static uint16 FrameTypeGet(AtPdhChannel self)
    {
    return mThis(self)->frameType;
    }

/* This function is to set Line code of a DS1/E1 channel */
static eAtRet LineCodeSet(AtPdhChannel self, uint16 lineCode)
    {
	AtUnused(lineCode);
	AtUnused(self);
    /* This card doesn't support Line Code inside */
    return cAtError;
    }

/* This function is to get Line code of a DS1/E1 channel */
static uint16 LineCodeGet(AtPdhChannel self)
    {
	AtUnused(self);
    /* This card doesn't support Line Code inside */
    return 0;
    }

static eBool LedStateIsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

/* This function is to set Led state of a DS1/E1 channel */
static eAtRet LedStateSet(AtPdhChannel self, eAtLedState ledState)
    {
    uint32 regVal;
    uint32 regAddr   = cThaRegDS1E1LedStateCtrl;
    uint32 portId    = mMethodsGet(mThis(self))->FlatId(mThis(self), cAtModulePdh);
    uint8 force;
    uint8 state;
    uint32 mask, shift;

    if (!AtLedStateIsValid(ledState))
        return cAtErrorModeNotSupport;

    regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    force = (ledState == cAtLedStateBlink) ? 0 : 1;
    mask = cThaRegDS1E1LedStateForceMask(portId);
    shift = cThaRegDS1E1LedStateForceShift(portId);
    mFieldIns(&regVal, mask, shift, force);

    state = (ledState == cAtLedStateOn) ? 0 : 1;
    mask = cThaRegDS1E1LedStateStatMask(portId);
    shift = cThaRegDS1E1LedStateStatShift(portId);
    mFieldIns(&regVal, mask, shift, state);

    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }

/* This function is to get Led state of a DS1/E1 channel */
static eAtLedState LedStateGet(AtPdhChannel self)
    {
    uint32 regVal;
    eBool forced, isOn;
    uint32 portId = mMethodsGet(mThis(self))->FlatId(mThis(self), cAtModulePdh);

    regVal = mChannelHwRead(self, cThaRegDS1E1LedStateCtrl, cAtModulePdh);
    forced = (regVal & cThaRegDS1E1LedStateForceMask(portId)) ? cAtTrue : cAtFalse;
    isOn   = (regVal & cThaRegDS1E1LedStateStatMask(portId))  ? cAtFalse : cAtTrue;

    if (forced)
        return isOn ? cAtLedStateOn : cAtLedStateOff;
    else
        return cAtLedStateBlink;
    }

static uint32 AllDs0SignalingEnableMask(eBool isE1)
    {
    return isE1 ? 0 : 0x3F000000;
    }

static uint32 AllDs0SignalingDisableMask(eBool isE1)
    {
    return isE1 ? cBit29_0 : cBit23_0;
    }

static uint32 Ds0SignalingDisableMaskCreate(eBool isE1, uint32 de1TimeslotBitmap)
    {
    uint8 i;
    uint32 sigBypassMask = 0;
    eBool allSlotsByPassed;

    /* If frame mode is DS1, mask will be returned de1TimeslotBitmap with 24 TS.
     * So we just create mask for E1 frame mode */
    if (!isE1)
        return (~de1TimeslotBitmap) & cBit23_0;

    for (i = 0; i < cThaMaxE1TimeslotNum; i++)
        {
        if ((de1TimeslotBitmap & (cBit0 << i)) != 0)
            {
            if ((i == 0) || (i == 16))
                continue; /* Don't care TS #0 and #16 */

            /* Shift << 1 bit for TS#1-15 and shift << 2 bits for TS#17-31 */
            sigBypassMask = (i < 16) ? (sigBypassMask | (cBit0 << (i - 1))) : (sigBypassMask | (cBit0 << (i - 2)));
            }
        }

    sigBypassMask = (~sigBypassMask) & cBit29_0;

    /* If there is at least one time slot needs to carry signaling, hardware
     * requires no slots are bypassed */
    allSlotsByPassed = (sigBypassMask == AllDs0SignalingDisableMask(isE1));
    if (!allSlotsByPassed)
        sigBypassMask = AllDs0SignalingEnableMask(isE1);

    return sigBypassMask;
    }

static uint32 TimeslotDefaultOffset(ThaPdhDe1 self)
    {
    return 32 * AtChannelIdGet((AtChannel)self);
    }

static eAtRet SignalingBufferReset(ThaPdhDe1 self, uint32 sigBitmap)
    {
    uint8  timeslot;
    eBool  isE1;
    uint8  maxTimeslot;
    uint32 baseAddr;

    if (!ThaModulePdhCasSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self)))
        return cAtOk;

    /* Some products need to fill full signaling buffer with a constant value
     * without caring about signaling bitmap */
    if (!ThaModulePdhNeedConfigureSignalingBufferPerTimeslot((ThaModulePdh)AtChannelModuleGet((AtChannel)self)))
        sigBitmap = 0;

    isE1 = AtPdhDe1IsE1((AtPdhDe1)self);
    maxTimeslot = isE1 ? cThaMaxE1TimeslotNum : cThaMaxDS1TimeslotNum;
    baseAddr = ThaRegDS1E1J1TxFrmrSignalingBuffer((ThaModulePdh)AtChannelModuleGet((AtChannel)self)) + ThaPdhDe1TimeslotDefaultOffset(self);
    for (timeslot = 0; timeslot < maxTimeslot; timeslot++)
        {
        uint8 cAnyValue = 0xA;
        uint32 signalingByPassed = ((sigBitmap & (cBit0 << timeslot)) == 0) ? cAtTrue : cAtFalse;
        uint32 regAddr = baseAddr + timeslot;
        if (isE1 && ((timeslot == 0) || (timeslot == 16)))
            signalingByPassed = cAtTrue;

        mChannelHwWrite(self, regAddr, signalingByPassed ? cAnyValue : 0x0UL, cAtModulePdh);
        }

    return cAtOk;
    }

static eAtRet HwCasIdleCodeSet(ThaPdhDe1 self, uint32 ts, uint8 idle)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 baseAddress = ThaRegDS1E1J1TxFrmrSignalingBuffer(modulePdh);
    uint32 address = baseAddress + ThaPdhDe1TimeslotDefaultOffset(self) + ts;

    mChannelHwWrite(self, address, idle, cAtModulePdh);

    return cAtOk;
    }

static uint8 HwCasIdleCodeGet(ThaPdhDe1 self, uint32 ts)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    uint32 baseAddress = ThaRegDS1E1J1TxFrmrSignalingBuffer(modulePdh);
    uint32 address = baseAddress + ThaPdhDe1TimeslotDefaultOffset(self) + ts;
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    uint32 idle = regVal & cBit3_0;

    return (uint8)idle;
    }

static eAtRet SignalingPatternSet(AtPdhDe1 self, uint32 ts, uint8 abcd)
    {
    if (AtPdhDe1SignalingIsSupported(self))
        return ThaPdhDe1HwCasIdleCodeSet(mThis(self), ts, abcd);
    return cAtErrorModeNotSupport;
    }

static uint8 SignalingPatternGet(AtPdhDe1 self, uint32 ts)
    {
    if (AtPdhDe1SignalingIsSupported(self))
        return ThaPdhDe1HwCasIdleCodeGet(mThis(self), ts);
    return 0;
    }

static eBool SignalingIsSupported(AtPdhDe1 self)
    {
    return ThaModulePdhCasSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static eBool SignalingBitmaskIsValid(AtPdhDe1 self, uint32 sigBitmap)
    {
    /* For E1, timeslot 0 and timeslot 16 cannot be enabled signaling */
    if (AtPdhDe1IsE1(self))
        {
        eBool containsTs0Ts16 = (sigBitmap & cBit16) || (sigBitmap & cBit0);
        return containsTs0Ts16 ? cAtFalse : cAtTrue;
        }

    /* For DS1, only time slot 0-23 are valid */
    return (sigBitmap & (~cBit23_0)) ? cAtFalse : cAtTrue;
    }

static eAtRet Ds0SignalingEnable(ThaPdhDe1 self, uint32 sigBitmap)
    {
    uint32 disableMask = Ds0SignalingDisableMaskCreate(AtPdhDe1IsE1((AtPdhDe1)self), sigBitmap);
    return Ds0SignalingDisable(self, disableMask);
    }

/* This function is to set signaling DS0 bitmap of a DE1 to hardware
 * sigBitmap will be input in high-level and will be converted to sigBypassMask.
 * This value active in low-level */
static eAtRet SignalingDs0MaskSet(AtPdhDe1 self, uint32 sigBitmap)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;

    if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        return cAtErrorModeNotSupport;

    if (!SignalingBitmaskIsValid(self, sigBitmap))
        return cAtErrorInvlParm;

    Ds0SignalingEnable(de1, sigBitmap);
    SignalingBufferReset(de1, sigBitmap);

    /* Save to database */
    de1->signalingBitMap = sigBitmap;

    return cAtOk;
    }

/* this funciton is to get signaling DS0 bitmap of a DE1 and convert to SW mask */
static uint32 SignalingDs0MaskGet(AtPdhDe1 self)
    {
    return mThis(self)->signalingBitMap;
    }

static eBool SignalingIsApplicable(AtPdhDe1 self)
    {
    if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        return cAtFalse;

    return cAtTrue;
    }

static eBool Timeslot16BoundToPw(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    AtIterator nxds0Iterator;
    AtPdhNxDS0 nxds0;
    eBool boundToPw = cAtFalse;

    /* The whole DS1/E1 is bound to PW */
    if (AtChannelBoundPwGet((AtChannel)self))
        return cAtTrue;

    nxds0Iterator = AtListIteratorCreate(de1->allNxDs0s);
    while ((nxds0 = (AtPdhNxDS0)AtIteratorNext(nxds0Iterator)) != NULL)
        {
        if (AtChannelBoundPwGet((AtChannel)nxds0) && (AtPdhNxDS0BitmapGet(nxds0) & cBit16))
            {
            boundToPw = cAtTrue;
            break;
            }
        }
    AtObjectDelete((AtObject)nxds0Iterator);

    return boundToPw;
    }

static eBool NeedCarryTimeslot16OnPw(AtPdhDe1 self)
    {
    return ThaModulePdhNeedCarryTimeslot16OnPw((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static eBool TxRetimingSignalingIsSupported(AtPdhDe1 self)
    {
    if (!ThaPdhDe1RetimingIsSupported((ThaPdhDe1)self))
        return cAtFalse;

    return ThaModulePdhTxRetimingSignalingIsSupported(PdhModule((AtChannel)self));
    }

static eBool CanChangeTxSignal(AtPdhDe1 self)
    {
    AtPw pseudowire = AtChannelBoundPwGet((AtChannel)self);
    AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel)self);

    if (pseudowire == NULL)
        return cAtTrue;

    if ((engine != NULL) && AtPrbsEngineIsEnabled(engine))
        return cAtTrue;

    if (AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self))
        return cAtTrue;

    if (ThaPdhChannelShouldRejectTxChangeWhenBoundPw((AtPdhChannel)self))
        return cAtFalse;

    return cAtTrue;
    }

static eBool LoopcodeIsActive(AtPdhDe1 self)
    {
    eAtPdhDe1Loopcode loopCode = AtPdhDe1TxLoopcodeGet(self);
    if ((loopCode != cAtPdhDe1LoopcodeInvalid) && (loopCode != cAtPdhDe1LoopcodeInbandDisable))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet SignalingEnable(AtPdhDe1 self, eBool enable)
    {
    eAtRet ret = cAtOk;
    ThaPdhDe1 de1 = mThis(self);
    uint32 signalingMask;

    if (!SignalingIsApplicable(self))
        return cAtErrorModeNotSupport;

    if (enable && !CanChangeTxSignal(self))
        return cAtErrorNotApplicable;

    if (enable && LoopcodeIsActive(self))
        return cAtErrorNotApplicable;

    signalingMask = AtPdhDe1SignalingDs0MaskGet(self);

    /* Legacy logic does not allow to enable signaling if DS0 mask is not asigned */
    if ((signalingMask == 0) && enable)
        return cAtErrorNotReady;

    if (enable)
        {
        eBool needTransparent = Timeslot16BoundToPw(self) && NeedCarryTimeslot16OnPw(self);
        SignalingMultiframeEnable(de1, needTransparent ? cAtFalse : cAtTrue);
        ret |= AtPdhDe1SignalingDs0MaskSet(self, signalingMask);
        if (needTransparent)
            ThaPdhDe1SignalingTransparent(de1);
        }
    else
        {
        SignalingMultiframeEnable(de1, cAtFalse);
        ret |= AllDs0SignalingDisable(de1);
        ret |= SignalingBufferReset(mThis(self), 0x0); /* Bypass all slots */
        }

    ret |= ThaCfgDs1E1FrmTxSigCpuMdEnSet(de1, cAtFalse);
    if (TxRetimingSignalingIsSupported(self))
        {
        eBool signalingEnable = (enable && AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self)) ? cAtTrue : cAtFalse;
        ret |= ThaPdhDe1RetimingControllerSlipBufferTxSignalingEnable(mDe1RetimingController(self), signalingEnable);
        }

    mThis(self)->signalingEnabled = enable;

    return ret;
    }

static eAtRet TxSignalingEnable(AtPdhDe1 self, eBool enable)
    {
    AtAttController att = AtChannelAttController((AtChannel)self);
    if (att)
        {
        uint32 signalingMask = AtPdhDe1SignalingDs0MaskGet(self);
        uint8 ts_i = 0;
        for (ts_i=0; ts_i<32; ts_i++)
            {
            if ((uint32)(1<<ts_i) &  signalingMask)
                AtAttControllerTxSignalingEnable(att, ts_i, enable);
            }
        return cAtOk;
        }

    return cAtError;
    }

static eAtRet RxSignalingEnable(AtPdhDe1 self, eBool enable)
    {
    AtAttController att = AtChannelAttController((AtChannel)self);
    if (att)
        {
        uint32 signalingMask = AtPdhDe1SignalingDs0MaskGet(self);
        uint8 ts_i = 0;
        for (ts_i=0; ts_i<32; ts_i++)
            {
            if ((uint32)(1<<ts_i) &  signalingMask)
                AtAttControllerRxSignalingEnable(att, ts_i, enable);
            }
        return cAtOk;
        }

    return cAtError;
    }

/* This function is to get signaling state */
static eBool SignalingIsEnabled(AtPdhDe1 self)
    {
    return mThis(self)->signalingEnabled;
    }

static eBool IsDs1FrameMode(uint8 frameValue)
    {
    if ((frameValue == cAtPdhDs1FrmSf) ||
        (frameValue == cAtPdhDs1FrmEsf) ||
        (frameValue == cAtPdhDs1FrmDDS) ||
        (frameValue == cAtPdhDs1FrmSLC))
        return cAtTrue;

    return cAtFalse;
    }

static eBool TimeslotBitmapIsValid(uint32 timeslotBitmap, uint8 frameType)
    {
    uint32 unusedBits = IsDs1FrameMode(frameType) ? cBit31_24 : 0;
    return (timeslotBitmap & unusedBits) ? cAtFalse : cAtTrue;
    }

static eBool CanUseTimeslot16InNxDs0(AtPdhDe1 self)
    {
    return ThaModulePdhCanUseTimeslot16InNxDs0((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static eBool CanUseTimeslot0InNxDs0(AtPdhDe1 self)
    {
    return ThaModulePdhCanUseTimeslot0InNxDs0((ThaModulePdh)AtChannelModuleGet((AtChannel)self));
    }

static eBool SlotsNotInUsed(AtPdhDe1 self, uint32 timeslotBitmap)
    {
    uint32 inusedMask = AtPdhDe1Ds0MaskGet(self);

    /* Check if frame mode is unframe, return NULL */
    eAtPdhDe1FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    if (AtPdhDe1IsUnframeMode(frameType))
        return cAtFalse;

    /* Can not create nxDs0 with bitmap contains TS #0 when running E1 framing */
    if (((frameType == cAtPdhE1Frm) || (frameType == cAtPdhE1MFCrc)) &&
        (timeslotBitmap & cBit0) && !CanUseTimeslot0InNxDs0(self))
        return cAtFalse;

    /* Can not create nxDS0 with bitmap contains #16 when running signaling */
    if (ThaPdhDe1IsE1Signaling(self) && (timeslotBitmap & cBit16) && !CanUseTimeslot16InNxDs0(self))
        return cAtFalse;

    /* Cannot create if timeslot is being used */
    if ((inusedMask & timeslotBitmap) || (timeslotBitmap == 0x0))
        return cAtFalse;

    if (!TimeslotBitmapIsValid(timeslotBitmap, frameType))
        return cAtFalse;

    return cAtTrue;
    }

/* This function is to create a nxDs0 group of a DE1 channel */
static AtPdhNxDS0 NxDs0Create(AtPdhDe1 self, uint32 timeslotBitmap)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    uint32 inusedMask = AtPdhDe1Ds0MaskGet(self);
    AtPdhNxDS0 nxds0;

    /* Check if input is valid */
    if (!SlotsNotInUsed(self, timeslotBitmap))
        return NULL;

    /* Create NxDS0 */
    nxds0 = AtModulePdhNxDs0Create((AtModulePdh)AtChannelModuleGet((AtChannel)self), self, timeslotBitmap);
    if (nxds0 == NULL)
    	return NULL;

    if (de1->allNxDs0s == NULL)
        de1->allNxDs0s = AtListCreate(cThaMaxE1TimeslotNum);

    if (AtListObjectAdd(de1->allNxDs0s, (AtObject)nxds0) != cAtOk)
        {
        AtObjectDelete((AtObject)nxds0);
        return NULL;
        }

    AtChannelEnable((AtChannel)nxds0, cAtFalse);
    AtPdhDe1Ds0MaskSet(self, (inusedMask | timeslotBitmap));

    return nxds0;
    }

static eAtRet EncapConnectionEnable(AtChannel self, eBool enable)
    {
    uint32 mask;
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);

    mask = mMethodsGet((ThaPdhDe1)self)->EncapDs0BitMaskGet((ThaPdhDe1)self);

    ret |= ThaModuleAbstractMapDs0EncapConnectionEnable(
                                    (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap),
                                    self, mask, enable);
    ret |= ThaModuleAbstractMapDs0EncapConnectionEnable(
                                    (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap),
                                    self, mask, enable);

    return ret;
    }

static AtPdhNxDS0 NxDs0Get(AtPdhDe1 self, uint32 timeslotBitmap)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    AtIterator nxds0Iterator;
    AtPdhNxDS0 nxds0;

    /* Unframe mode does not have any NxDS0 group */
    if (AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        return NULL;

    nxds0Iterator = AtListIteratorCreate(de1->allNxDs0s);
    while ((nxds0 = (AtPdhNxDS0)AtIteratorNext(nxds0Iterator)) != NULL)
        {
        if (timeslotBitmap == ThaPdhNxDs0MaskGet(nxds0))
            {
            AtObjectDelete((AtObject)nxds0Iterator);
            return nxds0;
            }
        }

    AtObjectDelete((AtObject)nxds0Iterator);

    /* return NULL if can't get any nxDs0 object */
    return NULL;
    }

static AtIterator NxDs0IteratorCreate(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;

    if (de1->allNxDs0s == NULL)
        return NULL;

    return AtListIteratorCreate(de1->allNxDs0s);
    }

static ThaCdrController CdrControllerCreate(ThaPdhDe1 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrDe1CdrControllerCreate(cdrModule, (AtPdhDe1)self);
    }

static AtEncapBinder EncapBinder(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceEncapBinder(device);
    }

/* This function is binding to encap  */
static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtChannel nxds0;

    /* Check if this DE1 has any NxDs0 bound to an encap channel, return error */
    AtIterator nxds0Iterator = AtPdhDe1nxDs0IteratorCreate((AtPdhDe1)self);
    while ((nxds0 = (AtChannel)AtIteratorNext(nxds0Iterator)) != NULL)
        {
        if (AtChannelBoundEncapChannelGet(nxds0) != NULL)
            {
            AtObjectDelete((AtObject)nxds0Iterator);
            return cAtError;
            }
        }

    AtObjectDelete((AtObject)nxds0Iterator);

    /* Hardware binding */
    ret |= AtEncapBinderBindDe1ToEncapChannel(EncapBinder(self), self, encapChannel);

    /* Let super deal with database */
    ret |= m_AtChannelMethods->BindToEncapChannel(self, encapChannel);

    return ret;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtEncapBinderDe1EncapHwIdAllocate(EncapBinder(self), (AtPdhDe1)self);
    }

static uint32 BoundEncapHwIdGet(AtChannel self)
    {
    return AtEncapBinderDe1BoundEncapHwIdGet(EncapBinder(self), self);
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);
    AtPdhDe1 de1 = (AtPdhDe1)self;

    m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    ret |= ThaModuleAbstractMapBindDe1ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), de1, pseudowire);
    ret |= ThaModuleAbstractMapBindDe1ToPseudowire((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), de1, pseudowire);
    if (ret != cAtOk)
        return ret;

    /* In case of unbinding, need to update timing configuration */
    if (pseudowire == NULL)
        {
        if ((AtChannelTimingModeGet(self) == cAtTimingModeAcr) ||
            (AtChannelTimingModeGet(self) == cAtTimingModeDcr))
            ret |= AtChannelTimingSet(self, cAtTimingModeSys, NULL);
        }

    if (!AtPdhDe1IsUnframeMode(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        ret |= De1FramingBindToPwSatop((AtPdhChannel)self, pseudowire);

    if (ret != cAtOk)
        return ret;

    if (pseudowire)
        ThaModulePwDe1BoundPwIdSaveToHwStorage((ThaModulePw)AtDeviceModuleGet(device, cAtModulePw), de1, (uint16)AtChannelIdGet((AtChannel)pseudowire));
    else
        ThaModulePwDe1BoundPwIdHwStorageClear((ThaModulePw)AtDeviceModuleGet(device, cAtModulePw), de1);

    return cAtOk;
    }

static uint32 BoundPwHwIdGet(AtChannel self)
    {
    AtDevice device    = AtChannelDeviceGet(self);
    AtPdhDe1 de1       = (AtPdhDe1)self;

    return ThaModuleAbstractMapDe1BoundPwHwIdGet((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap), de1);
    }

static uint8 NumBlocksNeedToBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
	AtUnused(encapChannel);
    return AtPdhDe1IsE1((AtPdhDe1)self) ? 16 : 12;
    }

static uint8 DefaultFrameMode(AtPdhChannel self)
    {
	AtUnused(self);
    return cAtPdhE1UnFrm;
    }

static eBool NxDs0IsBusy(AtPdhNxDS0 nxDs0)
    {
    if (AtChannelBoundEncapChannel((AtChannel)nxDs0) ||
        AtChannelBoundPwGet((AtChannel)nxDs0))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet NxDs0Delete(AtPdhDe1 self, AtPdhNxDS0 nxDs0)
    {
    uint32 inusedMask;
    ThaPdhDe1 de1 = (ThaPdhDe1)self;

    /* This channel is busy with another encap channel */
    if (NxDs0IsBusy(nxDs0))
        return cAtErrorChannelBusy;

    if (AtListObjectRemove(de1->allNxDs0s, (AtObject)nxDs0) != cAtOk)
        return cAtErrorInvlParm;

    /* Update to database */
    inusedMask = AtPdhDe1Ds0MaskGet(self) & (~(ThaPdhNxDs0MaskGet(nxDs0)));
    AtPdhDe1Ds0MaskSet(self, inusedMask);

    /* Delete nxDS0 object */
    AtObjectDelete((AtObject)nxDs0);

    return cAtOk;
    }

static void UnforceAll(AtChannel self)
    {
    (void)AtChannelRxAlarmUnForce(self, AtChannelRxForcableAlarmsGet(self));
    (void)AtChannelTxAlarmUnForce(self, AtChannelTxForcableAlarmsGet(self));
    }

static eAtRet TxE1SsmSet(AtChannel self, uint8 ssm)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    mRegFieldSet(regVal, cThaTxDE1TxSsmThres, 0x1F);
    mRegFieldSet(regVal, cThaTxDE1DLCfgSsmVal, ssm);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool HasSsmThreshold(AtChannel self)
    {
    /* From the version that support AIS downstream, hardware remove SSM threshold configuration */
    return ThaModulePdhAisDownStreamIsSupported((ThaModulePdh)AtChannelModuleGet(self)) ? cAtFalse : cAtTrue;
    }

static eAtRet HWDataLinkEnable(AtPdhChannel self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 hwEnabled = enable ? 1 : 0;

    /* Tx Enable */
    regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cThaTxDs1DlkEn, hwEnabled);
    mRegFieldSet(regVal, cThaTxDE1MdlEn, hwEnabled);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    /* Rx Enable */
    regAddr = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mRegFieldSet(regVal, cThaRxDs1DlkEn, hwEnabled);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool HasDataLink(uint16 frameType)
    {
    /* Current, only support the ESF for PRM operations */
    return (frameType == cAtPdhDs1FrmEsf) ? cAtTrue : cAtFalse;
    }

static eAtRet DataLinkEnable(AtPdhChannel self, eBool enable)
    {
    if (!ThaModulePdhPrmIsSupported((ThaModulePdh)PdhModule((AtChannel)self)))
        return cAtFalse;

    if (HasDataLink(AtPdhChannelFrameTypeGet(self)))
        return HWDataLinkEnable(self, enable);

    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool DataLinkIsEnabled(AtPdhChannel self)
    {
    uint32 address, regVal;

    if (!ThaModulePdhPrmIsSupported((ThaModulePdh)PdhModule((AtChannel)self)))
        return cAtFalse;

    /* Current, only support the ESF for PRM operations */
    if (AtPdhChannelFrameTypeGet(self) != cAtPdhDs1FrmEsf)
        return cAtFalse;

    address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    return (regVal & cThaRxDs1DlkEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet DataLinkReset(AtChannel self)
    {
    if (ThaModulePdhPrmIsSupported((ThaModulePdh)PdhModule(self)))
        return HWDataLinkEnable((AtPdhChannel)self, cAtFalse);
    return cAtOk;
    }

static eAtRet HwDefaultSet(AtChannel self)
    {
    uint32 address, regVal;
    AtDevice device = AtChannelDeviceGet(self);
    eBool mfasCheck = ThaModulePdhDe1MFASChecked((ThaModulePdh)AtDeviceModuleGet(device, cAtModulePdh));

    /* Rx */
    address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaRxDE1FrcLofMask, cThaRxDE1FrcLofShift, 0);
    mFieldIns(&regVal, cThaRxDE1AisMaskEnMask, cThaRxDE1AisMaskEnShift, 1);
    mFieldIns(&regVal, cThaRxDE1SaCfgMask, cThaRxDE1SaCfgShift, 0);
    mFieldIns(&regVal, cThaRxDE1MonOnlyMask, cThaRxDE1MonOnlyShift, 0);
    mFieldIns(&regVal, cThaRxDE1MFASChkEnMask, cThaRxDE1MFASChkEnShift, mfasCheck ? 1 : 0);

    if (HasSsmThreshold(self))
        mFieldIns(&regVal, cThaRxDE1SsmThresholdMask, cThaRxDE1SsmThresholdShift, 0x6);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    /* Tx */
    address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1FrcCrcErrMask, cThaTxDE1FrcCrcErrShift, 0);
    mFieldIns(&regVal, cThaTxDE1AutoCrcErrMask, cThaTxDE1AutoCrcErrShift, 1);
    mFieldIns(&regVal, cThaTxDE1FrcYelMask, cThaTxDE1FrcYelShift, 0);
    mFieldIns(&regVal, cThaTxDE1DLCfgMask, cThaTxDE1DLCfgShift, 0);
    mFieldIns(&regVal, cThaTxDE1DLSwapMask, cThaTxDE1DLSwapShift, 0);
    mFieldIns(&regVal, cThaTxDE1DLBypassMask, cThaTxDE1DLBypassShift, 0);
    mFieldIns(&regVal, cThaTxDE1PayAisInsMask, cThaTxDE1PayAisInsShift, 0);
    mFieldIns(&regVal, cThaTxDE1LineAisInsMask, cThaTxDE1LineAisInsShift, 0);
    mFieldIns(&regVal, cThaTxDE1FbitBypassMask, cThaTxDE1FbitBypassShift, 0);
    mFieldIns(&regVal, cThaTxDE1TxSaBitMask, cThaTxDE1TxSaBitShift, 1);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    DataLinkReset(self);

    return cAtOk;
    }

static uint8 TxE1SsmGet(AtChannel self)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cThaTxDE1DLCfgSsmVal);
    }

static uint8 SsmTxHwSaBitMaskGet(ThaPdhDe1 self)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    uint32 fieldMask = mFieldMask(self, TxFrmrE1SaCfg);
    uint8 fieldShift = mFieldShift(self, TxFrmrE1SaCfg);
    return ThaPdhDe1RxSaBitHw2Sw((uint8)mRegField(regVal, field));
    }

static eAtRet SsmHwEnable(ThaPdhDe1 self, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mRegFieldSet(regVal, cThaTxDE1DLCfgSsmEn, enable ? 1 : 0);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SsmHwIsEnabled(ThaPdhDe1 self)
    {
    uint32 regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    return (regVal & cThaTxDE1DLCfgSsmEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet(self);

    /* Need to reset database so that default framing can be applied */
    mThis(self)->frameType = 0;

    /* Super Init */
    ret |= m_AtChannelMethods->Init(self);

    ret |= HwDefaultSet(self);
    if (ThaPdhDe1RetimingIsSupported((ThaPdhDe1)self))
        ThaPdhRetimingControllerSlipBufferDefaultSet(RetimingControllerGet((ThaPdhDe1)self));
    if (AtPdhChannelRxSlipBufferIsSupported((AtPdhChannel)self))
        AtPdhChannelRxSlipBufferEnable((AtPdhChannel)self, cAtFalse);

    /* Default frame mode */
    ret |= AtPdhChannelFrameTypeNoLockSet(pdhChannel, mMethodsGet(pdhChannel)->DefaultFrameMode(pdhChannel));

    /* By pass signaling */
    ret |= SignalingMultiframeEnable(mThis(self), cAtFalse);
    ret |= AllDs0SignalingDisable(mThis(self));
    ret |= SignalingBufferReset(mThis(self), 0x0);

    /* Default timing */
    ret |= AtChannelTimingSet(self, cAtTimingModeSys, self);

    /* Flush MAP/DEMAP configure */
    ret |= ThaModuleAbstractMapDe1Init((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap),   (AtPdhDe1)self);
    ret |= ThaModuleAbstractMapDe1Init((ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap), (AtPdhDe1)self);

    /* Default threshold */
    ret |= ThresholdUpdate(pdhChannel);

    /* Handle threshold for AIS All One Patterns */
    ret |= HandleAisAllOneDetection(pdhChannel, pdhModule, AtPdhDe1IsUnframeMode(mMethodsGet(pdhChannel)->DefaultFrameMode(pdhChannel)) ? cAtFalse : cAtTrue);

    /* Clear all interrupt mask and forced alarms */
    ret |= AtChannelInterruptMaskSet(self, AtChannelInterruptMaskGet(self), 0x0);
    ret |= AtChannelRxAlarmUnForce(self, cAtPdhDe1AlarmLof | cAtPdhDe1AlarmAis | cAtPdhDe1AlarmLos); /* LOF in Rx */
    ret |= AtChannelTxAlarmUnForce(self, cAtPdhDe1AlarmRai|cAtPdhDe1AlarmAis); /* RAI and AIS in Tx */
    ret |= AtChannelEnable(self, cAtTrue);
    ret |= AtChannelLoopbackSet(self, cAtPdhLoopbackModeRelease);

    if (AtPdhChannelLedStateIsSupported((AtPdhChannel)self))
        AtPdhChannelLedStateSet((AtPdhChannel)self, cAtLedStateBlink);

    ret |= AtPdhChannelAutoRaiEnable((AtPdhChannel)self, cAtTrue);
    UnforceAll(self);

    if (AtPdhChannelLinePrbsEngineGet((AtPdhChannel)self))
        AtPrbsEngineEnable(AtPdhChannelLinePrbsEngineGet((AtPdhChannel)self), cAtFalse);

    if (ThaModulePdhInbandLoopcodeIsSupported(pdhModule))
        AtPdhDe1LoopcodeSend((AtPdhDe1)self, cAtPdhDe1LoopcodeInbandDisable);

    if (ThaModulePdhDe1IdleCodeInsertionIsSupported(pdhModule))
        AtPdhChannelIdleEnable((AtPdhChannel)self, cAtFalse);

    return ret;
    }

static uint32 EncapDs0BitMaskGet(ThaPdhDe1 self)
    {
    eAtPdhDe1FrameType frameType;
    uint32 ds0BitMask = 0xFFFFFFFF;

    frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    if ((frameType == cAtPdhE1Frm) || (frameType == cAtPdhE1MFCrc))
        {
        /* If E1 framing mode is not unframe, the first timeslot is not used.
         * And if signalling is enabled on it, the timeslot 16 is also unused */

        ds0BitMask = (ds0BitMask & (~cBit0));
        if (AtPdhDe1SignalingIsEnabled((AtPdhDe1)self))
            ds0BitMask = (ds0BitMask & (~cBit16));

        return ds0BitMask;
        }

    /* DS1 unframe, timeslots from 0 to 24 */
    if (frameType == cAtPdhDs1J1UnFrm)
        return (ds0BitMask & (~cBit31_25));

    /* DS1 frame, timeslot from 0 to 23 */
    if ((frameType != cAtPdhDe1FrameUnknown) && (frameType != cAtPdhE1UnFrm))
        ds0BitMask = (ds0BitMask & (~cBit31_24));

    return ds0BitMask;
    }

static uint32 SatopDs0BitMaskGet(ThaPdhDe1 self)
    {
    /* HW recommend, in case of DS1 LIU, need to bind 25 TS */
    return AtPdhDe1IsE1((AtPdhDe1)self) ? cBit31_0 : cBit24_0;
    }

static uint8 PartId(ThaPdhDe1 self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    /* Need to show CDR internal state */
    if (CdrController(self))
        ThaCdrControllerDebug(CdrController(self));

    if (ThaPdhDe1RetimingIsSupported((ThaPdhDe1)self))
        ThaPdhRetimingControllerSlipBufferTxHwDebug(RetimingControllerGet((ThaPdhDe1)self));

    return cAtOk;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorType, eBool force)
    {
    uint32 regAddr, regVal;

    if (errorType != cAtPdhDe1ErrorCrc)
        return cAtErrorModeNotSupport;

    regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1FrcCrcErrMask, cThaTxDE1FrcCrcErrShift, force ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);

    return (regVal & cThaTxDE1FrcCrcErrMask) ? cAtPdhDe1ErrorCrc : 0;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    return HelperTxErrorForce(self, errorType, cAtTrue);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    return HelperTxErrorForce(self, errorType, cAtFalse);
    }

static uint8 PppBlockRange(AtChannel self)
    {
	AtUnused(self);
    return 2;
    }

static eAtRet DefaultThreshold(ThaPdhDe1 self, tThaCfgPdhDs1E1FrmRxThres *threshold)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    return ThaModulePdhDefaultDe1Threshold(pdhModule, threshold);
    }

static uint32 FlatId(ThaPdhDe1 self, uint32 phyModule)
    {
    AtUnused(phyModule);
    return AtChannelIdGet((AtChannel)self);
    }

static uint8 MlpppBlockRange(AtChannel self)
    {
	AtUnused(self);
    return 6;
    }

static eBool NxDs0IsSupported(AtPdhDe1 self)
    {
	AtUnused(self);
    /* All of AF6 products support NxDS0 */
    return cAtTrue;
    }

static eAtRet AutoRaiEnable(AtPdhChannel self, eBool enable)
	{
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1AutoYelMask, cThaTxDE1AutoYelShift, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
	}

static eBool AutoRaiIsEnabled(AtPdhChannel self)
	{
    uint32 regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    return mBinToBool(mRegField(regVal, cThaTxDE1AutoYel));
	}

static eBool TryRestoringBoundSatopPw(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    AtModulePw modulePw = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    AtPdhDe1 de1 = (AtPdhDe1)self;
    uint16 pwHwId = ThaModulePwDe1BoundPwIdGetFromHwStorage((ThaModulePw)modulePw, de1);
    AtPw pw;
    eAtRet ret;

    if (pwHwId == cThaModulePwInvalidPwId)
        {
        AtChannelBoundPwSet(self, NULL);
        return cAtFalse;
        }

    pw = (AtPw)AtModulePwSAToPCreate(modulePw, pwHwId);
    ret = AtPwCircuitBind(pw, self);

    if (ret != cAtOk)
        {
        AtModulePwDeletePw(modulePw, pwHwId);
        return cAtFalse;
        }

    AtChannelWarmRestore((AtChannel)pw);
    return cAtTrue;
    }

static eAtRet RestoreCesopPw(AtChannel self, tNxDs0BoundPw *nxDs0Group)
    {
    AtDevice device = AtChannelDeviceGet(self);
    AtModulePw modulePw = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    AtPdhDe1 de1 = (AtPdhDe1)self;
    AtPdhNxDS0 nxDs0 = AtPdhDe1NxDs0Create(de1, nxDs0Group->nxDs0Mask);
    AtPw pw = (AtPw)AtModulePwCESoPCreate(modulePw, nxDs0Group->pwHwId, nxDs0Group->cesopMode);

    eAtRet ret = AtPwCircuitBind(pw, (AtChannel)nxDs0);
    if (ret != cAtOk)
        {
        AtModulePwDeletePw(modulePw, nxDs0Group->pwHwId);
        AtPdhDe1NxDs0Delete(de1, nxDs0);
        return ret;
        }

    return AtChannelWarmRestore((AtChannel)pw);
    }

static eAtRet WarmRestore(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtDevice device;
    AtList NxDs0BindPwGroups;
    tNxDs0BoundPw *nxDs0Group;

    /* Restore frame type */
    mThis(self)->frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    /* Try restoring Satop first */
    if (TryRestoringBoundSatopPw(self))
        return cAtOk;

    /* Not satop, try ces mode */
    device = AtChannelDeviceGet(self);
    NxDs0BindPwGroups = AtListCreate(0);

    /* Ask module map for nxDs0 bound pw list */
    if (ThaModuleMapDe1GetNxDs0BoundPwList((ThaModuleMap)AtDeviceModuleGet(device, cThaModuleMap), (AtPdhDe1)self, NxDs0BindPwGroups) != cAtOk)
        {
        AtObjectDelete((AtObject)NxDs0BindPwGroups);
        return cAtOk;
        }

    /* Nothing to restore */
    if(AtListLengthGet(NxDs0BindPwGroups) == 0)
        {
        AtObjectDelete((AtObject)NxDs0BindPwGroups);
        return cAtOk;
        }

    while (AtListLengthGet(NxDs0BindPwGroups) > 0)
        {
        nxDs0Group = (tNxDs0BoundPw *)((void*)AtListObjectRemoveAtIndex(NxDs0BindPwGroups, 0));
        ret |= RestoreCesopPw(self, nxDs0Group);
        AtOsalMemFree(nxDs0Group);
        }

    AtObjectDelete((AtObject)NxDs0BindPwGroups);
    return ret;
    }

static uint8 SliceGet(ThaPdhDe1 self)
    {
    uint8 slice = 0, hwSts = 0;
    AtPdhChannel de1 = (AtPdhChannel)self;
    AtPdhChannel de2 = (AtPdhChannel)AtPdhChannelParentChannelGet(de1);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(de1);

    /* It is over VC */
    if (vc)
        {
        ThaSdhChannel2HwMasterStsId(vc, cAtModulePrbs, &slice, &hwSts);
        return slice;
        }

    /* M13 */
    if (de2)
        {
        ThaPdhChannelHwIdGet(de1, cAtModulePdh, &slice, &hwSts);
        return slice;
        }

    /* LIU */
    return 0;
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    }

static uint8 TxInbandLoopcodeSwToHw(uint32 txCode)
    {
    switch (txCode)
        {
        case cAtPdhDe1LoopcodeLineActivate:        return cHwTxInbandCsuLoopUp;
        case cAtPdhDe1LoopcodeLineDeactivate:      return cHwTxInbandCsuLoopDown;
        case cAtPdhDe1LoopcodeFacility1Activate:   return cHwTxInbandFacility1LoopUp;
        case cAtPdhDe1LoopcodeFacility1Deactivate: return cHwTxInbandFacility1LoopDown;
        case cAtPdhDe1LoopcodeFacility2Activate:   return cHwTxInbandFacility2LoopUp;
        case cAtPdhDe1LoopcodeFacility2Deactivate: return cHwTxInbandFacility2LoopDown;
        default:                                   return cHwTxInbandLoopCodeNone;
        }
    }

static eBool CasIsEnabled(AtPdhDe1 self)
    {
    eBool casEnabled = AtPdhDe1SignalingIsEnabled(self);
    return casEnabled;
    }

static eAtModulePdhRet TxLoopcodeSet(AtPdhDe1 self, eAtPdhDe1Loopcode loopcode)
    {
    uint8 hwLoopCodeVal = 0;
    eAtRet ret = cAtOk;
    eBool isAutoRai = cAtTrue;

    if ((loopcode != cAtPdhDe1LoopcodeInbandDisable) && !CanChangeTxSignal(self))
        return cAtErrorNotApplicable;

    if (!ThaModulePdhInbandLoopcodeIsSupported(PdhModule((AtChannel)self)))
        return cAtErrorModeNotSupport;

    if (AtPdhDe1IsE1((AtPdhDe1)self) && (loopcode != cAtPdhDe1LoopcodeInbandDisable))
        return cAtErrorModeNotSupport;

    if ((loopcode != cAtPdhDe1LoopcodeInbandDisable) && CasIsEnabled(self))
        return cAtErrorNotApplicable;

    hwLoopCodeVal = TxInbandLoopcodeSwToHw(loopcode);
    isAutoRai = (hwLoopCodeVal == cHwTxInbandLoopCodeNone) ? cAtTrue : cAtFalse;

    ret |= AtPdhChannelAutoRaiEnable((AtPdhChannel)self, isAutoRai);
    ret |= ThaModuleAbstractMapDe1LoopcodeSet(MapModule((AtPdhChannel)self), self, hwLoopCodeVal);

    return ret;
    }

static uint32 TxInbandLoopcodeHw2Sw(uint8 txCode)
    {
    switch(txCode)
        {
        case cHwTxInbandCsuLoopUp:          return cAtPdhDe1LoopcodeLineActivate;
        case cHwTxInbandCsuLoopDown:        return cAtPdhDe1LoopcodeLineDeactivate;
        case cHwTxInbandFacility1LoopUp:    return cAtPdhDe1LoopcodeFacility1Activate;
        case cHwTxInbandFacility1LoopDown:  return cAtPdhDe1LoopcodeFacility1Deactivate;
        case cHwTxInbandFacility2LoopUp:    return cAtPdhDe1LoopcodeFacility2Activate;
        case cHwTxInbandFacility2LoopDown:  return cAtPdhDe1LoopcodeFacility2Deactivate;
        case cHwTxInbandLoopCodeNone:       return cAtPdhDe1LoopcodeInbandDisable;
        default:                            return cAtPdhDe1LoopcodeInvalid;
        }
    }

static eAtPdhDe1Loopcode TxLoopcodeGet(AtPdhDe1 self)
    {
    uint8 txLoopcode;

    if (!ThaModulePdhInbandLoopcodeIsSupported(PdhModule((AtChannel)self)) || AtPdhDe1IsE1((AtPdhDe1)self))
        return cAtPdhDe1LoopcodeInvalid;

    txLoopcode = ThaModuleAbstractMapDe1LoopcodeGet(MapModule((AtPdhChannel)self), self);
    return TxInbandLoopcodeHw2Sw(txLoopcode);
    }

static uint32 RxInbandLoopCodeHw2Sw(uint8 rxCode)
    {
    switch(rxCode)
        {
        case cHwRxInbandCsuLoopUp:          return cAtPdhDe1LoopcodeLineActivate;
        case cHwRxInbandCsuLoopDown:        return cAtPdhDe1LoopcodeLineDeactivate;
        case cHwRxInbandFacility1LoopUp:    return cAtPdhDe1LoopcodeFacility1Activate;
        case cHwRxInbandFacility1LoopDown:  return cAtPdhDe1LoopcodeFacility1Deactivate;
        case cHwRxInbandFacility2LoopUp:    return cAtPdhDe1LoopcodeFacility2Activate;
        case cHwRxInbandFacility2LoopDown:  return cAtPdhDe1LoopcodeFacility2Deactivate;
        case cHwRxInbandLoopCodeNone:       return cAtPdhDe1LoopcodeInbandDisable;
        default:                            return cAtPdhDe1LoopcodeInvalid;
        }
    }

static uint32 RxFramerInbandLpcStatus(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAf6Reg_ds1_rxfrm_inband_lpc_sta_Base;
    }

static uint8 RxHwLoopcodeGet(ThaPdhDe1 self)
    {
    uint32 address = RxFramerInbandLpcStatus(self) + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    return (uint8)(regVal & cAf6_ds1_rxfrm_inband_lpc_sta_iblpc_sta_Mask);
    }

static eAtPdhDe1Loopcode RxLoopcodeGet(AtPdhDe1 self)
    {
    uint8 rxLoopcode;

    if (!ThaModulePdhInbandLoopcodeIsSupported(PdhModule((AtChannel)self)) || AtPdhDe1IsE1((AtPdhDe1)self))
        return cAtPdhDe1LoopcodeInvalid;

    rxLoopcode = mMethodsGet(mThis(self))->RxHwLoopcodeGet(mThis(self));
    return RxInbandLoopCodeHw2Sw(rxLoopcode);
    }

static eBool IdleSignalIsSupported(AtPdhChannel self)
    {
    return ThaModulePdhDe1IdleCodeInsertionIsSupported(PdhModule((AtChannel)self));
    }

static eAtRet IdleEnable(AtPdhChannel self, eBool enable)
    {
    if (enable && !CanChangeTxSignal((AtPdhDe1)self))
        return cAtErrorNotApplicable;

    if (!AtPdhChannelIdleSignalIsSupported(self))
        return cAtErrorModeNotSupport;

    return ThaModuleAbstractMapDe1IdleEnable(MapModule((AtPdhChannel)self), (AtPdhDe1)self, enable);
    }

static eBool IdleIsEnabled(AtPdhChannel self)
    {
    if (!AtPdhChannelIdleSignalIsSupported(self))
        return cAtFalse;

    return ThaModuleAbstractMapDe1IdleIsEnabled(MapModule((AtPdhChannel)self), (AtPdhDe1)self);
    }

static eAtRet IdleCodeSet(AtPdhChannel self, uint8 idleCode)
    {
    if (!CanChangeTxSignal((AtPdhDe1)self))
        return cAtErrorNotApplicable;

    if (!AtPdhChannelIdleSignalIsSupported(self))
        return cAtErrorModeNotSupport;

    return ThaModuleAbstractMapDe1IdleCodeSet(MapModule((AtPdhChannel)self), (AtPdhDe1)self, idleCode);
    }

static uint8 IdleCodeGet(AtPdhChannel self)
    {
    if (!AtPdhChannelIdleSignalIsSupported(self))
        return 0;

    return ThaModuleAbstractMapDe1IdleCodeGet(MapModule((AtPdhChannel)self), (AtPdhDe1)self);
    }

static uint32 TxFramerDlkInsBomControl(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAf6Reg_dej1_tx_framer_dlk_ins_bom_ctrl_Base;
    }

static uint32 UserNtimesToLocalNtimes(AtPdhDe1 self, uint32 nTimes)
    {
    uint32 maxNtimes = AtPdhDe1TxBomSentMaxNTimes(self);
    if (nTimes >= maxNtimes)
        return 0xF; /* send continuously */
    return nTimes;
    }

static eAtRet TxHwBomSet(ThaPdhDe1 self, uint8 message, uint32 threshold)
    {
    uint32 address = TxFramerDlkInsBomControl(self) + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 localMaxNtimes = UserNtimesToLocalNtimes((AtPdhDe1)self, threshold);
    uint32 oldNtimes = mRegField(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_);

    if ((oldNtimes == 0xF) && (localMaxNtimes == 0))
        {
        mRegFieldSet(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_, 1);
        mRegFieldSet(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_, localMaxNtimes);
        mChannelHwWrite(self, address, regVal, cAtModulePdh);
        AtOsalUSleep(50000);
        }

    mRegFieldSet(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMEn_, 1);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_, localMaxNtimes);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_, message);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static uint32 TxHwBomThresGet(ThaPdhDe1 self)
    {
    uint32 address = TxFramerDlkInsBomControl(self) + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);

    return (uint32)mRegField(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMRptTime_);
    }

static eBool IsRetimingBomEnabled(AtPdhDe1 self)
    {
    if (AtPdhChannelTxSlipBufferIsEnabled((AtPdhChannel)self) &&
        ThaPdhRetimingControllerSlipBufferSsmIsEnabled(RetimingControllerGet((ThaPdhDe1)self)))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 TxBomSentCfgNTimesGet(AtPdhDe1 self)
    {
    if (!BomIsSupported(mThis(self)))
        return 0;

    if (IsRetimingBomEnabled(self))
        return ThaPdhRetimingControllerSlipBufferTxSsmSentThresholdGet(RetimingControllerGet((ThaPdhDe1)self));

    return mMethodsGet(mThis(self))->TxHwBomThresGet(mThis(self));
    }

static eAtModulePdhRet TxBomNTimesSend(AtPdhDe1 self, uint8 bomCode, uint32 nTimes)
    {
    if (!CanChangeTxSignal(self) && (nTimes != 0))
        return cAtErrorNotApplicable;

    if (!BomIsSupported(mThis(self)))
        return cAtErrorModeNotSupport;

    if (IsRetimingBomEnabled(self))
        return ThaPdhRetimingControllerSlipBufferSsmBomSend(RetimingControllerGet((ThaPdhDe1)self), bomCode, nTimes);

    return mMethodsGet(mThis(self))->TxHwBomSet(mThis(self), bomCode, nTimes);
    }

static eAtModulePdhRet TxBomWithModeSet(AtPdhDe1 self, uint8 bomCode, eAtPdhBomSentMode sentMode)
    {
    static uint32 cThreshold[] = {cThaPdhMinBomThreshold,
                                  cThaPdhStdBomThreshold,
                                  cThaPdhMaxBomThreshold };
    if (sentMode < mCount(cThreshold))
        return TxBomNTimesSend(self, bomCode, cThreshold[sentMode]);

    return cAtErrorInvlParm;
    }

static eAtPdhBomSentMode TxBomModeGet(AtPdhDe1 self)
    {
    uint32 bomThreshold = TxBomSentCfgNTimesGet(self);

    if (bomThreshold == 0)
        return cAtPdhBomSentModeInvalid;

    if (bomThreshold < cThaPdhMaxBomThreshold)
        return cAtPdhBomSentModeOneShot;

    return cAtPdhBomSentModeContinuous;
    }

static eAtModulePdhRet TxBomSet(AtPdhDe1 self, uint8 bomCode)
    {
    if (!CanChangeTxSignal(self))
        return cAtErrorNotApplicable;

    if (!BomIsSupported(mThis(self)))
        return cAtErrorModeNotSupport;

    if (IsRetimingBomEnabled(self))
        return ThaPdhRetimingControllerSlipBufferSsmBomSend(RetimingControllerGet((ThaPdhDe1)self), bomCode, cThaPdhStdBomThreshold);

    return mMethodsGet(mThis(self))->TxHwBomSet(mThis(self), bomCode, cThaPdhStdBomThreshold);
    }

static uint8 TxHwBomGet(ThaPdhDe1 self)
    {
    uint32 address = TxFramerDlkInsBomControl(self) + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cAf6_dej1_tx_framer_dlk_ins_bom_ctrl_DlkBOMMsg_);
    }

static uint8 TxBomGet(AtPdhDe1 self)
    {
    if (!BomIsSupported(mThis(self)))
        return 0;

    if (IsRetimingBomEnabled(self))
        return ThaPdhRetimingControllerSlipBufferTxSsmGet(RetimingControllerGet((ThaPdhDe1)self));

    return mMethodsGet(mThis(self))->TxHwBomGet(mThis(self));
    }

static uint32 RxFramerDlkBomStatus(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAf6Reg_ds1_rxfrm_fdl_lpc_sta_Base;
    }

static uint8 RxHwBomGet(ThaPdhDe1 self)
    {
    uint32 address = RxFramerDlkBomStatus(self) + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cAf6_ds1_rxfrm_fdl_lpc_sta_bom_6bitpat_);
    }

static uint8 RxHwCurrentBomGet(ThaPdhDe1 self)
    {
    uint32 address = RxFramerDlkBomStatus(self) + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint8 bomVal = (uint8)mRegField(regVal, cAf6_ds1_rxfrm_fdl_lpc_sta_bom_6bitpat_);
    uint8 currentIdle = (uint8)mRegField(regVal, cAf6_ds1_rxfrm_fdl_lpc_sta_bom_idle_);

    if (currentIdle == 0)
        return cAtPdhDe1FdlIdleFlag;
    return bomVal;
    }

static uint8 RxBomGet(AtPdhDe1 self)
    {
    if (BomIsSupported(mThis(self)))
        return mMethodsGet(mThis(self))->RxHwBomGet(mThis(self));
    return 0;
    }

static uint8 RxCurrentBomGet(AtPdhDe1 self)
    {
    if (BomIsSupported(mThis(self)))
        return mMethodsGet(mThis(self))->RxHwCurrentBomGet(mThis(self));
    return cAtPdhDe1FdlIdleFlag;
    }

static AtPrbsEngine LinePrbsEngineGet(AtPdhChannel self)
    {
    if (mThis(self)->linePrbsEngine == NULL)
        mThis(self)->linePrbsEngine = ThaModulePdhDe1LinePrbsEngineCreate(PdhModule((AtChannel)self), (AtPdhDe1)self);
    return mThis(self)->linePrbsEngine;
    }

static eBool SaBitIsValid(uint8 saBit)
	{
    switch (saBit)
        {
        case cAtPdhE1Sa8 : return cAtTrue;
        case cAtPdhE1Sa7 : return cAtTrue;
        case cAtPdhE1Sa6 : return cAtTrue;
        case cAtPdhE1Sa5 : return cAtTrue;
        case cAtPdhE1Sa4 : return cAtTrue;
        case 0           : return cAtTrue;

        default:
            return cAtFalse;
        }
	}

static uint8 RxSaBitSw2Hw(uint8 saBitsMask)
    {
    switch (saBitsMask)
        {
        case cAtPdhE1Sa8  : return 0x7;
        case cAtPdhE1Sa7  : return 0x6;
        case cAtPdhE1Sa6  : return 0x5;
        case cAtPdhE1Sa5  : return 0x4;
        case cAtPdhE1Sa4  : return 0x3;
        default:
            return 0x00;
        }
    }

static uint8 RxSaBitHw2Sw(uint8 hwValue)
    {
    switch (hwValue)
        {
        case 0x7  : return cAtPdhE1Sa8;
        case 0x6  : return cAtPdhE1Sa7;
        case 0x5  : return cAtPdhE1Sa6;
        case 0x4  : return cAtPdhE1Sa5;
        case 0x3  : return cAtPdhE1Sa4;
        default:
            return 0x00;
        }
    }

static eAtRet SsmRxSaBitsMaskSet(ThaPdhDe1 self, uint8 saBitsMask)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self , address, cAtModulePdh);
    mRegFieldSet(regVal, cThaRxDE1SaCfg, RxSaBitSw2Hw(saBitsMask));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet SsmTxSaBitsMaskSet(ThaPdhDe1 self, uint8 saBitsMask)
    {
    uint32 fieldMask = mFieldMask(self, TxFrmrE1SaCfg);
    uint8 fieldShift = mFieldShift(self, TxFrmrE1SaCfg);
    uint32 address = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self , address, cAtModulePdh);
    mRegFieldSet(regVal, field, RxSaBitSw2Hw(saBitsMask));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet HwSsmSaBitsMaskSet(ThaPdhDe1 self, uint8 saBit)
    {
    eAtRet ret = cAtOk;

    /* TX: set both location: slipbuffer and tx framer */
    if (ThaPdhDe1RetimingIsSupported(self))
        ThaPdhDe1RetimingControllerSlipBufferSsmSaBitsMaskSet((ThaPdhDe1RetimingController)RetimingControllerGet(self), saBit);

    /* TX */
    ret |= SsmTxSaBitsMaskSet(self, saBit);

    /* RX */
    ret |= SsmRxSaBitsMaskSet(self, saBit);

    return ret;
    }

static eAtRet SsmSaBitsMaskSet(AtPdhDe1 self, uint8 saBit)
    {
    if (!ThaModulePdhDe1SsmIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self)))
        return cAtErrorModeNotSupport;

    if (!SaBitIsValid(saBit))
        return cAtErrorInvlParm;

    HwSsmSaBitsMaskSet((ThaPdhDe1)self, saBit);

    return cAtOk;
    }

static uint8 SsmSaBitsMaskGet(AtPdhDe1 self)
    {
    if (ThaPdhDe1RetimingIsSupported((ThaPdhDe1)self))
        return ThaPdhDe1RetimingControllerSlipBufferSsmSaBitsMaskGet((ThaPdhDe1RetimingController)RetimingControllerGet((ThaPdhDe1)self));

    if (ThaModulePdhDe1SsmIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self)))
        return SsmTxHwSaBitMaskGet((ThaPdhDe1)self);

    return 0;
    }

static void E1CrcSsmSaDefault(AtPdhChannel self, eBool oldEnable, eBool newEnable)
    {
    if ((AtPdhChannelFrameTypeGet(self) == cAtPdhE1MFCrc) && (mBoolToBin(oldEnable) != mBoolToBin(newEnable)))
        {
        if (oldEnable && !newEnable)
            AtPdhDe1SsmSaBitsMaskSet((AtPdhDe1)self, 0);
        else if (!oldEnable && newEnable)
            AtPdhDe1SsmSaBitsMaskSet((AtPdhDe1)self, cAtPdhE1Sa4);
        }
    }

static eAtRet SsmEnable(AtPdhChannel self, eBool enable)
    {
    eBool oldEnable = cAtFalse;
    eAtRet ret = cAtOk;

    if (enable && !CanChangeTxSignal((AtPdhDe1)self))
        return cAtErrorNotApplicable;

    if (!SsmIsSupported(self))
        return cAtErrorModeNotSupport;

    /* If slip buffer is enabled, SSM is inserted from slip buffer engine */
    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        {
        ret = mMethodsGet(mThis(self))->SsmHwEnable(mThis(self), cAtFalse);
        oldEnable = AtPdhChannelSsmIsEnabled(self);
        ret |= ThaPdhRetimingControllerSlipBufferSsmEnable(RetimingControllerGet((ThaPdhDe1)self), enable);
        E1CrcSsmSaDefault(self, oldEnable, enable);
        return ret;
        }

    oldEnable = AtPdhChannelSsmIsEnabled(self);
    ret = mMethodsGet(mThis(self))->SsmHwEnable(mThis(self), enable);
    E1CrcSsmSaDefault(self, oldEnable, enable);
    return ret;
    }

static eAtRet TxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    eAtRet ret;

    if (!ThaPdhDe1RetimingIsSupported((ThaPdhDe1)self))
        return enable ? cAtErrorModeNotSupport : cAtOk;

    ret = ThaPdhRetimingControllerSlipBufferEnable(RetimingControllerGet((ThaPdhDe1)self), enable);
    if (TxRetimingSignalingIsSupported((AtPdhDe1)self))
        {
        eBool signalingEnable = (enable && AtPdhDe1SignalingIsEnabled((AtPdhDe1)self)) ? cAtTrue : cAtFalse;
        ret |= ThaPdhDe1RetimingControllerSlipBufferTxSignalingEnable(mDe1RetimingController(self), signalingEnable);
        }

    return ret;
    }

static eBool TxSlipBufferIsEnabled(AtPdhChannel self)
    {
    if (ThaPdhDe1RetimingIsSupported((ThaPdhDe1)self))
        return ThaPdhRetimingControllerSlipBufferIsEnabled(RetimingControllerGet((ThaPdhDe1)self));
    return cAtFalse;
    }

static eBool SsmIsEnabled(AtPdhChannel self)
    {
    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        return ThaPdhRetimingControllerSlipBufferSsmIsEnabled(RetimingControllerGet((ThaPdhDe1)self));
    return mMethodsGet(mThis(self))->SsmHwIsEnabled(mThis(self));
    }

static eAtRet TxSsmSet(AtPdhChannel self, uint8 value)
    {
    if (!CanChangeTxSignal((AtPdhDe1)self))
        return cAtErrorNotApplicable;

    if (!ThaModulePdhDe1SsmIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self)) || !SsmIsSupported(self))
        return cAtErrorModeNotSupport;

    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        return ThaPdhRetimingControllerSlipBufferSsmBomSend(RetimingControllerGet((ThaPdhDe1)self), value, cThaPdhStdBomThreshold);

    if (AtPdhDe1IsE1((AtPdhDe1)self))
        return TxE1SsmSet((AtChannel)self, value);

    return AtPdhDe1TxBomSet((AtPdhDe1)self, value);
    }

static uint8 TxSsmGet(AtPdhChannel self)
    {
    if (AtPdhChannelTxSlipBufferIsEnabled(self))
        return ThaPdhRetimingControllerSlipBufferTxSsmGet(RetimingControllerGet((ThaPdhDe1)self));

    if (AtPdhDe1IsE1((AtPdhDe1)self))
        return TxE1SsmGet((AtChannel)self);

    return AtPdhDe1TxBomGet((AtPdhDe1)self);
    }

static uint32 RxFrmrStatE1SsmMask(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Mask;
    }

static uint8 RxFrmrStatE1SsmShift(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAf6_dej1_rxfrm_hw_stat_RXE1SSMSta_Shift;
    }

static uint8 RxE1SsmGet(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    uint32 baseOffset = De1RxFramerHwStatusRegister((AtPdhChannel)self);
    uint32 address = baseOffset + mDs1E1J1DefaultOffset(self);
    uint32 regVal  = mChannelHwRead(self , address, cAtModulePdh);
    uint32 rxE1SsmStaMask = mFieldMask(de1, RxFrmrStatE1Ssm);
    uint8 rxE1SsmStaShift = mFieldShift(de1, RxFrmrStatE1Ssm);
    return (uint8)mRegField(regVal, rxE1SsmSta);
    }

static uint8 RxSsmGet(AtPdhChannel self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)self;

    if (AtPdhDe1IsE1(de1))
        return RxE1SsmGet(de1);

    return AtPdhDe1RxBomGet(de1);
    }

static uint32 RxSlipBufferRegAddress(AtPdhChannel self)
    {
    uint32 address = cAf6Reg_rxslipbuf_pen_Base;
    address = address + ThaPdhDe1DefaultOffset((ThaPdhDe1)self);
    return address;
    }

static uint32 RxSlipBufferCounterRegAddress(AtPdhChannel self, eBool r2c)
    {
    uint32 roOffset = ThaModulePdhRxSlipBufferCounterRoOffset((ThaModulePdh)AtChannelModuleGet((AtChannel)self), r2c);
    uint32 address = cAf6Reg_rxfrm_rxslip_cnt_Base + roOffset;
    address = address + ThaPdhDe1DefaultOffset((ThaPdhDe1)self);
    return address;
    }

static uint32 RxSlipBufferCounterGet(AtPdhChannel self, eBool r2c)
    {
    uint32 address = RxSlipBufferCounterRegAddress(self, r2c);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    return regVal;
    }

static uint32 FrameTypeToRxSlipBufferFrameType(uint16 frameType)
    {
    if (AtPdhDe1FrameTypeIsE1(frameType))
        return 0x2;
    if (frameType == cAtPdhDs1FrmSf)
        return 0x1;
    if (frameType == cAtPdhDs1FrmEsf)
        return 0x3;
    return 0x1;
    }

static eAtRet HwRxSlipBufferFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    uint32 address = RxSlipBufferRegAddress(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_rxslipbuf_pen_rxslpmd_, FrameTypeToRxSlipBufferFrameType(frameType));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static eAtRet HwRxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    uint32 address = RxSlipBufferRegAddress(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_rxslipbuf_pen_rxslpen_, mBoolToBin(enable));
    mChannelHwWrite(self, address, regVal, cAtModulePdh);
    return cAtOk;
    }

static eAtRet HwRxSlipBufferIsEnabled(AtPdhChannel self)
    {
    uint32 address = RxSlipBufferRegAddress(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 ret = mRegField(regVal, cAf6_rxslipbuf_pen_rxslpen_);
    return (eBool)mBinToBool(ret);
    }

static eBool RxSlipBufferIsSupported(AtPdhChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    return ThaModulePdhDe1RxSlipBufferIsSupported(module);
    }

static eAtRet RxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    if (!AtPdhChannelRxSlipBufferIsSupported(self))
        return cAtErrorModeNotSupport;
    return HwRxSlipBufferEnable(self, enable);
    }

static eBool  RxSlipBufferIsEnabled(AtPdhChannel self)
    {
    if (!AtPdhChannelRxSlipBufferIsSupported(self))
        return cAtFalse;
    return HwRxSlipBufferIsEnabled(self);
    }


static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPdhDe1 object = (ThaPdhDe1)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(timeslotBitmap);
    mEncodeUInt(signalingBitMap);
    mEncodeUInt(signalingEnabled);
    mEncodeUInt(frameType);
    mEncodeObject(linePrbsEngine);
    mEncodeObject(prmController);
    mEncodeObject(surEngine);
    mEncodeUInt(txAisExplicitlyForced);

    mEncodeObjectDescription(cdrController);
    mEncodeObjectDescription(prbsEngine);
    mEncodeObjectDescription(clockExtractor);
    mEncodeObject(retimingController);
    mEncodeList(allNxDs0s);
    mEncodeNone(attController);
    }

static eBool De1PrmIsSupported(AtPdhDe1 self)
    {
    if ((AtPdhChannelFrameTypeGet((AtPdhChannel)self) == cAtPdhDs1FrmEsf) &&
        ThaModulePdhPrmIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self)))
        return cAtTrue;
    return cAtFalse;
    }

static AtPdhPrmController PrmControllerGet(AtPdhDe1 self)
    {
    if (!De1PrmIsSupported(self))
        return NULL;

    if (mThis(self)->prmController == NULL)
        {
        mThis(self)->prmController = ThaModulePdhPrmControllerCreate(PdhModule((AtChannel)self), self);
        AtPdhPrmControllerInit(mThis(self)->prmController);
        }

    return mThis(self)->prmController;
    }

static eAtRet LosDetectionByAllZeroPatternEnable(AtPdhChannel self, eBool enable)
    {
    tThaCfgPdhDs1E1FrmRxThres defaultThreshold;
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint32 losThreshold;

    mMethodsGet(mThis(self))->DefaultThreshold(mThis(self), &defaultThreshold);
    losThreshold = enable ? defaultThreshold.losCntThres : 0;
    mFieldIns(&regVal, cThaRxDE1LosCntThresMask, cThaRxDE1LosCntThresShift, losThreshold);
    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet LosDetectionEnable(AtPdhChannel self, eBool enable)
    {
    if (ThaModulePdhDetectDe1LosByAllZeroPattern(PdhModule((AtChannel)self)))
        return LosDetectionByAllZeroPatternEnable(self, enable);

    return m_AtPdhChannelMethods->LosDetectionEnable(self, enable);
    }

static eBool LosDetectionIsEnabled(AtPdhChannel self)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint8 losThreshold = (uint8)mRegField(regVal, cThaRxDE1LosCntThres);

    return (losThreshold == 0) ? cAtFalse : cAtTrue;
    }

static eAtRet AisAllOnesDetectionEnable(AtPdhChannel self, eBool enable)
    {
    uint32 aisThreshold;
    tThaCfgPdhDs1E1FrmRxThres defaultThreshold;
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);

    mMethodsGet(mThis(self))->DefaultThreshold(mThis(self), &defaultThreshold);
    aisThreshold = enable ? defaultThreshold.aisThres : 0;
    mFieldIns(&regVal,
              cThaRxDE1AisCntThresMask,
              cThaRxDE1AisCntThresShift,
              aisThreshold);

    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool AisAllOnesDetectionIsEnabled(AtPdhChannel self)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);
    uint8 aisThreshold = (uint8) mRegField(regVal, cThaRxDE1AisCntThres);

    return (aisThreshold == 0) ? cAtFalse : cAtTrue;
    }

static AtErrorGenerator ValidErrorGeneratorGet(AtPdhChannel self, AtErrorGenerator generator,
                                               void (*PdhChannelErrorGeneratorRefSet)(AtPdhChannel channel, AtErrorGenerator generator))
    {
    AtPdhChannel source = NULL;

    if (generator == NULL)
        return generator;

    source = (AtPdhChannel)AtErrorGeneratorSourceGet(generator);
    if (source == self)
        return generator;

    if (source != NULL)
        PdhChannelErrorGeneratorRefSet(source, NULL);

    if (source == NULL || !AtErrorGeneratorIsStarted(generator))
        {
        PdhChannelErrorGeneratorRefSet(self, generator);
        AtErrorGeneratorSourceSet(generator, (AtObject)self);
        AtErrorGeneratorInit(generator);
        return generator;
        }

    return NULL;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtPdhChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtErrorGenerator generator = ThaModulePdhDe1TxErrorGeneratorGet(module);

    return ValidErrorGeneratorGet(self, generator, AtPdhChannelTxErrorGeneratorRefSet);
    }

static AtErrorGenerator RxErrorGeneratorGet(AtPdhChannel self)
    {
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtErrorGenerator generator = ThaModulePdhDe1RxErrorGeneratorGet(module);

    return ValidErrorGeneratorGet(self, generator, AtPdhChannelRxErrorGeneratorRefSet);
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = mMethodsGet((ThaPdhDe1)self)->EncapDs0BitMaskGet((ThaPdhDe1)self);
    return ThaModuleAbstractMapDs0EncapConnectionEnable(
                                    (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap),
                                    self, mask, enable);
    }

static eBool TxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = mMethodsGet((ThaPdhDe1)self)->EncapDs0BitMaskGet((ThaPdhDe1)self);
    return ThaModuleAbstractMapDs0EncapConnectionIsEnabled(
                                    (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleMap),
                                    self, mask);
    }

static eAtRet RxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = mMethodsGet((ThaPdhDe1)self)->EncapDs0BitMaskGet((ThaPdhDe1)self);
    return ThaModuleAbstractMapDs0EncapConnectionEnable(
                                    (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap),
                                    self, mask, enable);
    }

static eBool RxEncapConnectionIsEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 mask = mMethodsGet((ThaPdhDe1)self)->EncapDs0BitMaskGet((ThaPdhDe1)self);
    return ThaModuleAbstractMapDs0EncapConnectionIsEnabled(
                                    (ThaModuleAbstractMap)AtDeviceModuleGet(device, cThaModuleDemap),
                                    self, mask);
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelOnly)
    {
    /* Although high-layer stopped its alarm forwarding, but HW sometimes relays
     * the alarm forwarding status of DS1/E1 channels for a moment so that DS1/E1
     * framer can be re-aligned. Therefore, we cannot check this status bit
     * for alarm forwarding clearance reporting. */
    uint32 defect = DefectGetAndFilter((AtChannel)self, HwDefectGet((AtChannel)self));
    AtUnused(subChannelOnly);
    if (defect & AtPdhChannelDefectsCauseAlarmForwarding(self))
        return;

    AtChannelAllAlarmListenersCall((AtChannel)self, cAtPdhDe1AlarmAis, 0);
    }

static uint32 ErrorGeneratorHwId(ThaPdhDe1 self)
    {
    uint8 hwSlice24, hwSts24, hwVtg, hwVt;
    ThaPdhDe1HwIdGet(self, cAtModulePdh, &hwSlice24, &hwSts24,  &hwVtg, &hwVt);
    return (uint32) ((hwSts24 << 5) + (hwVtg << 2) + hwVt);
    }

static eAtRet CascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5)
    {
    *layer5 = 0;
    return ThaPdhDe1HwIdFactorGet((ThaPdhDe1)self, phyModule, layer1, layer2, layer3, layer4);
    }

static eAtRet SwFrameTypeSet(ThaPdhDe1 self, uint16 frameType)
    {
    self->frameType = frameType;
    return cAtOk;
    }

static eAtClockState HwClockStateTranslate(AtChannel self, uint8 hwState)
    {
    ThaModuleCdr moduleCdr = (ThaModuleCdr)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleCdr);

    return ThaModuleCdrClockStateFromLoChannelHwStateGet(moduleCdr, hwState);
    }

static eAtRet HwIdConvert(ThaPdhDe1 self, uint32 phyModule, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    eAtRet ret = ThaPdhChannelHwIdGet((AtPdhChannel)self, phyModule, hwSlice, hwSts);

    *hwVtg = 0;
    *hwVt  = *hwSts;
    *hwSts = 0;

    return ret;
    }

static eAtRet AlarmAffectingHwEnable(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);

    /* Although LOF affecting can be disabled only in case SAToP Framing
     * monitoring, but we do not include the constraint here for freely
     * configuration sequence. */
    if (alarmType & cAtPdhDe1AlarmLof)
        mRegFieldSet(regVal, cThaRxDE1LOFFrwDis, (enable) ? 0 : 1);

    if (alarmType & cAtPdhDe1AlarmAis)
        mRegFieldSet(regVal, cThaRxDE1AisMaskEn, (enable) ? 1 : 0);

    if (alarmType & cAtPdhDe1AlarmLomf)
        {
        if (ThaModulePdhDe1LomfConsequentialActionIsSupported(PdhModule((AtChannel)self)))
            mRegFieldSet(regVal, cThaRxDE1LomfDstrEn, (enable) ? 1 : 0);
        }

    mChannelHwWrite(self, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtRet AlarmAffectingEnable(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    if (AtPdhChannelCanChangeAlarmAffecting(self, alarmType, enable))
        return AlarmAffectingHwEnable(self, alarmType, enable);

    return cAtErrorModeNotSupport;
    }

static eBool AlarmAffectingIsEnabled(AtPdhChannel self, uint32 alarmType)
    {
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, address, cAtModulePdh);

    if (alarmType & cAtPdhDe3AlarmLos)
        return cAtTrue;

    if (alarmType & cAtPdhDe1AlarmLof)
        return (regVal & cThaRxDE1LOFFrwDisMask) ? cAtFalse : cAtTrue;

    if (alarmType & cAtPdhDe3AlarmAis)
        return (regVal & cThaRxDE1AisMaskEnMask) ? cAtTrue : cAtFalse;

    if (alarmType & cAtPdhDe1AlarmLomf)
        {
        if (ThaModulePdhDe1LomfConsequentialActionIsSupported(PdhModule((AtChannel)self)))
            return (regVal & cThaRxDE1LomfDstrEnMask) ? cAtTrue : cAtFalse;
        }

    return cAtFalse;
    }

static eBool CanChangeAlarmAffecting(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    static const uint32 alarmsAffecting = cAtPdhDe1AlarmLof|cAtPdhDe1AlarmAis;

    /* LOS always causes AIS forwarding. */
    if (alarmType & cAtPdhDe1AlarmLos)
        return enable;

    if (alarmType & alarmsAffecting)
            return cAtTrue;

    if (alarmType & cAtPdhDe1AlarmLomf)
        {
        if (ThaModulePdhDe1LomfConsequentialActionIsSupported(PdhModule((AtChannel)self)))
            return cAtTrue;
        }

    return enable ? cAtFalse : cAtTrue;
    }

static uint32 AlarmAffectingMaskGet(AtPdhChannel self)
    {
        uint32 address = cThaRegDS1E1J1RxFrmrCtrl + mDs1E1J1DefaultOffset(self);
        uint32 regVal  = mChannelHwRead(self, address, cAtModulePdh);
    uint32 alarms  = cAtPdhDe1AlarmLos;

    /* Although LOF affecting can be disabled only in case SAToP Framing
     * monitoring, but we do not include the constraint here for freely
     * configuration sequence. */
        if (mRegField(regVal, cThaRxDE1LOFFrwDis) == 0)
            alarms |= cAtPdhDe1AlarmLof;

    if (regVal & cThaRxDE1AisMaskEnMask)
            alarms |= cAtPdhDe1AlarmAis;

    if (ThaModulePdhDe1LomfConsequentialActionIsSupported(PdhModule((AtChannel)self)))
        {
        if (regVal & cThaRxDE1LomfDstrEnMask)
            alarms |= cAtPdhDe1AlarmLomf;
        }

    return alarms;
    }

static eAtRet TxSquelch(AtChannel self, eBool squelched)
    {
    if (squelched)
        return HelperTxAlarmForce(self, cAtPdhDe1AlarmAis, cAtTrue);

    return HelperTxAlarmForce(self, cAtPdhDe1AlarmAis, mThis(self)->txAisExplicitlyForced);
    }

static eAtRet DidBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwDidBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static eAtRet WillBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    return ThaPrbsEnginePwWillBind((ThaPrbsEngine)AtChannelPrbsEngineGet(self), pseudowire);
    }

static ThaModuleConcate ModuleConcate(AtChannel self)
    {
    return (ThaModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleConcateBindDe1ToSourceConcateGroup(ModuleConcate(self), self, group);
    ret |= ThaModuleAbstractMapBindDe1ToConcateGroup(MapModule((AtPdhChannel)self), (AtPdhDe1)self, group);

    return ret;
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    eAtRet ret = cAtOk;

    ret |= ThaModuleConcateBindDe1ToSinkConcateGroup(ModuleConcate(self), self, group);
    ret |= ThaModuleAbstractMapBindDe1ToConcateGroup(DemapModule((AtPdhChannel)self), (AtPdhDe1)self, group);

    return ret;
    }

static uint32 BoundConcateHwIdGet(AtChannel self)
    {
    return ThaModuleAbstractMapDe1BoundConcateHwIdGet(MapModule((AtPdhChannel)self), (AtPdhDe1)self);
    }

static void OverrideAtObject(ThaPdhDe1 self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(ThaPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, TimingModeIsSupported);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, TimingSourceGet);
        mMethodOverride(m_AtChannelOverride, ClockStateGet);
        mMethodOverride(m_AtChannelOverride, HwClockStateGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SpecificDefectInterruptClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, AllConfigSet);
        mMethodOverride(m_AtChannelOverride, AllConfigGet);
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, EncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, NumBlocksNeedToBindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, PppBlockRange);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, MlpppBlockRange);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, MlpppBlockRange);
        mMethodOverride(m_AtChannelOverride, SurEngineAddress);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, BoundPwHwIdGet);
        mMethodOverride(m_AtChannelOverride, BoundEncapHwIdGet);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, TxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionEnable);
        mMethodOverride(m_AtChannelOverride, RxEncapConnectionIsEnabled);
        mMethodOverride(m_AtChannelOverride, AttController);
        mMethodOverride(m_AtChannelOverride, HwClockStateTranslate);
        mMethodOverride(m_AtChannelOverride, PwTimingRestore);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, TxSquelch);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, DidBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, WillBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        mMethodOverride(m_AtChannelOverride, BindToSinkGroup);
        mMethodOverride(m_AtChannelOverride, BoundConcateHwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(ThaPdhDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, mMethodsGet(channel), sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeGet);
        mMethodOverride(m_AtPdhChannelOverride, LineCodeSet);
        mMethodOverride(m_AtPdhChannelOverride, LineCodeGet);
        mMethodOverride(m_AtPdhChannelOverride, LedStateSet);
        mMethodOverride(m_AtPdhChannelOverride, LedStateGet);
        mMethodOverride(m_AtPdhChannelOverride, DefaultFrameMode);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, AutoRaiEnable);
        mMethodOverride(m_AtPdhChannelOverride, AutoRaiIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, LinePrbsEngineGet);
        mMethodOverride(m_AtPdhChannelOverride, LedStateIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, TxSlipBufferEnable);
        mMethodOverride(m_AtPdhChannelOverride, TxSlipBufferIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, TxSsmSet);
        mMethodOverride(m_AtPdhChannelOverride, TxSsmGet);
        mMethodOverride(m_AtPdhChannelOverride, RxSsmGet);
        mMethodOverride(m_AtPdhChannelOverride, SsmEnable);
        mMethodOverride(m_AtPdhChannelOverride, SsmIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkEnable);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, LosDetectionEnable);
        mMethodOverride(m_AtPdhChannelOverride, LosDetectionIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, TxErrorGeneratorGet);
        mMethodOverride(m_AtPdhChannelOverride, RxErrorGeneratorGet);
        mMethodOverride(m_AtPdhChannelOverride, HasParentAlarmForwarding);
        mMethodOverride(m_AtPdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtPdhChannelOverride, CascadeHwIdGet);
        mMethodOverride(m_AtPdhChannelOverride, AisAllOnesDetectionEnable);
        mMethodOverride(m_AtPdhChannelOverride, AisAllOnesDetectionIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkFcsBitOrderSet);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkFcsBitOrderGet);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtPdhChannelOverride, IdleSignalIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, IdleEnable);
        mMethodOverride(m_AtPdhChannelOverride, IdleIsEnabled);
        mMethodOverride(m_AtPdhChannelOverride, IdleCodeSet);
        mMethodOverride(m_AtPdhChannelOverride, IdleCodeGet);
        mMethodOverride(m_AtPdhChannelOverride, RxSlipBufferIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, RxSlipBufferEnable);
        mMethodOverride(m_AtPdhChannelOverride, RxSlipBufferIsEnabled);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtPdhDe1(ThaPdhDe1 self)
    {
    AtPdhDe1 object = (AtPdhDe1)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhDe1Override, mMethodsGet(object), sizeof(m_AtPdhDe1Override));
        mMethodOverride(m_AtPdhDe1Override, SignalingEnable);
        mMethodOverride(m_AtPdhDe1Override, TxSignalingEnable);
        mMethodOverride(m_AtPdhDe1Override, RxSignalingEnable);
        mMethodOverride(m_AtPdhDe1Override, SignalingIsEnabled);
        mMethodOverride(m_AtPdhDe1Override, SignalingDs0MaskSet);
        mMethodOverride(m_AtPdhDe1Override, SignalingDs0MaskGet);
        mMethodOverride(m_AtPdhDe1Override, SignalingPatternSet);
        mMethodOverride(m_AtPdhDe1Override, SignalingPatternGet);
        mMethodOverride(m_AtPdhDe1Override, SignalingIsSupported);
        mMethodOverride(m_AtPdhDe1Override, NxDs0IsSupported);
        mMethodOverride(m_AtPdhDe1Override, NxDs0Create);
        mMethodOverride(m_AtPdhDe1Override, NxDs0Delete);
        mMethodOverride(m_AtPdhDe1Override, NxDs0Get);
        mMethodOverride(m_AtPdhDe1Override, NxDs0IteratorCreate);
        mMethodOverride(m_AtPdhDe1Override, TxLoopcodeSet);
        mMethodOverride(m_AtPdhDe1Override, TxLoopcodeGet);
        mMethodOverride(m_AtPdhDe1Override, RxLoopcodeGet);
        mMethodOverride(m_AtPdhDe1Override, TxBomNTimesSend);
        mMethodOverride(m_AtPdhDe1Override, TxBomSentCfgNTimesGet);
        mMethodOverride(m_AtPdhDe1Override, TxBomSet);
        mMethodOverride(m_AtPdhDe1Override, TxBomGet);
        mMethodOverride(m_AtPdhDe1Override, RxBomGet);
        mMethodOverride(m_AtPdhDe1Override, RxCurrentBomGet);
        mMethodOverride(m_AtPdhDe1Override, TxBomWithModeSet);
        mMethodOverride(m_AtPdhDe1Override, TxBomModeGet);
        mMethodOverride(m_AtPdhDe1Override, SsmSaBitsMaskSet);
        mMethodOverride(m_AtPdhDe1Override, SsmSaBitsMaskGet);
        mMethodOverride(m_AtPdhDe1Override, PrmControllerGet);
        mMethodOverride(m_AtPdhDe1Override, AutoTxAisEnable);
        mMethodOverride(m_AtPdhDe1Override, AutoTxAisIsEnabled);
        }

    mMethodsSet(object, &m_AtPdhDe1Override);
    }

static void Override(ThaPdhDe1 self)
    {
    OverrideAtPdhDe1(self);
    OverrideAtPdhChannel(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaPdhDe1 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Ds1E1J1DefaultOffset);
        mMethodOverride(m_methods, De1ErrCounterOffset);
        mMethodOverride(m_methods, EncapDs0BitMaskGet);
        mMethodOverride(m_methods, CdrControllerCreate);
        mMethodOverride(m_methods, PartId);
        mMethodOverride(m_methods, DefaultThreshold);
        mMethodOverride(m_methods, FlatId);
        mMethodOverride(m_methods, AutoTxAisEnabledByDefault);
        mMethodOverride(m_methods, TimeslotDefaultOffset);
        mMethodOverride(m_methods, SliceGet);
        mMethodOverride(m_methods, SatopDs0BitMaskGet);
        mMethodOverride(m_methods, RxHwLoopcodeGet);
        mMethodOverride(m_methods, TxHwBomSet);
        mMethodOverride(m_methods, TxHwBomThresGet);
        mMethodOverride(m_methods, TxHwBomGet);
        mMethodOverride(m_methods, RxHwBomGet);
        mMethodOverride(m_methods, RxHwCurrentBomGet);
        mMethodOverride(m_methods, HwFrameTypeAtModuleMapSet);
        mMethodOverride(m_methods, MapLineOffset);
        mMethodOverride(m_methods, ShortestVcGet);
        mMethodOverride(m_methods, Ds0OffsetFactorForMap);
        mMethodOverride(m_methods, HwIdFactorGet);
        mMethodOverride(m_methods, HwIdConvert);
        mMethodOverride(m_methods, ShouldConsiderLofWhenLos);
        mMethodOverride(m_methods, ShouldPreserveService);
        mMethodOverride(m_methods, RetimingIsSupported);
        mMethodOverride(m_methods, RetimingControllerCreate);
        mMethodOverride(m_methods, RxFrmrStatE1SsmMask);
        mMethodOverride(m_methods, RxFrmrStatE1SsmShift);
        mMethodOverride(m_methods, TxFrmrE1SaCfgMask);
        mMethodOverride(m_methods, TxFrmrE1SaCfgShift);
        mMethodOverride(m_methods, SsmHwEnable);
        mMethodOverride(m_methods, SsmHwIsEnabled);
        mMethodOverride(m_methods, HwCasIdleCodeSet);
        mMethodOverride(m_methods, HwCasIdleCodeGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtPdhDe1 ThaPdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPdhDe1));

    /* Super constructor */
    if (AtPdhDe1ObjectInit((AtPdhDe1)self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override((ThaPdhDe1)self);

    /* Setup methods */
    MethodsInit((ThaPdhDe1)self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtPdhDe1 ThaPdhDe1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPdhDe1));
    if (newDe1 == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhDe1ObjectInit(newDe1, channelId, module);
    }

/* Function to convert hardware flat ID local in slice */
uint32 ThaPdhDe1FlatId(ThaPdhDe1 self)
    {
    if (self)
        return mMethodsGet(self)->FlatId(self, cAtModulePdh);
    return 0;
    }

uint32 ThaPdhDe1FlatIdInPhyModule(ThaPdhDe1 self, uint32 phyModule)
    {
    if (self)
        return mMethodsGet(self)->FlatId(self, phyModule);
    return 0;
    }

ThaCdrController ThaPdhDe1CdrControllerGet(ThaPdhDe1 self)
    {
    if (self == NULL)
        return NULL;

    if (self->cdrController == NULL)
        self->cdrController = mMethodsGet(self)->CdrControllerCreate(self);

    return self->cdrController;
    }

uint32 ThaPdhDe1DefaultOffset(ThaPdhDe1 self)
    {
    return mMethodsGet(self)->Ds1E1J1DefaultOffset(self);
    }

uint32 ThaPdhDe1EncapDs0BitMaskGet(ThaPdhDe1 self)
    {
    return mMethodsGet(self)->EncapDs0BitMaskGet(self);
    }

uint32 ThaPdhDe1SatopDs0BitMaskGet(ThaPdhDe1 self)
    {
    return mMethodsGet(self)->SatopDs0BitMaskGet(self);
    }

uint8 ThaPdhDe1PartId(ThaPdhDe1 self)
    {
    return (uint8)(self ? mMethodsGet(self)->PartId(self) : 0);
    }

void ThaPdhDe1ClockExtractorSet(ThaPdhDe1 self, AtClockExtractor clockExtractor)
    {
    self->clockExtractor = clockExtractor;
    }

AtClockExtractor ThaPdhDe1ClockExtractorGet(ThaPdhDe1 self)
    {
    return self->clockExtractor;
    }

eBool ThaPdhDe1AutoTxAisEnabledByDefault(ThaPdhDe1 self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->AutoTxAisEnabledByDefault(self, pw);
    return cAtFalse;
    }

void ThaPdhDe1SignalingTransparent(ThaPdhDe1 self)
    {
    SignalingMultiframeEnable(self, cAtFalse);
    AllDs0SignalingDisable(self);
    SignalingBufferReset(self, 0x0);
    }

eAtRet ThaPdhDe1AutoInsertFBit(ThaPdhDe1 self, eBool autoInsert)
    {
    uint32 regAddr, regVal;

    if (self == NULL)
        return cAtErrorNullPointer;

    regAddr = cThaRegDS1E1J1TxFrmrCtrl + mDs1E1J1DefaultOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal, cThaTxDE1FbitBypassMask, cThaTxDE1FbitBypassShift, autoInsert ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

uint8 ThaPdhDe1SliceGet(ThaPdhDe1 self)
    {
    if (self)
        return mMethodsGet(self)->SliceGet(self);
    return 0;
    }

eBool ThaPdhDe1IsE1Signaling(AtPdhDe1 self)
    {
    return (mThis(self)->signalingEnabled && AtPdhDe1IsE1(self)) ? cAtTrue : cAtFalse;
    }

AtSurEngine ThaPdhDe1SurEngine(ThaPdhDe1 self)
    {
    return mThis(self)->surEngine;
    }

uint32 ThaPdhDe1ErrorGeneratorHwId(ThaPdhDe1 self)
    {
    if (self)
        return ErrorGeneratorHwId(self);
    return cInvalidUint32;
    }

uint32 ThaPdhDe1MapLineOffset(ThaPdhDe1 self)
    {
    if (self)
        return mMethodsGet(self)->MapLineOffset(self);
    return cBit31_0;
    }

uint8 ThaPdhDe1NumDs0Timeslots(AtPdhDe1 self)
    {
    return AtPdhDe1IsE1(self) ? 32 : 24;
    }

AtSdhChannel ThaPdhDe1ShortestVcGet(ThaPdhDe1 self)
    {
    return mMethodsGet(self)->ShortestVcGet(self);
    }

void ThaPdhDe1Ds0OffsetFactorForMap(ThaPdhDe1 self, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwVt)
    {
    mMethodsGet(self)->Ds0OffsetFactorForMap(self, slice, hwStsInSlice, hwVtg, hwVt);
    }

eAtRet ThaPdhDe1HwIdFactorGet(ThaPdhDe1 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1)
    {
    if (self)
        return mMethodsGet(self)->HwIdFactorGet(self, phyModule, slice, hwStsInSlice, hwVtg, hwDe1);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhDe1SwFrameTypeSet(ThaPdhDe1 self, uint16 frameType)
    {
    if (self)
        return SwFrameTypeSet(self, frameType);
    return cAtErrorNullPointer;
    }

void ThaPdhDe1CdrControllerDelete(ThaPdhDe1 self)
    {
    if (self == NULL)
        return;

    AtObjectDelete((AtObject)self->cdrController);
    self->cdrController = NULL;
    }

eAtRet ThaPdhDe1HwIdGet(ThaPdhDe1 self, uint32 phyModule, uint8 *hwSlice, uint8 *hwSts,  uint8 *hwVtg, uint8 *hwVt)
    {
    if (self)
        return mMethodsGet(mThis(self))->HwIdConvert(mThis(self), phyModule, hwSlice, hwSts, hwVtg, hwVt);
    return cAtError;
    }

uint8 ThaPdhDe1FrameModeSw2Hw(ThaPdhDe1 self, eAtPdhDe1FrameType swFrameMode)
    {
    AtUnused(self);
    return FrameModeSw2Hw(swFrameMode);
    }

eBool ThaPdhDe1RetimingIsSupported(ThaPdhDe1 self)
    {
    if (self)
        return mMethodsGet(self)->RetimingIsSupported(self);
    return cAtFalse;
    }

eAtRet ThaPdhDe1TxFramerBypass(ThaPdhDe1 self, eBool bypass)
    {
    if (self)
        return TxFramerBypass(self, bypass);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaPdhDe1MapBypass(ThaPdhDe1 self, eBool bypass)
    {
    if (self)
        return MapBypass(self, bypass);
    return cAtErrorObjectNotExist;
    }

uint8 ThaPdhDe1RxSaBitSw2Hw(uint8 saBitsMask)
    {
    return RxSaBitSw2Hw(saBitsMask);
    }

uint8 ThaPdhDe1RxSaBitHw2Sw(uint8 hwValue)
    {
    return RxSaBitHw2Sw(hwValue);
    }

eAtRet ThaPdhDe1RxFramerMonitorOnlyEnable(ThaPdhDe1 self, eBool enable)
    {
    if (self)
        return RxFramerMonitorOnlyEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

eAtRet  ThaPdhDe1BeforeChangeFrameTypeCheck(AtPdhChannel self, uint16 frameType)
    {
    if (self)
        return BeforeChangeFrameTypeCheck(self, frameType);

    return cAtErrorNullPointer;
    }

eAtRet ThaPdhDe1HwSsmSaBitsMaskSet(ThaPdhDe1 self, uint8 saBit)
    {
    if (self)
        return HwSsmSaBitsMaskSet(self, saBit);

    return cAtErrorNullPointer;
    }

uint8 ThaPdhDe1TxHwSaBitMaskGet(ThaPdhDe1 self)
    {
    return SsmTxHwSaBitMaskGet(self);
    }

eBool ThaPdhDe1SsmIsSupported(ThaPdhDe1 self)
    {
    if (self)
        return (eBool)(SsmIsSupported((AtPdhChannel)self) && ThaModulePdhDe1SsmIsSupported((ThaModulePdh)AtChannelModuleGet((AtChannel)self)));

    return cAtFalse;
    }

eAtRet ThaPdhDe1DbSignalingEnabled(ThaPdhDe1 self, eBool en)
     {
     if (self)
         {
         mThis(self)->signalingEnabled = en;
         return cAtOk;
         }
     return cAtError;
     }

eAtRet ThaPdhDe1FastFrameSearchEnable(ThaPdhDe1 self, eBool enable)
    {
    if (self)
        return FastFrameSearchEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhDe1FastFrameSearchIsEnabled(ThaPdhDe1 self)
    {
    if (self)
        return FastFrameSearchIsEnabled(self);
    return cAtFalse;
    }

uint32 ThaPdhDe1TimeslotDefaultOffset(ThaPdhDe1 self)
    {
    return mMethodsGet(self)->TimeslotDefaultOffset(self);
    }

eAtRet ThaPdhDe1HwCasIdleCodeSet(ThaPdhDe1 self, uint32 ts, uint8 idle)
    {
    if (self)
        return mMethodsGet(self)->HwCasIdleCodeSet(self, ts, idle);
    return cAtErrorNullPointer;
    }

uint8 ThaPdhDe1HwCasIdleCodeGet(ThaPdhDe1 self, uint32 ts)
    {
    if (self)
        return mMethodsGet(self)->HwCasIdleCodeGet(self, ts);
    return 0;
    }

eAtRet ThaPdhDe1HwRxSlipBufferFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    if (self)
        return HwRxSlipBufferFrameTypeSet(self, frameType);
    return cAtErrorNullPointer;
    }

 uint32 ThaPdhDe1RxSlipBuferCounter(AtPdhChannel self, eBool r2c)
     {
     if (self)
         {
         if (AtPdhChannelRxSlipBufferIsSupported((AtPdhChannel)self) && AtPdhChannelRxSlipBufferIsEnabled((AtPdhChannel)self))
             return RxSlipBufferCounterGet(self, r2c);
         return 0;
         }
     return 0;
     }

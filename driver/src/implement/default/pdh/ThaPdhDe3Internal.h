/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe3Internal.h
 * 
 * Created Date: Feb 2, 2013
 *
 * Description : Thalassa DS3/E3 class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE3INTERNAL_H_
#define _THAPDHDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSurEngine.h"
#include "../../../generic/pdh/AtPdhDe3Internal.h"
#include "../cdr/controllers/ThaCdrController.h"
#include "ThaPdhDe3.h"
#include "retimingcontrollers/ThaPdhRetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe3Methods
    {
    eAtRet (*HwIdFactorGet)(ThaPdhDe3 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice);
    uint32 (*DefaultOffset)(ThaPdhDe3 self);
    uint32 (*DemapDefaultOffset)(ThaPdhDe3 self);
    ThaCdrController (*CdrControllerCreate)(ThaPdhDe3 self);
    uint32 (*BpvCounterRead2Clear)(ThaPdhDe3 self, eBool clear);
    uint32 (*TxCsCounterRead2Clear)(ThaPdhDe3 self, eBool clear);
    uint32 (*InterruptRead2Clear)(ThaPdhDe3 self, eBool clear);
    eBool (*BerPathIsEnabled)(ThaPdhDe3 self);
    void  (*BerPathEnable)(ThaPdhDe3 self, eBool enable);
    eBool (*BerLineIsEnabled)(ThaPdhDe3 self);
    void  (*BerLineEnable)(ThaPdhDe3 self, eBool enable);
    eAtRet (*De3FramingBindToPwSatop)(ThaPdhDe3 self, AtPw pseudowire);
    eBool (*IsManagedByModulePdh)(ThaPdhDe3 self);
    uint8 (*DefaultLosThreshold)(ThaPdhDe3 self);
    eBool (*ShouldConsiderLofWhenLos)(ThaPdhDe3 self);
    ThaPdhRetimingController (*RetimingControllerCreate)(ThaPdhDe3 self);
    eBool (*RetimingIsSupported)(ThaPdhDe3 self);
    uint8 (*DefaultAisAllOnesThreshold)(ThaPdhDe3 self);
    }tThaPdhDe3Methods;

typedef struct tThaPdhDe3
    {
    tAtPdhDe3 super;
    const tThaPdhDe3Methods *methods;

    /* Private data */
    ThaCdrController cdrController;
    AtSurEngine surEngine;
    uint16 frameType;
    AtPrbsEngine prbsEngine;
    AtPrbsEngine linePrbsEngine;

    /* Save to update clock extractor when timing mode is changed */
    AtClockExtractor clockExtractor;
    AtPdhMdlController pathMessage;
    AtPdhMdlController testSignal;
    AtPdhMdlController idleSignal;
    AtPdhMdlController rxCurrentMdlController;
    AtObjectAny        attController;

    /* Retiming */
    ThaPdhRetimingController retimingController;

    /* For squelching */
    uint8 txAisExplicitlyForced;

    eBool enablePrbsDe3Level;
    }tThaPdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void PdhDe3SdhParentMappedTypeSet(AtPdhChannel self);
AtPdhDe3 ThaPdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha60210031PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE3INTERNAL_H_ */


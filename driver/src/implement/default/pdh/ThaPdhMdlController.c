/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhMdlController.c
 *
 * Created Date: May 30, 2015
 *
 * Description : MDL Controller class
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaModulePdh.h"
#include "ThaPdhMdlControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cHwMdlBufferSize (cMdlDataBufferSizes - 3)
#define cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask       cBit0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhMdlController)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef uint16 (*RawMsgFunc)(AtPdhMdlController, uint8 *, uint16);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class*/
static char m_methodsInit = 0;
static tThaPdhMdlControllerMethods m_methods;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtPdhMdlControllerMethods m_AtPdhMdlControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtPdhMdlControllerMethods *m_AtPdhMdlControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void TypeSet(AtPdhMdlController self, uint32 type)
    {
    ThaPdhMdlController mdl = (ThaPdhMdlController)self;
    uint8 msgType;
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage:
            msgType = cMdlMsgTypePath;
            break;
        case cAtPdhMdlControllerTypeIdleSignal:
            msgType = cMdlMsgTypeIdleTest;
            break;
        case cAtPdhMdlControllerTypeTestSignal:
            msgType = cMdlMsgTestSignal;
            break;
        default:
            msgType = 0;
            break;
        }

    /* Default LAPD Header */
    mdl->txBuffer[cMdlAddrStartIndex] = cMdlAddress1CVal;
    mdl->txBuffer[cMdlAddrStartIndex + 1] = cMdlAddress2AnsiVal;
    mdl->txBuffer[cMdlAddrStartIndex + 2] = cMdlControlVal;

    /* Build Default MDL Message contents */
    ThaMdlPktUtilPutLapdMessageTypeToBuffer(mdl->txBuffer, cMdlTypeIndex, msgType);
    ThaMdlPktUtilPutZeroToBuffer(mdl->txBuffer, cMdlEICStartIndex, cMdlEICStringSize);
    ThaMdlPktUtilPutZeroToBuffer(mdl->txBuffer, cMdlLICStartIndex, cMdlLICStringSize);
    ThaMdlPktUtilPutZeroToBuffer(mdl->txBuffer, cMdlFICStartIndex, cMdlFICStringSize);
    ThaMdlPktUtilPutZeroToBuffer(mdl->txBuffer, cMdlUINITStartIndex, cMdlUINTStringSize);
    ThaMdlPktUtilPutZeroToBuffer(mdl->txBuffer, cMdlPortPfiGenStartIndex, cMdlPortPfiGenStringSize);
    mThis(self)->standard = cAtPdhMdlStandardAnsi;
    }

static eAtPdhMdlControllerType TypeGet(AtPdhMdlController self)
    {
    ThaPdhMdlController mdl = (ThaPdhMdlController)self;
    uint8 typeValue = mdl->txBuffer[cMdlTypeIndex];

    switch (typeValue)
        {
        case cMdlMsgTypePath: return cAtPdhMdlControllerTypePathMessage;
        case cMdlMsgTypeIdleTest: return cAtPdhMdlControllerTypeIdleSignal;
        case cMdlMsgTestSignal: return cAtPdhMdlControllerTypeTestSignal;
        default : return cAtPdhMdlControllerTypeUnknown;
        }
    }

static void Lock(AtPdhMdlController self)
    {
    AtOsalMutexLock(mThis(self)->mutex);
    }

static void Unlock(AtPdhMdlController self)
    {
    AtOsalMutexUnLock(mThis(self)->mutex);
    }

static eAtRet Enable(AtPdhMdlController self, eBool enable)
    {
    if (mThis(self)->enable == enable)
        return cAtOk;

    Lock(self);
    mThis(self)->enable = enable;
    Unlock(self);

    return cAtOk;
    }

static eBool IsEnabled(AtPdhMdlController self)
    {
    return mThis(self)->enable;
    }

static eBool IsCommomElement(uint32 element)
    {
    switch (element)
        {
        case cAtPdhMdlDataElementEic:  return cAtTrue;
        case cAtPdhMdlDataElementLic:  return cAtTrue;
        case cAtPdhMdlDataElementFic:  return cAtTrue;
        case cAtPdhMdlDataElementUnit: return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool DataElementIsValid(AtPdhMdlController self, eAtPdhMdlDataElement element)
    {
    eAtPdhMdlControllerType type = AtPdhMdlControllerTypeGet(self);
    if (IsCommomElement(element))
        return cAtTrue;

    if ((type == cAtPdhMdlControllerTypePathMessage) && (element == cAtPdhMdlDataElementPfi))
        return cAtTrue;

    if ((type == cAtPdhMdlControllerTypeIdleSignal) &&  (element == cAtPdhMdlDataElementIdleSignalPortNumber))
        return cAtTrue;

    if ((type == cAtPdhMdlControllerTypeTestSignal) && (element == cAtPdhMdlDataElementTestSignalGeneratorNumber))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 DataElementOffSet(uint32 element)
    {
    switch (element)
        {
        case cAtPdhMdlDataElementEic:  return cMdlEICStartIndex;
        case cAtPdhMdlDataElementLic:  return cMdlLICStartIndex;
        case cAtPdhMdlDataElementFic:  return cMdlFICStartIndex;
        case cAtPdhMdlDataElementUnit: return cMdlUINITStartIndex;
        default : return cMdlPortPfiGenStartIndex;
        }
    }

static uint8 DataElementLengthGet(uint32 element)
    {
    switch (element)
        {
        case cAtPdhMdlDataElementEic:  return cMdlEICStringSize;
        case cAtPdhMdlDataElementLic:  return cMdlLICStringSize;
        case cAtPdhMdlDataElementFic:  return cMdlFICStringSize;
        case cAtPdhMdlDataElementUnit: return cMdlUINTStringSize;
        default : return cMdlPortPfiGenStringSize;
        }
    }

static eAtRet TxDataElementSet(AtPdhMdlController self,
                               eAtPdhMdlDataElement element,
                               const uint8 *data,
                               uint16 length)
    {
    uint8 remainBytes;
    uint8 elementSize;
    uint8 offset;
    if (!DataElementIsValid(self, element))
        return cAtErrorInvlParm;

    if (DataElementLengthGet(element) < length)
        return cAtErrorOutOfRangParm;

    remainBytes = 0;
    elementSize = DataElementLengthGet(element);
    offset =  DataElementOffSet(element);

    /* Write to software buffer */
    Lock(self);
    if (length < elementSize)
        {
        remainBytes = (uint8)(elementSize - length);
        ThaMdlPktUtilPutZeroToBuffer(mThis(self)->txBuffer, (uint8)(length + offset), remainBytes);
        }
    ThaMdlPktUtilPutDataArrayToBuffer(mThis(self)->txBuffer, offset,  data, (uint8)length);
    Unlock(self);
    return cAtOk;
    }

static uint16 TxDataElementGet(AtPdhMdlController self,
                               eAtPdhMdlDataElement element,
                               uint8 *data,
                               uint16 bufferSize)
    {
    uint16 size;
    uint8 offset;
    if (!DataElementIsValid(self, element))
        return 0;

    Lock(self);
    offset = DataElementOffSet(element);
    size = DataElementLengthGet(element);
    if (size > bufferSize)
        size = bufferSize;
    else
        ThaMdlPktUtilPutZeroToBuffer(data, (uint8)size, (uint8)(bufferSize - size));

    ThaMdlPktUtilPutDataArrayFromBuffer(&mThis(self)->txBuffer[offset], data, (uint8)size);

    Unlock(self);

    return size;
    }

static eBool RxMsgIsAnsiFormat(AtPdhMdlController self)
    {
    return (mThis(self)->rxBuffer[1] == cMdlAddress2AnsiVal) ? cAtTrue : cAtFalse;
    }

static uint16 RxDataElementGet(AtPdhMdlController self,
                               eAtPdhMdlDataElement element,
                               uint8 *data,
                               uint16 bufferSize)
    {
    uint16 size;
    uint8 offset;
    if (!DataElementIsValid(self, element))
        return 0;

    offset = DataElementOffSet(element);
    size = DataElementLengthGet(element);
    if (!RxMsgIsAnsiFormat(self))
        offset = (uint8)(offset - 1);

    if (size > bufferSize)
        size = bufferSize;
    else
        ThaMdlPktUtilPutZeroToBuffer(data, (uint8)size, (uint8)(bufferSize - size));

    ThaMdlPktUtilPutDataArrayFromBuffer(&mThis(self)->rxBuffer[offset], data, (uint8)size);

    return size;
    }

static uint16 RxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    uint16 size = cMdlDataBufferSizes;
    if (bufferSize < cMdlDataBufferSizes)
        size = bufferSize;
    else
        ThaMdlPktUtilPutZeroToBuffer(data, (uint8)size, (uint8)(bufferSize - size));

    Lock(self);
    ThaMdlPktUtilPutDataArrayFromBuffer(mThis(self)->rxBuffer, data, (uint8)size);
    Unlock(self);
    return size;
    }

static eAtRet TxCRBitSet(AtPdhMdlController self, uint8 value)
    {
    if (value > 1)
        return cAtErrorOutOfRangParm;

    Lock(self);
    mThis(self)->txBuffer[cMdlAddrStartIndex] = (value) ? cMdlAddress1RVal:cMdlAddress1CVal;
    Unlock(self);
    return cAtOk;
    }

static uint8  TxCRBitGet(AtPdhMdlController self)
    {
    return (mThis(self)->txBuffer[cMdlAddrStartIndex] == cMdlAddress1RVal) ? 1 : 0;
    }

static uint8  RxCRBitGet(AtPdhMdlController self)
    {
    return (mThis(self)->rxBuffer[cMdlAddrStartIndex] == cMdlAddress1RVal) ? 1 : 0;
    }

static const char *MsgTypeString(uint32 type)
    {
    switch (type)
        {
        case cAtPdhMdlControllerTypePathMessage: return "path";
        case cAtPdhMdlControllerTypeIdleSignal: return "idle-signal";
        case cAtPdhMdlControllerTypeTestSignal: return "test-signal";
        default : return "unknown";
        }
    }

static void CopyMsg(uint8 *des, uint8 *src, uint32 size)
    {
    uint32 idx;
    for (idx = 0; idx < size; idx++)
        des[idx] = src[idx];
    }

static eBool TxMsgIsAnsiFormat(AtPdhMdlController self)
    {
    return (mThis(self)->txBuffer[1] == cMdlAddress2AnsiVal) ? cAtTrue : cAtFalse;
    }

static uint16 AnsiTxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    uint16 size = cMdlDataBufferSizes;
    if (bufferSize < cMdlDataBufferSizes)
        size = bufferSize;
    else
        ThaMdlPktUtilPutZeroToBuffer(data, (uint8)size, (uint8)(bufferSize - size));

    Lock(self);
    ThaMdlPktUtilPutDataArrayFromBuffer(mThis(self)->txBuffer, data, (uint8)size);
    Unlock(self);
    return size;
    }

static uint16 AttTxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    uint16 size = cMdlDataBufferSizes - 1;
    if (bufferSize < (cMdlDataBufferSizes - 1))
        size = bufferSize;
    else
        ThaMdlPktUtilPutZeroToBuffer(data, (uint8)size, (uint8)(bufferSize - size));
    data[0] = mThis(self)->txBuffer[0];
    data[1] = mThis(self)->txBuffer[1];
    Lock(self);
    CopyMsg(&data[2], &mThis(self)->txBuffer[3], (uint32)(size - 2));
    Unlock(self);
    return size;
    }

static uint16 TxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    if (TxMsgIsAnsiFormat(self))
        return AnsiTxMesssageGet(self, data, bufferSize);
    return AttTxMesssageGet(self, data, bufferSize);
    }

static AtDevice DeviceGet(AtPdhMdlController self)
    {
    return AtChannelDeviceGet((AtChannel)AtPdhMdlControllerDe3Get(self));
    }

static eAtRet Send(AtPdhMdlController self)
    {
    if (mThis(self)->enable == cAtFalse)
        return cAtErrorRsrcNoAvail;

    if (!AtDeviceIsSimulated(DeviceGet(self)))
        return mMethodsGet(mThis(self))->HwSend(mThis(self), mThis(self)->txBuffer, cHwMdlBufferSize);

    /* For simulation */
    Lock(self);
    CopyMsg(mThis(self)->rxBuffer, mThis(self)->txBuffer, cMdlDataBufferSizes);
    Unlock(self);

    return cAtOk;
    }

static eBool MessageReceived(AtPdhMdlController self)
    {
    eBool newMsg = mMethodsGet(mThis(self))->HwMessageReceived(mThis(self));
    if (newMsg)
        mMethodsGet(mThis(self))->HwReceived(mThis(self), mThis(self)->rxBuffer,  cMdlDataBufferSizes);
    return newMsg;
    }

static void BufferPrint(AtPdhMdlController self, const char *desDirStr, const char *stdStr, RawMsgFunc rawMsgFuncGet)
    {
    uint8 buffer[100];
    uint32 loc_i;
    uint16 length = rawMsgFuncGet(self, buffer, 100);
    AtPrintc(cSevNeutral, "*==============================================================================================*\r\n");
    AtPrintc(cSevInfo, "%s with format %s, with length %d bytes\r\n", desDirStr, stdStr, length);
    AtPrintc(cSevNeutral, "*==============================================================================================*\r\n");
    for (loc_i = 0; loc_i < 100; loc_i++)
        {
        AtPrintc(cSevDebug, "0x%02x", buffer[loc_i]);
        if ((loc_i +1)%16 ==0)
            AtPrintc(cSevDebug, "\r\n");
        else
            AtPrintc(cSevDebug, ", ");
        }
    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevNeutral, "*==============================================================================================*\r\n");
    }

static void Debug(AtPdhMdlController self)
    {
    AtPrintc(cSevNeutral, "*==============================================================================================*\r\n");
    AtPrintc(cSevInfo, "controller-description-string:     %s \r\n", MsgTypeString(TypeGet(self)));
    BufferPrint(self, " Tx-Raw-Msg : ", TxMsgIsAnsiFormat(self) ? "Ansi" :"AT&T", TxMesssageGet);
    BufferPrint(self, " Rx-Raw-Msg : ", RxMsgIsAnsiFormat(self) ? "Ansi" :"AT&T", RxMesssageGet);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhMdlController);
    }

static void Delete(AtObject self)
    {
    AtOsalMutexDestroy(mThis(self)->mutex);
    mThis(self)->mutex = NULL;
    m_AtObjectMethods->Delete(self);
    }

static eAtRet HwStandardHeaderSet(ThaPdhMdlController self, uint32 standard)
    {
    uint8 headerVal = (standard == cAtPdhMdlStandardAnsi) ? cMdlAddress2AnsiVal : cMdlAddress2AttVal;
    mThis(self)->txBuffer[cMdlAddrStartIndex + 1] = headerVal;
    return cAtOk;
    }

static eAtRet HwSend(ThaPdhMdlController self, uint8 *messageBuffer, uint32 bufferLength)
    {
    AtUnused(self);
    AtUnused(messageBuffer);
    AtUnused(bufferLength);

    return cAtErrorNotImplemented;
    }

static uint32 HwReceived(ThaPdhMdlController self, uint8 *messageBuffer, uint32 bufferLength)
    {
    AtUnused(self);
    AtUnused(messageBuffer);
    AtUnused(bufferLength);

    return 0;
    }

static eBool HwMessageReceived(ThaPdhMdlController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPdhMdlController object = (ThaPdhMdlController)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt8Array(txBuffer, cMdlDataBufferSizes);
    mEncodeUInt8Array(rxBuffer, cMdlDataBufferSizes);
    mEncodeUInt(enable);
    mEncodeUInt(standard);
    mEncodeNone(mutex);
    }

static uint32 DefectHistoryClear(AtPdhMdlController self)
    {
    return mMethodsGet(mThis(self))->DefectHistoryReadToClear(mThis(self), cAtTrue);
    }

static uint32 DefectHistoryGet(AtPdhMdlController self)
    {
    return mMethodsGet(mThis(self))->DefectHistoryReadToClear(mThis(self), cAtFalse);
    }

static ThaInterruptManager InterruptManager(AtPdhMdlController self)
    {
    AtChannel de3 = (AtChannel)AtPdhMdlControllerDe3Get(self);
    return ThaModulePdhMdlInterruptManager((ThaModulePdh)AtChannelModuleGet(de3));
    }

static uint32 InterruptEnableRegister(AtPdhMdlController self)
    {
    ThaInterruptManager manager = InterruptManager(self);
    return ThaInterruptManagerBaseAddress(manager) +
           ThaInterruptManagerInterruptEnableRegister(manager);
    }

static uint32 InterruptStatusRegister(AtPdhMdlController self)
    {
    ThaInterruptManager manager = InterruptManager(self);
    return ThaInterruptManagerBaseAddress(manager) +
           ThaInterruptManagerInterruptStatusRegister(manager);
    }

static eAtRet InterruptMaskSet(AtPdhMdlController self, uint32 defectMask, uint32 enableMask)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get(self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self));
    uint32 regAddr = InterruptEnableRegister(self) + offset;
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);
    if (defectMask & cAtPdhDs3AlarmMdlChange)
        {
        if (enableMask & cAtPdhDs3AlarmMdlChange)
            regVal |= cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask;
        else
            regVal &= (uint32)(~cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask);
        }
    mChannelHwWrite(de3, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtPdhMdlController self)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get(self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self));
    uint32 regAddr = InterruptEnableRegister(self) + offset;
    uint32 regVal = mChannelHwRead(de3, regAddr, cAtModulePdh);

    if (regVal & cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask)
        return cAtPdhDs3AlarmMdlChange;

    return 0;
    }

static void InterruptProcess(AtPdhMdlController self, uint8 slice, uint8 de3, AtHal hal)
    {
    AtPdhDe3 de3Channel = AtPdhMdlControllerDe3Get(self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self));
    uint32 address = InterruptStatusRegister(self) + offset;
    uint32 intrMask = AtHalRead(hal, InterruptEnableRegister(self) + offset);
    uint32 intrStatus = AtHalRead(hal, address);
    uint32 events = 0;
    AtUnused(slice);
    AtUnused(de3);

    intrStatus &= intrMask;

    if (intrStatus & cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask)
        events |= cAtPdhDs3AlarmMdlChange;

    /* Clear interrupt */
    AtHalWrite(hal, address, intrStatus);

    AtChannelAllAlarmListenersCall((AtChannel)de3Channel, events, 0);
    }

static uint32 DefectHistoryReadToClear(ThaPdhMdlController self, eBool r2c)
    {
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);
    uint32 address = InterruptStatusRegister((AtPdhMdlController)self) +
                     mMethodsGet(mThis(self))->InterruptOffset(mThis(self));
    uint32 regValue = mChannelHwRead(de3, address, cAtModulePdh);
    if (r2c)
        mChannelHwWrite(de3, address, regValue, cAtModulePdh);
    return (regValue & cAf6_mdl_cfgen_int_mdl_cfgen_int_Mask) ? cAtPdhDs3AlarmMdlChange : 0;
    }

static uint32 InterruptOffset(ThaPdhMdlController self)
    {
    uint8 sliceId, hwIdInSlice;
    AtPdhDe3 de3 = AtPdhMdlControllerDe3Get((AtPdhMdlController)self);

    if (ThaPdhChannelHwIdGet((AtPdhChannel)de3, cAtModulePdh, &sliceId, &hwIdInSlice) != cAtOk)
        return cBit31_0;

    return (hwIdInSlice + (sliceId * 32UL));
    }

/* Initialize implementation */
static void MethodsInit(AtPdhMdlController self)
    {
    ThaPdhMdlController thaMdl = (ThaPdhMdlController)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HwStandardHeaderSet);
        mMethodOverride(m_methods, HwSend);
        mMethodOverride(m_methods, HwReceived);
        mMethodOverride(m_methods, HwMessageReceived);
        mMethodOverride(m_methods, DefectHistoryReadToClear);
        mMethodOverride(m_methods, InterruptOffset);
        }

    mMethodsGet(thaMdl) = &m_methods;
    }

static void OverrideAtPdhMdlController(ThaPdhMdlController self)
    {
    AtPdhMdlController controller = (AtPdhMdlController) self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhMdlControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhMdlControllerOverride, m_AtPdhMdlControllerMethods, sizeof(m_AtPdhMdlControllerOverride));
        mMethodOverride(m_AtPdhMdlControllerOverride, TypeGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, Enable);
        mMethodOverride(m_AtPdhMdlControllerOverride, IsEnabled);
        mMethodOverride(m_AtPdhMdlControllerOverride, TxDataElementSet);
        mMethodOverride(m_AtPdhMdlControllerOverride, TxDataElementGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, RxDataElementGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, RxMesssageGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, TxCRBitSet);
        mMethodOverride(m_AtPdhMdlControllerOverride, TxCRBitGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, RxCRBitGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, Debug);
        mMethodOverride(m_AtPdhMdlControllerOverride, TxMesssageGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, Send);
        mMethodOverride(m_AtPdhMdlControllerOverride, MessageReceived);
        mMethodOverride(m_AtPdhMdlControllerOverride, DefectHistoryClear);
        mMethodOverride(m_AtPdhMdlControllerOverride, DefectHistoryGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtPdhMdlControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtPdhMdlControllerOverride, InterruptProcess);
        }
    mMethodsSet(controller, &m_AtPdhMdlControllerOverride);
    }

static void OverrideAtObject(ThaPdhMdlController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPdhMdlController self)
    {
    OverrideAtPdhMdlController(self);
    OverrideAtObject(self);
    }

AtPdhMdlController ThaPdhMdlControllerObjectInit(AtPdhMdlController self, AtPdhDe3 de3, uint32 mdlType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhMdlControllerObjectInit(self, de3) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override((ThaPdhMdlController)self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->mutex = AtOsalMutexCreate();
    TypeSet(self, mdlType);

    return self;
    }

AtPdhMdlController ThaPdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhMdlController newMdlController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newMdlController == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhMdlControllerObjectInit(newMdlController, self, mdlType);
    }

eAtRet ThaPdhMdlControllerHwStandardHeaderSet(AtPdhMdlController self, uint32 standard)
    {
    if (self)
        return mMethodsGet(mThis(self))->HwStandardHeaderSet(mThis(self), standard);
    return cAtError;
    }

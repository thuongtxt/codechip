/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhPrmControllerInternal.h
 *
 * Created Date: Oct 27, 2015
 *
 * Description : PRM Controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPRMCONTROLLERINTERNAL_H_
#define _THAPDHPRMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "ThaPdhDe1Internal.h"
#include "ThaModulePdh.h"
#include "ThaPdhPrmController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPrmControllerMethods
    {
    uint32 (*CounterOffset)(ThaPdhPrmController self, eBool read2Clear);
    eAtModulePdhRet (*HwEnable)(ThaPdhPrmController self, eBool enable);
    eAtModulePdhRet (*HwTxStandardSet)(ThaPdhPrmController self, uint8 standard);
    eAtModulePdhRet (*HwRxStandardSet)(ThaPdhPrmController self, uint8 standard);
    eAtModulePdhRet (*HwTxCRBitSet)(ThaPdhPrmController self, uint8 value);
    eAtModulePdhRet (*HwTxLBBitSet)(ThaPdhPrmController self, uint8 value);
    eAtModulePdhRet (*HwCRBitExpectSet)(ThaPdhPrmController self, uint8 value);
    uint32 (*DefectHistoryReadToClear)(ThaPdhPrmController self, eBool r2c);
    uint32 (*Read2ClearOffset)(ThaPdhPrmController self, eBool r2c);
    uint32 (*InterruptOffset)(ThaPdhPrmController self, uint8 *sliceId);

    /* Counters register */
    uint32 (*TxMessageRegister)(ThaPdhPrmController self);
    uint32 (*TxByteRegister)(ThaPdhPrmController self);
    uint32 (*RxGoodMessageRegister)(ThaPdhPrmController self);
    uint32 (*RxByteRegister)(ThaPdhPrmController self);
    uint32 (*RxDiscardRegister)(ThaPdhPrmController self);
    uint32 (*RxMissRegister)(ThaPdhPrmController self);

    /* For FCS bit-Order */
    eAtModulePdhRet (*HwFcsBitOrderSet)(AtPdhPrmController self, eAtBitOrder order);
    eAtBitOrder (*HwFcsBitOrderGet)(AtPdhPrmController self);

    /* Backward compatible */
    uint32 (*StartVersionHasRxEnabledCfgRegister)(ThaPdhPrmController self);
    } tThaPdhPrmControllerMethods;

typedef struct tThaPdhPrmController
    {
    tAtPdhPrmController super;
    const tThaPdhPrmControllerMethods *methods;
    }tThaPdhPrmController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhPrmController ThaPdhPrmControllerObjectInit(AtPdhPrmController self, AtPdhDe1 de1);

#endif /* _THAPDHPRMCONTROLLERINTERNAL_H_ */

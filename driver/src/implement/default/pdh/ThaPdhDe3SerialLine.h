/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe3SerialLine.h
 * 
 * Created Date: Jun 12, 2016
 *
 * Description : Data and interface of the PDH serial line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE3SERIALLINE_H_
#define _THAPDHDE3SERIALLINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pdh/AtPdhSerialLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe3SerialLine *ThaPdhDe3SerialLine;

typedef struct tThaPdhDe3SerialLineMethods
    {
    eAtRet (*HwIdFactorGet)(ThaPdhDe3SerialLine self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice);
    AtPdhDe3 (*De3ObjectGet)(ThaPdhDe3SerialLine self);
    eAtRet (*MapEc1Line)(ThaPdhDe3SerialLine self, AtSdhLine line);
    eAtRet (*MapDe3Line)(ThaPdhDe3SerialLine self, AtPdhDe3 de3);
    }tThaPdhDe3SerialLineMethods;

typedef struct tThaPdhDe3SerialLine
    {
    tAtPdhSerialLine super;
    const tThaPdhDe3SerialLineMethods *methods;
    } tThaPdhDe3SerialLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaPdhDe3SerialLineHwIdFactorGet(ThaPdhDe3SerialLine self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice);
AtPdhSerialLine ThaPdhDe3SerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module);

/* Access DS3/E3 */
AtPdhDe3 ThaPdhDe3SerialLineDe3ObjectGet(ThaPdhDe3SerialLine self);

/* Access EC1 */
AtSdhLine ThaPdhDe3SerialLineEc1ObjectGet(ThaPdhDe3SerialLine self);

/* HW mapping between LIU and core */
eAtRet ThaPdhDe3SerialLineMapEc1(ThaPdhDe3SerialLine self, AtSdhLine line);
eAtRet ThaPdhDe3SerialLineMapDe3(ThaPdhDe3SerialLine self, AtPdhDe3 de3);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE3SERIALLINE_H_ */


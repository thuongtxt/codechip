/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe3RetimingController.c
 *
 * Created Date: May 29, 2017
 *
 * Description : Retiming controller implementation for DS3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhDe3RetimingControllerInternal.h"
#include "../../man/ThaDeviceInternal.h"
#include "../ThaModulePdhReg.h"
#include "../ThaModulePdhForStmReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhDe3RetimingController)(self))

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tThaPdhDe3RetimingControllerMethods m_methods;
static char m_methodsInit = 0;

/* Override */
static tThaPdhRetimingControllerMethods m_ThaPdhRetimingControllerOverride;

/* Save super implementation */
static const tThaPdhRetimingControllerMethods *m_ThaPdhRetimingControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe3 De3(ThaPdhDe3RetimingController self)
    {
    return (ThaPdhDe3)ThaPdhRetimingControllerOwnerGet((ThaPdhRetimingController)self);
    }

static uint32 SlipBufferSsmEnableRegMask(AtPdhChannel self)
    {
    return cBit0 << (AtChannelIdGet((AtChannel)self) % 24);
    }

static uint32 SlipBufferSsmEnableRegShift(AtPdhChannel self)
    {
    return AtChannelIdGet((AtChannel)self) % 24;
    }

static uint32 PartOffset(ThaPdhDe3 self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceModulePartOffset(device, cAtModulePdh, (uint8)ThaPdhDe3PartId(self));
    }

static uint32 DefaultSliceOffset(ThaPdhDe3 self)
    {
    uint8 sliceId, hwIdInSlice;

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &sliceId, &hwIdInSlice);
    return PartOffset(self) + (uint32)ThaModulePdhSliceOffset((ThaModulePdh)AtChannelModuleGet((AtChannel)self), sliceId);
    }

static uint32 SlipBufferSsmEnableRegister(ThaPdhDe3RetimingController self)
    {
    ThaPdhDe3 thade3 = De3(self);
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)thade3);
    return cAf6Reg_ssm_enable + DefaultSliceOffset(thade3) + ThaModulePdhBaseAddress(module);
    }

static uint32 SlipBufferTxFramerControlRegister(ThaPdhDe3RetimingController self)
    {
    ThaPdhDe3 thade3 = De3(self);
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)thade3);
    return cAf6Reg_txctrl_pen_Base + ThaPdhDe3DefaultOffset(thade3) + ThaModulePdhBaseAddress(module);
    }

static uint32 SlipBufferRxFramerControlRegister(ThaPdhDe3RetimingController self)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)thade3);
    return cAf6Reg_rxctrl_pen_Base + ThaPdhDe3DefaultOffset(thade3) + ThaModulePdhBaseAddress(module);
    }

static uint32 SlipBufferRxFrameStatusRegister(ThaPdhDe3RetimingController self)
    {
    ThaPdhDe3 thade3 = De3(self);
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)thade3);

    /* show hw frame status */
    return cAf6_rxfrm_status_pen + ThaPdhDe3DefaultOffset(thade3) + ThaModulePdhBaseAddress(module);
    }

static uint32 SlipBufferTxSlipStickyRegister(ThaPdhDe3RetimingController self)
    {
    ThaPdhDe3 thade3 = De3(self);
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)thade3);

    /* show data slip sticky */
    return cAf6_txslip_stk0_upen + DefaultSliceOffset(thade3) + ThaModulePdhBaseAddress(module);
    }

static eAtRet SlipBufferSsmBomSend(ThaPdhRetimingController self, uint8 value, uint32 nTimes)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    uint32 regVal;

    AtUnused(nTimes);
    regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl_pen_E3G832_SSMMess_, value);
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static uint8 SlipBufferTxSsmGet(ThaPdhRetimingController self)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    uint32 regVal  = mChannelHwRead(thade3, address, cAtModulePdh);
    return (uint8)mRegField(regVal, cAf6_txctrl_pen_E3G832_SSMMess_);
    }

static eAtRet SlipBufferEnable(ThaPdhRetimingController self, eBool enable)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address = mMethodsGet(mThis(self))->SlipBufferSsmEnableRegister(mThis(self));
    uint32 mask = SlipBufferSsmEnableRegMask((AtPdhChannel)thade3);
    uint32 shift = SlipBufferSsmEnableRegShift((AtPdhChannel)thade3);
    uint32 regVal;

    regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    mFieldIns(&regVal, mask, shift, enable ? 1 : 0);
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);

    address = mMethodsGet(mThis(self))->SlipBufferRxFramerControlRegister(mThis(self));
    regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    mFieldIns(&regVal, cAf6_rxctrl_pen_RxDE3RxDE3Enb_Mask, cAf6_rxctrl_pen_RxDE3RxDE3Enb_Shift, enable ? 1 : 0);
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static void SlipBufferDefaultSet(ThaPdhRetimingController self)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address, regVal = 0;

    address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    mFieldIns(&regVal, cAf6_txctrl_pen_Ds3Cbit_SSMInvert_Mask, cAf6_txctrl_pen_Ds3Cbit_SSMInvert_Shift, 0);/*MSB first*/
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);

    address = mMethodsGet(mThis(self))->SlipBufferRxFramerControlRegister(mThis(self));
    mChannelHwWrite(thade3, address, 0, cAtModulePdh);

    ThaPdhRetimingControllerSlipBufferEnable(self, cAtFalse);
    }

static eAtModulePdhRet TxUdlBitOrderSet(ThaPdhDe3RetimingController self, eAtBitOrder bitOrder)
    {
    ThaPdhDe3 thade3 = De3(self);
    uint32 address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    uint32 regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    uint8 invert = (bitOrder == cAtBitOrderMsb) ? 0 : 1;
    mFieldIns(&regVal, cAf6_txctrl_pen_Ds3Cbit_SSMInvert_Mask, cAf6_txctrl_pen_Ds3Cbit_SSMInvert_Shift, invert);
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eAtBitOrder TxUdlBitOrderGet(ThaPdhDe3RetimingController self)
    {
    ThaPdhDe3 thade3 = De3(self);
    uint32 address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    uint32 regVal  = mChannelHwRead(thade3, address, cAtModulePdh);
    uint32 inverted = regVal & cAf6_txctrl_pen_Ds3Cbit_SSMInvert_Mask;
    return inverted ? cAtBitOrderLsb : cAtBitOrderMsb;
    }

static eBool SlipBufferIsEnabled(ThaPdhRetimingController self)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address = mMethodsGet(mThis(self))->SlipBufferSsmEnableRegister(mThis(self));
    uint32 mask = SlipBufferSsmEnableRegMask((AtPdhChannel)thade3);
    uint32 regVal;
    regVal = mChannelHwRead(thade3, address, cAtModulePdh);

    return (regVal & mask) ? cAtTrue : cAtFalse;
    }

static eBool IsDs3Framed(uint16 frameType)
    {
    if ((frameType == cAtPdhDs3FrmCbitUnChn)      ||
        (frameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (frameType == cAtPdhDs3FrmCbitChnl21E1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl28Ds1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl21E1s))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TxSlipBufferFramedModeSw2Hw(uint16 frameType)
    {
    if (frameType == cAtPdhE3Frmg832)
        return 0;
    if (frameType == cAtPdhE3FrmG751 ||  frameType == cAtPdhE3FrmG751Chnl16E1s)
        return 1;
    if (IsDs3Framed(frameType))
        return 2;
    if (frameType == cAtPdhDs3Unfrm)
        return 3;
    if (frameType == cAtPdhE3Unfrm)
        return 4;

    return 5;
    }

static void SlipBufferTxHwSsmFrameTypeUpdate(ThaPdhRetimingController self, uint16 frameType)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address, regVal;

    address = mMethodsGet(mThis(self))->SlipBufferRxFramerControlRegister(mThis(self));
    regVal  = mChannelHwRead(thade3, address, cAtModulePdh);
    mRegFieldSet(regVal, cThaRxDS3E3Md, TxSlipBufferFramedModeSw2Hw(frameType));
    mFieldIns(&regVal, cAf6_rxctrl_pen_RxDE3LofThres_Mask, cAf6_rxctrl_pen_RxDE3LofThres_Shift, 1);
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);

    address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    mRegFieldSet(regVal, cThaTxDS3E3Md, TxSlipBufferFramedModeSw2Hw(frameType));
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);
    }

static eAtRet SlipBufferSsmEnable(ThaPdhRetimingController self, eBool enable)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    uint32 regVal;

    regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_txctrl_pen_SSM_G832SSMEna_, enable ? 1 : 0);
    mChannelHwWrite(thade3, address, regVal, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferSsmIsEnabled(ThaPdhRetimingController self)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address = mMethodsGet(mThis(self))->SlipBufferTxFramerControlRegister(mThis(self));
    uint32 regVal;
    regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    return (regVal & cAf6_txctrl_pen_SSM_G832SSMEna_Mask) ? cAtTrue : cAtFalse;
    }

static void SlipBufferTxHwDebug(ThaPdhRetimingController self)
    {
    ThaPdhDe3 thade3 = De3(mThis(self));
    uint32 address, regVal;

    /* show hw frame status */
    address = mMethodsGet(mThis(self))->SlipBufferRxFrameStatusRegister(mThis(self));
    regVal  = mChannelHwRead(thade3, address, cAtModulePdh) & cBit2_0;
    AtPrintc(cSevInfo, "Slip Buffer RX frame status: ");
    if (regVal == 3)
        AtPrintc(cSevNormal, "In frame\n");
    else if (regVal == 0)
        AtPrintc(cSevCritical, "LOF\n");
    else
        AtPrintc(cSevWarning, "Unkown code = 0x%0x\n", regVal);

    /* show data slip sticky */
    address = mMethodsGet(mThis(self))->SlipBufferTxSlipStickyRegister(mThis(self));
    regVal = mChannelHwRead(thade3, address, cAtModulePdh);
    AtPrintc(cSevInfo, "Slip Buffer Data slip sticky: %s \n", regVal ? "raise":"clear");
    if (regVal & SlipBufferSsmEnableRegMask((AtPdhChannel)thade3))
        mChannelHwWrite(thade3, address, SlipBufferSsmEnableRegMask((AtPdhChannel)thade3), cAtModulePdh);
    }

static uint32 TxCsCounter(ThaPdhRetimingController self, eBool r2c)
    {
    ThaPdhDe3 thade3 = (ThaPdhDe3)De3(mThis(self));
    ThaModulePdh module = (ThaModulePdh)AtChannelModuleGet((AtChannel)thade3);
    uint32 offset1 = ThaPdhDe3DefaultOffset(thade3) + ThaModulePdhBaseAddress(module);
    uint32 offset = (!r2c) ? (32 + offset1) : offset1;
    uint32 address = cAf6_txslip_cnt0_pen + offset;

    return mChannelHwRead(thade3, address, cAtModulePdh);
    }

static void OverrideThaPdhRetimingController(ThaPdhDe3RetimingController self)
    {
    ThaPdhRetimingController variable = (ThaPdhRetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhRetimingControllerMethods = mMethodsGet(variable);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhRetimingControllerOverride, m_ThaPdhRetimingControllerMethods, sizeof(m_ThaPdhRetimingControllerOverride));

        mMethodOverride(m_ThaPdhRetimingControllerOverride, TxCsCounter);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmBomSend);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxSsmGet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxHwSsmFrameTypeUpdate);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferDefaultSet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxHwDebug);
        }

    mMethodsSet(variable, &m_ThaPdhRetimingControllerOverride);
    }

static void Override(ThaPdhDe3RetimingController self)
    {
    OverrideThaPdhRetimingController(self);
    }

static void MethodsInit(ThaPdhDe3RetimingController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, SlipBufferTxFramerControlRegister);
        mMethodOverride(m_methods, SlipBufferRxFramerControlRegister);
        mMethodOverride(m_methods, SlipBufferSsmEnableRegister);
        mMethodOverride(m_methods, SlipBufferRxFrameStatusRegister);
        mMethodOverride(m_methods, SlipBufferTxSlipStickyRegister);
        mMethodOverride(m_methods, TxUdlBitOrderSet);
        mMethodOverride(m_methods, TxUdlBitOrderGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhDe3RetimingController);
    }

ThaPdhDe3RetimingController ThaPdhDe3RetimingControllerObjectInit(ThaPdhDe3RetimingController self, AtPdhChannel owner)
    {
    if (self == NULL)
        return NULL;

    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhRetimingControllerObjectInit((ThaPdhRetimingController)self, owner) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPdhDe3RetimingController ThaPdhDe3RetimingControllerNew(AtPdhChannel owner)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPdhDe3RetimingController newThaPdhDe3RetimingController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newThaPdhDe3RetimingController == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhDe3RetimingControllerObjectInit(newThaPdhDe3RetimingController, owner);
    }

eAtModulePdhRet ThaPdhDe3RetimingControllerTxUdlBitOrderSet(ThaPdhDe3RetimingController self, eAtBitOrder bitOrder)
    {
    if (self)
        return mMethodsGet(self)->TxUdlBitOrderSet(self, bitOrder);

    return cAtErrorNullPointer;
    }

eAtBitOrder ThaPdhDe3RetimingControllerTxUdlBitOrderGet(ThaPdhDe3RetimingController self)
    {
    if (self)
        return mMethodsGet(self)->TxUdlBitOrderGet(self);

    return 0;
    }

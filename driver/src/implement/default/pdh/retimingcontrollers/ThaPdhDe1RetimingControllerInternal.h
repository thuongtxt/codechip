/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe1RetimingControllerInternal.h
 * 
 * Created Date: May 31, 2017
 *
 * Description : DS1/E1 retiming controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE1RETIMINGCONTROLLERINTERNAL_H_
#define _THAPDHDE1RETIMINGCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPdhRetimingControllerInternal.h"
#include "ThaPdhDe1RetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe1RetimingControllerMethods
    {
    uint32 (*SlipBufferRxFramerControlRegister)(ThaPdhDe1RetimingController self);
    uint32 (*SlipBufferSsmRxStatusRegister)(ThaPdhDe1RetimingController self);
    uint32 (*SlipBufferSsmRxCrcCounterRegister)(ThaPdhDe1RetimingController self);
    uint32 (*SlipBufferSsmRxReiCounterRegister)(ThaPdhDe1RetimingController self);
    uint32 (*SlipBufferSsmRxFbeCounterRegister)(ThaPdhDe1RetimingController self);
    void   (*SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate)(ThaPdhDe1RetimingController self, uint16 frameType);

    void   (*SlipBufferTxHwDebugFrameStickyShow)(ThaPdhDe1RetimingController self);
    eAtRet (*SlipBufferSsmBomToggleSending)(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes);
    eAtRet (*SlipBufferSsmBomValueSet)(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes);
    void   (*SlipBufferSsmSaBitsMaskSet)(ThaPdhDe1RetimingController self, uint8 saBitsMask);
    uint8  (*SlipBufferSsmSaBitsMaskGet)(ThaPdhDe1RetimingController self);

    eBool  (*SlipBufferBomSendingIsEnabled)(ThaPdhDe1RetimingController self);
    eBool  (*SlipBufferBomSendingIsReady)(ThaPdhDe1RetimingController self);
    eBool  (*SlipBufferBomSendingCanWaitForReady)(ThaPdhDe1RetimingController self);
    eAtRet (*SlipBufferSsmBomValueForToggleSendingSet)(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes, eBool isToggle);

    eAtRet (*SlipBufferTxSignalingEnable)(ThaPdhDe1RetimingController self, eBool enable);
    eBool (*SlipBufferTxSignalingIsEnabled)(ThaPdhDe1RetimingController self);
    }tThaPdhDe1RetimingControllerMethods;

typedef struct tThaPdhDe1RetimingController
    {
    tThaPdhRetimingController super;
    tThaPdhDe1RetimingControllerMethods *methods;
    } tThaPdhDe1RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPdhDe1RetimingController ThaPdhDe1RetimingControllerObjectInit(ThaPdhDe1RetimingController self, AtPdhChannel owner);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE1RETIMINGCONTROLLERINTERNAL_H_ */


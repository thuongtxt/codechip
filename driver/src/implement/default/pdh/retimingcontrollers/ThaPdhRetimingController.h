/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhRetimingController.h
 * 
 * Created Date: May 29, 2017
 *
 * Description : Retiming controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHRETIMINGCONTROLLER_H_
#define _THAPDHRETIMINGCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhRetimingController *ThaPdhRetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 ThaPdhRetimingControllerTxCsCounter(ThaPdhRetimingController self, eBool r2c);
eAtRet ThaPdhRetimingControllerSlipBufferEnable(ThaPdhRetimingController self, eBool enable);
eBool ThaPdhRetimingControllerSlipBufferIsEnabled(ThaPdhRetimingController self);
eAtRet ThaPdhRetimingControllerSlipBufferSsmEnable(ThaPdhRetimingController self, eBool enable);
eBool  ThaPdhRetimingControllerSlipBufferSsmIsEnabled(ThaPdhRetimingController self);
eAtRet ThaPdhRetimingControllerSlipBufferSsmBomSend(ThaPdhRetimingController self, uint8 value, uint32 nTimes);
uint8 ThaPdhRetimingControllerSlipBufferTxSsmGet(ThaPdhRetimingController self);
uint32 ThaPdhRetimingControllerSlipBufferTxSsmSentThresholdGet(ThaPdhRetimingController self);

void   ThaPdhRetimingControllerSlipBufferTxHwSsmFrameTypeUpdate(ThaPdhRetimingController self, uint16 frameType);
void   ThaPdhRetimingControllerSlipBufferDefaultSet(ThaPdhRetimingController self);
void   ThaPdhRetimingControllerSlipBufferTxHwDebug(ThaPdhRetimingController self);
AtPdhChannel ThaPdhRetimingControllerOwnerGet(ThaPdhRetimingController self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHRETIMINGCONTROLLER_H_ */


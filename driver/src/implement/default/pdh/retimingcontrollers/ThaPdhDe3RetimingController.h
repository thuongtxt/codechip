/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe3RetimingController.h
 * 
 * Created Date: May 29, 2017
 *
 * Description : DS3/E3 retiming controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE3RETIMINGCONTROLLER_H_
#define _THAPDHDE3RETIMINGCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPdhRetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe3RetimingController *ThaPdhDe3RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concretes */
ThaPdhDe3RetimingController ThaPdhDe3RetimingControllerNew(AtPdhChannel owner);

eAtModulePdhRet ThaPdhDe3RetimingControllerTxUdlBitOrderSet(ThaPdhDe3RetimingController self, eAtBitOrder bitOrder);
eAtBitOrder ThaPdhDe3RetimingControllerTxUdlBitOrderGet(ThaPdhDe3RetimingController self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE3RETIMINGCONTROLLER_H_ */


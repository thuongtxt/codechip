/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhRetimingControllerInternal.h
 * 
 * Created Date: May 30, 2017
 *
 * Description : Internal data and methods for the ds1/ds3 retiming controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHRETIMINGCONTROLLERINTERNAL_H_
#define _THAPDHRETIMINGCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "AtPdhChannel.h"
#include "AtPdhDe1.h"
#include "ThaPdhRetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhRetimingControllerMethods
    {
    uint32 (*TxCsCounter)(ThaPdhRetimingController self, eBool r2c);
    eAtRet (*SlipBufferEnable)(ThaPdhRetimingController self, eBool enable);
    eBool  (*SlipBufferIsEnabled)(ThaPdhRetimingController self);
    eAtRet (*SlipBufferSsmEnable)(ThaPdhRetimingController self, eBool enable);
    eBool  (*SlipBufferSsmIsEnabled)(ThaPdhRetimingController self);
    eAtRet (*SlipBufferSsmBomSend)(ThaPdhRetimingController self, uint8 value, uint32 nTimes);
    uint8  (*SlipBufferTxSsmGet)(ThaPdhRetimingController self);
    uint32  (*SlipBufferTxSsmSentThresholdGet)(ThaPdhRetimingController self);

    void   (*SlipBufferTxHwSsmFrameTypeUpdate)(ThaPdhRetimingController self, uint16 frameType);
    void   (*SlipBufferDefaultSet)(ThaPdhRetimingController self);
    void   (*SlipBufferTxHwDebug)(ThaPdhRetimingController self);
    } tThaPdhRetimingControllerMethods;

typedef struct tThaPdhRetimingController
    {
    tAtObject super;
    tThaPdhRetimingControllerMethods *methods;

    /* Private data */
    AtPdhChannel pdhChannel;
    } tThaPdhRetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPdhRetimingController ThaPdhRetimingControllerObjectInit(ThaPdhRetimingController self, AtPdhChannel owner);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHRETIMINGCONTROLLERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe1RetimingController.h
 * 
 * Created Date: May 29, 2017
 *
 * Description : DS1/E1 retiming controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE1RETIMINGCONTROLLER_H_
#define _THAPDHDE1RETIMINGCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe1RetimingController *ThaPdhDe1RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPdhDe1RetimingController ThaPdhDe1RetimingControllerNew(AtPdhChannel owner);
void ThaPdhDe1RetimingControllerSlipBufferSsmSaBitsMaskSet(ThaPdhDe1RetimingController self, uint8 saBitsMask);
uint8 ThaPdhDe1RetimingControllerSlipBufferSsmSaBitsMaskGet(ThaPdhDe1RetimingController self);
eAtRet ThaPdhDe1RetimingControllerSlipBufferTxSignalingEnable(ThaPdhDe1RetimingController self, eBool enable);
eBool ThaPdhDe1RetimingControllerSlipBufferTxSignalingIsEnabled(ThaPdhDe1RetimingController self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE1RETIMINGCONTROLLER_H_ */


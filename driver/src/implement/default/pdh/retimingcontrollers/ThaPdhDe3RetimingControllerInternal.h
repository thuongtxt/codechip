/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe3TimingControllerInternal.h
 * 
 * Created Date: Jun 2, 2017
 *
 * Description : DS3 retiming controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE3TIMINGCONTROLLERINTERNAL_H_
#define _THAPDHDE3TIMINGCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPdhRetimingControllerInternal.h"
#include "ThaPdhDe3RetimingController.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe3RetimingControllerMethods
    {
    uint32 (*SlipBufferTxFramerControlRegister)(ThaPdhDe3RetimingController self);
    uint32 (*SlipBufferRxFramerControlRegister)(ThaPdhDe3RetimingController self);
    uint32 (*SlipBufferSsmEnableRegister)(ThaPdhDe3RetimingController self);
    uint32 (*SlipBufferTxSlipStickyRegister)(ThaPdhDe3RetimingController self);
    uint32 (*SlipBufferRxFrameStatusRegister)(ThaPdhDe3RetimingController self);
    eAtModulePdhRet (*TxUdlBitOrderSet)(ThaPdhDe3RetimingController self, eAtBitOrder bitOrder);
    eAtBitOrder (*TxUdlBitOrderGet)(ThaPdhDe3RetimingController self);
    }tThaPdhDe3RetimingControllerMethods;

typedef struct tThaPdhDe3RetimingController
    {
    tThaPdhRetimingController super;
    tThaPdhDe3RetimingControllerMethods *methods;
    } tThaPdhDe3RetimingController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPdhDe3RetimingController ThaPdhDe3RetimingControllerObjectInit(ThaPdhDe3RetimingController self, AtPdhChannel owner);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE3TIMINGCONTROLLERINTERNAL_H_ */


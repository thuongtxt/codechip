/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe1RetimingController.c
 *
 * Created Date: May 29, 2017
 *
 * Description : DS1/E1 retiming controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhDe1RetimingControllerInternal.h"
#include "../../man/ThaDeviceInternal.h"
#include "../ThaModulePdhReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhDe1RetimingController)(self))

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaPdhDe1RetimingControllerMethods m_methods;

/* Override */
static tThaPdhRetimingControllerMethods m_ThaPdhRetimingControllerOverride;

/* Save super implementation */
static const tThaPdhRetimingControllerMethods *m_ThaPdhRetimingControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPdhDe1 De1(ThaPdhDe1RetimingController self)
    {
    return (ThaPdhDe1)ThaPdhRetimingControllerOwnerGet((ThaPdhRetimingController)self);
    }

static uint32 BaseAddress(ThaPdhDe1RetimingController self)
    {
    AtChannel channel = (AtChannel)De1(self);
    return ThaModulePdhBaseAddress((ThaModulePdh)AtChannelModuleGet(channel));
    }

static uint32 AbsoluteOffset(ThaPdhDe1RetimingController self)
    {
    return BaseAddress(self) + AtChannelIdGet((AtChannel)De1(self));
    }

static uint32 AddressWithLocalAddress(ThaPdhDe1RetimingController self, uint32 localAddress)
    {
    return localAddress + AbsoluteOffset(self);
    }

static uint32 SlipBufferTxFramerControlRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_txctrl_pen_Base);
    }

static uint32 SlipBufferRxFramerControlRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_rxctrl_pen_Base);
    }

static uint32 SlipBufferSsmEnableRegister(ThaPdhDe1RetimingController self)
    {
    return cAf6Reg_ssm_enable + BaseAddress(self);
    }

static uint32 SlipBufferSsmRxStatusRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6_rxfrm_status_pen);
    }

static uint32 SlipBufferSsmRxCrcCounterRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6_rxfrm_crc_err_cnt + 64);
    }

static uint32 SlipBufferSsmRxReiCounterRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6_rxfrm_rei_cnt + 64);
    }

static uint32 SlipBufferSsmRxFbeCounterRegister(ThaPdhDe1RetimingController self)
    {
    return AddressWithLocalAddress(self, cAf6_rxfrm_fbe_cnt + 64);
    }

static void SlipBufferRxFramerControlTxHwSsmFrameTypeUpdate(ThaPdhDe1RetimingController self, uint16 frameType)
    {
    uint32 address, regVal;
    AtChannel channel = (AtChannel)De1(self);

    address = mMethodsGet(mThis(self))->SlipBufferRxFramerControlRegister(mThis(self));
    regVal  = mChannelHwRead(channel , address, cAtModulePdh);
    mRegFieldSet(regVal, cAf6_rxctrl_pen_RxDE1Md_, ThaPdhDe1FrameModeSw2Hw((ThaPdhDe1)channel, frameType));
    mFieldIns(&regVal, cAf6_rxctrl_pen_RxDE1LofThres_Mask, cAf6_rxctrl_pen_RxDE1LofThres_Shift, 1);
    mChannelHwWrite(channel, address, regVal, cAtModulePdh);
    }

static void SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate(ThaPdhDe1RetimingController self, uint16 frameType)
    {
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtChannel channel = (AtChannel)De1(self);

    address = SlipBufferTxFramerControlRegister(mThis(self));
    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_TxDE1Md_, ThaPdhDe1FrameModeSw2Hw((ThaPdhDe1)channel, frameType));
    mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    }

static void SlipBufferTxHwSsmFrameTypeUpdate(ThaPdhRetimingController self, uint16 frameType)
    {
    SlipBufferRxFramerControlTxHwSsmFrameTypeUpdate(mThis(self), frameType);
    mMethodsGet(mThis(self))->SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate(mThis(self), frameType);
    }

static void SlipBufferTxHwDebugFrameStatusShow(ThaPdhDe1RetimingController self)
    {
    uint32 address, regVal;
    AtChannel channel = (AtChannel)De1(self);

    AtPrintc(cSevNormal, "* Slip buffer:\r\n");

    /* show hw frame status */
    address = mMethodsGet(self)->SlipBufferSsmRxStatusRegister(self);
    regVal  = mChannelHwRead(channel, address, cAtModulePdh) & cBit2_0;
    AtPrintc(cSevNormal, "  Slip Buffer RX frame status: ");
    if (regVal == 5)
        AtPrintc(cSevNormal, "In frame\n");
    else if (regVal == 0)
        AtPrintc(cSevCritical, "LOF\n");
    else
        AtPrintc(cSevWarning, "Unkown code = 0x%0x\n", regVal);
    }

static void SlipBufferTxHwDebugFrameCounterShow(ThaPdhDe1RetimingController self)
    {
    uint32 address, regVal;
    AtChannel channel = (AtChannel)De1(self);

    /* show CRC counter */
    address = mMethodsGet(self)->SlipBufferSsmRxCrcCounterRegister(self);
    regVal  = mChannelHwRead(channel, address, cAtModulePdh) & cBit7_0;
    AtPrintc(cSevNormal, "  Slip Buffer RX frame CRC counter: %d\n", regVal);

    /* show REI */
    address = mMethodsGet(self)->SlipBufferSsmRxReiCounterRegister(self);
    regVal  = mChannelHwRead(channel, address, cAtModulePdh) & cBit7_0;
    AtPrintc(cSevNormal, "  Slip Buffer RX frame REI counter: %d\n", regVal);

    /* show FBE */
    address = mMethodsGet(self)->SlipBufferSsmRxFbeCounterRegister(self);
    regVal  = mChannelHwRead(channel , address, cAtModulePdh) & cBit7_0;
    AtPrintc(cSevNormal, "  Slip Buffer RX frame FBE counter: %d\n", regVal);
    }

static void SlipBufferTxHwDebugFrameStickyShow(ThaPdhDe1RetimingController self)
    {
    uint32 address, regVal, fieldIdx, regIdx;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtChannel channel = (AtChannel)De1(self);
    uint32 baseAddress = BaseAddress(self);

    fieldIdx = AtChannelIdGet(channel);
    regIdx = fieldIdx/32;
    /* show data slip sticky */
    address = cAf6_txslip_stk0_upen + baseAddress;
    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    mFieldGet(longRegVal[regIdx], (cBit0 << (fieldIdx%32)), (fieldIdx%32), uint32, &regVal);
    AtPrintc(cSevNormal, "  Slip Buffer Data slip sticky: %s \n", regVal ? "raise":"clear");
    if (regVal)
        {
        AtOsalMemInit((void*)longRegVal, 0, sizeof(longRegVal));
        mFieldIns(&longRegVal[regIdx], (cBit0 << (fieldIdx%32)), (fieldIdx%32), regVal);
        mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
        }

    /* show frame slip sticky */
    address = cAf6_txslip_stk1_upen + baseAddress;
    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    mFieldGet(longRegVal[regIdx], (cBit0 << (fieldIdx%32)), (fieldIdx%32), uint32, &regVal);
    AtPrintc(cSevNormal, "  Slip Buffer Frame slip sticky: %s \n", regVal ? "raise":"clear");
    if (regVal)
        {
        AtOsalMemInit((void*)longRegVal, 0, sizeof(longRegVal));
        mFieldIns(&longRegVal[regIdx], (cBit0 << (fieldIdx%32)), (fieldIdx%32), regVal);
        mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
        }
    }

static void SlipBufferTxHwDebug(ThaPdhRetimingController self)
    {
    SlipBufferTxHwDebugFrameStatusShow(mThis(self));
    SlipBufferTxHwDebugFrameCounterShow(mThis(self));
    mMethodsGet(mThis(self))->SlipBufferTxHwDebugFrameStickyShow(mThis(self));
    }

static uint32 SsmEnableRegDwordIndex(AtPdhChannel self)
    {
    return AtChannelIdGet((AtChannel)self) / 32;
    }

static uint32 SlipBufferSsmEnableRegMask(AtPdhChannel self)
    {
    return cBit0 << (AtChannelIdGet((AtChannel)self) % 32);
    }

static uint32 SlipBufferSsmEnableRegShift(AtPdhChannel self)
    {
    return AtChannelIdGet((AtChannel)self) % 32;
    }

static eAtRet SlipBufferEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address = SlipBufferSsmEnableRegister(mThis(self));
    uint32 regIndex = SsmEnableRegDwordIndex(channel);
    uint32 mask = SlipBufferSsmEnableRegMask(channel);
    uint32 shift = SlipBufferSsmEnableRegShift(channel);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    mFieldIns(&longRegVal[regIndex], mask, shift, enable ? 1 : 0);
    mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferIsEnabled(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address = SlipBufferSsmEnableRegister(mThis(self));
    uint32 regIndex = SsmEnableRegDwordIndex(channel);
    uint32 mask = SlipBufferSsmEnableRegMask(channel);
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(channel , address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    return (longRegVal[regIndex] & mask) ? cAtTrue : cAtFalse;
    }

static void SlipBufferDefaultSet(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address;
    uint32 longRegVal[cThaLongRegMaxSize];

    address = SlipBufferTxFramerControlRegister(mThis(self));
    AtOsalMemInit((void*)longRegVal, 0, sizeof(longRegVal));
    mFieldIns(&longRegVal[1], cAf6_txctrl_pen_E1_Bypass_Enable_Mask, cAf6_txctrl_pen_E1_Bypass_Enable_Shift, 1);
    mFieldIns(&longRegVal[1], cAf6_txctrl_pen_E2_Bypass_Enable_Mask, cAf6_txctrl_pen_E2_Bypass_Enable_Shift, 1);
    mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);

    address = mMethodsGet(mThis(self))->SlipBufferRxFramerControlRegister(mThis(self));
    mChannelHwWrite(channel, address, 0, cAtModulePdh);

    SlipBufferEnable(self, cAtFalse);
    }

static uint32 UserNtimesToLocalNtimes(AtPdhDe1 self, uint32 nTimes)
    {
    uint32 maxNtimes = AtPdhDe1TxBomSentMaxNTimes(self);
    if (nTimes >= maxNtimes)
        return 0; /* send continuously */

    if (nTimes == 0) /* stop sent */
        return 1;

    return nTimes;
    }

static uint32 SlipBufferSsmBomThresholdGet(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 address = SlipBufferTxFramerControlRegister(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    return mRegField(longRegVal[0], cAf6_txctrl_pen_SSMCount_);
    }

static eAtRet SlipBufferSsmBomValueNtimesSet(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 address = SlipBufferTxFramerControlRegister(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_SSMMess_, value);
    mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_SSMCount_, nTimes);
    mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);

    return cAtOk;
    }

static eAtRet SlipBufferSsmBomValueSet(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    SlipBufferSsmBomValueNtimesSet(self, value, nTimes);

    return cAtOk;
    }

static eAtRet SlipBufferSsmBomValueForToggleSendingSet(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes, eBool isToggle)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = SlipBufferTxFramerControlRegister(self);

    if (isToggle)
        {
        mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
        mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_SSM_Enable_, 0);
        mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
        }

    SlipBufferSsmBomValueNtimesSet(self, value, UserNtimesToLocalNtimes((AtPdhDe1)De1(self), nTimes));

    if (isToggle)
        {
        /* Must sleep to compliant to HW design */
        AtOsalUSleep(250);

        mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
        mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_SSM_Enable_, 1);
        mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
        }

    return cAtOk;
    }

static eBool SlipBufferBomSendingIsReady(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address, dwordIndex = SsmEnableRegDwordIndex(channel);

    address = cAf6_txtrl_ssmsta_pen + BaseAddress(self);
    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    return (longRegVal[dwordIndex] & SlipBufferSsmEnableRegMask(channel)) ? cAtTrue : cAtFalse;
    }

static eBool SlipBufferBomSendingCanWaitForReady(ThaPdhDe1RetimingController self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)De1(self));
    return ThaModulePdhSlipBufferBomSendingCanWaitForReady(pdhModule);
    }

static eAtRet SlipBufferBomSendingWaitForReady(ThaPdhDe1RetimingController self)
    {
    uint32 elapse = 0, timeoutMs = 100;
    AtOsal osal = AtSharedDriverOsalGet();
    tAtOsalCurTime curTime, startTime;

    if (!mMethodsGet(self)->SlipBufferBomSendingCanWaitForReady(self))
        return cAtOk;

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);

    while (elapse < timeoutMs)
        {
        if (mMethodsGet(self)->SlipBufferBomSendingIsReady(self))
            return cAtOk;

        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorDevBusy;
    }

static eBool SlipBufferBomIsSending(ThaPdhDe1RetimingController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)De1(self);
    uint32 maxNtimes = AtPdhDe1TxBomSentMaxNTimes(de1);
    uint32 previousNtimes = AtPdhDe1TxBomSentCfgNTimes(de1);

    if (!mMethodsGet(self)->SlipBufferBomSendingCanWaitForReady(self))
        return cAtFalse;

    if ((maxNtimes == previousNtimes) && mMethodsGet(self)->SlipBufferBomSendingIsEnabled(self))
        return cAtTrue;

    if (mMethodsGet(self)->SlipBufferBomSendingIsReady(self))
        return cAtFalse;

    return cAtTrue;
    }

static eBool IsContinouslySentAndContinouoslySendTheSameValueNow(ThaPdhDe1RetimingController self, uint8 value, uint32 newNtimes)
    {
    AtPdhDe1 de1 = (AtPdhDe1)De1(self);
    uint32 maxNtimes = AtPdhDe1TxBomSentMaxNTimes(de1);
    uint32 previousNtimes = AtPdhDe1TxBomSentCfgNTimes(de1);
    uint8  previousBomSent = AtPdhDe1TxBomGet(de1);

    if (SlipBufferBomIsSending(self)
        && (maxNtimes == previousNtimes)
        && (maxNtimes == newNtimes)
        && (value == previousBomSent))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsContinouslySentAndOneShotSendWithTheSameValueNow(ThaPdhDe1RetimingController self, uint8 value, uint32 newNtimes)
    {
    AtPdhDe1 de1 = (AtPdhDe1)De1(self);
    uint32 maxNtimes = AtPdhDe1TxBomSentMaxNTimes(de1);
    uint32 previousNtimes = AtPdhDe1TxBomSentCfgNTimes(de1);
    uint8  previousBomSent = AtPdhDe1TxBomGet(de1);

    if ((maxNtimes == previousNtimes)
        && (maxNtimes != newNtimes)
        && (value == previousBomSent))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsContinouslySending(ThaPdhDe1RetimingController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)De1(self);
    uint32 maxNtimes = AtPdhDe1TxBomSentMaxNTimes(de1);
    uint32 previousNtimes = AtPdhDe1TxBomSentCfgNTimes(de1);

    if (SlipBufferBomIsSending(self)
        && (maxNtimes == previousNtimes))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsWillStopSending(uint32 nTimes)
    {
    return (nTimes == 0) ? cAtTrue : cAtFalse;
    }

static eBool BomToggleShouldSend(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    if (!IsContinouslySending(self) && IsWillStopSending(nTimes))
        return cAtFalse;

    if (IsContinouslySentAndContinouoslySendTheSameValueNow(self, value, nTimes))
        return cAtFalse;

    return cAtTrue;
    }

static eBool ShouldWaitPreviouslySentFinished(ThaPdhDe1RetimingController self)
    {
    return !IsContinouslySending(self) ? cAtTrue : cAtFalse;
    }

static eBool ShouldToggleHw(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    if (IsContinouslySentAndOneShotSendWithTheSameValueNow(self, value, nTimes))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet SlipBufferSsmBomToggleSending(ThaPdhDe1RetimingController self, uint8 value, uint32 nTimes)
    {
    eAtRet ret = cAtOk;
    eBool shouldToggle = ShouldToggleHw(self, value, nTimes);

    if (!BomToggleShouldSend(self, value, nTimes))
        return cAtOk;

    if (ShouldWaitPreviouslySentFinished(self))
        {
        ret = SlipBufferBomSendingWaitForReady(self);
        if (ret != cAtOk)
            return ret;
        }

    return mMethodsGet(self)->SlipBufferSsmBomValueForToggleSendingSet(self, value, nTimes, shouldToggle);
    }

static eBool SlipBufferBomSendingIsEnabled(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = SlipBufferTxFramerControlRegister(self);

    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    return mRegField(longRegVal[0], cAf6_txctrl_pen_SSM_Enable_) ? cAtTrue : cAtFalse;
    }

static void SlipBufferSsmSaBitsMaskSet(ThaPdhDe1RetimingController self, uint8 saBitsMask)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThis(self));
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_SSMSasel_, saBitsMask);
    mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    }

static uint8 SlipBufferSsmSaBitsMaskGet(ThaPdhDe1RetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThis(self));
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    return (uint8)mRegField(longRegVal[0], cAf6_txctrl_pen_SSMSasel_);
    }

static uint32 TxCsCounter(ThaPdhRetimingController self, eBool r2c)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 channelId = AtChannelIdGet((AtChannel)channel);
    uint32 offset = (!r2c) ? (64 + channelId) : channelId;
    uint32 baseAddress = cAf6_txslip_cnt0_pen + BaseAddress(mThis(self));
    return mChannelHwRead(channel, (baseAddress + offset), cAtModulePdh);
    }

static eAtRet SlipBufferSsmEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThis(self));
    uint32 longRegVal[cThaLongRegMaxSize];
    eBool isE1 = AtPdhDe1IsE1((AtPdhDe1)channel);

    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_SSM_Enable_, enable ? 1 : 0);
    mRegFieldSet(longRegVal[0], cAf6_txctrl_pen_SSMCount_, isE1 ? 0 : 10); /* Just need to send continously in case of E1 */
    mChannelHwLongWrite(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);

    return cAtOk;
    }

static eBool SlipBufferSsmIsEnabled(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThis(self));
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    return (longRegVal[0] & cAf6_txctrl_pen_SSM_Enable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool SsmIsSentContinously(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint8 nTimes = AtPdhDe1IsE1((AtPdhDe1)channel) ? 0 : 10;
    return (nTimes == 0) ? cAtTrue : cAtFalse;
    }

static eAtRet SlipBufferSsmBomSend(ThaPdhRetimingController self, uint8 value, uint32 nTimes)
    {
    if (SsmIsSentContinously(self))
        return mMethodsGet(mThis(self))->SlipBufferSsmBomValueSet(mThis(self), value, 0);

    return mMethodsGet(mThis(self))->SlipBufferSsmBomToggleSending(mThis(self), value, nTimes);
    }

static uint8 SlipBufferTxSsmGet(ThaPdhRetimingController self)
    {
    AtPdhChannel channel = (AtPdhChannel)De1(mThis(self));
    uint32 address = SlipBufferTxFramerControlRegister(mThis(self));
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(channel, address, longRegVal, cThaLongRegMaxSize, cAtModulePdh);
    return (uint8)mRegField(longRegVal[0], cAf6_txctrl_pen_SSMMess_);
    }

static uint32 SlipBufferTxSsmSentThresholdGet(ThaPdhRetimingController self)
    {
    uint32 hwThreshold = SlipBufferSsmBomThresholdGet(mThis(self));

    if (hwThreshold == 0)
        return AtPdhDe1TxBomSentMaxNTimes((AtPdhDe1)De1(mThis(self)));

    return hwThreshold;
    }

static eAtRet SlipBufferTxSignalingEnable(ThaPdhDe1RetimingController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool SlipBufferTxSignalingIsEnabled(ThaPdhDe1RetimingController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaPdhRetimingController(ThaPdhDe1RetimingController self)
    {
    ThaPdhRetimingController variable = (ThaPdhRetimingController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhRetimingControllerMethods = mMethodsGet(variable);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhRetimingControllerOverride, m_ThaPdhRetimingControllerMethods, sizeof(m_ThaPdhRetimingControllerOverride));

        mMethodOverride(m_ThaPdhRetimingControllerOverride, TxCsCounter);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmEnable);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmIsEnabled);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferSsmBomSend);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxSsmGet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxSsmSentThresholdGet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxHwSsmFrameTypeUpdate);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferDefaultSet);
        mMethodOverride(m_ThaPdhRetimingControllerOverride, SlipBufferTxHwDebug);
        }

    mMethodsSet(variable, &m_ThaPdhRetimingControllerOverride);
    }

static void Override(ThaPdhDe1RetimingController self)
    {
    OverrideThaPdhRetimingController(self);
    }

static void MethodsInit(ThaPdhDe1RetimingController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, SlipBufferRxFramerControlRegister);
        mMethodOverride(m_methods, SlipBufferSsmRxStatusRegister);
        mMethodOverride(m_methods, SlipBufferSsmRxCrcCounterRegister);
        mMethodOverride(m_methods, SlipBufferSsmRxReiCounterRegister);
        mMethodOverride(m_methods, SlipBufferSsmRxFbeCounterRegister);
        mMethodOverride(m_methods, SlipBufferTxFramerControlTxHwSsmFrameTypeUpdate);
        mMethodOverride(m_methods, SlipBufferSsmSaBitsMaskSet);
        mMethodOverride(m_methods, SlipBufferSsmSaBitsMaskGet);
        mMethodOverride(m_methods, SlipBufferSsmBomValueSet);

        mMethodOverride(m_methods, SlipBufferBomSendingIsEnabled);
        mMethodOverride(m_methods, SlipBufferBomSendingCanWaitForReady);
        mMethodOverride(m_methods, SlipBufferBomSendingIsReady);
        mMethodOverride(m_methods, SlipBufferSsmBomValueForToggleSendingSet);
        mMethodOverride(m_methods, SlipBufferSsmBomToggleSending);
        mMethodOverride(m_methods, SlipBufferTxHwDebugFrameStickyShow);

        mMethodOverride(m_methods, SlipBufferTxSignalingEnable);
        mMethodOverride(m_methods, SlipBufferTxSignalingIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhDe1RetimingController);
    }

ThaPdhDe1RetimingController ThaPdhDe1RetimingControllerObjectInit(ThaPdhDe1RetimingController self, AtPdhChannel owner)
    {
    if (self == NULL)
        return NULL;

    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhRetimingControllerObjectInit((ThaPdhRetimingController)self, owner) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPdhDe1RetimingController ThaPdhDe1RetimingControllerNew(AtPdhChannel owner)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPdhDe1RetimingController newThaPdhDe1RetimingController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newThaPdhDe1RetimingController == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhDe1RetimingControllerObjectInit(newThaPdhDe1RetimingController, owner);
    }

void ThaPdhDe1RetimingControllerSlipBufferSsmSaBitsMaskSet(ThaPdhDe1RetimingController self, uint8 saBitsMask)
    {
    if (self)
        mMethodsGet(mThis(self))->SlipBufferSsmSaBitsMaskSet(self, saBitsMask);
    }

uint8 ThaPdhDe1RetimingControllerSlipBufferSsmSaBitsMaskGet(ThaPdhDe1RetimingController self)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferSsmSaBitsMaskGet(self);

    return 0;
    }

eAtRet ThaPdhDe1RetimingControllerSlipBufferTxSignalingEnable(ThaPdhDe1RetimingController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferTxSignalingEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhDe1RetimingControllerSlipBufferTxSignalingIsEnabled(ThaPdhDe1RetimingController self)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferTxSignalingIsEnabled(self);
    return cAtFalse;
    }

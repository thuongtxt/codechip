/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhRetimingController.c
 *
 * Created Date: May 29, 2017
 *
 * Description : DS1/DS3 retiming controller implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaPdhRetimingControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaPdhRetimingControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxCsCounter(ThaPdhRetimingController self, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);
    return 0;
    }

static eAtRet SlipBufferEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool SlipBufferIsEnabled(ThaPdhRetimingController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SlipBufferSsmEnable(ThaPdhRetimingController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool SlipBufferSsmIsEnabled(ThaPdhRetimingController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SlipBufferSsmBomSend(ThaPdhRetimingController self, uint8 value, uint32 nTimes)
    {
    AtUnused(self);
    AtUnused(value);
    AtUnused(nTimes);
    return cAtErrorModeNotSupport;
    }

static uint8 SlipBufferTxSsmGet(ThaPdhRetimingController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 SlipBufferTxSsmSentThresholdGet(ThaPdhRetimingController self)
    {
    AtUnused(self);
    return 0;
    }

static void SlipBufferTxHwSsmFrameTypeUpdate(ThaPdhRetimingController self, uint16 frameType)
    {
    AtUnused(self);
    AtUnused(frameType);
    }

static void SlipBufferDefaultSet(ThaPdhRetimingController self)
    {
    AtUnused(self);
    }

static void SlipBufferTxHwDebug(ThaPdhRetimingController self)
    {
    AtUnused(self);
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "retiming_controller";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPdhRetimingController object = (ThaPdhRetimingController)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(pdhChannel);
    }

static void OverrideAtObject(ThaPdhRetimingController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPdhRetimingController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaPdhRetimingController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TxCsCounter);
        mMethodOverride(m_methods, SlipBufferEnable);
        mMethodOverride(m_methods, SlipBufferIsEnabled);
        mMethodOverride(m_methods, SlipBufferSsmEnable);
        mMethodOverride(m_methods, SlipBufferSsmIsEnabled);
        mMethodOverride(m_methods, SlipBufferSsmBomSend);
        mMethodOverride(m_methods, SlipBufferTxSsmGet);
        mMethodOverride(m_methods, SlipBufferTxSsmSentThresholdGet);
        mMethodOverride(m_methods, SlipBufferTxHwSsmFrameTypeUpdate);
        mMethodOverride(m_methods, SlipBufferDefaultSet);
        mMethodOverride(m_methods, SlipBufferTxHwDebug);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhRetimingController);
    }

ThaPdhRetimingController ThaPdhRetimingControllerObjectInit(ThaPdhRetimingController self, AtPdhChannel owner)
    {
    if (self == NULL)
        return NULL;

    /* Clear memory */
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->pdhChannel = owner;

    return self;
    }

uint32 ThaPdhRetimingControllerTxCsCounter(ThaPdhRetimingController self, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->TxCsCounter(self, r2c);
    return 0;
    }

eAtRet ThaPdhRetimingControllerSlipBufferEnable(ThaPdhRetimingController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhRetimingControllerSlipBufferIsEnabled(ThaPdhRetimingController self)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferIsEnabled(self);
    return cAtFalse;
    }

eAtRet ThaPdhRetimingControllerSlipBufferSsmEnable(ThaPdhRetimingController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferSsmEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhRetimingControllerSlipBufferSsmIsEnabled(ThaPdhRetimingController self)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferSsmIsEnabled(self);
    return cAtFalse;
    }

eAtRet ThaPdhRetimingControllerSlipBufferSsmBomSend(ThaPdhRetimingController self, uint8 value, uint32 nTimes)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferSsmBomSend(self, value, nTimes);
    return cAtErrorNullPointer;
    }

uint8 ThaPdhRetimingControllerSlipBufferTxSsmGet(ThaPdhRetimingController self)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferTxSsmGet(self);
    return 0;
    }

uint32 ThaPdhRetimingControllerSlipBufferTxSsmSentThresholdGet(ThaPdhRetimingController self)
    {
    if (self)
        return mMethodsGet(self)->SlipBufferTxSsmSentThresholdGet(self);
    return 0;
    }

void ThaPdhRetimingControllerSlipBufferTxHwSsmFrameTypeUpdate(ThaPdhRetimingController self, uint16 frameType)
    {
    if (self)
        mMethodsGet(self)->SlipBufferTxHwSsmFrameTypeUpdate(self, frameType);
    }

void ThaPdhRetimingControllerSlipBufferDefaultSet(ThaPdhRetimingController self)
    {
    if (self)
        mMethodsGet(self)->SlipBufferDefaultSet(self);
    }

void ThaPdhRetimingControllerSlipBufferTxHwDebug(ThaPdhRetimingController self)
    {
    if (self)
        mMethodsGet(self)->SlipBufferTxHwDebug(self);
    }

AtPdhChannel ThaPdhRetimingControllerOwnerGet(ThaPdhRetimingController self)
    {
    return self ? self->pdhChannel : NULL;
    }

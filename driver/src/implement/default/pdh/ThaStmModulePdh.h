/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaStmModulePdh.h
 * 
 * Created Date: Nov 13, 2012
 *
 * Description : STM PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THASTMMODULEPDH_H_
#define _THASTMMODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModulePdh.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaPdhNotCare 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* The following enumeration defines constants indicating sources selected for
the SPE mapper */
typedef enum eThaStsMapMd
    {
    cThaStsMapDs3Vc3Md,     /* DS3 map VC3 */
    cThaStsMapE3Vc3Md,      /* E3 map VC3 */
    cThaStsMapDs3Tu3Md,     /* DS3 map TU3 */
    cThaStsMapE3Tu3Md,      /* E3 map TU3 */
    cThaStsMapUnknown
    }eThaStsMapMd;

typedef enum eThaStsVtMapCtrl
    {
    cThaVt2E1MapMd          = 0,    /* E1 to Vt2 Map */
    cThaVt15Ds1MapMd        = 1,    /* DS1 to Vt15 Map */
    cThaVc3Ds3MapMd         = 3,    /* DS3 to VC3 Map */
    cThaVc3E3MapMd          = 2,    /* E3 to VC3 Mapp */
    cThaTu3Ds3MapMd         = cThaVc3Ds3MapMd,   /* DS3 to Tu3 Map */
    cThaTu3E3MapMd          = cThaVc3E3MapMd    /* E3 to Tu3 Map */
    }eThaStsVtMapCtrl;

typedef enum eThaPdhStsVtDmapCtrl
    {
    cThaStsVtVt15Ds1DmapMd          = 0x00,     /* Vt15 to DS1 DeMap */
    cThaStsVtVt2E1DmapMd            = 0x01,     /* E1 to Vt2 DeMap */
    cThaStsVtVc3Ds3DmapMd           = 0x32,     /* VC3 to DS3 DeMap */
    cThaStsVtVc3E3DmapMd            = 0x33,     /* VC3 to E3 DeMap */
    cThaStsVtVt15BCepDmapMd         = 0x34,     /* VT1.5/TU11 Basic CEP */
    cThaStsVtVt2BCepDmapMd          = 0x35,     /* VT2/TU12 Basic CEP */
    cThaStsVtVc3FracCepVt15DmapMd   = 0x58,     /* STS1/VC3 Fractional CEP
                                            carrying VT15/TU11 */
    cThaStsVtVc3FracCepVt2DmapMd    = 0x18,     /* STS1/VC3 Fractional CEP
                                            carrying VT15/TU11 */
    cThaStsVtVc3FracCepE3DmapMd     = 0x28,     /* STS1/VC3 Fractional CEP
                                            carrying E3 */
    cThaStsVtVc3FracCepDs3DmapMd    = 0x68,     /* STS1/VC3 Fractional CEP
                                            carrying Ds3 */
    cThaStsVtVc3BCepDmapMd          = 0x38,     /* VC3 Basic CEP */
    cThaStsVtVc4FracCepVt15DmapMd   = 0x59,     /* STS3c/VC4 Fractional CEP
                                            carrying VT15/TU11 */
    cThaStsVtVc4FracCepVt2DmapMd    = 0x19,     /* STS3c/VC4 Fractional CEP
                                            carrying VT15/TU11 */
    cThaStsVtVc4FracCepE3DmapMd     = 0x29,     /* STS3c/VC4 Fractional CEP
                                            carrying E3 */
    cThaStsVtVc4FracCepDs3DmapMd    = 0x69,     /* STS3c/VC4 Fractional CEP
                                            carrying DS3 */
    cThaStsVtVc4BCepDmapMd          = 0x39,     /* VC4 Basic CEP */
    cThaStsVtVc4_4cBCepDmapMd       = 0x3A,     /* VC4-4c Basic CEP */
    cThaStsVtTu3BCepDmapMd          = 0x3C,     /* TU3 Basic CEP */
    cThaStsVtDisDmapMd              = 0x0F      /* Disable STS/VC/VT/TU/DS1/E1/DS3/E3 */
    }eThaPdhStsVtDmapCtrl;

typedef enum eThaPdhStsVtMapCtrl
    {
    cThaStsVtVt15Ds1MapMd          = 0x00,     /* Vt15 to DS1 Map */
    cThaStsVtVt2E1MapMd            = 0x01,     /* E1 to Vt2 Map */
    cThaStsVtVc3E3MapMd            = 0x02,     /* VC3 to E3 Map */
    cThaStsVtTu3E3MapMd            = 0x12,     /* TU3 to E3 Map */
    cThaStsVtVc3Ds3MapMd           = 0x03,     /* VC3 to DS3 Map */
    cThaStsVtTu3Ds3MapMd           = 0x13,     /* TU3 to DS3 Map */
    cThaStsVtVc12BCepMapMd         = 0x04,     /* VT2/TU12 Basic CEP */
    cThaStsVtVc11BCepMapMd         = 0x04,     /* VT15/TU11 Basic CEP */
    cThaStsVtVc3FracCepVt2MapMd    = 0x06,     /* STS1/VC3 Fractional CEP
                                                carrying VT15/TU11 */
    cThaStsVtVc3FracCepVt15MapMd   = 0x16,     /* STS1/VC3 Fractional CEP
                                            carrying VT15/TU11 */
    cThaStsVtVc3FracCepE3MapMd     = 0x07,     /* STS1/VC3 Fractional CEP
                                            carrying E3 */
    cThaStsVtVc3FracCepDs3MapMd    = 0x17,     /* STS1/VC3 Fractional CEP
                                            carrying Ds3 */
    cThaStsVtVc3BCepMapMd          = 0x08,     /* VC3 Basic CEP */
    cThaStsVtVc4FracCepVt2MapMd    = 0x0A,     /* STS3c/VC4 Fractional CEP
                                                carrying VT15/TU11 */
    cThaStsVtVc4FracCepVt15MapMd   = 0x1A,     /* STS3c/VC4 Fractional CEP
                                            carrying VT15/TU11 */
    cThaStsVtVc4FracCepE3MapMd     = 0x0B,     /* STS3c/VC4 Fractional CEP
                                            carrying E3 */
    cThaStsVtVc4FracCepDs3MapMd    = 0x1B,     /* STS3c/VC4 Fractional CEP
                                            carrying DS3 */
    cThaStsVtVc4BCepMapMd          = 0x0C,     /* VC4 Basic CEP */
    cThaStsVtTu3BCepMapMd          = 0x0D,     /* TU3 Basic CEP */
    cThaStsVtVc4_4cBCepMapMd       = 0x1C,     /* VC4-4c Basic CEP */
    cThaStsVtDisMapMd              = 0x0F      /* Disable STS/VC/VT/TU/DS1/E1/DS3/E3 */
    }eThaPdhStsVtMapCtrl;

typedef enum eThaPdhStsPayloadType
    {
    cThaPdhTu11Vt15             = 0 ,                                     /* Type of VT/TUs in TUG-2 is  VT15/TU11*/
    cThaPdhTu12Vt2              = 1 ,                                     /* Type of VT/TUs in TUG-2 is VT2/TU12 */
    cThaPdhStsVc                = 3                                      /* VC/STS/DS3/E3 */
    }eThaPdhStsPayloadType;

typedef enum eThaPdhVtMapMd
    {
    cThaVt15Ds1MapMode = 0, /* DS1 to VT1.5 Map/Demap */
    cThaVt2E1MapMode   = 1, /* E1 to VT2 Map/Demap */
    cThaVt15C11MapMode = 2, /* C11 to VT1.5 Map/Demap */
    cThaVt2C12MapMode  = 3  /* C12 to VT2 Map/Demap */
    }eThaPdhVtMapMd;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh ThaStmModulePdhNew(AtDevice device);

eAtRet ThaPdhStsMapMdSet(AtModulePdh self, AtSdhChannel sdhVc, eThaStsMapMd mapMd);
eAtRet ThaPdhSts1M13Bypass(AtModulePdh self, AtSdhChannel channel, uint8 stsId);
eAtRet ThaPdhRxVtgTypeSet(AtModulePdh self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, eThaPdhStsPayloadType vtgType);
eAtRet ThaPdhVtMapMdSet(AtModulePdh self, AtSdhChannel vc1x, eThaPdhVtMapMd mode);
eAtRet ThaStmModulePdhStsVtMapJitterSet(AtSdhChannel channel, uint32 jitter);
eThaStsMapMd ThaPdhStsMapMdGet(AtModulePdh self, AtSdhChannel sdhVc);

eAtRet ThaPdhDe3MapMdSet(AtModulePdh self, AtPdhChannel channel);
eAtRet ThaPdhDe1MapMdSet(AtModulePdh self, AtPdhChannel channel);
eAtRet ThaPdhDe1RxVtgTypeSet(AtModulePdh self, AtPdhChannel de2, eThaPdhStsPayloadType vtgType);

/* For CEP application */
eAtRet ThaPdhStsVtMapCtrlSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsVtMapCtrl mapMd);
eAtRet ThaPdhStsVtDeMapCtrlSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsVtDmapCtrl demapMd);
eAtRet ThaPdhStsDeMapCtrlSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsVtDmapCtrl demapMd);
eAtRet ThaPdhVcBypassMapMdSet(AtSdhVc sdhVc);
eAtRet ThaPdhStsVtDeMapVc4FractionalVc3Set(AtModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3);
eAtRet ThaPdhStsVtDemapConcatSet(AtModulePdh self, AtSdhChannel channel, uint8 startSts, uint8 numSts);
eAtRet ThaPdhStsVtDemapConcatRelease(AtModulePdh self, AtSdhChannel channel, uint8 stsId);
void ThaPdhStsMapDemapInit(ThaModulePdh self);

eBool ThaModulePdhSdhChannelIsConcatenated(AtSdhChannel sdhChannel);
eAtRet ThaStmModulePdhRxDe3VtgTypeSet(AtModulePdh self, AtPdhChannel channel, eThaPdhStsPayloadType vtgType);
eAtRet ThaStmModulePdhRxStsVtgTypeSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsPayloadType vtgType);
eAtRet ThaPdhStsMapAutoAisEnable(AtModulePdh self, AtSdhChannel sdhVc, eBool enabled);

/* Utils to control PDH over SONET Over CEP mode */
eAtRet ThaPdhStsDe3LiuOverVc3Enable(AtModulePdh self, AtSdhChannel sdhVc, eBool enable);
eBool ThaPdhStsDe3LiuOverVc3IsEnabled(AtModulePdh self, AtSdhChannel sdhVc);
eAtRet ThaPdhStsDe3LiuOverVc3CepUpdateConfig(AtModulePdh self, AtSdhChannel sdhVc);
eAtRet ThaStmModulePdhDs1E1FrmRxMuxSet(ThaStmModulePdh self, AtSdhChannel sdhVc);

/* DS3/E3 LIU over CEP */
eBool ThaStmModulePdhLiuDe3OverVc3IsSupported(ThaStmModulePdh self);
eAtRet ThaPdhStsDe3LiuMapVc3Enable(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool enable);
eBool ThaPdhStsDe3LiuMapVc3IsEnabled(ThaStmModulePdh self, AtSdhChannel sdhVc);
eAtRet ThaPdhStsVtDe1LiuMapVc1xEnable(ThaStmModulePdh self, AtSdhChannel vc1x, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _THASTMMODULEPDH_H_ */


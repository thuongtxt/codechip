/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaModulePdhInternal.h
 * 
 * Created Date: May 27, 2013
 *
 * Description : PDH module class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPDHINTERNAL_H_
#define _THAMODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "../../../generic/pdh/AtModulePdhInternal.h"
#include "../util/ThaUtil.h"
#include "debugger/ThaPdhDebugger.h"

#include "ThaStmModulePdh.h"
#include "ThaPdhNxDs0.h"
#include "ThaPdhDe3.h"
#include "ThaPdhDe2.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePdhMethods
    {
    eAtRet (*DefaultDe1Threshold)(ThaModulePdh self, tThaCfgPdhDs1E1FrmRxThres *threshold);
    eBool (*DetectDe1LosByAllZeroPattern)(ThaModulePdh self);
    eBool (*DetectDe3LosByAllZeroPattern)(ThaModulePdh self);
    eBool (*TxDe1AutoAisByDefault)(ThaModulePdh self);
    eBool (*TxDe1AutoAisSwAutoControl)(ThaModulePdh self);
    eBool (*MFASChecked)(ThaModulePdh self);
    eBool (*CanUseTimeslot16InNxDs0)(ThaModulePdh self);
    eBool (*CanUseTimeslot0InNxDs0)(ThaModulePdh self);
    eBool (*NeedCarryTimeslot16OnPw)(ThaModulePdh self);
    eBool (*CasSupported)(ThaModulePdh self);
    eBool (*NeedConfigureSignalingBufferPerTimeslot)(ThaModulePdh self);
    eBool (*HasLiuLoopback)(ThaModulePdh self);
    eBool (*ShouldInvertLiuClock)(ThaModulePdh self);
    eBool (*CanForceDe1Los)(ThaModulePdh self);
    eBool (*HasM13)(ThaModulePdh self, uint8 partId);
    int32 (*SliceOffset)(ThaModulePdh self, uint32 sliceId);
    uint32 (*SliceBase)(ThaModulePdh self, uint32 sliceId); /* Base address of slice. */
    uint8 (*NumSlices)(ThaModulePdh self);
    uint8 (*NumStsInSlice)(ThaModulePdh self);
    uint32 (*BaseAddress)(ThaModulePdh self);
    uint32 (*M12E12ControlOffset)(ThaModulePdh self, ThaPdhDe2 de2);
    uint32 (*M12E12CounterOffset)(ThaModulePdh self, ThaPdhDe2 de2, eBool r2c);
    void (*Vc4_4cConfigurationClear)(ThaModulePdh self, AtSdhChannel sdhChannel, uint8 stsId);
    eAtRet (*De2DefaultSet)(ThaModulePdh self);
    eAtRet (*DefaultSet)(ThaModulePdh self);
    uint8 (*DefaultDe3JitterStuffingMode)(ThaModulePdh self);
    eBool (*CanHotChangeFrameType)(ThaModulePdh self);
    uint32 (*DefaultInbandLoopcodeThreshold)(ThaModulePdh self);

    /* Managed DS3/E3 object for VC-3 mapping */
    AtPdhChannel (*VcDe3ObjectGet)(ThaModulePdh self, AtSdhChannel vc3);

    /* Interrupt processing. */
    eBool (*HasDe3Interrupt)(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore);
    eBool (*HasDe1Interrupt)(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore);
    uint32 (*De3InterruptStatusGet)(ThaModulePdh self, uint32 glbIntr, AtHal hal); /* Return slice OR interrupt */
    uint32 (*De1InterruptStatusGet)(ThaModulePdh self, uint32 glbIntr, AtHal hal); /* Return slice OR interrupt */
    void (*De3InterruptProcess)(ThaModulePdh self, uint32 de3Intr, AtHal hal);
    void (*De1InterruptProcess)(ThaModulePdh self, uint32 de1Intr, AtHal hal);
    eBool (*De1LiuShouldReportLofWhenLos)(ThaModulePdh self);
    eBool (*De3LiuShouldReportLofWhenLos)(ThaModulePdh self);
    eAtRet (*De1InterruptEnable)(ThaModulePdh self, eBool enable);
    eAtRet (*De3InterruptEnable)(ThaModulePdh self, eBool enable);
    eBool (*ShouldReportBerViaBerController)(ThaModulePdh self);

    /* For interrupt processing only
     * Please important notice that sliceId and de3Id are hardware ID numbering
     * for interrupt processing. They may not be compatible to hardware ID
     * numbering of configuration. */
    AtPdhDe3 (*De3ChannelFromHwIdGet)(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id);
    AtPdhDe1 (*De1ChannelFromHwIdGet)(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id);

    /* Interrupt managers */
    AtInterruptManager (*PrmInterruptManagerObjectCreate)(ThaModulePdh self);
    AtInterruptManager (*MdlInterruptManagerObjectCreate)(ThaModulePdh self);

    /* For backward compatible */
    uint32 (*StartVersionSupportEbitAlarm)(ThaModulePdh self);
    uint32 (*StartVersionSupportDe3Disabling)(ThaModulePdh self);
    eBool (*De3TxLiuEnableIsSupported)(ThaModulePdh self);
    uint32 (*StartVersionFixDs3AisInterrupt)(ThaModulePdh self);
    eBool  (*De1RxSlipBufferIsSupported)(ThaModulePdh self);
    eBool (*TxRetimingSignalingIsSupported)(ThaModulePdh self);
    eBool (*ShouldRejectTxChangeWhenChannelBoundPw)(ThaModulePdh self);

    /* Registers */
    uint32 (*StmDe1StsDeftOffset)(ThaModulePdh self, uint8 stsId);
    uint32 (*BpvErrorCounterRegister)(ThaModulePdh self);
    eBool (*TxLiuIsPositiveClockInvert)(ThaModulePdh self);

    /* Registers */
    uint32 (*DS3E3RxFrmrperChnCurStatCtrlBase)(ThaModulePdh self);
    uint32 (*DS1E1J1TxFrmrSignalingBufferRegister)(ThaModulePdh self);
    uint32 (*RxPerStsVCPldCtrlBase)(ThaModulePdh self);
    uint32 (*RxM12E12MuxBase)(ThaModulePdh self);
    uint32 (*DS3E3RxFrmrperChnIntrEnCtrlBase)(ThaModulePdh self);
    uint32 (*RxM12E12FrmChnIntrEnCtrlBase)(ThaModulePdh self);
    uint32 (*RxM12E12FrmChnIntrChngStatusBase)(ThaModulePdh self);
    uint32 (*RxM12E12FrmChnAlarmCurStatusBase)(ThaModulePdh self);
    uint32 (*DS1E1J1RxFrmrCrcErrCntBase)(ThaModulePdh self);
    uint32 (*DS1E1J1RxFrmrReiCntBase)(ThaModulePdh self);
    uint32 (*DS1E1J1RxFrmrFBECntBase)(ThaModulePdh self);
    uint32 (*DS1E1J1RxFrmrperChnIntrEnCtrlBase)(ThaModulePdh self);
    uint32 (*De1RxFrmrPerChnCurrentAlmBase)(ThaModulePdh self);
    uint32 (*De1RxFrmrPerChnInterruptAlmBase)(ThaModulePdh self);
    uint32 (*RxM23E23FrmrReiCntBase)(ThaModulePdh self);
    uint32 (*RxM23E23FrmrFBECntBase)(ThaModulePdh self);
    uint32 (*RxM23E23FrmrParCntBase)(ThaModulePdh self);
    uint32 (*RxM23E23FrmrCbitParCntBase)(ThaModulePdh self);
    uint32 (*RxDs3E3OHProCtrlBase)(ThaModulePdh self);
    uint32 (*RxDe3FeacBufferBase)(ThaModulePdh self);
    uint32 (*DS3E3RxFrmrperChnIntrStatCtrlBase)(ThaModulePdh self);
    uint32 (*De1RxFramerHwStatusBase)(ThaModulePdh self);
    uint32 (*Rxm23e23ChnIntrCfgBase)(ThaModulePdh self);

    /* Diagnostic registers */
    uint32 (*DiagLineBaseAddress)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineRxClockInvertRegister)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineTestEnableRegister)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineTestStatusRegister)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineTestErrorInsertRegister)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineModeRegister)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineRemoteLoopbackRegister)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineEc1ModeRegister)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineMask)(ThaModulePdh self, AtPdhChannel channel);
    uint32 (*DiagLineShift)(ThaModulePdh self, AtPdhChannel channel);

    /* PRBS */
    AtPrbsEngine (*De1PrbsEngineCreate)(ThaModulePdh self, AtPdhDe1 de1);
    AtPrbsEngine (*De3PrbsEngineCreate)(ThaModulePdh self, AtPdhDe3 de3);
    AtPrbsEngine (*NxDs0PrbsEngineCreate)(ThaModulePdh self, AtPdhNxDS0 nxDs0);

    /* For debugging */
    void (*SdhChannelRegsShow)(ThaModulePdh self, AtSdhChannel sdhChannel);
    void (*PdhChannelRegsShow)(ThaModulePdh self, AtPdhChannel pdhChannel);
    void (*De3RegsShow)(ThaModulePdh self, AtPdhChannel de3);
    eBool (*RegExist)(ThaModulePdh self, uint32 addressBase);
    ThaPdhDebugger (*DebuggerObjectCreate)(ThaModulePdh self);

    /* PRM */
    eBool (*PrmShouldCompareCRBit)(ThaModulePdh self);
    uint32 (*De1PrmOffset)(ThaModulePdh self, ThaPdhDe1 de1);
    uint32 (*PrmStsAndTug2Offset)(ThaModulePdh self);
    eBool  (*PrmFcsBitOrderIsSupported)(ThaModulePdh self);
    AtPdhPrmController (*PrmControllerObjectCreate)(ThaModulePdh self, AtPdhDe1 de1);
    AtPktAnalyzer (*PrmAnalyzerObjectCreate)(ThaModulePdh self);

    /* DS3/E3 OffSet ReadOnly and Read2Clear Registers */
    uint32 (*De3CounterOffset)(ThaModulePdh self, eBool r2c);

    /* MDL */
    eBool  (*MdlIsSupported)(ThaModulePdh self);
    AtPdhMdlController (*MdlControllerCreate)(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType);

    /* Backward compatible */
    uint32 (*StartVersionSupportBom)(ThaModulePdh self);
    eBool (*InbandLoopcodeIsSupported)(ThaModulePdh self);
    uint32 (*StartVersionSupportPrm)(ThaModulePdh self);
    uint32 (*StartVersionSupportDe1Ssm)(ThaModulePdh self);
    uint32 (*StartVersionSupportDe3Ssm)(ThaModulePdh self);
    uint32 (*StartVersionSupportDe1SsmSendingReadyIndication)(ThaModulePdh self);
    uint32 (*StartVersionSupportAisDownStream)(ThaModulePdh self);
    uint32 (*StartVersionSupportDe3FramedSatop)(ThaModulePdh self);
    eBool  (*De3FramedSatopIsSupported)(ThaModulePdh self);
    eBool  (*De3AisSelectablePatternIsSupported)(ThaModulePdh self);
    uint32 (*StartVersionSupportAlarmForwardingStatus)(ThaModulePdh self);
    eBool  (*ForcedAisCorrectionIsSupported)(ThaModulePdh self);
    eBool  (*LiuForcedAisIsSupported)(ThaModulePdh self);
    eBool  (*RxHw3BitFeacSignalIsSupported)(ThaModulePdh self);
    eBool (*PrmIsSupported)(ThaModulePdh self);
    uint32 (*StartVersionSupportDe1LomfConsequentialAction)(ThaModulePdh self);
    uint32 (*StartVersionSupportDe1IdleCodeInsertion)(ThaModulePdh self);
    eBool (*De1FastSearchIsSupported)(ThaModulePdh self);

    /* Diag error generator common engine */
    AtErrorGenerator (*De1TxErrorGeneratorGet)(ThaModulePdh self);
    AtErrorGenerator (*De1RxErrorGeneratorGet)(ThaModulePdh self);
    AtErrorGenerator (*De3TxErrorGeneratorGet)(ThaModulePdh self);
    AtErrorGenerator (*De3RxErrorGeneratorGet)(ThaModulePdh self);

    /* To handle Divide Coefficient for Error Generator */
    uint32 (*ErrorGeneratorBerDivCoefficient)(ThaModulePdh self);

	/* Optional for AIS All Ones Pattern */
    eBool (*ShouldDetectAisAllOnesWithUnframedMode)(ThaModulePdh self);
    AtObjectAny (*De1AttControllerCreate)(ThaModulePdh self, AtChannel channel);
    AtObjectAny (*De2AttControllerCreate)(ThaModulePdh self, AtChannel channel);
    AtObjectAny (*De3AttControllerCreate)(ThaModulePdh self, AtChannel channel);

    /* For debugging counter mode */
    eAtRet (*DebugCountersModuleSet)(ThaModulePdh self, uint32 module);

    /* For channelized BERT */
    eAtRet (*HoVcUnChannelize)(ThaModulePdh self, AtSdhChannel hoVc);
    eAtRet (*HoVcChannelizeRestore)(ThaModulePdh self, AtSdhChannel hoVc);

    /* Register offset */
    uint32 (*TimeslotDefaultOffset)(ThaModulePdh self, ThaPdhDe1 de1);
    uint32 (*RxSlipBufferCounterRoOffset)(ThaModulePdh self, eBool r2c);
    }tThaModulePdhMethods;

typedef struct tThaModulePdh
    {
    tAtModulePdh super;
    const tThaModulePdhMethods *methods;

    /* Private data */
    eBool interruptIsEnabled;
    ThaPdhDebugger debugger;
    uint32 countersModule;
    AtPktAnalyzer prmAnalyzer;
    }tThaModulePdh;

typedef struct tThaStmModulePdhMethods
    {
    int32 (*OffsetByHwSts)(ThaStmModulePdh self, uint8 stsHwId, uint8 slice);
    uint32 (*DefaultOffset)(ThaStmModulePdh self, AtSdhChannel sdhVc, uint8 stsId, uint8 vtgId, uint8 vtId);
    int32 (*OffsetBySwSts)(ThaStmModulePdh self, AtSdhChannel sdhVc, uint8 sts);
    uint8 (*CoreOfSts)(ThaStmModulePdh self, AtSdhChannel sdhVc, uint8 stsId);
    eAtRet (*StsVtDemapConcatSet)(ThaStmModulePdh self, AtSdhChannel channel, uint8 startSts, uint8 numSts);
    eBool (*StsIsSlaveInChannel)(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId);
    eAtRet (*StsVtDemapConcatRelease)(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId);
    eAtRet (*Ds1E1FrmRxMuxSet)(ThaStmModulePdh self, AtSdhChannel sdhVc);

    uint32 (*StsVtDemapControlOffset)(ThaStmModulePdh self, AtPdhChannel channel);
    uint32 (*PdhRxPerStsVcPayloadControlOffset)(ThaStmModulePdh self, AtPdhChannel channel);
    eAtRet (*StsVtDemapVc4FractionalVc3Set)(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3);

    /* Backward compatible */
    eBool (*LiuDe3OverVc3IsSupported)(ThaStmModulePdh self);

    mDefineMaskShiftField(ThaStmModulePdh, RegStsVtDemapCcatEn)
    mDefineMaskShiftField(ThaStmModulePdh, RegStsVTDemapCcatMaster)
    }tThaStmModulePdhMethods;

typedef struct tThaStmModulePdh
    {
    tThaModulePdh super;
    const tThaStmModulePdhMethods *methods;

    /* Private data */
    }tThaStmModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh ThaModulePdhObjectInit(AtModulePdh self, AtDevice device);
AtModulePdh ThaStmModulePdhObjectInit(AtModulePdh self, AtDevice device);

/* Debugger */
ThaPdhDebugger ThaModulePdhDebugger(ThaModulePdh self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPDHINTERNAL_H_ */


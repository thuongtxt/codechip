/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhPrmAnalyzerReg.h
 * 
 * Created Date: Nov 7, 2017
 *
 * Description : PRM analyzer registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPRMANALYZERREG_H_
#define _THAPDHPRMANALYZERREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : CONFIG CONTROL RX PRM MONITOR
Reg Addr   : 0xA042
Reg Formula:
    Where  :
Reg Desc   :
config control DeStuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_prm_ctrl_Base                                                                      0xA042

/*--------------------------------------
BitField Name: prmid_cfg
BitField Type: R/W
BitField Desc: PRM ID config monitor,
BitField Bits: [13:04]
--------------------------------------*/
#define cAf6_upen_prm_ctrl_prmid_cfg_Mask                                                             cBit13_4
#define cAf6_upen_prm_ctrl_prmid_cfg_Shift                                                                   4

/*--------------------------------------
BitField Name: prm_mon_en
BitField Type: R/W
BitField Desc: (1) : enable monitor, (0) disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_prm_ctrl_prm_mon_en_Mask                                                               cBit0
#define cAf6_upen_prm_ctrl_prm_mon_en_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : BUFFER RX PRM MONITOR MESSAGE
Reg Addr   : 0x08800 - 0x00887F
Reg Formula: 0x08800+ $LOC
    Where  :
           + $LOC (0-127) : LOC ID
Reg Desc   :
config standard message PRM

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxprm_mon_Base                                                                    0x08800
#define cAf6Reg_upen_rxprm_mon_WidthVal                                                                     32

/*--------------------------------------
BitField Name: val_mes
BitField Type: R/W
BitField Desc: value message of PRM
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_rxprm_mon_val_mes_Mask                                                               cBit7_0
#define cAf6_upen_rxprm_mon_val_mes_Shift                                                                    0

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPRMANALYZERREG_H_ */


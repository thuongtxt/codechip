/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhPrmAnalyzer.h
 * 
 * Created Date: Nov 7, 2017
 *
 * Description : PRM analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPRMANALYZER_H_
#define _THAPDHPRMANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPrmAnalyzer * ThaPdhPrmAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer ThaPdhPrmAnalyzerNew(AtChannel channel);

uint32 ThaPdhPrmAnalyzerUpenRxPrmCtrlAddress(ThaPdhPrmAnalyzer self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPRMANALYZER_H_ */


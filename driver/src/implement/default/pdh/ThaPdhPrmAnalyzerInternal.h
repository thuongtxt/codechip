/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhPrmAnalyzerInternal.h
 * 
 * Created Date: Nov 7, 2017
 *
 * Description : PRM analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPRMANALYZERINTERNAL_H_
#define _THAPDHPRMANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/pktanalyzer/AtPktAnalyzerInternal.h"
#include "ThaPdhPrmAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPrmAnalyzerMethods
    {
    eAtRet (*ChannelHwSet)(ThaPdhPrmAnalyzer self, AtChannel channel);
    uint32 (*UpenRxPrmCtrlBase)(ThaPdhPrmAnalyzer self);
    uint32 (*UpenRxprmMonBase)(ThaPdhPrmAnalyzer self);
    }tThaPdhPrmAnalyzerMethods;

typedef struct tThaPdhPrmAnalyzer
    {
    tAtPktAnalyzer super;
    const tThaPdhPrmAnalyzerMethods *methods;
    }tThaPdhPrmAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer ThaPdhPrmAnalyzerObjectInit(AtPktAnalyzer self, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPRMANALYZERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhMdlControllerInternal.h
 * 
 * Created Date:
 *
 * Description : PDH DE3
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHMDLCONTROLLERINTERNAL_H_
#define _THAPDHMDLCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe3.h"
#include "../../../generic/pdh/AtPdhMdlControllerInternal.h"
#include "../../default/util/ThaUtil.h"
#include "ThaPdhMdlController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif


#define cMdlPeriodicInMs 1000 /* 1000 Mili-seconds */

/* MDL ADDRESS */
#define cMdlAddrStartIndex                  0
#define cMdlAddress1Index                   cMdlAddrStartIndex
#define cMdlAddressSize                     2
#define cMdlAddress1CVal                    0x3C
#define cMdlAddress1RVal                    0x3E
#define cMdlAddress2AnsiVal                 0x01
#define cMdlAddress2AttVal                  0x08

/* MDL COMTROL */
#define cMdlControlStartIndex              (cMdlAddrStartIndex+cMdlAddressSize)
#define cMdlControlSize                     1
#define cMdlControlVal                      0x03

/* MDL TYPE */
#define cMdlTypeStartIndex                 (cMdlControlStartIndex+cMdlControlSize)
#define cMdlTypeIndex                       cMdlTypeStartIndex
#define cMdlTypeSize                        1

/* MDL EIC */
#define cMdlEICStartIndex                   (cMdlTypeStartIndex+cMdlTypeSize)
#define cMdlLICStartIndex                   (cMdlEICStartIndex+cMdlEICStringSize)
#define cMdlFICStartIndex                   (cMdlLICStartIndex+cMdlLICStringSize)
#define cMdlUINITStartIndex                 (cMdlFICStartIndex+cMdlFICStringSize)
#define cMdlPortPfiGenStartIndex            (cMdlUINITStartIndex+cMdlUINTStringSize)


/* MDL MESSEGE BUFFERSIZE W/O FCS and FLAGS */
#define cMdlDataBufferSizes (cMdlPortPfiGenStartIndex + cMdlPortPfiGenStringSize)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhMdlControllerMethods
    {
    eAtRet (*HwStandardHeaderSet)(ThaPdhMdlController self, uint32 standard);
    eAtRet (*HwSend)(ThaPdhMdlController self, uint8 *messageBuffer, uint32 bufferLength);
    uint32 (*HwReceived)(ThaPdhMdlController self, uint8 *messageBuffer, uint32 bufferLength);
    eBool  (*HwMessageReceived)(ThaPdhMdlController self);
    uint32 (*DefectHistoryReadToClear)(ThaPdhMdlController self, eBool r2c);
    uint32 (*InterruptOffset)(ThaPdhMdlController self);
    } tThaPdhMdlControllerMethods;

typedef struct tThaPdhMdlController
    {
    tAtPdhMdlController super;
    const tThaPdhMdlControllerMethods *methods;

    /* Private data */
    uint8 txBuffer[cMdlDataBufferSizes];
    uint8 rxBuffer[cMdlDataBufferSizes];
    eBool enable;
    uint32 standard;
    AtOsalMutex mutex;
    }tThaPdhMdlController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHMDLCONTROLLERINTERNAL_H_ */

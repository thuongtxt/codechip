/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaModulePdh
 *
 * File        : ThaModulePdh.h
 *
 * Created Date: Sep 10, 2012
 *
 * Author      : ntdung
 *
 * Description : This file contains common prototypes of PDH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPDH_H_
#define _THAMODULEPDH_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "AtPktAnalyzer.h"
#include "ThaPdhDe1.h"
#include "ThaPdhDe2.h"
#include "ThaPdhDe3.h"
#include "../man/intrcontroller/ThaInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaModulePdhOriginalBaseAddress 0x700000

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tThaModulePdh * ThaModulePdh;
typedef struct tThaStmModulePdh * ThaStmModulePdh;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh ThaModulePdhNew(AtDevice device);
AtIterator ThaModulePdhRegisterIteratorCreate(AtModule module);

eAtRet ThaModulePdhDefaultDe1Threshold(ThaModulePdh self, tThaCfgPdhDs1E1FrmRxThres *threshold);
uint32 ThaModulePdhPartOffset(AtModule self, uint8 partId);
eBool ThaModulePdhTxDe1AutoAisSwAutoControl(ThaModulePdh self);
eBool ThaModulePdhTxDe1AutoAisByDefault(ThaModulePdh self);
eBool ThaModulePdhDe1MFASChecked(ThaModulePdh self);
eBool ThaModulePdhCanUseTimeslot16InNxDs0(ThaModulePdh self);
eBool ThaModulePdhCanUseTimeslot0InNxDs0(ThaModulePdh self);
eBool ThaModulePdhNeedCarryTimeslot16OnPw(ThaModulePdh self);
eBool ThaModulePdhCasSupported(ThaModulePdh self);
eBool ThaModulePdhNeedConfigureSignalingBufferPerTimeslot(ThaModulePdh self);
eBool ThaModulePdhEbitAlarmSupported(ThaModulePdh self);
eBool ThaModulePdhHasM13(ThaModulePdh self, uint8 partId);
void ThaModulePdhM13Init(ThaModulePdh self);
eBool ThaModulePdhTxLiuIsPositiveClockInvert(ThaModulePdh self);
eBool ThaModulePdhCanDisableDe3(ThaModulePdh self);
uint8 ThaModulePdhDefaultDe3JitterStuffingMode(ThaModulePdh self);
eBool ThaModulePdhDs3AisInterruptIsFixed(ThaModulePdh self);
eBool ThaModulePdhDe1LiuShouldReportLofWhenLos(ThaModulePdh self);
eBool ThaModulePdhDe3LiuShouldReportLofWhenLos(ThaModulePdh self);
eBool ThaModulePdhRxHw3BitFeacSignalIsSupported(ThaModulePdh self);
eBool ThaModulePdhCanHotChangeFrameType(ThaModulePdh self);

void ThaModulePdhVc4_4cConfigurationClear(ThaModulePdh self, AtSdhChannel sdhChannel, uint8 stsId);
eBool ThaModulePdhHasLiuLoopback(ThaModulePdh self);
eBool ThaModulePdhCanForceDe1Los(ThaModulePdh self);

int32 ThaModulePdhSliceOffset(ThaModulePdh self, uint32 sliceId);
uint32 ThaModulePdhM12E12ControlOffset(ThaModulePdh self, ThaPdhDe2 de2);
uint32 ThaModulePdhM12E12CounterOffset(ThaModulePdh self, ThaPdhDe2 de2, eBool r2c);

AtPrbsEngine ThaModulePdhDe1PrbsEngineCreate(ThaModulePdh self, AtPdhDe1 de1);
AtPrbsEngine ThaModulePdhDe3PrbsEngineCreate(ThaModulePdh self, AtPdhDe3 de3);
AtPrbsEngine ThaModulePdhNxDs0PrbsEngineCreate(ThaModulePdh self, AtPdhNxDS0 nxDs0);
AtPrbsEngine ThaModulePdhDe1LinePrbsEngineCreate(ThaModulePdh self, AtPdhDe1 de1);
AtPrbsEngine ThaModulePdhDe3LinePrbsEngineCreate(ThaModulePdh self, AtPdhDe3 de3);

/* Registers */
uint32 ThaModulePdhTxDE1SigDS1Mask(ThaModulePdh self);
uint32 ThaModulePdhTxDE1SigDS1Shift(ThaModulePdh self);
uint32 ThaModulePdhTxDE1SigLineIDMask(ThaModulePdh self);
uint32 ThaModulePdhTxDE1SigLineIDShift(ThaModulePdh self);
uint32 ThaModulePdhBpvErrorCounterRegister(ThaModulePdh self);
uint32 ThaModulePdhDe1RxFrmrPerChnCurrentAlmRegister(ThaModulePdh self);
uint32 ThaModulePdhDe1RxFrmrPerChnInterruptAlmRegister(ThaModulePdh self);
uint32 ThaModulePdhDS3E3RxFrmrperChnIntrEnCtrlRegister(ThaModulePdh self);
uint32 ThaModulePdhDS3E3RxFrmrperChnIntrStatCtrlRegister(ThaModulePdh self);
uint32 ThaModulePdhDS3E3RxFrmrperChnCurStatCtrlRegister(ThaModulePdh self);
uint32 ThaRegDS1E1J1TxFrmrSignalingBuffer(ThaModulePdh self);
uint32 ThaModulePdhRxPerStsVCPldCtrlRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM12E12MuxRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM12E12FrmChnIntrEnCtrlRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM12E12FrmChnIntrChngStatusRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM12E12FrmChnAlarmCurStatusRegister(ThaModulePdh self);
uint32 ThaModulePdhDS1E1J1RxFrmrCrcErrCntRegister(ThaModulePdh self);
uint32 ThaModulePdhDS1E1J1RxFrmrReiCntRegister(ThaModulePdh self);
uint32 ThaModulePdhDS1E1J1RxFrmrFBECntRegister(ThaModulePdh self);
uint32 ThaModulePdhDS1E1J1RxFrmrperChnIntrEnCtrlRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM23E23FrmrReiCntRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM23E23FrmrFBECntRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM23E23FrmrParCntRegister(ThaModulePdh self);
uint32 ThaModulePdhRxM23E23FrmrCbitParCntRegister(ThaModulePdh self);
uint32 ThaModulePdhRxDs3E3OHProCtrlRegister(ThaModulePdh self);
uint32 ThaModulePdhRxDe3FeacBufferRegister(ThaModulePdh self);
uint32 ThaModulePdhDe1RxFramerHwStatusRegister(ThaModulePdh self);
uint32 ThaModulePdhRxm23e23ChnIntrCfgBase(ThaModulePdh self);

/* Diagnostic registers */
uint32 ThaModulePdhDiagLineTestEnableRegister(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineTestStatusRegister(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineTestErrorInsertRegister(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineModeRegister(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineRemoteLoopbackRegister(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineRxClockInvertRegister(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineEc1ModeRegister(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineMask(ThaModulePdh self, AtPdhChannel channel);
uint32 ThaModulePdhDiagLineShift(ThaModulePdh self, AtPdhChannel channel);
eAtRet ThaModulePdhDiagLiuLoopbackSet(AtChannel self, uint8 loopbackMode);
uint8 ThaModulePdhDiagLiuLoopbackGet(AtChannel self);
eAtRet ThaModulePdhDiagLiuRxClockInvertEnable(AtChannel self, eBool enable);
eAtRet ThaModulePdhDiagLiuLineModeSet(AtChannel self, eBool isDSxMode);
eAtRet ThaModulePdhDiagLiuEc1ModeEnable(AtChannel self, eBool enable);

/* Slice and hw ID in slice retrive */
eAtRet ThaPdhChannelHwIdGet(AtPdhChannel channel, eAtModule module,  uint8* sliceId, uint8 *hwIdInSlice);
eAtRet ThaPdhChannelHwSlice48AndSts48IdGet(AtPdhChannel channel, uint8* hwSlice48Id, uint8 *hwSts48Id);

/* For debugging */
void ThaModulePdhSdhChannelRegsShow(AtSdhChannel sdhChannel);
void ThaModulePdhPdhChannelRegsShow(AtPdhChannel pdhChannel);
eAtRet ThaModulePdhDebugCountersModuleSet(ThaModulePdh self, uint32 module);
uint32 ThaModulePdhDebugCountersModuleGet(ThaModulePdh self);

/* For interrupt processing. */
AtPdhDe3 ThaModulePdhDe3ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id);
AtPdhDe1 ThaModulePdhDe1ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id);
eAtRet ThaModulePdhDe3InterruptEnable(ThaModulePdh self, eBool enable);
eAtRet ThaModulePdhDe1InterruptEnable(ThaModulePdh self, eBool enable);
void ThaModulePdhInterruptProcess(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore);

/* Base address as default */
uint32 ThaModulePdhBaseAddress(ThaModulePdh self);
uint32 ThaModulePdhSliceBase(ThaModulePdh self, uint32 sliceId);
uint8 ThaModulePdhNumSlices(ThaModulePdh self);
uint8 ThaModulePdhNumStsInSlice(ThaModulePdh self);

/* For LOS detection */
eBool ThaModulePdhDetectDe1LosByAllZeroPattern(ThaModulePdh self);
eBool ThaModulePdhDetectDe3LosByAllZeroPattern(ThaModulePdh self);

/* For SSM  */
eBool ThaModulePdhDe1SsmIsSupported(ThaModulePdh self);
eBool ThaModulePdhSlipBufferBomSendingCanWaitForReady(ThaModulePdh self);
eBool ThaModulePdhDe3SsmIsSupported(ThaModulePdh self);
eBool ThaModulePdhTxRetimingSignalingIsSupported(ThaModulePdh self);

/* PRM */
uint32 ThaModulePdhPrmBaseAddress(ThaModulePdh self);
eBool ThaModulePdhPrmIsSupported(ThaModulePdh self);
AtPdhPrmController ThaModulePdhPrmControllerCreate(ThaModulePdh self, AtPdhDe1 de1);
uint32 ThaModulePdhDe1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1);
uint32 ThaModulePdhPrmStsAndTug2Offset(ThaModulePdh self);
ThaInterruptManager ThaModulePdhPrmInterruptManager(ThaModulePdh self);
AtPktAnalyzer ThaModulePdhPrmAnalyzer(ThaModulePdh self);

/* MDL */
uint32 ThaModulePdhMdlBaseAddress(ThaModulePdh self);
AtPdhMdlController ThaModulePdhMdlControllerCreate(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType);
eBool ThaModulePdhMdlIsSupported(ThaModulePdh self);
eBool ThaModulePdhDatalinkIsSupported(ThaModulePdh self);
ThaInterruptManager ThaModulePdhMdlInterruptManager(ThaModulePdh self);

/* Error generator for ds1/ds3 serial interface */
AtErrorGenerator ThaModulePdhDe1TxErrorGeneratorGet(ThaModulePdh self);
AtErrorGenerator ThaModulePdhDe1RxErrorGeneratorGet(ThaModulePdh self);
AtErrorGenerator ThaModulePdhDe3TxErrorGeneratorGet(ThaModulePdh self);
AtErrorGenerator ThaModulePdhDe3RxErrorGeneratorGet(ThaModulePdh self);
uint32 ThaModulePdhErrorGeneratorBerDivCoefficient(ThaModulePdh self);

/* For AIS All One Pattern */
eBool ThaModulePdhShouldDetectAisAllOnesWithUnframedMode(ThaModulePdh self);

/* BER Monitoring */
eBool ThaModulePdhShouldReportBerViaBerController(ThaModulePdh self);

/* Managed DS3/E3 object for VC-3 mapping */
AtPdhChannel ThaModulePdhVcDe3ObjectGet(ThaModulePdh self, AtSdhChannel vc3);

/* Backward compatible */
eBool ThaModulePdhAisDownStreamIsSupported(ThaModulePdh self);
eBool ThaModulePdhDe3TxLiuEnableIsSupported(ThaModulePdh self);
eBool ThaModulePdhDe3FramedSatopIsSupported(ThaModulePdh self);
eBool ThaModulePdhDe3AisSelectablePatternIsSupported(ThaModulePdh self);
eBool ThaModulePdhBomIsSupported(ThaModulePdh self);
eBool ThaModulePdhInbandLoopcodeIsSupported(ThaModulePdh self);
eBool ThaModulePdhFramerForcedAisCorrectionIsSupported(ThaModulePdh self);
eBool ThaModulePdhLiuForcedAisIsSupported(ThaModulePdh self);
eBool ThaModulePdhAlarmForwardingStatusIsSupported(ThaModulePdh self);
eBool ThaModulePdhPrmFcsBitOrderIsSupported(ThaModulePdh self);
eBool ThaModulePdhDe1LomfConsequentialActionIsSupported(ThaModulePdh self);
eBool ThaModulePdhDe1IdleCodeInsertionIsSupported(ThaModulePdh self);
eBool ThaPdhChannelShouldRejectTxChangeWhenBoundPw(AtPdhChannel channel);
eBool ThaModulePdhDe1FastSearchIsSupported(ThaModulePdh self);

/* For channelized BERT */
eAtRet ThaModulePdhHoVcUnChannelize(ThaModulePdh self, AtSdhChannel hoVc);
eAtRet ThaModulePdhHoVcChannelizeRestore(ThaModulePdh self, AtSdhChannel hoVc);

/* DS0 signalling */
uint32 ThaModulePdhTimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1);
eBool ThaModulePdhDe1RxSlipBufferIsSupported(ThaModulePdh self);
uint32 ThaModulePdhRxSlipBufferCounterRoOffset(ThaModulePdh self, eBool r2c);

void ThaModulePdhChannelIdsShow(ThaModulePdh self, AtPdhChannel pdhChannel);

/* Concrete products */
AtModulePdh Tha60030022ModulePdhNew(AtDevice device);
AtModulePdh Tha60000031ModulePdhNew(AtDevice device);
AtModulePdh Tha60030111ModulePdhNew(AtDevice device);
AtModulePdh Tha60031021ModulePdhNew(AtDevice device);
AtModulePdh Tha60035011ModulePdhNew(AtDevice device);
AtModulePdh Tha60070041ModulePdhNew(AtDevice device);
AtModulePdh Tha60071011ModulePdhNew(AtDevice device);
AtModulePdh Tha60030081ModulePdhNew(AtDevice device);
AtModulePdh Tha60031031ModulePdhNew(AtDevice device);
AtModulePdh Tha60031032ModulePdhNew(AtDevice device);
AtModulePdh Tha60031033ModulePdhNew(AtDevice device);
AtModulePdh Tha60035021ModulePdhNew(AtDevice device);
AtModulePdh Tha60071021ModulePdhNew(AtDevice device);
AtModulePdh Tha60060011ModulePdhNew(AtDevice device);
AtModulePdh Tha60070023ModulePdhNew(AtDevice device);
AtModulePdh Tha60150011ModulePdhNew(AtDevice device);
AtModulePdh Tha60031071ModulePdhNew(AtDevice device);
AtModulePdh Tha60031035ModulePdhNew(AtDevice device);
AtModulePdh Tha60091135ModulePdhNew(AtDevice device);
AtModulePdh Tha60091023ModulePdhNew(AtDevice device);
AtModulePdh Tha60091132ModulePdhNew(AtDevice device);
AtModulePdh Tha60210031ModulePdhNew(AtDevice device);
AtModulePdh Tha60210021ModulePdhNew(AtDevice device);
AtModulePdh Tha60220031ModulePdhNew(AtDevice device);
AtModulePdh Tha60220041ModulePdhNew(AtDevice device);
AtModulePdh Tha60070051ModulePdhNew(AtDevice device);
AtModulePdh Tha61031031ModulePdhNew(AtDevice device);
AtModulePdh Tha60240021ModulePdhNew(AtDevice device);
AtModulePdh Tha61210011ModulePdhNew(AtDevice device);
AtModulePdh Tha60210011ModulePdhNew(AtDevice device);
AtModulePdh Tha61031031ModulePdhNew(AtDevice device);
AtModulePdh Tha61150011ModulePdhNew(AtDevice device);
AtModulePdh Tha61031031ModulePdhNew(AtDevice device);
AtModulePdh Tha61150011ModulePdhNew(AtDevice device);
AtModulePdh Tha61210031ModulePdhNew(AtDevice device);
AtModulePdh Tha60210051ModulePdhNew(AtDevice device);
AtModulePdh Tha60031032CommonModulePdhNew(AtDevice device);
AtModulePdh Tha60210012ModulePdhNew(AtDevice device);
AtModulePdh Tha60210061ModulePdhNew(AtDevice device);
AtModulePdh ThaStmPwProductModulePdhNew(AtDevice device);
AtModulePdh ThaPdhPwProductModulePdhNew(AtDevice device);
AtModulePdh ThaPdhPwProductModulePdhNew(AtDevice device);
AtModulePdh Tha60290011ModulePdhNew(AtDevice device);
AtModulePdh Tha62290011ModulePdhNew(AtDevice device);
AtModulePdh Tha6A000010ModulePdhNew(AtDevice device);
AtModulePdh Tha6A033111ModulePdhNew(AtDevice device);
AtModulePdh Tha6A210031ModulePdhNew(AtDevice device);
AtModulePdh Tha6A290011ModulePdhNew(AtDevice device);
AtModulePdh Tha6A290021ModulePdhNew(AtDevice device);
AtModulePdh Tha60290022ModulePdhNew(AtDevice device);
AtModulePdh Tha6A290022ModulePdhNew(AtDevice device);
AtModulePdh Tha60290022ModulePdhV3New(AtDevice device);
AtModulePdh Tha60291011ModulePdhNew(AtDevice device);
AtModulePdh Tha60291022ModulePdhNew(AtDevice device);
AtModulePdh Tha60290061ModulePdhNew(AtDevice device);
AtModulePdh Tha60290081ModulePdhNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPDH_H_ */

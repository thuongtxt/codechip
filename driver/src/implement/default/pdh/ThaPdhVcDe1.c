/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhVcDe1.c
 *
 * Created Date: Oct 2, 2012
 *
 * Description : This file contains a subclass to inherit ThaPdhDe1 for STM MLPPP card
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "commacro.h"
#include "../util/ThaUtil.h"

#include "ThaPdhVcDe1Internal.h"
#include "ThaModulePdhForStmReg.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "../sdh/ThaModuleSdh.h"
#include "../cdr/ThaModuleCdrStm.h"
#include "../pdh/ThaModulePdhInternal.h"
#include "../man/ThaDevice.h"
#include "../map/ThaModuleStmMap.h"
#include "../../../generic/pdh/AtPdhChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((ThaPdhVcDe1)self)

#define cThaTxFrmrE1SaCfgMask                  cBit15_13
#define cThaTxFrmrE1SaCfgShift                 13

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhVcDe1Methods m_methods;

/* Override */
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tThaPdhDe1Methods    m_ThaPdhDe1Override;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tThaPdhDe1Methods    *m_ThaPdhDe1Methods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaPdhDe1, TxFrmrE1SaCfg)

static eAtRet HwIdFactorGet(ThaPdhDe1 self, uint32 phyModule, uint32 *slice, uint32 *hwStsInSlice, uint32 *hwVtg, uint32 *hwDe1)
    {
    uint8 hwSlice, hwSts;

    AtSdhChannel sdhChannel = ThaPdhDe1ShortestVcGet(self);
    *hwVtg = (uint32)AtSdhChannelTug2Get(sdhChannel);
    *hwDe1  = (uint32)AtSdhChannelTu1xGet(sdhChannel);
    ThaSdhChannel2HwMasterStsId(sdhChannel, phyModule, &hwSlice, &hwSts);
    *slice = (uint32)hwSlice;
    *hwStsInSlice = (uint32)hwSts;
    return cAtOk;
    }

static AtSdhChannel ShortestVcGet(ThaPdhDe1 self)
    {
    return (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    }

static uint32 FlatId(ThaPdhDe1 self, uint32 phyModule)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    ThaPdhDe1HwIdGet(self, phyModule, &slice, &hwIdInSlice, &vtg, &vt);
    return (hwIdInSlice * 32UL) + (vtg * 4UL) + vt;
    }

static int32 SliceOffset(ThaPdhDe1 self)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    ThaPdhDe1HwIdGet(self, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt);
    return ThaModulePdhSliceOffset(pdhModule, slice);
    }

static uint32 Ds1E1J1DefaultOffset(ThaPdhDe1 self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    uint32 partOffset = ThaDeviceModulePartOffset(device, cAtModulePdh, mMethodsGet(self)->PartId(self));
    return (uint32)((int32)ThaPdhDe1FlatId(self) + (int32)partOffset + SliceOffset(self));
    }

static uint32 De1ErrCounterOffset(ThaPdhDe1 de1, eBool r2cEn)
    {
    uint32 offset = mMethodsGet(de1)->Ds1E1J1DefaultOffset(de1);
    return r2cEn ? offset : (512 + offset);
    }

static uint32 TimeslotDefaultOffset(ThaPdhDe1 self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    return ThaModulePdhTimeslotDefaultOffset(pdhModule, self);
    }

static eBool IsE1Framing(uint16 frameType)
    {
    if ((frameType == cAtPdhE1UnFrm) ||
        (frameType == cAtPdhE1Frm)   ||
        (frameType == cAtPdhE1MFCrc))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsDs1Framing(uint16 frameType)
    {
    if ((frameType == cAtPdhDs1J1UnFrm) ||
        (frameType == cAtPdhDs1FrmSf)   ||
        (frameType == cAtPdhDs1FrmEsf)  ||
        (frameType == cAtPdhDs1FrmDDS)  ||
        (frameType == cAtPdhDs1FrmSLC)  ||
        (frameType == cAtPdhJ1FrmSf)    ||
        (frameType == cAtPdhJ1FrmEsf))
        return cAtTrue;

    return cAtFalse;
    }

static eBool FrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
    if (mMethodsGet(mThis(self))->ShouldConstrainToVc1xMapping(mThis(self)))
        return mMethodsGet(mThis(self))->FrameTypeIsSupportForVc1xMapping(mThis(self), frameType);
    return m_AtPdhChannelMethods->FrameTypeIsSupported(self, frameType);
    }

/* This function is to set frame type of a DS1/E1 channel */
static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret;

    /* Reuse super implementation */
    ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->VtMapModeSet(mThis(self), frameType);
    }

static eAtRet DefaultThreshold(ThaPdhDe1 self, tThaCfgPdhDs1E1FrmRxThres *threshold)
    {
    eAtRet ret = m_ThaPdhDe1Methods->DefaultThreshold(self, threshold);
    threshold->aisThres = 1;

    return ret;
    }

static uint8 DefaultFrameMode(AtPdhChannel self)
    {
    AtSdhVc sdhVc = AtPdhChannelVcInternalGet(self);
    eAtSdhChannelType sdhChannelType;

    if (sdhVc == NULL)
        return cAtPdhDs1J1UnFrm;

    sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)sdhVc);

    if (sdhChannelType == cAtSdhChannelTypeVc12)
        return cAtPdhE1UnFrm;
    if (sdhChannelType == cAtSdhChannelTypeVc11)
        return cAtPdhDs1J1UnFrm;

    return cAtPdhDs1J1UnFrm;
    }

static eBool LedStateIsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LedStateSet(AtPdhChannel self, eAtLedState ledState)
    {
	AtUnused(ledState);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtLedState LedStateGet(AtPdhChannel self)
    {
	AtUnused(self);
    return cAtLedStateOff;
    }

static uint32 RxFrmrStatE1SsmMask(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cBit10_7;
    }

static uint8 RxFrmrStatE1SsmShift(ThaPdhDe1 self)
    {
    AtUnused(self);
    return 7;
    }

static eAtRet SsmHwEnable(ThaPdhDe1 self, eBool enable)
    {
    /* No explicit enable bit, driven via Sa-bits mask */
    if (enable && ThaPdhDe1TxHwSaBitMaskGet(self))
        return cAtOk;

    /* synchronize tx&rx sa mask */
    return ThaPdhDe1HwSsmSaBitsMaskSet(self, enable ? cAtPdhE1Sa4 : 0);
    }

static eBool SsmHwIsEnabled(ThaPdhDe1 self)
    {
    if (ThaPdhDe1TxHwSaBitMaskGet(self) > 0)
        return cAtTrue;
    return cAtFalse;
    }

static const char *IdString(AtChannel self)
    {
    return AtChannelIdString((AtChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self));
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    /* BPV-EXZ is not applicable for this class */
    if (counterType == cAtPdhDe1CounterBpvExz)
        return 0;

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    /* BPV-EXZ is not applicable for this class */
    if (counterType == cAtPdhDe1CounterBpvExz)
        return 0;

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static ThaCdrController CdrControllerCreate(ThaPdhDe1 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleCdr cdrModule = (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    return ThaModuleCdrVcDe1CdrControllerCreate(cdrModule, (AtPdhDe1)self);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtPdhLoopbackModeLocalPayload)
        return cAtFalse;

    return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);
    }

static uint32 EncapDs0BitMaskGet(ThaPdhDe1 self)
    {
    eAtPdhDe1FrameType frameType;
    uint32 ds0BitMask = 0xFFFFFFFF;

    frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    if ((frameType == cAtPdhE1Frm) || (frameType == cAtPdhE1MFCrc))
        {
        /* If E1 framing mode is not unframe, the first timeslot is not used.
         * And if signaling is enabled on it, the timeslot 16 is also unused */
        ds0BitMask = (ds0BitMask & (~cBit0));
        if (AtPdhDe1SignalingIsEnabled((AtPdhDe1)self))
            ds0BitMask = (ds0BitMask & (~cBit16));

        return ds0BitMask;
        }

    /* DS1 frame, timeslot from 0 to 23 */
    if ((frameType != cAtPdhDe1FrameUnknown) && (frameType != cAtPdhE1UnFrm))
        ds0BitMask = (ds0BitMask & (~cBit31_24));

    return ds0BitMask;
    }

static uint32 SatopDs0BitMaskGet(ThaPdhDe1 self)
    {
    return (AtPdhDe1IsE1((AtPdhDe1)self)) ? cBit31_0 : cBit23_0;
    }

static uint8 PartId(ThaPdhDe1 self)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    return ThaModuleSdhPartOfChannel(sdhVc);
    }

static uint32 VcDefaultOffset(ThaStmModulePdh modulePdh, AtSdhVc sdhVc)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    return mMethodsGet(modulePdh)->DefaultOffset(modulePdh, sdhChannel,
                                                 AtSdhChannelSts1Get(sdhChannel),
                                                 AtSdhChannelTug2Get(sdhChannel),
                                                 AtSdhChannelTu1xGet(sdhChannel));
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    uint32 forceAbleAlarms = m_AtChannelMethods->RxForcibleAlarmsGet(self);
    return (uint32)((int32)forceAbleAlarms & (~cAtPdhDe1AlarmLos));
    }

static uint8 SdhVc1xHwStsGet(AtSdhChannel vc)
    {
    uint8 hwSlice, hwSts = 0;

    if (vc)
        ThaSdhChannel2HwMasterStsId(vc, cAtModulePdh, &hwSlice, &hwSts);

    return hwSts;
    }

static eBool FrameTypeIsSupportForVc1xMapping(ThaPdhVcDe1 self, uint16 frameType)
    {
    AtSdhChannel vc1x = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet(vc1x);

    if ((sdhChannelType == cAtSdhChannelTypeVc12) && (IsE1Framing(frameType)))
        return cAtTrue;
    if ((sdhChannelType == cAtSdhChannelTypeVc11) && (IsDs1Framing(frameType)))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet VtMapModeSet(ThaPdhVcDe1 self, uint16 frameType)
    {
    AtSdhChannel vc1x = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePdh);
    return ThaPdhVtMapMdSet(pdhModule, vc1x, IsE1Framing(frameType) ? cThaVt2E1MapMode : cThaVt15Ds1MapMode);
    }

static uint32 MapLineOffset(ThaPdhDe1 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleStmMap mapModule = (ThaModuleStmMap)AtDeviceModuleGet(device, cThaModuleMap);
    return ThaModuleStmMapPdhDe1MapLineOffset(mapModule, (AtPdhDe1)self);
    }

static eAtRet HwIdConvert(ThaPdhDe1 self, uint32 phyModule, uint8 *hwSlice, uint8 *hwSts, uint8 *hwVtg, uint8 *hwVt)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);
    eAtRet ret = ThaPdhChannelHwIdGet((AtPdhChannel)self, phyModule, hwSlice, hwSts);

    *hwVtg = AtSdhChannelTug2Get(sdhVc);
    *hwVt  = AtSdhChannelTu1xGet(sdhVc);

    return ret;
    }

static eBool RetimingIsSupported(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldConstrainToVc1xMapping(ThaPdhVcDe1 self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtChannel(ThaPdhVcDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(ThaPdhVcDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, DefaultFrameMode);
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeIsSupported);
        mMethodOverride(m_AtPdhChannelOverride, LedStateSet);
        mMethodOverride(m_AtPdhChannelOverride, LedStateGet);
        mMethodOverride(m_AtPdhChannelOverride, LedStateIsSupported);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe1(ThaPdhVcDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhDe1Methods = mMethodsGet(de1);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, m_ThaPdhDe1Methods, sizeof(tThaPdhDe1Methods));
        mMethodOverride(m_ThaPdhDe1Override, Ds1E1J1DefaultOffset);
        mMethodOverride(m_ThaPdhDe1Override, De1ErrCounterOffset);
        mMethodOverride(m_ThaPdhDe1Override, EncapDs0BitMaskGet);
        mMethodOverride(m_ThaPdhDe1Override, CdrControllerCreate);
        mMethodOverride(m_ThaPdhDe1Override, PartId);
        mMethodOverride(m_ThaPdhDe1Override, DefaultThreshold);
        mMethodOverride(m_ThaPdhDe1Override, FlatId);
        mMethodOverride(m_ThaPdhDe1Override, TimeslotDefaultOffset);
        mMethodOverride(m_ThaPdhDe1Override, SatopDs0BitMaskGet);
        mMethodOverride(m_ThaPdhDe1Override, ShortestVcGet);
        mMethodOverride(m_ThaPdhDe1Override, HwIdFactorGet);
        mMethodOverride(m_ThaPdhDe1Override, HwIdConvert);
        mMethodOverride(m_ThaPdhDe1Override, MapLineOffset);
        mMethodOverride(m_ThaPdhDe1Override, RetimingIsSupported);
        mMethodOverride(m_ThaPdhDe1Override, RxFrmrStatE1SsmMask);
        mMethodOverride(m_ThaPdhDe1Override, RxFrmrStatE1SsmShift);
        mMethodOverride(m_ThaPdhDe1Override, SsmHwEnable);
        mMethodOverride(m_ThaPdhDe1Override, SsmHwIsEnabled);
        mMethodOverride(m_ThaPdhDe1Override, TxFrmrE1SaCfgMask);
        mMethodOverride(m_ThaPdhDe1Override, TxFrmrE1SaCfgShift);
        }

    mMethodsSet(de1, &m_ThaPdhDe1Override);
    }

/* General override of ThaPdhVcDe1 */
static void Override(ThaPdhVcDe1 self)
    {
    OverrideThaPdhDe1(self);
    OverrideAtPdhChannel(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(ThaPdhVcDe1 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, FrameTypeIsSupportForVc1xMapping);
        mMethodOverride(m_methods, VtMapModeSet);
        mMethodOverride(m_methods, ShouldConstrainToVc1xMapping);
        }

    mMethodsSet(self, &m_methods);
    }

AtPdhDe1 ThaPdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPdhVcDe1));

    /* Supper constructor */
    if (ThaPdhDe1ObjectInit(self, SdhVc1xHwStsGet((AtSdhChannel)vc1x), module) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Cache VC-1x */
    AtPdhChannelVcSet((AtPdhChannel)self, vc1x);

    return self;
    }

/* Create a new ThaPdhVcDe1 object */
AtPdhDe1 ThaPdhVcDe1ObjectNew(AtSdhVc vc1x, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newVcDe1 = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaPdhVcDe1));
    if (newVcDe1 == NULL)
        return NULL;

    /* construct it */
    return ThaPdhVcDe1ObjectInit(newVcDe1, vc1x, module);
    }

/* Help function to configure bypass DE1 when mapping Encap channel to VC1x */
eAtRet ThaSdhVcPdhFrameBypass(AtSdhVc sdhVc)
    {
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhVc), cAtModulePdh);
    uint32 regAddr, regVal = 0;
    uint16 frameTypeVal = 0;

    /* Rx frame */
    regAddr = cThaRegDS1E1J1RxFrmrCtrl + VcDefaultOffset(pdhModule, sdhVc);
    mFieldIns(&regVal, cThaRxDE1MdMask, cThaRxDE1MdShift, frameTypeVal);
    mModuleHwWrite(pdhModule, regAddr, regVal);

    /* Tx frame */
    regAddr = cThaRegDS1E1J1TxFrmrCtrl + VcDefaultOffset(pdhModule, sdhVc);
    mFieldIns(&regVal, cThaTxDE1MdMask, cThaTxDE1MdShift, frameTypeVal);
    mModuleHwWrite(pdhModule, regAddr, regVal);

    regVal = 0x7FFFFFFF;
    regAddr = cThaRegDS1E1J1TxFrmrSigingInsCtrl + VcDefaultOffset(pdhModule, sdhVc);
    mFieldIns(&regVal, cThaTXDE1SigBypassMask, cThaTXDE1SigBypassShift, cThaTXDE1SigBypassMaxVal);
    mModuleHwWrite(pdhModule, regAddr, regVal);

    return cAtOk;
    }

eAtRet ThaPdhDe1HwSlice48AndSts48IdGet(ThaPdhDe1 self, uint8 *hwSlice48Id, uint8 *hwSts48Id,  uint8 *hwVtg, uint8 *hwVt)
    {
    uint8 slice24, sts24;
    eAtRet ret = ThaPdhDe1HwIdGet(self, cAtModulePdh, &slice24, &sts24,  hwVtg, hwVt);
    if (ret != cAtOk)
        return ret;
    *hwSlice48Id = (uint8) (slice24 / 2);
    *hwSts48Id = (uint8) ((sts24 * 2) + (slice24 % 2));
    return cAtOk;
    }

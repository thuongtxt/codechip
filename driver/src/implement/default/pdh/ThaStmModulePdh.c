/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaStmModulePdh.c
 *
 * Created Date: Oct 6, 2012
 *
 * Description : STM PDH concrete implementation
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhVc.h"

#include "../pdh/ThaModulePdh.h"
#include "../man/ThaDeviceReg.h"
#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "../sdh/ThaModuleSdh.h"

#include "../../../generic/common/AtChannelInternal.h"
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../generic/pdh/AtPdhDe3Internal.h"

#include "../ocn/ThaModuleOcn.h"
#include "../sdh/ThaModuleSdh.h"
#include "../sdh/ThaSdhVcInternal.h"

#include "ThaPdhDe3Internal.h"
#include "ThaModulePdhInternal.h"
#include "ThaModulePdhForStmReg.h"
#include "ThaStmModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaPdhNumDs2E2InDs3E3 7
#define cDefaultStsVtMapThr 12

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaStmModulePdh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaStmModulePdhMethods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tAtModulePdhMethods  m_AtModulePdhOverride;
static tThaModulePdhMethods m_ThaModulePdhOverride;

/* Save super implementations */
static const tAtModuleMethods       *m_AtModuleMethods = NULL;
static const tThaModulePdhMethods   *m_ThaModulePdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x00700000) && (localAddress <= 0x7FFFFF))
        return cAtTrue;

    return cAtFalse;
    }

static int32 OffsetByHwSts(ThaStmModulePdh self, uint8 stsHwId, uint8 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    return stsHwId;
    }

static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
	AtUnused(self);
    return 0;
    }

static AtHal HalOfChannel(ThaStmModulePdh pdhModule, AtSdhChannel channel, uint8 stsId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)pdhModule);
    uint8 core = mMethodsGet(pdhModule)->CoreOfSts(pdhModule, channel, stsId);

    return AtIpCoreHalGet(AtDeviceIpCoreGet(device, core));
    }

static uint32 ChannelRead(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId, uint32 address)
    {
    return mModuleHwReadByHal(self, address, HalOfChannel(self, channel, stsId));
    }

static void ChannelWrite(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId, uint32 address, uint32 value)
    {
    mModuleHwWriteByHal(self, address, value, HalOfChannel(self, channel, stsId));
    }

static uint8 NumSlice(AtModule self)
    {
    ThaModuleOcn moduleOcn = (ThaModuleOcn)AtDeviceModuleGet((AtModuleDeviceGet(self)), cThaModuleOcn);
    return ThaModuleOcnNumSlices(moduleOcn);
    }

static uint8 NumStsInOneSlice(AtModule self)
    {
    ThaModuleOcn moduleOcn = (ThaModuleOcn)AtDeviceModuleGet((AtModuleDeviceGet(self)), cThaModuleOcn);
    return ThaModuleOcnNumStsInOneSlice(moduleOcn);
    }

static void StmDs1E1InterruptProcess(AtModule self, AtIpCore ipCore)
    {
    uint8 stsId, vtId, numSlice, numStsInSlice, slice_i;
    uint32 perVtIntrStatus;
    uint32 address;
    AtPdhChannel pdhChannel;
    uint32 perStsIntrEn = mModuleHwRead(self, cThaRegDS1E1J1RxFrmrperStsVCIntrEnCtrl);
    uint32 perStsIntrStatus = mModuleHwRead(self, cThaRegDS1E1J1RxFrmrperStsVCIntrOrStat);

    numSlice = NumSlice(self);
    numStsInSlice = NumStsInOneSlice(self);
    perStsIntrStatus &= perStsIntrEn;

    AtUnused(ipCore);

    for (slice_i = 0; slice_i < numSlice; slice_i++)
        {
        for (stsId = 0; stsId < numStsInSlice; stsId++)
            {
            /* Get which sts cause interrupt */
            if (!(perStsIntrStatus & cThaRegDS1E1J1RxFrmrperStsVCIntrStsIdMask(stsId)))
                continue;

            address = (uint32)((int32)cThaRegDS1E1J1RxFrmrperChnIntrOrStat + mMethodsGet((ThaStmModulePdh)self)->OffsetByHwSts((ThaStmModulePdh)self, stsId, slice_i));
            perVtIntrStatus = mModuleHwRead(self, address);
            /* Get which VT-DE1 cause interrupt */
            for (vtId = 0; vtId < cThaOcnNumVtInSts; vtId++)
                {
                if (!(perVtIntrStatus & cThaRegDS1E1J1RxFrmrperChnIntrMask(vtId)))
                    continue;

                /* Get PDH channel from STS ID, VT ID */
                pdhChannel = AtModulePdhHwVtId2PdhChannel((AtModulePdh)self, stsId, vtId);
                if (pdhChannel)
                    AtChannelAllAlarmListenersCall((AtChannel)pdhChannel,
                                                   AtChannelDefectInterruptGet((AtChannel)pdhChannel),
                                                   AtChannelDefectGet((AtChannel)pdhChannel));
                }
            }
        }
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    /* Ds1 E1 interrupt */
    if (glbIntr & cThaRegChipIntrStaPDHDS1E1IntAlarm)
        StmDs1E1InterruptProcess(self, ipCore);
    }

static uint32 ChannelPartOffset(ThaStmModulePdh self, AtSdhChannel channel)
    {
    return ThaModulePdhPartOffset((AtModule)self, ThaModuleSdhPartOfChannel(channel));
    }

static uint32 DefaultOffset(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 slice, hwSts;

    ThaSdhChannelHwStsGet(channel, cAtModulePdh, stsId, &slice, &hwSts);
    return (hwSts * 32UL) + (vtgId * 4UL) + vtId + ChannelPartOffset(self, channel);
    }

static uint32 StsVtDemapControlOffset(ThaStmModulePdh self, AtPdhChannel channel)
    {
    uint8 slice = 0, hwSts = 0, vtgId = 0, vtId = 0;
    eAtPdhChannelType type = AtPdhChannelTypeGet(channel);
    AtUnused(self);
    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cAtModulePdh, &slice, &hwSts);

    if ((type == cAtPdhChannelTypeDs1) ||
        (type == cAtPdhChannelTypeE1))
        {
        vtId  = (uint8)AtChannelIdGet((AtChannel)channel);
        vtgId = (uint8)AtChannelIdGet((AtChannel)AtPdhChannelParentChannelGet(channel));
        }
    else if ((type == cAtPdhChannelTypeDs2) ||
             (type == cAtPdhChannelTypeE2))
        {
        vtId = 0;
        vtgId = (uint8)AtChannelIdGet((AtChannel)channel);
        }

    return (hwSts * 32UL) + (vtgId * 4UL) + vtId + (uint32)ThaModulePdhSliceOffset((ThaModulePdh)self, slice);
    }

static uint8 CoreOfSts(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId)
    {
	AtUnused(stsId);
	AtUnused(channel);
    return AtModuleDefaultCoreGet((AtModule)self);
    }

static int32 OffsetBySwSts(ThaStmModulePdh self, AtSdhChannel sdhVc, uint8 sts)
    {
    uint8 slice, hwSts;

    ThaSdhChannelHwStsGet(sdhVc, cAtModulePdh, sts, &slice, &hwSts);
    return (int32)(hwSts + ChannelPartOffset(self, sdhVc));
    }

static uint32 PdhRxPerStsVcPayloadControlOffset(ThaStmModulePdh self, AtPdhChannel channel)
    {
    uint8 slice = 0, hwSts = 0;
    AtUnused(self);

    ThaPdhChannelHwIdGet((AtPdhChannel)channel, cAtModulePdh, &slice, &hwSts);

    return (hwSts) + (uint32)ThaModulePdhSliceOffset((ThaModulePdh)self, slice);
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
	AtUnused(partId);
	AtUnused(self);
    return cAtTrue;
    }

static uint32 RegStsVtDemapCcatEnMask(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cThaRegStsVtDemapCcatEnMask;
    }

static uint8  RegStsVtDemapCcatEnShift(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cThaRegStsVtDemapCcatEnShift;
    }

static uint32 RegStsVTDemapCcatMasterMask(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cThaRegStsVTDemapCcatMasterMask;
    }

static uint8  RegStsVTDemapCcatMasterShift(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cThaRegStsVTDemapCcatMasterShift;
    }

static uint32 De1RxFrmrPerChnCurrentAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x55400;
    }

static uint32 De1RxFrmrPerChnInterruptAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x55200;
    }

static eAtRet Ds1E1FrmRxMuxSet(ThaStmModulePdh self, AtSdhChannel sdhVc)
    {
    static const uint8 cFromOcnInterface = 0;
    uint32 dReg, regAddress;
    uint8 stsId = AtSdhChannelSts1Get(sdhVc);
    uint8 vtgId = AtSdhChannelTug2Get(sdhVc);
    uint8 vtId  = AtSdhChannelTu1xGet(sdhVc);

    regAddress = (uint32)(cThaRegDS1E1J1RxFrmrMuxCtrl2 + mMethodsGet(self)->OffsetBySwSts(self, sdhVc, stsId));
    dReg = ChannelRead(self, sdhVc, stsId, regAddress);
    mFieldIns(&dReg, cThaDE1MuxSelMask2(vtgId, vtId), cThaDE1MuxSelShift2(vtgId, vtId), cFromOcnInterface);
    ChannelWrite(self, sdhVc, stsId, regAddress, dReg);

    return cAtOk;
    }

static eAtRet RxStsVtgTypeSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsPayloadType vtgType)
    {
    ThaStmModulePdh modulePdh = mThis(self);
    uint8 sts_i;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhVc) + sts_i);
        uint8 vtg_i;
        uint32 regAddress = (uint32)((int32)ThaModulePdhRxPerStsVCPldCtrlRegister((ThaModulePdh)self) + mMethodsGet(modulePdh)->OffsetBySwSts(modulePdh, sdhVc, stsId));
        uint32 regValue   = ChannelRead(modulePdh, sdhVc, stsId, regAddress);
        for (vtg_i = 0; vtg_i < cThaPdhNumDs2E2InDs3E3; vtg_i++)
            mFieldIns(&regValue, cThaPdhVtRxMapTug2TypeMask2(vtg_i), cThaPdhVtRxMapTug2TypeShift2(vtg_i), vtgType);
        ChannelWrite(modulePdh, sdhVc, stsId, regAddress, regValue);
        }

    return cAtOk;
    }

static eAtRet RxDe3VtgTypeSet(AtModulePdh self, AtPdhChannel channel, eThaPdhStsPayloadType vtgType)
    {
    uint32 regValue, regAddress;
    ThaStmModulePdh modulePdh = mThis(self);
    uint8 vtg_i;

    regAddress = ThaModulePdhRxPerStsVCPldCtrlRegister((ThaModulePdh)self) + mMethodsGet(modulePdh)->PdhRxPerStsVcPayloadControlOffset(modulePdh, channel);
    regValue = mChannelHwRead((AtChannel)channel, regAddress, cAtModulePdh);
    for (vtg_i = 0; vtg_i < cThaPdhNumDs2E2InDs3E3; vtg_i++)
        {
        mFieldIns(&regValue, cThaPdhVtRxMapTug2TypeMask2(vtg_i), cThaPdhVtRxMapTug2TypeShift2(vtg_i), vtgType);
        }
    mChannelHwWrite((AtChannel)channel, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

static eBool IsMappedToTu3(AtSdhChannel vc3)
    {
    if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(vc3)) == cAtSdhChannelTypeTu3)
        return cAtTrue;

    return cAtFalse;
    }

static uint8 MapType(AtSdhVc sdhVc)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)sdhVc);

    if (sdhChannelType == cAtSdhChannelTypeVc4_4c)  return 0xC;
    if (sdhChannelType == cAtSdhChannelTypeVc4)     return 0x9;
    if (sdhChannelType == cAtSdhChannelTypeVc3)     return (IsMappedToTu3((AtSdhChannel)sdhVc)) ? 0xd : 0x5;
    if (sdhChannelType == cAtSdhChannelTypeVc11)    return 0x1;
    if (sdhChannelType == cAtSdhChannelTypeVc12)    return 0;

    return 0xFF;
    }

static uint8 DemapType(AtSdhVc sdhVc)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)sdhVc);

    if (sdhChannelType == cAtSdhChannelTypeVc4_4c)  return 0xA;
    if (sdhChannelType == cAtSdhChannelTypeVc4)     return 0x9;
    if (sdhChannelType == cAtSdhChannelTypeVc3)     return (IsMappedToTu3((AtSdhChannel)sdhVc)) ? 0xc : 0x8;
    if (sdhChannelType == cAtSdhChannelTypeVc11)    return 0x4;
    if (sdhChannelType == cAtSdhChannelTypeVc12)    return 0x5;

    return 0xFF;
    }

static uint8 VtgType(AtSdhChannel sdhVc)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet(sdhVc);

    if (sdhChannelType == cAtSdhChannelTypeVc4)  return 0x3;
    if (sdhChannelType == cAtSdhChannelTypeVc3)  return 0x3;
    if (sdhChannelType == cAtSdhChannelTypeVc11) return 0;
    if (sdhChannelType == cAtSdhChannelTypeVc12) return 1;

    return 0xFF;
    }

static eBool IsHoVc(AtSdhVc sdhVc)
    {
    eAtSdhChannelType sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)sdhVc);

    if ((sdhChannelType == cAtSdhChannelTypeVc4_4c) ||
        (sdhChannelType == cAtSdhChannelTypeVc4)    ||
        (sdhChannelType == cAtSdhChannelTypeVc3))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HoVcVtgTypeSet(AtModulePdh pdhModule, AtSdhChannel channel, uint8 sts, uint8 vtgTypeVal)
    {
    uint8 vtg_i;
    eAtRet ret = cAtOk;

    for (vtg_i = 0; vtg_i < cThaPdhNumDs2E2InDs3E3; vtg_i++)
        ret |= ThaPdhRxVtgTypeSet(pdhModule, channel, sts, vtg_i, vtgTypeVal);

    return ret;
    }

static eAtRet VtgTypeSet(AtSdhVc sdhVc, uint8 vtgTypeVal)
    {
    eAtRet ret = cAtOk;
    uint8 vtgId = AtSdhChannelTug2Get((AtSdhChannel)sdhVc);
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhVc), cAtModulePdh);
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    uint8 sts_i;

    AtSdhLine line = ThaSdhLineFromChannel(sdhChannel);
    if (line == NULL)
        return cAtErrorNullPointer;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        {
        uint8 sts1Id, slice, hwSts;
        sts1Id = (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i);

        ThaSdhChannelHwStsGet(sdhChannel, cAtModulePdh, sts1Id, &slice, &hwSts);

        /* Set VTG type for Vc1x */
        ret |= ThaPdhRxVtgTypeSet(pdhModule, sdhChannel, sts1Id, vtgId, vtgTypeVal);

        if (IsHoVc(sdhVc))
            ret |= HoVcVtgTypeSet(pdhModule, sdhChannel, sts1Id, vtgTypeVal);
        }

    return ret;
    }

static uint8 CepMode(eThaPdhStsVtDmapCtrl demapMd)
    {
    return (uint8)((demapMd & cThaStsVtDemapCepMdMask) >> cThaStsVtDemapCepMdShift);
    }

static ThaStmModulePdh StmModulePdh(AtSdhChannel sdhChannel)
    {
    return (ThaStmModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhChannel), cAtModulePdh);
    }

static eBool StsIsSlaveInChannel(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId)
    {
    uint8 slice, hwMasterSts;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint32 StsVtDemapCcatEnMask = mMethodsGet(pdhModule)->RegStsVtDemapCcatEnMask(pdhModule);
    uint32 StsVTDemapCcatMasterMask = mMethodsGet(pdhModule)->RegStsVTDemapCcatMasterMask(pdhModule);
    uint8 StsVTDemapCcatMasterShift = mMethodsGet(pdhModule)->RegStsVTDemapCcatMasterShift(pdhModule);
    uint32 regAddress, regVal;
    eBool isSlave;

    /* Convert to hardware ID */
    ThaSdhChannel2HwMasterStsId(channel, cAtModulePdh, &slice, &hwMasterSts);

    /* This can be a master */
    regAddress = (uint32)(cThaRegStsVtDemapConcatControl + mMethodsGet(pdhModule)->OffsetBySwSts(pdhModule, channel, stsId));
    regVal = ChannelRead(pdhModule, channel, stsId, regAddress);
    isSlave = (regVal & StsVtDemapCcatEnMask) ? cAtTrue : cAtFalse;
    if (!isSlave)
        return cAtFalse;

    /* Check if this is slaver of this master STS */
    if (mRegField(regVal, StsVTDemapCcatMaster) == hwMasterSts)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet StsVtDemapConcatSet(ThaStmModulePdh self, AtSdhChannel channel, uint8 startSts, uint8 numSts)
    {
    uint8 sts_i;
    uint8 slice, hwMasterSts;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint32 StsVtDemapCcatEnMask = mMethodsGet(pdhModule)->RegStsVtDemapCcatEnMask(pdhModule);
    uint8 StsVtDemapCcatEnShift = mMethodsGet(pdhModule)->RegStsVtDemapCcatEnShift(pdhModule);
    uint32 StsVTDemapCcatMasterMask = mMethodsGet(pdhModule)->RegStsVTDemapCcatMasterMask(pdhModule);
    uint8 StsVTDemapCcatMasterShift = mMethodsGet(pdhModule)->RegStsVTDemapCcatMasterShift(pdhModule);

    ThaSdhChannelHwStsGet(channel, cAtModulePdh, startSts, &slice, &hwMasterSts);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 regVal, regAddress;
        uint8 stsId = (uint8)(startSts + sts_i);

        /* Read from hardware */
        regAddress = (uint32)(cThaRegStsVtDemapConcatControl +  mMethodsGet(pdhModule)->OffsetBySwSts(pdhModule, channel, stsId));
        regVal = ChannelRead(pdhModule, channel, stsId, regAddress);

        if (sts_i == 0) /* Master */
            {
            mRegFieldSet(regVal, StsVtDemapCcatEn, 0);

            /* HW recommend, should write master of itself */
            mRegFieldSet(regVal, StsVTDemapCcatMaster, hwMasterSts);

            /* Write to hardware */
            ChannelWrite(pdhModule, channel, stsId, regAddress, regVal);
            continue;
            }

        mRegFieldSet(regVal, StsVtDemapCcatEn, 1);
        mRegFieldSet(regVal, StsVTDemapCcatMaster, hwMasterSts);

        /* Write to hardware */
        ChannelWrite(pdhModule, channel, stsId, regAddress, regVal);
        }

    return cAtOk;
    }

static eAtRet StsVtDemapConcatRelease(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId)
    {
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint32 regVal, regAddress;
    uint32 StsVtDemapCcatEnMask = mMethodsGet(pdhModule)->RegStsVtDemapCcatEnMask(pdhModule);
    uint8 StsVtDemapCcatEnShift = mMethodsGet(pdhModule)->RegStsVtDemapCcatEnShift(pdhModule);
    uint32 StsVTDemapCcatMasterMask = mMethodsGet(pdhModule)->RegStsVTDemapCcatMasterMask(pdhModule);
    uint8 StsVTDemapCcatMasterShift = mMethodsGet(pdhModule)->RegStsVTDemapCcatMasterShift(pdhModule);

    /* Read from hardware */
    regAddress = (uint32)(cThaRegStsVtDemapConcatControl +  mMethodsGet(pdhModule)->OffsetBySwSts(pdhModule, channel, stsId));
    regVal = ChannelRead(pdhModule, channel, stsId, regAddress);

    mRegFieldSet(regVal, StsVtDemapCcatEn, 0);
    mRegFieldSet(regVal, StsVTDemapCcatMaster, 0);

    /* Write to hardware */
    ChannelWrite(pdhModule, channel, stsId, regAddress, regVal);

    return cAtOk;
    }

static eBool IsVc1x(AtSdhChannel sdhVc)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhVc);

    if ((vcType == cAtSdhChannelTypeVc12) ||
        (vcType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet StsVtDemapVc4FractionalVc3Set(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3)
    {
    uint8 sts_i;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
        uint32 regVal, regAddress;
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhVc) + sts_i);
        uint8 slice, hwSts;

        ThaSdhChannelHwStsGet(sdhVc, cAtModulePdh, stsId, &slice, &hwSts);

        /* Read from hardware */
        regAddress = cThaRegStsVtDemapCepVc4FractionalVc3 +  ChannelPartOffset((ThaStmModulePdh)self, sdhVc);
        regVal = ChannelRead(pdhModule, sdhVc, stsId, regAddress);

        mFieldIns(&regVal,
                  cThaRegStsVtDemapCepVc4FractionalVc3Mask(hwSts),
                  cThaRegStsVtDemapCepVc4FractionalVc3Shift(hwSts),
                  (isVc4FractionalVc3) ? 1 : 0);

        /* Write to hardware */
        ChannelWrite(pdhModule, sdhVc, stsId, regAddress, regVal);
        }

    return cAtOk;
    }

static eAtRet Tug2MappingRestore(AtModulePdh pdhModule, AtSdhChannel tug2)
    {
    uint8 mapType = AtSdhChannelMapTypeGet(tug2);
    uint8 sts1Id = AtSdhChannelSts1Get(tug2);
    uint8 vtgId = AtSdhChannelTug2Get(tug2);
    eAtRet ret = cAtOk;
    uint8 subChannel_i, numSubChannels;

    if (mapType == cAtSdhTugMapTypeTug2Map3xTu12s)
        ret |= ThaPdhRxVtgTypeSet(pdhModule, tug2, sts1Id, vtgId, cThaPdhTu12Vt2);

    if (mapType == cAtSdhTugMapTypeTug2Map4xTu11s)
        ret |= ThaPdhRxVtgTypeSet(pdhModule, tug2, sts1Id, vtgId, cThaPdhTu11Vt15);

    numSubChannels = AtSdhChannelNumberOfSubChannelsGet(tug2);
    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        AtSdhChannel tu1x = AtSdhChannelSubChannelGet(tug2, subChannel_i);
        AtSdhChannel vc1x = NULL;

        if (tu1x)
            vc1x = AtSdhChannelSubChannelGet(tu1x, 0);

        if (vc1x)
            {
            mapType = AtSdhChannelMapTypeGet(vc1x);
            ret |= ThaPdhVtMapMdSet(pdhModule, vc1x, ThaSdhVc1xPdhVcMapMode(vc1x, mapType));
            if (AtChannelBoundPwGet((AtChannel)vc1x))
                ret |= ThaSdhVc1xPdhPwBindingCtrlSet(vc1x);
            }
        }

    return ret;
    }

static eAtRet HoVcUnChannelize(ThaModulePdh self, AtSdhChannel hoVc)
    {
    eAtRet ret = cAtOk;

    ret |= ThaStmModulePdhRxStsVtgTypeSet((AtModulePdh)self, hoVc, cThaPdhStsVc);
    ret |= ThaPdhVcBypassMapMdSet((AtSdhVc)hoVc);

    return ret;
    }

static uint8 VcDefaultJitterStuffing(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return (uint8)(IsVc1x(sdhVc) ? 8 : ThaModulePdhDefaultDe3JitterStuffingMode((ThaModulePdh)self));
    }

static eAtRet StsVtMapDefaultSet(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint8 sts_i;
    uint8 vtgId = AtSdhChannelTug2Get(sdhVc);
    uint8 vtId = AtSdhChannelTu1xGet(sdhVc);
    uint8 jitterStuffing = VcDefaultJitterStuffing(self, sdhVc);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        ThaStmModulePdh modulePdh = mThis(self);
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhVc) + sts_i);
        uint32 regAddr = cThaRegStsVtMapCtrl + mMethodsGet(modulePdh)->DefaultOffset(modulePdh, sdhVc, stsId, vtgId, vtId);
        uint32 regVal = ChannelRead(modulePdh, sdhVc, stsId, regAddr);

        mRegFieldSet(regVal, cThaStsVtMapThr, cDefaultStsVtMapThr);
        mRegFieldSet(regVal, cThaStsVtMapJitCfg, jitterStuffing);

        ChannelWrite(modulePdh, sdhVc, stsId, regAddr, regVal);
        }

    return cAtOk;
    }

static eAtRet Vc3ChannelizeRestore(AtModulePdh pdhModule, AtSdhChannel channel)
    {
    eAtRet ret = cAtOk;
    uint8 mapType;

    ret |= ThaStmModulePdhRxStsVtgTypeSet(pdhModule, channel, cThaPdhStsVc);

    mapType = AtSdhChannelMapTypeGet(channel);
    if (mapType == cAtSdhVcMapTypeVc3MapDe3)
        {
        ThaPdhDe3 de3 = (ThaPdhDe3)AtSdhChannelMapChannelGet(channel);
        uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de3);
        ThaCdrController cdr = ThaPdhDe3CdrControllerGet(de3);
        ret |= ThaSdhAuVcDs3Map((ThaSdhAuVc)channel);
        ret |= ThaPdhDe3HwFrameTypeSet(de3, frameType);
        if (AtChannelBoundPwGet((AtChannel)de3) && !AtPdhDe3FrameTypeIsUnframed(frameType))
            ret |= ThaPdhDe3FramingBypass(de3, cAtTrue);
        ret |= ThaCdrControllerChannelMapTypeSet(cdr);
        }

    if ((mapType == cAtSdhVcMapTypeVc3MapC3))
        {
        ret |= ThaPdhVcBypassMapMdSet((AtSdhVc)channel);
        ret |= ThaPdhStsMapAutoAisEnable(pdhModule, channel, cAtTrue);
        }

    if ((mapType != cAtSdhVcMapTypeVc3Map7xTug2s) && (mapType != cAtSdhVcMapTypeNone))
        ret |= StsVtMapDefaultSet(pdhModule, channel);

    return ret;
    }

static eAtRet SdhVcChannelizeRestore(AtModulePdh pdhModule, AtSdhChannel channel)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);
    uint8 subChannel_i;
    eAtRet ret = cAtOk;

    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        uint32 channelType;
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, subChannel_i);

        if (subChannel == NULL)
            continue;

        channelType = AtSdhChannelTypeGet(subChannel);

        switch (channelType)
            {
            case cAtSdhChannelTypeTug2:
                ret |= Tug2MappingRestore(pdhModule, subChannel);
                break;

            case cAtSdhChannelTypeVc3:
                ret |= Vc3ChannelizeRestore(pdhModule, subChannel);
                break;

            default:
                ret |= SdhVcChannelizeRestore(pdhModule, subChannel);
            }
        }

    if ((numSubChannels == 0) && (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeVc3))
        ret |= Vc3ChannelizeRestore(pdhModule, channel);

    return ret;
    }

static eAtRet HoVcChannelizeRestore(ThaModulePdh self, AtSdhChannel hoVc)
    {
    return SdhVcChannelizeRestore((AtModulePdh)self, hoVc);
    }

static eBool LiuDe3OverVc3IsSupported(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 RegisterBase(ThaModulePdh self, uint32 absoluteAddress)
    {
    AtUnused(self);
    return absoluteAddress - cThaModulePdhOriginalBaseAddress;
    }

static void MultiVtRegsDisplay(ThaModulePdh self, const char* regName, AtSdhChannel sdhChannel, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
        uint8 vtId = AtSdhChannelTu1xGet(sdhChannel);
        uint32 offset = mMethodsGet(mThis(self))->DefaultOffset(mThis(self), sdhChannel, (uint8)(startSts + sts_i), vtgId, vtId);
        uint32 address = regBaseAddress + offset;
        ThaDeviceChannelRegValueDisplay(channel, address, cAtModulePdh);
        }
    }

static void MultiStsRegsDisplay(ThaModulePdh self, const char* regName, AtSdhChannel sdhChannel, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        int32 offset = mMethodsGet(mThis(self))->OffsetBySwSts(mThis(self), sdhChannel, (uint8)(startSts + sts_i));
        uint32 address = (uint32)((int32)regBaseAddress + offset);
        ThaDeviceChannelRegValueDisplay(channel, address, cAtModulePdh);
        }
    }

static void SdhChannelRegsShow(ThaModulePdh self, AtSdhChannel sdhChannel)
    {
    MultiVtRegsDisplay(self, "STS/VT Demap Control", sdhChannel, cThaRegStsVtDmapCtrl);
    MultiVtRegsDisplay(self, "STS/VT Map Control", sdhChannel, cThaRegStsVtMapCtrl);

    if (mMethodsGet(self)->RegExist(self, RegisterBase(self, cThaRegStsVtDemapConcatControl)))
        MultiStsRegsDisplay(self, "STS/VT Demap Concatenation Control", sdhChannel, cThaRegStsVtDemapConcatControl);

    MultiStsRegsDisplay(self, "PDH Rx Per STS/VC Payload Control", sdhChannel, ThaModulePdhRxPerStsVCPldCtrlRegister(self));
    MultiStsRegsDisplay(self, "RxM23E23 Control", sdhChannel, cThaRegRxM23E23Ctrl);
    MultiStsRegsDisplay(self, "TxM23E23 Control", sdhChannel, cThaRegTxM23E23Ctrl);
    MultiStsRegsDisplay(self, "RxDS3E3 OH Pro Control", sdhChannel, ThaModulePdhRxDs3E3OHProCtrlRegister(self));
    }

static void PdhChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(pdhChannel);

    if (sdhChannel)
        SdhChannelRegsShow(self, sdhChannel);

    m_ThaModulePdhMethods->PdhChannelRegsShow(self, pdhChannel);
    }

static void De3RegsShow(ThaModulePdh self, AtPdhChannel de3)
    {
    AtSdhChannel vc3 = (AtSdhChannel)AtPdhChannelVcInternalGet(de3);
    ThaPdhDe3 thade3 = (ThaPdhDe3)de3;
    uint32 offset = 0;
    uint32 address = 0;

    if (vc3)
        return;

    offset  = mMethodsGet(thade3)->DemapDefaultOffset(thade3);
    address = cThaRegStsVtDmapCtrl + offset;
    ThaDeviceRegNameDisplay("STS/VT Demap Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    offset  = mMethodsGet(thade3)->DemapDefaultOffset(thade3);
    address = cThaRegStsVtMapCtrl + offset;
    ThaDeviceRegNameDisplay("STS/VT Map Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    if (mMethodsGet(self)->RegExist(self, RegisterBase(self, cThaRegStsVtDemapConcatControl)))
        {
        offset = mMethodsGet(thade3)->DefaultOffset(thade3);
        address = cThaRegStsVtDemapConcatControl + offset;
        ThaDeviceRegNameDisplay("STS/VT Demap Concatenation Control");
        ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);
        }

    offset = mMethodsGet(thade3)->DefaultOffset(thade3);
    address = cThaRegPdhRxPerStsVCPldCtrl + offset;
    ThaDeviceRegNameDisplay("PDH Rx Per STS/VC Payload Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    offset  = mMethodsGet(thade3)->DefaultOffset(thade3);
    address = cThaRegDS1E1J1RxFrmrMuxCtrl2 + offset;
    ThaDeviceRegNameDisplay("DS1/E1/J1 Rx Framer Mux Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    m_ThaModulePdhMethods->De3RegsShow(self, de3);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        }
    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    AtModulePdh pdhModule = (AtModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_AtModulePdhOverride));
        mMethodOverride(m_AtModulePdhOverride, NumberOfDe1sGet);
        }

    mMethodsSet(pdhModule, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdhMethods = mMethodsGet(pdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, HasM13);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnCurrentAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnInterruptAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, HoVcUnChannelize);
        mMethodOverride(m_ThaModulePdhOverride, HoVcChannelizeRestore);
        mMethodOverride(m_ThaModulePdhOverride, SdhChannelRegsShow);
        mMethodOverride(m_ThaModulePdhOverride, PdhChannelRegsShow);
        mMethodOverride(m_ThaModulePdhOverride, De3RegsShow);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    }

static void MethodsInit(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultOffset);
        mMethodOverride(m_methods, CoreOfSts);
        mMethodOverride(m_methods, OffsetByHwSts);
        mMethodOverride(m_methods, OffsetBySwSts);
        mMethodOverride(m_methods, StsVtDemapConcatSet);
        mMethodOverride(m_methods, StsVtDemapConcatRelease);
        mMethodOverride(m_methods, StsVtDemapControlOffset);
        mMethodOverride(m_methods, PdhRxPerStsVcPayloadControlOffset);
        mMethodOverride(m_methods, StsIsSlaveInChannel);
        mMethodOverride(m_methods, Ds1E1FrmRxMuxSet);
        mMethodOverride(m_methods, StsVtDemapVc4FractionalVc3Set);
        mMethodOverride(m_methods, LiuDe3OverVc3IsSupported);

        /* Bit fields */
        mBitFieldOverride(ThaStmModulePdh, m_methods, RegStsVtDemapCcatEn)
        mBitFieldOverride(ThaStmModulePdh, m_methods, RegStsVTDemapCcatMaster)
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmModulePdh);
    }

/* Constructor */
AtModulePdh ThaStmModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Initializing additional methods */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtModulePdh ThaStmModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ThaStmModulePdhObjectInit(newModule, device);
    }

eAtRet ThaPdhStsMapMdSet(AtModulePdh self, AtSdhChannel sdhVc, eThaStsMapMd mapMd)
    {
    eAtRet ret;
    eThaStsVtMapCtrl stsMapMd;
    eThaPdhStsVtDmapCtrl stsDeMapMd;
    uint32 regVal, dRegAddress;
    eBool isByPass;
    uint32 tempValue;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint8 isTu3Mode;
    uint32 offset;
    uint8 stsId = AtSdhChannelSts1Get(sdhVc);

    if (pdhModule == NULL)
        return cAtErrorNullPointer;

    /* Map */
    offset = mMethodsGet(pdhModule)->DefaultOffset(pdhModule, sdhVc, stsId, cThaPdhNotCare, cThaPdhNotCare);
    dRegAddress = cThaRegStsVtMapCtrl + offset;
    regVal = ChannelRead(pdhModule, sdhVc, stsId, dRegAddress);

    stsMapMd   = cThaVc3Ds3MapMd;
    stsDeMapMd = cThaStsVtVc3Ds3DmapMd;
    isByPass = cAtFalse;
    switch (mapMd)
        {
        case cThaStsMapDs3Vc3Md:
            stsDeMapMd = cThaStsVtVc3Ds3DmapMd;
            stsMapMd  = cThaVc3Ds3MapMd;
            isTu3Mode = 0;
            break;
        case cThaStsMapE3Vc3Md:
            stsDeMapMd = cThaStsVtVc3E3DmapMd;
            stsMapMd   = cThaVc3E3MapMd;
            isTu3Mode  = 0;
            break;
        case cThaStsMapDs3Tu3Md:
            stsDeMapMd = cThaStsVtVc3Ds3DmapMd; /* Should be same as VC3 DS3 */
            stsMapMd  = cThaTu3Ds3MapMd;
            isByPass  = cAtTrue;
            isTu3Mode = 1;
            break;
        case cThaStsMapE3Tu3Md:
            stsDeMapMd = cThaStsVtVc3E3DmapMd;
            stsMapMd   = cThaTu3E3MapMd;
            isByPass   = cAtTrue;
            isTu3Mode  = 1;
            break;
        case cThaStsMapUnknown:
        default:
            return cAtErrorModeNotSupport;
        }

    /* StsVtMapBypass: cBit1, store in 1 uint32 */
    mFieldIns(&regVal,
              cThaStsVtMapBypassMask,
              cThaStsVtMapBypassShift,
              mBoolToBin(isByPass));

    /* StsVtMapMd[2:0]: cBit0, store in 1 uint32 */
    mFieldIns(&regVal,
              cThaStsVtMapMdMask,
              cThaStsVtMapMdShift,
              stsMapMd);

    mFieldIns(&regVal,
              cThaStsVtMapThrMask,
              cThaStsVtMapThrShift,
              cDefaultStsVtMapThr);

    mFieldIns(&regVal,
              cThaStsVtMapJitIDCfgMask,
              cThaStsVtMapJitIDCfgShift,
              0);

    mFieldIns(&regVal,
              cThaStsVtMapJitCfgMask,
              cThaStsVtMapJitCfgShift,
              ThaModulePdhDefaultDe3JitterStuffingMode((ThaModulePdh)pdhModule));

    mFieldIns(&regVal,
              cThaStsVtMapCenterFifoMask,
              cThaStsVtMapCenterFifoShift,
              0);

    mFieldIns(&regVal,
              cThaStsVtMapAismaskMask,
              cThaStsVtMapAismaskShift,
              1);

    /* Write to hardware */
    ChannelWrite(pdhModule, sdhVc, stsId, dRegAddress, regVal);

    /* DeMap */
    regVal = 0;

    /* Read from hardware */
    dRegAddress = cThaRegStsVtDmapCtrl + offset;
    regVal = ChannelRead(pdhModule, sdhVc, stsId, dRegAddress);

    mFieldIns(&regVal, cThaStsVtDemapCepMdMask, cThaStsVtDemapCepMdShift, isTu3Mode);

    tempValue = ((stsDeMapMd & cThaStsVtDemapFrmtypeMask) >> cThaStsVtDemapFrmtypeShift);
    mFieldIns(&regVal, cThaStsVtDemapFrmtypeMask, cThaStsVtDemapFrmtypeShift, tempValue);

    tempValue = stsDeMapMd & cThaStsVtDemapSigtypeMask;
    mFieldIns(&regVal, cThaStsVtDemapSigtypeMask, cThaStsVtDemapSigtypeShift, tempValue);
    mFieldIns(&regVal, cThaStsVtDmapAutoAisMask, cThaStsVtDmapAutoAisShift, 1);

    /* StsVtDemapFrcAis: cBit4, store in 1 uint32 */
    mFieldIns(&regVal, cThaStsVtDmapFrcAisMask, cThaStsVtDmapFrcAisShift, 0);

    /* Write to hardware */
    ChannelWrite(pdhModule, sdhVc, stsId, dRegAddress, regVal);

    ret = mMethodsGet(mThis(self))->Ds1E1FrmRxMuxSet(mThis(self), sdhVc);
    if (ret != cAtOk)
        return ret;

    ret |= RxStsVtgTypeSet(self, sdhVc, cThaPdhStsVc);
    return ret;
    }

eAtRet ThaPdhDe3MapMdSet(AtModulePdh self, AtPdhChannel channel)
    {
    eAtRet ret = 0;
    eThaStsVtMapCtrl stsMapMd;
    eThaPdhStsVtDmapCtrl stsDeMapMd;
    uint32 regVal, dRegAddress;
    eBool isByPass;
    uint32 tempValue;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint8 isTu3Mode;
    uint32 offset;

    if (pdhModule == NULL)
        return cAtErrorNullPointer;

    /* Map */
    offset = mMethodsGet(pdhModule)->StsVtDemapControlOffset(pdhModule, channel);
    dRegAddress = cThaRegStsVtMapCtrl + offset;
    regVal = mChannelHwRead((AtChannel)channel, dRegAddress, cAtModulePdh);

    isByPass = cAtFalse;
    isTu3Mode = 0;
    if (AtPdhDe3IsE3((AtPdhDe3)channel))
        {
        stsDeMapMd = cThaStsVtVc3E3DmapMd;
        stsMapMd   = cThaVc3E3MapMd;
        }
    else
        {
        stsDeMapMd = cThaStsVtVc3Ds3DmapMd;
        stsMapMd  = cThaVc3Ds3MapMd;
        }

    /* StsVtMapBypass: cBit1, store in 1 uint32 */
    mFieldIns(&regVal, cThaStsVtMapBypassMask, cThaStsVtMapBypassShift, mBoolToBin(isByPass));

    /* StsVtMapMd[2:0]: cBit0, store in 1 uint32 */
    mFieldIns(&regVal, cThaStsVtMapMdMask, cThaStsVtMapMdShift, stsMapMd);

    mFieldIns(&regVal, cThaStsVtMapThrMask, cThaStsVtMapThrShift, cDefaultStsVtMapThr);

    mFieldIns(&regVal, cThaStsVtMapJitIDCfgMask, cThaStsVtMapJitIDCfgShift, 0);

    mFieldIns(&regVal, cThaStsVtMapJitCfgMask, cThaStsVtMapJitCfgShift, 1);

    mFieldIns(&regVal, cThaStsVtMapCenterFifoMask, cThaStsVtMapCenterFifoShift, 0);

    mFieldIns(&regVal, cThaStsVtMapAismaskMask, cThaStsVtMapAismaskShift, 1);

    /* Write to hardware */
    mChannelHwWrite((AtChannel)channel, dRegAddress, regVal, cAtModulePdh);

    /* DeMap */
    regVal = 0;

    /* Read from hardware */
    dRegAddress = cThaRegStsVtDmapCtrl + offset;
    regVal = mChannelHwRead((AtChannel)channel, dRegAddress, cAtModulePdh);

    mFieldIns(&regVal, cThaStsVtDemapCepMdMask, cThaStsVtDemapCepMdShift, isTu3Mode);

    tempValue = ((stsDeMapMd & cThaStsVtDemapFrmtypeMask) >> cThaStsVtDemapFrmtypeShift);
    mFieldIns(&regVal, cThaStsVtDemapFrmtypeMask, cThaStsVtDemapFrmtypeShift, tempValue);

    tempValue = stsDeMapMd & cThaStsVtDemapSigtypeMask;
    mFieldIns(&regVal, cThaStsVtDemapSigtypeMask, cThaStsVtDemapSigtypeShift, tempValue);
    mFieldIns(&regVal, cThaStsVtDmapAutoAisMask, cThaStsVtDmapAutoAisShift, 1);

    /* StsVtDemapFrcAis: cBit4, store in 1 uint32 */
    mFieldIns(&regVal, cThaStsVtDmapFrcAisMask, cThaStsVtDmapFrcAisShift, 0);

    /* Write to hardware */
    mChannelHwWrite((AtChannel)channel, dRegAddress, regVal, cAtModulePdh);

    ret = RxDe3VtgTypeSet(self, channel, cThaPdhStsVc);

    return ret;
    }

eThaStsMapMd ThaPdhStsMapMdGet(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint32 regVal, dRegAddress, offset;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint8 stsId = AtSdhChannelSts1Get(sdhVc);
    uint32 stsMapMode;
    uint8 isTu3;

    offset = mMethodsGet(pdhModule)->DefaultOffset(pdhModule, sdhVc, stsId, cThaPdhNotCare, cThaPdhNotCare);
    dRegAddress = cThaRegStsVtMapCtrl + offset;
    regVal = ChannelRead(pdhModule, sdhVc, stsId, dRegAddress);

    stsMapMode = mRegField(regVal, cThaStsVtMapMd);
    isTu3 = (uint8)mRegField(regVal, cThaStsVtMapBypass);
    if(stsMapMode == cThaVc3Ds3MapMd)
        return (isTu3) ? cThaStsMapDs3Tu3Md : cThaStsMapDs3Vc3Md;

    if(stsMapMode == cThaVc3E3MapMd)
        return (isTu3) ? cThaStsMapE3Tu3Md : cThaStsMapE3Vc3Md;

    return cThaStsMapUnknown;
    }

eAtRet ThaPdhSts1M13Bypass(AtModulePdh self, AtSdhChannel sdhVc, uint8 stsId)
    {
    uint32 regValue, regAddress;
    int32 offset;
    ThaStmModulePdh pdhModule = mThis(self);
    uint8 partId = ThaModuleSdhPartOfChannel((AtSdhChannel)sdhVc);

    if (!ThaModulePdhHasM13((ThaModulePdh)self, partId))
        return cAtOk;

    offset     = mMethodsGet(pdhModule)->OffsetBySwSts(pdhModule, sdhVc, stsId);
    regAddress = (uint32)(cThaRegRxM23E23Ctrl + offset);
    regValue   = ChannelRead(pdhModule, sdhVc, stsId, regAddress);
    mFieldIns(&regValue, cThaRxDS3E3MdMask, cThaRxDS3E3MdShift, 0x8);
    mFieldIns(&regValue, cThaRxDS3E3LineLoopMask, cThaRxDS3E3LineLoopShift, 0x0);
    ChannelWrite(pdhModule, sdhVc, stsId, regAddress, regValue);

    regAddress = (uint32)(cThaRegTxM23E23Ctrl + offset);
    regValue   = ChannelRead(pdhModule, sdhVc, stsId, regAddress);
    mFieldIns(&regValue, cThaTxDS3E3MdMask, cThaTxDS3E3MdShift, 0x8);
    mFieldIns(&regValue, cThaTxDE3RmtPayLoopMask, cThaTxDE3RmtPayLoopShift, 0x0);
    mFieldIns(&regValue, cThaTxDE3RmtLineLoopMask, cThaTxDE3RmtLineLoopShift, 0x0);
    ChannelWrite(pdhModule, sdhVc, stsId, regAddress, regValue);

    return cAtOk;
    }

eAtRet ThaPdhRxVtgTypeSet(AtModulePdh self, AtSdhChannel tug2, uint8 stsId, uint8 vtgId, eThaPdhStsPayloadType vtgType)
    {
    uint32 regValue, regAddress;
    ThaStmModulePdh modulePdh = mThis(self);

    regAddress = (uint32)((int32)ThaModulePdhRxPerStsVCPldCtrlRegister((ThaModulePdh)self) + mMethodsGet(modulePdh)->OffsetBySwSts(modulePdh, tug2, stsId));
    regValue = ChannelRead(modulePdh, tug2, stsId, regAddress);
    mFieldIns(&regValue, cThaPdhVtRxMapTug2TypeMask2(vtgId), cThaPdhVtRxMapTug2TypeShift2(vtgId), vtgType);
    ChannelWrite(modulePdh, tug2, stsId, regAddress, regValue);

    return cAtOk;
    }

eAtRet ThaPdhDe1RxVtgTypeSet(AtModulePdh self, AtPdhChannel de2, eThaPdhStsPayloadType vtgType)
    {
    uint32 regValue, regAddress;
    ThaStmModulePdh modulePdh = mThis(self);
    uint8 vtgId = (uint8)AtChannelIdGet((AtChannel)de2);
    regAddress = ThaModulePdhRxPerStsVCPldCtrlRegister((ThaModulePdh)self) + mMethodsGet(modulePdh)->PdhRxPerStsVcPayloadControlOffset(modulePdh, de2);
    regValue = mChannelHwRead((AtChannel)de2, regAddress, cAtModulePdh);
    mFieldIns(&regValue, cThaPdhVtRxMapTug2TypeMask2(vtgId), cThaPdhVtRxMapTug2TypeShift2(vtgId), vtgType);
    mChannelHwWrite((AtChannel)de2, regAddress, regValue, cAtModulePdh);

    return cAtOk;
    }

eAtRet ThaPdhVtMapMdSet(AtModulePdh self, AtSdhChannel vc1x, eThaPdhVtMapMd mode)
    {
    uint32 regAddr, regVal;
    eThaStsVtMapCtrl stsMapMd;
    eThaPdhStsVtDmapCtrl stsDeMapMd;
    uint32 tempValue;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint32 offset;
    uint8 vtgId    = AtSdhChannelTug2Get(vc1x);
    uint8 vtId     = AtSdhChannelTu1xGet(vc1x);
    uint8 stsId    = AtSdhChannelSts1Get(vc1x);

    if (pdhModule == NULL)
        return cAtErrorNullPointer;

    /* Need to bypass M13/E13 */
    if ((mode == cThaVt15Ds1MapMode) || (mode == cThaVt2E1MapMode))
        ThaPdhSts1M13Bypass(self, vc1x, AtSdhChannelSts1Get(vc1x));

    offset     = mMethodsGet(pdhModule)->DefaultOffset(pdhModule, vc1x, stsId, vtgId, vtId);
    regAddr    = cThaRegStsVtMapCtrl + offset;
    regVal     = ChannelRead(pdhModule, vc1x, stsId, regAddr);
    stsMapMd   = cThaVt15Ds1MapMd;
    stsDeMapMd = cThaStsVtVt15Ds1DmapMd;

    switch (mode)
        {
        case cThaVt15Ds1MapMode:
            stsMapMd   = cThaVt15Ds1MapMd;
            stsDeMapMd = cThaStsVtVt15Ds1DmapMd;
            break;
        case cThaVt2E1MapMode:
            stsDeMapMd = cThaStsVtVt2E1DmapMd;
            stsMapMd   = cThaVt2E1MapMd;
            break;
        case cThaVt15C11MapMode:
            stsDeMapMd = cThaStsVtVt15BCepDmapMd;
            stsMapMd   = cThaVt15Ds1MapMd;
            break;
        case cThaVt2C12MapMode:
            stsDeMapMd = cThaStsVtVt2BCepDmapMd;
            stsMapMd   = cThaVt2E1MapMd;
            break;
        default:
            return cAtErrorModeNotSupport;
        }

    mFieldIns(&regVal, cThaStsVtMapBypassMask, cThaStsVtMapBypassShift, 0);
    mFieldIns(&regVal, cThaStsVtMapMdMask, cThaStsVtMapMdShift, stsMapMd);
    mFieldIns(&regVal, cThaStsVtMapThrMask, cThaStsVtMapThrShift, cDefaultStsVtMapThr);
    mFieldIns(&regVal, cThaStsVtMapJitIDCfgMask, cThaStsVtMapJitIDCfgShift, 0);
    mFieldIns(&regVal, cThaStsVtMapJitCfgMask, cThaStsVtMapJitCfgShift, 8); /* Default jitter configuration for system mode */
    mFieldIns(&regVal, cThaStsVtMapCenterFifoMask, cThaStsVtMapCenterFifoShift, 0);
    mFieldIns(&regVal, cThaStsVtMapAismaskMask, cThaStsVtMapAismaskShift, 1);

    /* Apply */
    ChannelWrite(pdhModule, vc1x, stsId, regAddr, regVal);

    /* DeMap */
    regAddr = cThaRegStsVtDmapCtrl + offset;
    regVal = ChannelRead(pdhModule, vc1x, stsId, regAddr);

    tempValue = ((stsDeMapMd & cThaStsVtDemapCepMdMask) >> cThaStsVtDemapCepMdShift);
    mFieldIns(&regVal, cThaStsVtDemapCepMdMask, cThaStsVtDemapCepMdShift, tempValue);

    tempValue = ((stsDeMapMd & cThaStsVtDemapFrmtypeMask) >> cThaStsVtDemapFrmtypeShift);
    mFieldIns(&regVal, cThaStsVtDemapFrmtypeMask, cThaStsVtDemapFrmtypeShift, tempValue);

    tempValue = stsDeMapMd & cThaStsVtDemapSigtypeMask;
    mFieldIns(&regVal, cThaStsVtDemapSigtypeMask, cThaStsVtDemapSigtypeShift, tempValue);

    mFieldIns(&regVal, cThaStsVtDmapAutoAisMask, cThaStsVtDmapAutoAisShift, 1);
    mFieldIns(&regVal, cThaStsVtDmapFrcAisMask, cThaStsVtDmapFrcAisShift, 0);

    /* Apply */
    ChannelWrite(pdhModule, vc1x, stsId, regAddr, regVal);

    return mMethodsGet(pdhModule)->Ds1E1FrmRxMuxSet(pdhModule, vc1x);
    }

eAtRet ThaPdhStsDeMapCtrlSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsVtDmapCtrl demapMd)
    {
    eAtRet ret = cAtOk;

    ret |= ThaPdhStsVtDeMapCtrlSet(self, sdhVc, demapMd);
    ret |= RxStsVtgTypeSet(self, sdhVc, cThaPdhStsVc);

    return ret;
    }

eAtRet ThaPdhVcBypassMapMdSet(AtSdhVc sdhVc)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhVc), cAtModulePdh);
    ThaStmModulePdh stmModulePdh = (ThaStmModulePdh)pdhModule;
    AtSdhChannel sdhChannel = (AtSdhChannel)sdhVc;
    uint8 vtgId = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId  = AtSdhChannelTu1xGet(sdhChannel);
    uint32 regAddr, regVal, offset;
    eAtRet ret = cAtOk;
    uint8 sts_i;

    if (stmModulePdh == NULL)
        return cAtErrorNullPointer;

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
        {
        uint8 sts1Id = (uint8)(AtSdhChannelSts1Get(sdhChannel) + sts_i);
        uint8 slice, hwStsId;
        ThaSdhChannelHwStsGet(sdhChannel, cAtModulePdh, sts1Id, &slice, &hwStsId);
        offset  = mMethodsGet(stmModulePdh)->DefaultOffset(stmModulePdh, sdhChannel, sts1Id, vtgId, vtId);

        /* Map */
        regVal = 0;
        regAddr = cThaRegStsVtMapCtrl + offset;
        mFieldIns(&regVal, cThaStsVtMapMdMask, cThaStsVtMapMdShift, MapType(sdhVc));
        ChannelWrite(stmModulePdh, sdhChannel, sts1Id, regAddr, regVal);

        /* DeMap */
        regVal = 0;
        regAddr = cThaRegStsVtDmapCtrl + offset;
        mFieldIns(&regVal, cThaStsVtDemapSigtypeMask, cThaStsVtDemapSigtypeShift, DemapType(sdhVc));
        ChannelWrite(stmModulePdh, sdhChannel, sts1Id, regAddr, regVal);

        ret |= mMethodsGet(stmModulePdh)->Ds1E1FrmRxMuxSet(stmModulePdh, sdhChannel);
        ret |= ThaPdhSts1M13Bypass(pdhModule, sdhChannel, sts1Id);
        }

    /* Set VTG type */
    ret |= VtgTypeSet(sdhVc, VtgType(sdhChannel));

    return ret;
    }

eAtRet ThaPdhStsVtMapCtrlSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsVtMapCtrl mapMd)
    {
    uint8 sts_i;
    uint8 vtgId = AtSdhChannelTug2Get(sdhVc);
    ThaStmModulePdh modulePdh = mThis(self);
    uint32 stsVtMapBypass = ((uint32)mapMd) >> cThaStsVtMapBypassShift;
    uint8 vtId = AtSdhChannelTu1xGet(sdhVc);
    uint8 jitterStuffing = VcDefaultJitterStuffing(self, sdhVc);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhVc) + sts_i);
        uint32 regVal, dRegAddress;

        dRegAddress = cThaRegStsVtMapCtrl + mMethodsGet(pdhModule)->DefaultOffset(pdhModule, sdhVc, stsId, vtgId, vtId);
        regVal = ChannelRead(modulePdh, sdhVc, stsId, dRegAddress);

        mFieldIns(&regVal,
                  cThaStsVtMapBypassMask,
                  cThaStsVtMapBypassShift,
                  stsVtMapBypass);
        mFieldIns(&regVal,
                  cThaStsVtMapMdMask,
                  cThaStsVtMapMdShift,
                  mapMd);

        mFieldIns(&regVal,
                  cThaStsVtMapAismaskMask,
                  cThaStsVtMapAismaskShift,
                  0);

        mFieldIns(&regVal,
                  cThaStsVtMapThrMask,
                  cThaStsVtMapThrShift,
                  cDefaultStsVtMapThr);

        mFieldIns(&regVal,
                  cThaStsVtMapJitIDCfgMask,
                  cThaStsVtMapJitIDCfgShift,
                  0);

        mFieldIns(&regVal,
                  cThaStsVtMapJitCfgMask,
                  cThaStsVtMapJitCfgShift,
                  jitterStuffing);

        mFieldIns(&regVal,
                  cThaStsVtMapCenterFifoMask,
                  cThaStsVtMapCenterFifoShift,
                  0);

        /* Write to hardware */
        ChannelWrite(modulePdh, sdhVc, stsId, dRegAddress, regVal);
        }

    return cAtOk;
    }

eAtRet ThaPdhStsVtDeMapCtrlSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsVtDmapCtrl demapMd)
    {
    uint8 vtgId = AtSdhChannelTug2Get(sdhVc);
    uint8 sts_i, vtId;

    vtId = AtSdhChannelTu1xGet(sdhVc);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhVc); sts_i++)
        {
        ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
        uint32 regVal, regAddress;
        uint8 frmType;
        uint8 stsId = (uint8)(AtSdhChannelSts1Get(sdhVc) + sts_i);

        /* Read from hardware */
        regAddress = cThaRegStsVtDmapCtrl + mMethodsGet(pdhModule)->DefaultOffset(pdhModule, sdhVc, stsId, vtgId, vtId);
        regVal = ChannelRead(pdhModule, sdhVc, stsId, regAddress);

        mFieldIns(&regVal,
                  cThaStsVtDemapCepMdMask,
                  cThaStsVtDemapCepMdShift,
                  CepMode(demapMd));

        frmType = (uint8)(((uint32)demapMd & cThaStsVtDemapFrmtypeMask) >> cThaStsVtDemapFrmtypeShift);
        mFieldIns(&regVal,
                  cThaStsVtDemapFrmtypeMask,
                  cThaStsVtDemapFrmtypeShift,
                  frmType);

        mFieldIns(&regVal,
                  cThaStsVtDemapSigtypeMask,
                  cThaStsVtDemapSigtypeShift,
                  demapMd);

        mFieldIns(&regVal,
                  cThaStsVtDmapAutoAisMask,
                  cThaStsVtDmapAutoAisShift,
                  1);

        /* StsVtDemapFrcAis: cBit4, store in 1 uint32 */
        mFieldIns(&regVal,
                  cThaStsVtDmapFrcAisMask,
                  cThaStsVtDmapFrcAisShift,
                  0);

        /* Write to hardware */
        ChannelWrite(pdhModule, sdhVc, stsId, regAddress, regVal);
        }

    return cAtOk;
    }

eAtRet ThaPdhStsVtDeMapVc4FractionalVc3Set(AtModulePdh self, AtSdhChannel sdhVc, eBool isVc4FractionalVc3)
    {
    if (self)
        return mMethodsGet(mThis(self))->StsVtDemapVc4FractionalVc3Set(mThis(self), sdhVc, isVc4FractionalVc3);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhStsVtDemapConcatSet(AtModulePdh self, AtSdhChannel channel, uint8 startSts, uint8 numSts)
    {
    if (self)
        return mMethodsGet(mThis(self))->StsVtDemapConcatSet(mThis(self), channel, startSts, numSts);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhStsVtDemapConcatRelease(AtModulePdh self, AtSdhChannel channel, uint8 stsId)
    {
    if (self)
        return mMethodsGet(mThis(self))->StsVtDemapConcatRelease(mThis(self), channel, stsId);
    return cAtErrorNullPointer;
    }

eAtRet ThaStmModulePdhStsVtMapJitterSet(AtSdhChannel sdhChannel, uint32 jitter)
    {
    ThaStmModulePdh modulePdh = StmModulePdh(sdhChannel);
    uint8 stsId    = AtSdhChannelSts1Get(sdhChannel);
    uint8 vtgId    = AtSdhChannelTug2Get(sdhChannel);
    uint8 vtId     = AtSdhChannelTu1xGet(sdhChannel);
    uint32 regAddr, regVal;

    if (modulePdh == NULL)
        return cAtErrorNullPointer;

    regAddr = cThaRegStsVtMapCtrl + mMethodsGet(modulePdh)->DefaultOffset(modulePdh, sdhChannel, stsId, vtgId, vtId);
    regVal  = mChannelHwRead(sdhChannel, regAddr, cAtModulePdh);

    mRegFieldSet(regVal, cThaStsVtMapJitCfg, jitter);
    mChannelHwWrite(sdhChannel, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

eBool ThaModulePdhSdhChannelIsConcatenated(AtSdhChannel sdhChannel)
    {
    uint8 sts_i;
    uint8 numStsInChannel = AtSdhChannelNumSts(sdhChannel);
    AtDevice device = AtChannelDeviceGet((AtChannel)sdhChannel);
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    uint8 masterSts;

    if (pdhModule == NULL)
        return cAtFalse;

    masterSts = AtSdhChannelSts1Get(sdhChannel);
    for (sts_i = 1; sts_i < numStsInChannel; sts_i++)
        {
        if (!mMethodsGet(pdhModule)->StsIsSlaveInChannel(pdhModule, sdhChannel, (uint8)(sts_i + masterSts)))
            return cAtFalse;
        }

    return cAtTrue;
    }

eAtRet ThaStmModulePdhRxDe3VtgTypeSet(AtModulePdh self, AtPdhChannel channel, eThaPdhStsPayloadType vtgType)
    {
    return RxDe3VtgTypeSet(self, channel, vtgType);
    }

eAtRet ThaStmModulePdhDs1E1FrmRxMuxSet(ThaStmModulePdh self, AtSdhChannel sdhVc)
    {
    if (self)
        return Ds1E1FrmRxMuxSet(self, sdhVc);

    return cAtErrorNullPointer;
    }

eAtRet ThaStmModulePdhRxStsVtgTypeSet(AtModulePdh self, AtSdhChannel sdhVc, eThaPdhStsPayloadType vtgType)
    {
    if (self)
        return RxStsVtgTypeSet(self, sdhVc, vtgType);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhStsMapAutoAisEnable(AtModulePdh self, AtSdhChannel sdhVc, eBool enabled)
    {
    uint32 regVal, regAddr;
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;
    uint32 offset;
    uint8 stsId = AtSdhChannelSts1Get(sdhVc);

    offset = mMethodsGet(pdhModule)->DefaultOffset(pdhModule, sdhVc, stsId, cThaPdhNotCare, cThaPdhNotCare);
    regAddr = cThaRegStsVtDmapCtrl + offset;
    regVal = ChannelRead(pdhModule, sdhVc, stsId, regAddr);
    mFieldIns(&regVal, cThaStsVtDmapAutoAisMask, cThaStsVtDmapAutoAisShift, enabled ? 1 : 0);
    ChannelWrite(pdhModule, sdhVc, stsId, regAddr, regVal);

    return cAtOk;
    }

eBool ThaStmModulePdhLiuDe3OverVc3IsSupported(ThaStmModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->LiuDe3OverVc3IsSupported(self);

    return cAtFalse;
    }

void ThaPdhStsMapDemapInit(ThaModulePdh self)
    {
    static const uint32 cFromPdhInterface = 0xFFFFFFF;
    uint8 numSlices, numStsInSlice;
    uint8 sliceId, stsId;

    if (!ThaStmModulePdhLiuDe3OverVc3IsSupported((ThaStmModulePdh)self))
        return;

    numSlices = mMethodsGet(self)->NumSlices(self);
    numStsInSlice = mMethodsGet(self)->NumStsInSlice(self);
    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        int32 sliceOffset = ThaModulePdhSliceOffset(self, sliceId);

        for (stsId = 0; stsId < numStsInSlice; stsId++)
            {
            mModuleHwWrite(self, (uint32)(cThaRegDS1E1J1RxFrmrMuxCtrl2 + stsId + sliceOffset), cFromPdhInterface);
            mModuleHwWrite(self, (uint32)(cThaRegStsVtMapCtrl + stsId * 32 + sliceOffset), 0x0);
            mModuleHwWrite(self, (uint32)(cThaRegStsVtDmapCtrl + stsId * 32 + sliceOffset), 0x0);
            }
        }
    }

eAtRet ThaPdhStsDe3LiuMapVc3Enable(ThaStmModulePdh self, AtSdhChannel sdhVc, eBool enable)
    {
    uint32 regVal, address;
    uint32 offset;

    if ((self == NULL) || (sdhVc == NULL))
        return cAtErrorNullPointer;

    if (!ThaStmModulePdhLiuDe3OverVc3IsSupported(self))
        return cAtErrorModeNotSupport;

    offset = mMethodsGet(self)->DefaultOffset(self, sdhVc, AtSdhChannelSts1Get(sdhVc), cThaPdhNotCare, cThaPdhNotCare);
    address = cThaRegStsVtMapCtrl + offset;
    regVal = mModuleHwRead(self, address);
    mRegFieldSet(regVal, cThaStsVtMapPdhOverSdhMd, enable ? 1 : 0);
    mModuleHwWrite(self, address, regVal);

    address = cThaRegStsVtDmapCtrl + offset;
    regVal = mModuleHwRead(self, address);
    mRegFieldSet(regVal, cThaStsVtDmapPdhOverSdhMd, enable ? 1 : 0);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }

eBool ThaPdhStsDe3LiuMapVc3IsEnabled(ThaStmModulePdh self, AtSdhChannel sdhVc)
    {
    uint32 regVal, address;
    uint32 offset;

    if ((self == NULL) || (sdhVc == NULL))
        return cAtFalse;

    if (!ThaStmModulePdhLiuDe3OverVc3IsSupported(self))
        return cAtFalse;

    offset = mMethodsGet(self)->DefaultOffset(self, sdhVc, AtSdhChannelSts1Get(sdhVc), cThaPdhNotCare, cThaPdhNotCare);
    address = cThaRegStsVtMapCtrl + offset;
    regVal = mModuleHwRead(self, address);
    return mRegField(regVal, cThaStsVtMapPdhOverSdhMd) ? cAtTrue : cAtFalse;
    }

eAtRet ThaPdhStsVtDe1LiuMapVc1xEnable(ThaStmModulePdh self, AtSdhChannel vc1x, eBool enable)
    {
    uint32 regVal, address;
    uint32 offset;
    uint8 stsId, vtgId, vtId;

    if ((self == NULL) || (vc1x == NULL))
        return cAtErrorNullPointer;

    if (!ThaStmModulePdhLiuDe3OverVc3IsSupported(self))
        return cAtErrorModeNotSupport;

    stsId = AtSdhChannelSts1Get(vc1x);
    vtgId = AtSdhChannelTug2Get(vc1x);
    vtId  = AtSdhChannelTu1xGet(vc1x);

    offset = mMethodsGet(self)->DefaultOffset(self, vc1x, stsId, vtgId, vtId);
    address = cThaRegStsVtMapCtrl + offset;
    regVal = mModuleHwRead(self, address);
    mRegFieldSet(regVal, cThaStsVtMapPdhOverSdhMd, enable ? 1 : 0);
    mModuleHwWrite(self, address, regVal);

    address = cThaRegStsVtDmapCtrl + offset;
    regVal = mModuleHwRead(self, address);
    mRegFieldSet(regVal, cThaStsVtDmapPdhOverSdhMd, enable ? 1 : 0);
    mModuleHwWrite(self, address, regVal);

    return cAtOk;
    }


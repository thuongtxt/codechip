/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ThaPdhErrorGenerator Module Name
 *
 * File        : ThaPdhErrorGenerator.c
 *
 * Created Date: Mar 29, 2016
 *
 * Description : Implementation of the ThaPdhErrorGenerator class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhErrorGeneratorInternal.h"
#include "../ThaModulePdh.h"
#include "../../../../generic/man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6RegField_errins_thr_type_Mask               cBit30_29
#define cAf6RegField_errins_thr_type_Shift               29
#define cAf6RegField_errins_thr_mode_Mask               cBit28
#define cAf6RegField_errins_thr_mode_Shift              28
#define cAf6RegField_errins_thr_error_num_Mask          cBit27_0
#define cAf6RegField_errins_thr_error_num_Shift         0

#define cAf6RegField_errins_en_cfg_InsID_Mask           cBit9_0
#define cAf6RegField_errins_en_cfg_InsID_Shift          0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhErrorGenerator)(self))
/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhErrorGeneratorMethods m_methods;
static tAtErrorGeneratorMethods m_AtErrorGeneratorOverride;

static const tAtErrorGeneratorMethods *m_AtErrorGeneratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhErrorGenerator);
    }

static uint32 RegAddress_errins_en_cfg(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 RegField_errins_en_cfg_channelId_Mask(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegField_errins_en_cfg_InsID_Mask;
    }

static uint32 RegField_errins_en_cfg_channelId_Shift(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegField_errins_en_cfg_InsID_Shift;
    }

static uint32 RegAddress_errins_thr(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 RegField_errins_thr_errorType_Mask(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegField_errins_thr_type_Mask;
    }

static uint32 RegField_errins_thr_errorType_Shift(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegField_errins_thr_type_Shift;
    }

static uint32 RegField_errins_thr_Mode_Mask(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegField_errins_thr_mode_Mask;
    }

static uint32 RegField_errins_thr_Mode_Shift(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegField_errins_thr_mode_Shift;
    }

static uint32 ChannelIdGet(ThaPdhErrorGenerator self)
    {
    uint8 sliceId, hwIdInSlice;
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet((AtErrorGenerator)self);
    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &sliceId, &hwIdInSlice);

    return hwIdInSlice;
    }

static eBool ChannelIsReady(AtErrorGenerator self)
    {
    AtObject source = AtErrorGeneratorSourceGet(self);
    if (source == NULL)
        return cAtFalse;
    return cAtTrue;
    }

static ThaModulePdh PdhModule(AtPdhChannel self)
    {
    return (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    }

static uint32 ErrinsEnCfgAddress(ThaPdhErrorGenerator self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet((AtErrorGenerator)self);
    uint8 sliceId, hwIdInSlice;
    uint32 address, sliceAddress;

    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &sliceId, &hwIdInSlice);
    sliceAddress = (uint32)ThaModulePdhSliceOffset(PdhModule(channel), sliceId);
    address = mMethodsGet(mThis(self))->RegAddress_errins_en_cfg(mThis(self)) + sliceAddress;
    return address;
    }

static uint32 ErrinsThrAddress(ThaPdhErrorGenerator self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet((AtErrorGenerator)self);
    uint8 sliceId, hwIdInSlice;
    uint32 address, sliceAddress;

    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &sliceId, &hwIdInSlice);
    sliceAddress = (uint32)ThaModulePdhSliceOffset(PdhModule(channel), sliceId);
    address = mMethodsGet(mThis(self))->RegAddress_errins_thr(mThis(self)) + sliceAddress;
    return address;
    }

static void HwChannelSet(AtErrorGenerator self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet(self);
    uint32 address = ErrinsEnCfgAddress(mThis(self));
    uint32 regVal = mModuleHwRead(PdhModule(channel), address);
    uint32 mask = mMethodsGet(mThis(self))->RegField_errins_en_cfg_channelId_Mask(mThis(self));
    uint32 shift = mMethodsGet(mThis(self))->RegField_errins_en_cfg_channelId_Shift(mThis(self));
    uint32 channelId = mMethodsGet(mThis(self))->ChannelIdGet(mThis(self));
    mFieldIns(&regVal, mask, shift, channelId);

    mModuleHwWrite(PdhModule(channel), address, regVal);
    }

static eAtRet SourceSet(AtErrorGenerator self, AtObject channel)
    {
    eAtRet ret = m_AtErrorGeneratorMethods->SourceSet(self, channel);

    if (ret != cAtOk)
        return ret;

    if (channel != NULL )
        HwChannelSet(self);

    return cAtOk;
    }

static uint32 SwErrorType2HwErrorType(uint32 swErrorType)
    {
    if (swErrorType == cAtPdhErrorGeneratorErrorTypeFullBitStream)
        return 3;
    if (swErrorType == cAtPdhErrorGeneratorErrorTypePayload)
        return 2;
    if (swErrorType == cAtPdhErrorGeneratorErrorTypeFbit)
        return 1;
    return 0;
    }

static uint32 HwErrorType2SwErrorType(uint32 hwErrorType)
    {
    if (hwErrorType == 3)
        return cAtPdhErrorGeneratorErrorTypeFullBitStream;
    if (hwErrorType == 2)
        return cAtPdhErrorGeneratorErrorTypePayload;
    if (hwErrorType == 1)
        return cAtPdhErrorGeneratorErrorTypeFbit;
    return 0;
    }

static void HwErrorTypeSet(AtErrorGenerator self, uint32 errorType)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet(self);
    uint32 address, regVal, mask, shift;

    address = ErrinsThrAddress(mThis(self));
    regVal = mModuleHwRead(PdhModule(channel), address);
    mask = mMethodsGet(mThis(self))->RegField_errins_thr_errorType_Mask(mThis(self));
    shift = mMethodsGet(mThis(self))->RegField_errins_thr_errorType_Shift(mThis(self));

    mFieldIns(&regVal, mask, shift, errorType);
    mModuleHwWrite(PdhModule(channel), address, regVal);
    }

static uint32 HwErrorTypeGet(AtErrorGenerator self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet(self);
    uint32 address, regVal, mask, shift, errorType;

    address = ErrinsThrAddress(mThis(self));
    regVal = mModuleHwRead(PdhModule(channel), address);
    mask = mMethodsGet(mThis(self))->RegField_errins_thr_errorType_Mask(mThis(self));
    shift = mMethodsGet(mThis(self))->RegField_errins_thr_errorType_Shift(mThis(self));

    mFieldGet(regVal, mask, shift, uint32, &errorType);
    return errorType;
    }

static eAtRet ErrorTypeSet(AtErrorGenerator self, uint32 errorType)
    {

    if (!mMethodsGet(mThis(self))->ErrorTypeIsSupported(mThis(self), errorType))
        return cAtErrorModeNotSupport;

    if (!ChannelIsReady(self))
        return cAtErrorNotReady;

    HwErrorTypeSet(self, SwErrorType2HwErrorType(errorType));

    return cAtOk;
    }

static uint32 ErrorTypeGet(AtErrorGenerator self)
    {
    if (!ChannelIsReady(self))
        return cAtPdhErrorGeneratorErrorTypeInvalid;

    return HwErrorType2SwErrorType(HwErrorTypeGet(self));
    }

static uint32 MaxErrorNum(void)
    {
    return 0xFFFFFFF;
    }

static void HwErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet(self);
    uint32 address;
    uint32 regVal;

    address = ErrinsThrAddress(mThis(self));
    regVal = mModuleHwRead(PdhModule(channel), address);
    mFieldIns(&regVal, cAf6RegField_errins_thr_error_num_Mask, cAf6RegField_errins_thr_error_num_Shift, errorNum);

    mModuleHwWrite(PdhModule(channel), address, regVal);
    }

static uint32 ErrorRateToErrorNum(AtErrorGenerator self, eAtBerRate errorRate)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet(self);
    return AtErrorGeneratorErrorBitRateToErrorNum(errorRate)
            / ThaModulePdhErrorGeneratorBerDivCoefficient(PdhModule(channel));
    }

static eAtRet ErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    if (errorNum > MaxErrorNum())
        return cAtErrorOutOfRangParm;

    if (!ChannelIsReady(self))
        return cAtErrorNotReady;

    if (AtErrorGeneratorModeGet(self) != cAtErrorGeneratorModeOneshot)
        return cAtErrorNotApplicable;

    HwErrorNumSet(self, errorNum);
    AtErrorGeneratorErrorNumDbSet(self, errorNum);
    return cAtOk;
    }

static eAtRet ErrorRateSet(AtErrorGenerator self, eAtBerRate errorRate)
    {
    uint32 errorNum = AtErrorGeneratorErrorRateToErrorNum(self, errorRate);

    if (errorNum > MaxErrorNum())
        return cAtErrorOutOfRangParm;

    if (!ChannelIsReady(self))
        return cAtErrorNotReady;

    if (AtErrorGeneratorModeGet(self) != cAtErrorGeneratorModeContinuous)
        return cAtErrorNotApplicable;

    if (AtErrorGeneratorIsStarted(self))
        HwErrorNumSet(self, errorNum);

    AtErrorGeneratorErrorRateDbSet(self, errorRate);
    return cAtOk;
    }

static void HwModeSet(AtErrorGenerator self, uint32 hwMode)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet(self);
    uint32 address, regVal, mask, shift;

    address = ErrinsThrAddress(mThis(self));
    regVal = mModuleHwRead(PdhModule(channel), address);
    mask = mMethodsGet(mThis(self))->RegField_errins_thr_Mode_Mask(mThis(self));
    shift = mMethodsGet(mThis(self))->RegField_errins_thr_Mode_Shift(mThis(self));

    mFieldIns(&regVal, mask, shift, hwMode);
    mModuleHwWrite(PdhModule(channel), address, regVal);
    }

static uint32 SwMode2HwMode(uint32 swMode)
    {
    if (swMode == cAtErrorGeneratorModeOneshot)
        return 0;
    if (swMode == cAtErrorGeneratorModeContinuous)
        return 1;
    return 0;
    }

static uint32 HwModeGet(AtErrorGenerator self)
    {
    AtPdhChannel channel = (AtPdhChannel)AtErrorGeneratorSourceGet(self);
    uint32 address, regVal, mask, shift, hwMode;

    address = ErrinsThrAddress(mThis(self));
    regVal = mModuleHwRead(PdhModule(channel), address);
    mask = mMethodsGet(mThis(self))->RegField_errins_thr_Mode_Mask(mThis(self));
    shift = mMethodsGet(mThis(self))->RegField_errins_thr_Mode_Shift(mThis(self));

    mFieldGet(regVal, mask, shift, uint32, &hwMode);
    return hwMode;
    }

static uint32 HwMode2SwMode(uint32 hwMode)
    {
    if (hwMode == 0)
        return cAtErrorGeneratorModeOneshot;
    if (hwMode == 1)
        return cAtErrorGeneratorModeContinuous;
    return cAtErrorGeneratorModeInvalid;
    }

static eAtRet ModeSet(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    if (!AtErrorGeneratorModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    if (!ChannelIsReady(self))
        return cAtErrorNotReady;

    if (AtErrorGeneratorIsStarted(self) &&
        AtErrorGeneratorModeGet(self) == cAtErrorGeneratorModeContinuous &&
        mode == cAtErrorGeneratorModeOneshot)
        return cAtErrorNotApplicable;

    HwModeSet(self, SwMode2HwMode(mode));

    return cAtOk;
    }

static eAtErrorGeneratorMode ModeGet(AtErrorGenerator self)
    {
    if (!ChannelIsReady(self))
        return cAtErrorGeneratorModeInvalid;

    return HwMode2SwMode(HwModeGet(self));
    }

static eAtRet OneShotStart(AtErrorGenerator self)
    {
    uint32 errorNum = AtErrorGeneratorErrorNumGet(self);

    if (errorNum == 0)
        return cAtErrorInvlParm;

    HwErrorNumSet(self, AtErrorGeneratorErrorNumGet(self));

    return cAtOk;
    }

static eAtRet ContinuousStart(AtErrorGenerator self)
    {
    uint32 errorNum = AtErrorGeneratorErrorRateToErrorNum(self, AtErrorGeneratorErrorRateGet(self));

    if (errorNum == 0)
        return cAtErrorInvlParm;

    HwErrorNumSet(self, errorNum);
    AtErrorGeneratorContinuousStartedDbSet(self, cAtTrue);
    return cAtOk;
    }

static eAtRet Start(AtErrorGenerator self)
    {
    if (!ChannelIsReady(self))
        return cAtErrorNotReady;

    if (AtErrorGeneratorIsStarted(self))
        return cAtOk;

    if (AtErrorGeneratorModeGet(self) == cAtErrorGeneratorModeOneshot)
        return OneShotStart(self);

    if (AtErrorGeneratorModeGet(self) == cAtErrorGeneratorModeContinuous)
        return ContinuousStart(self);

    return cAtErrorNotReady;
    }

static eAtRet Stop(AtErrorGenerator self)
    {
    if (AtErrorGeneratorModeGet(self) != cAtErrorGeneratorModeContinuous)
        return cAtOk;

    if (!ChannelIsReady(self))
        return cAtErrorNotReady;

    HwErrorNumSet(self, 0);
    AtErrorGeneratorContinuousStartedDbSet(self, cAtFalse);
    return cAtOk;
    }

static eBool IsStarted(AtErrorGenerator self)
    {
    if (!ChannelIsReady(self))
        return cAtFalse;

    return m_AtErrorGeneratorMethods->IsStarted(self);
    }

static eBool ErrorTypeIsSupported(ThaPdhErrorGenerator self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtPdhErrorGeneratorErrorTypeFullBitStream)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet Init(AtErrorGenerator self)
    {
    if (AtErrorGeneratorSourceGet(self) == NULL)
        return cAtOk;

    AtErrorGeneratorErrorTypeSet(self, cAtPdhErrorGeneratorErrorTypeFullBitStream);
    AtErrorGeneratorModeSet(self, cAtErrorGeneratorModeOneshot);
    AtErrorGeneratorErrorNumSet(self, 0);
    AtErrorGeneratorStop(self);
    return cAtOk;
    }

static void AtErrorGeneratorOverride(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtErrorGeneratorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtErrorGeneratorOverride, m_AtErrorGeneratorMethods, sizeof(m_AtErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_AtErrorGeneratorOverride, SourceSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorTypeGet);
        mMethodOverride(m_AtErrorGeneratorOverride, ModeSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ModeGet);
        mMethodOverride(m_AtErrorGeneratorOverride, Start);
        mMethodOverride(m_AtErrorGeneratorOverride, Stop);
        mMethodOverride(m_AtErrorGeneratorOverride, IsStarted);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorNumSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorRateSet);
        mMethodOverride(m_AtErrorGeneratorOverride, ErrorRateToErrorNum);
        mMethodOverride(m_AtErrorGeneratorOverride, Init);
        }

    mMethodsSet(self, &m_AtErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    AtErrorGeneratorOverride(self);
    }

static void MethodsInit(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ChannelIdGet);
        mMethodOverride(m_methods, RegAddress_errins_en_cfg);
        mMethodOverride(m_methods, RegAddress_errins_thr);
        mMethodOverride(m_methods, RegField_errins_thr_errorType_Mask);
        mMethodOverride(m_methods, RegField_errins_thr_errorType_Shift);
        mMethodOverride(m_methods, RegField_errins_thr_Mode_Mask);
        mMethodOverride(m_methods, RegField_errins_thr_Mode_Shift);
        mMethodOverride(m_methods, RegField_errins_en_cfg_channelId_Mask);
        mMethodOverride(m_methods, RegField_errins_en_cfg_channelId_Shift);
        mMethodOverride(m_methods, ErrorTypeIsSupported);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

AtErrorGenerator ThaPdhErrorGeneratorObjectInit(AtErrorGenerator self, AtPdhChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtErrorGeneratorObjectInit(self, (AtObject)channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

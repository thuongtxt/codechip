/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ThaPdhErrorGenerator module name
 * 
 * File        : ThaPdhErrorGeneratorInternal.h
 * 
 * Created Date: Mar 29, 2016
 *
 * Description : Methods and data of the ThaPdhErrorGenerator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_DEFAULT_PDH_DIAG_THAPDHERRORGENERATORINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_DEFAULT_PDH_DIAG_THAPDHERRORGENERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/diag/AtErrorGeneratorInternal.h"
#include "ThaPdhErrorGenerator.h"
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhErrorGeneratorMethods
    {
    uint32 (*ChannelIdGet)(ThaPdhErrorGenerator self);
    uint32 (*RegAddress_errins_en_cfg)(ThaPdhErrorGenerator self);
    uint32 (*RegField_errins_en_cfg_channelId_Mask)(ThaPdhErrorGenerator self);
    uint32 (*RegField_errins_en_cfg_channelId_Shift)(ThaPdhErrorGenerator self);

    uint32 (*RegAddress_errins_thr)(ThaPdhErrorGenerator self);
    uint32 (*RegField_errins_thr_errorType_Mask)(ThaPdhErrorGenerator self);
    uint32 (*RegField_errins_thr_errorType_Shift)(ThaPdhErrorGenerator self);
    uint32 (*RegField_errins_thr_Mode_Mask)(ThaPdhErrorGenerator self);
    uint32 (*RegField_errins_thr_Mode_Shift)(ThaPdhErrorGenerator self);
    eBool (*ErrorTypeIsSupported)(ThaPdhErrorGenerator self, uint32 mode);
    }tThaPdhErrorGeneratorMethods;

typedef struct tThaPdhErrorGenerator
    {
    tAtErrorGenerator super;
    const tThaPdhErrorGeneratorMethods *methods;

    } tThaPdhErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator ThaPdhErrorGeneratorObjectInit(AtErrorGenerator self, AtPdhChannel channel);
AtErrorGenerator ThaPdhDe3RxErrorGeneratorObjectInit(AtErrorGenerator self, AtPdhChannel channel);
AtErrorGenerator ThaPdhDe3TxErrorGeneratorObjectInit(AtErrorGenerator self, AtPdhChannel channel);
AtErrorGenerator ThaPdhDe1RxErrorGeneratorObjectInit(AtErrorGenerator self, AtPdhChannel channel);
AtErrorGenerator ThaPdhDe1TxErrorGeneratorObjectInit(AtErrorGenerator self, AtPdhChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_DEFAULT_PDH_DIAG_THAPDHERRORGENERATORINTERNAL_H_ */


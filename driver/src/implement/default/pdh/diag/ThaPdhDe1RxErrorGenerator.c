/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaDiagPdhDe1RxErrorGenerator.c
 *
 * Created Date: Mar 22, 2016
 *
 * Description : Implementation of the ThaPdhDe1RxErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "ThaPdhErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6RegAddress_de1_rx_errins_en_cfg 0x00750017

#define cAf6RegAddress_de1_rx_errins_thr_cfg 0x00750018
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaPdhErrorGeneratorMethods m_ThaPdhErrorGeneratorOverride;
static tAtErrorGeneratorMethods m_AtErrorGeneratorOverride;

static const tAtErrorGeneratorMethods *m_AtErrorGeneratorMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhErrorGenerator);
    }

static uint32 RegAddress_errins_en_cfg(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegAddress_de1_rx_errins_en_cfg;
    }

static uint32 RegAddress_errins_thr(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegAddress_de1_rx_errins_thr_cfg;
    }

static eBool ModeIsSupported(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    AtUnused(self);
    if (mode == cAtErrorGeneratorModeOneshot ||
        mode == cAtErrorGeneratorModeContinuous)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 ChannelIdGet(ThaPdhErrorGenerator self)
    {
    return ThaPdhDe1ErrorGeneratorHwId((ThaPdhDe1)AtErrorGeneratorSourceGet((AtErrorGenerator)self));
    }

static void AtErrorGeneratorOverride(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtErrorGeneratorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtErrorGeneratorOverride, m_AtErrorGeneratorMethods, sizeof(m_AtErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_AtErrorGeneratorOverride, ModeIsSupported);
        }

    mMethodsSet(self, &m_AtErrorGeneratorOverride);
    }

static void ThaPdhErrorGeneratorOverride(AtErrorGenerator self)
    {
    ThaPdhErrorGenerator generator = (ThaPdhErrorGenerator)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhErrorGeneratorOverride, mMethodsGet(generator), sizeof(m_ThaPdhErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPdhErrorGeneratorOverride, RegAddress_errins_en_cfg);
        mMethodOverride(m_ThaPdhErrorGeneratorOverride, RegAddress_errins_thr);
        mMethodOverride(m_ThaPdhErrorGeneratorOverride, ChannelIdGet);
        }

    mMethodsSet(generator, &m_ThaPdhErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    AtErrorGeneratorOverride(self);
    ThaPdhErrorGeneratorOverride(self);
    }

static AtErrorGenerator ObjectInit(AtErrorGenerator self, AtPdhChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhDe3RxErrorGeneratorObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtErrorGenerator ThaPdhDe1RxErrorGeneratorNew(AtPdhChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(errorGenerator, channel);
    }

void ThaPdhDe1RxErrorGeneratorHwDefaultSet(ThaModulePdh self)
    {
    uint32 address, sliceAddress;
    uint8 i, numSlices = ThaModulePdhNumSlices(self);
    for (i = 0; i < numSlices; i++)
        {
        sliceAddress = (uint32)ThaModulePdhSliceOffset(self, i);
        address = cAf6RegAddress_de1_rx_errins_en_cfg + sliceAddress;
        mModuleHwWrite(self, address, 0);
        address = cAf6RegAddress_de1_rx_errins_thr_cfg + sliceAddress;
        mModuleHwWrite(self, address, 0);
        }
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ThaPdhErrorGenerator module name
 * 
 * File        : ThaPdhErrorGenerator.h
 * 
 * Created Date: Mar 29, 2016
 *
 * Description : Interfaces of the ThaPdhErrorGenerator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_DEFAULT_PDH_DIAG_THAPDHERRORGENERATOR_H_
#define _DRIVER_SRC_IMPLEMENT_DEFAULT_PDH_DIAG_THAPDHERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h"
#include "../ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhErrorGenerator *ThaPdhErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator ThaPdhDe3RxDiagErrorGeneratorNew(AtPdhChannel channel);
AtErrorGenerator ThaPdhDe3TxErrorGeneratorNew(AtPdhChannel channel);
AtErrorGenerator ThaPdhDe1RxErrorGeneratorNew(AtPdhChannel channel);
AtErrorGenerator ThaPdhDe1TxErrorGeneratorNew(AtPdhChannel channel);
void ThaPdhDe3RxErrorGeneratorHwDefaultSet(ThaModulePdh self);
void ThaPdhDe3TxErrorGeneratorHwDefaultSet(ThaModulePdh self);
void ThaPdhDe1TxErrorGeneratorHwDefaultSet(ThaModulePdh self);
void ThaPdhDe1RxErrorGeneratorHwDefaultSet(ThaModulePdh self);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_DEFAULT_PDH_DIAG_THAPDHERRORGENERATOR_H_ */


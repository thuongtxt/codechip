/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe3RxErrorGenerator.c
 *
 * Created Date: Mar 22, 2016
 *
 * Description : Implementation of the ThaPdhDe3RxErrorGenerator, a concrete class of the
 *               AtErrorGenerator.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "ThaPdhErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6RegAddress_errins_en_cfg 0x007400C0
#define cAf6RegField_RxDE3errorsingle_en_Mask     cBit12
#define cAf6RegField_RxDE3errorsingle_en_Shift    12
#define cAf6RegField_RxDE3errormode_Mask          cBit11_10
#define cAf6RegField_RxDE3errormode_Shift         10
#define cAf6RegField_RxDE3ErrInsID_Mask           cBit9_0
#define cAf6RegField_RxDE3ErrInsID_Shift          0

#define cAf6RegAddress_errins_thr_cfg 0x007400C1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhErrorGenerator)self)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhErrorGeneratorMethods m_ThaPdhErrorGeneratorOverride;

static const tThaPdhErrorGeneratorMethods *m_ThaPdhErrorGeneratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhErrorGenerator);
    }

static uint32 RegAddress_errins_en_cfg(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegAddress_errins_en_cfg;
    }

static uint32 RegAddress_errins_thr(ThaPdhErrorGenerator self)
    {
    AtUnused(self);
    return cAf6RegAddress_errins_thr_cfg;
    }

static void ThaPdhErrorGeneratorOverride(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPdhErrorGeneratorMethods = mMethodsGet(mThis(self));
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhErrorGeneratorOverride, m_ThaPdhErrorGeneratorMethods, sizeof(m_ThaPdhErrorGeneratorOverride));

        /* Setup methods */
        mMethodOverride(m_ThaPdhErrorGeneratorOverride, RegAddress_errins_en_cfg);
        mMethodOverride(m_ThaPdhErrorGeneratorOverride, RegAddress_errins_thr);
        }

    mMethodsSet(mThis(self), &m_ThaPdhErrorGeneratorOverride);
    }

static void Override(AtErrorGenerator self)
    {
    ThaPdhErrorGeneratorOverride(self);
    }

AtErrorGenerator ThaPdhDe3RxErrorGeneratorObjectInit(AtErrorGenerator self, AtPdhChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhErrorGeneratorObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtErrorGenerator ThaPdhDe3RxDiagErrorGeneratorNew(AtPdhChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtErrorGenerator errorGenerator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (errorGenerator == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhDe3RxErrorGeneratorObjectInit(errorGenerator, channel);
    }

void ThaPdhDe3RxErrorGeneratorHwDefaultSet(ThaModulePdh self)
    {
    uint32 address, sliceAddress;
    uint8 i, numSlices = ThaModulePdhNumSlices(self);
    for (i = 0; i < numSlices; i++)
        {
        sliceAddress = (uint32)ThaModulePdhSliceOffset(self, i);
        address = cAf6RegAddress_errins_en_cfg + sliceAddress;
        mModuleHwWrite(self, address, 0);
        address = cAf6RegAddress_errins_thr_cfg + sliceAddress;
        mModuleHwWrite(self, address, 0);
        }
    }

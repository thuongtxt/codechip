/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe2De1.h
 * 
 * Created Date: Apr 1, 2015
 *
 * Description : DE1 which is mapped to DE2
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE2DE1_H_
#define _THAPDHDE2DE1_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPdhVcDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe2De1 * ThaPdhDe2De1;
typedef struct tThaPdhDe2De1
    {
    tThaPdhVcDe1 super;
    }tThaPdhDe2De1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 ThaPdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

eAtRet ThaPdhDe2De1HwIdGet(ThaPdhDe1 self, uint8 *hwSlice, uint8 *hwStsInSlice, uint8 *hwDe2Id, uint8 *hwDe1Id, eAtModule phyModule);
uint8 ThaPdhDe2De1HwDe2IdGet(ThaPdhDe1 self);
uint8 ThaPdhDe2De1HwDe1IdGet(ThaPdhDe1 self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE2DE1_H_ */


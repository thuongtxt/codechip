/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe3.h
 * 
 * Created Date: Feb 2, 2013
 *
 * Description : DS3/E3
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE3_H_
#define _THAPDHDE3_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe3.h" /* Super class */
#include "../cdr/controllers/ThaCdrController.h"
#include "AtClockExtractor.h"
#include "retimingcontrollers/ThaPdhRetimingController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe3 * ThaPdhDe3;

typedef enum eThaPdhDe3WorkingSide
    {
    cThaPdhDe3WorkingSideLiu,
    cThaPdhDe3WorkingSideSdh
    }eThaPdhDe3WorkingSide;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concretes */
AtPdhDe3 ThaPdhDe3New(uint32 channelId, AtModulePdh module);

ThaCdrController ThaPdhDe3CdrControllerGet(ThaPdhDe3 self);
uint8 ThaPdhDe3PartId(ThaPdhDe3 self);
eAtRet ThaPdhDe3MapSet(AtPdhChannel self);
AtPrbsEngine ThaPdhDe3CachePrbsEngine(ThaPdhDe3 self, AtPrbsEngine engine);
eAtRet ThaPdhDe3WorkingSideSet(ThaPdhDe3 self, eThaPdhDe3WorkingSide workingSide);
uint32 ThaPdhDe3DefaultOffset(ThaPdhDe3 self);
eAtRet ThaPdhDe3HwIdFactorGet(ThaPdhDe3 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice);
eBool ThaPdhDe3IsManagedByModulePdh(ThaPdhDe3 self);
eAtRet ThaPdhDe3De1Hw2SwIdGet(ThaPdhDe3 de3, uint8 hwDe2Id, uint8 hwDe1Id, uint8* de2Id, uint8* de1Id);
eAtRet ThaPdhDe3RxFramingMonOnlyEnable(ThaPdhDe3 self, eBool enable);
eAtRet ThaPdhDe3TxFramerBypass(ThaPdhDe3 self, eBool bypass);
eAtRet ThaPdhDe3MapBypass(ThaPdhDe3 self, eBool bypass);
eAtRet ThaPdhDe3FramingBypass(ThaPdhDe3 self, eBool isBypass);
eAtRet ThaPdhDe3HwFrameTypeSet(ThaPdhDe3 self, uint16 frameType);
AtPdhMdlController ThaPdhDe3MdlControllerGet(ThaPdhDe3 self);
eAtRet ThaPdhDe3HwFrameTypeSet(ThaPdhDe3 self, uint16 frameType);
eAtRet ThaPdhDe3MappingDisable(ThaPdhDe3 self);
eAtRet ThaPdhDe3MappingRestore(ThaPdhDe3 self);

/* Product concretes */
AtPdhDe3 Tha60210031PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha60210011PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha60210061PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha60290011PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha60210061PdhVcDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha62290011PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha60290022PdhVcDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha60290022PdhVcDe3V3New(uint32 channelId, AtModulePdh module);

/* CLOCK EXTRACTOR */
void ThaPdhDe3ClockExtractorSet(ThaPdhDe3 self, AtClockExtractor clockExtractor);
AtClockExtractor ThaPdhDe3ClockExtractorGet(ThaPdhDe3 self);

/* Retiming */
eBool ThaPdhDe3RetimingIsSupported(ThaPdhDe3 self);
ThaPdhRetimingController ThaPdhDe3RetimingControllerGet(ThaPdhDe3 self);

eAtRet ThaPdhDe3PrbsDe3LevelSet(ThaPdhDe3 self, eBool en);
eBool ThaPdhDe3PrbsDe3LevelGet(ThaPdhDe3 self);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE3_H_ */


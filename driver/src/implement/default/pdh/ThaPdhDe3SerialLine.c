/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDe3SerialLine.c
 *
 * Created Date: Jun 12, 2016
 *
 * Description : Implementation for the PDH serial line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhVcInternal.h"
#include "ThaModulePdh.h"
#include "ThaPdhDe3SerialLine.h"
#include "ThaPdhDe3.h"


/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((ThaPdhDe3SerialLine)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhDe3SerialLineMethods m_methods;

/* Override */
static tAtPdhChannelMethods     m_AtPdhChannelOverride;
static tAtPdhSerialLineMethods  m_AtPdhSerialLineOverride;

/* Save super implementation */
static const tAtPdhSerialLineMethods *m_AtPdhSerialLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HwIdFactorGet(ThaPdhDe3SerialLine self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice)
    {
    AtUnused(phyModule);
    *slice = 0;
    *hwIdInSlice = AtChannelIdGet((AtChannel)self);
    return cAtOk;
    }

static eAtRet CascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5)
    {
    *layer3 = 0;
    *layer4 = 0;
    *layer5 = 0;
    return ThaPdhDe3SerialLineHwIdFactorGet((ThaPdhDe3SerialLine)self, phyModule, layer1, layer2);
    }

static AtModulePdh PdhModule(AtPdhSerialLine self)
    {
    return (AtModulePdh)AtChannelModuleGet((AtChannel)self);
    }

static AtPdhDe3 De3ObjectGet(ThaPdhDe3SerialLine self)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)self);
    return AtModulePdhDe3Get(PdhModule((AtPdhSerialLine)self), lineId);
    }

static AtSdhLine Ec1ObjectGet(ThaPdhDe3SerialLine self)
    {
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleSdh);
    return AtModuleSdhLineGet(sdhModule, (uint8)(lineId + AtModuleSdhStartEc1LineIdGet(sdhModule)));
    }

static AtChannel ChannelGet(AtPdhSerialLine self)
    {
    if (AtPdhSerialLineModeGet(self) == cAtPdhDe3SerialLineModeEc1)
        return (AtChannel)ThaPdhDe3SerialLineEc1ObjectGet((ThaPdhDe3SerialLine)self);

    return (AtChannel)ThaPdhDe3SerialLineDe3ObjectGet((ThaPdhDe3SerialLine)self);
    }

static eAtRet ModeSet(AtPdhSerialLine self, uint32 mode)
    {
    eAtRet ret = cAtOk;
    uint32 currentMode = AtPdhSerialLineModeGet(self);
    ThaPdhDe3 de3 = (ThaPdhDe3)ThaPdhDe3SerialLineDe3ObjectGet(mThis(self));
    AtChannel channel;

    if (currentMode == mode)
        return cAtOk;

    if (AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    /* Changing mode shall change the associated channel interface. So we need
     * to clean-up it before applying new mode. */
    ret = AtChannelHardwareCleanup(AtPdhSerialLineChannelGet(self));
    if (ret != cAtOk)
        return ret;

    /* We need to change VC3 mapping C3 to delete the reference VC3-DE3
     * before changing from EC1 mode to LIU mode */
    if (currentMode == cAtPdhDe3SerialLineModeEc1)
        {
        AtSdhLine line = ThaPdhDe3SerialLineEc1ObjectGet(mThis(self));
        AtSdhVc vc3 = AtSdhLineVc3Get(line, 0, 0);

        ret = AtSdhChannelMapTypeSet((AtSdhChannel)vc3, cAtSdhVcMapTypeVc3MapC3);
        if (ret != cAtOk)
            return ret;
        }

    /* Call super's implementation */
    ret = m_AtPdhSerialLineMethods->ModeSet(self, mode);

    if (mode == cAtPdhDe3SerialLineModeEc1)
        {
        AtSdhLine line = ThaPdhDe3SerialLineEc1ObjectGet(mThis(self));

        ret  = ThaPdhDe3WorkingSideSet(de3, cThaPdhDe3WorkingSideSdh);
        ret |= ThaPdhDe3SerialLineMapEc1(mThis(self), line);

        /* Reset the associated DS3/E3 channel. */
        ret |= AtChannelInit((AtChannel)de3);
        }
    else
        {
        AtBerController controller;

        ret  = ThaPdhDe3WorkingSideSet(de3, cThaPdhDe3WorkingSideLiu);
        ret |= ThaPdhDe3SerialLineMapDe3(mThis(self), (AtPdhDe3)de3);

        /* Re-initialize DS3/E3 line BER to take affect. */
        controller = AtPdhChannelLineBerControllerGet((AtPdhChannel)de3);
        if (controller && AtBerControllerIsEnabled(controller))
            {
            ret |= AtBerControllerEnable(controller, cAtFalse);
            ret |= AtBerControllerEnable(controller, cAtTrue);
            }
        }

    if (ret != cAtOk)
        return ret;

    /* Get associated channel after new mode is applied. */
    channel = AtPdhSerialLineChannelGet(self);

    /* Initialize the associated channel. */
    ret = AtChannelInit(channel);
    if (ret != cAtOk)
        {
        m_AtPdhSerialLineMethods->ModeSet(self, currentMode);
        return ret;
        }

    return ret;
    }

static eAtRet MapEc1Line(ThaPdhDe3SerialLine self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    /* Nothing to do here. */
    return cAtOk;
    }

static eAtRet MapDe3Line(ThaPdhDe3SerialLine self, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    /* Nothing to do here. */
    return cAtOk;
    }

static void MethodsInit(AtPdhSerialLine self)
    {
    ThaPdhDe3SerialLine serLine = (ThaPdhDe3SerialLine)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwIdFactorGet);
        mMethodOverride(m_methods, De3ObjectGet);
        mMethodOverride(m_methods, MapEc1Line);
        mMethodOverride(m_methods, MapDe3Line);
        }

    mMethodsSet(serLine, &m_methods);
    }

static void OverrideAtPdhChannel(AtPdhSerialLine self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, mMethodsGet(channel), sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, CascadeHwIdGet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtPdhSerialLine(AtPdhSerialLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhSerialLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhSerialLineOverride, mMethodsGet(self), sizeof(m_AtPdhSerialLineOverride));

        mMethodOverride(m_AtPdhSerialLineOverride, ChannelGet);
        mMethodOverride(m_AtPdhSerialLineOverride, ModeSet);
        }

    mMethodsSet(self, &m_AtPdhSerialLineOverride);
    }

static void Override(AtPdhSerialLine self)
    {
    OverrideAtPdhChannel(self);
    OverrideAtPdhSerialLine(self);
    }

AtPdhSerialLine ThaPdhDe3SerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaPdhDe3SerialLine));

    /* Supper constructor */
    if (AtPdhSerialLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet ThaPdhDe3SerialLineHwIdFactorGet(ThaPdhDe3SerialLine self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice)
    {
    if (self)
        return mMethodsGet(self)->HwIdFactorGet(self, phyModule, slice, hwIdInSlice);
    return cAtErrorNullPointer;
    }

AtPdhDe3 ThaPdhDe3SerialLineDe3ObjectGet(ThaPdhDe3SerialLine self)
    {
    if (self)
        return mMethodsGet(self)->De3ObjectGet(self);

    return NULL;
    }

AtSdhLine ThaPdhDe3SerialLineEc1ObjectGet(ThaPdhDe3SerialLine self)
    {
    if (self)
        return Ec1ObjectGet(self);
    return NULL;
    }

eAtRet ThaPdhDe3SerialLineMapEc1(ThaPdhDe3SerialLine self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->MapEc1Line(self, line);
    return cAtErrorNullPointer;
    }

eAtRet ThaPdhDe3SerialLineMapDe3(ThaPdhDe3SerialLine self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->MapDe3Line(self, de3);
    return cAtErrorNullPointer;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhMdlInterruptManager.c
 *
 * Created Date: Sep 20, 2017
 *
 * Description : Default MDL interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/pdh/AtPdhMdlControllerInternal.h"
#include "../ThaModulePdh.h"
#include "ThaPdhMdlInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Default interrupt address/mask based on DS3 CEM */
#define cAf6_inten_status_MDLIntEnable_Mask     cBit25

/* Global */
#define cAf6Reg_mdl_sta_int_Base                0xA1FF
#define cAf6Reg_mdl_en_int_Base                 0xA1FE
#define cAf6_mdl_en_int_mdlen_int_Mask          cBit1_0

/* Slice */
#define cAf6Reg_mdl_intsta_Base                 0xA1C0

/* Channels */
#define cAf6Reg_mdl_cfgen_int_Base              0xA100
#define cAf6Reg_mdl_int_sta_Base                0xA140
#define cAf6_mdl_int_sta_mdlint_sta_Mask        cBit0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)             ((ThaPdhMdlInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhMdlInterruptManagerMethods m_methods;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtInterruptManager self)
    {
    return (ThaModulePdh)AtInterruptManagerModuleGet(self);
    }

/* Default interrupt process based on DS3 CEM */
static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    ThaModulePdh pdhModule = PdhModule(self);
    ThaPdhMdlInterruptManager manager = (ThaPdhMdlInterruptManager)self;
    uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    uint32 numSlices = ThaInterruptManagerNumSlices((ThaInterruptManager)self);
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices((ThaInterruptManager)self);
    uint32 intrStatus = AtHalRead(hal, (baseAddress + mMethodsGet(manager)->GlbInterruptStatusRegister(manager)));
    uint32 intrMask = AtHalRead(hal, (baseAddress + mMethodsGet(manager)->GlbInterruptEnableRegister(manager)));
    uint32 sliceAddr = mMethodsGet(manager)->SliceInterruptStatusRegister(manager);
    uint8 slice_i;
    AtUnused(glbIntr);
    intrStatus &= intrMask;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        if (intrStatus & cIteratorMask(slice_i))
            {
            uint32 intrStatus24 = AtHalRead(hal, (baseAddress + sliceAddr + slice_i));
            uint8 de3InVc;

            for (de3InVc = 0; de3InVc < numStsInSlice; de3InVc++)
                {
                if (intrStatus24 & cIteratorMask(de3InVc))
                    {
                    AtPdhDe3 de3Channel = ThaModulePdhDe3ChannelFromHwIdGet(pdhModule, cAtModulePdh, slice_i, de3InVc);
                    AtPdhMdlController controller = ThaPdhDe3MdlControllerGet((ThaPdhDe3)de3Channel);

                    AtPdhMdlControllerInterruptProcess(controller, slice_i, de3InVc, hal);
                    }
                }
            }
        }
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x600000;
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_inten_status_MDLIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    AtModule module = AtInterruptManagerModuleGet((AtInterruptManager)self);
    uint32 regAddress = ThaInterruptManagerBaseAddress(self) + mMethodsGet(mThis(self))->GlbInterruptEnableRegister(mThis(self));
    AtModuleHwWrite(module, regAddress, (enable) ? cBit31_0 : 0x0, hal);
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 24;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_int_sta_Base;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_cfgen_int_Base;
    }

static uint32 GlbInterruptEnableRegister(ThaPdhMdlInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_en_int_Base;
    }

static uint32 GlbInterruptStatusRegister(ThaPdhMdlInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_sta_int_Base;
    }

static uint32 SliceInterruptStatusRegister(ThaPdhMdlInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_mdl_intsta_Base;
    }

static void MethodsInit(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, GlbInterruptEnableRegister);
        mMethodOverride(m_methods, GlbInterruptStatusRegister);
        mMethodOverride(m_methods, SliceInterruptStatusRegister);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, NumStsInSlice);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhMdlInterruptManager);
    }

AtInterruptManager ThaPdhMdlInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager ThaPdhMdlInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhMdlInterruptManagerObjectInit(newManager, module);
    }

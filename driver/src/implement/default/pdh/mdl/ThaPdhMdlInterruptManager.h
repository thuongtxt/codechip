/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhMdlInterruptManager.h
 * 
 * Created Date: Sep 20, 2017
 *
 * Description : Default MDL interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHMDLINTERRUPTMANAGER_H_
#define _THAPDHMDLINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/interrupt/AtInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhMdlInterruptManager * ThaPdhMdlInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager ThaPdhMdlInterruptManagerNew(AtModule module);

/* Concretes */
AtInterruptManager Tha60210011PdhMdlInterruptManagerNew(AtModule module);
AtInterruptManager Tha60210021PdhMdlInterruptManagerNew(AtModule module);
AtInterruptManager Tha60210012PdhMdlInterruptManagerNew(AtModule module);
AtInterruptManager Tha60210061PdhMdlInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290022PdhMdlInterruptManagerNew(AtModule module);
AtInterruptManager Tha6A290021PdhMdlInterruptManagerNew(AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAPDHMDLINTERRUPTMANAGER_H_ */


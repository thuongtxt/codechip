/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhMdlInterruptManagerInternal.h
 * 
 * Created Date: Sep 20, 2017
 *
 * Description : Default MDL interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHMDLINTERRUPTMANAGERINTERNAL_H_
#define _THAPDHMDLINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/intrcontroller/ThaInterruptManagerInternal.h"
#include "ThaPdhMdlInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhMdlInterruptManagerMethods
    {
    uint32 (*GlbInterruptEnableRegister)(ThaPdhMdlInterruptManager self);
    uint32 (*GlbInterruptStatusRegister)(ThaPdhMdlInterruptManager self);
    uint32 (*SliceInterruptStatusRegister)(ThaPdhMdlInterruptManager self);
    }tThaPdhMdlInterruptManagerMethods;

typedef struct tThaPdhMdlInterruptManager
    {
    tThaInterruptManager super;
    struct tThaPdhMdlInterruptManagerMethods *methods;
    }tThaPdhMdlInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager ThaPdhMdlInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAPDHMDLINTERRUPTMANAGERINTERNAL_H_ */


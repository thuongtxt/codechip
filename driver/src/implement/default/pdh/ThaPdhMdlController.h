/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhMdlController.h
 *
 * Created Date: May 29, 2015
 *
 * Description : PDH
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _THAMDLCONTROLLER_H_
#define _THAMDLCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* MDL Controller class */
typedef struct tThaPdhMdlController * ThaPdhMdlController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* For Inherent   */
AtPdhMdlController ThaPdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType);
AtPdhMdlController ThaPdhMdlControllerObjectInit(AtPdhMdlController self, AtPdhDe3 de3, uint32 mdlType);

/* For periodic sending   */
void ThaPdhMdlControllerPeriodProcess(AtPdhMdlController self, uint32 periodInMs);
eAtRet ThaPdhMdlControllerHwStandardHeaderSet(AtPdhMdlController self, uint32 standard);

/* Concrete classes */
AtPdhMdlController Tha60290022PdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType);
AtPdhMdlController Tha6A290021PdhMdlControllerNew(AtPdhDe3 self, uint32 mdlType);

#endif /* _THAMDLCONTROLLER_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaPdhDebugger.h
 * 
 * Created Date: Nov 12, 2015
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDEBUGGER_H_
#define _THAPDHDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtPdhDe1.h"
#include "../ThaModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDebugger *ThaPdhDebugger;

typedef enum eThaPdhDebuggerPrmPayloadBit
    {
    cThaPdhDebuggerPrmPayloadBitG1  = cBit0,
    cThaPdhDebuggerPrmPayloadBitG2  = cBit1,
    cThaPdhDebuggerPrmPayloadBitG3  = cBit2,
    cThaPdhDebuggerPrmPayloadBitG4  = cBit3,
    cThaPdhDebuggerPrmPayloadBitG5  = cBit4,
    cThaPdhDebuggerPrmPayloadBitG6  = cBit5,
    cThaPdhDebuggerPrmPayloadBitSE  = cBit6,
    cThaPdhDebuggerPrmPayloadBitFE  = cBit7,
    cThaPdhDebuggerPrmPayloadBitLV  = cBit8,
    cThaPdhDebuggerPrmPayloadBitSL  = cBit9,
    cThaPdhDebuggerPrmPayloadBitLB  = cBit10,
    cThaPdhDebuggerPrmPayloadBitNmNi= cBit11
    }eThaPdhDebuggerPrmPayloadBit;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPdhDebugger ThaPdhDebuggerNew(ThaModulePdh pdhModule);
ThaPdhDebugger Tha60210031PdhDebuggerNew(ThaModulePdh pdhModule);
ThaPdhDebugger Tha60210021PdhDebuggerNew(ThaModulePdh pdhModule);
ThaPdhDebugger Tha60210011PdhDebuggerNew(ThaModulePdh pdhModule);
ThaPdhDebugger Tha60210061PdhDebuggerNew(ThaModulePdh modulePdh);

ThaModulePdh ThaPdhDebuggerModuleGet(ThaPdhDebugger self);
eAtModulePdhRet ThaPdhDebuggerPrmChannelSelect(ThaPdhDebugger self, AtPdhDe1 de1);
uint32 ThaPdhDebuggerPrmChannelSelectGet(ThaPdhDebugger self);
eAtModulePdhRet ThaPdhDebuggerPrmMonitorEnable(ThaPdhDebugger self, eBool enable);
eBool ThaPdhDebuggerPrmMonitorIsEnabled(ThaPdhDebugger self);
eAtModulePdhRet ThaPdhDebuggerPrmForcedFullPrmPayload(ThaPdhDebugger self, eBool enable);
eBool ThaPdhDebuggerPrmIsForcedFullPrmPayload(ThaPdhDebugger self);
eAtModulePdhRet ThaPdhDebuggerPrmForce8BitPrmPayload(ThaPdhDebugger self, eBool enable);
eBool ThaPdhDebuggerPrmIsForced8BitPrmPayload(ThaPdhDebugger self);
eAtModulePdhRet ThaPdhDebuggerPrmForcePayLoad(ThaPdhDebugger self, uint32 payloadMap, eBool forced);
uint32 ThaPdhDebuggerPrmIsForcedPayLoad(ThaPdhDebugger self, uint32 *payLoadValue);
eAtModulePdhRet ThaPdhDebuggerPrmForceNmNi(ThaPdhDebugger self, uint32 payloadMap, uint8 swValue);
uint8 ThaPdhDebuggerPrmUnForceNmNi(ThaPdhDebugger self);
eAtRet ThaPdhDebuggerDe1LiuLoopbackSet(ThaPdhDebugger self, AtPdhDe1 de1, uint8 loopbackMode);
uint8 ThaPdhDebuggerDe1LiuLoopbackGet(ThaPdhDebugger self, AtPdhDe1 de1);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDEBUGGER_H_ */


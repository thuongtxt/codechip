/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhDebugger.c
 *
 * Created Date: Nov 12, 2015
 *
 * Description : PDH Debugger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhDebuggerInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhDebugger)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaPdhDebuggerMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePdhRet PrmChannelSelect(ThaPdhDebugger self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cAtErrorNotImplemented;
    }

static uint32 PrmChannelSelectGet(ThaPdhDebugger self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eAtModulePdhRet PrmMonitorEnable(ThaPdhDebugger self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PrmMonitorIsEnabled(ThaPdhDebugger self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePdhRet PrmForcedFullPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PrmIsForcedFullPrmPayload(ThaPdhDebugger self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePdhRet PrmForce8BitPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PrmIsForced8BitPrmPayload(ThaPdhDebugger self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePdhRet PrmForcePayLoad(ThaPdhDebugger self, uint32 payloadMap, eBool forced)
    {
    AtUnused(self);
    AtUnused(payloadMap);
    AtUnused(forced);
    return cAtErrorNotImplemented;
    }

static uint32 PrmIsForcedPayLoad(ThaPdhDebugger self, uint32 *payLoadValue)
    {
    AtUnused(self);
    AtUnused(payLoadValue);
    return cBit31_0;
    }

static eAtModulePdhRet PrmForceNmNi(ThaPdhDebugger self, uint32 payloadMap, uint8 swValue)
    {
    AtUnused(self);
    AtUnused(payloadMap);
    AtUnused(swValue);
    return cAtErrorNotImplemented;
    }

static uint8 PrmUnForceNmNi(ThaPdhDebugger self)
    {
    AtUnused(self);
    return cBit7_0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaPdhDebugger object = (ThaPdhDebugger)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(pdhModule);
    }

static eAtRet De1LiuLoopbackSet(ThaPdhDebugger self, AtPdhDe1 de1, uint8 loopbackMode)
    {
    AtUnused(self);
    AtUnused(de1);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 De1LiuLoopbackGet(ThaPdhDebugger self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cAtLoopbackModeRelease;
    }

static void OverrideAtObject(ThaPdhDebugger self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaPdhDebugger self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(ThaPdhDebugger self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PrmChannelSelect);
        mMethodOverride(m_methods, PrmChannelSelectGet);
        mMethodOverride(m_methods, PrmMonitorEnable);
        mMethodOverride(m_methods, PrmMonitorIsEnabled);
        mMethodOverride(m_methods, PrmForcedFullPrmPayload);
        mMethodOverride(m_methods, PrmIsForcedFullPrmPayload);
        mMethodOverride(m_methods, PrmForce8BitPrmPayload);
        mMethodOverride(m_methods, PrmIsForced8BitPrmPayload);
        mMethodOverride(m_methods, PrmForcePayLoad);
        mMethodOverride(m_methods, PrmIsForcedPayLoad);
        mMethodOverride(m_methods, PrmForceNmNi);
        mMethodOverride(m_methods, PrmUnForceNmNi);
        mMethodOverride(m_methods, De1LiuLoopbackSet);
        mMethodOverride(m_methods, De1LiuLoopbackGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhDebugger);
    }

ThaPdhDebugger ThaPdhDebuggerObjectInit(ThaPdhDebugger self, ThaModulePdh pdhModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    mThis(self)->pdhModule = pdhModule;

    return self;
    }

ThaPdhDebugger ThaPdhDebuggerNew(ThaModulePdh pdhModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPdhDebugger newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhDebuggerObjectInit(newChecker, pdhModule);
    }

ThaModulePdh ThaPdhDebuggerModuleGet(ThaPdhDebugger self)
    {
    if (self)
        return self->pdhModule;
    return NULL;
    }

eAtModulePdhRet ThaPdhDebuggerPrmChannelSelect(ThaPdhDebugger self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->PrmChannelSelect(self, de1);
    return cAtErrorNullPointer;
    }

uint32 ThaPdhDebuggerPrmChannelSelectGet(ThaPdhDebugger self)
    {
    if (self)
        return mMethodsGet(self)->PrmChannelSelectGet(self);
    return cBit31_0;
    }

eAtModulePdhRet ThaPdhDebuggerPrmMonitorEnable(ThaPdhDebugger self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PrmMonitorEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhDebuggerPrmMonitorIsEnabled(ThaPdhDebugger self)
    {
    if (self)
        return mMethodsGet(self)->PrmMonitorIsEnabled(self);
    return cAtFalse;
    }

eAtModulePdhRet ThaPdhDebuggerPrmForcedFullPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PrmForcedFullPrmPayload(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhDebuggerPrmIsForcedFullPrmPayload(ThaPdhDebugger self)
    {
    if (self)
        return mMethodsGet(self)->PrmIsForcedFullPrmPayload(self);
    return cAtFalse;
    }

eAtModulePdhRet ThaPdhDebuggerPrmForce8BitPrmPayload(ThaPdhDebugger self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PrmForce8BitPrmPayload(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaPdhDebuggerPrmIsForced8BitPrmPayload(ThaPdhDebugger self)
    {
    if (self)
        return mMethodsGet(self)->PrmIsForced8BitPrmPayload(self);
    return cAtFalse;
    }

eAtModulePdhRet ThaPdhDebuggerPrmForcePayLoad(ThaPdhDebugger self, uint32 payloadMap, eBool forced)
    {
    if (self)
        return mMethodsGet(self)->PrmForcePayLoad(self, payloadMap, forced);
    return cAtErrorNullPointer;
    }

uint32 ThaPdhDebuggerPrmIsForcedPayLoad(ThaPdhDebugger self, uint32 *payLoadValue)
    {
    if (self)
        return mMethodsGet(self)->PrmIsForcedPayLoad(self, payLoadValue);
    return cBit31_0;
    }

eAtModulePdhRet ThaPdhDebuggerPrmForceNmNi(ThaPdhDebugger self, uint32 payloadMap, uint8 swValue)
    {
    if (self)
        return mMethodsGet(self)->PrmForceNmNi(self, payloadMap, swValue);
    return cAtErrorNullPointer;
    }

uint8 ThaPdhDebuggerPrmUnForceNmNi(ThaPdhDebugger self)
    {
    if (self)
        return mMethodsGet(self)->PrmUnForceNmNi(self);
    return cBit7_0;
    }

eAtRet ThaPdhDebuggerDe1LiuLoopbackSet(ThaPdhDebugger self, AtPdhDe1 de1, uint8 loopbackMode)
    {
    if (self)
        return mMethodsGet(self)->De1LiuLoopbackSet(self, de1, loopbackMode);
    return cAtErrorNullPointer;
    }

uint8 ThaPdhDebuggerDe1LiuLoopbackGet(ThaPdhDebugger self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1LiuLoopbackGet(self, de1);
    return cAtLoopbackModeRelease;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDebuggerInternal.h
 * 
 * Created Date: Nov 12, 2015
 *
 * Description : Debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDEBUGGERINTERNAL_H_
#define _THAPDHDEBUGGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "ThaPdhDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mPrmDebugForcedPayload(value, nameMask, expectedVal, bitMask)\
       do                                                            \
         {                                                           \
           if (mRegField(value, nameMask) == expectedVal)            \
               {                                                     \
               hwPayLoadValue = hwPayLoadValue | bitMask;            \
               numberBit++;                                          \
               }                                                     \
         }while(0);

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDebuggerMethods
    {
    eAtModulePdhRet (*PrmChannelSelect)(ThaPdhDebugger self, AtPdhDe1 de1);
    uint32 (*PrmChannelSelectGet)(ThaPdhDebugger self);
    eAtModulePdhRet (*PrmMonitorEnable)(ThaPdhDebugger self, eBool enable);
    eBool (*PrmMonitorIsEnabled)(ThaPdhDebugger self);
    eAtModulePdhRet (*PrmForcedFullPrmPayload)(ThaPdhDebugger self, eBool enable);
    eBool (*PrmIsForcedFullPrmPayload)(ThaPdhDebugger self);
    eAtModulePdhRet (*PrmForce8BitPrmPayload)(ThaPdhDebugger self, eBool enable);
    eBool (*PrmIsForced8BitPrmPayload)(ThaPdhDebugger self);
    eAtModulePdhRet (*PrmForcePayLoad)(ThaPdhDebugger self, uint32 payloadMap, eBool forced);
    uint32 (*PrmIsForcedPayLoad)(ThaPdhDebugger self, uint32 *payLoadValue);
    eAtModulePdhRet (*PrmForceNmNi)(ThaPdhDebugger self, uint32 payloadMap, uint8 swValue);
    uint8 (*PrmUnForceNmNi)(ThaPdhDebugger self);
    eAtRet (*De1LiuLoopbackSet)(ThaPdhDebugger self, AtPdhDe1 de1, uint8 loopbackMode);
    uint8 (*De1LiuLoopbackGet)(ThaPdhDebugger self, AtPdhDe1 de1);
    }tThaPdhDebuggerMethods;

typedef struct tThaPdhDebugger
    {
    tAtObject super;
    const tThaPdhDebuggerMethods* methods;

    /* Private */
    ThaModulePdh pdhModule;
    }tThaPdhDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPdhDebugger ThaPdhDebuggerObjectInit(ThaPdhDebugger self, ThaModulePdh pdhModule);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDEBUGGERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhPrmInterruptManagerInternal.h
 * 
 * Created Date: Sep 19, 2017
 *
 * Description : Default PRM interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPRMINTERRUPTMANAGERINTERNAL_H_
#define _THAPDHPRMINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/intrcontroller/ThaInterruptManagerInternal.h"
#include "ThaPdhPrmInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPrmInterruptManager
    {
    tThaInterruptManager super;
    }tThaPdhPrmInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager ThaPdhPrmInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAPRMINTERRUPTMANAGERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhPrmInterruptManager.h
 * 
 * Created Date: Sep 19, 2017
 *
 * Description : Default PRM interrupt manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPRMINTERRUPTMANAGER_H_
#define _THAPDHPRMINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/man/interrupt/AtInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPrmInterruptManager * ThaPdhPrmInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager ThaPdhPrmInterruptManagerNew(AtModule module);

/* Concretes */
AtInterruptManager Tha60210011PdhPrmInterruptManagerNew(AtModule module);
AtInterruptManager Tha60210021PdhPrmInterruptManagerNew(AtModule module);
AtInterruptManager Tha60210012PdhPrmInterruptManagerNew(AtModule module);
AtInterruptManager Tha60210061PdhPrmInterruptManagerNew(AtModule module);
AtInterruptManager Tha60290022PdhPrmInterruptManagerNew(AtModule module);
AtInterruptManager Tha6A290021PdhPrmInterruptManagerNew(AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THAPRMINTERRUPTMANAGER_H_ */


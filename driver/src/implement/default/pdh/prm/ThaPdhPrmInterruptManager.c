/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhPrmInterruptManager.c
 *
 * Created Date: Sep 19, 2017
 *
 * Description : Default PRM interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../../generic/pdh/AtPdhPrmControllerInternal.h"
#include "../ThaModulePdh.h"
#include "ThaPdhPrmInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Default interrupt address/mask based on DS3 CEM */
#define cAf6_inten_status_PRMIntEnable_Mask     cBit24

#define cAf6Reg_prm_lb_sta_int_Base             0x1AFFF
#define cAf6Reg_prm_lb_en_int_Base              0x1AFFE
#define cAf6_prm_lb_en_int_prm_lben_int_Mask    cBit31_0

#define cAf6Reg_prm_lb_intsta_Base              0x1AC00

#define cAf6Reg_prm_cfg_lben_int_Base           0x1A000
#define cAf6Reg_prm_lb_int_sta_Base             0x1A400
#define cAf6Reg_prm_lb_int_crrsta_Base          0x1A800

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)             ((ThaPdhPrmInterruptManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePdh PdhModule(AtInterruptManager self)
    {
    return (ThaModulePdh)AtInterruptManagerModuleGet(self);
    }

/* Default interrupt process based on DS3 CEM */
static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    ThaInterruptManager manager = (ThaInterruptManager)self;
    ThaModulePdh pdhModule = PdhModule(self);
    uint32 baseAddress = ThaInterruptManagerBaseAddress(manager);
    uint32 numSlices = ThaInterruptManagerNumSlices(manager);
    uint32 numStsInSlice = ThaInterruptManagerNumStsInSlices(manager);
    uint32 intrStatus = AtHalRead(hal, (baseAddress + cAf6Reg_prm_lb_sta_int_Base));
    uint32 intrMask = AtHalRead(hal, (baseAddress + cAf6Reg_prm_lb_en_int_Base));
    uint8 de3;
    AtUnused(glbIntr);
    intrStatus &= intrMask;

    for (de3 = 0; de3 < numStsInSlice; de3++)
        {
        if (intrStatus & cIteratorMask(de3))
            {
            /* Get DE3 channel OR interrupt. */
            uint32 intrDe1Status = AtHalRead(hal, (baseAddress + (uint32)cAf6Reg_prm_lb_intsta_Base + de3));
            uint8 de28 = 0;
            uint8 de2, de1;

            for (de2 = 0; de2 < 7; de2++)
                {
                for (de1 = 0; de1 < 4; de1++)
                    {
                    /* Check for DS1/E1 channel OR interrupt. */
                    if (intrDe1Status & cIteratorMask(de28))
                        {
                        uint8 slice24;
                        for (slice24 = 0; slice24 < numSlices; slice24++)
                            {
                            AtPdhDe1 de1Channel = ThaModulePdhDe1ChannelFromHwIdGet(pdhModule, cAtModulePdh, slice24, de3, de2, de1);
                            AtPdhPrmController controller = AtPdhDe1PrmControllerGet(de1Channel);
                            AtPdhPrmControllerInterruptProcess(controller, slice24, de3, de2, de1, hal);
                            }
                        }
                    de28++;
                    }
                }
            }
        }
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    AtUnused(self);
    return 0x600000;
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_inten_status_PRMIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    AtModule module = AtInterruptManagerModuleGet((AtInterruptManager)self);
    uint32 regAddress = ThaInterruptManagerBaseAddress(self) + cAf6Reg_prm_lb_en_int_Base;
    AtModuleHwWrite(module, regAddress, (enable) ? cAf6_prm_lb_en_int_prm_lben_int_Mask : 0x0, hal);
    }

static uint32 NumSlices(ThaInterruptManager self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumStsInSlice(ThaInterruptManager self)
    {
    AtUnused(self);
    return 24;
    }

static uint32 InterruptStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_lb_int_sta_Base;
    }

static uint32 InterruptEnableRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_cfg_lben_int_Base;
    }

static uint32 CurrentStatusRegister(ThaInterruptManager self)
    {
    AtUnused(self);
    return cAf6Reg_prm_lb_int_crrsta_Base;
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        mMethodOverride(m_ThaInterruptManagerOverride, NumSlices);
        mMethodOverride(m_ThaInterruptManagerOverride, NumStsInSlice);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptStatusRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptEnableRegister);
        mMethodOverride(m_ThaInterruptManagerOverride, CurrentStatusRegister);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPrmInterruptManager);
    }

AtInterruptManager ThaPdhPrmInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager ThaPdhPrmInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPrmInterruptManagerObjectInit(newManager, module);
    }

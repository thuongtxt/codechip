/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhVcDe1Internal.h
 * 
 * Created Date: Feb 4, 2015
 *
 * Description : VC DS1/E1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHVCDE1INTERNAL_H_
#define _THAPDHVCDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaPdhVcDe1.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhVcDe1Methods
    {
    eBool (*FrameTypeIsSupportForVc1xMapping)(ThaPdhVcDe1 self, uint16 frameType);
    eAtRet (*VtMapModeSet)(ThaPdhVcDe1 self, uint16 frameType);
    eBool (*ShouldConstrainToVc1xMapping)(ThaPdhVcDe1 self);
    }tThaPdhVcDe1Methods;

typedef struct tThaPdhVcDe1
    {
    tThaPdhDe1 super;
    const tThaPdhVcDe1Methods *methods;
    } tThaPdhVcDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 ThaPdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHVCDE1INTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe2.h
 * 
 * Created Date: Feb 4, 2015
 *
 * Description : PDH DE2
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHDE2_H_
#define _THAPDHDE2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe2.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhDe2 * ThaPdhDe2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe2 ThaPdhDe2New(uint32 channelId, AtModulePdh module);

/* Product concretes */
AtPdhDe2 Tha6A290011PdhDe2New(uint32 channelId, AtModulePdh module);
AtPdhDe2 Tha6A290021PdhDe2New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60150011PdhDe2De1New(uint32 channelId, AtModulePdh module);
eAtRet ThaPdhDe2HwIdFactorGet(ThaPdhDe2 self, uint32 phyModule, uint32 *slice, uint32 *hwIdInSlice, uint32 *hwDe2);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHDE2_H_ */


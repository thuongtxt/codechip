/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : ThaPdhPrmController.c
 *
 * Created Date: Oct 27, 2015
 *
 * Description : PRM controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/ThaDevice.h"
#include "../../../generic/pktanalyzer/AtPktAnalyzerInternal.h"
#include "ThaPdhPrmControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaPdhPrmController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tThaPdhPrmControllerMethods m_methods;

/* Override */
static tAtPdhPrmControllerMethods m_AtPdhPrmControllerOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterOffset(ThaPdhPrmController self, eBool read2Clear)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhPrmControllerDe1Get((AtPdhPrmController)self);
    uint32 baseAddress = ThaModulePdhPrmBaseAddress((ThaModulePdh)AtChannelModuleGet((AtChannel)de1));
    uint32 offset  = baseAddress + mMethodsGet(de1)->Ds1E1J1DefaultOffset(de1);
    return (offset + mMethodsGet(self)->Read2ClearOffset(self, read2Clear));
    }

static uint32 CounterRead2Clear(AtPdhPrmController self, uint16 counterType, eBool read2Clear)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtPdhPrmControllerDe1Get(self);
    uint32 counterOffset = mMethodsGet(mThis(self))->CounterOffset(mThis(self), read2Clear);

    switch (counterType)
        {
        case cAtPdhDe1PrmCounterTxMessage       :
            return mChannelHwRead(de1, mMethodsGet(mThis(self))->TxMessageRegister(mThis(self)) + counterOffset, cAtModulePdh);
        case cAtPdhDe1PrmCounterTxByteMessage   :
            return mChannelHwRead(de1, mMethodsGet(mThis(self))->TxByteRegister(mThis(self)) + counterOffset, cAtModulePdh);
        case cAtPdhDe1PrmCounterRxGoodMessage   :
            return mChannelHwRead(de1, mMethodsGet(mThis(self))->RxGoodMessageRegister(mThis(self)) + counterOffset, cAtModulePdh);
        case cAtPdhDe1PrmCounterRxByteMessage   :
            return mChannelHwRead(de1, mMethodsGet(mThis(self))->RxByteRegister(mThis(self)) + counterOffset, cAtModulePdh);
        case cAtPdhDe1PrmCounterRxDiscardMessage:
            return mChannelHwRead(de1, mMethodsGet(mThis(self))->RxDiscardRegister(mThis(self)) + counterOffset, cAtModulePdh);
        case cAtPdhDe1PrmCounterRxMissMessage   :
            return mChannelHwRead(de1, mMethodsGet(mThis(self))->RxMissRegister(mThis(self)) + counterOffset, cAtModulePdh);
        default                                 :
            return 0;
        }
    }

static uint32 CounterGet(AtPdhPrmController self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPdhPrmController self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static uint32 TxMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return 0xDEADCAFE;
    }

static uint32 TxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return 0xDEADCAFE;
    }

static uint32 RxGoodMessageRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return 0xDEADCAFE;
    }

static uint32 RxByteRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return 0xDEADCAFE;
    }

static uint32 RxDiscardRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return 0xDEADCAFE;
    }

static uint32 RxMissRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return 0xDEADCAFE;
    }

static uint32 DefectHistoryClear(AtPdhPrmController self)
    {
    return mMethodsGet(mThis(self))->DefectHistoryReadToClear(mThis(self), cAtTrue);
    }

static uint32 DefectHistoryGet(AtPdhPrmController self)
    {
    return mMethodsGet(mThis(self))->DefectHistoryReadToClear(mThis(self), cAtFalse);
    }

static ThaInterruptManager InterruptManager(AtPdhPrmController self)
    {
    AtChannel de1 = (AtChannel)AtPdhPrmControllerDe1Get(self);
    return ThaModulePdhPrmInterruptManager((ThaModulePdh)AtChannelModuleGet(de1));
    }

static uint32 InterruptStatusRegister(AtPdhPrmController self)
    {
    ThaInterruptManager manager = InterruptManager(self);
    return ThaInterruptManagerBaseAddress(manager) +
           ThaInterruptManagerInterruptStatusRegister(manager);
    }

static uint32 InterruptEnableRegister(AtPdhPrmController self)
    {
    ThaInterruptManager manager = InterruptManager(self);
    return ThaInterruptManagerBaseAddress(manager) +
           ThaInterruptManagerInterruptEnableRegister(manager);
    }

static uint32 CurrentStatusRegister(AtPdhPrmController self)
    {
    ThaInterruptManager manager = InterruptManager(self);
    return ThaInterruptManagerBaseAddress(manager) +
           ThaInterruptManagerCurrentStatusRegister(manager);
    }

static ThaPdhDe1 De1Get(AtPdhPrmController self)
    {
    return (ThaPdhDe1)AtPdhPrmControllerDe1Get(self);
    }

static uint8 RxLBBitGet(AtPdhPrmController self)
    {
    uint8 sliceId = 0;
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get(self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self), &sliceId);
    uint32 regAddress = CurrentStatusRegister(self) + offset;
    uint32 regValue = mChannelHwRead(de1, regAddress, cAtModulePdh);
    uint32 channelMask = cBit0 << sliceId;
    return (regValue & channelMask) ? 1 : 0;
    }

static uint32 InterruptMaskGet(AtPdhPrmController self)
    {
    uint32 regVal, regAddr;
    uint8 sliceId = 0;
    ThaPdhDe1 de1 = De1Get(self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self), &sliceId);
    uint32 channelMask = cBit0 << sliceId;

    regAddr = InterruptEnableRegister(self) + offset;
    regVal = mChannelHwRead(de1, regAddr, cAtModulePdh);
    if (regVal & channelMask)
        return cAtPdhDs1AlarmPrmLBBitChange;

    return 0;
    }

static eAtModulePdhRet InterruptMaskSet(AtPdhPrmController self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regVal, regAddr;
    uint8 sliceId = 0;
    ThaPdhDe1 de1 = De1Get(self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self), &sliceId);
    uint32 channelMask = cBit0 << sliceId;

    regAddr = InterruptEnableRegister(self) + offset;
    regVal  = mChannelHwRead(de1, regAddr, cAtModulePdh);
    if (defectMask & cAtPdhDs1AlarmPrmLBBitChange)
        {
        if (enableMask & cAtPdhDs1AlarmPrmLBBitChange)
            regVal |= channelMask;
        else
            regVal &= (uint32)(~channelMask);
        }
    mChannelHwWrite(de1, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

static void InterruptProcess(AtPdhPrmController self, uint8 slice, uint8 de3Id, uint8 de2Id, uint8 de1Id, AtHal hal)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get(self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self), NULL);
    uint32 intrAddr = InterruptStatusRegister(self) + offset;
    uint32 intrMask = AtHalRead(hal, InterruptEnableRegister(self) + offset);
    uint32 intrStatus = AtHalRead(hal, intrAddr);
    uint32 events = 0;
    uint32 defects = 0;
    uint32 channelMask = cBit0 << slice;
    AtUnused(de3Id);
    AtUnused(de2Id);
    AtUnused(de1Id);

    intrStatus &= (channelMask & intrMask);

    if (intrStatus & channelMask)
        events |= cAtPdhDs1AlarmPrmLBBitChange;

    /* Do not notify listener when there is no events */
    if (!events)
        return;

    /* Clear interrupt */
    AtHalWrite(hal, intrAddr, intrStatus);

    AtChannelAllAlarmListenersCall((AtChannel)de1, events, defects);
    }

static uint32 DefectHistoryReadToClear(ThaPdhPrmController self, eBool r2c)
    {
    uint8 sliceId = 0;
    ThaPdhDe1 de1 = (ThaPdhDe1)De1Get((AtPdhPrmController)self);
    uint32 offset = mMethodsGet(mThis(self))->InterruptOffset(mThis(self), &sliceId);
    uint32 address = InterruptStatusRegister((AtPdhPrmController)self) + offset;
    uint32 regValue = mChannelHwRead(de1, address, cAtModulePdh);
    uint32 channelMask = cBit0 << sliceId;

    regValue &= channelMask;

    if (r2c)
        mChannelHwWrite(de1, address, regValue, cAtModulePdh);

    return (regValue) ?  cAtPdhDs1AlarmPrmLBBitChange : 0;
    }

static eAtModulePdhRet HwTxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    AtUnused(self);
    AtUnused(standard);
    /* Let concrete class do  */
    return cAtErrorNotImplemented;
    }

static eAtModulePdhRet HwRxStandardSet(ThaPdhPrmController self, uint8 standard)
    {
    AtUnused(self);
    AtUnused(standard);
    /* Let concrete class do  */
    return cAtErrorNotImplemented;
    }

static eAtModulePdhRet HwTxCRBitSet(ThaPdhPrmController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    /* Let concrete class do  */
    return cAtErrorNotImplemented;
    }

static eAtModulePdhRet HwTxLBBitSet(ThaPdhPrmController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    /* Let concrete class do  */
    return cAtErrorNotImplemented;
    }

static eAtModulePdhRet HwEnable(ThaPdhPrmController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eAtModulePdhRet TxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    uint8 hwStandard;

    if (standard == cAtPdhDe1PrmStandardUnknown)
        return cAtErrorInvlParm;

    hwStandard = (standard == cAtPdhDe1PrmStandardAnsi) ? 0 : 1;
    return mMethodsGet(mThis(self))->HwTxStandardSet(mThis(self), hwStandard);
    }

static eAtModulePdhRet RxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    uint8 hwStandard;

    if (standard == cAtPdhDe1PrmStandardUnknown)
        return cAtErrorInvlParm;

    hwStandard = (standard == cAtPdhDe1PrmStandardAnsi) ? 0 : 1;
    return mMethodsGet(mThis(self))->HwRxStandardSet(mThis(self), hwStandard);
    }

static eAtModulePdhRet TxCRBitSet(AtPdhPrmController self, uint8 value)
    {
    /* Only support 2 value 0 and 1 */
    if (value > 1)
        return cAtErrorInvlParm;

    return mMethodsGet(mThis(self))->HwTxCRBitSet(mThis(self), value);
    }

static eAtModulePdhRet TxLBBitSet(AtPdhPrmController self, uint8 value)
    {
    /* Only support 2 value 0 and 1 */
    if (value > 1)
        return cAtErrorInvlParm;

    return mMethodsGet(mThis(self))->HwTxLBBitSet(mThis(self), value);
    }

static eAtModulePdhRet CRBitExpectSet(AtPdhPrmController self, uint8 value)
    {
    /* Only support 2 value 0 and 1 */
    if (value > 1)
        return cAtErrorInvlParm;

    return mMethodsGet(mThis(self))->HwCRBitExpectSet(mThis(self), value);
    }

static eAtModulePdhRet StandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    eAtRet ret = cAtOk;

    ret |= AtPdhPrmControllerTxStandardSet(self, standard);
    ret |= AtPdhPrmControllerRxStandardSet(self, standard);

    return ret;
    }

static eAtPdhDe1PrmStandard StandardGet(AtPdhPrmController self)
    {
    return AtPdhPrmControllerTxStandardGet(self);
    }

static eAtModulePdhRet Enable(AtPdhPrmController self, eBool enable)
    {
    eAtModulePdhRet ret = mMethodsGet(mThis(self))->HwEnable(mThis(self), enable);

    /* Enable PRM need enable insert LB bit from CPU and revert to default when disable */
    if (enable)
        {
        AtPdhPrmControllerTxLBBitSet(self, 0);
        AtPdhPrmControllerTxLBBitEnable(self, cAtTrue);
        }
    else
        AtPdhPrmControllerTxLBBitEnable(self, cAtFalse);

    return ret;
    }

static eAtModulePdhRet HwCRBitExpectSet(ThaPdhPrmController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static ThaModulePdh ModulePdh(AtPdhPrmController self)
    {
    return (ThaModulePdh) AtChannelModuleGet((AtChannel) AtPdhPrmControllerDe1Get(self));
    }

static eAtModulePdhRet FcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order)
    {
    if (ThaModulePdhPrmFcsBitOrderIsSupported(ModulePdh(self)))
        return mMethodsGet(mThis(self))->HwFcsBitOrderSet(self, order);

    return (order == cAtBitOrderLsb) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtBitOrder FcsBitOrderGet(AtPdhPrmController self)
    {
    if (ThaModulePdhPrmFcsBitOrderIsSupported(ModulePdh(self)))
        return mMethodsGet(mThis(self))->HwFcsBitOrderGet(self);

    return cAtBitOrderLsb;
    }

static eAtModulePdhRet HwFcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order)
    {
    AtUnused(self);
    return (order == cAtBitOrderLsb) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtBitOrder HwFcsBitOrderGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtBitOrderLsb;
    }

static uint32 StartVersionHasRxEnabledCfgRegister(ThaPdhPrmController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static ThaVersionReader VersionReader(ThaPdhPrmController self)
    {
    AtPdhDe1 de1 = AtPdhPrmControllerDe1Get((AtPdhPrmController)self);
    AtDevice device = AtChannelDeviceGet((AtChannel)de1);
    return ThaDeviceVersionReader(device);
    }

static eBool HasRxEnabledCfgRegister(ThaPdhPrmController self)
    {
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 supportedVersion = mMethodsGet(self)->StartVersionHasRxEnabledCfgRegister(self);
    return (currentVersion >= supportedVersion) ? cAtTrue : cAtFalse;
    }

static uint32 Read2ClearOffset(ThaPdhPrmController self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? 0 : 64;
    }

static uint32 InterruptOffset(ThaPdhPrmController self, uint8 *sliceId)
    {
    AtUnused(self);
    if (sliceId)
        *sliceId = 0;
    return 0;
    }

static ThaModulePdh PdhModule(AtPdhPrmController self)
    {
    AtChannel de1 = (AtChannel)AtPdhPrmControllerDe1Get(self);
    return (ThaModulePdh)AtChannelModuleGet(de1);
    }

static AtPktAnalyzer Analyzer(AtPdhPrmController self)
    {
    AtPktAnalyzer analyzer = ThaModulePdhPrmAnalyzer(PdhModule(self));
    if (analyzer == NULL)
        return NULL;
    AtPktAnalyzerChannelSet(analyzer, (AtChannel)AtPdhPrmControllerDe1Get(self));
    return analyzer;
    }

static AtList CaptureRxMessages(AtPdhPrmController self)
    {
    AtPktAnalyzer analyzer = Analyzer(self);
    if (analyzer == NULL)
        return NULL;

    AtPktAnalyzerCaptureRxPackets(analyzer);
    return AtPktAnalyzerPacketListGet(analyzer);
    }

static eAtRet CaptureStart(AtPdhPrmController self)
    {
    return AtPktAnalyzerStart(Analyzer(self));
    }

static eAtRet CaptureStop(AtPdhPrmController self)
    {
    return AtPktAnalyzerStop(Analyzer(self));
    }

static eBool CaptureIsStarted(AtPdhPrmController self)
    {
    return AtPktAnalyzerIsStarted(Analyzer(self));
    }

static void OverrideAtPdhPrmController(AtPdhPrmController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhPrmControllerOverride, mMethodsGet(self), sizeof(m_AtPdhPrmControllerOverride));
        mMethodOverride(m_AtPdhPrmControllerOverride, CounterGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, CounterClear);
        mMethodOverride(m_AtPdhPrmControllerOverride, DefectHistoryClear);
        mMethodOverride(m_AtPdhPrmControllerOverride, DefectHistoryGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxStandardSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, RxStandardSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxCRBitSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, TxLBBitSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, CRBitExpectSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, StandardSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, StandardGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, Enable);
        mMethodOverride(m_AtPdhPrmControllerOverride, FcsBitOrderSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, FcsBitOrderGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, RxLBBitGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtPdhPrmControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtPdhPrmControllerOverride, InterruptProcess);
        mMethodOverride(m_AtPdhPrmControllerOverride, CaptureRxMessages);
        mMethodOverride(m_AtPdhPrmControllerOverride, CaptureStart);
        mMethodOverride(m_AtPdhPrmControllerOverride, CaptureStop);
        mMethodOverride(m_AtPdhPrmControllerOverride, CaptureIsStarted);
        }

    mMethodsSet(self, &m_AtPdhPrmControllerOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideAtPdhPrmController(self);
    }

static void MethodsInit(ThaPdhPrmController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CounterOffset);
        mMethodOverride(m_methods, DefectHistoryReadToClear);
        mMethodOverride(m_methods, TxMessageRegister);
        mMethodOverride(m_methods, TxByteRegister);
        mMethodOverride(m_methods, RxGoodMessageRegister);
        mMethodOverride(m_methods, RxByteRegister);
        mMethodOverride(m_methods, RxDiscardRegister);
        mMethodOverride(m_methods, RxMissRegister);
        mMethodOverride(m_methods, HwTxStandardSet);
        mMethodOverride(m_methods, HwRxStandardSet);
        mMethodOverride(m_methods, HwTxCRBitSet);
        mMethodOverride(m_methods, HwTxLBBitSet);
        mMethodOverride(m_methods, HwEnable);
        mMethodOverride(m_methods, HwCRBitExpectSet);
        mMethodOverride(m_methods, HwFcsBitOrderSet);
        mMethodOverride(m_methods, HwFcsBitOrderGet);
        mMethodOverride(m_methods, StartVersionHasRxEnabledCfgRegister);
        mMethodOverride(m_methods, Read2ClearOffset);
        mMethodOverride(m_methods, InterruptOffset);
        }

    mMethodsGet(self) = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPrmController);
    }

AtPdhPrmController ThaPdhPrmControllerObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhPrmControllerObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhPrmController ThaPdhPrmControllerNew(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhPrmController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaPdhPrmControllerObjectInit(newController, de1);
    }

eBool ThaPdhPrmControllerHasRxEnabledCfgRegister(ThaPdhPrmController self)
    {
    if (self)
        return HasRxEnabledCfgRegister(self);
    return cAtFalse;
    }



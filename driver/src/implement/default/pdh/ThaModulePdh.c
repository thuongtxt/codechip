/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtModulePdh.c
 *
 * Created Date: Aug 27, 2012
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/man/AtIpCoreInternal.h"
#include "../../../generic/prbs/AtModulePrbsInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDeviceReg.h"
#include "../man/ThaDeviceInternal.h"
#include "../man/ThaIpCoreInternal.h"
#include "../man/intrcontroller/ThaInterruptManager.h"
#include "../map/ThaModuleMap.h"
#include "../ocn/ThaModuleOcn.h"
#include "../prbs/ThaModulePrbs.h"
#include "prm/ThaPdhPrmInterruptManager.h"
#include "mdl/ThaPdhMdlInterruptManager.h"
#include "ThaModulePdhInternal.h"
#include "ThaPdhDe3Internal.h"
#include "ThaPdhDe1.h"
#include "ThaPdhDe2.h"
#include "ThaPdhDe3.h"
#include "ThaPdhVcDe1.h"
#include "ThaPdhNxDs0.h"
#include "ThaPdhDe3SerialLine.h"
#include "ThaModulePdhReg.h"
#include "ThaModulePdhForStmReg.h"
#include "ThaPdhMdlController.h"
#include "ThaPdhPrmController.h"
#include "ThaPdhPrmAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegInbandLoopcodeStatChangeDetection  0xE0000
#define cThaInbandLoopcodeStatChangeDetection_Mask cBit31
#define cThaInbandLoopcodeStatChangeDetection_Shift 31
#define cThaRegInbandLoopcodeDetection  0xE0001

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePdh)self)

#define mAbsoluteRegMake(self, method)                                         \
    do                                                                         \
        {                                                                      \
        if (self)                                                              \
            return mMethodsGet(self)->method(self) + cThaModulePdhOriginalBaseAddress; \
        return cInvalidUint32;                                                 \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eThaPdhInterruptManager
    {
    cThaPdhPrmInterruptManager = 0,
    cThaPdhMdlInterruptManager = 1
    }eThaPdhInterruptManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePdhMethods m_methods;

/* Override */
static tAtModuleMethods    m_AtModuleOverride;
static tAtObjectMethods    m_AtObjectOverride;
static tAtModulePdhMethods m_AtModulePdhOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsStmProduct(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceModuleIsSupported(device, cAtModuleSdh);
    }

/* This function is to delete PDH module */
static void Delete(AtObject self)
    {
    /* Delete private data */
    AtObjectDelete((AtObject)mThis(self)->debugger);
    mThis(self)->debugger = NULL;
    AtObjectDelete((AtObject)mThis(self)->prmAnalyzer);
    mThis(self)->prmAnalyzer = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x00700000) && (localAddress <= 0x7A0200))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 NumPart(ThaModulePdh self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cAtModulePdh);
    }

static eAtRet PartDefaultSet(ThaModulePdh self, uint8 partId)
    {
    mModuleHwWrite(self, cThaRegPdhGlbCtrl + ThaModulePdhPartOffset((AtModule)self, partId), cThaRegPdhGlbCtrlRstVal);
    return cAtOk;
    }

static eBool ShouldInvertLiuClock(ThaModulePdh self)
    {
    /* Let concrete product determine. Not all of them require this */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LiuClockInvertInit(ThaModulePdh self)
    {
    uint32 txInvertValue = ThaModulePdhTxLiuIsPositiveClockInvert((ThaModulePdh)self) ? cBit31_0 : 0;
    mModuleHwWrite(self, cThaRegPDHLIUTxClockInvertConfiguration, txInvertValue);
    mModuleHwWrite(self, cThaRegPDHLIURxClockInvertConfiguration, 0);
    return cAtOk;
    }

static eAtRet De2DefaultSet(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet DefaultSet(ThaModulePdh self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumPart(self); part_i++)
        ret |= PartDefaultSet(self, part_i);

    if (mMethodsGet(mThis(self))->ShouldInvertLiuClock(mThis(self)))
        ret |= LiuClockInvertInit(self);

    return ret;
    }

static eBool PrmShouldCompareCRBit(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet DefaultInbandLoopcodeThresholdSet(ThaModulePdh self)
    {
    static const uint8 cThresholdScaleFactor = 10;
    uint32 slice_i;
    uint32 defaultThres = mMethodsGet(self)->DefaultInbandLoopcodeThreshold(self) / cThresholdScaleFactor;

    for (slice_i = 0; slice_i < ThaModulePdhNumSlices(self); slice_i++)
        {
        uint32 baseAddr = ThaModulePdhSliceBase(self, slice_i);
        uint32 stateChangeDetectionRegAddrress = baseAddr + cThaRegInbandLoopcodeStatChangeDetection;
        uint32 regVal = mModuleHwRead(self, stateChangeDetectionRegAddrress);
        mRegFieldSet(regVal, cThaInbandLoopcodeStatChangeDetection_, 1);
        mModuleHwWrite(self, stateChangeDetectionRegAddrress, regVal);

        mModuleHwWrite(self, baseAddr + cThaRegInbandLoopcodeDetection, defaultThres);
        }

    return cAtOk;
    }

static eAtRet Init(AtModule self)
    {
    ThaModulePdh modulePdh = (ThaModulePdh)self;
    /* Super implementation */
	eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (ThaModulePdhPrmIsSupported((ThaModulePdh)self))
        AtModulePdhPrmCrCompareEnable((AtModulePdh)self, mMethodsGet(mThis(self))->PrmShouldCompareCRBit(mThis(self)));

    mMethodsGet(mThis(self))->De2DefaultSet(mThis(self));
    if (ThaModulePdhInbandLoopcodeIsSupported(modulePdh))
        DefaultInbandLoopcodeThresholdSet(modulePdh);

    return mMethodsGet(mThis(self))->DefaultSet(mThis(self));
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

/* This function is to get number of DS1/E1 channels */
static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
	AtUnused(self);
    return 16;
    }

static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
    return ThaPdhDe1New(de1Id, self);
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return ThaPdhVcDe1ObjectNew((AtSdhVc)sdhVc, self);
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId(sdhVc, cAtModulePdh, &slice, &hwSts);
    return ThaPdhDe3New(hwSts, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return ThaPdhDe2De1New(de1Id, self);
    }

static AtPdhDe2 De2Create(AtModulePdh self, uint32 de2Id)
    {
    return ThaPdhDe2New(de2Id, self);
    }

static AtPdhNxDS0 NxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap)
    {
	AtUnused(self);
    return ThaPdhNxDs0New(de1, timeslotBitmap);
    }

static void InterruptHwEnable(AtIpCore ipCore, eBool enable)
    {
    ThaIpCorePdhHwInterruptEnable((ThaIpCore)ipCore, enable);
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    uint8 i, numCore;
    AtIpCore ipCore;
    AtDevice device = AtModuleDeviceGet(self);

    numCore = AtDeviceNumIpCoresGet(device);

    for (i = 0; i < numCore; i++)
        {
        ipCore = AtDeviceIpCoreGet(device, i);

        /* If a product supports interrupt pin enable register, just simple
         * enable module interrupt on IPcore. Otherwise, we need to check if
         * IPcore interrupt was enabled before enable module interrupt. */
        if (ThaIpCoreCanEnableHwInterruptPin((ThaIpCore)ipCore) || AtIpCoreInterruptIsEnabled(ipCore))
            InterruptHwEnable(ipCore, enable);

        /* Store to DB */
        ((ThaModulePdh)self)->interruptIsEnabled = enable;
        }

    /* Calling super to enable further interrupt, e.g. PRM/MDL */
    return m_AtModuleMethods->InterruptEnable(self, enable);
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    return ((ThaModulePdh)self)->interruptIsEnabled;
    }

/* Enable interrupt on one core */
static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    if(InterruptIsEnabled(self))
        InterruptHwEnable(ipCore, enable);
    }

static void InterruptProcess(AtModule self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (hal == NULL)
        return;

    if (mMethodsGet(pdhModule)->HasDe3Interrupt(pdhModule, glbIntr, ipCore))
        {
        uint32 de3Intr = mMethodsGet(pdhModule)->De3InterruptStatusGet(pdhModule, glbIntr, hal);
        mMethodsGet(pdhModule)->De3InterruptProcess(pdhModule, de3Intr, hal);
        }

    if (mMethodsGet(pdhModule)->HasDe1Interrupt(pdhModule, glbIntr, ipCore))
        {
        uint32 de1Intr = mMethodsGet(pdhModule)->De1InterruptStatusGet(pdhModule, glbIntr, hal);
        mMethodsGet(pdhModule)->De1InterruptProcess(pdhModule, de1Intr, hal);
        }

    /* Calling super to process further interrupt, e.g. PRM/MDL */
    m_AtModuleMethods->InterruptProcess(self, glbIntr, ipCore);
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    if (managerIndex == cThaPdhPrmInterruptManager)
        {
        if (!ThaModulePdhPrmIsSupported(mThis(self)))
            return NULL;

        return mMethodsGet(mThis(self))->PrmInterruptManagerObjectCreate(mThis(self));
        }

    if (managerIndex == cThaPdhMdlInterruptManager)
        {
        if (!ThaModulePdhMdlIsSupported(mThis(self)))
            return NULL;

        return mMethodsGet(mThis(self))->MdlInterruptManagerObjectCreate(mThis(self));
        }

    return NULL;
    }

static AtInterruptManager PrmInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return ThaPdhPrmInterruptManagerNew((AtModule)self);
    }

static AtInterruptManager MdlInterruptManagerObjectCreate(ThaModulePdh self)
    {
    return ThaPdhMdlInterruptManagerNew((AtModule)self);
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModulePdhRegisterIteratorCreate(self);
    }

static eAtRet DefaultDe1Threshold(ThaModulePdh self, tThaCfgPdhDs1E1FrmRxThres *threshold)
    {
    threshold->aisThres    = 3;
    threshold->lofThres    = 2;
    threshold->losCntThres = mMethodsGet(self)->DetectDe1LosByAllZeroPattern(self) ? 3 : 0;
    threshold->raiThres    = 3;

    return cAtOk;
    }

static eBool TxDe1AutoAisSwAutoControl(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool TxDe1AutoAisByDefault(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool MFASChecked(ThaModulePdh self)
    {
	AtUnused(self);
    /* Basically, this feature should be enabled, but many products so far do not
     * enable this feature as default. So let it be disabled until customer
     * request this */
    return cAtFalse;
    }

static eBool DetectLosByAllZeroPattern(ThaModulePdh self)
    {
	AtUnused(self);
    /* Most of products at this moment do not use this feature, so disable it
     * as default and let concrete products determine whether they need or not.
     * But for EP, this should be enabled */
    if (ThaDeviceIsEp((ThaDevice)AtModuleDeviceGet((AtModule)self)))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanUseTimeslot16InNxDs0(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool CanUseTimeslot0InNxDs0(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool NeedCarryTimeslot16OnPw(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool CasSupported(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool NeedConfigureSignalingBufferPerTimeslot(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportEbitAlarm(ThaModulePdh self)
    {
	AtUnused(self);
    return cBit31_0;
    }

static eBool HasLiuLoopback(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 BpvErrorCounterRegister(ThaModulePdh self)
    {
    AtUnused(self);
    return cThaRegLiuBpvpErrCnt;
    }

static eBool CanForceDe1Los(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
    AtUnused(self);
    AtUnused(partId);
    return cAtFalse;
    }

static uint32 RegisterBase(ThaModulePdh self, uint32 absoluteAddress)
    {
    AtUnused(self);
    return absoluteAddress - cThaModulePdhOriginalBaseAddress;
    }

static uint32 De1RxFrmrPerChnCurrentAlmBase(ThaModulePdh self)
    {
    return RegisterBase(self, 0x755040);
    }

static uint32 De1RxFrmrPerChnInterruptAlmBase(ThaModulePdh self)
    {
    return RegisterBase(self, 0x755020);
    }

static uint32 DS3E3RxFrmrperChnIntrStatCtrlBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegDS3E3RxFrmrperChnIntrStatCtrl);
    }

static uint32 DS3E3RxFrmrperChnCurStatCtrlBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegDS3E3RxFrmrperChnCurStatCtrl);
    }

static uint32 DS1E1J1TxFrmrSignalingBufferRegister(ThaModulePdh self)
    {
    AtUnused(self);
    return cThaRegDS1E1J1TxFrmrSignalingBuffer;
    }

static AtPdhDe3 De3ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id)
    {
    AtUnused(self);
    AtUnused(phyModule);
    AtUnused(sliceId);
    AtUnused(de3Id);
    return NULL;
    }

static AtPdhDe1 De1ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id)
    {
    AtUnused(self);
    AtUnused(phyModule);
    AtUnused(sliceId);
    AtUnused(de3Id);
    AtUnused(de2Id);
    AtUnused(de1Id);
    return NULL;
    }

static AtModulePrbs PrbsModule(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    }

static AtPrbsEngine De1PrbsEngineCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtModulePrbs prbsModule = PrbsModule(self);

    if (AtModulePrbsAllChannelsSupported(prbsModule))
        return AtModulePrbsDe1PrbsEngineCreate(prbsModule, 0, de1);

    return NULL;
    }

static AtPrbsEngine De3PrbsEngineCreate(ThaModulePdh self, AtPdhDe3 de3)
    {
    AtModulePrbs prbsModule = PrbsModule(self);

    if (AtModulePrbsAllChannelsSupported(prbsModule))
        return AtModulePrbsDe3PrbsEngineCreate(prbsModule, 0, de3);

    return NULL;
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(ThaModulePdh self, AtPdhNxDS0 nxds0)
    {
    AtModulePrbs prbsModule = PrbsModule(self);

    if (AtModulePrbsAllChannelsSupported(prbsModule))
        return AtModulePrbsNxDs0PrbsEngineCreate(PrbsModule(self), 0, nxds0);

    return NULL;
    }

static int32 SliceOffset(ThaModulePdh self, uint32 sliceId)
    {
    AtUnused(self);
    AtUnused(sliceId);
    return 0;
    }

static uint32 SliceBase(ThaModulePdh self, uint32 sliceId)
    {
    AtUnused(self);
    AtUnused(sliceId);
    return cThaModulePdhOriginalBaseAddress;
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 NumStsInSlice(ThaModulePdh self)
    {
    AtUnused(self);
    return 24;
    }

static uint32 BaseAddress(ThaModulePdh self)
    {
    AtUnused(self);
    return cThaModulePdhOriginalBaseAddress;
    }

static ThaModulePdh ModulePdh(AtChannel channel)
    {
    return (ThaModulePdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cAtModulePdh);
    }

static void SdhChannelRegsShow(ThaModulePdh self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    /* Let subclass do their jobs */
    }

static void De3RegsShow(ThaModulePdh self, AtPdhChannel de3)
    {
    ThaPdhDe3 thade3 = (ThaPdhDe3)de3;
    uint32 offset = 0;
    uint32 address = 0;
    AtUnused(self);

    offset = mMethodsGet(thade3)->DefaultOffset(thade3);
    address = cThaRegRxM23E23Ctrl + offset;
    ThaDeviceRegNameDisplay("RxM23E23 Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    offset = mMethodsGet(thade3)->DefaultOffset(thade3);
    address = cThaRegTxM23E23Ctrl + offset;
    ThaDeviceRegNameDisplay("TxM23E23 Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);

    offset = mMethodsGet(thade3)->DefaultOffset(thade3);
    address = cThaRegRxDs3E3OHProCtrl + offset;
    ThaDeviceRegNameDisplay("RxDS3E3 OH Pro Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de3, address, cAtModulePdh);
    }

static void De2LayerRegsShow(ThaModulePdh self, AtPdhChannel de2)
    {
    uint32 address = cThaRegRxM12E12Ctrl + ThaModulePdhM12E12ControlOffset(self, (ThaPdhDe2)de2);

    ThaDeviceRegNameDisplay("RxM12E12 Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de2, address, cAtModulePdh);

    address = cThaRegTxM12E12Ctrl + ThaModulePdhM12E12ControlOffset(self, (ThaPdhDe2)de2);
    ThaDeviceRegNameDisplay("TxM12E12 Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de2, address, cAtModulePdh);
    }

static void De2RegsShow(ThaModulePdh self, AtPdhChannel de2)
    {
    AtPdhChannel de3 = AtPdhChannelParentChannelGet(de2);

    if (de3)
        mMethodsGet(self)->De3RegsShow(self, de3);

    De2LayerRegsShow(self, de2);
    }

static void De1LayerRegsShow(ThaModulePdh self, AtPdhChannel de1)
    {
    ThaPdhDe1 thade1 = (ThaPdhDe1)de1;
    uint32 offset = mMethodsGet(thade1)->Ds1E1J1DefaultOffset(thade1);
    uint32 address = cThaRegDS1E1J1RxFrmrCtrl + offset;
    AtUnused(self);

    ThaDeviceRegNameDisplay("DS1/E1/J1 Rx Framer Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de1, address, cAtModulePdh);

    address = cThaRegDS1E1J1TxFrmrCtrl + offset;
    ThaDeviceRegNameDisplay("DS1/E1/J1 Tx Framer Control");
    ThaDeviceChannelRegValueDisplay((AtChannel)de1, address, cAtModulePdh);
    }

static void De1RegsShow(ThaModulePdh self, AtPdhChannel de1)
    {
    AtPdhChannel de2 = AtPdhChannelParentChannelGet(de1);

    if (de2)
        De2RegsShow(self, de2);

    De1LayerRegsShow(self, de1);
    }

static void SdhChannelIdsShow(ThaModulePdh self, AtSdhChannel sdhChannel)
    {
    uint8 slice, hwSts;
    uint8 stsId = AtSdhChannelSts1Get(sdhChannel);
    uint8 numStsInSlice = ThaModulePdhNumStsInSlice(self);

    ThaSdhChannelHwStsGet(sdhChannel, cAtModulePdh, stsId, &slice, &hwSts);

    AtPrintc(cSevInfo,   "* SW-STS    Slice-%d    HW-STS(0-%d)\r\n", numStsInSlice, numStsInSlice - 1U);
    AtPrintc(cSevNormal, "  %3u       %3u         %u\r\n", stsId, slice, hwSts);
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo,   "* PDH registers of channel '%s':\r\n", AtObjectToString((AtObject)sdhChannel));
    }

static void PdhChannelIdsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    uint8 slice, hwSts;
    uint8 numStsInSlice = ThaModulePdhNumStsInSlice(self);

    ThaPdhChannelHwIdGet(pdhChannel, cAtModulePdh, &slice, &hwSts);

    AtPrintc(cSevInfo,   "* Slice-%d    HW-STS(0-%d)\r\n", numStsInSlice, numStsInSlice - 1U);
    AtPrintc(cSevNormal, "  %3u         %3u\r\n", slice, hwSts);
    AtPrintc(cSevNormal, "\r\n");
    }

static void PdhChannelRegsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(pdhChannel);

    if ((channelType == cAtPdhChannelTypeDs1) || (channelType == cAtPdhChannelTypeE1))
        {
        De1RegsShow(self, pdhChannel);
        return;
        }

    if ((channelType == cAtPdhChannelTypeDs2) || (channelType == cAtPdhChannelTypeE2))
        {
        De2RegsShow(self, pdhChannel);
        return;
        }

    if ((channelType == cAtPdhChannelTypeDs3) || (channelType == cAtPdhChannelTypeE3))
        {
        mMethodsGet(self)->De3RegsShow(self, pdhChannel);
        return;
        }
    }

static uint32 M12E12CounterOffset(ThaModulePdh self, ThaPdhDe2 de2, eBool r2c)
    {
    AtUnused(self);
    AtUnused(de2);
    AtUnused(r2c);
    return 0;
    }

static uint32 M12E12ControlOffset(ThaModulePdh self, ThaPdhDe2 de2)
    {
    uint8 slice, hwIdInSlice;
    int32 sliceOffset;
    AtPdhChannel de3 = AtPdhChannelParentChannelGet((AtPdhChannel)de2);

    ThaPdhChannelHwIdGet(de3, cAtModulePdh, &slice, &hwIdInSlice);
    sliceOffset = ThaModulePdhSliceOffset((ThaModulePdh)self, slice);

    return (uint32)((hwIdInSlice * 8) + (int32)AtChannelIdGet((AtChannel)de2) + sliceOffset);
    }

static eBool TxLiuIsPositiveClockInvert(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool  De1RxSlipBufferIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModulePdh object = (ThaModulePdh)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(interruptIsEnabled);
    mEncodeObjectDescription(debugger);
    mEncodeUInt(countersModule);
    mEncodeObject(prmAnalyzer);
    }

static eBool InbandLoopcodeIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 RecommendedPeriodInMs(AtModule self)
    {
    AtUnused(self);
    return 100;
    }

static AtPrbsEngine De1LinePrbsEngineCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtModulePrbs prbsModule = PrbsModule(self);
    return AtModulePrbsDe1LinePrbsEngineCreate(prbsModule, de1);
    }

static AtPrbsEngine De3LinePrbsEngineCreate(ThaModulePdh self, AtPdhDe3 de3)
    {
    AtModulePrbs prbsModule = PrbsModule(self);
    return AtModulePrbsDe3LinePrbsEngineCreate(prbsModule, de3);
    }

static uint32 De3CounterOffset(ThaModulePdh self, eBool r2c)
    {
    AtUnused(self);
    return (r2c) ? 0 : 16UL;
    }

static uint32 DiagLineBaseAddress(ThaModulePdh self, AtPdhChannel channel)
    {
    uint32 coreId = AtChannelIdGet((AtChannel)channel) / 24;
    AtUnused(self);
    return (coreId == 0) ? 0xF30000UL : 0xF40000UL;
    }

static uint32 DiagLineModeRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    return mMethodsGet(self)->DiagLineBaseAddress(self, channel) + 0x0;
    }

static uint32 DiagLineRemoteLoopbackRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    return mMethodsGet(self)->DiagLineBaseAddress(self, channel) + 0x1;
    }

static uint32 DiagLineTestEnableRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    return mMethodsGet(self)->DiagLineBaseAddress(self, channel) + 0x2;
    }

static uint32 DiagLineTestStatusRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    return mMethodsGet(self)->DiagLineBaseAddress(self, channel) + 0x5;
    }

static uint32 DiagLineTestErrorInsertRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    return mMethodsGet(self)->DiagLineBaseAddress(self, channel) + 0x4;
    }

static uint32 DiagLineRxClockInvertRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    return mMethodsGet(self)->DiagLineBaseAddress(self, channel) + 0x8;
    }

static uint32 DiagLineEc1ModeRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    return mMethodsGet(self)->DiagLineBaseAddress(self, channel) + 0x9;
    }

static uint32 DiagLineMask(ThaModulePdh self, AtPdhChannel channel)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)channel);
    AtUnused(self);

    return (cBit0 << ((lineId) % 24));
    }

static uint32 DiagLineShift(ThaModulePdh self, AtPdhChannel channel)
    {
    uint32 lineId = AtChannelIdGet((AtChannel)channel);
    AtUnused(self);

    return lineId % 24;
    }

static AtPdhMdlController MdlControllerCreate(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType)
    {
	AtUnused(self);
	return ThaPdhMdlControllerNew(de3, mdlType);
    }

static eBool HasDe3Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(ipCore);
    return cAtFalse;
    }

static eBool HasDe1Interrupt(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(ipCore);
    return cAtFalse;
    }

static uint32 De3InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    return 0;
    }

static uint32 De1InterruptStatusGet(ThaModulePdh self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    return 0;
    }

static void De3InterruptProcess(ThaModulePdh self, uint32 de3Intr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(de3Intr);
    AtUnused(hal);
    }

static void De1InterruptProcess(ThaModulePdh self, uint32 de1Intr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(de1Intr);
    AtUnused(hal);
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportBom(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartVersionSupportDe1SsmSendingReadyIndication(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartVersionSupportDe1Ssm(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartVersionSupportDe3Ssm(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartVersionSupportDe3FramedSatop(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool De3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportDe3Disabling(ThaModulePdh self)
    {
    /* Concrete must know. So far, almost products do not support */
    AtUnused(self);
    return cBit31_0;
    }

static eBool De3TxLiuEnableIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 StartVersionSupportDe1LomfConsequentialAction(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool ForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool LiuForcedAisIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanDisableDe3(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 versionNumber = AtDeviceVersionNumber(device);
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;
    return (versionNumber >= mMethodsGet(self)->StartVersionSupportDe3Disabling(self)) ? cAtTrue : cAtFalse;
    }

static eBool RxHw3BitFeacSignalIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPdhPrmController PrmControllerObjectCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    AtUnused(self);
    return ThaPdhPrmControllerNew(de1);
    }

static ThaPdhDebugger DebuggerObjectCreate(ThaModulePdh self)
    {
    return ThaPdhDebuggerNew(self);
    }

static ThaPdhDebugger Debugger(ThaModulePdh self)
    {
    if (mThis(self)->debugger == NULL)
        mThis(self)->debugger = mMethodsGet(self)->DebuggerObjectCreate(self);
    return mThis(self)->debugger;
    }

static eBool DetectDe1LosByAllZeroPattern(ThaModulePdh self)
    {
    return DetectLosByAllZeroPattern(self);
    }

static eBool DetectDe3LosByAllZeroPattern(ThaModulePdh self)
    {
    return DetectLosByAllZeroPattern(self);
    }

static uint32 StartVersionSupportAlarmForwardingStatus(ThaModulePdh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint8 DefaultDe3JitterStuffingMode(ThaModulePdh self)
    {
    /* Fine stuffing mode. IMPORTANT: the HW RD says mode 8, but indeed it is
     * invalid value as confirmed by HW team.
     *
     * For more detail:
     *
     * - Mode 1 (which is corresponding to hardware value 1): The allowed stuff
     *   frequency is +/-500ppm, that means the frequency of DS3 and STS1 can be
     *   up to +/-500ppm. The weakness of this mode is that if the two systems
     *   are not much difference in frequency, some time a burst of stuffing may
     *   happen. Many ADMs cannot accept this burst and data got error. The reason
     *   we provide this mode is for some private boxes in some countries using
     *   E3 mapping to adapt big frequency difference. In our design E3 and DS3
     *   are sharing the same engine so this mode also apply to DS3.
     *
     * - Mode 2 (which is corresponding to hardware value 9): The allowed stuff
     *   frequency is +/-170ppm. That means the stuff burst can be reduced and
     *   help to prevent data error at the ADM. In most applications, it is better
     *   to use Mode 2. For applications do not have much frequency difference
     *   between DS3 and STS1, the design needs to be modified to make the stuff
     *   frequency as small as possible. The frequency difference in many applications
     *   should be +/- 50ppm and the design can be modifed to match with this
     *   difference. */
    AtUnused(self);
    return 9;
    }

static AtErrorGenerator De1TxErrorGeneratorGet(ThaModulePdh self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator De1RxErrorGeneratorGet(ThaModulePdh self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator De3TxErrorGeneratorGet(ThaModulePdh self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator De3RxErrorGeneratorGet(ThaModulePdh self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 StartVersionFixDs3AisInterrupt(ThaModulePdh self)
    {
    /* The issue is, when the remote side force AIS and the local side is boot,
     * after booting, the DE3 AIS is report as clear. And most of old versions
     * have this issue. */
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 De1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cInvalidUint32;
    }

static uint32 PrmStsAndTug2Offset(ThaModulePdh self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool PrmFcsBitOrderIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 ErrorGeneratorBerDivCoefficient(ThaModulePdh self)
    {
    AtUnused(self);
    return 1;
    }

static eBool ShouldDetectAisAllOnesWithUnframedMode(ThaModulePdh self)
    {
    /* As Default Implement */
    AtUnused(self);
    return cAtTrue;
    }

static AtObjectAny De1AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return NULL;
    }

static AtObjectAny De2AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return NULL;
    }

static AtObjectAny De3AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return NULL;
    }

static eBool DebugCountersModuleIsSupported(ThaModulePdh self, uint32 module)
    {
    AtUnused(self);
    if ((module == cAtModulePdh) || (module == cThaModulePmc))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DebugCountersModuleSet(ThaModulePdh self, uint32 module)
    {
    if (DebugCountersModuleIsSupported(self, module))
        {
        self->countersModule = module;
        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static void CounterModuleSetup(AtModule self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    mThis(self)->countersModule = ThaDeviceDefaultCounterModule(device, cAtModulePdh);
    }

static eAtRet Setup(AtModule self)
    {
    CounterModuleSetup(self);
    return m_AtModuleMethods->Setup(self);
    }

static eBool De1LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    /* Should declare this for required products */
    AtUnused(self);
    return cAtFalse;
    }

static eBool De3LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    /* Should declare this for required products */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 RxPerStsVCPldCtrlBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegPdhRxPerStsVCPldCtrl);
    }

static uint32 RxM12E12MuxBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM12E12Mux);
    }

static uint32 DS3E3RxFrmrperChnIntrEnCtrlBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegDS3E3RxFrmrperChnIntrEnCtrl);
    }

static uint32 RxM12E12FrmChnIntrEnCtrlBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM12E12FrmChnIntrEnCtrl);
    }

static uint32 RxM12E12FrmChnIntrChngStatusBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM12E12FrmChnIntrChngStatus);
    }

static uint32 RxM12E12FrmChnAlarmCurStatusBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM12E12FrmChnAlarmCurStatus);
    }

static uint32 DS1E1J1RxFrmrCrcErrCntBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegDS1E1J1RxFrmrCrcErrCnt);
    }

static uint32 DS1E1J1RxFrmrReiCntBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegDS1E1J1RxFrmrReiCnt);
    }

static uint32 DS1E1J1RxFrmrFBECntBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegDS1E1J1RxFrmrFBECnt);
    }

static uint32 DS1E1J1RxFrmrperChnIntrEnCtrlBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegDS1E1J1RxFrmrperChnIntrEnCtrl);
    }

static uint32 RxM23E23FrmrReiCntBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM23E23FrmrReiCntBase);
    }

static uint32 RxM23E23FrmrFBECntBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM23E23FrmrFBECntBase);
    }

static uint32 RxM23E23FrmrParCntBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM23E23FrmrParCntBase);
    }

static uint32 RxM23E23FrmrCbitParCntBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxM23E23FrmrCbitParCntBase);
    }

static uint32 RxDs3E3OHProCtrlBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaRegRxDs3E3OHProCtrl);
    }

static uint32 RxDe3FeacBufferBase(ThaModulePdh self)
    {
    return RegisterBase(self, cThaReg_rx_de3_feac_buffer);
    }

static uint32 De1RxFramerHwStatusBase(ThaModulePdh self)
    {
    return RegisterBase(self, 0x754400);
    }

static uint32 Rxm23e23ChnIntrCfgBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x000405FE;
    }

static eBool RegExist(ThaModulePdh self, uint32 addressBase)
    {
    AtUnused(self);
    AtUnused(addressBase);
    return cAtTrue;
    }

static eAtRet De1InterruptEnable(ThaModulePdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    /* To enable global PDH DS1/E1 interrupts. */
    return cAtOk;
    }

static eAtRet De3InterruptEnable(ThaModulePdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    /* To enable global PDH DS3/E3 interrupts. */
    return cAtOk;
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret;
    AtModule counterModule;

    ret = m_AtModuleMethods->Debug(self);

    /* Display counter module */
    counterModule = AtDeviceModuleGet(AtModuleDeviceGet(self), mThis(self)->countersModule);
    AtPrintc(cSevNormal, "* Counter module: %s\r\n", AtModuleTypeString(counterModule));

    return ret;
    }

static eBool CanHotChangeFrameType(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool De1SsmIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionSupportDe1Ssm(self);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool PrmIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaVersionReader reader = ThaDeviceVersionReader(device);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(reader);
    return (version >= mMethodsGet(self)->StartVersionSupportPrm(self)) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportDe1IdleCodeInsertion(ThaModulePdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static ThaVersionReader VersionReader(ThaModulePdh self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool De1IdleCodeInsertionIsSupported(ThaModulePdh self)
    {
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportDe1IdleCodeInsertion(self);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static AtPktAnalyzer PrmAnalyzerObjectCreate(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaPdhPrmAnalyzerNew(NULL);
    }

static AtPktAnalyzer PrmAnalyzer(ThaModulePdh self)
    {
    if (self->prmAnalyzer == NULL)
        self->prmAnalyzer = mMethodsGet(self)->PrmAnalyzerObjectCreate(self);
    return self->prmAnalyzer;
    }

static eAtRet HoVcUnChannelize(ThaModulePdh self, AtSdhChannel hoVc)
    {
    AtUnused(self);
    AtUnused(hoVc);
    return cAtOk;
    }

static eAtRet HoVcChannelizeRestore(ThaModulePdh self, AtSdhChannel hoVc)
    {
    AtUnused(self);
    AtUnused(hoVc);
    return cAtOk;
    }

static eBool ShouldReportBerViaBerController(ThaModulePdh self)
    {
    AtUnused(self);
    /* Default BER SD/SF/TCA defects are reported within DE3/DE1 channel register. */
    return cAtFalse;
    }

static uint32 TimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    uint8 slice, hwIdInSlice, vtg, vt;
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)de1);
    uint32 partOffset = ThaDeviceModulePartOffset(device, cAtModulePdh, ThaPdhDe1PartId(de1));
    int32 sliceOffset;
    ThaPdhDe1HwIdGet(de1, cAtModulePdh, &slice, &hwIdInSlice, &vtg, &vt);
    sliceOffset = ThaModulePdhSliceOffset(self, slice);
    return (uint32)((1024 * hwIdInSlice) + (128 * vtg) + (32 * vt) + (int32)partOffset + sliceOffset);
    }

static uint32 DefaultInbandLoopcodeThreshold(ThaModulePdh self)
    {
    AtUnused(self);
    /* Let sub-class determine the default threshold. */
    return 0;
    }

static uint32 RxSlipBufferCounterRoOffset(ThaModulePdh self, eBool r2c)
    {
    AtUnused(self);
    if (r2c)
        return 0;
    return 1024;
    }

static eBool TxRetimingSignalingIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldRejectTxChangeWhenChannelBoundPw(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool De1FastSearchIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPdhChannel VcDe3ObjectGet(ThaModulePdh self, AtSdhChannel vc3)
    {
    AtUnused(self);
    AtUnused(vc3);
    return NULL;
    }

static void OverrideAtModulePdh(ThaModulePdh self)
    {
    AtModulePdh module = (AtModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(module), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, NumberOfDe1sGet);
        mMethodOverride(m_AtModulePdhOverride, De1Create);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, De2Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, NxDs0Create);
        }

    mMethodsSet(module, &m_AtModulePdhOverride);
    }

static void OverrideAtModule(ThaModulePdh self)
    {
    AtModule object = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, InterruptEnable);
        mMethodOverride(m_AtModuleOverride, InterruptIsEnabled);
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, InterruptOnIpCoreEnable);
        mMethodOverride(m_AtModuleOverride, InterruptProcess);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, RecommendedPeriodInMs);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(object, &m_AtModuleOverride);
    }

static void OverrideAtObject(ThaModulePdh self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtObject pdhObject = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(pdhObject);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(pdhObject, &m_AtObjectOverride);
    }

static void Override(ThaModulePdh self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    }

static void MethodsInit(ThaModulePdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultDe1Threshold);
        mMethodOverride(m_methods, TxDe1AutoAisByDefault);
        mMethodOverride(m_methods, TxDe1AutoAisSwAutoControl);
        mMethodOverride(m_methods, DetectDe1LosByAllZeroPattern);
        mMethodOverride(m_methods, DetectDe3LosByAllZeroPattern);
        mMethodOverride(m_methods, MFASChecked);
        mMethodOverride(m_methods, CanUseTimeslot16InNxDs0);
        mMethodOverride(m_methods, CanUseTimeslot0InNxDs0);
        mMethodOverride(m_methods, NeedCarryTimeslot16OnPw);
        mMethodOverride(m_methods, CasSupported);
        mMethodOverride(m_methods, NeedConfigureSignalingBufferPerTimeslot);
        mMethodOverride(m_methods, HasLiuLoopback);
        mMethodOverride(m_methods, StartVersionSupportEbitAlarm);
        mMethodOverride(m_methods, BpvErrorCounterRegister);
        mMethodOverride(m_methods, ShouldInvertLiuClock);
        mMethodOverride(m_methods, CanForceDe1Los);
        mMethodOverride(m_methods, HasM13);
        mMethodOverride(m_methods, De1RxFrmrPerChnCurrentAlmBase);
        mMethodOverride(m_methods, De1RxFrmrPerChnInterruptAlmBase);
        mMethodOverride(m_methods, DS3E3RxFrmrperChnIntrStatCtrlBase);
        mMethodOverride(m_methods, DS3E3RxFrmrperChnCurStatCtrlBase);
        mMethodOverride(m_methods, DS1E1J1TxFrmrSignalingBufferRegister);
        mMethodOverride(m_methods, De3ChannelFromHwIdGet);
        mMethodOverride(m_methods, De1ChannelFromHwIdGet);
        mMethodOverride(m_methods, De1PrbsEngineCreate);
        mMethodOverride(m_methods, De3PrbsEngineCreate);
        mMethodOverride(m_methods, NxDs0PrbsEngineCreate);
        mMethodOverride(m_methods, SliceOffset);
        mMethodOverride(m_methods, SliceBase);
        mMethodOverride(m_methods, SdhChannelRegsShow);
        mMethodOverride(m_methods, PdhChannelRegsShow);
        mMethodOverride(m_methods, De3RegsShow);
        mMethodOverride(m_methods, M12E12CounterOffset);
        mMethodOverride(m_methods, M12E12ControlOffset);
        mMethodOverride(m_methods, NumSlices);
        mMethodOverride(m_methods, NumStsInSlice);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, De2DefaultSet);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, TxLiuIsPositiveClockInvert);
        mMethodOverride(m_methods, De3CounterOffset);
        mMethodOverride(m_methods, DiagLineTestEnableRegister);
        mMethodOverride(m_methods, DiagLineTestStatusRegister);
        mMethodOverride(m_methods, DiagLineTestErrorInsertRegister);
        mMethodOverride(m_methods, DiagLineModeRegister);
        mMethodOverride(m_methods, DiagLineRemoteLoopbackRegister);
        mMethodOverride(m_methods, DiagLineRxClockInvertRegister);
        mMethodOverride(m_methods, DiagLineEc1ModeRegister);
        mMethodOverride(m_methods, DiagLineMask);
        mMethodOverride(m_methods, DiagLineShift);
        mMethodOverride(m_methods, DiagLineBaseAddress);
        mMethodOverride(m_methods, MdlControllerCreate);
        mMethodOverride(m_methods, HasDe3Interrupt);
        mMethodOverride(m_methods, HasDe1Interrupt);
        mMethodOverride(m_methods, De3InterruptStatusGet);
        mMethodOverride(m_methods, De1InterruptStatusGet);
        mMethodOverride(m_methods, De3InterruptProcess);
        mMethodOverride(m_methods, De1InterruptProcess);
        mMethodOverride(m_methods, MdlIsSupported);
        mMethodOverride(m_methods, StartVersionSupportPrm);
        mMethodOverride(m_methods, StartVersionSupportBom);
        mMethodOverride(m_methods, InbandLoopcodeIsSupported);
        mMethodOverride(m_methods, StartVersionSupportDe1Ssm);
        mMethodOverride(m_methods, StartVersionSupportDe3Ssm);
        mMethodOverride(m_methods, StartVersionSupportDe3FramedSatop);
        mMethodOverride(m_methods, De3FramedSatopIsSupported);
        mMethodOverride(m_methods, De3AisSelectablePatternIsSupported);
        mMethodOverride(m_methods, StartVersionSupportDe1SsmSendingReadyIndication);
        mMethodOverride(m_methods, StartVersionSupportDe3Disabling);
        mMethodOverride(m_methods, De3TxLiuEnableIsSupported);
        mMethodOverride(m_methods, StartVersionSupportAisDownStream);
        mMethodOverride(m_methods, StartVersionFixDs3AisInterrupt);
        mMethodOverride(m_methods, ForcedAisCorrectionIsSupported);
        mMethodOverride(m_methods, LiuForcedAisIsSupported);
        mMethodOverride(m_methods, PrmShouldCompareCRBit);
        mMethodOverride(m_methods, PrmControllerObjectCreate);
        mMethodOverride(m_methods, DebuggerObjectCreate);
        mMethodOverride(m_methods, StartVersionSupportAlarmForwardingStatus);
        mMethodOverride(m_methods, StartVersionSupportDe1LomfConsequentialAction);
        mMethodOverride(m_methods, DefaultDe3JitterStuffingMode);
        mMethodOverride(m_methods, De1TxErrorGeneratorGet);
        mMethodOverride(m_methods, De1RxErrorGeneratorGet);
        mMethodOverride(m_methods, De3TxErrorGeneratorGet);
        mMethodOverride(m_methods, De3RxErrorGeneratorGet);
        mMethodOverride(m_methods, De1PrmOffset);
        mMethodOverride(m_methods, PrmStsAndTug2Offset);
        mMethodOverride(m_methods, PrmFcsBitOrderIsSupported);
        mMethodOverride(m_methods, ErrorGeneratorBerDivCoefficient);
        mMethodOverride(m_methods, ShouldDetectAisAllOnesWithUnframedMode);
        mMethodOverride(m_methods, De1AttControllerCreate);
        mMethodOverride(m_methods, De2AttControllerCreate);
        mMethodOverride(m_methods, De3AttControllerCreate);
        mMethodOverride(m_methods, DebugCountersModuleSet);
        mMethodOverride(m_methods, De1LiuShouldReportLofWhenLos);
        mMethodOverride(m_methods, De3LiuShouldReportLofWhenLos);
        mMethodOverride(m_methods, RxPerStsVCPldCtrlBase);
        mMethodOverride(m_methods, RxM12E12MuxBase);
        mMethodOverride(m_methods, DS3E3RxFrmrperChnIntrEnCtrlBase);
        mMethodOverride(m_methods, RxM12E12FrmChnIntrEnCtrlBase);
        mMethodOverride(m_methods, RxM12E12FrmChnIntrChngStatusBase);
        mMethodOverride(m_methods, RxM12E12FrmChnAlarmCurStatusBase);
        mMethodOverride(m_methods, DS1E1J1RxFrmrCrcErrCntBase);
        mMethodOverride(m_methods, DS1E1J1RxFrmrReiCntBase);
        mMethodOverride(m_methods, DS1E1J1RxFrmrFBECntBase);
        mMethodOverride(m_methods, DS1E1J1RxFrmrperChnIntrEnCtrlBase);
        mMethodOverride(m_methods, RxM23E23FrmrReiCntBase);
        mMethodOverride(m_methods, RxM23E23FrmrFBECntBase);
        mMethodOverride(m_methods, RxM23E23FrmrParCntBase);
        mMethodOverride(m_methods, RxM23E23FrmrCbitParCntBase);
        mMethodOverride(m_methods, RxDs3E3OHProCtrlBase);
        mMethodOverride(m_methods, RxDe3FeacBufferBase);
        mMethodOverride(m_methods, De1RxFramerHwStatusBase);
        mMethodOverride(m_methods, Rxm23e23ChnIntrCfgBase);
        mMethodOverride(m_methods, RegExist);
        mMethodOverride(m_methods, De1InterruptEnable);
        mMethodOverride(m_methods, De3InterruptEnable);
        mMethodOverride(m_methods, RxHw3BitFeacSignalIsSupported);
        mMethodOverride(m_methods, CanHotChangeFrameType);
        mMethodOverride(m_methods, PrmIsSupported);
        mMethodOverride(m_methods, StartVersionSupportDe1IdleCodeInsertion);
        mMethodOverride(m_methods, PrmInterruptManagerObjectCreate);
        mMethodOverride(m_methods, MdlInterruptManagerObjectCreate);
        mMethodOverride(m_methods, PrmAnalyzerObjectCreate);
        mMethodOverride(m_methods, HoVcUnChannelize);
        mMethodOverride(m_methods, HoVcChannelizeRestore);
        mMethodOverride(m_methods, ShouldReportBerViaBerController);
        mMethodOverride(m_methods, TimeslotDefaultOffset);
        mMethodOverride(m_methods, De1RxSlipBufferIsSupported);
        mMethodOverride(m_methods, DefaultInbandLoopcodeThreshold);
        mMethodOverride(m_methods, RxSlipBufferCounterRoOffset);
        mMethodOverride(m_methods, De3TxLiuEnableIsSupported);
        mMethodOverride(m_methods, TxRetimingSignalingIsSupported);
        mMethodOverride(m_methods, ShouldRejectTxChangeWhenChannelBoundPw);
        mMethodOverride(m_methods, VcDe3ObjectGet);
        mMethodOverride(m_methods, De1FastSearchIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

AtModulePdh ThaModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModulePdh));

    /* Super constructor */
    if (AtModulePdhObjectInit((AtModulePdh)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaModulePdh)self);
    MethodsInit((ThaModulePdh)self);
    m_methodsInit = 1;

    /* Set default for counter mode */
    mThis(self)->countersModule = cAtModulePdh;

    return self;
    }

AtModulePdh ThaModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModulePdh));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePdhObjectInit(newModule, device);
    }

uint32 ThaModulePdhPartOffset(AtModule self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet(self);
    return ThaDeviceModulePartOffset(device, cAtModulePdh, partId);
    }

eAtRet ThaModulePdhDefaultDe1Threshold(ThaModulePdh self, tThaCfgPdhDs1E1FrmRxThres *threshold)
    {
    if (self)
        return mMethodsGet(self)->DefaultDe1Threshold(self, threshold);

    return cAtErrorNullPointer;
    }

uint32 ThaModulePdhTxDE1SigDS1Mask(ThaModulePdh self)
    {
    return IsStmProduct(self) ? cBit9 : cBit5;
    }

uint32 ThaModulePdhTxDE1SigDS1Shift(ThaModulePdh self)
    {
    return IsStmProduct(self) ? 9 : 5;
    }

uint32 ThaModulePdhTxDE1SigLineIDMask(ThaModulePdh self)
    {
    return IsStmProduct(self) ? cBit8_0 : cBit4_0;
    }

uint32 ThaModulePdhTxDE1SigLineIDShift(ThaModulePdh self)
    {
	AtUnused(self);
    return 0;
    }

eBool ThaModulePdhTxDe1AutoAisSwAutoControl(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->TxDe1AutoAisSwAutoControl(self);

    return cAtFalse;
    }

eBool ThaModulePdhTxDe1AutoAisByDefault(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->TxDe1AutoAisByDefault(self);

    return cAtFalse;
    }

void ThaModulePdhVc4_4cConfigurationClear(ThaModulePdh self, AtSdhChannel sdhChannel, uint8 stsId)
    {
    ThaStmModulePdh pdhModule = (ThaStmModulePdh)self;

    uint32 offset  = mMethodsGet(pdhModule)->DefaultOffset(pdhModule, sdhChannel, stsId, cThaPdhNotCare, cThaPdhNotCare);
    uint32 regAddr = cThaRegStsVtDmapCtrl + offset;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    if (mRegField(regVal, cThaStsVtDemapSigtype) == 0xA)
        mRegFieldSet(regVal, cThaStsVtDemapSigtype, 0x9);

    mModuleHwWrite(self, regAddr, regVal);
    }

eBool ThaModulePdhDe1MFASChecked(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->MFASChecked(self);
    return cAtFalse;
    }

eBool ThaModulePdhCanUseTimeslot16InNxDs0(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->CanUseTimeslot16InNxDs0(self);
    return cAtFalse;
    }

eBool ThaModulePdhCanUseTimeslot0InNxDs0(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->CanUseTimeslot0InNxDs0(self);
    return cAtFalse;
    }

eBool ThaModulePdhNeedCarryTimeslot16OnPw(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->NeedCarryTimeslot16OnPw(self);
    return cAtFalse;
    }

eBool ThaModulePdhCasSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->CasSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhNeedConfigureSignalingBufferPerTimeslot(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->NeedConfigureSignalingBufferPerTimeslot(self);
    return cAtFalse;
    }

eBool ThaModulePdhEbitAlarmSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportEbitAlarm(self);
    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePdhHasLiuLoopback(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->HasLiuLoopback(self);
    return cAtFalse;
    }

uint32 ThaModulePdhBpvErrorCounterRegister(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->BpvErrorCounterRegister(self);
    return cBit31_0;
    }

uint32 ThaModulePdhDS3E3RxFrmrperChnIntrEnCtrlRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, DS3E3RxFrmrperChnIntrEnCtrlBase);
    }

uint32 ThaModulePdhDS3E3RxFrmrperChnIntrStatCtrlRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, DS3E3RxFrmrperChnIntrStatCtrlBase);
    }

uint32 ThaModulePdhDS3E3RxFrmrperChnCurStatCtrlRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, DS3E3RxFrmrperChnCurStatCtrlBase);
    }

eBool ThaModulePdhCanForceDe1Los(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->CanForceDe1Los(self);
    return cAtFalse;
    }

eBool ThaModulePdhHasM13(ThaModulePdh self, uint8 partId)
    {
    if (self)
        return mMethodsGet(self)->HasM13(self, partId);
    return cAtFalse;
    }

uint32 ThaModulePdhDe1RxFrmrPerChnCurrentAlmRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, De1RxFrmrPerChnCurrentAlmBase);
    }

uint32 ThaModulePdhDe1RxFrmrPerChnInterruptAlmRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, De1RxFrmrPerChnInterruptAlmBase);
    }

/*
 * Get Slice and HW ID in slice to access HW block (slice) and channel's registers
 *
 * @param channel PDH channel
 * @param phyModule Physical module context in which slice and HW ID of channel need to be retrieved
 * @param sliceId Output slice
 * @param hwIdInSlice Output hw ID corresponding to channel in slice
 * @return AT return code
 */
eAtRet ThaPdhChannelHwIdGet(AtPdhChannel channel, eAtModule phyModule,  uint8* sliceId, uint8 *hwIdInSlice)
    {
    uint32 u32slice = 0, u32sts = 0, u32vtg = 0, u32vt = 0, u32ds0 = 0;
    eAtRet ret;

    ret = AtPdhChannelCascadeHwIdGet(channel, phyModule, &u32slice, &u32sts, &u32vtg, &u32vt, &u32ds0);
    *sliceId = (uint8)u32slice;
    *hwIdInSlice = (uint8)u32sts;
    mDriverErrorLog(ret, "Channel for hwslice and hwsts\r\n");

    return ret;
    }

eAtRet ThaPdhChannelHwSlice48AndSts48IdGet(AtPdhChannel channel, uint8* hwSlice48Id, uint8 *hwSts48Id)
    {
    uint8 slice24, sts24;
    eAtRet ret = ThaPdhChannelHwIdGet(channel, cAtModulePdh,  &slice24, &sts24);
    if (ret != cAtOk)
        return ret;

    *hwSlice48Id = (uint8) (slice24 / 2);
    *hwSts48Id = (uint8) (sts24 * 2 + slice24 % 2);
    return cAtOk;
    }

AtPdhDe3 ThaModulePdhDe3ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id)
    {
    if (self)
        return mMethodsGet(self)->De3ChannelFromHwIdGet(self, phyModule, sliceId, de3Id);
    return 0;
    }

AtPdhDe1 ThaModulePdhDe1ChannelFromHwIdGet(ThaModulePdh self, eAtModule phyModule, uint8 sliceId, uint8 de3Id, uint8 de2Id, uint8 de1Id)
    {
    if (self)
        return mMethodsGet(self)->De1ChannelFromHwIdGet(self, phyModule, sliceId, de3Id, de2Id, de1Id);
    return 0;
    }

AtPrbsEngine ThaModulePdhDe1PrbsEngineCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1PrbsEngineCreate(self, de1);
    return NULL;
    }

AtPrbsEngine ThaModulePdhDe3PrbsEngineCreate(ThaModulePdh self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->De3PrbsEngineCreate(self, de3);
    return NULL;
    }

AtPrbsEngine ThaModulePdhNxDs0PrbsEngineCreate(ThaModulePdh self, AtPdhNxDS0 nxDs0)
    {
    if (self)
        return mMethodsGet(self)->NxDs0PrbsEngineCreate(self, nxDs0);
    return NULL;
    }

int32 ThaModulePdhSliceOffset(ThaModulePdh self, uint32 sliceId)
    {
    if (self)
        return mMethodsGet(self)->SliceOffset(self, sliceId);
    return 0;
    }

uint32 ThaRegDS1E1J1TxFrmrSignalingBuffer(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->DS1E1J1TxFrmrSignalingBufferRegister(self);
    return cBit31_0;
    }

uint32 ThaModulePdhM12E12ControlOffset(ThaModulePdh self, ThaPdhDe2 de2)
    {
    if (self)
        return mMethodsGet(self)->M12E12ControlOffset(self, de2);
    return cBit31_0;
    }
uint32 ThaModulePdhM12E12CounterOffset(ThaModulePdh self, ThaPdhDe2 de2, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->M12E12CounterOffset(self, de2, r2c);
    return cBit31_0;
    }
void ThaModulePdhSdhChannelRegsShow(AtSdhChannel sdhChannel)
    {
    ThaModulePdh self = ModulePdh((AtChannel)sdhChannel);
    if (self)
        {
        SdhChannelIdsShow(self, sdhChannel);
        mMethodsGet(self)->SdhChannelRegsShow(self, sdhChannel);
        }
    }

void ThaModulePdhPdhChannelRegsShow(AtPdhChannel pdhChannel)
    {
    ThaModulePdh self = ModulePdh((AtChannel)pdhChannel);
    if (self)
        {
        PdhChannelIdsShow(self, pdhChannel);
        AtPrintc(cSevInfo,   "* PDH registers of channel '%s':\r\n", AtObjectToString((AtObject)pdhChannel));

        mMethodsGet(self)->PdhChannelRegsShow(self, pdhChannel);
        }
    }

void ThaModulePdhM13Init(ThaModulePdh self)
    {
    uint8 numSlices, numStsInSlice;
    uint8 sliceId, stsId;

    if (!ThaModulePdhHasM13(self, 0))
        return;

    numSlices = mMethodsGet(self)->NumSlices(self);
    numStsInSlice = mMethodsGet(self)->NumStsInSlice(self);
    for (sliceId = 0; sliceId < numSlices; sliceId++)
        {
        int32 sliceOffset = ThaModulePdhSliceOffset(self, sliceId);

        for (stsId = 0; stsId < numStsInSlice; stsId++)
            {
            mModuleHwWrite(self, (uint32)(cThaRegRxM23E23Ctrl + stsId + sliceOffset), 0x8);
            mModuleHwWrite(self, (uint32)(cThaRegTxM23E23Ctrl + stsId + sliceOffset), 0x8);
            }
        }
    }

eBool ThaModulePdhTxLiuIsPositiveClockInvert(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->TxLiuIsPositiveClockInvert(self);
    return cAtFalse;
    }

AtPrbsEngine ThaModulePdhDe1LinePrbsEngineCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    if (self)
        return De1LinePrbsEngineCreate(self, de1);
    return NULL;
    }

AtPrbsEngine ThaModulePdhDe3LinePrbsEngineCreate(ThaModulePdh self, AtPdhDe3 de3)
    {
    if (self)
        return De3LinePrbsEngineCreate(self, de3);
    return NULL;
    }

uint32 ThaModulePdhDiagLineTestEnableRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineTestEnableRegister(self, channel);
    return cBit31_0;
    }

uint32 ThaModulePdhDiagLineTestStatusRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineTestStatusRegister(self, channel);
    return cBit31_0;
    }

uint32 ThaModulePdhDiagLineTestErrorInsertRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineTestErrorInsertRegister(self, channel);
    return cBit31_0;
    }

uint32 ThaModulePdhDiagLineModeRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineModeRegister(self, channel);
    return cBit31_0;
    }

uint32 ThaModulePdhDiagLineRemoteLoopbackRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineRemoteLoopbackRegister(self, channel);
    return cBit31_0;
    }

uint32 ThaModulePdhDiagLineMask(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineMask(self, channel);
    return 0;
    }

uint32 ThaModulePdhDiagLineShift(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineShift(self, channel);
    return 0;
    }

uint32 ThaModulePdhDiagLineRxClockInvertRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineRxClockInvertRegister(self, channel);
    return 0;
    }

uint32 ThaModulePdhDiagLineEc1ModeRegister(ThaModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->DiagLineEc1ModeRegister(self, channel);
    return 0;
    }

eAtRet ThaModulePdhDiagLiuLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    uint32 regAddr = ThaModulePdhDiagLineRemoteLoopbackRegister(pdhModule, pdhChannel);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    uint8 remoteLoopback = (loopbackMode == cAtPdhLoopbackModeRemoteLine) ? 1 :0;

    mFieldIns(&regVal,
              ThaModulePdhDiagLineMask(pdhModule, pdhChannel),
              ThaModulePdhDiagLineShift(pdhModule, pdhChannel),
              remoteLoopback);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

uint8 ThaModulePdhDiagLiuLoopbackGet(AtChannel self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    uint32 regAddr = ThaModulePdhDiagLineRemoteLoopbackRegister(pdhModule, pdhChannel);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);

    if (regVal & ThaModulePdhDiagLineMask(pdhModule, pdhChannel))
        return cAtPdhLoopbackModeRemoteLine;

    return cAtPdhLoopbackModeRelease;
    }

eAtRet ThaModulePdhDiagLiuRxClockInvertEnable(AtChannel self, eBool enable)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    uint32 regAddr = ThaModulePdhDiagLineRxClockInvertRegister(pdhModule, pdhChannel);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal,
              ThaModulePdhDiagLineMask(pdhModule, pdhChannel),
              ThaModulePdhDiagLineShift(pdhModule, pdhChannel),
              (enable) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

eAtRet ThaModulePdhDiagLiuLineModeSet(AtChannel self, eBool isDSxMode)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    uint32 regAddr = ThaModulePdhDiagLineModeRegister(pdhModule, pdhChannel);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal,
              ThaModulePdhDiagLineMask(pdhModule, pdhChannel),
              ThaModulePdhDiagLineShift(pdhModule, pdhChannel),
              (isDSxMode) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

eAtRet ThaModulePdhDiagLiuEc1ModeEnable(AtChannel self, eBool enable)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)self);
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    uint32 regAddr = ThaModulePdhDiagLineEc1ModeRegister(pdhModule, pdhChannel);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePdh);
    mFieldIns(&regVal,
              ThaModulePdhDiagLineMask(pdhModule, pdhChannel),
              ThaModulePdhDiagLineShift(pdhModule, pdhChannel),
              (enable) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePdh);

    return cAtOk;
    }

AtPdhMdlController ThaModulePdhMdlControllerCreate(ThaModulePdh self, AtPdhDe3 de3, uint32 mdlType)
    {
    if (self)
        return mMethodsGet(self)->MdlControllerCreate(self, de3, mdlType);
    return NULL;
    }

eBool ThaModulePdhMdlIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->MdlIsSupported(self);
    return cAtFalse;
    }

uint32 ThaModulePdhBaseAddress(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0x0;
    }

eBool ThaModulePdhDetectDe1LosByAllZeroPattern(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->DetectDe1LosByAllZeroPattern(self);
    return cAtFalse;
    }

eBool ThaModulePdhDetectDe3LosByAllZeroPattern(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->DetectDe3LosByAllZeroPattern(self);
    return cAtFalse;
    }

eBool ThaModulePdhPrmIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->PrmIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhBomIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (self == NULL)
        return cAtFalse;

    return (AtDeviceVersionNumber(device) >= mMethodsGet(self)->StartVersionSupportBom(self)) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePdhInbandLoopcodeIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->InbandLoopcodeIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhDe1SsmIsSupported(ThaModulePdh self)
    {
    if (self)
        return De1SsmIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhDe3SsmIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (self == NULL)
        return cAtFalse;

    return (AtDeviceVersionNumber(device) >= mMethodsGet(self)->StartVersionSupportDe3Ssm(self)) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePdhDe3FramedSatopIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->De3FramedSatopIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhDe3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->De3AisSelectablePatternIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhCanDisableDe3(ThaModulePdh self)
    {
    if (self)
        return CanDisableDe3(self);
    return cAtFalse;
    }

uint32 ThaModulePdhPrmBaseAddress(ThaModulePdh self)
    {
    AtInterruptManager prmManager;
    if (self == NULL)
        return 0x0;

    prmManager = AtModuleInterruptManagerAtIndexGet((AtModule)self, cThaPdhPrmInterruptManager);
    return ThaInterruptManagerBaseAddress((ThaInterruptManager)prmManager);
    }

uint32 ThaModulePdhMdlBaseAddress(ThaModulePdh self)
    {
    return ThaInterruptManagerBaseAddress(ThaModulePdhMdlInterruptManager(self));
    }

ThaInterruptManager ThaModulePdhMdlInterruptManager(ThaModulePdh self)
    {
    return (ThaInterruptManager)AtModuleInterruptManagerAtIndexGet((AtModule)self, cThaPdhMdlInterruptManager);
    }

ThaInterruptManager ThaModulePdhPrmInterruptManager(ThaModulePdh self)
    {
    return (ThaInterruptManager)AtModuleInterruptManagerAtIndexGet((AtModule)self, cThaPdhPrmInterruptManager);
    }

AtPdhPrmController ThaModulePdhPrmControllerCreate(ThaModulePdh self, AtPdhDe1 de1)
    {
    if (ThaModulePdhPrmIsSupported(self))
        return mMethodsGet(self)->PrmControllerObjectCreate(self, de1);
    return NULL;
    }

eBool ThaModulePdhAisDownStreamIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion;

    if (ThaDeviceIsEp((ThaDevice)device) || (AtDeviceAllFeaturesAvailableInSimulation(device) == cAtTrue))
        return cAtTrue;

    startSupportedVersion = mMethodsGet(self)->StartVersionSupportAisDownStream(self);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePdhFramerForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    return mMethodsGet(self)->ForcedAisCorrectionIsSupported(self);
    }

eBool ThaModulePdhLiuForcedAisIsSupported(ThaModulePdh self)
    {
    return mMethodsGet(self)->LiuForcedAisIsSupported(self);
    }

eBool ThaModulePdhDe3TxLiuEnableIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->De3TxLiuEnableIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhSlipBufferBomSendingCanWaitForReady(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportDe1SsmSendingReadyIndication(self);
    return (AtDeviceVersionNumber(device) >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePdhDatalinkIsSupported(ThaModulePdh self)
    {
    return ThaModulePdhPrmIsSupported(self);
    }

ThaPdhDebugger ThaModulePdhDebugger(ThaModulePdh self)
    {
    if (self)
        return Debugger(self);
    return NULL;
    }

eBool ThaModulePdhAlarmForwardingStatusIsSupported(ThaModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 version = AtDeviceVersionNumber(device);
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportAlarmForwardingStatus(self);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

eBool ThaModulePdhDe1LomfConsequentialActionIsSupported(ThaModulePdh self)
    {
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    uint32 startSupportedVersion = mMethodsGet(self)->StartVersionSupportDe1LomfConsequentialAction(self);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

uint8 ThaModulePdhDefaultDe3JitterStuffingMode(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->DefaultDe3JitterStuffingMode(self);
    return 1; /* Which is default long time ago */
    }

AtErrorGenerator ThaModulePdhDe1TxErrorGeneratorGet(ThaModulePdh self)
    {
    mAttributeGet(De1TxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

AtErrorGenerator ThaModulePdhDe1RxErrorGeneratorGet(ThaModulePdh self)
    {
    mAttributeGet(De1RxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

AtErrorGenerator ThaModulePdhDe3TxErrorGeneratorGet(ThaModulePdh self)
    {
    mAttributeGet(De3TxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

AtErrorGenerator ThaModulePdhDe3RxErrorGeneratorGet(ThaModulePdh self)
    {
    mAttributeGet(De3RxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

eBool ThaModulePdhDs3AisInterruptIsFixed(ThaModulePdh self)
    {
    AtDevice device;

    device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= mMethodsGet(self)->StartVersionFixDs3AisInterrupt(self))
        return cAtTrue;

    return cAtFalse;
    }

uint32 ThaModulePdhDe1PrmOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1PrmOffset(self, de1);

    return cInvalidUint32;
    }

uint32 ThaModulePdhPrmStsAndTug2Offset(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->PrmStsAndTug2Offset(self);

    return cInvalidUint32;
    }

eBool ThaModulePdhPrmFcsBitOrderIsSupported(ThaModulePdh self)
    {
    mAttributeGet(PrmFcsBitOrderIsSupported, eBool, cAtFalse);
    }

uint32 ThaModulePdhErrorGeneratorBerDivCoefficient(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->ErrorGeneratorBerDivCoefficient(self);

    return cInvalidUint32;
    }

eBool ThaModulePdhShouldDetectAisAllOnesWithUnframedMode(ThaModulePdh self)
    {
    mAttributeGet(ShouldDetectAisAllOnesWithUnframedMode, eBool, cAtFalse);
    }

uint32 ThaModulePdhSliceBase(ThaModulePdh self, uint32 sliceId)
    {
    if (self)
        return mMethodsGet(self)->SliceBase(self, sliceId);
    return cInvalidUint32;
    }

uint8 ThaModulePdhNumSlices(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->NumSlices(self);
    return 0;
    }

uint8 ThaModulePdhNumStsInSlice(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->NumStsInSlice(self);
    return 0;
    }

eAtRet ThaModulePdhDebugCountersModuleSet(ThaModulePdh self, uint32 module)
    {
    if (self)
        return mMethodsGet(self)->DebugCountersModuleSet(self, module);
    return cAtErrorNullPointer;
    }

uint32 ThaModulePdhDebugCountersModuleGet(ThaModulePdh self)
    {
    return self ? self->countersModule : cAtModuleSdh;
    }

eBool ThaModulePdhDe1LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->De1LiuShouldReportLofWhenLos(self);
    return cAtFalse;
    }

eBool ThaModulePdhDe3LiuShouldReportLofWhenLos(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->De3LiuShouldReportLofWhenLos(self);
    return cAtFalse;
    }

uint32 ThaModulePdhRxPerStsVCPldCtrlRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxPerStsVCPldCtrlBase);
    }

uint32 ThaModulePdhRxM12E12MuxRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM12E12MuxBase);
    }

uint32 ThaModulePdhRxM12E12FrmChnIntrEnCtrlRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM12E12FrmChnIntrEnCtrlBase);
    }

uint32 ThaModulePdhRxM12E12FrmChnIntrChngStatusRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM12E12FrmChnIntrChngStatusBase);
    }

uint32 ThaModulePdhRxM12E12FrmChnAlarmCurStatusRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM12E12FrmChnAlarmCurStatusBase);
    }

uint32 ThaModulePdhDS1E1J1RxFrmrCrcErrCntRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, DS1E1J1RxFrmrCrcErrCntBase);
    }

uint32 ThaModulePdhDS1E1J1RxFrmrReiCntRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, DS1E1J1RxFrmrReiCntBase);
    }

uint32 ThaModulePdhDS1E1J1RxFrmrFBECntRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, DS1E1J1RxFrmrFBECntBase);
    }

uint32 ThaModulePdhDS1E1J1RxFrmrperChnIntrEnCtrlRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, DS1E1J1RxFrmrperChnIntrEnCtrlBase);
    }

uint32 ThaModulePdhRxM23E23FrmrReiCntRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM23E23FrmrReiCntBase);
    }

uint32 ThaModulePdhRxM23E23FrmrFBECntRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM23E23FrmrFBECntBase);
    }

uint32 ThaModulePdhRxM23E23FrmrParCntRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM23E23FrmrParCntBase);
    }

uint32 ThaModulePdhRxM23E23FrmrCbitParCntRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxM23E23FrmrCbitParCntBase);
    }

uint32 ThaModulePdhRxDs3E3OHProCtrlRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxDs3E3OHProCtrlBase);
    }

uint32 ThaModulePdhRxDe3FeacBufferRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, RxDe3FeacBufferBase);
    }

uint32 ThaModulePdhDe1RxFramerHwStatusRegister(ThaModulePdh self)
    {
    mAbsoluteRegMake(self, De1RxFramerHwStatusBase);
    }

uint32 ThaModulePdhRxm23e23ChnIntrCfgBase(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->Rxm23e23ChnIntrCfgBase(self);
    return cInvalidUint32;
    }

eAtRet ThaModulePdhDe1InterruptEnable(ThaModulePdh self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->De1InterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePdhDe3InterruptEnable(ThaModulePdh self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->De3InterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModulePdhRxHw3BitFeacSignalIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->RxHw3BitFeacSignalIsSupported(self);
    return cAtFalse;
    }

eBool ThaModulePdhCanHotChangeFrameType(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->CanHotChangeFrameType(self);
    return cAtFalse;
    }

eBool ThaModulePdhDe1IdleCodeInsertionIsSupported(ThaModulePdh self)
    {
    if (self)
        return De1IdleCodeInsertionIsSupported(self);
    return cAtFalse;
    }

void ThaModulePdhInterruptProcess(ThaModulePdh self, uint32 glbIntr, AtIpCore ipCore)
    {
    if (self)
        InterruptProcess((AtModule)self, glbIntr, ipCore);
    }

AtPktAnalyzer ThaModulePdhPrmAnalyzer(ThaModulePdh self)
    {
    if (self)
        return PrmAnalyzer(self);
    return NULL;
    }

eAtRet ThaModulePdhHoVcUnChannelize(ThaModulePdh self, AtSdhChannel hoVc)
    {
    if (self)
        return mMethodsGet(self)->HoVcUnChannelize(self, hoVc);
    return cAtErrorObjectNotExist;
    }

eAtRet ThaModulePdhHoVcChannelizeRestore(ThaModulePdh self, AtSdhChannel hoVc)
    {
    if (self)
        return mMethodsGet(self)->HoVcChannelizeRestore(self, hoVc);
    return cAtErrorObjectNotExist;
    }

eBool ThaModulePdhShouldReportBerViaBerController(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->ShouldReportBerViaBerController(self);
    return cAtFalse;
    }

uint32 ThaModulePdhTimeslotDefaultOffset(ThaModulePdh self, ThaPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->TimeslotDefaultOffset(self, de1);
    return 0;
    }

eBool ThaModulePdhDe1RxSlipBufferIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->De1RxSlipBufferIsSupported(self);
    return cAtFalse;
    }

uint32 ThaModulePdhRxSlipBufferCounterRoOffset(ThaModulePdh self, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->RxSlipBufferCounterRoOffset(self, r2c);
    return 0;
    }

eBool ThaModulePdhTxRetimingSignalingIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->TxRetimingSignalingIsSupported(self);
    return cAtFalse;
    }

eBool ThaPdhChannelShouldRejectTxChangeWhenBoundPw(AtPdhChannel channel)
    {
    ThaModulePdh modulePdh;

    if (channel == NULL)
        return cAtFalse;

    modulePdh = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    return mMethodsGet(modulePdh)->ShouldRejectTxChangeWhenChannelBoundPw(modulePdh);
    }

AtPdhChannel ThaModulePdhVcDe3ObjectGet(ThaModulePdh self, AtSdhChannel vc3)
    {
    if (self)
        return mMethodsGet(self)->VcDe3ObjectGet(self, vc3);
    return NULL;
    }

eBool ThaModulePdhDe1FastSearchIsSupported(ThaModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->De1FastSearchIsSupported(self);
    return cAtFalse;
    }

void ThaModulePdhChannelIdsShow(ThaModulePdh self, AtPdhChannel pdhChannel)
    {
    if (self)
        PdhChannelIdsShow(self, pdhChannel);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : ThaModuleCosV2.c
 *
 * Created Date: May 16, 2013
 *
 * Description : COS version 2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleCosInternal.h"
#include "../pw/ThaModulePwV2Reg.h"
#include "controllers/ThaCosController.h"
#include "ThaModuleCosAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPwOffset(cos, pw) mMethodsGet((ThaModuleCos)cos)->PwDefaultOffset((ThaModuleCos)cos, pw)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCosMethods m_ThaModuleCosOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwRtpPayloadTypeSet(ThaModuleCos self, AtPw pw, uint8 payloadType)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModuleCos);
    mRegFieldSet(regVal, cThaRegPwTxEthPwRtpPtValue, payloadType);
    mChannelHwWrite(pw, regAddr, regVal, cThaModuleCos);

    return cAtOk;
    }

static uint8 PwRtpPayloadTypeGet(ThaModuleCos self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModuleCos);

    return (uint8)mRegField(regVal, cThaRegPwTxEthPwRtpPtValue);
    }

static eAtRet PwRtpSsrcSet(ThaModuleCos self, AtPw pw, uint32 ssrc)
    {
    uint32 regAddr;

    regAddr = cThaRegPwTxRtpSsrcValue + mPwOffset(self, pw);
    mChannelHwWrite(pw, regAddr, ssrc, cThaModuleCos);

    return cAtOk;
    }

static uint32 PwRtpSsrcGet(ThaModuleCos self, AtPw pw)
    {
    return mChannelHwRead(pw, cThaRegPwTxRtpSsrcValue + mPwOffset(self, pw), cThaModuleCos);
    }

static eAtRet PwHeaderLengthReset(ThaModuleCos self, uint8 partId, uint32 localPwId)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + localPwId + ThaModuleCosPartOffset(self, partId);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cThaRegPwTxEthHdrLengthCtrlTxHdrLen, 0);
    mRegFieldSet(regVal, cThaRegPwTxForceControlWordError, 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet PwHeaderLengthSet(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaRegPwTxEthHdrLengthCtrlTxHdrLen, hdrLenInByte);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint8 PwHeaderLengthGet(ThaModuleCos self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPwTxEthHdrLengthCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (uint8)mRegField(regVal, cThaRegPwTxEthHdrLengthCtrlTxHdrLen);
    }

static eAtRet PwCwSequenceModeSet(ThaModuleCos self, AtPw pw, eAtPwCwSequenceMode sequenceMode)
    {
	AtUnused(pw);
	AtUnused(self);
    if (sequenceMode == cAtPwCwSequenceModeWrapZero)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtPwCwSequenceMode PwCwSequenceModeGet(ThaModuleCos self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return cAtPwCwSequenceModeWrapZero;
    }

static eBool PwCwSequenceModeIsSupported(ThaModuleCos self, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(self);
    return (sequenceMode == cAtPwCwSequenceModeWrapZero) ? cAtTrue : cAtFalse;
    }

static eAtRet PwCwLengthModeSet(ThaModuleCos self, AtPw pw, eAtPwCwLengthMode lengthMode)
    {
	AtUnused(pw);
	AtUnused(self);
    if (lengthMode == cAtPwCwLengthModePayload)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtPwCwLengthMode PwCwLengthModeGet(ThaModuleCos self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return cAtPwCwLengthModePayload;
    }

static eBool PwCwLengthModeIsSupported(ThaModuleCos self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(self);
    return (lengthMode == cAtPwCwLengthModePayload) ? cAtTrue : cAtFalse;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(localAddress);
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet DefaultSet(ThaModuleCos self)
    {
    return ThaModuleCosAllPwsReset(self);
    }

static eAtRet AsyncDefaultSet(ThaModuleCos self)
    {
    return ThaModuleCosDefaultAllPwsAsyncReset(self);
    }

static eAtRet Debug(AtModule self)
    {
	AtUnused(self);
    return cAtOk;
    }

static ThaCosEthPortController EthPortControllerCreate(ThaModuleCos self)
    {
    return ThaCosEthPortControllerV2New(self);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModuleCos(AtModule self)
    {
    ThaModuleCos cos = (ThaModuleCos)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCosOverride, mMethodsGet(cos), sizeof(m_ThaModuleCosOverride));

        mMethodOverride(m_ThaModuleCosOverride, PwRtpSsrcSet);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpPayloadTypeSet);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpPayloadTypeGet);
        mMethodOverride(m_ThaModuleCosOverride, PwRtpSsrcGet);
        mMethodOverride(m_ThaModuleCosOverride, PwCwSequenceModeGet);
        mMethodOverride(m_ThaModuleCosOverride, PwCwLengthModeSet);
        mMethodOverride(m_ThaModuleCosOverride, PwCwLengthModeGet);
        mMethodOverride(m_ThaModuleCosOverride, PwCwLengthModeIsSupported);
        mMethodOverride(m_ThaModuleCosOverride, PwCwSequenceModeSet);
        mMethodOverride(m_ThaModuleCosOverride, PwCwSequenceModeIsSupported);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthSet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthGet);
        mMethodOverride(m_ThaModuleCosOverride, DefaultSet);
        mMethodOverride(m_ThaModuleCosOverride, AsyncDefaultSet);
        mMethodOverride(m_ThaModuleCosOverride, PwHeaderLengthReset);
        mMethodOverride(m_ThaModuleCosOverride, EthPortControllerCreate);
        }

    mMethodsSet(cos, &m_ThaModuleCosOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCos(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleCosV2);
    }

AtModule ThaModuleCosV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleCosObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleCosV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleCosV2ObjectInit(newModule, device);
    }

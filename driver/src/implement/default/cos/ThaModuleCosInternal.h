/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS internal module
 * 
 * File        : ThaModuleCosInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECOSINTERNAL_H_
#define _THAMODULECOSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleCos.h"
#include "../../../generic/pw/AtPwInternal.h"
#include "../../../generic/man/AtModuleInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "controllers/ThaCosController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleCosMethods
    {
    uint32 (*BaseAddress)(ThaModuleCos self);
    uint32 (*PwDefaultOffset)(ThaModuleCos self, AtPw pw);
    eAtRet (*DefaultSet)(ThaModuleCos self);
    eAtRet (*AsyncDefaultSet)(ThaModuleCos self);
    eAtRet (*PwRtpSsrcSet)(ThaModuleCos self, AtPw pw, uint32 ssrc);
    eAtRet (*PwRtpPayloadTypeSet)(ThaModuleCos self, AtPw pw, uint8 payloadType);
    uint8 (*PwRtpPayloadTypeGet)(ThaModuleCos self, AtPw pw);
    uint32 (*PwRtpSsrcGet)(ThaModuleCos self, AtPw pw);
    eAtRet (*PwCwSequenceModeSet)(ThaModuleCos self, AtPw pw, eAtPwCwSequenceMode sequenceMode);
    eAtPwCwSequenceMode (*PwCwSequenceModeGet)(ThaModuleCos self, AtPw pw);
    eBool (*PwCwSequenceModeIsSupported)(ThaModuleCos self, eAtPwCwSequenceMode sequenceMode);
    eAtRet (*PwCwLengthModeSet)(ThaModuleCos self, AtPw pw, eAtPwCwLengthMode lengthMode);
    eBool (*PwCwLengthModeIsSupported)(ThaModuleCos self, eAtPwCwLengthMode lengthMode);
    eAtPwCwLengthMode (*PwCwLengthModeGet)(ThaModuleCos self, AtPw pw);
    eBool (*IsPwPart)(ThaModuleCos self, uint8 partId);

    eAtRet (*PwHeaderLengthReset)(ThaModuleCos self, uint8 partId, uint32 localPwId);
    eAtRet (*PwHeaderLengthSet)(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte);
    uint8 (*PwHeaderLengthGet)(ThaModuleCos self, AtPw pw);
    eAtRet (*PwHeaderLengthHwSet)(ThaModuleCos self, AtPw pw, uint8 hwHdrLenValue);
    uint8 (*PwHeaderLengthHwGet)(ThaModuleCos self, AtPw pw);

    ThaCosEthPortController (*EthPortControllerCreate)(ThaModuleCos self);

    }tThaModuleCosMethods;

typedef struct tThaModuleCos
    {
    tAtModule super;
    const tThaModuleCosMethods *methods;

    /* Private data */
    ThaCosEthPortController ethPortController;

    /* Async init */
    uint32 asyncInitState;
    uint32 asyncInitDefaultInitState;
    uint32 asyncInitAllPwResetInitState;
    uint32 asyncInitAllPwResetInitPerPartState;
    }tThaModuleCos;

typedef struct tThaModuleCosV2
    {
    tThaModuleCos super;
    }tThaModuleCosV2;

typedef struct tThaModuleCosPpp
    {
    tThaModuleCos super;
    }tThaModuleCosPpp;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleCosObjectInit(AtModule module, AtDevice device);
AtModule ThaModuleCosV2ObjectInit(AtModule self, AtDevice device);
AtModule ThaModuleCosPppObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECOSINTERNAL_H_ */


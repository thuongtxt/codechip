/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : ThaCosEthPortControllerV2V2.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : COS ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModuleCosV2Reg.h"
#include "ThaCosEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCosEthPortControllerMethods m_ThaCosEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 EthPortOutcomingPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortOutcomingbyteRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxByteCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortOamTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxOamPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortOamFrequencyTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPortPWTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPortPktLensmallerthan64bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxsmallerthan64bytesPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom65to127bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxfrom65to127bytesPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom128to255bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxfrom128to255bytesPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom256to511bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxfrom256to511bytesPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom512to1024bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxfrom512to1024bytesPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom1025to1528bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxfrom1025to1528bytesPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortPktJumboLenRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthTxJumboPktCnt(r2c) + ThaCosEthPortControllerPartOffset(self, port), cThaModuleCos);
    }

static uint32 EthPortPktTimePktToPRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCosEthPortControllerV2);
    }

static void OverrideThaCosEthPortController(ThaCosEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCosEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaCosEthPortControllerOverride));
        
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingbyteRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOamTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOamFrequencyTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPWTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktJumboLenRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktTimePktToPRead2Clear);
        }

    mMethodsSet(self, &m_ThaCosEthPortControllerOverride);
    }

static void Override(ThaCosEthPortController self)
    {
    OverrideThaCosEthPortController(self);
    }

ThaCosEthPortController ThaCosEthPortControllerV2ObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosEthPortControllerObjectInit(self, cos) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController ThaCosEthPortControllerV2New(ThaModuleCos cos)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCosEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaCosEthPortControllerV2ObjectInit(newController, cos);
    }

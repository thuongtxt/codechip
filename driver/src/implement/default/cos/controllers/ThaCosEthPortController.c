/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : ThaCosEthPortController.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : COS ETH Port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ThaModuleCosReg.h"
#include "ThaCosEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaCosEthPortControllerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth EthModule(AtEthPort port)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)port);
    return (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static uint32 PartOffset(ThaCosEthPortController self, AtEthPort port)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)port);
    ThaModuleEth ethModule = EthModule(port);
    uint8 partId = ThaModuleEthPartOfPort(ethModule, port);
	AtUnused(self);
    return ThaDeviceModulePartOffset(device, cThaModuleCla, partId);
    }

static uint32 DefaultEthPortCounterOffset(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    uint8 localId = ThaModuleEthLocalEthPortId(EthModule(port), port);

    r2c = r2c ? 0 : 1; /* Hardware use 0 for read to clear, 1 for read-only */
    return (uint32)((128UL * r2c) + (256UL * localId) + mMethodsGet(self)->PartOffset(self, port));
    }

static uint32 EthPortOutcomingPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosOutcomingPktCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortOutcomingbyteRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosOutcomingbyteCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortOamTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthOamTxPktCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortOamFrequencyTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthOamFrequencyTxPktCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPWTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPWTxPktCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLensmallerthan64bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktLensmallerthan64bytesCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom65to127bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktLenfrom65to127bytesCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom128to255bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktLenfrom128to255bytesCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom256to511bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktLenfrom256to511bytesCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom512to1024bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktLenfrom512to1024bytesCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktLenfrom1025to1528bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktLenfrom1025to1528bytesCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktJumboLenRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktJumboLenCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktTimePktToPRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegCosEthPktTimePktToPCnt + DefaultEthPortCounterOffset(self, port, r2c), cThaModuleCos);
    }

static uint32 EthPortPktUnicastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPortPktBcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPortPktMcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 EthPortPktLenfrom1529to2047bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(self);
    AtUnused(port);
    AtUnused(r2c);
    return 0;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCosEthPortController);
    }

static void MethodsInit(ThaCosEthPortController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Counters */
        mMethodOverride(m_methods, EthPortOutcomingPktRead2Clear);
        mMethodOverride(m_methods, EthPortOutcomingbyteRead2Clear);
        mMethodOverride(m_methods, EthPortOamTxPktRead2Clear);
        mMethodOverride(m_methods, EthPortOamFrequencyTxPktRead2Clear);
        mMethodOverride(m_methods, EthPortPWTxPktRead2Clear);
        mMethodOverride(m_methods, EthPortPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_methods, EthPortPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_methods, EthPortPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_methods, EthPortPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_methods, EthPortPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_methods, EthPortPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_methods, EthPortPktJumboLenRead2Clear);
        mMethodOverride(m_methods, EthPortPktTimePktToPRead2Clear);
        mMethodOverride(m_methods, PartOffset);
        mMethodOverride(m_methods, DefaultEthPortCounterOffset);
        mMethodOverride(m_methods, EthPortPktUnicastPktRead2Clear);
        mMethodOverride(m_methods, EthPortPktBcastPktRead2Clear);
        mMethodOverride(m_methods, EthPortPktMcastPktRead2Clear);
        mMethodOverride(m_methods, EthPortPktLenfrom1529to2047bytesRead2Clear);
        }

    mMethodsSet(self, &m_methods);
    }

ThaCosEthPortController ThaCosEthPortControllerObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosControllerObjectInit((ThaCosController)self, cos) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController ThaCosEthPortControllerNew(ThaModuleCos cos)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCosEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaCosEthPortControllerObjectInit(newController, cos);
    }

uint32 ThaCosEthPortControllerOutcomingPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortOutcomingPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerOutcomingbyteRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortOutcomingbyteRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerOamTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortOamTxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerOamFrequencyTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortOamFrequencyTxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPWTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPWTxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktLensmallerthan64bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktLensmallerthan64bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktLenfrom65to127bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktLenfrom65to127bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktLenfrom128to255bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktLenfrom128to255bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktLenfrom256to511bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktLenfrom256to511bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktLenfrom512to1024bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktLenfrom512to1024bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktLenfrom1025to1528bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktLenfrom1025to1528bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktJumboLenRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktJumboLenRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPktTimePktToPRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if(self)
        return mMethodsGet(self)->EthPortPktTimePktToPRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerPartOffset(ThaCosEthPortController self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->PartOffset(self, port);

    return 0;
    }

uint32 ThaCosEthPortControllerEthPortPktUnicastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPortPktUnicastPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerEthPortPktBcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPortPktBcastPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerEthPortPktMcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPortPktMcastPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaCosEthPortControllerEthPortPktLenfrom1529to2047bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPortPktLenfrom1529to2047bytesRead2Clear(self, port, r2c);

    return 0;
    }

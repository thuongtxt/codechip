/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : ThaCosEthPortControllerInternal.h
 * 
 * Created Date: Dec 3, 2013
 *
 * Description : COS ETH Port controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACOSETHPORTCONTROLLERINTERNAL_H_
#define _THACOSETHPORTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaCosControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCosPppPwEthPortController * ThaCosPppPwEthPortController;

typedef enum eThaCosEthPortCounterType
    {
    cThaCosEthPortOutcomingPkt,
    cThaCosEthPortOutcomingbyte,
    cThaCosEthPortOamTxPkt,
    cThaCosEthPortOamFrequencyTxPkt,
    cThaCosEthPortPWTxPkt,
    cThaCosEthPortPktLensmallerthan64bytes,
    cThaCosEthPortPktLenfrom65to127bytes,
    cThaCosEthPortPktLenfrom128to255bytes,
    cThaCosEthPortPktLenfrom256to511bytes,
    cThaCosEthPortPktLenfrom512to1024bytes,
    cThaCosEthPortPktLenfrom1025to1528bytes,
    cThaCosEthPortPktJumboLen,
    cThaCosEthPortPktTimePktToP
    }eThaCosEthPortCounterType;

typedef struct tThaCosEthPortControllerMethods
    {
    uint32 (*EthPortOutcomingPktRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortOutcomingbyteRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortOamTxPktRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortOamFrequencyTxPktRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPWTxPktRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktLensmallerthan64bytesRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktLenfrom65to127bytesRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktLenfrom128to255bytesRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktLenfrom256to511bytesRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktLenfrom512to1024bytesRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktLenfrom1025to1528bytesRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktJumboLenRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktTimePktToPRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktLenfrom1529to2047bytesRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktUnicastPktRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktBcastPktRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPortPktMcastPktRead2Clear)(ThaCosEthPortController self, AtEthPort port, eBool r2c);

    uint32 (*PartOffset)(ThaCosEthPortController self, AtEthPort port);
    uint32 (*DefaultEthPortCounterOffset)(ThaCosEthPortController self, AtEthPort port, eBool r2c);
    }tThaCosEthPortControllerMethods;

typedef struct tThaCosEthPortController
    {
    tThaCosController super;
    const tThaCosEthPortControllerMethods *methods;

    }tThaCosEthPortController;

typedef struct tThaCosEthPortControllerV2
    {
    tThaCosEthPortController super;

    }tThaCosEthPortControllerV2;

typedef struct tThaCosPppPwEthPortControllerMethods
    {
    ThaCosEthPortController (*PppEthPortControllerCreate)(ThaCosPppPwEthPortController self);
    ThaCosEthPortController (*PwEthPortControllerCreate)(ThaCosPppPwEthPortController self);
    ThaCosEthPortController (*ControllerOfCounter)(ThaCosPppPwEthPortController self, AtEthPort port, eThaCosEthPortCounterType counterType);
    ThaCosEthPortController (*ControllerOfPort)(ThaCosPppPwEthPortController self, AtEthPort port);
    }tThaCosPppPwEthPortControllerMethods;

typedef struct tThaCosPppPwEthPortController
    {
    tThaCosEthPortController super;
    const tThaCosPppPwEthPortControllerMethods *methods;

    /* Private data */
    ThaCosEthPortController pppEthPortController;
    ThaCosEthPortController pwEthPortController;
    }tThaCosPppPwEthPortController;
    
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCosEthPortController ThaCosPppPwEthPortControllerPwEthPortController(ThaCosPppPwEthPortController self);
ThaCosEthPortController ThaCosPppPwEthPortControllerPppEthPortController(ThaCosPppPwEthPortController self);
ThaCosEthPortController ThaCosPppPwEthPortControllerObjectInit(ThaCosEthPortController self, ThaModuleCos cos);

#ifdef __cplusplus
}
#endif
#endif /* _THACOSETHPORTCONTROLLERINTERNAL_H_ */


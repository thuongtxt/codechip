/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : ThaCosController.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : COS controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "ThaCosControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaCosController object = (ThaCosController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(cosModule);
    }

static void OverrideAtObject(ThaCosController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaCosController self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCosController);
    }

ThaCosController ThaCosControllerObjectInit(ThaCosController self, ThaModuleCos cosModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Private data */
    Override(self);
    m_methodsInit = 1;

    self->cosModule = cosModule;

    return self;
    }

ThaModuleCos ThaCosControllerModuleGet(ThaCosController self)
    {
    return self ? self->cosModule : NULL;
    }

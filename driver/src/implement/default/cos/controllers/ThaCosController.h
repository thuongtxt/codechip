/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : ThaCosController.h
 * 
 * Created Date: Nov 29, 2013
 *
 * Description : COS controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACOSCONTROLLER_H_
#define _THACOSCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "../ThaModuleCos.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
ThaCosEthPortController ThaCosEthPortControllerNew(ThaModuleCos cos);
ThaCosEthPortController ThaCosEthPortControllerV2New(ThaModuleCos cos);

ThaModuleCos ThaCosControllerModuleGet(ThaCosController self);

uint32 ThaCosEthPortControllerOutcomingPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerOutcomingbyteRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerOamTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerOamFrequencyTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPWTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktLensmallerthan64bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktLenfrom65to127bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktLenfrom128to255bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktLenfrom256to511bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktLenfrom512to1024bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktLenfrom1025to1528bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktJumboLenRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerPktTimePktToPRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerEthPortPktUnicastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerEthPortPktBcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerEthPortPktMcastPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaCosEthPortControllerEthPortPktLenfrom1529to2047bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c);

/* Product concretes */
ThaCosEthPortController Tha60030080CosEthPortControllerNew(ThaModuleCos cos);
ThaCosEthPortController Tha60031033CosEthPortControllerNew(ThaModuleCos cos);
ThaCosEthPortController Tha60060011CosEthPortControllerNew(ThaModuleCos cos);
ThaCosEthPortController Tha60091023CosEthPortControllerNew(ThaModuleCos cos);
ThaCosEthPortController Tha60091023CosPppEthPortControllerNew(ThaModuleCos cos);
ThaCosEthPortController Tha60091023CosPwEthPortControllerNew(ThaModuleCos cos);
ThaCosEthPortController Tha60210011CosEthPortControllerNew(ThaModuleCos cos);

#ifdef __cplusplus
}
#endif
#endif /* _THACOSCONTROLLER_H_ */


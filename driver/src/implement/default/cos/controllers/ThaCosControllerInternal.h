/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : ThaCosControllerInternal.h
 * 
 * Created Date: Nov 29, 2013
 *
 * Description : COS controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACOSCONTROLLERINTERNAL_H_
#define _THACOSCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/ThaDeviceInternal.h"
#include "ThaCosController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaCosController
    {
    tAtObject super;

    /* Private data */
    ThaModuleCos cosModule;
    }tThaCosController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCosController ThaCosControllerObjectInit(ThaCosController self, ThaModuleCos cosModule);
ThaCosEthPortController ThaCosEthPortControllerObjectInit(ThaCosEthPortController self, ThaModuleCos cos);
ThaCosEthPortController ThaCosEthPortControllerV2ObjectInit(ThaCosEthPortController self, ThaModuleCos cos);
uint32 ThaCosEthPortControllerPartOffset(ThaCosEthPortController self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THACOSCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : ThaCosPppPwEthPortController.c
 *
 * Created Date: Dec 3, 2013
 *
 * Description : COS ETH Port controller for products that have both PPP and PW 
 * 				 feature
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaCosEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaCosPppPwEthPortController *)((void *)self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tThaCosPppPwEthPortControllerMethods m_methods;

/* Override */
static tAtObjectMethods                m_AtObjectOverride;
static tThaCosEthPortControllerMethods m_ThaCosEthPortControllerOverride;

/* Save super implementations */
static const tAtObjectMethods         *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DeleteController(ThaCosEthPortController *controller)
    {
    AtObjectDelete((AtObject)*controller);
    *controller = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteController(&mThis(self)->pppEthPortController);
    DeleteController(&mThis(self)->pwEthPortController);

    m_AtObjectMethods->Delete(self);
    }

static ThaCosEthPortController PppEthPortControllerCreate(ThaCosPppPwEthPortController self)
    {
    return ThaCosEthPortControllerNew(ThaCosControllerModuleGet((ThaCosController)self));
    }

static ThaCosEthPortController PwEthPortControllerCreate(ThaCosPppPwEthPortController self)
    {
    return ThaCosEthPortControllerV2New(ThaCosControllerModuleGet((ThaCosController)self));
    }

static ThaCosEthPortController ControllerOfCounter(ThaCosPppPwEthPortController self, AtEthPort port, eThaCosEthPortCounterType counterType)
    {
	AtUnused(counterType);
	AtUnused(port);
	AtUnused(self);
    /* Concrete should know */
    return NULL;
    }

static ThaCosEthPortController ControllerOfPort(ThaCosPppPwEthPortController self, AtEthPort port)
    {
	AtUnused(port);
    return ThaCosPppPwEthPortControllerPwEthPortController(self);
    }

static uint32 EthPortOutcomingPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOutcomingPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortOutcomingPkt), port, r2c);
    }

static uint32 EthPortOutcomingbyteRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOutcomingbyteRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortOutcomingbyte), port, r2c);
    }

static uint32 EthPortOamTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOamTxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortOamTxPkt), port, r2c);
    }

static uint32 EthPortOamFrequencyTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOamFrequencyTxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortOamFrequencyTxPkt), port, r2c);
    }

static uint32 EthPortPWTxPktRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPWTxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPWTxPkt), port, r2c);
    }

static uint32 EthPortPktLensmallerthan64bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLensmallerthan64bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktLensmallerthan64bytes), port, r2c);
    }

static uint32 EthPortPktLenfrom65to127bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom65to127bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktLenfrom65to127bytes), port, r2c);
    }

static uint32 EthPortPktLenfrom128to255bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom128to255bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktLenfrom128to255bytes), port, r2c);
    }

static uint32 EthPortPktLenfrom256to511bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom256to511bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktLenfrom256to511bytes), port, r2c);
    }

static uint32 EthPortPktLenfrom512to1024bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom512to1024bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktLenfrom512to1024bytes), port, r2c);
    }

static uint32 EthPortPktLenfrom1025to1528bytesRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom1025to1528bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktLenfrom1025to1528bytes), port, r2c);
    }

static uint32 EthPortPktJumboLenRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktJumboLenRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktJumboLen), port, r2c);
    }

static uint32 EthPortPktTimePktToPRead2Clear(ThaCosEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktTimePktToPRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaCosEthPortPktTimePktToP), port, r2c);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaCosEthPortController);
    }

static void OverrideAtObject(ThaCosEthPortController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaCosEthPortController(ThaCosEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCosEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaCosEthPortControllerOverride));

        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOutcomingbyteRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOamTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortOamFrequencyTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPWTxPktRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktJumboLenRead2Clear);
        mMethodOverride(m_ThaCosEthPortControllerOverride, EthPortPktTimePktToPRead2Clear);
        }

    mMethodsSet(self, &m_ThaCosEthPortControllerOverride);
    }

static void Override(ThaCosEthPortController self)
    {
    OverrideAtObject(self);
    OverrideThaCosEthPortController(self);
    }

static void MethodsInit(ThaCosPppPwEthPortController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Counters */
        mMethodOverride(m_methods, PppEthPortControllerCreate);
        mMethodOverride(m_methods, PwEthPortControllerCreate);
        mMethodOverride(m_methods, ControllerOfCounter);
        mMethodOverride(m_methods, ControllerOfPort);
        }

    mMethodsSet(self, &m_methods);
    }

ThaCosEthPortController ThaCosPppPwEthPortControllerObjectInit(ThaCosEthPortController self, ThaModuleCos cos)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCosEthPortControllerObjectInit(self, cos) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaCosEthPortController ThaCosPppPwEthPortControllerPppEthPortController(ThaCosPppPwEthPortController self)
    {
    if (self->pppEthPortController == NULL)
        self->pppEthPortController = mMethodsGet(self)->PppEthPortControllerCreate(self);
    return self->pppEthPortController;
    }

ThaCosEthPortController ThaCosPppPwEthPortControllerPwEthPortController(ThaCosPppPwEthPortController self)
    {
    if (self->pwEthPortController == NULL)
        self->pwEthPortController = mMethodsGet(self)->PwEthPortControllerCreate(self);
    return self->pwEthPortController;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : ThaModuleCosV2Reg.h
 * 
 * Created Date: May 22, 2013
 *
 * Description : COS registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECOSV2REG_H_
#define _THAMODULECOSV2REG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit Packet Counter
Address : 0x50001A(R_O), 0x50001B((r2c))
Description: Count total number of  packets transmitted to Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxPktCnt(r2c)  (uint32)(0x50001A + (r2c))

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit Byte Counter
Address : 0x500018(R_O), 0x500019((r2c))
Description: Count total number of  bytes transmitted to Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxByteCnt(r2c)  (uint32)(0x500018 + (r2c))

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit OAM Packet Counter
Address : 0x500010(R_O), 0x500011(R2C)
Description: Count number of transmitted OAM packets (ARP, ICMP,...) to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxOamPktCnt(r2c)  (uint32)(0x500010 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit 0to64Byte Packet Counter
Address : 0x500030(R_O), 0x500031(R2C)
Description: Count total number of  transmitted packets length from 0 to 64 bytes to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxsmallerthan64bytesPktCnt(r2c)  (uint32)(0x500030 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit 65to127Byte Packet Counter
Address : 0x500032(R_O), 0x500033(R2C)
Description: Count total number of  transmitted packets length from 65 to 127 bytes to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxfrom65to127bytesPktCnt(r2c)  (uint32)(0x500032 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit 128to255Byte Packet Counter
Address : 0x500034(R_O), 0x500035(R2C)
Description: Count total number of  transmitted packets length from 128 to 255 bytes to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxfrom128to255bytesPktCnt(r2c)  (uint32)(0x500034 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit 256to511Byte Packet Counter
Address : 0x500036(R_O), 0x500037(R2C)
Description: Count total number of  transmitted packets length from 256 to 511 bytes to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxfrom256to511bytesPktCnt(r2c)  (uint32)(0x500036 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit 512to1024Byte Packet Counter
Address : 0x500038(R_O), 0x500039(R2C)
Description: Count total number of  transmitted packets length from 512 to 1024 bytes to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxfrom512to1024bytesPktCnt(r2c)  (uint32)(0x500038 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit 1025to1528Byte Packet Counter
Address : 0x50003A(R_O), 0x50003B(R2C)
Description: Count total number of  transmitted packets length from 1025 to 1528 bytes to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxfrom1025to1528bytesPktCnt(r2c)  (uint32)(0x50003A + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Transmit Jumbo Packet Counter
Address : 0x50003C(R_O), 0x50003D(R2C)
Description: Count total number of  transmitted packets length Jumbo to
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthTxJumboPktCnt(r2c)  (uint32)(0x50003C + r2c)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECOSV2REG_H_ */


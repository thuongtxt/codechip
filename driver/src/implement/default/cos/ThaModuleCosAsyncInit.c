/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS
 *
 * File        : ThaModuleCosAsyncInit.c
 *
 * Created Date: Aug 20, 2016
 *
 * Description : Async init implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../pw/ThaModulePw.h"
#include "../eth/ThaModuleEth.h"
#include "ThaModuleCosInternal.h"
#include "ThaModuleCosReg.h"
#include "controllers/ThaCosController.h"
#include "ThaModuleCosAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static void AsyncAllPwsResetStateSet(ThaModuleCos self, uint32 state)
    {
    self->asyncInitAllPwResetInitState = state;
    }

static uint32 AsyncAllPwsResetStateGet(ThaModuleCos self)
    {
    return self->asyncInitAllPwResetInitState;
    }

static void AsyncAllPwsResetInitPerPartStateSet(ThaModuleCos self, uint32 state)
    {
    self->asyncInitAllPwResetInitPerPartState = state;
    }

static uint32 AsyncAllPwsResetInitPerPartStateGet(ThaModuleCos self)
    {
    return self->asyncInitAllPwResetInitPerPartState;
    }

static uint32 MaxEntryPerAsyncInitGet(ThaModuleCos self, uint32 state)
    {
    uint32 max0 = (state + 1) * 1000;
    uint32 max1 = ThaModulePwNumPwsPerPart(ThaModuleCosPwModule(self));

    if (max0 < max1)
        return max0;
    return max1;
    }

static uint32 StartEntryPerAsyncInitGet(ThaModuleCos self, uint32 state)
    {
    AtUnused(self);
    return state * 1000;
    }

static eAtRet PartPwHeaderLengthAsyncReset(ThaModuleCos self, uint32 part)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    uint32 state = AsyncAllPwsResetInitPerPartStateGet(self);
    uint32 maxEntry = MaxEntryPerAsyncInitGet(self, state);
    for (i = StartEntryPerAsyncInitGet(self, state); i < maxEntry; i++)
        {
        ret = mMethodsGet(self)->PwHeaderLengthReset(self, (uint8) part, i);
        if (ret != cAtOk)
            {
            AsyncAllPwsResetInitPerPartStateSet(self, 0);
            return ret;
            }
        }

    state += 1;
    if (maxEntry == ThaModulePwNumPwsPerPart(ThaModuleCosPwModule(self)))
        {
        ret = cAtOk;
        state = 0;
        }
    else
        ret = cAtErrorAgain;

    AsyncAllPwsResetInitPerPartStateSet(self, state);
    return ret;
    }

eAtRet ThaModuleCosDefaultAllPwsAsyncReset(ThaModuleCos self)
    {
    ThaDevice device = (ThaDevice) AtModuleDeviceGet((AtModule) self);
    eAtRet ret = cAtOk;
    uint32 state = AsyncAllPwsResetStateGet(self);

    if (mMethodsGet(self)->IsPwPart(self, (uint8) state))
        ret = PartPwHeaderLengthAsyncReset(self, state);

    if (ret == cAtOk)
        {
        state += 1;
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        state = 0;

    if (state == ThaDeviceNumPartsOfModule(device, cThaModuleCos))
        {
        ret = cAtOk;
        state = 0;
        }

    AsyncAllPwsResetStateSet(self, state);
    return ret;
    }

typedef enum eThaModuleCosAsyncDefaultInitState
    {
    cThaModuleCosAsyncDefaultInitStateFifoAndQueu = 0,
    cThaModuleCosAsyncDefaultInitStateAllPwReset
    } eThaModuleCosAsyncDefaultInitState;

static void AsyncDefaultInitStateSet(ThaModuleCos self, uint32 state)
    {
    self->asyncInitDefaultInitState = state;
    }
static uint32 AsyncDefaultInitStateGet(ThaModuleCos self)
    {
    return self->asyncInitDefaultInitState;
    }

eAtRet ThaModuleCosAsyncDefaultSetMain(ThaModuleCos self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncDefaultInitStateGet(self);
    switch (state)
        {
        case cThaModuleCosAsyncDefaultInitStateFifoAndQueu:
            ThaModuleCosDefaultFifoAndQueSet(self);
            state = cThaModuleCosAsyncDefaultInitStateAllPwReset;
            ret = cAtErrorAgain;
            break;
        case cThaModuleCosAsyncDefaultInitStateAllPwReset:
            ret = ThaModuleCosDefaultAllPwsAsyncReset(self);
            if (ret == cAtOk || !AtDeviceAsyncRetValIsInState(ret))
                state = cThaModuleCosAsyncDefaultInitStateFifoAndQueu;
            break;
        default:
            state = cThaModuleCosAsyncDefaultInitStateFifoAndQueu;
            ret = cAtErrorDevFail;
        }
    AsyncDefaultInitStateSet(self, state);
    return ret;
    }

typedef enum eThaModuleCosAsyncInitState
    {
    cThaModuleCosAsyncInitStateSuperInit = 0,
    cThaModuleCosAsyncInitStateDefaultSet
    } eThaModuleCosAsyncInitState;

static void AsyncInitStateSet(ThaModuleCos self, uint32 state)
    {
    self->asyncInitState = state;
    }

static uint32 AsyncInitStateGet(ThaModuleCos self)
    {
    return self->asyncInitState;
    }

eAtRet ThaModuleCosAsyncInitMain(ThaModuleCos self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncInitStateGet(self);
    switch (state)
        {
        case cThaModuleCosAsyncInitStateSuperInit:
            ret = ThaModuleCosSuperAsyncInit((AtModule) self);
            if (ret == cAtOk)
                {
                state = cThaModuleCosAsyncInitStateDefaultSet;
                ret = cAtErrorAgain;
                }
            break;
        case cThaModuleCosAsyncInitStateDefaultSet:
            ret = ThaModuleCosDefaultAsyncSet(self);
            if (ret == cAtOk || !AtDeviceAsyncRetValIsInState(ret))
                state = cThaModuleCosAsyncInitStateSuperInit;
            break;
        default:
            state = cThaModuleCosAsyncInitStateSuperInit;
            ret = cAtErrorDevFail;
        }
    AsyncInitStateSet(self, state);
    return ret;
    }

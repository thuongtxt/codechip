/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS
 * 
 * File        : ThaModuleCosReg.h
 * 
 * Created Date: May 21, 2013
 *
 * Description : COS registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECOSREG_H_
#define _THAMODULECOSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------------
Reg Name: COS En-queue Pw Control
Reg Addr: 0x03C0C00
          The address format for these registers is 0x03C0C00 + PseudowireID
          Where: PseudowireID (0-1023) Pseudowire Identification
Reg Desc: This ram is used to configure parameters for each pseudowire
          channel.
------------------------------------------------------------------------------*/
#define cThaRegCosEnqueuePwCtrl                    (0x3C0C00)





/*--------------------------------------
BitField Name: CosEnquePseudowireSequenceZeroMode
BitField Type: R/W
BitField Desc: Disable sequence mode (Sequence number are all zero)

--------------------------------------*/
#define cThaCosEnquePseudowireSequenceZeroModeMask         cBit19 /* cBit51 */
#define cThaCosEnquePseudowireSequenceZeroModeShift        19
#define cThaCosEnquePseudowireSequenceZeroModeDwIndex      1

/*--------------------------------------
BitField Name: COSEnquePseudowireSequenceCountMode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 1: Zero not in count
               - 0: Zero in count
--------------------------------------*/
#define cThaCosEnquePseudowireSeqCountModeMask         cBit13 /* cBit45 */
#define cThaCosEnquePseudowireSeqCountModeShift        13
#define cThaCosEnquePseudowireSeqCountModeDwIndex      1

/*--------------------------------------
BitField Name: COSEnquePseudowireCWLengthFieldMode
BitField Type: R/W
BitField Desc: set length field mode
               - 1: Payload mode, cw + rtp + payload < 64
               - 0: Full packet mode, cw + rtp + payload < 64 - mac header -
                 psn header - fcs
--------------------------------------*/
#define cThaCosEnquePseudowireCWLenFieldModeMask       cBit12 /* cBit44 */
#define cThaCosEnquePseudowireCWLenFieldModeShift      12
#define cThaCosEnquePseudowireCWLenFieldModeDwIndex    1

/*--------------------------------------
BitField Name: COSEnquePseudowireRTPSSRC
BitField Type: R/W
BitField Desc: Identifies the synchronization source. This identifier should be
               chosen randomly, with the intent that no two synchronization
               sources within the same SSRC identifier
--------------------------------------*/
#define cThaCosEnquePseudowireRTPSSRCHwHeadMask       cBit11_0
#define cThaCosEnquePseudowireRTPSSRCHwHeadShift      0
#define cThaCosEnquePseudowireRTPSSRCHwTailMask       cBit31_12
#define cThaCosEnquePseudowireRTPSSRCHwTailShift      12
#define cThaCosEnquePseudowireRTPSSRCSwHeadMask       cBit31_20
#define cThaCosEnquePseudowireRTPSSRCSwHeadShift      20
#define cThaCosEnquePseudowireRTPSSRCSwTailMask       cBit19_0
#define cThaCosEnquePseudowireRTPSSRCDwIndex           0

/*--------------------------------------
BitField Name: COSEnquePseudowireRTPPayloadType
BitField Type: R/W
BitField Desc: One payload type value must be allocated from the range of
               dynamic values for each direction of the bundle. The same
               payload type value may be reused for both directions of the
               bundle and also reused between different bundles.
--------------------------------------*/
#define cThaCosEnquePseudowireRTPPldTypeMask           cBit11_7
#define cThaCosEnquePseudowireRTPPldTypeShift          7
#define cThaCosEnquePseudowireRTPPldTypeDwIndex        0

/*--------------------------------------
BitField Name: COSEnquePseudowireHeaderLength
BitField Type: R/W
BitField Desc: header length of each pseudowire channel includes MAC header
               length (excepts FCS length) and PSN header length.
--------------------------------------*/
#define cThaCosEnquePseudowireHdrLenMask               cBit6_0
#define cThaCosEnquePseudowireHdrLenShift              0
#define cThaCosEnquePseudowireHdrLenDwIndex            0

/*------------------------------------------------------------------------------
Reg Name: COS De-packet FIFO Thresholds Control
Reg Addr: 0x003CC001
          The address format for these registers is 0x003CC001
Reg Desc: This register is used to configure threshold values for data info
          buffer. Data will be read or written out of or into this buffer when
          FIFO length of buffer is larger or smaller than data writing or
          reading thresholds.
------------------------------------------------------------------------------*/
#define cThaRegCosDepacketFifoThressCtrl           (0x3CC001)





/*--------------------------------------
BitField Name: COSDepktDataInfoBufferWritingThreshold
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaCosDepktDataInfoBufWritingThresMask        cBit31_16
#define cThaCosDepktDataInfoBufWritingThresShift       16

/*--------------------------------------
BitField Name: COSDepktDataInfoBufferReadingThreshold
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
--------------------------------------*/
#define cThaCosDepktDataInfoBufReadingThresMask        cBit15_0
#define cThaCosDepktDataInfoBufReadingThresShift       0

/*------------------------------------------------------------------------------
Reg Name: COS En-queue Route Map Control
Reg Addr: 0x003C0400
          The address format for these registers is 0x003C0400 + InQueID
          Where: InQueID (0-33) Input Queue Identification
Reg Desc: This register is used to configure global parameters for En-queue
          module.
------------------------------------------------------------------------------*/
#define cThaRegCosEnqueueRouteMapCtrl              (0x3C0400)





/*--------------------------------------
BitField Name: COSEnqueDataSource
BitField Type: R/W
BitField Desc: type of data source that will be transferred through input queue
               port
               - 0: bmt buffer
               - 1: pseudo wire loop back
               - 2: hardware oam
--------------------------------------*/
#define cThaCosEnqueDataSourceMask                     cBit3_2
#define cThaCosEnqueDataSourceShift                    2

/*--------------------------------------
BitField Name: CosEnqueGigabitPortID
BitField Type: R/W
BitField Desc: gigabit port id for this input queue, have 4 gigabit ports
--------------------------------------*/
#define cThaCosEnqueGigabitPortIDMask                     cBit1_0
#define cThaCosEnqueGigabitPortIDShift                    0

/*------------------------------------------------------------------------------
Reg Name: Cos De-packet BM Violation Length � Mode Status
Reg Addr: 0x003CC003
          The address format for these registers is 0x003CC003
Reg Desc: This register is used to report errors when Cos checks info packet that has received from BM.
------------------------------------------------------------------------------*/
#define cThaRegCosDepacketBMViolationLengthModeStatus     0x003CC003

#define cThaCosDepktBMOversizePacketErrorMask             cBit0

#define cThaCosDepktBMUndersizePacketErrorMask            cBit1

#define cThaCosDepktBMInvalidModeMask                     cBit0

/*------------------------------------------------------------------------------
Reg Name: Cos De-packet BM Packet Error Counter Status
Reg Addr: 0x003CC003
          The address format for these registers is 0x003CC003
Reg Desc: This register is used to report number errors when Cos checks info
          packet that has received from BM.
------------------------------------------------------------------------------*/
/* FIXME: This register has same address as cThaRegCosDepacketBMViolationLengthModeStatus */

/*------------------------------------------------------------------------------
Reg Name: Cos De-packet BM Loss of Start � End Request Status
Reg Addr: 0x003CC004
          The address format for these registers is 0x003CC004
Reg Desc: This register is used to report loss of start or end errors when Cos
          requests packet from DDR
------------------------------------------------------------------------------*/
#define cThaRegCosDepacketBMLossOfStartEndRequestStatus   0x003CC004

#define cThaCosDepktRequestLossofStartMask                cBit1

#define cThaCosDepktRequestLossofEndMask                  cBit0

/*------------------------------------------------------------------------------
Reg Name: Cos De-packet Info Buffer Accessing Errors Status
Reg Addr: 0x003CC006
          The address format for these registers is  0x003CC006 + DequeID * 0x4000
          Where: DequeID (0 � 1) Output Cache Identification
Reg Desc: Where: DequeID (0 � 1) Output Cache Identification
------------------------------------------------------------------------------*/
#define cThaRegCosDepacketInfoBufferAccessingErrorsStatus 0x003CC006UL

#define cThaCosDepktRequestInfoBufferReadingErrorMask     cBit0

#define cThaCosDepktRequestInfoBufferWritingErrorMask     cBit1

#define cThaCosDepktRequestInfoBufferNotEmptyMask         cBit2

#define cThaCosDepktRequestInfoBufferFullMask             cBit3

/*------------------------------------------------------------------------------
Reg Name: COS Outcoming packet counter
Reg Addr: 0x540001 + 128*R2C + 256*GEID
          The address format for these registers is 0x540001 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the transmit packet at
          GE port
------------------------------------------------------------------------------*/
#define cThaRegCosOutcomingPktCnt 0x540001

/*------------------------------------------------------------------------------
Reg Name: COS Outcoming byte counter
Reg Addr: 0x54004C + 128*R2C + 256*GEID
          The address format for these registers is 0x54004C + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the transmit byte at GE
          port
------------------------------------------------------------------------------*/
#define cThaRegCosOutcomingbyteCnt 0x54004C

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet OAM Transmit packet counter
Reg Addr: 0x540042 + 128*R2C + 256*GEID
          The address format for these registers is 0x540042 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Transmit OAM packet
------------------------------------------------------------------------------*/
#define cThaRegCosEthOamTxPktCnt 0x540042

/*--------------------------------------
BitField Name: TxOAMPktCnt[31:0]
BitField Type: R2C; RO
BitField Desc: OAM transmit packet counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet OAM Frequency Transmit packet counter
Reg Addr: 0x540043 + 128*R2C + 256*GEID
          The address format for these registers is 0x540043 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Transmit OAM Frequency
          packet
------------------------------------------------------------------------------*/
#define cThaRegCosEthOamFrequencyTxPktCnt 0x540043

/*--------------------------------------
BitField Name: TxOAMFreqPktCnt[31:0]
BitField Type: R2C; RO
BitField Desc: OAM Frequency transmit packet counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet PW Transmit packet counter
Reg Addr: 0x540044 + 128*R2C + 256*GEID
          The address format for these registers is 0x540044 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Transmit PW packet
------------------------------------------------------------------------------*/
#define cThaRegCosEthPWTxPktCnt 0x540044

/*--------------------------------------
BitField Name: TxPWPktCnt[31:0]
BitField Type: R2C; RO
BitField Desc: PW transmit packet counterCOS
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Length smaller than 64 bytes counter
Reg Addr: 0x54004F + 128*R2C + 256*GEID
          The address format for these registers is 0x54004F + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length
          smaller than 64 bytes
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktLensmallerthan64bytesCnt 0x54004F

/*--------------------------------------
BitField Name: TxPkt0to64Cnt[31:0]
BitField Type: R2C; RO
BitField Desc: Transmit packet with length smaller than 64 bytes counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Length from 65 to 127 bytes counter
Reg Addr: 0x540045 + 128*R2C + 256*GEID
          The address format for these registers is 0x540045 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          65 to 127 bytes
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktLenfrom65to127bytesCnt 0x540045

/*--------------------------------------
BitField Name: TxPkt65to127Cnt[31:0]
BitField Type: R2C; RO
BitField Desc: Transmit packet with length from 65 to 127 bytes counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Length from 128 to 255 bytes counter
Reg Addr: 0x540046 + 128*R2C + 256*GEID
          The address format for these registers is 0x540046 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          128 to 255 bytes
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktLenfrom128to255bytesCnt 0x540046

/*--------------------------------------
BitField Name: TxPkt128to255Cnt[31:0]
BitField Type: R2C; RO
BitField Desc: Transmit packet with length from 128 to 255 bytes counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Length from 256 to 511 bytes counter
Reg Addr: 0x540047 + 128*R2C + 256*GEID
          The address format for these registers is 0x540047 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          256 to 511 bytes
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktLenfrom256to511bytesCnt 0x540047

/*--------------------------------------
BitField Name: TxPkt256to511Cnt[31:0]
BitField Type: R2C; RO
BitField Desc: Transmit packet with length from 256 to 511 bytes counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Length from 512 to 1024 bytes counter
Reg Addr: 0x540048 + 128*R2C + 256*GEID
          The address format for these registers is 0x540048 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          512 to 1024 bytes
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktLenfrom512to1024bytesCnt 0x540048

/*--------------------------------------
BitField Name: TxPkt512to1024Cnt[31:0]
BitField Type: R2C; RO
BitField Desc: Transmit packet with length from 512 to 1024 bytes counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Length from 1025 to 1528 bytes counter
Reg Addr: 0x540049 + 128*R2C + 256*GEID
          The address format for these registers is 0x540049 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          1025 to 1528 bytes
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktLenfrom1025to1528bytesCnt 0x540049

/*--------------------------------------
BitField Name: TxPkt1025to1528Cnt[31:0]
BitField Type: R2C; RO
BitField Desc: Transmit packet with length from 1025 to 1528 bytes counter
BitField Bits: 31_0
--------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Jumbo Length counter
Reg Addr: 0x54004A + 128*R2C + 256*GEID
          The address format for these registers is 0x54004A + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with jumbo length
          larger than 1528 bytes
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktJumboLenCnt 0x54004A

/*--------------------------------------
BitField Name: TxPktJumboCnt[31:0]
BitField Type: R2C; RO
BitField Desc: Transmit packet with Jumbo length larger than 1528 bytes counter
BitField Bits: 31_0
--------------------------------------*/

/* Register for COS queue mode */

/*------------------------------------------------------------------------------
Reg Name: COS Ethernet packet Timing packet (ToP) counter
Reg Addr: 0x540041 + 128*R2C + 256*GEID
          The address format for these registers is 0x540041 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Transmit Timing over
          packet (ToP)
------------------------------------------------------------------------------*/
#define cThaRegCosEthPktTimePktToPCnt 0x540041

/*--------------------------------------
BitField Name: TxToPPktCnt[31:0]
BitField Type: R2C; RO
BitField Desc: ToP transmit packet counter
BitField Bits: 31_0
--------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECOSREG_H_ */


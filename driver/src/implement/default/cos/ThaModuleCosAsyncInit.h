/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : ThaModuleCosAsyncInit.h
 * 
 * Created Date: Aug 20, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECOSASYNCINIT_H_
#define _THAMODULECOSASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

ThaModulePw ThaModuleCosPwModule(ThaModuleCos self);
eAtRet ThaModuleCosSuperAsyncInit(AtModule self);
eAtRet ThaModuleCosDefaultAsyncSet(ThaModuleCos self);
eAtRet ThaModuleCosAsyncInitMain(ThaModuleCos self);
eAtRet ThaModuleCosAsyncDefaultSetMain(ThaModuleCos self);
void ThaModuleCosDefaultFifoAndQueSet(ThaModuleCos self);
eAtRet ThaModuleCosDefaultAllPwsAsyncReset(ThaModuleCos self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECOSASYNCINIT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : COS (internal)
 *
 * File        : ThaModuleCos.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : COS internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../pw/ThaModulePw.h"
#include "../eth/ThaModuleEth.h"
#include "ThaModuleCosInternal.h"
#include "ThaModuleCosReg.h"
#include "controllers/ThaCosController.h"
#include "ThaModuleCosAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPwOffset(cos, pw) mMethodsGet((ThaModuleCos)cos)->PwDefaultOffset((ThaModuleCos)cos, pw)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)
#define mThis(self) ((ThaModuleCos)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleCosMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwRtpSsrcSet(ThaModuleCos self, AtPw pw, uint32 ssrc)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    mFieldIns(&longRegVal[0],
              cThaCosEnquePseudowireRTPSSRCHwTailMask,
              cThaCosEnquePseudowireRTPSSRCHwTailShift,
              (ssrc & cThaCosEnquePseudowireRTPSSRCSwTailMask));
    mFieldIns(&longRegVal[1],
              cThaCosEnquePseudowireRTPSSRCHwHeadMask,
              cThaCosEnquePseudowireRTPSSRCHwHeadShift,
              (ssrc & cThaCosEnquePseudowireRTPSSRCSwHeadMask) >> cThaCosEnquePseudowireRTPSSRCSwHeadShift);

    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    return cAtOk;
    }

static eAtRet PwRtpPayloadTypeSet(ThaModuleCos self, AtPw pw, uint8 payloadType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    mFieldIns(&longRegVal[cThaCosEnquePseudowireRTPPldTypeDwIndex],
              cThaCosEnquePseudowireRTPPldTypeMask,
              cThaCosEnquePseudowireRTPPldTypeShift,
              (payloadType - ThaModulePwMinRtpPayloadType()));

    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    return cAtOk;
    }

static uint8 PwRtpPayloadTypeGet(ThaModuleCos self, AtPw pw)
    {
    uint8 payloadType;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    mFieldGet(longRegVal[cThaCosEnquePseudowireRTPPldTypeDwIndex],
              cThaCosEnquePseudowireRTPPldTypeMask,
              cThaCosEnquePseudowireRTPPldTypeShift,
              uint8,
              &payloadType);

    return (uint8)(payloadType + ThaModulePwMinRtpPayloadType());
    }

static uint32 PwRtpSsrcGet(ThaModuleCos self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);
    uint32 ssrc, ssrcTail, ssrcHead;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    mFieldGet(longRegVal[cThaCosEnquePseudowireRTPSSRCDwIndex],
              cThaCosEnquePseudowireRTPSSRCHwTailMask,
              cThaCosEnquePseudowireRTPSSRCHwTailShift,
              uint32,
              &ssrcTail);

    mFieldGet(longRegVal[cThaCosEnquePseudowireRTPSSRCDwIndex + 1],
              cThaCosEnquePseudowireRTPSSRCHwHeadMask,
              cThaCosEnquePseudowireRTPSSRCHwHeadShift,
              uint32,
              &ssrcHead);

    ssrc = ssrcHead;
    ssrc <<= cThaCosEnquePseudowireRTPSSRCSwHeadShift;
    ssrc |= (ssrcTail & cThaCosEnquePseudowireRTPSSRCSwTailMask);

    return ssrc;
    }

static uint8 HwValueBySequenceMdGet(eAtPwCwSequenceMode seqMd)
    {
    if (seqMd == cAtPwCwSequenceModeWrapZero) return 0;
    if (seqMd == cAtPwCwSequenceModeSkipZero) return 1;
    if (seqMd == cAtPwCwSequenceModeDisable)  return 2;

    return 0;
    }

static eAtRet PwCwSequenceModeSet(ThaModuleCos self, AtPw pw, eAtPwCwSequenceMode sequenceMode)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);

    if (mOutOfRange(sequenceMode, cAtPwCwSequenceModeWrapZero, cAtPwCwSequenceModeDisable))
        return cAtErrorInvlParm;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);
    if (sequenceMode != cAtPwCwSequenceModeDisable)
        {
        uint8 seqMd = HwValueBySequenceMdGet(sequenceMode);

        mFieldIns(&longRegVal[cThaCosEnquePseudowireSequenceZeroModeDwIndex],
                  cThaCosEnquePseudowireSequenceZeroModeMask,
                  cThaCosEnquePseudowireSequenceZeroModeShift,
                  0);
        mFieldIns(&longRegVal[cThaCosEnquePseudowireSeqCountModeDwIndex],
                  cThaCosEnquePseudowireSeqCountModeMask,
                  cThaCosEnquePseudowireSeqCountModeShift,
                  seqMd);
        }

    else
        mFieldIns(&longRegVal[cThaCosEnquePseudowireSequenceZeroModeDwIndex],
                  cThaCosEnquePseudowireSequenceZeroModeMask,
                  cThaCosEnquePseudowireSequenceZeroModeShift,
                  1);

    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    return cAtOk;
    }

static eBool PwCwSequenceModeIsSupported(ThaModuleCos self, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(self);
    if (mOutOfRange(sequenceMode, cAtPwCwSequenceModeWrapZero, cAtPwCwSequenceModeDisable))
        return cAtFalse;
    return cAtTrue;
    }

static eAtPwCwSequenceMode SequenceModeHw2Sw(uint8 value)
    {
    if (value == 0) return cAtPwCwSequenceModeWrapZero;
    if (value == 1) return cAtPwCwSequenceModeSkipZero;
    if (value == 2) return cAtPwCwSequenceModeDisable;

    return cAtPwCwSequenceModeWrapZero;
    }

static eAtPwCwSequenceMode PwCwSequenceModeGet(ThaModuleCos self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);
    uint8 hwSeqMd;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);
    mFieldGet(longRegVal[cThaCosEnquePseudowireSequenceZeroModeDwIndex],
              cThaCosEnquePseudowireSequenceZeroModeMask,
              cThaCosEnquePseudowireSequenceZeroModeShift,
              uint8,
              &hwSeqMd);

    if (hwSeqMd == 1)
        return cAtPwCwSequenceModeDisable;

    mFieldGet(longRegVal[cThaCosEnquePseudowireSeqCountModeDwIndex],
              cThaCosEnquePseudowireSeqCountModeMask,
              cThaCosEnquePseudowireSeqCountModeShift,
              uint8,
              &hwSeqMd);

    return SequenceModeHw2Sw(hwSeqMd);
    }

static eAtRet PwCwLengthModeSet(ThaModuleCos self, AtPw pw, eAtPwCwLengthMode lengthMode)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);

    if (mOutOfRange(lengthMode, cAtPwCwLengthModeFullPacket, cAtPwCwLengthModePayload))
        return cAtErrorInvlParm;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);
    mFieldIns(&longRegVal[cThaCosEnquePseudowireCWLenFieldModeDwIndex],
               cThaCosEnquePseudowireCWLenFieldModeMask,
               cThaCosEnquePseudowireCWLenFieldModeShift,
               (lengthMode == cAtPwCwLengthModeFullPacket) ? 0 : 1);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    return cAtOk;
    }

static eBool PwCwLengthModeIsSupported(ThaModuleCos self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(self);
    if (mOutOfRange(lengthMode, cAtPwCwLengthModeFullPacket, cAtPwCwLengthModePayload))
        return cAtFalse;
    return cAtTrue;
    }

static eAtPwCwLengthMode PwCwLengthModeGet(ThaModuleCos self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);
    uint8 bitVal;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);
    mFieldGet(longRegVal[cThaCosEnquePseudowireCWLenFieldModeDwIndex],
              cThaCosEnquePseudowireCWLenFieldModeMask,
              cThaCosEnquePseudowireCWLenFieldModeShift,
              uint8,
              &bitVal);

    return (bitVal == 0) ? cAtPwCwLengthModeFullPacket : cAtPwCwLengthModePayload;
    }

static eAtRet PwHeaderLengthSet(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);
    mFieldIns(&longRegVal[cThaCosEnquePseudowireHdrLenDwIndex],
              cThaCosEnquePseudowireHdrLenMask,
              cThaCosEnquePseudowireHdrLenShift,
              hdrLenInByte);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);

    return cAtOk;
    }

static uint8 PwHeaderLengthGet(ThaModuleCos self, AtPw pw)
    {
    uint8 hdrLenInByte;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + mPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCos);
    mFieldGet(longRegVal[cThaCosEnquePseudowireHdrLenDwIndex],
              cThaCosEnquePseudowireHdrLenMask,
              cThaCosEnquePseudowireHdrLenShift,
              uint8,
              &hdrLenInByte);

    return hdrLenInByte;
    }

static uint32 PwDefaultOffset(ThaModuleCos self, AtPw pw)
    {
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
    return mPwHwId(pw) + ThaModuleCosPartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    }

static uint32 BaseAddress(ThaModuleCos self)
    {
	AtUnused(self);
    return 0x3C0000;
    }

static eAtRet PwHeaderLengthReset(ThaModuleCos self, uint8 partId, uint32 localPwId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCosEnqueuePwCtrl + localPwId + ThaModuleCosPartOffset(self, partId);
    uint8 coreId = AtModuleDefaultCoreGet((AtModule)self);
    AtIpCore core = AtDeviceIpCoreGet((AtDevice)AtModuleDeviceGet((AtModule)self), coreId);
    mModuleHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[cThaCosEnquePseudowireHdrLenDwIndex], cThaCosEnquePseudowireHdrLen, 0);
    mModuleHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleCos self)
    {
    ThaModuleCosDefaultFifoAndQueSet(self);

    return ThaModuleCosAllPwsReset(self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;
    ThaModuleCos cosModule = (ThaModuleCos)self;

    ret  = m_AtModuleMethods->Init(self);
    ret |= mMethodsGet(cosModule)->DefaultSet(cosModule);

    return ret;
    }

static eAtRet AsyncDefaultSet(ThaModuleCos self)
    {
    return ThaModuleCosAsyncDefaultSetMain(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return ThaModuleCosAsyncInitMain((ThaModuleCos)self);
    }

static void Delete(AtObject self)
    {
    /* Delete private data */
    AtObjectDelete((AtObject)mThis(self)->ethPortController);
    mThis(self)->ethPortController = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    ThaModuleCos cosModule = (ThaModuleCos)self;
    uint32 baseAddress = mMethodsGet(cosModule)->BaseAddress(cosModule);

    /* 0x3C0000 -> 0x3CFFFF */
    if ((localAddress >= (0x0 + baseAddress)) && (localAddress <= (0xFFFF + baseAddress)))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    ThaModuleCos cosModule = (ThaModuleCos)self;
    uint32 baseAddress = mMethodsGet(cosModule)->BaseAddress(cosModule);
    static eBool initialized = cAtFalse;
    static uint32 holdRegisters[3];
    static const uint8 numHoldRegs = 3;
    uint8 i;

    /* Initialize hold registers */
    if (!initialized)
        {
        initialized = cAtTrue;
        for (i = 0; i < numHoldRegs; i++)
            holdRegisters[i] = 0x24000 + baseAddress + i;

        initialized = cAtTrue;
        }

    /* Number of hold registers */
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = numHoldRegs;

    return holdRegisters;
    }

static eAtRet Debug(AtModule self)
    {
    uint32 regVal;
    uint8 queue_i;

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "COS information\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");

    /* Show packet error status */
    regVal = mModuleHwRead(self, cThaRegCosDepacketBMViolationLengthModeStatus);
    AtPrintc(cSevNormal, "- Packet error status: ");
    if (regVal == 0)
        AtPrintc(cSevInfo, "none\r\n");
    else
        {
        if (regVal & cThaCosDepktBMOversizePacketErrorMask)
            AtPrintc(cSevCritical, "oversize ");
        if (regVal & cThaCosDepktBMUndersizePacketErrorMask)
            AtPrintc(cSevCritical, "undersize ");
        if (regVal & cThaCosDepktBMInvalidModeMask)
            AtPrintc(cSevCritical, "invalidMode ");
        AtPrintc(cSevNormal, "\r\n");
        }

    mModuleHwWrite(self, cThaRegCosDepacketBMViolationLengthModeStatus, regVal);

    /* Show request error status */
    AtPrintc(cSevNormal, "- Loss of start/end request error status:");
    regVal = mModuleHwRead(self, cThaRegCosDepacketBMLossOfStartEndRequestStatus);
    if (regVal == 0)
        AtPrintc(cSevInfo, " none\r\n");
    else
        {
        if (regVal & cThaCosDepktRequestLossofStartMask)
            AtPrintc(cSevCritical, " lossOfStart");
        if (regVal & cThaCosDepktRequestLossofEndMask)
            AtPrintc(cSevCritical, " lossOfEnd");
        AtPrintc(cSevNormal, "\r\n");
        }
    mModuleHwWrite(self, cThaRegCosDepacketBMLossOfStartEndRequestStatus, regVal);

    /* Show buffer accessing error status */
    AtPrintc(cSevNormal, "- Buffer access error status\r\n");
    for (queue_i = 0; queue_i < 2; queue_i++)
        {
        uint32 address;

        AtPrintc(cSevNormal, "  * Queue %d: ", queue_i + 1);
        address = (uint32)(cThaRegCosDepacketInfoBufferAccessingErrorsStatus + queue_i);
        regVal = mModuleHwRead(self, address);
        if (regVal == 0)
            AtPrintc(cSevInfo, "none\r\n");
        else
            {
            if (regVal & cThaCosDepktRequestInfoBufferReadingErrorMask)
                AtPrintc(cSevCritical, " readError");
            if (regVal & cThaCosDepktRequestInfoBufferWritingErrorMask)
                AtPrintc(cSevCritical, " writeError");
            if (regVal & cThaCosDepktRequestInfoBufferNotEmptyMask)
                AtPrintc(cSevCritical, " bufferNotEmpty");
            if (regVal & cThaCosDepktRequestInfoBufferFullMask)
                AtPrintc(cSevCritical, " bufferFull");
            AtPrintc(cSevNormal, "\r\n");
            }
        mModuleHwWrite(self, address, regVal);
        }

    return cAtOk;
    }

eAtRet ThaModuleCosPwHeaderLengthSet(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte)
    {
    if (self)
        return mMethodsGet(self)->PwHeaderLengthSet(self, pw, hdrLenInByte);
    return cAtError;
    }

static ThaCosEthPortController EthPortControllerCreate(ThaModuleCos self)
    {
    return ThaCosEthPortControllerNew(self);
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (mThis(self)->ethPortController == NULL)
        mThis(self)->ethPortController = mMethodsGet(mThis(self))->EthPortControllerCreate(mThis(self));

    return cAtOk;
    }

static eBool IsPwPart(ThaModuleCos self, uint8 partId)
    {
	AtUnused(partId);
	AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleCos object = (ThaModuleCos)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(ethPortController);
    mEncodeUInt(asyncInitState);
    mEncodeUInt(asyncInitDefaultInitState);
    mEncodeUInt(asyncInitAllPwResetInitState);
    mEncodeUInt(asyncInitAllPwResetInitPerPartState);
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "cos";
    }

static eAtRet PwHeaderLengthHwSet(ThaModuleCos self, AtPw pw, uint8 hwHdrLenValue)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(hwHdrLenValue);
    return cAtOk;
    }

static uint8 PwHeaderLengthHwGet(ThaModuleCos self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaModuleCos self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwRtpSsrcSet);
        mMethodOverride(m_methods, PwRtpPayloadTypeSet);
        mMethodOverride(m_methods, PwRtpPayloadTypeGet);
        mMethodOverride(m_methods, PwRtpSsrcGet);
        mMethodOverride(m_methods, PwCwSequenceModeGet);
        mMethodOverride(m_methods, PwCwLengthModeSet);
        mMethodOverride(m_methods, PwCwLengthModeGet);
        mMethodOverride(m_methods, PwCwLengthModeIsSupported);
        mMethodOverride(m_methods, PwCwSequenceModeSet);
        mMethodOverride(m_methods, PwCwSequenceModeIsSupported);
        mMethodOverride(m_methods, PwHeaderLengthSet);
        mMethodOverride(m_methods, PwHeaderLengthGet);
        mMethodOverride(m_methods, PwHeaderLengthHwSet);
        mMethodOverride(m_methods, PwHeaderLengthHwGet);
        mMethodOverride(m_methods, PwDefaultOffset);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, AsyncDefaultSet);
        mMethodOverride(m_methods, PwHeaderLengthReset);
        mMethodOverride(m_methods, EthPortControllerCreate);
        mMethodOverride(m_methods, IsPwPart);
        }

    mMethodsSet(self, &m_methods);
    }

AtModule ThaModuleCosObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModuleCos));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModuleCos, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaModuleCos)self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleCosNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModuleCos));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleCosObjectInit(newModule, device);
    }

static eAtRet PartPwsReset(ThaModuleCos self, uint8 partId)
    {
    uint32 pw_i;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(self)->IsPwPart(self, partId))
        return cAtOk;

    for (pw_i = 0; pw_i < ThaModulePwNumPwsPerPart(ThaModuleCosPwModule(self)); pw_i++)
        ret |= mMethodsGet(self)->PwHeaderLengthReset(self, partId, pw_i);

    return ret;
    }

eAtRet ThaModuleCosAllPwsReset(ThaModuleCos self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    eAtRet ret = cAtOk;
    uint8 part_i;

    for (part_i = 0; part_i < ThaDeviceNumPartsOfModule(device, cThaModuleCos); part_i++)
        ret |= PartPwsReset(self, part_i);

    return ret;
    }

uint32 ThaModuleCosEthPortOutcomingPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOutcomingPktRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortOutcomingbyteRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOutcomingbyteRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortOamTxPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOamTxPktRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortOamFrequencyTxPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerOamFrequencyTxPktRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPWTxPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPWTxPktRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktLensmallerthan64bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLensmallerthan64bytesRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktLenfrom65to127bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom65to127bytesRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktLenfrom128to255bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom128to255bytesRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktLenfrom256to511bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom256to511bytesRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktLenfrom512to1024bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom512to1024bytesRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktLenfrom1025to1528bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktLenfrom1025to1528bytesRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktJumboLenRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktJumboLenRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktTimePktToPRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerPktTimePktToPRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosPartOffset(ThaModuleCos self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleCos, partId);
    }

ThaCosEthPortController ThaModuleCosEthPortControllerGet(ThaModuleCos self)
    {
    if (self == NULL)
        return NULL;

    if (self->ethPortController == NULL)
        self->ethPortController = mMethodsGet(self)->EthPortControllerCreate(self);

    return self->ethPortController;
    }

uint32 ThaModuleCosEthPortPktLenfrom1529to2047bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerEthPortPktLenfrom1529to2047bytesRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktUnicastPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerEthPortPktUnicastPktRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktBcastPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerEthPortPktBcastPktRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

uint32 ThaModuleCosEthPortPktMcastPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c)
    {
    return ThaCosEthPortControllerEthPortPktMcastPktRead2Clear(ThaModuleCosEthPortControllerGet(self), port, r2c);
    }

eBool ThaModuleCosPwCwSequenceModeIsSupported(ThaModuleCos self, eAtPwCwSequenceMode sequenceMode)
    {
    if (self)
        return mMethodsGet(self)->PwCwSequenceModeIsSupported(self, sequenceMode);
    return cAtFalse;
    }

eBool ThaModuleCosPwCwLengthModeIsSupported(ThaModuleCos self, eAtPwCwLengthMode lengthMode)
    {
    if (self)
        return mMethodsGet(self)->PwCwLengthModeIsSupported(self, lengthMode);
    return cAtFalse;
    }

ThaModulePw ThaModuleCosPwModule(ThaModuleCos self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

eAtRet ThaModuleCosSuperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

eAtRet ThaModuleCosDefaultAsyncSet(ThaModuleCos self)
    {
    return mMethodsGet(self)->AsyncDefaultSet(self);
    }

void ThaModuleCosDefaultFifoAndQueSet(ThaModuleCos self)
    {
    uint32 regVal, regAddr;
    uint8 wQueId;

    /* Reset COS De-packet FIFO Thresholds Control */
    regAddr = cThaRegCosDepacketFifoThressCtrl;
    regVal  = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cThaCosDepktDataInfoBufWritingThresMask, cThaCosDepktDataInfoBufWritingThresShift, 64);
    mFieldIns(&regVal, cThaCosDepktDataInfoBufReadingThresMask, cThaCosDepktDataInfoBufReadingThresShift, 500);
    mModuleHwWrite(self, regAddr, regVal);

    /* Set default configuration for queues */
    for (wQueId = 0; wQueId < 8; wQueId++)
        {
        regAddr = (uint32)(cThaRegCosEnqueueRouteMapCtrl + wQueId);
        regVal  = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, cThaCosEnqueGigabitPortIDMask, cThaCosEnqueGigabitPortIDShift, 0);
        mFieldIns(&regVal, cThaCosEnqueDataSourceMask, cThaCosEnqueDataSourceShift, 0);
        mModuleHwWrite(self, regAddr, regVal);
        }

    }

uint8 ThaModuleCosPwHeaderLengthGet(ThaModuleCos self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwHeaderLengthGet(self, pw);
    return 0;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : COS (internal module)
 * 
 * File        : ThaModuleCos.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECOS_H_
#define _THAMODULECOS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleCos * ThaModuleCos;

typedef struct tThaCosController        *ThaCosController;
typedef struct tThaCosEthPortController *ThaCosEthPortController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Methods of COS go here */
AtModule ThaModuleCosNew(AtDevice device);
AtModule ThaModuleCosPppNew(AtDevice device);
AtModule ThaModuleCosV2New(AtDevice device);

eAtRet ThaModuleCosAllPwsReset(ThaModuleCos self);

uint32 ThaModuleCosEthPortOutcomingPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortOutcomingbyteRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortOamTxPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortOamFrequencyTxPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPWTxPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktLensmallerthan64bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktLenfrom65to127bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktLenfrom128to255bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktLenfrom256to511bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktLenfrom512to1024bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktLenfrom1025to1528bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktJumboLenRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktTimePktToPRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktLenfrom1529to2047bytesRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktUnicastPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktBcastPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
uint32 ThaModuleCosEthPortPktMcastPktRead2Clear(ThaModuleCos self, AtEthPort port, eBool r2c);
eBool ThaModuleCosPwCwSequenceModeIsSupported(ThaModuleCos self, eAtPwCwSequenceMode sequenceMode);
eBool ThaModuleCosPwCwLengthModeIsSupported(ThaModuleCos self, eAtPwCwLengthMode lengthMode);

uint32 ThaModuleCosPartOffset(ThaModuleCos self, uint8 partId);
ThaCosEthPortController ThaModuleCosEthPortControllerGet(ThaModuleCos self);

eAtRet ThaModuleCosPwHeaderLengthSet(ThaModuleCos self, AtPw pw, uint8 hdrLenInByte);
uint8 ThaModuleCosPwHeaderLengthGet(ThaModuleCos self, AtPw pw);

/* Product concretes */
AtModule Tha60030080ModuleCosNew(AtDevice device);
AtModule Tha60060011ModuleCosNew(AtDevice device);
AtModule Tha60031033ModuleCosNew(AtDevice device);
AtModule Tha60091023ModuleCosNew(AtDevice device);
AtModule Tha60210011ModuleCosNew(AtDevice device);
AtModule Tha60210012ModuleCosNew(AtDevice device);
AtModule Tha60290022ModuleCosNew(AtDevice device);
AtModule Tha60290081ModuleCosNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECOS_H_ */


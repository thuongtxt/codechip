/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA (internal module)
 * 
 * File        : ThaModulePda.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPDA_H_
#define _THAMODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwHdlc.h"
#include "AtPwCESoP.h"
#include "../pw/ThaPwToh.h"
#include "../pw/adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePda * ThaModulePda;
typedef struct tThaModulePdaV2 * ThaModulePdaV2;

enum eThaPdaJitterBufferState
    {
    cThaPdaJitterBufferStateStart   = 0,
    cThaPdaJitterBufferStateFilling = 1
    };

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModulePdaNew(AtDevice device);
AtModule ThaModulePdaV2New(AtDevice device);
AtModule ThaStmPwModulePdaV2New(AtDevice device);

uint32 ThaModulePdaPwLopsThresholdMin(ThaModulePda self, AtPw adapter);
uint32 ThaModulePdaPwLopsThresholdMax(ThaModulePda self, AtPw adapter);
eAtModulePwRet ThaModulePdaPwLopsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets);
eAtRet ThaModulePdaPwLopsClearThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets);
uint32 ThaModulePdaPwLopsThresholdMax(ThaModulePda self, AtPw adapter);
uint32 ThaModulePdaPwLopsThresholdMin(ThaModulePda self, AtPw adapter);
uint32 ThaModulePdaBaseAddress(ThaModulePda self);

eAtModulePwRet ThaModulePdaPwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets);
uint32 ThaModulePdaPwLofsSetThresholdGet(ThaModulePda self, AtPw pw);

eAtRet ThaModulePdaPwCwSequenceModeSet(ThaModulePda self, AtPw pw, eAtPwCwSequenceMode seqMd);
eAtRet ThaModulePdaPwJitterBufferDelaySet(ThaModulePda self, AtPw pw, uint32 microseconds);
eAtRet ThaModulePdaPwJitterBufferSizeSet(ThaModulePda self, AtPw pw, uint32 microseconds);
eAtRet ThaModulePdaPwReoderingEnable(ThaModulePda self, AtPw pw, eBool enable);
eBool ThaModulePdaPwReorderingIsEnable(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint32 ThaModulePdaCEPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint32 ThaModulePdaCESoPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames);
uint32 ThaModulePdaMaxJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint32 ThaModulePdaMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint32 ThaModulePdaCEPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint32 ThaModulePdaCESoPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames);
uint32 ThaModulePdaMaxJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
uint32 ThaModulePdaPwJitterBufferSizeGet(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaPwJitterBufferDelayGet(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaMaxJitterDelayForJitterBuffer(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us);

eAtRet ThaModulePdaPwCenterJitterEnable(ThaModulePda self, AtPw pw, eBool enable);
eAtRet ThaModulePdaPwCenterJitterStart(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaPwCenterJitterStop(ThaModulePda self, AtPw pw);

uint16 ThaModulePdaCEPMaxPayloadSize(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaCEPMinPayloadSize(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaCESoPMaxPayloadSize(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaCESoPMinPayloadSize(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaCESoPMaxNumFrames(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaCESoPMinNumFrames(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaSAToPMaxPayloadSize(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaSAToPMinPayloadSize(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaPwMinPayloadSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize);
uint32 ThaModulePdaPwMaxPayloadSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize);
eBool ThaModulePdaCEPPayloadSizeIsValid(ThaModulePda self, AtPw pw, uint16 payloadSize);

eAtRet ThaModulePdaHotConfigureStart(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaHotConfigureStop(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaJitterBufferEmptyWait(ThaModulePda self, AtPw pw);

uint32 ThaModulePdaMinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaMaxNumPacketsForJitterDelay(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaCEPMinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw);
eBool ThaModulePdaPwJitterBufferSizeIsValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs);
eBool ThaModulePdaPwJitterDelayIsValid(ThaModulePda self, AtPw pw, uint32 jitterDelayUs);
eBool ThaModulePdaPwJitterBufferAndJitterDelayAreValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs);
eBool ThaModulePdaShouldSetDefaultJitterBufferSizeOnJitterDelaySet(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaDefaultJitterBufferSize(ThaModulePda self, AtPw pw, uint32 jitterDelayUs);
uint32 ThaModulePdaPwNumPacketsByUsAndPayload(ThaModulePda self, AtPw pw, uint32 bufferSizeInUs, uint32 payloadSizeInByte);

uint32 ThaModulePdaMaxNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize);
uint32 ThaModulePdaMinNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize);
uint32 ThaModulePdaMaxNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize);
uint32 ThaModulePdaMinNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize);
eBool ThaModulePdaCepPayloadSizeShouldExcludeHeader(ThaModulePda self);
eAtRet ThaModulePdaCepIndicate(ThaModulePda self, AtPw pw, eBool isCep);
eBool ThaModulePdaNeedPreventReconfigure(ThaModulePda self);

eAtRet ThaModulePdaPwJitterBufferSizeInNumOfPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets);
eAtRet ThaModulePdaPwJitterBufferDelayInNumOfPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets);
uint16 ThaModulePdaPwJitterBufferSizeInPacketGet(ThaModulePda self, AtPw pw);
uint16 ThaModulePdaPwJitterBufferDelayInPacketGet(ThaModulePda self, AtPw pw);

uint32 ThaModulePdaJitterBufferWatermarkMinPackets(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaJitterBufferWatermarkMaxPackets(ThaModulePda self, AtPw pw);
eBool ThaModulePdaJitterBufferWatermarkIsSupported(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaJitterBufferWatermarkReset(ThaModulePda self, AtPw pw);

uint16 ThaModulePdaCalculateNumPacketsByUs(ThaModulePda self, AtPw pw, uint32 us);
uint32 ThaModulePdaCalculateUsByNumberOfPacket(ThaModulePda self, AtPw pw, uint32 numPkts);

eAtModulePwRet ThaModulePdaPwIdleCodeSet(ThaModulePda self, ThaPwAdapter pwAdapter, uint8 idleCode);
uint8  ThaModulePdaPwIdleCodeGet(ThaModulePda self, ThaPwAdapter pwAdapter);

eAtRet ThaModulePdaPwCircuitSliceSet(ThaModulePda self, AtPw pw, uint8 slice);
void ThaModulePdaPwJitterPayloadLengthSet(ThaModulePda self, AtPw pw, uint32 payloadSizeInBytes);
eAtRet ThaModulePdaPwNumDs0TimeslotsSet(ThaModulePda self, AtPw pw, uint16 numTimeslots);
eAtRet ThaModulePdaPwPdaModeSet(ThaModulePda self, AtPw pw, uint8 mode);
uint8 ThaModulePdaPwPdaModeGet(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaPwCircuitTypeSet(ThaModulePda self, AtPw pw, AtChannel circuit);

/* For CEP */
eAtRet ThaModulePdaPwCircuitIdSet(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaPwCircuitIdReset(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaPwPdaTdmLookupEnable(ThaModulePda self, AtPw pw, eBool enable);
eAtRet ThaModulePdaVcDeAssemblerSet(ThaModulePda self, AtSdhChannel vc);
eAtRet ThaModulePdaPwCepEquip(ThaModulePda self, AtPw pw, AtChannel subChannel, eBool equip);
eAtRet ThaModulePdaPwCepEbmEnable(ThaModulePda self, AtPw pw, eBool enable);
eAtRet ThaModulePdaPwEparEnable(ThaModulePda self, AtPw pwAdapter, eBool enable);
eBool ThaModulePdaPwEparIsEnabled(ThaModulePda self, AtPw pwAdapter);

/* For TOH */
eAtRet ThaModulePdaBindLineToPw(ThaModulePda self, AtSdhLine line, AtPw pw);
eAtRet ThaModulePdaPwTohByteEnable(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
eAtRet ThaModulePdaPwTohFirstByteEnable(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
eAtRet ThaModulePdaPwTohEnable(ThaModulePda self, AtPw pw, eBool enable);
tThaTohByte* ThaModulePdaPwTohFirstByteGet(ThaModulePda self, AtPw pw, tThaTohByte *tohByte);
uint8 ThaModulePdaPwTohBytesBitmapGet(ThaModulePda self, AtPw pw, uint32 *bitmaps, uint8 numBitmap);

/* Part management */
uint32 ThaModulePdaPartOffset(ThaModulePda self, uint8 partId);

/* Counters */
uint32 ThaModulePdaPwRxJitBufOverrunGet(ThaModulePda self, AtPw pw, eBool clear);
uint32 ThaModulePdaPwRxJitBufUnderrunGet(ThaModulePda self, AtPw pw, eBool clear);
uint32 ThaModulePdaPwRxLopsGet(ThaModulePda self, AtPw pw, eBool clear);
uint32 ThaModulePdaPwRxPacketsSentToTdmGet(ThaModulePda self, AtPw pw, eBool clear);
eBool ThaModulePdaPwRxPacketsSentToTdmCounterIsSupported(ThaModulePda self);
uint32 ThaModulePdaNumCurrentPacketsInJitterBuffer(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaNumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pwAdapter);
uint32 ThaModulePdaPwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear);
uint32 ThaModulePdaPwRxReorderedPacketsGet(ThaModulePda self, AtPw pw, eBool clear);
uint32 ThaModulePdaPwRxLostPacketsGet(ThaModulePda self, AtPw pw, eBool clear);
uint32 ThaModulePdaPwRxOutOfSeqDropedPacketsGet(ThaModulePda self, AtPw pw, eBool clear);

/* Alarm */
uint32 ThaModulePdaPwAlarmGet(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaPwDefectHistoryGet(ThaModulePda self, AtPw pw, eBool read2Clear);

eBool ThaModulePdaPwLopsThresholdIsValid(ThaModulePda self, AtPw pw, uint32 numPackets);
uint32 ThaModulePdaPwLopsSetThresholdGet(ThaModulePda self, AtPw pw);
uint32 ThaModulePdaPwLopsClearThresholdGet(ThaModulePda self, AtPw pw);

eAtRet ThaModulePdaPwCwPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode);
eAtPwPktReplaceMode ThaModulePdaPwCwPktReplaceModeGet(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaPwLopsPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode);
eAtPwPktReplaceMode ThaModulePdaPwLopsPktReplaceModeGet(ThaModulePda self, AtPw pw);
eBool ThaModulePdaPwCanControlLopsPktReplaceMode(ThaModulePda self);
eBool ThaModulePdaPwPktReplaceModeIsSupported(ThaModulePda self, eAtPwPktReplaceMode pktReplaceMode);
uint8 ThaModulePdaLbitPktReplaceModeSw2Hw(eAtPwPktReplaceMode pktReplaceMode);
uint8 ThaModulePdaLbitPktReplaceModeHw2Sw(eAtPwPktReplaceMode pktReplaceMode);
eAtModulePwRet ThaModulePdaDefaultPwCwPktReplaceModeV1Set(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode);
eAtPwPktReplaceMode ThaModulePdaDefaultPwCwPktReplaceModeV1Get(ThaModulePda self, AtPw pw);

/* Auto-RDI */
eAtRet ThaModulePdaAutoInsertRdiEnable(ThaModulePda self, AtPw pw, eBool enable);
eBool ThaModulePdaAutoInsertRdiIsEnabled(ThaModulePda self, AtPw pw);
eBool ThaModulePdaShouldAutoInsertRdi(ThaModulePda self);

/* To Control Rx Mbit Auto */
eBool ThaModulePdaPwCwAutoRxMBitIsConfigurable(ThaModulePda self);
eBool ThaModulePdaPwCwAutoRxMBitIsEnabled(ThaModulePda self, AtPw pw);
eAtRet ThaModulePdaPwCwAutoRxMBitEnable(ThaModulePda self, AtPw pw, eBool enable);

/* For debugging */
void ThaModulePdaPwDebug(ThaModulePda self, AtPw pw);
void ThaModulePdaPwRegsShow(AtPw pw);
void ThaModulePdaHdlcChannelRegsShow(AtHdlcChannel channel);
void ThaModulePdaEthFlowRegsShow(AtEthFlow flow);

/* For PRBS PSN channel ID*/
uint32 ThaModulePdaPwTdmOffset(ThaModulePda self, AtPw pw);

/* Jitter buffer limitation */
eBool ThaModulePdaResourceIsAvailable(ThaModulePda self, AtPw adapter, uint32 newOccupiedBlock);
eAtRet ThaModulePdaTotalInUsedRamBlockUpdate(ThaModulePda self, AtPw adapter, uint32 newOccupiedBlock);
uint32 ThaModulePdaPwNumBlockByBufferInUs(ThaModulePda self, AtPw adapter, uint32 bufferSizeInUs, uint32 payloadSizeInByte);
uint32 ThaModulePdaPwNumBlockByBufferInPkt(ThaModulePda self, AtPw adapter, uint32 bufferSizeInPkt, uint32 payloadSizeInByte);
uint32 ThaModulePdaNumUnUsedBlocks(ThaModulePda self);
uint32 ThaModulePdaMaxNumJitterBufferBlocks(ThaModulePda self);
uint32 ThaModulePdaJitterBufferBlockSizeInByte(ThaModulePda self);
eBool ThaModulePdaPwVariantJitterBufferSizeIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds);
eBool ThaModulePdaPwVariantJitterBufferDelayIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds);

/* Types */
eAtRet ThaModulePdaHdlcPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
eAtRet ThaModulePdaMlpppPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
eAtRet ThaModulePdaPwTohModeSet(ThaModulePda self, ThaPwAdapter adapter);
eAtRet ThaModulePdaSAToPModeSet(ThaModulePda self, ThaPwAdapter adapter, AtChannel circuit);
eAtRet ThaModulePdaCESoPModeSet(ThaModulePda self, ThaPwAdapter adapter);
eAtRet ThaModulePdaCESoPCasModeSet(ThaModulePda self, ThaPwAdapter adapter, eAtPwCESoPCasMode mode);
eAtPwCESoPCasMode ThaModulePdaCESoPCasModeGet(ThaModulePda self, ThaPwAdapter adapter);
eAtRet ThaModulePdaCepModeSet(ThaModulePda self, ThaPwAdapter adapter, AtSdhChannel sdhVc);

/* Registers */
uint32 ThaModulePdaPrbsStatusRegister(ThaModulePda self);
uint32 ThaModulePdaV2JitBufCtrlAddress(ThaModulePdaV2 self, AtPw pw);

/* Product concretes */
AtModule Tha60030111ModulePdaNew(AtDevice device);
AtModule Tha60031021ModulePdaNew(AtDevice device);
AtModule Tha60091023ModulePdaNew(AtDevice device);
AtModule Tha60031031ModulePdaNew(AtDevice device);
AtModule Tha60150011ModulePdaNew(AtDevice device);
AtModule Tha60210011ModulePdaNew(AtDevice device);
AtModule ThaStmPwProductModulePdaNew(AtDevice device);
AtModule ThaPdhPwProductModulePdaNew(AtDevice device);
AtModule Tha60210021ModulePdaNew(AtDevice device);
AtModule Tha60210031ModulePdaNew(AtDevice device);
AtModule Tha60210051ModulePdaNew(AtDevice device);
AtModule Tha60210012ModulePdaNew(AtDevice device);
AtModule Tha60210061ModulePdaNew(AtDevice device);
AtModule Tha60290011ModulePdaNew(AtDevice device);
AtModule Tha60290022ModulePdaNew(AtDevice device);
AtModule Tha60291011ModulePdaNew(AtDevice device);
AtModule Tha60291022ModulePdaNew(AtDevice device);
AtModule Tha60290081ModulePdaNew(AtDevice device);
AtModule Tha6A290081ModulePdaNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPDA_H_ */


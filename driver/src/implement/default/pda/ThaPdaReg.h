/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : ThaPdaReg.h
 * 
 * Created Date: Nov 19, 2012
 *
 * Description : PDA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDAREG_H_
#define _THAPDAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name: PwPda Rx Buffer Management Thresholds Control
Reg Addr: 0x24E000 - 0x24E3FF
          The address format for these registers is 0x24E000 + PWID
          Where: PWID (0 � 1023) Pseudowire Identification
Reg Desc: The register configure thresholds for RX pseudowires. The
          JitterBufferSize, PwRate   and PayloadSize variables are used to
          calculate RxBuffNumAct(similar to PDV) of TDM pseudowire
          JitterBufferSize: Size of jitter buffer performed by number of
          milisecond, this variable is applied only for TDM pseudowire.
          Maximum value of Packet delay variation(PDV)  is often equal to a
          half of JitterBufferSize PwRate: Rate of pseudowire performed by
          number of byte per 1 milisecond. PayloadSize: PayloadSize of TDM
          pseudowire packet performed by number of byte RxBuffNumAct=
          (JitterBufferSize*PwRate)/(2*PayloadSize) +
          (JitterBufferSize*PwRate)%(2*PayloadSize)
------------------------------------------------------------------------------*/
#define cThaRegPwPdaRxBufManmentThressCtrl   (0x24E000)

/*--------------------------------------
BitField Name: RxBuffEnterLOFS
BitField Type: R/W
BitField Desc: Number of consecutive lost TDM packets to enter lost of frame
               state(LOFS)
--------------------------------------*/
#define cThaRxBuffEnterLofSMask                        cBit25_21
#define cThaRxBuffEnterLofSShift                       21

/*--------------------------------------
BitField Name: RxBuffExitLOFS
BitField Type: R/W
BitField Desc: Number of consecutive good TDM packets to exit lost of frame
               state(LOFS)
--------------------------------------*/
#define cThaRxBuffExitLofSMask                         cBit20_16
#define cThaRxBuffExitLofSShift                        16

/*--------------------------------------
BitField Name: RxBuffNumAct
BitField Type: R/W
BitField Desc: Number of  TDM packets stored in queue (also pseudowire) at the
               first time before sending out to prevent jitter.
--------------------------------------*/
#define cThaRxBuffNumActMask                           cBit15_0
#define cThaRxBuffNumActShift                          0

/*------------------------------------------------------------------------------
Reg Name: PwPda Rx Buffer Management Initialized  Read Only
Reg Addr: 0x252001
Reg Desc: The read only register is used to monitor initializing process.
------------------------------------------------------------------------------*/
#define cThaRegPwPdaRxBufManmentInitializedReadOnly (0x252001)

/*--------------------------------------
BitField Name: RxBuffInitState
BitField Type: R_O
BitField Desc:
               - 0: Engine is not in initialized state. CPU can write a new
                 initialized command
               - 1: Engine is in initialized state. All initialized commands
                 received from CPU will be ignorged.
--------------------------------------*/
#define cThaRxBuffInitStateMask                        cBit0
#define cThaRxBuffInitStateShift                       0

/*------------------------------------------------------------------------------
Reg Name: PwPda Reorder Timeout Control
Reg Addr: 0x244000 - 0x2443FF
          The address format for these registers is 0x244000 + PWID
          Where: PWID (0 � 1023) Pseudowire Identification
Reg Desc: The register is used to configure reordering packet timeout. The
          JitterBufferSize, PwRate   and PayloadSize variables are used to
          calculate PDAReorderTimeout JitterBufferSize: Size of jitter buffer
          performed by number of milisecond, this variable is applied only for
          TDM pseudowire. Maximum value of Packet delay variation(PDV)  is
          often equal to a half of JitterBufferSize PwRate: Rate of pseudowire
          performed by number of byte per 1 milisecond. PayloadSize:
          PayloadSize of TDM pseudowire packet performed by number of byte
          JitterTimeout = (JitterBufferSize/2) � (PayloadSize/PwRate) �
          (PayloadSize%PwRate) SeqTimeout = (32*PayloadSize)/PwRate +
          (32*PayloadSize)%PwRate For TDM Pseudowire: PDAReorderTimeout =
          Min(JitterTimeout,SeqTimeout,255) For ATM Pseudowire:
          PDAReorderTimeout = Min(SeqTimeout,255)
------------------------------------------------------------------------------*/
#define cThaRegPwPdaReorderTimeoutCtrl       (0x244000)

/*
#define cThaUnusedMask                                 cBit31_9
#define cThaUnusedShift                                9
#define cThaUnusedRstVal                               0x0
*/

/*--------------------------------------
BitField Name: PDAReorderSeqMode
BitField Type: R/W
BitField Desc: Pseudowire reordering sequence number mode
               - 1: Sequence number will roll from 65535 to 1, this mode is
                 often used for ATM pseudowire
               - 0: Sequence number will roll from 65535 to 0, this mode is
                 often used for TDM pseudowire
--------------------------------------*/
#define cThaPDAReorderSeqModeMask                      cBit8
#define cThaPDAReorderSeqModeShift                     8

/*--------------------------------------
BitField Name: PDAReorderTimeout
BitField Type: R/W
BitField Desc: Maximum number of milliseconds that engine can wait for the
               arrival of a packet before declaring it as a lost one. When a
               consecutive 16 timeout events is detected, the pseudowire will
               enter lost of frame state (LOFS)
               - 0: The pseudowire is disabled, all of packets received from
                 PSN which belong to the pseudowire are discarded regardless of
                 reordering enable configuration at CLASSIFY function.
               - 1: Invalid value because step to update is every 1 miliseconds
               - 2 to 255: Valid value. This value must be smaller than the
                 packet delay variation (PDV)
--------------------------------------*/
#define cThaPDAReorderTimeoutMask                      cBit7_0
#define cThaPDAReorderTimeoutShift                     0

/*------------------------------------------------------------------------------
Reg Name: PwPda Rx Buffer Management Memory Allocation Control
Reg Addr: 0x24E400 - 0x24E7FF
          The address format for these registers is 0x24E400 + PWID
          Where: PWID (0 � 1023) Pseudowire Identification
Reg Desc: The register configure memory allocation for Rx queue (pseudowire).
          The RxBuffMaxBlk represents for jitter buffer size. The formular to
          calculate is as follow: RxBuffMaxBlk = 2*RxBuffNumAct *
          (PayloadSize/64 + PayloadSize%64)
------------------------------------------------------------------------------*/
#define cThaRegPwPdaRxBufManmentMemoryAllocationCtrl (0x24E400)

/*--------------------------------------
BitField Name: RxBuffMaxBlk
BitField Type: R/W
BitField Desc: Maximum number of 64-byte blocks (in total of 970752 blocks)
               allocated for the queue.
--------------------------------------*/
#define cThaRxBuffMaxBlkMask                           cBit15_0
#define cThaRxBuffMaxBlkShift                          0

/*------------------------------------------------------------------------------
Reg Name: PwPda Rx Buffer Management Initialized Control
Reg Addr: 0x252000
Reg Desc: The register initializes new configurations of one queue without
          affecting other running queues. Write this register when Payload
          De-Assembler has been already activated will start initializing.
------------------------------------------------------------------------------*/
#define cThaRegPwPdaRxBufManmentInitializedCtrl     (0x252000)

#define cThaRegPwPdaRxBufManmentInitializedCtrl1RstVal 0x00000000
#define cThaRegPwPdaRxBufManmentInitializedCtrl0RstVal 0x00000000

#define cThaRegPwPdaRxBufManmentInitializedCtrl1RwMsk 0x1FFFFFFF
#define cThaRegPwPdaRxBufManmentInitializedCtrl0RwMsk 0xFFFFFFFF

#define cThaRegPwPdaRxBufManmentInitializedCtrl1RwcMsk 0x00000000
#define cThaRegPwPdaRxBufManmentInitializedCtrl0RwcMsk 0x00000000

#define cThaRegPwPdaRxBufManmentInitializedCtrlDwMin 0
#define cThaRegPwPdaRxBufManmentInitializedCtrlDwMax 1
#define cThaRegPwPdaRxBufManmentInitializedCtrlDwNum 2

/*--------------------------------------
BitField Name: InitPDAReorderSeqMode
BitField Type: R/W
BitField Desc: Identical to PDAReorderSeqMode  in 4.11.4
--------------------------------------*/
#define cThaInitPDAReorderSeqModeMask                  cBit28 /* cBit60 */
#define cThaInitPDAReorderSeqModeShift                 28
#define cThaInitPDAReorderSeqModeMaxVal                0x1
#define cThaInitPDAReorderSeqModeMinVal                0x0
#define cThaInitPDAReorderSeqModeNumDw                 1
#define cThaInitPDAReorderSeqModeDwIndex               1
#define cThaInitPDAReorderSeqModeRstVal                0x0

/*--------------------------------------
BitField Name: InitPDAReorderTimeout
BitField Type: R/W
BitField Desc: Identical to PDAReorderTimeout  in 4.11.4
--------------------------------------*/
#define cThaInitPDAReorderTimeoutMask                  cBit27_20 /* cBit59_52 */
#define cThaInitPDAReorderTimeoutShift                 20
#define cThaInitPDAReorderTimeoutMaxVal                0xFF
#define cThaInitPDAReorderTimeoutMinVal                0x0
#define cThaInitPDAReorderTimeoutNumDw                 1
#define cThaInitPDAReorderTimeoutDwIndex               1
#define cThaInitPDAReorderTimeoutRstVal                0x0

/*--------------------------------------
BitField Name: InitRxBuffEnterLOFS
BitField Type: R/W
BitField Desc: Identical to RxBuffEnterLOFS in 4.11.10
--------------------------------------*/
#define cThaInitRxBuffEnterLofSMask                    cBit19_15 /* cBit51_47 */
#define cThaInitRxBuffEnterLofSShift                   15
#define cThaInitRxBuffEnterLofSMaxVal                  0x1F
#define cThaInitRxBuffEnterLofSMinVal                  0x0
#define cThaInitRxBuffEnterLofSNumDw                   1
#define cThaInitRxBuffEnterLofSDwIndex                 1
#define cThaInitRxBuffEnterLofSRstVal                  0x0

/*--------------------------------------
BitField Name: InitRxBuffExitLOFS
BitField Type: R/W
BitField Desc: Identical to RxBuffExitLOFS in 4.11.10
--------------------------------------*/
#define cThaInitRxBuffExitLofSMask                     cBit14_10 /* cBit46_42 */
#define cThaInitRxBuffExitLofSShift                    10
#define cThaInitRxBuffExitLofSMaxVal                   0x1F
#define cThaInitRxBuffExitLofSMinVal                   0x0
#define cThaInitRxBuffExitLofSNumDw                    1
#define cThaInitRxBuffExitLofSDwIndex                  1
#define cThaInitRxBuffExitLofSRstVal                   0x0

/*--------------------------------------
BitField Name: InitRxBuffNumAct
BitField Type: R/W
BitField Desc: Identical to RxBuffNumAct  in 4.11.10
--------------------------------------*/
#define cThaInitRxBuffNumAct0HwHeadMask                cBit9_0
#define cThaInitRxBuffNumAct0HwHeadShift               0
#define cThaInitRxBuffNumAct0HwTailMask                cBit31_26
#define cThaInitRxBuffNumAct0HwTailShift               26
#define cThaInitRxBuffNumAct0SwHeadMask                cBit15_6
#define cThaInitRxBuffNumAct0SwHeadShift               6
#define cThaInitRxBuffNumAct0SwTailMask                cBit5_0
#define cThaInitRxBuffNumAct0SwTailShift               0
#define cThaInitRxBuffNumActMaxVal                     0xFFFF
#define cThaInitRxBuffNumActMinVal                     0x0
#define cThaInitRxBuffNumActNumDw                      1
#define cThaInitRxBuffNumActDwIndex                    0
#define cThaInitRxBuffNumActRstVal                     0x0

/*--------------------------------------
BitField Name: InitRxBuffMaxBlk
BitField Type: R/W
BitField Desc: Identical to RxBuffMaxBlk in 4.11.11
--------------------------------------*/
#define cThaInitRxBuffMaxBlkMask                       cBit25_10
#define cThaInitRxBuffMaxBlkShift                      10
#define cThaInitRxBuffMaxBlkMaxVal                     0xFFFF
#define cThaInitRxBuffMaxBlkMinVal                     0x0
#define cThaInitRxBuffMaxBlkNumDw                      1
#define cThaInitRxBuffMaxBlkDwIndex                    0
#define cThaInitRxBuffMaxBlkRstVal                     0x0

/*--------------------------------------
BitField Name: Initreq
BitField Type: R/W
BitField Desc: Set 1 to request JB auto clear buffer,
               then monitor 0x252001 to know when JB finish clear.
               Set 0 to disable request JB clear
--------------------------------------*/
#define cThaInitReqMask                       cBit12
#define cThaInitReqShift                      12
#define cThaInitReqMaxVal                     0x1
#define cThaInitReqMinVal                     0x0
#define cThaInitReqNumDw                      1
#define cThaInitReqDwIndex                    0
#define cThaInitReqRstVal                     0x0

/*--------------------------------------
BitField Name: InitQueueID
BitField Type: R/W
BitField Desc: Queue( Pseudowire) ID to be initialized
--------------------------------------*/
#define cThaInitQueIDMask                              cBit11_0
#define cThaInitQueIDShift                             0
#define cThaInitQueIDMaxVal                            0xFFF
#define cThaInitQueIDMinVal                            0x0
#define cThaInitQueIDNumDw                             1
#define cThaInitQueIDDwIndex                           0
#define cThaInitQueIDRstVal                            0x0

/*------------------------------------------------------------------------------
Reg Name: PwPda TDM Mode Control
Reg Addr: 0x24A000 - 0x24A3FF
          The address format for these registers is 0x24A000 + PWID
          Where: PWID (0 � 1023) Pseudowire Identification
Reg Desc: The register configures modes of TDM Payload De-Assembler function
------------------------------------------------------------------------------*/
#define cThaRegPwPdaTDMModeCtrl                     (0x24A000)

/*--------------------------------------
BitField Name: PDARepMode
BitField Type: R/W
BitField Desc: Mode to replace lost or L bit set packets
               - 0: Replace by replaying previous good packet (often for voice
                 or video application)
               - 1: Replace by AIS
               - 2: Replace by a configuration idle code
BitField Bits: 16_15
--------------------------------------*/
#define cThaPDARepModeMask                             cBit16_15
#define cThaPDARepModeShift                            15

/*--------------------------------------
BitField Name: PDANxDS0/CEP_StsId
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 14_5
--------------------------------------*/
#define cThaPDANxDS0CEPStsIdMask                       cBit14_5
#define cThaPDANxDS0CEPStsIdShift                      5

/*--------------------------------------
BitField Name: PDAMode
BitField Type: R/W
BitField Desc: is number of DS0 time slot in Structured Mode (include CESoPSN
               and AAL1) When PDAMode indicates CEP PW: CEP_StsId[3:0] is STS
               ID that transports CEP PW
BitField Bits: 4_1
--------------------------------------*/
#define cThaPDAModeMask                                cBit4_1
#define cThaPDAModeShift                               1

/*--------------------------------------
BitField Name: PDAE1orT1/CEP_EBM_En
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 0: T1 (for PDH PW)/ EBM field is enabled (for CEP PW)
               - 1: E1 (for PDH PW)/ EBM field is disabled (for CEP PW)
BitField Bits: 0
--------------------------------------*/
#define cThaPDAE1orT1CEPEBmEnMask                      cBit0
#define cThaPDAE1orT1CEPEBmEnShift                     0
#define cThaPDAE1orT1CEPEBmEnMaxVal                    0x1
#define cThaPDAE1orT1CEPEBmEnMinVal                    0x0
#define cThaPDAE1orT1CEPEBmEnRstVal                    0x0

/*------------------------------------------------------------------------------
Reg Name: PwPda Idle Code Control
Reg Addr: 0x24AC00
Reg Desc: This register configures idle code for TDM packet replacement
------------------------------------------------------------------------------*/
#define cThaRegPwPdaIdleCodeCtrl                    (0x24AC00)

/*--------------------------------------
BitField Name: PDAIdleCode
BitField Type: R/W
BitField Desc: Idle code for TDM packet replacement
--------------------------------------*/
#define cThaPDAIdleCodeMask                            cBit7_0
#define cThaPDAIdleCodeShift                           0

/*------------------------------------------------------------------------------
Reg Name: PwPda Initialize Free Block Control
Reg Addr: 0x242000
Reg Desc: This register is used for initializing free block of queue
          controller. Write this register when Payload De-Assembler block has
          already been activated will start initializing PDAFreeBlockNum
          number of free block.
------------------------------------------------------------------------------*/
#define cThaRegPwPdaInitializeFreeBlkCtrl           (0x242000)

#define cThaPwPdaMaxFreeBlockNum                    970752

/*------------------------------------------------------------------------------
Reg Name: PDA PW bytes counter
Reg Addr: 0x260000 � 0x2601FF(R2C), 0x260800 � 0x2609FF(R_O)
Reg Desc: The register provides counter for number of Rx PW bytes
------------------------------------------------------------------------------*/
#define cThaRegPDAPwByteR2cCnt         (0x260000)
#define cThaRegPDAPwByteRoCnt         (0x260800)

/*------------------------------------------------------------------------------
Reg Name: PDA PW packets counter
Reg Addr: 0x261000 � 0x2611FF(R2C), 0x261800 � 0x2619FF(R_O)
Reg Desc: The register provides counter for number of Rx PW packets
------------------------------------------------------------------------------*/
#define cThaRegPDAPwPktR2cCnt          (0x261000)
#define cThaRegPDAPwPktRoCnt          (0x261800)

/*------------------------------------------------------------------------------
Reg Name: PDA Reorder Lost Packet Counter
Reg Addr: 0x262000 � 0x2621FF(R2C), 0x262800 � 0x2629FF(R_O)
Reg Desc: Count number of reorder LOST packet of the Pseudowire
------------------------------------------------------------------------------*/
#define cThaRegPDAPwLostPktR2cCnt          (0x262000)
#define cThaRegPDAPwLostPktRoCnt          (0x262800)

/*------------------------------------------------------------------------------
Reg Name: PDA Reorder Drop Packet Counter
Reg Addr: 0x263000 � 0x2631FF(R2C), 0x263800 � 0x2639FF(R_O)
Reg Desc: Count number of reorder drop packet of the Pseudowire
------------------------------------------------------------------------------*/
#define cThaRegPDAPwDropPktR2cCnt          (0x263000)
#define cThaRegPDAPwDropPktRoCnt          (0x263800)

/*------------------------------------------------------------------------------
Reg Name: PDA Reorder Out Of Sequence Packet Counter
Reg Addr: 0x264000 � 0x2641FF(R2C), 0x264800 � 0x2649FF(R_O)
Reg Desc: Count number of reorder out of sequence packet of the Pseudowire
------------------------------------------------------------------------------*/
#define cThaRegPDAPwReorderedOutOfSeqPktR2cCnt         (0x264000)
#define cThaRegPDAPwReorderedOutOfSeqPktRoCnt         (0x264800)

/*------------------------------------------------------------------------------
Reg Name: PDA Normal State To Loss of Packet State Transition Counter
Reg Addr: 0x265000 � 0x2651FF(R2C), 0x265800 � 0x2659FF(R_O)
Reg Desc: Count number of transitions from normal state to loss of packet state of the Pseudowire
------------------------------------------------------------------------------*/
#define cThaRegPDAPwLofsR2cCnt         (0x265000)
#define cThaRegPDAPwLofsRoCnt         (0x265800)

/*------------------------------------------------------------------------------
Reg Name: PDA Overrun Packet Counter
Reg Addr: 0x266000 � 0x2661FF(R2C), 0x266800 � 0x2669FF(R_O)
Reg Desc: Count number of overrun packet of the Pseudowire
------------------------------------------------------------------------------*/
#define cThaRegPDAPwOverrunPktR2cCnt       (0x266000)
#define cThaRegPDAPwOverrunPktRoCnt       (0x266800)

/*------------------------------------------------------------------------------
Reg Name: Thalassa PW TDM Jitter Buffer Status
Reg Addr: 0x24E800 - 0x24EBFF
          The address format for these registers is 0x24E800 + pwId
Reg Desc: The register provide jitter buffer information
------------------------------------------------------------------------------*/
#define cThaRegPDATdmJitBufStat               0x24E800

/*--------------------------------------
BitField Name: RxBuffNumPkt[15:0]
BitField Type: R_O
BitField Desc: 16 bit
--------------------------------------*/
#define cThaRxBufNumPktMask                           cBit19_4
#define cThaRxBufNumPktShift                          4

/*--------------------------------------
BitField Name: RxBuffState[3:0]
BitField Type: R_O
BitField Desc: Jitter buffer status
    0: START – no packet in jitter buffer
    1: BUFFER – jitter buffer is filling packet to enough
    RxBuffNumAct before sending to TDM side
    2: READY – jitter buffer is ready to send packet to TDM side
    others: don't care
--------------------------------------*/
#define cThaRxBufStateMask                           cBit3_0
#define cThaRxBufStateShift                          0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA EPAR Line ID Lookup Control
Reg Addr: 0x250000-0x2501FF
Reg Desc: These registers are used to look up from pseudowire ID to EPAR line ID
------------------------------------------------------------------------------*/
#define cThaRegPDAEparLineIdLkCtr                    0x250000

/*--------------------------------------
BitField Name: PDAPwOverrunPktCnt
BitField Type: R/W
BitField Desc: number of overrun packet
BitField Bits: 1
--------------------------------------*/
#define cThaPDAPdaEparLineIdMask                  cBit9_1
#define cThaPDAPdaEparLineIdShift                 1

#define cThaPDAPdaEparLineEnMask                  cBit0
#define cThaPDAPdaEparLineEnShift                 0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA Per Alarm Current Status
------------------------------------------------------------------------------*/
#define cThaRegThalassaPDAAlmCurrentStat               0x255800

#define cThaPDAUnderrunCurStatMask                     cBit2
#define cThaPDAOverrunCurStatMask                      cBit1
#define cThaPDALofsCurStatMask                         cBit0

/*------------------------------------------------------------------------------
Reg Name: Thalassa PDA Per Alarm Interrupt Status
------------------------------------------------------------------------------*/
#define cThaRegThalassaPDAAlmIntrStat               0x255400

/*------------------------------------------------------------------------------
Reg Name   : Pseudowire PDA Jitter Buffer Watermark Status
Reg Addr   : 0x0001C000 - 0x0001D7FF #The address format for these registers is 0x0001C000 + PWID
Reg Formula: 0x0001C000 +  PWID
    Where  :
           + $PWID(0-6143): Pseudowire ID
Reg Desc   :
This register show jitter buffer water mark per pseudo-wire, write to this register will reset water mark

------------------------------------------------------------------------------*/
#define cAf6Reg_ramjitbufwater_Base                                                                 0x0001C000
#define cAf6Reg_ramjitbufwater_WidthVal                                                                     32

/*--------------------------------------
BitField Name: JitBufMaxPk
BitField Type: WRC
BitField Desc: Water mark Maximum number of packet in jitter buffer
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_ramjitbufwater_JitBufMaxPk_Mask                                                         cBit23_12
#define cAf6_ramjitbufwater_JitBufMaxPk_Shift                                                               12

/*--------------------------------------
BitField Name: JitBufMinPk
BitField Type: WRC
BitField Desc: Water mark Minimum number of packet in jitter buffer
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_ramjitbufwater_JitBufMinPk_Mask                                                          cBit11_0
#define cAf6_ramjitbufwater_JitBufMinPk_Shift                                                                0

#ifdef __cplusplus
}
#endif
#endif /* _THAPDAREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA (internal)
 *
 * File        : ThaModulePda.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : PDA internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../man/ThaDeviceInternal.h"
#include "../cla/pw/ThaModuleClaPwInternal.h"
#include "../pw/adapters/ThaPwAdapter.h"
#include "../pw/ThaModulePw.h"
#include "../pw/ThaPwUtil.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "../util/ThaUtil.h"
#include "../pw/ThaPwToh.h"
#include "AtPwCep.h"
#include "ThaPdaReg.h"
#include "commacro.h"
#include "ThaModulePdaInternal.h"
#include "AtMpBundle.h"
/*--------------------------- Define -----------------------------------------*/
#define cThaPwPdaBlockSize 64UL
#define cMaxLopsInUs 1000000

#define cThaRegPDALbitPkRepModeMask       cBit29
#define cThaRegPDALbitPkRepModeShift      29
#define cThaRegPDALopsPkRepModeMask       cBit30
#define cThaRegPDALopsPkRepModeShift      30

#define cMinCepJitterDelayInUs      125
#define cMinCepJitterBufferInUs     250
#define cMinCESoPJitterDelayInUs    125
#define cMinCESoPJitterBufferInUs   250

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)  ((ThaModulePda)self)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)

#define mDevice(self)    AtModuleDeviceGet((AtModule)self)
#define mClaModule(self) (ThaModuleCla)AtDeviceModuleGet(mDevice(self), cThaModuleCla)
#define mClaPwController(self) ThaModuleClaPwControllerGet(mClaModule(self))
#define mClaController(self) ((ThaClaController)mClaPwController(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePdaMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineRegAdress(ThaModulePda, PwPdaIdleCodeCtrl)

static uint32 PWPdaTdmModeCtrl(ThaModulePda self, AtPw pw)
    {
	AtUnused(self);
	AtUnused(pw);
    return cThaRegPwPdaTDMModeCtrl;
    }

static eBool PwJitterBufferIsEmpty(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPDATdmJitBufStat + mPwOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    if ((regVal & cThaRxBufStateMask) == 0)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet HotConfigureStart(ThaModulePda self, AtPw pw)
    {
    eAtRet ret = cAtOk;

    if (!ThaPwAdapterClaIsEnabled(pw))
        return cAtOk;

    ret |= ThaPwAdapterClaEnable(pw, cAtFalse);
    ret |= ThaModulePdaJitterBufferEmptyWait(self, pw);

    return ret;
    }

static eAtRet HotConfigureStop(ThaModulePda self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static uint32 PwLopsThresholdMin(ThaModulePda self, AtPw adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return 4;
    }

static uint32 PwLopsThresholdMax(ThaModulePda self, AtPw adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return 31;
    }

static eBool LopsThresholdHasLimitOneSecond(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PwLopsThresholdIsValid(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 lopsInUs;
    AtChannel circuit;
    uint32 min = mMethodsGet(self)->PwLopsThresholdMin(self, pw);
    uint32 max = mMethodsGet(self)->PwLopsThresholdMax(self, pw);

    if (mOutOfRange(numPackets, min, max))
        return cAtFalse;

    if (mMethodsGet(self)->LopsThresholdHasLimitOneSecond(self) == cAtFalse)
        return cAtTrue;

    circuit = AtPwBoundCircuitGet(pw);
    if (circuit == NULL)
        return cAtTrue;

    lopsInUs = ThaModulePdaCalculateUsByNumberOfPacket(self, pw, numPackets);
    return (lopsInUs > cMaxLopsInUs) ? cAtFalse : cAtTrue;
    }

static eAtRet PwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address, regVal;

    address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead((AtChannel)pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaRxBuffEnterLofSMask, cThaRxBuffEnterLofSShift, numPackets);
    mChannelHwWrite((AtChannel)pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 PwLofsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;
    uint32 rxBuffEnterLofs;

    address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    mFieldGet(regVal, cThaRxBuffEnterLofSMask, cThaRxBuffEnterLofSShift, uint32,  &rxBuffEnterLofs);

    return rxBuffEnterLofs;
    }

static eAtRet PwLofsClearThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address, regVal;

    address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    regVal = mChannelHwRead((AtChannel)pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaRxBuffExitLofSMask, cThaRxBuffExitLofSShift, numPackets);
    mChannelHwWrite((AtChannel)pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 PwLofsClearThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;
    uint8 rxBuffEnterLofs;

    address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    mFieldGet(regVal, cThaRxBuffExitLofSMask, cThaRxBuffExitLofSShift, uint8, &rxBuffEnterLofs);

    return rxBuffEnterLofs;
    }

static uint32 PwCircuitDataRateBytePer125Us(AtPw pw)
    {
    return AtChannelDataRateInBytesPer125Us(AtPwBoundCircuitGet(pw));
    }

static uint32 JitterTimeoutCalculate(AtPw pw, eBool reoderingEnable)
    {
    uint32 bufferSizeInMs = mMethodsGet(pw)->JitterBufferSizeGet(pw) / 1000;
    AtUnused(reoderingEnable);
    return (bufferSizeInMs / 2) + 1;
    }

static eAtRet PwReorderingEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    uint32 address, regVal;
    uint32 timeoutMs = JitterTimeoutCalculate(pw, enable);

    address = cThaRegPwPdaReorderTimeoutCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead((AtChannel)pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaPDAReorderTimeoutMask, cThaPDAReorderTimeoutShift, timeoutMs);
    mChannelHwWrite((AtChannel)pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eBool PwReorderingIsEnable(ThaModulePda self, AtPw pw)
    {
	return ThaClaPwControllerReorderIsEnable(mClaPwController(self), pw);
    }

static uint32 PwPayloadSizeInBytes(AtPw pwAdapter)
    {
    if (AtPwTypeGet(pwAdapter) == cAtPwTypeCESoP)
        return (uint32)(AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)AtPwBoundCircuitGet(pwAdapter)) * AtPwPayloadSizeGet(pwAdapter));

    if (AtPwTypeGet(pwAdapter) == cAtPwTypeCEP)
        {
        AtPwCep pwCep = (AtPwCep)ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
        if (AtPwCepModeGet(pwCep) == cAtPwCepModeFractional)
            return ThaPwCepFractionalRealPayloadSizeCalculate(pwAdapter, AtPwPayloadSizeGet(pwAdapter));
        }

    if (AtPwTypeGet(pwAdapter) == cAtPwTypeToh)
        {
        AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
        return (uint32)(AtPwPayloadSizeGet(pwAdapter) * AtPwTohNumEnabledBytesGet(pwToh));
        }

    return AtPwPayloadSizeGet(pwAdapter);
    }

static uint32 CalculateNumPacketsByUs(AtPw pw, uint32 numUs, uint32 payloadSizeInBytes)
    {
    uint32 numBytes;
    if (payloadSizeInBytes == 0)
        return 0;

    numBytes = (numUs / 125) * PwCircuitDataRateBytePer125Us(pw);
    return mRoundUpDivide(numBytes, payloadSizeInBytes);
    }

static uint32 CalculateNumberOfBlockByUs(AtPw pw, uint32 numUs)
    {
    uint32 payloadSize = PwPayloadSizeInBytes(pw);
    uint32 numPackets = CalculateNumPacketsByUs(pw, numUs, payloadSize);
    uint32 numBlockPerPkt = (uint32)mRoundUpDivide(payloadSize, cThaPwPdaBlockSize);

    return mRoundUpDivide(numPackets, 2) * numBlockPerPkt * 2;
    }

static uint32 CalculateUsByNumPackets(AtPw pw, uint32 numPkts)
    {
    uint32 numBytes = numPkts * AtPwPayloadSizeGet(pw);
    uint32 rateBytePer125Us = PwCircuitDataRateBytePer125Us(pw);
    if (rateBytePer125Us == 0)
        return 0;
    return (numBytes / rateBytePer125Us) * 125;
    }

static eBool PwJitterBufferSizeIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 numBlocks = CalculateNumberOfBlockByUs(pw, microseconds);
	AtUnused(self);

    if (numBlocks > cThaRxBuffMaxBlkMask)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet PwJitterBufferSizeSet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 address;
    uint32 numBlocks;

    numBlocks = CalculateNumberOfBlockByUs(pw, microseconds);
    address = cThaRegPwPdaRxBufManmentMemoryAllocationCtrl + mPwOffset(self, pw);
    mChannelHwWrite(pw, address, numBlocks, cThaModulePda);

    return cAtOk;
    }

static uint32 PwJitterBufferSizeGet(ThaModulePda self, AtPw pw)
    {
    uint32 address = cThaRegPwPdaRxBufManmentMemoryAllocationCtrl + mPwOffset(self, pw);
    uint32 numBlocks = mChannelHwRead(pw, address, cThaModulePda);
    uint32 payloadSize = PwPayloadSizeInBytes(pw);
    uint32 numBlockPerPkt = mRoundUpDivide(payloadSize, cThaPwPdaBlockSize);
    uint32 numPkts;

    if (numBlockPerPkt == 0)
        return 0;

    numPkts = numBlocks / numBlockPerPkt;
    return CalculateUsByNumPackets(pw, numPkts);
    }

static eBool PwJitterBufferDelayIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
	AtUnused(self);
    return (CalculateNumPacketsByUs(pw, microseconds, PwPayloadSizeInBytes(pw)) <= cThaRxBuffNumActMask) ? cAtTrue : cAtFalse;
    }

static eBool PwJitterBufferSizeIsValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs)
    {
    uint32 jitterDelayUs = AtPwJitterBufferDelayGet(pw);
    AtUnused(self);

    if (jitterDelayUs > jitterBufferUs)
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwJitterDelayIsValid(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    uint32 jitterBufferUs = AtPwJitterBufferSizeGet(pw);
    AtUnused(self);

    if (jitterDelayUs > jitterBufferUs)
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwJitterBufferAndJitterDelayAreValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs)
    {
    AtUnused(self);
    AtUnused(pw);

    if (jitterBufferUs < jitterDelayUs)
        return cAtFalse;

    return cAtTrue;
    }

static eBool ShouldSetDefaultJitterBufferSizeOnJitterDelaySet(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* Default implementation should handle jitter buffer and jitter delay
     * independently */
    return cAtFalse;
    }

static uint32 DefaultJitterBufferSize(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    AtUnused(self);
    AtUnused(pw);
    return jitterDelayUs * 2U;
    }

static eAtRet PwJitterBufferDelaySet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 address, regVal;

    address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaRxBuffNumActMask, cThaRxBuffNumActShift, CalculateNumPacketsByUs(pw, microseconds, PwPayloadSizeInBytes(pw)));
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 PwJitterBufferDelayGet(ThaModulePda self, AtPw pw)
    {
    uint32 numberOfPackets;
    uint32 address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    mFieldGet(regVal, cThaRxBuffNumActMask, cThaRxBuffNumActShift, uint32, &numberOfPackets);

    return CalculateUsByNumPackets(pw, numberOfPackets);
    }

static uint32 NumCurrentPacketsInJitterBuffer(ThaModulePda self, AtPw pw)
    {
    uint32 address = cThaRegPDATdmJitBufStat + mPwOffset(self, pw);
    uint32 regVal;
    uint16 numPackets;

    regVal = mChannelHwRead(pw, address, cThaModulePda);
    mFieldGet(regVal, cThaRxBufNumPktMask, cThaRxBufNumPktShift, uint16, &numPackets);

    return numPackets;
    }

static uint32 JitterBufferWatermarkAddress(ThaModulePda self, AtPw pw)
    {
    return cAf6Reg_ramjitbufwater_Base + mPwOffset(self, pw) + ThaModulePdaBaseAddress(self);
    }

static uint32 JitterBufferWatermarkMinPackets(ThaModulePda self, AtPw pw)
    {
    uint32 address = JitterBufferWatermarkAddress(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModulePda);
    return mRegField(regVal, cAf6_ramjitbufwater_JitBufMinPk_);
    }

static uint32 JitterBufferWatermarkMaxPackets(ThaModulePda self, AtPw pw)
    {
    uint32 address = JitterBufferWatermarkAddress(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModulePda);
    return mRegField(regVal, cAf6_ramjitbufwater_JitBufMaxPk_);
    }

static eBool JitterBufferWatermarkIsSupported(ThaModulePda self, AtPw pw)
    {
    /* Concrete must know */
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static eAtRet JitterBufferWatermarkReset(ThaModulePda self, AtPw pw)
    {
    uint32 address = JitterBufferWatermarkAddress(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModulePda);
    mRegFieldSet(regVal, cAf6_ramjitbufwater_JitBufMaxPk_, 0);
    mRegFieldSet(regVal, cAf6_ramjitbufwater_JitBufMinPk_, 0);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwPdaModeSet(ThaModulePda self, AtPw pw, uint8 mode)
    {
    uint32 address, regVal;
    eAtRet ret = cAtOk;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaPDAModeMask, cThaPDAModeShift, mode);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return ret;
    }

static uint8 PwPdaModeGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;
    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    return (uint8)mRegField(regVal, cThaPDAMode);
    }

static uint8 SequenceModeSw2Hw(eAtPwCwSequenceMode seqMd)
    {
    if (seqMd == cAtPwCwSequenceModeWrapZero) return 0;
    if (seqMd == cAtPwCwSequenceModeSkipZero) return 1;
    if (seqMd == cAtPwCwSequenceModeDisable)  return 2;

    return 0;
    }

static eAtRet PwCwSequenceModeSet(ThaModulePda self, AtPw pw, eAtPwCwSequenceMode seqMd)
    {
    uint32 address, regVal;
    eBool claEnabled = ThaPwAdapterClaIsEnabled(pw);
    eAtRet ret = cAtOk;

    ThaPwAdapterStartCenterJitterBuffer(pw);
    ThaModulePdaHotConfigureStart(self, pw);
    address = cThaRegPwPdaReorderTimeoutCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaPDAReorderSeqModeMask, cThaPDAReorderSeqModeShift, SequenceModeSw2Hw(seqMd));
    mChannelHwWrite(pw, address, regVal, cThaModulePda);
    ret = ThaPwAdapterClaEnable(pw, claEnabled);
    ThaModulePdaHotConfigureStop(self, pw);
    ThaPwAdapterStopCenterJitterBuffer(pw);

    return ret;
    }

static uint8 PktReplaceModeSw2Hw(eAtPwPktReplaceMode pktReplaceMode)
    {
    if (pktReplaceMode == cAtPwPktReplaceModePreviousGoodPacket) return 0;
    if (pktReplaceMode == cAtPwPktReplaceModeAis)                return 1;
    if (pktReplaceMode == cAtPwPktReplaceModeIdleCode)           return 2;

    return 1;
    }

static eBool PktReplaceModeV1IsSupported(ThaModulePda self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtUnused(self);
    return mOutOfRange(pktReplaceMode, cAtPwPktReplaceModePreviousGoodPacket, cAtPwPktReplaceModeIdleCode) ? cAtFalse : cAtTrue;
    }

static eAtModulePwRet PwCwPktReplaceModeV1Set(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    uint32 address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);
    uint8 fieldVal = PktReplaceModeSw2Hw(pktReplaceMode);

    mFieldIns(&regVal,
              mMethodsGet(self)->PktReplaceModeMask(self, pw),
              mMethodsGet(self)->PktReplaceModeShift(self, pw),
              fieldVal);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static uint8 PktReplaceModeHw2Sw(eAtPwPktReplaceMode pktReplaceMode)
    {
    if (pktReplaceMode == 0) return cAtPwPktReplaceModePreviousGoodPacket;
    if (pktReplaceMode == 1) return cAtPwPktReplaceModeAis;
    if (pktReplaceMode == 2) return cAtPwPktReplaceModeIdleCode;

    return cAtPwPktReplaceModeAis;
    }

static eAtPwPktReplaceMode PwCwPktReplaceModeV1Get(ThaModulePda self, AtPw pw)
    {
    uint32 address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);
    uint8 fieldVal;

    mFieldGet(regVal,
              mMethodsGet(self)->PktReplaceModeMask(self, pw),
              mMethodsGet(self)->PktReplaceModeShift(self, pw),
              uint8, &fieldVal);

    return PktReplaceModeHw2Sw(fieldVal);
    }

static eAtRet PwLopsPktReplaceModeV1Set(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    return mMethodsGet(self)->PwCwPktReplaceModeSet(self, pw, pktReplaceMode);
    }

static eAtPwPktReplaceMode PwLopsPktReplaceModeV1Get(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->PwCwPktReplaceModeGet(self, pw);
    }

static eBool PktReplaceModeV2IsSupported(ThaModulePda self, eAtPwPktReplaceMode pktReplaceMode)
    {
	AtUnused(self);
    if ((pktReplaceMode == cAtPwPktReplaceModeIdleCode) || (pktReplaceMode == cAtPwPktReplaceModeAis))
        return cAtTrue;

    return cAtFalse;
    }

static eBool PktReplaceModeIsSupported(ThaModulePda self, eAtPwPktReplaceMode pktReplaceMode)
    {
    if (!mMethodsGet(self)->CanControlLopsPktReplaceMode(self))
        return PktReplaceModeV1IsSupported(self, pktReplaceMode);

    return PktReplaceModeV2IsSupported(self, pktReplaceMode);
    }

static uint8 LbitPktReplaceModeSw2Hw(eAtPwPktReplaceMode pktReplaceMode)
    {
    if (pktReplaceMode == cAtPwPktReplaceModeAis)      return 0;
    if (pktReplaceMode == cAtPwPktReplaceModeIdleCode) return 1;

    return 0;
    }

static eAtModulePwRet LbitPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regVal, cThaRegPDALbitPkRepMode, LbitPktReplaceModeSw2Hw(pktReplaceMode));
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtModulePwRet PwCwPktReplaceModeV2Set(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    return mMethodsGet(self)->LbitPktReplaceModeSet(self, pw, pktReplaceMode);
    }

static eAtModulePwRet PwCwPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    if (!mMethodsGet(self)->CanControlLopsPktReplaceMode(self))
        return PwCwPktReplaceModeV1Set(self, pw, pktReplaceMode);

    return PwCwPktReplaceModeV2Set(self, pw, pktReplaceMode);
    }

static uint8 LbitPktReplaceModeHw2Sw(eAtPwPktReplaceMode pktReplaceMode)
    {
    if (pktReplaceMode == 0) return cAtPwPktReplaceModeAis;
    if (pktReplaceMode == 1) return cAtPwPktReplaceModeIdleCode;

    return cAtPwPktReplaceModeAis;
    }

static eAtPwPktReplaceMode LbitPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return LbitPktReplaceModeHw2Sw(mRegField(regVal, cThaRegPDALbitPkRepMode));
    }

static eAtPwPktReplaceMode PwCwPktReplaceModeV2Get(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->LbitPktReplaceModeGet(self, pw);
    }

static eAtPwPktReplaceMode PwCwPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    if (!mMethodsGet(self)->CanControlLopsPktReplaceMode(self))
        return PwCwPktReplaceModeV1Get(self, pw);

    return PwCwPktReplaceModeV2Get(self, pw);
    }

static eAtRet LopPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    uint32 address, regVal;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    mRegFieldSet(regVal, cThaRegPDALopsPkRepMode, LbitPktReplaceModeSw2Hw(pktReplaceMode));
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwLopsPktReplaceModeV2Set(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    return mMethodsGet(self)->LopPktReplaceModeSet(self, pw, pktReplaceMode);
    }

static eAtModulePwRet PwLopsPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    eAtRet ret;

    if (!mMethodsGet(self)->CanControlLopsPktReplaceMode(self))
        return PwLopsPktReplaceModeV1Set(self, pw, pktReplaceMode);

    /* When packet replace mode for L-bit and LOPS reparation is supported, old field also need to configure */
    ret = PwCwPktReplaceModeV1Set(self, pw, pktReplaceMode);
    if (ret != cAtOk)
        return ret;

    return PwLopsPktReplaceModeV2Set(self, pw, pktReplaceMode);
    }

static eAtPwPktReplaceMode LopPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;
    eAtPwPktReplaceMode replaceMode;

    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    replaceMode = LbitPktReplaceModeHw2Sw(mRegField(regVal, cThaRegPDALopsPkRepMode));

    return replaceMode;
    }

static eAtPwPktReplaceMode PwLopsPktReplaceModeV2Get(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->LopPktReplaceModeGet(self, pw);
    }

static eAtPwPktReplaceMode PwLopsPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    eAtPwPktReplaceMode replaceMode;

    if (!mMethodsGet(self)->CanControlLopsPktReplaceMode(self))
        return PwLopsPktReplaceModeV1Get(self, pw);

    replaceMode = PwLopsPktReplaceModeV2Get(self, pw);

    /* Must match with old field that handled by PwCwPktReplaceModeV1Get */
    if (replaceMode == PwCwPktReplaceModeV1Get(self, pw))
        return replaceMode;

    return cAtPwPktReplaceModeInvalid;
    }

static uint8 NumParts(ThaModulePda self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModulePda);
    }

static eAtModulePwRet ModuleIdleCodeSet(ThaModulePda self, uint8 idleCode)
    {
    uint8 part;

    for (part = 0; part < NumParts(self); part++)
        {
        uint32 address = mMethodsGet(self)->PwPdaIdleCodeCtrl(self) + ThaModulePdaPartOffset(self, part);
        uint32 regVal  = mModuleHwRead(self, address);
        mFieldIns(&regVal, cThaPDAIdleCodeMask, cThaPDAIdleCodeShift, idleCode);
        mModuleHwWrite(self, address, regVal);
        }

    return cAtOk;
    }

static uint8 ModuleIdleCodeGet(ThaModulePda self)
    {
    uint8  idleCode;
    uint32 address = mMethodsGet(self)->PwPdaIdleCodeCtrl(self) + ThaModulePdaPartOffset(self, 0);
    uint32 regVal  = mModuleHwRead(self, address);

    mFieldGet(regVal, cThaPDAIdleCodeMask, cThaPDAIdleCodeShift, uint8, &idleCode);

    return idleCode;
    }

static eBool ModuleIdleCodeIsSupported(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModulePw PwModule(ThaModulePda self)
    {
    return (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    }

static uint32 PwDefaultOffset(ThaModulePda self, AtPw pw)
    {
    ThaModulePw pwModule = PwModule(self);
    uint8 partId = ThaModulePwPartOfPw(pwModule, pw);
    return mPwHwId(pw) + ThaModulePdaPartOffset(self, partId);
    }

static uint32 CounterOffset(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->PwDefaultOffset(self, pw);
    }

static uint32 PwTdmOffset(ThaModulePda self, AtPw pw)
    {
    /* Default implementation, offset by circuit and offset by pw are same */
    return mPwOffset(self, pw);
    }

static uint32 PwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    uint32 address = (clear ? cThaRegPDAPwByteR2cCnt : cThaRegPDAPwByteRoCnt) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return regVal;
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    uint32 address = (clear ? cThaRegPDAPwReorderedOutOfSeqPktR2cCnt : cThaRegPDAPwReorderedOutOfSeqPktRoCnt) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return regVal;
    }

static uint32 PwRxLostPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    uint32 address = (clear ? cThaRegPDAPwLostPktR2cCnt : cThaRegPDAPwLostPktRoCnt) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return regVal;
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    uint32 address = (clear ? cThaRegPDAPwDropPktR2cCnt : cThaRegPDAPwDropPktRoCnt) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return regVal;
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    uint32 address = (clear ? cThaRegPDAPwOverrunPktR2cCnt : cThaRegPDAPwOverrunPktRoCnt) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return regVal;
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	AtUnused(clear);
	AtUnused(pw);
	AtUnused(self);
    /* Hardware not support */
    return 0;
    }

static uint32 PwRxLopsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    uint32 address = (clear ? cThaRegPDAPwLofsR2cCnt : cThaRegPDAPwLofsRoCnt) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return regVal;
    }

static uint32 PwRxPacketsSentToTdmGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    uint32 address = (clear ? cThaRegPDAPwPktR2cCnt : cThaRegPDAPwPktRoCnt) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return regVal;
    }

static eBool PwRxPacketsSentToTdmCounterIsSupported(ThaModulePda self)
    {
    if (self)
        return cAtTrue;
    else
        return cAtFalse;
    }

static void PwJitterPayloadLengthSet(ThaModulePda self, AtPw pw, uint32 payloadSizeInBytes)
    {
	AtUnused(payloadSizeInBytes);
	AtUnused(pw);
	AtUnused(self);
    }

static uint32 PwAlarmHw2Sw(uint32 hwAlarm)
    {
    uint32 alarms  = 0;

    if (hwAlarm & cThaPDAUnderrunCurStatMask) alarms |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (hwAlarm & cThaPDAOverrunCurStatMask)  alarms |= cAtPwAlarmTypeJitterBufferOverrun;
    if (hwAlarm & cThaPDALofsCurStatMask)     alarms |= cAtPwAlarmTypeLops;

    return alarms;
    }

static uint32 PwAlarmGet(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr = cThaRegThalassaPDAAlmCurrentStat + mPwOffset(self, pw);
    return PwAlarmHw2Sw(mChannelHwRead(pw, regAddr, cThaModulePda));
    }

static uint32 PwDefectHistoryGet(ThaModulePda self, AtPw pw, eBool read2Clear)
    {
    uint32 regAddr = cThaRegThalassaPDAAlmIntrStat + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);
    uint32 history = PwAlarmHw2Sw(regVal);

    if (read2Clear)
        mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return history;
    }

static const char *JitterBufferStateString(ThaModulePda self, uint32 hwState)
    {
    AtUnused(self);
    if (hwState == cThaPdaJitterBufferStateStart)   return "start";
    if (hwState == cThaPdaJitterBufferStateFilling) return "filling";
    if (hwState == 2) return "ready";

    return "xxx";
    }

static uint8 JitterBufferState(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr = cThaRegPDATdmJitBufStat + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);
    return (uint8)mRegField(regVal, cThaRxBufState);
    }

static void PwDebug(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPDATdmJitBufStat + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);
    AtPrintc(cSevNormal, "* Jitter buffer size: %u(us)\r\n", mMethodsGet(self)->PwJitterBufferSizeGet(self, pw));
    AtPrintc(cSevNormal, "* Jitter delay %u(us)\r\n", mMethodsGet(self)->PwJitterBufferDelayGet(self, pw));
    AtPrintc(cSevNormal, "* Jitter buffer state: %s\r\n", mMethodsGet(self)->JitterBufferStateString(self, mRegField(regVal, cThaRxBufState)));
    AtPrintc(cSevNormal, "* Number of packets in jitter buffer: %u\r\n", (uint32)mRegField(regVal, cThaRxBufNumPkt));
    }

static eAtRet PwNumDs0TimeslotsSet(ThaModulePda self, AtPw pw, uint16 numTimeslots)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mPwOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mFieldIns(&regVal, cThaPDANxDS0CEPStsIdMask, cThaPDANxDS0CEPStsIdShift, numTimeslots);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PartDefaultSet(ThaModulePda self, uint8 partId)
    {
    uint32 partOffset = ThaModulePdaPartOffset((ThaModulePda)self, partId);

    /* Free all PDA block */
    mModuleHwWrite(self, cThaRegPwPdaInitializeFreeBlkCtrl + partOffset, cThaPwPdaMaxFreeBlockNum);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModulePda self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < NumParts((ThaModulePda)self); part_i++)
        ret |= mMethodsGet(self)->PartDefaultSet(self, part_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Restart resource counter */
    ((ThaModulePda)self)->numOccupiedBlocks = 0;

    return DefaultSet((ThaModulePda)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 JitterBufferEmptyAdditionalTimeout(ThaModulePda self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    uint32 additionalTimeoutMs = 512; /* Plus 512ms just for safe */

    if (ThaModulePwJitterEmptyTimeoutGet(pwModule))
        additionalTimeoutMs = ThaModulePwJitterEmptyTimeoutGet(pwModule);

    return additionalTimeoutMs;
    }

static eBool NeedHandleBufferRamLimitation(ThaModulePda self)
    {
    return ThaModulePwShouldStrictlyLimitResources(PwModule(self));
    }

static eAtRet Debug(AtModule self)
    {
    ThaModulePda modulePda = (ThaModulePda)self;
    uint32 timeout = mMethodsGet(mThis(self))->JitterBufferEmptyAdditionalTimeout(mThis(self));

	AtUnused(self);
    AtPrintc(cSevInfo, "PDA information\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");
    AtPrintc(cSevNormal, "* Jitter buffer empty timeout: %d (ms)\r\n", timeout);
    AtPrintc(cSevNormal, "* Jitter buffer empty max waiting time: %d (ms)\r\n", mThis(self)->maxJitterEmptyWaitingTime);

    /* Clear debug counters */
    mThis(self)->maxJitterEmptyWaitingTime = 0;

    if (!NeedHandleBufferRamLimitation(modulePda))
        return cAtOk;

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* Maximum number of block : %u\r\n", mMethodsGet(modulePda)->MaxNumJitterBufferBlocks(modulePda));
    AtPrintc(cSevNormal, "* Block size in byte      : %u\r\n", mMethodsGet(modulePda)->JitterBufferBlockSizeInByte(modulePda));
    AtPrintc(cSevNormal, "* Number of in-used block : %u\r\n", modulePda->numOccupiedBlocks);

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eAtRet PwCircuitTypeSet(ThaModulePda self, AtPw pw, AtChannel circuit)
    {
    uint32 regAddr, regVal;
    eAtPwType pwType = AtPwTypeGet(pw);
    AtPdhDe1 de1 = NULL;

    if (pwType == cAtPwTypeSAToP)
        de1 = (AtPdhDe1)circuit;
    if (pwType == cAtPwTypeCESoP)
        de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)circuit);

    if (de1 == NULL)
        return cAtOk;

    regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mPwOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mFieldIns(&regVal, cThaPDAE1orT1CEPEBmEnMask, cThaPDAE1orT1CEPEBmEnShift, AtPdhDe1IsE1(de1) ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 MinNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(pw);
	AtUnused(self);
    return 4;
    }

static uint32 HwLimitNumPktForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
	AtUnused(pw);
	AtUnused(self);
    if (payloadSize == 0)
        return 0;

    return (cThaRxBuffMaxBlkMask * cThaPwPdaBlockSize) / payloadSize;
    }

static uint32 MinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    AtUnused(pw);
    AtUnused(self);
    return 2;
    }

static uint32 MaxNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw));
    }

static uint32 CEPMinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->MinNumPacketsForJitterDelay(self, pw);
    }

static uint32 MinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 payloadSizeInBytes = ThaPwUtilPayloadSizeInBytes(pw, circuit, payloadSize);
    uint32 rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit);
    uint32 minNumPackets = mMethodsGet(self)->MinNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw));
    uint32 jitterBufferUs = ThaPwUtilTimeInUsForNumPackets(minNumPackets,
                                          rateInBytePerMs,
                                          payloadSizeInBytes);
    uint32 minJitterBufferUs = mMethodsGet(self)->MinNumMicrosecondsForJitterBufferSize(self, pw, payloadSize);
    return mMax(jitterBufferUs, minJitterBufferUs);
    }

static uint32 CEPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 minJitterBufferUs = mMethodsGet(self)->MinJitterBufferSize(self, pw, circuit, payloadSize);
    if (minJitterBufferUs < cMinCepJitterBufferInUs)
        return cMinCepJitterBufferInUs;

    return minJitterBufferUs;
    }

static uint32 CESoPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 minJitterBufferUs = mMethodsGet(self)->MinJitterBufferSize(self, pw, circuit, payloadSize);
    if (minJitterBufferUs < cMinCESoPJitterBufferInUs)
        return cMinCESoPJitterBufferInUs;

    return minJitterBufferUs;
    }

static uint32 MaxJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 payloadSizeInBytes = ThaPwUtilPayloadSizeInBytes(pw, circuit, payloadSize);
    uint32 rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit);
    uint32 maxNumPackets = mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw));
    uint32 bufferSizeUs = ThaPwUtilTimeInUsForNumPackets(maxNumPackets,
                                                         rateInBytePerMs,
                                                         payloadSizeInBytes);
    uint32 minpayloadSize = ThaModulePdaPwMinPayloadSize(self, pw, circuit, bufferSizeUs);

    uint32 maxBufferSizeUs = mMethodsGet(self)->MaxNumMicrosecondsForJitterBufferSize(self, pw, payloadSize);
    uint32 minpayloadSizeInBytes = ThaPwUtilPayloadSizeInBytes(pw, circuit, minpayloadSize);

    if (minpayloadSizeInBytes > payloadSizeInBytes) /* this case to support the case user use max payloadsize smaller the the pda min payloadsize 1bytes deviation */
        bufferSizeUs = ThaPwUtilTimeInUsForNumPackets((uint32)(maxNumPackets - 1),
                                                      rateInBytePerMs,
                                                      payloadSizeInBytes);
    return mMin(bufferSizeUs, maxBufferSizeUs);
    }

static uint32 MinNumMicrosecondsForJitterDelay(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    return mMethodsGet(self)->MinNumMicrosecondsForJitterBufferSize(self, pw, payloadSize) / 2;
    }

static uint32 MinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 payloadSizeInBytes = ThaPwUtilPayloadSizeInBytes(pw, circuit, payloadSize);
    uint32 rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit);
    uint32 delayUs = ThaPwUtilTimeInUsForNumPackets(mMethodsGet(self)->MinNumPacketsForJitterDelay(self, pw),
                                                    rateInBytePerMs,
                                                    payloadSizeInBytes);
    uint32 minDelayUs = MinNumMicrosecondsForJitterDelay(self, pw, payloadSize);

    return mMax(delayUs, minDelayUs);
    }

static uint32 MaxJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 maxJitterBufferSize = AtPwMaxJitterBufferSize(pw, circuit, payloadSize);
    uint32 maxJitterDelay = AtPwMaxJitterDelayForJitterBufferSize(pw, maxJitterBufferSize);
    AtUnused(self);
    return maxJitterDelay;
    }

static uint32 MaxJitterDelayForJitterBuffer(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us)
    {
    AtUnused(self);
    AtUnused(pw);
    /* Default implementation allows jitter delay max to jitter buffer */
    return jitterBufferSize_us;
    }

static uint32 CEPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    uint32 delayUs = mMethodsGet(self)->MinJitterDelay(self, pw, circuit, payloadSize);
    if (delayUs < cMinCepJitterDelayInUs)
        return cMinCepJitterDelayInUs;

    return delayUs;
    }

static uint32 CESoPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames)
    {
    uint32 delayUs = mMethodsGet(self)->MinJitterDelay(self, pw, circuit, payloadSizeInFrames);
    if (delayUs < cMinCESoPJitterDelayInUs)
        return cMinCESoPJitterDelayInUs;

    return delayUs;
    }

/*
 * Give current jitter buffer size, what is:
 * - Min payload size
 * - Max payload size
 *
 * When payload size is changed ==> number of packets applied for jitter buffer
 * size and delay must be updated to reflect their current values in microsecond.
 *
 * We always have this constrain, let's analyze it
 *     MinNumPacket <= numPacketForTime(jitterBufferUs) <= MaxNumPackets
 * <=> MinNumPacket <= numBytesForUs / payloadSize <= MaxNumPackets
 * <=> numBytesForUs / MaxNumPackets <= payloadSize <= numBytesForUs / MinNumPacket
 *
 * So, MinPayloadSize = (numBytesForUs / MaxNumPackets)
 *     MaxPayloadSize = (numBytesForUs / MinNumPacket)
 */
static uint16 PwMinPayloadSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint32 numBytes = ThaPwUtilNumBytesForTimeInUs(jitterBufferSize, ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit));
    uint32 maxNumPackets = mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw));
    uint32 payloadSize = (uint16)(numBytes / maxNumPackets);

    if (numBytes % maxNumPackets)
        payloadSize = payloadSize + 1;

    return (uint16)((payloadSize == 0) ? 1 : payloadSize);
    }

static uint16 SAToPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
	AtUnused(self);
	AtUnused(pw);
    return 1900;
    }

static uint16 CESoPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->SAToPMaxPayloadSize(self, pw);
    }

static uint16 CESoPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static uint16 CEPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtSdhChannel sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    AtUnused(self);
    if (AtSdhChannelTypeGet(sdhVc) == cAtSdhChannelTypeVc4_4c)
        return 4096;

    /* With current FPGA design, just support 8191 bytes */
    return 8191;
    }

static uint32 PwPayloadSizeWithBufferLimit(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 numBytes, uint32 jitterBufferSize)
    {
    uint32 minNumPackets = mMethodsGet(self)->MinNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw));
    uint32 payloadSizeWithBufferLimit = numBytes / minNumPackets;
    uint32 payloadSize = ThaPwUtilPayloadSizeInByteToHwUnit(pw, circuit, payloadSizeWithBufferLimit);

    if (payloadSize == 0)
        return 0;

    /* Round-down payload size until jitter buffer is no longer less than minimum jitter buffer. */
    while ((AtPwMinJitterBufferSize(pw, circuit, (uint16)payloadSize) > jitterBufferSize))
        {
        payloadSize--;
        }

    return ThaPwUtilPayloadSizeInBytes(pw, circuit, payloadSize);
    }

static uint32 PwMaxPayloadSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize)
    {
    uint32 numBytesForTimeInUs = ThaPwUtilNumBytesForTimeInUs(jitterBufferSize, ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit));
    uint32 payloadSizeWithBufferLimit = PwPayloadSizeWithBufferLimit(self, pw, circuit, numBytesForTimeInUs, jitterBufferSize);
    uint32 payloadSizeWithLopsLimit;
    uint32 lopsThreshold;
    uint32 maxPayloadSize;

    if (mMethodsGet(self)->LopsThresholdHasLimitOneSecond(self) == cAtFalse)
        return (payloadSizeWithBufferLimit == 0) ? 1 : payloadSizeWithBufferLimit;

    lopsThreshold = AtPwLopsSetThresholdGet(pw);
    if (lopsThreshold == 0)
        return payloadSizeWithBufferLimit;

    numBytesForTimeInUs = ThaPwUtilNumBytesForTimeInUs(cMaxLopsInUs, ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit));
    payloadSizeWithLopsLimit = numBytesForTimeInUs / lopsThreshold;
    maxPayloadSize = mMin(payloadSizeWithLopsLimit, payloadSizeWithBufferLimit);

    return (maxPayloadSize == 0) ? 1 : maxPayloadSize;
    }

static uint32 CalculateNumberOfBlockByNumOfPacket(AtPw pw, uint32 numPackets)
    {
    uint16 payloadSize = AtPwPayloadSizeGet(pw);
    uint32 numBlockPerPkt = (uint32)mRoundUpDivide(payloadSize, cThaPwPdaBlockSize);
    return mRoundUpDivide(numPackets, 2) * numBlockPerPkt * 2;
    }

static eAtRet PwJitterBufferSizeInPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address;
    uint32 numBlocks;

    numBlocks = CalculateNumberOfBlockByNumOfPacket(pw, numPackets);
    address = cThaRegPwPdaRxBufManmentMemoryAllocationCtrl + mPwOffset(self, pw);
    mChannelHwWrite(pw, address, numBlocks, cThaModulePda);

    return cAtOk;
    }

static uint32 CalculateNumOfPacketByNumberOfBlock(AtPw pw, uint32 numBlocks)
    {
    uint16 payloadSize = AtPwPayloadSizeGet(pw);
    uint32 numBlockPerPkt = (uint32)mRoundUpDivide(payloadSize, cThaPwPdaBlockSize);
    return mRoundUpDivide(numBlocks, numBlockPerPkt);
    }

static uint16 PwJitterBufferSizeInPacketGet(ThaModulePda self, AtPw pw)
    {
    uint32 address;
    uint32 numBlocks;

    address = cThaRegPwPdaRxBufManmentMemoryAllocationCtrl + mPwOffset(self, pw);
    numBlocks = mChannelHwRead(pw, address, cThaModulePda);

    return (uint16)CalculateNumOfPacketByNumberOfBlock(pw, numBlocks);
    }

static eAtRet PwJitterBufferDelayInPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address, regVal;

    address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaRxBuffNumActMask, cThaRxBuffNumActShift, numPackets);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return cAtOk;
    }

static uint16 PwJitterBufferDelayInPacketGet(ThaModulePda self, AtPw pw)
    {
    uint32 address, regVal;

    address = cThaRegPwPdaRxBufManmentThressCtrl + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);

    return (uint16)mRegField(regVal, cThaRxBuffNumAct);
    }

static uint16 NumberOfPacketByUs(ThaModulePda self, AtPw pw, uint32 numUs)
    {
    AtUnused(self);
    return (uint16)CalculateNumPacketsByUs(pw, (uint16)numUs, PwPayloadSizeInBytes(pw));
    }

static uint32 UsByNumberOfPacket(ThaModulePda self, AtPw pw, uint32 numPkts)
    {
	AtUnused(self);
    return CalculateUsByNumPackets(pw, numPkts);
    }

static eBool JitterBufferCenterIsSupported(ThaModulePda self)
    {
	AtUnused(self);
    /* Although feature should be supported for all of PW products, but
     * at this moment, not all of FPGAs can support this mode. So, let concrete
     * product determine whether this feature is used or not */
    return cAtFalse;
    }

static eBool IsSimulated(ThaModulePda self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self));
    }

static eBool CenterEngineIsReady(ThaModulePda self, AtPw pw)
    {
    const uint32 cTimeoutMs = 1500;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regAddr, regVal;

    regAddr = cThaRegPwPdaRxBufManmentInitializedReadOnly + mPwOffset(self, pw);
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cTimeoutMs)
        {
        regVal = mChannelHwRead(pw, regAddr, cThaModulePda);

        if (IsSimulated(self))
            return cAtTrue;

        if (regVal == 0) /* Ready */
            return cAtTrue;

        /* Continue waiting */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtFalse;
    }

static void CenterEngineReadyForce(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr = cThaRegPwPdaRxBufManmentInitializedReadOnly + mPwOffset(self, pw);
    mChannelHwWrite(pw, regAddr, 0, cThaModulePda);
    }

static eAtRet PwCenterJitterStart(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal = 0;

    if (!mMethodsGet(self)->JitterBufferCenterIsSupported(self))
        return cAtOk;

    /* Engine is not ready for some reason, just force it become ready for next
     * action. If not, jitter configuration will be fail forever */
    if (!CenterEngineIsReady(self, pw))
        {
        AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation,
                     "Jitter center engine is not ready, it is forced to ready state\r\n");
        CenterEngineReadyForce(self, pw);
        return cAtErrorDevBusy;
        }

    /* Make request for this PW */
    regVal   = 0;
    regAddr  = cThaRegPwPdaRxBufManmentInitializedCtrl + mPwOffset(self, pw);
    mRegFieldSet(regVal, cThaInitQueID, mPwHwId(pw));
    mRegFieldSet(regVal, cThaInitReq, 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    /* Check for ready */
    if (!CenterEngineIsReady(self, pw))
        {
        AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation,
                     "Jitter center engine cannot finish request, it is forced to ready state\r\n");
        CenterEngineReadyForce(self, pw);
        return cAtErrorDevBusy;
        }

    return cAtOk;
    }

static eAtRet PwCenterJitterStop(ThaModulePda self, AtPw pw)
    {
    uint32 regVal;

    AtUnused(self);

    regVal  = mChannelHwRead((AtChannel)pw, cThaRegPwPdaRxBufManmentInitializedCtrl, cThaModulePda);
    mRegFieldSet(regVal, cThaInitReq, 0);
    mChannelHwWrite((AtChannel)pw, cThaRegPwPdaRxBufManmentInitializedCtrl, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    /* Not all of products can support this. So let concrete classes do */
    return 0;
    }

static eAtRet VcDeAssemblerSet(ThaModulePda self, AtSdhChannel vc)
    {
	AtUnused(vc);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PwCircuitIdSet(ThaModulePda self, AtPw pw)
    {
    uint8 slice, sts;
    uint32 address, regVal;
    AtSdhChannel vc;

    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtOk;

    vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    ThaSdhChannel2HwMasterStsId((AtSdhChannel)vc, cThaModulePda, &slice, &sts);
    address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal  = mChannelHwRead(pw, address, cThaModulePda);
    mFieldIns(&regVal, cThaPDANxDS0CEPStsIdMask, cThaPDANxDS0CEPStsIdShift, sts);
    mChannelHwWrite(pw, address, regVal, cThaModulePda);

    return ThaModulePdaVcDeAssemblerSet(self, vc);
    }

static eAtRet PwCircuitIdReset(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static eAtRet PwPdaTdmLookupEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static uint32 EparLineIdLkCtr(ThaModulePda self)
    {
	AtUnused(self);
    return cThaRegPDAEparLineIdLkCtr;
    }

static eAtRet PwCepEquip(ThaModulePda self, AtPw pw, AtChannel subChannel, eBool equip)
    {
	AtUnused(equip);
	AtUnused(subChannel);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PwCepEbmEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
	AtUnused(enable);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

/* TOH pseudowire */
static eAtRet BindLineToPw(ThaModulePda self, AtSdhLine line, AtPw pseudowire)
    {
	AtUnused(pseudowire);
	AtUnused(line);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PwTohByteEnable(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
	AtUnused(enable);
	AtUnused(tohByte);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PwTohFirstByteEnable(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
	AtUnused(enable);
	AtUnused(tohByte);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static tThaTohByte* PwTohFirstByteGet(ThaModulePda self, AtPw pw, tThaTohByte *tohByte)
    {
	AtUnused(tohByte);
	AtUnused(pw);
	AtUnused(self);
    return NULL;
    }

static eAtRet PwTohEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
	AtUnused(enable);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static uint8 PwTohBytesBitmapGet(ThaModulePda self, AtPw pw, uint32 *bitmaps, uint8 numBitmap)
    {
	AtUnused(bitmaps);
	AtUnused(pw);
	AtUnused(self);
	AtUnused(numBitmap);
    return 0;
    }

static eBool PwIdleCodeIsSupported(ThaModulePda self)
    {
    return ThaModulePwIdleCodeIsSupported(PwModule(self));
    }

static eAtModulePwRet PwIdleCodeSet(ThaModulePda self, ThaPwAdapter pwAdapter, uint8 idleCode)
    {
	AtUnused(idleCode);
	AtUnused(pwAdapter);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8  PwIdleCodeGet(ThaModulePda self, ThaPwAdapter pwAdapter)
    {
	AtUnused(pwAdapter);
    return mMethodsGet(self)->ModuleIdleCodeGet(self);
    }

static eAtRet PwCircuitSliceSet(ThaModulePda self, AtPw pw, uint8 slice)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(slice);
    return cAtOk;
    }

static uint32 PktReplaceModeMask(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaPDARepModeMask;
    }

static uint8 PktReplaceModeShift(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaPDARepModeShift;
    }

static eAtRet PwLopsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    /* Default device does not support this threshold, just support loss of frame state defined by MEF 8 */
    return mMethodsGet(self)->PwLofsSetThresholdSet(self, pw, numPackets);
    }

static uint32 PwLopsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    /* Default device does not support this threshold, just support loss of frame state defined by MEF 8 */
    return mMethodsGet(self)->PwLofsSetThresholdGet(self, pw);
    }

static eAtRet PwLopsClearThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    /* Default device does not support this threshold, just support loss of frame state defined by MEF 8 */
    return mMethodsGet(self)->PwLofsClearThresholdSet(self, pw, numPackets);
    }

static uint32 PwLopsClearThresholdGet(ThaModulePda self, AtPw pw)
    {
    /* Default device does not support this threshold, just support loss of frame state defined by MEF 8 */
    return mMethodsGet(self)->PwLofsClearThresholdGet(self, pw);
    }

static eAtRet AutoInsertRdiEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
	AtUnused(pw);
	AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool AutoInsertRdiIsEnabled(ThaModulePda self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldAutoInsertRdi(ThaModulePda self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool CircuitIsDe3(AtChannel circuit)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet((AtPdhChannel)circuit);

    if ((channelType == cAtPdhChannelTypeE3) ||
        (channelType == cAtPdhChannelTypeDs3))
        return cAtTrue;

    return cAtFalse;
    }

static uint16 SAToPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint32 circuitRate = AtChannelDataRateInBytesPer125Us(circuit);
    AtUnused(self);

    /* Return default when circuit is NULL */
    if (circuitRate == 0)
        return 64;

    if (CircuitIsDe3(circuit))
        return 256;

    return (uint16)circuitRate;
    }

static uint16 CEPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    eAtSdhChannelType vcType;
    AtUnused(self);

    vcType = AtSdhChannelTypeGet((AtSdhChannel)circuit);
    if ((vcType == cAtSdhChannelTypeVc11) || (vcType == cAtSdhChannelTypeVc12))
        return 16;

    if ((vcType == cAtSdhChannelTypeVc3) || (vcType == cAtSdhChannelTypeVc4))
        return 32;

    /* Default */
    return 64;
    }

static eBool CEPPayloadSizeIsValid(ThaModulePda self, AtPw pw, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(payloadSize);
    return cAtTrue;
    }

static uint16 CESoPMaxNumFrames(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* We do not know exactly history. Just use the default value as same as
     * defined in module PWE to not break logic. */
    return 512;
    }

static uint16 CESoPMinNumFrames(ThaModulePda self, AtPw pw)
    {
    AtPdhNxDS0 circuit = (AtPdhNxDS0)AtPwBoundCircuitGet(pw);
    AtUnused(self);
    /* As same constrain defined in CESoP adapter. */
    return (AtPdhNxDs0NumTimeslotsGet(circuit) >= 2) ? 1 : 2;
    }

static void PwRegsShow(ThaModulePda self, AtPw pw)
    {
    /* Let concrete product do */
    AtUnused(self);
    AtUnused(pw);
    }

static uint32 EparLineIdMask(ThaModulePda self)
    {
    AtUnused(self);
    return cThaPDAPdaEparLineIdMask;
    }

static uint8 EparLineIdShift(ThaModulePda self)
    {
    AtUnused(self);
    return cThaPDAPdaEparLineIdShift;
    }

static uint32 EparSliceIdMask(ThaModulePda self)
    {
    /* Common products do not have this, return zero so that no fields would be touched */
    AtUnused(self);
    return 0;
    }

static uint8 EparSliceIdShift(ThaModulePda self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PwEparEnable(ThaModulePda self, AtPw pwAdapter, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 slice, hwSts;
    uint32 flatId;
    uint32 mask, shift;
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pwAdapter);
    if ((sdhChannel == NULL) || (pwAdapter == NULL))
        mChannelError(pwAdapter, cAtErrorNullPointer);

    if (enable)
        ThaPwAdapterStartCenterJitterBuffer(pwAdapter);

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePda, &slice, &hwSts);
    flatId = (uint32)((hwSts * 32) + (AtSdhChannelTug2Get(sdhChannel) * 4) + AtSdhChannelTu1xGet(sdhChannel));
    regAddr = mMethodsGet(self)->EparLineIdLkCtr(self) + mPwOffset(self, pwAdapter);
    regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);

    mask  = mMethodsGet(self)->EparSliceIdMask(self);
    shift = mMethodsGet(self)->EparSliceIdShift(self);
    mFieldIns(&regVal, mask, shift, slice);

    mask  = mMethodsGet(self)->EparLineIdMask(self);
    shift = mMethodsGet(self)->EparLineIdShift(self);
    mFieldIns(&regVal, mask, shift, flatId);

    mFieldIns(&regVal, cThaPDAPdaEparLineEnMask, cThaPDAPdaEparLineEnShift, enable ? 1 : 0);
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePda);

    if (enable)
        ThaPwAdapterStopCenterJitterBuffer(pwAdapter);

    return cAtOk;
    }

static eBool PwEparIsEnabled(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr, regVal;
    uint8 slice, hwSts;
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPwBoundCircuitGet(pwAdapter);
    if ((sdhChannel == NULL) || (pwAdapter == NULL))
        return cAtFalse;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModulePda, &slice, &hwSts);
    regAddr = mMethodsGet(self)->EparLineIdLkCtr(self) + mPwOffset(self, pwAdapter);
    regVal = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    return mBinToBool(mRegField(regVal, cThaPDAPdaEparLineEn));
    }

static eBool NeedProtectWhenSetLopsThreshold(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 MaxNumJitterBufferBlocks(ThaModulePda self)
    {
    /* Concrete product will override this */
    AtUnused(self);
    return 0;
    }

static uint32 JitterBufferBlockSizeInByte(ThaModulePda self)
    {
    /* Concrete product will override this */
    AtUnused(self);
    return 0;
    }

static uint32 TotalOccupiedBlocksCalculate(ThaModulePda self, AtPw adapter, uint32 newPwNumBlocks)
    {
    uint32 pwNumBlock = AtPwJitterBufferNumBlocksGet(adapter);
    uint32 newTotalOccupiedBlocks = self->numOccupiedBlocks + newPwNumBlocks;

    if (newTotalOccupiedBlocks < pwNumBlock)
        return 0;

    return (newTotalOccupiedBlocks - pwNumBlock);
    }

static uint32 JitterBufferEmptyTimeout(ThaModulePda self, AtPw pw)
    {
    uint32 timeoutMs = AtPwJitterBufferSizeGet(pw) / 1000;
    uint32 additionalTimeoutMs = mMethodsGet(mThis(self))->JitterBufferEmptyAdditionalTimeout(mThis(self));
    return timeoutMs + additionalTimeoutMs;
    }

static uint32 CalculateNumPacketsByUsAndPayload(ThaModulePda self, AtPw adapter, uint32 bufferSizeInUs, uint32 payloadSizeInByte)
    {
    AtUnused(self);
    return CalculateNumPacketsByUs(adapter, bufferSizeInUs, payloadSizeInByte);
    }

static uint32 NumUnUsedBlock(ThaModulePda self)
    {
    return mMethodsGet(self)->MaxNumJitterBufferBlocks(self) - self->numOccupiedBlocks;
    }

static uint32 NumBlockPerPacket(ThaModulePda self, uint32 payloadSizeInByte)
    {
    uint32 jitterBufferBlockSizeInByte = mMethodsGet(self)->JitterBufferBlockSizeInByte(self);
    uint32 numBlockForPayload;

    if (jitterBufferBlockSizeInByte == 0)
        return 0;

    numBlockForPayload = payloadSizeInByte / jitterBufferBlockSizeInByte;
    if ((payloadSizeInByte % jitterBufferBlockSizeInByte) != 0)
        numBlockForPayload = numBlockForPayload + 1;

    return numBlockForPayload;
    }

static uint32 MaxNumPacketByUnUsedBlocks(ThaModulePda self, uint32 payloadSizeInByte)
    {
    uint32 numBlockPerPacket;

    if (self == NULL)
        return cInvalidUint32;

    numBlockPerPacket = NumBlockPerPacket(self, payloadSizeInByte);
    if (numBlockPerPacket)
        return NumUnUsedBlock(self) / numBlockPerPacket;

    return cInvalidUint32;
    }

static uint32 MaxNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    uint32 maxNumPacketsBufferSize = mMethodsGet(self)->HwLimitNumPktForJitterBufferSize(self, pw, payloadSize);
    if (!NeedHandleBufferRamLimitation(self))
        return maxNumPacketsBufferSize;

    return mMin(maxNumPacketsBufferSize, MaxNumPacketByUnUsedBlocks(self, ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw)));
    }

static uint32 MinNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    return 0;
    }

static uint32 MaxNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(pw);
    AtUnused(self);
    return cBit31_0;
    }

static eBool CepPayloadSizeShouldExcludeHeader(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet CepIndicate(ThaModulePda self, AtPw pw, eBool isCep)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(isCep);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModulePda object = (ThaModulePda)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(jitterCenterDisabled);
    mEncodeUInt(numOccupiedBlocks);
    mEncodeNone(maxJitterEmptyWaitingTime);
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "pda";
    }

static void HdlcChannelRegsShow(ThaModulePda self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(self);
    AtUnused(hdlcChannel);
    }

static eBool NeedPreventReconfigure(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(payloadType);
    return cAtErrorNotImplemented;
    }

static uint32 BaseAddress(ThaModulePda self)
    {
    AtUnused(self);
    return 0;
    }

static eBool LimitResourceIsEnabled(AtPw self)
    {
    ThaModulePw  pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)self);
    return ThaModulePwResourcesLimitationIsDisabled(pwModule) ? cAtFalse : cAtTrue;
    }

static uint32 PrbsStatusBase(ThaModulePda self)
    {
    AtUnused(self);
    return 0x80800;
    }

static uint32 PrbsStatusRegister(ThaModulePda self)
    {
    return ThaModulePdaBaseAddress(self) + mMethodsGet(self)->PrbsStatusBase(self);
    }

static eAtRet PwTohModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, 0);
    }

static eBool IsDe3(AtPdhChannel circuit)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet(circuit);
    if ((channelType == cAtPdhChannelTypeDs3) || (channelType == cAtPdhChannelTypeE3))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 SatopMode(AtChannel circuit)
    {
    if (IsDe3((AtPdhChannel)circuit))
        return 0xF;
    return 0;
    }

static eAtRet SAToPModeSet(ThaModulePda self, ThaPwAdapter adapter, AtChannel circuit)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, SatopMode(circuit));
    }

static uint8 CESoPMode(ThaPwAdapter adapter)
    {
    static const uint8 cBasicMode        = 2;
    static const uint8 cBasicWithCasMode = 3;
    AtPwCESoP pw = (AtPwCESoP)ThaPwAdapterPwGet(adapter);

    switch (AtPwCESoPModeGet(pw))
        {
        case cAtPwCESoPModeBasic  : return cBasicMode;
        case cAtPwCESoPModeWithCas: return cBasicWithCasMode;
        case cAtPwCESoPModeUnknown:
        default                   : return cBasicMode;
        }
    }

static eAtRet CESoPModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, CESoPMode(adapter));
    }

static eAtRet CESoPCasModeSet(ThaModulePda self, ThaPwAdapter adapter, eAtPwCESoPCasMode mode)
    {
    AtPw pw = ThaPwAdapterPwGet(adapter);
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPwBoundCircuitGet(pw);
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxDs0);
    AtUnused(self);

    if (AtPdhDe1IsE1(de1))
        return (mode == cAtPwCESoPCasModeE1) ? cAtOk : cAtErrorModeNotSupport;

    return (mode == cAtPwCESoPCasModeDs1) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPwCESoPCasMode CESoPCasModeGet(ThaModulePda self, ThaPwAdapter adapter)
    {
    AtPw pw = ThaPwAdapterPwGet(adapter);
    AtPdhNxDS0 nxDs0 = (AtPdhNxDS0)AtPwBoundCircuitGet(pw);
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(nxDs0);
    AtUnused(self);

    if (AtPdhDe1IsE1(de1))
        return cAtPwCESoPCasModeE1;

    return cAtPwCESoPCasModeDs1;
    }

static uint8 CepMode(AtSdhChannel sdhVc)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhVc);

    if (vcType == cAtSdhChannelTypeVc4_4c)
        return 8;

    if (vcType == cAtSdhChannelTypeVc3)
        {
        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3MapC3)
            return 8;

        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc3MapDe3)
            return 10;

        return 9; /* VC3 fractional VC1x */
        }

    if (vcType == cAtSdhChannelTypeVc4)
        {
        if (AtSdhChannelMapTypeGet(sdhVc) == cAtSdhVcMapTypeVc4MapC4)
            return 8;

        return 11; /* Vc4 fractional vc1x or vc3 */
        }

    if ((vcType == cAtSdhChannelTypeVc12) || (vcType == cAtSdhChannelTypeVc11))
        return 12;

    return 0xFF;
    }

static eAtRet CepModeSet(ThaModulePda self, ThaPwAdapter adapter, AtSdhChannel sdhVc)
    {
    return ThaModulePdaPwPdaModeSet(self, (AtPw)adapter, CepMode(sdhVc));
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PwCwAutoRxMBitEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    AtUnused(self);

    if (AtPwTypeGet(pw) != cAtPwTypeCESoP)
        return cAtErrorInvlParm;

    /* Cannot disable with current FPGA */
    if (enable)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool PwCwAutoRxMBitIsEnabled(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);

    if (AtPwTypeGet(pw) == cAtPwTypeCESoP)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet MlpppPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(payloadType);
    return cAtErrorNotImplemented;
    }

static uint32 NumFreeBlocksHwGet(ThaModulePda self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    ThaModulePda modulePda = (ThaModulePda)self;
    uint32 maxNumBlocks = mMethodsGet(modulePda)->MaxNumJitterBufferBlocks(modulePda);
    uint32 numFreeBlocks = mMethodsGet(modulePda)->NumFreeBlocksHwGet(modulePda);
    AtDevice device = AtModuleDeviceGet(self);

    if (AtDeviceIsSimulated(device) || AtDeviceInAccessible(device))
        return cAtOk;

    AtModuleResourceDiminishedLog(self, "Jitter buffer blocks", numFreeBlocks, maxNumBlocks);
    return cAtOk;
    }

static void DebuggerEntriesFill(AtModule self, AtDebugger debugger)
    {
    ThaModulePda modulePda = (ThaModulePda)self;
    uint32 maxNumBlocks = mMethodsGet(modulePda)->MaxNumJitterBufferBlocks(modulePda);
    uint32 numFreeBlocks = mMethodsGet(modulePda)->NumFreeBlocksHwGet(modulePda);

    if (!AtDeviceShouldCheckHwResource(AtModuleDeviceGet(self)))
        return;

    m_AtModuleMethods->DebuggerEntriesFill(self, debugger);
    AtDebuggerEntryAdd(debugger, AtDebugEntryInfoResourceNew("Jitter buffer blocks", maxNumBlocks, numFreeBlocks));
    }

static eBool CanControlLopsPktReplaceMode(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(ThaModulePda self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PartDefaultSet);
        mMethodOverride(m_methods, PwDefaultOffset);
        mMethodOverride(m_methods, PwLopsThresholdMax);
        mMethodOverride(m_methods, PwLopsThresholdMin);
        mMethodOverride(m_methods, PwTdmOffset);
        mMethodOverride(m_methods, PwLofsSetThresholdSet);
        mMethodOverride(m_methods, PwLofsSetThresholdGet);
        mMethodOverride(m_methods, PwLopsThresholdMax);
        mMethodOverride(m_methods, PwLopsThresholdMin);
        mMethodOverride(m_methods, PwLofsClearThresholdSet);
        mMethodOverride(m_methods, PwLofsClearThresholdGet);
        mMethodOverride(m_methods, PwLopsThresholdIsValid);
        mMethodOverride(m_methods, PwLopsSetThresholdSet);
        mMethodOverride(m_methods, PwLopsSetThresholdGet);
        mMethodOverride(m_methods, PwLopsClearThresholdSet);
        mMethodOverride(m_methods, PwLopsClearThresholdGet);
        mMethodOverride(m_methods, PwReorderingEnable);
        mMethodOverride(m_methods, PwReorderingIsEnable);
        mMethodOverride(m_methods, PwJitterBufferSizeIsSupported);
        mMethodOverride(m_methods, PwJitterBufferDelayIsSupported);
        mMethodOverride(m_methods, PwJitterBufferSizeIsValid);
        mMethodOverride(m_methods, PwJitterDelayIsValid);
        mMethodOverride(m_methods, PwJitterBufferAndJitterDelayAreValid);
        mMethodOverride(m_methods, ShouldSetDefaultJitterBufferSizeOnJitterDelaySet);
        mMethodOverride(m_methods, DefaultJitterBufferSize);
        mMethodOverride(m_methods, MinNumPacketsForJitterBufferSize);
        mMethodOverride(m_methods, MaxNumPacketsForJitterBufferSize);
        mMethodOverride(m_methods, HwLimitNumPktForJitterBufferSize);
        mMethodOverride(m_methods, MinNumPacketsForJitterDelay);
        mMethodOverride(m_methods, MaxNumPacketsForJitterDelay);
        mMethodOverride(m_methods, CEPMinNumPacketsForJitterDelay);
        mMethodOverride(m_methods, PwJitterBufferDelaySet);
        mMethodOverride(m_methods, PwJitterBufferDelayGet);
        mMethodOverride(m_methods, MinJitterDelay);
        mMethodOverride(m_methods, CEPMinJitterDelay);
        mMethodOverride(m_methods, CESoPMinJitterDelay);
        mMethodOverride(m_methods, MaxJitterDelay);
        mMethodOverride(m_methods, MaxJitterDelayForJitterBuffer);
        mMethodOverride(m_methods, PwJitterBufferSizeSet);
        mMethodOverride(m_methods, PwJitterBufferSizeGet);
        mMethodOverride(m_methods, MinJitterBufferSize);
        mMethodOverride(m_methods, CEPMinJitterBufferSize);
        mMethodOverride(m_methods, CESoPMinJitterBufferSize);
        mMethodOverride(m_methods, MaxJitterBufferSize);
        mMethodOverride(m_methods, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_methods, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_methods, PwJitterBufferIsEmpty);
        mMethodOverride(m_methods, PwPdaModeSet);
        mMethodOverride(m_methods, PwPdaModeGet);
        mMethodOverride(m_methods, PwCwSequenceModeSet);
        mMethodOverride(m_methods, LbitPktReplaceModeSet);
        mMethodOverride(m_methods, LbitPktReplaceModeGet);
        mMethodOverride(m_methods, LopPktReplaceModeSet);
        mMethodOverride(m_methods, LopPktReplaceModeGet);
        mMethodOverride(m_methods, PwCwPktReplaceModeSet);
        mMethodOverride(m_methods, PwCwPktReplaceModeGet);
        mMethodOverride(m_methods, PwLopsPktReplaceModeSet);
        mMethodOverride(m_methods, PwLopsPktReplaceModeGet);
        mMethodOverride(m_methods, CanControlLopsPktReplaceMode);
        mMethodOverride(m_methods, PktReplaceModeIsSupported);
        mMethodOverride(m_methods, ModuleIdleCodeSet);
        mMethodOverride(m_methods, ModuleIdleCodeGet);
        mMethodOverride(m_methods, ModuleIdleCodeIsSupported);
        mMethodOverride(m_methods, PwRxBytesGet);
        mMethodOverride(m_methods, PwRxReorderedPacketsGet);
        mMethodOverride(m_methods, PwRxLostPacketsGet);
        mMethodOverride(m_methods, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_methods, PwRxJitBufOverrunGet);
        mMethodOverride(m_methods, PwRxJitBufUnderrunGet);
        mMethodOverride(m_methods, PwRxLopsGet);
        mMethodOverride(m_methods, PwRxPacketsSentToTdmGet);
        mMethodOverride(m_methods, PwRxPacketsSentToTdmCounterIsSupported);
        mMethodOverride(m_methods, PwJitterPayloadLengthSet);
        mMethodOverride(m_methods, PwDebug);
        mMethodOverride(m_methods, PwNumDs0TimeslotsSet);
        mMethodOverride(m_methods, PwCircuitTypeSet);
        mMethodOverride(m_methods, PwAlarmGet);
        mMethodOverride(m_methods, PwDefectHistoryGet);
        mMethodOverride(m_methods, PwMinPayloadSize);
        mMethodOverride(m_methods, PwMaxPayloadSize);
        mMethodOverride(m_methods, PwJitterBufferSizeInPacketSet);
        mMethodOverride(m_methods, PwJitterBufferSizeInPacketGet);
        mMethodOverride(m_methods, PwJitterBufferDelayInPacketSet);
        mMethodOverride(m_methods, PwJitterBufferDelayInPacketGet);
        mMethodOverride(m_methods, JitterBufferWatermarkMinPackets);
        mMethodOverride(m_methods, JitterBufferWatermarkMaxPackets);
        mMethodOverride(m_methods, JitterBufferWatermarkIsSupported);
        mMethodOverride(m_methods, JitterBufferWatermarkReset);
        mMethodOverride(m_methods, NumberOfPacketByUs);
        mMethodOverride(m_methods, UsByNumberOfPacket);
        mMethodOverride(m_methods, HotConfigureStart);
        mMethodOverride(m_methods, HotConfigureStop);
        mMethodOverride(m_methods, JitterBufferCenterIsSupported);
        mMethodOverride(m_methods, PwCenterJitterStart);
        mMethodOverride(m_methods, PwCenterJitterStop);
        mMethodOverride(m_methods, JitterBufferState);
        mMethodOverride(m_methods, PwCircuitIdSet);
        mMethodOverride(m_methods, PwCircuitIdReset);
        mMethodOverride(m_methods, PwPdaTdmLookupEnable);
        mMethodOverride(m_methods, VcDeAssemblerSet);
        mMethodOverride(m_methods, SAToPMaxPayloadSize);
        mMethodOverride(m_methods, CESoPMaxPayloadSize);
        mMethodOverride(m_methods, CESoPMinPayloadSize);
        mMethodOverride(m_methods, CEPMaxPayloadSize);
        mMethodOverride(m_methods, PwIdleCodeIsSupported);
        mMethodOverride(m_methods, PwIdleCodeSet);
        mMethodOverride(m_methods, PwIdleCodeGet);
        mMethodOverride(m_methods, PwCircuitSliceSet);
        mMethodOverride(m_methods, PwCepEquip);
        mMethodOverride(m_methods, PwCepEbmEnable);
        mMethodOverride(m_methods, BindLineToPw);
        mMethodOverride(m_methods, PwTohByteEnable);
        mMethodOverride(m_methods, PwTohFirstByteEnable);
        mMethodOverride(m_methods, PwTohFirstByteGet);
        mMethodOverride(m_methods, PwTohEnable);
        mMethodOverride(m_methods, PwTohBytesBitmapGet);
        mMethodOverride(m_methods, AutoInsertRdiEnable);
        mMethodOverride(m_methods, AutoInsertRdiIsEnabled);
        mMethodOverride(m_methods, ShouldAutoInsertRdi);
        mMethodOverride(m_methods, SAToPMinPayloadSize);
        mMethodOverride(m_methods, CEPMinPayloadSize);
        mMethodOverride(m_methods, CESoPMaxNumFrames);
        mMethodOverride(m_methods, CESoPMinNumFrames);
        mMethodOverride(m_methods, JitterBufferStateString);
        mMethodOverride(m_methods, PwRegsShow);
        mMethodOverride(m_methods, PwEparEnable);
        mMethodOverride(m_methods, PwEparIsEnabled);
        mMethodOverride(m_methods, NeedProtectWhenSetLopsThreshold);
        mMethodOverride(m_methods, MaxNumJitterBufferBlocks);
        mMethodOverride(m_methods, JitterBufferBlockSizeInByte);
        mMethodOverride(m_methods, NumFreeBlocksHwGet);
        mMethodOverride(m_methods, JitterBufferEmptyAdditionalTimeout);
        mMethodOverride(m_methods, CalculateNumPacketsByUsAndPayload);
        mMethodOverride(m_methods, MinNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_methods, MaxNumMicrosecondsForJitterBufferSize);
        mMethodOverride(m_methods, CepPayloadSizeShouldExcludeHeader);
        mMethodOverride(m_methods, CepIndicate);
        mMethodOverride(m_methods, LopsThresholdHasLimitOneSecond);
        mMethodOverride(m_methods, HdlcChannelRegsShow);
        mMethodOverride(m_methods, NeedPreventReconfigure);
        mMethodOverride(m_methods, HdlcPwPayloadTypeSet);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, MlpppPwPayloadTypeSet);
        mMethodOverride(m_methods, PrbsStatusBase);
        mMethodOverride(m_methods, PwTohModeSet);
        mMethodOverride(m_methods, SAToPModeSet);
        mMethodOverride(m_methods, CESoPModeSet);
        mMethodOverride(m_methods, CESoPCasModeSet);
        mMethodOverride(m_methods, CESoPCasModeGet);
        mMethodOverride(m_methods, CepModeSet);
        mMethodOverride(m_methods, CEPPayloadSizeIsValid);

        /* Registers */
        mMethodOverride(m_methods, PwPdaIdleCodeCtrl);
        mMethodOverride(m_methods, PWPdaTdmModeCtrl);
        mMethodOverride(m_methods, EparLineIdLkCtr);

        /* Bit fields */
        mBitFieldOverride(ThaModulePda, m_methods, PktReplaceMode)
        mBitFieldOverride(ThaModulePda, m_methods, EparLineId)
        mBitFieldOverride(ThaModulePda, m_methods, EparSliceId)

        /* Counters */
        mMethodOverride(m_methods, CounterOffset);

        /* CesOPSM, Rx Bit control */
        mMethodOverride(m_methods, PwCwAutoRxMBitEnable);
        mMethodOverride(m_methods, PwCwAutoRxMBitIsEnabled);
        mMethodOverride(m_methods, PwCwAutoRxMBitIsConfigurable);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, HwResourceCheck);
        mMethodOverride(m_AtModuleOverride, DebuggerEntriesFill);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModule ThaModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaModulePda));

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModulePda, device) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Methods init */
    MethodsInit((ThaModulePda)self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaModulePda));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePdaObjectInit(newModule, device);
    }

static void HdlcBundleChannelRegsShow(ThaModulePda self, AtEthFlow flow)
    {
    uint8 linkIndex;
    AtHdlcLink hdlcLink;
    AtHdlcChannel hdlcChannel;
    AtMpBundle mpBundle = AtEthFlowMpBundleGet(flow);

    for (linkIndex = 0; linkIndex < AtHdlcBundleNumberOfLinksGet((AtHdlcBundle)mpBundle); linkIndex++)
        {
        hdlcLink = AtHdlcBundleLinkGet((AtHdlcBundle)mpBundle, linkIndex);
        hdlcChannel = AtHdlcLinkHdlcChannelGet(hdlcLink);
        mMethodsGet(self)->HdlcChannelRegsShow(self, hdlcChannel);
        }
    }

void ThaModulePdaEthFlowRegsShow(AtEthFlow flow)
    {
    ThaModulePda modulePda = (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModulePda);
    AtHdlcLink hdlcLink = AtEthFlowHdlcLinkGet(flow);
    AtHdlcChannel hdlcChannel = AtHdlcLinkHdlcChannelGet(hdlcLink);

    AtPrintc(cSevInfo, "* PDA registers of '%s':\r\n", AtObjectToString((AtObject)flow));

    if (hdlcLink)
        mMethodsGet(modulePda)->HdlcChannelRegsShow(modulePda, hdlcChannel);
    else
        HdlcBundleChannelRegsShow(modulePda, flow);
    }

eAtModulePwRet ThaModulePdaPwLopsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 currentThreshold;
    eBool claEnabled;
    eAtRet ret = cAtOk;

    /* Check parameter */
    if (!ThaModulePdaPwLopsThresholdIsValid(self, pw, numPackets))
        {
        AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation,
                     "Can not set LOPS threshold due to value is not valid\r\n");
        return cAtErrorOutOfRangParm;
        }

    /* Do nothing if there is no change */
    currentThreshold = mMethodsGet(self)->PwLopsSetThresholdGet(self, pw);
    if (numPackets == currentThreshold)
        return cAtOk;

    if (!mMethodsGet(self)->NeedProtectWhenSetLopsThreshold(self))
        {
        mMethodsGet(self)->PwLopsSetThresholdSet(self, pw, numPackets);
        return cAtOk;
        }

    claEnabled = ThaPwAdapterClaIsEnabled(pw);
    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwLopsSetThresholdSet(self, pw, numPackets);
    ret = ThaPwAdapterClaEnable(pw, claEnabled);
    ThaModulePdaHotConfigureStop(self, pw);

    return ret;
    }

eAtRet ThaModulePdaPwLopsClearThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 currentThreshold;
    eBool claEnabled;

    /* Check parameter */
    if (!ThaModulePdaPwLopsThresholdIsValid(self, pw, numPackets))
        mChannelError(pw, cAtErrorOutOfRangParm);

    /* Do nothing if there is no change */
    currentThreshold = mMethodsGet(self)->PwLopsClearThresholdGet(self, pw);
    if (numPackets == currentThreshold)
        return cAtOk;

    if (!mMethodsGet(self)->NeedProtectWhenSetLopsThreshold(self))
        {
        mMethodsGet(self)->PwLopsClearThresholdSet(self, pw, numPackets);
        return cAtOk;
        }

    claEnabled = ThaPwAdapterClaIsEnabled(pw);
    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwLopsClearThresholdSet(self, pw, numPackets);
    ThaPwAdapterClaEnable(pw, claEnabled);
    ThaModulePdaHotConfigureStop(self, pw);

    return cAtOk;
    }

eAtModulePwRet ThaModulePdaPwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 currentThreshold;
    eBool claEnabled;
    eAtRet ret = cAtOk;

    if (mMethodsGet(self)->NeedPreventReconfigure(self))
        {
   	 	/* Do nothing if there is no change */
    	currentThreshold = mMethodsGet(self)->PwLofsSetThresholdGet(self, pw);
    	if (numPackets == currentThreshold)
        	return cAtOk;
		}

    if (!mMethodsGet(self)->NeedProtectWhenSetLopsThreshold(self))
        {
        mMethodsGet(self)->PwLofsSetThresholdSet(self, pw, numPackets);
        return cAtOk;
        }

    claEnabled = ThaPwAdapterClaIsEnabled(pw);
    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwLofsSetThresholdSet(self, pw, numPackets);
    ret = ThaPwAdapterClaEnable(pw, claEnabled);
    ThaModulePdaHotConfigureStop(self, pw);

    return ret;
    }

uint32 ThaModulePdaPwLofsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwLofsSetThresholdGet(self, pw);

    return 0;
    }

eAtRet ThaModulePdaPwCwSequenceModeSet(ThaModulePda self, AtPw pw, eAtPwCwSequenceMode seqMd)
    {
    return mMethodsGet(self)->PwCwSequenceModeSet(self, pw, seqMd);
    }

eAtRet ThaModulePdaPwJitterBufferDelaySet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    eBool claEnabled = ThaPwAdapterClaIsEnabled(pw);
    eAtRet ret = cAtOk;

    /* Do nothing if there is no change */
    if (microseconds == mMethodsGet(self)->PwJitterBufferDelayGet(self, pw))
        return cAtOk;

    if (LimitResourceIsEnabled(pw) && !mMethodsGet(self)->PwJitterBufferDelayIsSupported(self, pw, microseconds))
        return cAtErrorModeNotSupport;

    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwJitterBufferDelaySet(self, pw, microseconds);
    ret |= ThaModulePdaPwReoderingEnable(self, pw, mMethodsGet(pw)->ReorderingIsEnabled(pw));
    ret |= ThaPwAdapterClaEnable(pw, claEnabled);
    ThaModulePdaHotConfigureStop(self, pw);

    return ret;
    }

eAtRet ThaModulePdaPwJitterBufferSizeSet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    eBool claEnabled = ThaPwAdapterClaIsEnabled(pw);

    if (LimitResourceIsEnabled(pw) && !mMethodsGet(self)->PwJitterBufferSizeIsSupported(self, pw, microseconds))
        return cAtErrorModeNotSupport;

    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwJitterBufferSizeSet(self, pw, microseconds);
    mChannelSuccessAssert(pw, ThaModulePdaPwReoderingEnable(self, pw, mMethodsGet(pw)->ReorderingIsEnabled(pw)));
    ThaPwAdapterClaEnable(pw, claEnabled);

    return cAtOk;
    }

eAtRet ThaModulePdaPwReoderingEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
	eAtRet ret = cAtOk;
    eBool claEnabled = ThaPwAdapterClaIsEnabled(pw);

    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwReorderingEnable(self, pw, enable);

    ret |= ThaClaPwControllerReorderEnable(mClaPwController(self), pw, enable);
    ret |= ThaPwAdapterClaEnable(pw, claEnabled);

    ThaModulePdaHotConfigureStop(self, pw);

    return ret;
    }

eAtRet ThaModulePdaJitterBufferEmptyWait(ThaModulePda self, AtPw pw)
    {
    uint32 timeoutMs, elapseMs, previousWaitTimeMs;
    tAtOsalCurTime startTime, curTime;
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (self == NULL)
        return cAtOk;

    if (AtDeviceWarmRestoreIsStarted(device))
        return cAtOk;

    if (AtDeviceIsSimulated(device))
        return cAtOk;

    timeoutMs = JitterBufferEmptyTimeout(self, pw);
    elapseMs = 0;
    previousWaitTimeMs = 0;
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseMs < timeoutMs)
        {
        /* Check if buffer is empty then exit */
        if (mMethodsGet(self)->PwJitterBufferIsEmpty(self, pw))
            {
            /* Profile for debugging */
            if (elapseMs > self->maxJitterEmptyWaitingTime)
                self->maxJitterEmptyWaitingTime = elapseMs;

            return cAtOk;
            }

        /* Release CPU at every 1ms continuous polling */
        if ((elapseMs - previousWaitTimeMs) >= 1)
            {
            AtOsalUSleep(1000);
            previousWaitTimeMs = elapseMs;
            }

        /* Wait */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtChannelLog((AtChannel)pw, cAtLogLevelWarning, AtSourceLocation, "Cannot wait for jitter buffer empty\r\n");

    return cAtErrorChannelBusy;
    }

uint32 ThaModulePdaMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinJitterBufferSize(self, pw, circuit, payloadSize);
    return 0;
    }

uint32 ThaModulePdaCEPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->CEPMinJitterBufferSize(self, pw, circuit, payloadSize);
    return 0;
    }

uint32 ThaModulePdaCESoPMinJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames)
    {
    if (self)
        return mMethodsGet(self)->CESoPMinJitterBufferSize(self, pw, circuit, payloadSizeInFrames);
    return 0;
    }

uint32 ThaModulePdaMaxJitterBufferSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MaxJitterBufferSize(self, pw, circuit, payloadSize);
    return 0;
    }

uint32 ThaModulePdaMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinJitterDelay(self, pw, circuit, payloadSize);
    return 0;
    }

uint32 ThaModulePdaCEPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->CEPMinJitterDelay(self, pw, circuit, payloadSize);
    return 0;
    }

uint32 ThaModulePdaCESoPMinJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSizeInFrames)
    {
    if (self)
        return mMethodsGet(self)->CESoPMinJitterDelay(self, pw, circuit, payloadSizeInFrames);
    return 0;
    }

uint32 ThaModulePdaMaxJitterDelay(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MaxJitterDelay(self, pw, circuit, payloadSize);
    return 0;
    }

uint32 ThaModulePdaMaxJitterDelayForJitterBuffer(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us)
    {
    if (self)
        return mMethodsGet(self)->MaxJitterDelayForJitterBuffer(self, pw, jitterBufferSize_us);
    return 0;
    }

uint16 ThaModulePdaPwMinPayloadSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize)
    {
    if (self)
        return mMethodsGet(self)->PwMinPayloadSize(self, pw, circuit, jitterBufferSize);
    return 0;
    }

uint32 ThaModulePdaPwMaxPayloadSize(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize)
    {
    if (self)
        return mMethodsGet(self)->PwMaxPayloadSize(self, pw, circuit, jitterBufferSize);
    return 0;
    }

uint32 ThaModulePdaMinNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinNumPacketsForJitterBufferSize(self, pw, payloadSize);
    return 0;
    }

uint32 ThaModulePdaMaxNumPacketsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, payloadSize);

    return 0;
    }

uint32 ThaModulePdaMinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->MinNumPacketsForJitterDelay(self, pw);

    return 0;
    }

uint32 ThaModulePdaMaxNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->MaxNumPacketsForJitterDelay(self, pw);

    return 0;
    }

uint32 ThaModulePdaCEPMinNumPacketsForJitterDelay(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->CEPMinNumPacketsForJitterDelay(self, pw);

    return 0;
    }

eAtRet ThaModulePdaPwJitterBufferSizeInNumOfPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    eBool claEnabled = ThaPwAdapterClaIsEnabled(pw);

    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwJitterBufferSizeInPacketSet(self, pw, numPackets);
    mChannelSuccessAssert(pw, ThaModulePdaPwReoderingEnable(self, pw, mMethodsGet(pw)->ReorderingIsEnabled(pw)));
    ThaPwAdapterClaEnable(pw, claEnabled);
    ThaModulePdaHotConfigureStop(self, pw);

    return cAtOk;
    }

eAtRet ThaModulePdaPwJitterBufferDelayInNumOfPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    eBool claEnabled = ThaPwAdapterClaIsEnabled(pw);
    eAtRet ret = cAtOk;

    ThaModulePdaHotConfigureStart(self, pw);
    mMethodsGet(self)->PwJitterBufferDelayInPacketSet(self, pw, numPackets);
    mChannelSuccessAssert(pw, ThaModulePdaPwReoderingEnable(self, pw, mMethodsGet(pw)->ReorderingIsEnabled(pw)));
    ret = ThaPwAdapterClaEnable(pw, claEnabled);
    ThaModulePdaHotConfigureStop(self, pw);

    return ret;
    }

uint16 ThaModulePdaCalculateNumPacketsByUs(ThaModulePda self, AtPw pw, uint32 us)
    {
    if(self)
        return mMethodsGet(self)->NumberOfPacketByUs(self, pw, us);
    return 0;
    }

uint32 ThaModulePdaCalculateUsByNumberOfPacket(ThaModulePda self, AtPw pw, uint32 numPkts)
    {
    if (self)
        return mMethodsGet(self)->UsByNumberOfPacket(self, pw ,numPkts);

    return 0;
    }

eAtRet ThaModulePdaHotConfigureStart(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->HotConfigureStart(self, pw);
    }

eAtRet ThaModulePdaHotConfigureStop(ThaModulePda self, AtPw pw)
    {
    return mMethodsGet(self)->HotConfigureStop(self, pw);
    }

eAtRet ThaModulePdaPwCenterJitterEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
	AtUnused(pw);
    self->jitterCenterDisabled = enable ? cAtFalse : cAtTrue;
    return cAtOk;
    }

eAtRet ThaModulePdaPwCenterJitterStart(ThaModulePda self, AtPw pw)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->jitterCenterDisabled)
        return cAtOk;

    return mMethodsGet(self)->PwCenterJitterStart(self, pw);
    }

eAtRet ThaModulePdaPwCenterJitterStop(ThaModulePda self, AtPw pw)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->jitterCenterDisabled)
        return cAtOk;

    return mMethodsGet(self)->PwCenterJitterStop(self, pw);
    }

uint32 ThaModulePdaPartOffset(ThaModulePda self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModulePda, partId);
    }

eAtRet ThaModulePdaPwCircuitIdSet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCircuitIdSet(self, pw);
    return cAtError;
    }

eAtRet ThaModulePdaPwCircuitIdReset(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCircuitIdReset(self, pw);
    return cAtError;
    }

eAtRet ThaModulePdaPwPdaTdmLookupEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwPdaTdmLookupEnable(self, pw, enable);
    return cAtError;
    }

eAtRet ThaModulePdaVcDeAssemblerSet(ThaModulePda self, AtSdhChannel vc)
    {
    if (self)
        return mMethodsGet(self)->VcDeAssemblerSet(self, vc);
    return cAtError;
    }

uint16 ThaModulePdaSAToPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->SAToPMaxPayloadSize(self, pw);
    return 0;
    }

uint16 ThaModulePdaCESoPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->CESoPMaxPayloadSize(self, pw);
    return 0;
    }

uint16 ThaModulePdaCESoPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->CESoPMinPayloadSize(self, pw);
    return 0;
    }

uint16 ThaModulePdaCESoPMaxNumFrames(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->CESoPMaxNumFrames(self, pw);
    return 0;
    }

uint16 ThaModulePdaCESoPMinNumFrames(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->CESoPMinNumFrames(self, pw);
    return 0;
    }

uint16 ThaModulePdaCEPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->CEPMaxPayloadSize(self, pw);
    return 0;
    }

eAtRet ThaModulePdaPwCepEquip(ThaModulePda self, AtPw pw, AtChannel subChannel, eBool equip)
    {
    if (self)
        return mMethodsGet(self)->PwCepEquip(self, pw, subChannel, equip);

    return cAtError;
    }

eAtRet ThaModulePdaPwCepEbmEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCepEbmEnable(self, pw, enable);
    return cAtError;
    }

eAtModulePwRet ThaModulePdaPwIdleCodeSet(ThaModulePda self, ThaPwAdapter pwAdapter, uint8 idleCode)
    {
    if (self)
        return mMethodsGet(self)->PwIdleCodeSet(self, pwAdapter, idleCode);
    return cAtError;
    }

uint8  ThaModulePdaPwIdleCodeGet(ThaModulePda self, ThaPwAdapter pwAdapter)
    {
    if (self)
        return mMethodsGet(self)->PwIdleCodeGet(self, pwAdapter);
    return 0x0;
    }

eAtRet ThaModulePdaBindLineToPw(ThaModulePda self, AtSdhLine line, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->BindLineToPw(self, line, pw);
    return cAtError;
    }

eAtRet ThaModulePdaPwTohByteEnable(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohByteEnable(self, pw, tohByte, enable);
    return cAtError;
    }

eAtRet ThaModulePdaPwTohFirstByteEnable(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohFirstByteEnable(self, pw, tohByte, enable);
    return cAtError;
    }

tThaTohByte* ThaModulePdaPwTohFirstByteGet(ThaModulePda self, AtPw pw, tThaTohByte *tohByte)
    {
    if (self)
        return mMethodsGet(self)->PwTohFirstByteGet(self, pw, tohByte);
    return NULL;
    }

eAtRet ThaModulePdaPwTohEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwTohEnable(self, pw, enable);
    return cAtOk;
    }

uint8 ThaModulePdaPwTohBytesBitmapGet(ThaModulePda self, AtPw pw, uint32 *bitmaps, uint8 numBitmap)
    {
    if (self)
        return mMethodsGet(self)->PwTohBytesBitmapGet(self, pw, bitmaps, numBitmap);

    return 0;
    }

eAtRet ThaModulePdaPwCircuitSliceSet(ThaModulePda self, AtPw pw, uint8 slice)
    {
    if (self)
        return mMethodsGet(self)->PwCircuitSliceSet(self, pw, slice);
    return cAtError;
    }

void ThaModulePdaPwJitterPayloadLengthSet(ThaModulePda self, AtPw pw, uint32 payloadSizeInBytes)
    {
    if (self)
        mMethodsGet(self)->PwJitterPayloadLengthSet(self, pw, payloadSizeInBytes);
    }

eAtRet ThaModulePdaPwNumDs0TimeslotsSet(ThaModulePda self, AtPw pw, uint16 numTimeslots)
    {
    if (self)
        return mMethodsGet(self)->PwNumDs0TimeslotsSet(self, pw, numTimeslots);

    return cAtError;
    }

eAtRet ThaModulePdaPwPdaModeSet(ThaModulePda self, AtPw pw, uint8 mode)
    {
    if (self)
        return mMethodsGet(self)->PwPdaModeSet(self, pw, mode);

    return cAtError;
    }

uint8 ThaModulePdaPwPdaModeGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwPdaModeGet(self, pw);
    return cInvalidUint8;
    }

eAtRet ThaModulePdaPwCircuitTypeSet(ThaModulePda self, AtPw pw, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->PwCircuitTypeSet(self, pw, circuit);

    return cAtError;
    }

uint32 ThaModulePdaPwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxBytesGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePdaPwRxReorderedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxReorderedPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePdaPwRxLostPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxLostPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePdaPwRxOutOfSeqDropedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxOutOfSeqDropedPacketsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePdaPwRxJitBufOverrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxJitBufOverrunGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePdaPwRxJitBufUnderrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxJitBufUnderrunGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePdaPwRxLopsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxLopsGet(self, pw, clear);

    return 0;
    }

uint32 ThaModulePdaPwRxPacketsSentToTdmGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->PwRxPacketsSentToTdmGet(self, pw, clear);

    return 0;
    }

eBool ThaModulePdaPwRxPacketsSentToTdmCounterIsSupported(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->PwRxPacketsSentToTdmCounterIsSupported(self);

    return cAtFalse;
    }

eBool ThaModulePdaPwLopsThresholdIsValid(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    if (self)
        return mMethodsGet(self)->PwLopsThresholdIsValid(self, pw, numPackets);

    return cAtFalse;
    }

uint32 ThaModulePdaPwLopsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwLopsSetThresholdGet(self, pw);

    return 0;
    }

uint32 ThaModulePdaPwLopsClearThresholdGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwLopsClearThresholdGet(self, pw);

    return 0;
    }

uint32 ThaModulePdaNumCurrentPacketsInJitterBuffer(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->NumCurrentPacketsInJitterBuffer(self, pw);

    return 0;
    }

uint32 ThaModulePdaNumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    if (self)
        return mMethodsGet(self)->NumCurrentAdditionalBytesInJitterBuffer(self, pwAdapter);

    return 0;
    }

eAtRet ThaModulePdaPwCwPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    if (self)
        {
        if (!mMethodsGet(self)->PktReplaceModeIsSupported(self, pktReplaceMode))
            return cAtErrorModeNotSupport;

        return mMethodsGet(self)->PwCwPktReplaceModeSet(self, pw, pktReplaceMode);
        }

    return cAtError;
    }

eAtPwPktReplaceMode ThaModulePdaPwCwPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwPktReplaceModeGet(self, pw);

    return cAtPwPktReplaceModeInvalid;
    }

eBool ThaModulePdaPwReorderingIsEnable(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwReorderingIsEnable(self, pw);

    return cAtFalse;
    }

uint32 ThaModulePdaPwAlarmGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwAlarmGet(self, pw);

    return 0;
    }

uint32 ThaModulePdaPwDefectHistoryGet(ThaModulePda self, AtPw pw, eBool read2Clear)
    {
    if (self)
        return mMethodsGet(self)->PwDefectHistoryGet(self, pw, read2Clear);

    return 0;
    }

void ThaModulePdaPwDebug(ThaModulePda self, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->PwDebug(self, pw);
    }

eAtRet ThaModulePdaAutoInsertRdiEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->AutoInsertRdiEnable(self, pw, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModulePdaAutoInsertRdiIsEnabled(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->AutoInsertRdiIsEnabled(self, pw);
    return cAtFalse;
    }

eBool ThaModulePdaShouldAutoInsertRdi(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->ShouldAutoInsertRdi(self);
    return cAtFalse;
    }

uint32 ThaModulePdaPwJitterBufferSizeGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwJitterBufferSizeGet(self, pw);
    return 0;
    }

uint32 ThaModulePdaPwJitterBufferDelayGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwJitterBufferDelayGet(self, pw);
    return 0;
    }

uint16 ThaModulePdaPwJitterBufferSizeInPacketGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwJitterBufferSizeInPacketGet(self, pw);
    return 0;
    }

uint16 ThaModulePdaPwJitterBufferDelayInPacketGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwJitterBufferDelayInPacketGet(self, pw);
    return 0;
    }

uint16 ThaModulePdaSAToPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->SAToPMinPayloadSize(self, pw);
    return 0;
    }

uint16 ThaModulePdaCEPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->CEPMinPayloadSize(self, pw);
    return 0;
    }

void ThaModulePdaPwRegsShow(AtPw pw)
    {
    ThaModulePda self = (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModulePda);
    AtPrintc(cSevNormal, "* PDA registers of channel '%s':\r\n", AtObjectToString((AtObject)pw));
    if (self)
        mMethodsGet(self)->PwRegsShow(self, pw);

    AtPrintc(cSevInfo, "\r\n");
    }

void ThaModulePdaHdlcChannelRegsShow(AtHdlcChannel channel)
    {
    ThaModulePda self = (ThaModulePda)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModulePda);
    AtPrintc(cSevNormal, "* PDA registers of channel '%s':\r\n", AtObjectToString((AtObject)channel));
    if (self)
        mMethodsGet(self)->HdlcChannelRegsShow(self, channel);
    }

uint32 ThaModulePdaPwLopsThresholdMax(ThaModulePda self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->PwLopsThresholdMax(self, adapter);
    return 0x0;
    }

uint32 ThaModulePdaPwLopsThresholdMin(ThaModulePda self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->PwLopsThresholdMin(self, adapter);
    return 0x0;
    }

eAtRet ThaModulePdaPwEparEnable(ThaModulePda self, AtPw pwAdapter, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwEparEnable(self, pwAdapter, enable);
    return cAtErrorNullPointer;
    }

eBool ThaModulePdaPwEparIsEnabled(ThaModulePda self, AtPw pwAdapter)
    {
    if (self)
        return mMethodsGet(self)->PwEparIsEnabled(self, pwAdapter);
    return cAtFalse;
    }

uint32 ThaModulePdaPwTdmOffset(ThaModulePda self, AtPw pw)
    {
    if (self)	
	    return mMethodsGet(self)->PwTdmOffset(self, pw);
	return 0;
    }

uint32 ThaModulePdaPwNumBlockByBufferInUs(ThaModulePda self, AtPw adapter, uint32 bufferSizeInUs, uint32 payloadSizeInByte)
    {
    uint32 bufferSizeInPkt;
    if (self == NULL)
        return 0;

    bufferSizeInPkt = mMethodsGet(self)->CalculateNumPacketsByUsAndPayload(self, adapter, bufferSizeInUs, payloadSizeInByte);
    return bufferSizeInPkt * NumBlockPerPacket(self, payloadSizeInByte);
    }

uint32 ThaModulePdaPwNumBlockByBufferInPkt(ThaModulePda self, AtPw adapter, uint32 bufferSizeInPkt, uint32 payloadSizeInByte)
    {
    if (self == NULL)
        return 0;

    AtUnused(adapter);
    return bufferSizeInPkt * NumBlockPerPacket(self, payloadSizeInByte);
    }

uint32 ThaModulePdaPwNumPacketsByUsAndPayload(ThaModulePda self, AtPw pw, uint32 bufferSizeInUs, uint32 payloadSizeInByte)
    {
    if (self)
        return mMethodsGet(self)->CalculateNumPacketsByUsAndPayload(self, pw, bufferSizeInUs, payloadSizeInByte);
    return 0;
    }

eBool ThaModulePdaResourceIsAvailable(ThaModulePda self, AtPw adapter, uint32 newOccupiedBlock)
    {
    uint32 newTotalOccupiedBlocks;

    if (self == NULL)
        return cAtTrue;

    if (!NeedHandleBufferRamLimitation(self))
        return cAtTrue;

    newTotalOccupiedBlocks = TotalOccupiedBlocksCalculate(self, adapter, newOccupiedBlock);
    if (newTotalOccupiedBlocks > mMethodsGet(self)->MaxNumJitterBufferBlocks(self))
        return cAtFalse;

    return cAtTrue;
    }

eAtRet ThaModulePdaTotalInUsedRamBlockUpdate(ThaModulePda self, AtPw adapter, uint32 newOccupiedBlock)
    {
    if (self == NULL)
        return cAtOk;

    if (!NeedHandleBufferRamLimitation(self))
        return cAtOk;

    self->numOccupiedBlocks = TotalOccupiedBlocksCalculate(self, adapter, newOccupiedBlock);
    ThaPwAdapterNumRamBlockForBufferSet((ThaPwAdapter)adapter, newOccupiedBlock);
    return cAtOk;
    }

uint32 ThaModulePdaMaxNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MaxNumMicrosecondsForJitterBufferSize(self, pw, payloadSize);

    return 0;
    }

uint32 ThaModulePdaMinNumMicrosecondsForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinNumMicrosecondsForJitterBufferSize(self, pw, payloadSize);

    return 0;
    }

uint32 ThaModulePdaJitterBufferWatermarkMinPackets(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->JitterBufferWatermarkMinPackets(self, pw);

    return 0;
    }

uint32 ThaModulePdaJitterBufferWatermarkMaxPackets(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->JitterBufferWatermarkMaxPackets(self, pw);

    return 0;
    }

eBool ThaModulePdaJitterBufferWatermarkIsSupported(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->JitterBufferWatermarkIsSupported(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePdaJitterBufferWatermarkReset(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->JitterBufferWatermarkReset(self, pw);

    return cAtFalse;
    }

eBool ThaModulePdaCepPayloadSizeShouldExcludeHeader(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->CepPayloadSizeShouldExcludeHeader(self);

    return cAtFalse;
    }

eAtRet ThaModulePdaCepIndicate(ThaModulePda self, AtPw pw, eBool isCep)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->CepIndicate(self, pw, isCep);
    }
    
eBool ThaModulePdaNeedPreventReconfigure(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->NeedPreventReconfigure(self);

    return cAtFalse;
    }

void ThaModulePdaRegDisplay(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pw, address, cThaModulePda);
    }

void ThaModulePdaLongRegDisplay(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegLongValueDisplay((AtChannel)pw, address, cThaModulePda);
    }

eAtRet ThaModulePdaHdlcPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    if (self)
        return mMethodsGet(self)->HdlcPwPayloadTypeSet(self, adapter, payloadType);

    return cAtErrorNullPointer;
    }

uint32 ThaModulePdaBaseAddress(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return cInvalidUint32;
    }

uint32 ThaModulePdaPrbsStatusRegister(ThaModulePda self)
    {
    if (self)
        return PrbsStatusRegister(self);
    return cInvalidUint32;
    }

eAtRet ThaModulePdaPwTohModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    if (self)
        return mMethodsGet(self)->PwTohModeSet(self, adapter);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePdaSAToPModeSet(ThaModulePda self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (self)
        return mMethodsGet(self)->SAToPModeSet(self, adapter, circuit);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePdaCESoPModeSet(ThaModulePda self, ThaPwAdapter adapter)
    {
    if (self)
        return mMethodsGet(self)->CESoPModeSet(self, adapter);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePdaCESoPCasModeSet(ThaModulePda self, ThaPwAdapter adapter, eAtPwCESoPCasMode mode)
    {
    if (self)
        return mMethodsGet(self)->CESoPCasModeSet(self, adapter, mode);
    return cAtErrorNullPointer;
    }

eAtPwCESoPCasMode ThaModulePdaCESoPCasModeGet(ThaModulePda self, ThaPwAdapter adapter)
    {
    if (self)
        return mMethodsGet(self)->CESoPCasModeGet(self, adapter);
    return cAtErrorNullPointer;
    }

eAtRet ThaModulePdaCepModeSet(ThaModulePda self, ThaPwAdapter adapter, AtSdhChannel sdhVc)
    {
    if (self)
        return mMethodsGet(self)->CepModeSet(self, adapter, sdhVc);
    return cAtErrorNullPointer;
    }

eBool ThaModulePdaPwCwAutoRxMBitIsEnabled(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoRxMBitIsEnabled(self, pw);

    return cAtFalse;
    }

eAtRet ThaModulePdaPwCwAutoRxMBitEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoRxMBitEnable(self, pw, enable);

    return cAtError;
    }

eBool ThaModulePdaPwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->PwCwAutoRxMBitIsConfigurable(self);

    return cAtFalse;
    }

eAtRet ThaModulePdaMlpppPwPayloadTypeSet(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    if (self)
        return mMethodsGet(self)->MlpppPwPayloadTypeSet(self, adapter, payloadType);
    return cAtErrorNullPointer;
    }

uint32 ThaModulePdaNumUnUsedBlocks(ThaModulePda self)
    {
    if (self)
        return NumUnUsedBlock(self);
    return 0;
    }

uint32 ThaModulePdaMaxNumJitterBufferBlocks(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumJitterBufferBlocks(self);
    return 0;
    }

uint32 ThaModulePdaJitterBufferBlockSizeInByte(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->JitterBufferBlockSizeInByte(self);
    return 0;
    }

eAtRet ThaModulePdaPwLopsPktReplaceModeSet(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    if (self)
        {
        if (!mMethodsGet(self)->PktReplaceModeIsSupported(self, pktReplaceMode))
            return cAtErrorModeNotSupport;

        return mMethodsGet(self)->PwLopsPktReplaceModeSet(self, pw, pktReplaceMode);
        }

    return cAtErrorNullPointer;
    }

eAtPwPktReplaceMode ThaModulePdaPwLopsPktReplaceModeGet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwLopsPktReplaceModeGet(self, pw);
    return cAtPwPktReplaceModeInvalid;
    }

eBool ThaModulePdaPwCanControlLopsPktReplaceMode(ThaModulePda self)
    {
    if (self)
        return mMethodsGet(self)->CanControlLopsPktReplaceMode(self);
    return cAtFalse;
    }

eBool ThaModulePdaPwPktReplaceModeIsSupported(ThaModulePda self, eAtPwPktReplaceMode pktReplaceMode)
    {
    if (self)
        return mMethodsGet(self)->PktReplaceModeIsSupported(self, pktReplaceMode);
    return cAtFalse;
    }

uint8 ThaModulePdaLbitPktReplaceModeSw2Hw(eAtPwPktReplaceMode pktReplaceMode)
    {
    return LbitPktReplaceModeSw2Hw(pktReplaceMode);
    }

uint8 ThaModulePdaLbitPktReplaceModeHw2Sw(eAtPwPktReplaceMode pktReplaceMode)
    {
    return LbitPktReplaceModeHw2Sw(pktReplaceMode);
    }

eAtModulePwRet ThaModulePdaDefaultPwCwPktReplaceModeV1Set(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode)
    {
    return PwCwPktReplaceModeV1Set(self, pw, pktReplaceMode);
    }

eAtPwPktReplaceMode ThaModulePdaDefaultPwCwPktReplaceModeV1Get(ThaModulePda self, AtPw pw)
    {
    return PwCwPktReplaceModeV1Get(self, pw);
    }

eBool ThaModulePdaPwJitterBufferSizeIsValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs)
    {
    if (self)
        return mMethodsGet(self)->PwJitterBufferSizeIsValid(self, pw, jitterBufferUs);
    return cAtFalse;
    }

eBool ThaModulePdaPwJitterDelayIsValid(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    if (self)
        return mMethodsGet(self)->PwJitterDelayIsValid(self, pw, jitterDelayUs);
    return cAtFalse;
    }

eBool ThaModulePdaPwJitterBufferAndJitterDelayAreValid(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs)
    {
    if (self)
        return mMethodsGet(self)->PwJitterBufferAndJitterDelayAreValid(self, pw, jitterBufferUs, jitterDelayUs);
    return cAtFalse;
    }

eBool ThaModulePdaShouldSetDefaultJitterBufferSizeOnJitterDelaySet(ThaModulePda self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->ShouldSetDefaultJitterBufferSizeOnJitterDelaySet(self, pw);
    return cAtFalse;
    }

uint32 ThaModulePdaDefaultJitterBufferSize(ThaModulePda self, AtPw pw, uint32 jitterDelayUs)
    {
    if (self)
        return mMethodsGet(self)->DefaultJitterBufferSize(self, pw, jitterDelayUs);
    return 0;
    }

eBool ThaModulePdaCEPPayloadSizeIsValid(ThaModulePda self, AtPw pw, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->CEPPayloadSizeIsValid(self, pw, payloadSize);
    return cAtTrue;
    }

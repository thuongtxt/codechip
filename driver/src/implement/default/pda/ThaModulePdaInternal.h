/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA internal module
 * 
 * File        : ThaModulePdaInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEPDAINTERNAL_H_
#define _THAMODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "../../../generic/pw/AtPwInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "../util/ThaUtil.h"
#include "ThaModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cDefaultJitterTimeout 0x200

/*--------------------------- Macros -----------------------------------------*/
#define mRo(clear) ((clear) ? 0 : 1)
#define mPwOffset(self, pw) mMethodsGet((ThaModulePda)self)->PwDefaultOffset((ThaModulePda)self, (AtPw)pw)
#define mTdmOffset(self, pw) mMethodsGet((ThaModulePda)self)->PwTdmOffset((ThaModulePda)self, (AtPw)pw)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModulePdaMethods
    {
    eAtRet (*PartDefaultSet)(ThaModulePda self, uint8 partId);
    uint32 (*BaseAddress)(ThaModulePda self);

    /* Offset base on PW itself */
    uint32 (*PwDefaultOffset)(ThaModulePda self, AtPw pw);
    uint32 (*PwLopsThresholdMin)(ThaModulePda self, AtPw adapter);
    uint32 (*PwLopsThresholdMax)(ThaModulePda self, AtPw adapter);
    eBool (*PwLopsThresholdIsValid)(ThaModulePda self, AtPw pw, uint32 numPackets);

    /* Offset base on circuit of PW */
    uint32 (*PwTdmOffset)(ThaModulePda self, AtPw pw);

    /* Loss of frame state defined by MEF-8 */
    eAtRet (*PwLofsSetThresholdSet)(ThaModulePda self, AtPw pw, uint32 numPackets);
    uint32 (*PwLofsSetThresholdGet)(ThaModulePda self, AtPw pw);
    eAtRet (*PwLofsClearThresholdSet)(ThaModulePda self, AtPw pw, uint32 numPackets);
    uint32 (*PwLofsClearThresholdGet)(ThaModulePda self, AtPw pw);

    /* Loss of packet synchronization defined by RFC4842 */
    eAtRet (*PwLopsSetThresholdSet)(ThaModulePda self, AtPw pw, uint32 numPackets);
    uint32 (*PwLopsSetThresholdGet)(ThaModulePda self, AtPw pw);
    eAtRet (*PwLopsClearThresholdSet)(ThaModulePda self, AtPw pw, uint32 numPackets);
    uint32 (*PwLopsClearThresholdGet)(ThaModulePda self, AtPw pw);
    eBool  (*NeedProtectWhenSetLopsThreshold)(ThaModulePda self);
    eBool (*LopsThresholdHasLimitOneSecond)(ThaModulePda self);
    eBool  (*NeedPreventReconfigure)(ThaModulePda self);

    eAtRet (*PwReorderingEnable)(ThaModulePda self, AtPw pw, eBool enable);
    eBool  (*PwReorderingIsEnable)(ThaModulePda self, AtPw pw);

    /* Jitter buffer */
    eBool  (*PwJitterBufferSizeIsSupported)(ThaModulePda self, AtPw pw, uint32 microseconds);
    eBool  (*PwJitterBufferDelayIsSupported)(ThaModulePda self, AtPw pw, uint32 microseconds);
    eBool  (*PwJitterBufferSizeIsValid)(ThaModulePda self, AtPw pw, uint32 jitterBufferUs);
    eBool  (*PwJitterDelayIsValid)(ThaModulePda self, AtPw pw, uint32 jitterDelayUs);
    eBool  (*PwJitterBufferAndJitterDelayAreValid)(ThaModulePda self, AtPw pw, uint32 jitterBufferUs, uint32 jitterDelayUs);
    eBool  (*ShouldSetDefaultJitterBufferSizeOnJitterDelaySet)(ThaModulePda self, AtPw pw);
    uint32 (*DefaultJitterBufferSize)(ThaModulePda self, AtPw pw, uint32 jitterDelayUs);
    uint32 (*MinNumPacketsForJitterBufferSize)(ThaModulePda self, AtPw pw, uint32 payloadSize);
    uint32 (*MaxNumPacketsForJitterBufferSize)(ThaModulePda self, AtPw pw, uint32 payloadSize);
    uint32 (*HwLimitNumPktForJitterBufferSize)(ThaModulePda self, AtPw pw, uint32 payloadSize);
    eAtRet (*PwJitterBufferSizeSet)(ThaModulePda self, AtPw pw, uint32 microseconds);
    uint32 (*PwJitterBufferSizeGet)(ThaModulePda self, AtPw pw);
    uint32 (*MinJitterBufferSize)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*CEPMinJitterBufferSize)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*CESoPMinJitterBufferSize)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*MaxJitterBufferSize)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*MinNumPacketsForJitterDelay)(ThaModulePda self, AtPw pw);
    uint32 (*MaxNumPacketsForJitterDelay)(ThaModulePda self, AtPw pw);
    uint32 (*CEPMinNumPacketsForJitterDelay)(ThaModulePda self, AtPw pw);
    eAtRet (*PwJitterBufferDelaySet)(ThaModulePda self, AtPw pw, uint32 microseconds);
    uint32 (*PwJitterBufferDelayGet)(ThaModulePda self, AtPw pw);
    uint32 (*MinJitterDelay)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*CEPMinJitterDelay)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*CESoPMinJitterDelay)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*MaxJitterDelay)(ThaModulePda self, AtPw pw, AtChannel circuit, uint16 payloadSize);
    uint32 (*MaxJitterDelayForJitterBuffer)(ThaModulePda self, AtPw pw, uint32 jitterBufferSize_us);
    uint32 (*NumCurrentPacketsInJitterBuffer)(ThaModulePda self, AtPw pw);
    uint32 (*NumCurrentAdditionalBytesInJitterBuffer)(ThaModulePda self, AtPw pwAdapter);
    eBool  (*PwJitterBufferIsEmpty)(ThaModulePda self, AtPw pw);
    void   (*PwJitterPayloadLengthSet)(ThaModulePda self, AtPw pw, uint32 payloadSizeInBytes);
    eAtRet (*PwJitterBufferSizeInPacketSet)(ThaModulePda self, AtPw pw, uint32 numPackets);
    eAtRet (*PwJitterBufferDelayInPacketSet)(ThaModulePda self, AtPw pw, uint32 numPackets);
    uint16 (*PwJitterBufferSizeInPacketGet)(ThaModulePda self, AtPw pw);
    uint16 (*PwJitterBufferDelayInPacketGet)(ThaModulePda self, AtPw pw);
    uint32 (*JitterBufferEmptyAdditionalTimeout)(ThaModulePda self);
    uint32 (*MinNumMicrosecondsForJitterBufferSize)(ThaModulePda self, AtPw pw, uint32 payloadSize);
    uint32 (*MaxNumMicrosecondsForJitterBufferSize)(ThaModulePda self, AtPw pw, uint32 payloadSize);
    eBool  (*CepPayloadSizeShouldExcludeHeader)(ThaModulePda self);
    eAtRet (*CepIndicate)(ThaModulePda self, AtPw pw, eBool enable);

    /* Jitter buffer water mark */
    uint32 (*JitterBufferWatermarkMinPackets)(ThaModulePda self, AtPw pw);
    uint32 (*JitterBufferWatermarkMaxPackets)(ThaModulePda self, AtPw pw);
    eBool (*JitterBufferWatermarkIsSupported)(ThaModulePda self, AtPw pw);
    eAtRet (*JitterBufferWatermarkReset)(ThaModulePda self, AtPw pw);

    /* Jitter center */
    eBool  (*JitterBufferCenterIsSupported)(ThaModulePda self);
    eAtRet (*PwCenterJitterStart)(ThaModulePda self, AtPw pw);
    eAtRet (*PwCenterJitterStop)(ThaModulePda self, AtPw pw);
    uint8  (*JitterBufferState)(ThaModulePda self, AtPw pw);
    const char* (*JitterBufferStateString)(ThaModulePda self, uint32 hwState);

    /* Hot configure jitter related parameters */
    eAtRet (*HotConfigureStart)(ThaModulePda self, AtPw pw);
    eAtRet (*HotConfigureStop)(ThaModulePda self, AtPw pw);

    /* Convert between microsecond and number of packet */
    uint16 (*NumberOfPacketByUs)(ThaModulePda self, AtPw pw, uint32 numUs);
    uint32 (*UsByNumberOfPacket)(ThaModulePda self, AtPw pw, uint32 numPkts);

    /* Payload size */
    eAtRet (*PwNumDs0TimeslotsSet)(ThaModulePda self, AtPw pw, uint16 numTimeslots);
    uint16 (*PwMinPayloadSize)(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize);
    uint32 (*PwMaxPayloadSize)(ThaModulePda self, AtPw pw, AtChannel circuit, uint32 jitterBufferSize);
    uint16 (*SAToPMaxPayloadSize)(ThaModulePda self, AtPw pw);
    uint16 (*SAToPMinPayloadSize)(ThaModulePda self, AtPw pw);
    uint16 (*CESoPMaxPayloadSize)(ThaModulePda self, AtPw pw);
    uint16 (*CESoPMinPayloadSize)(ThaModulePda self, AtPw pw);
    uint16 (*CEPMaxPayloadSize)(ThaModulePda self, AtPw pw);
    uint16 (*CEPMinPayloadSize)(ThaModulePda self, AtPw pw);
    uint16 (*CESoPMaxNumFrames)(ThaModulePda self, AtPw pw);
    uint16 (*CESoPMinNumFrames)(ThaModulePda self, AtPw pw);

    /* Some special payload size are eliminated depended on product, although
     * they are within range from minimum to maximum payload size */
    eBool (*CEPPayloadSizeIsValid)(ThaModulePda self, AtPw pw, uint16 payloadSize);

    /* For jitter buffer limitation */
    uint32 (*MaxNumJitterBufferBlocks)(ThaModulePda self);
    uint32 (*JitterBufferBlockSizeInByte)(ThaModulePda self);
    uint32 (*CalculateNumPacketsByUsAndPayload)(ThaModulePda self, AtPw pw, uint32 numUs, uint32 payloadSizeInBytes);
    uint32 (*NumFreeBlocksHwGet)(ThaModulePda self);

    /* For CEP */
    eAtRet (*PwPdaModeSet)(ThaModulePda self, AtPw pw, uint8 mode);
    uint8 (*PwPdaModeGet)(ThaModulePda self, AtPw pw);
    eAtRet (*PwCircuitTypeSet)(ThaModulePda self, AtPw pw, AtChannel circuit);
    eAtRet (*PwCircuitIdSet)(ThaModulePda self, AtPw pw);
    eAtRet (*PwCircuitIdReset)(ThaModulePda self, AtPw pw);
    eAtRet (*PwPdaTdmLookupEnable)(ThaModulePda self, AtPw pw, eBool enable);
    eAtRet (*VcDeAssemblerSet)(ThaModulePda self, AtSdhChannel vc);
    eAtRet (*PwCepEquip)(ThaModulePda self, AtPw pw, AtChannel subChannel, eBool equip);
    eAtRet (*PwCepEbmEnable)(ThaModulePda self, AtPw pw, eBool enable);
    eAtRet (*PwEparEnable)(ThaModulePda self, AtPw pwAdapter, eBool enable);
    eBool  (*PwEparIsEnabled)(ThaModulePda self, AtPw pwAdapter);

    eAtRet (*PwCwSequenceModeSet)(ThaModulePda self, AtPw pw, eAtPwCwSequenceMode sequenceMode);
    eAtModulePwRet (*PwCwPktReplaceModeSet)(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode);
    eAtPwPktReplaceMode (*PwCwPktReplaceModeGet)(ThaModulePda self, AtPw pw);
    eAtModulePwRet (*PwLopsPktReplaceModeSet)(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode);
    eAtPwPktReplaceMode (*PwLopsPktReplaceModeGet)(ThaModulePda self, AtPw pw);
    eBool (*CanControlLopsPktReplaceMode)(ThaModulePda self);
    eBool (*PktReplaceModeIsSupported)(ThaModulePda self, eAtPwPktReplaceMode pktReplaceMode);
    eAtModulePwRet (*LbitPktReplaceModeSet)(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode);
    eAtPwPktReplaceMode (*LbitPktReplaceModeGet)(ThaModulePda self, AtPw pw);
    eAtRet (*LopPktReplaceModeSet)(ThaModulePda self, AtPw pw, eAtPwPktReplaceMode pktReplaceMode);
    eAtPwPktReplaceMode (*LopPktReplaceModeGet)(ThaModulePda self, AtPw pw);

    /* CesoPsn */
    eBool (*PwCwAutoRxMBitIsConfigurable)(ThaModulePda self);
    eAtRet (*PwCwAutoRxMBitEnable)(ThaModulePda self, AtPw pw, eBool enable);
    eBool (*PwCwAutoRxMBitIsEnabled)(ThaModulePda self, AtPw pw);

    /* TOH pseudowire */
    eAtRet (*BindLineToPw)(ThaModulePda self, AtSdhLine line, AtPw pseudowire);
    eAtRet (*PwTohByteEnable)(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
    eAtRet (*PwTohFirstByteEnable)(ThaModulePda self, AtPw pw, const tThaTohByte *tohByte, eBool enable);
    eAtRet (*PwTohEnable)(ThaModulePda self, AtPw pw, eBool enable);
    tThaTohByte* (*PwTohFirstByteGet)(ThaModulePda self, AtPw pw, tThaTohByte *tohByte);
    uint8 (*PwTohBytesBitmapGet)(ThaModulePda self, AtPw pw, uint32 *bitmaps, uint8 numBitmap);

    /* IDLE code */
    eAtModulePwRet (*ModuleIdleCodeSet)(ThaModulePda self, uint8 idleCode);
    uint8  (*ModuleIdleCodeGet)(ThaModulePda self);
    eBool (*ModuleIdleCodeIsSupported)(ThaModulePda self);
    eBool  (*PwIdleCodeIsSupported)(ThaModulePda self);
    eAtModulePwRet (*PwIdleCodeSet)(ThaModulePda self, ThaPwAdapter pwAdapter, uint8 idleCode);
    uint8  (*PwIdleCodeGet)(ThaModulePda self, ThaPwAdapter pwAdapter);
    eAtRet (*PwCircuitSliceSet)(ThaModulePda self, AtPw pw, uint8 slice);

    /* Counter get */
    uint32 (*PwRxBytesGet)(ThaModulePda self, AtPw pw, eBool clear);
    uint32 (*PwRxReorderedPacketsGet)(ThaModulePda self, AtPw pw, eBool clear);
    uint32 (*PwRxLostPacketsGet)(ThaModulePda self, AtPw pw, eBool clear);
    uint32 (*PwRxOutOfSeqDropedPacketsGet)(ThaModulePda self, AtPw pw, eBool clear);
    uint32 (*PwRxJitBufOverrunGet)(ThaModulePda self, AtPw pw, eBool clear);
    uint32 (*PwRxJitBufUnderrunGet)(ThaModulePda self, AtPw pw, eBool clear);
    uint32 (*PwRxLopsGet)(ThaModulePda self, AtPw pw, eBool clear);
    uint32 (*PwRxPacketsSentToTdmGet)(ThaModulePda self, AtPw pw, eBool clear);
    eBool  (*PwRxPacketsSentToTdmCounterIsSupported)(ThaModulePda self);

    /* Alarms */
    uint32 (*PwAlarmGet)(ThaModulePda self, AtPw pw);
    uint32 (*PwDefectHistoryGet)(ThaModulePda self, AtPw pw, eBool read2Clear);

    /* Debug */
    void (*PwDebug)(ThaModulePda self, AtPw pw);
    void (*PwRegsShow)(ThaModulePda self, AtPw pw);
    void (*HdlcChannelRegsShow)(ThaModulePda self, AtHdlcChannel channel);
    void (*EthFlowRegsShow)(ThaModulePda self, AtEthFlow flow);

    /* Auto play RDI on circuit */
    eAtRet (*AutoInsertRdiEnable)(ThaModulePda self, AtPw pw, eBool enable);
    eBool (*AutoInsertRdiIsEnabled)(ThaModulePda self, AtPw pw);
    eBool (*ShouldAutoInsertRdi)(ThaModulePda self);

    /* Register */
    uint32 (*PwPdaIdleCodeCtrl)(ThaModulePda self);
    uint32 (*PWPdaTdmModeCtrl)(ThaModulePda self, AtPw pw);
    uint32 (*EparLineIdLkCtr)(ThaModulePda self);
    uint32 (*PrbsStatusBase)(ThaModulePda self);

    uint32 (*PktReplaceModeMask)(ThaModulePda self, AtPw pw);
    uint8  (*PktReplaceModeShift)(ThaModulePda self, AtPw pw);
    uint32 (*EparLineIdMask)(ThaModulePda self);
    uint8 (*EparLineIdShift)(ThaModulePda self);
    uint32 (*EparSliceIdMask)(ThaModulePda self);
    uint8 (*EparSliceIdShift)(ThaModulePda self);

    /* PDA counter */
    uint32 (*CounterOffset)(ThaModulePda self, AtPw pw);

    /* Types */
    eAtRet (*HdlcPwPayloadTypeSet)(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
    eAtRet (*MlpppPwPayloadTypeSet)(ThaModulePda self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
    eAtRet (*PwTohModeSet)(ThaModulePda self, ThaPwAdapter adapter);
    eAtRet (*SAToPModeSet)(ThaModulePda self, ThaPwAdapter adapter, AtChannel circuit);
    eAtRet (*CESoPModeSet)(ThaModulePda self, ThaPwAdapter adapter);
    eAtRet (*CESoPCasModeSet)(ThaModulePda self, ThaPwAdapter adapter, eAtPwCESoPCasMode mode);
    eAtPwCESoPCasMode (*CESoPCasModeGet)(ThaModulePda self, ThaPwAdapter adapter);
    eAtRet (*CepModeSet)(ThaModulePda self, ThaPwAdapter adapter, AtSdhChannel sdhVc);
    }tThaModulePdaMethods;

typedef struct tThaModulePda
    {
    tAtModule super;
    const tThaModulePdaMethods *methods;

    /* Private data */
    eBool jitterCenterDisabled;
    uint32 numOccupiedBlocks;

    /* For debugging */
    uint32 maxJitterEmptyWaitingTime;
    }tThaModulePda;

typedef struct tThaModulePdaV2Methods
    {
    eBool  (*NeedToCfgCircuitType)(ThaModulePdaV2 self);
    uint32 (*SdhChannelOffset)(ThaModulePdaV2 self, AtSdhChannel channel);
    uint32 (*SdhChannelStsOffset)(ThaModulePdaV2 self, AtSdhChannel channel, uint8 stsId);
    uint32 (*SdhChannelSliceOffset)(ThaModulePdaV2 self, uint8 slice);
    eBool (*CepFractionalIsSupported)(ThaModulePdaV2 self);

    /* Registers */
    uint32 (*PwAlarmIntrStat)(ThaModulePdaV2 self);
    uint32 (*PwAlarmCurrentStat)(ThaModulePdaV2 self);

    uint32 (*PDATdmJitBufStat)(ThaModulePdaV2 self);
    mDefineMaskShiftField(ThaModulePdaV2, BufferNumPkt)
    mDefineMaskShiftField(ThaModulePdaV2, BufferNumAdditionalBytes)

    uint32 (*PWPdaJitBufCtrl)(ThaModulePdaV2 self);
    mDefineMaskShiftField(ThaModulePdaV2, JitterBufferSizePkt)
    mDefineMaskShiftField(ThaModulePdaV2, JitterBufferDelayPkt)

    uint32 (*PdaIdleCodeMask)(ThaModulePdaV2 self, AtPw pw);
    uint8  (*PdaIdleCodeShift)(ThaModulePdaV2 self, AtPw pw);
    uint32 (*AisRdiUneqOffMask)(ThaModulePdaV2 self, AtPw pw);
    uint8  (*AisRdiUneqOffShift)(ThaModulePdaV2 self, AtPw pw);

    mDefineMaskShiftField(ThaModulePdaV2, PdaFirstStsId)
    mDefineMaskShiftField(ThaModulePdaV2, PdaNumNxDs0)

    /* Hot configure control */
    uint32 (*HotConfigureEngineStatus)(ThaModulePdaV2 self);
    uint32 (*HotConfigureEngineControl)(ThaModulePdaV2 self);
    uint32 (*HotConfigureEnginePwId)(ThaModulePdaV2 self, AtPw pw);
    mDefineMaskShiftField(ThaModulePdaV2, HotConfigureEngineRequestInfoPwId)

    /* Debug */
    void (*JitterBufferRegShow)(ThaModulePdaV2 self, AtPw pw);
    }tThaModulePdaV2Methods;

typedef struct tThaModulePdaV2
    {
    tThaModulePda super;
    const tThaModulePdaV2Methods *methods;

    /* Private */
    uint32 centerEngineReadyWaitTimeInMs;
    }tThaModulePdaV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModulePdaObjectInit(AtModule module, AtDevice device);
AtModule ThaModulePdaV2ObjectInit(AtModule self, AtDevice device);

void ThaModulePdaRegDisplay(AtPw pw, const char* regName, uint32 address);
void ThaModulePdaLongRegDisplay(AtPw pw, const char* regName, uint32 address);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEPDAINTERNAL_H_ */


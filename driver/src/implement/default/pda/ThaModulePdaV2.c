/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : ThaModulePdaV2.c
 *
 * Created Date: May 15, 2013
 *
 * Description : PDA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "ThaModulePdaInternal.h"
#include "../pw/ThaModulePwV2Reg.h"
#include "../util/ThaUtil.h"
#include "../pw/adapters/ThaPwAdapter.h"
#include "../pw/ThaPwUtil.h"
#include "../sdh/ThaModuleSdh.h"
#include "../sdh/ThaSdhVcInternal.h"
#include "../pw/ThaPwToh.h"
#include "AtPwCep.h"
#include "../sdh/ThaSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/* For jitter hot configure */
#define cThaRegHotConfigureEngineStatus 0x241001

#define cThaRegHotConfigureEngineRequestInfo        0x241000
#define cThaHotConfigureEngineRequestInfoPwIdMask   cBit9_1
#define cThaHotConfigureEngineRequestInfoPwIdShift  1
#define cThaHotConfigureEngineRequestInfoInitMask   cBit0
#define cThaHotConfigureEngineRequestInfoInitShift  0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModulePdaV2)self)
#define cTu1xBitMask(tug2, tu) (cBit0 << ((tug2) * 4 + (tu)))
#define cTu1xBitShift(tug2, tu) ((tug2) * 4 + (tu))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModulePdaV2Methods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtModuleMethods     m_AtModuleOverride;
static tThaModulePdaMethods m_ThaModulePdaOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtModuleMethods     *m_AtModuleMethods     = NULL;
static const tThaModulePdaMethods *m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineRegAdress(ThaModulePda, PwPdaIdleCodeCtrl)
mDefineRegAdress(ThaModulePdaV2, PWPdaJitBufCtrl)
mDefineRegAdress(ThaModulePdaV2, PDATdmJitBufStat)

static uint32 PWPdaTdmModeCtrl(ThaModulePda self, AtPw pw)
    {
	AtUnused(self);
	AtUnused(pw);
    return cThaRegPWPdaMdCtrl;
    }

static eBool PwJitterBufferIsEmpty(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->PDATdmJitBufStat(mThis(self)) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);
    return (mRegField(regVal, cThaJitBufState) == 0) ? cAtTrue : cAtFalse;
    }

static eAtRet PwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[1], cThaRegPWPdaJitBufCtrlSetLofsHead, ((numPackets >> 3) & 0x3));
    mRegFieldSet(longRegVal[0], cThaRegPWPdaJitBufCtrlSetLofsTail, numPackets & 0x7);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 PwLofsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 setThres1;
    uint8 setThres2;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    setThres1 = (uint8)mRegField(longRegVal[1], cThaRegPWPdaJitBufCtrlSetLofsHead);
    setThres2 = (uint8)mRegField(longRegVal[0], cThaRegPWPdaJitBufCtrlSetLofsTail);

    return (uint8)((((setThres1 & 0x3) << 3)) | ((setThres2 & 0x7)));
    }

static eAtRet PwLofsClearThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[0], cThaRegPWPdaJitBufCtrlClearLofs, numPackets);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 PwLofsClearThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return (uint8)mRegField(longRegVal[0], cThaRegPWPdaJitBufCtrlClearLofs);
    }

static uint16 PwJitterBufferDelayInPacketGet(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];

    regAddr = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    return (uint16)mPolyRegField(longRegVal[cThaRegPWPdaJitBufCtrlPdvSizePkDwIndex], mThis(self), JitterBufferDelayPkt);
    }

static uint32 ReoderTimeoutCalculate(ThaModulePda self, AtPw pw)
    {
    uint16 jitterDelayInPackets;
    uint32 payloadInBytes;
    uint32 speedIn64Kbps;
    AtChannel circuit = AtPwBoundCircuitGet(pw);

    jitterDelayInPackets = ThaModulePdaPwJitterBufferDelayInPacketGet(self, pw);
    if (jitterDelayInPackets >= 32)
        jitterDelayInPackets = 32;

    speedIn64Kbps = mSpeedIn64Kbps(ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit));
    if (speedIn64Kbps == 0)
        return cDefaultJitterTimeout;

    payloadInBytes = ThaPwUtilPayloadSizeInBytes(pw, circuit, AtPwPayloadSizeGet(pw));
    return (jitterDelayInPackets * payloadInBytes) / speedIn64Kbps;
    }

static eAtRet PwReorderingEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 reorderTimeout;

    if (!enable)
        {
        regAddr = cThaRegPWPdaReorderCtrl + mPwOffset(self, pw);
        regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
        mRegFieldSet(regVal, cThaRegPWPdaReorderCtrlReorEn, 0);
        mRegFieldSet(regVal, cThaRegPWPdaReorderCtrlReorTimeout, cDefaultJitterTimeout);
        mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

        return cAtOk;
        }

    regAddr = cThaRegPWPdaReorderCtrl + mPwOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);

    /* HW recommended, reorder time out must >= 2 */
    reorderTimeout = ReoderTimeoutCalculate(self, pw);
    if (reorderTimeout < 2)
        reorderTimeout = 2;

    mRegFieldSet(regVal, cThaRegPWPdaReorderCtrlReorTimeout, reorderTimeout);
    mRegFieldSet(regVal, cThaRegPWPdaReorderCtrlReorEn, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eBool PwReorderingIsEnable(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = cThaRegPWPdaReorderCtrl + mPwOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    return (regVal & cThaRegPWPdaReorderCtrlReorEnMask) ? cAtTrue : cAtFalse;
    }

static uint32 PwPayloadSizeInBytes(AtPw pwAdapter)
    {
    if (AtPwTypeGet(pwAdapter) == cAtPwTypeCESoP)
        return (uint32)(AtPdhNxDs0NumTimeslotsGet((AtPdhNxDS0)AtPwBoundCircuitGet(pwAdapter)) * AtPwPayloadSizeGet(pwAdapter));

    if (AtPwTypeGet(pwAdapter) == cAtPwTypeCEP)
        {
        AtPwCep pwCep = (AtPwCep)ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
        if (AtPwCepModeGet(pwCep) == cAtPwCepModeFractional)
            return ThaPwCepFractionalRealPayloadSizeCalculate(pwAdapter, AtPwPayloadSizeGet(pwAdapter));
        }

    if (AtPwTypeGet(pwAdapter) == cAtPwTypeToh)
        {
        AtPwToh pwToh = (AtPwToh)ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
        return (uint32)(AtPwPayloadSizeGet(pwAdapter) * AtPwTohNumEnabledBytesGet(pwToh));
        }

    return AtPwPayloadSizeGet(pwAdapter);
    }

static uint32 CorrectNumAdditionalBytes(ThaModulePda self, AtPw pw, uint32 additionalBytes)
    {
	AtUnused(self);
    /* Just need to correct in case of CEP */
    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return additionalBytes;

    /* Formula to correct as HW recommend: round to multiple of 32 */
    if (additionalBytes % 32 == 0)
        return additionalBytes;

    return (((additionalBytes / 32) + 1) * 32);
    }

static uint32 NumCurrentPacketsInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr, regVal;
    uint32 numPackets, additionalBytes;
    uint32 payloadSizeInBytes = PwPayloadSizeInBytes(pwAdapter);

    if (payloadSizeInBytes == 0)
        return 0;

    regAddr = mMethodsGet(mThis(self))->PDATdmJitBufStat(mThis(self)) + mPwOffset(self, pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    numPackets      = mPolyRegField(regVal, mThis(self), BufferNumPkt);
    additionalBytes = CorrectNumAdditionalBytes(self, pwAdapter, mPolyRegField(regVal, mThis(self), BufferNumAdditionalBytes));

    return numPackets + (additionalBytes / payloadSizeInBytes);
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(ThaModulePda self, AtPw pwAdapter)
    {
    uint32 regAddr, regVal;
    uint32 additionalBytes;
    uint32 payloadSizeInBytes = PwPayloadSizeInBytes(pwAdapter);

    if (payloadSizeInBytes == 0)
        return 0;

    regAddr = mMethodsGet(mThis(self))->PDATdmJitBufStat(mThis(self)) + mPwOffset(self, pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    additionalBytes = CorrectNumAdditionalBytes(self, pwAdapter, mPolyRegField(regVal, mThis(self), BufferNumAdditionalBytes));
    return additionalBytes % payloadSizeInBytes;
    }

static uint32 PwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxPldOctCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxReorderedPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxLostPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxReorderLostPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxReorderDropPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxJitBufOverrunPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwJitBufUnderrunEvtCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxLopsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxLofsTransCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxPacketsSentToTdmGet(ThaModulePda self, AtPw pw, eBool clear)
    {
	AtUnused(clear);
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static void PwJitterPayloadLengthSet(ThaModulePda self, AtPw pw, uint32 payloadSizeInBytes)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];

    regAddr = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[cThaRegPWPdaJitBufCtrlPldLenDwIndex], cThaRegPWPdaJitBufCtrlPldLen, payloadSizeInBytes);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    }

static uint32 HwLimitNumPktForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(pw);
	AtUnused(self);
    return 4095;
    }

static eBool PwJitterBufferSizeIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 numPackets;
    if ((microseconds < mMethodsGet(self)->MinNumMicrosecondsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))) ||
        (microseconds > mMethodsGet(self)->MaxNumMicrosecondsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))))
        return cAtFalse;

    numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    if ((numPackets < mMethodsGet(self)->MinNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))) ||
        (numPackets > mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))))
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwJitterBufferDelayIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 numPackets;

    if ((microseconds < (mMethodsGet(self)->MinNumMicrosecondsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw)) / 2)) ||
        (microseconds > mMethodsGet(self)->MaxNumMicrosecondsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))))
        return cAtFalse;

    numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    if ((numPackets < AtPwMinJitterBufferDelayInPacketGet(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw))) ||
        (numPackets > mMethodsGet(self)->MaxNumPacketsForJitterDelay(self, pw)))
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwVariantJitterBufferSizeIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 numPackets;
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    if ((microseconds < AtPwMinJitterBufferSize(pw, circuit, AtPwPayloadSizeGet(pw))) ||
        (microseconds > mMethodsGet(self)->MaxNumMicrosecondsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))))
        return cAtFalse;

    numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    if ((numPackets < mMethodsGet(self)->MinNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))) ||
        (numPackets > mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))))
        return cAtFalse;

    return cAtTrue;
    }

static eBool PwVariantJitterBufferDelayIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 numPackets;
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    if ((microseconds < AtPwMinJitterDelay(pw, circuit, AtPwPayloadSizeGet(pw))) ||
        (microseconds > mMethodsGet(self)->MaxNumMicrosecondsForJitterBufferSize(self, pw, AtPwPayloadSizeGet(pw))))
        return cAtFalse;

    numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    if ((numPackets < AtPwMinJitterBufferDelayInPacketGet(pw, AtPwBoundCircuitGet(pw), AtPwPayloadSizeGet(pw))) ||
        (numPackets > mMethodsGet(self)->MaxNumPacketsForJitterDelay(self, pw)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet PwJitterBufferSizeInPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;

    regAddr = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mPolyRegFieldSet(longRegVal[cThaRegPWPdaJitBufCtrlJitBufSizePkDwIndex], mThis(self), JitterBufferSizePkt, numPackets);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    mChannelSuccessAssert(pw, AtPwReorderingEnable(pw, AtPwReorderingIsEnabled(pw)));

    return cAtOk;
    }

static uint16 PwJitterBufferSizeInPacketGet(ThaModulePda self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;

    regAddr = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    return (uint16)mPolyRegField(longRegVal[cThaRegPWPdaJitBufCtrlJitBufSizePkDwIndex], mThis(self), JitterBufferSizePkt);
    }

static eAtRet PwJitterBufferSizeSet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint16 numPackets;

    numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    return ThaModulePdaPwJitterBufferSizeInNumOfPacketSet(self, pw, numPackets);
    }

static eAtRet PwJitterBufferDelaySet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    return ThaModulePdaPwJitterBufferDelayInNumOfPacketSet(self, pw, numPackets);
    }

static eAtRet PwJitterBufferDelayInPacketSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;

    regAddr = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mPolyRegFieldSet(longRegVal[cThaRegPWPdaJitBufCtrlPdvSizePkDwIndex], mThis(self), JitterBufferDelayPkt, numPackets);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    mChannelSuccessAssert(pw, AtPwReorderingEnable(pw, AtPwReorderingIsEnabled(pw)));

    return cAtOk;
    }

static eAtRet PwCwSequenceModeSet(ThaModulePda self, AtPw pw, eAtPwCwSequenceMode sequenceMode)
    {
	AtUnused(pw);
	AtUnused(self);
    if (sequenceMode == cAtPwCwSequenceModeWrapZero)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint32 PwJitterBufferSizeGet(ThaModulePda self, AtPw pw)
    {
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint32 payloadSize = AtPwPayloadSizeGet(pw);
	return ThaPwUtilTimeInUsForNumPackets(ThaModulePdaPwJitterBufferSizeInPacketGet(self, pw),
                                 ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit),
                                 ThaPwUtilPayloadSizeInBytes(pw, circuit, payloadSize));
    }

static uint32 PwJitterBufferDelayGet(ThaModulePda self, AtPw pw)
    {
    AtChannel circuit = AtPwBoundCircuitGet(pw);
    uint32 payloadSize = AtPwPayloadSizeGet(pw);
    return ThaPwUtilTimeInUsForNumPackets(ThaModulePdaPwJitterBufferDelayInPacketGet(self, pw),
                                 ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, circuit),
                                 ThaPwUtilPayloadSizeInBytes(pw, circuit, payloadSize));
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModulePdaV2);
    }

static eAtRet PwCircuitTypeSet(ThaModulePda self, AtPw pw, AtChannel circuit)
    {
    uint32 regAddr, regVal;
    eAtPwType pwType;
    AtPdhDe1 de1 = NULL;
    eBool needToCfgCircuitType = mMethodsGet(mThis(self))->NeedToCfgCircuitType(mThis(self));

    if (!needToCfgCircuitType)
        return cAtOk;

    pwType = AtPwTypeGet(pw);
    if (pwType == cAtPwTypeSAToP)
        de1 = (AtPdhDe1)circuit;
    if (pwType == cAtPwTypeCESoP)
        de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)circuit);

    if (de1 == NULL)
        return cAtOk;

    regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cThaRegPWPdaMdCtrlT1E1, AtPdhDe1IsE1(de1) ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwNumDs0TimeslotsSet(ThaModulePda self, AtPw pw, uint16 numTimeslots)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mPolyRegFieldSet(regVal, mThis(self), PdaNumNxDs0, numTimeslots);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static const char *JitterBufferStateString(ThaModulePda self, uint32 hwState)
    {
    AtUnused(self);

    if (hwState == cThaPdaJitterBufferStateStart)   return "start";
    if (hwState == cThaPdaJitterBufferStateFilling) return "filling";
    if (hwState == 3) return "read wait";
    if (hwState == 4) return "read request";
    if (hwState == 5) return "replay";
    if (hwState == 6) return "near empty";
    if (hwState == 7) return "not valid";

    return "xxx";
    }

static uint8 JitterBufferState(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(mThis(self))->PDATdmJitBufStat(mThis(self)) + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);

    return (uint8)mRegField(regVal, cThaJitBufState);
    }

static void PwDebug(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal;
    eBool jitterBufferFull;

    AtPrintc(cSevNormal, "* Jitter buffer size: %u(us)\r\n", mMethodsGet(self)->PwJitterBufferSizeGet(self, pw));
    AtPrintc(cSevNormal, "* Jitter delay %u(us)\r\n", mMethodsGet(self)->PwJitterBufferDelayGet(self, pw));

    regAddr = mMethodsGet(mThis(self))->PDATdmJitBufStat(mThis(self)) + mPwOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);
    AtPrintc(cSevNormal, "* Number of packets in jitter buffer: %u\r\n", (uint32)mPolyRegField(regVal, mThis(self), BufferNumPkt));

    jitterBufferFull = mRegField(regVal, cThaRxJitBufFull) ? cAtTrue : cAtFalse;
    AtPrintc(cSevNormal, "* Jitter buffer is full: ");
    AtPrintc(jitterBufferFull ? cSevCritical : cSevNormal, "%s\r\n", jitterBufferFull ? "full" : "not full");

    AtPrintc(cSevNormal, "* Jitter buffer state: %s\r\n", mMethodsGet(self)->JitterBufferStateString(self, mMethodsGet(self)->JitterBufferState(self, pw)));
    }

static uint32 PwAlarmHw2Sw(uint32 hwAlarm)
    {
    uint32 alarms  = 0;

    if (hwAlarm & cThaPmcPwUnderrunCurStatMask) alarms |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (hwAlarm & cThaPmcPwOverrunCurStatMask)  alarms |= cAtPwAlarmTypeJitterBufferOverrun;
    if (hwAlarm & cThaPmcPwLofsCurStatMask)     alarms |= cAtPwAlarmTypeLops;
    if (hwAlarm & cThaPmcPwLbitCurStatMask)     alarms |= cAtPwAlarmTypeLBit;
    if (hwAlarm & cThaPmcPwRbitCurStatMask)     alarms |= cAtPwAlarmTypeRBit;

    return alarms;
    }

static uint32 PwAlarmCurrentStat(ThaModulePdaV2 self)
    {
	AtUnused(self);
    return cThaRegPmcPwAlarmCurrentStat;
    }

static uint32 PwAlarmIntrStat(ThaModulePdaV2 self)
    {
	AtUnused(self);
    return cThaRegPmcPwAlarmIntrStat;
    }

static uint32 PwAlarmGet(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->PwAlarmCurrentStat(mThis(self)) + mPwOffset(self, pw);
    return PwAlarmHw2Sw(mChannelHwRead(pw, regAddr, cThaModulePda));
    }

static uint32 PwDefectHistoryGet(ThaModulePda self, AtPw pw, eBool read2Clear)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->PwAlarmIntrStat(mThis(self)) + mPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);
    uint32 history = PwAlarmHw2Sw(regVal);

    if (read2Clear)
        mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return history;
    }

static eAtRet PartDefaultSet(ThaModulePda self, uint8 partId)
    {
    uint32 offset, statusReg, controlReg;

    if (!mMethodsGet(self)->JitterBufferCenterIsSupported(self))
        return cAtOk;

    offset     = ThaModulePdaPartOffset(self, partId);
    statusReg  = mMethodsGet(mThis(self))->HotConfigureEngineStatus(mThis(self));
    controlReg = mMethodsGet(mThis(self))->HotConfigureEngineControl(mThis(self));

    mModuleHwWrite(self, statusReg  + offset, 0);
    mModuleHwWrite(self, controlReg + offset, 0);

    return cAtOk;
    }

static uint16 NumberOfPacketByUs(ThaModulePda self, AtPw pw, uint32 numUs)
    {
    uint32 rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, AtPwBoundCircuitGet(pw));
    uint32 maxNumPackets = mMethodsGet(self)->MaxNumPacketsForJitterBufferSize(self, pw, ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));
    uint32 numPackets = ThaPwUtilNumPacketsForTimeInUs(numUs, rateInBytePerMs, ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));
    /* TODO: This calculation shall round down to hardware limit (if any).
     * Does it violate the original behavior, which may only calculate packets based on the user input? */
    return (uint16)mMin(maxNumPackets, numPackets);
    }

static uint32 UsByNumberOfPacket(ThaModulePda self, AtPw pw, uint32 numPkts)
    {
    uint32 rateInBytePerMs;

    AtUnused(self);

    rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, AtPwBoundCircuitGet(pw));
    if (rateInBytePerMs == 0)
        return 0;

    return ThaPwUtilTimeInUsForNumPackets(numPkts, rateInBytePerMs, ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));
    }

static uint32 PwPartOffset(ThaModulePda self, AtPw pw)
    {
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
    uint8 partId = ThaModulePwPartOfPw(pwModule, pw);
    return ThaModulePdaPartOffset(self, partId);
    }

static eBool IsSimulated(ThaModulePda self)
    {
    return AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self));
    }

static eBool CenterEngineIsReady(ThaModulePda self, AtPw pw)
    {
    const uint32 cTimeoutMs = 1500;
    const uint32 cRestCycleMs = 100;
    const uint32 cRestTimeMs = 200;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0, rest_i = 0;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 regAddr;

    regAddr = mMethodsGet(mThis(self))->HotConfigureEngineStatus(mThis(self)) + PwPartOffset(self, pw);
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTime < cTimeoutMs)
        {
        uint32 numCycles;
        uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePda);

        if (IsSimulated(self))
            return cAtTrue;

        if (regVal == 0) /* Ready */
            {
            if (elapseTime > mThis(self)->centerEngineReadyWaitTimeInMs)
                mThis(self)->centerEngineReadyWaitTimeInMs = elapseTime;

            return cAtTrue;
            }

        /* Continue waiting */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);

        /* Have some rest each time elapse increase 100ms */
        numCycles = elapseTime / cRestCycleMs;
        if (numCycles > rest_i)
            {
            AtOsalUSleep(cRestTimeMs * 1000);
            rest_i = numCycles;
            }
        }

    return cAtFalse;
    }

static eAtRet CenterEngineReadyForce(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->HotConfigureEngineStatus(mThis(self)) + PwPartOffset(self, pw);
    mChannelHwWrite(pw, regAddr, 0, cThaModulePda);
    return cAtOk;
    }

static uint32 HotConfigureEngineRequestInfoPwIdMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaHotConfigureEngineRequestInfoPwIdMask;
    }

static uint8 HotConfigureEngineRequestInfoPwIdShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaHotConfigureEngineRequestInfoPwIdShift;
    }

static eAtRet CenterEngineWaitReady(ThaModulePda self, AtPw pw)
    {
    eBool canWait = cAtTrue;

    if ((AtPwEthPortGet(pw) == NULL) && (AtPwBoundCircuitGet(pw) == NULL))
        canWait = cAtFalse;

    if (!canWait)
        return CenterEngineReadyForce(self, pw);

    if (CenterEngineIsReady(self, pw))
        return cAtOk;

    /* Engine is not ready for some reason, just force it become ready for next
     * action. If not, jitter configuration will be fail forever */
    AtChannelLog((AtChannel)pw, cAtLogLevelCritical, AtSourceLocation,
                 "Jitter center engine is not ready, it is forced to ready state\r\n");
    CenterEngineReadyForce(self, pw);

    return cAtErrorDevBusy;
    }

static eAtRet PwCenterJitterStart(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr, regVal = 0;
    uint32 pwIdMask;
    uint8 pwIdShift;
    uint32 pwId;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(self)->JitterBufferCenterIsSupported(self))
        return cAtOk;

    ret = CenterEngineWaitReady(self, pw);
    if (ret != cAtOk)
        return ret;

    /* Make request for this PW */
    regVal   = 0;
    regAddr  = mMethodsGet(mThis(self))->HotConfigureEngineControl(mThis(self)) + PwPartOffset(self, pw);

    pwIdMask  = mMethodsGet(mThis(self))->HotConfigureEngineRequestInfoPwIdMask(mThis(self));
    pwIdShift = mMethodsGet(mThis(self))->HotConfigureEngineRequestInfoPwIdShift(mThis(self));

    pwId = mMethodsGet(mThis(self))->HotConfigureEnginePwId(mThis(self), pw);
    mRegFieldSet(regVal, pwId, pwId);
    mRegFieldSet(regVal, cThaHotConfigureEngineRequestInfoInit, 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return CenterEngineWaitReady(self, pw);
    }

static eAtRet PwCenterJitterStop(ThaModulePda self, AtPw pw)
    {
    uint32 regVal = 0;
    uint32 address = mMethodsGet(mThis(self))->HotConfigureEngineControl(mThis(self));
    uint32 pwId;

    uint32 pwIdMask  = mMethodsGet(mThis(self))->HotConfigureEngineRequestInfoPwIdMask(mThis(self));
    uint8 pwIdShift = mMethodsGet(mThis(self))->HotConfigureEngineRequestInfoPwIdShift(mThis(self));

    pwId = mMethodsGet(mThis(self))->HotConfigureEnginePwId(mThis(self), pw);
    mRegFieldSet(regVal, pwId, pwId);
    mChannelHwWrite(pw, address + PwPartOffset(self, pw), regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet HotConfigureStart(ThaModulePda self, AtPw pw)
    {
    if (mMethodsGet(self)->JitterBufferCenterIsSupported(self))
        return cAtOk;

    return m_ThaModulePdaMethods->HotConfigureStart(self, pw);
    }

static eAtRet HotConfigureStop(ThaModulePda self, AtPw pw)
    {
    if (mMethodsGet(self)->JitterBufferCenterIsSupported(self))
        return cAtOk;

    return m_ThaModulePdaMethods->HotConfigureStop(self, pw);
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);
    AtPrintc(cSevNormal, "* centerEngineReadyWaitTime: %u(ms)\r\n", mThis(self)->centerEngineReadyWaitTimeInMs);

    /* Clear statistic */
    mThis(self)->centerEngineReadyWaitTimeInMs = 0;

    return cAtOk;
    }

static uint32 VcPartOffset(ThaModulePda self, AtSdhChannel vc)
    {
    return ThaModulePdaPartOffset(self, ThaModuleSdhPartOfChannel(vc));
    }

static eAtRet VcDeAssemblerConcate(ThaModulePda self, AtSdhChannel vc)
    {
    uint8 sts_i, hwSlice;
    uint8 masterSts = AtSdhChannelSts1Get(vc);
    uint8 masterHwSts = 0;
    uint32 partOffset = VcPartOffset(self, vc);

    ThaSdhChannelHwStsGet(vc, cThaModulePda, masterSts, &hwSlice, &masterHwSts);
    for (sts_i = 0; sts_i < AtSdhChannelNumSts(vc); sts_i++)
        {
        uint8 stsId    = (uint8)(masterSts + sts_i);
        uint32 offset  = mMethodsGet(mThis(self))->SdhChannelStsOffset(mThis(self), vc, stsId) + partOffset;
        uint32 regAddr = cThaRegPDACEPDeAssemblerControl + offset;
        uint32 regVal  = mChannelHwRead(vc, regAddr, cThaModulePda);
        uint8 concate  = (sts_i == 0) ? cThaRegPDASTSVCCcatMaster : cThaRegPDASTSVCCcatSlave;

        mRegFieldSet(regVal, cThaRegPDASTSVCCcatEn, concate);
        mRegFieldSet(regVal, cThaRegPDASTSVCCcatMstID, masterHwSts);
        mChannelHwWrite(vc, regAddr, regVal, cThaModulePda);
        }

    return cAtOk;
    }

static eAtRet Tug3Vc3FragmentTypeSet(ThaModulePda self, AtSdhChannel tug3Vc3, eBool isTug3)
    {
    uint8 vtgId;
    uint32 regAddr, regVal;
    uint8 tug3Vc3Map7xTug2s = (isTug3) ? cAtSdhTugMapTypeTug3Map7xTug2s : cAtSdhVcMapTypeVc3Map7xTug2s;
    uint32 partOffset = VcPartOffset(self, tug3Vc3);
    uint32 offset;

    if (AtSdhChannelMapTypeGet(tug3Vc3) != tug3Vc3Map7xTug2s)
        return cAtOk;

    offset = mMethodsGet(mThis(self))->SdhChannelOffset(mThis(self), tug3Vc3);
    regAddr = cThaRegPDACEPDeAssemblerControl + offset + partOffset;

    regVal  = mChannelHwRead(tug3Vc3, regAddr, cThaModulePda);
    for (vtgId = 0; vtgId < AtSdhChannelNumberOfSubChannelsGet(tug3Vc3); vtgId++)
        {
        AtSdhChannel tug = AtSdhChannelSubChannelGet(tug3Vc3, vtgId);
        uint8 hwType = (AtSdhChannelMapTypeGet(tug) == cAtSdhTugMapTypeTug2Map3xTu12s) ? 1 : 0;
        mFieldIns(&regVal, cThaRegPDAVTTypeMask(vtgId), cThaRegPDAVTTypeShift(vtgId), hwType);
        }
    mChannelHwWrite(tug3Vc3, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet Tug3PwCepFractionalSet(ThaModulePda self, AtSdhChannel tug3)
    {
    uint8 slice, hwSts;
    uint32 sliceOffset, regAddr, regVal;
    uint8 tug3FractionalVc3 = 0;

    ThaSdhChannel2HwMasterStsId(tug3, cThaModulePda, &slice, &hwSts);
    sliceOffset = mMethodsGet(mThis(self))->SdhChannelSliceOffset(mThis(self), slice);
    regAddr = cThaRegPDACepDeAssemblerVc4FractionalVc3Control + sliceOffset + VcPartOffset(self, tug3);
    regVal = mChannelHwRead(tug3, regAddr, cThaModulePda);

    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        tug3FractionalVc3 = 1;

    mFieldIns(&regVal, cThaRegPDACepVC4FractionalVC3Mask(hwSts), cThaRegPDACepVC4FractionalVC3Shift(hwSts), tug3FractionalVc3);
    mChannelHwWrite(tug3, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet Vc4FragmentTypeSet(ThaModulePda self, AtSdhChannel vc)
    {
    uint8 tug3Id;
    eAtRet ret = cAtOk;
    AtSdhChannel tug3;
    ThaModulePdaV2 modulePdaV2 = mThis(self);
    eBool fractionalSupported;

    if (AtSdhChannelMapTypeGet(vc) == cAtSdhVcMapTypeVc4MapC4)
        return cAtOk;

    fractionalSupported = mMethodsGet(modulePdaV2)->CepFractionalIsSupported(modulePdaV2);
    for (tug3Id = 0; tug3Id < AtSdhChannelNumberOfSubChannelsGet(vc); tug3Id++)
        {
        tug3 = AtSdhChannelSubChannelGet(vc, tug3Id);
        ret |= Tug3Vc3FragmentTypeSet(self, tug3, cAtTrue);
        if (fractionalSupported)
            ret |= Tug3PwCepFractionalSet(self, tug3);
        }

    return ret;
    }

static eAtRet Vc3PwCepFractionalSet(ThaModulePda self, AtSdhChannel vc3)
    {
    uint8 slice, hwSts;
    uint32 sliceOffset, regAddr, regVal;

    ThaSdhChannel2HwMasterStsId(vc3, cThaModulePda, &slice, &hwSts);

    sliceOffset = mMethodsGet(mThis(self))->SdhChannelSliceOffset(mThis(self), slice);
    regAddr     = cThaRegPDACepDeAssemblerVc4FractionalVc3Control + sliceOffset + VcPartOffset(self, vc3);
    regVal      = mChannelHwRead(vc3, regAddr, cThaModulePda);
    mFieldIns(&regVal, cThaRegPDACepVC4FractionalVC3Mask(hwSts), cThaRegPDACepVC4FractionalVC3Mask(hwSts), 0);
    mChannelHwWrite(vc3, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet Vc3FragmentTypeSet(ThaModulePda self, AtSdhChannel vc)
    {
    eAtRet ret = cAtOk;
    ThaModulePdaV2 modulePdaV2 = mThis(self);

    ret |= Tug3Vc3FragmentTypeSet(self, vc, cAtFalse);

    if (mMethodsGet(modulePdaV2)->CepFractionalIsSupported(modulePdaV2))
        ret |= Vc3PwCepFractionalSet(self, vc);

    return ret;
    }

static eAtRet VcFragmentTypeSet(ThaModulePda self, AtSdhChannel vc)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(vc);

    if (vcType == cAtSdhChannelTypeVc3)
        return Vc3FragmentTypeSet(self, vc);

    if (vcType == cAtSdhChannelTypeVc4)
        return Vc4FragmentTypeSet(self, vc);

    return cAtOk;
    }

static eAtRet VcDeAssemblerSet(ThaModulePda self, AtSdhChannel vc)
    {
    eAtRet ret = cAtOk;

    ret |= VcDeAssemblerConcate(self, vc);
    ret |= VcFragmentTypeSet(self, vc);

    return ret;
    }

static eAtRet Tug3CepFractionalVc3Equip(ThaModulePda self, AtSdhChannel tug3, AtChannel subChannel, eBool equip)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 offset = mMethodsGet(mThis(self))->SdhChannelOffset(mThis(self), tug3);
	AtUnused(subChannel);

    regAddr = cThaRegPDACepDeAssemblerEbmConfig + offset + VcPartOffset(self, tug3);
    regVal = mChannelHwRead(tug3, regAddr, cThaModulePda);

    mRegFieldSet(regVal, cThaRegPDACepDeAssemblerTBit, mBoolToBin(equip));
    mChannelHwWrite(tug3, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 Vc1xMaskSet(uint32 currentMask, AtChannel vc1x, eBool equip)
    {
    uint8 tug2 = AtSdhChannelTug2Get((AtSdhChannel)vc1x);
    uint8 tu1x = AtSdhChannelTu1xGet((AtSdhChannel)vc1x);

    mFieldIns(&currentMask, cTu1xBitMask(tug2, tu1x), cTu1xBitShift(tug2, tu1x), mBoolToBin(equip));
    return currentMask;
    }

static eAtRet Tug3Vc3CepFractionalVc1xEquip(ThaModulePda self, AtSdhChannel tug3Vc3, AtChannel subChannel, eBool equip)
    {
    uint32 regAddr, regVal, vc1xMask;
    uint32 offset = mMethodsGet(mThis(self))->SdhChannelOffset(mThis(self), tug3Vc3);

    regAddr  = cThaRegPDACepDeAssemblerEbmConfig + offset + VcPartOffset(self, tug3Vc3);
    regVal   = mChannelHwRead(tug3Vc3, regAddr, cThaModulePda);
    vc1xMask = mRegField(regVal, cThaRegPDACepDeAssemblerEbmBit);
    vc1xMask = Vc1xMaskSet(vc1xMask, subChannel, equip);

    mRegFieldSet(regVal, cThaRegPDACepDeAssemblerEbmBit, vc1xMask);
    mChannelHwWrite(tug3Vc3, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet Tug3CepFractionalSubChannelEquip(ThaModulePda self, AtSdhChannel tug3, AtChannel subChannel, eBool equip)
    {
    if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3MapVc3)
        return Tug3CepFractionalVc3Equip(self, tug3, subChannel, equip);

    return Tug3Vc3CepFractionalVc1xEquip(self, tug3, subChannel, equip);
    }

static eAtRet Vc4CepFractionalSubChannelEquip(ThaModulePda self, AtSdhChannel vc, AtChannel subChannel, eBool equip)
    {
    uint8 tug3Id;
    eAtRet ret = cAtOk;
    AtSdhChannel tug3;

    for (tug3Id = 0; tug3Id < AtSdhChannelNumberOfSubChannelsGet(vc); tug3Id++)
        {
        tug3 = AtSdhChannelSubChannelGet(vc, tug3Id);
        ret |= Tug3CepFractionalSubChannelEquip(self, tug3, subChannel, equip);
        }

    return ret;
    }

static eBool PwCepEbmIsEnabled(ThaModulePda self, AtPw pw)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    uint8 slice, hwSts, bitVal;
    uint32 sliceOffset, regAddr, regVal;

    ThaSdhChannel2HwMasterStsId(vc, cThaModulePda, &slice, &hwSts);
    sliceOffset = mMethodsGet(mThis(self))->SdhChannelSliceOffset(mThis(self), slice);
    regAddr     = cThaRegPDACEPDeAssemblerEBMModeControl + sliceOffset + VcPartOffset(self, vc);
    regVal      = mChannelHwRead(vc, regAddr, cThaModulePda);
    mFieldGet(regVal, cThaRegPDAEBMMdMask(hwSts), cThaRegPDAEBMMdShift(hwSts), uint8, &bitVal);

    return (bitVal) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCepEquip(ThaModulePda self, AtPw pw, AtChannel subChannel, eBool equip)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(vc);

    /* HW recommend, if enable EBM mode, should not configure bit mask, they will be updated
     * by HW automatically
     */
    if (PwCepEbmIsEnabled(self, pw))
        return cAtOk;

    if (vcType == cAtSdhChannelTypeVc3)
        return Tug3Vc3CepFractionalVc1xEquip(self, vc, subChannel, equip);

    if (vcType == cAtSdhChannelTypeVc4)
        return Vc4CepFractionalSubChannelEquip(self, vc, subChannel, equip);

    return cAtOk;
    }

static eAtRet TdmControlEbmEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    uint32 regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cThaRegPWPdaMdEbmEn, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwCepEbmEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    uint8 hwSts, slice, sts_i;
    uint32 sliceOffset, regAddr, regVal;
    uint8 sts1Id = AtSdhChannelSts1Get(vc);
    ThaModulePdaV2 modulePdaV2 = mThis(self);

    if (!mMethodsGet(modulePdaV2)->CepFractionalIsSupported(modulePdaV2))
        return TdmControlEbmEnable(self, pw, cAtFalse);

    ThaSdhChannel2HwMasterStsId(vc, cThaModulePda, &slice, &hwSts);
    sliceOffset = mMethodsGet(mThis(self))->SdhChannelSliceOffset(mThis(self), slice);
    regAddr     = cThaRegPDACEPDeAssemblerEBMModeControl + sliceOffset + VcPartOffset(self, vc);
    regVal      = mChannelHwRead(vc, regAddr, cThaModulePda);

    for (sts_i = 0; sts_i < AtSdhChannelNumSts(vc); sts_i++)
        {
        ThaSdhChannelHwStsGet(vc, cThaModulePda, (uint8)(sts1Id + sts_i), &slice, &hwSts);
        mFieldIns(&regVal, cThaRegPDAEBMMdMask(hwSts), cThaRegPDAEBMMdShift(hwSts), enable ? 0 : 1);
        }

    mChannelHwWrite(vc, regAddr, regVal, cThaModulePda);

    /* PDA TDM Mode Control */
    return TdmControlEbmEnable(self, pw, enable);
    }

static eAtRet HwStsIdSet(ThaModulePda self, AtPw pw)
    {
    uint8 hwSlice, hwSts;
    uint32 regAddr, regVal;
    eAtPwType pwType = AtPwTypeGet(pw);

    if (pwType == cAtPwTypeCEP)
        {
        AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
        ThaSdhChannel2HwMasterStsId(vc, cThaModulePda, &hwSlice, &hwSts);
        }

    else if (pwType == cAtPwTypeSAToP)
        {
        AtPdhChannel pdhChannel = (AtPdhChannel)AtPwBoundCircuitGet(pw);
        ThaPdhChannelHwIdGet(pdhChannel, cThaModulePda, &hwSlice, &hwSts);
        }

    else
        return cAtOk;

    regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mPolyRegFieldSet(regVal, mThis(self), PdaFirstStsId, hwSts);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwCircuitIdSet(ThaModulePda self, AtPw pw)
    {
    eAtRet ret = HwStsIdSet(self, pw);
    if (ret != cAtOk)
        return ret;

    if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        {
        AtSdhChannel vc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
        return ThaModulePdaVcDeAssemblerSet(self, vc);
        }

    return cAtOk;
    }

static uint32 EparLineIdLkCtr(ThaModulePda self)
    {
	AtUnused(self);
    return cThaRegPDAEPARLineIDLookupControl;
    }

static eAtRet PwTohEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pw);
    uint32 partOffset, address, regVal;
    uint8 lineHwId;
    eAtSdhLineRate rate;

    if (line == NULL)
        return cAtOk;

    partOffset = ThaModulePdaPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)line));
    address = cThaRegThalassaPDATohPseudowireLineEnCtrl + partOffset;
    regVal = mChannelHwRead(line, address, cThaModulePda);
    lineHwId = (uint8)AtChannelHwIdGet((AtChannel)line);
    rate = AtSdhLineRateGet(line);

    mFieldIns(&regVal, cThaPdaTohOcnLineRateMask(lineHwId), cThaPdaTohOcnLineRateShift(lineHwId),
              (rate == cAtSdhLineRateStm1) ? 0 : 1);

    mFieldIns(&regVal, cThaPdaTohOcnLineEnMask(lineHwId), cThaPdaTohOcnLineEnShift(lineHwId), mBoolToBin(enable));
    mFieldIns(&regVal, cThaPdaTohPwLineEnMask(lineHwId), cThaPdaTohPwLineEnShift(lineHwId), mBoolToBin(enable));
    mFieldIns(&regVal, cThaPdaTohOcnLineGrpMask, cThaPdaTohOcnLineGrpShift, 0);

    mChannelHwWrite(line, address, regVal, cThaModulePda);
    return cAtOk;
    }

static uint32 TohMappingRegOffset(ThaModulePda self, AtSdhLine line, uint8 stsId)
    {
    uint32 partOffset = ThaModulePdaPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)line));
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    eAtSdhLineRate lowRate = ThaSdhLineLowRate((ThaSdhLine)line);

    if (rate == lowRate)
        return stsId * 4UL + AtChannelHwIdGet((AtChannel)line) + partOffset;

    return stsId + partOffset;
    }

static void TohMappingReset(ThaModulePda self, AtSdhLine line)
    {
    uint8 sts_i, numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    uint32 address;
    uint32 longReg[cThaLongRegMaxSize];
    AtOsalMemInit(longReg, 0, sizeof(uint32) * cThaLongRegMaxSize);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        address = cThaRegThalassaPDATohPseudowireByteMappingCtrl + TohMappingRegOffset(self, line, sts_i);
        mChannelHwLongWrite(line, address, longReg, cThaLongRegMaxSize, cThaModulePda);
        }
    }

static eAtRet BindLineToPw(ThaModulePda self, AtSdhLine line, AtPw pw)
    {
    uint32 offset = ThaModulePdaPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)line));
    uint8 lineHwId = (uint8)AtChannelHwIdGet((AtChannel)line);
    uint32 regVal = 0;
    uint32 address = cThaRegThalassaPDATohPseudowireLineIdCtrl + (uint32)lineHwId + offset;

    TohMappingReset(self, line);

    if (pw == NULL)
        return cAtOk;

    mRegFieldSet(regVal, cThaPdaTohPwIdLine, AtChannelHwIdGet((AtChannel)pw));
    mChannelHwWrite(line, address, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtModulePwRet PwIdleCodeSet(ThaModulePda self, ThaPwAdapter pwAdapter, uint8 idleCode)
    {
    uint32 regAddr, regVal;
    AtPw pw;

    if (!mMethodsGet(self)->PwIdleCodeIsSupported(self))
        return cAtErrorModeNotSupport;

    pw = (AtPw)pwAdapter;
    regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    mFieldIns(&regVal, mMethodsGet(mThis(self))->PdaIdleCodeMask(mThis(self), pw),
              mMethodsGet(mThis(self))->PdaIdleCodeShift(mThis(self), pw), idleCode);

    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwTohByteEnable(ThaModulePda self, AtPw pwAdapter, const tThaTohByte *tohByte, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0, col = 0;

    if (line == NULL)
        return cAtError;

    address = cThaRegThalassaPDATohPseudowireByteMappingCtrl + TohMappingRegOffset(self, line, tohByte->sts1);
    ThaTohByteRowColGet(tohByte, &row, &col);
    mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePda);

    mFieldIns(&longReg[cThaPdaTohPwByteEnCtrlDwIndex],
              cThaPdaTohPwByteEnCtrlMask(row, col),
              cThaPdaTohPwByteEnCtrlShift(row, col),
              mBoolToBin(enable));

    mChannelHwLongWrite(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePda);
    return cAtOk;
    }

static eAtRet PwTohFirstByteEnable(ThaModulePda self, AtPw pwAdapter, const tThaTohByte *tohByte, eBool enable)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0, col = 0;

    if (line == NULL)
        return cAtError;

    address = cThaRegThalassaPDATohPseudowireByteMappingCtrl + TohMappingRegOffset(self, line, tohByte->sts1);
    ThaTohByteRowColGet(tohByte, &row, &col);
    mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePda);

    mRegFieldSet(longReg[cThaPdaTohPwFirstByteEnCtrlDwIndex], cThaPdaTohPwFirstByteEnCtrl, mBoolToBin(enable));
    mRegFieldSet(longReg[cThaPdaTohPwFirstByteColCtrlDwIndex], cThaPdaTohPwFirstByteColCtrl, col);
    mRegFieldSet(longReg[cThaPdaTohPwFirstByteRowCtrlTailDwIndex], cThaPdaTohPwFirstByteRowCtrlTail, row);
    mRegFieldSet(longReg[cThaPdaTohPwFirstByteRowCtrlHeadDwIndex], cThaPdaTohPwFirstByteRowCtrlHead, row >> 3);

    mChannelHwLongWrite(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePda);
    return cAtOk;
    }

static tThaTohByte* PwTohFirstByteGet(ThaModulePda self, AtPw pwAdapter, tThaTohByte *tohByte)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 row = 0, col = 0;
    uint8 numSts, sts_i;
    eBool firstFound = cAtFalse;

    if ((line == NULL) || (tohByte == NULL))
        return NULL;

    numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        address = cThaRegThalassaPDATohPseudowireByteMappingCtrl + TohMappingRegOffset(self, line, sts_i);
        mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePda);

        if (mRegField(longReg[cThaPdaTohPwFirstByteEnCtrlDwIndex], cThaPdaTohPwFirstByteEnCtrl) == 0)
            continue;

        /* Opps, There is more than one last byte, return NULL to indicate that something wrong */
        if (firstFound)
            return NULL;

        firstFound = cAtTrue;
        col = (uint8)mRegField(longReg[cThaPdaTohPwFirstByteColCtrlDwIndex], cThaPdaTohPwFirstByteColCtrl);
        row = (uint8)mRegField(longReg[cThaPdaTohPwFirstByteRowCtrlHeadDwIndex], cThaPdaTohPwFirstByteRowCtrlHead);
        row = (uint8)((uint32)(row << 3) | mRegField(longReg[cThaPdaTohPwFirstByteRowCtrlTailDwIndex], cThaPdaTohPwFirstByteRowCtrlTail));

        tohByte->sts1 = sts_i;
        tohByte->ohByte = ThaTohByteFromRowColGet(row, col);

        if ((uint32)tohByte->ohByte == cThaInvalidTohByte)
            return NULL;
        }

    return (firstFound) ? tohByte : NULL;
    }

static uint8 PwTohBytesBitmapGet(ThaModulePda self, AtPw pwAdapter, uint32 *bitmaps, uint8 numBitmap)
    {
    AtSdhLine line = (AtSdhLine)AtPwBoundCircuitGet(pwAdapter);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 address;
    uint8 numSts, sts_i;

    if (line == NULL)
        return 0;

    numSts = (AtSdhLineRateGet(line) == cAtSdhLineRateStm1) ? 3 : 12;
    if (bitmaps == NULL)
        return numSts;

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        if (sts_i >= numBitmap)
            break;

        address = cThaRegThalassaPDATohPseudowireByteMappingCtrl + TohMappingRegOffset(self, line, sts_i);
        mChannelHwLongRead(pwAdapter, address, longReg, cThaLongRegMaxSize, cThaModulePda);

        bitmaps[sts_i] = mRegField(longReg[cThaPdaTohPwByteEnCtrlDwIndex], cThaPdaTohPwByteBitmap);
        }

    return numSts;
    }

static uint8  PwIdleCodeGet(ThaModulePda self, ThaPwAdapter pwAdapter)
    {
    uint32 regAddr, regVal;
    AtPw pw = (AtPw)pwAdapter;
    uint32 idleCodeMask = mMethodsGet(mThis(self))->PdaIdleCodeMask(mThis(self), pw);
    uint8  idleCodeShift = mMethodsGet(mThis(self))->PdaIdleCodeShift(mThis(self), pw);

    if (!mMethodsGet(self)->PwIdleCodeIsSupported(self))
        return m_ThaModulePdaMethods->PwIdleCodeGet(self, pwAdapter);

    regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pwAdapter);
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePda);
    return (uint8)(mRegField(regVal, idleCode));
    }

static eBool NeedToCfgCircuitType(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PdaFirstStsIdMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaFirstStsIdMask;
    }

static uint8  PdaFirstStsIdShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaFirstStsIdShift;
    }

static uint32 PdaIdleCodeMask(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaRegPWPdaMdCtrlIdleCodeMask;
    }

static uint8 PdaIdleCodeShift(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaRegPWPdaMdCtrlIdleCodeShift;
    }

static uint32 PdaNumNxDs0Mask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaMdCtrlNxDs0Mask;
    }

static uint8 PdaNumNxDs0Shift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaMdCtrlNxDs0Shift;
    }

static uint32 SdhChannelOffset(ThaModulePdaV2 self, AtSdhChannel channel)
    {
    uint8 hwSlice, hwSts;
    ThaSdhChannel2HwMasterStsId(channel, cThaModulePda, &hwSlice, &hwSts);

    return hwSts + mMethodsGet(self)->SdhChannelSliceOffset(self, hwSlice);
    }

static uint32 SdhChannelStsOffset(ThaModulePdaV2 self, AtSdhChannel channel, uint8 stsId)
    {
    uint8 hwSlice, hwSts;
    ThaSdhChannelHwStsGet(channel, cThaModulePda, stsId, &hwSlice, &hwSts);

    return hwSts + mMethodsGet(self)->SdhChannelSliceOffset(self, hwSlice);
    }

static uint32 SdhChannelSliceOffset(ThaModulePdaV2 self, uint8 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    return 0;
    }

static eAtRet AutoInsertRdiEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    uint32 regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);

    mFieldIns(&regVal, mMethodsGet(mThis(self))->AisRdiUneqOffMask(mThis(self), pw),
              mMethodsGet(mThis(self))->AisRdiUneqOffShift(mThis(self), pw), enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eBool AutoInsertRdiIsEnabled(ThaModulePda self, AtPw pw)
    {
    uint32 regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mTdmOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePda);

    return (regVal & mMethodsGet(mThis(self))->AisRdiUneqOffMask(mThis(self), pw)) ? cAtFalse : cAtTrue;
    }

static eBool CepFractionalIsSupported(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 JitterBufferSizePktMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaJitBufCtrlJitBufSizePkMask;
    }

static uint8 JitterBufferSizePktShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaJitBufCtrlJitBufSizePkShift;
    }

static uint32 JitterBufferDelayPktMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaJitBufCtrlPdvSizePkMask;
    }

static uint8 JitterBufferDelayPktShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegPWPdaJitBufCtrlPdvSizePkShift;
    }

static uint32 BufferNumPktMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRxBufNumPktMask;
    }

static uint8  BufferNumPktShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRxBufNumPktShift;
    }

static uint32 BufferNumAdditionalBytesMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRxBufNumAdditionalBytesMask;
    }

static uint8  BufferNumAdditionalBytesShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRxBufNumAdditionalBytesShift;
    }

static eAtModulePwRet ModuleIdleCodeSet(ThaModulePda self, uint8 idleCode)
    {
    AtUnused(self);
    AtUnused(idleCode);
    return cAtErrorModeNotSupport;
    }

static uint8 ModuleIdleCodeGet(ThaModulePda self)
    {
    AtUnused(self);
    return 0x0;
    }

static eBool ModuleIdleCodeIsSupported(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 AisRdiUneqOffMask(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaRegPWPdaRDIOffMask;
    }

static uint8 AisRdiUneqOffShift(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cThaRegPWPdaRDIOffShift;
    }

static eAtRet PwCwAutoRxMBitEnable(ThaModulePda self, AtPw pwAdapter, eBool enable)
    {
	/* Default same logic as Auto-RDI insertion */
	if (mMethodsGet(self)->PwCwAutoRxMBitIsConfigurable(self))
		return mMethodsGet(self)->AutoInsertRdiEnable(self, pwAdapter, enable);

	return m_ThaModulePdaMethods->PwCwAutoRxMBitEnable(self, pwAdapter, enable);
    }

static eBool PwCwAutoRxMBitIsEnabled(ThaModulePda self, AtPw pwAdapter)
    {
	if (mMethodsGet(self)->PwCwAutoRxMBitIsConfigurable(self))
		return mMethodsGet(self)->AutoInsertRdiIsEnabled(self, pwAdapter);

	return m_ThaModulePdaMethods->PwCwAutoRxMBitIsEnabled(self, pwAdapter);
    }

static uint32 CalculateNumPacketsByUsAndPayload(ThaModulePda self, AtPw adapter, uint32 bufferSizeInUs, uint32 payloadSizeInByte)
    {
    uint32 rateInBytePerMs = ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)adapter, AtPwBoundCircuitGet(adapter));
    AtUnused(self);
    return ThaPwUtilNumPacketsForTimeInUs(bufferSizeInUs, rateInBytePerMs, payloadSizeInByte);
    }

static uint32 HotConfigureEngineStatus(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegHotConfigureEngineStatus;
    }

static uint32 HotConfigureEngineControl(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cThaRegHotConfigureEngineRequestInfo;
    }

static uint32 HotConfigureEnginePwId(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    return ThaModulePwLocalPwId((ThaModulePw)AtChannelModuleGet((AtChannel)pw), pw);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(centerEngineReadyWaitTimeInMs);
    }

static void JitterBufferRegShow(ThaModulePdaV2 self, AtPw pw)
    {
    ThaModulePdaRegDisplay(pw, "Pseudowire PDA Jitter Buffer Control", mMethodsGet(self)->PWPdaJitBufCtrl(self) + mPwOffset(self, pw));
    }

static void PwRegsShow(ThaModulePda self, AtPw pw)
    {
    AtPw adapter = (AtPw)ThaPwAdapterGet(pw);
    uint32 offset = mPwOffset(self, adapter);

    uint32 address = mMethodsGet(self)->PWPdaTdmModeCtrl(self, adapter) + offset;
    ThaModulePdaRegDisplay(adapter, "Pseudowire PDA Mode Control", address);

    mMethodsGet(mThis(self))->JitterBufferRegShow(mThis(self), adapter);

    address = mMethodsGet(mThis(self))->PDATdmJitBufStat(mThis(self)) + offset;
    ThaModulePdaRegDisplay(adapter, "PDA TDM Jitter Buffer States", address);

    address = cThaRegPWPdaReorderCtrl + offset;
    ThaModulePdaRegDisplay(adapter, "Pseudowire PDA Reorder Control", address);

    address = mMethodsGet(self)->PwPdaIdleCodeCtrl(self) + offset;
    ThaModulePdaRegDisplay(adapter, "PwPda Idle Code Control", address);

    address = mMethodsGet(self)->EparLineIdLkCtr(self) + offset;
    ThaModulePdaRegDisplay(adapter, "PDA EPAR Line ID Lookup Control", address);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PartDefaultSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsClearThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsClearThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, PwReorderingEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwReorderingIsEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelayIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, HwLimitNumPktForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelaySet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelayGet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeGet);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferIsEmpty);
        mMethodOverride(m_ThaModulePdaOverride, PwCwSequenceModeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLopsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxPacketsSentToTdmGet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterPayloadLengthSet);
        mMethodOverride(m_ThaModulePdaOverride, PwNumDs0TimeslotsSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitTypeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwDebug);
        mMethodOverride(m_ThaModulePdaOverride, PwAlarmGet);
        mMethodOverride(m_ThaModulePdaOverride, PwDefectHistoryGet);
        mMethodOverride(m_ThaModulePdaOverride, HotConfigureStart);
        mMethodOverride(m_ThaModulePdaOverride, HotConfigureStop);

        mMethodOverride(m_ThaModulePdaOverride, PwCenterJitterStart);
        mMethodOverride(m_ThaModulePdaOverride, PwCenterJitterStop);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferState);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferStateString);

        mMethodOverride(m_ThaModulePdaOverride, VcDeAssemblerSet);
        mMethodOverride(m_ThaModulePdaOverride, PwCepEquip);
        mMethodOverride(m_ThaModulePdaOverride, PwCepEbmEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwCircuitIdSet);
        mMethodOverride(m_ThaModulePdaOverride, BindLineToPw);
        mMethodOverride(m_ThaModulePdaOverride, PwTohByteEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwTohFirstByteEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwTohFirstByteGet);
        mMethodOverride(m_ThaModulePdaOverride, PwTohEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwTohBytesBitmapGet);

        mMethodOverride(m_ThaModulePdaOverride, PwIdleCodeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwIdleCodeGet);
        mMethodOverride(m_ThaModulePdaOverride, ModuleIdleCodeSet);
        mMethodOverride(m_ThaModulePdaOverride, ModuleIdleCodeGet);
        mMethodOverride(m_ThaModulePdaOverride, ModuleIdleCodeIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, AutoInsertRdiEnable);
        mMethodOverride(m_ThaModulePdaOverride, AutoInsertRdiIsEnabled);
        mMethodOverride(m_ThaModulePdaOverride, PwRegsShow);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsEnabled);

        /* Registers */
        mMethodOverride(m_ThaModulePdaOverride, PwPdaIdleCodeCtrl);
        mMethodOverride(m_ThaModulePdaOverride, PWPdaTdmModeCtrl);
        mMethodOverride(m_ThaModulePdaOverride, EparLineIdLkCtr);

        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeInPacketSet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeInPacketGet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelayInPacketSet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelayInPacketGet);
        mMethodOverride(m_ThaModulePdaOverride, NumberOfPacketByUs);
        mMethodOverride(m_ThaModulePdaOverride, UsByNumberOfPacket);
        mMethodOverride(m_ThaModulePdaOverride, CalculateNumPacketsByUsAndPayload);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModulePda(self);
    }

static void MethodsInit(ThaModulePdaV2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PwAlarmIntrStat);
        mMethodOverride(m_methods, PwAlarmCurrentStat);
        mMethodOverride(m_methods, NeedToCfgCircuitType);
        mMethodOverride(m_methods, SdhChannelOffset);
        mMethodOverride(m_methods, SdhChannelSliceOffset);
        mMethodOverride(m_methods, SdhChannelStsOffset);
        mMethodOverride(m_methods, CepFractionalIsSupported);

        mMethodOverride(m_methods, PDATdmJitBufStat);
        mBitFieldOverride(ThaModulePdaV2, m_methods, BufferNumPkt)
        mBitFieldOverride(ThaModulePdaV2, m_methods, BufferNumAdditionalBytes)

        mMethodOverride(m_methods, PWPdaJitBufCtrl);
        mBitFieldOverride(ThaModulePdaV2, m_methods, JitterBufferSizePkt)
        mBitFieldOverride(ThaModulePdaV2, m_methods, JitterBufferDelayPkt)

        mBitFieldOverride(ThaModulePdaV2, m_methods, PdaFirstStsId)
        mBitFieldOverride(ThaModulePdaV2, m_methods, PdaIdleCode)
        mBitFieldOverride(ThaModulePdaV2, m_methods, PdaNumNxDs0)
        mBitFieldOverride(ThaModulePdaV2, m_methods, AisRdiUneqOff)

        mMethodOverride(m_methods, HotConfigureEngineStatus);
        mMethodOverride(m_methods, HotConfigureEngineControl);
        mMethodOverride(m_methods, HotConfigureEnginePwId);
        mBitFieldOverride(ThaModulePdaV2, m_methods, HotConfigureEngineRequestInfoPwId)

        mMethodOverride(m_methods, JitterBufferRegShow);
        }

    mMethodsSet(self, &m_methods);
    }

AtModule ThaModulePdaV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((ThaModulePdaV2)self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModulePdaV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModulePdaV2ObjectInit(newModule, device);
    }

uint32 ThaModulePdaV2JitBufCtrlAddress(ThaModulePdaV2 self, AtPw pw)
    {
    uint32 address = mMethodsGet(mThis(self))->PWPdaJitBufCtrl(mThis(self)) + mPwOffset(self, pw);
    return address;
    }

eBool ThaModulePdaPwVariantJitterBufferSizeIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    return PwVariantJitterBufferSizeIsSupported(self, pw, microseconds);
    }

eBool ThaModulePdaPwVariantJitterBufferDelayIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    return PwVariantJitterBufferDelayIsSupported(self, pw, microseconds);
    }

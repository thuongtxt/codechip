/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ThaBerMeasureTimeHardEngine.c
 *
 * Created Date: Nov 13, 2017
 *
 * Description : BER-Detection and clearing time Measurement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/ber/AtModuleBerInternal.h"
#include "../../../generic/ber/AtBerMeasureTimeEngineInternal.h"
#include "../man/ThaDevice.h"
#include "ThaBerMeasureTime.h"
#include "ThaBerMeasureTimeReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaBerMeasureTimeHardEngine
    {
    tAtBerMeasureTimeHardEngine super;
    }tThaBerMeasureTimeHardEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerMeasureTimeEngineMethods m_AtBerMeasureOverride;

/* Save super implementation */
static const tAtBerMeasureTimeEngineMethods *m_AtBerMeasureMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumParts(AtModule self)
    {
    ThaDevice device = (ThaDevice) AtModuleDeviceGet((AtModule) self);
    return ThaDeviceNumPartsOfModule(device, cAtModuleBer);
    }

static uint32 PartOffset(AtModule self, uint8 partId)
    {
    ThaDevice device = (ThaDevice) AtModuleDeviceGet(self);
    return ThaDeviceModulePartOffset(device, cAtModuleBer, partId);
    }

static void MeasureTimeReset(AtModuleBer self, AtBerController controller)
    {
    uint8 partId;
    uint32 regAddress, regValue, offset;
    uint8 numParts = NumParts((AtModule) self);
    uint8 partAffect = AtBerControllerPartId(controller);

    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer) self);

    for (partId = 0; partId < numParts; partId++)
        {
        offset = baseAddress + PartOffset((AtModule) self, partId);

        /* clear Input Error */
        regAddress = cThaBerMeasureCtrlErrorInputBase + offset;
        regValue = mModuleHwRead(self, regAddress);
        mRegFieldSet(regValue, cThaBerMeasureCtrlErrorInputBaseStop, 1); /* Disable Input Error As Default */
        mRegFieldSet(regValue, cThaBerMeasureCtrlErrorDetect, (partId == partAffect) ? 1 : 0);
        mModuleHwWrite(self, regAddress, regValue);
        }
    }

static uint32 TimerRegisterBase(eAtBerTimerType type)
    {
    switch ((uint32) type)
        {
        case cAtBerTimerTypeSfDetection: return cThaBerMeasureSfDetectionTimerBase;
        case cAtBerTimerTypeSfClearing:  return cThaBerMeasureSfTimerBase;
        case cAtBerTimerTypeSdClearing:  return cThaBerMeasureSdTimerBase;
        case cAtBerTimerTypeTcaClearing: return cThaBerMeasureTcaTimerBase;
        default:                         return cInvalidUint32;
        }
    }

static void MeasureTimeErrorInputDisable(AtModuleBer self, uint8 partIdAffect, eBool disable)
    {
    uint8 partId;
    uint32 regAddress, regValue;
    uint8 numParts = NumParts((AtModule) self);
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer) self);

    for (partId = 0; partId < numParts; partId++)
        {
        regAddress = cThaBerMeasureCtrlErrorInputBase + baseAddress + PartOffset((AtModule) self, partId);
        regValue = mModuleHwRead(self, regAddress);
        if (partId == partIdAffect)
            {
            mRegFieldSet(regValue, cThaBerMeasureCtrlErrorInputBaseStop, disable ? 1 : 0);
            }
        else
            {
            mRegFieldSet(regValue, cThaBerMeasureCtrlErrorInputBaseStop, 0);
            }
        mModuleHwWrite(self, regAddress, regValue);
        }
    }

static void MeasureTimeInit(AtModuleBer self, AtBerController controller)
    {
    uint32 regAddress, regValue;
    uint32 hwChannelId = AtBerControllerMeasureEngineId(controller);
    uint8  partId = (uint8)AtBerControllerPartId(controller);
    uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer) self);
    uint32 offset = baseAddress + PartOffset((AtModule) self, partId);

    /* Clearing time */
    regAddress = cThaBerMeasureCtrlChannelBase + offset;
    regValue = mModuleHwRead(self, regAddress);
    mRegFieldSet(regValue, cThaBerMeasureCtrlChannelId, hwChannelId);
    mModuleHwWrite(self, regAddress, regValue);

    /* Detection time */
    regAddress = cThaBerMeasureCtrlErrorInputBase + offset;
    regValue = mModuleHwRead(self, regAddress);
    mRegFieldSet(regValue, cThaBerMeasureCtrlChannelId, hwChannelId);
    mModuleHwWrite(self, regAddress, regValue);
    }

static uint32 MeasureTimeLatch(AtModuleBer self, uint8 partIdAffect, uint32 base)
    {
    if (base != cInvalidUint32)
        {
        uint32 baseAddress = AtModuleBerBaseAddress((AtModuleBer) self);
        uint32 regAddr =  baseAddress + PartOffset((AtModule) self, partIdAffect) + base;
        uint32 hwValue = mModuleHwRead(self, regAddr);
        return (hwValue >> 3);
        }

    return 0;
    }

static eAtRet Init(AtBerMeasureTimeEngine self)
    {
	ThaBerMeasureTimeInit(self);
    return m_AtBerMeasureMethods->Init(self);
    }

static uint32 EstimatedTimeGet(AtBerMeasureTimeEngine self, eAtBerTimerType type)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    AtModuleBer berModule = (AtModuleBer)AtBerControllerModuleGet(controller);
    return MeasureTimeLatch(berModule, AtBerControllerPartId(controller), TimerRegisterBase(type));
    }

static eAtRet EstimatedTimeClear(AtBerMeasureTimeEngine self, eAtBerTimerType type)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    AtModuleBer berModule = (AtModuleBer)AtBerControllerModuleGet(controller);
    MeasureTimeLatch(berModule, AtBerControllerPartId(controller), TimerRegisterBase(type));
    return cAtOk;
    }

static eAtRet InputErrorConnect(AtBerMeasureTimeEngine self)
    {
    return ThaBerMeasureTimeErrorInputDisable(self, cAtFalse);
    }

static eAtRet InputErrorDisConnect(AtBerMeasureTimeEngine self)
    {
    return ThaBerMeasureTimeErrorInputDisable(self, cAtTrue);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaBerMeasureTimeHardEngine);
    }

static void OverrideAtBerMeasureTimeEngine(AtBerMeasureTimeEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerMeasureMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerMeasureOverride, m_AtBerMeasureMethods, sizeof(m_AtBerMeasureOverride));

        mMethodOverride(m_AtBerMeasureOverride, Init);
        mMethodOverride(m_AtBerMeasureOverride, EstimatedTimeGet);
        mMethodOverride(m_AtBerMeasureOverride, EstimatedTimeClear);
        mMethodOverride(m_AtBerMeasureOverride, InputErrorConnect);
        mMethodOverride(m_AtBerMeasureOverride, InputErrorDisConnect);
        }

    mMethodsSet(self, &m_AtBerMeasureOverride);
    }

static AtBerMeasureTimeEngine ObjectInit(AtBerMeasureTimeEngine self, AtBerController controller)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerMeasureTimeHardEngineObjectInit(self, controller) == NULL)
        return NULL;

    /* Setup methods */
    OverrideAtBerMeasureTimeEngine(self);
    m_methodsInit = 1;

    return self;
    }

AtBerMeasureTimeEngine ThaBerMeasureTimeHardEngineNew(AtBerController controller)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerMeasureTimeEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, controller);
    }

eAtRet ThaBerMeasureTimeErrorInputDisable(AtBerMeasureTimeEngine self, eBool disable)
    {
    if (self)
        {
        AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
        AtModuleBer berModule = (AtModuleBer)AtBerControllerModuleGet(controller);
        MeasureTimeErrorInputDisable(berModule, AtBerControllerPartId(controller), disable);
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

void ThaBerMeasureTimeInit(AtBerMeasureTimeEngine self)
    {
    if (self)
        {
        AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
        AtModuleBer berModule = (AtModuleBer)AtBerControllerModuleGet(controller);
        MeasureTimeReset(berModule, controller);
        MeasureTimeInit(berModule, controller);
        }
    }


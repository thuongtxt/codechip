/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60BerMeasureTime.h
 * 
 * Created Date: Nov 15, 2017
 *
 * Description : BER-Detection and clearing time Measurement
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THABERMEASURETIME_H_
#define _THABERMEASURETIME_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaBerMeasureTimeErrorInputDisable(AtBerMeasureTimeEngine self, eBool disable);
void ThaBerMeasureTimeInit(AtBerMeasureTimeEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _THABERMEASURETIME_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : ThaModuleBer.h
 * 
 * Created Date: Feb 6, 2013
 *
 * Description : Thalassa BER module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEBER_H_
#define _THAMODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ber/AtModuleSoftBer.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cThaModuleBerDefaultMaxNumEngines 400

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleHardBer *ThaModuleHardBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer ThaStmModuleBerNew(AtDevice device, uint32 maxNumControllers);

/* Concrete BER controllers */
AtBerController ThaSdhLineRsBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController ThaSdhLineMsBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController ThaSdhPathBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

AtBerController Tha60290022PdhDe3BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController Tha60290022PdhDe1BerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

/* Products */
AtModuleBer Tha60031031ModuleBerNew(AtDevice device);
AtModuleBer Tha60210011ModuleBerNew(AtDevice device);
AtModuleBer Tha60210021ModuleBerNew(AtDevice device);
AtModuleBer Tha60210031ModuleBerNew(AtDevice device);
AtModuleBer Tha60210051ModuleBerNew(AtDevice device);
AtModuleBer Tha60210061ModuleBerNew(AtDevice device);
AtModuleBer Tha60290022ModuleBerNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEBER_H_ */


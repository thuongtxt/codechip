/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ThaBerMeasureTimeSoftEngine.c
 *
 * Created Date: Nov 13, 2017
 *
 * Description : BER-Detection and clearing time Measurement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/ber/AtModuleBerInternal.h"
#include "../../../generic/ber/AtBerMeasureTimeEngineInternal.h"
#include "../man/ThaDevice.h"
#include "ThaBerMeasureTime.h"
#include "ThaBerMeasureTimeReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaBerMeasureTimeSoftEngine
    {
    tAtBerMeasureTimeSoftEngine super;
    }tThaBerMeasureTimeSoftEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerMeasureTimeEngineMethods m_AtBerMeasureOverride;

/* Save super implementation */
static const tAtBerMeasureTimeEngineMethods *m_AtBerMeasureMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtBerMeasureTimeEngine self)
    {
	ThaBerMeasureTimeInit(self);
    return m_AtBerMeasureMethods->Init(self);
    }

static eAtRet InputErrorConnect(AtBerMeasureTimeEngine self)
    {
    return ThaBerMeasureTimeErrorInputDisable(self, cAtFalse);
    }

static eAtRet InputErrorDisConnect(AtBerMeasureTimeEngine self)
    {
    return ThaBerMeasureTimeErrorInputDisable(self, cAtTrue);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaBerMeasureTimeSoftEngine);
    }

static void OverrideAtBerMeasureTimeEngine(AtBerMeasureTimeEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerMeasureMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerMeasureOverride, m_AtBerMeasureMethods, sizeof(m_AtBerMeasureOverride));

        mMethodOverride(m_AtBerMeasureOverride, Init);
        mMethodOverride(m_AtBerMeasureOverride, InputErrorConnect);
        mMethodOverride(m_AtBerMeasureOverride, InputErrorDisConnect);
        }

    mMethodsSet(self, &m_AtBerMeasureOverride);
    }

static AtBerMeasureTimeEngine ObjectInit(AtBerMeasureTimeEngine self, AtBerController controller)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerMeasureTimeSoftEngineObjectInit(self, controller) == NULL)
        return NULL;

    /* Setup methods */
    OverrideAtBerMeasureTimeEngine(self);
    m_methodsInit = 1;

    return self;
    }

AtBerMeasureTimeEngine ThaBerMeasureTimeSoftEngineNew(AtBerController controller)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerMeasureTimeEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, controller);
    }

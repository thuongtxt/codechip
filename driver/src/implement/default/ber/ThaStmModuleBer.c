/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ThaStmModuleBer.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : BER module for STM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/ThaDevice.h"
#include "ThaModuleBerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tThaStmModuleBer *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSoftBerMethods m_AtModuleSoftBerOverride;
static tAtModuleBerMethods     m_AtModuleBerOverride;

/* Save super implementation */
static const tAtModuleSoftBerMethods *m_AtModuleSoftBerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaStmModuleBer);
    }

static AtBerController SdhLineRsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    return ThaSdhLineRsBerSoftControllerNew(controllerId, line, self);
    }

static AtBerController SdhLineMsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    return ThaSdhLineMsBerSoftControllerNew(controllerId, line, self);
    }

static AtBerController SdhPathBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel path)
    {
    return (AtBerController)ThaSdhPathBerSoftControllerNew(controllerId, path, self);
    }

static eBool BerProcessCanRunWithTaskPeriod(AtModuleSoftBer self, uint32 periodInMs)
    {
    /* When simulate, let it run with any period */
    if (AtDeviceIsSimulated(AtModuleDeviceGet((AtModule)self)))
        return cAtTrue;

    return m_AtModuleSoftBerMethods->BerProcessCanRunWithTaskPeriod(self, periodInMs);
    }

static void OverrideAtModuleSoftBer(AtModuleBer self)
    {
    AtModuleSoftBer softBerModule = (AtModuleSoftBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSoftBerMethods = mMethodsGet(softBerModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSoftBerOverride, m_AtModuleSoftBerMethods, sizeof(m_AtModuleSoftBerOverride));

        mMethodOverride(m_AtModuleSoftBerOverride, BerProcessCanRunWithTaskPeriod);
        }

    mMethodsSet(softBerModule, &m_AtModuleSoftBerOverride);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(self), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, SdhLineRsBerControlerObjectCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerObjectCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhPathBerControlerObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleSoftBer(self);
    OverrideAtModuleBer(self);
    }

AtModuleBer ThaStmModuleBerObjectInit(AtModuleBer self, AtDevice device, uint32 maxNumControllers)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleSoftBerObjectInit((AtModuleSoftBer)self, device, maxNumControllers) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer ThaStmModuleBerNew(AtDevice device, uint32 maxNumControllers)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newBerModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBerModule == NULL)
        return NULL;

    /* Construct it */
    return ThaStmModuleBerObjectInit(newBerModule, device, maxNumControllers);
    }

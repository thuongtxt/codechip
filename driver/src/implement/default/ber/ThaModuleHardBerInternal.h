/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : ThaModuleHardBerInternal.h
 * 
 * Created Date: Feb 6, 2018
 *
 * Description : Default hardware-based BER module.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEHARDBERINTERNAL_H_
#define _THAMODULEHARDBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ber/AtModuleBerInternal.h"
#include "ThaModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleHardBerMethods
    {
    /* BER controller factory */
    AtBerController (*AuVcBerControllerCreate)(ThaModuleHardBer self, AtSdhChannel auVc);
    AtBerController (*Tu3VcBerControllerCreate)(ThaModuleHardBer self, AtSdhChannel tu3Vc);
    AtBerController (*Vc1xBerControllerCreate)(ThaModuleHardBer self, AtSdhChannel vc1x);
    AtBerController (*De1PathBerControllerCreate)(ThaModuleHardBer self, AtPdhChannel de1);
    AtBerController (*De1LineBerControllerCreate)(ThaModuleHardBer self, AtPdhChannel de1);
    AtBerController (*De3PathBerControllerCreate)(ThaModuleHardBer self, AtPdhChannel de3);
    AtBerController (*De3LineBerControllerCreate)(ThaModuleHardBer self, AtPdhChannel de3);
    }tThaModuleHardBerMethods;

typedef struct tThaModuleHardBer
    {
    tAtModuleBer super;
    const tThaModuleHardBerMethods *methods;
    }tThaModuleHardBer;

/*--------------------------- Forward declarations ---------------------------*/
AtModuleBer ThaModuleHardBerObjectInit(AtModuleBer self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEHARDBERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : ThaBerMeasureTimeReg.h
 * 
 * Created Date: Nov 13, 2017
 *
 * Description : BER-Detection and clearing time Measurement
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THABERMEASURETIMEREG_H_
#define _THABERMEASURETIMEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Control set ID */
#define cThaBerMeasureCtrlChannelBase                      0x60003
#define cThaBerMeasureCtrlChannelIdMask                    cBit14_0
#define cThaBerMeasureCtrlChannelIdShift                   0

/* Control set ID and InputError */
#define cThaBerMeasureCtrlErrorInputBase                   0x60013
#define cThaBerMeasureCtrlErrorInputBaseStopMask           cBit20
#define cThaBerMeasureCtrlErrorInputBaseStopShift          20
#define cThaBerMeasureCtrlErrorDetectMask                  cBit18
#define cThaBerMeasureCtrlErrorDetectShift                 18

/* Read timer from hardware */
#define cThaBerMeasureSfTimerBase                          0x60032
#define cThaBerMeasureSdTimerBase                          0x60033
#define cThaBerMeasureTcaTimerBase                         0x60034
#define cThaBerMeasureSfDetectionTimerBase                 0x60012

#ifdef __cplusplus
}
#endif
#endif /* _THABERMEASURETIMEREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ThaSdhPathBerController.c
 *
 * Created Date: Feb 20, 2013
 *
 * Description : Path BER soft controller for Thalassa product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtSdhPath.h"
#include "../../../generic/ber/AtSdhPathBerSoftControllerInternal.h"
#include "../../../generic/common/AtChannelInternal.h"
#include "ThaModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaSdhPathBerController * ThaSdhPathBerController;

typedef struct tThaSdhPathBerController
    {
    tAtSdhPathBerSoftController super;

    /* Private data */
    uint32 bipErrorCounter;
    }tThaSdhPathBerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerSoftControllerMethods m_AtBerSoftControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhPathBerController);
    }

/* The previous version use a utility function to handle rolling logic, but
 * doing so make switching context happen lot of times when BER monitoring task
 * is running and many channels are monitored. The following logic may be
 * duplicate but let's accept that to reduce CPU usage. */
static uint32 ChannelBitErrorGet(AtBerSoftController self)
    {
    ThaSdhPathBerController controller = (ThaSdhPathBerController)self;
    AtChannel channel = ((AtBerController)self)->monitoredChannel;
    uint32 newCounter = mMethodsGet(channel)->ReadOnlyCntGet(channel, cAtSdhPathCounterTypeBip);
    uint32 resultCounterValue;
    eBool isRolled = (newCounter < controller->bipErrorCounter) ? cAtTrue : cAtFalse;

    if (isRolled)
        resultCounterValue = newCounter + (cBit23_0 - controller->bipErrorCounter) + 1;
    else
        resultCounterValue = newCounter - controller->bipErrorCounter;

    controller->bipErrorCounter = newCounter;

    return resultCounterValue;
    }

static void OverrideAtBerSoftController(AtBerController self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerSoftControllerOverride, mMethodsGet(softController), sizeof(m_AtBerSoftControllerOverride));
        mMethodOverride(m_AtBerSoftControllerOverride, ChannelBitErrorGet);
        }

    mMethodsSet(softController, &m_AtBerSoftControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerSoftController(self);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhPathBerSoftControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController ThaSdhPathBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

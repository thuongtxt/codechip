/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ThaSdhLineRsBerController.c
 *
 * Created Date: Feb 20, 2013
 *
 * Description : Line BER soft controller for RS layer of Thalassa product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtSdhLine.h"
#include "../../../generic/ber/AtSdhLineBerSoftControllerInternal.h"
#include "../../../generic/common/AtChannelInternal.h"
#include "../sdh/ThaModuleSdh.h"
#include "../sdh/ThaSdhLine.h"
#include "ThaModuleBer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaSdhLineRsBerController * ThaSdhLineRsBerController;

typedef struct tThaSdhLineRsBerController
    {
    tAtSdhLineRsBerSoftController super;

    /* Private data */
    uint32 b1ErrorCounter;
    }tThaSdhLineRsBerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerSoftControllerMethods m_AtBerSoftControllerOverride;

/* Save super implementation */
static const tAtBerSoftControllerMethods *m_AtBerSoftControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaSdhLineRsBerController);
    }

/* The previous version use a utility function to handle rolling logic, but
 * doing so make switching context happen lot of times when BER monitoring task
 * is running and many channels are monitored. The following logic may be
 * duplicate but let's accept that to reduce CPU usage. */
static uint32 ChannelBitErrorGet(AtBerSoftController self)
    {
    uint32 resultCounterValue;
    uint32 newCounters;
    ThaSdhLineRsBerController controller = (ThaSdhLineRsBerController)self;
    AtChannel channel = AtBerControllerMonitoredChannel((AtBerController)self);
    if (channel == NULL)
        return 0;

    newCounters = mMethodsGet(channel)->ReadOnlyCntGet(channel, cAtSdhLineCounterTypeB1);
    if (newCounters < controller->b1ErrorCounter)
        resultCounterValue = newCounters + (cBit22_0 - controller->b1ErrorCounter) + 1;
    else
        resultCounterValue = newCounters - controller->b1ErrorCounter;

    controller->b1ErrorCounter = newCounters;

    return resultCounterValue;
    }

static void DefectChanged(AtBerSoftController self, uint32 changedAlarms, uint32 currentStatus)
    {
    ThaSdhLine   line      = (ThaSdhLine)AtBerControllerMonitoredChannel((AtBerController)self);
    ThaModuleSdh sdhModule = (ThaModuleSdh)AtChannelModuleGet((AtChannel)line);

    if (ThaModuleSdhBerHardwareInterruptIsSupported(sdhModule))
        ThaSdhLineBerStatusLatch(line, changedAlarms, currentStatus);
    else
        m_AtBerSoftControllerMethods->DefectChanged(self, changedAlarms, currentStatus);
    }

static uint32 BerCurrentDefectGet(AtBerSoftController self)
    {
    return ThaSdhLineBerDefectGet((ThaSdhLine)AtBerControllerMonitoredChannel((AtBerController)self));
    }

static void OverrideAtBerSoftController(AtBerController self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerSoftControllerMethods = mMethodsGet(softController);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerSoftControllerOverride, m_AtBerSoftControllerMethods, sizeof(m_AtBerSoftControllerOverride));
        mMethodOverride(m_AtBerSoftControllerOverride, ChannelBitErrorGet);
        mMethodOverride(m_AtBerSoftControllerOverride, DefectChanged);
        mMethodOverride(m_AtBerSoftControllerOverride, BerCurrentDefectGet);
        }

    mMethodsSet(softController, &m_AtBerSoftControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerSoftController(self);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhLineRsBerSoftControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController ThaSdhLineRsBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

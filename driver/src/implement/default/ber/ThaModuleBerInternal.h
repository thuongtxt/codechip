/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : ThaModuleBerInternal.h
 * 
 * Created Date: Jul 18, 2013
 *
 * Description : Thalassa BER module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULEBERINTERNAL_H_
#define _THAMODULEBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/ber/AtModuleSoftBerInternal.h"
#include "ThaModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaStmModuleBer
    {
    tAtModuleSoftBer super;
    }tThaStmModuleBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer ThaStmModuleBerObjectInit(AtModuleBer self, AtDevice device, uint32 maxNumControllers);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEBERINTERNAL_H_ */


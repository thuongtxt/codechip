/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : ThaModuleHardBer.c
 *
 * Created Date: Feb 6, 2018
 *
 * Description : Default hardware-based BER module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../generic/sdh/AtSdhChannelInternal.h"
#include "../man/ThaDeviceInternal.h"
#include "ThaModuleHardBerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleHardBer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleHardBerMethods m_methods;

/* Override */
static tAtModuleBerMethods  m_AtModuleBerOverride;

/* Cache super */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtBerController SdhPathBerControlerCreate(AtModuleBer self, AtChannel path)
    {
    AtSdhChannel vc = (AtSdhChannel)path;
    AtSdhChannel parent = AtSdhChannelParentChannelGet((AtSdhChannel)path);

    if (AtSdhChannelIsHoVc(vc))
        return mMethodsGet(mThis(self))->AuVcBerControllerCreate(mThis(self), (AtSdhChannel)path);

    if ((AtSdhChannelTypeGet(vc) == cAtSdhChannelTypeVc3) && (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3))
        return mMethodsGet(mThis(self))->Tu3VcBerControllerCreate(mThis(self), (AtSdhChannel)path);

    if (AtSdhChannelIsVc(vc))
        return mMethodsGet(mThis(self))->Vc1xBerControllerCreate(mThis(self), (AtSdhChannel)path);

    return NULL;
    }

static AtBerController AuVcBerControllerCreate(ThaModuleHardBer self, AtSdhChannel auVc)
    {
    AtUnused(self);
    AtUnused(auVc);
    return NULL;
    }

static AtBerController Tu3VcBerControllerCreate(ThaModuleHardBer self, AtSdhChannel tu3Vc)
    {
    AtUnused(self);
    AtUnused(tu3Vc);
    return NULL;
    }

static AtBerController Vc1xBerControllerCreate(ThaModuleHardBer self, AtSdhChannel vc1x)
    {
    AtUnused(self);
    AtUnused(vc1x);
    return NULL;
    }

static void ControllerDelete(AtModuleBer self, AtBerController controller)
    {
    AtBerMeasureTimeEngine engine = AtModuleBerMeasureTimeEngineGet(self);
    if (AtBerMeasureTimeEngineControllerGet(engine) == controller)
        AtModuleBerMeasureTimeEngineDeleteNoLock(self, engine);

    if (!AtDeviceIsDeleting(AtModuleDeviceGet((AtModule)self)))
        AtBerControllerEnable(controller, cAtFalse);

    AtObjectDelete((AtObject)controller);
    }

static AtBerController PdhChannelPathBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet((AtPdhChannel)pdhChannel);

    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        return mMethodsGet(mThis(self))->De1PathBerControllerCreate(mThis(self), (AtPdhChannel)pdhChannel);

    if ((channelType == cAtPdhChannelTypeE3) || (channelType == cAtPdhChannelTypeDs3))
        return mMethodsGet(mThis(self))->De3PathBerControllerCreate(mThis(self), (AtPdhChannel)pdhChannel);

    return NULL;
    }

static AtBerController De1PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return NULL;
    }

static AtBerController De1LineBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return NULL;
    }

static AtBerController PdhChannelLineBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet((AtPdhChannel)pdhChannel);

    if ((channelType == cAtPdhChannelTypeE1) || (channelType == cAtPdhChannelTypeDs1))
        return mMethodsGet(mThis(self))->De1LineBerControllerCreate(mThis(self), (AtPdhChannel)pdhChannel);

    if ((channelType == cAtPdhChannelTypeE3) || (channelType == cAtPdhChannelTypeDs3))
        return mMethodsGet(mThis(self))->De3LineBerControllerCreate(mThis(self), (AtPdhChannel)pdhChannel);

    return NULL;
    }

static AtBerController De3PathBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static AtBerController De3LineBerControllerCreate(ThaModuleHardBer self, AtPdhChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static uint32 BaseAddress(AtModuleBer self)
    {
    ThaModulePoh modulePoh = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    return ThaModulePohBaseAddress(modulePoh);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(self), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, SdhPathBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, ControllerDelete);
        mMethodOverride(m_AtModuleBerOverride, PdhChannelPathBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, PdhChannelLineBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, BaseAddress);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleBer(self);
    }

static void MethodsInit(AtModuleBer self)
    {
    ThaModuleHardBer module = mThis(self);
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, AuVcBerControllerCreate);
        mMethodOverride(m_methods, Tu3VcBerControllerCreate);
        mMethodOverride(m_methods, Vc1xBerControllerCreate);
        mMethodOverride(m_methods, De1PathBerControllerCreate);
        mMethodOverride(m_methods, De1LineBerControllerCreate);
        mMethodOverride(m_methods, De3PathBerControllerCreate);
        mMethodOverride(m_methods, De3LineBerControllerCreate);
        }

    mMethodsSet(module, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleHardBer);
    }

AtModuleBer ThaModuleHardBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }


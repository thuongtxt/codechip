/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaModuleClaAsyncInit.c
 *
 * Created Date: Aug 19, 2016
 *
 * Description : AsyncInit implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleClaInternal.h"
#include "ppp/ThaModuleClaPppReg.h"
#include "ThaModuleClaReg.h"
#include "../eth/ThaModuleEth.h"
#include "../util/ThaUtil.h"
#include "controllers/ThaClaEthFlowController.h"
#include "controllers/ThaClaEthPortController.h"
#include "hbce/ThaHbce.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleCla)self)
#define mClaEthFlowController(self) ThaModuleClaEthFlowControllerObjectGet((ThaModuleCla)self)
#define mClaPwController(self) ThaModuleClaPwControllerGet((ThaModuleCla)self)
#define mClaEthPortController(self) ThaModuleClaEthPortControllerGet((ThaModuleCla)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eThaModuleClaAsyncInitState
    {
    cThaModuleClaAsyncInitStateSuperInit = 0,
    cThaModuleClaAsyncInitStateEthFlowControllerInit,
    cThaModuleClaAsyncInitStatePwControllerInit,
    cThaModuleClaAsyncInitStateEthPortController
    }eThaModuleClaAsyncInitState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncStateSet(ThaModuleCla self, uint32 state)
    {
    self->asyncInitState = state;
    }

static uint32 AsyncStateGet(ThaModuleCla self)
    {
    return self->asyncInitState;
    }

eAtRet ThaModuleClaAsyncInitMain(ThaModuleCla self)
    {
    uint32 state = AsyncStateGet(self);
    eAtRet ret = cAtOk;

    switch (state)
        {
        case cThaModuleClaAsyncInitStateSuperInit:
            ret = ThaModuleClaSuperAsyncInit((AtModule)self);
            if (ret == cAtOk)
                {
                state = cThaModuleClaAsyncInitStateEthFlowControllerInit;
                ret = cAtErrorAgain;
                }
            break;
        case cThaModuleClaAsyncInitStateEthFlowControllerInit:
            if (mClaEthFlowController(self) != NULL)
                {
                ret = ThaClaControllerAsyncInit((ThaClaController)mClaEthFlowController(self));
                if (ret == cAtOk)
                    {
                    state = cThaModuleClaAsyncInitStatePwControllerInit;
                    ret = cAtErrorAgain;
                    }
                else if (!AtDeviceAsyncRetValIsInState(ret))
                    state = cThaModuleClaAsyncInitStateSuperInit;
                }
            else
                {
                state = cThaModuleClaAsyncInitStatePwControllerInit;
                ret = cAtErrorAgain;
                }
            break;
        case cThaModuleClaAsyncInitStatePwControllerInit:
            if (mClaPwController(self) != NULL)
                {
                ret = ThaClaControllerAsyncInit((ThaClaController)mClaPwController(self));
                if (ret == cAtOk)
                    {
                    state = cThaModuleClaAsyncInitStateEthPortController;
                    ret = cAtErrorAgain;
                    }
                else if (!AtDeviceAsyncRetValIsInState(ret))
                    state = cThaModuleClaAsyncInitStateSuperInit;
                }
            else
                {
                state = cThaModuleClaAsyncInitStateEthPortController;
                ret = cAtErrorAgain;
                }
            break;
        case cThaModuleClaAsyncInitStateEthPortController:
            if (mClaEthPortController(self) != NULL)
                {
                ret = ThaClaControllerAsyncInit((ThaClaController)mClaEthPortController(self));
                if (ret == cAtOk || !AtDeviceAsyncRetValIsInState(ret))
                    state = cThaModuleClaAsyncInitStateSuperInit;
                }
            else
                {
                state = cThaModuleClaAsyncInitStateSuperInit;
                ret = cAtOk;
                }
            break;
        default:
            state = cThaModuleClaAsyncInitStateSuperInit;
            ret = cAtErrorDevFail;
        }

    AsyncStateSet(self, state);
    return ret;
    }

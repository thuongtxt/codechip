/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA (internal module)
 * 
 * File        : ThaModuleCla.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLA_H_
#define _THAMODULECLA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../eth/ThaEthPort.h"
#include "../eth/ThaEthFlow.h"
#include "../pw/ThaModulePw.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleCla * ThaModuleCla;
typedef struct tThaModuleClaPpp * ThaModuleClaPpp;

typedef struct tThaClaController     * ThaClaController;
typedef struct tThaClaPwController   * ThaClaPwController;
typedef struct tThaClaPwControllerV2 * ThaClaPwControllerV2;
typedef struct tThaClaEthPortController *ThaClaEthPortController;
typedef struct tThaClaEthFlowController *ThaClaEthFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/* Concrete CLAs */
AtModule ThaModuleClaPppNew(AtDevice device);
AtModule ThaModuleClaPwNew(AtDevice device);
AtModule ThaModuleClaPwV2New(AtDevice device);
AtModule ThaModuleClaStmPwV2New(AtDevice device);

/* APIs */
eAtRet ThaModuleClaAllTrafficsDeactivate(ThaModuleCla self);

/* PPP/MLPPP feature */
eAtRet ThaModuleClaPppVlanLookupCtrl(ThaModuleCla self, ThaEthFlow flow, uint16 lkType, uint32 channelId, uint32 vlanId, eBool  enable);
eAtRet ThaModuleClaFlowLookupCtrl(ThaModuleCla self, ThaEthFlow flow, uint32 hwFlowId, uint32 classId, uint32 queueId, uint32 vlanOffSet, eBool enable);
uint8 ThaModuleClaPppVlanLookupModeGet(ThaModuleCla self, ThaEthFlow flow);
uint32 ThaModuleClaPppMaxNumVlans(ThaModuleCla self);
eAtRet ThaModuleClaEthPortMacCheckEnable(ThaModuleCla self, AtEthPort port, eBool enable);
eBool ThaModuleClaEthPortMacCheckIsEnabled(ThaModuleCla self, AtEthPort port);
eAtRet ThaModuleClaPppPsnToTdmEthTypeSet(ThaModuleCla self, uint32 entryIndex, uint32 ethType);
uint32 ThaModuleClaPppPsnToTdmEthTypeGet(ThaModuleCla self, uint32 entryIndex);

eAtRet ThaModuleClaEthPortMacGet(ThaModuleCla self, AtEthPort port, uint8 *macAddr);
eAtRet ThaModuleClaEthPortMacSet(ThaModuleCla self, AtEthPort port, uint8 *macAddr);

/* Counters */
uint32 ThaModuleClaIncomPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaIncombyteRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktBusErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktFcsErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktoversizeRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktundersizeRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktLensmallerthan64bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktLenfrom65to127bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktLenfrom128to255bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktLenfrom256to511bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktLenfrom512to1024bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktLenfrom1025to1528bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktJumboLenRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaPWunSuppedRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaHCBElookupnotmatchPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktDscdRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaPauFrmRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaPauFrmErrPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaBcastRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMcastRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaARPRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthOamRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthOamRxErrPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthOamtype0RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthOamtype1RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaIPv4PktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaICMPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaIPv6PktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaICMPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMEFRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMEFErrRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMPLSRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMPLSErrRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMPLSDataRxPktwithPHPMdRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMPLSDataRxPktwithoutPHPMdRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaLDPIPv4withPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaLDPIPv6withPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMPLSoverIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaMPLSoverIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaL2TPPktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaL2TPIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaL2TPIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaUDPPktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaUDPIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaUDPIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktForwardtoCpuRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktForwardtoPDaRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktTimePktToPRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaRxHdrErrPacketsRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaRxErrPsnPacketsRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaEthPktLenfrom1529to2047bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaUnicastRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);
uint32 ThaModuleClaPhysicalErrorRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c);

/* Register iterators */
AtIterator ThaModuleClaPppRegisterIteratorCreate(AtModule module);

uint8 ThaModuleClaNumParts(ThaModuleCla self);
uint32 ThaModuleClaPartOffset(ThaModuleCla self, uint8 partId);
eAtRet ThaModuleClaPwPsnHeaderUpdate(ThaModuleCla self, AtPw adapter);
eBool ThaModuleClaShouldBalanceCrcPolynomialsUsage(ThaModuleCla self);

eAtRet ThaModuleClaEthPortMaxPacketSizeSet(ThaModuleCla self, AtEthPort port, uint32 maxPacketSize);
uint32 ThaModuleClaEthPortMaxPacketSizeGet(ThaModuleCla self, AtEthPort port);
eAtRet ThaModuleClaEthPortMinPacketSizeSet(ThaModuleCla self, AtEthPort port, uint32 minPacketSize);
uint32 ThaModuleClaEthPortMinPacketSizeGet(ThaModuleCla self, AtEthPort port);

uint16 ThaModuleClaEthPortSVlanTpid(ThaModuleCla self, AtEthPort port);
eBool ThaModuleClaPwRxDuplicatedPacketsIsSupported(ThaModuleCla self);

/* Access controllers */
ThaClaPwController ThaModuleClaPwControllerGet(ThaModuleCla self);
ThaClaEthFlowController ThaModuleClaEthFlowControllerGet(ThaModuleCla self);
ThaClaEthPortController ThaModuleClaEthPortControllerGet(ThaModuleCla self);

eAtRet ThaModuleClaEthPortIpV4Set(ThaModuleCla self, AtEthPort port, uint8 *address);
eAtRet ThaModuleClaEthPortIpV4Get(ThaModuleCla self, AtEthPort port, uint8 *address);
eAtRet ThaModuleClaEthPortIpV6Set(ThaModuleCla self, AtEthPort port, uint8 *address);
eAtRet ThaModuleClaEthPortIpV6Get(ThaModuleCla self, AtEthPort port, uint8 *address);

/* For debugging */
void ThaModuleClaPwRegsShow(AtPw pw);
void ThaModuleClaEthPortRegShow(AtEthPort port);
void ThaModuleClaEthFlowRegsShow(AtEthFlow flow);
void ThaModuleClaRegDisplay(AtPw pw, const char* regName, uint32 address);
void ThaModuleClaLongRegDisplay(AtPw pw, const char* regName, uint32 address);
eAtRet ThaModuleClaSubPortVlanCheckingEnable(ThaModuleCla self, eBool enable);
eBool  ThaModuleClaSubPortVlanCheckingIsEnabled(ThaModuleCla self);
eBool ThaModuleClaCLAHBCEHashingTabCtrlIsUsed(ThaModuleCla self);

/* CES with CAS */
eAtRet ThaModuleClaCESoPCASLengthSet(ThaModuleCla self, AtPw pw, uint8 casLen);

/* Warm restore */
ThaPwAdapter ThaModuleClaHbcePwFromFlowId(ThaModuleCla self, uint32 flowId);

/* Product concretes */
AtModule Tha60030022ModuleClaPppNew(AtDevice device);
AtModule Tha60030051ModuleClaPppNew(AtDevice device);
AtModule Tha60031032ModuleClaPppNew(AtDevice device);
AtModule Tha60091132ModuleClaPppNew(AtDevice device);
AtModule Tha60030101ModuleClaPppNew(AtDevice device);
AtModule Tha60030011ModuleClaNew(AtDevice device);
AtModule Tha60030080ModuleClaNew(AtDevice device);
AtModule Tha60000031ModuleClaNew(AtDevice device);
AtModule Tha60031021ModuleClaNew(AtDevice device);
AtModule Tha60091023ModuleClaNew(AtDevice device);
AtModule Tha60031031ModuleClaNew(AtDevice device);
AtModule Tha60150011ModuleClaNew(AtDevice device);
AtModule Tha60031033ModuleClaNew(AtDevice device);
AtModule Tha60210011ModuleClaNew(AtDevice device);
AtModule Tha60210021ModuleClaNew(AtDevice device);
AtModule Tha60210031ModuleClaNew(AtDevice device);
AtModule Tha60220031ModuleClaNew(AtDevice device);
AtModule Tha60210051ModuleClaNew(AtDevice device);
AtModule Tha60290021ModuleClaNew(AtDevice device);
AtModule Tha60210012ModuleClaNew(AtDevice device);
AtModule Tha60290011ModuleClaNew(AtDevice device);
AtModule Tha6A290011ModuleClaNew(AtDevice device);
AtModule Tha60290022ModuleClaNew(AtDevice device);
AtModule Tha60290022ModuleClaV3New(AtDevice device);
AtModule ThaStmPwProductModuleClaNew(AtDevice device);
AtModule ThaPdhPwProductModuleClaNew(AtDevice device);
AtModule Tha60291011ModuleClaNew(AtDevice device);
AtModule Tha60291022ModuleClaNew(AtDevice device);
AtModule Tha60290081ModuleClaNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLA_H_ */


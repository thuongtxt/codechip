/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaHbceEntity.c
 *
 * Created Date: Mar 6, 2013
 *
 * Description : HBCE top class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaHbceEntityInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mEntityIsValid(self) (self ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaHbceEntityMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHbceEntity object = (ThaHbceEntity)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(claModule);
    }

static void OverrideAtObject(ThaHbceEntity self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaHbceEntity self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHbceEntity);
    }

static void Debug(ThaHbceEntity self)
    {
    const char *description = AtObjectToString((AtObject)self);
    if (description == NULL)
        return;

    AtPrintc(cSevNormal, "%s", description);
    }

static eAtRet Init(ThaHbceEntity self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static uint32 Restore(ThaHbceEntity self, uint8 page)
    {
    AtUnused(self);
    AtUnused(page);
    return 0;
    }

static eAtRet AsyncInit(ThaHbceEntity self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eBool ShouldOptimizeInit(ThaHbceEntity self)
    {
    /* Concrete should determine */
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(ThaHbceEntity self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, AsyncInit);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, Restore);
        mMethodOverride(m_methods, ShouldOptimizeInit);
        }

    mMethodsSet(self, &m_methods);
    }

ThaHbceEntity ThaHbceEntityObjectInit(ThaHbceEntity self, AtModule claModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* And private data */
    self->claModule = claModule;

    return self;
    }

eAtRet ThaHbceEntityInit(ThaHbceEntity self)
    {
    if (mEntityIsValid(self))
        return mMethodsGet(self)->Init(self);

    return cAtError;
    }

eAtRet ThaHbceEntityAsyncInit(ThaHbceEntity self)
    {
    if (mEntityIsValid(self))
        return mMethodsGet(self)->AsyncInit(self);

    return cAtError;
    }

AtModule ThaHbceEntityClaModuleGet(ThaHbceEntity self)
    {
    return self ? self->claModule : NULL;
    }

void ThaHbceEntityDebug(ThaHbceEntity self)
    {
    if (mEntityIsValid(self))
        mMethodsGet(self)->Debug(self);
    }

uint32 ThaHbceEntityRestore(ThaHbceEntity self, uint8 page)
    {
    if (self)
        return mMethodsGet(self)->Restore(self, page);
    return 0;
    }

eBool ThaHbceEntityShouldOptimizeInit(ThaHbceEntity self)
    {
    if (self)
        return mMethodsGet(self)->ShouldOptimizeInit(self);
    return cAtFalse;
    }

eBool ThaHbceEntityInAccessible(ThaHbceEntity self)
    {
    return AtModuleInAccessible(ThaHbceEntityClaModuleGet(self));
    }

eBool ThaHbceEntityAccessible(ThaHbceEntity self)
    {
    return AtModuleAccessible(ThaHbceEntityClaModuleGet(self));
    }

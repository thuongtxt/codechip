/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaHbce.c
 *
 * Created Date: Mar 6, 2013
 *
 * Description : Hash Base Classification Engine
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPsn.h"

#include "../../man/ThaDeviceInternal.h"
#include "../../pw/ThaPwInternal.h"
#include "../pw/ThaModuleClaPwReg.h"
#include "../pw/ThaModuleClaPwInternal.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"
#include "../controllers/ThaClaPwControllerInternal.h"

#include "ThaHbce.h"
#include "ThaHbceEntry.h"
#include "ThaHbceEntityInternal.h"
#include "ThaHbceMemoryPool.h"
#include "ThaHbceMemoryCell.h"
#include "ThaHbceMemoryCellContent.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mInAccessible(self) ThaHbceEntityInAccessible((ThaHbceEntity)self)
#define mHbceIsValid(self) (self ? cAtTrue : cAtFalse)
#define mFieldMask(module, field)                                              \
        mMethodsGet(module)->field##Mask(module)
#define mFieldShift(module, field)                                             \
        mMethodsGet(module)->field##Shift(module)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaHbceMethods m_methods;

/* Override */
static tThaHbceEntityMethods m_ThaHbceEntityOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHbce object = (ThaHbce)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(core);
    mEncodeUInt(identifier);
    mEncodeObjects(entries, mMethodsGet(object)->MaxNumEntries(object));
    mEncodeObject(memoryPool);
    mEncodeNone(registersCache);
    mEncodeUInt(hwAsyncInitState);
    mEncodeUInt(asyncInitState);
    mEncodeNone(standby);
    }

static const char *ToString(AtObject self)
    {
    static char buffer[16];
    ThaHbce hbce = (ThaHbce)self;
    AtSnprintf(buffer, sizeof(buffer) - 1, "hbce_%u", hbce->identifier + 1);
    return buffer;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHbce);
    }

static uint32 MaxNumEntries(ThaHbce self)
    {
	AtUnused(self);
    return 1024;
    }

static ThaHbceEntry EntryObjectCreate(ThaHbce self, uint32 entryIndex)
    {
    return ThaHbceEntryNew(self, entryIndex);
    }

static ThaHbceMemoryPool MemoryPoolCreate(ThaHbce self)
    {
    return ThaHbceMemoryPoolNew(self);
    }

static eAtRet AllEntriesCreate(ThaHbce self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 maxNumEntries = mMethodsGet(self)->MaxNumEntries(self);
    uint32 i;
    ThaHbceEntry *newEntries;
    eAtRet ret = cAtOk;

    /* Allocate the whole memory to hold all entries */
    newEntries = mMethodsGet(osal)->MemAlloc(osal, sizeof(ThaHbceEntry) * maxNumEntries);
    if (newEntries == NULL)
        return cAtErrorRsrcNoAvail;

    /* Create all entries */
    for (i = 0; i < maxNumEntries; i++)
        {
        newEntries[i] = mMethodsGet(self)->EntryObjectCreate(self, i);

        /* Remove all created entries if the new one cannot be created */
        if (newEntries[i] == NULL)
            {
            uint32 j;

            for (j = 0; j < i; j++)
                AtObjectDelete((AtObject)newEntries[j]);
            mMethodsGet(osal)->MemFree(osal, newEntries);

            return cAtErrorRsrcNoAvail;
            }

        /* Initialize this entry */
        ret |= ThaHbceEntityInit((ThaHbceEntity)newEntries[i]);
        }

    self->entries = newEntries;

    return ret;
    }

static eAtRet CellContentRemove(ThaHbce self, ThaHbceMemoryCellContent cellContent, ThaPwHeaderController controller)
    {
    eAtRet ret = cAtOk;
    ThaHbceEntry hashEntry = ThaHbceMemoryCellContentHashEntryGet(cellContent);
    ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(cellContent);

    ret |= ThaHbceEntryCellContentRemove(hashEntry, cellContent);
    if (ret != cAtOk)
        return ret;

    /* Then release it */
    ret |= ThaHbceMemoryPoolUnUseCell(self->memoryPool, cell);

    /* Reset cell content */
    if (controller)
        ret |= ThaPwHeaderControllerHbceMemoryCellContentSet(controller, NULL);
    ret |= ThaHbceMemoryCellContentPwHeaderControllerSet(cellContent, NULL);

    return ret;
    }

static eBool DeviceIsDeleting(ThaHbce self)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    AtDevice device = AtModuleDeviceGet(claModule);
    return AtDeviceIsDeleting(device);
    }

static eAtRet RemoveAllPwsAtEntry(ThaHbce self, ThaHbceEntry entry)
    {
    eAtRet ret = cAtOk;
    uint32 numFlows = ThaHbceEntryNumFlowsGet(entry);
    eBool deviceDeleting = DeviceIsDeleting(self);
    eBool accessible =  ThaHbceEntityAccessible((ThaHbceEntity)self);

    while (numFlows)
        {
        ThaHbceMemoryCellContent cellContent = ThaHbceEntryCellContentAtIndex(entry, 0);
        uint32 previousNumFlows = ThaHbceEntryNumFlowsGet(entry);
        ThaPwHeaderController controller = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);

        /* Remove the cell entry. Only log warning on active driver. See commit
         * message of this change for more detail. */
        if ((controller == NULL) && !deviceDeleting && accessible)
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning, "PwHeaderController is NULL\n");
        ret |= CellContentRemove(self, cellContent, controller);

        numFlows = ThaHbceEntryNumFlowsGet(entry);

        if (previousNumFlows == numFlows)
            {
            AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "Cell removing may be fail, ret = %s\n", AtRet2String(ret));
            ret |= cAtErrorDevFail;
            break;
            }
        }

    return ret;
    }

static eAtRet RemoveAllPws(ThaHbce self)
    {
    uint32 i;
    uint32 maxNumEntries = mMethodsGet(self)->MaxNumEntries(self);
    eAtRet ret = cAtOk;

    for (i = 0; i < maxNumEntries; i++)
        ret |= RemoveAllPwsAtEntry(self, self->entries[i]);

    return ret;
    }

static eAtRet AllEntriesDelete(ThaHbce self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 maxNumEntries = mMethodsGet(self)->MaxNumEntries(self);
    uint32 i;
    eAtRet ret = cAtOk;

    if (self->entries == NULL)
        return cAtOk;

    if (!DeviceIsDeleting(self))
        ret |= RemoveAllPws(self);

    /* Delete all entries */
    for (i = 0; i < maxNumEntries; i++)
        AtObjectDelete((AtObject)(self->entries[i]));

    /* And the memory that hold them */
    mMethodsGet(osal)->MemFree(osal, self->entries);
    self->entries = NULL;

    return ret;
    }

static eAtRet SwInit(ThaHbce self)
    {
    eAtRet ret = cAtOk;

    if (self->entries)
        ret |= ThaHbceAllEntriesDelete(self);
    ret |= AllEntriesCreate(self);

    return ret;
    }

static uint32 EntryOffset(ThaHbce self, uint32 entryIndex, uint8 pageId)
    {
	AtUnused(self);
    return (pageId * 0x400UL) + entryIndex;
    }

static uint32 *LongRegisterContentClear(uint32 *longRegValue, uint8 regSizeInDwords)
    {
    AtOsal osal;
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longRegValue, 0, sizeof(uint32) * regSizeInDwords);

    return longRegValue;
    }

static uint16 LongWrite(ThaHbce self, uint32 address, uint32 *longValues, uint16 numDwords, AtIpCore core)
    {
    AtModule claModule = (AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    if (claModule == NULL)
    	return 0;
    return mMethodsGet(claModule)->HwLongWriteOnCore(claModule, address, longValues, numDwords, core);
    }

static ThaClaPwController ClaPwController(ThaHbce self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return ThaModuleClaPwControllerGet(claModule);
    }

static uint32 CLAHBCEHashingTabCtrl(ThaHbce self)
    {
    return ThaClaPwControllerCLAHBCEHashingTabCtrl(ClaPwController(self)) + ThaHbcePartOffset(self);
    }

static uint32 Read(ThaHbce self, uint32 address)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return mModuleHwReadByHal(claModule, address, AtIpCoreHalGet(self->core));
    }

static void Write(ThaHbce self, uint32 address, uint32 value)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    mModuleHwWriteByHal(claModule, address, value, AtIpCoreHalGet(self->core));
    }

static void EntryInitOnPage(ThaHbce self, uint32 entryIndex, uint8 pageId)
    {
    uint32 address = CLAHBCEHashingTabCtrl(self) + mMethodsGet(self)->EntryOffset(self, entryIndex, pageId);

    if (ThaHbceEntityShouldOptimizeInit((ThaHbceEntity)self))
        Write(self, address, 0);
    else
        {
        uint32 longRegValue[cLongRegSizeInDwords];
        LongRegisterContentClear(longRegValue, cLongRegSizeInDwords);
        LongWrite(self, address, longRegValue, cLongRegSizeInDwords, self->core);
        }
    }

static void EntryInit(ThaHbce self, uint32 entryIndex)
    {
    static const uint8 cNumPages = 2;
    uint8 page_i;

    for (page_i = 0; page_i < cNumPages; page_i++)
        if (self)
            mMethodsGet(self)->EntryInitOnPage(self, entryIndex, page_i);
    }

static uint32 CLAHBCEGlbCtrl(ThaHbce self)
    {
    return ThaClaPwControllerCLAHBCEGlbCtrl(ClaPwController(self)) + ThaHbcePartOffset(self);
    }

static void MasterInit(ThaHbce self)
    {
    uint32 longRegValue[cLongRegSizeInDwords];
    ThaClaPwController pwController = ClaPwController(self);

    if (!mMethodsGet(self)->HasPages(self))
        return;

    LongRegisterContentClear(longRegValue, cLongRegSizeInDwords);

    mFieldIns(&longRegValue[cThaClaHbceCodingSelectedModeDwIndex],
              ThaClaPwControllerClaHbceCodingSelectedModeMask(pwController),
              ThaClaPwControllerClaHbceCodingSelectedModeShift(pwController),
              mMethodsGet(self)->CodeLevel(self));

    mFieldIns(&longRegValue[cThaClaHbceTimeoutValDwIndex],
              ThaClaPwControllerClaHbceTimeoutValMask(pwController),
              ThaClaPwControllerClaHbceTimeoutValShift(pwController),
              0x100);

    LongWrite(self, CLAHBCEGlbCtrl(self), longRegValue, cLongRegSizeInDwords, self->core);
    }

static void AllEntriesInit(ThaHbce self)
    {
    uint32 i;

    for (i = 0; i < mMethodsGet(self)->MaxNumEntries(self); i++)
        ThaHbceEntryInit(self, i);
    }

static uint8 CodeLevel(ThaHbce self)
    {
	AtUnused(self);
    return 1;
    }

static eAtRet HwInit(ThaHbce self)
    {
    ThaHbceMasterInit(self);
    mMethodsGet(self)->AllEntriesInit(self);

    return cAtOk;
    }

static eAtRet Init(ThaHbceEntity self)
    {
    eAtRet ret = cAtOk;
    ThaHbce hbce = (ThaHbce)self;

    /* Initialize it self */
    ret |= HwInit(hbce);
    ret |= ThaHbceSwInit(hbce);
    if (ret != cAtOk)
        return ret;

    /* Create its memory pool */
    if (hbce->memoryPool == NULL)
        hbce->memoryPool = mMethodsGet(hbce)->MemoryPoolCreate(hbce);
    if (hbce->memoryPool == NULL)
        {
        ThaHbceAllEntriesDelete(hbce);
        return cAtErrorRsrcNoAvail;
        }

    /* Initialize this pool */
    return ThaHbceEntityInit((ThaHbceEntity)(hbce->memoryPool));
    }

static void StandbyCleanup(ThaHbce self)
    {
    while (AtListLengthGet(self->standby.entries) > 0)
        {
        tThaHbceStandbyEntry *ha = (tThaHbceStandbyEntry *)AtListObjectRemoveAtIndex(self->standby.entries, 0);
        ThaHbceEntry entry = ThaHbceEntryAtIndex(self, ha->entryIndex);
        entry->standby = NULL;
        AtOsalMemFree(ha);
        }

    AtObjectDelete((AtObject)self->standby.entries);
    self->standby.entries = NULL;
    }

static eAtRet AsyncInit(ThaHbceEntity self)
    {
    return ThaHbceAsyncInitMain(self);
    }

static void Delete(AtObject self)
    {
    ThaHbce hbce = (ThaHbce)self;
    eAtRet ret = cAtOk;

    ThaHbceStandbyCleanup(hbce);
    if (hbce->entries)
        ret |= ThaHbceAllEntriesDelete(hbce);

    /* Delete its memory pool */
    AtObjectDelete((AtObject)(hbce->memoryPool));
    hbce->memoryPool = NULL;

    if (ret != cAtOk)
        AtModuleLog((AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self), cAtLogLevelCritical, AtSourceLocation, "ERROR: Hbce delete but got failure with error code %d\r\n", ret);

    m_AtObjectMethods->Delete(self);
    }

static eBool HashIndexIsValid(ThaHbce self, uint32 hashIndex)
    {
    return (hashIndex < mMethodsGet(self)->MaxNumEntries(self)) ? cAtTrue : cAtFalse;
    }

static ThaHbceEntry EntryAtIndex(ThaHbce self, uint32 entryIndex)
    {
    if (!mHbceIsValid(self))
        return NULL;

    if (!HashIndexIsValid(self, entryIndex))
        return NULL;

    if (self->entries)
        return self->entries[entryIndex];

    if (AllEntriesCreate(self) == cAtOk)
        return self->entries[entryIndex];

    return NULL;
    }

static void StandbyDebug(ThaHbce self)
    {
    AtIterator iterator;
    tThaHbceStandbyEntry *haEntry;

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "(CLA standby) HBCE information:\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");

    /* Nothing to display */
    if (AtListLengthGet(self->standby.entries) == 0)
        {
        AtPrintc(cSevNormal, "Nothing to display\r\n");
        return;
        }

    AtPrintc(cSevNormal, "Hit entries: %d entries\r\n", AtListLengthGet(self->standby.entries));
    iterator = AtListIteratorCreate(self->standby.entries);
    while ((haEntry = (tThaHbceStandbyEntry *)AtIteratorNext(iterator)) != NULL)
        {
        AtPrintc(cSevNormal,
                 "- Entry %d: %d (hits)\r\n",
                 haEntry->entryIndex + 1,
                 haEntry->hitCounts);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void Debug(ThaHbceEntity self)
    {
    ThaHbce hbce = (ThaHbce)self;
    uint32 numEntries = mMethodsGet(hbce)->MaxNumEntries(hbce);
    uint32 i;
    uint32 maxConflictsEntry = cInvalidUint32, maxConflictCount = 0;

    if (numEntries == 0)
        return;

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "(CLA) HBCE information:\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");
    if (mMethodsGet(hbce)->HasPages(hbce))
        AtPrintc(cSevNormal, "* Active page: %d\r\n", ThaHbceActivePage(hbce));

    for (i = 0; i < numEntries; i++)
        {
        uint32 numFlows, flow_i;
        ThaHbceEntry entry;

        /* Ignore empty entry */
        entry = EntryAtIndex(hbce, i);
        numFlows = ThaHbceEntryNumFlowsGet(entry);
        if (numFlows == 0)
            continue;

        /* Dump it */
        AtPrintc(cSevNormal, "* Entry %u: ", i + 1);
        if (numFlows > 1)
            AtPrintc(cSevWarning, "%u flows\r\n", numFlows);
        else
            AtPrintc(cSevNormal, "%u flows\r\n", numFlows);
        for (flow_i = 0; flow_i < numFlows; flow_i++)
            {
            ThaHbceMemoryCellContent cellContent = ThaHbceEntryCellContentAtIndex(entry, flow_i);
            ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(cellContent);
            AtPrintc(cSevNormal, "    - Cell %4u: %s\r\n", ThaHbceMemoryCellIndexGet(cell) + 1, AtObjectToString((AtObject)cellContent));
            }

        if (numFlows > maxConflictCount)
            {
            maxConflictsEntry = i;
            maxConflictCount = numFlows;
            }
        }

    AtPrintc(cSevInfo, "-> Max conflict entry: %d with %d conflicts\r\n", maxConflictsEntry + 1, maxConflictCount);

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "(CLA) Memory pool information:\r\n");
    AtPrintc(cSevInfo, "========================================================\r\n");
    ThaHbceEntityDebug((ThaHbceEntity)(hbce->memoryPool));
    StandbyDebug(hbce);
    }

static eAtRet PwEnable(ThaHbce self, ThaPwAdapter pwAdapter, eBool enable)
    {
    eAtRet ret = mMethodsGet(self)->PwHeaderControllerEnable(self, ThaPwAdapterHeaderController(pwAdapter), enable);
    if(ret != cAtOk)
        return ret;

    /* Do not enable backup PSN if PW does not belong to HS group */
    if ((AtPwHsGroupGet(ThaPwAdapterPwGet(pwAdapter)) == NULL) && (enable == cAtTrue))
        return cAtOk;

    return mMethodsGet(self)->PwHeaderControllerEnable(self, ThaPwAdapterBackupHeaderController(pwAdapter), enable);
    }

static eBool PwIsEnabled(ThaHbce self, ThaPwAdapter pwAdapter)
    {
    return mMethodsGet(self)->PwHeaderControllerIsEnabled(self, ThaPwAdapterHeaderController(pwAdapter));
    }

static ThaHbceMemoryPool MemoryPoolGet(ThaHbce self)
    {
    if (!mHbceIsValid(self))
        return NULL;

    if (self->memoryPool)
        return self->memoryPool;

    self->memoryPool = mMethodsGet(self)->MemoryPoolCreate(self);
    return self->memoryPool;
    }

static eBool NewPwLabelExistInHashEntry(ThaHbce self, ThaPwHeaderController controller, AtPwPsn newPsn, ThaHbceEntry hashEntry)
    {
    uint32 flow_i;
    uint32 newLabel = ThaHbcePsnLabelForHashing(self, newPsn);
    uint32 numFlows = ThaHbceEntryNumFlowsGet(hashEntry);
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        ThaHbceMemoryCellContent cellContent = ThaHbceEntryCellContentAtIndex(hashEntry, flow_i);
        ThaPwHeaderController existingController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
        uint32 existingLabel = ThaHbcePsnLabelForHashing(self, ThaPwHeaderControllerPsnGet(existingController));

        /* Ignore itself */
        if (existingController == controller)
            continue;

        if (existingLabel == newLabel)
            {
            AtChannel thisPw = (AtChannel)ThaPwAdapterPwGet(ThaPwHeaderControllerAdapterGet(controller));
            AtChannel existingPw = (AtChannel)ThaPwAdapterPwGet(ThaPwHeaderControllerAdapterGet(existingController));
            AtChannelLog(thisPw, cAtLogLevelWarning, AtSourceLocation,
                         "Conflict label with PW %u\r\n", AtChannelIdGet((AtChannel)existingPw));
            return cAtTrue;
            }
        }

    return cAtFalse;
    }

static ThaHbceMemoryCell AllocateCellForPw(ThaHbce self, ThaPwHeaderController controller, uint32 hashIndex)
    {
    ThaHbceEntry hashEntry = EntryAtIndex(self, hashIndex);
    ThaHbceMemoryCell cell = NULL;
    ThaHbceMemoryCellContent cellContent = NULL;
    ThaHbceMemoryPool memoryPool = MemoryPoolGet(self);
    uint32 nextCellIndex;
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);

    /* No conflict, use last cell in memory pool */
    eBool noConflict = (ThaHbceEntryNumFlowsGet(hashEntry) == 0);
    if (noConflict)
        {
        uint32 lastCellIndex = ThaHbceMemoryPoolNumUsedCells(memoryPool);
        return ThaHbceMemoryPoolUseCell(memoryPool, lastCellIndex);
        }

    /* Check to see if there is existing pw that has same label as adding pw, if yes, return NULL */
    if (ThaHbceNewPwLabelExistInHashEntry(self, controller, psn, hashEntry))
        return NULL;

    /* Need to shift down all cells after nextCellIndex */
    cellContent = ThaHbceEntryFirstCellContentGet(hashEntry);
    cell = ThaHbceMemoryCellContentCellGet(cellContent);
    nextCellIndex = ThaHbceEntryNumFlowsGet(hashEntry) + ThaHbceMemoryCellIndexGet(cell);

    return ThaHbceMemoryPoolUseCell(memoryPool, nextCellIndex);
    }

static uint32 HashIndexGetOnPart(ThaHbce self, uint8 partId, const uint32 *hwLabel)
    {
    uint32 longReg[cLongRegSizeInDwords];
    uint16 codeLevel;
    uint32 hbcePatern59_50;
    uint32 hbcePatern49_40;
    uint32 hbcePatern39_30;
    uint32 swHbcePatern29_20;
    uint32 swHbcePatern19_10;
    uint32 swHbcePatern9_0;
    uint32 msb, lsb;
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    AtDevice device = AtModuleDeviceGet((AtModule)claModule);
    uint32 partOffset = ThaDeviceModulePartOffset((ThaDevice)device, cThaModuleCla, partId);
    uint32 regAddr = ThaClaPwControllerCLAHBCEGlbCtrl(ThaModuleClaPwControllerGet(claModule)) + partOffset;
    ThaClaPwController pwController = ClaPwController(self);

    /* On the standby, reduce read operations as much as possible */
    if (mInAccessible(self))
        ThaHbceStandbyGlbCtrlCached(self, longReg, cLongRegSizeInDwords);

    /* On active, no limit on reading */
    else
        mModuleHwLongRead(claModule, regAddr, longReg, cLongRegSizeInDwords, self->core);

    mFieldGet(longReg[cThaClaHbceCodingSelectedModeDwIndex],
              ThaClaPwControllerClaHbceCodingSelectedModeMask(pwController),
              ThaClaPwControllerClaHbceCodingSelectedModeShift(pwController),
              uint16,
              &codeLevel);

    mFieldGet(longReg[cThaHbcePatern59_50DwIndex],
              cThaHbcePatern59_50Mask,
              cThaHbcePatern59_50Shift,
              uint32,
              &hbcePatern59_50);

    mFieldGet(longReg[cThaHbcePatern49_40DwIndex],
              cThaHbcePatern49_40Mask,
              cThaHbcePatern49_40Shift,
              uint32,
              &hbcePatern49_40);

    mFieldGet(longReg[cThaHbcePatern39_30DwIndex + 1],
              cThaHbcePatern39_30HwHeadMask,
              cThaHbcePatern39_30HwHeadShift,
              uint32,
              &msb);

    mFieldGet(longReg[cThaHbcePatern39_30DwIndex],
              cThaHbcePatern39_30HwTailMask,
              cThaHbcePatern39_30HwTailShift,
              uint32,
              &lsb);

    hbcePatern39_30 = 0;
    mFieldIns(&hbcePatern39_30,
              cThaHbcePatern39_30SwHeadMask,
              cThaHbcePatern39_30SwHeadShift,
              msb);

    mFieldIns(&hbcePatern39_30,
              cThaHbcePatern39_30SwTailMask,
              cThaHbcePatern39_30SwTailShift,
              lsb);

    mFieldGet(hwLabel[cThaHbcePatern29_20DwIndex],
              cThaHbcePatern29_20Mask,
              cThaHbcePatern29_20Shift,
              uint32,
              &swHbcePatern29_20);

    mFieldGet(hwLabel[cThaHbcePatern19_10DwIndex],
              cThaHbcePatern19_10Mask,
              cThaHbcePatern19_10Shift,
              uint32,
              &swHbcePatern19_10);

    mFieldGet(hwLabel[cThaHbcePatern9_0DwIndex],
              cThaHbcePatern9_0Mask,
              cThaHbcePatern9_0Shift,
              uint32,
              &swHbcePatern9_0);

    /* Calculate hash index */
    if (codeLevel == 0)
        return swHbcePatern9_0;
    if (codeLevel == 1)
        return swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 2)
        return swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 3)
        return hbcePatern39_30 ^ swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 4)
        return hbcePatern49_40 ^ hbcePatern39_30 ^ swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 5)
        return hbcePatern59_50 ^ hbcePatern49_40 ^ hbcePatern49_40 ^ swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;

    return 0x0;
    }

static void HwHcbeLabel(ThaHbce self, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *hwLabel)
    {
    uint32 msb, lsb;
    uint8 ethPortId = (uint8)(ethPort ? (uint8)AtChannelHwIdGet((AtChannel)ethPort) : 0);
	AtUnused(self);

    mFieldIns(&hwLabel[1], cThaHbceLblId1HwTailMask, cThaHbceLblId1HwTailShift, 0);

    mFieldGet(label, cThaHbceLblId0SwHeadMask, cThaHbceLblId0SwHeadShift, uint32, &msb);
    mFieldIns(&hwLabel[1], cThaHbceLblId0HwHeadMask, cThaHbceLblId0HwHeadShift, msb);

    mFieldGet(label, cThaHbceLblId0SwTailMask, cThaHbceLblId0SwTailShift, uint32, &lsb);
    mFieldIns(&hwLabel[0], cThaHbceLblId0HwTailMask, cThaHbceLblId0HwTailShift, lsb);

    mFieldIns(&hwLabel[0], cThaHbcePktTypeMask, cThaHbcePktTypeShift, ThaHbcePktType(psnType));

    mFieldIns(&hwLabel[0], cThaHbceGePortMask, cThaHbceGePortShift, ethPortId);
    }

static eAtRet RemainBitsGet(const uint32 *hwLabel, uint32 *remainBits)
    {
    uint32 msb, lsb;

    mFieldGet(hwLabel[1],
              cThaHbceMemoryPool1HwTailMask,
              cThaHbceMemoryPool1HwTailShift,
              uint32,
              &lsb);
    mFieldIns(&remainBits[1],
              cThaHbceMemoryPool1SwTailMask,
              cThaHbceMemoryPool1SwTailShift,
              lsb);

    mFieldGet(hwLabel[1],
              cThaHbceMemoryPool0HwHeadMask,
              cThaHbceMemoryPool0HwHeadShift,
              uint32,
              &msb);

    mFieldGet(hwLabel[0],
              cThaHbceMemoryPool0HwTailMask,
              cThaHbceMemoryPool0HwTailShift,
              uint32,
              &lsb);

    mFieldIns(&remainBits[0],
              cThaHbceMemoryPool0SwHeadMask,
              cThaHbceMemoryPool0SwHeadShift,
              msb);

    mFieldIns(&remainBits[0],
              cThaHbceMemoryPool0SwTailMask,
              cThaHbceMemoryPool0SwTailShift,
              lsb);

    return cAtOk;
    }

static uint32 PsnHash(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits)
    {
    uint32 hwLabel[2];
    uint32 hashIndex;
    AtOsal osal = AtSharedDriverOsalGet();

    mMethodsGet(osal)->MemInit(osal, hwLabel, 0, sizeof(hwLabel));
    HwHcbeLabel(self, ethPort, label, psnType, hwLabel);
    hashIndex = HashIndexGetOnPart(self, partId, hwLabel);

    if (remainBits)
        RemainBitsGet(hwLabel, remainBits);

    return hashIndex;
    }

static uint32 PwPsnHash(ThaHbce self, ThaPwAdapter pwAdapter, AtPwPsn psn, uint32 *remainBits)
    {
    ThaModulePw pwModule = (ThaModulePw)AtChannelModuleGet((AtChannel)pwAdapter);
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);

    if ((psnType == cAtPwPsnTypeMef) && (AtPwPsnTypeGet(AtPwPsnLowerPsnGet(psn)) == cAtPwPsnTypeMpls))
        psnType = cAtPwPsnTypeMpls;

    return mMethodsGet(self)->PsnHash(self,
                                      ThaModulePwPartOfPw(pwModule, (AtPw)pwAdapter),
                                      AtPwEthPortGet((AtPw)pwAdapter),
                                      ThaHbcePsnLabelForHashing(self, psn),
                                      psnType,
                                      remainBits);
    }

static eBool PwPsnIsSupported(AtPwPsn psn)
    {
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn) ;

    if (psn == NULL)
        return cAtFalse;

    if ((psnType == cAtPwPsnTypeMef)  ||
        (psnType == cAtPwPsnTypeMpls) ||
        (psnType == cAtPwPsnTypeUdp))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 StandbyPage(ThaHbce self)
    {
    return (ThaHbceActivePage(self) == 0) ? 1 : 0;
    }

static void PageSwitch(ThaHbce self, uint8 toPage)
    {
    uint32 regVal;
    ThaClaPwController pwController = ClaPwController(self);

    regVal = Read(self, CLAHBCEGlbCtrl(self));
    mFieldIns(&regVal,
              ThaClaPwControllerClaHbceHashingTabSelectedPageMask(pwController),
              ThaClaPwControllerClaHbceHashingTabSelectedPageShift(pwController),
              toPage);
    Write(self, CLAHBCEGlbCtrl(self), regVal);
    }

static eAtRet HashEntryApply(ThaHbce self, uint32 entryIndex, uint8 page)
    {
    uint32 regVal, address;
    uint32 startMemoryCell;
    ThaHbceMemoryPool memoryPool = self->memoryPool;
    ThaHbceEntry hashEntry = EntryAtIndex(self, entryIndex);
    ThaHbceMemoryCell firstCell;
    ThaClaPwController pwController = ClaPwController(self);

    /* If there is no flow in this entry, reset it */
    address = CLAHBCEHashingTabCtrl(self) + mMethodsGet(self)->EntryOffset(self, entryIndex, page);
    if (ThaHbceEntryNumFlowsGet(hashEntry) == 0)
        {
        Write(self, address, 0);
        return cAtOk;
        }

    /* Set number of flows and start memory cell */
    firstCell = ThaHbceMemoryCellContentCellGet(ThaHbceEntryFirstCellContentGet(hashEntry));
    regVal = 0;
    mFieldIns(&regVal,
              ThaClaPwControllerCLAHbceFlowNumMask(pwController),
              ThaClaPwControllerCLAHbceFlowNumShift(pwController),
              ThaHbceEntryNumFlowsGet(hashEntry));
    startMemoryCell = (ThaHbceMemoryPoolMaxNumCells(memoryPool) * page) + ThaHbceMemoryCellIndexGet(firstCell);
    mFieldIns(&regVal,
              ThaClaPwControllerCLAHbceMemoryStartPtrMask(pwController),
              ThaClaPwControllerCLAHbceMemoryStartPtrShift(pwController),
              startMemoryCell);
    Write(self, address, regVal);

    return cAtOk;
    }

static eAtRet HashTableApply(ThaHbce self, uint8 page)
    {
    uint16 hashEntry_i;
    eAtRet ret = cAtOk;

    for (hashEntry_i = 0; hashEntry_i < mMethodsGet(self)->MaxNumEntries(self); hashEntry_i ++)
        ret |= mMethodsGet(self)->HashEntryApply(self, hashEntry_i, page);

    return ret;
    }

static eBool HashEntryContainsPwHeaderController(ThaHbceEntry entry, ThaPwHeaderController controller)
    {
    uint32 cell_i;
    uint32 numFlows = ThaHbceEntryNumFlowsGet(entry);

    for (cell_i = 0; cell_i < numFlows; cell_i++)
        {
        ThaHbceMemoryCellContent cell = ThaHbceEntryCellContentAtIndex(entry, cell_i);
        if (ThaHbceMemoryCellContentPwHeaderControllerGet(cell) == controller)
            return cAtTrue;
        }

    return cAtFalse;
    }

uint8 ThaHbceActivePage(ThaHbce self)
    {
    uint8 activePage;
    ThaClaPwController pwController;
    uint32 regVal = 0;

    if (mInAccessible(self))
        return 0;

    if (self == NULL)
        return 0;

    if (!mMethodsGet(self)->HasPages(self))
        return 0;

    pwController = ClaPwController(self);
    regVal = Read(self, CLAHBCEGlbCtrl(self));
    mFieldGet(regVal,
              ThaClaPwControllerClaHbceHashingTabSelectedPageMask(pwController),
              ThaClaPwControllerClaHbceHashingTabSelectedPageShift(pwController),
              uint8,
              &activePage);

    return activePage;
    }

static eAtRet Apply2Hardware(ThaHbce self)
    {
    eAtRet ret = cAtOk;
    uint8 standbyPage;
    ThaHbceMemoryPool memoryPool;

    memoryPool = self->memoryPool;

    /* Apply software configuration to standby page */
    standbyPage = StandbyPage(self);
    ret |= ThaHbceMemoryPoolApplyToHardware(memoryPool, standbyPage);
    ret |= HashTableApply(self, standbyPage);
    if (ret != cAtOk)
        return ret;

    /* Switch page */
    PageSwitch(self, standbyPage);

    return cAtOk;
    }

static ThaHbceMemoryCell SearchCellForPw(ThaHbce self, ThaPwAdapter pwAdapter, ThaHbceEntry hashEntry)
    {
    ThaHbceMemoryPool memoryPool = MemoryPoolGet(self);
    ThaHbceMemoryCellContent content;
    uint32 firstCellIndex, flow_i;

    if (memoryPool == NULL)
        return NULL;

    /* Get first cell index */
    content = ThaHbceEntryFirstCellContentGet(hashEntry);
    firstCellIndex = ThaHbceMemoryCellIndexGet(ThaHbceMemoryCellContentCellGet(content));

    content = NULL;
    for (flow_i = 0; flow_i < ThaHbceEntryNumFlowsGet(hashEntry); flow_i++)
        {
        if (ThaHbceMemoryPoolCellHwFlowIdGet(memoryPool, firstCellIndex + flow_i) != AtChannelHwIdGet((AtChannel)pwAdapter))
            continue;

        /* Found out cell content for this pw */
        content = ThaHbceEntryCellContentAtIndex(hashEntry, flow_i);
        break;
        }

    return ThaHbceMemoryCellContentCellGet(content);
    }

static eAtRet PwHeaderControllerEnable(ThaHbce self, ThaPwHeaderController controller, eBool enable)
    {
    /* Nothing to do if PW has not been added to HBCE */
    ThaHbceMemoryCellContent cellContent;
    ThaHbceMemoryCell cell;

    if (!mHbceIsValid(self))
        return cAtErrorInvlParm;

    if (mInAccessible(self))
        return cAtOk;

    cellContent = ThaPwHeaderControllerHbceMemoryCellContentGet(controller);
    if (cellContent == NULL)
        return cAtErrorNullPointer;

    /* Ask memory pool to enable/disable this cell */
    cell = ThaHbceMemoryCellContentCellGet(cellContent);
    return ThaHbceMemoryPoolCellEnable(self->memoryPool, cell, enable);
    }

static eBool PwHeaderControllerIsEnabled(ThaHbce self, ThaPwHeaderController controller)
    {
    ThaHbceMemoryCellContent cellContent;
    ThaHbceMemoryCell cell;

    cellContent = ThaPwHeaderControllerHbceMemoryCellContentGet(controller);
    if (cellContent == NULL)
        return cAtFalse;

    cell = ThaHbceMemoryCellContentCellGet(cellContent);
    return ThaHbceMemoryPoolCellIsEnabled(self->memoryPool, cell);
    }

static uint32 CellGroupWorkingMask(ThaHbce self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static uint32 CellGroupWorkingShift(ThaHbce self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 Restore(ThaHbceEntity self, uint8 page)
    {
    ThaHbce hbce = (ThaHbce)self;
    uint32 remained = 0;
    AtIterator iterator;
    tThaHbceStandbyEntry *haEntry;

    /* Do not need to scan all of entries */
    iterator = AtListIteratorCreate(hbce->standby.entries);
    while ((haEntry = (tThaHbceStandbyEntry *)AtIteratorNext(iterator)) != NULL)
        {
        ThaHbceEntity entry = (ThaHbceEntity)hbce->entries[haEntry->entryIndex];
        remained = remained + ThaHbceEntityRestore(entry, page);
        }
    AtObjectDelete((AtObject)iterator);

    /* Done, need to cleanup standby database */
    if (!AtDriverKeepDatabaseAfterRestore())
        ThaHbceStandbyCleanup(hbce);

    return remained;
    }

static eBool StandbyHitCountEntryExist(ThaHbce self, ThaHbceEntry entry)
    {
    AtIterator iterator = AtListIteratorGet(self->standby.entries);
    tThaHbceStandbyEntry *haHitCountEntry;

    AtIteratorRestart(iterator);
    while ((haHitCountEntry = (tThaHbceStandbyEntry *)AtIteratorNext(iterator)) != NULL)
        {
        if (ThaHbceEntryIndexGet(entry) == haHitCountEntry->entryIndex)
            return cAtTrue;
        }

    return cAtFalse;
    }

static void StandbyEntryHitCountIncrease(ThaHbce self, uint32 entryIndex)
    {
    ThaHbceEntry entry = ThaHbceEntryAtIndex(self, entryIndex);
    tThaHbceStandbyEntry *newHa;

    /* Database was not cleanup before (autotest only). Only add new entry when
     * it does not exist */
    if (AtDriverKeepDatabaseAfterRestore() && StandbyHitCountEntryExist(self, entry))
        return;

    /* Already created, just increase */
    if (entry->standby)
        {
        entry->standby->hitCounts = entry->standby->hitCounts + 1;
        return;
        }

    /* Create new data structure to store */
    newHa = AtOsalMemAlloc(sizeof(tThaHbceStandbyEntry));
    AtOsalMemInit(newHa, 0, sizeof(tThaHbceStandbyEntry));
    newHa->hitCounts = 1;
    newHa->entryIndex = entryIndex;

    /* Cache it */
    entry->standby = newHa;
    AtListObjectAdd(self->standby.entries, (AtObject)newHa);
    }

static void StandbyEntryHitCountDecrease(ThaHbce self, uint32 entryIndex)
    {
    ThaHbceEntry entry = ThaHbceEntryAtIndex(self, entryIndex);

    if (entry->standby == NULL)
        return;

    entry->standby->hitCounts = entry->standby->hitCounts - 1;
    if (entry->standby->hitCounts > 0)
        return;

    AtListObjectRemove(self->standby.entries, (AtObject)entry->standby);
    AtOsalMemFree(entry->standby);
    entry->standby = NULL;
    }

static uint32 HitCountGet(ThaHbce self, uint32 entryIndex)
    {
    ThaHbceEntry entry = ThaHbceEntryAtIndex(self, entryIndex);
    return entry->standby ? entry->standby->hitCounts : 0;
    }

static eAtRet StandbyPwAdd(ThaHbce self, ThaPwHeaderController controller)
    {
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);
    ThaPwAdapter adapter;
    uint32 hashEntryIndex;

    if (psn == NULL)
        return cAtOk;

    adapter = ThaPwHeaderControllerAdapterGet(controller);
    hashEntryIndex = ThaHbcePwPsnHash(self, adapter, psn, NULL);
    ThaHbceStandbyEntryHitCountIncrease(self, hashEntryIndex);

    return cAtOk;
    }

static eAtRet StandbyPwRemove(ThaHbce self, ThaPwHeaderController controller)
    {
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);
    ThaPwAdapter adapter;
    uint32 hashEntryIndex;

    if (psn == NULL)
        return cAtOk;

    adapter = ThaPwHeaderControllerAdapterGet(controller);
    hashEntryIndex = ThaHbcePwPsnHash(self, adapter, psn, NULL);
    ThaHbceStandbyEntryHitCountDecrease(self, hashEntryIndex);

    return cAtOk;
    }

static void StandbyGlbCtrlCache(ThaHbce self)
    {
    tThaHbceRegisterCache *cache = ThaHbceRegisterCache(self);
    ThaClaPwController pwController = ClaPwController(self);
    uint8 codeLevel = mMethodsGet(self)->CodeLevel(self);
    mFieldIns(&(cache->glbCtrlRegVal[cThaClaHbceCodingSelectedModeDwIndex]),
              ThaClaPwControllerClaHbceCodingSelectedModeMask(pwController),
              ThaClaPwControllerClaHbceCodingSelectedModeShift(pwController),
              codeLevel);

    cache->registersCached = cAtTrue;
    }

static eBool HasPages(ThaHbce self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void MethodsInit(ThaHbce self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, CodeLevel);
        mMethodOverride(m_methods, MaxNumEntries);
        mMethodOverride(m_methods, EntryObjectCreate);
        mMethodOverride(m_methods, MemoryPoolCreate);
        mMethodOverride(m_methods, PwEnable);
        mMethodOverride(m_methods, PwIsEnabled);
        mMethodOverride(m_methods, AllocateCellForPw);
        mMethodOverride(m_methods, Apply2Hardware);
        mMethodOverride(m_methods, HashEntryApply);
        mMethodOverride(m_methods, SearchCellForPw);
        mMethodOverride(m_methods, PsnHash);
        mMethodOverride(m_methods, PwHeaderControllerEnable);
        mMethodOverride(m_methods, PwHeaderControllerIsEnabled);
        mMethodOverride(m_methods, CellGroupWorkingMask);
        mMethodOverride(m_methods, CellGroupWorkingShift);
        mMethodOverride(m_methods, HasPages);
        mMethodOverride(m_methods, NewPwLabelExistInHashEntry);
        mMethodOverride(m_methods, StandbyPwAdd);
        mMethodOverride(m_methods, StandbyPwRemove);
        mMethodOverride(m_methods, StandbyCleanup);

        /* Register offsets */
        mMethodOverride(m_methods, EntryOffset);

        mMethodOverride(m_methods, EntryInitOnPage);
        mMethodOverride(m_methods, EntryInit);
        mMethodOverride(m_methods, MasterInit);
        mMethodOverride(m_methods, AllEntriesInit);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaHbceEntity(ThaHbce self)
    {
    ThaHbceEntity hbceObject = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, mMethodsGet(hbceObject), sizeof(m_ThaHbceEntityOverride));
        mMethodOverride(m_ThaHbceEntityOverride, Init);
        mMethodOverride(m_ThaHbceEntityOverride, AsyncInit);
        mMethodOverride(m_ThaHbceEntityOverride, Debug);
        mMethodOverride(m_ThaHbceEntityOverride, Restore);
        }

    mMethodsSet(hbceObject, &m_ThaHbceEntityOverride);
    }

static void OverrideAtObject(ThaHbce self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaHbce self)
    {
    OverrideThaHbceEntity(self);
    OverrideAtObject(self);
    }

ThaHbce ThaHbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceEntityObjectInit((ThaHbceEntity)self, claModule) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    self->core       = core;
    self->identifier = hbceId;
    self->standby.entries = AtListCreate(0);

    return self;
    }

ThaHbce ThaHbceNew(uint8 hbceId, AtModule claModule, AtIpCore core)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbce newHbce = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newHbce == NULL)
        return NULL;

    /* Construct it */
    return ThaHbceObjectInit(newHbce, hbceId, claModule, core);
    }

eBool ThaHbcePsnCanBeUsed(ThaHbce self, ThaPwHeaderController controller, AtPwPsn newPsn)
    {
    uint32 remainBits[2];
    uint32 hashIndex;
    ThaHbceEntry hashEntry;
    eBool noConflict;
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwAdapter pwAdapter = ThaPwHeaderControllerAdapterGet(controller);

    if (!mHbceIsValid(self))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                                AtSourceLocation, "%s is called, HCBE is invalid!\r\n", AtFunction);
        return cAtFalse;
        }

    /* Have the hash entry */
    mMethodsGet(osal)->MemInit(osal, remainBits, 0, sizeof(remainBits));
    hashIndex = PwPsnHash(self, pwAdapter, newPsn, remainBits);
    hashEntry = EntryAtIndex(self, hashIndex);
    if (hashEntry == NULL)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                                AtSourceLocation, "%s is called, hash entry is null!\r\n", AtFunction);
        return cAtFalse;
        }

    if (HashEntryContainsPwHeaderController(hashEntry, controller))
        return cAtTrue;

    /* This hash entry is full */
    if (ThaHbceEntryIsFull(hashEntry))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                                AtSourceLocation, "%s is called, hash entry %u is full!\r\n", AtFunction, hashIndex);
        return cAtFalse;
        }

    /* Entry does not contain any flow yet, new label can be used */
    noConflict = (ThaHbceEntryNumFlowsGet(hashEntry) == 0);
    if (noConflict)
        return cAtTrue;

    /* Label is in used */
    if (ThaHbceNewPwLabelExistInHashEntry(self, controller, newPsn, hashEntry))
        return cAtFalse;

    return cAtTrue;
    }

eAtRet ThaHbcePwAdd(ThaHbce self, ThaPwHeaderController controller)
    {
    ThaHbceMemoryCell freeCell;
    ThaHbceMemoryCellContent cellContent;
    ThaHbceEntry hashEntry;
    uint32 hashIndex;
    eAtRet ret = cAtOk;
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwAdapter pwAdapter = ThaPwHeaderControllerAdapterGet(controller);
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);
    uint32 remainBits[2];

    if (!mHbceIsValid(self))
        return cAtErrorInvlParm;

    /* Cannot add if PW has not been set PSN */
    if (!PwPsnIsSupported(psn))
        return cAtErrorModeNotSupport;

    /* On standby driver, there would be no HW access and HBCE will be rebuilt
     * after switch over by AtDriverRestore() */
    if (mInAccessible(self))
        return mMethodsGet(self)->StandbyPwAdd(self, controller);

    /* Hash */
    mMethodsGet(osal)->MemInit(osal, remainBits, 0, sizeof(remainBits));
    hashIndex = PwPsnHash(self, pwAdapter, psn, remainBits);
    if (!HashIndexIsValid(self, hashIndex))
        return cAtErrorInvlParm;

    /* Allocate memory cell for this PW */
    freeCell = mMethodsGet(self)->AllocateCellForPw(self, controller, hashIndex);
    if (freeCell == NULL)
        return cAtErrorRsrcNoAvail;

    cellContent = ThaHbceMemoryCellContentGet(freeCell);
    ret |= ThaHbceMemoryCellContentRemainedBitsSet(cellContent, remainBits, 2);
    hashEntry = EntryAtIndex(self, hashIndex);

    ret |= ThaHbceMemoryCellContentEnable(cellContent, AtChannelIsEnabled((AtChannel)pwAdapter));
    ret |= ThaHbceMemoryCellContentPwHeaderControllerSet(cellContent, controller);

    /* Add this cell to this entry */
    ret |= ThaHbceEntryCellContentAdd(hashEntry, cellContent);
    ret |= ThaPwHeaderControllerHbceMemoryCellContentSet(controller, cellContent);

    return ret;
    }

eAtRet ThaHbcePwRemove(ThaHbce self, ThaPwHeaderController controller)
    {
    ThaHbceMemoryCellContent cellContent;

    if (!mHbceIsValid(self))
        return cAtErrorInvlParm;

    /* On standby driver, there would be no HW access and HBCE will be rebuilt
     * after switch over by AtDriverRestore() */
    if (mInAccessible(self))
        return mMethodsGet(self)->StandbyPwRemove(self, controller);

     /* Do nothing if PW has not been added to HBCE */
    cellContent = ThaPwHeaderControllerHbceMemoryCellContentGet(controller);
    if (cellContent == NULL)
        return cAtOk;

    return CellContentRemove(self, cellContent, controller);
    }

eBool ThaHbcePwIsEnabled(ThaHbce self, ThaPwAdapter pwAdapter)
    {
    if (!mHbceIsValid(self))
        return cAtFalse;
    return mMethodsGet(self)->PwIsEnabled(self, pwAdapter);
    }

AtIpCore ThaHbceCoreGet(ThaHbce self)
    {
    if (mHbceIsValid(self))
        return self->core;

    return NULL;
    }

uint32 ThaHbcePartOffset(ThaHbce self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self));
    return ThaDeviceModulePartOffset(device, cThaModuleCla, self->identifier);
    }

uint32 ThaHbcePsnLabelForHashing(ThaHbce self, AtPwPsn psn)
    {
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);
    AtPwPsn lowerPsn = AtPwPsnLowerPsnGet(psn);
	AtUnused(self);

    if (psnType == cAtPwPsnTypeUdp)  return AtPwUdpPsnExpectedPortGet((AtPwUdpPsn)psn);
    if (psnType == cAtPwPsnTypeMpls) return AtPwMplsPsnExpectedLabelGet((AtPwMplsPsn)psn);

    if ((psnType == cAtPwPsnTypeMef) && (AtPwPsnTypeGet(lowerPsn) == cAtPwPsnTypeMpls))
        return AtPwMplsPsnExpectedLabelGet((AtPwMplsPsn)lowerPsn);

    if (psnType == cAtPwPsnTypeMef)  return AtPwMefPsnExpectedEcIdGet((AtPwMefPsn)psn);

    return 0;
    }

ThaPwAdapter ThaHbcePwAdapterByPsn(ThaHbce self, uint32 label, eAtPwPsnType psnType, AtEthPort port)
    {
    uint32 remainBits[2];
    uint32 hashIndex;
    ThaHbceEntry hashEntry;
    uint32 flow_i;
    uint32 numFlows;
    ThaModuleEth ethModule = (ThaModuleEth)AtChannelModuleGet((AtChannel)port);
    AtOsal osal = AtSharedDriverOsalGet();

    if (!mHbceIsValid(self))
        return NULL;

    mMethodsGet(osal)->MemInit(osal, remainBits, 0, sizeof(remainBits));

    /* Find the hash entry */
    hashIndex = mMethodsGet(self)->PsnHash(self, ThaModuleEthPartOfPort(ethModule, port), port, label, psnType, remainBits);
    hashEntry = EntryAtIndex(self, hashIndex);
    if (hashEntry == NULL)
        return NULL;

    /* Look up */
    numFlows = ThaHbceEntryNumFlowsGet(hashEntry);
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        ThaHbceMemoryCellContent cellContent = ThaHbceEntryCellContentAtIndex(hashEntry, flow_i);
        ThaPwHeaderController existingController = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent);
        uint32 existingLabel = ThaHbcePsnLabelForHashing(self, ThaPwHeaderControllerPsnGet(existingController));

        if (existingLabel == label)
            return ThaPwHeaderControllerAdapterGet(existingController);
        }

    return NULL;
    }

static uint32 NumFlowsInHwOfHashEntry(ThaHbce self, uint32 hashIndex, uint32 *startCellIndex)
    {
    ThaHbceMemoryPool memoryPool = MemoryPoolGet(self);
    uint8 activatedPage = ThaHbceActivePage(self);
    uint32 address = CLAHBCEHashingTabCtrl(self) + mMethodsGet(self)->EntryOffset(self, hashIndex, activatedPage);
    uint32 entryVal = Read(self, address);
    uint8 numFlows;
    ThaClaPwController pwController = ClaPwController(self);
    uint32 flatCellIndex;

    mFieldGet(entryVal,
              ThaClaPwControllerCLAHbceMemoryStartPtrMask(pwController),
              ThaClaPwControllerCLAHbceMemoryStartPtrShift(pwController),
              uint32,
              &flatCellIndex);

    if (flatCellIndex < ThaHbceMemoryPoolMaxNumCells(memoryPool) * activatedPage) /* Something wrong */
        return 0;

    *startCellIndex = flatCellIndex - (ThaHbceMemoryPoolMaxNumCells(memoryPool) * activatedPage);

    mFieldGet(entryVal,
              ThaClaPwControllerCLAHbceFlowNumMask(pwController),
              ThaClaPwControllerCLAHbceFlowNumShift(pwController),
              uint8,
              &numFlows);
    return numFlows;
    }

static void HashEntryAllFlowsRestore(ThaHbce self, ThaHbceEntry hashEntry, uint32 hashIndex)
    {
    uint32 startCellIndex = 0, flow_i;
    uint32 numFlowsOfHashEntry = NumFlowsInHwOfHashEntry(self, hashIndex, &startCellIndex);
    ThaHbceMemoryPool memoryPool = MemoryPoolGet(self);

    if (numFlowsOfHashEntry == 0)
        return;

    for (flow_i = 0; flow_i < numFlowsOfHashEntry; flow_i++)
        {
        ThaHbceMemoryCell cell = ThaHbceMemoryPoolCellWarmRestore(memoryPool, startCellIndex + flow_i);
        ThaHbceEntryCellContentAdd(hashEntry, ThaHbceMemoryCellContentGet(cell));
        }
    }

eAtRet ThaHbcePwWarmRestore(ThaHbce self, ThaPwHeaderController controller)
    {
    ThaHbceMemoryCell cell;
    ThaHbceMemoryCellContent cellContent;
    ThaHbceEntry hashEntry;
    uint32 remainBits[2];
    uint32 hashIndex;
    eAtRet ret = cAtOk;
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwAdapter pwAdapter = ThaPwHeaderControllerAdapterGet(controller);
    AtPwPsn psn = ThaPwHeaderControllerPsnGet(controller);

    if (!mHbceIsValid(self))
        return cAtErrorInvlParm;

    /* Cannot add if PW has not been set PSN */
    if (!PwPsnIsSupported(psn))
        return cAtErrorModeNotSupport;

    /* Hash */
    mMethodsGet(osal)->MemInit(osal, remainBits, 0, sizeof(remainBits));
    hashIndex = PwPsnHash(self, pwAdapter, psn, remainBits);
    if (!HashIndexIsValid(self, hashIndex))
        return cAtErrorInvlParm;

    hashEntry = EntryAtIndex(self, hashIndex);
    if (ThaHbceEntryNumFlowsGet(hashEntry) == 0)
        HashEntryAllFlowsRestore(self, hashEntry, hashIndex);

    /* Search memory cell for this PW */
    cell = mMethodsGet(self)->SearchCellForPw(self, pwAdapter, hashEntry);
    if (cell == NULL)
        return cAtErrorRsrcNoAvail;

    /* Save information */
    cellContent = ThaHbceMemoryCellContentGet(cell);
    ret |= ThaHbceMemoryCellContentRemainedBitsSet(cellContent, remainBits, 2);
    ret |= ThaHbceMemoryCellContentEnable(cellContent, AtChannelIsEnabled((AtChannel)pwAdapter));
    ret |= ThaHbceMemoryCellContentPwHeaderControllerSet(cellContent, controller);
    ret |= ThaPwHeaderControllerHbceMemoryCellContentSet(controller, cellContent);

    return ret;
    }

eAtRet ThaHbcePwEnable(ThaHbce self, ThaPwAdapter pwAdapter, eBool enable)
    {
    return (self) ? mMethodsGet(self)->PwEnable(self, pwAdapter, enable) : cAtErrorNullPointer;
    }

eAtRet ThaHbcePwHeaderControllerEnable(ThaHbce self, ThaPwHeaderController controller, eBool enable)
    {
    return (self) ? mMethodsGet(self)->PwHeaderControllerEnable(self, controller, enable) : cAtErrorNullPointer;
    }

eBool ThaHbcePwHeaderControllerIsEnabled(ThaHbce self, ThaPwHeaderController controller)
    {
    if (self)
        return mMethodsGet(self)->PwHeaderControllerIsEnabled(self, controller);
    return cAtFalse;
    }

ThaHbceMemoryPool ThaHbceMemoryPoolGet(ThaHbce self)
    {
    return (self) ? MemoryPoolGet(self) : NULL;
    }

eBool ThaHbceNewPwLabelExistInHashEntry(ThaHbce self, ThaPwHeaderController controller, AtPwPsn newPsn, ThaHbceEntry hashEntry)
    {
    if (self)
        return mMethodsGet(self)->NewPwLabelExistInHashEntry(self, controller, newPsn, hashEntry);
    return cAtFalse;
    }

ThaHbceEntry ThaHbceEntryAtIndex(ThaHbce self, uint32 entryIndex)
    {
    return (self) ? EntryAtIndex(self, entryIndex) : NULL;
    }

uint32 ThaHbceCLAHBCEHashingTabCtrl(ThaHbce self)
    {
    return (self) ? CLAHBCEHashingTabCtrl(self) : 0xFFFFFFFF;
    }

eAtRet ThaHbceApply2Hardware(ThaHbce self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (mInAccessible(self))
        return cAtOk;

    return mMethodsGet(self)->Apply2Hardware(self);
    }

uint8 ThaHbcePktType(eAtPwPsnType psnType)
    {
    if (psnType == cAtPwPsnTypeUdp)  return 1;
    if (psnType == cAtPwPsnTypeMpls) return 2;
    if (psnType == cAtPwPsnTypeMef)  return 0;

    return 0;
    }

void ThaHbceStandbyGlbCtrlCached(ThaHbce self, uint32 *longReg, uint32 numDwords)
    {
    tThaHbceRegisterCache *cache = ThaHbceRegisterCache(self);

    if (!cache->registersCached)
        StandbyGlbCtrlCache(self);

    if (numDwords > cLongRegSizeInDwords)
        numDwords = cLongRegSizeInDwords;

    AtOsalMemCpy(longReg, cache->glbCtrlRegVal, sizeof(uint32) * numDwords);
    }

uint32 ThaHbcePwPsnHash(ThaHbce self, ThaPwAdapter pwAdapter, AtPwPsn psn, uint32 *remainBits)
    {
    if (self)
        return PwPsnHash(self, pwAdapter, psn, remainBits);
    return 0;
    }

void ThaHbceStandbyEntryHitCountIncrease(ThaHbce self, uint32 entryIndex)
    {
    if (self)
        StandbyEntryHitCountIncrease(self, entryIndex);
    }

void ThaHbceStandbyEntryHitCountDecrease(ThaHbce self, uint32 entryIndex)
    {
    if (self)
        StandbyEntryHitCountDecrease(self, entryIndex);
    }

uint32 ThaHbceStandbyEntryHitCountGet(ThaHbce self, uint32 entryIndex)
    {
    if (self)
        return HitCountGet(self, entryIndex);
    return 0;
    }

void ThaHbceStandbyCleanup(ThaHbce self)
    {
    if (self)
        mMethodsGet(self)->StandbyCleanup(self);
    }

uint32 ThaHbceMaxNumEntries(ThaHbce self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumEntries(self);
    return 0x0;
    }

void ThaHbceMasterInit(ThaHbce self)
    {
    if (self)
        mMethodsGet(self)->MasterInit(self);
    }

eAtRet ThaHbceSwInit(ThaHbce self)
    {
    if (self)
        return SwInit(self);
    return cAtErrorNullPointer;
    }

eAtRet ThaHbceAllEntriesDelete(ThaHbce self)
    {
    if (self)
        return AllEntriesDelete(self);
    return cAtErrorNullPointer;
    }

void ThaHbceEntryInit(ThaHbce self, uint32 entryIndex)
    {
    if (self)
        mMethodsGet(self)->EntryInit(self, entryIndex);
    }

eBool ThaHbcePwPsnIsSupported(AtPwPsn psn)
    {
    return PwPsnIsSupported(psn);
    }

eAtPwPsnType ThaHbcePsnTypeForHashing(ThaHbce self, AtPwPsn psn)
    {
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);
    AtUnused(self);

    if ((psnType == cAtPwPsnTypeMef) && (AtPwPsnTypeGet(AtPwPsnLowerPsnGet(psn)) == cAtPwPsnTypeMpls))
        psnType = cAtPwPsnTypeMpls;

    return psnType;
    }


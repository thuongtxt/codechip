/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaHbceMemoryPool.c
 *
 * Created Date: Mar 6, 2013
 *
 * Description : HBCE memory pool
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaHbceMemoryPool.h"
#include "ThaHbceEntityInternal.h"
#include "ThaHbceMemoryCell.h"
#include "ThaHbceMemoryCellContent.h"
#include "ThaHbce.h"
#include "ThaHbceEntry.h"

#include "../pw/ThaModuleClaPwReg.h"
#include "../pw/ThaModuleClaPwInternal.h"
#include "../controllers/ThaClaPwControllerInternal.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPoolIsValid(self) (self ? cAtTrue : cAtFalse)
#define mThis(self) ((tThaHbceMemoryPool *)self)
#define mInAccessible(self) ThaHbceEntityInAccessible((ThaHbceEntity)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaHbceMemoryPoolMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tThaHbceEntityMethods m_ThaHbceEntityOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHbceMemoryPool object = (ThaHbceMemoryPool)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(cells, ThaHbceMemoryPoolMaxNumCells(object));
    mEncodeUInt(numUsedCells);
    mEncodeUInt(maxNumCells);
    mEncodeObjectDescription(hbce);
    mEncodeUInt(hwAsyncInitState);
    mEncodeUInt(asyncInitState);
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "memory_pool";
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHbceMemoryPool);
    }

static uint32 MaxNumCells(ThaHbceMemoryPool self)
    {
	AtUnused(self);
    return 1024;
    }

static ThaHbceMemoryCell CellCreate(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return ThaHbceMemoryCellNew(self, cellIndex);
    }

static uint32 OffsetForCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
	AtUnused(self);
    return cellIndex;
    }

static eAtRet AllCellsCreate(ThaHbceMemoryPool self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryCell *allCells;
    uint32 numCells, i;

    /* Create memory to hold all of cells */
    numCells = ThaHbceMemoryPoolMaxNumCells(self);
    allCells = mMethodsGet(osal)->MemAlloc(osal, sizeof(ThaHbceMemoryCell) * numCells);
    if (allCells == NULL)
        return cAtErrorRsrcNoAvail;

    /* And all of concrete cells */
    for (i = 0; i < numCells; i++)
        {
        uint32 j;

        /* Delete all created cells if one of them cannot be created */
        allCells[i] = mMethodsGet(self)->CellCreate(self, i);
        if (allCells[i] == NULL)
            {
            for (j = 0; j < i; j++)
                AtObjectDelete((AtObject)allCells[j]);

            mMethodsGet(osal)->MemFree(osal, allCells);
            return cAtErrorRsrcNoAvail;
            }
        }

    self->cells = allCells;
    self->maxNumCells = numCells;

    return cAtOk;
    }

static eAtRet AllCellsDelete(ThaHbceMemoryPool self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 i;

    /* Delete all cells */
    if (self->cells == NULL)
        return cAtOk;
    for (i = 0; i < self->maxNumCells; i++)
        AtObjectDelete((AtObject)(self->cells[i]));

    /* Delete memory that holds all cells */
    mMethodsGet(osal)->MemFree(osal, self->cells);
    self->cells = NULL;
    self->numUsedCells = 0;

    return cAtOk;
    }

static AtIpCore Core(ThaHbceMemoryPool self)
    {
    return ThaHbceCoreGet(self->hbce);
    }

static uint32 PartOffset(ThaHbceMemoryPool self)
    {
    return ThaHbcePartOffset(self->hbce);
    }

static uint32 ClaHbceLookingUpInformationCtrl(ThaHbceMemoryPool self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet(claModule);
    return ThaClaPwControllerClaHbceLookingUpInformationCtrl(claPwController) + PartOffset(self);
    }

static eAtRet CellInit(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule claModule = (AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 regAddr;
    uint32 longRegVal[cLongRegSizeInDwords];

    if (claModule == NULL)
    	return cAtErrorObjectNotExist;

    mMethodsGet(osal)->MemInit(osal, longRegVal, 0, sizeof(longRegVal));
    regAddr = ClaHbceLookingUpInformationCtrl(self) + mMethodsGet(self)->OffsetForCell(self, cellIndex);
    mMethodsGet(claModule)->HwLongWriteOnCore(claModule,
                                              regAddr,
                                              longRegVal,
                                              cLongRegSizeInDwords,
                                              Core(self));

    return cAtOk;
    }

static eAtRet HwInit(ThaHbceEntity self)
    {
    uint32 i;
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;

    for (i = 0; i < ThaHbceMemoryPoolMaxNumCells(pool); i++)
        mMethodsGet(pool)->CellInit(pool, i);

    return cAtOk;
    }

static eAtRet Init(ThaHbceEntity self)
    {
    eAtRet ret = cAtOk;

    ret |= ThaHbceMemoryPoolSwInit(self);
    ret |= HwInit(self);

    return ret;
    }

static eAtRet AsyncInit(ThaHbceEntity self)
    {
    return ThaHbceMemoryPoolAsyncInitMain((ThaHbceMemoryPool)self);
    }

static void Delete(AtObject self)
    {
    AllCellsDelete((ThaHbceMemoryPool)self);
    m_AtObjectMethods->Delete(self);
    }

static eBool CellIndexIsValid(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return (cellIndex < ThaHbceMemoryPoolMaxNumCells(self)) ? cAtTrue : cAtFalse;
    }

static ThaHbceMemoryCell CellAtIndex(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    if (!mPoolIsValid(self))
        return NULL;

    if (!CellIndexIsValid(self, cellIndex))
        return NULL;

    if (self->cells)
        return self->cells[cellIndex];

    if (AllCellsCreate(self) == cAtOk)
        return self->cells[cellIndex];

    return NULL;
    }

static void Debug(ThaHbceEntity self)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;
    uint32 numCells = ThaHbceMemoryPoolMaxNumCells(pool);
    uint32 i;

    if ((numCells == 0) || (ThaHbceMemoryPoolNumUsedCells(pool) == 0))
        {
        AtPrintc(cSevNormal, "No cell is used\r\n");
        return;
        }

    for (i = 0; i < numCells; i++)
        {
        ThaHbceMemoryCell cell = CellAtIndex(pool, i);
        const char *cellDescription = AtObjectToString((AtObject)cell);
        if (cellDescription)
            AtPrintc(cSevNormal, "* Cell %4u: %s\r\n", i + 1, cellDescription);
        }
    }

static ThaClaPwController ClaPwController(ThaHbceMemoryPool self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return ThaModuleClaPwControllerGet(claModule);
    }

static uint32 ClaHbceFlowIdMask(ThaHbceMemoryPool self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet(claModule);
    return ThaClaPwControllerClaHbceFlowIdMask(claPwController);
    }

static uint8 ClaHbceFlowIdShift(ThaHbceMemoryPool self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet(claModule);
    return ThaClaPwControllerClaHbceFlowIdShift(claPwController);
    }

static void ApplyCellToHardware(ThaHbceMemoryPool self, uint32 cellIndex, uint8 page)
    {
    uint32 address;
    uint32 regVal = 0;
    ThaHbceMemoryCellContent cellContent;
    ThaPwAdapter pwAdapter;
    AtHal hal = AtIpCoreHalGet(Core(self));
    uint32 storeId;
    ThaClaPwController pwController = ClaPwController(self);
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    cellContent = ThaHbceMemoryCellContentGet(CellAtIndex(self, cellIndex));
    if (cellContent == NULL)
        return;

    if (ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL) == NULL)
        return;

    storeId = ThaHbceMemoryCellContentRemainedBitsGet(cellContent, NULL)[0];
    pwAdapter = ThaPwHeaderControllerAdapterGet(ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent));

    mFieldIns(&regVal,
              ClaHbceFlowIdMask(self),
              ClaHbceFlowIdShift(self),
              AtChannelHwIdGet((AtChannel)pwAdapter));

    mFieldIns(&regVal,
              mFieldMask(pwController, ClaHbceFlowEnb),
              mFieldShift(pwController, ClaHbceFlowEnb),
              ((pwAdapter) && AtChannelIsEnabled((AtChannel)pwAdapter)) ? 1 : 0);

    mFieldIns(&regVal,
              mFieldMask(pwController, ClaHbceStoreId),
              mFieldShift(pwController, ClaHbceStoreId),
              storeId);

    address = ClaHbceLookingUpInformationCtrl(self) + (ThaHbceMemoryPoolMaxNumCells(self) * page) + cellIndex;
    mModuleHwWriteByHal(claModule, address, regVal, hal);
    }

static ThaHbceMemoryCell NextCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);
    return CellAtIndex(self, cellIndex + 1);
    }

static ThaHbceMemoryCell PreviousCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);

    if (cellIndex == 0)
        return NULL;

    return CellAtIndex(self, cellIndex - 1);
    }

static eBool IsLastCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);

    return (cellIndex == (ThaHbceMemoryPoolNumUsedCells(self) - 1)) ? cAtTrue : cAtFalse;
    }

static void CellContentExchange(ThaHbceMemoryCell cell1, ThaHbceMemoryCell cell2)
    {
    ThaHbceMemoryCellContent cell1Content = ThaHbceMemoryCellContentGet(cell1);
    ThaHbceMemoryCellContent cell2Content = ThaHbceMemoryCellContentGet(cell2);

    ThaHbceMemoryCellContentSet(cell1, cell2Content);
    ThaHbceMemoryCellContentSet(cell2, cell1Content);

    ThaHbceMemoryCellContentCellSet(cell1Content, cell2);
    ThaHbceMemoryCellContentCellSet(cell2Content, cell1);
    }

static ThaHbceMemoryCell ShiftDown(ThaHbceMemoryPool self, uint32 position)
    {
    ThaHbceMemoryCell thisCell = CellAtIndex(self, position);
    uint32 numUsedCells;
    ThaHbceMemoryCell currentCell, previousCell;

    if (thisCell == NULL)
        return NULL;

    /* Check if pool has enough space for shift down operation */
    numUsedCells = ThaHbceMemoryPoolNumUsedCells(self);
    if (numUsedCells == ThaHbceMemoryPoolMaxNumCells(self))
        return NULL;

    /* Move cell content until we reach start cell */
    currentCell  = CellAtIndex(self, numUsedCells);
    previousCell = CellAtIndex(self, ThaHbceMemoryCellIndexGet(currentCell) - 1);
    while (previousCell && (previousCell != thisCell))
        {
        CellContentExchange(currentCell, previousCell);

        /* Continue */
        currentCell  = previousCell;
        previousCell = PreviousCell(self, currentCell);
        }

    /* Last time */
    if (previousCell)
        CellContentExchange(currentCell, previousCell);
    else
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning,
                    "How previous cell is NULL when shift down from position %u?\r\n", position);
        }

    /* Room is available, reset its content */
    AtObjectDelete((AtObject)ThaHbceMemoryCellContentGet(thisCell));
    ThaHbceMemoryCellContentSet(thisCell, NULL);

    return thisCell;
    }

static ThaHbceMemoryCell ShiftUp(ThaHbceMemoryPool self, uint32 position)
    {
    ThaHbceMemoryCell currentCell = CellAtIndex(self, position);
    ThaHbceMemoryCell nextCell = NextCell(self, currentCell);
    uint32 cell_i = 0;

    while (cell_i <= ThaHbceMemoryPoolMaxNumCells(self))
        {
        CellContentExchange(currentCell, nextCell);

        if (IsLastCell(self, nextCell))
            return nextCell;

        /* Next swapping */
        currentCell = nextCell;
        nextCell = NextCell(self, currentCell);
        cell_i = cell_i + 1;
        }

    return NULL;
    }

static ThaHbceMemoryCell StandbyUseCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryCell cell = CellAtIndex(self, cellIndex);
    ThaHbceEntityInit((ThaHbceEntity)cell);
    self->numUsedCells = self->numUsedCells + 1;

    return cell;
    }

static eAtRet StandbyUnUseCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryCell cell = CellAtIndex(self, cellIndex);
    ThaHbceEntityInit((ThaHbceEntity)cell);
    self->numUsedCells = self->numUsedCells - 1;

    return cAtOk;
    }

static ThaHbceMemoryCell UseCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryCell cell;
    uint32 numUsedCells = ThaHbceMemoryPoolNumUsedCells(self);

    if (mInAccessible(self))
        return StandbyUseCell(self, cellIndex);

    /* Not any cell can be used */
    if (cellIndex > numUsedCells)
        return NULL;

    /* out of cell */
    if (numUsedCells >= ThaHbceMemoryPoolMaxNumCells(self))
        {
        AtModuleLog((AtModule)ThaHbceEntityClaModuleGet((ThaHbceEntity)self), cAtLogLevelCritical, AtSourceLocation, "ERROR: HbceMemoryPool is Out of cell, numUsedCells = %d", numUsedCells);
        return NULL;
        }

    /* If cell index is in the middle of the pool, need to shift down to make room */
    if (cellIndex == numUsedCells)
        cell = CellAtIndex(self, cellIndex);
    else
        cell = ShiftDown(self, cellIndex);

    /* Clean it */
    ThaHbceEntityInit((ThaHbceEntity)cell);
    self->numUsedCells = self->numUsedCells + 1;

    return cell;
    }

static eAtRet UnUseCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint32 numUsedCells = ThaHbceMemoryPoolNumUsedCells(self);
    uint32 cellIndex = ThaHbceMemoryCellIndexGet(cell);
    eBool isMiddleCell;
    ThaHbceMemoryCell freeCell = cell;

    if (cell == NULL)
        return cAtErrorNullPointer;

    if (mInAccessible(self))
        return StandbyUnUseCell(self, cellIndex);

    /* Need to shift up for middle cell */
    isMiddleCell = (cellIndex < (numUsedCells - 1)) ? cAtTrue : cAtFalse;
    if (isMiddleCell)
        freeCell = ShiftUp(self, cellIndex);

    /* Clean this cell */
    ThaHbceEntityInit((ThaHbceEntity)freeCell);

    self->numUsedCells = numUsedCells - 1;

    return cAtOk;
    }

static eAtRet CellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    uint8 activePage = ThaHbceActivePage(self->hbce);
    uint32 pageStartCell = ThaHbceMemoryPoolMaxNumCells(self) * activePage;
    uint32 address = ClaHbceLookingUpInformationCtrl(self) + pageStartCell + ThaHbceMemoryCellIndexGet(cell);
    AtHal hal = AtIpCoreHalGet(Core(self));
    uint32 regVal;
    ThaHbceMemoryCellContent cellContent = ThaHbceMemoryCellContentGet(cell);
    ThaClaPwController pwController = ClaPwController(self);
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    /* Update hardware */
    regVal = mModuleHwReadByHal(claModule, address, hal);
    mFieldIns(&regVal, mFieldMask(pwController, ClaHbceFlowEnb), mFieldShift(pwController, ClaHbceFlowEnb), enable ? 1 : 0);
    mModuleHwWriteByHal(claModule, address, regVal, hal);

    /* Update software database */
    ThaHbceMemoryCellContentEnable(cellContent, enable);

    return cAtOk;
    }

static eBool CellIsEnabled(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    uint8 activePage = ThaHbceActivePage(self->hbce);
    uint32 pageStartCell = ThaHbceMemoryPoolMaxNumCells(self) * activePage;
    uint32 address = ClaHbceLookingUpInformationCtrl(self) + pageStartCell + ThaHbceMemoryCellIndexGet(cell);
    AtHal hal = AtIpCoreHalGet(Core(self));
    ThaClaPwController pwController = ClaPwController(self);
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);

    return (mModuleHwReadByHal(claModule, address, hal) & mFieldMask(pwController, ClaHbceFlowEnb)) ? cAtTrue : cAtFalse;
    }

static ThaPwHeaderController StandbyRestoreCellHeaderController(ThaHbceMemoryPool self, ThaPwAdapter pwAdapter, ThaHbceMemoryCell cell, uint8 page)
    {
    AtUnused(self);
    AtUnused(cell);
    AtUnused(page);
    return ThaPwAdapterHeaderController(pwAdapter);
    }

static uint32 StandbyRestoreCellFromHardware(ThaHbceMemoryPool self, ThaHbceEntry entry, uint32 cellIndex, uint8 page)
    {
    uint32 address;
    uint32 regVal = 0;
    ThaHbceMemoryCellContent cellContent;
    ThaPwAdapter pwAdapter;
    AtHal hal = AtIpCoreHalGet(Core(self));
    uint32 storeId;
    ThaClaPwController pwController = ClaPwController(self);
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet(claModule), cAtModulePw);
    uint32 flowId;
    ThaHbceMemoryCell cell;
    eBool enabled;
    ThaPwHeaderController headerController;
    uint32 remainBits[2];
    eAtRet ret = cAtOk;
    AtPw pw;
    AtPwGroup pwGroup;
    uint32 remaingingBits[2];
    uint32 entryIndex = ThaHbceEntryIndexGet(entry);

    address = ClaHbceLookingUpInformationCtrl(self) + (ThaHbceMemoryPoolMaxNumCells(self) * page) + cellIndex;
    regVal = mModuleHwReadByHal(claModule, address, hal);

    /* The cell may be not configured at all and it does not belong to any PW */
    mFieldGet(regVal, ClaHbceFlowIdMask(self), ClaHbceFlowIdShift(self), uint32, &flowId);
    enabled = (regVal & mFieldMask(pwController, ClaHbceFlowEnb)) ? cAtTrue : cAtFalse;
    mFieldGet(regVal, mFieldMask(pwController, ClaHbceStoreId), mFieldShift(pwController, ClaHbceStoreId), uint32, &storeId);

    pwAdapter = ThaModulePwHwPwId2Adapter(pwModule, flowId);
    if (pwAdapter == NULL)
        return 0;

    /* Enabling should match */
    if (enabled != ThaPwAdapterIsEnabled(pwAdapter))
        return 0;

    /* If entry is specified, it must match with the hash value. */
    pw = ThaPwAdapterPwGet(pwAdapter);
    pwGroup = AtPwHsGroupGet(pw);
    AtOsalMemInit(remaingingBits, 0, sizeof(remaingingBits));
    if (entryIndex != cInvalidUint32)
        {
        uint32 hashIndex = ThaHbcePwPsnHash(ThaHbceMemoryPoolHbceGet(self), pwAdapter, AtPwPsnGet((AtPw)pwAdapter), remaingingBits);

        /* This may not be the primary header controller, and it can be backup */
        if ((hashIndex != entryIndex) && (pwGroup != NULL))
            hashIndex = ThaHbcePwPsnHash(ThaHbceMemoryPoolHbceGet(self), pwAdapter, AtPwBackupPsnGet((AtPw)pwAdapter), remaingingBits);

        /* No header controller match this */
        if (hashIndex != entryIndex)
            return 0;
        }

    /* TODO: Need to compare remaining bit also. */

    /* Have enough information, restore cell */
    cell = CellAtIndex(self, cellIndex);
    ret = ThaHbceEntityInit((ThaHbceEntity)cell);
    if (ret != cAtOk)
        {
        AtModuleLog(claModule, cAtLogLevelCritical, AtSourceLocation,
                    "Initialize cell %d fail, ret = %s\r\n",
                    cellIndex + 1,
                    AtRet2String(ret));
        }
    cellContent = ThaHbceMemoryCellContentGet(cell);

    /* Restore */
    headerController = mMethodsGet(self)->StandbyRestoreCellHeaderController(self, pwAdapter, cell, page);
    ThaPwHeaderControllerHbceMemoryCellContentSet(headerController, cellContent);
    ThaHbceMemoryCellContentPwHeaderControllerSet(cellContent, headerController);
    ThaHbceMemoryCellContentEnable(cellContent, enabled);
    remainBits[0] = storeId;
    remainBits[1] = 0;
    ThaHbceMemoryCellContentRemainedBitsSet(cellContent, remainBits, 2);

    /* This cell is used, so increase number of used cells */
    self->numUsedCells = self->numUsedCells + 1;

    return 0;
    }

static void NumUsedCellsReset(ThaHbceMemoryPool self)
    {
    self->numUsedCells = 0;
    }

static void MethodsInit(ThaHbceMemoryPool self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxNumCells);
        mMethodOverride(m_methods, CellCreate);
        mMethodOverride(m_methods, OffsetForCell);
        mMethodOverride(m_methods, ApplyCellToHardware);
        mMethodOverride(m_methods, UseCell);
        mMethodOverride(m_methods, UnUseCell);
        mMethodOverride(m_methods, CellEnable);
        mMethodOverride(m_methods, CellIsEnabled);
        mMethodOverride(m_methods, CellInit);
        mMethodOverride(m_methods, StandbyRestoreCellHeaderController);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaHbceEntity(ThaHbceMemoryPool self)
    {
    ThaHbceEntity hbceObject = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, mMethodsGet(hbceObject), sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, Init);
        mMethodOverride(m_ThaHbceEntityOverride, AsyncInit);
        mMethodOverride(m_ThaHbceEntityOverride, Debug);
        }

    mMethodsSet(hbceObject, &m_ThaHbceEntityOverride);
    }

static void OverrideAtObject(ThaHbceMemoryPool self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaHbceMemoryPool self)
    {
    OverrideAtObject(self);
    OverrideThaHbceEntity(self);
    }

ThaHbceMemoryPool ThaHbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceEntityObjectInit((ThaHbceEntity)self, ThaHbceEntityClaModuleGet((ThaHbceEntity)hbce)) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    self->hbce = hbce;

    return self;
    }

ThaHbceMemoryPool ThaHbceMemoryPoolNew(ThaHbce hbce)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryPool newPool = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPool == NULL)
        return NULL;

    /* Construct it */
    return ThaHbceMemoryPoolObjectInit(newPool, hbce);
    }

uint32 ThaHbceMemoryPoolMaxNumCells(ThaHbceMemoryPool self)
    {
    if (!mPoolIsValid(self))
        return 0;

    if (self->maxNumCells == 0)
        self->maxNumCells = mMethodsGet(self)->MaxNumCells(self);

    return self->maxNumCells;
    }

uint32 ThaHbceMemoryPoolNumUsedCells(ThaHbceMemoryPool self)
    {
    if (mPoolIsValid(self))
        return self->numUsedCells;

    return 0;
    }

ThaHbceMemoryCell ThaHbceMemoryPoolUseCell(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    if (!mPoolIsValid(self))
        return NULL;

    return mMethodsGet(self)->UseCell(self, cellIndex);
    }

eAtRet ThaHbceMemoryPoolUnUseCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    if (!mPoolIsValid(self))
        return cAtErrorInvlParm;

    return mMethodsGet(self)->UnUseCell(self, cell);
    }

eAtRet ThaHbceMemoryPoolApplyToHardware(ThaHbceMemoryPool self, uint8 page)
    {
    uint32 cellIndex;

    if (!mPoolIsValid(self))
        return cAtErrorInvlParm;

    for (cellIndex = 0; cellIndex < ThaHbceMemoryPoolMaxNumCells(self); cellIndex ++)
        mMethodsGet(self)->ApplyCellToHardware(self, cellIndex, page);

    return cAtOk;
    }

ThaHbceMemoryCell ThaHbceMemoryPoolCellWarmRestore(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    ThaHbceMemoryCell cell;

    if (!mPoolIsValid(self))
        return NULL;

    /* Just get the cell at desire index, init it and return */
    cell = CellAtIndex(self, cellIndex);

    /* Clean it */
    ThaHbceEntityInit((ThaHbceEntity)cell);
    self->numUsedCells = self->numUsedCells + 1;

    return cell;
    }

uint32 ThaHbceMemoryPoolCellHwFlowIdGet(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    uint8 page = ThaHbceActivePage(self->hbce);
    AtHal hal = AtIpCoreHalGet(Core(self));
    uint32 address = ClaHbceLookingUpInformationCtrl(self) + (ThaHbceMemoryPoolMaxNumCells(self) * page) + cellIndex;
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    uint32 cellVal = mModuleHwReadByHal(claModule, address, hal);
    uint32 flowId;

    mFieldGet(cellVal, ClaHbceFlowIdMask(self), ClaHbceFlowIdShift(self), uint32, &flowId);
    return flowId;
    }

ThaHbceMemoryCell ThaHbceMemoryPoolCellAtIndex(ThaHbceMemoryPool self, uint32 cellIndex)
    {
    return (self) ? CellAtIndex(self, cellIndex) : NULL;
    }

uint32 ThaHbceMemoryPoolClaHbceLookingUpInformationCtrl(ThaHbceMemoryPool self)
    {
    return (self) ? ClaHbceLookingUpInformationCtrl(self) : 0xFFFFFFFF;
    }

eAtRet ThaHbceMemoryPoolCellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->CellEnable(self, cell, enable);
    return cAtErrorNullPointer;
    }

eBool ThaHbceMemoryPoolCellIsEnabled(ThaHbceMemoryPool self, ThaHbceMemoryCell cell)
    {
    if (self)
        return mMethodsGet(self)->CellIsEnabled(self, cell);
    return cAtFalse;
    }

ThaHbce ThaHbceMemoryPoolHbceGet(ThaHbceMemoryPool self)
    {
    return (self) ? self->hbce : NULL;
    }

uint32 ThaHbceMemoryPoolStandbyRestoreCellFromHardware(ThaHbceMemoryPool self, ThaHbceEntry entry, uint32 cellIndex, uint8 page)
    {
    if (self)
        return StandbyRestoreCellFromHardware(self, entry, cellIndex, page);
    return 0;
    }

eAtRet ThaHbceMemoryPoolSwInit(ThaHbceEntity self)
    {
    ThaHbceMemoryPool pool = (ThaHbceMemoryPool)self;

    AllCellsDelete(pool);
    return AllCellsCreate(pool);
    }

void ThaHbceMemoryPoolNumUsedCellsReset(ThaHbceMemoryPool self)
    {
    if (self)
        NumUsedCellsReset(self);
    }

void ThaHbceMemoryPoolApplyCellToHardware(ThaHbceMemoryPool self, uint32 cellIndex, uint8 page)
    {
    if (self)
        mMethodsGet(self)->ApplyCellToHardware(self, cellIndex, page);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbceClasses.h
 * 
 * Created Date: Mar 13, 2013
 *
 * Description : All HBCE related classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCECLASSES_H_
#define _THAHBCECLASSES_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHbceEntity            * ThaHbceEntity;
typedef struct tThaHbce                  * ThaHbce;
typedef struct tThaHbceEntry             * ThaHbceEntry;
typedef struct tThaHbceMemoryPool        * ThaHbceMemoryPool;
typedef struct tThaHbceMemoryCell        * ThaHbceMemoryCell;
typedef struct tThaHbceMemoryCellContent * ThaHbceMemoryCellContent;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCECLASSES_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbceEntityInternal.h
 * 
 * Created Date: Mar 6, 2013
 *
 * Description : HBCE top class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCEOBJECTINTERNAL_H_
#define _THAHBCEOBJECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/common/AtObjectInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

#include "ThaHbceEntity.h"
#include "ThaHbce.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cLongRegSizeInDwords 5

/*--------------------------- Macros -----------------------------------------*/
#define mFieldMask(module, field)                                              \
        mMethodsGet(module)->field##Mask(module)
#define mFieldShift(module, field)                                             \
        mMethodsGet(module)->field##Shift(module)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaHbceEntityMethods
    {
    eAtRet (*Init)(ThaHbceEntity self);
    eAtRet (*AsyncInit)(ThaHbceEntity self);
    void (*Debug)(ThaHbceEntity self);
    uint32 (*Restore)(ThaHbceEntity self, uint8 page);
    eBool (*ShouldOptimizeInit)(ThaHbceEntity self);
    }tThaHbceEntityMethods;

typedef struct tThaHbceEntity
    {
    tAtObject super;
    const tThaHbceEntityMethods *methods;

    /* Private data */
    AtModule claModule;
    }tThaHbceEntity;

typedef struct tThaHbceMethods
    {
    uint8 (*CodeLevel)(ThaHbce self);
    eBool (*HasPages)(ThaHbce self);

    /* Entry management */
    uint32 (*MaxNumEntries)(ThaHbce self);
    ThaHbceEntry (*EntryObjectCreate)(ThaHbce self, uint32 entryIndex);

    /* Memory pool */
    ThaHbceMemoryPool (*MemoryPoolCreate)(ThaHbce self);

    eAtRet (*PwEnable)(ThaHbce self, ThaPwAdapter pwAdapter, eBool enable);
    eBool (*PwIsEnabled)(ThaHbce self, ThaPwAdapter pwAdapter);
    eAtRet (*PwHeaderControllerEnable)(ThaHbce self, ThaPwHeaderController controller, eBool enable);
    eBool (*PwHeaderControllerIsEnabled)(ThaHbce self, ThaPwHeaderController controller);

    ThaHbceMemoryCell (*AllocateCellForPw)(ThaHbce self, ThaPwHeaderController controller, uint32 hashIndex);
    eAtRet (*Apply2Hardware)(ThaHbce self);
    eAtRet (*HashEntryApply)(ThaHbce self, uint32 entryIndex, uint8 page);
    ThaHbceMemoryCell (*SearchCellForPw)(ThaHbce self, ThaPwAdapter pwAdapter, ThaHbceEntry hashEntry);
    uint32 (*PsnHash)(ThaHbce self, uint8 partId, AtEthPort ethPort, uint32 label, eAtPwPsnType psnType, uint32 *remainBits);
    eBool  (*NewPwLabelExistInHashEntry)(ThaHbce self, ThaPwHeaderController controller, AtPwPsn newPsn, ThaHbceEntry hashEntry);
    eAtRet (*StandbyPwAdd)(ThaHbce self, ThaPwHeaderController controller);
    eAtRet (*StandbyPwRemove)(ThaHbce self, ThaPwHeaderController controller);
    void   (*StandbyCleanup)(ThaHbce self);

    /* Registers */
    uint32 (*EntryOffset)(ThaHbce self, uint32 entryIndex, uint8 pageId);
    uint32 (*CellGroupWorkingMask)(ThaHbce self);
    uint32 (*CellGroupWorkingShift)(ThaHbce self);

    /* Init */
    void (*EntryInitOnPage)(ThaHbce self, uint32 entryIndex, uint8 pageId);
    void (*EntryInit)(ThaHbce self, uint32 entryIndex);
    void (*MasterInit)(ThaHbce self);
    void (*AllEntriesInit)(ThaHbce self);
    }tThaHbceMethods;

typedef struct tThaHbceRegisterCache
    {
    uint32 glbCtrlRegVal[cLongRegSizeInDwords];
    eBool registersCached;
    }tThaHbceRegisterCache;

typedef struct tThaHbceStandbyEntry
    {
    uint32 entryIndex;
    uint32 hitCounts;
    }tThaHbceStandbyEntry;

typedef struct tThaHbceStandby
    {
    tThaHbceRegisterCache registersCache;
    AtList entries; /* Of tThaHbceStandbyEntry */
    }tThaHbceStandby;

typedef struct tThaHbce
    {
    tThaHbceEntity super;
    const tThaHbceMethods *methods;

    /* Private data */
    AtIpCore core;
    uint8 identifier;
    ThaHbceEntry *entries;
    ThaHbceMemoryPool memoryPool;

    /* For standby driver */
    tThaHbceStandby standby;

    /*Async init*/
    uint32 hwAsyncInitState;
    uint32 asyncInitState;
    }tThaHbce;

typedef struct tThaStmPwProductHbce
    {
    tThaHbce super;
    }tThaStmPwProductHbce;

typedef struct tThaHbceMemoryPoolMethods
    {
    uint32 (*MaxNumCells)(ThaHbceMemoryPool self);
    ThaHbceMemoryCell (*CellCreate)(ThaHbceMemoryPool self, uint32 cellIndex);
    uint32 (*OffsetForCell)(ThaHbceMemoryPool self, uint32 cellIndex);
    void (*ApplyCellToHardware)(ThaHbceMemoryPool self, uint32 cellIndex, uint8 page);
    ThaHbceMemoryCell (*UseCell)(ThaHbceMemoryPool self, uint32 cellIndex);
    eAtRet (*UnUseCell)(ThaHbceMemoryPool self, ThaHbceMemoryCell cell);
    eAtRet (*CellEnable)(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable);
    eAtRet (*CellHwEnable)(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable);
    eBool (*CellIsEnabled)(ThaHbceMemoryPool self, ThaHbceMemoryCell cell);
    eAtRet (*CellInit)(ThaHbceMemoryPool self, uint32 cellIndex);
    ThaPwHeaderController (*StandbyRestoreCellHeaderController)(ThaHbceMemoryPool self, ThaPwAdapter pwAdapter, ThaHbceMemoryCell cell, uint8 page);
    }tThaHbceMemoryPoolMethods;

typedef struct tThaHbceMemoryPool
    {
    tThaHbceEntity super;
    const tThaHbceMemoryPoolMethods *methods;

    /* Private data */
    ThaHbceMemoryCell *cells;
    uint32 maxNumCells;
    uint32 numUsedCells;
    ThaHbce hbce;

    /*Async init*/
    uint32 hwAsyncInitState;
    uint32 asyncInitState;
    }tThaHbceMemoryPool;

typedef struct tThaHbceMemoryCell
    {
    tThaHbceEntity super;

    /* Private data */
    uint32 cellIndex;
    ThaHbceMemoryCellContent cellContent;
    }tThaHbceMemoryCell;

typedef struct tThaHbceEntryMethods
	{
	uint32 (*NumFlowsGet)(ThaHbceEntry self);
	ThaHbceMemoryCellContent (*CellContentAtIndex)(ThaHbceEntry self, uint32 cellIndex);
	eAtRet (*CellContentObjectAdd)(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent);
	eAtRet (*CellContentObjectRemove)(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent);
	AtList (*FlowsListObjectCreate)(ThaHbceEntry self);
	}tThaHbceEntryMethods;

typedef struct tThaHbceEntry
	{
	tThaHbceEntity super;
    const tThaHbceEntryMethods *methods;

	/* Private data */
	AtList flows;
	uint32 entryIndex;
	ThaHbce hbce;

	/* Standby driver */
	tThaHbceStandbyEntry *standby;
	}tThaHbceEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceEntity ThaHbceEntityObjectInit(ThaHbceEntity self, AtModule claModule);
ThaHbce ThaHbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce ThaStmPwProductHbceObjectInit(ThaHbce self, uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbceMemoryCell ThaHbceMemoryCellObjectInit(ThaHbceMemoryCell self, ThaHbceMemoryPool memoryPool, uint32 cellIndex);
ThaHbceEntry ThaHbceEntryObjectInit(ThaHbceEntry self, ThaHbce hbce, uint32 entryIndex);
void ThaHbceEntryFlowListSerialize(ThaHbceEntry self, AtList flows, AtCoder encoder);

tThaHbceRegisterCache *ThaHbceRegisterCache(ThaHbce self);

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCEOBJECTINTERNAL_H_ */


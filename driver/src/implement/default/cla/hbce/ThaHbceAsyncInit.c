/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HBCE
 *
 * File        : ThaHbceAsyncInit.c
 *
 * Created Date: Aug 19, 2016
 *
 * Description : Async Init implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPsn.h"

#include "../../man/ThaDeviceInternal.h"
#include "../../pw/ThaPwInternal.h"
#include "../pw/ThaModuleClaPwReg.h"
#include "../pw/ThaModuleClaPwInternal.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"
#include "../controllers/ThaClaPwControllerInternal.h"

#include "ThaHbce.h"
#include "ThaHbceEntry.h"
#include "ThaHbceEntityInternal.h"
#include "ThaHbceMemoryPool.h"
#include "ThaHbceMemoryCell.h"
#include "ThaHbceMemoryCellContent.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

typedef enum eThaHbceInitState
    {
    cThaHbceInitStateAsyncMasterHwInit = 0,
    cThaHbceInitStateAsyncHashHwInit,
    cThaHbceInitStateSwInit,
    cThaHbceInitStateAsyncCellHwInit
    } eThaHbceInitState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 MaxEntryPerAsyncInitGet(ThaHbce self, uint32 state)
    {
    uint32 max0 = (state + 1)*1000;
    uint32 max1 = mMethodsGet(self)->MaxNumEntries(self);

    if (max0 < max1)
        return max0;
    return max1;
    }

static uint32 StartEntryPerAsyncInitGet(ThaHbce self, uint32 state)
    {
    AtUnused(self);
    return state * 1000;
    }

static void AsyncHbceHwInitStateSet(ThaHbce self, uint32 state)
    {
    self->hwAsyncInitState = state;
    }

static uint32 AsyncHbceHwInitStateGet(ThaHbce self)
    {
    return self->hwAsyncInitState;
    }

static eAtRet AllEntriesAsyncInit(ThaHbce self)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    uint32 state = AsyncHbceHwInitStateGet(self);
    uint32 maxEntry = MaxEntryPerAsyncInitGet(self, state);
    for (i = StartEntryPerAsyncInitGet(self, state); i < maxEntry; i++)
        ThaHbceEntryInit(self, i);

    state += 1;
    if (maxEntry == mMethodsGet(self)->MaxNumEntries(self))
        {
        ret = cAtOk;
        state = 0;
        }
    else
        ret = cAtErrorAgain;

    AsyncHbceHwInitStateSet(self, state);
    return ret;
    }

static void AsyncHbceInitStateSet(ThaHbce self, uint32 state)
    {
    self->asyncInitState = state;
    }

static uint32 AsyncHbceInitStateGet(ThaHbce self)
    {
    return self->asyncInitState;
    }

eAtRet ThaHbceAsyncInitMain(ThaHbceEntity self)
    {
    eAtRet ret = cAtOk;
    ThaHbce hbce = (ThaHbce)self;
    uint32 state = AsyncHbceInitStateGet(hbce);

    switch (state)
        {
        case cThaHbceInitStateAsyncMasterHwInit:
            /* Initialize it self */
            ThaHbceMasterInit(hbce);
            state = cThaHbceInitStateAsyncHashHwInit;
            ret = cAtErrorAgain;
            break;
        case cThaHbceInitStateAsyncHashHwInit:
            /* Initialize it self */
            ret = AllEntriesAsyncInit(hbce);
            if (ret == cAtOk)
                {
                state = cThaHbceInitStateSwInit;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cThaHbceInitStateAsyncMasterHwInit;
            break;
        case cThaHbceInitStateSwInit:
            ret = ThaHbceSwInit(hbce);
            if (ret != cAtOk)
                {
                state = cThaHbceInitStateAsyncMasterHwInit;
                break;
                }
            /* Create its memory pool */
            if (hbce->memoryPool == NULL)
                hbce->memoryPool = mMethodsGet(hbce)->MemoryPoolCreate(hbce);
            if (hbce->memoryPool == NULL)
                {
                ThaHbceAllEntriesDelete(hbce);
                ret = cAtErrorRsrcNoAvail;
                state = cThaHbceInitStateAsyncMasterHwInit;
                break;
                }
            state = cThaHbceInitStateAsyncCellHwInit;
            ret = cAtErrorAgain;
            break;
        case cThaHbceInitStateAsyncCellHwInit:
            /* Initialize this pool */
            ret = ThaHbceEntityAsyncInit((ThaHbceEntity)(hbce->memoryPool));
            if (ret == cAtOk || !AtDeviceAsyncRetValIsInState(ret))
                state = cThaHbceInitStateAsyncMasterHwInit;
            break;
        default:
            state = cThaHbceInitStateAsyncMasterHwInit;
            ret = cAtErrorDevFail;
        }

    AsyncHbceInitStateSet(hbce, state);
    return ret;
    }



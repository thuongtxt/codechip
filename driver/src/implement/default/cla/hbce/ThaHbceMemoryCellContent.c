/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaHbceMemoryCellContent.c
 *
 * Created Date: Mar 6, 2013
 *
 * Description : Memory cell content
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../pw/adapters/ThaPwAdapter.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"

#include "ThaHbceMemoryCell.h"
#include "ThaHbceMemoryCellContent.h"
#include "ThaHbceEntry.h"
#include "ThaHbceEntityInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaHbceMemoryCellRemainedBitsSizeInDword 2

/*--------------------------- Macros -----------------------------------------*/
#define mContentIsValid(self) 	(self ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaHbceMemoryCellContent
    {
    tThaHbceEntity super;

    /* Private data */
    ThaPwHeaderController controller;
    ThaHbceEntry hashEntry;
    ThaHbceMemoryCell cell; /* Cell containing this content */
    eBool enabled;
    uint32 remainedBits[cThaHbceMemoryCellRemainedBitsSizeInDword];
    uint8 remainedBitsSizeInDword;
    }tThaHbceMemoryCellContent;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceEntityMethods m_ThaHbceEntityOverride;
static tAtObjectMethods      m_AtObjectOverride;

/* To save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;
static const tThaHbceEntityMethods *m_ThaHbceEntityMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static ThaHbceMemoryCellContent ObjectInit(ThaHbceMemoryCellContent self, ThaHbceMemoryCell cell);

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaHbceMemoryCellContent);
    }

static void RemainedBitsInvalidate(ThaHbceMemoryCellContent self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self->remainedBits, 0xFF, sizeof(self->remainedBits));
    }

static eAtRet Init(ThaHbceEntity self)
    {
    ThaHbceMemoryCellContent cellContent = (ThaHbceMemoryCellContent)self;
    ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(cellContent);

    if (cell == NULL)
        return cAtErrorRsrcNoAvail;

    /* Just initialize this object */
    if (ObjectInit(cellContent, cell) == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static const char *RemaindBitString(ThaHbceMemoryCellContent self)
    {
    static char description[128];
    static char numberString[16];
    uint8 numRemainDwords, i;
    uint32 *remainedBits;
    AtOsal osal = AtSharedDriverOsalGet();

    numRemainDwords = 0;
    remainedBits = ThaHbceMemoryCellContentRemainedBitsGet(self, &numRemainDwords);
    if ((remainedBits == NULL) || (numRemainDwords == 0))
        return "None";

    mMethodsGet(osal)->MemInit(osal, description, 0, sizeof(description));
    for (i = 0; i < numRemainDwords; i++)
        {
        AtSprintf(numberString, "0x%08x", remainedBits[i]);
        if (((AtStrlen(description) + AtStrlen(numberString)) + 1) < sizeof(description))
            {
            AtStrcat(description, ",");
            AtStrcat(description, numberString);
            }
        }

    return &(description[1]); /* Start from 1 to ignore the first ',' */
    }

static const char *ToString(AtObject self)
    {
    static char description[128];
    ThaHbceMemoryCellContent cellContent = (ThaHbceMemoryCellContent)self;
    ThaHbceMemoryCell cell = ThaHbceMemoryCellContentCellGet(cellContent);
    ThaPwAdapter pwAdapter;
    ThaHbceEntry hashEntry;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 remain = sizeof(description);

    /* Nothing to show */
    pwAdapter = ThaPwHeaderControllerAdapterGet(ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent));
    hashEntry = ThaHbceMemoryCellContentHashEntryGet(cellContent);
    if ((pwAdapter == NULL) && (hashEntry == NULL))
        return NULL;

    mMethodsGet(osal)->MemInit(osal, description, 0, sizeof(description));

    /* PW */
    if (pwAdapter == NULL)
        AtSnprintf(description, remain, "PW: None");
    else
        AtSnprintf(description, remain, "PW: %4u", AtChannelIdGet((AtChannel)pwAdapter) + 1);

    /* Enable/disable status */
    remain = remain - AtStrlen(description);
    AtSnprintf(description, remain, "%s, %s", description, ThaHbceMemoryCellContentIsEnabled(cellContent) ? "Enabled " : "Disabled");

    /* Remained bits */
    remain = remain - AtStrlen(description);
    AtSnprintf(description, remain, "%s, Remained bits: %s", description, RemaindBitString(cellContent));

    /* Hash entry */
    remain = remain - AtStrlen(description);
    if (hashEntry == NULL)
        AtSnprintf(description, remain, "%s, Hash entry: None", description);
    else
        AtSnprintf(description, remain, "%s, Hash entry: %u", description, ThaHbceEntryIndexGet(hashEntry) + 1);

    remain = remain - AtStrlen(description);
    AtSnprintf(description, remain, "%s, Cell index: %d", description, ThaHbceMemoryCellIndexGet(cell) + 1);

    return description;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHbceMemoryCellContent object = (ThaHbceMemoryCellContent)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(controller);
    mEncodeObjectDescription(hashEntry);
    mEncodeObjectDescription(cell);

    mEncodeUInt(enabled);
    mEncodeUInt32Array(remainedBits, cThaHbceMemoryCellRemainedBitsSizeInDword);
    mEncodeUInt(remainedBitsSizeInDword);
    }

static eBool IsEmpty(ThaHbceMemoryCellContent self)
    {
    if ((self->controller != NULL) ||
        (self->hashEntry  != NULL))
        return cAtFalse;

    return cAtTrue;
    }

static void OverrideAtObject(ThaHbceMemoryCellContent self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaHbceEntity(ThaHbceMemoryCellContent self)
    {
    ThaHbceEntity entity = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntityMethods = mMethodsGet(entity);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, m_ThaHbceEntityMethods, sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, Init);
        }

    mMethodsSet(entity, &m_ThaHbceEntityOverride);
    }

static void Override(ThaHbceMemoryCellContent self)
    {
    OverrideThaHbceEntity(self);
    OverrideAtObject(self);
    }

static ThaHbceMemoryCellContent ObjectInit(ThaHbceMemoryCellContent self, ThaHbceMemoryCell cell)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceEntityObjectInit((ThaHbceEntity)self, ThaHbceEntityClaModuleGet((ThaHbceEntity)cell)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* And private data */
    self->cell = cell;

    return self;
    }

ThaHbceMemoryCellContent ThaHbceMemoryCellContentNew(ThaHbceMemoryCell cell)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryCellContent newContent = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newContent == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newContent, cell);
    }

eAtRet ThaHbceMemoryCellContentEnable(ThaHbceMemoryCellContent self, eBool enable)
    {
    if (!mContentIsValid(self))
        return cAtErrorInvlParm;

    self->enabled = enable;
    return cAtOk;
    }

eBool ThaHbceMemoryCellContentIsEnabled(ThaHbceMemoryCellContent self)
    {
    if (mContentIsValid(self))
        return self->enabled;

    return cAtFalse;
    }

eAtRet ThaHbceMemoryCellContentRemainedBitsSet(ThaHbceMemoryCellContent self, const uint32 *remainedBits, uint8 sizeInDwords)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!mContentIsValid(self))
        return cAtErrorInvlParm;

    if (sizeInDwords > cThaHbceMemoryCellRemainedBitsSizeInDword)
        return cAtErrorRsrcNoAvail;

    mMethodsGet(osal)->MemCpy(osal, self->remainedBits, remainedBits, sizeInDwords * sizeof(uint32));
    self->remainedBitsSizeInDword = sizeInDwords;

    return cAtOk;
    }

uint32 *ThaHbceMemoryCellContentRemainedBitsGet(ThaHbceMemoryCellContent self, uint8 *sizeInDwords)
    {
    if (!mContentIsValid(self))
        return NULL;

    if (sizeInDwords)
        *sizeInDwords = self->remainedBitsSizeInDword;

    return self->remainedBits;
    }

ThaHbceMemoryCell ThaHbceMemoryCellContentCellGet(ThaHbceMemoryCellContent self)
    {
    if (mContentIsValid(self))
        return self->cell;

    return NULL;
    }

eAtRet ThaHbceMemoryCellContentCellSet(ThaHbceMemoryCellContent self, ThaHbceMemoryCell cell)
    {
    if (!mContentIsValid(self))
        return cAtErrorInvlParm;

    self->cell = cell;
    return cAtOk;
    }

ThaPwHeaderController ThaHbceMemoryCellContentPwHeaderControllerGet(ThaHbceMemoryCellContent self)
    {
    if (mContentIsValid(self))
        return self->controller;

    return NULL;
    }

eAtRet ThaHbceMemoryCellContentPwHeaderControllerSet(ThaHbceMemoryCellContent self, ThaPwHeaderController controller)
    {
    if (!mContentIsValid(self))
        return cAtErrorInvlParm;

    self->controller = controller;
    return cAtOk;
    }

eAtRet ThaHbceMemoryCellContentHashEntrySet(ThaHbceMemoryCellContent self, ThaHbceEntry hashEntry)
    {
    if (!mContentIsValid(self))
        return cAtErrorInvlParm;

    self->hashEntry = hashEntry;
    return cAtOk;
    }

ThaHbceEntry ThaHbceMemoryCellContentHashEntryGet(ThaHbceMemoryCellContent self)
    {
    if (mContentIsValid(self))
        return self->hashEntry;

    return NULL;
    }

eBool ThaHbceMemoryCellContentIsEmpty(ThaHbceMemoryCellContent self)
    {
    if (self)
        return IsEmpty(self);
    return cAtTrue;
    }

void ThaHbceMemoryCellContentRemainedBitsInvalidate(ThaHbceMemoryCellContent self)
    {
    if (self)
        RemainedBitsInvalidate(self);
    }

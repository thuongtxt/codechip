/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaHbceMemoryCell.c
 *
 * Created Date: Mar 6, 2013
 *
 * Description : Memory pool cell
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaHbceMemoryCell.h"
#include "ThaHbceMemoryCellContent.h"
#include "ThaHbceEntityInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mCellIsValid(self) (self ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaHbceEntityMethods m_ThaHbceEntityOverride;
static tAtObjectMethods      m_AtObjectOverride;

/* Save super implementation */
static const tThaHbceEntityMethods *m_ThaHbceEntityMethods = NULL;
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHbceMemoryCell object = (ThaHbceMemoryCell)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(cellIndex);

    /* TODO: it would be cleaner if we do not check like this. Need to check
     * again the case that PW is deleted, its memory cell would be deleted also */
    if (ThaHbceMemoryCellContentIsEmpty(object->cellContent))
        AtCoderEncodeObject(encoder, NULL, "cellContent");
    else
        mEncodeObject(cellContent);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaHbceMemoryCell);
    }

static void Delete(AtObject self)
    {
    /* Delete its content */
    ThaHbceMemoryCell cell = (ThaHbceMemoryCell)self;
    AtObjectDelete((AtObject)(cell->cellContent));
    cell->cellContent = NULL;

    /* And fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static eAtRet Init(ThaHbceEntity self)
    {
    ThaHbceMemoryCell cell = (ThaHbceMemoryCell)self;

    /* Let the super does first */
    eAtRet ret = m_ThaHbceEntityMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Create cell content */
    if (cell->cellContent == NULL)
        cell->cellContent = ThaHbceMemoryCellContentNew(cell);
    if (cell->cellContent == NULL)
        return cAtErrorNullPointer;

    /* Initialize its cell content */
    return ThaHbceEntityInit((ThaHbceEntity)ThaHbceMemoryCellContentGet(cell));
    }

static const char *ToString(AtObject self)
    {
    ThaHbceMemoryCellContent cellContent = ThaHbceMemoryCellContentGet((ThaHbceMemoryCell)self);
    if (cellContent == NULL)
        return NULL;

    return AtObjectToString((AtObject)cellContent);
    }

static void OverrideAtObject(ThaHbceMemoryCell self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaHbceEntity(ThaHbceMemoryCell self)
    {
    ThaHbceEntity hbceObject = (ThaHbceEntity)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntityMethods = mMethodsGet(hbceObject);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, m_ThaHbceEntityMethods, sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, Init);
        }

    mMethodsSet(hbceObject, &m_ThaHbceEntityOverride);
    }

static void Override(ThaHbceMemoryCell self)
    {
    OverrideThaHbceEntity(self);
    OverrideAtObject(self);
    }

ThaHbceMemoryCell ThaHbceMemoryCellObjectInit(ThaHbceMemoryCell self, ThaHbceMemoryPool memoryPool, uint32 cellIndex)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceEntityObjectInit((ThaHbceEntity)self, ThaHbceEntityClaModuleGet((ThaHbceEntity)memoryPool)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);

    /* Setup private data */
    self->cellIndex = cellIndex;
    m_methodsInit = 1;
    
    return self;
    }

ThaHbceMemoryCell ThaHbceMemoryCellNew(ThaHbceMemoryPool memoryPool, uint32 cellIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceMemoryCell newCell = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCell == NULL)
        return NULL;

    /* Construct it */
    return ThaHbceMemoryCellObjectInit(newCell, memoryPool, cellIndex);
    }

eAtRet ThaHbceMemoryCellContentSet(ThaHbceMemoryCell self, ThaHbceMemoryCellContent content)
    {
    if (!mCellIsValid(self))
        return cAtErrorInvlParm;

    self->cellContent = content;
    return cAtOk;
    }

ThaHbceMemoryCellContent ThaHbceMemoryCellContentGet(ThaHbceMemoryCell self)
    {
    if (!mCellIsValid(self))
        return NULL;

    return self->cellContent;
    }

uint32 ThaHbceMemoryCellIndexGet(ThaHbceMemoryCell self)
    {
    if (mCellIsValid(self))
        return self->cellIndex;

    return 0;
    }

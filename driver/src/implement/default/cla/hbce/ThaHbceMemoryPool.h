/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbceMemoryPool.h
 * 
 * Created Date: Mar 6, 2013
 *
 * Description : HBCE memory pool
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCEMEMORYPOOL_H_
#define _THAHBCEMEMORYPOOL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHbceClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceMemoryPool ThaHbceMemoryPoolNew(ThaHbce hbce);
ThaHbceMemoryPool ThaHbceMemoryPoolObjectInit(ThaHbceMemoryPool self, ThaHbce hbce);
eAtRet ThaHbceMemoryPoolAsyncInitMain(ThaHbceMemoryPool self);
eAtRet ThaHbceMemoryPoolSwInit(ThaHbceEntity self);

/* Cell management */
uint32 ThaHbceMemoryPoolMaxNumCells(ThaHbceMemoryPool self);
uint32 ThaHbceMemoryPoolNumUsedCells(ThaHbceMemoryPool self);
ThaHbceMemoryCell ThaHbceMemoryPoolUseCell(ThaHbceMemoryPool self, uint32 cellIndex);
eAtRet ThaHbceMemoryPoolUnUseCell(ThaHbceMemoryPool self, ThaHbceMemoryCell cell);
eAtRet ThaHbceMemoryPoolCellEnable(ThaHbceMemoryPool self, ThaHbceMemoryCell cell, eBool enable);
eBool ThaHbceMemoryPoolCellIsEnabled(ThaHbceMemoryPool self, ThaHbceMemoryCell cell);
uint32 ThaHbceMemoryPoolStandbyRestoreCellFromHardware(ThaHbceMemoryPool self, ThaHbceEntry entry, uint32 cellIndex, uint8 page);
void ThaHbceMemoryPoolNumUsedCellsReset(ThaHbceMemoryPool self);

eAtRet ThaHbceMemoryPoolApplyToHardware(ThaHbceMemoryPool self, uint8 page);
uint32 ThaHbceMemoryPoolCellHwFlowIdGet(ThaHbceMemoryPool self, uint32 cellIndex);
ThaHbceMemoryCell ThaHbceMemoryPoolCellWarmRestore(ThaHbceMemoryPool self, uint32 cellIndex);
ThaHbceMemoryCell ThaHbceMemoryPoolCellAtIndex(ThaHbceMemoryPool self, uint32 cellIndex);
uint32 ThaHbceMemoryPoolClaHbceLookingUpInformationCtrl(ThaHbceMemoryPool self);
ThaHbce ThaHbceMemoryPoolHbceGet(ThaHbceMemoryPool self);
void ThaHbceMemoryPoolApplyCellToHardware(ThaHbceMemoryPool self, uint32 cellIndex, uint8 page);

/* Product concretes */
ThaHbceMemoryPool Tha60210031HbceMemoryPoolNew(ThaHbce hbce);
ThaHbceMemoryPool Tha60210011HbceMemoryPoolNew(ThaHbce hbce);
ThaHbceMemoryPool Tha60210012HbceMemoryPoolNew(ThaHbce hbce);
ThaHbceMemoryPool Tha60290022HbceMemoryPoolNew(ThaHbce hbce);
ThaHbceMemoryPool Tha60290022HbceMemoryPoolV3New(ThaHbce hbce);
ThaHbceMemoryPool Tha60290081HbceMemoryPoolNew(ThaHbce hbce);

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCEMEMORYPOOL_H_ */


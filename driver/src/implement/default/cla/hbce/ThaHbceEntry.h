/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbceEntry.h
 * 
 * Created Date: Mar 6, 2013
 *
 * Description : HBCE entry
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCEENTRY_H_
#define _THAHBCEENTRY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHbceClasses.h"
#include "ThaHbceEntity.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceEntry ThaHbceEntryNew(ThaHbce hbce, uint32 entryIndex);

uint32 ThaHbceEntryIndexGet(ThaHbceEntry self);
ThaHbceMemoryCellContent ThaHbceEntryFirstCellContentGet(ThaHbceEntry self);

uint32 ThaHbceEntryNumFlowsGet(ThaHbceEntry self);
eAtRet ThaHbceEntryCellContentAdd(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent);
eAtRet ThaHbceEntryCellContentRemove(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent);
ThaHbceMemoryCellContent ThaHbceEntryCellContentAtIndex(ThaHbceEntry self, uint32 cellIndex);
eBool ThaHbceEntryIsFull(ThaHbceEntry self);

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCEENTRY_H_ */


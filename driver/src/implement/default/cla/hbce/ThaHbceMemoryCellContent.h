/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbceMemoryCellContent.h
 * 
 * Created Date: Mar 6, 2013
 *
 * Description : Memory pool cell content
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCEMEMORYCELLCONTENT_H_
#define _THAHBCEMEMORYCELLCONTENT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../pw/adapters/ThaPwAdapter.h"
#include "ThaHbceClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceMemoryCellContent ThaHbceMemoryCellContentNew(ThaHbceMemoryCell cell);

eAtRet ThaHbceMemoryCellContentEnable(ThaHbceMemoryCellContent self, eBool enable);
eBool ThaHbceMemoryCellContentIsEnabled(ThaHbceMemoryCellContent self);

eAtRet ThaHbceMemoryCellContentRemainedBitsSet(ThaHbceMemoryCellContent self, const uint32 *remainedBits, uint8 sizeInDwords);
uint32 *ThaHbceMemoryCellContentRemainedBitsGet(ThaHbceMemoryCellContent self, uint8 *sizeInDwords);

ThaHbceMemoryCell ThaHbceMemoryCellContentCellGet(ThaHbceMemoryCellContent self);
eAtRet ThaHbceMemoryCellContentCellSet(ThaHbceMemoryCellContent self, ThaHbceMemoryCell cell);

ThaPwHeaderController ThaHbceMemoryCellContentPwHeaderControllerGet(ThaHbceMemoryCellContent self);
eAtRet ThaHbceMemoryCellContentPwHeaderControllerSet(ThaHbceMemoryCellContent self, ThaPwHeaderController controller);

eAtRet ThaHbceMemoryCellContentHashEntrySet(ThaHbceMemoryCellContent self, ThaHbceEntry hashEntry);
ThaHbceEntry ThaHbceMemoryCellContentHashEntryGet(ThaHbceMemoryCellContent self);

eBool ThaHbceMemoryCellContentIsEmpty(ThaHbceMemoryCellContent self);
void ThaHbceMemoryCellContentRemainedBitsInvalidate(ThaHbceMemoryCellContent self);

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCEMEMORYCELLCONTENT_H_ */


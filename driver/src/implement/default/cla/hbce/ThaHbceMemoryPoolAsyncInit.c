/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : HBCE
 *
 * File        : ThaHbceMemoryPoolAsyncInit.c
 *
 * Created Date: Aug 19, 2016
 *
 * Description : Async init implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaHbceMemoryPool.h"
#include "ThaHbceEntityInternal.h"
#include "ThaHbceMemoryCell.h"
#include "ThaHbceMemoryCellContent.h"
#include "ThaHbce.h"
#include "ThaHbceEntry.h"

#include "../pw/ThaModuleClaPwReg.h"
#include "../pw/ThaModuleClaPwInternal.h"
#include "../controllers/ThaClaPwControllerInternal.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPoolIsValid(self) (self ? cAtTrue : cAtFalse)
#define mThis(self) ((tThaHbceMemoryPool *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eThaHbceMemoryPoolInitState
    {
    cThaHbceMemoryPoolInitStateSwInit = 0,
    cThaHbceMemoryPoolInitStateHwCellInit
    } eThaHbceMemoryPoolInitState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 MaxCellPerAsyncInitGet(ThaHbceMemoryPool self, uint32 state)
    {
    uint32 max0 = (state + 1)*1000;
    uint32 max1 = ThaHbceMemoryPoolMaxNumCells(self);

    if (max0 < max1)
        return max0;
    return max1;
    }

static uint32 StartCellPerAsyncInitGet(ThaHbceMemoryPool self, uint32 state)
    {
    AtUnused(self);
    return state * 1000;
    }

static void AsyncMemoryPoolHwInitStateSet(ThaHbceMemoryPool self, uint32 state)
    {
    self->hwAsyncInitState = state;
    }

static uint32 AsyncMemoryPoolHwInitStateGet(ThaHbceMemoryPool self)
    {
    return self->hwAsyncInitState;
    }

static eAtRet ThaHbceMemoryPoolAsyncHwInit(ThaHbceMemoryPool self)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    uint32 state = AsyncMemoryPoolHwInitStateGet(self);
    uint32 maxEntry = MaxCellPerAsyncInitGet(self, state);

    for (i = StartCellPerAsyncInitGet(self, state); i < maxEntry; i++)
        mMethodsGet(self)->CellInit(self, i);

    state += 1;
    if (maxEntry == ThaHbceMemoryPoolMaxNumCells(self))
        {
        ret = cAtOk;
        state = 0;
        }
    else
        ret = cAtErrorAgain;

    AsyncMemoryPoolHwInitStateSet(self, state);
    return ret;
    }

static void AsyncHbceMemoryPoolInitStateSet(ThaHbceMemoryPool self, uint32 state)
    {
    self->asyncInitState = state;
    }

static uint32 AsyncHbceMemoryPoolInitStateGet(ThaHbceMemoryPool self)
    {
    return self->asyncInitState;
    }

eAtRet ThaHbceMemoryPoolAsyncInitMain(ThaHbceMemoryPool self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncHbceMemoryPoolInitStateGet(self);

    switch (state)
        {
        case cThaHbceMemoryPoolInitStateSwInit:
            ret = ThaHbceMemoryPoolSwInit((ThaHbceEntity)self);
            if (ret == cAtOk)
                {
                state = cThaHbceMemoryPoolInitStateHwCellInit;
                ret = cAtErrorAgain;
                }
            break;
        case cThaHbceMemoryPoolInitStateHwCellInit:
          ret = ThaHbceMemoryPoolAsyncHwInit(self);
          if (ret == cAtOk || !AtDeviceAsyncRetValIsInState(ret))
              state = cThaHbceMemoryPoolInitStateSwInit;
          break;
        default:
            ret = cThaHbceMemoryPoolInitStateSwInit;
            ret = cAtErrorDevFail;
        }

    AsyncHbceMemoryPoolInitStateSet(self, state);
    return ret;
    }

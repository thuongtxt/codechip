/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbce.h
 * 
 * Created Date: Mar 6, 2013
 *
 * Description : Hash Base Classification Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCE_H_
#define _THAHBCE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHbceClasses.h"
#include "../../pw/adapters/ThaPwAdapter.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concrete */
ThaHbce ThaHbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);

AtIpCore ThaHbceCoreGet(ThaHbce self);
uint32 ThaHbcePartOffset(ThaHbce self);
void ThaHbceMasterInit(ThaHbce self);
eAtRet ThaHbceSwInit(ThaHbce self);
eAtRet ThaHbceAllEntriesDelete(ThaHbce self);
void ThaHbceEntryInit(ThaHbce self, uint32 entryIndex);
eAtRet ThaHbceAsyncInitMain(ThaHbceEntity self);

eAtRet ThaHbcePwAdd(ThaHbce self, ThaPwHeaderController controller);
eAtRet ThaHbcePwRemove(ThaHbce self, ThaPwHeaderController controller);
eAtRet ThaHbceMemoryCellContentRemove(ThaHbce self, ThaHbceMemoryCellContent cellContent);
eAtRet ThaHbceApply2Hardware(ThaHbce self);
eBool ThaHbcePwIsEnabled(ThaHbce self, ThaPwAdapter pwAdapter);
eAtRet ThaHbcePwEnable(ThaHbce self, ThaPwAdapter pw, eBool enable);
eBool ThaHbcePsnCanBeUsed(ThaHbce self, ThaPwHeaderController controller, AtPwPsn psn);
uint32 ThaHbcePsnLabelForHashing(ThaHbce self, AtPwPsn psn);
ThaPwAdapter ThaHbcePwAdapterByPsn(ThaHbce self, uint32 label, eAtPwPsnType psnType, AtEthPort port);
eAtRet ThaHbcePwWarmRestore(ThaHbce self, ThaPwHeaderController controller);
uint32 ThaHbcePwPsnHash(ThaHbce self, ThaPwAdapter pwAdapter, AtPwPsn psn, uint32 *remainBits);
eBool ThaHbcePwPsnIsSupported(AtPwPsn psn);

uint8 ThaHbceActivePage(ThaHbce self);
void ThaHbcePageSwitch(ThaHbce self, uint8 toPage);
uint8 ThaHbceStandbyPage(ThaHbce self);

ThaHbceMemoryPool ThaHbceMemoryPoolGet(ThaHbce self);
eBool ThaHbceNewPwLabelExistInHashEntry(ThaHbce self, ThaPwHeaderController controller, AtPwPsn newPsn, ThaHbceEntry hashEntry);
ThaHbceEntry ThaHbceEntryAtIndex(ThaHbce self, uint32 entryIndex);
uint32 ThaHbceCLAHBCEHashingTabCtrl(ThaHbce self);
uint32 ThaHbceMaxNumEntries(ThaHbce self);

eAtRet ThaHbcePwHeaderControllerEnable(ThaHbce self, ThaPwHeaderController controller, eBool enable);
eBool ThaHbcePwHeaderControllerIsEnabled(ThaHbce self, ThaPwHeaderController controller);

uint8 ThaHbcePktType(eAtPwPsnType psnType);
eAtPwPsnType ThaHbcePsnTypeForHashing(ThaHbce self, AtPwPsn psn);

/* For standby driver */
void ThaHbceStandbyGlbCtrlCached(ThaHbce self, uint32 *longReg, uint32 numDwords);
void ThaHbceStandbyEntryHitCountIncrease(ThaHbce self, uint32 entryIndex);
void ThaHbceStandbyEntryHitCountDecrease(ThaHbce self, uint32 entryIndex);
uint32 ThaHbceStandbyEntryHitCountGet(ThaHbce self, uint32 entryIndex);
void ThaHbceStandbyCleanup(ThaHbce self);

/* Product concretes */
ThaHbce Tha60031031HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce Tha60210031HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce Tha60210011HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce Tha60210012HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce Tha60290022HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce Tha60290022HbceV3New(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce ThaStmPwProductHbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);
ThaHbce Tha60290081HbceNew(uint8 hbceId, AtModule claModule, AtIpCore core);

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbceEntity.h
 * 
 * Created Date: Mar 6, 2013
 *
 * Description : HBCE top class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCEOBJECT_H_
#define _THAHBCEOBJECT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHbceClasses.h"
#include "AtObject.h" /* Super class */
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet ThaHbceEntityAsyncInit(ThaHbceEntity self);
eAtRet ThaHbceEntityInit(ThaHbceEntity self);
AtModule ThaHbceEntityClaModuleGet(ThaHbceEntity self);
void ThaHbceEntityDebug(ThaHbceEntity self);
uint32 ThaHbceEntityRestore(ThaHbceEntity self, uint8 page);
eBool ThaHbceEntityShouldOptimizeInit(ThaHbceEntity self);
eBool ThaHbceEntityInAccessible(ThaHbceEntity self);
eBool ThaHbceEntityAccessible(ThaHbceEntity self);

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCEOBJECT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaHbceEntry.c
 *
 * Created Date: Mar 6, 2013
 *
 * Description : HBCE entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtList.h"

#include "../pw/ThaModuleClaPw.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"

#include "ThaHbceEntry.h"
#include "ThaHbceMemoryCellContent.h"
#include "ThaHbceEntityInternal.h"
#include "ThaHbceMemoryPool.h"
#include "ThaHbceMemoryCell.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mEntryIsValid(self) (self ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaHbceEntryMethods m_methods;

/* Override */
static tThaHbceEntityMethods m_ThaHbceEntityOverride;
static tAtObjectMethods      m_AtObjectOverride;

/* Save super implementation */
static const tThaHbceEntityMethods *m_ThaHbceEntityMethods = NULL;
static const tAtObjectMethods      *m_AtObjectMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaHbceEntry);
    }

static uint16 MaxNumFlowsPerEntry(ThaHbceEntry self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return ThaClaPwControllerHbceMaxFlowsPerEntry(ThaModuleClaPwControllerGet(claModule));
    }

static AtList FlowsListCreate(ThaHbceEntry self)
    {
    return AtListCreate(MaxNumFlowsPerEntry(self));
    }

static AtList FlowsListGet(ThaHbceEntry self)
    {
    if (self->flows == NULL)
        self->flows = FlowsListCreate(self);
    return self->flows;
    }

static eAtRet Init(ThaHbceEntity self)
    {
    eAtRet ret;
    ThaHbceEntry entry = (ThaHbceEntry)self;

    /* Super initialization */
    ret = m_ThaHbceEntityMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Create list to hold all flows */
    if (FlowsListGet(entry) == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    ThaHbceEntry entry = (ThaHbceEntry)self;

    AtObjectDelete((AtObject)(entry->flows));
    entry->flows = NULL;

    m_AtObjectMethods->Delete(self);
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSnprintf(description, sizeof(description) - 1, "hbce_entry.%u", ThaHbceEntryIndexGet((ThaHbceEntry)self) + 1);
    return description;
    }

static eAtStdCompareResult SerializeCellContentCompare(const void *content1, const void *content2)
    {
    ThaHbceMemoryCellContent cellContent1 = (ThaHbceMemoryCellContent)(size_t)content1;
    ThaHbceMemoryCellContent cellContent2 = (ThaHbceMemoryCellContent)(size_t)content2;
    ThaPwHeaderController controller1 = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent1);
    ThaPwHeaderController controller2 = ThaHbceMemoryCellContentPwHeaderControllerGet(cellContent2);
    uint32 pwId1 = AtChannelIdGet((AtChannel)ThaPwHeaderControllerAdapterGet(controller1));
    uint32 pwId2 = AtChannelIdGet((AtChannel)ThaPwHeaderControllerAdapterGet(controller2));

    if (pwId1 < pwId2)
        return cAtStdCompareResultLessThan;
    if (pwId1 > pwId2)
        return cAtStdCompareResultGreaterThan;

    return cAtStdCompareResultEqual;
    }

static void SerializeCellsBubbleSort(ThaHbceMemoryCellContent *cells, uint32 numCells)
    {
    uint32 i, j;

    for (i = 0; i < (numCells - 1); i++)
        {
        for (j = 0; j < numCells - i - 1; j++)
            {
            if (SerializeCellContentCompare(cells[j], cells[j + 1]) == cAtStdCompareResultGreaterThan)
                {
                ThaHbceMemoryCellContent temp = cells[j];
                cells[j] = cells[j + 1];
                cells[j + 1] = temp;
                }
            }
        }
    }

static void FlowListSerialize(ThaHbceEntry self, AtList flows, AtCoder encoder)
    {
    uint32 flow_i;
    uint32 numFlows;
    ThaHbceMemoryCellContent *cells;

    AtUnused(self);

    /* Put all cells to array for easily sort */
    numFlows = AtListLengthGet(flows);
    if (numFlows == 0)
        return;
    cells = AtOsalMemAlloc(numFlows * (sizeof(ThaHbceMemoryCellContent)));
    if (cells == NULL)
        return;

    /* Sort this array */
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        cells[flow_i] = (ThaHbceMemoryCellContent)AtListObjectGet(flows, flow_i);

    SerializeCellsBubbleSort(cells, numFlows);

    /* Serialize */
    for (flow_i = 0; flow_i < numFlows; flow_i++)
        {
        char key[16];
        AtObject flow = (AtObject)cells[flow_i];
        AtSnprintf(key, sizeof(key) - 1, "flows[%u]", flow_i);
        AtCoderEncodeString(encoder, AtObjectToString(flow), key);
        }

    AtOsalMemFree(cells);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaHbceEntry object = (ThaHbceEntry)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(entryIndex);
    ThaHbceEntryFlowListSerialize(object, object->flows, encoder);
    mEncodeObjectDescription(hbce);
    mEncodeNone(standby);
    }

static uint32 NumFlowsGet(ThaHbceEntry self)
    {
	return AtListLengthGet(FlowsListGet(self));
    }

static ThaHbceMemoryCellContent CellContentAtIndex(ThaHbceEntry self, uint32 cellIndex)
    {
	return (ThaHbceMemoryCellContent)AtListObjectGet(FlowsListGet(self), cellIndex);
    }

static eAtRet CellContentObjectAdd(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent)
	{
    return AtListObjectAdd(FlowsListGet(self), (AtObject)cellContent);
	}

static eAtRet CellContentObjectRemove(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent)
	{
    return AtListObjectRemove(FlowsListGet(self), (AtObject)cellContent);
	}

static AtHal Hal(ThaHbceEntry self)
    {
    return AtIpCoreHalGet(ThaHbceCoreGet(self->hbce));
    }

static uint32 Read(ThaHbceEntry self, uint32 address)
    {
    AtModule claModule = ThaHbceEntityClaModuleGet((ThaHbceEntity)self);
    return mModuleHwReadByHal(claModule, address, Hal(self));
    }

static ThaClaPwController ClaPwController(ThaHbceEntity self)
    {
    ThaModuleCla claModule = (ThaModuleCla)ThaHbceEntityClaModuleGet(self);
    return ThaModuleClaPwControllerGet(claModule);
    }

static uint32 Restore(ThaHbceEntity self, uint8 page)
    {
    ThaHbceEntry entry = (ThaHbceEntry)self;
    ThaHbce hbce = entry->hbce;
    uint32 regVal, regAddr;
    uint32 startMemoryCell;
    ThaHbceMemoryPool memoryPool = hbce->memoryPool;
    ThaClaPwController pwController = ClaPwController(self);
    uint32 numFlows;
    uint32 localCellIndex, cell_i;
    AtModule module = ThaHbceEntityClaModuleGet(self);

    regAddr = ThaHbceCLAHBCEHashingTabCtrl(hbce) + mMethodsGet(hbce)->EntryOffset(hbce, entry->entryIndex, page);
    regVal = Read(entry, regAddr);
    mFieldGet(regVal,
              ThaClaPwControllerCLAHbceFlowNumMask(pwController),
              ThaClaPwControllerCLAHbceFlowNumShift(pwController),
              uint32,
              &numFlows);
    if (numFlows == 0)
        return 0;

    /* Have a fresh list of flows */
    if (entry->flows == NULL)
        entry->flows = FlowsListCreate(entry);
    else
        AtListFlush(entry->flows);

    /* Start cells reconstruction from start memory cell */
    mFieldGet(regVal,
              ThaClaPwControllerCLAHbceMemoryStartPtrMask(pwController),
              ThaClaPwControllerCLAHbceMemoryStartPtrShift(pwController),
              uint32,
              &startMemoryCell);
    localCellIndex = startMemoryCell % ThaHbceMemoryPoolMaxNumCells(memoryPool);
    for (cell_i = 0; cell_i < numFlows; cell_i++)
        {
        uint32 cellIndex = localCellIndex + cell_i;
        ThaHbceMemoryCell cell;
        eAtRet ret;

        ThaHbceMemoryPoolStandbyRestoreCellFromHardware(memoryPool, entry, cellIndex, page);
        cell = ThaHbceMemoryPoolCellAtIndex(memoryPool, cellIndex);
        if (cell == NULL)
            {
            AtModuleLog(module, cAtLogLevelCritical, AtSourceLocation,
                        "Cell %d has NULL content\r\n",
                        cellIndex + 1);
            continue;
            }

        ret = ThaHbceEntryCellContentAdd(entry, ThaHbceMemoryCellContentGet(cell));
        if (ret != cAtOk)
            {
            AtModuleLog(module, cAtLogLevelCritical, AtSourceLocation,
                        "Cannot add cell %d to entry %d, ret = %s\r\n",
                        cellIndex + 1, ThaHbceEntryIndexGet(entry) + 1,
                        AtRet2String(ret));
            }
        }

    return 0;
    }

static void OverrideAtObject(ThaHbceEntry self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaHbceEntity(ThaHbceEntry self)
    {
    ThaHbceEntity entity = (ThaHbceEntity)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHbceEntityMethods = mMethodsGet(entity);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHbceEntityOverride, m_ThaHbceEntityMethods, sizeof(m_ThaHbceEntityOverride));

        mMethodOverride(m_ThaHbceEntityOverride, Init);
        mMethodOverride(m_ThaHbceEntityOverride, Restore);
        }

    mMethodsSet(entity, &m_ThaHbceEntityOverride);
    }

static void Override(ThaHbceEntry self)
    {
    OverrideThaHbceEntity(self);
    OverrideAtObject(self);
    }

static void MethodsInit(ThaHbceEntry self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NumFlowsGet);
        mMethodOverride(m_methods, CellContentAtIndex);
        mMethodOverride(m_methods, CellContentObjectAdd);
        mMethodOverride(m_methods, CellContentObjectRemove);
        }

    mMethodsSet(self, &m_methods);
    }

ThaHbceEntry ThaHbceEntryObjectInit(ThaHbceEntry self, ThaHbce hbce, uint32 entryIndex)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHbceEntityObjectInit((ThaHbceEntity)self, ThaHbceEntityClaModuleGet((ThaHbceEntity)hbce)) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->entryIndex = entryIndex;
    self->hbce = hbce;

    return self;
    }

ThaHbceEntry ThaHbceEntryNew(ThaHbce hbce, uint32 entryIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaHbceEntry newEntry = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEntry == NULL)
        return NULL;

    /* Construct it */
    return ThaHbceEntryObjectInit(newEntry, hbce, entryIndex);
    }

uint32 ThaHbceEntryIndexGet(ThaHbceEntry self)
    {
    if (mEntryIsValid(self))
        return self->entryIndex;

    return 0;
    }

ThaHbceMemoryCellContent ThaHbceEntryFirstCellContentGet(ThaHbceEntry self)
    {
    if (mEntryIsValid(self))
        return (ThaHbceMemoryCellContent)AtListObjectGet(self->flows, 0);

    return NULL;
    }

uint32 ThaHbceEntryNumFlowsGet(ThaHbceEntry self)
    {
    if (mEntryIsValid(self))
        return mMethodsGet(self)->NumFlowsGet(self);

    return 0;
    }

eAtRet ThaHbceEntryCellContentAdd(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent)
    {
    eAtRet ret = cAtOk;

    if (!mEntryIsValid(self))
        return cAtErrorInvlParm;

    ret = mMethodsGet(self)->CellContentObjectAdd(self, cellContent);
    if (ret != cAtOk)
        return ret;

    return ThaHbceMemoryCellContentHashEntrySet(cellContent, self);
    }

eAtRet ThaHbceEntryCellContentRemove(ThaHbceEntry self, ThaHbceMemoryCellContent cellContent)
    {
    eAtRet ret;

    if (!mEntryIsValid(self))
        return cAtErrorNullPointer;

    ret = mMethodsGet(self)->CellContentObjectRemove(self, cellContent);
    if (ret != cAtOk)
        return ret;

    return ThaHbceMemoryCellContentHashEntrySet(cellContent, NULL);
    }

ThaHbceMemoryCellContent ThaHbceEntryCellContentAtIndex(ThaHbceEntry self, uint32 cellIndex)
    {
    if (mEntryIsValid(self))
        return mMethodsGet(self)->CellContentAtIndex(self, cellIndex);

    return NULL;
    }

eBool ThaHbceEntryIsFull(ThaHbceEntry self)
    {
    return (ThaHbceEntryNumFlowsGet(self) == MaxNumFlowsPerEntry(self)) ? cAtTrue : cAtFalse;
    }

tThaHbceRegisterCache *ThaHbceRegisterCache(ThaHbce self)
    {
    return self ? &(self->standby.registersCache) : NULL;
    }

void ThaHbceEntryFlowListSerialize(ThaHbceEntry self, AtList flows, AtCoder encoder)
    {
    if (self)
        FlowListSerialize(self, flows, encoder);
    }

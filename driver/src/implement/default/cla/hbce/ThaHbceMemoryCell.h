/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaHbceMemoryCell.h
 * 
 * Created Date: Mar 6, 2013
 *
 * Description : Memory pool cell
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAHBCEMEMORYCELL_H_
#define _THAHBCEMEMORYCELL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaHbceClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaHbceMemoryCell ThaHbceMemoryCellNew(ThaHbceMemoryPool memoryPool, uint32 cellIndex);

eAtRet ThaHbceMemoryCellContentSet(ThaHbceMemoryCell self, ThaHbceMemoryCellContent content);
ThaHbceMemoryCellContent ThaHbceMemoryCellContentGet(ThaHbceMemoryCell self);

uint32 ThaHbceMemoryCellIndexGet(ThaHbceMemoryCell self);

#ifdef __cplusplus
}
#endif
#endif /* _THAHBCEMEMORYCELL_H_ */


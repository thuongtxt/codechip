/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaModuleCla.c
 *
 * Created Date: Mar 25, 2013
 *
 * Description : CLA abstract module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleClaInternal.h"
#include "ppp/ThaModuleClaPppReg.h"
#include "ThaModuleClaReg.h"
#include "../eth/ThaModuleEth.h"
#include "../util/ThaUtil.h"
#include "controllers/ThaClaEthFlowController.h"
#include "controllers/ThaClaEthPortController.h"
#include "hbce/ThaHbce.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleCla)self)
#define mClaEthFlowController(self) ThaModuleClaEthFlowControllerObjectGet((ThaModuleCla)self)
#define mClaPwController(self) ThaModuleClaPwControllerGet((ThaModuleCla)self)
#define mClaEthPortController(self) ThaModuleClaEthPortControllerGet((ThaModuleCla)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleClaMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleCla);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    if ((localAddress >= 0x440000) &&
        (localAddress <= 0x54FFFF))
        return cAtTrue;

    return cAtFalse;
    }

static void StickiesClear(AtModule self)
    {
    static const uint32 cAllOne = 0xFFFFFFFF;
    uint8 part_i;
    ThaModuleCla claModule = (ThaModuleCla)self;

    for (part_i = 0; part_i < ThaModuleClaNumParts(claModule); part_i++)
        mModuleHwWrite(self, cThaDebugClaSticky0 + ThaModuleClaPartOffset(claModule, part_i), cAllOne);
    }

static eAtRet Debug(AtModule self)
    {
    /* Information of super */
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    mModuleDebugRegPrint(self, ClaSticky0);

    /* Clear for next time */
    StickiesClear(self);

    return cAtOk;
    }

static eAtRet SubPortVlanCheckingEnable(ThaModuleCla self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool  SubPortVlanCheckingIsEnabled(ThaModuleCla self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
	AtUnused(self);
    return NULL;
    }

static ThaClaEthFlowController EthFlowControllerCreate(ThaModuleCla self)
    {
	AtUnused(self);
    return NULL;
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
	AtUnused(self);
    return NULL;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (mThis(self)->pwController == NULL)
        mThis(self)->pwController = mMethodsGet(mThis(self))->PwControllerCreate(mThis(self));

    if (mThis(self)->ethFlowController == NULL)
        mThis(self)->ethFlowController = mMethodsGet(mThis(self))->EthFlowControllerCreate(mThis(self));

    if (mThis(self)->ethPortController == NULL)
        mThis(self)->ethPortController = mMethodsGet(mThis(self))->EthPortControllerCreate(mThis(self));

    return cAtOk;
    }

static eAtRet DefaultSet(ThaModuleCla self)
    {
	AtUnused(self);
    return cAtOk;
    }

static void EthFlowRegsShow(ThaModuleCla self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    }

static void DeleteController(AtObject *controller)
    {
    AtObjectDelete(*controller);
    *controller = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteController((AtObject*)&mThis(self)->pwController);
    DeleteController((AtObject*)&mThis(self)->ethFlowController);
    DeleteController((AtObject*)&mThis(self)->ethPortController);
    m_AtObjectMethods->Delete(self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = ThaModuleClaSuperInit(self);

    if (mClaEthFlowController(self) != NULL)
        ret |= ThaClaControllerInit((ThaClaController)mClaEthFlowController(self));

    if (mClaPwController(self) != NULL)
        ret |= ThaClaControllerInit((ThaClaController)mClaPwController(self));

    if (mClaEthPortController(self) != NULL)
        ret |= ThaClaControllerInit((ThaClaController)mClaEthPortController(self));

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return ThaModuleClaAsyncInitMain((ThaModuleCla)self);
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "cla";
    }

static eAtRet AllTrafficsDeactivate(ThaModuleCla self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void PwRegsShow(ThaModuleCla self, AtPw pw)
    {
    AtPw adapter = (AtPw)ThaPwAdapterGet(pw);
    ThaClaPwControllerPwRegsShow(ThaModuleClaPwControllerGet(self), adapter);
    }

static void EthPortRegsShow(ThaModuleCla self, AtEthPort port)
    {
    /* Let concrete product do */
    AtUnused(self);
    AtUnused(port);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaModuleCla object = (ThaModuleCla)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(pwController);
    mEncodeObject(ethFlowController);
    mEncodeObject(ethPortController);
    mEncodeUInt(asyncInitState);
    }

static eBool PwRxDuplicatedPacketsIsSupported(ThaModuleCla self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 Restore(AtModule self)
    {
    return ThaClaControllerRestore((ThaClaController)mThis(self)->pwController);
    }

static eBool CLAHBCEHashingTabCtrlIsUsed(ThaModuleCla self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet CESoPCASLengthSet(ThaModuleCla self, AtPw pw, uint8 casLen)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(casLen);
    return cAtOk;
    }

static ThaPwAdapter HbcePwFromFlowId(ThaModuleCla self, uint32 flowId)
    {
    AtUnused(self);
    AtUnused(flowId);
    return NULL;
    }

static eAtRet PwPsnHeaderUpdate(ThaModuleCla self, AtPw adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return cAtOk;
    }

static eBool ShouldBalanceCrcPolynomialsUsage(ThaModuleCla self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(ThaModuleCla self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, PwControllerCreate);
        mMethodOverride(m_methods, EthFlowControllerCreate);
        mMethodOverride(m_methods, EthPortControllerCreate);
        mMethodOverride(m_methods, AllTrafficsDeactivate);
        mMethodOverride(m_methods, PwRegsShow);
        mMethodOverride(m_methods, EthPortRegsShow);
        mMethodOverride(m_methods, PwRxDuplicatedPacketsIsSupported);
        mMethodOverride(m_methods, SubPortVlanCheckingEnable);
        mMethodOverride(m_methods, SubPortVlanCheckingIsEnabled);
        mMethodOverride(m_methods, EthFlowRegsShow);
        mMethodOverride(m_methods, CLAHBCEHashingTabCtrlIsUsed);
        mMethodOverride(m_methods, CESoPCASLengthSet);
        mMethodOverride(m_methods, HbcePwFromFlowId);
        mMethodOverride(m_methods, PwPsnHeaderUpdate);
        mMethodOverride(m_methods, ShouldBalanceCrcPolynomialsUsage);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, Restore);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

AtModule ThaModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit(self, cThaModuleCla, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit((ThaModuleCla)self);
    m_methodsInit = 1;

    return self;
    }

void ThaModuleClaEthFlowRegsShow(AtEthFlow flow)
    {
    ThaModuleCla self = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)flow), cThaModuleCla);
    AtPrintc(cSevInfo, "* CLA registers of flow '%s':\r\n", AtObjectToString((AtObject)flow));

    if (self)
        mMethodsGet(self)->EthFlowRegsShow(self, flow);
    }

uint32 ThaModuleClaIncomPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIncomPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaIncombyteRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIncombyteRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktBusErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktBusErrRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktFcsErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktFcsErrRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktoversizeRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktoversizeRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktundersizeRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktundersizeRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktLensmallerthan64bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktLensmallerthan64bytesRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktLenfrom65to127bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktLenfrom65to127bytesRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktLenfrom128to255bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktLenfrom128to255bytesRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktLenfrom256to511bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktLenfrom256to511bytesRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktLenfrom512to1024bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktLenfrom512to1024bytesRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktLenfrom1025to1528bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktLenfrom1025to1528bytesRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktJumboLenRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktJumboLenRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaPWunSuppedRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerPWunSuppedRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaHCBElookupnotmatchPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerHCBElookupnotmatchPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktDscdRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktDscdRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaPauFrmRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerPauFrmRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaPauFrmErrPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerPauFrmErrPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaBcastRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerBcastRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMcastRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMcastRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaARPRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerARPRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthOamRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthOamRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthOamRxErrPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthOamRxErrPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthOamtype0RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthOamtype0RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthOamtype1RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthOamtype1RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIPv4RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaIPv4PktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIPv4PktErrRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaICMPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerICMPv4RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIPv6RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaIPv6PktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIPv6PktErrRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaICMPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerICMPv6RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMEFRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMEFRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMEFErrRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMEFErrRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMPLSRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMPLSRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMPLSErrRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMPLSErrRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMPLSDataRxPktwithPHPMdRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMPLSDataRxPktwithPHPMdRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMPLSDataRxPktwithoutPHPMdRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMPLSDataRxPktwithoutPHPMdRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaLDPIPv4withPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerLDPIPv4withPHPMdRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaLDPIPv6withPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerLDPIPv6withPHPMdRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMPLSoverIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMPLSoverIPv4RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaMPLSoverIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMPLSoverIPv6RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaL2TPPktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerL2TPPktErrRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaL2TPIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerL2TPIPv4RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaL2TPIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerL2TPIPv6RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaUDPPktErrRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerUDPPktErrRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaUDPIPv4RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerUDPIPv4RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaUDPIPv6RxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerUDPIPv6RxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktForwardtoCpuRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktForwardtoCpuRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktForwardtoPDaRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktForwardtoPDaRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaEthPktTimePktToPRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktTimePktToPRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaRxHdrErrPacketsRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerRxHdrErrPacketsRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaRxErrPsnPacketsRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerRxErrPsnPacketsRead2Clear(ethPortController, port, r2c);
    }

eAtRet ThaModuleClaEthPortMacCheckEnable(ThaModuleCla self, AtEthPort port, eBool enable)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMacCheckEnable(ethPortController, port, enable);
    }

eBool ThaModuleClaEthPortMacCheckIsEnabled(ThaModuleCla self, AtEthPort port)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMacCheckIsEnabled(ethPortController, port);
    }

eAtRet ThaModuleClaEthPortMacSet(ThaModuleCla self, AtEthPort port, uint8 *macAddr)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMacSet(ethPortController, port, macAddr);
    }

eAtRet ThaModuleClaEthPortMacGet(ThaModuleCla self, AtEthPort port, uint8 *macAddr)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMacGet(ethPortController, port, macAddr);
    }

uint32 ThaModuleClaPartOffset(ThaModuleCla self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleCla, partId);
    }

uint8 ThaModuleClaNumParts(ThaModuleCla self)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceNumPartsOfModule(device, cThaModuleCla);
    }

eAtRet ThaModuleClaEthPortMaxPacketSizeSet(ThaModuleCla self, AtEthPort port, uint32 maxPacketSize)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMaxPacketSizeSet(ethPortController, port, maxPacketSize);
    }

uint32 ThaModuleClaEthPortMaxPacketSizeGet(ThaModuleCla self, AtEthPort port)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMaxPacketSizeGet(ethPortController, port);
    }

eAtRet ThaModuleClaEthPortMinPacketSizeSet(ThaModuleCla self, AtEthPort port, uint32 minPacketSize)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMinPacketSizeSet(ethPortController, port, minPacketSize);
    }

uint32 ThaModuleClaEthPortMinPacketSizeGet(ThaModuleCla self, AtEthPort port)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerMinPacketSizeGet(ethPortController, port);
    }

uint16 ThaModuleClaEthPortSVlanTpid(ThaModuleCla self, AtEthPort port)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerSVlanTpid(ethPortController, port);
    }

ThaClaPwController ThaModuleClaPwControllerGet(ThaModuleCla self)
    {
    if (self == NULL)
        return NULL;

    if (self->pwController == NULL)
        self->pwController = mMethodsGet(self)->PwControllerCreate(self);

    return self->pwController;
    }

ThaClaEthFlowController ThaModuleClaEthFlowControllerGet(ThaModuleCla self)
    {
    if (self == NULL)
        return NULL;

    if (self->ethFlowController == NULL)
        self->ethFlowController = mMethodsGet(self)->EthFlowControllerCreate(self);

    return self->ethFlowController;
    }

ThaClaEthPortController ThaModuleClaEthPortControllerGet(ThaModuleCla self)
    {
    if (self == NULL)
        return NULL;

    if (self->ethPortController == NULL)
        self->ethPortController = mMethodsGet(self)->EthPortControllerCreate(self);

    return self->ethPortController;
    }

eAtRet ThaModuleClaEthPortIpV4Set(ThaModuleCla self, AtEthPort port, uint8 *address)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIpV4Set(ethPortController, port, address);
    }

eAtRet ThaModuleClaEthPortIpV4Get(ThaModuleCla self, AtEthPort port, uint8 *address)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIpV4Get(ethPortController, port, address);
    }

eAtRet ThaModuleClaEthPortIpV6Set(ThaModuleCla self, AtEthPort port, uint8 *address)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIpV6Set(ethPortController, port, address);
    }

eAtRet ThaModuleClaEthPortIpV6Get(ThaModuleCla self, AtEthPort port, uint8 *address)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerIpV6Get(ethPortController, port, address);
    }

eAtRet ThaModuleClaAllTrafficsDeactivate(ThaModuleCla self)
    {
    if (self)
        return mMethodsGet(self)->AllTrafficsDeactivate(self);
    return cAtOk;
    }

uint32 ThaModuleClaEthPktLenfrom1529to2047bytesRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerEthPktLenfrom1529to2047bytesRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaUnicastRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerUnicastRxPktRead2Clear(ethPortController, port, r2c);
    }

uint32 ThaModuleClaPhysicalErrorRxPktRead2Clear(ThaModuleCla self, AtEthPort port, eBool r2c)
    {
    ThaClaEthPortController ethPortController = ThaModuleClaEthPortControllerGet(self);
    return ThaClaEthPortControllerPhysicalErrorRxPktRead2Clear(ethPortController, port, r2c);
    }

void ThaModuleClaPwRegsShow(AtPw pw)
    {
    ThaModuleCla self = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModuleCla);
    AtPrintc(cSevInfo, "* CLA registers of channel '%s':\r\n", AtObjectToString((AtObject)pw));
    if (self)
        mMethodsGet(self)->PwRegsShow(self, pw);
    AtPrintc(cSevInfo, "\r\n");
    }

void ThaModuleClaEthPortRegShow(AtEthPort port)
    {
    ThaModuleCla self = (ThaModuleCla)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)port), cThaModuleCla);
    AtPrintc(cSevInfo, "* CLA registers of channel '%s':\r\n", AtObjectToString((AtObject)port));
    if (self)
        mMethodsGet(self)->EthPortRegsShow(self, port);
    AtPrintc(cSevInfo, "\r\n");
    }

eBool ThaModuleClaPwRxDuplicatedPacketsIsSupported(ThaModuleCla self)
    {
    if (self)
        return mMethodsGet(self)->PwRxDuplicatedPacketsIsSupported(self);
    return cAtFalse;
    }

void ThaModuleClaLongRegDisplay(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegLongValueDisplay((AtChannel)pw, address, cThaModuleCla);
    }

void ThaModuleClaRegDisplay(AtPw pw, const char* regName, uint32 address)
    {
    ThaDeviceRegNameDisplay(regName);
    ThaDeviceChannelRegValueDisplay((AtChannel)pw, address, cThaModuleCla);
    }

eAtRet ThaModuleClaSuperInit(AtModule self)
    {
    return m_AtModuleMethods->Init(self);
    }

eAtRet ThaModuleClaSuperAsyncInit(AtModule self)
    {
    return m_AtModuleMethods->AsyncInit(self);
    }

ThaClaEthFlowController ThaModuleClaEthFlowControllerObjectGet(ThaModuleCla self)
    {
    return self ? self->ethFlowController : NULL;
    }

eAtRet ThaModuleClaSubPortVlanCheckingEnable(ThaModuleCla self, eBool enable)
    {
    return mMethodsGet(mThis(self))->SubPortVlanCheckingEnable(self, enable);
    }

eBool  ThaModuleClaSubPortVlanCheckingIsEnabled(ThaModuleCla self)
    {
    return mMethodsGet(mThis(self))->SubPortVlanCheckingIsEnabled(self);
    }

eBool ThaModuleClaCLAHBCEHashingTabCtrlIsUsed(ThaModuleCla self)
    {
    if (self)
        return mMethodsGet(self)->CLAHBCEHashingTabCtrlIsUsed(self);
    return cAtFalse;
    }

eAtRet ThaModuleClaCESoPCASLengthSet(ThaModuleCla self, AtPw pw, uint8 casLen)
    {
    if (self)
        return mMethodsGet(self)->CESoPCASLengthSet(self, pw, casLen);
    return cAtErrorNullPointer;
    }

ThaPwAdapter ThaModuleClaHbcePwFromFlowId(ThaModuleCla self, uint32 flowId)
    {
    if (self)
        return mMethodsGet(self)->HbcePwFromFlowId(self, flowId);
    return NULL;
    }

eAtRet ThaModuleClaInit(AtModule self)
    {
    if (self)
        return Init(self);
    return cAtErrorNullPointer;
    }

eAtRet ThaModuleClaPwPsnHeaderUpdate(ThaModuleCla self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->PwPsnHeaderUpdate(self, adapter);
    return cAtErrorNullPointer;
    }

eBool ThaModuleClaShouldBalanceCrcPolynomialsUsage(ThaModuleCla self)
    {
    if (self)
        return mMethodsGet(self)->ShouldBalanceCrcPolynomialsUsage(self);
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaModuleClaReg.h
 * 
 * Created Date: Jun 5, 2013
 *
 * Description : CLA common registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLAREG_H_
#define _THAMODULECLAREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------------
Reg Name: Classify Global PSN Control
Reg Addr: 0x440000
Description: This register controls operation modes of PSN interface.
------------------------------------------------------------------------------*/
#define cThaRegClaGlbPsnCtrl         0x440000

#define cThaRegClaMacCheckDisMask    cBit26
#define cThaRegClaMacCheckDisShift   26

/*------------------------------------------------------------------------------
Reg Name: CLA Incoming packet counter
Reg Addr: 0x540000 + 128*R2C + 256*GEID
          The address format for these registers is 0x540000 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive packet at GE
          port
------------------------------------------------------------------------------*/
#define cThaRegClaIncomPktCnt 0x540000

/*------------------------------------------------------------------------------
Reg Name: CLA Incoming byte counter
Reg Addr: 0x54004B + 128*R2C + 256*GEID
          The address format for these registers is 0x54004B + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive byte at GE
          port
------------------------------------------------------------------------------*/
#define cThaRegClaIncombyteCnt 0x54004B

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Bus error counter
Reg Addr: 0x540035 + 128*R2C + 256*GEID
          The address format for these registers is 0x540035 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Ethernet packet Bus
          error (GAP, PRE, RCV)
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktBusErrCnt 0x540035

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet FCS error counter
Reg Addr: 0x540036 + 128*R2C + 256*GEID
          The address format for these registers is 0x540036 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Ethernet packet FCS
          error
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktFcsErrCnt 0x540036

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Forward to CPU counter
Reg Addr: 0x540037 + 128*R2C + 256*GEID
          The address format for these registers is 0x540037 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Ethernet packet which
          forwarded to CPU
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktForwardtoCpuCnt  (0x540037)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Forward to PDA counter
Reg Addr: 0x540038 + 128*R2C + 256*GEID
          The address format for these registers is 0x540038 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Ethernet packet which
          forwarded to PDA
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktForwardtoPDacnter  (0x540038)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Discard counter
Reg Addr: 0x540039 + 128*R2C + 256*GEID
          The address format for these registers is 0x540039 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Ethernet packet which
          discard to PDA
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktDscdCnt  (0x540039)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Timing packet (ToP) counter
Reg Addr: 0x54003A + 128*R2C + 256*GEID
          The address format for these registers is 0x54003A + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Timing over packet (ToP)
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktTimePktToPCnt  (0x54003A)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Length smaller than 64 bytes counter
Reg Addr: 0x54004E + 128*R2C + 256*GEID
          The address format for these registers is 0x54004E + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length
          smaller than 64 bytes
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktLensmallerthan64bytesCnt  (0x54004E)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Length from 65 to 127 bytes counter
Reg Addr: 0x54003B + 128*R2C + 256*GEID
          The address format for these registers is 0x54003B + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          65 to 127 bytes
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktLenfrom65to127bytesCnt  (0x54003B)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Length from 128 to 255 bytes counter
Reg Addr: 0x54003C + 128*R2C + 256*GEID
          The address format for these registers is 0x54003C + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          128 to 255 bytes
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktLenfrom128to255bytesCnt  (0x54003C)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Length from 256 to 511 bytes counter
Reg Addr: 0x54003D + 128*R2C + 256*GEID
          The address format for these registers is 0x54003D + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          256 to 511 bytes
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktLenfrom256to511bytesCnt  (0x54003D)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Length from 512 to 1024 bytes counter
Reg Addr: 0x54003E + 128*R2C + 256*GEID
          The address format for these registers is 0x54003E + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          512 to 1024 bytes
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktLenfrom512to1024bytesCnt  (0x54003E)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Length from 1025 to 1528 bytes counter
Reg Addr: 0x54003F + 128*R2C + 256*GEID
          The address format for these registers is 0x54003F + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with length from
          1025 to 1528 bytes
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktLenfrom1025to1528bytesCnt  (0x54003F)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet Jumbo Length counter
Reg Addr: 0x540040 + 128*R2C + 256*GEID
          The address format for these registers is 0x540040 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet with jumbo length
          larger than 1528 bytes
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktJumboLenCnt  (0x540040)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet over size counter
Reg Addr: 0x540034 + 128*R2C + 256*GEID
          The address format for these registers is 0x540034 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Ethernet packet length
          over size
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktoversizeCnt 0x540034

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet packet under size counter
Reg Addr: 0x54004D + 128*R2C + 256*GEID
          The address format for these registers is 0x54004D + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of Ethernet packet length
          under size
------------------------------------------------------------------------------*/
#define cThaRegClaEthPktundersizeCnt 0x54004D

/*------------------------------------------------------------------------------
Reg Name: CLA PW un-supported receive packet counter
Reg Addr: 0x540027 + 128*R2C + 256*GEID
          The address format for these registers is 0x540027 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the un-supported receive
          packet at GE port, which is PW Termination packet and don't belong
          to the types which classification support
------------------------------------------------------------------------------*/
#define cThaRegClaPWunSuppedRxPktCnt 0x540027

/*------------------------------------------------------------------------------
Reg Name: CLA HCBE lookup not match packet counter
Reg Addr: 0x540033 + 128*R2C + 256*GEID
          The address format for these registers is 0x540033 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of packet which HCBE
          look-up not match
------------------------------------------------------------------------------*/
#define cThaRegClaHCBElookupnotmatchPktCnt 0x540033

/*------------------------------------------------------------------------------
Reg Name: CLA Pause frame receive packet counter
Reg Addr: 0x54000B + 128*R2C + 256*GEID
          The address format for these registers is 0x54000B + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive pause frame
          packet at GE port. DA is (01-80-C2-00-00-01) and ETHType is (88-08)
------------------------------------------------------------------------------*/
#define cThaRegClaPauFrmRxPktCnt 0x54000B

/*------------------------------------------------------------------------------
Reg Name: CLA Pause frame error packet counter
Reg Addr: 0x540029 + 128*R2C + 256*GEID
          The address format for these registers is 0x540029 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive pause frame
          error packet at GE port.  DA is (01-80-C2-00-00-01) and ETHType is
          (88-08), but OpCode isn't (0001)
------------------------------------------------------------------------------*/
#define cThaRegClaPauFrmErrPktCnt 0x540029

/*------------------------------------------------------------------------------
Reg Name: CLA Broadcast receive packet counter
Reg Addr: 0x540002 + 128*R2C + 256*GEID
          The address format for these registers is 0x540002 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive Broadcast
          packet at GE port. DA is (FF-FF-FF-FF-FF-FF) and Mark is
          (FF-FF-FF-FF-FF-FF)
------------------------------------------------------------------------------*/
#define cThaRegClaBcastRxPktCnt 0x540002

/*------------------------------------------------------------------------------
Reg Name: CLA Multicast receive packet counter
Reg Addr: 0x540003 + 128*R2C + 256*GEID
          The address format for these registers is 0x540003 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive Multicast
          packet at GE port, which match with multicast address and subnet
          mask
------------------------------------------------------------------------------*/
#define cThaRegClaMcastRxPktCnt 0x540003

/*------------------------------------------------------------------------------
Reg Name: CLA ARP receive packet counter
Reg Addr: 0x54000D + 128*R2C + 256*GEID
          The address format for these registers is 0x54000D + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive ARP packet
          at GE port, which EthType  match with 0x806
------------------------------------------------------------------------------*/
#define cThaRegClaARPRxPktCnt 0x54000D

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet OAM receive packet counter
Reg Addr: 0x54000C + 128*R2C + 256*GEID
          The address format for these registers is 0x54000C + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive Ethernet OAM
          packet at GE port. DA is (01-80-C2-00-00-02) and ETHType is (8809)
------------------------------------------------------------------------------*/
#define cThaRegClaEthOamRxPktCnt 0x54000C

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet OAM receive error packet counter
Reg Addr: 0x54002A + 128*R2C + 256*GEID
          The address format for these registers is 0x54002A + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive Ethernet OAM
          error packet at GE port. DA is (01-80-C2-00-00-02) and ETHType is
          (8809) but OpCode isn't in among (00,01,02,0,03,04,05,FE)
------------------------------------------------------------------------------*/
#define cThaRegClaEthOamRxErrPktCnt  (0x54002A)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet OAM type 0 receive packet counter
Reg Addr: 0x540004 + 128*R2C + 256*GEID
          The address format for these registers is 0x540004 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive Ethernet OAM
          Type 0 packet at GE port, which DA match with a DA programmable
------------------------------------------------------------------------------*/
#define cThaRegClaEthOamtype0RxPktCnt  (0x540004)

/*------------------------------------------------------------------------------
Reg Name: CLA Ethernet OAM type 1 receive packet counter
Reg Addr: 0x540005 + 128*R2C + 256*GEID
          The address format for these registers is 0x540005 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive Ethernet OAM
          Type 1 packet at GE port, which DA match with a DA programmable
------------------------------------------------------------------------------*/
#define cThaRegClaEthOamtype1RxPktCnt  (0x540005)

/*------------------------------------------------------------------------------
Reg Name: CLA IPv4 receive packet counter
Reg Addr: 0x540014 + 128*R2C + 256*GEID
          The address format for these registers is 0x540014 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the IPv4 receive packet
          at GE port, which is PW Termination packet or MLPPP Termination
          packet, IPv4 (EthType = 0x0800)
------------------------------------------------------------------------------*/
#define cThaRegClaIPv4RxPktCnt  (0x540014)

/*------------------------------------------------------------------------------
Reg Name: CLA IPv4 Packet Error counter
Reg Addr: 0x54002D + 128*R2C + 256*GEID
          The address format for these registers is 0x54002D + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the Ipv4 Error receive
          packet at GE port, which is PW Termination packet and  Header Length
          is error, Flag is error, DA IP address error, Total Length is error,
          Check Sum is Error
------------------------------------------------------------------------------*/
#define cThaRegClaIPv4PktErrCnt       (0x54002D)

/*------------------------------------------------------------------------------
Reg Name: CLA ICMPv4 receive packet counter
Reg Addr: 0x540016 + 128*R2C + 256*GEID
          The address format for these registers is 0x540016 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive ICMPv4
          packet at GE port, which is Ipv4 packet (EthType = 0x0800) and
          UDPProtocol match with 0x01
------------------------------------------------------------------------------*/
#define cThaRegClaICMPv4RxPktCnt  (0x540016)

/*------------------------------------------------------------------------------
Reg Name: CLA IPv6 receive packet counter
Reg Addr: 0x540015 + 128*R2C + 256*GEID
          The address format for these registers is 0x540015 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the IPv6 receive packet
          at GE port, which is PW Termination packet or MLPPP Termination
          packet, IPv6 (EthType = 0x86DD)
------------------------------------------------------------------------------*/
#define cThaRegClaIPv6RxPktCnt  (0x540015)

/*------------------------------------------------------------------------------
Reg Name: CLA ICMPv6 receive packet counter
Reg Addr: 0x540018 + 128*R2C + 256*GEID
          The address format for these registers is 0x540018 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the receive ICMPv6
          packet at GE port, which is Ipv6 packet (EthType = 0x0800) and
          UDPProtocol match with 0x01
------------------------------------------------------------------------------*/
#define cThaRegClaICMPv6RxPktCnt  (0x540018)

/*------------------------------------------------------------------------------
Reg Name: CLA MEF receive packet counter
Reg Addr: 0x540012 + 128*R2C + 256*GEID
          The address format for these registers is 0x540012 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MEF receive packet
          at GE port, which is PW Termination packet, MEF-8 (EthType = 0x88D8)
------------------------------------------------------------------------------*/
#define cThaRegClaMEFRxPktCnt  (0x540012)

/*------------------------------------------------------------------------------
Reg Name: CLA MEF Error receive packet counter
Reg Addr: 0x54002B + 128*R2C + 256*GEID
          The address format for these registers is 0x54002B + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MEF Error receive
          packet at GE port, which is PW Termination packet, MEF-8 (EthType =
          0x88D8) and Reserved bit value isn't 0x012
------------------------------------------------------------------------------*/
#define cThaRegClaMEFErrRxPktCnt  (0x54002B)

/*------------------------------------------------------------------------------
Reg Name: CLA MPLS Error receive packet counter
Reg Addr: 0x54002C + 128*R2C + 256*GEID
          The address format for these registers is 0x54002C + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MPLS Error receive
          packet at GE port, which is PW Termination packet, MPLS (ETHType =
          0x8847) or MPLSoIP and None Label
------------------------------------------------------------------------------*/
#define cThaRegClaMPLSErrRxPktCnt  (0x54002C)

/*------------------------------------------------------------------------------
Reg Name: CLA IPv6 Packet Error counter
Reg Addr: 0x54002E + 128*R2C + 256*GEID
          The address format for these registers is 0x54002E + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the Ipv6 Error receive
          packet at GE port, which is PW Termination packet and Payload length
          is error, DA IP address is error
------------------------------------------------------------------------------*/
#define cThaRegClaIPv6PktErrCnt       (0x54002E)

/*------------------------------------------------------------------------------
Reg Name: CLA UDP Packet Error counter
Reg Addr: 0x54002F + 128*R2C + 256*GEID
          The address format for these registers is 0x54002F + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the UDP over IP or over
          MPLS Error receive packet at GE port, which is PW Termination packet
          and UDP length is error, Check Sum is error
------------------------------------------------------------------------------*/
#define cThaRegClaUDPPktErrCnt        (0x54002F)

/*------------------------------------------------------------------------------
Reg Name: CLA L2TP Packet Error counter
Reg Addr: 0x540031 + 128*R2C + 256*GEID
          The address format for these registers is 0x540031 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the L2TP Error receive
          packet at GE port
------------------------------------------------------------------------------*/
#define cThaRegClaL2TPPktErrCnt       (0x540031)

/*------------------------------------------------------------------------------
Reg Name: CLA MPLS without PHP mode Outer lookup not match counter
Reg Addr: 0x540032 + 128*R2C + 256*GEID
          The address format for these registers is 0x540032 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of MPLS without PHP mode
          packet which Outer look-up not match
------------------------------------------------------------------------------*/
#define cThaRegClaMPLSwithoutPHPMdOuterlookupnotmatchCnt  (0x540032)

/*------------------------------------------------------------------------------
Reg Name: CLA MPLS receive packet counter
Reg Addr: 0x540013 +128* R2C
          The address format for these registers is 0x540013 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MEF receive packet
          at GE port, which is PW Termination packet or MLPPP Termination
          packet, MPLS (EthType = 0x8847)
------------------------------------------------------------------------------*/
#define cThaRegClaMPLSRxPktCnt  (0x540013)

/*------------------------------------------------------------------------------
Reg Name: CLA MPLS Data receive packet with PHP mode counter
Reg Addr: 0x54001D + 128*R2C + 256*GEID
          The address format for these registers is 0x54001D + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MLPS with PHP mode
          receive packet at GE port, which is PW Termination packet, MLPS
          (EthType = 0x8847), PHP mode, 1 Label is Tunnel Label
------------------------------------------------------------------------------*/
#define cThaRegClaMPLSDataRxPktwithPHPMdCnt  (0x54001D)

/*------------------------------------------------------------------------------
Reg Name: CLA LDPIPv4 over MLPS without PHP mode  receive packet counter
Reg Addr: 0x54001B + 128*R2C + 256*GEID
          The address format for these registers is 0x54001B + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the LDP over MLPS
          without PHP mode receive packet at GE port, which is PW Termination
          packet, MLPS (EthType = 0x8847), non PHP mode
------------------------------------------------------------------------------*/
#define cThaRegClaLDPIPv4overMLPSwithoutPHPMdRxPktCnt  (0x54001B)

/*------------------------------------------------------------------------------
Reg Name: CLA LDPIPv6 with PHP mode  receive packet counter
Reg Addr: 0x54001A + 128*R2C + 256*GEID
          The address format for these registers is 0x54001A + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the LDP over UDPIPv6
          with PHP mode receive packet at GE port, which is PW Termination
          packet, IPv6, PHP mode, UDP Protocol (0x11),  UDP Destination port
          number 646
------------------------------------------------------------------------------*/
#define cThaRegClaLDPIPv6withPHPMdRxPktCnt  (0x54001A)

/*------------------------------------------------------------------------------
Reg Name: CLA LDPIPv6 over MLPS without PHP mode  receive packet counter
Reg Addr: 0x54001C + 128*R2C + 256*GEID
          The address format for these registers is 0x54001C + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the LDP over MLPS
          without PHP mode receive packet at GE port, which is PW Termination
          packet, MLPS (EthType = 0x8847), non PHP mode
------------------------------------------------------------------------------*/
#define cThaRegClaLDPIPv6overMLPSwithoutPHPMdRxPktCnt  (0x54001C)

/*------------------------------------------------------------------------------
Reg Name: CLA MPLS over IPv4 receive packet counter
Reg Addr: 0x540021 + 128*R2C + 256*GEID
          The address format for these registers is 0x540021 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MPLS over IPv4
          receive packet at GE port, which is PW Termination packet,
          IPv4(EthType = 0x800), MPLS Protocol (0x89)
------------------------------------------------------------------------------*/
#define cThaRegClaMPLSoverIPv4RxPktCnt  (0x540021)

/*------------------------------------------------------------------------------
Reg Name: CLA MPLS over IPv6 receive packet counter
Reg Addr: 0x540022 + 128*R2C + 256*GEID
          The address format for these registers is 0x540022 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MPLS over IPv6
          receive packet at GE port, which is PW Termination packet,
          IPv6(EthType = 0x86DD), MPLS Protocol (0x89)
------------------------------------------------------------------------------*/
#define cThaRegClaMPLSoverIPv6RxPktCnt  (0x540022)

/*------------------------------------------------------------------------------
Reg Name: CLA L2TP IPv4 receive packet counter
Reg Addr: 0x540025 + 128*R2C + 256*GEID
          The address format for these registers is 0x540025 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the L2TP IPv4 receive
          packet at GE port, which is PW Termination packet, IPv4(EthType =
          0x800), L2TP Protocol (0x73)
------------------------------------------------------------------------------*/
#define cThaRegClaL2TPIPv4RxPktCnt  (0x540025)

/*------------------------------------------------------------------------------
Reg Name: CLA L2TP IPv6 receive packet counter
Reg Addr: 0x540026 + 128*R2C + 256*GEID
          The address format for these registers is 0x540026 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the L2TP IPv6 receive
          packet at GE port, which is PW Termination packet, IPv6(EthType =
          0x800), L2TP Protocol (0x73)
------------------------------------------------------------------------------*/
#define cThaRegClaL2TPIPv6RxPktCnt  (0x540026)

/*------------------------------------------------------------------------------
Reg Name: CLA UDP IPv4 receive packet counter
Reg Addr: 0x54001F + 128*R2C + 256*GEID
          The address format for these registers is 0x54001F + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the UDPIPv4 receive
          packet at GE port, which is PW Termination packet, IPv4(EthType =
          0x800), UDP Protocol (0x11)
------------------------------------------------------------------------------*/
#define cThaRegClaUDPIPv4RxPktCnt  (0x54001F)

/*------------------------------------------------------------------------------
Reg Name: CLA UDP IPv6 receive packet counter
Reg Addr: 0x540020 + 128*R2C + 256*GEID
          The address format for these registers is 0x540020 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the UDPIPv4 receive
          packet at GE port, which is PW Termination packet, IPv6(EthType =
          0x86DD), UDP Protocol (0x11)
------------------------------------------------------------------------------*/
#define cThaRegClaUDPIPv6RxPktCnt  (0x540020)

/*------------------------------------------------------------------------------
Reg Name: CLA MPLS Data receive packet without PHP mode counter
Reg Addr: 0x54001E + 128*R2C + 256*GEID
          The address format for these registers is 0x54001E + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the MLPS without PHP
          mode receive packet at GE port, which is PW Termination packet, MLPS
          (EthType = 0x8847), PHP mode, more than 2 Label (Tunnel Label, PW
          Label)
------------------------------------------------------------------------------*/
#define cThaRegClaMPLSDataRxPktwithoutPHPMdCnt  (0x54001E)

/*------------------------------------------------------------------------------
Reg Name: CLA LDPIPv4 with PHP mode  receive packet counter
Reg Addr: 0x540019 + 128*R2C + 256*GEID
          The address format for these registers is 0x540019 + 128*R2C +
          256*GEID
          Where:
          * R2C (0-1): Read only or Read To clear (0: Read To Clear, 1: read
          Only)
          * GEID (0-1): GE port ID
Reg Desc: The register provides counter for number of the LDP over udpipv4
          with PHP mode receive packet at GE port, which is PW Termination
          packet, Ipv4, PHP mode, UDP Protocol (0x11),  UDP Destination port
          number 646
------------------------------------------------------------------------------*/
#define cThaRegClaLDPIPv4withPHPMdRxPktCnt  (0x540019)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLAREG_H_ */


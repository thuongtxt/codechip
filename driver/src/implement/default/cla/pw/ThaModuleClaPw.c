/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaModuleClaPw.c
 *
 * Created Date: Mar 25, 2013
 *
 * Description : CLA module of PW products
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleClaPwInternal.h"
#include "ThaModuleClaPwReg.h"
#include "../controllers/ThaClaPwController.h"
#include "../controllers/ThaClaEthPortController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPwController(self) ThaModuleClaPwControllerGet((ThaModuleCla)self)
#define mThis(self) (ThaModuleClaPw)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleClaPwMethods m_methods;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleClaMethods m_ThaModuleClaOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleClaPw);
    }

static uint32 BaseAddress(ThaModuleClaPw self)
    {
	AtUnused(self);
    return 0x440000;
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);

    ThaClaControllerDebug((ThaClaController)mPwController(self));
    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtModuleMethods->Setup(self);
    ret |= ThaClaControllerSetup((ThaClaController)mPwController(self));

    return ret;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(self);
    localAddress = localAddress & cBit23_0;
    if ((localAddress >= 0x00440000) && (localAddress <= 0x0047FFFF))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    uint32 baseAddress = BaseAddress((ThaModuleClaPw)self);
    static eBool initialized = cAtFalse;
    static uint32 holdRegisters[3];
    static const uint8 numHoldRegs = 3;
    uint8 i;

    /* Initialize hold registers */
    if (!initialized)
        {
        initialized = cAtTrue;
        for (i = 0; i < numHoldRegs; i++)
            holdRegisters[i] = baseAddress + i;

        initialized = cAtTrue;
        }

    /* Number of hold registers */
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = numHoldRegs;

    return holdRegisters;
    }

static eAtRet DefaultSet(ThaModuleCla self)
    {
    return ThaClaControllerDefaultSet((ThaClaController)mPwController(self));
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return ThaClaPwControllerNew(self);
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return ThaClaPwEthPortControllerNew(self);
    }

static uint32 PBitPktCounterReg(ThaModuleClaPw self, eBool clear)
    {
	AtUnused(self);
    return cThaRegClaDecapPBitPktCnt(clear);
    }

static uint32 PwCounterOffset(ThaModuleClaPw self, AtPw adapter)
    {
    AtUnused(self);
    return AtChannelIdGet((AtChannel)adapter);
    }

static eBool CanControlPwSuppression(ThaModuleClaPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(ThaModuleClaPw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PBitPktCounterReg);
        mMethodOverride(m_methods, PwCounterOffset);
        mMethodOverride(m_methods, CanControlPwSuppression);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, DefaultSet);
        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    }

AtModule ThaModuleClaPwObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleClaPwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleClaPwObjectInit(newModule, device);
    }

uint32 ThaModuleClaPwCounterOffset(ThaModuleClaPw self, AtPw adapter)
    {
    if (self)
        return mMethodsGet(self)->PwCounterOffset(self, adapter);
    return cBit31_0;
    }

eAtRet ThaModuleClaPwSuppressEnable(ThaModuleClaPw self, AtPw pw, eBool enable)
    {
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet((ThaModuleCla)self);
    return ThaClaPwControllerPwSuppressEnable(claPwController, pw, enable);
    }

eBool ThaModuleClaPwSuppressIsEnabled(ThaModuleClaPw self, AtPw pw)
    {
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet((ThaModuleCla)self);
    return ThaClaPwControllerPwSuppressIsEnabled(claPwController, pw);
    }

eBool ThaModuleClaPwCanControlPwSuppression(ThaModuleClaPw self)
    {
    if (self)
        return mMethodsGet(self)->CanControlPwSuppression(self);
    return cAtFalse;
    }

void ThaMdouleClaPwDebug(AtModule self)
	{
	ThaClaControllerDebug((ThaClaController)mPwController(self));
	}

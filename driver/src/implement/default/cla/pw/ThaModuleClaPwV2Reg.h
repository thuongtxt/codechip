/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaModuleClaPwV2Reg.h
 * 
 * Created Date: May 13, 2013
 *
 * Description : CLA version 2 registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLAPWV1REG_H_
#define _THAMODULECLAPWV1REG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaModuleClaPwReg.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlow_Mask           cBit31_24
#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlow_Shift          24

#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpWorking_Mask          cBit0
#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpWorking_Shift         0

#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_Mask          cBit1
#define cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_Shift         1

/*------------------------------------------------------------------------------
Reg Name: Classify Global PSN Control
Reg Addr: 0x440000
Description: This register controls operation modes of PSN interface.
------------------------------------------------------------------------------*/
#define cThaRegClaVlanTagModeEnMask      cBit30
#define cThaRegClaVlanTagModeEnShift     30

#define cThaRegClaRxSendLbit2CdrEnMask   cBit29
#define cThaRegClaRxSendLbit2CdrEnShift  29

/*------------------------------------------------------------------------------
HBCE registers
------------------------------------------------------------------------------*/
#undef  cThaRegCLAHBCEGlbCtrl
#define cThaRegCLAHBCEGlbCtrl                       0x448000

#undef  cThaRegCLAHBCEHashingTabCtrl
#define cThaRegCLAHBCEHashingTabCtrl                0x448800

#undef cThaRegClaHbceLookingUpInformationCtrl
#define cThaRegClaHbceLookingUpInformationCtrl      0x449000

/*------------------------------------------------------------------------------
Reg Name: Mac Address Bit31_0 Control
Reg Addr: 0x000100
Reg Desc: This register configures bit 31 to 0 of MAC address.
------------------------------------------------------------------------------*/
#define cThaRegEthMacAddr31_0   0x000100

/*------------------------------------------------------------------------------
Reg Name: Mac Address Bit47_32 Control
Reg Addr: 0x000101
Reg Desc: This register configures bit 47 to 32 of MAC address.
------------------------------------------------------------------------------*/
#define cThaRegEthMacAddr47_32   0x000101

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive Total Packet Counter
Address : 0x500014(R_O), 0x500015(R2C)
Description: Count total number of packets received from Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxPktCnt(r2c)  (uint32)(0x500014 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive Total Byte Counter
Address : 0x500012(R_O), 0x500013(R2C)
Description: Count total number of bytes received from Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxByteCnt(r2c)  (uint32)(0x500012 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive FCS Error Packet Counter
Address : 0x500000 - 500001
          Where : 0x500000 is RO address,  0x500001 is R2C address
Description: Count number of FCS error packets detected
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxFcsErrPktCnt(r2c)  (uint32)(0x500000 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive OAM Packet Counter
Address : 0x50000E(R_O), 0x50000F(R2C)
Description: Count number of classified OAM packets (ARP, ICMP,...) received
from Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxOamPktCnt(r2c)  (uint32)(0x50000E + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive UDP Port Error Packet Counter
Address : 0x500006 � 0x500007
          Where: 0x500006 is RO address,  0x500007 is R2C address
Description: Count number of UDP port error packets detected
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxUpdErrPktCnt(r2c)  (uint32)(0x500006 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive Header Error Packet Counter
Address : 0x500002 � 0x500003
          Where: 0x500002 is RO address,  0x500003 is R2C address
Description: Count number of Ethernet MAC header error packets detected
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxHdrErrPktCnt(r2c)  (uint32)(0x500002 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive PSN Header Error Packet Counter
Address : 0x500004 � 0x500005
          Where: 0x500004 is RO address,  0x500005 is R2C address
Description: Count number of PSN header error packets detected
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxPsnErrPktCnt(r2c)  (uint32)(0x500004 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Classify Pseudowire Type Control
Reg Addr: 0x441200-0x44120F
Format  : 0x441200 + pwid, where pwid = [0, , 127]
Description:
------------------------------------------------------------------------------*/
#define cThaRegClaPwTypeCtrl                    0x449C00

/*cBit43_12*/
#define cThaRegClaRxEthRtpSsrcValueHeadMask     cBit11_0  /* cBit43_32 - dRegVal[1] */
#define cThaRegClaRxEthRtpSsrcValueHeadShift    0
#define cThaRegClaRxEthRtpSsrcValueSwHeadMask   cBit31_20
#define cThaRegClaRxEthRtpSsrcValueSwHeadShift  20
#define cThaRegClaRxEthRtpSsrcValueTailMask     cBit31_12 /* cBit31_12 - dRegVal[0] */
#define cThaRegClaRxEthRtpSsrcValueTailShift    12
#define cThaRegClaRxEthRtpSsrcValueSwTailMask   cBit19_0
#define cThaRegClaRxEthRtpSsrcValueSwTailShift  0

#define cThaRegClaRxEthRtpPtValueMask           cBit11_5  /* dRegVal[0] */
#define cThaRegClaRxEthRtpPtValueShift          5

#define cThaRegClaRxEthRtpEnMask                cBit4     /* dRegVal[0] */
#define cThaRegClaRxEthRtpEnShift               4

#define cThaRegClaRxEthRtpSsrcChkEnMask         cBit3     /* dRegVal[0] */
#define cThaRegClaRxEthRtpSsrcChkEnShift        3

#define cThaRegClaRxEthRtpPtChkEnMask           cBit2     /* dRegVal[0] */
#define cThaRegClaRxEthRtpPtChkEnShift          2

#define cThaRegClaRxEthPwTypeMask               cBit1_0   /* dRegVal[0] */
#define cThaRegClaRxEthPwTypeShift              0
#define cThaRegClaRxEthPwTypeCESwithCAS         1
#define cThaRegClaRxEthPwTypeCEP                2

/* CEP Mode */
#define cThaRegClaRxEthCepModeMask              cBit14_13
#define cThaRegClaRxEthCepModeShift             13
#define cThaRegClaRxEthCepModeDwordIndex        1
#define cThaRegClaRxEthCepModeBasic             0
#define cThaRegClaRxEthCepModeVc3Fractional     2
#define cThaRegClaRxEthCepModeVc4Fractional     3

/* CEP payload length */
#define cThaRegClaRxEthPwLenMask                cBit28_15
#define cThaRegClaRxEthPwLenShift               15
#define cThaRegClaRxEthPwLenDwordIndex          1

/* CEP EBM CPU mode */
#define cThaRegClaRxEthPwEbmCpuModeMask         cBit29
#define cThaRegClaRxEthPwEbmCpuModeShift        29
#define cThaRegClaRxEthPwEbmCpuModeDwIndex      1

/*------------------------------------------------------------------------------
Reg Name: Classify Pseudowire Enable Control
Reg Addr: 0x441100
Format  : 0x441100 + pwid/8, where pwid = [0, , 127]
Description:
------------------------------------------------------------------------------*/
#define cThaRegClaPwEnCtrl                     0x441100

#define cThaRegClaPwEnCtrlMask(pwId)           (cBit0 << ((pwId) % 8))
#define cThaRegClaPwEnCtrlShift(pwId)          ((pwId) % 8)

/*------------------------------------------------------------------------------
Reg Name: Classify Pseudowire Identification Control
Reg Addr: 0x441000
Format  : 0x441000 + pwid/8, where pwid = [0, , 127]
Description:
------------------------------------------------------------------------------*/
#define cThaRegClaPwIdenCtrl                    0x441000

/* Pseudowire identification value for PwId[2:0] equal to 0(0,8,16,...,120).
 * Classify engine will compare MPLS inner label or UDP port or MEF8ECID to
 * specify pseudowire packet received from Ethernet side */
#define cThaRegClaPw1stIdenMask               cBit19_0
#define cThaRegClaPw1stIdenShift              0

/*cBit39_20*/
#define cThaRegClaPw2ndIdenHeadMask           cBit7_0
#define cThaRegClaPw2ndIdenHeadShift          0
#define cThaRegClaPw2ndIdenTailMask           cBit31_20
#define cThaRegClaPw2ndIdenTailShift          20

/*cBit59_40*/
#define cThaRegClaPw3thIdenMask               cBit27_8
#define cThaRegClaPw3thIdenShift              8

/*cBit79_60*/
#define cThaRegClaPw4thIdenHeadMask           cBit15_0
#define cThaRegClaPw4thIdenHeadShift          0
#define cThaRegClaPw4thIdenTailMask           cBit31_28
#define cThaRegClaPw4thIdenTailShift          28

/*cBit99_80*/
#define cThaRegClaPw5thIdenHeadMask           cBit3_0
#define cThaRegClaPw5thIdenHeadShift          0
#define cThaRegClaPw5thIdenTailMask           cBit31_16
#define cThaRegClaPw5thIdenTailShift          16

/*cBit119_100*/
#define cThaRegClaPw6thIdenMask               cBit23_4
#define cThaRegClaPw6thIdenShift              4

/*cBit139_120*/
#define cThaRegClaPw7thIdenHeadMask           cBit11_0
#define cThaRegClaPw7thIdenHeadShift          0
#define cThaRegClaPw7thIdenTailMask           cBit31_24
#define cThaRegClaPw7thIdenTailShift          24

/*cBit159_140*/
#define cThaRegClaPw8thIdenMask               cBit31_12
#define cThaRegClaPw8thIdenShift              12

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive 0to64Byte Packet Counter
Address : 0x500020(R_O), 0x500021(R2C)
Description: Count total number of  received packets length from 0 to 64 bytes from
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxsmallerthan64bytesPktCnt(r2c)  (uint32)(0x500020 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive 65to127Byte Packet Counter
Address : 0x500022(R_O), 0x500023(R2C)
Description: Count total number of  received packets length from 65 to 127 bytes from
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxfrom65to127bytesPktCnt(r2c)  (uint32)(0x500022 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive 128to255Byte Packet Counter
Address : 0x500024(R_O), 0x500025(R2C)
Description: Count total number of  received packets length from 128 to 255 bytes from
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxfrom128to255bytesPktCnt(r2c)  (uint32)(0x500024 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive 256to511Byte Packet Counter
Address : 0x500026(R_O), 0x500027(R2C)
Description: Count total number of  received packets length from 256 to 511 bytes from
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxfrom256to511bytesPktCnt(r2c)  (uint32)(0x500026 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive 512to1024Byte Packet Counter
Address : 0x500028(R_O), 0x500029(R2C)
Description: Count total number of  received packets length from 512 to 1024 bytes from
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxfrom512to1024bytesPktCnt(r2c)  (uint32)(0x500028 + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive 1025to1528Byte Packet Counter
Address : 0x50002A(R_O), 0x50002B(R2C)
Description: Count total number of  received packets length from 1025 to 1528 bytes from
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxfrom1025to1528bytesPktCnt(r2c)  (uint32)(0x50002A + r2c)

/*------------------------------------------------------------------------------
Reg Name: Ethernet Receive Jumbo Packet Counter
Address : 0x50002C(R_O), 0x50002D(R2C)
Description: Count total number of  received packets length Jumbo from
Ethernet port
------------------------------------------------------------------------------*/
#define cThaRegPmcEthRxJumboPktCnt(r2c)  (uint32)(0x50002C + r2c)

/*------------------------------------------------------------------------------
Reg Name: Pseudowire Receive P Bit Packet Counter
Reg Addr: 0x51C800 � 0x51C87F(R_O), 0x51C000 � 0x51C07F(R2C)
          The address format for these registers is 0x51C800 + PwId (R_O), 0x51C000 + PwId (R2C)
Reg Desc:
------------------------------------------------------------------------------*/
#define cThaRegPseudowireReceivePBitPacketCounter(ro) (uint32)(0x51C000 + ((ro) * 0x800))

/*==============================================================================
Reg. Name   : CDR Pseudowire Look Up Control
Address     : 0x322000-0x32207F
Format      : 0x322000 + PWID, where PWID=[0, 127]
Description : This register is used to select a PW for CDR function.
==============================================================================*/
#define cThaRegCDRPwLookupCtrl                  0x322000

/*------------------------------------------------------------------------------
Bit Field Name  : PwCdrDis
Bit Field Type  : R/W
Bit Field Desc  : In SAToP mode, CDR MUST be enabled for the respective PW.
                  In CESoPSN mode, only ONE PW from the same T1/E1 line has
                  CDR enabled.
Bit Field Value : 0: Enable CDR
                  1: Disable CDR (default)
------------------------------------------------------------------------------*/
#define cThaCDRPwLookupCtrlCdrDisMask           cBit5
#define cThaCDRPwLookupCtrlCdrDisShift          5

/*------------------------------------------------------------------------------
Bit Field Name  : PwCdrLineId
Bit Field Type  : R/W
Bit Field Desc  : PDH line ID lookup from pseudowire ID.
------------------------------------------------------------------------------*/
#define cThaCDRPwLookupCtrlLineIdMask           cBit4_0
#define cThaCDRPwLookupCtrlLineIdShift          0

/*------------------------------------------------------------------------------
Reg Name: IP Address Bit31_0 Control
Reg Addr: 0x000102
Reg Desc: This register configures bit 31 to 0 of IPv4 and IPv6
------------------------------------------------------------------------------*/
#define cThaRegEthIpAddr31_0   0x000102


/*------------------------------------------------------------------------------
Reg Name: IP Address Bit63_32 Control
Reg Addr: 0x000103
Reg Desc: This register configures bit 63 to 32 of IPv6
------------------------------------------------------------------------------*/
#define cThaRegEthIpAddr63_32   0x000103

/*------------------------------------------------------------------------------
Reg Name: IP Address Bit95_64 Control
Reg Addr: 0x000104
Reg Desc: This register configures bit 95 to 64 of IPv6
------------------------------------------------------------------------------*/
#define cThaRegEthIpAddr95_64   0x000104

/*------------------------------------------------------------------------------
Reg Name: IP Address Bit127_96 Control
Reg Addr: 0x000105
Reg Desc: This register configures bit 127 to 96 of IPv6
------------------------------------------------------------------------------*/
#define cThaRegEthIpAddr127_96   0x000105

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLAPWV1REG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaModuleClaPwInternal.h
 * 
 * Created Date: Mar 25, 2013
 *
 * Description : CLA module of PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLAPWINTERNAL_H_
#define _THAMODULECLAPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/pw/AtPwInternal.h"

#include "../ThaModuleClaInternal.h"
#include "../ThaModuleCla.h"
#include "../hbce/ThaHbce.h"
#include "ThaModuleClaPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleClaPwMethods
    {
    /* Registers */
    uint32 (*PBitPktCounterReg)(ThaModuleClaPw self, eBool clear);
    uint32 (*PwCounterOffset)(ThaModuleClaPw self, AtPw adapter);
    eBool  (*CanControlPwSuppression)(ThaModuleClaPw self);
    }tThaModuleClaPwMethods;

typedef struct tThaModuleClaPw
    {
    tThaModuleCla super;
    const tThaModuleClaPwMethods *methods;

    /* Private data */
    }tThaModuleClaPw;

typedef struct tThaModuleClaPwV2Methods
    {
    /* Registers */
    uint32 (*ClaPwTypeCtrlReg)(ThaModuleClaPwV2 self);
    uint32 (*CDRPwLookupCtrlReg)(ThaModuleClaPwV2 self);

    /* Bit fields */
    uint32 (*CDRPwLookupCtrlCdrDisMask)(ThaModuleClaPwV2 self);
    uint8  (*CDRPwLookupCtrlCdrDisShift)(ThaModuleClaPwV2 self);
    uint32 (*CDRPwLookupCtrlLineIdMask)(ThaModuleClaPwV2 self);
    uint8  (*CDRPwLookupCtrlLineIdShift)(ThaModuleClaPwV2 self);

    uint32 (*ClaPwTypePwLenMask)(ThaModuleClaPwV2 self);
    uint8  (*ClaPwTypePwLenShift)(ThaModuleClaPwV2 self);

    uint32 (*ClaPwTypeCepModeMask)(ThaModuleClaPwV2 self);
    uint8  (*ClaPwTypeCepModeShift)(ThaModuleClaPwV2 self);

    uint32 (*ClaPwEbmCpuModeMask)(ThaModuleClaPwV2 self);
    uint8  (*ClaPwEbmCpuModeShift)(ThaModuleClaPwV2 self);

    /* HSPW */
    uint32 (*HspwPerGrpEnbReg)(ThaModuleClaPwV2 self, eBool isWorking, uint32 groupIdx);
    void (*HsGroupPwAddHwWrite)(AtPw pwAdapter, AtPwGroup pwGroup, uint32 regAddr, eBool isPrimary);
    void (*HsGroupPwRemoveHwWrite)(AtPw pwAdapter, uint32 regAddr);
    eAtRet (*HsGroupEnable)(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eBool enable);
    eBool (*HsGroupIsEnabled)(ThaModuleClaPwV2 self, AtPwGroup pwGroup);
    eAtRet (*HsGroupLabelSetSelect)(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
    uint32 (*HsGroupSelectedLabelSetGet)(ThaModuleClaPwV2 self, AtPwGroup pwGroup);
    }tThaModuleClaPwV2Methods;

typedef struct tThaModuleClaPwV2
    {
    tThaModuleClaPw super;
    const tThaModuleClaPwV2Methods *methods;

    /* Private data */
    }tThaModuleClaPwV2;

typedef struct tThaModuleClaStmPwV2
    {
    tThaModuleClaPwV2 super;
    }tThaModuleClaStmPwV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleClaPwObjectInit(AtModule self, AtDevice device);
AtModule ThaModuleClaPwV2ObjectInit(AtModule self, AtDevice device);
AtModule ThaModuleClaStmPwV2ObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLAPWINTERNAL_H_ */


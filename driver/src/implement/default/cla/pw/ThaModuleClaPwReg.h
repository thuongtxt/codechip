/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaModuleClaPwReg.h
 *
 * Created Date: Mar 25, 2013
 *
 * Description : CLA registers of PW product
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLAPWREG_H_
#define _THAMODULECLAPWREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/*------------------------------------------------------------------------------
Reg Name: Classify Global PSN Control
Reg Addr: 0x444000
Description: This register controls operation modes of PSN interface.
------------------------------------------------------------------------------*/
#define cThaRegClaGlbPsnCtrl         0x444000

/*--------------------------------------
BitField Name: CLAIdenPHPCheckEnable
BitField Type: R/W
BitField Desc: enable to check PHP
BitField Bits: 103
               - 1: enalbe
               - 0: disable
--------------------------------------*/
#define cThaCLAIdenPHPCheckEnableMask      cBit7
#define cThaCLAIdenPHPCheckEnableShift     7
#define cThaCLAIdenPHPCheckEnableDwIndex   3

/*--------------------------------------
BitField Name: CLAIdenBypassHBCE
BitField Type: R/W
BitField Desc: bypass HBCE
BitField Bits: 77
--------------------------------------*/
#define cThaClaIdenBypassHBCEMask                      cBit13
#define cThaClaIdenBypassHBCEShift                     13

#define cThaClaIdenBypassHBCEDwIndex                   2

/*--------------------------------------
BitField Name: CLAIdenPHPMode
BitField Type: R/W
BitField Desc: PHP mode settings affect only the ingress tunnel behavior when
               it is established with LDP. They do not affect PWs configured
               for an IP-based PSN type (MPLSoIP, UDPoIP)
               - 0: PHP is disabled
               - 1: PHP is enabled
BitField Bits: 34
--------------------------------------*/
#define cThaClaIdenPHPModeMask                         cBit2
#define cThaClaIdenPHPModeShift                        2
#define cThaClaIdenPHPModeDwIndex                      1

/*--------------------------------------
BitField Name: CLAIdenMaxLength
BitField Type: R/W
BitField Desc: maximum packet length can receive
BitField Bits: 31_16
--------------------------------------*/
#define cThaClaIdenMaxLenMask                          cBit31_16
#define cThaClaIdenMaxLenShift                         16

#define cThaClaIdenMaxLenDwIndex                       0

/*------------------------------------------------------------------------------
Reg Name: CLA Decap Pseudo wire Control
Reg Addr: 0x00442400
          The address format for these registers is 0x00442400 + PwID
          Where: PwID (0 - 1024) Pseudo wire Identification
Reg Desc: This register allows operator can configure pseudo wire parameters.
------------------------------------------------------------------------------*/
#define cThaRegClaDecapPwCtrl                      (0x442400)

/*--------------------------------------
BitField Name: CLADecSequenceMisMatchCheckEnable
BitField Type: R/W
BitField Desc: when control word's sequence number is not match with RTP's
               sequence number sequence number not match error will be set
               - 0 or 1: don't check
               - 2: check, count and discard
               - 3: check, count and don't discard
BitField Bits: 91_90
--------------------------------------*/
#define cThaClaDecSeqMisMatchChkEnMask                 cBit27_26
#define cThaClaDecSeqMisMatchChkEnShift                26
#define cThaClaDecSeqMisMatchChkEnDwIndex              2

/*--------------------------------------
BitField Name: CLADecRtpSSRCMisMatchCheckEnable
BitField Type: R/W
BitField Desc: SSRC does not match SSRC value defined
               - 0 or 1: don't check
               - 2: check, count and discard
               - 3: check, count and don't discard
BitField Bits: 89_88
--------------------------------------*/
#define cThaClaDecRtpSSRCMisMatchChkEnMask             cBit25_24
#define cThaClaDecRtpSSRCMisMatchChkEnShift            24

#define cThaClaDecRtpSSRCMisMatchChkEnDwIndex          2

/*--------------------------------------
BitField Name: CLADecRtpPayloadTypeMisMatchCheckEnable
BitField Type: R/W
BitField Desc: RTP Payload type does not match RTP payload type value defined
               - 0 or 1: don't check
               - 2: check, count and discard
               - 3: check, count and don't discard
BitField Bits: 87_86
--------------------------------------*/
#define cThaClaDecRtpPldTypeMisMatchChkEnMask          cBit23_22
#define cThaClaDecRtpPldTypeMisMatchChkEnShift         22

#define cThaClaDecRtpPldTypeMisMatchChkEnDwIndex       2

/*--------------------------------------
BitField Name: CLADecMalformCheckEnable
BitField Type: R/W
BitField Desc: the payload size does not match the size defined for this flow
               or control word length field value does not match the size
               defined or If RTP is used, the PT value in its RTP header does
               not correspond to one of the PT values allocated for this pseudo
               wire channel
               - 0 or 1: don't check
               - 2: check, count and discard
               - 3: check, count and don't discard
BitField Bits: 85_84
--------------------------------------*/
#define cThaClaDecMalformChkEnMask                     cBit21_20
#define cThaClaDecMalformChkEnShift                    20

#define cThaClaDecMalformChkEnDwIndex                  2

/*--------------------------------------
BitField Name: CLADecRtpPayloadType
BitField Type: R/W
BitField Desc: one payload type value must be allocated from the range of
               dynamic values for each direction of the bundle. The same
               payload type value may be reused for both directions of the
               bundle and also reused between different bundles
BitField Bits: 83_77
--------------------------------------*/
#define cThaClaDecRtpPldTypeMask                       cBit19_13
#define cThaClaDecRtpPldTypeShift                      13

#define cThaClaDecRtpPldTypeDwIndex                    2

/*--------------------------------------
BitField Name: CLADecRtpSSRCValue
BitField Type: R/W
BitField Desc: Where RTP is being used, further protection may be provided by
               checking the value of the SSRC field against this known SSRC
               value
BitField Bits: 76_45
--------------------------------------*/
#define cThaClaDecRtpSSRCValHwHeadMask                 cBit12_0
#define cThaClaDecRtpSSRCValHwHeadShift                0
#define cThaClaDecRtpSSRCValHwTailMask                 cBit31_13
#define cThaClaDecRtpSSRCValHwTailShift                13
#define cThaClaDecRtpSSRCValSwHeadMask                 cBit31_19
#define cThaClaDecRtpSSRCValSwHeadShift                19
#define cThaClaDecRtpSSRCValSwTailMask                 cBit18_0
#define cThaClaDecRtpSSRCValSwTailShift                0

#define cThaClaDecRtpSSRCValDwIndex                    1

/*--------------------------------------
BitField Name: CLADecControlWordLengthFieldMode
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
               - 0: Full packet mode, cw + rtp + payload < 64 - mac header -
                 psn header - fcs
               - 1: Payload mode, cw + rtp + payload < 64
BitField Bits: 44
--------------------------------------*/
#define cThaClaDecCwLenFieldModeMask                   cBit12
#define cThaClaDecCwLenFieldModeShift                  12

#define cThaClaDecCwLenFieldModeDwIndex                1

/*--------------------------------------
BitField Name: CLADecRealLengthCheckEnable
BitField Type: R/W
BitField Desc: set to check real payload length, this packet will be malformed
               when payload size does not match this size defined
               - 0: is disabled
               - 1: is enabled
BitField Bits: 43
--------------------------------------*/
#define cThaClaDecRealLenChkEnMask                     cBit11
#define cThaClaDecRealLenChkEnShift                    11

#define cThaClaDecRealLenChkEnDwIndex                  1

/*--------------------------------------
BitField Name: CLADecFieldLengthCheckEnable
BitField Type: R/W
BitField Desc: set to check field length when pseudowire has control word field
               - 0: is disabled
               - 1: is enabled
BitField Bits: 42
--------------------------------------*/
#define cThaClaDecFieldLenChkEnMask                    cBit10
#define cThaClaDecFieldLenChkEnShift                   10

#define cThaClaDecFieldLenChkEnDwIndex                 1

/*--------------------------------------
BitField Name: CLADecPayloadType
BitField Type: R/W
BitField Desc: type of payload that pseudowire channel contained
               - 0: Satop/CEP
               - 1: CES
               - 2: ATM Cell mode N to 1
               - 3: ATM Cell mode 1 to 1 VCC
               - 4: ATM Cell mode 1 to 1 VPC
               - 5: ATM Frame mode AAL5
BitField Bits: 41_39
--------------------------------------*/
#define cThaClaDecPldTypeMask                          cBit9_7
#define cThaClaDecPldTypeShift                         7

#define cThaClaDecPldTypeDwIndex                       1

/*--------------------------------------
BitField Name: CLADecPseudowireEnable
BitField Type: R/W
BitField Desc: set pseudowire is enabled or not
               - 0: is disabled
               - 1: is enabled
BitField Bits: 32
--------------------------------------*/
#define cThaClaDecPseudowireEnMask                     cBit0
#define cThaClaDecPseudowireEnShift                    0

#define cThaClaDecPseudowireEnDwIndex                  1

/*--------------------------------------
BitField Name: CLADecPayloadSize
BitField Type: R/W
BitField Desc: this is an unsigned binary number indicating the length of the
               payload.
BitField Bits: 31_18
--------------------------------------*/
#define cThaClaDecPldSizeMask                          cBit31_18
#define cThaClaDecPldSizeShift                         18

#define cThaClaDecPldSizeDwIndex                       0

/*--------------------------------------
BitField Name: CLADecRTPEnable
BitField Type: R/W
BitField Desc: indicates incoming packet contains RTP field
               - 0: is disabled
               - 1: is enabled
BitField Bits: 17
--------------------------------------*/
#define cThaClaDecRtpFieldEnMask                            cBit17
#define cThaClaDecRtpFieldEnShift                           17

#define cThaClaDecRtpFieldEnDwIndex                         0



/*--------------------------------------
BitField Name: CLADecRTPPositon
BitField Type: R/W
BitField Desc: RTP's position in packet
               - 0: cw + rtp
               - 1: rtp + cw
BitField Bits: 16
--------------------------------------*/
#define cThaClaDecRtpPositonMask                       cBit16
#define cThaClaDecRtpPositonShift                      16

#define cThaClaDecRtpPositonDwIndex                    0

/*--------------------------------------
BitField Name: CLADecControlWordType
BitField Type: R/W
BitField Desc: type of control word
               If CLADecPseudowireType: PDH/HDLC:
               - 0: no CW
               - 1: CW 4 bytes
               - other: unused
               If CLADecPseudowireType: ATM:
               - 0: no CW
               - 1: CW 4 bytes
               - 2: CW 3 bytes
               - other: unused
               If CLADecPseudowireType: CEP:
               - 0: no CW
               - 1: CW 4 bytes/structpoint
               - 2: CW 4 bytes/structpoint + EBM VC-3
               - 3: CW 4 bytes/structpoint + EBM VC-4
BitField Bits: 15_14
--------------------------------------*/
#define cThaClaDecCwTypeMask                           cBit15_14
#define cThaClaDecCwTypeShift                          14

#define cThaClaDecCwTypeDwIndex                        0

/*--------------------------------------
BitField Name: CLADecPseudowireType
BitField Type: R/W
BitField Desc: type of payload of incoming packet
               - 0: PDH payload
               - 1: ATM payload
               - 2: HDLC payload
               - 3: CEP payload
BitField Bits: 13_11
--------------------------------------*/
#define cThaClaDecPwTypeMask                   cBit13_11
#define cThaClaDecPwTypeShift                  11

#define cThaClaDecPwTypeDwIndex                0

/*--------------------------------------
BitField Name: CLADecCDRId
BitField Type: R/W
BitField Desc: line identification for per pseudo wire
BitField Bits: 10_2
--------------------------------------*/
#define cThaClaDecCDRIdMask                           cBit10_2
#define cThaClaDecCDRIdShift                          2

#define cThaClaDecCDRIdDwIndex                        0

/*--------------------------------------
BitField Name: CLADecPacketReOrderedEnable
BitField Type: R/W
BitField Desc: allows to set re-order incoming packet
               - 0: is disabled
               - 1: is enabled
BitField Bits: 1
--------------------------------------*/
#define cThaClaDecPktReOrderEnMask                     cBit1
#define cThaClaDecPktReOrderEnShift                    1


/*--------------------------------------
BitField Name: CLADecInfoForCDREnable
BitField Type: R/W
BitField Desc: transfer received info for CDR module
               - 0: is disabled
               - 1: is enabled
BitField Bits: 0
--------------------------------------*/
#define cThaClaDecInfoForCDREnMask                     cBit0
#define cThaClaDecInfoForCDREnShift                    0

#define cThaClaDecInfoForCDREnDwIndex                  0

/* HBCE */
/*
BitField Name: HbceLabelId
BitField Type: R/W
BitField Desc:
BitField Bits: 53_3
*/
#define cThaHbceLblId1HwTailMask                       cBit21_3
#define cThaHbceLblId1HwTailShift                      3
#define cThaHbceLblId0HwHeadMask                       cBit2_0
#define cThaHbceLblId0HwHeadShift                      0
#define cThaHbceLblId0HwTailMask                       cBit31_3
#define cThaHbceLblId0HwTailShift                      3
#define cThaHbceLblId0SwHeadMask                       cBit31_29
#define cThaHbceLblId0SwHeadShift                      29
#define cThaHbceLblId0SwTailMask                       cBit28_0
#define cThaHbceLblId0SwTailShift                      0

/*
BitField Name: HbcePktType
BitField Type: R/W
BitField Desc: cBit2_1
*/
#define cThaHbcePktTypeMask                            cBit2_1
#define cThaHbcePktTypeShift                           1

/*
BitField Name: HbceLabelId
BitField Type: R/W
BitField Desc: cBit31_0
*/
#define cThaHbceGePortMask                             cBit0
#define cThaHbceGePortShift                            0

/*--------------------------------------
BitField Name: HbceMemoryPool
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 53_10
--------------------------------------*/
#define cThaHbceMemoryPool1HwTailMask                  cBit21_10
#define cThaHbceMemoryPool1HwTailShift                 10
#define cThaHbceMemoryPool1SwTailMask                  cBit11_0
#define cThaHbceMemoryPool1SwTailShift                 0
#define cThaHbceMemoryPool0HwHeadMask                  cBit9_0
#define cThaHbceMemoryPool0HwHeadShift                 0
#define cThaHbceMemoryPool0HwTailMask                  cBit31_10
#define cThaHbceMemoryPool0HwTailShift                 10
#define cThaHbceMemoryPool0SwHeadMask                  cBit31_22
#define cThaHbceMemoryPool0SwHeadShift                 22
#define cThaHbceMemoryPool0SwTailMask                  cBit21_0
#define cThaHbceMemoryPool0SwTailShift                 0

/*
BitField Name: HbceHashIndex
BitField Type: R/W
BitField Desc:
*/

 /*--------------------------------------
BitField Name: HbcePatern59_50
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 59_50
--------------------------------------*/
#define cThaHbcePatern59_50Mask                         cBit27_18
#define cThaHbcePatern59_50Shift                        18
#define cThaHbcePatern59_50DwIndex                      1

/*--------------------------------------
BitField Name: HbcePatern49_40
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 49_40
--------------------------------------*/
#define cThaHbcePatern49_40Mask                         cBit17_8
#define cThaHbcePatern49_40Shift                        8
#define cThaHbcePatern49_40DwIndex                      1

/*--------------------------------------
BitField Name: HbcePatern39_30
BitField Type:
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 39_30
--------------------------------------*/
#define cThaHbcePatern39_30HwHeadMask                   cBit7_0
#define cThaHbcePatern39_30HwHeadShift                  0
#define cThaHbcePatern39_30HwTailMask                   cBit31_30
#define cThaHbcePatern39_30HwTailShift                  30
#define cThaHbcePatern39_30SwHeadMask                   cBit9_2
#define cThaHbcePatern39_30SwHeadShift                  2
#define cThaHbcePatern39_30SwTailMask                   cBit1_0
#define cThaHbcePatern39_30SwTailShift                  0
#define cThaHbcePatern39_30DwIndex                      0

#define cThaHbcePatern29_20Mask                        cBit29_20
#define cThaHbcePatern29_20Shift                       20
#define cThaHbcePatern29_20DwIndex                     0

#define cThaHbcePatern19_10Mask                        cBit19_10
#define cThaHbcePatern19_10Shift                       10
#define cThaHbcePatern19_10DwIndex                     0

#define cThaHbcePatern9_0Mask                          cBit9_0
#define cThaHbcePatern9_0Shift                         0
#define cThaHbcePatern9_0DwIndex                       0

/*------------------------------------------------------------------------------
Reg Name: CLA HBCE Global Control
Reg Addr: 0x44C000
Reg Desc: This register is used to configure global signals for HBCE module.
Change  : 08-May-2009: old-0x440000 new-0x44C000
------------------------------------------------------------------------------*/
#define cThaRegCLAHBCEGlbCtrl                          0x44C000

/*--------------------------------------
BitField Name: CLAHbceTimeoutValue
BitField Type: R/W
BitField Desc: time out value for HBCE module.
BitField Bits: 19_4
--------------------------------------*/
#define cThaClaHbceTimeoutValMask                      cBit19_4
#define cThaClaHbceTimeoutValShift                     4

#define cThaClaHbceTimeoutValDwIndex                   0

/*--------------------------------------
BitField Name: CLAHbceHashingTableSelectedPage
BitField Type: R/W
BitField Desc: selects hashing table page to access looking up information
               buffer
               - 1: page 1
               - 0: page 0
BitField Bits: 3
--------------------------------------*/
#define cThaClaHbceHashingTabSelectedPageMask          cBit3
#define cThaClaHbceHashingTabSelectedPageShift         3


/*--------------------------------------
BitField Name: CLAHbceCodingSelectedMode
BitField Type: R/W
BitField Desc: selects mode to code origin id
               - 4/5/6/7: code level 4/5/6/7
               - 3: code level 3 (xor 4 subgroups origin id together)
               - 2: code level 2
               - 1: code level 1
               - 0: none xor origin id
BitField Bits: 2_0
--------------------------------------*/
#define cThaClaHbceCodingSelectedModeMask              cBit2_0
#define cThaClaHbceCodingSelectedModeShift             0
#define cThaClaHbceCodingSelectedModeDwIndex           0

/*------------------------------------------------------------------------------
Reg Name: CLA HBCE Hashing Table Control
Reg Addr: 0x44C400
          The address format for these registers is 0x44C400 + PageID*0x200
          + HashID
          Where: PageID (0 - 1) Hashing Table Page Identification
                 HashID (0 - 1023) Hashing Table Identification
Reg Desc: HBCE module uses 10 bits for tabindex. tabindex is generated by
          hasding function applied to the origin id. The collisions due to
          hashing are handled by pointer to variable size blocks. Handling
          variable  size blocks requires a dynamic memory management scheme.
          The number of entries in each variable size block is defined by the
          number of rules that collide within a specific entry of the
          tabindex. Hashing function applies an XOR function to all bits of
          tabindex. The indexes to the tabindex are generated by hashing
          function and therefore collisions may occur. In order to resolve
          these collisions efficiently, HBCE define a complex data structure
          associated with each entry of the orgin id. Hashing table has two
          fields, flow number (flownum) (number of labelid mapped to this
          particular table entry) and memory start pointer (memsptr) (hold a
          pointer to the variable size block and the number of  rules that
          collide). In case, a table entry might be empty  which means that it
          is not mapped to any flow address rule, flownum = 0. Moreover, a
          table entry may be mapped to many flow address rules. In this case,
          where collisions occur, hashing table have to store a pointer to
          the variable size block and the number of rules that collide. The
          number of colliding rules also indicates the size of the block. The
          formating of hashing table page0 in each case is shown as below.
Change  : 08-May-2009: old-0x440200     new-0x44C200
                       old-PageID*0x200 new-PageID*0x400
------------------------------------------------------------------------------*/
#define cThaRegCLAHBCEHashingTabCtrl                   0x44C400

/*--------------------------------------
BitField Name: CLAHbceFlowNumber
BitField Type: R/W
BitField Desc: flow number, others: collision format
               - 0: empty format
               - 1: normal format
--------------------------------------*/
#define cThaCLAHbceFlowNumMask                         cBit14_11
#define cThaCLAHbceFlowNumShift                        11

/*--------------------------------------
BitField Name: CLAHbceMemoryStartPointer
BitField Type: R/W
BitField Desc: memory start pointer
--------------------------------------*/
#define cThaCLAHbceMemoryStartPtrMask                  cBit10_0
#define cThaCLAHbceMemoryStartPtrShift                 0

/*------------------------------------------------------------------------------
Reg Name: CLA HBCE Looking Up Information Control
Reg Addr: 0x0044D800 - 0x0044DC3F
          The address format for these registers is 0x0044D800 + CellID
          Where: CellID (0 - 1087) Cell Info Identification
Reg Desc: Description:In HBCE module, all operations access this memory in
          order to examine whether an exact match exists or not. The Memory
          Pool implements dynamic memory management scheme and supports
          variable size blocks. It supports requests for allocation and
          deallocation of variable size blocks when inserting and deleting
          lookup address occur. Linking between multiple blocks is implemented
          by writing the address of the  next  block  in  the  previous block.
          In general, HBCE implementation is based on sequential accesses to
          memory pool and to the dynamically allocated collision nodes. The
          formating of memory pool word is shown as below. Normal Format
Change  : 08-May-2009: old-0x441800     new-0x44D800
------------------------------------------------------------------------------*/
#define cThaRegClaHbceLookingUpInformationCtrl         0x44D800

/*--------------------------------------
BitField Name: CLAHbceMemoryStatus
BitField Type: R/W
BitField Desc:
BitField Bits: 23
--------------------------------------*/
#define cThaClaHbceMemoryStatusMask                    cBit23
#define cThaClaHbceMemoryStatusShift                   23

/*--------------------------------------
BitField Name: CLAHbceFlowID
BitField Type: R/W
BitField Desc: number identifying the output port of HBCE module
BitField Bits: 22_14
--------------------------------------*/
#define cThaClaHbceFlowIdMask                          cBit22_14
#define cThaClaHbceFlowIdShift                         14


/*--------------------------------------
BitField Name: CLAHbceFlowEnb
BitField Type: R/W
BitField Desc: set active for this flow
               0: is disabled, that mean this packet will be discarded
               1: is enabled
BitField Bits: 13
--------------------------------------*/
#define cThaClaHbceFlowEnbMask                         cBit13
#define cThaClaHbceFlowEnbShift                        13


/*--------------------------------------
BitField Name: CLAHbceStoreID
BitField Type: R/W
BitField Desc: To identify a certain lookup address rule within a particular
table entry HBCE also need to save some additional information so as to be able
to distinguish those that collide. But, HBCE don't need to save all bits and it
can take advantage of the fact that the XOR function can be inversed,
is defined as storeid.
BitField Bits: 43_0
--------------------------------------*/
#define cThaClaHbceStoreIdMask                         cBit12_0
#define cThaClaHbceStoreIdShift                        0



/*------------------------------------------------------------------------------
Reg Name: CLA IDEN Ethernet OAM Address 1 Control
Reg Addr: 0x00444005
          The address format for these registers is 0x00444005 + GeID*0x2000
          Where: GeID (0 - 1) Gigabit Identification
Reg Desc: This register is used to configure destination addresses for pause
          frame and OAM PDU packet
------------------------------------------------------------------------------*/
#define cThaRegClaIdENEthernetOamAddress1Ctrl (0x444005)





/*------------------------------------------------------------------------------
Reg Name: CLA IDEN Ethernet OAM Address 2 Control
Reg Addr: 0x00444006
          The address format for these registers is 0x00444006 + GeID*0x2000
          Where: GeID (0 - 1) Gigabit Identification
Reg Desc: This register is used to configure destination addresses for
          ethernet oam packet, these address will be used in future.
------------------------------------------------------------------------------*/
#define cThaRegClaIdENEthernetOamAddress2Ctrl (0x444006)





/*------------------------------------------------------------------------------
Reg Name: CLA IDEN MLPPP termination Destination Address Control
Reg Addr: 0x00444002
          The address format for these registers is 0x00444002 + GeID*0x2000
          Where: GeID (0 - 1) Gigabit Identification
Reg Desc: This register is used to configure MLPPP termination destination
          address. This module supports two MLPPP termination destination
          addresses.
------------------------------------------------------------------------------*/
#define cThaRegClaIdENMLPppterminationDaCtrl (0x444002)





/*------------------------------------------------------------------------------
Reg Name: CLA IDEN Multicast and Broadcast Group Address Control
Reg Addr: 0x00444003
          The address format for these registers is 0x00444002 + GeID*0x2000
          Where: GeID (0 - 1) Gigabit Identification
Reg Desc: This register is used to configure Broadcast and Multicast group
          address.
------------------------------------------------------------------------------*/
#define cThaRegCLAIdENMcastandBcastGrpAddressCtrl (0x444003)





/*------------------------------------------------------------------------------
Reg Name: CLA IDEN Multicast and Broadcast Mask Control
Reg Addr: 0x00444004
          The address format for these registers is 0x00444003 + GeID*0x2000
          Where: GeID (0 - 1) Gigabit Identification
Reg Desc: This register is used to configure Broadcast and Multicast mask
          address. A packet with broadcast or multicast address that matches
          with mask address and group address, will be in group broadcast or
          multicast
------------------------------------------------------------------------------*/





/*------------------------------------------------------------------------------
Reg Name: CLA IDEN MAC Discard and Forward Control
Reg Addr: 0x0044400F
          The address format for these registers is 0x0044400F + GeID*0x2000
          Where: GeID (0 - 1) Gigabit Identification
Reg Desc: This register allows operator can select many optionals from
          incoming packet, when a received packet incomes, we can set 4 mode
          for it: 0: Forward this packet to PDA module 1: Discard this packet
          2: Forward this packet to Low queue OAM buffer 3: Forward this
          packet to High queue OAM buffer
------------------------------------------------------------------------------*/
#define cThaRegClaIdENMACDscdandForwardCtrl  (0x44400F)





/*--------------------------------------
BitField Name: CLAIdenForwardDiscardDaBroadcastPacket
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 95_94
--------------------------------------*/
#define cThaClaIdenForwardDscdDaBcastPktMask           cBit31_30
#define cThaClaIdenForwardDscdDaBcastPktShift          30

/*--------------------------------------
BitField Name: CLAIdenForwardDiscardDaMulticastPacket
BitField Type: R/W
BitField Desc: _ERRORS___LACK_OF_DESCRIPTION
BitField Bits: 93_92
--------------------------------------*/
#define cThaClaIdenForwardDscdDaMcastPktDwIndex        2

/*------------------------------------------------------------------------------
Reg Name: CLA Decap Pseudo wire Set Default Value Control
Reg Addr: 0x00442800
Reg Desc: This register allows operator can configure pseudo wire default
          parameters for all pseudo wire.
------------------------------------------------------------------------------*/
#define cThaRegClaDecapPwSetDeftValCtrl             (0x442800)


/*------------------------------------------------------------------------------
Reg Name: CLA Decap Forwarded to PDA Packet Counter
Reg Addr: 0x441A00 - 0x441B4F (R_O), 0x441800 - 0x44194F(R2C),
          The address format for these registers is 0x441800 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the forwarded to PDA
          packets
------------------------------------------------------------------------------*/
#define cThaRegClaDecapForwardedtoPDAPktCnt(r2c)  ((r2c) ? 0x441800 : 0x441A00)

/*------------------------------------------------------------------------------
Reg Name: CLA Decap LBit Packet Counter
Reg Addr: 0x441E00 - 0x441F4F (R_O), 0x441C00 - 0x441D4F(R2C),
          The address format for these registers is 0x441C00 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the forwarded to PDA
          packets with L bit
------------------------------------------------------------------------------*/
#define cThaRegClaDecapLBitPktCnt(r2c)             ((r2c) ? 0x441C00 : 0x441E00)

/*------------------------------------------------------------------------------
Reg Name: CLA Decap RBit Packet Counter
Reg Addr: 0x443200 - 0x44334F (R_O), 0x443000 - 0x44314F(R2C),
          The address format for these registers is 0x443000 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the forwarded to PDA
          packets with R bit
------------------------------------------------------------------------------*/
#define cThaRegClaDecapRBitPktCnt(r2c)              ((r2c) ? 0x443000 : 0x443200)

/*------------------------------------------------------------------------------
Reg Name: CLA Decap Sequence Not Match Packet Counter
Reg Addr: 0x443600 - 0x44374F (R_O), 0x443400 - 0x44354F(R2C),
          The address format for these registers is 0x443400 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the received packets but
          sequence number field of Control Word and RTP is not the same.
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: CLA Decap Malformed Packet Counter
Reg Addr: 0x443A00 - 0x443B4F (R_O), 0x443800 - 0x44394F(R2C),
          The address format for these registers is 0x443800 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the received packets
          which is malformed
------------------------------------------------------------------------------*/
#define cThaRegClaDecapMalformedPktCnt(r2c)       ((r2c) ? 0x443800 : 0x443A00)

/*------------------------------------------------------------------------------
Reg Name: CLA Decap SSRC Mismatch Packet Counter
Reg Addr: 0x443E00 - 0x443F4F (R_O), 0x443C00 - 0x443D4F(R2C),
          The address format for these registers is 0x443C00 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the received packets but
          SSRC field of RTP is not match.
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name: CLA Decap MBit Packet Counter
Reg Addr: 0x442A00 – 0x442B4F (R_O), 0x442800 – 0x44294F(R2C),
          The address format for these registers is 0x443C00 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the received packets but
          SSRC field of RTP is not match.
------------------------------------------------------------------------------*/
#define cThaRegClaDecapMBitPktCnt(r2c)    ((r2c) ? 0x442800 : 0x442A00)

/*------------------------------------------------------------------------------
Reg Name: CLA Decap PBit Packet Counter
Reg Addr: 0x442E00 – 0x442F4F (R_O), 0x442C00 – 0x442D4F(R2C),
          The address format for these registers is 0x443C00 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the received packets but
          SSRC field of RTP is not match.
------------------------------------------------------------------------------*/
#define cThaRegClaDecapPBitPktCnt(r2c)    ((r2c) ? 0x442C00 : 0x442E00)

/*------------------------------------------------------------------------------
Reg Name: CLA Decap Forwarded to CPU Packet Counter
Reg Addr: 0x441600 - 0x44174F (R_O), 0x441400 - 0x44154F(R2C),
          The address format for these registers is 0x441400 + 0x200*RO
          Where: pwid (0 - 335) Pseudowire IDs
Reg Desc: The register provides counter for number of the forwarded to CPU
          packets
------------------------------------------------------------------------------*/
#define cThaRegClaDecapForwardedtoCpuPktCnt(r2c)  ((r2c) ? 0x441400 : 0x441600)

/*------------------------------------------------------------------------------
Reg Name: CLA IDEN Pseudowire termination Destination Address Control
Reg Addr: 0x00444001
          The address format for these registers is 0x00444001 + GeID*0x2000
          Where: GeID (0 - 1) Gigabit Identification
Reg Desc: This register is used to configure unicast address. This module
          supports two unicast addresses.
Reg Desc: This register is used to configure pseudowire termination
          destination address. This module supports two pseudowire termination
          destination addresses.
------------------------------------------------------------------------------*/
#define cThaRegCLAIdenPseudowireTerDestAddressCtrl       (0x444001)

#define cThaClaIdenMacAddress00Mask                  cBit15_8
#define cThaClaIdenMacAddress00Shift                 8
#define cThaClaIdenMacAddress00DwIndex               1


#define cThaClaIdenMacAddress01Mask                  cBit7_0
#define cThaClaIdenMacAddress01Shift                 0
#define cThaClaIdenMacAddress01DwIndex               1


#define cThaClaIdenMacAddress02Mask                  cBit31_24
#define cThaClaIdenMacAddress02Shift                 24
#define cThaClaIdenMacAddress02DwIndex               0


#define cThaClaIdenMacAddress03Mask                  cBit23_16
#define cThaClaIdenMacAddress03Shift                 16
#define cThaClaIdenMacAddress03DwIndex               0


#define cThaClaIdenMacAddress04Mask                  cBit15_8
#define cThaClaIdenMacAddress04Shift                 8
#define cThaClaIdenMacAddress04DwIndex               0


#define cThaClaIdenMacAddress05Mask                  cBit7_0
#define cThaClaIdenMacAddress05Shift                 0
#define cThaClaIdenMacAddress05DwIndex               0

/*------------------------------------------------------------------------------
Reg Name: CLA Decap Pseudo wire current status
Reg Addr: 0x0044F800
          The address format for these registers is 0x0044F800 + PwID
          Where: PwID (0 - 1024) Pseudo wire Identification
Reg Desc: This register is used to get current status of Lbit/Rbit
------------------------------------------------------------------------------*/
#define cThaRegClaDecapPwAlm                       0x0044F800

#define cThaClaDecRxMBitCurStatusMask              cBit2

#define cThaClaDecRxRBitCurStatusMask              cBit1

#define cThaClaDecRxLBitCurStatusMask              cBit0

/*------------------------------------------------------------------------------
Reg Name: CLA Decap Pseudo wire Alarm interrupt status
Reg Addr: 0x0044F400
          The address format for these registers is 0x0044F400 + PwID
          Where: PwID (0 - 1024) Pseudo wire Identification
Reg Desc: This register is used to get Lbit/Rbit interrupt
------------------------------------------------------------------------------*/
#define cThaRegClaDecapPwAlmIntr                   0x0044F400

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLAPWREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaModuleClaPwV2.c
 *
 * Created Date: May 15, 2013
 *
 * Description : CLA for PW product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleClaPwInternal.h"
#include "ThaModuleClaPwV2Reg.h"
#include "../controllers/ThaClaEthPortController.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"
#include "../hbce/ThaHbceMemoryCell.h"
#include "../hbce/ThaHbceMemoryPool.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidRegAddr 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaModuleClaPwV2)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaModuleClaPwV2Methods m_methods;

/* Override */
static tThaModuleClaMethods   m_ThaModuleClaOverride;
static tAtModuleMethods       m_AtModuleOverride;
static tThaModuleClaPwMethods m_ThaModuleClaPwOverride;

/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(localAddress);
	AtUnused(self);
    return cAtFalse;
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return ThaClaPwEthPortControllerV2New(self);
    }

static uint32 PBitPktCounterReg(ThaModuleClaPw self, eBool clear)
    {
    uint8 readOnly = clear ? 0 : 1;
	AtUnused(self);
    return cThaRegPseudowireReceivePBitPacketCounter(readOnly);
    }

static uint32 ClaPwTypeCtrlReg(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaRegClaPwTypeCtrl;
    }

static uint32 CDRPwLookupCtrlReg(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaRegCDRPwLookupCtrl;
    }

static uint32 CDRPwLookupCtrlCdrDisMask(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlCdrDisMask;
    }

static uint8  CDRPwLookupCtrlCdrDisShift(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlCdrDisShift;
    }

static uint32 CDRPwLookupCtrlLineIdMask(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlLineIdMask;
    }

static uint8  CDRPwLookupCtrlLineIdShift(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlLineIdShift;
    }

static uint32 ClaPwTypePwLenMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cThaRegClaRxEthPwLenMask;
    }

static uint8 ClaPwTypePwLenShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cThaRegClaRxEthPwLenShift;
    }

static uint32 ClaPwTypeCepModeMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cThaRegClaRxEthCepModeMask;
    }

static uint8 ClaPwTypeCepModeShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cThaRegClaRxEthCepModeShift;
    }

static uint32 ClaPwEbmCpuModeMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cThaRegClaRxEthPwEbmCpuModeMask;
    }

static uint8  ClaPwEbmCpuModeShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cThaRegClaRxEthPwEbmCpuModeShift;
    }

static uint32 HspwPerGrpEnbReg(ThaModuleClaPwV2 self, eBool isWorking, uint32 groupIdx)
    {
    AtUnused(self);
    AtUnused(isWorking);
    AtUnused(groupIdx);
    return cBit31_0;
    }

static eAtRet HsGroupEnable(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eBool enable)
    {
    eAtPwGroupLabelSet labelSet = AtPwGroupRxSelectedLabelGet(pwGroup);
    eBool isWorking = (labelSet == cAtPwGroupLabelSetPrimary) ? cAtTrue : cAtFalse;
    uint32 regAddr = ThaModuleClaHSPWPerGrpEnbReg(self, isWorking, AtPwGroupIdGet(pwGroup));

    mModuleHwWrite(self, regAddr, mBoolToBin(enable));
    return cAtOk;
    }

static eBool HsGroupIsEnabled(ThaModuleClaPwV2 self, AtPwGroup pwGroup)
    {
    eAtPwGroupLabelSet labelSet = AtPwGroupRxSelectedLabelGet(pwGroup);
    eBool isWorking = (labelSet == cAtPwGroupLabelSetPrimary) ? cAtTrue : cAtFalse;
    uint32 regAddr = ThaModuleClaHSPWPerGrpEnbReg(self, isWorking, AtPwGroupIdGet(pwGroup));

    uint32 regVal  = mModuleHwRead(self, regAddr);

    return mBinToBool(regVal & cBit0);
    }

static eAtRet HsGroupLabelSetSelect(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    uint32 regAddr = ThaModuleClaHSPWPerGrpEnbReg((ThaModuleClaPwV2)self, cAtFalse, AtPwGroupIdGet(pwGroup));
    mModuleHwWrite(self, regAddr, (labelSet == cAtPwGroupLabelSetPrimary) ? 0 : 1);

    regAddr = ThaModuleClaHSPWPerGrpEnbReg(self, cAtTrue, AtPwGroupIdGet(pwGroup));
    mModuleHwWrite(self, regAddr, (labelSet == cAtPwGroupLabelSetPrimary) ? 1 : 0);

    return cAtOk;
    }

static uint32 HsGroupSelectedLabelSetGet(ThaModuleClaPwV2 self, AtPwGroup pwGroup)
    {
    uint32 regAddr = ThaModuleClaHSPWPerGrpEnbReg(self, cAtFalse, AtPwGroupIdGet(pwGroup));
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal == 0) ? cAtPwGroupLabelSetPrimary : cAtPwGroupLabelSetBackup;
    }


static void HsGroupPwAddHwWrite(AtPw pwAdapter, AtPwGroup pwGroup, uint32 regAddr, eBool isPrimary)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 groupId = AtPwGroupIdGet(pwGroup);

    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpIDFlow_, groupId);
    mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_, 0);
    mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpWorking_, mBoolToBin(isPrimary));

    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    }

static ThaHbce ModuleClaAndPwToHbce(ThaModuleCla self, AtPw pwAdapter)
    {
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet(self);
    return ThaClaPwControllerHbceGet(claPwController, pwAdapter);
    }

static void HsGroupPwRemoveHwWrite(AtPw pwAdapter, uint32 regAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceFlowDirect_, 1);
    mRegFieldSet(longRegVal[1], cAf6_Classify_HBCE_Looking_Up_Information_Control_CLAHbceGrpWorking_, 1);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideThaModuleClaPw(AtModule self)
    {
    ThaModuleClaPw claModule = (ThaModuleClaPw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwOverride));

        mMethodOverride(m_ThaModuleClaPwOverride, PBitPktCounterReg);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    OverrideThaModuleClaPw(self);
    }

static void MethodsInit(ThaModuleClaPwV2 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ClaPwTypeCtrlReg);
        mMethodOverride(m_methods, CDRPwLookupCtrlReg);
        mMethodOverride(m_methods, HspwPerGrpEnbReg);
        mMethodOverride(m_methods, HsGroupEnable);
        mMethodOverride(m_methods, HsGroupIsEnabled);
        mMethodOverride(m_methods, HsGroupLabelSetSelect);
        mMethodOverride(m_methods, HsGroupSelectedLabelSetGet);
        mMethodOverride(m_methods, HsGroupPwAddHwWrite);
        mMethodOverride(m_methods, HsGroupPwRemoveHwWrite);

        /* Bit fields */
        mMethodOverride(m_methods, CDRPwLookupCtrlCdrDisMask);
        mMethodOverride(m_methods, CDRPwLookupCtrlCdrDisShift);
        mMethodOverride(m_methods, CDRPwLookupCtrlLineIdMask);
        mMethodOverride(m_methods, CDRPwLookupCtrlLineIdShift);
        mMethodOverride(m_methods, ClaPwTypePwLenMask);
        mMethodOverride(m_methods, ClaPwTypePwLenShift);
        mMethodOverride(m_methods, ClaPwTypeCepModeMask);
        mMethodOverride(m_methods, ClaPwTypeCepModeShift);
        mMethodOverride(m_methods, ClaPwEbmCpuModeMask);
        mMethodOverride(m_methods, ClaPwEbmCpuModeShift);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
	{
    return sizeof(tThaModuleClaPwV2);
	}

AtModule ThaModuleClaPwV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClaPwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleClaPwV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleClaPwV2ObjectInit(newModule, device);
    }

uint32 ThaModuleClaPwV2ClaPwTypeCtrlReg(ThaModuleClaPwV2 self)
    {
    return self ? mMethodsGet(self)->ClaPwTypeCtrlReg(self) : cInvalidRegAddr;
    }

uint32 ThaModuleClaPwV2CDRPwLookupCtrlReg(ThaModuleClaPwV2 self)
    {
    return self ? mMethodsGet(self)->CDRPwLookupCtrlReg(self) : cInvalidRegAddr;
    }

uint32 ThaModuleClaHSPWPerGrpEnbReg(ThaModuleClaPwV2 self, eBool isWorking, uint32 groupIdx)
    {
    return self ? mMethodsGet(self)->HspwPerGrpEnbReg(self, isWorking, groupIdx) : cInvalidRegAddr;
    }

uint32 ThaModuleClaPwV2CDRPwLookupCtrlCdrDisMask(ThaModuleClaPwV2 self)
    {
    return self ? mMethodsGet(self)->CDRPwLookupCtrlCdrDisMask(self) : 0;
    }

uint8 ThaModuleClaPwV2CDRPwLookupCtrlCdrDisShift(ThaModuleClaPwV2 self)
    {
    return (uint8)(self ? mMethodsGet(self)->CDRPwLookupCtrlCdrDisShift(self) : 0);
    }

uint32 ThaModuleClaPwV2CDRPwLookupCtrlLineIdMask(ThaModuleClaPwV2 self)
    {
    return self ? mMethodsGet(self)->CDRPwLookupCtrlLineIdMask(self) : 0;
    }

uint8  ThaModuleClaPwV2CDRPwLookupCtrlLineIdShift(ThaModuleClaPwV2 self)
    {
    return (uint8)(self ? mMethodsGet(self)->CDRPwLookupCtrlLineIdShift(self) : 0);
    }

eAtRet ThaModuleClaPwV2HsGroupEnable(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eBool enable)
    {
    return self ? mMethodsGet(self)->HsGroupEnable(self, pwGroup, enable) : cAtErrorNullPointer;
    }

eBool ThaModuleClaPwV2HsGroupIsEnabled(ThaModuleClaPwV2 self, AtPwGroup pwGroup)
    {
    return (eBool)(self ? mMethodsGet(self)->HsGroupIsEnabled(self, pwGroup) : cAtFalse);
    }

eAtRet ThaModuleClaPwV2HsGroupLabelSetSelect(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet)
    {
    return self ? mMethodsGet(self)->HsGroupLabelSetSelect(self, pwGroup, labelSet) : cAtErrorNullPointer;
    }

uint32 ThaModuleClaPwV2HsGroupSelectedLabelSetGet(ThaModuleClaPwV2 self, AtPwGroup pwGroup)
    {
    return self ?  mMethodsGet(self)->HsGroupSelectedLabelSetGet(self, pwGroup) : 0;
    }

uint32 ThaModuleClaPwV2HbceLookingUpInformationCtrl(ThaModuleClaPwV2 self, ThaPwHeaderController controller, uint8 page)
    {
    ThaHbce hbce;
    uint32 cellIndex;
    ThaHbceMemoryCell cell;
    ThaClaPwController claPwController;
    ThaHbceMemoryPool pool;
    ThaHbceMemoryCellContent cellContent = ThaPwHeaderControllerHbceMemoryCellContentGet(controller);

    if (cellContent == NULL)
        return cInvalidUint32;

    claPwController = ThaModuleClaPwControllerGet((ThaModuleCla)self);
    hbce = ThaClaPwControllerHbceGet(claPwController, (AtPw)ThaPwHeaderControllerAdapterGet(controller));
    pool = ThaHbceMemoryPoolGet(hbce);

    cell = ThaHbceMemoryCellContentCellGet(cellContent);
    cellIndex = ThaHbceMemoryCellIndexGet(cell);
    return ThaModuleClaPwV2HbceCellLookingUpInformationCtrl(self, pool, cellIndex, page);
    }

eAtRet ThaModuleClaPwV2HsGroupPwAdd(ThaModuleClaPwV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    ThaPwHeaderController headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pwAdapter);
    ThaPwHeaderController backupHeaderController = ThaPwAdapterBackupHeaderController((ThaPwAdapter)pwAdapter);
    uint32 regAddress;
    uint8 page = ThaHbceActivePage(ModuleClaAndPwToHbce((ThaModuleCla)self, pwAdapter));

    regAddress = ThaModuleClaPwV2HbceLookingUpInformationCtrl(self, headerController, page);
    if (regAddress != cInvalidUint32)
        mMethodsGet(self)->HsGroupPwAddHwWrite(pwAdapter, pwGroup, regAddress, cAtTrue);

    regAddress = ThaModuleClaPwV2HbceLookingUpInformationCtrl(self, backupHeaderController, page);
    if (regAddress != cInvalidUint32)
        mMethodsGet(self)->HsGroupPwAddHwWrite(pwAdapter, pwGroup, regAddress, cAtFalse);

    return cAtOk;
    }

eAtRet ThaModuleClaPwV2HsGroupPwRemove(ThaModuleClaPwV2 self, AtPwGroup pwGroup, AtPw pwAdapter)
    {
    ThaPwHeaderController headerController = ThaPwAdapterHeaderController((ThaPwAdapter)pwAdapter);
    ThaPwHeaderController backupHeaderController = ThaPwAdapterBackupHeaderController((ThaPwAdapter)pwAdapter);
    uint32 regAddress;
    uint8 page = ThaHbceActivePage(ModuleClaAndPwToHbce((ThaModuleCla)self, pwAdapter));

    AtUnused(pwGroup);

    regAddress = ThaModuleClaPwV2HbceLookingUpInformationCtrl(self, headerController, page);
    if (regAddress != cInvalidUint32)
        mMethodsGet(self)->HsGroupPwRemoveHwWrite(pwAdapter, regAddress);

    regAddress = ThaModuleClaPwV2HbceLookingUpInformationCtrl(self, backupHeaderController, page);
    if (regAddress != cInvalidUint32)
        mMethodsGet(self)->HsGroupPwRemoveHwWrite(pwAdapter, regAddress);

    return cAtOk;
    }

uint32 ThaModuleClaPwV2HbceCellLookingUpInformationCtrl(ThaModuleClaPwV2 self, ThaHbceMemoryPool pool, uint32 cellIndex, uint8 page)
    {
    ThaClaPwController claPwController = ThaModuleClaPwControllerGet((ThaModuleCla)self);
    return ThaClaPwControllerClaHbceLookingUpInformationCtrl(claPwController) + cellIndex + (ThaHbceMemoryPoolMaxNumCells(pool) * page);
    }


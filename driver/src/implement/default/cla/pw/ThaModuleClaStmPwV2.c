/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaModuleClaStmPwV2.c
 *
 * Created Date: Jul 23, 2013
 *
 * Description : CLA module of PW product version 2 (STM product)
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaModuleClaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/
/*==============================================================================
Reg Name : CDR Pseudowire Look Up Control
==============================================================================*/
#define cThaCDRPwLookupCtrlCdrDisMask  cBit9
#define cThaCDRPwLookupCtrlCdrDisShift 9
#define cThaCDRPwLookupCtrlLineIdMask  cBit8_0
#define cThaCDRPwLookupCtrlLineIdShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaMethods     m_ThaModuleClaOverride;
static tThaModuleClaPwV2Methods m_ThaModuleClaPwV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CDRPwLookupCtrlCdrDisMask(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlCdrDisMask;
    }

static uint8  CDRPwLookupCtrlCdrDisShift(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlCdrDisShift;
    }

static uint32 CDRPwLookupCtrlLineIdMask(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlLineIdMask;
    }

static uint8  CDRPwLookupCtrlLineIdShift(ThaModuleClaPwV2 self)
    {
	AtUnused(self);
    return cThaCDRPwLookupCtrlLineIdShift;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleClaStmPwV2);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return ThaClaPwControllerV2New(self);
    }

static void OverrideThaModuleClaPwV2(AtModule self)
    {
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwV2Override, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwV2Override));

        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisShift);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlLineIdMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlLineIdShift);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwV2Override);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    OverrideThaModuleClaPwV2(self);
    }

AtModule ThaModuleClaStmPwV2ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClaPwV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleClaStmPwV2New(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleClaStmPwV2ObjectInit(newModule, device);
    }

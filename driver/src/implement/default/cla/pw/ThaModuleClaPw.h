/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaModuleClaPw.h
 * 
 * Created Date: Apr 1, 2013
 *
 * Description : CLA module of PW product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLAPW_H_
#define _THAMODULECLAPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwGroup.h"
#include "../ThaModuleCla.h" /* Super class */
#include "../../pw/ThaModulePw.h"
#include "../../cla/hbce/ThaHbceClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleClaPw   * ThaModuleClaPw;
typedef struct tThaModuleClaPwV2 * ThaModuleClaPwV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Registers */
uint32 ThaModuleClaPwV2ClaPwTypeCtrlReg(ThaModuleClaPwV2 self);
uint32 ThaModuleClaPwV2CDRPwLookupCtrlReg(ThaModuleClaPwV2 self);
uint32 ThaModuleClaPwV2CDRPwLookupCtrlCdrDisMask(ThaModuleClaPwV2 self);
uint8  ThaModuleClaPwV2CDRPwLookupCtrlCdrDisShift(ThaModuleClaPwV2 self);
uint32 ThaModuleClaPwV2CDRPwLookupCtrlLineIdMask(ThaModuleClaPwV2 self);
uint8  ThaModuleClaPwV2CDRPwLookupCtrlLineIdShift(ThaModuleClaPwV2 self);

uint32 ThaModuleClaHSPWPerGrpEnbReg(ThaModuleClaPwV2 self, eBool isWorking, uint32 groupIdx);
eAtRet ThaModuleClaPwV2HsGroupEnable(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eBool enable);
eBool  ThaModuleClaPwV2HsGroupIsEnabled(ThaModuleClaPwV2 self, AtPwGroup pwGroup);
eAtRet ThaModuleClaPwV2HsGroupLabelSetSelect(ThaModuleClaPwV2 self, AtPwGroup pwGroup, eAtPwGroupLabelSet labelSet);
uint32 ThaModuleClaPwV2HsGroupSelectedLabelSetGet(ThaModuleClaPwV2 self, AtPwGroup pwGroup);
uint32 ThaModuleClaPwV2HbceLookingUpInformationCtrl(ThaModuleClaPwV2 self, ThaPwHeaderController controller, uint8 page);
uint32 ThaModuleClaPwV2HbceCellLookingUpInformationCtrl(ThaModuleClaPwV2 self, ThaHbceMemoryPool pool, uint32 cellIndex, uint8 page);
eAtRet ThaModuleClaPwV2HsGroupPwAdd(ThaModuleClaPwV2 self, AtPwGroup pwGroup, AtPw pwAdapter);
eAtRet ThaModuleClaPwV2HsGroupPwRemove(ThaModuleClaPwV2 self, AtPwGroup pwGroup, AtPw pwAdapter);

uint32 ThaModuleClaPwCounterOffset(ThaModuleClaPw self, AtPw adapter);
eAtRet ThaModuleClaPwSuppressEnable(ThaModuleClaPw self, AtPw pw, eBool enable);
eBool ThaModuleClaPwSuppressIsEnabled(ThaModuleClaPw self, AtPw pw);
eBool ThaModuleClaPwCanControlPwSuppression(ThaModuleClaPw self);
void ThaMdouleClaPwDebug(AtModule self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLAPW_H_ */


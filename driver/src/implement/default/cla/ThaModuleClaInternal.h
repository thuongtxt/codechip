/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA internal module
 * 
 * File        : ThaModuleClaInternal.h
 * 
 * Created Date: Sep 10, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLAINTERNAL_H_
#define _THAMODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../generic/man/AtModuleInternal.h"
#include "ThaModuleCla.h"
#include "controllers/ThaClaEthFlowController.h"
#include "controllers/ThaClaEthPortController.h"
#include "controllers/ThaClaPwController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleClaMethods
    {
    eAtRet (*DefaultSet)(ThaModuleCla self);
    eAtRet (*AllTrafficsDeactivate)(ThaModuleCla self);
    eAtRet (*PwPsnHeaderUpdate)(ThaModuleCla self, AtPw adapter);
    eBool (*ShouldBalanceCrcPolynomialsUsage)(ThaModuleCla self);

    /* Controller factory methods */
    ThaClaPwController (*PwControllerCreate)(ThaModuleCla self);
    ThaClaEthFlowController (*EthFlowControllerCreate)(ThaModuleCla self);
    ThaClaEthPortController (*EthPortControllerCreate)(ThaModuleCla self);

    eBool (*PwRxDuplicatedPacketsIsSupported)(ThaModuleCla self);

    /* For debugging */
    void (*PwRegsShow)(ThaModuleCla self, AtPw pw);
    void (*EthPortRegsShow)(ThaModuleCla self, AtEthPort port);
    eAtRet (*SubPortVlanCheckingEnable)(ThaModuleCla self, eBool enable);
    eBool  (*SubPortVlanCheckingIsEnabled)(ThaModuleCla self);
    void (*EthFlowRegsShow)(ThaModuleCla self, AtEthFlow ethFlow);
    eBool (*CLAHBCEHashingTabCtrlIsUsed)(ThaModuleCla self);
    eAtRet (*CESoPCASLengthSet)(ThaModuleCla self, AtPw pw, uint8 casLen);

    /* For warm restore*/
    ThaPwAdapter (*HbcePwFromFlowId)(ThaModuleCla self, uint32 flowId);
    }tThaModuleClaMethods;

typedef struct tThaModuleCla
    {
    tAtModule super;
    const tThaModuleClaMethods *methods;

    /* Private data */
    ThaClaPwController pwController;
    ThaClaEthFlowController ethFlowController;
    ThaClaEthPortController ethPortController;

    /*async init*/
    uint32 asyncInitState;

    }tThaModuleCla;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleClaObjectInit(AtModule module, AtDevice device);
eAtRet ThaModuleClaAsyncInitMain(ThaModuleCla self);
eAtRet ThaClaPwControllerAsyncInitMain(ThaClaController self);
eAtRet ThaModuleClaSuperInit(AtModule self);
eAtRet ThaModuleClaSuperAsyncInit(AtModule self);
eAtRet ThaModuleClaAsyncInitMain(ThaModuleCla self);
ThaClaEthFlowController ThaModuleClaEthFlowControllerObjectGet(ThaModuleCla self);
eAtRet ThaModuleClaInit(AtModule self);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLAINTERNAL_H_ */


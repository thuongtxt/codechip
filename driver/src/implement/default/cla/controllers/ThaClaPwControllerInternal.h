/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaPwControllerInternal.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA controller for PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLAPWCONTROLLERINTERNAL_H_
#define _THACLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../pw/adapters/ThaPwAdapter.h"
#include "../hbce/ThaHbce.h"
#include "ThaClaControllerInternal.h"
#include "ThaClaPwController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mClaPwOffset(self, pw_) mMethodsGet((ThaClaPwController)self)->PwDefaultOffset((ThaClaPwController)self, pw_)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClaPwControllerMethods
    {
    uint32 (*PwDefaultOffset)(ThaClaPwController self, AtPw pw);
    uint8 (*PartOfPw)(ThaClaPwController self, AtPw pw);

    /* RTP */
    eAtRet (*PwRtpEnable)(ThaClaPwController self, AtPw pw, eBool enable);
    eBool  (*PwRtpIsEnabled)(ThaClaPwController self, AtPw pw);
    eAtRet (*PwRtpSsrcSet)(ThaClaPwController self, AtPw pw, uint32 ssrc);
    uint32 (*PwRtpSsrcGet)(ThaClaPwController self, AtPw pw);
    eAtRet (*PwRtpPayloadTypeSet)(ThaClaPwController self, AtPw pw, uint8 payloadType);
    uint8 (*PwRtpPayloadTypeGet)(ThaClaPwController self, AtPw pw);

    /* Control word */
    eAtRet (*PwCwLengthModeSet)(ThaClaPwController self, AtPw pw, eAtPwCwLengthMode lengthMode);
    eBool (*PwCwIsEnabled)(ThaClaPwController self, AtPw pw);
    eAtRet (*PwCwEnable)(ThaClaPwController self, AtPw pw, eBool enable);
    eAtRet (*PwReorderEnable)(ThaClaPwController self, AtPw pw, eBool enable);
    eBool (*PwReorderIsEnable)(ThaClaPwController self, AtPw pw);

    /* PSN */
    eAtRet (*PwLabelSet)(ThaClaPwController self, AtPw pw, AtPwPsn psn);
    ThaHbce (*HbceCreate)(ThaClaPwController self, uint8 hbceId, AtIpCore core);
    uint8 (*HbceMaxFlowsPerEntry)(ThaClaPwController self);

    /* Counters */
    uint32 (*RxPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxMalformedPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxStrayPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxDuplicatedPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxOamPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxLbitPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxRbitPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxMbitPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxNbitPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxPbitPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);
    uint32 (*RxDiscardedPacketsGet)(ThaClaPwController self, AtPw pw, eBool clear);

    /* CDR */
    eAtRet (*CdrEnable)(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable);
    eBool  (*CdrIsEnabled)(ThaClaPwController self, AtPw pw);
    uint16 (*CdrIdGet)(ThaClaPwController self, AtPw pw);
    eAtRet (*PartPwCdrDisable)(ThaClaPwController self, uint32 localPwId, uint8 partId, AtIpCore core);
    eAtRet (*CdrCircuitSliceSet)(ThaClaPwController self, AtPw pw, uint8 slice);

    /* Pseudowire Payload */
    eBool (*PwTypeIsSupported)(ThaClaPwController self, eAtPwType pwType);
    eAtRet (*PwTypeSet)(ThaClaPwController self, AtPw pw, eAtPwType pwType);
    eAtRet (*PayloadSizeSet)(ThaClaPwController self, AtPw pw, uint16 payloadSize);
    uint16 (*PayloadSizeGet)(ThaClaPwController self, AtPw pw);

    /* Alarms */
    uint32 (*PwAlarmGet)(ThaClaPwController self, AtPw pw);
    uint32 (*PwDefectHistoryGet)(ThaClaPwController self, AtPw pw, eBool read2Clear);

    /* Error checking */
    eAtRet (*PwRtpSequenceMistmatchCheckModeSet)(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
    eAtRet (*PwRtpSsrcMismatchCheckModeSet)(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
    eThaPwErrorCheckingMode (*PwRtpSsrcMismatchCheckModeGet)(ThaClaPwController self, AtPw pw);
    eAtRet (*PwRtpPldTypeMismatchCheckModeSet)(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
    eThaPwErrorCheckingMode (*PwRtpPldTypeMismatchCheckModeGet)(ThaClaPwController self, AtPw pw);
    eAtRet (*PwMalformCheckModeSet)(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
    eAtRet (*PwRealLenCheckModeSet)(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
    eAtRet (*PwLenFieldCheckModeSet)(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);

    /* PW lookup */
    eBool (*PwLookupModeIsSupported)(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode);
    eAtRet (*PwLookupModeSet)(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode);
    eThaPwLookupMode (*PwLookupModeGet)(ThaClaPwController self, AtPw pw);
    eThaPwLookupMode (*PartPwLookupModeGet)(ThaClaPwController self, uint8 partId);
    eAtRet (*PwExpectedCVlanSet)(ThaClaPwController self, AtPw pwAdapter, tAtEthVlanTag *cVlan);
    eAtRet (*PwExpectedSVlanSet)(ThaClaPwController self, AtPw pwAdapter, tAtEthVlanTag *sVlan);
    eAtRet (*PwLookupRemove)(ThaClaPwController self, AtPw pwAdapter, eBool applyHardware);
    eAtRet (*PwLookupEnable)(ThaClaPwController self, AtPw pwAdapter, eBool enable);
    eBool (*PwExpectedVlanTagsConflict)(ThaClaPwController self,
                                        tAtEthVlanTag *expectedCVlan1, tAtEthVlanTag *expectedSVlan1,
                                        tAtEthVlanTag *expectedCVlan2, tAtEthVlanTag *expectedSVlan2);

    /* PW CEP */
    eAtRet (*PwCepEbmEnable)(ThaClaPwController self, AtPw pw, eBool enable);

    /* Global configuration */
    eAtRet (*SendLbitPacketToCdrEnable)(ThaClaPwController self, eBool enable);
    eBool (*SendLbitPacketToCdrIsEnabled)(ThaClaPwController self);
    eAtRet (*PwSuppressEnable)(ThaClaPwController self, AtPw pw, eBool enable);
    eBool (*PwSuppressIsEnabled)(ThaClaPwController self, AtPw pw);

    /* Pw HDLC */
    eAtRet (*HdlcPwPayloadTypeSet)(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
    eAtRet (*MlpppPwPayloadTypeSet)(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
    eAtPwHdlcPayloadType (*DefaultPayloadType)(ThaClaPwController self);

    /* Register polymorphism */
    uint32 (*CLAHBCEGlbCtrl)(ThaClaPwController self);
    uint32 (*CLAHBCEHashingTabCtrl)(ThaClaPwController self);
    uint32 (*ClaHbceLookingUpInformationCtrl)(ThaClaPwController self);

    /* Bit fields polymorphism */
    mDefineMaskShiftField(ThaClaPwController, ClaHbceFlowId)
    mDefineMaskShiftField(ThaClaPwController, ClaHbceFlowEnb)
    mDefineMaskShiftField(ThaClaPwController, ClaHbceStoreId)

    mDefineMaskShiftField(ThaClaPwController, ClaHbceCodingSelectedMode)
    mDefineMaskShiftField(ThaClaPwController, ClaHbceHashingTabSelectedPage)
    mDefineMaskShiftField(ThaClaPwController, ClaHbceTimeoutVal)

    mDefineMaskShiftField(ThaClaPwController, CLAHbceFlowNum)
    mDefineMaskShiftField(ThaClaPwController, CLAHbceMemoryStartPtr)

    /* Debug */
    void (*PwRegsShow)(ThaClaPwController self, AtPw pw);
    void (*PktAnalyzerCla2CdrDataFlush)(ThaClaPwController self, AtPw pw);
    eAtRet (*PktAnalyzerCla2CdrTrigger)(ThaClaPwController self, AtPw pw);
    eBool (*PktAnalyzerCla2CdrHoIsEnabled)(ThaClaPwController self, AtPw pw, uint16 packetId);
    eBool (*PktAnalyzerCla2CdrPacketIsError)(ThaClaPwController self, AtPw pw, uint16 packetId);
    uint32 (*PktAnalyzerCla2CdrRtpTimeStampGet)(ThaClaPwController self, AtPw pw, uint16 packetId);
    uint32 (*PktAnalyzerCla2CdrRtpTimeStampOffset)(ThaClaPwController self, AtPw pw, uint16 previousPktId, uint16 nextPktId);
    uint32 (*PktAnalyzerCla2CdrRtpSequenceNumberGet)(ThaClaPwController self, AtPw pw, uint16 packetId);
    uint32 (*PktAnalyzerCla2CdrControlWordSequenceNumberGet)(ThaClaPwController self, AtPw pw, uint16 packetId);
    }tThaClaPwControllerMethods;

typedef struct tThaClaPwController
    {
    tThaClaController super;
    const tThaClaPwControllerMethods *methods;

    /* Private data */
    AtList hbces;

    /*Async init*/
    uint32 asyncInitState;
    uint32 asyncInitAllHbceInitState;

    }tThaClaPwController;

typedef struct tThaClaPwControllerV2
    {
    tThaClaPwController super;
    }tThaClaPwControllerV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController ThaClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);
ThaClaPwController ThaClaPwControllerV2ObjectInit(ThaClaPwController self, ThaModuleCla cla);
ThaClaPwController ThaClaStmPwControllerV2ObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THACLAPWCONTROLLERINTERNAL_H_ */


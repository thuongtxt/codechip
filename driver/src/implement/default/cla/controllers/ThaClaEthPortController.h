/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaEthPortController.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA ETH Port controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLAETHPORTCONTROLLER_H_
#define _THACLAETHPORTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaClaController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController ThaClaPppEthPortControllerNew(ThaModuleCla self);
ThaClaEthPortController ThaClaPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController ThaClaPwEthPortControllerV2New(ThaModuleCla cla);
ThaClaEthPortController ThaStmPwProductClaPwEthPortControllerV2New(ThaModuleCla cla);

eAtRet ThaClaEthPortControllerMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable);
eBool ThaClaEthPortControllerMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port);

eAtRet ThaClaEthPortControllerMacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr);
eAtRet ThaClaEthPortControllerMacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr);

uint16 ThaClaEthPortControllerSVlanTpid(ThaClaEthPortController self, AtEthPort port);

eAtRet ThaClaEthPortControllerMaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize);
uint32 ThaClaEthPortControllerMaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port);
eAtRet ThaClaEthPortControllerMinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize);
uint32 ThaClaEthPortControllerMinPacketSizeGet(ThaClaEthPortController self, AtEthPort port);

eAtRet ThaClaEthPortControllerIpV4Set(ThaClaEthPortController self, AtEthPort port, uint8 *address);
eAtRet ThaClaEthPortControllerIpV4Get(ThaClaEthPortController self, AtEthPort port, uint8 *address);
eAtRet ThaClaEthPortControllerIpV6Set(ThaClaEthPortController self, AtEthPort port, uint8 *address);
eAtRet ThaClaEthPortControllerIpV6Get(ThaClaEthPortController self, AtEthPort port, uint8 *address);

/* Counters */
uint32 ThaClaEthPortControllerIncomPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerIncombyteRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktBusErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktoversizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktundersizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktLensmallerthan64bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktLenfrom65to127bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktLenfrom128to255bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktLenfrom256to511bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktLenfrom512to1024bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktLenfrom1025to1528bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktJumboLenRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerPWunSuppedRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerHCBElookupnotmatchPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktDscdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerPauFrmRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerPauFrmErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerBcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerARPRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthOamRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthOamRxErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthOamtype0RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthOamtype1RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerIPv4PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerICMPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerIPv6PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerICMPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMEFRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMEFErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMPLSRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMPLSErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMPLSDataRxPktwithPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMPLSDataRxPktwithoutPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerLDPIPv4withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerLDPIPv6withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMPLSoverIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerMPLSoverIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerL2TPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerL2TPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerL2TPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerUDPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerUDPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerUDPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktForwardtoCpuRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktForwardtoPDaRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktTimePktToPRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerRxHdrErrPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerRxErrPsnPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerEthPktLenfrom1529to2047bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerUnicastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);
uint32 ThaClaEthPortControllerPhysicalErrorRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c);

/* Product concretes */
ThaClaEthPortController Tha60031031ClaPwEthPortControllerV2New(ThaModuleCla cla);
ThaClaEthPortController Tha60031032ClaPppEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60031033ClaPppPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60091023ClaEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60091023ClaPppEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60091023ClaPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60210011ClaPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60210051ClaPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60210012ClaPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60210012ClaPppPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60290021ClaPwEthPortControllerNew(ThaModuleCla cla);
ThaClaEthPortController Tha60290081ClaEthPortControllerNew(ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THACLAETHPORTCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaPppEthPortController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA ETH port controller for PPP product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaClaEthPortControllerInternal.h"
#include "../ppp/ThaModuleClaPppReg.h"
#include "../ppp/ThaModuleClaPppInternal.h"
#include "../../eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods  m_ThaClaEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleCla ClaModule(ThaClaController self)
    {
    return ThaClaControllerModuleGet(self);
    }

static AtModuleEth EthModule(ThaClaController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ClaModule((ThaClaController)self));
    return (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static eBool EthPortBypassFcs(ThaClaEthPortController self, AtEthPort port)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self));
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return ThaModuleEthPortShouldByPassFcs(ethModule, (uint8)AtChannelIdGet((AtChannel)port));
    }

static eAtRet MaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    uint32 regAddr, regVal;
    ThaModuleEth ethModule = (ThaModuleEth)EthModule((ThaClaController)self);

    if (EthPortBypassFcs(self, port))
        maxPacketSize = maxPacketSize - 4;

    if (ThaModuleEthPort10GbSupported(ethModule))
        maxPacketSize = maxPacketSize - 8;

    if (EthPortBypassFcs(self, port))
        maxPacketSize = maxPacketSize - 4;

    if (maxPacketSize > (cThaClaPktLenMaxMask >> cThaClaPktLenMaxShift))
        return cAtErrorOutOfRangParm;

    regAddr = cThaClaPacketLengthCtrl + mMethodsGet(self)->EthPortPartOffset(self, port);
    regVal  = mChannelHwRead(port, regAddr, cThaModuleCla);
    mFieldIns(&regVal, cThaClaPktLenMaxMask, cThaClaPktLenMaxShift, maxPacketSize);
    mChannelHwWrite(port, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static uint32 MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 regAddr = cThaClaPacketLengthCtrl + mMethodsGet(self)->EthPortPartOffset(self, port);
    uint32 regVal  = mChannelHwRead(port, regAddr, cThaModuleCla);
    uint32 maxPacketSize = mRegField(regVal, cThaClaPktLenMax);
    ThaModuleEth ethModule = (ThaModuleEth)EthModule((ThaClaController)self);

    if (EthPortBypassFcs(self, port))
        maxPacketSize = maxPacketSize + 4;

    if (ThaModuleEthPort10GbSupported(ethModule))
        maxPacketSize = maxPacketSize + 8;

    return maxPacketSize;
    }

static eAtRet MinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    uint32 regAddr, regVal;

    if (minPacketSize > (cThaClaPktLenMinMask >> cThaClaPktLenMinShift))
        return cAtErrorOutOfRangParm;

    regAddr = cThaClaPacketLengthCtrl +  mMethodsGet(self)->EthPortPartOffset(self, port);
    regVal  = mChannelHwRead(port, regAddr, cThaModuleCla);
    mFieldIns(&regVal, cThaClaPktLenMinMask, cThaClaPktLenMinShift, minPacketSize);
    mChannelHwWrite(port, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static uint32 MinPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 regAddr = cThaClaPacketLengthCtrl +  mMethodsGet(self)->EthPortPartOffset(self, port);
    uint32 regVal  = mChannelHwRead(port, regAddr, cThaModuleCla);
    return mRegField(regVal, cThaClaPktLenMin);
    }

static eAtRet EthPortMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
	AtUnused(port);
	AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool EthPortMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return cAtFalse;
    }

static eBool NeedMaxPacketSizeDefaultSetup(ThaClaEthPortController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaPppEthPortController);
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckEnable);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckIsEnabled);
        mMethodOverride(m_ThaClaEthPortControllerOverride, NeedMaxPacketSizeDefaultSetup);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    }

ThaClaEthPortController ThaClaPppEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController ThaClaPppEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaClaPppEthPortControllerObjectInit(newController, cla);
    }


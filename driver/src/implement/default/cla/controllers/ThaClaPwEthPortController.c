/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaPwEthPortController.c
 *
 * Created Date: Nov 27, 2013
 *
 * Description : CLA controller for Ethernet port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaClaEthPortControllerInternal.h"
#include "../../man/ThaDeviceInternal.h"
#include "../pw/ThaModuleClaPwReg.h"
#include "../ThaModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaClaPwEthPortController
    {
    tThaClaEthPortController super;

    /* Private data */
    }tThaClaPwEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet MacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCLAIdenPseudowireTerDestAddressCtrl + mMethodsGet(self)->EthPortPartOffset(self, port);

    if (macAddr == NULL)
        return cAtError;

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cThaClaIdenMacAddress00DwIndex],
              cThaClaIdenMacAddress00Mask,
              cThaClaIdenMacAddress00Shift,
              macAddr[0]);
    mFieldIns(&longRegVal[cThaClaIdenMacAddress01DwIndex],
              cThaClaIdenMacAddress01Mask,
              cThaClaIdenMacAddress01Shift,
              macAddr[1]);
    mFieldIns(&longRegVal[cThaClaIdenMacAddress02DwIndex],
              cThaClaIdenMacAddress02Mask,
              cThaClaIdenMacAddress02Shift,
              macAddr[2]);
    mFieldIns(&longRegVal[cThaClaIdenMacAddress03DwIndex],
              cThaClaIdenMacAddress03Mask,
              cThaClaIdenMacAddress03Shift,
              macAddr[3]);
    mFieldIns(&longRegVal[cThaClaIdenMacAddress04DwIndex],
              cThaClaIdenMacAddress04Mask,
              cThaClaIdenMacAddress04Shift,
              macAddr[4]);
    mFieldIns(&longRegVal[cThaClaIdenMacAddress05DwIndex],
              cThaClaIdenMacAddress05Mask,
              cThaClaIdenMacAddress05Shift,
              macAddr[5]);
    mChannelHwLongWrite(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet MacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegCLAIdenPseudowireTerDestAddressCtrl + mMethodsGet(self)->EthPortPartOffset(self, port);

    if (macAddr == NULL)
        return cAtError;

    mChannelHwLongRead(port, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldGet(longRegVal[cThaClaIdenMacAddress00DwIndex],
              cThaClaIdenMacAddress00Mask,
              cThaClaIdenMacAddress00Shift,
              uint8,
              &macAddr[0]);
    mFieldGet(longRegVal[cThaClaIdenMacAddress01DwIndex],
              cThaClaIdenMacAddress01Mask,
              cThaClaIdenMacAddress01Shift,
              uint8,
              &macAddr[1]);
    mFieldGet(longRegVal[cThaClaIdenMacAddress02DwIndex],
              cThaClaIdenMacAddress02Mask,
              cThaClaIdenMacAddress02Shift,
              uint8,
              &macAddr[2]);
    mFieldGet(longRegVal[cThaClaIdenMacAddress03DwIndex],
              cThaClaIdenMacAddress03Mask,
              cThaClaIdenMacAddress03Shift,
              uint8,
              &macAddr[3]);
    mFieldGet(longRegVal[cThaClaIdenMacAddress04DwIndex],
              cThaClaIdenMacAddress04Mask,
              cThaClaIdenMacAddress04Shift,
              uint8,
              &macAddr[4]);
    mFieldGet(longRegVal[cThaClaIdenMacAddress05DwIndex],
              cThaClaIdenMacAddress05Mask,
              cThaClaIdenMacAddress05Shift,
              uint8,
              &macAddr[5]);

    return cAtOk;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaPwEthPortController);
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        mMethodOverride(m_ThaClaEthPortControllerOverride, MacSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacGet);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    }

static ThaClaEthPortController ObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController ThaClaPwEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

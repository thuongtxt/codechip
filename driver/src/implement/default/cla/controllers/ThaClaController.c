/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA abstract controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../ThaModuleClaReg.h"
#include "ThaClaControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaClaControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ClaGlbPsnCtrl(ThaClaController self)
    {
	AtUnused(self);
    return cThaRegClaGlbPsnCtrl;
    }

static eAtRet DefaultSet(ThaClaController self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleClaNumParts(ThaClaControllerModuleGet(self)); part_i++)
        ret |= mMethodsGet(self)->PartDefaultSet(self, part_i);

    return ret;
    }

static eBool PartIsUnused(ThaClaController self, uint8 partId)
    {
	AtUnused(partId);
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet Init(ThaClaController self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleClaNumParts(ThaClaControllerModuleGet(self)); part_i++)
        {
        if (mMethodsGet(self)->PartIsUnused(self, part_i))
            continue;
        ret |= mMethodsGet(self)->PartInit(self, part_i);
        ret |= mMethodsGet(self)->PartDefaultSet(self, part_i);
        }

    return ret;
    }

static eAtRet AsyncInit(ThaClaController self)
    {
    return Init(self);
    }

static void Debug(ThaClaController self)
    {
	AtUnused(self);
    /* Let concrete class do */
    }

static eAtRet Setup(ThaClaController self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
	AtUnused(enable);
	AtUnused(channel);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eBool ChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
	AtUnused(channel);
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet PartInit(ThaClaController self, uint8 partId)
    {
	AtUnused(partId);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet PartDefaultSet(ThaClaController self, uint8 partId)
    {
	AtUnused(partId);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet MefOverMplsEnable(ThaClaController self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static uint32 MefOverMplsExpectedEcidGet(ThaClaController self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static eAtRet MefOverMplsExpectedMacGet(ThaClaController self, AtPw pw, uint8 *expectedMac)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(expectedMac);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaClaController object = (ThaClaController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(claModule);
    }

static uint32 Restore(ThaClaController self)
    {
    AtUnused(self);
    return 0;
    }

static void Override(ThaClaController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaController);
    }

static void MethodsInit(ThaClaController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ClaGlbPsnCtrl);
        mMethodOverride(m_methods, DefaultSet);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, AsyncInit);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, PartInit);
        mMethodOverride(m_methods, PartDefaultSet);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, ChannelEnable);
        mMethodOverride(m_methods, ChannelIsEnabled);
        mMethodOverride(m_methods, PartIsUnused);
        mMethodOverride(m_methods, MefOverMplsEnable);
        mMethodOverride(m_methods, MefOverMplsExpectedEcidGet);
        mMethodOverride(m_methods, MefOverMplsExpectedMacGet);
        mMethodOverride(m_methods, Restore);
        }

    mMethodsSet(self, &m_methods);
    }

ThaClaController ThaClaControllerObjectInit(ThaClaController self, ThaModuleCla claModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->claModule = claModule;

    return self;
    }

eAtRet ThaClaControllerChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->ChannelEnable(self, channel, enable);
    return cAtError;
    }

eBool ThaClaControllerChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->ChannelIsEnabled(self, channel);
    return cAtFalse;
    }

ThaModuleCla ThaClaControllerModuleGet(ThaClaController self)
    {
    return self ? self->claModule : NULL;
    }

eAtRet ThaClaControllerInit(ThaClaController self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);

    return cAtError;
    }

eAtRet ThaClaControllerAsyncInit(ThaClaController self)
    {
    if (self)
        return mMethodsGet(self)->AsyncInit(self);

    return cAtError;
    }

eAtRet ThaClaControllerSetup(ThaClaController self)
    {
    if (self)
        return mMethodsGet(self)->Setup(self);
    return cAtError;
    }

eAtRet ThaClaControllerDefaultSet(ThaClaController self)
    {
    if (self)
        return mMethodsGet(self)->DefaultSet(self);
    return cAtError;
    }

uint32 ThaClaControllerRead(ThaClaController self, uint32 address)
    {
    if (self)
        return mModuleHwRead(self->claModule, address);
    return cInvalidValue;
    }

void ThaClaControllerWrite(ThaClaController self, uint32 address, uint32 value)
    {
    if (self)
        mModuleHwWrite(self->claModule, address, value);
    }

void ThaClaControllerDebug(ThaClaController self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

uint32 ThaClaControllerClaGlbPsnCtrl(ThaClaController self)
    {
    if (self)
        return mMethodsGet(self)->ClaGlbPsnCtrl(self);
    return cInvalidValue;
    }

eBool ThaClaControllerPartIsUnused(ThaClaController self, uint8 partId)
    {
    if (self)
        return mMethodsGet(self)->PartIsUnused(self, partId);

    return cAtTrue;
    }

eAtRet ThaClaControllerMefOverMplsEnable(ThaClaController self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->MefOverMplsEnable(self, pw, enable);
    return cAtErrorNullPointer;
    }

uint32 ThaClaControllerMefOverMplsExpectedEcidGet(ThaClaController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->MefOverMplsExpectedEcidGet(self, pw);

    return 0;
    }

eAtRet ThaClaControllerMefOverMplsExpectedMacGet(ThaClaController self, AtPw pw, uint8 *expectedMac)
    {
    if (self)
        return mMethodsGet(self)->MefOverMplsExpectedMacGet(self, pw, expectedMac);

    return cAtError;
    }

uint32 ThaClaControllerRestore(ThaClaController self)
    {
    if (self)
        return mMethodsGet(self)->Restore(self);
    return 0;
    }

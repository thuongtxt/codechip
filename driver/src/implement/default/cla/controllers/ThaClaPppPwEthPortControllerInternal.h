/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaPppPwEthPortControllerInternal.h
 * 
 * Created Date: Nov 29, 2013
 *
 * Description : CLA ETH Port controller for product that has two separate parts,
 *               one for PPP/MLPPP feature and the other one for PW feature
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLAPPPPWETHPORTCONTROLLERINTERNAL_H_
#define _THACLAPPPPWETHPORTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaClaEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClaPppPwEthPortController * ThaClaPppPwEthPortController;

typedef struct tThaClaPppPwEthPortControllerMethods
    {
    ThaClaEthPortController (*PppEthPortControllerCreate)(ThaClaPppPwEthPortController self);
    ThaClaEthPortController (*PwEthPortControllerCreate)(ThaClaPppPwEthPortController self);
    ThaClaEthPortController (*ControllerOfCounter)(ThaClaPppPwEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType);
    ThaClaEthPortController (*ControllerOfPort)(ThaClaPppPwEthPortController self, AtEthPort port);
    }tThaClaPppPwEthPortControllerMethods;

typedef struct tThaClaPppPwEthPortController
    {
    tThaClaEthPortController super;
    const tThaClaPppPwEthPortControllerMethods *methods;

    /* Private data */
    ThaClaEthPortController pppEthPortController;
    ThaClaEthPortController pwEthPortController;
    }tThaClaPppPwEthPortController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController ThaClaPppPwEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla);
ThaClaEthPortController ThaClaPppPwEthPortControllerPppEthPortController(ThaClaPppPwEthPortController self);
ThaClaEthPortController ThaClaPppPwEthPortControllerPwEthPortController(ThaClaPppPwEthPortController self);

ThaClaEthPortController ThaClaPppPwEthPortControllerPppEthPortController(ThaClaPppPwEthPortController self);
ThaClaEthPortController ThaClaPppPwEthPortControllerPwEthPortController(ThaClaPppPwEthPortController self);

#ifdef __cplusplus
}
#endif
#endif /* _THACLAPPPPWETHPORTCONTROLLERINTERNAL_H_ */


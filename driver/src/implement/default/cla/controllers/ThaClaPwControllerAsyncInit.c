/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaPwControllerAsyncInit.c
 *
 * Created Date: Aug 19, 2016
 *
 * Description : Async init implementaion
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../hbce/ThaHbce.h"
#include "../hbce/ThaHbceEntity.h"
#include "../pw/ThaModuleClaPwInternal.h"
#include "../../pw/ThaModulePw.h"
#include "../../eth/ThaModuleEth.h"
#include "../pw/ThaModuleClaPwReg.h"
#include "../pw/ThaModuleClaPw.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"

#include "ThaClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaClaPwController)self)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)

#define mDevice(self) AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self))
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eThaClaPwControllerAsyncInitState
    {
    cThaClaPwControllerAsyncInitStateSupperInit = 0,
    cThaClaPwControllerAsyncInitStateDefaultInit,
    cThaClaPwControllerAsyncInitStateAllCdrDisable,
    cThaClaPwControllerAsyncInitStateHbceHashAndCell
    }eThaClaPwControllerAsyncInitState;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AsyncAllHbceInitStateSet(ThaClaPwController self, uint32 state)
    {
    self->asyncInitAllHbceInitState = state;
    }

static uint32 AsyncAllHbceInitStateGet(ThaClaPwController self)
    {
    return self->asyncInitAllHbceInitState;
    }

static eAtRet AsyncInitAllHbces(ThaClaPwController self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncAllHbceInitStateGet(self);
    ThaHbceEntity hbce;

    if (!mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, (uint8)state))
        {
        hbce = (ThaHbceEntity)AtListObjectGet(mThis(self)->hbces, state);
        ret = ThaHbceEntityAsyncInit(hbce);
        if (ret == cAtOk)
            {
            state += 1;
            ret = cAtErrorAgain;
            }
            
        else if (!AtDeviceAsyncRetValIsInState(ret))
            {
            state = 0;
            AsyncAllHbceInitStateSet(self, state);
            return ret;
            }
        }
    else
        state += 1;

    if (state == ThaModuleClaNumParts(ThaClaControllerModuleGet((ThaClaController)self)))
        {
        ret = cAtOk;
        state = 0;
        }
    else
        ret = cAtErrorAgain;

    AsyncAllHbceInitStateSet(self, state);
    return ret;
    }

static void AsyncStateSet(ThaClaPwController self, uint32 state)
    {
    self->asyncInitState = state;
    }

static uint32 AsyncStateGet(ThaClaPwController self)
    {
    return self->asyncInitState;
    }


eAtRet ThaClaPwControllerAsyncInitMain(ThaClaController self)
    {
    eAtRet ret = cAtOk;
    uint32 state = AsyncStateGet((ThaClaPwController)self);
    tAtOsalCurTime profileTime;
    AtDevice dev = AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet(self));
    AtOsalCurTimeGet(&profileTime);

    switch(state)
        {
        case cThaClaPwControllerAsyncInitStateSupperInit:
            ret = ThaClaPwControllerSuperAsyncInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(dev, cAtTrue, AtSourceLocation, "ThaClaPwControllerSuperInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(dev, cAtTrue, &profileTime, AtSourceLocation, "ThaClaControllerSuperInit");
            if (ret == cAtOk)
                {
                state = cThaClaPwControllerAsyncInitStateDefaultInit;
                ret = cAtErrorAgain;
                }
            break;
        case cThaClaPwControllerAsyncInitStateDefaultInit:
            ret = mMethodsGet(self)->DefaultSet(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(dev, cAtTrue, AtSourceLocation, "DefaultSet");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(dev, cAtTrue, &profileTime, AtSourceLocation, "OldSequenceInitStateMove");
            if (ret == cAtOk)
                {
                state = cThaClaPwControllerAsyncInitStateAllCdrDisable;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cThaClaPwControllerAsyncInitStateSupperInit;
            break;
        case cThaClaPwControllerAsyncInitStateAllCdrDisable:
            ret = ThaClaPwControllerAllCdrDisable(mThis(self));
            AtDeviceAccessTimeCountSinceLastProfileCheck(dev, cAtTrue, AtSourceLocation, "ThaClaPwControllerAllCdrDisable");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(dev, cAtTrue, &profileTime, AtSourceLocation, "OldSequenceInitStateMove");
            if (ret == cAtOk)
                {
                state = cThaClaPwControllerAsyncInitStateHbceHashAndCell;
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
                state = cThaClaPwControllerAsyncInitStateSupperInit;
            break;
        case cThaClaPwControllerAsyncInitStateHbceHashAndCell:
            ret = AsyncInitAllHbces((ThaClaPwController)self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(dev, cAtTrue, AtSourceLocation, "AsyncInitAllHbces");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(dev, cAtTrue, &profileTime, AtSourceLocation, "OldSequenceInitStateMove");
            if (ret == cAtOk || !AtDeviceAsyncRetValIsInState(ret))
                state = cThaClaPwControllerAsyncInitStateSupperInit;
            break;
        default:
            state = cThaClaPwControllerAsyncInitStateSupperInit;
            ret = cAtErrorDevFail;
       }

    AsyncStateSet((ThaClaPwController)self, state);
    return ret;
    }

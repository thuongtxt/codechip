/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaPwController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA controller for PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwHdlc.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../hbce/ThaHbce.h"
#include "../hbce/ThaHbceEntity.h"
#include "../pw/ThaModuleClaPwInternal.h"
#include "../../pw/ThaModulePw.h"
#include "../../eth/ThaModuleEth.h"
#include "../pw/ThaModuleClaPwReg.h"
#include "../pw/ThaModuleClaPw.h"
#include "../../pw/headercontrollers/ThaPwHeaderController.h"

#include "ThaClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaClaPwController)self)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)

#define mDevice(self) AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self))
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaClaPwControllerMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tThaClaControllerMethods m_ThaClaControllerOverride;

/* Save super implementation */
static const tAtObjectMethods         *m_AtObjectMethods         = NULL;
static const tThaClaControllerMethods *m_ThaClaControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineRegAdress(ThaClaController,   ClaGlbPsnCtrl)
mDefineRegAdress(ThaClaPwController, CLAHBCEGlbCtrl)
mDefineRegAdress(ThaClaPwController, CLAHBCEHashingTabCtrl)
mDefineRegAdress(ThaClaPwController, ClaHbceLookingUpInformationCtrl)

mDefineMaskShift(ThaClaPwController, ClaHbceFlowId)
mDefineMaskShift(ThaClaPwController, ClaHbceFlowEnb)
mDefineMaskShift(ThaClaPwController, ClaHbceStoreId)

mDefineMaskShift(ThaClaPwController, ClaHbceCodingSelectedMode)
mDefineMaskShift(ThaClaPwController, ClaHbceHashingTabSelectedPage)
mDefineMaskShift(ThaClaPwController, ClaHbceTimeoutVal)

mDefineMaskShift(ThaClaPwController, CLAHbceFlowNum)
mDefineMaskShift(ThaClaPwController, CLAHbceMemoryStartPtr)

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaPwController);
    }

static ThaHbce HbceCreate(ThaClaPwController self, uint8 hbceId, AtIpCore core)
    {
    return ThaHbceNew(hbceId, (AtModule)mClaModule(self), core);
    }

static void DeleteAllHbces(ThaClaPwController self)
    {
    AtListDeleteWithObjectHandler(self->hbces, AtObjectDelete);
    self->hbces = NULL;
    }

static eAtRet CreateAllHbces(ThaClaPwController self)
    {
    uint8 part_i;
    AtIpCore core;

    if (self->hbces)
        DeleteAllHbces(self);

    self->hbces = AtListCreate(ThaModuleClaNumParts(mClaModule(self)));
    if (self->hbces == NULL)
        return cAtErrorRsrcNoAvail;

    core = AtDeviceIpCoreGet(mDevice(self), AtModuleDefaultCoreGet((AtModule)mClaModule(self)));
    for (part_i = 0; part_i < ThaModuleClaNumParts(mClaModule(self)); part_i++)
        {
        ThaHbce newHbce = mMethodsGet(self)->HbceCreate(self, part_i, core);
        if (newHbce == NULL)
            {
            DeleteAllHbces(self);
            return cAtErrorRsrcNoAvail;
            }

        AtListObjectAdd(self->hbces, (AtObject)newHbce);
        }

    return cAtOk;
    }

static eAtRet HbceSetup(ThaClaPwController self)
    {
    /* Do not need to check error code, not all of product support HBCEs */
    CreateAllHbces(self);
    return cAtOk;
    }

static eAtRet Setup(ThaClaController self)
    {
    /* Super setup */
    eAtRet ret = m_ThaClaControllerMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Create HBCE */
    return HbceSetup(mThis(self));
    }

static void Delete(AtObject self)
    {
    DeleteAllHbces(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static uint32 PwDefaultOffset(ThaClaPwController self, AtPw pw)
    {
    return mPwHwId(pw) + ThaClaPwControllerPartOffset(self, pw);
    }

static eAtRet PwRtpEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;

    regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cThaClaDecRtpFieldEnDwIndex],
              cThaClaDecRtpFieldEnMask,
              cThaClaDecRtpFieldEnShift,
              enable ? 1 : 0);

    /* Do not need to configure if RTP needs to be disabled */
    if (enable)
        {
        eAtPwPsnType psnType;
        eBool ipPsn;

        psnType = AtPwPsnTypeGet(AtPwPsnGet(pw));
        ipPsn = (psnType == cAtPwPsnTypeIPv4) || (psnType == cAtPwPsnTypeIPv6);

        mFieldIns(&longRegVal[cThaClaDecRtpPositonDwIndex],
                  cThaClaDecRtpPositonMask,
                  cThaClaDecRtpPositonShift,
                  ipPsn ? 1 : 0);
        }

    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwRtpIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;
    uint8 bitVal;

    regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    bitVal = (uint8)(mRegField(longRegVal[cThaClaDecRtpFieldEnDwIndex], cThaClaDecRtpFieldEn));

    return ((bitVal > 0) ? cAtTrue : cAtFalse);
    }

static eAtRet PwRtpSsrcSet(ThaClaPwController self, AtPw pw, uint32 ssrc)
    {
    uint32 ssrc_Head, ssrc_Tail;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mFieldGet(ssrc, cThaClaDecRtpSSRCValSwHeadMask, cThaClaDecRtpSSRCValSwHeadShift, uint32, &ssrc_Head);
    mFieldGet(ssrc, cThaClaDecRtpSSRCValSwTailMask, cThaClaDecRtpSSRCValSwTailShift, uint32, &ssrc_Tail);

    mFieldIns(&longRegVal[cThaClaDecRtpSSRCValDwIndex + 1],
              cThaClaDecRtpSSRCValHwHeadMask,
              cThaClaDecRtpSSRCValHwHeadShift,
              ssrc_Head);
    mFieldIns(&longRegVal[cThaClaDecRtpSSRCValDwIndex],
              cThaClaDecRtpSSRCValHwTailMask,
              cThaClaDecRtpSSRCValHwTailShift,
              ssrc_Tail);

    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 PwRtpSsrcGet(ThaClaPwController self, AtPw pw)
    {
    uint32 ssrc_Head, ssrc_Tail;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);
    uint32 ssrc = 0;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    ssrc_Head = mRegField(longRegVal[cThaClaDecRtpSSRCValDwIndex + 1], cThaClaDecRtpSSRCValHwHead);
    ssrc_Tail = mRegField(longRegVal[cThaClaDecRtpSSRCValDwIndex], cThaClaDecRtpSSRCValHwTail);
    mRegFieldSet(ssrc, cThaClaDecRtpSSRCValSwHead, ssrc_Head);
    mRegFieldSet(ssrc, cThaClaDecRtpSSRCValSwTail, ssrc_Tail);

    return ssrc;
    }

static eAtRet PwRtpPayloadTypeSet(ThaClaPwController self, AtPw pw, uint8 payloadType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cThaClaDecRtpPldTypeDwIndex],
              cThaClaDecRtpPldTypeMask,
              cThaClaDecRtpPldTypeShift,
              payloadType);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint8 PwRtpPayloadTypeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (uint8)mRegField(longRegVal[cThaClaDecRtpPldTypeDwIndex], cThaClaDecRtpPldType);
    }

static uint8 PwCwLengthModeToHwValue(eAtPwCwLengthMode lengthMode)
    {
    if (lengthMode == cAtPwCwLengthModeFullPacket) return 0;
    if (lengthMode == cAtPwCwLengthModePayload)    return 1;

    return 0;
    }

static eAtRet PwCwLengthModeSet(ThaClaPwController self, AtPw pw, eAtPwCwLengthMode lengthMode)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cThaClaDecCwLenFieldModeDwIndex],
              cThaClaDecCwLenFieldModeMask,
              cThaClaDecCwLenFieldModeShift,
              PwCwLengthModeToHwValue(lengthMode));
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwCwIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (longRegVal[cThaClaDecCwTypeDwIndex] & cThaClaDecCwTypeMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwCwEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cThaClaDecCwTypeDwIndex],
              cThaClaDecCwTypeMask,
              cThaClaDecCwTypeShift,
              mBoolToBin(enable));
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwReorderEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[cThaClaDecCwTypeDwIndex], cThaClaDecPktReOrderEn, enable ? 1 : 0);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwReorderIsEnable(ThaClaPwController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (longRegVal[cThaClaDecCwTypeDwIndex] & cThaClaDecPktReOrderEnMask) ? cAtTrue : cAtFalse;
    }

static uint32 RxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapForwardedtoPDAPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static uint32 RxMalformedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapMalformedPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static uint32 RxStrayPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	AtUnused(clear);
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static uint32 RxDuplicatedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    AtUnused(clear);
    AtUnused(pw);
    AtUnused(self);
    return 0;
    }

static uint32 RxOamPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapForwardedtoCpuPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static uint32 RxLbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapLBitPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static uint32 RxRbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapRBitPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static uint32 RxMbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapMBitPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static uint32 RxNbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapMBitPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static uint32 RxPbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    uint32 address = cThaRegClaDecapPBitPktCnt(clear) + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, address, cThaModuleCla);

    return regVal;
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtPw pw = (AtPw)channel;
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[cThaClaDecPseudowireEnDwIndex], cThaClaDecPseudowireEn, enable ? 1 : 0);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    /* Not need to check return code because some product does not support HBCE engine */
    ThaPwAdapterHbceEnable(pw, enable);

    return cAtOk;
    }

static eBool ChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    AtPw pw = (AtPw)channel;
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    if (ThaPwAdapterHbceIsEnabled(pw))
        return cAtTrue;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (longRegVal[cThaClaDecPseudowireEnDwIndex] & cThaClaDecPseudowireEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet CdrEnable(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[cThaClaDecCDRIdDwIndex], cThaClaDecCDRId, cdrId);
    mRegFieldSet(longRegVal[cThaClaDecInfoForCDREnDwIndex], cThaClaDecInfoForCDREn, enable ? 1 : 0);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool CdrIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint8 enable;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldGet(longRegVal[cThaClaDecInfoForCDREnDwIndex],
              cThaClaDecInfoForCDREnMask,
              cThaClaDecInfoForCDREnShift,
              uint8,
              &enable);

    return mBinToBool(enable);
    }

static uint16 CdrIdGet(ThaClaPwController self, AtPw pw)
    {
    uint16 cdrId;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldGet(longRegVal[cThaClaDecInfoForCDREnDwIndex],
              cThaClaDecInfoForCDREnMask,
              cThaClaDecInfoForCDREnShift,
              uint8,
              &cdrId);

    return cdrId;
    }

static uint8 HwPwType(eAtPwType pwType)
    {
    if (pwType == cAtPwTypeSAToP) return 0x0;
    if (pwType == cAtPwTypeCESoP) return 0x0;
    if (pwType == cAtPwTypeCEP)   return 0x3;
    if (pwType == cAtPwTypeATM)   return 0x1;

    return 0x3;
    }

static eBool PwTypeIsSupported(ThaClaPwController self, eAtPwType pwType)
    {
	AtUnused(pwType);
	AtUnused(self);
    return cAtTrue;
    }

static uint8 PwType2HwPayloadType(eAtPwType pwType)
    {
    if (pwType == cAtPwTypeCESoP)
        return 1;
    return 0;
    }

static eAtRet PwTypeSet(ThaClaPwController self, AtPw pw, eAtPwType pwType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[cThaClaDecPwTypeDwIndex], cThaClaDecPwType, HwPwType(pwType));
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[cThaClaDecPldTypeDwIndex], cThaClaDecPldType, PwType2HwPayloadType(pwType));
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PayloadSizeSet(ThaClaPwController self, AtPw pw, uint16 payloadSize)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mFieldIns(&longRegVal[cThaClaDecPldSizeDwIndex],
              cThaClaDecPldSizeMask,
              cThaClaDecPldSizeShift,
              payloadSize);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwLabelSet(ThaClaPwController self, AtPw pw, AtPwPsn psn)
    {
	AtUnused(psn);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PartPwCdrDisable(ThaClaPwController self, uint32 localPwId, uint8 partId, AtIpCore core)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleCla moduleCla = ThaClaControllerModuleGet((ThaClaController)self);

    uint32 offset = ThaModuleClaPartOffset(moduleCla, partId);
    uint32 address = cThaRegClaDecapPwCtrl + localPwId + offset;

    mModuleHwLongRead(moduleCla, address, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[cThaClaDecInfoForCDREnDwIndex], cThaClaDecInfoForCDREn, 0);
    mModuleHwLongWrite(moduleCla, address, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static ThaModulePw PwModule(ThaClaPwController self)
    {
    return (ThaModulePw)AtDeviceModuleGet(mDevice(self), cAtModulePw);
    }

static eAtRet PartAllCdrDisable(ThaClaPwController self, uint8 partId)
    {
    uint32 i;
    AtIpCore defaultCore = AtDeviceIpCoreGet(mDevice(self), AtModuleDefaultCoreGet((AtModule)mClaModule(self)));
    eAtRet ret = cAtOk;

    for (i = 0; i < ThaModulePwNumPwsPerPart(PwModule(self)); i++)
        ret |= mMethodsGet(self)->PartPwCdrDisable(self, i, partId, defaultCore);

    return ret;
    }

static eAtRet PartDefaultSet(ThaClaPwController self, uint8 partId)
    {
    uint32 regAddr;
    uint8 longRegSize = 5;
    uint32 dataBuffer[5];
    AtModule module = (AtModule)mClaModule(self);
    AtIpCore ipcore = AtDeviceIpCoreGet(AtModuleDeviceGet(module), AtModuleDefaultCoreGet(module));
    uint32 partOffset = ThaModuleClaPartOffset((ThaModuleCla)module, partId);

    dataBuffer[0] = 0xFFFFFFFF;
    dataBuffer[1] = 0xFFFFFFFF;
    dataBuffer[2] = 0xFFFFFFFF;
    dataBuffer[3] = 0xFFFFFFFF;
    dataBuffer[4] = 0;

    mModuleHwLongWrite(module, cThaRegClaIdENEthernetOamAddress1Ctrl     + partOffset, dataBuffer, longRegSize, ipcore);
    mModuleHwLongWrite(module, cThaRegClaIdENMLPppterminationDaCtrl      + partOffset, dataBuffer, longRegSize, ipcore);
    mModuleHwLongWrite(module, cThaRegCLAIdENMcastandBcastGrpAddressCtrl + partOffset, dataBuffer, longRegSize, ipcore);
    mModuleHwLongWrite(module, cThaRegClaIdENEthernetOamAddress2Ctrl     + partOffset, dataBuffer, longRegSize, ipcore);

    regAddr = cThaRegClaIdENMACDscdandForwardCtrl + partOffset;
    mModuleHwLongRead(module, regAddr, dataBuffer, longRegSize, ipcore);
    mRegFieldSet(dataBuffer[cThaClaIdenForwardDscdDaMcastPktDwIndex], cThaClaIdenForwardDscdDaBcastPkt, 1);
    mModuleHwLongWrite(module, regAddr, dataBuffer, longRegSize, ipcore);

    dataBuffer[0] = 0x0;
    dataBuffer[1] = 0x0;
    dataBuffer[2] = 0x0;
    dataBuffer[3] = 0x0;
    dataBuffer[4] = 0;
    mModuleHwLongWrite(module, cThaRegClaDecapPwSetDeftValCtrl + partOffset, dataBuffer, longRegSize, ipcore);

    regAddr = mMethodsGet((ThaClaController)self)->ClaGlbPsnCtrl((ThaClaController)self) + partOffset;
    mModuleHwLongRead(module, regAddr, dataBuffer, longRegSize, ipcore);
    mRegFieldSet(dataBuffer[cThaClaIdenBypassHBCEDwIndex], cThaClaIdenBypassHBCE, 0);

    /* Disable PHP checking */
    mRegFieldSet(dataBuffer[cThaCLAIdenPHPCheckEnableDwIndex], cThaCLAIdenPHPCheckEnable, 0);
    mRegFieldSet(dataBuffer[cThaClaIdenPHPModeDwIndex], cThaClaIdenPHPMode, 1);

    /* Packet max length (in bytes) */
    mRegFieldSet(dataBuffer[cThaClaIdenMaxLenDwIndex], cThaClaIdenMaxLen, 10000);
    mModuleHwLongWrite(module, regAddr, dataBuffer, longRegSize, ipcore);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaClaController self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleClaNumParts(ThaClaControllerModuleGet(self)); part_i++)
        {
        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;

        ret |= PartDefaultSet(mThis(self), part_i);
        }

    return ret;
    }

static eAtRet InitAllHbces(ThaClaController self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    if (mThis(self)->hbces == NULL)
        return cAtOk;

    for (part_i = 0; part_i < ThaModuleClaNumParts(ThaClaControllerModuleGet(self)); part_i++)
        {
        ThaHbceEntity hbce;
        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;

        hbce = (ThaHbceEntity)AtListObjectGet(mThis(self)->hbces, part_i);
        ret |= ThaHbceEntityInit(hbce);
        }

    return ret;
    }

static eAtRet Init(ThaClaController self)
    {
    eAtRet ret = cAtOk;

    ret |= ThaClaPwControllerSuperInit(self);

    ret |= mMethodsGet(self)->DefaultSet(self);

    ret |= ThaClaPwControllerAllCdrDisable(mThis(self));

    ret |= InitAllHbces(self);

    return ret;
    }

static eAtRet AsyncInit(ThaClaController self)
    {
    return ThaClaPwControllerAsyncInitMain(self);
    }

static void Debug(ThaClaController self)
    {
    uint8 part_i;
    ThaHbceEntity hbce;

    if (mThis(self)->hbces == NULL)
        return;

    if (AtListLengthGet(mThis(self)->hbces) == 1)
        {
        hbce = (ThaHbceEntity)AtListObjectGet(mThis(self)->hbces, 0);
        ThaHbceEntityDebug(hbce);
        return;
        }

    for (part_i = 0; part_i < ThaModuleClaNumParts(mClaModule(self)); part_i++)
        {
        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;

        AtPrintc(cSevInfo, "* HBCE#%d\r\n", part_i + 1);
        hbce = (ThaHbceEntity)AtListObjectGet(mThis(self)->hbces, part_i);
        ThaHbceEntityDebug(hbce);
        }

    return;
    }

static uint32 AlarmsHw2Sw(uint32 hwAlarms)
    {
    uint32 alarms  = 0;

    if (hwAlarms & cThaClaDecRxMBitCurStatusMask) alarms |= cAtPwAlarmTypeMBit;
    if (hwAlarms & cThaClaDecRxRBitCurStatusMask) alarms |= cAtPwAlarmTypeRBit;
    if (hwAlarms & cThaClaDecRxLBitCurStatusMask) alarms |= cAtPwAlarmTypeLBit;

    return alarms;
    }

static uint32 PwAlarmGet(ThaClaPwController self, AtPw pw)
    {
    uint32 regAddr = cThaRegClaDecapPwAlm + mClaPwOffset(self, pw);
    return AlarmsHw2Sw(mChannelHwRead(pw, regAddr, cThaModuleCla));
    }

static uint32 PwDefectHistoryGet(ThaClaPwController self, AtPw pw, eBool read2Clear)
    {
    uint32 regAddr = cThaRegClaDecapPwAlmIntr + mClaPwOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModuleCla);
    uint32 history = AlarmsHw2Sw(regVal);

    if (read2Clear)
        mChannelHwWrite(pw, regAddr, regVal, cThaModuleCla);

    return history;
    }

static uint8 PwErrorCheckModeSw2Hw(eThaPwErrorCheckingMode errorCheckingMode)
    {
    if (errorCheckingMode == cThaPwErrorCheckingModeNoCheck)
        return 0;
    if (errorCheckingMode == cThaPwErrorCheckingModeCheckCountAndDiscard)
        return 2;
    if (errorCheckingMode == cThaPwErrorCheckingModeCheckCountAndNotDiscard)
        return 3;

    return 0;
    }

static eThaPwErrorCheckingMode PwErrorCheckModeHw2Sw(uint32 hwMode)
    {
    if (hwMode == 0) return cThaPwErrorCheckingModeNoCheck;
    if (hwMode == 2) return cThaPwErrorCheckingModeCheckCountAndDiscard;
    if (hwMode == 3) return cThaPwErrorCheckingModeCheckCountAndNotDiscard;

    return cThaPwErrorCheckingModeNoCheck;
    }

static eAtRet PwRtpSequenceMistmatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[cThaClaDecSeqMisMatchChkEnDwIndex], cThaClaDecSeqMisMatchChkEn, PwErrorCheckModeSw2Hw(checkMode));
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwRtpSsrcMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[cThaClaDecRtpSSRCMisMatchChkEnDwIndex], cThaClaDecRtpSSRCMisMatchChkEn, PwErrorCheckModeSw2Hw(checkMode));
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eThaPwErrorCheckingMode PwRtpSsrcMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return PwErrorCheckModeHw2Sw(mRegField(pdRegVal[cThaClaDecRtpSSRCMisMatchChkEnDwIndex], cThaClaDecRtpSSRCMisMatchChkEn));
    }

static eAtRet PwRtpPldTypeMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[cThaClaDecRtpPldTypeMisMatchChkEnDwIndex], cThaClaDecRtpPldTypeMisMatchChkEn, PwErrorCheckModeSw2Hw(checkMode));
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eThaPwErrorCheckingMode PwRtpPldTypeMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return PwErrorCheckModeHw2Sw(mRegField(pdRegVal[cThaClaDecRtpPldTypeMisMatchChkEnDwIndex], cThaClaDecRtpPldTypeMisMatchChkEn));
    }

static eAtRet PwMalformCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[cThaClaDecMalformChkEnDwIndex], cThaClaDecMalformChkEn, PwErrorCheckModeSw2Hw(checkMode));
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwRealLenCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[cThaClaDecRealLenChkEnDwIndex], cThaClaDecRealLenChkEn, (checkMode == cThaPwErrorCheckingModeNoCheck) ? 0 : 1);
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet PwLenFieldCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cThaRegClaDecapPwCtrl + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[cThaClaDecFieldLenChkEnDwIndex], cThaClaDecFieldLenChkEn, (checkMode == cThaPwErrorCheckingModeNoCheck) ? 0 : 1);
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwLookupModeIsSupported(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
	AtUnused(pw);
	AtUnused(self);
    return (lookupMode == cThaPwLookupModePsn) ? cAtTrue : cAtFalse;
    }

static eThaPwLookupMode PartPwLookupModeGet(ThaClaPwController self, uint8 partId)
    {
    AtUnused(self);
    AtUnused(partId);
    return cThaPwLookupModePsn;
    }

static eAtRet PwLookupModeSet(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
	AtUnused(pw);
	AtUnused(self);
    if (lookupMode == cThaPwLookupModePsn)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eThaPwLookupMode PwLookupModeGet(ThaClaPwController self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return cThaPwLookupModePsn;
    }

static eAtRet PwExpectedCVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *cVlan)
    {
	AtUnused(cVlan);
	AtUnused(pw);
	AtUnused(self);
    /* Concrete must do */
    return cAtErrorModeNotSupport;
    }

static eAtRet PwExpectedSVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *sVlan)
    {
	AtUnused(sVlan);
	AtUnused(pw);
	AtUnused(self);
    /* Concrete must do */
    return cAtErrorModeNotSupport;
    }

static eAtRet PwLookupRemove(ThaClaPwController self, AtPw pwAdapter, eBool applyHardware)
    {
    ThaPwAdapter adapter = (ThaPwAdapter)pwAdapter;
    ThaHbce hbce;
    eAtRet ret = cAtOk;
    ThaPwHeaderController controller;

    if (adapter == NULL)
        return cAtOk;

    hbce = ThaClaPwControllerHbceGet(self, pwAdapter);
    if (hbce == NULL)
        return cAtOk;

    controller = ThaPwAdapterHeaderController(adapter);
    if (ThaPwHeaderControllerHbceMemoryCellContentGet(controller))
        {
        ret |= ThaHbcePwRemove(hbce, controller);
        if (applyHardware)
            ret |= ThaHbceApply2Hardware(hbce);
        }

    return ret;
    }

static eAtRet PwLookupEnable(ThaClaPwController self, AtPw pwAdapter, eBool enable)
    {
    ThaPwHeaderController controller;
    eAtRet ret = cAtOk;

    if (pwAdapter == NULL)
        return cAtOk;

    controller = ThaPwAdapterHeaderController((ThaPwAdapter)pwAdapter);
    if (ThaPwHeaderControllerHbceMemoryCellContentGet(ThaPwAdapterHeaderController((ThaPwAdapter)pwAdapter)))
        {
        ThaHbce hbce = ThaClaPwControllerHbceGet(self, pwAdapter);
        ret |= ThaHbcePwHeaderControllerEnable(hbce, controller, enable);
        }

    return ret;
    }

static eBool PwExpectedVlanTagsConflict(ThaClaPwController self,
                                        tAtEthVlanTag *expectedCVlan1, tAtEthVlanTag *expectedSVlan1,
                                        tAtEthVlanTag *expectedCVlan2, tAtEthVlanTag *expectedSVlan2)
    {
	AtUnused(expectedSVlan2);
	AtUnused(expectedCVlan2);
	AtUnused(expectedSVlan1);
	AtUnused(expectedCVlan1);
	AtUnused(self);
    return cAtFalse;
    }

static uint8 HbceMaxFlowsPerEntry(ThaClaPwController self)
    {
	AtUnused(self);
    return 16;
    }

static eAtRet PwCepEbmEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
	AtUnused(enable);
	AtUnused(pw);
	AtUnused(self);
    return cAtError;
    }

static uint8 PartOfPw(ThaClaPwController self, AtPw pw)
    {
    return ThaModulePwPartOfPw(PwModule(self), pw);
    }

static eAtRet SendLbitPacketToCdrEnable(ThaClaPwController self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtOk;
    }

static eBool SendLbitPacketToCdrIsEnabled(ThaClaPwController self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet CdrCircuitSliceSet(ThaClaPwController self, AtPw pw, uint8 slice)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(slice);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    ThaClaPwController object = (ThaClaPwController)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeList(hbces);
    mEncodeUInt(asyncInitState);
    mEncodeUInt(asyncInitAllHbceInitState);
    }

static const char *ToString(AtObject self)
    {
    AtModule module = (AtModule)ThaClaControllerModuleGet((ThaClaController)self);
    AtDevice dev = AtModuleDeviceGet(module);
    static char str[64];

    AtSprintf(str, "%scla_pw_controller", AtDeviceIdToString(dev));
    return str;
    }

static uint32 Restore(ThaClaController self)
    {
    AtList hbces = mThis(self)->hbces;
    AtIterator iterator;
    ThaHbceEntity hbce;
    uint32 remained = 0;

    iterator = AtListIteratorCreate(hbces);
    while ((hbce = (ThaHbceEntity)AtIteratorNext(iterator)))
        {
        uint8 page = ThaHbceActivePage((ThaHbce)hbce);
        remained += ThaHbceEntityRestore(hbce, page);
        }
    AtObjectDelete((AtObject)iterator);

    return remained;
    }

static void PktAnalyzerCla2CdrDataFlush(ThaClaPwController self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    }

static eAtRet PktAnalyzerCla2CdrTrigger(ThaClaPwController self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtErrorNotImplemented;
    }

static eBool PktAnalyzerCla2CdrHoIsEnabled(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(packetId);
    return cAtFalse;
    }

static eBool PktAnalyzerCla2CdrPacketIsError(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(packetId);
    return cAtFalse;
    }

static uint32 PktAnalyzerCla2CdrRtpTimeStampGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(packetId);
    return 0;
    }

static uint32 PktAnalyzerCla2CdrRtpTimeStampOffset(ThaClaPwController self, AtPw pw, uint16 previousPktId, uint16 nextPktId)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(previousPktId);
    AtUnused(nextPktId);
    return 0;
    }

static uint32 PktAnalyzerCla2CdrRtpSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(packetId);
    return 0;
    }

static uint32 PktAnalyzerCla2CdrControlWordSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(packetId);
    return 0;
    }

static void PwRegsShow(ThaClaPwController self, AtPw pw)
    {
    uint32 offset = mClaPwOffset(self, pw);
    uint32 address = cThaRegClaDecapPwCtrl + offset;
    ThaModuleClaLongRegDisplay(pw, "CLA Decap Pseudowire Control", address);
    }

static eAtRet PwSuppressEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static eBool PwSuppressIsEnabled(ThaClaPwController self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtFalse;
    }

static uint32 RxDiscardedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    /* Most of products do not support this counter, it is calculated from other counters,
     * see ThaPwAdapter.c for more information
     */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(clear);

    return 0;
    }

static eAtRet HdlcPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(payloadType);
    return cAtErrorNotImplemented;
    }

static eAtRet MlpppPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(payloadType);
    return cAtErrorNotImplemented;
    }

static eAtPwHdlcPayloadType DefaultPayloadType(ThaClaPwController self)
    {
    AtUnused(self);
    return cAtPwHdlcPayloadTypeFull;
    }

static void MethodsInit(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_methods, PwDefaultOffset);
        mMethodOverride(m_methods, PartOfPw);
        mMethodOverride(m_methods, PwRtpEnable);
        mMethodOverride(m_methods, PwRtpIsEnabled);
        mMethodOverride(m_methods, PwRtpSsrcSet);
        mMethodOverride(m_methods, PwRtpSsrcGet);
        mMethodOverride(m_methods, PwRtpPayloadTypeSet);
        mMethodOverride(m_methods, PwRtpPayloadTypeGet);
        mMethodOverride(m_methods, PwCwLengthModeSet);
        mMethodOverride(m_methods, PwCwIsEnabled);
        mMethodOverride(m_methods, PwCwEnable);
        mMethodOverride(m_methods, PwReorderEnable);
        mMethodOverride(m_methods, PwReorderIsEnable);
        mMethodOverride(m_methods, HbceCreate);
        mMethodOverride(m_methods, HbceMaxFlowsPerEntry);
        mMethodOverride(m_methods, RxPacketsGet);
        mMethodOverride(m_methods, RxMalformedPacketsGet);
        mMethodOverride(m_methods, RxStrayPacketsGet);
        mMethodOverride(m_methods, RxDuplicatedPacketsGet);
        mMethodOverride(m_methods, RxOamPacketsGet);
        mMethodOverride(m_methods, RxLbitPacketsGet);
        mMethodOverride(m_methods, RxRbitPacketsGet);
        mMethodOverride(m_methods, RxMbitPacketsGet);
        mMethodOverride(m_methods, RxNbitPacketsGet);
        mMethodOverride(m_methods, RxPbitPacketsGet);
        mMethodOverride(m_methods, CdrEnable);
        mMethodOverride(m_methods, CdrIsEnabled);
        mMethodOverride(m_methods, CdrIdGet);
        mMethodOverride(m_methods, PartPwCdrDisable);
        mMethodOverride(m_methods, PwTypeSet);
        mMethodOverride(m_methods, PwTypeIsSupported);
        mMethodOverride(m_methods, PayloadSizeSet);
        mMethodOverride(m_methods, RxDiscardedPacketsGet);

        mMethodOverride(m_methods, CLAHBCEGlbCtrl);
        mMethodOverride(m_methods, CLAHBCEHashingTabCtrl);
        mMethodOverride(m_methods, ClaHbceLookingUpInformationCtrl);
        mMethodOverride(m_methods, PwLabelSet);
        mMethodOverride(m_methods, PwAlarmGet);
        mMethodOverride(m_methods, PwDefectHistoryGet);

        /* Error checking */
        mMethodOverride(m_methods, PwRtpSequenceMistmatchCheckModeSet);
        mMethodOverride(m_methods, PwRtpSsrcMismatchCheckModeSet);
        mMethodOverride(m_methods, PwRtpSsrcMismatchCheckModeGet);
        mMethodOverride(m_methods, PwRtpPldTypeMismatchCheckModeSet);
        mMethodOverride(m_methods, PwRtpPldTypeMismatchCheckModeGet);
        mMethodOverride(m_methods, PwMalformCheckModeSet);
        mMethodOverride(m_methods, PwRealLenCheckModeSet);
        mMethodOverride(m_methods, PwLenFieldCheckModeSet);

        /* Global configuration */
        mMethodOverride(m_methods, SendLbitPacketToCdrEnable);
        mMethodOverride(m_methods, SendLbitPacketToCdrIsEnabled);

        /* Lookup mode */
        mMethodOverride(m_methods, PwLookupModeIsSupported);
        mMethodOverride(m_methods, PartPwLookupModeGet);
        mMethodOverride(m_methods, PwLookupModeSet);
        mMethodOverride(m_methods, PwLookupModeGet);
        mMethodOverride(m_methods, PwExpectedCVlanSet);
        mMethodOverride(m_methods, PwExpectedSVlanSet);
        mMethodOverride(m_methods, PwLookupRemove);
        mMethodOverride(m_methods, PwLookupEnable);
        mMethodOverride(m_methods, PwExpectedVlanTagsConflict);
        mMethodOverride(m_methods, PwCepEbmEnable);
        mMethodOverride(m_methods, CdrCircuitSliceSet);
        mMethodOverride(m_methods, PwRegsShow);
        mMethodOverride(m_methods, PwSuppressEnable);
        mMethodOverride(m_methods, PwSuppressIsEnabled);

        /* Packet analyzer */
        mMethodOverride(m_methods, PktAnalyzerCla2CdrDataFlush);
        mMethodOverride(m_methods, PktAnalyzerCla2CdrTrigger);
        mMethodOverride(m_methods, PktAnalyzerCla2CdrHoIsEnabled);
        mMethodOverride(m_methods, PktAnalyzerCla2CdrPacketIsError);
        mMethodOverride(m_methods, PktAnalyzerCla2CdrRtpTimeStampGet);
        mMethodOverride(m_methods, PktAnalyzerCla2CdrRtpTimeStampOffset);
        mMethodOverride(m_methods, PktAnalyzerCla2CdrRtpSequenceNumberGet);
        mMethodOverride(m_methods, PktAnalyzerCla2CdrControlWordSequenceNumberGet);

        mMethodOverride(m_methods, HdlcPwPayloadTypeSet);
        mMethodOverride(m_methods, MlpppPwPayloadTypeSet);
        mMethodOverride(m_methods, DefaultPayloadType);

        /* Bit field override */
        mBitFieldOverride(ThaClaPwController, m_methods, ClaHbceFlowId)
        mBitFieldOverride(ThaClaPwController, m_methods, ClaHbceFlowEnb)
        mBitFieldOverride(ThaClaPwController, m_methods, ClaHbceStoreId)

        mBitFieldOverride(ThaClaPwController, m_methods, ClaHbceCodingSelectedMode)
        mBitFieldOverride(ThaClaPwController, m_methods, ClaHbceHashingTabSelectedPage)
        mBitFieldOverride(ThaClaPwController, m_methods, ClaHbceTimeoutVal)

        mBitFieldOverride(ThaClaPwController, m_methods, CLAHbceFlowNum)
        mBitFieldOverride(ThaClaPwController, m_methods, CLAHbceMemoryStartPtr)
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(ThaClaPwController self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaClaController(ThaClaPwController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, m_ThaClaControllerMethods, sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, Init);
        mMethodOverride(m_ThaClaControllerOverride, AsyncInit);
        mMethodOverride(m_ThaClaControllerOverride, Setup);
        mMethodOverride(m_ThaClaControllerOverride, DefaultSet);
        mMethodOverride(m_ThaClaControllerOverride, Debug);
        mMethodOverride(m_ThaClaControllerOverride, ChannelEnable);
        mMethodOverride(m_ThaClaControllerOverride, ChannelIsEnabled);
        mMethodOverride(m_ThaClaControllerOverride, Restore);

        /* Registers */
        mMethodOverride(m_ThaClaControllerOverride, ClaGlbPsnCtrl);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideAtObject(self);
    OverrideThaClaController(self);
    }

ThaClaPwController ThaClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaControllerObjectInit((ThaClaController)self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController ThaClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaClaPwControllerObjectInit(newController, cla);
    }

ThaHbce ThaClaPwControllerHbceForPart(ThaClaPwController self, uint8 partId)
    {
    if (self == NULL)
        return NULL;

    if (self->hbces)
        return (ThaHbce)AtListObjectGet(self->hbces, partId);

    if (CreateAllHbces(self) == cAtOk)
        return (ThaHbce)AtListObjectGet(self->hbces, partId);

    return NULL;
    }

uint8 ThaClaPwControllerPartOfPw(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PartOfPw(self, pw);

    return 0;
    }

ThaHbce ThaClaPwControllerHbceGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
    	return ThaClaPwControllerHbceForPart(self, mMethodsGet(self)->PartOfPw(self, pw));
    return NULL;
    }

eAtRet ThaClaPwControllerPwLabelSet(ThaClaPwController self, AtPw pw, AtPwPsn psn)
    {
    if (self)
        return mMethodsGet(self)->PwLabelSet(self, pw, psn);
    return cAtError;
    }

eAtRet ThaClaPwControllerRtpSequenceMistmatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    if (self)
        return mMethodsGet(self)->PwRtpSequenceMistmatchCheckModeSet(self, pw, checkMode);
    return cAtError;
    }

eAtRet ThaClaPwControllerRtpSsrcMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    if (self)
        return mMethodsGet(self)->PwRtpSsrcMismatchCheckModeSet(self, pw, checkMode);
    return cAtError;
    }

eThaPwErrorCheckingMode ThaClaPwControllerRtpSsrcMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRtpSsrcMismatchCheckModeGet(self, pw);
    return cThaPwErrorCheckingModeNoCheck;
    }

eThaPwErrorCheckingMode ThaClaPwControllerRtpPldTypeMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRtpPldTypeMismatchCheckModeGet(self, pw);
    return cThaPwErrorCheckingModeNoCheck;
    }

eAtRet ThaClaPwControllerRtpPldTypeMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    if (self)
        return mMethodsGet(self)->PwRtpPldTypeMismatchCheckModeSet(self, pw, checkMode);
    return cAtError;
    }

eAtRet ThaClaPwControllerMalformCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    if (self)
        return mMethodsGet(self)->PwMalformCheckModeSet(self, pw, checkMode);
    return cAtError;
    }

eAtRet ThaClaPwControllerRealLenCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    if (self)
        return mMethodsGet(self)->PwRealLenCheckModeSet(self, pw, checkMode);
    return cAtError;
    }

eAtRet ThaClaPwControllerLenFieldCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    if (self)
        return mMethodsGet(self)->PwLenFieldCheckModeSet(self, pw, checkMode);
    return cAtError;
    }

eAtRet ThaClaPwControllerReorderEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwReorderEnable(self, pw, enable);
    return cAtError;
    }

eBool ThaClaPwControllerReorderIsEnable(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwReorderIsEnable(self, pw);
    return cAtFalse;
    }

eBool ThaClaPwControllerLookupModeIsSupported(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    if (self)
        return mMethodsGet(self)->PwLookupModeIsSupported(self, pw, lookupMode);
    return cAtFalse;
    }

eAtRet ThaClaPwControllerLookupModeSet(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    if (self)
        return mMethodsGet(self)->PwLookupModeSet(self, pw, lookupMode);
    return cAtError;
    }

eThaPwLookupMode ThaClaPwControllerLookupModeGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwLookupModeGet(self, pw);
    return cThaPwLookupModeInvalid;
    }

eAtRet ThaClaPwControllerExpectedCVlanSet(ThaClaPwController self, AtPw pwAdapter, tAtEthVlanTag *cVlan)
    {
    if (self)
        return mMethodsGet(self)->PwExpectedCVlanSet(self, pwAdapter, cVlan);
    return cAtError;
    }

eAtRet ThaClaPwControllerExpectedSVlanSet(ThaClaPwController self, AtPw pwAdapter, tAtEthVlanTag *sVlan)
    {
    if (self)
        return mMethodsGet(self)->PwExpectedSVlanSet(self, pwAdapter, sVlan);
    return cAtError;
    }

uint32 ThaClaPwControllerPartOffset(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return ThaModuleClaPartOffset(ThaClaControllerModuleGet((ThaClaController)self), mMethodsGet(self)->PartOfPw(self, pw));
    return cInvalidValue;
    }

eAtRet ThaClaPwControllerLookupRemove(ThaClaPwController self, AtPw pwAdapter, eBool applyHardware)
    {
    if (self)
        return mMethodsGet(self)->PwLookupRemove(self, pwAdapter, applyHardware);
    return cAtOk;
    }

eAtRet ThaClaPwControllerPwLookupEnable(ThaClaPwController self, AtPw pwAdapter, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwLookupEnable(self, pwAdapter, enable);
    return cAtError;
    }

eThaPwLookupMode ThaClaPwControllerPartPwLookupModeGet(ThaClaPwController self, uint8 partId)
    {
    if (self)
        return mMethodsGet(self)->PartPwLookupModeGet(self, partId);
    return cThaPwLookupModeInvalid;
    }

eAtRet ThaClaPwControllerCdrEnable(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->CdrEnable(self, pw, cdrId, enable);
    return cAtError;
    }

uint32 ThaClaPwControllerRxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxMalformedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxMalformedPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxStrayPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxStrayPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxDuplicatedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxDuplicatedPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxOamPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxOamPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxLbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxLbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxRbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxRbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxMbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxMbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxNbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxNbitPacketsGet(self, pw, clear);
    return 0;
    }

uint32 ThaClaPwControllerRxPbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxPbitPacketsGet(self, pw, clear);
    return 0;
    }

eBool ThaClaPwControllerPwTypeIsSupported(ThaClaPwController self, eAtPwType pwType)
    {
    if (self)
        return mMethodsGet(self)->PwTypeIsSupported(self, pwType);
    return cAtFalse;
    }

eAtRet ThaClaPwControllerPwTypeSet(ThaClaPwController self, AtPw pw, eAtPwType pwType)
    {
    if (self)
        return mMethodsGet(self)->PwTypeSet(self, pw, pwType);
    return cAtError;
    }

eAtRet ThaClaPwControllerPayloadSizeSet(ThaClaPwController self, AtPw pw, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->PayloadSizeSet(self, pw, payloadSize);
    return cAtError;
    }

eAtRet ThaClaPwControllerPwRtpEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwRtpEnable(self, pw, enable);
    return cAtError;
    }

eBool ThaClaPwControllerPwRtpIsEnabled(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRtpIsEnabled(self, pw);
    return cAtFalse;
    }

eAtRet ThaClaPwControllerPwRtpPayloadTypeSet(ThaClaPwController self, AtPw pw, uint8 payloadType)
    {
    if (self)
        return mMethodsGet(self)->PwRtpPayloadTypeSet(self, pw, payloadType);
    return cAtError;
    }

uint8 ThaClaPwControllerPwRtpPayloadTypeGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRtpPayloadTypeGet(self, pw);
    return 0xFF;
    }

eAtRet ThaClaPwControllerPwRtpSsrcSet(ThaClaPwController self, AtPw pw, uint32 ssrc)
    {
    if (self)
        return mMethodsGet(self)->PwRtpSsrcSet(self, pw, ssrc);
    return cAtError;
    }

uint32 ThaClaPwControllerPwRtpSsrcGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwRtpSsrcGet(self, pw);
    return cInvalidValue;
    }

eAtRet ThaClaPwControllerPwCwLengthModeSet(ThaClaPwController self, AtPw pw, eAtPwCwLengthMode lengthMode)
    {
    if (self)
        return mMethodsGet(self)->PwCwLengthModeSet(self, pw, lengthMode);
    return cAtError;
    }

eBool ThaClaPwControllerPwCwIsEnabled(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwCwIsEnabled(self, pw);
    return cAtFalse;
    }

eAtRet ThaClaPwControllerPwCwEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwCwEnable(self, pw, enable);
    return cAtError;
    }

uint32 ThaClaPwControllerPwAlarmGet(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwAlarmGet(self, pw);
    return 0;
    }

uint32 ThaClaPwControllerPwDefectHistoryGet(ThaClaPwController self, AtPw pw, eBool read2Clear)
    {
    if (self)
        return mMethodsGet(self)->PwDefectHistoryGet(self, pw, read2Clear);
    return 0;
    }

uint32 ThaClaPwControllerCLAHBCEGlbCtrl(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->CLAHBCEGlbCtrl(self);
    return cInvalidValue;
    }

uint32 ThaClaPwControllerCLAHBCEHashingTabCtrl(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->CLAHBCEHashingTabCtrl(self);
    return cInvalidValue;
    }

uint32 ThaClaPwControllerClaHbceLookingUpInformationCtrl(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceLookingUpInformationCtrl(self);
    return cInvalidValue;
    }

eBool ThaClaPwControllerExpectedVlanTagsConflict(ThaClaPwController self,
                                                 tAtEthVlanTag *expectedCVlan1, tAtEthVlanTag *expectedSVlan1,
                                                 tAtEthVlanTag *expectedCVlan2, tAtEthVlanTag *expectedSVlan2)
    {
    if (self)
        return mMethodsGet(self)->PwExpectedVlanTagsConflict(self,
                                                             expectedCVlan1, expectedSVlan1,
                                                             expectedCVlan2, expectedSVlan2);
    return cAtFalse;
    }

uint8 ThaClaPwControllerHbceMaxFlowsPerEntry(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->HbceMaxFlowsPerEntry(self);
    return 0;
    }

eAtRet ThaClaPwControllerPwCepEbmEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
	if (self)
		return mMethodsGet(self)->PwCepEbmEnable(self, pw, enable);
	return cAtErrorNullPointer;
    }

eAtRet ThaClaPwControllerSendLbitPacketToCdrEnable(ThaClaPwController self, eBool enable)
    {
	if (self)
		return mMethodsGet(self)->SendLbitPacketToCdrEnable(self, enable);
	return cAtErrorNullPointer;
    }

uint32 ThaClaPwControllerClaHbceFlowIdMask(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceFlowIdMask(self);
    return 0;
    }

uint8 ThaClaPwControllerClaHbceFlowIdShift(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceFlowIdShift(self);
    return 0;
    }

eBool ThaClaPwControllerSendLbitPacketToCdrIsEnabled(ThaClaPwController self)
    {
    return mMethodsGet(self)->SendLbitPacketToCdrIsEnabled(self);
    }

eAtRet ThaClaPwControllerCdrCircuitSliceSet(ThaClaPwController self, AtPw pw, uint8 slice)
    {
    if (self)
        return mMethodsGet(self)->CdrCircuitSliceSet(self, pw, slice);
    return cAtErrorNullPointer;
    }

uint32 ThaClaPwControllerClaHbceCodingSelectedModeMask(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceCodingSelectedModeMask(self);
    return 0;
    }

uint8 ThaClaPwControllerClaHbceCodingSelectedModeShift(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceCodingSelectedModeShift(self);
    return 0;
    }

uint32 ThaClaPwControllerClaHbceHashingTabSelectedPageMask(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceHashingTabSelectedPageMask(self);
    return 0;
    }

uint8 ThaClaPwControllerClaHbceHashingTabSelectedPageShift(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceHashingTabSelectedPageShift(self);
    return 0;
    }

uint32 ThaClaPwControllerClaHbceTimeoutValMask(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceTimeoutValMask(self);
    return 0;
    }

uint8 ThaClaPwControllerClaHbceTimeoutValShift(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->ClaHbceTimeoutValShift(self);
    return 0;
    }

uint32 ThaClaPwControllerCLAHbceFlowNumMask(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->CLAHbceFlowNumMask(self);
    return 0;
    }

uint8 ThaClaPwControllerCLAHbceFlowNumShift(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->CLAHbceFlowNumShift(self);
    return 0;
    }

uint32 ThaClaPwControllerCLAHbceMemoryStartPtrMask(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->CLAHbceMemoryStartPtrMask(self);
    return 0;
    }

uint8 ThaClaPwControllerCLAHbceMemoryStartPtrShift(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->CLAHbceMemoryStartPtrShift(self);
    return 0;
    }

eBool ThaClaPwControllerCdrIsEnabled(ThaClaPwController self, AtPw pw)
    {
    if ((self == NULL) || (pw == NULL))
        return cAtFalse;

    return mMethodsGet(self)->CdrIsEnabled(self, pw);
    }

void ThaClaPwControllerPwRegsShow(ThaClaPwController self, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->PwRegsShow(self, pw);
    }

eAtRet ThaClaPwControllerPwSuppressEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PwSuppressEnable(self, pw, enable);
    return cAtErrorNullPointer;
    }

eBool ThaClaPwControllerPwSuppressIsEnabled(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwSuppressIsEnabled(self, pw);
    return cAtFalse;
    }

uint32 ThaClaPwControllerRxDiscardedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->RxDiscardedPacketsGet(self, pw, clear);
    return 0;
    }

void ThaClaPwControllerPktAnalyzerCla2CdrDataFlush(ThaClaPwController self, AtPw pw)
    {
    if (self)
        mMethodsGet(self)->PktAnalyzerCla2CdrDataFlush(self, pw);
    }

eAtRet ThaClaPwControllerPktAnalyzerCla2CdrTrigger(ThaClaPwController self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PktAnalyzerCla2CdrTrigger(self, pw);
    return cAtFalse;
    }

eBool ThaClaPwControllerPktAnalyzerCla2CdrHoIsEnabled(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    if (self)
        return mMethodsGet(self)->PktAnalyzerCla2CdrHoIsEnabled(self, pw, packetId);
    return cAtFalse;
    }

eBool ThaClaPwControllerPktAnalyzerCla2CdrPacketIsError(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    if (self)
        return mMethodsGet(self)->PktAnalyzerCla2CdrPacketIsError(self, pw, packetId);
    return cAtFalse;
    }

uint32 ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    if (self)
        return mMethodsGet(self)->PktAnalyzerCla2CdrRtpTimeStampGet(self, pw, packetId);
    return 0;
    }

uint32 ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampOffset(ThaClaPwController self, AtPw pw, uint16 previousPktId, uint16 nextPktId)
    {
    if (self)
        return mMethodsGet(self)->PktAnalyzerCla2CdrRtpTimeStampOffset(self, pw, previousPktId, nextPktId);
    return 0;
    }

uint32 ThaClaPwControllerPktAnalyzerCla2CdrRtpSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    if (self)
        return mMethodsGet(self)->PktAnalyzerCla2CdrRtpSequenceNumberGet(self, pw, packetId);
    return 0;
    }

uint32 ThaClaPwControllerPktAnalyzerCla2CdrControlWordSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId)
    {
    if (self)
        return mMethodsGet(self)->PktAnalyzerCla2CdrControlWordSequenceNumberGet(self, pw, packetId);
    return 0;
    }

eAtRet ThaClaPwControllerHdlcPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    if (self)
        return mMethodsGet(self)->HdlcPwPayloadTypeSet(self, adapter, payloadType);
    return cAtErrorNullPointer;
    }

eAtRet ThaClaPwControllerSuperInit(ThaClaController self)
    {
    return m_ThaClaControllerMethods->Init(self);;
    }

eAtRet ThaClaPwControllerSuperAsyncInit(ThaClaController self)
    {
    return m_ThaClaControllerMethods->AsyncInit(self);;
    }

eAtRet ThaClaPwControllerAllCdrDisable(ThaClaPwController self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;

    for (part_i = 0; part_i < ThaModuleClaNumParts(mClaModule(self)); part_i++)
        {
        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;

        ret |= PartAllCdrDisable(self, part_i);
        }

    return ret;
    }

eAtRet ThaClaPwControllerMlpppPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType)
    {
    if (self)
        return mMethodsGet(self)->MlpppPwPayloadTypeSet(self, adapter, payloadType);
    return cAtErrorNullPointer;
    }

eAtPwHdlcPayloadType ThaClaPwControllerDefaultPayloadType(ThaClaPwController self)
    {
    if (self)
        return mMethodsGet(self)->DefaultPayloadType(self);
    return cAtPwHdlcPayloadTypeInvalid;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaPwControllerV2.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA controller for PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPsn.h"
#include "AtPwCESoP.h"
#include "AtPwCep.h"
#include "ThaClaPwControllerInternal.h"
#include "../pw/ThaModuleClaPwV2Reg.h"
#include "../../pw/ThaModulePwV2Reg.h"
#include "../../pw/ThaModulePw.h"
#include "../../pw/adapters/ThaPwAdapter.h"
#include "../../eth/ThaModuleEth.h"
#include "../hbce/ThaHbce.h"
#include "../pw/ThaModuleClaPwInternal.h"
#include "ThaClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaClaPwControllerV2)self
#define mRo(clear) ((clear) ? 0 : 1)
#define mPwHwId(pw_) AtChannelHwIdGet((AtChannel)pw_)
#define mDevice(self) AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self))
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaControllerMethods   m_ThaClaControllerOverride;
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineRegAdress(ThaClaPwController,   CLAHBCEGlbCtrl)
mDefineRegAdress(ThaClaPwController,   CLAHBCEHashingTabCtrl)
mDefineRegAdress(ThaClaPwController,   ClaHbceLookingUpInformationCtrl)

mDefineMaskShift(ThaClaPwController, ClaHbceFlowId)
mDefineMaskShift(ThaClaPwController, ClaHbceFlowEnb)
mDefineMaskShift(ThaClaPwController, ClaHbceStoreId)

static ThaModulePw PwModule(ThaClaPwController self)
    {
    return (ThaModulePw)AtDeviceModuleGet(mDevice(self), cAtModulePw);
    }

static uint32 PwCtrlOffset(ThaClaPwController self, AtPw pw)
    {
    return (mPwHwId(pw) / 8) + ThaModuleClaPartOffset(mClaModule(self), ThaModulePwPartOfPw(PwModule(self), pw));
    }

static eAtRet ChannelEnable(ThaClaController self, AtChannel channel, eBool enable)
    {
    uint32 regAddr, regVal;
    AtPw pw = (AtPw)channel;
    uint32 pwId = AtChannelIdGet((AtChannel)pw);

    /* Try disable PW by HBCE first */
    if (ThaClaPwControllerHbceGet((ThaClaPwController)self, pw) && (ThaPwAdapterHbceEnable(pw, enable) == cAtOk))
        return cAtOk;

    regAddr = cThaRegClaPwEnCtrl + PwCtrlOffset((ThaClaPwController)self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModuleCla);
    mFieldIns(&regVal, cThaRegClaPwEnCtrlMask(pwId), cThaRegClaPwEnCtrlShift(pwId), enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool ChannelIsEnabled(ThaClaController self, AtChannel channel)
    {
    uint32 regAddr, regVal;
    AtPw pw = (AtPw)channel;
    ThaHbce hbce = ThaClaPwControllerHbceGet((ThaClaPwController)self, pw);
    uint32 pwId = AtChannelIdGet((AtChannel)pw);

    /* If HBCE exist, ask it for this information */
    if (hbce)
        return ThaHbcePwIsEnabled(hbce, (ThaPwAdapter)pw);

    regAddr = cThaRegClaPwEnCtrl + PwCtrlOffset((ThaClaPwController)self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModuleCla);
    return (regVal & cThaRegClaPwEnCtrlMask(pwId)) ? cAtTrue : cAtFalse;
    }

static eAtRet PwRtpEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 rtpEnable = enable ? 1 : 0;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cThaRegClaRxEthRtpEn, rtpEnable);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eBool PwRtpIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 rtpEnable;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    rtpEnable = (uint8)(mRegField(longRegVal[0], cThaRegClaRxEthRtpEn));

    return ((rtpEnable > 0) ? cAtTrue : cAtFalse);
    }

static eAtRet PwRtpPayloadTypeSet(ThaClaPwController self, AtPw pw, uint8 payloadType)
    {
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cThaRegClaRxEthRtpPtValue, payloadType);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint8 PwRtpPayloadTypeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 payloadType = 0;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    payloadType = (uint8)mRegField(longRegVal[0], cThaRegClaRxEthRtpPtValue);

    return payloadType;
    }

static eAtRet PwRtpSsrcSet(ThaClaPwController self, AtPw pw, uint32 ssrc)
    {
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[1], cThaRegClaRxEthRtpSsrcValueHead, (ssrc & cBit31_20) >> 20);
    mRegFieldSet(longRegVal[0], cThaRegClaRxEthRtpSsrcValueTail, ssrc & cBit19_0);
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 PwRtpSsrcGet(ThaClaPwController self, AtPw pw)
    {
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 msb, lsb;
    uint32 ssrc = 0;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    msb = mRegField(longRegVal[1], cThaRegClaRxEthRtpSsrcValueHead);
    lsb = mRegField(longRegVal[0], cThaRegClaRxEthRtpSsrcValueTail);
    mRegFieldSet(ssrc, cThaRegClaRxEthRtpSsrcValueSwHead, msb);
    mRegFieldSet(ssrc, cThaRegClaRxEthRtpSsrcValueSwTail, lsb);

    return ssrc;
    }

static eAtRet PwCwEnable(ThaClaPwController self_, AtPw pw, eBool enable)
    {
	AtUnused(pw);
	AtUnused(self_);
    if (enable)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eBool PwCwIsEnabled(ThaClaPwController self_, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self_);
    return cAtTrue;
    }

static uint32 RxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxMalformedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxMalformPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxStrayPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxStrayPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxOamPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
	AtUnused(clear);
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static uint32 RxLbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxLbitPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxRbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxRbitPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxMbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    if (ThaModulePwMbitCounterIsSupported(PwModule(self)))
        return mChannelHwRead(pw,  cThaRegPmcPwRxMbitCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);

    return 0;
    }

static uint32 RxNbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxNbitPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxPbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cThaRegPmcPwRxPbitPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static eAtRet PwCwLengthModeSet(ThaClaPwController self, AtPw pw, eAtPwCwLengthMode lengthMode)
    {
	AtUnused(lengthMode);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PartPwCdrDisable(ThaClaPwController self, uint32 localPwId, uint8 partId, AtIpCore core)
    {
    uint32 offset = ThaModuleClaPartOffset(mClaModule(self), partId);
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);
    uint32 address = ThaModuleClaPwV2CDRPwLookupCtrlReg(claModule) + localPwId + offset;
    AtHal hal = AtIpCoreHalGet(core);
    uint32 regVal = mModuleHwReadByHal(claModule, address, hal);
    uint32 mask   = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisMask(claModule);
    uint32 shift  = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisShift(claModule);
    mFieldIns(&regVal, mask, shift, 1); /* 1 is disabled */
    mModuleHwWriteByHal(claModule, address, regVal, hal);

    return cAtOk;
    }

static eAtRet CdrEnable(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable)
    {
    uint32 address, regVal;
    uint32 mask, shift;
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);

    if (ThaPwAdapterIsLogicPw((ThaPwAdapter)pw))
        return cAtOk;

    address = ThaModuleClaPwV2CDRPwLookupCtrlReg(claModule) + mClaPwOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModuleCla);

    mask  = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisMask(claModule);
    shift = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisShift(claModule);
    mFieldIns(&regVal, mask, shift, enable ? 0 : 1);

    mask  = ThaModuleClaPwV2CDRPwLookupCtrlLineIdMask(claModule);
    shift = ThaModuleClaPwV2CDRPwLookupCtrlLineIdShift(claModule);
    mFieldIns(&regVal, mask, shift, cdrId);

    mChannelHwWrite(pw, address, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool CdrIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 address, regVal;
    uint32 disableMask;
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);

    if (ThaPwAdapterIsLogicPw((ThaPwAdapter)pw))
        return cAtFalse;

    address = ThaModuleClaPwV2CDRPwLookupCtrlReg(claModule) + mClaPwOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModuleCla);

    disableMask  = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisMask(claModule);
    return (regVal & disableMask) ? cAtFalse : cAtTrue;
    }

static uint16 CdrIdGet(ThaClaPwController self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static eBool PwTypeIsSupported(ThaClaPwController self, eAtPwType pwType)
    {
	AtUnused(self);
    if ((pwType == cAtPwTypeCESoP) ||
        (pwType == cAtPwTypeSAToP) ||
        (pwType == cAtPwTypeCEP)   ||
        (pwType == cAtPwTypeToh))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 RxEthPwType(ThaClaPwControllerV2 self, AtPw pwAdapter, eAtPwType pwType)
    {
    AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
	AtUnused(self);

    if ((pwType == cAtPwTypeCESoP) && (AtPwCESoPModeGet((AtPwCESoP)pw) == cAtPwCESoPModeWithCas))
        return cThaRegClaRxEthPwTypeCESwithCAS;

    if (pwType == cAtPwTypeCEP)
        return cThaRegClaRxEthPwTypeCEP;

    return 0;
    }

static uint8 HwCepMode(ThaClaPwController self, AtPw pwAdapter)
    {
    AtSdhChannel boundVc;
    AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
	AtUnused(self);

    if (AtPwCepModeGet((AtPwCep)pw) == cAtPwCepModeBasic)
        return cThaRegClaRxEthCepModeBasic;

    boundVc = (AtSdhChannel)AtPwBoundCircuitGet(pw);
    if (AtSdhChannelTypeGet(boundVc) == cAtSdhChannelTypeVc4)
        return cThaRegClaRxEthCepModeVc4Fractional;

    return cThaRegClaRxEthCepModeVc3Fractional;
    }

static eAtRet PwTypeSet(ThaClaPwController self, AtPw pw, eAtPwType pwType)
    {
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleClaPwV2 claModule;

    if (!ThaClaPwControllerPwTypeIsSupported(self, pwType))
        return cAtErrorModeNotSupport;

    claModule = (ThaModuleClaPwV2)mClaModule(self);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(longRegVal[0], cThaRegClaRxEthPwType, RxEthPwType((ThaClaPwControllerV2)self, pw, pwType));

    if (pwType == cAtPwTypeCEP)
        mPolyRegFieldSet(longRegVal[cThaRegClaRxEthCepModeDwordIndex], claModule, ClaPwTypeCepMode, HwCepMode(self, pw));

    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return cAtOk;
    }

static uint32 ClaGlbPsnCtrl(ThaClaController self)
    {
	AtUnused(self);
    return 0x440000;
    }

static eAtRet PayloadSizeSet(ThaClaPwController self, AtPw pwAdapter, uint16 payloadSize)
    {
    AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);

    if ((AtPwTypeGet(pw) == cAtPwTypeCEP) || (AtPwTypeGet(pw) == cAtPwTypeToh))
        {
        uint32 longRegVal[cThaLongRegMaxSize];
        uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress((ThaClaPwControllerV2)self, pwAdapter) + mClaPwOffset(self, pwAdapter);
        ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);
        mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
        mPolyRegFieldSet(longRegVal[cThaRegClaRxEthPwLenDwordIndex], claModule, ClaPwTypePwLen, payloadSize);
        mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
        }

    return cAtOk;
    }

static uint16 PayloadSizeGet(ThaClaPwController self, AtPw pwAdapter)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;
    AtPw pw;
    ThaModuleClaPwV2 claModule;

    pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return 0;

    claModule = (ThaModuleClaPwV2)mClaModule(self);
    regAddr = ThaClaPwControllerV2PwTypeCtrlAddress((ThaClaPwControllerV2)self, pwAdapter) + mClaPwOffset(self, pwAdapter);
    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (uint16)mPolyRegField(longRegVal[cThaRegClaRxEthPwLenDwordIndex], claModule, ClaPwTypePwLen);
    }

static uint32 PwLabelGet(ThaClaPwController self, AtPw pw, AtPwPsn psn)
    {
    eAtPwPsnType psnType = AtPwPsnTypeGet(psn);
    AtPwPsn lowerPsn = AtPwPsnLowerPsnGet(psn);
    AtUnused(self);

    if (psnType == cAtPwPsnTypeMpls)
        {
        tAtPwMplsLabel label;
        if (AtPwMplsPsnInnerLabelGet((AtPwMplsPsn)psn, &label) == cAtOk)
            return label.label;
        }

    if (psnType == cAtPwPsnTypeUdp)
        {
        uint32 port;
        AtPwUdpPsn udp = (AtPwUdpPsn)psn;

        if ((port = AtPwUdpPsnSourcePortGet(udp)) != cAtPwPsnUdpUnusedPort)
            return port;
        if ((port = AtPwUdpPsnDestPortGet(udp)) != cAtPwPsnUdpUnusedPort)
            return port;
        }

    if ((psnType == cAtPwPsnTypeMef) && (AtPwPsnTypeGet(lowerPsn) == cAtPwPsnTypeMpls))
        {
        tAtPwMplsLabel label;
        if (AtPwMplsPsnInnerLabelGet((AtPwMplsPsn)lowerPsn, &label) == cAtOk)
            return label.label;
        }

    if (psnType == cAtPwPsnTypeMef)
        return AtPwMefPsnExpectedEcIdGet((AtPwMefPsn)psn);

    /* For unknown case, temporary use PW ID as label */
    return AtChannelIdGet((AtChannel)pw);
    }

static eAtRet PwLabelSet(ThaClaPwController self, AtPw pw, AtPwPsn psn)
    {
    uint32 regAddr;
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 pwLocalId;
    uint32 label;

    /* If HBCE exist, let it do this */
    if (ThaClaPwControllerHbceGet(self, pw) || (psn == NULL))
        return cAtOk;

    label = PwLabelGet(self, pw, psn);
    regAddr = cThaRegClaPwIdenCtrl + PwCtrlOffset(self, pw);
    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    pwLocalId = AtChannelIdGet((AtChannel)pw) % 8;
    switch (pwLocalId)
        {
        case 0:
            mRegFieldSet(longRegVal[0], cThaRegClaPw1stIden, label);
            break;
        case 1:
            mRegFieldSet(longRegVal[0], cThaRegClaPw2ndIdenTail, (label & cBit11_0));
            mRegFieldSet(longRegVal[1], cThaRegClaPw2ndIdenHead, (label & cBit19_12) >> 12);
            break;
        case 2:
            mRegFieldSet(longRegVal[1], cThaRegClaPw3thIden, label);
            break;
        case 3:
            mRegFieldSet(longRegVal[2], cThaRegClaPw4thIdenHead, (label & cBit19_4) >> 4);
            mRegFieldSet(longRegVal[1], cThaRegClaPw4thIdenTail, label & cBit3_0);
            break;
        case 4:
            mRegFieldSet(longRegVal[3], cThaRegClaPw5thIdenHead, (label & cBit19_16) >> 16);
            mRegFieldSet(longRegVal[2], cThaRegClaPw5thIdenTail, label & cBit15_0);
            break;
        case 5:
            mRegFieldSet(longRegVal[3], cThaRegClaPw6thIden, label);
            break;
        case 6:
            mRegFieldSet(longRegVal[4], cThaRegClaPw7thIdenHead, (label & cBit19_8) >> 8);
            mRegFieldSet(longRegVal[3], cThaRegClaPw7thIdenTail, label & cBit7_0);
            break;

        /* Must be 7 (this is to pass Coverity check) */
        default:
            mRegFieldSet(longRegVal[4], cThaRegClaPw8thIden, label);
            break;
        }
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet DefaultSet(ThaClaController self)
    {
	AtUnused(self);
    return cAtOk;
    }

static eBool IsErrorCheckingMode(eThaPwErrorCheckingMode checkMode)
    {
    if ((checkMode == cThaPwErrorCheckingModeCheckCountAndDiscard) ||
        (checkMode == cThaPwErrorCheckingModeCheckCountAndNotDiscard))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwRtpSequenceMistmatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
	AtUnused(checkMode);
	AtUnused(pw);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet PwRtpSsrcMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint8  ssrcMismatchCheckEn = 0;

    if (IsErrorCheckingMode(checkMode))
        ssrcMismatchCheckEn = 1;

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[0], cThaRegClaRxEthRtpSsrcChkEn, ssrcMismatchCheckEn);
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eThaPwErrorCheckingMode PwRtpSsrcMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (pdRegVal[0] & cThaRegClaRxEthRtpSsrcChkEnMask) ? cThaPwErrorCheckingModeCheckCountAndDiscard : cThaPwErrorCheckingModeNoCheck;
    }

static eAtRet PwRtpPldTypeMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);
    uint8  pldTypeMismatchCheckEn = 0;

    if (IsErrorCheckingMode(checkMode))
        pldTypeMismatchCheckEn = 1;

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mRegFieldSet(pdRegVal[0], cThaRegClaRxEthRtpPtChkEn, pldTypeMismatchCheckEn);
    mChannelHwLongWrite(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eThaPwErrorCheckingMode PwRtpPldTypeMismatchCheckModeGet(ThaClaPwController self, AtPw pw)
    {
    uint32 pdRegVal[cThaLongRegMaxSize];
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, pdRegVal, cThaLongRegMaxSize, cThaModuleCla);
    return (pdRegVal[0] & cThaRegClaRxEthRtpPtChkEnMask) ? cThaPwErrorCheckingModeCheckCountAndDiscard : cThaPwErrorCheckingModeNoCheck;
    }

static eAtRet PwMalformCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
	AtUnused(pw);
	AtUnused(self);
    if (IsErrorCheckingMode(checkMode))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtRet PwRealLenCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
	AtUnused(checkMode);
	AtUnused(pw);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet PwLenFieldCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode)
    {
	AtUnused(checkMode);
	AtUnused(pw);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 PwAlarmGet(ThaClaPwController self, AtPw pw)
    {
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static uint32 PwDefectHistoryGet(ThaClaPwController self, AtPw pw, eBool read2Clear)
    {
	AtUnused(read2Clear);
	AtUnused(pw);
	AtUnused(self);
    return 0;
    }

static eAtRet PwReorderEnable(ThaClaPwController self, AtPw pw, eBool enable)
    {
	AtUnused(enable);
	AtUnused(pw);
	AtUnused(self);
    return cAtOk;
    }

static uint8 HbceMaxFlowsPerEntry(ThaClaPwController self)
    {
	AtUnused(self);
    return 15;
    }

static eAtRet PwCepEbmEnable(ThaClaPwController self, AtPw pwAdapter, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr;
    AtPw pw;
    ThaModuleClaPwV2 claModule;

    pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
    if (AtPwTypeGet(pw) != cAtPwTypeCEP)
        return cAtOk;

    claModule = (ThaModuleClaPwV2)mClaModule(self);
    regAddr = ThaClaPwControllerV2PwTypeCtrlAddress((ThaClaPwControllerV2)self, pwAdapter) + mClaPwOffset(self, pwAdapter);
    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mPolyRegFieldSet(longRegVal[cThaRegClaRxEthPwEbmCpuModeDwIndex], claModule, ClaPwEbmCpuMode, (enable) ? 0 : 1);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static eAtRet SendLbitPacketToCdrEnable(ThaClaPwController self, eBool enable)
    {
    uint8 partId;

    for (partId = 0; partId < ThaModuleClaNumParts(mClaModule(self)); partId++)
        {
        ThaClaController controller = (ThaClaController)self;
        uint32 regAddr = ThaClaControllerClaGlbPsnCtrl(controller) + ThaModuleClaPartOffset(mClaModule(self), partId);
        uint32 regVal  = mClaControllerRead(self, regAddr);
        mRegFieldSet(regVal, cThaRegClaRxSendLbit2CdrEn, mBoolToBin(enable));
        mClaControllerWrite(self, regAddr, regVal);
        }

    return cAtOk;
    }

static eBool SendLbitPacketToCdrIsEnabled(ThaClaPwController self)
    {
    ThaClaController controller = (ThaClaController)self;
    uint32 regAddr = ThaClaControllerClaGlbPsnCtrl(controller) + ThaModuleClaPartOffset(mClaModule(self), 0);
    uint32 regVal  = mClaControllerRead(self, regAddr);
    return mBinToBool(mRegField(regVal, cThaRegClaRxSendLbit2CdrEn));
    }

static void PwRegsShow(ThaClaPwController self, AtPw pw)
    {
    uint32 offset = mClaPwOffset(self, pw);
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);

    uint32 address = ThaClaPwControllerV2PwTypeCtrlAddress(mThis(self), pw) + offset;
    ThaModuleClaLongRegDisplay(pw, "Classify Pseudowire Type Control", address);

    address = ThaModuleClaPwV2CDRPwLookupCtrlReg(claModule) + offset;
    ThaModuleClaRegDisplay(pw, "CDR Pseudowire Look Up Control", address);
    }

static void OverrideThaClaController(ThaClaPwController self)
    {
    ThaClaController claController = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, mMethodsGet(claController), sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, DefaultSet);
        mMethodOverride(m_ThaClaControllerOverride, ClaGlbPsnCtrl);
        mMethodOverride(m_ThaClaControllerOverride, ChannelEnable);
        mMethodOverride(m_ThaClaControllerOverride, ChannelIsEnabled);
        }

    mMethodsSet(claController, &m_ThaClaControllerOverride);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(self), sizeof(m_ThaClaPwControllerOverride));

        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPayloadTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPayloadTypeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwCwLengthModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwCwIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PwCwEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMalformedPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxStrayPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxOamPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxLbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxRbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxNbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrIdGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PartPwCdrDisable);
        mMethodOverride(m_ThaClaPwControllerOverride, PwTypeIsSupported);
        mMethodOverride(m_ThaClaPwControllerOverride, PwTypeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PayloadSizeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PayloadSizeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, CLAHBCEGlbCtrl);
        mMethodOverride(m_ThaClaPwControllerOverride, CLAHBCEHashingTabCtrl);
        mMethodOverride(m_ThaClaPwControllerOverride, ClaHbceLookingUpInformationCtrl);
        mMethodOverride(m_ThaClaPwControllerOverride, HbceMaxFlowsPerEntry);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLabelSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSequenceMistmatchCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcMismatchCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpSsrcMismatchCheckModeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPldTypeMismatchCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRtpPldTypeMismatchCheckModeGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwMalformCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRealLenCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLenFieldCheckModeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwAlarmGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwDefectHistoryGet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwReorderEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, PwCepEbmEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, SendLbitPacketToCdrEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, SendLbitPacketToCdrIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRegsShow);

        /* Bit field override */
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceFlowId)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceFlowEnb)
        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceStoreId)
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaController(self);
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaPwControllerV2);
    }

ThaClaPwController ThaClaPwControllerV2ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController ThaClaPwControllerV2New(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaClaPwControllerV2ObjectInit(newController, cla);
    }

uint32 ThaClaPwControllerV2PwTypeCtrlAddress(ThaClaPwControllerV2 self, AtPw pw)
    {
    AtUnused(pw);
    return ThaModuleClaPwV2ClaPwTypeCtrlReg((ThaModuleClaPwV2)mClaModule(self));
    }

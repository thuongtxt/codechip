/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaEthFlowController.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA ETH flow controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLAETHFLOWCONTROLLER_H_
#define _THACLAETHFLOWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaClaControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Default concretes */
ThaClaEthFlowController ThaClaEthFlowControllerNew(ThaModuleCla cla);

uint8 ThaClaEthFlowControllerVlanLookupModeGet(ThaClaEthFlowController self, ThaEthFlow flow);
eAtRet ThaClaEthFlowControllerVlanLookupCtrl(ThaClaEthFlowController self, ThaEthFlow flow, uint16 lkType, uint32 channelId, uint32 vlanId, eBool  enable);
eAtRet ThaClaEthFlowControllerFlowLookupCtrl(ThaClaEthFlowController self, ThaEthFlow flow, uint32 hwFlowId, uint32 classId, uint32 queueId, uint32 vlanOffSet, eBool enable);
eAtRet ThaClaEthFlowControllerAllTrafficsDeactivate(ThaClaEthFlowController self);
uint32 ThaClaEthFlowControllerMaxNumVlans(ThaClaEthFlowController self);
eAtRet ThaClaEthFlowControllerPsnToTdmEthTypeSet(ThaClaEthFlowController self, uint32 entryIndex, uint32 ethType);
uint32 ThaClaEthFlowControllerPsnToTdmEthTypeGet(ThaClaEthFlowController self, uint32 entryIndex);

/* Product concretes */
ThaClaEthFlowController Tha60030022ClaEthFlowControllerNew(ThaModuleCla cla);
ThaClaEthFlowController Tha60030051ClaEthFlowControllerNew(ThaModuleCla cla);
ThaClaEthFlowController Tha60031032ClaEthFlowControllerNew(ThaModuleCla cla);
ThaClaEthFlowController Tha60031033ClaEthFlowControllerNew(ThaModuleCla cla);
ThaClaEthFlowController Tha60091023ClaEthFlowControllerNew(ThaModuleCla cla);
ThaClaEthFlowController Tha60091132ClaEthFlowControllerNew(ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THACLAETHFLOWCONTROLLER_H_ */


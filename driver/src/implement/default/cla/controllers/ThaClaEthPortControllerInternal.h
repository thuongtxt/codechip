/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaEthPortControllerInternal.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA Ethernet port controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLAETHPORTCONTROLLERINTERNAL_H_
#define _THACLAETHPORTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "ThaClaEthPortController.h"
#include "ThaClaControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eThaClaEthPortCounterType
    {
    cThaClaEthPortCounterTypeIncomPkt,
    cThaClaEthPortCounterTypeIncombyte,
    cThaClaEthPortCounterTypeEthPktBusErr,
    cThaClaEthPortCounterTypeEthPktFcsErr,
    cThaClaEthPortCounterTypeEthPktoversize,
    cThaClaEthPortCounterTypeEthPktundersize,
    cThaClaEthPortCounterTypeEthPktLensmallerthan64bytes,
    cThaClaEthPortCounterTypeEthPktLenfrom65to127bytes,
    cThaClaEthPortCounterTypeEthPktLenfrom128to255bytes,
    cThaClaEthPortCounterTypeEthPktLenfrom256to511bytes,
    cThaClaEthPortCounterTypeEthPktLenfrom512to1024bytes,
    cThaClaEthPortCounterTypeEthPktLenfrom1025to1528bytes,
    cThaClaEthPortCounterTypeEthPktJumboLen,
    cThaClaEthPortCounterTypePWunSuppedRxPkt,
    cThaClaEthPortCounterTypeHCBElookupnotmatchPkt,
    cThaClaEthPortCounterTypeEthPktDscd,
    cThaClaEthPortCounterTypePauFrmRxPkt,
    cThaClaEthPortCounterTypePauFrmErrPkt,
    cThaClaEthPortCounterTypeBcastRxPkt,
    cThaClaEthPortCounterTypeMcastRxPkt,
    cThaClaEthPortCounterTypeARPRxPkt,
    cThaClaEthPortCounterTypeEthOamRxPkt,
    cThaClaEthPortCounterTypeEthOamRxErrPkt,
    cThaClaEthPortCounterTypeEthOamtype0RxPkt,
    cThaClaEthPortCounterTypeEthOamtype1RxPkt,
    cThaClaEthPortCounterTypeIPv4RxPkt,
    cThaClaEthPortCounterTypeIPv4PktErr,
    cThaClaEthPortCounterTypeICMPv4RxPkt,
    cThaClaEthPortCounterTypeIPv6RxPkt,
    cThaClaEthPortCounterTypeIPv6PktErr,
    cThaClaEthPortCounterTypeICMPv6RxPkt,
    cThaClaEthPortCounterTypeMEFRxPkt,
    cThaClaEthPortCounterTypeMEFErrRxPkt,
    cThaClaEthPortCounterTypeMPLSRxPkt,
    cThaClaEthPortCounterTypeMPLSErrRxPkt,
    cThaClaEthPortCounterTypeMPLSwithoutPHPMdOuterlookupnotmatch,
    cThaClaEthPortCounterTypeMPLSDataRxPktwithPHPMd,
    cThaClaEthPortCounterTypeMPLSDataRxPktwithoutPHPMd,
    cThaClaEthPortCounterTypeLDPIPv4withPHPMdRxPkt,
    cThaClaEthPortCounterTypeLDPIPv4overMLPSwithoutPHPMdRxPkt,
    cThaClaEthPortCounterTypeLDPIPv6withPHPMdRxPkt,
    cThaClaEthPortCounterTypeLDPIPv6overMLPSwithoutPHPMdRxPkt,
    cThaClaEthPortCounterTypeMPLSoverIPv4RxPkt,
    cThaClaEthPortCounterTypeMPLSoverIPv6RxPkt,
    cThaClaEthPortCounterTypeL2TPPktErr,
    cThaClaEthPortCounterTypeL2TPIPv4RxPkt,
    cThaClaEthPortCounterTypeL2TPIPv6RxPkt,
    cThaClaEthPortCounterTypeUDPPktErr,
    cThaClaEthPortCounterTypeUDPIPv4RxPkt,
    cThaClaEthPortCounterTypeUDPIPv6RxPkt,
    cThaClaEthPortCounterTypeEthPktForwardtoCpu,
    cThaClaEthPortCounterTypeEthPktForwardtoPDa,
    cThaClaEthPortCounterTypeEthPktTimePktToP,
    cThaClaEthPortCounterTypeRxHdrErrPackets,
    cThaClaEthPortCounterTypeRxErrPsnPackets
    }eThaClaEthPortCounterType;

typedef struct tThaClaEthPortControllerMethods
    {
    uint32 (*EthPortPartOffset)(ThaClaEthPortController self, AtEthPort port);
    eAtRet (*EthPortMacCheckEnable)(ThaClaEthPortController self, AtEthPort port, eBool enable);
    eBool (*EthPortMacCheckIsEnabled)(ThaClaEthPortController self, AtEthPort port);

    /* Counters */
    uint32 (*CounterPartOffset)(ThaClaEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType);
    uint32 (*IncomPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*IncombyteRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktBusErrRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktFcsErrRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktoversizeRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktundersizeRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktLensmallerthan64bytesRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktLenfrom65to127bytesRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktLenfrom128to255bytesRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktLenfrom256to511bytesRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktLenfrom512to1024bytesRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktLenfrom1025to1528bytesRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktLenfrom1529to2047bytesRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktJumboLenRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*PWunSuppedRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*HCBElookupnotmatchPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktDscdRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*PauFrmRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*PauFrmErrPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*UnicastRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*BcastRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*McastRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*ARPRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthOamRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthOamRxErrPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthOamtype0RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthOamtype1RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*IPv4RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*IPv4PktErrRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*ICMPv4RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*IPv6RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*IPv6PktErrRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*ICMPv6RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MEFRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MEFErrRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MPLSRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MPLSErrRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MPLSDataRxPktwithPHPMdRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MPLSDataRxPktwithoutPHPMdRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*LDPIPv4withPHPMdRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*LDPIPv6withPHPMdRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MPLSoverIPv4RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*MPLSoverIPv6RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*L2TPPktErrRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*L2TPIPv4RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*L2TPIPv6RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*UDPPktErrRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*UDPIPv4RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*UDPIPv6RxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktForwardtoCpuRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktForwardtoPDaRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*EthPktTimePktToPRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*RxHdrErrPacketsRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*RxErrPsnPacketsRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);
    uint32 (*PhysicalErrorRxPktRead2Clear)(ThaClaEthPortController self, AtEthPort port, eBool r2c);

    /* Max/Min packet size configure */
    eAtRet (*MaxPacketSizeSet)(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize);
    uint32 (*MaxPacketSizeGet)(ThaClaEthPortController self, AtEthPort port);
    eAtRet (*MinPacketSizeSet)(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize);
    uint32 (*MinPacketSizeGet)(ThaClaEthPortController self, AtEthPort port);

    eAtRet (*MacSet)(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr);
    eAtRet (*MacGet)(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr);
	
	/* IP addresses */
    eAtRet (*IpV4AddressSet)(ThaClaEthPortController self, AtEthPort port, uint8 *address);
    eAtRet (*IpV4AddressGet)(ThaClaEthPortController self, AtEthPort port, uint8 *address);
    eAtRet (*IpV6AddressSet)(ThaClaEthPortController self, AtEthPort port, uint8 *address);
    eAtRet (*IpV6AddressGet)(ThaClaEthPortController self, AtEthPort port, uint8 *address);

    uint16 (*SVlanTpid)(ThaClaEthPortController self, AtEthPort port);

    /* To handle max packet size */
    eBool (*NeedMaxPacketSizeDefaultSetup)(ThaClaEthPortController self);
    uint32 (*DefaultMaxPacketSizeForPort)(ThaClaEthPortController self, AtEthPort port);
    }tThaClaEthPortControllerMethods;

typedef struct tThaClaEthPortController
    {
    tThaClaController super;
    const tThaClaEthPortControllerMethods *methods;

    /* Private data */
    }tThaClaEthPortController;

typedef struct tThaClaPppEthPortController
    {
    tThaClaEthPortController super;

    /* Private data */
    }tThaClaPppEthPortController;

typedef struct tThaClaPwEthPortControllerV2
    {
    tThaClaEthPortController super;

    /* Private data */
    }tThaClaPwEthPortControllerV2;

typedef struct tThaStmPwProductClaPwEthPortControllerV2
    {
    tThaClaPwEthPortControllerV2 super;
    }tThaStmPwProductClaPwEthPortControllerV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController ThaClaEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla);
ThaClaEthPortController ThaClaPwEthPortControllerV2ObjectInit(ThaClaEthPortController self, ThaModuleCla cla);
ThaClaEthPortController ThaClaPppEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla);
ThaClaEthPortController ThaStmPwProductClaPwEthPortControllerV2ObjectInit(ThaClaEthPortController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THACLAETHPORTCONTROLLERINTERNAL_H_ */


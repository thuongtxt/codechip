/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaEthFlowController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA ETH Flow controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePpp.h"
#include "../../man/ThaDeviceInternal.h"
#include "../../eth/ThaEthFlow.h"
#include "../ppp/ThaModuleClaPppReg.h"
#include "../ppp/ThaModuleClaPppInternal.h"
#include "ThaClaEthFlowControllerInternal.h"
#include "ThaClaControllerInternal.h"
#include "../../eth/controller/ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaClaEthFlowController)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaClaEthFlowControllerMethods m_methods;

/* Override */
static tThaClaControllerMethods m_ThaClaControllerOverride;

/* Save super implementation */
static const tThaClaControllerMethods *m_ThaClaControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern uint32 ThaModulePppNumBundlePerPart(AtModulePpp self);

/*--------------------------- Implementation ---------------------------------*/
static uint32 PartOffset(ThaClaEthFlowController self, ThaEthFlow flow)
    {
	AtUnused(self);
    return ThaEthFlowPartOffset(flow, cThaModuleCla);
    }

static uint32 MaxVlanIdGet(ThaClaController self)
    {
	AtUnused(self);
    return 4096;
    }

static uint8 VlanPriorityNumGet(ThaClaController self)
    {
	AtUnused(self);
    return 8;
    }

static uint8 PsnMode(ThaClaEthFlowController self)
    {
    ThaModuleClaPpp claModule = (ThaModuleClaPpp)ThaClaControllerModuleGet((ThaClaController)self);
    ThaModuleEth ethModule = (ThaModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)claModule), cAtModuleEth);
    ThaEthFlowHeaderProvider headerProvided = ThaModuleEthFlowHeaderProviderGet(ethModule);
    return ThaEthFlowHeaderProviderPsnMode(headerProvided);
    }

static void ClaOperationCtrlInit(ThaClaEthFlowController self, uint8 partId)
    {
    uint32 regVal;
    ThaModuleCla claModule = ThaClaControllerModuleGet((ThaClaController)self);
    uint32 partAddr = cThaClaOperationCtrl + ThaModuleClaPartOffset(claModule, partId);

    /* Write register CLA Operation Control */
    regVal = mModuleHwRead(claModule, partAddr);
    mFieldIns(&regVal, cThaClaPktModeMask, cThaClaPktModeShift, PsnMode((ThaClaEthFlowController)self));
    mFieldIns(&regVal, cThaClaFcsRmvMask, cThaClaFcsRmvShift,   mMethodsGet(self)->FcsRemoved(self));
    mFieldIns(&regVal, cThaClaVlanRmvMask, cThaClaVlanRmvShift, mMethodsGet(self)->VlanRemoved(self));
    mFieldIns(&regVal, cThaClaVlanLkModeMask, cThaClaVlanLkModeShift, 0x1);
    mModuleHwWrite(claModule, partAddr, regVal);
    }

static void FlowLookupCtrlInit(ThaClaController self, uint8 partId)
    {
    uint32 localBundleId, bundle_i, priority_i;
    ThaModuleCla claModule = ThaClaControllerModuleGet(self);
    uint8 vlanPriorityNum = VlanPriorityNumGet(self);
    AtModulePpp pppModule = (AtModulePpp)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)claModule), cAtModulePpp);

    for (bundle_i = 0; bundle_i < AtModulePppMaxBundlesGet(pppModule); bundle_i++)
        {
        localBundleId = (bundle_i % ThaModulePppNumBundlePerPart(pppModule));
        for (priority_i = 0; priority_i < vlanPriorityNum; priority_i++)
            {
            uint32 partOffset = (vlanPriorityNum * localBundleId) + priority_i + ThaModuleClaPartOffset(claModule, partId);
            mModuleHwWrite(claModule, cThaClaFlowLookupCtrl + partOffset, 0);
            }
        }
    }

static void VlanLookupCtrlInit(ThaClaController self, uint8 partId)
    {
    uint32 i;
    ThaModuleCla claModule = ThaClaControllerModuleGet(self);
    uint32 baseAddress = cThaClaVlanLookupCtrl + ThaModuleClaPartOffset(claModule, partId);

    for (i = 0; i < MaxVlanIdGet(self); i++)
        mModuleHwWrite(claModule, baseAddress + i, 0);
    }

static eAtRet QueuePriorityDefaultSet(ThaClaController self, uint8 partId)
    {
    static const uint8 cMaxNumQueues = 8;
    uint8 queue_i;
    ThaModuleCla claModule = ThaClaControllerModuleGet(self);
    uint32 regAddr = cThaClaPriorityVlanCtrl + ThaModuleClaPartOffset(claModule, partId);
    uint32 regVal  = mModuleHwRead(claModule, regAddr);

    for (queue_i = 0; queue_i < cMaxNumQueues; queue_i++)
        {
        uint8 queuePriority = mMethodsGet(mThis(self))->QueueHwPriority(mThis(self), queue_i);
        uint32 mask  = cThaClaQueueVlanPriMask(queue_i);
        uint32 shift = cThaClaQueueVlanPriShift(queue_i);
        mFieldIns(&regVal, mask, shift, queuePriority);
        }

    mModuleHwWrite(claModule, regAddr, regVal);

    return cAtOk;
    }

static eAtRet PartDefaultSet(ThaClaController self, uint8 partId)
    {
    if (ThaClaControllerPartIsUnused(self, partId))
        return cAtOk;

    ClaOperationCtrlInit((ThaClaEthFlowController)self, partId);
    FlowLookupCtrlInit(self, partId);
    VlanLookupCtrlInit(self, partId);

    if (mMethodsGet(mThis(self))->NeedInitializeQueuePriority(mThis(self)))
        QueuePriorityDefaultSet(self, partId);

    return cAtOk;
    }

static eAtRet PartInit(ThaClaController self, uint8 partId)
    {
    eAtRet ret = cAtOk;

    if (ThaClaControllerPartIsUnused(self, partId))
        return cAtOk;

    ret |= m_ThaClaControllerMethods->PartInit(self, partId);
    ret |= mMethodsGet(self)->PartDefaultSet(self, partId);

    return ret;
    }

static uint8 VlanLookupModeGet(ThaClaEthFlowController self, ThaEthFlow flow)
    {
    uint32 regAddr = cThaClaOperationCtrl + mMethodsGet(self)->PartOffset(self, flow);
    uint32 regVal  = ThaClaControllerRead((ThaClaController)self, regAddr);

    return (regVal & cThaClaVlanLkModeMask) ? 1 : 0;
    }

static eAtRet VlanLookupCtrl(ThaClaEthFlowController self, ThaEthFlow flow, uint16 lkType, uint32 channelId, uint32 vlanId, eBool  enable)
    {
    uint32 currentChannelId;
    uint16 lookupOffset = mMethodsGet(self)->VlanLookupOffset(self, lkType);
    uint32 regAddr = cThaClaVlanLookupCtrl + vlanId + lookupOffset + mMethodsGet(self)->PartOffset(self, flow);
    uint32 regVal  = mChannelHwRead(flow, regAddr, cThaModuleCla);

    if (enable)
        {
        mFieldIns(&regVal, cThaClaVlanLkTypeMask, cThaClaVlanLkTypeShift, lkType);
        mFieldIns(&regVal, cThaClaVlanLkCIDMask, cThaClaVlanLkCIDShift, channelId);
        mFieldIns(&regVal, cThaClaVlanLkEnMaks, cThaClaVlanLkEnShift, 1);
        }

    /* When disable need to check if this VLAN belong to this channel otherwise do nothing */
    else
        {
        mFieldGet(regVal, cThaClaVlanLkCIDMask, cThaClaVlanLkCIDShift, uint32, &currentChannelId);
        if (currentChannelId == channelId)
            mFieldIns(&regVal, cThaClaVlanLkEnMaks, cThaClaVlanLkEnShift, 0);
        }

    mChannelHwWrite(flow, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eAtRet FlowLookupCtrl(ThaClaEthFlowController self, ThaEthFlow flow, uint32 hwFlowId, uint32 classId, uint32 queueId, uint32 vlanOffSet, eBool enable)
    {
    uint32 regVal;
    uint32 regAddr = cThaClaFlowLookupCtrl + vlanOffSet + mMethodsGet(self)->PartOffset(self, flow);

    regVal = mChannelHwRead(flow, regAddr, cThaModuleCla);
    if (enable)
        {
        mFieldIns(&regVal, cThaClaBundLkMpFlowIdMask, cThaClaBundLkMpFlowIdShift, hwFlowId);
        mFieldIns(&regVal, cThaClaBundLkMpFlowClassMaks, cThaClaBundLkMpFlowClassShift, classId);
        mFieldIns(&regVal, cThaClaBundLkMpQueueIdMask, cThaClaBundLkMpQueueIdShift, queueId);
        }

    mFieldIns(&regVal, cThaClaBundLkMpFlowEnMaks, cThaClaBundLkMpFlowEnShift, enable ? 1 : 0);
    mChannelHwWrite(flow, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eAtRet VlanDeactivateOnPart(ThaClaEthFlowController self, uint32 vlanId, uint8 partId)
    {
    ThaModuleCla claModule = ThaClaControllerModuleGet((ThaClaController)self);
    uint32 regAddr = cThaClaVlanLookupCtrl + vlanId + ThaModuleClaPartOffset(claModule, partId);
    uint32 regVal  = mModuleHwRead(claModule, regAddr);

    mFieldIns(&regVal, cThaClaVlanLkEnMaks, cThaClaVlanLkEnShift, 0);
    mModuleHwWrite(claModule, regAddr, regVal);

    return cAtOk;
    }

static eAtRet AllTrafficsDeactivateOnPart(ThaClaEthFlowController self, uint8 partId)
    {
    eAtRet ret = cAtOk;
    uint32 vlan;
    uint32 numVlans;

    if (self == NULL)
        return cAtError;

    /* Ignore unused part */
    if (ThaClaControllerPartIsUnused((ThaClaController)self, partId))
        return cAtOk;

    numVlans = ThaModuleClaPppMaxNumVlans(ThaClaControllerModuleGet((ThaClaController)self));
    for (vlan = 0; vlan < numVlans; vlan++)
        ret |= VlanDeactivateOnPart(self, vlan, partId);

    return ret;
    }

static eAtRet AllTrafficsDeactivate(ThaClaEthFlowController self)
    {
    eAtRet ret = cAtOk;
    ThaModuleCla claModule = ThaClaControllerModuleGet((ThaClaController)self);
    uint8 partId;

    for (partId = 0; partId < ThaModuleClaNumParts(claModule); partId++)
        {
        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, partId))
            continue;

        ret |= AllTrafficsDeactivateOnPart(self, partId);
        }

    return ret;
    }

static uint32 MaxNumVlans(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return 1024;
    }

static uint32 PidEntryEnableMask(ThaClaEthFlowController self, uint32 entryIndex, eBool enable)
    {
	AtUnused(entryIndex);
	AtUnused(self);
    return enable ? 0xFFFF : 0;
    }

static eAtRet PsnToTdmEthTypeSet(ThaClaEthFlowController self, uint32 entryIndex, uint32 ethType)
    {
    ThaModuleCla claModule = ThaClaControllerModuleGet((ThaClaController)self);
    uint8 part_i;

    for (part_i = 0; part_i < ThaModuleClaNumParts(claModule); part_i++)
        {
        uint32 offset  = ThaModuleClaPartOffset(claModule, part_i);
        uint32 regAddr = cThaClaEthType2PppPidCtrl + entryIndex + offset;
        uint32 regVal  = mModuleHwRead(claModule, regAddr);

        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;

        mFieldIns(&regVal, cThaClaEthTypeValueMask, cThaClaEthTypeValueShift, ethType);
        mModuleHwWrite(claModule, regAddr, regVal);
        }

    return cAtOk;
    }

static uint32 PsnToTdmEthTypeGet(ThaClaEthFlowController self, uint32 entryIndex)
    {
    uint8 part_i;
    uint32 hwEthType = 0;
    ThaModuleCla claModule = ThaClaControllerModuleGet((ThaClaController)self);

    for (part_i = 0; part_i < ThaModuleClaNumParts(claModule); part_i++)
        {
        uint32 regVal = mModuleHwRead(claModule, cThaClaEthType2PppPidCtrl + entryIndex);

        if (mMethodsGet((ThaClaController)self)->PartIsUnused((ThaClaController)self, part_i))
            continue;
        mFieldGet(regVal, cThaClaEthTypeValueMask, cThaClaEthTypeValueShift, uint32, &hwEthType);
        }

    return hwEthType;
    }

static uint8 FcsRemoved(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return 1;
    }

static uint8 VlanRemoved(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return 1;
    }

static uint16 VlanLookupOffset(ThaClaEthFlowController self, uint16 lkType)
    {
	AtUnused(lkType);
	AtUnused(self);
    return 0;
    }

static uint8 QueueHwPriority(ThaClaEthFlowController self, uint8 queueId)
    {
    const uint8 cHighPriority = 0;
    const uint8 cLowPriority  = 1;
	AtUnused(self);
    return (uint8)((queueId == 7) ? cHighPriority : cLowPriority);
    }

static eBool NeedInitializeQueuePriority(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaEthFlowController);
    }

static void OverrideThaClaController(ThaClaEthFlowController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, m_ThaClaControllerMethods, sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, PartInit);
        mMethodOverride(m_ThaClaControllerOverride, PartDefaultSet);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void MethodsInit(ThaClaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, VlanLookupModeGet);
        mMethodOverride(m_methods, VlanLookupCtrl);
        mMethodOverride(m_methods, FlowLookupCtrl);
        mMethodOverride(m_methods, MaxNumVlans);
        mMethodOverride(m_methods, PsnToTdmEthTypeSet);
        mMethodOverride(m_methods, PsnToTdmEthTypeGet);
        mMethodOverride(m_methods, AllTrafficsDeactivate);
        mMethodOverride(m_methods, PidEntryEnableMask);
        mMethodOverride(m_methods, FcsRemoved);
        mMethodOverride(m_methods, VlanRemoved);
        mMethodOverride(m_methods, PartOffset);
        mMethodOverride(m_methods, VlanLookupOffset);
        mMethodOverride(m_methods, QueueHwPriority);
        mMethodOverride(m_methods, NeedInitializeQueuePriority);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(ThaClaEthFlowController self)
    {
    OverrideThaClaController(self);
    }

ThaClaEthFlowController ThaClaEthFlowControllerObjectInit(ThaClaEthFlowController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaControllerObjectInit((ThaClaController)self, cla) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthFlowController ThaClaEthFlowControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthFlowController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaClaEthFlowControllerObjectInit(newController, cla);
    }

uint8 ThaClaEthFlowControllerVlanLookupModeGet(ThaClaEthFlowController self, ThaEthFlow flow)
    {
    if (self)
        return mMethodsGet(self)->VlanLookupModeGet(self, flow);

    return 0;
    }

eAtRet ThaClaEthFlowControllerVlanLookupCtrl(ThaClaEthFlowController self, ThaEthFlow flow, uint16 lkType, uint32 channelId, uint32 vlanId, eBool  enable)
    {
    if (self)
        return mMethodsGet(self)->VlanLookupCtrl(self, flow, lkType, channelId, vlanId, enable);

    return 0;
    }

eAtRet ThaClaEthFlowControllerFlowLookupCtrl(ThaClaEthFlowController self, ThaEthFlow flow, uint32 hwFlowId, uint32 classId, uint32 queueId, uint32 vlanOffSet, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->FlowLookupCtrl(self, flow, hwFlowId, classId, queueId, vlanOffSet, enable);

    return 0;
    }

eAtRet ThaClaEthFlowControllerAllTrafficsDeactivate(ThaClaEthFlowController self)
    {
    if (self)
        return mMethodsGet(self)->AllTrafficsDeactivate(self);

    return cAtError;
    }

uint32 ThaClaEthFlowControllerMaxNumVlans(ThaClaEthFlowController self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumVlans(self);

    return 0;
    }

eAtRet ThaClaEthFlowControllerPsnToTdmEthTypeSet(ThaClaEthFlowController self, uint32 entryIndex, uint32 ethType)
    {
    if (self)
        return mMethodsGet(self)->PsnToTdmEthTypeSet(self, entryIndex, ethType);

    return cAtError;
    }

uint32 ThaClaEthFlowControllerPsnToTdmEthTypeGet(ThaClaEthFlowController self, uint32 entryIndex)
    {
    if (self)
        return mMethodsGet(self)->PsnToTdmEthTypeGet(self, entryIndex);

    return 0;
    }

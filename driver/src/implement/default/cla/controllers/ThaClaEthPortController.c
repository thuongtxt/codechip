/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaEthPortController.c
 *
 * Created Date: Nov 26, 2013
 *
 * Description : CLA ETH port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaClaEthPortControllerInternal.h"
#include "ThaClaControllerInternal.h"
#include "../ThaModuleClaReg.h"
#include "../pw/ThaModuleClaPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (ThaClaEthPortController)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaClaEthPortControllerMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tThaClaControllerMethods m_ThaClaControllerOverride;

/* Save supper implement */
static const tAtObjectMethods         *m_AtObjectMethods         = NULL;
static const tThaClaControllerMethods *m_ThaClaControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleEth EthModule(AtEthPort port)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)port);
    return (ThaModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    }

static uint32 EthPortPartOffset(ThaClaEthPortController self, AtEthPort port)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)port);
    ThaModuleEth ethModule = EthModule(port);
    uint8 partId = ThaModuleEthPartOfPort(ethModule, port);
	AtUnused(self);
    return ThaDeviceModulePartOffset(device, cThaModuleCla, partId);
    }

static uint32 CounterPartOffset(ThaClaEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType)
    {
	AtUnused(counterType);
    return mMethodsGet(self)->EthPortPartOffset(self, port);
    }

static uint32 CounterLocalOffset(ThaClaEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType, eBool r2c)
    {
    uint8 localId = ThaModuleEthLocalEthPortId(EthModule(port), port);
	AtUnused(counterType);
	AtUnused(self);

    r2c = r2c ? 0 : 1; /* Hardware use 0 for read to clear, 1 for read-only */
    return (128UL * r2c) + (256UL * localId);
    }

static uint32 CounterOffset(ThaClaEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType, eBool r2c)
    {
    return CounterLocalOffset(self, port, counterType, r2c) + mMethodsGet(self)->CounterPartOffset(self, port, counterType);
    }

static uint32 IncomPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaIncomPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeIncomPkt, r2c), cThaModuleCla);
    }

static uint32 IncombyteRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaIncombyteCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeIncombyte, r2c), cThaModuleCla);
    }

static uint32 EthPktBusErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktBusErrCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktBusErr, r2c), cThaModuleCla);
    }

static uint32 EthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktFcsErrCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktFcsErr, r2c), cThaModuleCla);
    }

static uint32 EthPktoversizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktoversizeCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktoversize, r2c), cThaModuleCla);
    }

static uint32 EthPktundersizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktundersizeCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktundersize, r2c), cThaModuleCla);
    }

static uint32 EthPktLensmallerthan64bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktLensmallerthan64bytesCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktLensmallerthan64bytes, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom65to127bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktLenfrom65to127bytesCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktLenfrom65to127bytes, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom128to255bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktLenfrom128to255bytesCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktLenfrom128to255bytes, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom256to511bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktLenfrom256to511bytesCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktLenfrom256to511bytes, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom512to1024bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktLenfrom512to1024bytesCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktLenfrom512to1024bytes, r2c), cThaModuleCla);
    }

static uint32 EthPktLenfrom1025to1528bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktLenfrom1025to1528bytesCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktLenfrom1025to1528bytes, r2c), cThaModuleCla);
    }

static uint32 EthPktJumboLenRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktJumboLenCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktJumboLen, r2c), cThaModuleCla);
    }

static uint32 PWunSuppedRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaPWunSuppedRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypePWunSuppedRxPkt, r2c), cThaModuleCla);
    }

static uint32 HCBElookupnotmatchPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaHCBElookupnotmatchPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeHCBElookupnotmatchPkt, r2c), cThaModuleCla);
    }

static uint32 EthPktDscdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktDscdCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktDscd, r2c), cThaModuleCla);
    }

static uint32 PauFrmRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaPauFrmRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypePauFrmRxPkt, r2c), cThaModuleCla);
    }

static uint32 PauFrmErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaPauFrmErrPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypePauFrmErrPkt, r2c), cThaModuleCla);
    }

static uint32 BcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaBcastRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeBcastRxPkt, r2c), cThaModuleCla);
    }

static uint32 McastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMcastRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMcastRxPkt, r2c), cThaModuleCla);
    }

static uint32 ARPRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaARPRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeARPRxPkt, r2c), cThaModuleCla);
    }

static uint32 EthOamRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthOamRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthOamRxPkt, r2c), cThaModuleCla);
    }

static uint32 EthOamRxErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthOamRxErrPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthOamRxErrPkt, r2c), cThaModuleCla);
    }

static uint32 EthOamtype0RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthOamtype0RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthOamtype0RxPkt, r2c), cThaModuleCla);
    }

static uint32 EthOamtype1RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthOamtype1RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthOamtype1RxPkt, r2c), cThaModuleCla);
    }

static uint32 IPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaIPv4RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeIPv4RxPkt, r2c), cThaModuleCla);
    }

static uint32 IPv4PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaIPv4PktErrCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeIPv4PktErr, r2c), cThaModuleCla);
    }

static uint32 ICMPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaICMPv4RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeICMPv4RxPkt, r2c), cThaModuleCla);
    }

static uint32 IPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaIPv6RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeIPv6RxPkt, r2c), cThaModuleCla);
    }

static uint32 IPv6PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaIPv6PktErrCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeIPv6PktErr, r2c), cThaModuleCla);
    }

static uint32 ICMPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaICMPv6RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeICMPv6RxPkt, r2c), cThaModuleCla);
    }

static uint32 MEFRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMEFRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMEFRxPkt, r2c), cThaModuleCla);
    }

static uint32 MEFErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMEFErrRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMEFErrRxPkt, r2c), cThaModuleCla);
    }

static uint32 MPLSRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMPLSRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMPLSRxPkt, r2c), cThaModuleCla);
    }

static uint32 MPLSErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMPLSErrRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMPLSErrRxPkt, r2c), cThaModuleCla);
    }

static uint32 MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMPLSwithoutPHPMdOuterlookupnotmatchCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMPLSwithoutPHPMdOuterlookupnotmatch, r2c), cThaModuleCla);
    }

static uint32 MPLSDataRxPktwithPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMPLSDataRxPktwithPHPMdCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMPLSDataRxPktwithPHPMd, r2c), cThaModuleCla);
    }

static uint32 MPLSDataRxPktwithoutPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMPLSDataRxPktwithoutPHPMdCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMPLSDataRxPktwithoutPHPMd, r2c), cThaModuleCla);
    }

static uint32 LDPIPv4withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaLDPIPv4withPHPMdRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeLDPIPv4withPHPMdRxPkt, r2c), cThaModuleCla);
    }

static uint32 LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaLDPIPv4overMLPSwithoutPHPMdRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeLDPIPv4overMLPSwithoutPHPMdRxPkt, r2c), cThaModuleCla);
    }

static uint32 LDPIPv6withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaLDPIPv6withPHPMdRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeLDPIPv6withPHPMdRxPkt, r2c), cThaModuleCla);
    }

static uint32 LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaLDPIPv6overMLPSwithoutPHPMdRxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeLDPIPv6overMLPSwithoutPHPMdRxPkt, r2c), cThaModuleCla);
    }

static uint32 MPLSoverIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMPLSoverIPv4RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMPLSoverIPv4RxPkt, r2c), cThaModuleCla);
    }

static uint32 MPLSoverIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaMPLSoverIPv6RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeMPLSoverIPv6RxPkt, r2c), cThaModuleCla);
    }

static uint32 L2TPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaL2TPPktErrCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeL2TPPktErr, r2c), cThaModuleCla);
    }

static uint32 L2TPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaL2TPIPv4RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeL2TPIPv4RxPkt, r2c), cThaModuleCla);
    }

static uint32 L2TPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaL2TPIPv6RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeL2TPIPv6RxPkt, r2c), cThaModuleCla);
    }

static uint32 UDPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaUDPPktErrCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeUDPPktErr, r2c), cThaModuleCla);
    }

static uint32 UDPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaUDPIPv4RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeUDPIPv4RxPkt, r2c), cThaModuleCla);
    }

static uint32 UDPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaUDPIPv6RxPktCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeUDPIPv6RxPkt, r2c), cThaModuleCla);
    }

static uint32 EthPktForwardtoCpuRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktForwardtoCpuCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktForwardtoCpu, r2c), cThaModuleCla);
    }

static uint32 EthPktForwardtoPDaRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktForwardtoPDacnter + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktForwardtoPDa, r2c), cThaModuleCla);
    }

static uint32 EthPktTimePktToPRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegClaEthPktTimePktToPCnt + CounterOffset(self, port, cThaClaEthPortCounterTypeEthPktTimePktToP, r2c), cThaModuleCla);
    }

static uint32 RxHdrErrPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 RxErrPsnPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static eAtRet MaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    if (maxPacketSize == mMethodsGet(self)->MaxPacketSizeGet(self, port))
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static uint32 MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    /* All of products should support this size */
    return 1518;
    }

static eAtRet MinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    if (minPacketSize == mMethodsGet(self)->MinPacketSizeGet(self, port))
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static uint32 MinPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    /* All of products must support this size */
    return 64;
    }

static eAtRet EthPortMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
    uint32 partOffset = mMethodsGet(self)->EthPortPartOffset(self, port);
    uint32 regAddr = ThaClaControllerClaGlbPsnCtrl((ThaClaController)self) + partOffset;
    uint32 regVal  = mChannelHwRead(port, regAddr, cThaModuleCla);
    mRegFieldSet(regVal, cThaRegClaMacCheckDis, enable ? 0 : 1);
    mChannelHwWrite(port, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool EthPortMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
    uint32 partOffset = mMethodsGet(self)->EthPortPartOffset(self, port);
    uint32 regAddr = ThaClaControllerClaGlbPsnCtrl((ThaClaController)self) + partOffset;
    uint32 regVal  = mChannelHwRead(port, regAddr, cThaModuleCla);
    return (regVal & cThaRegClaMacCheckDisMask) ? cAtFalse : cAtTrue;
    }

static eAtRet MacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
	AtUnused(macAddr);
	AtUnused(port);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet MacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
	AtUnused(macAddr);
	AtUnused(port);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet IpV4AddressSet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
	AtUnused(address);
	AtUnused(port);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtRet IpV4AddressGet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
	AtUnused(address);
	AtUnused(port);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtRet IpV6AddressSet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
	AtUnused(address);
	AtUnused(port);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtRet IpV6AddressGet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
	AtUnused(address);
	AtUnused(port);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static uint16 SVlanTpid(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return 0x88a8;
    }

static eBool NeedMaxPacketSizeDefaultSetup(ThaClaEthPortController self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultMaxPacketSizeForPort(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return 9600;
    }

static eAtRet MaxPacketSizeDefaultSetup(ThaClaController self)
    {
    uint8 part_i;
    eAtRet ret = cAtOk;
    ThaModuleCla claModule;
    AtModuleEth ethModule;

    if (!mMethodsGet(mThis(self))->NeedMaxPacketSizeDefaultSetup(mThis(self)))
        return cAtOk;

    claModule = ThaClaControllerModuleGet(self);
    ethModule  = (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)claModule), cAtModuleEth);

    for (part_i = 0; part_i < ThaModuleClaNumParts(ThaClaControllerModuleGet(self)); part_i++)
        {
        AtEthPort port;
        uint32 maxPacketSize;

        if (mMethodsGet(self)->PartIsUnused(self, part_i))
            continue;

        /* The engine receive packet which removed 4byte FCS field(at CLA).
         * So we need configure real maximum packet size is 9596 */
        port = AtModuleEthPortGet(ethModule, part_i);
        maxPacketSize = mMethodsGet(mThis(self))->DefaultMaxPacketSizeForPort(mThis(self), port);
        ret |= ThaClaEthPortControllerMaxPacketSizeSet((ThaClaEthPortController)self, port, maxPacketSize);
        }

    return ret;
    }

static eAtRet Init(ThaClaController self)
    {
    eAtRet ret = m_ThaClaControllerMethods->Init(self);
    
    if (ret != cAtOk)
        return ret;

    return MaxPacketSizeDefaultSetup(self);
    }

static eAtRet AsyncInit(ThaClaController self)
    {
    return Init(self);
    }

static uint32 EthPktLenfrom1529to2047bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(r2c);
    AtUnused(port);
    AtUnused(self);
    return 0;
    }

static uint32 UnicastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(r2c);
    AtUnused(port);
    AtUnused(self);
    return 0;
    }

static uint32 PhysicalErrorRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    AtUnused(r2c);
    AtUnused(port);
    AtUnused(self);
    return 0;
    }

static const char *ToString(AtObject self)
    {
    AtModule module = (AtModule)ThaClaControllerModuleGet((ThaClaController)self);
    AtDevice dev = AtModuleDeviceGet(module);
    static char str[64];

    AtSprintf(str, "%scla_eport_controller", AtDeviceIdToString(dev));
    return str;
    }

static void MethodsInit(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EthPortPartOffset);
        mMethodOverride(m_methods, EthPortMacCheckEnable);
        mMethodOverride(m_methods, EthPortMacCheckIsEnabled);
        mMethodOverride(m_methods, MaxPacketSizeSet);
        mMethodOverride(m_methods, MaxPacketSizeGet);
        mMethodOverride(m_methods, MinPacketSizeSet);
        mMethodOverride(m_methods, MinPacketSizeGet);
	
		/* IP Addresses */
        mMethodOverride(m_methods, IpV4AddressSet);
        mMethodOverride(m_methods, IpV4AddressGet);
        mMethodOverride(m_methods, IpV6AddressSet);
        mMethodOverride(m_methods, IpV6AddressGet);

        /* Counters */
        mMethodOverride(m_methods, CounterPartOffset);
        mMethodOverride(m_methods, IncomPktRead2Clear);
        mMethodOverride(m_methods, IncombyteRead2Clear);
        mMethodOverride(m_methods, EthPktBusErrRead2Clear);
        mMethodOverride(m_methods, EthPktFcsErrRead2Clear);
        mMethodOverride(m_methods, EthPktoversizeRead2Clear);
        mMethodOverride(m_methods, EthPktundersizeRead2Clear);
        mMethodOverride(m_methods, EthPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_methods, EthPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_methods, EthPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_methods, EthPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_methods, EthPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_methods, EthPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_methods, EthPktJumboLenRead2Clear);
        mMethodOverride(m_methods, PWunSuppedRxPktRead2Clear);
        mMethodOverride(m_methods, HCBElookupnotmatchPktRead2Clear);
        mMethodOverride(m_methods, EthPktDscdRead2Clear);
        mMethodOverride(m_methods, PauFrmRxPktRead2Clear);
        mMethodOverride(m_methods, PauFrmRxPktRead2Clear);
        mMethodOverride(m_methods, PauFrmErrPktRead2Clear);
        mMethodOverride(m_methods, BcastRxPktRead2Clear);
        mMethodOverride(m_methods, McastRxPktRead2Clear);
        mMethodOverride(m_methods, ARPRxPktRead2Clear);
        mMethodOverride(m_methods, EthOamRxPktRead2Clear);
        mMethodOverride(m_methods, EthOamRxErrPktRead2Clear);
        mMethodOverride(m_methods, EthOamtype0RxPktRead2Clear);
        mMethodOverride(m_methods, EthOamtype1RxPktRead2Clear);
        mMethodOverride(m_methods, IPv4RxPktRead2Clear);
        mMethodOverride(m_methods, IPv4PktErrRead2Clear);
        mMethodOverride(m_methods, ICMPv4RxPktRead2Clear);
        mMethodOverride(m_methods, IPv6RxPktRead2Clear);
        mMethodOverride(m_methods, IPv6PktErrRead2Clear);
        mMethodOverride(m_methods, ICMPv6RxPktRead2Clear);
        mMethodOverride(m_methods, MEFRxPktRead2Clear);
        mMethodOverride(m_methods, MEFErrRxPktRead2Clear);
        mMethodOverride(m_methods, MPLSRxPktRead2Clear);
        mMethodOverride(m_methods, MPLSErrRxPktRead2Clear);
        mMethodOverride(m_methods, MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear);
        mMethodOverride(m_methods, MPLSDataRxPktwithPHPMdRead2Clear);
        mMethodOverride(m_methods, MPLSDataRxPktwithoutPHPMdRead2Clear);
        mMethodOverride(m_methods, LDPIPv4withPHPMdRxPktRead2Clear);
        mMethodOverride(m_methods, LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear);
        mMethodOverride(m_methods, LDPIPv6withPHPMdRxPktRead2Clear);
        mMethodOverride(m_methods, LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear);
        mMethodOverride(m_methods, MPLSoverIPv4RxPktRead2Clear);
        mMethodOverride(m_methods, MPLSoverIPv6RxPktRead2Clear);
        mMethodOverride(m_methods, L2TPPktErrRead2Clear);
        mMethodOverride(m_methods, L2TPIPv4RxPktRead2Clear);
        mMethodOverride(m_methods, L2TPIPv6RxPktRead2Clear);
        mMethodOverride(m_methods, UDPPktErrRead2Clear);
        mMethodOverride(m_methods, UDPIPv4RxPktRead2Clear);
        mMethodOverride(m_methods, UDPIPv6RxPktRead2Clear);
        mMethodOverride(m_methods, EthPktForwardtoCpuRead2Clear);
        mMethodOverride(m_methods, EthPktForwardtoPDaRead2Clear);
        mMethodOverride(m_methods, EthPktTimePktToPRead2Clear);
        mMethodOverride(m_methods, RxHdrErrPacketsRead2Clear);
        mMethodOverride(m_methods, RxErrPsnPacketsRead2Clear);
        mMethodOverride(m_methods, UnicastRxPktRead2Clear);
        mMethodOverride(m_methods, EthPktLenfrom1529to2047bytesRead2Clear);
        mMethodOverride(m_methods, PhysicalErrorRxPktRead2Clear);

        mMethodOverride(m_methods, MacSet);
        mMethodOverride(m_methods, MacGet);

        mMethodOverride(m_methods, SVlanTpid);
        mMethodOverride(m_methods, NeedMaxPacketSizeDefaultSetup);
        mMethodOverride(m_methods, DefaultMaxPacketSizeForPort);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(ThaClaEthPortController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaClaController(ThaClaEthPortController self)
    {
    ThaClaController controller = (ThaClaController)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, m_ThaClaControllerMethods, sizeof(m_ThaClaControllerOverride));

        mMethodOverride(m_ThaClaControllerOverride, Init);
        mMethodOverride(m_ThaClaControllerOverride, AsyncInit);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideAtObject(self);
    OverrideThaClaController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaEthPortController);
    }

ThaClaEthPortController ThaClaEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaControllerObjectInit((ThaClaController)self, cla) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

uint32 ThaClaEthPortControllerIncomPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->IncomPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerIncombyteRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->IncombyteRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktBusErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktBusErrRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktFcsErrRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktoversizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktoversizeRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktundersizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktundersizeRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktLensmallerthan64bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktLensmallerthan64bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktLenfrom65to127bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktLenfrom65to127bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktLenfrom128to255bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktLenfrom128to255bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktLenfrom256to511bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktLenfrom256to511bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktLenfrom512to1024bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktLenfrom512to1024bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktLenfrom1025to1528bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktLenfrom1025to1528bytesRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktJumboLenRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktJumboLenRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerPWunSuppedRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->PWunSuppedRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerHCBElookupnotmatchPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->HCBElookupnotmatchPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktDscdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktDscdRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerPauFrmRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->PauFrmRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerPauFrmErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->PauFrmErrPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerBcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->BcastRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->McastRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerARPRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->ARPRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthOamRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthOamRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthOamRxErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthOamRxErrPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthOamtype0RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthOamtype0RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthOamtype1RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthOamtype1RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->IPv4RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerIPv4PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->IPv4PktErrRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerICMPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->ICMPv4RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->IPv6RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerIPv6PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->IPv6PktErrRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerICMPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->ICMPv6RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMEFRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MEFRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMEFErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MEFErrRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMPLSRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MPLSRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMPLSErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MPLSErrRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMPLSDataRxPktwithPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MPLSDataRxPktwithPHPMdRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMPLSDataRxPktwithoutPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MPLSDataRxPktwithoutPHPMdRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerLDPIPv4withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->LDPIPv4withPHPMdRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerLDPIPv6withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->LDPIPv6withPHPMdRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMPLSoverIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MPLSoverIPv4RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerMPLSoverIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->MPLSoverIPv6RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerL2TPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->L2TPPktErrRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerL2TPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->L2TPIPv4RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerL2TPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->L2TPIPv6RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerUDPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->UDPPktErrRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerUDPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->UDPIPv4RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerUDPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->UDPIPv6RxPktRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktForwardtoCpuRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktForwardtoCpuRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktForwardtoPDaRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktForwardtoPDaRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerEthPktTimePktToPRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktTimePktToPRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerRxHdrErrPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->RxHdrErrPacketsRead2Clear(self, port, r2c);

    return 0;
    }

uint32 ThaClaEthPortControllerRxErrPsnPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->RxErrPsnPacketsRead2Clear(self, port, r2c);

    return 0;
    }

eAtRet ThaClaEthPortControllerMaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    if (self)
        return mMethodsGet(self)->MaxPacketSizeSet(self, port, maxPacketSize);

    return 0;
    }

uint32 ThaClaEthPortControllerMaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->MaxPacketSizeGet(self, port);

    return 0;
    }

eAtRet ThaClaEthPortControllerMinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    if (self)
        return mMethodsGet(self)->MinPacketSizeSet(self, port, minPacketSize);

    return 0;
    }

uint32 ThaClaEthPortControllerMinPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->MinPacketSizeGet(self, port);

    return 0;
    }

eAtRet ThaClaEthPortControllerMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->EthPortMacCheckEnable(self, port, enable);

    return cAtError;
    }

eBool ThaClaEthPortControllerMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->EthPortMacCheckIsEnabled(self, port);

    return cAtFalse;
    }

eAtRet ThaClaEthPortControllerMacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    if (self)
        return mMethodsGet(self)->MacSet(self, port, macAddr);

    return cAtError;
    }

eAtRet ThaClaEthPortControllerMacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    if (self)
        return mMethodsGet(self)->MacGet(self, port, macAddr);

    return cAtError;
    }

uint16 ThaClaEthPortControllerSVlanTpid(ThaClaEthPortController self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->SVlanTpid(self, port);

    return 0x88a8;
    }

eAtRet ThaClaEthPortControllerIpV4Set(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV4AddressSet(self, port, address);

    return cAtError;
    }

eAtRet ThaClaEthPortControllerIpV4Get(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV4AddressGet(self, port, address);

    return cAtError;
    }

eAtRet ThaClaEthPortControllerIpV6Set(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV6AddressSet(self, port, address);

    return cAtError;
    }

eAtRet ThaClaEthPortControllerIpV6Get(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->IpV6AddressGet(self, port, address);

    return cAtError;
    }

uint32 ThaClaEthPortControllerEthPktLenfrom1529to2047bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->EthPktLenfrom1529to2047bytesRead2Clear(self, port, r2c);
    return 0;
    }

uint32 ThaClaEthPortControllerUnicastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->UnicastRxPktRead2Clear(self, port, r2c);
    return 0;
    }

uint32 ThaClaEthPortControllerPhysicalErrorRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->PhysicalErrorRxPktRead2Clear(self, port, r2c);
    return 0;
    }

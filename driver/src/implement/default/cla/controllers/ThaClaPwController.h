/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaPwController.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA controller for PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLAPWCONTROLLER_H_
#define _THACLAPWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPw.h"
#include "AtPwHdlc.h"
#include "../hbce/ThaHbceClasses.h"
#include "../../pw/ThaModulePw.h"
#include "ThaClaController.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eThaPwErrorCheckingMode
    {
    cThaPwErrorCheckingModeNoCheck,                 /* Don't check */
    cThaPwErrorCheckingModeCheckCountAndDiscard,    /* Check, count and discard */
    cThaPwErrorCheckingModeCheckCountAndNotDiscard  /* Check, count but do not discard */
    }eThaPwErrorCheckingMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete controller */
ThaClaPwController ThaClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController ThaClaPwControllerV2New(ThaModuleCla cla);

eAtRet ThaClaPwControllerCdrEnable(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable);
eBool ThaClaPwControllerCdrIsEnabled(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerSuperInit(ThaClaController self);
eAtRet ThaClaPwControllerAllCdrDisable(ThaClaPwController self);
eAtRet ThaClaPwControllerSuperAsyncInit(ThaClaController self);

/* Lookup */
eAtRet ThaClaPwControllerPwLabelSet(ThaClaPwController self, AtPw pw, AtPwPsn psn);
eBool ThaClaPwControllerLookupModeIsSupported(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode);
eAtRet ThaClaPwControllerLookupModeSet(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode);
eThaPwLookupMode ThaClaPwControllerLookupModeGet(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerLookupRemove(ThaClaPwController self, AtPw pwAdapter, eBool applyHardware);
eAtRet ThaClaPwControllerPwLookupEnable(ThaClaPwController self, AtPw pwAdapter, eBool enable);
eThaPwLookupMode ThaClaPwControllerPartPwLookupModeGet(ThaClaPwController self, uint8 partId);
eAtRet ThaClaPwControllerCdrCircuitSliceSet(ThaClaPwController self, AtPw pw, uint8 slice);

/* Counters */
uint32 ThaClaPwControllerRxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxMalformedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxStrayPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxOamPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxLbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxRbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxMbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxNbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxPbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxDuplicatedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);
uint32 ThaClaPwControllerRxDiscardedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear);

/* PW attributes */
eBool ThaClaPwControllerPwTypeIsSupported(ThaClaPwController self, eAtPwType pwType);
eAtRet ThaClaPwControllerPwTypeSet(ThaClaPwController self, AtPw pw, eAtPwType pwType);
eAtRet ThaClaPwControllerPayloadSizeSet(ThaClaPwController self, AtPw pw, uint16 payloadSize);

/* RTP */
eAtRet ThaClaPwControllerPwRtpEnable(ThaClaPwController self, AtPw pw, eBool enable);
eBool ThaClaPwControllerPwRtpIsEnabled(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerPwRtpPayloadTypeSet(ThaClaPwController self, AtPw pw, uint8 payloadType);
uint8 ThaClaPwControllerPwRtpPayloadTypeGet(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerPwRtpSsrcSet(ThaClaPwController self, AtPw pw, uint32 ssrc);
uint32 ThaClaPwControllerPwRtpSsrcGet(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerRtpSequenceMistmatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
eAtRet ThaClaPwControllerRtpSsrcMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
eThaPwErrorCheckingMode ThaClaPwControllerRtpSsrcMismatchCheckModeGet(ThaClaPwController self, AtPw pw);
eThaPwErrorCheckingMode ThaClaPwControllerRtpPldTypeMismatchCheckModeGet(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerRtpPldTypeMismatchCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);

/* Control word */
eAtRet ThaClaPwControllerPwCwLengthModeSet(ThaClaPwController self, AtPw pw, eAtPwCwLengthMode lengthMode);
eBool ThaClaPwControllerPwCwIsEnabled(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerPwCwEnable(ThaClaPwController self, AtPw pw, eBool enable);

/* Alarms */
uint32 ThaClaPwControllerPwAlarmGet(ThaClaPwController self, AtPw pw);
uint32 ThaClaPwControllerPwDefectHistoryGet(ThaClaPwController self, AtPw pw, eBool read2Clear);

/* Jitter related */
eAtRet ThaClaPwControllerReorderEnable(ThaClaPwController self, AtPw pw, eBool enable);
eBool ThaClaPwControllerReorderIsEnable(ThaClaPwController self, AtPw pw);

/* Error checking */
eAtRet ThaClaPwControllerMalformCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
eAtRet ThaClaPwControllerRealLenCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);
eAtRet ThaClaPwControllerLenFieldCheckModeSet(ThaClaPwController self, AtPw pw, eThaPwErrorCheckingMode checkMode);

/* VLANs */
eAtRet ThaClaPwControllerExpectedCVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *cVlan);
eAtRet ThaClaPwControllerExpectedSVlanSet(ThaClaPwController self, AtPw pw, tAtEthVlanTag *sVlan);
eBool ThaClaPwControllerExpectedVlanTagsConflict(ThaClaPwController self,
                                                 tAtEthVlanTag *expectedCVlan1, tAtEthVlanTag *expectedSVlan1,
                                                 tAtEthVlanTag *expectedCVlan2, tAtEthVlanTag *expectedSVlan2);
/* HBCE */
ThaHbce ThaClaPwControllerHbceGet(ThaClaPwController self, AtPw pw);
ThaHbce ThaClaPwControllerHbceForPart(ThaClaPwController self, uint8 partId);
uint8 ThaClaPwControllerHbceMaxFlowsPerEntry(ThaClaPwController self);
uint32 ThaClaPwControllerCLAHBCEGlbCtrl(ThaClaPwController self);
uint32 ThaClaPwControllerCLAHBCEHashingTabCtrl(ThaClaPwController self);
uint32 ThaClaPwControllerClaHbceLookingUpInformationCtrl(ThaClaPwController self);
uint32 ThaClaPwControllerClaHbceFlowIdMask(ThaClaPwController self);
uint8 ThaClaPwControllerClaHbceFlowIdShift(ThaClaPwController self);
uint32 ThaClaPwControllerClaHbceCodingSelectedModeMask(ThaClaPwController self);
uint8 ThaClaPwControllerClaHbceCodingSelectedModeShift(ThaClaPwController self);
uint32 ThaClaPwControllerClaHbceHashingTabSelectedPageMask(ThaClaPwController self);
uint8 ThaClaPwControllerClaHbceHashingTabSelectedPageShift(ThaClaPwController self);
uint32 ThaClaPwControllerClaHbceTimeoutValMask(ThaClaPwController self);
uint8 ThaClaPwControllerClaHbceTimeoutValShift(ThaClaPwController self);
uint32 ThaClaPwControllerCLAHbceFlowNumMask(ThaClaPwController self);
uint8 ThaClaPwControllerCLAHbceFlowNumShift(ThaClaPwController self);
uint32 ThaClaPwControllerCLAHbceMemoryStartPtrMask(ThaClaPwController self);
uint8 ThaClaPwControllerCLAHbceMemoryStartPtrShift(ThaClaPwController self);

uint32 ThaClaPwControllerV2PwTypeCtrlAddress(ThaClaPwControllerV2 self, AtPw pw);
uint32 ThaClaPwControllerPartOffset(ThaClaPwController self, AtPw pw);

/* CEP */
eAtRet ThaClaPwControllerPwCepEbmEnable(ThaClaPwController self, AtPw pw, eBool enable);

/* Global configuration */
eAtRet ThaClaPwControllerSendLbitPacketToCdrEnable(ThaClaPwController self, eBool enable);
eBool ThaClaPwControllerSendLbitPacketToCdrIsEnabled(ThaClaPwController self);

/* Part management */
uint8 ThaClaPwControllerPartOfPw(ThaClaPwController self, AtPw pw);

/* Debug */
void ThaClaPwControllerPwRegsShow(ThaClaPwController self, AtPw pw);
void ThaClaPwControllerPktAnalyzerCla2CdrDataFlush(ThaClaPwController self, AtPw pw);
eAtRet ThaClaPwControllerPktAnalyzerCla2CdrTrigger(ThaClaPwController self, AtPw pw);
eBool ThaClaPwControllerPktAnalyzerCla2CdrHoIsEnabled(ThaClaPwController self, AtPw pw, uint16 packetId);
eBool ThaClaPwControllerPktAnalyzerCla2CdrPacketIsError(ThaClaPwController self, AtPw pw, uint16 packetId);
uint32 ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampGet(ThaClaPwController self, AtPw pw, uint16 packetId);
uint32 ThaClaPwControllerPktAnalyzerCla2CdrRtpTimeStampOffset(ThaClaPwController self, AtPw pw, uint16 previousPktId, uint16 nextPktId);
uint32 ThaClaPwControllerPktAnalyzerCla2CdrRtpSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId);
uint32 ThaClaPwControllerPktAnalyzerCla2CdrControlWordSequenceNumberGet(ThaClaPwController self, AtPw pw, uint16 packetId);

/* Suppress */
eAtRet ThaClaPwControllerPwSuppressEnable(ThaClaPwController self, AtPw pw, eBool enable);
eBool ThaClaPwControllerPwSuppressIsEnabled(ThaClaPwController self, AtPw pw);

/* Payload Type */
eAtRet ThaClaPwControllerHdlcPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
eAtRet ThaClaPwControllerMlpppPwPayloadTypeSet(ThaClaPwController self, AtPw adapter, eAtPwHdlcPayloadType payloadType);
eAtPwHdlcPayloadType ThaClaPwControllerDefaultPayloadType(ThaClaPwController self);

/* Product concretes */
ThaClaPwController Tha60000031ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60030011ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60031021ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60031031ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60031033ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60091023ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60150011ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60210011ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha61210011ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60210031ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60210012ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60290011ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha6A290011ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60290022ClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60290022ClaPwControllerV3New(ThaModuleCla cla);
ThaClaPwController ThaStmPwProductClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController ThaPdhPwProductClaPwControllerNew(ThaModuleCla cla);
ThaClaPwController Tha60290081ClaPwControllerNew(ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THACLAPWCONTROLLER_H_ */


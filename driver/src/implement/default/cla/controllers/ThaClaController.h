/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaController.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA abstract controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLACONTROLLER_H_
#define _THACLACONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "../ThaModuleCla.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete controllers */
ThaClaPwController ThaClaStmPwControllerV2New(ThaModuleCla cla);

ThaModuleCla ThaClaControllerModuleGet(ThaClaController self);

eAtRet ThaClaControllerAsyncInit(ThaClaController self);
eAtRet ThaClaControllerInit(ThaClaController self);
eAtRet ThaClaControllerSetup(ThaClaController self);
eAtRet ThaClaControllerDefaultSet(ThaClaController self);

eAtRet ThaClaControllerChannelEnable(ThaClaController self, AtChannel channel, eBool enable);
eBool ThaClaControllerChannelIsEnabled(ThaClaController self, AtChannel channel);

uint32 ThaClaControllerRead(ThaClaController self, uint32 address);
void ThaClaControllerWrite(ThaClaController self, uint32 address, uint32 value);
void ThaClaControllerDebug(ThaClaController self);

eAtRet ThaClaControllerMefOverMplsEnable(ThaClaController self, AtPw pw, eBool enable);
uint32 ThaClaControllerMefOverMplsExpectedEcidGet(ThaClaController self, AtPw pw);
eAtRet ThaClaControllerMefOverMplsExpectedMacGet(ThaClaController self, AtPw pw, uint8 *expectedMac);

/* Registers */
uint32 ThaClaControllerClaGlbPsnCtrl(ThaClaController self);

/* For standby driver */
uint32 ThaClaControllerRestore(ThaClaController self);

#ifdef __cplusplus
}
#endif
#endif /* _THACLACONTROLLER_H_ */


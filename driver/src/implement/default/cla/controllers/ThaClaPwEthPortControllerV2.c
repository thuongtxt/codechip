/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaPwEthPortControllerV2.c
 *
 * Created Date: Nov 27, 2013
 *
 * Description : CLA Ethernet port controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaClaEthPortControllerInternal.h"
#include "../pw/ThaModuleClaPwV2Reg.h"
#include "../ThaModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ByteShift(uint8 byteId)
	{
	return (uint32)((3 - ((byteId) % 4)) * 8);
	}

static uint32 ByteMask(uint8 byteId)
	{
	return cBit7_0 << ByteShift(byteId);
	}

static eAtRet MacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 macDword0, macDword1;
    uint32 partOffset = mMethodsGet(self)->EthPortPartOffset(self, port);
    uint32 regAddr, regVal;

    macDword0 = 0;
    macDword1 = 0;

    macDword1 |= *macAddr; macDword1 <<= 8;
    macDword1 |= *(macAddr + 1);

    macDword0 |= *(macAddr + 2); macDword0 <<= 8;
    macDword0 |= *(macAddr + 3); macDword0 <<= 8;
    macDword0 |= *(macAddr + 4); macDword0 <<= 8;
    macDword0 |= *(macAddr + 5);

    ThaClaControllerWrite((ThaClaController)self, cThaRegEthMacAddr31_0  + partOffset, macDword0);

    regAddr = cThaRegEthMacAddr47_32 + partOffset;
    regVal  = ThaClaControllerRead((ThaClaController)self,  regAddr) & cBit31_16; /* Clear field bit15_0 */
    ThaClaControllerWrite((ThaClaController)self, regAddr, regVal | macDword1);

    return cAtOk;
    }

static eAtRet MacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    uint32 regVal;
    uint32 partOffset = mMethodsGet(self)->EthPortPartOffset(self, port);

    regVal = ThaClaControllerRead((ThaClaController)self, cThaRegEthMacAddr47_32 + partOffset);
    mFieldGet(regVal, ByteMask(2), ByteShift(2), uint8, &macAddr[0]);
    mFieldGet(regVal, ByteMask(3), ByteShift(3), uint8, &macAddr[1]);

    regVal = ThaClaControllerRead((ThaClaController)self, cThaRegEthMacAddr31_0 + partOffset);
    mFieldGet(regVal, ByteMask(0), ByteShift(0), uint8, &macAddr[2]);
    mFieldGet(regVal, ByteMask(1), ByteShift(1), uint8, &macAddr[3]);
    mFieldGet(regVal, ByteMask(2), ByteShift(2), uint8, &macAddr[4]);
    mFieldGet(regVal, ByteMask(3), ByteShift(3), uint8, &macAddr[5]);

    return cAtOk;
    }

static uint32 IncomPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 IncombyteRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxByteCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktBusErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxFcsErrPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktoversizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPktundersizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPktLensmallerthan64bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxsmallerthan64bytesPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktLenfrom65to127bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxfrom65to127bytesPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktLenfrom128to255bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxfrom128to255bytesPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktLenfrom256to511bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxfrom256to511bytesPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktLenfrom512to1024bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxfrom512to1024bytesPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktLenfrom1025to1528bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxfrom1025to1528bytesPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthPktJumboLenRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxJumboPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 PWunSuppedRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 HCBElookupnotmatchPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPktDscdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 PauFrmRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 PauFrmErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 BcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 McastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 ARPRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthOamRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxOamPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 EthOamRxErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthOamtype0RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthOamtype1RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 IPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 IPv4PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 ICMPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 IPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 IPv6PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 ICMPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MEFRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MEFErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MPLSRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MPLSErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MPLSDataRxPktwithPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MPLSDataRxPktwithoutPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 LDPIPv4withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 LDPIPv6withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MPLSoverIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 MPLSoverIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 L2TPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 L2TPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 L2TPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 UDPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxUpdErrPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 UDPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 UDPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPktForwardtoCpuRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPktForwardtoPDaRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 EthPktTimePktToPRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
	AtUnused(r2c);
	AtUnused(port);
	AtUnused(self);
    return 0;
    }

static uint32 RxHdrErrPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxHdrErrPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 RxErrPsnPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return mChannelHwRead(port, cThaRegPmcEthRxPsnErrPktCnt(r2c) + mMethodsGet(self)->EthPortPartOffset(self, port), cThaModuleCla);
    }

static uint32 MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return 2048;
    }

static eAtRet IpV4AddressSet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    uint32 ipv4Addr = 0;
	AtUnused(self);

    /* Build hardware values */
    ipv4Addr |= *(address + 0);
    ipv4Addr <<= 8;
    ipv4Addr |= *(address + 1);
    ipv4Addr <<= 8;
    ipv4Addr |= *(address + 2);
    ipv4Addr <<= 8;
    ipv4Addr |= *(address + 3);

    /* Apply */
    mChannelHwWrite(port, cThaRegEthIpAddr31_0  , ipv4Addr, cAtModuleEth);
    mChannelHwWrite(port, cThaRegEthIpAddr63_32 , 0x0     , cAtModuleEth);
    mChannelHwWrite(port, cThaRegEthIpAddr95_64 , 0x0     , cAtModuleEth);
    mChannelHwWrite(port, cThaRegEthIpAddr127_96, 0x0     , cAtModuleEth);

    return cAtOk;
    }

static eAtRet IpV4AddressGet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    uint32 ipv4Addr =  mChannelHwRead(port, cThaRegEthIpAddr31_0, cAtModuleEth);
    uint8 value;

	AtUnused(self);

    /* Convert to software value */
	value = (uint8)ipv4Addr; *(address + 3) |= (uint8)value;  ipv4Addr >>= 8;
    value = (uint8)ipv4Addr; *(address + 2) |= (uint8)value;  ipv4Addr >>= 8;
    value = (uint8)ipv4Addr; *(address + 1) |= (uint8)value;  ipv4Addr >>= 8;
    value = (uint8)ipv4Addr; * address      |= (uint8)value;

    return cAtOk;
    }

static eAtRet IpV6AddressSet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    uint32 ipv6Add31to0   = 0 ;
    uint32 ipv6Add63to32  = 0;
    uint32 ipv6Add95to64  = 0;
    uint32 ipv6Add127to96 = 0;
	AtUnused(self);

    /* Make hardware values */
    ipv6Add127to96 |= *(address + 0); ipv6Add127to96 <<= 8;
    ipv6Add127to96 |= *(address + 1); ipv6Add127to96 <<= 8;
    ipv6Add127to96 |= *(address + 2); ipv6Add127to96 <<= 8;
    ipv6Add127to96 |= *(address + 3);

    ipv6Add95to64  |= *(address + 4); ipv6Add95to64 <<= 8;
    ipv6Add95to64  |= *(address + 5); ipv6Add95to64 <<= 8;
    ipv6Add95to64  |= *(address + 6); ipv6Add95to64 <<= 8;
    ipv6Add95to64  |= *(address + 7);

    ipv6Add63to32  |= *(address + 8); ipv6Add63to32 <<= 8;
    ipv6Add63to32  |= *(address + 9); ipv6Add63to32 <<= 8;
    ipv6Add63to32  |= *(address + 10); ipv6Add63to32 <<= 8;
    ipv6Add63to32  |= *(address + 11);

    ipv6Add31to0   |= *(address + 12); ipv6Add31to0 <<= 8;
    ipv6Add31to0   |= *(address + 13); ipv6Add31to0 <<= 8;
    ipv6Add31to0   |= *(address + 14); ipv6Add31to0 <<= 8;
    ipv6Add31to0   |= *(address + 15);

    /* Apply */
    mChannelHwWrite(port, cThaRegEthIpAddr31_0  , ipv6Add31to0, cAtModuleEth);
    mChannelHwWrite(port, cThaRegEthIpAddr63_32 , ipv6Add63to32, cAtModuleEth);
    mChannelHwWrite(port, cThaRegEthIpAddr95_64 , ipv6Add95to64, cAtModuleEth);
    mChannelHwWrite(port, cThaRegEthIpAddr127_96, ipv6Add127to96, cAtModuleEth);

    return cAtOk;
    }

static eAtRet IpV6AddressGet(ThaClaEthPortController self, AtEthPort port, uint8 *address)
    {
    uint32 ipv6Add31to0;
    uint32 ipv6Add63to32;
    uint32 ipv6Add95to64;
    uint32 ipv6Add127to96;
    uint8 value;

	AtUnused(self);

    /* Read hardware */
    ipv6Add31to0   = mChannelHwRead(port, cThaRegEthIpAddr31_0  , cAtModuleEth);
    ipv6Add63to32  = mChannelHwRead(port, cThaRegEthIpAddr63_32 , cAtModuleEth);
    ipv6Add95to64  = mChannelHwRead(port, cThaRegEthIpAddr95_64 , cAtModuleEth);
    ipv6Add127to96 = mChannelHwRead(port, cThaRegEthIpAddr127_96, cAtModuleEth);

    /* And return software values */
    value = (uint8)ipv6Add127to96; *(address + 3) |= value; ipv6Add127to96 >>= 8;
    value = (uint8)ipv6Add127to96; *(address + 2) |= value; ipv6Add127to96 >>= 8;
    value = (uint8)ipv6Add127to96; *(address + 1) |= value; ipv6Add127to96 >>= 8;
    value = (uint8)ipv6Add127to96; *(address + 0) |= value;

    value = (uint8)ipv6Add95to64; *(address + 7) |= value; ipv6Add95to64 >>= 8;
    value = (uint8)ipv6Add95to64; *(address + 6) |= value; ipv6Add95to64 >>= 8;
    value = (uint8)ipv6Add95to64; *(address + 5) |= value; ipv6Add95to64 >>= 8;
    value = (uint8)ipv6Add95to64; *(address + 4) |= value;

    value = (uint8)ipv6Add63to32; *(address + 11) |= value; ipv6Add63to32 >>= 8;
    value = (uint8)ipv6Add63to32; *(address + 10) |= value; ipv6Add63to32 >>= 8;
    value = (uint8)ipv6Add63to32; *(address + 9)  |= value; ipv6Add63to32 >>= 8;
    value = (uint8)ipv6Add63to32; *(address + 8)  |= value;

    value = (uint8)ipv6Add31to0; *(address + 15) |= value; ipv6Add31to0 >>= 8;
    value = (uint8)ipv6Add31to0; *(address + 14) |= value; ipv6Add31to0 >>= 8;
    value = (uint8)ipv6Add31to0; *(address + 13) |= value; ipv6Add31to0 >>= 8;
    value = (uint8)ipv6Add31to0; *(address + 12) |= value;

    return cAtOk;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaPwEthPortControllerV2);
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeGet);

        /* Counters */
        mMethodOverride(m_ThaClaEthPortControllerOverride, IncomPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IncombyteRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktBusErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktFcsErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktoversizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktundersizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktJumboLenRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PWunSuppedRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, HCBElookupnotmatchPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktDscdRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PauFrmRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PauFrmRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PauFrmErrPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, BcastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, McastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, ARPRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamRxErrPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamtype0RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamtype1RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv4PktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, ICMPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv6PktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, ICMPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MEFRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MEFErrRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSErrRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSDataRxPktwithPHPMdRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSDataRxPktwithoutPHPMdRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv4withPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv6withPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSoverIPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSoverIPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, L2TPPktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, L2TPIPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, L2TPIPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UDPPktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UDPIPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UDPIPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktForwardtoCpuRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktForwardtoPDaRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktTimePktToPRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, RxHdrErrPacketsRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, RxErrPsnPacketsRead2Clear);

        mMethodOverride(m_ThaClaEthPortControllerOverride, MacSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacGet);

        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV4AddressSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV4AddressGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV6AddressSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IpV6AddressGet);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    }

ThaClaEthPortController ThaClaPwEthPortControllerV2ObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController ThaClaPwEthPortControllerV2New(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ThaClaPwEthPortControllerV2ObjectInit(newController, cla);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaControllerInternal.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA abtract controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLACONTROLLERINTERNAL_H_
#define _THACLACONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/ThaDeviceInternal.h"
#include "../../util/ThaUtil.h"
#include "ThaClaController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mClaControllerRead(self, address) ThaClaControllerRead((ThaClaController)self, address)
#define mClaControllerWrite(self, address, value) ThaClaControllerWrite((ThaClaController)self, address, value)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClaControllerMethods
    {
    eAtRet (*Init)(ThaClaController self);
    eAtRet (*AsyncInit)(ThaClaController self);
    eAtRet (*DefaultSet)(ThaClaController self);
    eAtRet (*Setup)(ThaClaController self);
    eAtRet (*PartInit)(ThaClaController self, uint8 partId);
    eAtRet (*PartDefaultSet)(ThaClaController self, uint8 partId);
    void (*Debug)(ThaClaController self);
    eAtRet (*ChannelEnable)(ThaClaController self, AtChannel channel, eBool enable);
    eBool (*ChannelIsEnabled)(ThaClaController self, AtChannel channel);
    eBool (*PartIsUnused)(ThaClaController self, uint8 partId);
    eAtRet (*MefOverMplsEnable)(ThaClaController self, AtPw pw, eBool enable);
    uint32 (*MefOverMplsExpectedEcidGet)(ThaClaController self, AtPw pw);
    eAtRet (*MefOverMplsExpectedMacGet)(ThaClaController self, AtPw pw, uint8 *expectedMac);
    uint32 (*Restore)(ThaClaController self);

    /* Registers */
    uint32 (*ClaGlbPsnCtrl)(ThaClaController self);
    }tThaClaControllerMethods;

typedef struct tThaClaController
    {
    tAtObject super;
    const tThaClaControllerMethods *methods;

    /* Private data */
    ThaModuleCla claModule;
    }tThaClaController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaController ThaClaControllerObjectInit(ThaClaController self, ThaModuleCla claModule);

eBool ThaClaControllerPartIsUnused(ThaClaController self, uint8 partId);

#ifdef __cplusplus
}
#endif
#endif /* _THACLACONTROLLERINTERNAL_H_ */


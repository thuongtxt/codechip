/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaPppPwEthPortController.c
 *
 * Created Date: Nov 29, 2013
 *
 * Description : CLA ETH Port controller for product that has two separate parts,
 *               one for PPP/MLPPP feature and the other one for PW feature
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaClaPppPwEthPortControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((ThaClaPppPwEthPortController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaClaPppPwEthPortControllerMethods m_methods;

/* Override */
static tAtObjectMethods                m_AtObjectOverride;
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/* Save super implementation */
static const tAtObjectMethods         *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaClaEthPortController PppEthPortControllerCreate(ThaClaPppPwEthPortController self)
    {
    return ThaClaPppEthPortControllerNew(ThaClaControllerModuleGet((ThaClaController)self));
    }

static ThaClaEthPortController PwEthPortControllerCreate(ThaClaPppPwEthPortController self)
    {
    return ThaClaPwEthPortControllerNew(ThaClaControllerModuleGet((ThaClaController)self));
    }

static void DeleteEthPortController(ThaClaEthPortController *controller)
    {
    AtObjectDelete((AtObject)*controller);
    *controller = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteEthPortController(&mThis(self)->pppEthPortController);
    DeleteEthPortController(&mThis(self)->pwEthPortController);

    m_AtObjectMethods->Delete(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaClaPppPwEthPortController);
    }

static ThaClaEthPortController ControllerOfCounter(ThaClaPppPwEthPortController self, AtEthPort port, eThaClaEthPortCounterType counterType)
    {
	AtUnused(counterType);
	AtUnused(port);
	AtUnused(self);
    /* Concrete should know */
    return NULL;
    }

static ThaClaEthPortController ControllerOfPort(ThaClaPppPwEthPortController self, AtEthPort port)
    {
	AtUnused(port);
    return ThaClaPppPwEthPortControllerPwEthPortController(self);
    }

static uint32 IncomPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerIncomPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeIncomPkt), port, r2c);
    }

static uint32 IncombyteRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerIncombyteRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeIncombyte), port, r2c);
    }

static uint32 EthPktBusErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktBusErrRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktBusErr), port, r2c);
    }

static uint32 EthPktFcsErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktFcsErrRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktFcsErr), port, r2c);
    }

static uint32 EthPktoversizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktoversizeRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktoversize), port, r2c);
    }

static uint32 EthPktundersizeRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktundersizeRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktundersize), port, r2c);
    }

static uint32 EthPktLensmallerthan64bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktLensmallerthan64bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktLensmallerthan64bytes), port, r2c);
    }

static uint32 EthPktLenfrom65to127bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktLenfrom65to127bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktLenfrom65to127bytes), port, r2c);
    }

static uint32 EthPktLenfrom128to255bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktLenfrom128to255bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktLenfrom128to255bytes), port, r2c);
    }

static uint32 EthPktLenfrom256to511bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktLenfrom256to511bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktLenfrom256to511bytes), port, r2c);
    }

static uint32 EthPktLenfrom512to1024bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktLenfrom512to1024bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktLenfrom512to1024bytes), port, r2c);
    }

static uint32 EthPktLenfrom1025to1528bytesRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktLenfrom1025to1528bytesRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktLenfrom1025to1528bytes), port, r2c);
    }

static uint32 EthPktJumboLenRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktJumboLenRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktJumboLen), port, r2c);
    }

static uint32 PWunSuppedRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerPWunSuppedRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypePWunSuppedRxPkt), port, r2c);
    }

static uint32 HCBElookupnotmatchPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerHCBElookupnotmatchPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeHCBElookupnotmatchPkt), port, r2c);
    }

static uint32 EthPktDscdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktDscdRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktDscd), port, r2c);
    }

static uint32 PauFrmRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerPauFrmRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypePauFrmRxPkt), port, r2c);
    }

static uint32 PauFrmErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerPauFrmErrPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypePauFrmErrPkt), port, r2c);
    }

static uint32 BcastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerBcastRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeBcastRxPkt), port, r2c);
    }

static uint32 McastRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMcastRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMcastRxPkt), port, r2c);
    }

static uint32 ARPRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerARPRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeARPRxPkt), port, r2c);
    }

static uint32 EthOamRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthOamRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthOamRxPkt), port, r2c);
    }

static uint32 EthOamRxErrPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthOamRxErrPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthOamRxErrPkt), port, r2c);
    }

static uint32 EthOamtype0RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthOamtype0RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthOamtype0RxPkt), port, r2c);
    }

static uint32 EthOamtype1RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthOamtype1RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthOamtype1RxPkt), port, r2c);
    }

static uint32 IPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerIPv4RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeIPv4RxPkt), port, r2c);
    }

static uint32 IPv4PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerIPv4PktErrRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeIPv4PktErr), port, r2c);
    }

static uint32 ICMPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerICMPv4RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeICMPv4RxPkt), port, r2c);
    }

static uint32 IPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerIPv6RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeIPv6RxPkt), port, r2c);
    }

static uint32 IPv6PktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerIPv6PktErrRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeIPv6PktErr), port, r2c);
    }

static uint32 ICMPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerICMPv6RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeICMPv6RxPkt), port, r2c);
    }

static uint32 MEFRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMEFRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMEFRxPkt), port, r2c);
    }

static uint32 MEFErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMEFErrRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMEFErrRxPkt), port, r2c);
    }

static uint32 MPLSRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMPLSRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMPLSRxPkt), port, r2c);
    }

static uint32 MPLSErrRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMPLSErrRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMPLSErrRxPkt), port, r2c);
    }

static uint32 MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMPLSwithoutPHPMdOuterlookupnotmatchRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMPLSwithoutPHPMdOuterlookupnotmatch), port, r2c);
    }

static uint32 MPLSDataRxPktwithPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMPLSDataRxPktwithPHPMdRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMPLSDataRxPktwithPHPMd), port, r2c);
    }

static uint32 MPLSDataRxPktwithoutPHPMdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMPLSDataRxPktwithoutPHPMdRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMPLSDataRxPktwithoutPHPMd), port, r2c);
    }

static uint32 LDPIPv4withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerLDPIPv4withPHPMdRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeLDPIPv4withPHPMdRxPkt), port, r2c);
    }

static uint32 LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerLDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeLDPIPv4overMLPSwithoutPHPMdRxPkt), port, r2c);
    }

static uint32 LDPIPv6withPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerLDPIPv6withPHPMdRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeLDPIPv6withPHPMdRxPkt), port, r2c);
    }

static uint32 LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerLDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeLDPIPv6overMLPSwithoutPHPMdRxPkt), port, r2c);
    }

static uint32 MPLSoverIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMPLSoverIPv4RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMPLSoverIPv4RxPkt), port, r2c);
    }

static uint32 MPLSoverIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerMPLSoverIPv6RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeMPLSoverIPv6RxPkt), port, r2c);
    }

static uint32 L2TPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerL2TPPktErrRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeL2TPPktErr), port, r2c);
    }

static uint32 L2TPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerL2TPIPv4RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeL2TPIPv4RxPkt), port, r2c);
    }

static uint32 L2TPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerL2TPIPv6RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeL2TPIPv6RxPkt), port, r2c);
    }

static uint32 UDPPktErrRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerUDPPktErrRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeUDPPktErr), port, r2c);
    }

static uint32 UDPIPv4RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerUDPIPv4RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeUDPIPv4RxPkt), port, r2c);
    }

static uint32 UDPIPv6RxPktRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerUDPIPv6RxPktRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeUDPIPv6RxPkt), port, r2c);
    }

static uint32 EthPktForwardtoCpuRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktForwardtoCpuRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktForwardtoCpu), port, r2c);
    }

static uint32 EthPktForwardtoPDaRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktForwardtoPDaRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktForwardtoPDa), port, r2c);
    }

static uint32 EthPktTimePktToPRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerEthPktTimePktToPRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeEthPktTimePktToP), port, r2c);
    }

static uint32 RxHdrErrPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerRxHdrErrPacketsRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeRxHdrErrPackets), port, r2c);
    }

static uint32 RxErrPsnPacketsRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    return ThaClaEthPortControllerRxErrPsnPacketsRead2Clear(mMethodsGet(mThis(self))->ControllerOfCounter(mThis(self), port, cThaClaEthPortCounterTypeRxErrPsnPackets), port, r2c);
    }

static eAtRet MacSet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    return ThaClaEthPortControllerMacSet(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port, macAddr);
    }

static eAtRet MacGet(ThaClaEthPortController self, AtEthPort port, uint8 *macAddr)
    {
    return ThaClaEthPortControllerMacGet(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port, macAddr);
    }

static eAtRet EthPortMacCheckEnable(ThaClaEthPortController self, AtEthPort port, eBool enable)
    {
    return ThaClaEthPortControllerMacCheckEnable(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port, enable);
    }

static eBool EthPortMacCheckIsEnabled(ThaClaEthPortController self, AtEthPort port)
    {
    return ThaClaEthPortControllerMacCheckIsEnabled(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port);
    }

static eAtRet MaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    return ThaClaEthPortControllerMaxPacketSizeSet(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port, maxPacketSize);
    }

static uint32 MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    return ThaClaEthPortControllerMaxPacketSizeGet(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port);
    }

static eAtRet MinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    return ThaClaEthPortControllerMinPacketSizeSet(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port, minPacketSize);
    }

static uint32 MinPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    return ThaClaEthPortControllerMinPacketSizeGet(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port);
    }

static uint16 SVlanTpid(ThaClaEthPortController self, AtEthPort port)
    {
    return ThaClaEthPortControllerSVlanTpid(mMethodsGet(mThis(self))->ControllerOfPort(mThis(self), port), port);
    }

static void OverrideAtObject(ThaClaEthPortController self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        mMethodOverride(m_ThaClaEthPortControllerOverride, IncomPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IncombyteRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktBusErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktFcsErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktoversizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktundersizeRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLensmallerthan64bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom65to127bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom128to255bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom256to511bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom512to1024bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktLenfrom1025to1528bytesRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktJumboLenRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PWunSuppedRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, HCBElookupnotmatchPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktDscdRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PauFrmRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PauFrmRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, PauFrmErrPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, BcastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, McastRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, ARPRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamRxErrPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamtype0RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthOamtype1RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv4PktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, ICMPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, IPv6PktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, ICMPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MEFRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MEFErrRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSErrRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSwithoutPHPMdOuterlookupnotmatchRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSDataRxPktwithPHPMdRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSDataRxPktwithoutPHPMdRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv4withPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv4overMLPSwithoutPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv6withPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, LDPIPv6overMLPSwithoutPHPMdRxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSoverIPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MPLSoverIPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, L2TPPktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, L2TPIPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, L2TPIPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UDPPktErrRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UDPIPv4RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, UDPIPv6RxPktRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktForwardtoCpuRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktForwardtoPDaRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktTimePktToPRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, RxHdrErrPacketsRead2Clear);
        mMethodOverride(m_ThaClaEthPortControllerOverride, RxErrPsnPacketsRead2Clear);

        mMethodOverride(m_ThaClaEthPortControllerOverride, MacSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MacGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckEnable);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPortMacCheckIsEnabled);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, SVlanTpid);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideAtObject(self);
    OverrideThaClaEthPortController(self);
    }

static void MethodsInit(ThaClaPppPwEthPortController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PppEthPortControllerCreate);
        mMethodOverride(m_methods, PwEthPortControllerCreate);
        mMethodOverride(m_methods, ControllerOfCounter);
        mMethodOverride(m_methods, ControllerOfPort);
        }

    mMethodsSet(self, &m_methods);
    }

ThaClaEthPortController ThaClaPppPwEthPortControllerObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController ThaClaPppPwEthPortControllerPppEthPortController(ThaClaPppPwEthPortController self)
    {
    if (self->pppEthPortController == NULL)
        self->pppEthPortController = mMethodsGet(self)->PppEthPortControllerCreate(self);
    return self->pppEthPortController;
    }

ThaClaEthPortController ThaClaPppPwEthPortControllerPwEthPortController(ThaClaPppPwEthPortController self)
    {
    if (self->pwEthPortController == NULL)
        self->pwEthPortController = mMethodsGet(self)->PwEthPortControllerCreate(self);
    return self->pwEthPortController;
    }

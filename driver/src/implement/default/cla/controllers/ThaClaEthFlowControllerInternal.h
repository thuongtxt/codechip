/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaClaEthFlowControllerInternal.h
 * 
 * Created Date: Nov 26, 2013
 *
 * Description : CLA Ethernet flow controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THACLAETHFLOWCONTROLLERINTERNAL_H_
#define _THACLAETHFLOWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "ThaClaEthFlowController.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaClaEthFlowControllerMethods
    {
    uint8 (*VlanLookupModeGet)(ThaClaEthFlowController self, ThaEthFlow flow);
    eAtRet (*VlanLookupCtrl)(ThaClaEthFlowController self, ThaEthFlow flow, uint16 lkType, uint32 channelId, uint32 vlanId, eBool enable);
    eAtRet (*FlowLookupCtrl)(ThaClaEthFlowController self, ThaEthFlow flow, uint32 hwFlowId, uint32 classId, uint32 queueId, uint32 vlanOffSet, eBool enable);
    uint32 (*MaxNumVlans)(ThaClaEthFlowController self);
    eAtRet (*PsnToTdmEthTypeSet)(ThaClaEthFlowController self, uint32 entryIndex, uint32 ethType);
    uint32 (*PsnToTdmEthTypeGet)(ThaClaEthFlowController self, uint32 entryIndex);
    eAtRet (*AllTrafficsDeactivate)(ThaClaEthFlowController self);
    uint32 (*PidEntryEnableMask)(ThaClaEthFlowController self, uint32 entryIndex, eBool enable);
    uint8 (*FcsRemoved)(ThaClaEthFlowController self);
    uint8 (*VlanRemoved)(ThaClaEthFlowController self);
    uint32 (*PartOffset)(ThaClaEthFlowController self, ThaEthFlow flow);
    uint16 (*VlanLookupOffset)(ThaClaEthFlowController self, uint16 lkType);
    uint8 (*QueueHwPriority)(ThaClaEthFlowController self, uint8 queueId);
    eBool (*NeedInitializeQueuePriority)(ThaClaEthFlowController self);
    }tThaClaEthFlowControllerMethods;

typedef struct tThaClaEthFlowController
    {
    tThaClaController super;
    const tThaClaEthFlowControllerMethods *methods;

    /* Private data */
    }tThaClaEthFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthFlowController ThaClaEthFlowControllerObjectInit(ThaClaEthFlowController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THACLAETHFLOWCONTROLLERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaClaReg.h
 *
 * Created Date: Aug 13, 2012
 *
 * Description : CLA for PPP products
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef THAMODULECLAPPPREG_H_
#define THAMODULECLAPPPREG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaModuleClaReg.h"

/*------------------------------------------------------------------------------
Reg Name: CLA FLOW Lookup control
Reg Addr: 0x44E000
          The address format for these registers is 0x44E000 + BundID*8 + PRICode
          Where:
           BundID (0-1023): Value BundID
           PRICode (0-7): is  Priority Code Point of Outer VLAN ID
Description: The register provides flow id base on BundID and PRIcode
------------------------------------------------------------------------------*/
#define cThaClaFlowLookupCtrl 0x44E000

/*--------------------------------------
BitField Name: ClaBundLkMpFlowEn
BitField Type: R/W
BitField Desc: MpFlow Enable
--------------------------------------*/
#define cThaClaBundLkMpFlowEnMaks                          cBit20
#define cThaClaBundLkMpFlowEnShift                         20

#define cThaClaBundLkMpQueueIdMask                         cBit19_16
#define cThaClaBundLkMpQueueIdShift                        16

/*--------------------------------------
BitField Name: ClaBundLkMpClass
BitField Type: R/W
BitField Desc: Class ID insert to MLPPP header
--------------------------------------*/
#define cThaClaBundLkMpFlowClassMaks                       cBit15_12
#define cThaClaBundLkMpFlowClassShift                      12

/*--------------------------------------
BitField Name: ClaBundLkMpFlowId
BitField Type: R/W
BitField Desc: MpFlowID, also class on MLPPP bundle
--------------------------------------*/
#define cThaClaBundLkMpFlowIdMask                          cBit10_0
#define cThaClaBundLkMpFlowIdShift                         0

/* NEW CLA FOR MLPPP */
/*------------------------------------------------------------------------------
Reg Name: CLA VLAN Lookup control
Reg Addr: 0x44D000
          The address format for these registers is 0x44D000 + VLANID
          Where: VLANID (0-4095): Value Outer VLAN ID
Reg Desc: The register provides connection ID base on outer VLAN
------------------------------------------------------------------------------*/
#define cThaClaVlanLookupCtrl   0x44D000

#define cThaClaVlanLkCIDMask  cBit9_0
#define cThaClaVlanLkCIDShift 0

#define cThaClaVlanLkTypeMask  cBit14_12
#define cThaClaVlanLkTypeShift 12

#define cThaClaVlanLkEnMaks    cBit16
#define cThaClaVlanLkEnShift   16

#define cThaClaVlanLookupCtrlNoneEditableFieldsMask (cBit31_17 | cBit11_10)

/*----------------------------------------------------------------------------
Reg Name: CLA ETH-TYPE to PPP-PID Control
Reg Addr: 0x44C030
          The address format for these register is 0x44C030 + PPP_TYPE
          where:
              PPP_TYPE (0-5): Value to Insert PPP_PID of MPEG Block, value 0 is lowest priority
Description: The register provides values Ethernet Type to get values PPP_PID of MPEG block.
------------------------------------------------------------------------------*/
#define cThaClaEthType2PppPidCtrl  0x44C030

#define cThaClaEthTypeValueMask    cBit15_0
#define cThaClaEthTypeValueShift   0

#define cThaClaEthTypeMaskMask     cBit31_16
#define cThaClaEthTypeMaskShift    16

/*-----------------------------------------------------------------------------
Reg Name: CLA Operation Control
Reg Addr: 0x44C020
          The address format for these register is 0x44C020

Description: The register provides global Operation control.
------------------------------------------------------------------------------*/
#define cThaClaOperationCtrl   0x44C020

#define cThaClaPktModeMask   cBit3_0
#define cThaClaPktModeShift  0

#define cThaClaFcsRmvMask    cBit4
#define cThaClaFcsRmvShift   4

#define cThaClaVlanRmvMask   cBit5
#define cThaClaVlanRmvShift  5

#define cThaClaVlanLkModeMask   cBit6
#define cThaClaVlanLkModeShift  6

#define cThaClaOperationCtrlNoneEditableFieldsMask (cBit31_17 | cBit11_0)

#define cThaDebugClaSticky0           0x0044C050

/*------------------------------------------------------------------------------
Reg Name: CLA Packet Length Control
Reg Addr:Address: 0x44C021
         The address format for these registers is 0x44C021
Reg Desc: The register configure max/min packet size at ethernet
------------------------------------------------------------------------------*/
#define cThaClaPacketLengthCtrl   0x44C021

#define cThaClaPktLenMaxMask  cBit29_16
#define cThaClaPktLenMaxShift 16

#define cThaClaPktLenMinMask  cBit5_0
#define cThaClaPktLenMinShift 0

/*------------------------------------------------------------------------------
Reg Name: CLA Priority VLAN Control
Reg Addr:Address: 0x44C023
         The address format for these registers is 0x44C023
Description: The register provides queue identification per priority of VLAN
------------------------------------------------------------------------------*/
#define cThaClaPriorityVlanCtrl           0x44C023

#define cThaClaQueueVlanPriMask(queueId)  (cBit2_0 << ((queueId) * 3))
#define cThaClaQueueVlanPriShift(queueId) ((queueId) * 3UL)

#endif /* THAMODULECLAPPPREG_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : ThaModuleClaRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : CLA register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "AtModulePpp.h"

#include "../../../../util/AtIteratorInternal.h"
#include "../../../../generic/memtest/AtModuleRegisterIteratorInternal.h"
#include "../ThaModuleCla.h"

#include "ThaModuleClaPppReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaModuleClaRegisterIterator * ThaModuleClaRegisterIterator;

typedef struct tThaModuleClaRegisterIterator
    {
    tAtModuleRegisterIterator super;

    /* Private data */
    uint32 currentVlan;
    uint16 bundleId;
    uint8 priority;
    }tThaModuleClaRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleClaRegisterIterator);
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    uint32 baseRegister = AtModuleRegisterIteratorCurrentBaseAddressGet(self);
    ThaModuleClaRegisterIterator iterator = (ThaModuleClaRegisterIterator)self;

    if (baseRegister == cThaClaVlanLookupCtrl)
        {
        iterator->currentVlan = iterator->currentVlan + 1;
        return iterator->currentVlan;
        }

    if (baseRegister == cThaClaFlowLookupCtrl)
        {
        static const uint32 cMaxNumVlans = 8;

        iterator->priority = (uint8)(iterator->priority + 1);
        if (iterator->priority == cMaxNumVlans)
            {
            iterator->priority = 0;
            iterator->bundleId = (uint8)(iterator->bundleId + 1);
            }

        return (iterator->bundleId * cMaxNumVlans) + iterator->priority;
        }

    /* Let super determine */
    return m_AtModuleRegisterIteratorMethods->NextOffset(self);
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    uint32 baseRegister = AtModuleRegisterIteratorCurrentBaseAddressGet(self);
    ThaModuleClaRegisterIterator iterator = (ThaModuleClaRegisterIterator)self;
    ThaModuleCla claModule = (ThaModuleCla)AtModuleRegisterIteratorModuleGet((AtModuleRegisterIterator)self);

    if (baseRegister == cThaClaVlanLookupCtrl)
        return (iterator->currentVlan >= ThaModuleClaPppMaxNumVlans(claModule)) ? cAtTrue : cAtFalse;

    if (baseRegister == cThaClaFlowLookupCtrl)
        {
        AtModulePpp pppModule = (AtModulePpp)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)claModule), cAtModulePpp);
        static const uint8 cMaxNumVlans = 8;

        if ((iterator->bundleId == AtModulePppMaxBundlesGet(pppModule)) &&
            (iterator->priority == cMaxNumVlans))
            return cAtTrue;

        return cAtFalse;
        }

    /* Let the super determine */
    return m_AtModuleRegisterIteratorMethods->NoChannelLeft(self);
    }

static AtObject NextGet(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    mNextReg(iterator, 0, cThaClaOperationCtrl);
    mNextReg(iterator, cThaClaOperationCtrl, cThaClaVlanLookupCtrl);
    mNextChannelRegister(iterator, cThaClaVlanLookupCtrl, cAtModuleRegisterIteratorEnd);

    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NoChannelLeft);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

static AtIterator ThaModuleClaPppRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;
    
    return self;
    }

AtIterator ThaModuleClaPppRegisterIteratorCreate(AtModule module)
    {
    return ThaModuleClaPppRegisterIteratorObjectInit(AtOsalMemAlloc(ObjectSize()), module);
    }

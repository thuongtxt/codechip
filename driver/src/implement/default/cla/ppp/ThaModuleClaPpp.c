/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA (internal)
 *
 * File        : ThaModuleCla.c
 *
 * Created Date: Sep 10, 2012
 *
 * Description : CLA internal module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePpp.h"
#include "../../eth/ThaEthPort.h"
#include "ThaModuleClaPppInternal.h"
#include "ThaModuleClaPppReg.h"
#include "../controllers/ThaClaEthFlowController.h"
#include "../controllers/ThaClaEthPortController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods     m_AtModuleOverride;
static tThaModuleClaMethods m_ThaModuleClaOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtIterator RegisterIteratorCreate(AtModule self)
    {
    return ThaModuleClaPppRegisterIteratorCreate(self);
    }

static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return ThaClaPppEthPortControllerNew(self);
    }

static ThaClaEthFlowController EthFlowControllerCreate(ThaModuleCla self)
    {
    return ThaClaEthFlowControllerNew(self);
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x440000, 0x440001, 0x440002};
	AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 3;

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static eAtRet AllTrafficsDeactivate(ThaModuleCla self)
    {
    ThaClaEthFlowController ethFlowController = ThaModuleClaEthFlowControllerGet(self);
    return ThaClaEthFlowControllerAllTrafficsDeactivate(ethFlowController);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, EthFlowControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, AllTrafficsDeactivate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, RegisterIteratorCreate);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaModuleClaPpp);
    }

AtModule ThaModuleClaPppObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule ThaModuleClaPppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ThaModuleClaPppObjectInit(newModule, device);
    }

eAtRet ThaModuleClaPppPsnToTdmEthTypeSet(ThaModuleCla self, uint32 entryIndex, uint32 ethType)
    {
    ThaClaEthFlowController ethFlowController = ThaModuleClaEthFlowControllerGet(self);
    return ThaClaEthFlowControllerPsnToTdmEthTypeSet(ethFlowController, entryIndex, ethType);
    }

uint32 ThaModuleClaPppPsnToTdmEthTypeGet(ThaModuleCla self, uint32 entryIndex)
    {
    ThaClaEthFlowController ethFlowController = ThaModuleClaEthFlowControllerGet(self);
    return ThaClaEthFlowControllerPsnToTdmEthTypeGet(ethFlowController, entryIndex);
    }

uint32 ThaModuleClaPppMaxNumVlans(ThaModuleCla self)
    {
    ThaClaEthFlowController ethFlowController = ThaModuleClaEthFlowControllerGet(self);
    return ThaClaEthFlowControllerMaxNumVlans(ethFlowController);
    }

eAtRet ThaModuleClaFlowLookupCtrl(ThaModuleCla self, ThaEthFlow flow, uint32 hwFlowId, uint32 classId, uint32 queueId, uint32 vlanOffSet, eBool enable)
    {
    ThaClaEthFlowController ethFlowController = ThaModuleClaEthFlowControllerGet(self);
    return ThaClaEthFlowControllerFlowLookupCtrl(ethFlowController, flow, hwFlowId, classId, queueId, vlanOffSet, enable);
    }

eAtRet ThaModuleClaPppVlanLookupCtrl(ThaModuleCla self, ThaEthFlow flow, uint16 lkType, uint32 channelId, uint32 vlanId, eBool  enable)
    {
    ThaClaEthFlowController ethFlowController = ThaModuleClaEthFlowControllerGet(self);
    return ThaClaEthFlowControllerVlanLookupCtrl(ethFlowController, flow, lkType, channelId, vlanId, enable);
    }

uint8 ThaModuleClaPppVlanLookupModeGet(ThaModuleCla self, ThaEthFlow flow)
    {
    ThaClaEthFlowController ethFlowController = ThaModuleClaEthFlowControllerGet(self);
    return ThaClaEthFlowControllerVlanLookupModeGet(ethFlowController, flow);
    }

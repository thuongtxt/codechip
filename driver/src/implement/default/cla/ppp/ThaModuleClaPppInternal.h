/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : ThaModuleClaPppInternal.h
 * 
 * Created Date: Aug 21, 2013
 *
 * Description : CLA module of PPP product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAMODULECLAPPPINTERNAL_H_
#define _THAMODULECLAPPPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../ThaModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaModuleClaPpp
    {
    tThaModuleCla super;
    }tThaModuleClaPpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule ThaModuleClaPppObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULECLAPPPINTERNAL_H_ */


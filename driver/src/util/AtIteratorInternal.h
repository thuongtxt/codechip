/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtInteratorInternal.h
 * 
 * Created Date: Oct 8, 2012
 *
 * Author      : nguyennt
 * 
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATINTERATORINTERNAL_H_
#define _ATINTERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIterator.h"
#include "AtList.h"

#include "../generic/common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of AT iterator */
typedef struct tAtIteratorMethods
    {
    AtObject (*NextGet)(AtIterator self);
    AtObject (*Remove)(AtIterator self);
    uint32 (*Count)(AtIterator self);
    void (*Restart)(AtIterator self);
    }tAtIteratorMethods;

/* AT Iterator object */
typedef struct tAtIterator
    {
    tAtObject super;
    tAtIteratorMethods *methods;
    }tAtIterator;

/* AT List Iterator object */
typedef struct tAtListIterator
    {
    tAtIterator super;

    AtList list;
    int32 index;
    void*  current;
    char started;
    }tAtListIterator;

/* AT List Iterator class */
typedef struct tAtListIterator * AtListIterator;

/* AT Array Iterator class */
typedef struct tAtArrayIterator * AtArrayIterator;

typedef struct tAtArrayIteratorMethods
    {
    eBool (*DataAtIndexIsValid)(AtArrayIterator self, uint32 objectIndex);
    AtObject (*DataAtIndex)(AtArrayIterator self, uint32 objectIndex);
    }tAtArrayIteratorMethods;

/* AT Array Iterator object */
typedef struct tAtArrayIterator
    {
    tAtIterator super;
    const tAtArrayIteratorMethods *methods;

    void *array;
    int32 currentIndex;
    uint32 capacity;
    int32 count;
    }tAtArrayIterator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIterator AtIteratorObjectInit(AtIterator self);
AtArrayIterator AtArrayIteratorNew(void *array, uint32 capacity);

#ifdef __cplusplus
}
#endif
#endif /* _ATINTERATORINTERNAL_H_ */


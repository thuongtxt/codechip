/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtDefaultCoder.c
 *
 * Created Date: May 26, 2015
 *
 * Description : Encoder/decoder default implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCoderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtDefaultCoder *)self)
#define mFile(self) AtDefaultCoderOutputFileGet((AtDefaultCoder)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDefaultCoder
    {
    tAtCoder super;

    /* Private data */
    uint32 currentLevel;
    AtFile outFile;
    }tAtDefaultCoder;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtCoderMethods m_AtCoderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSpacesPerLevel(AtCoder self)
    {
    AtUnused(self);
    return 4;
    }

static void LevelPrint(AtCoder self, uint32 level)
    {
    uint32 i;
    uint32 numSpaces = level * NumSpacesPerLevel(self);
    AtFile file = AtDefaultCoderOutputFileGet((AtDefaultCoder)self);

    for (i = 0; i < numSpaces; i++)
        AtFilePrintf(file, " ");
    }

static void EncodeUInt(AtCoder self, uint32 value, const char *key)
    {
    LevelPrint(self, mThis(self)->currentLevel);

    AtFilePrintf(mFile(self), "\"%s\": %u,\r\n", key, value);
    }

static void EncodeUInt64(AtCoder self, uint64 value, const char *key)
    {
    LevelPrint(self, mThis(self)->currentLevel);

    AtFilePrintf(mFile(self), "\"%s\": %llu,\r\n", key, value);
    }

static uint32 UInt8ArrayValueAtIndex(AtCoder self, void *values, uint32 valueIndex)
    {
    uint8 *array = (uint8 *)values;
    AtUnused(self);
    return array[valueIndex];
    }

static uint32 UInt16ArrayValueAtIndex(AtCoder self, void *values, uint32 valueIndex)
    {
    uint16 *array = (uint16 *)values;
    AtUnused(self);
    return array[valueIndex];
    }

static uint32 BoolArrayValueAtIndex(AtCoder self, void *values, uint32 valueIndex)
    {
    eBool *array = (eBool *)values;
    AtUnused(self);
    return array[valueIndex] ? 1 : 0;
    }

static uint32 UInt32ArrayValueAtIndex(AtCoder self, void *values, uint32 valueIndex)
    {
    uint32 *array = (uint32 *)values;
    AtUnused(self);
    return array[valueIndex];
    }

static void EncodeUIntArray(AtCoder self, void *values, uint32 size, const char *key,
                            uint32 (*ValueAtIndex)(AtCoder self, void *values, uint32 valueIndex))
    {
    uint32 i;

    LevelPrint(self, mThis(self)->currentLevel);

    if ((values == NULL) || (size == 0))
        {
        AtFilePrintf(mFile(self), "\"%s\": \"none\",\r\n", key);
        return;
        }

    AtFilePrintf(mFile(self), "\"%s\": [", key);

    for (i = 0; i < size; i++)
        {
        if (i > 0)
            AtFilePrintf(mFile(self), ",");
        AtFilePrintf(mFile(self), "%u", ValueAtIndex(self, values, i));
        }

    AtFilePrintf(mFile(self), "%s\r\n", "],");
    }

static void EncodeUInt8Array(AtCoder self, uint8 *values, uint32 size, const char *key)
    {
    EncodeUIntArray(self, values, size, key, UInt8ArrayValueAtIndex);
    }

static void EncodeUInt32Array(AtCoder self, uint32 *values, uint32 size, const char *key)
    {
    EncodeUIntArray(self, values, size, key, UInt32ArrayValueAtIndex);
    }

static void EncodeUInt16Array(AtCoder self, uint16 *values, uint32 size, const char *key)
    {
    EncodeUIntArray(self, values, size, key, UInt16ArrayValueAtIndex);
    }

static void EncodeBoolArray(AtCoder self, eBool *values, uint32 size, const char *key)
    {
    EncodeUIntArray(self, values, size, key, BoolArrayValueAtIndex);
    }

static void EncodeUInt64Array(AtCoder self, uint64 *values, uint32 size, const char *key)
    {
    uint32 i;

    LevelPrint(self, mThis(self)->currentLevel);

    if ((values == NULL) || (size == 0))
        {
        AtFilePrintf(mFile(self), "\"%s\": \"none\",\r\n", key);
        return;
        }

    AtFilePrintf(mFile(self), "\"%s\": [", key);

    for (i = 0; i < size; i++)
        {
        if (i > 0)
            AtFilePrintf(mFile(self), ",");
        AtFilePrintf(mFile(self), "%llu", values[i]);
        }

    AtFilePrintf(mFile(self), "%s\r\n", "],");
    }

static void EncodeObjectWithHandler(AtCoder self, AtObject object, const char *key, AtCoderSerializeHandler handler)
    {
    LevelPrint(self, mThis(self)->currentLevel);

    if (key == NULL)
        AtFilePrintf(mFile(self), "{\r\n");
    else
        AtFilePrintf(mFile(self), "\"%s\": {\r\n", key);

    mThis(self)->currentLevel = mThis(self)->currentLevel + 1;
    handler(object, self);
    mThis(self)->currentLevel = mThis(self)->currentLevel - 1;

    LevelPrint(self, mThis(self)->currentLevel);
    AtFilePrintf(mFile(self), "%s\r\n", "\"none\":\"none\"");

    LevelPrint(self, mThis(self)->currentLevel);
    AtFilePrintf(mFile(self), "%s\r\n", key ? "}," : "}");
    }

static void EncodeString(AtCoder self, const char *string, const char *key)
    {
    LevelPrint(self, mThis(self)->currentLevel);
    AtFilePrintf(mFile(self), "\"%s\": \"%s\",\r\n", key, string ? string : "none");
    }

static void EncodeObjectsWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                      AtCoderIndexHandler indexHandler,
                                      const char *key,
                                      AtCoderSerializeHandler serializeHandler)
    {
    uint32 object_i;

    LevelPrint(self, mThis(self)->currentLevel);

    if (objects == NULL)
        {
        AtFilePrintf(mFile(self), "\"%s\": \"none\",\r\n", key);
        return;
        }

    AtFilePrintf(mFile(self), "\"%s\": [\r\n", key);

    mThis(self)->currentLevel = mThis(self)->currentLevel + 1;
    for (object_i = 0; object_i < numObjects; object_i++)
        {
        AtObject object = indexHandler(self, objects, object_i);

        if (object_i > 0)
            {
            LevelPrint(self, mThis(self)->currentLevel);
            AtFilePrintf(mFile(self), "%s\r\n", ",");
            }

        LevelPrint(self, mThis(self)->currentLevel);
        AtFilePrintf(mFile(self), "%s\r\n", "{");

        serializeHandler(object, self);

        LevelPrint(self, mThis(self)->currentLevel);
        AtFilePrintf(mFile(self), "%s\r\n", "}");
        }

    mThis(self)->currentLevel = mThis(self)->currentLevel - 1;

    LevelPrint(self, mThis(self)->currentLevel);
    AtFilePrintf(mFile(self), "],\r\n");
    }

static void EncodeObjectsDescriptionWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                                 AtCoderIndexHandler indexHandler,
                                                 const char *key,
                                                 AtCoderObjectToStringHandler object2String)
    {
    uint32 object_i;

    LevelPrint(self, mThis(self)->currentLevel);

    if ((objects == NULL) || (numObjects == 0))
        {
        AtFilePrintf(mFile(self), "\"%s\": \"none\",\r\n", key);
        return;
        }

    AtFilePrintf(mFile(self), "\"%s\": [\r\n", key);

    mThis(self)->currentLevel = mThis(self)->currentLevel + 1;
    for (object_i = 0; object_i < numObjects; object_i++)
        {
        AtObject object = indexHandler(self, objects, object_i);
        const char *objectDesc;

        if (object_i > 0)
            AtFilePrintf(mFile(self), "%s\r\n", ",");

        LevelPrint(self, mThis(self)->currentLevel);
        objectDesc = object ? object2String(object) : "none";
        AtFilePrintf(mFile(self), "%s", objectDesc);
        }

    mThis(self)->currentLevel = mThis(self)->currentLevel - 1;

    LevelPrint(self, mThis(self)->currentLevel);
    AtFilePrintf(mFile(self), "],\r\n");
    }

static uint32 UInt32ListValueAtIndex(AtCoder self, void *values, uint32 valueIndex)
    {
    AtList list = values;
    void *object = AtListObjectGet(list, valueIndex);
    AtUnused(self);
    return (uint32)(size_t)object;
    }

static void EncodeUInt32List(AtCoder self, AtList list, const char *key)
    {
    EncodeUIntArray(self, list, AtListLengthGet(list), key, UInt32ListValueAtIndex);
    }

static AtFile OutputFileGet(AtDefaultCoder self)
    {
    return self->outFile;
    }

static void OverrideAtCoder(AtCoder self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtCoderOverride, mMethodsGet(self), sizeof(m_AtCoderOverride));

        mMethodOverride(m_AtCoderOverride, EncodeUInt);
        mMethodOverride(m_AtCoderOverride, EncodeUInt64);
        mMethodOverride(m_AtCoderOverride, EncodeUInt8Array);
        mMethodOverride(m_AtCoderOverride, EncodeUInt16Array);
        mMethodOverride(m_AtCoderOverride, EncodeUInt32Array);
        mMethodOverride(m_AtCoderOverride, EncodeUInt16Array);
        mMethodOverride(m_AtCoderOverride, EncodeUInt64Array);
        mMethodOverride(m_AtCoderOverride, EncodeBoolArray);
        mMethodOverride(m_AtCoderOverride, EncodeString);
        mMethodOverride(m_AtCoderOverride, EncodeObjectWithHandler);
        mMethodOverride(m_AtCoderOverride, EncodeUInt32List);
        mMethodOverride(m_AtCoderOverride, EncodeObjectsWithHandlers);
        mMethodOverride(m_AtCoderOverride, EncodeObjectsDescriptionWithHandlers);
        }

    mMethodsSet(self, &m_AtCoderOverride);
    }

static void Override(AtCoder self)
    {
    OverrideAtCoder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultCoder);
    }

static AtCoder ObjectInit(AtCoder self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtCoderObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->outFile = AtStdStandardOutput(AtStdSharedStdGet());

    return self;
    }

AtCoder AtDefaultCoderNew(void)
    {
    AtCoder newCoder = AtOsalMemAlloc(ObjectSize());
    return ObjectInit(newCoder);
    }

void AtDefaultCoderOutputFileSet(AtDefaultCoder self, AtFile outputFile)
    {
    if (self)
        self->outFile = outputFile;
    }

AtFile AtDefaultCoderOutputFileGet(AtDefaultCoder self)
    {
    if (self)
        return OutputFileGet(self);
    return NULL;
    }

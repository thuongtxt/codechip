/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtCoderUtil.h
 * 
 * Created Date: Jun 1, 2015
 *
 * Description : Encoder/decoder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCODERUTIL_H_
#define _ATCODERUTIL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mEncodeUInt(fieldName) AtCoderEncodeUInt(encoder, object->fieldName, #fieldName)
#define mEncodeUInt64(fieldName) AtCoderEncodeUInt64(encoder, object->fieldName, #fieldName)
#define mEncodeString(fieldName) AtCoderEncodeString(encoder, object->fieldName, #fieldName)
#define mEncodeObject(fieldName) AtCoderEncodeObject(encoder, (AtObject)(object->fieldName), #fieldName)
#define mEncodeChannelIdString(fieldName) AtCoderEncodeString(encoder, AtChannelIdDescriptionBuild((AtChannel)object->fieldName), #fieldName)
#define mEncodeObjectDescription(fieldName) AtCoderEncodeString(encoder, AtObjectToString((AtObject)object->fieldName), #fieldName)
#define mEncodeObjectDescriptionArray(fieldName, numObjects) AtCoderEncodeObjectDescriptionInArray(encoder, (AtObject *)object->fieldName, numObjects, #fieldName)
#define mEncodeObjectDescriptionList(fieldName) AtCoderEncodeObjectDescriptionInList(encoder, object->fieldName, #fieldName)
#define mEncodeList(fieldName) AtCoderEncodeList(encoder, object->fieldName, #fieldName)
#define mEncodeObjects(fieldName, numObjects) AtCoderEncodeObjects(encoder, (AtObject *)object->fieldName, numObjects, #fieldName)
#define mEncodeObjectWithHandler(fieldName, handler) AtCoderEncodeObjectWithHandler(encoder, (AtObject)(object->fieldName), #fieldName, handler)
#define mEncodeListWithHandler(fieldName, handler) AtCoderEncodeListWithHandler(encoder, object->fieldName, #fieldName, handler)
#define mEncodeObjectsWithHandler(fieldName, numObjects, handler) AtCoderEncodeObjectsWithHandler(encoder, (AtObject *)object->fieldName, numObjects, #fieldName, handler)
#define mEncodeUInt64Array(fieldName, numValues) AtCoderEncodeUInt64Array(encoder, object->fieldName, numValues, #fieldName)
#define mEncodeUInt32Array(fieldName, numValues) AtCoderEncodeUInt32Array(encoder, object->fieldName, numValues, #fieldName)
#define mEncodeUInt16Array(fieldName, numValues) AtCoderEncodeUInt16Array(encoder, object->fieldName, numValues, #fieldName)
#define mEncodeUInt8Array(fieldName, numValues) AtCoderEncodeUInt8Array(encoder, object->fieldName, numValues, #fieldName)
#define mEncodeBoolArray(fieldName, numValues) AtCoderEncodeBoolArray(encoder, object->fieldName, numValues, #fieldName)

#define mEncodeNone(fieldName)

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATCODERUTIL_H_ */


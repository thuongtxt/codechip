/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtCoderInternal.h
 * 
 * Created Date: May 26, 2015
 *
 * Description : Encoder/decoder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCODERINTERNAL_H_
#define _ATCODERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "AtObject.h"
#include "AtCoder.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCoderMethods
    {
    void (*Delete)(AtCoder self);

    void (*EncodeUInt64)(AtCoder self, uint64 value, const char *key);
    void (*EncodeUInt)(AtCoder self, uint32 value, const char *key);
    void (*EncodeObject)(AtCoder self, AtObject object, const char *key);
    void (*EncodeObjectWithHandler)(AtCoder self, AtObject object, const char *key, AtCoderSerializeHandler handler);
    void (*EncodeString)(AtCoder self, const char *string, const char *key);
    void (*EncodeObjects)(AtCoder self, AtObject *objects, uint32 numObjects, const char *key);
    void (*EncodeObjectsWithHandler)(AtCoder self, AtObject *objects, uint32 numObjects, const char *key, AtCoderSerializeHandler handler);
    void (*EncodeList)(AtCoder self, AtList list, const char *key);
    void (*EncodeListWithHandler)(AtCoder self, AtList list, const char *key, AtCoderSerializeHandler handler);
    void (*EncodeUInt32List)(AtCoder self, AtList list, const char *key);
    void (*EncodeUInt64Array)(AtCoder self, uint64 *values, uint32 size, const char *key);
    void (*EncodeUInt32Array)(AtCoder self, uint32 *values, uint32 size, const char *key);
    void (*EncodeUInt16Array)(AtCoder self, uint16 *values, uint32 size, const char *key);
    void (*EncodeUInt8Array)(AtCoder self, uint8 *values, uint32 size, const char *key);
    void (*EncodeBoolArray)(AtCoder self, eBool *values, uint32 size, const char *key);
    void (*EncodeObjectDescriptionInList)(AtCoder self, AtList list, const char *key);
    void (*EncodeObjectDescriptionInArray)(AtCoder self, AtObject *objects, uint32 numObjects, const char *key);

    /* Flexible methods */
    void (*EncodeObjectsWithHandlers)(AtCoder self, void *objects, uint32 numObjects,
                                      AtCoderIndexHandler indexHandler,
                                      const char *key,
                                      AtCoderSerializeHandler serializeHandler);
    void (*EncodeObjectsDescriptionWithHandlers)(AtCoder self, void *objects, uint32 numObjects,
                                                 AtCoderIndexHandler indexHandler,
                                                 const char *key,
                                                 AtCoderObjectToStringHandler object2String);

    uint32 (*DecodeUInt)(AtCoder self, const char *key);
    AtObject (*DecodeObject)(AtCoder self, const char *key);
    const char *(*DecodeString)(AtCoder self, const char *key);
    uint32 (*DecodeObjects)(AtCoder self, AtObject *objects, const char *key);
    }tAtCoderMethods;

typedef struct tAtCoder
    {
    const tAtCoderMethods *methods;
    }tAtCoder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCoder AtCoderObjectInit(AtCoder self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCODERINTERNAL_H_ */

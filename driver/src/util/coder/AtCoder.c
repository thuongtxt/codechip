/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtCoder.c
 *
 * Created Date:
 *
 * Description : Encoder/decoder
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCoderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtCoderMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtCoder self)
    {
    AtOsalMemFree(self);
    }

static void EncodeUInt(AtCoder self, uint32 value, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(value);
    }

static void EncodeUInt64(AtCoder self, uint64 value, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(value);
    }

static void EncodeUInt8Array(AtCoder self, uint8 *values, uint32 size, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(values);
    AtUnused(size);
    }

static void EncodeUInt32Array(AtCoder self, uint32 *values, uint32 size, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(values);
    AtUnused(size);
    }

static void EncodeUInt16Array(AtCoder self, uint16 *values, uint32 size, const char *key)
    {
    AtUnused(self);
    AtUnused(values);
    AtUnused(size);
    AtUnused(key);
    }

static void EncodeUInt64Array(AtCoder self, uint64 *values, uint32 size, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(values);
    AtUnused(size);
    }

static void EncodeBoolArray(AtCoder self, eBool *values, uint32 size, const char *key)
    {
    AtUnused(self);
    AtUnused(values);
    AtUnused(size);
    AtUnused(key);
    }

static void EncodeObject(AtCoder self, AtObject object, const char *key)
    {
    AtCoderEncodeObjectWithHandler(self, object, key, AtObjectSerialize);
    }

static void EncodeObjectWithHandler(AtCoder self, AtObject object, const char *key, AtCoderSerializeHandler handler)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(object);
    AtUnused(handler);
    }

static void EncodeString(AtCoder self, const char *string, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(string);
    }

static void EncodeObjects(AtCoder self, AtObject *objects, uint32 numObjects, const char *key)
    {
    AtCoderEncodeObjectsWithHandler(self, objects, numObjects, key, AtObjectSerialize);
    }

static AtObject ArrayObjectAtIndex(AtCoder self, void *objects, uint32 objectIndex)
    {
    AtObject *objectArray = (AtObject *)objects;
    AtUnused(self);
    return objectArray[objectIndex];
    }

static void EncodeObjectsWithHandler(AtCoder self, AtObject *objects, uint32 numObjects, const char *key, AtCoderSerializeHandler handler)
    {
    AtCoderEncodeObjectsWithHandlers(self, objects, numObjects, ArrayObjectAtIndex, key, handler);
    }

static void EncodeObjectsWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                      AtCoderIndexHandler indexHandler,
                                      const char *key,
                                      AtCoderSerializeHandler serializeHandler)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(objects);
    AtUnused(indexHandler);
    AtUnused(numObjects);
    AtUnused(serializeHandler);
    }

static void EncodeObjectsDescriptionWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                                 AtCoderIndexHandler indexHandler,
                                                 const char *key,
                                                 AtCoderObjectToStringHandler object2String)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(objects);
    AtUnused(indexHandler);
    AtUnused(numObjects);
    AtUnused(object2String);
    }

static void EncodeList(AtCoder self, AtList list, const char *key)
    {
    AtCoderEncodeListWithHandler(self, list, key, AtObjectSerialize);
    }

static AtObject ListObjectAtIndex(AtCoder self, void *objects, uint32 objectIndex)
    {
    AtUnused(self);
    return AtListObjectGet((AtList)objects, objectIndex);
    }

static void EncodeListWithHandler(AtCoder self, AtList list, const char *key, AtCoderSerializeHandler handler)
    {
    AtCoderEncodeObjectsWithHandlers(self, list, AtListLengthGet(list), ListObjectAtIndex, key, handler);
    }

static void EncodeUInt32List(AtCoder self, AtList list, const char *key)
    {
    AtUnused(self);
    AtUnused(list);
    AtUnused(key);
    }

static uint32 DecodeUInt(AtCoder self, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    return 0;
    }

static AtObject DecodeObject(AtCoder self, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    return NULL;
    }

static const char *DecodeString(AtCoder self, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    return NULL;
    }

static uint32 DecodeObjects(AtCoder self, AtObject *objects, const char *key)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(objects);
    return 0;
    }

static void EncodeObjectDescriptionInList(AtCoder self, AtList list, const char *key)
    {
    AtCoderEncodeObjectsDescriptionWithHandlers(self, list, AtListLengthGet(list), ListObjectAtIndex, key, AtObjectToString);
    }

static void EncodeObjectDescriptionInArray(AtCoder self, AtObject *objects, uint32 numObjects, const char *key)
    {
    AtCoderEncodeObjectsDescriptionWithHandlers(self, objects, numObjects, ArrayObjectAtIndex, key, AtObjectToString);
    }

static void MethodsInit(AtCoder self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Delete);
        mMethodOverride(m_methods, EncodeUInt);
        mMethodOverride(m_methods, EncodeUInt64);
        mMethodOverride(m_methods, EncodeUInt8Array);
        mMethodOverride(m_methods, EncodeUInt16Array);
        mMethodOverride(m_methods, EncodeUInt32Array);
        mMethodOverride(m_methods, EncodeUInt16Array);
        mMethodOverride(m_methods, EncodeUInt64Array);
        mMethodOverride(m_methods, EncodeBoolArray);
        mMethodOverride(m_methods, EncodeObject);
        mMethodOverride(m_methods, EncodeObjectWithHandler);
        mMethodOverride(m_methods, EncodeString);
        mMethodOverride(m_methods, EncodeObjects);
        mMethodOverride(m_methods, EncodeObjectsWithHandler);
        mMethodOverride(m_methods, EncodeList);
        mMethodOverride(m_methods, EncodeListWithHandler);
        mMethodOverride(m_methods, EncodeUInt32List);
        mMethodOverride(m_methods, EncodeObjectDescriptionInList);
        mMethodOverride(m_methods, EncodeObjectDescriptionInArray);
        mMethodOverride(m_methods, EncodeObjectsWithHandlers);
        mMethodOverride(m_methods, EncodeObjectsDescriptionWithHandlers);
        mMethodOverride(m_methods, DecodeUInt);
        mMethodOverride(m_methods, DecodeObject);
        mMethodOverride(m_methods, DecodeString);
        mMethodOverride(m_methods, DecodeObjects);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtCoder);
    }

AtCoder AtCoderObjectInit(AtCoder self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtCoderEncodeUInt(AtCoder self, uint32 value, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeUInt(self, value, key);
    }

void AtCoderEncodeUInt64(AtCoder self, uint64 value, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeUInt64(self, value, key);
    }

void AtCoderEncodeUInt64Array(AtCoder self, uint64 *values, uint32 size, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeUInt64Array(self, values, size, key);
    }

void AtCoderEncodeUInt32Array(AtCoder self, uint32 *values, uint32 size, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeUInt32Array(self, values, size, key);
    }

void AtCoderEncodeUInt16Array(AtCoder self, uint16 *values, uint32 size, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeUInt16Array(self, values, size, key);
    }

void AtCoderEncodeUInt8Array(AtCoder self, uint8 *values, uint32 size, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeUInt8Array(self, values, size, key);
    }

void AtCoderEncodeBoolArray(AtCoder self, eBool *values, uint32 size, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeBoolArray(self, values, size, key);
    }

void AtCoderEncodeObject(AtCoder self, AtObject object, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeObject(self, object, key);
    }

void AtCoderEncodeObjectWithHandler(AtCoder self, AtObject object, const char *key, AtCoderSerializeHandler handler)
    {
    if (self)
        mMethodsGet(self)->EncodeObjectWithHandler(self, object, key, handler);
    }

void AtCoderEncodeObjectsWithHandler(AtCoder self, AtObject *objects, uint32 numObjects, const char *key, AtCoderSerializeHandler handler)
    {
    if (self)
        mMethodsGet(self)->EncodeObjectsWithHandler(self, objects, numObjects, key, handler);
    }

void AtCoderEncodeString(AtCoder self, const char *string, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeString(self, string, key);
    }

void AtCoderEncodeObjects(AtCoder self, AtObject *objects, uint32 numObjects, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeObjects(self, objects, numObjects, key);
    }

void AtCoderEncodeList(AtCoder self, AtList list, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeList(self, list, key);
    }

void AtCoderEncodeListWithHandler(AtCoder self, AtList list, const char *key, AtCoderSerializeHandler handler)
    {
    if (self)
        mMethodsGet(self)->EncodeListWithHandler(self, list, key, handler);
    }

void AtCoderEncodeUInt32List(AtCoder self, AtList list, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeUInt32List(self, list, key);
    }

void AtCoderEncodeObjectDescriptionInList(AtCoder self, AtList list, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeObjectDescriptionInList(self, list, key);
    }

void AtCoderEncodeObjectDescriptionInArray(AtCoder self, AtObject *objects, uint32 numObjects, const char *key)
    {
    if (self)
        mMethodsGet(self)->EncodeObjectDescriptionInArray(self, objects, numObjects, key);
    }

uint32 AtCoderDecodeUInt(AtCoder self, const char *key)
    {
    if (self)
        return mMethodsGet(self)->DecodeUInt(self, key);
    return 0;
    }

AtObject AtCoderDecodeObject(AtCoder self, const char *key)
    {
    if (self)
        return mMethodsGet(self)->DecodeObject(self, key);
    return NULL;
    }

const char *AtCoderDecodeString(AtCoder self, const char *key)
    {
    if (self)
        return mMethodsGet(self)->DecodeString(self, key);
    return NULL;
    }

uint32 AtCoderDecodeObjects(AtCoder self, AtObject *objects, const char *key)
    {
    if (self)
        return mMethodsGet(self)->DecodeObjects(self, objects, key);
    return 0;
    }

void AtCoderDelete(AtCoder self)
    {
    if (self)
        mMethodsGet(self)->Delete(self);
    }

void AtCoderEncodeObjectsWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                      AtCoderIndexHandler indexHandler,
                                      const char *key,
                                      AtCoderSerializeHandler serializeHandler)
    {
    if (self)
        mMethodsGet(self)->EncodeObjectsWithHandlers(self, objects, numObjects, indexHandler, key, serializeHandler);
    }

void AtCoderEncodeObjectsDescriptionWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                                 AtCoderIndexHandler indexHandler,
                                                 const char *key,
                                                 AtCoderObjectToStringHandler object2String)
    {
    if (self)
        mMethodsGet(self)->EncodeObjectsDescriptionWithHandlers(self, objects, numObjects, indexHandler, key, object2String);
    }


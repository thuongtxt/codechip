/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtArrayIterator.c
 *
 * Created Date: Oct 18, 2012
 *
 * Description : Array iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Encap channel Iterator initialize */
static char m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;

static tAtArrayIteratorMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Get next element, return null if there is no next element */
static AtObject NextGet(AtIterator self)
    {
    AtArrayIterator iterator = (AtArrayIterator)self;

    /* Search to the end of array or reach un-null object */
    while (iterator->currentIndex < (int32)iterator->capacity - 1)
        {
        iterator->currentIndex++;
        if (mMethodsGet(iterator)->DataAtIndexIsValid(iterator, (uint32)iterator->currentIndex))
            {
            return mMethodsGet(iterator)->DataAtIndex(iterator, (uint32)iterator->currentIndex);
            }
        }

    /* When current index is last member so there is no next element */
    return NULL;
    }

static eBool DataAtIndexIsValid(AtArrayIterator self, uint32 Index)
    {
	AtUnused(Index);
	AtUnused(self);
    return cAtTrue;
    }

static AtObject DataAtIndex(AtArrayIterator self, uint32 Index)
    {
	AtUnused(Index);
	AtUnused(self);
    return NULL;
    }

/* Delete current element */
static AtObject Remove(AtIterator self)
    {
	AtUnused(self);
    /* Do not allow to remove object in this case */
    return NULL;
    }

/* Count */
static uint32 Count(AtIterator self)
    {
	AtUnused(self);
    return 0;
    }

static void Init(AtArrayIterator self)
    {
    self->currentIndex = -1;
    self->count = -1;
    }

static void Restart(AtIterator self)
    {
    Init((AtArrayIterator)self);
    }

static void OverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtIteratorOverride, mMethodsGet(iterator), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        mMethodOverride(m_AtIteratorOverride, Remove);
        mMethodOverride(m_AtIteratorOverride, Count);
        mMethodOverride(m_AtIteratorOverride, Restart);
        }

    mMethodsSet(iterator, &m_AtIteratorOverride);
    }

static void MethodsInit(AtArrayIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DataAtIndexIsValid);
        mMethodOverride(m_methods, DataAtIndex);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtArrayIterator);
    }

static AtArrayIterator ObjectInit(AtArrayIterator self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtIteratorObjectInit((AtIterator)self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtIterator(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtArrayIterator AtArrayIteratorNew(void *array, uint32 capacity)
    {
    /* Allocate memory */
    AtArrayIterator newIterator = AtOsalMemAlloc(ObjectSize());

    if (newIterator == NULL)
        return NULL;

    /* Construct it */
    if (ObjectInit(newIterator) == NULL)
        return NULL;

    /* Init it */
    newIterator->capacity = capacity;
    Init(newIterator);
    newIterator->array = array;

    return newIterator;
    }

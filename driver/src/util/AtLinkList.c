/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtLinkList.c
 *
 * Created Date: Oct 8, 2012
 *
 * Description : Link list
 *
 * Notes       :
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtListInternal.h"
#include "../generic/common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* To override super methods (require if need to override some Methods) */
static tAtObjectMethods m_AtObjectOverride;
static tAtListMethods   m_AtListOverride;

static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtListMethods   *m_AtListMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void NodeDelete(tAtLinkListNode *node)
    {
    AtOsalMemFree(node);
    }

static void AllNodesDelete(AtList self, void (*ObjectHandler)(AtObject object))
    {
    tAtLinkListNode *next;
    tAtLinkListNode *node = ((AtLinkList)self)->head;
    while(node)
        {
        next = node->next;
        if (ObjectHandler)
            ObjectHandler(node->object);
        NodeDelete(node);
        node = next;
        }
    }

static void Delete(AtObject self)
    {
    AllNodesDelete((AtList)self, NULL);
    m_AtObjectMethods->Delete(self);
    }

static void DeleteWithObjectHandler(AtList self, void (*ObjectHandler)(AtObject object))
    {
    AllNodesDelete((AtList)self, ObjectHandler);
    m_AtListMethods->DeleteWithObjectHandler(self, ObjectHandler);
    }

static tAtLinkListNode * AtLinkListNodeAtIndex(AtLinkList linkList, uint32 listIndex)
    {
    uint32 i;
    tAtLinkListNode *node = NULL;
    if (AtListIndexIsValid((AtList)linkList, listIndex))
        {
        node = linkList->head;
        for (i = 0; i < (listIndex); i++)
            node = node->next;
        }

    return node;
    }

static tAtLinkListNode * AtLinkListNodeNew(AtObject object)
    {
    tAtLinkListNode *newNode;
    newNode = AtOsalMemAlloc(sizeof(tAtLinkListNode));
    if (newNode)
        {
        newNode->object   = object;
        newNode->next     = NULL;
        newNode->previous = NULL;
        }

    return newNode;
    }

static AtObject ObjectAtIndexGet(AtList self, uint32 listIndex)
    {
    tAtLinkListNode *node = AtLinkListNodeAtIndex((AtLinkList)self, listIndex);
    if (node)
        return node->object;

    return NULL;
    }

static AtObject RemoveNode(AtLinkList linkList, tAtLinkListNode *node)
    {
    AtObject removedObject = NULL;
    removedObject = node->object;

    /* Re-arrange list */
    if (linkList->head == node)
        linkList->head = node->next;
    if (linkList->tail == node)
        linkList->tail = node->previous;

    if (node->previous != NULL)
        node->previous->next = node->next;
    if (node->next != NULL)
        node->next->previous = node->previous;

    NodeDelete(node);
    ((AtList)linkList)->length--;
    return removedObject;
    }

static AtObject RemoveAtIndex(AtList self, uint32 position)
    {
    AtObject removedObject = NULL;
    AtLinkList linkList    = (AtLinkList)self;
    tAtLinkListNode *node  = AtLinkListNodeAtIndex((AtLinkList)self, position);

    if (node)
        removedObject = RemoveNode(linkList, node);

    return removedObject;
    }

static eAtRet AddObjectToEnd(AtList self, AtObject object)
    {
    AtLinkList linkList = (AtLinkList)self;
    tAtLinkListNode *newNode = AtLinkListNodeNew(object);
    eAtRet ret = cAtError;

    if(newNode)
        {
        if (linkList->head == NULL)
            linkList->head = newNode;

        if (linkList->tail)
            linkList->tail->next = newNode;

        newNode->previous = linkList->tail;
        linkList->tail = newNode;

        self->length++;
        ret = cAtOk;
        }

    return ret;
    }

static AtObject IteratorNextGet(AtList self, AtListIterator iterator)
    {
    AtObject anObject = NULL;
    AtLinkList linkList = (AtLinkList)self;
    if (iterator->started)
        {
        iterator->current = ((tAtLinkListNode *)(iterator->current))->next;
        if (iterator->current)
            iterator->index++;
        }
    else /* first time get */
        {
        iterator->current = linkList->head;
        iterator->index = 0;
        iterator->started = 1;
        }

    if (iterator->current)
        anObject = ((tAtLinkListNode *)(iterator->current))->object;
    return anObject;
    }

static AtObject IteratorRemove(AtList self, AtListIterator iterator)
    {
    AtObject removedObject = NULL;
    tAtLinkListNode *newCurrentNode = NULL;
    tAtLinkListNode *currentNode = (tAtLinkListNode*)iterator->current;

    if (currentNode)
        {
        if (currentNode->previous)
            newCurrentNode = currentNode->previous;
        else
            iterator->started = 0; /* Restart iterator when deleting the first node */

        removedObject = RemoveNode((AtLinkList)self, currentNode);
        iterator->current = newCurrentNode;
        }

    return removedObject;
    }

static void Flush(AtList self)
    {
    AtLinkList linkList = (AtLinkList)self;

    while (self->length > 0)
        RemoveNode(linkList, linkList->head);
    }

static void OverrideAtObject(AtLinkList self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtList(AtLinkList self)
    {
    AtList list = (AtList)self;

    if (!m_methodsInit)
        {
        m_AtListMethods = mMethodsGet(list);
        AtOsalMemCpy(&m_AtListOverride, mMethodsGet(list), sizeof(m_AtListOverride));
        mMethodOverride(m_AtListOverride, ObjectAtIndexGet);
        mMethodOverride(m_AtListOverride, RemoveAtIndex);
        mMethodOverride(m_AtListOverride, AddObjectToEnd);
        mMethodOverride(m_AtListOverride, IteratorNextGet);
        mMethodOverride(m_AtListOverride, IteratorRemove);
        mMethodOverride(m_AtListOverride, Flush);
        mMethodOverride(m_AtListOverride, DeleteWithObjectHandler);
        }

    mMethodsSet(list, &m_AtListOverride);
    }

static void Override(AtLinkList self)
    {
    OverrideAtList(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLinkList);
    }

static AtLinkList ObjectInit(AtLinkList self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtListObjectInit((AtList)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->head = NULL;
    self->tail = NULL;

    return self;
    }

AtLinkList AtLinkListNew(void)
    {
    AtLinkList newList = AtOsalMemAlloc(ObjectSize());
    if (newList == NULL)
        return NULL;

    return ObjectInit(newList);
    }

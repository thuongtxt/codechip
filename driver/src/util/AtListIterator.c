/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtListIterator.c
 *
 * Created Date: Oct 8, 2012
 *
 * Description : List iterator
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtIteratorInternal.h"
#include "AtListInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtListIterator)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Init */
static void Init(AtListIterator self, AtList list)
    {
    mThis(self)->list    = list;
    mThis(self)->index   = -1;
    mThis(self)->current = NULL;
    mThis(self)->started = 0;
    }

/* Get next element, return null if there is no next element */
static AtObject NextGet(AtIterator self)
    {
    AtListIterator listIterator = mThis(self);
    return mMethodsGet(listIterator->list)->IteratorNextGet(listIterator->list, listIterator);
    }

/* Delete current element */
static AtObject Remove(AtIterator self)
    {
    AtListIterator listIterator = mThis(self);
    return mMethodsGet(listIterator->list)->IteratorRemove(listIterator->list, listIterator);
    }

/* Count */
static uint32 Count(AtIterator self)
    {
    AtListIterator listIterator = mThis(self);
    return mMethodsGet(listIterator->list)->LengthGet(listIterator->list);
    }

static void Restart(AtIterator self)
    {
    Init(mThis(self), mThis(self)->list);
    }

static void OverrideAtIterator(AtListIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtIteratorOverride, mMethodsGet(iterator), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        mMethodOverride(m_AtIteratorOverride, Remove);
        mMethodOverride(m_AtIteratorOverride, Count);
        mMethodOverride(m_AtIteratorOverride, Restart);
        }

    mMethodsSet(iterator, &m_AtIteratorOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtListIterator);
    }

static AtListIterator ObjectInit(AtListIterator self)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtIteratorObjectInit((AtIterator)self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtIterator(self);
    m_methodsInit = 1;

    return self;
    }

AtListIterator AtListIteratorNew(AtList list)
    {
    AtListIterator newIterator = AtOsalMemAlloc(ObjectSize());
    if (newIterator == NULL)
        return NULL;

    ObjectInit(newIterator);
    Init(newIterator, list);

    return newIterator;
    }

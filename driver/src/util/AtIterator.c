/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtIterator.c
 *
 * Created Date: Oct 8, 2012
 *
 * Description : Iterator
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtIteratorMethods m_methods;
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    /* Delete private data */

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

/* Get next element, return null if there is no next element */
static AtObject NextGet(AtIterator self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

/* Delete current element */
static AtObject Remove(AtIterator self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

/* Count */
static uint32 Count(AtIterator self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static void Restart(AtIterator self)
    {
    AtUnused(self);
    }

static void MethodsInit(AtIterator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NextGet);
        mMethodOverride(m_methods, Remove);
        mMethodOverride(m_methods, Count);
        mMethodOverride(m_methods, Restart);
        }

    mMethodsGet(self) = &m_methods;
    }

static void OverrideAtObject(AtIterator self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

/* Constructor of AT iterator */
AtIterator AtIteratorObjectInit(AtIterator self)
    {
    AtOsalMemInit(self, 0, sizeof(tAtIterator));

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtObject(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtIterator
 * @{
 */
/**
 * To count number of objects
 *
 * @param self This iterator
 *
 * @return Number of objects
 */
uint32 AtIteratorCount(AtIterator self)
    {
    if (self)
        return mMethodsGet(self)->Count(self);

    return 0;
    }

/**
 * Get next object
 *
 * @param self This iterator
 *
 * @return Next object or NULL if no object
 */
AtObject AtIteratorNext(AtIterator self)
    {
    if (self)
        return mMethodsGet(self)->NextGet(self);

    return NULL;
    }

/**
 * Remove current object
 *
 * @param self This iterator
 *
 * @return Object that is removed out of collection
 */
AtObject AtIteratorRemove(AtIterator self)
    {
    if (self)
        return mMethodsGet(self)->Remove(self);

    return NULL;
    }

/**
 * Restart iterating
 *
 * @param self This iterator
 */
void AtIteratorRestart(AtIterator self)
    {
    if (self)
        mMethodsGet(self)->Restart(self);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtDictionaryInternal.h
 * 
 * Created Date: Jun 3, 2019
 *
 * Description : Dictionary data structure representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDICTIONARYINTERNAL_H_
#define _ATDICTIONARYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDictionary.h"
#include "../../generic/common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/* Default dictionary */
typedef struct tAtDefaultDictionary * AtDefaultDictionary;

/* Default dictionary element */
typedef struct tAtDictionaryEntry *AtDictionaryEntry;

/* Methods of AT Dictionary */
typedef struct tAtDictionaryMethods
    {
    eAtRet (*ObjectAdd)(AtDictionary self, const void *key, AtObject object);
    eAtRet (*ObjectRemove)(AtDictionary self, const void *key);
    AtObject (*ObjectGet)(AtDictionary self, const void *key);
    eBool (*HasKey)(AtDictionary self, const void *key);
    uint32 (*Count)(AtDictionary self);
    AtIterator (*KeyIteratorCreate)(AtDictionary self);
    void (*DeleteWithObjectHandler)(AtDictionary self, void (*ObjectHandler)(AtObject object));
    }tAtDictionaryMethods;

typedef struct tAtDictionary
    {
    tAtObject super;
    tAtDictionaryMethods *methods;
    }tAtDictionary;

typedef struct tAtDictionaryEntry
    {
    const void *key;
    AtObject object;
    }tAtDictionaryEntry;

typedef struct tAtDefaultDictionary
    {
    tAtDictionary super;

    /* Private data */
    uint32 order;
    AtList *hashLists; /* Array of hash link-lists is size of 2^order */
    }tAtDefaultDictionary;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDictionary AtDictionaryObjectInit(AtDictionary self);
AtDictionary AtDefaultDictionaryObjectInit(AtDictionary self, uint32 order);

#ifdef __cplusplus
}
#endif
#endif /* _ATDICTIONARYINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtDefaultDictionary.c
 *
 * Created Date: Jun 5, 2019
 *
 * Description : Default dictionary implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtListInternal.h"
#include "../coder/AtCoderUtil.h"
#include "AtDictionaryInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)     ((AtDefaultDictionary)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtDictionaryMethods     m_AtDictionaryOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods = NULL;
static const tAtDictionaryMethods   *m_AtDictionaryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumHashLists(AtDefaultDictionary self)
    {
    return 1U << self->order;
    }

static void AllHashListDelete(AtDefaultDictionary self, void (*ObjectHandler)(AtObject object))
    {
    uint32 numLists = NumHashLists(self);
    uint32 i;

    if (self->hashLists == NULL)
        return;

    for (i = 0; i < numLists; i++)
        {
        if (self->hashLists[i])
            {
            if (ObjectHandler)
                AtListDeleteWithObjectHandler(self->hashLists[i], ObjectHandler);
            else
                AtObjectDelete((AtObject)self->hashLists[i]);
            self->hashLists[i] = NULL;
            }
        }
    AtOsalMemFree(self->hashLists);
    self->hashLists = NULL;
    }

static AtList *AllHashListCreate(AtDefaultDictionary self)
    {
    uint32 memSize = sizeof(AtLinkList) * NumHashLists(self);

    if (self->hashLists)
        return self->hashLists;

    self->hashLists = AtOsalMemAlloc(memSize);
    if (self->hashLists == NULL)
        return NULL;

    AtOsalMemInit(self->hashLists, 0, memSize);
    return self->hashLists;
    }

static void Delete(AtObject self)
    {
    AllHashListDelete(mThis(self), NULL);

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtDefaultDictionary object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(order);
    mEncodeObjects(hashLists, NumHashLists(object));
    }

static uint32 HashFromKey(AtDefaultDictionary self, const void *key)
    {
    char keyString[sizeof(void*)];
    char *s = keyString;
    uint32 hash, i;

    AtOsalMemCpy(keyString, key, sizeof(void*));

    /* K&R version 2 hashing */
    for (i = 0, hash = 0; i < sizeof(void*); i++, s++)
        hash = (uint32)*s + 31U * hash;

    /* Truncate to fit hash list */
    return hash & ((1U << self->order) - 1U);
    }

static AtList ListFromKey(AtDictionary self, const void *key)
    {
    AtDefaultDictionary dict = mThis(self);
    uint32 listIndex = 0;
    AtList list;

    /* Generate hash only if there is more than one list. */
    if (dict->order)
        listIndex = HashFromKey(dict, key);

    list = dict->hashLists[listIndex];
    if (list)
        return list;

    list = (AtList)AtLinkListNew();
    dict->hashLists[listIndex] = list;

    return list;
    }

static AtDictionaryEntry EntryFindInList(AtList list, const void *key)
    {
    AtIterator iter;
    AtDictionaryEntry entry = NULL;

    iter = AtListIteratorCreate(list);
    while ((entry = (AtDictionaryEntry)AtIteratorNext(iter)) != NULL)
        {
        if (key == entry->key)
            break;
        }

    AtObjectDelete((AtObject)iter);

    return entry;
    }

static AtDictionaryEntry EntryFind(AtDictionary self, const void *key)
    {
    AtList list = ListFromKey(self, key);
    return (list) ? EntryFindInList(list, key) : NULL;
    }

static AtDictionaryEntry EntryNew(const void *key, AtObject object)
    {
    AtDictionaryEntry entry = AtOsalMemAlloc(sizeof(tAtDictionaryEntry));
    if (entry == NULL)
        return NULL;

    entry->key = key;
    entry->object = object;
    return entry;
    }

static void EntryDelete(AtDictionaryEntry entry)
    {
    AtOsalMemFree(entry);
    }

static eAtRet ObjectAdd(AtDictionary self, const void *key, AtObject object)
    {
    AtList list = ListFromKey(self, key);
    AtDictionaryEntry entry;

    if (list == NULL)
        return cAtErrorRsrcNoAvail;

    if (EntryFindInList(list, key))
        return cAtErrorDuplicatedEntries;

    entry = EntryNew(key, object);
    if (entry)
        return AtListObjectAdd(list, (AtObject)entry);

    return cAtErrorRsrcNoAvail;
    }

static eAtRet ObjectRemove(AtDictionary self, const void *key)
    {
    AtList list = ListFromKey(self, key);
    AtDictionaryEntry entry;

    if (list == NULL)
        return cAtOk;

    entry = EntryFindInList(list, key);
    if (entry)
        {
        AtListObjectRemove(list, (AtObject)entry);
        EntryDelete(entry);
        }

    return cAtOk;
    }

static AtObject ObjectGet(AtDictionary self, const void *key)
    {
    AtDictionaryEntry entry = EntryFind(self, key);
    return (entry) ? entry->object : NULL;
    }

static eBool HasKey(AtDictionary self, const void *key)
    {
    AtDictionaryEntry entry = EntryFind(self, key);
    return (entry) ? cAtTrue : cAtFalse;
    }

static uint32 Count(AtDictionary self)
    {
    AtDefaultDictionary dict = (AtDefaultDictionary)self;
    uint32 count = 0;
    uint32 numLists = NumHashLists(dict);
    uint32 i;

    if (dict->hashLists == NULL)
        return 0;

    for (i = 0; i < numLists; i++)
        {
        if (dict->hashLists[i])
            count += AtListLengthGet(dict->hashLists[i]);
        }

    return count;
    }

static AtIterator KeyIteratorCreate(AtDictionary self)
    {
    AtDefaultDictionary dict = (AtDefaultDictionary)self;
    AtList mergedList;
    AtIterator iter;
    uint32 numLists = NumHashLists(dict);
    uint32 i;

    if (dict->hashLists == NULL)
        return NULL;

    mergedList = AtListCreate(0);
    for (i = 0; i < numLists; i++)
        {
        if (dict->hashLists[i])
            AtListObjectsAddFromList(mergedList, dict->hashLists[i]);
        }

    iter = AtListIteratorCreate(mergedList);
    AtObjectDelete((AtObject)mergedList);

    return iter;
    }

static void DeleteWithObjectHandler(AtDictionary self, void (*ObjectHandler)(AtObject object))
    {
    AllHashListDelete((AtDefaultDictionary)self, ObjectHandler);
    m_AtDictionaryMethods->DeleteWithObjectHandler(self, ObjectHandler);
    }

static eAtRet AtDefaultDictionaryInit(AtDictionary self, uint32 order)
    {
    AtDefaultDictionary dict = (AtDefaultDictionary)self;

    if (order > 31)
        return cAtErrorOutOfRangParm;

    dict->order = order;
    if (dict->hashLists)
        AllHashListDelete(dict, NULL);

    if (!AllHashListCreate(dict))
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static void OverrideAtDictionary(AtDictionary self)
    {
    if (!m_methodsInit)
        {
        m_AtDictionaryMethods = mMethodsGet(self);
        AtOsalMemCpy(&m_AtDictionaryOverride, mMethodsGet(self), sizeof(m_AtDictionaryOverride));

        mMethodOverride(m_AtDictionaryOverride, ObjectAdd);
        mMethodOverride(m_AtDictionaryOverride, ObjectRemove);
        mMethodOverride(m_AtDictionaryOverride, ObjectGet);
        mMethodOverride(m_AtDictionaryOverride, HasKey);
        mMethodOverride(m_AtDictionaryOverride, Count);
        mMethodOverride(m_AtDictionaryOverride, KeyIteratorCreate);
        mMethodOverride(m_AtDictionaryOverride, DeleteWithObjectHandler);
        }

    mMethodsGet(self) = &m_AtDictionaryOverride;
    }

static void OverrideAtObject(AtDictionary self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtDictionary self)
    {
    OverrideAtDictionary(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultDictionary);
    }

AtDictionary AtDefaultDictionaryObjectInit(AtDictionary self, uint32 order)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtDictionaryObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    if (AtDefaultDictionaryInit(self, order) != cAtOk)
        return NULL;

    return self;
    }

AtDictionary AtDefaultDictionaryNew(uint32 order)
    {
    AtDictionary newDict = AtOsalMemAlloc(ObjectSize());
    if (newDict == NULL)
        return NULL;

    if (AtDefaultDictionaryObjectInit(newDict, order) == NULL)
        {
        AtObjectDelete((AtObject)newDict);
        return NULL;
        }

    return newDict;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtDictionary.c
 *
 * Created Date: Jun 3, 2019
 *
 * Description : Generic dictionary implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDictionaryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtDictionaryMethods m_methods;

static tAtObjectMethods m_AtObjectOverride;

static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    m_AtObjectMethods->Delete(self);
    }

static eAtRet ObjectAdd(AtDictionary self, const void *key, AtObject object)
    {
    AtUnused(self);
    AtUnused(key);
    AtUnused(object);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ObjectRemove(AtDictionary self, const void *key)
    {
    AtUnused(self);
    AtUnused(key);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static AtObject ObjectGet(AtDictionary self, const void *key)
    {
    AtUnused(self);
    AtUnused(key);
    /* Let concrete class do */
    return NULL;
    }

static eBool HasKey(AtDictionary self, const void *key)
    {
    AtUnused(self);
    AtUnused(key);
    /* Let concrete class do */
    return cAtFalse;
    }

static uint32 Count(AtDictionary self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static AtIterator KeyIteratorCreate(AtDictionary self)
    {
    AtUnused(self);
    return NULL;
    }

static void DeleteWithObjectHandler(AtDictionary self, void (*ObjectHandler)(AtObject object))
    {
    AtUnused(ObjectHandler);
    Delete((AtObject)self);
    }

static void MethodsInit(AtDictionary self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ObjectAdd);
        mMethodOverride(m_methods, ObjectRemove);
        mMethodOverride(m_methods, ObjectGet);
        mMethodOverride(m_methods, HasKey);
        mMethodOverride(m_methods, Count);
        mMethodOverride(m_methods, KeyIteratorCreate);
        mMethodOverride(m_methods, DeleteWithObjectHandler);
        }

    mMethodsGet(self) = &m_methods;
    }

static void OverrideAtObject(AtDictionary self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

AtDictionary AtDictionaryObjectInit(AtDictionary self)
    {
    AtOsalMemInit(self, 0, sizeof(tAtDictionary));

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtObject(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtDictionary
 * @{
 */

/**
 * To add an object into dictionary by its key string
 *
 * @param self This dictionary
 * @param key The key string
 * @param object The object to add
 *
 * @return AT return code
 */
eAtRet AtDictionaryObjectAdd(AtDictionary self, const void *key, AtObject object)
    {
    if (self)
        return mMethodsGet(self)->ObjectAdd(self, key, object);
    return cAtErrorNullPointer;
    }

/**
 * To remove an object from dictionary by matching key string
 *
 * @param self This dictionary
 * @param key The key string
 *
 * @return AT return code
 */
eAtRet AtDictionaryObjectRemove(AtDictionary self, const void *key)
    {
    if (self)
        return mMethodsGet(self)->ObjectRemove(self, key);
    return cAtErrorNullPointer;
    }

/**
 * To get an object from dictionary by matching key string
 *
 * @param self This dictionary
 * @param key The key string
 *
 * @return Object
 */
AtObject AtDictionaryObjectGet(AtDictionary self, const void *key)
    {
    if (self)
        return mMethodsGet(self)->ObjectGet(self, key);
    return NULL;
    }

/**
 * To check if a key is existed in dictionary or not
 *
 * @param self This dictionary
 * @param key The key string
 *
 * @return cAtTrue if key is existed
 * @return cAtFalse if key is not existed
 */
eBool AtDictionaryHasKey(AtDictionary self, const void *key)
    {
    if (self)
        return mMethodsGet(self)->HasKey(self, key);
    return cAtFalse;
    }

/**
 * To get number of objects in dictionary
 *
 * @param self This dictionary
 *
 * @return Number of objects
 */
uint32 AtDictionaryCount(AtDictionary self)
    {
    if (self)
        return mMethodsGet(self)->Count(self);
    return 0;
    }

/**
 * Create iterator to iterate all of keys
 *
 * @param self This dictionary
 *
 * @return Iterator keys
 */
AtIterator AtDictionaryKeyIteratorCreate(AtDictionary self)
    {
    if (self)
        return mMethodsGet(self)->KeyIteratorCreate(self);
    return NULL;
    }

/**
 * Delete dictionary and give outside chance to handle object before it is deleted
 *
 * @param self This dictionary
 * @param ObjectHandler Object handler
 */
void AtDictionaryDeleteWithObjectHandler(AtDictionary self, void (*ObjectHandler)(AtObject object))
    {
    if (self)
        mMethodsGet(self)->DeleteWithObjectHandler(self, ObjectHandler);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtListInternal.h
 * 
 * Created Date: Oct 5, 2012
 *
 * Author      : nguyennt
 * 
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLISTINTERNAL_H_
#define _ATLISTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIteratorInternal.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Linked list class */
typedef struct tAtLinkList * AtLinkList;

/* Limited list class */
typedef struct tAtLimitedList * AtLimitedList;

typedef struct tAtListMethods
    {
    /* Get length of a list */
    uint32 (*LengthGet)(AtList self);
    void (*DeleteWithObjectHandler)(AtList self, void (*ObjectHandler)(AtObject object));

    /* Get member at index */
    AtObject (*ObjectAtIndexGet)(AtList self, uint32 objectIndex);

    /* Append object to end */
    eAtRet (*AddObjectToEnd)(AtList self, AtObject object);

    /* Remove at position */
    AtObject (*RemoveAtIndex)(AtList self, uint32 position);

    /* Iterator next get */
    AtObject (*IteratorNextGet)(AtList self, AtListIterator iterator);

    /* Iterator current remove */
    AtObject (*IteratorRemove)(AtList self, AtListIterator iterator);

    /* Flush */
    void (*Flush)(AtList self);
    }tAtListMethods;

typedef struct tAtList
    {
    tAtObject super;
    const tAtListMethods *methods;

    /* Private data */
    uint32 length;
    uint32 capacity;
    AtIterator iterator;
    }tAtList;

typedef struct tAtLimitedList
    {
    /* Super */
    tAtList super;

    /* Object array */
    AtObject *objects;
    }tAtLimitedList;

typedef struct tAtLinkListNode
    {
    AtObject object;
    struct tAtLinkListNode *next;
    struct tAtLinkListNode *previous;
    }tAtLinkListNode;

typedef struct tAtLinkList
    {
    /* Super */
    tAtList super;

    tAtLinkListNode *head;
    tAtLinkListNode *tail;
    }tAtLinkList;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtList AtListObjectInit(AtList self);
AtLimitedList AtLimitedListNew(uint32 capacity);
AtLinkList AtLinkListNew(void);

AtListIterator AtListIteratorNew(AtList list);
uint8 AtListIndexIsValid(AtList self, uint32 listIndex);

#ifdef __cplusplus
}
#endif
#endif /* _ATLISTINTERNAL_H_ */


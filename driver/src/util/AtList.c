/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtList.c
 *
 * Created Date: Oct 5, 2012
 *
 * Description : List
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtListInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtListMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 ListIsValid(AtList self)
    {
    return (self != NULL);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)((AtList)self)->iterator);
    ((AtList)self)->iterator = NULL;
    m_AtObjectMethods->Delete(self);
    }

static uint32 LengthGet(AtList self)
    {
    return self->length;
    }

static AtObject ObjectAtIndexGet(AtList self, uint32 listIndex)
    {
	AtUnused(listIndex);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtObject RemoveAtIndex(AtList self, uint32 position)
    {
	AtUnused(position);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtRet AddObjectToEnd(AtList self, AtObject object)
    {
	AtUnused(object);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

/* Iterator next get */
static AtObject IteratorNextGet(AtList self, AtListIterator iterator)
    {
	AtUnused(iterator);
	AtUnused(self);
    return NULL;
    }

/* Iterator current remove */
static AtObject IteratorRemove(AtList self, AtListIterator iterator)
    {
	AtUnused(iterator);
	AtUnused(self);
    return NULL;
    }

static void Flush(AtList self)
    {
	AtUnused(self);
    return;
    }

static uint32 ObjectsAddFromList(AtList self, AtList list)
    {
    uint32 numObjects = 0;
    AtIterator iterator = AtListIteratorCreate(list);
    AtObject object;

    while ((object = AtIteratorNext(iterator)) != NULL)
        {
        if (AtListObjectAdd(self, object) == cAtOk)
            numObjects = numObjects + 1;
        else
            break;
        }

    AtObjectDelete((AtObject)iterator);

    return numObjects;
    }

static AtIterator IteratorGet(AtList self)
    {
    if (self->iterator == NULL)
        self->iterator = AtListIteratorCreate(self);
    return self->iterator;
    }

static void DeleteWithObjectHandler(AtList self, void (*ObjectHandler)(AtObject object))
    {
    AtUnused(ObjectHandler);
    Delete((AtObject)self);
    }

static void MethodsInit(AtList self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LengthGet);
        mMethodOverride(m_methods, ObjectAtIndexGet);
        mMethodOverride(m_methods, RemoveAtIndex);
        mMethodOverride(m_methods, AddObjectToEnd);
        mMethodOverride(m_methods, IteratorNextGet);
        mMethodOverride(m_methods, IteratorRemove);
        mMethodOverride(m_methods, Flush);
        mMethodOverride(m_methods, DeleteWithObjectHandler);
        }

    mMethodsGet(self) = &m_methods;
    }

static void OverrideAtObject(AtList self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

uint8 AtListIndexIsValid(AtList self, uint32 listIndex)
    {
    return (AtListLengthGet((AtList)self) > listIndex) ? 1 : 0;
    }

/* Constructor of AtList */
AtList AtListObjectInit(AtList self)
    {
    AtOsalMemInit(self, 0, sizeof(tAtList));

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtObject(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtList
 * @{
 */

/**
 * Create a list with capacity
 *
 * @param capacity List capacity. The capacity will determine how a list is
 *        implemented and its performance.
 *        - If capacity is not 0, it is limited list and implemented by array.
 *        - If capacity is 0, it is none-limited list and implemented by link
 *          list
 *
 * @return Abstract list class
 */
AtList AtListCreate(uint32 capacity)
    {
    AtList newList;
    if (capacity)
        newList = (AtList)AtLimitedListNew(capacity);
    else
        newList = (AtList)AtLinkListNew();

    if (newList)
        {
        /* Empty list */
        newList->length = 0;
        newList->capacity = capacity;
        return newList;
        }
    return NULL;
    }

/**
 * Get length of a list (number of objects have been added to list)
 *
 * @param self This list
 *
 * @return Number of objects have been added to list
 */
uint32 AtListLengthGet(AtList self)
    {
    if (self)
        return mMethodsGet(self)->LengthGet(self);
    return 0;
    }

/**
 * Get object at specified index
 *
 * @param self This list
 * @param objectIndex Object index
 *
 * @return Object on success or NULL if index is invalid
 */
AtObject AtListObjectGet(AtList self, uint32 objectIndex)
    {
    if (self)
        return mMethodsGet(self)->ObjectAtIndexGet(self, objectIndex);
    return NULL;
    }

/**
 * Add object to a list
 *
 * @param self This list
 * @param object Object to add
 *
 * @return AT return code
 */
eAtRet AtListObjectAdd(AtList self, AtObject object)
    {
    if (self)
        return mMethodsGet(self)->AddObjectToEnd(self, object);
    return cAtError;
    }

/**
 * Remove object at specified index
 *
 * @param self This list
 * @param objectIndex Object index
 *
 * @return Object that has just been removed out of the list. NULL is returned
 *         if index is invalid
 */
AtObject AtListObjectRemoveAtIndex(AtList self, uint32 objectIndex)
    {
    if (self)
        return mMethodsGet(self)->RemoveAtIndex(self, objectIndex);
    return NULL;
    }

/**
 * Remove object by its reference out of list
 *
 * @param self This list
 * @param object Object to remove
 *
 * @return AT return code
 */
eAtRet AtListObjectRemove(AtList self, AtObject object)
    {
    AtIterator iterator;
    AtObject listObject = NULL;
    eAtRet ret = cAtError;

    if (!ListIsValid(self))
        return cAtError;

    iterator = AtListIteratorCreate(self);
    if (NULL == iterator)
        return cAtError;

    /* Search until reach the first appearance of object and remove it */
    while((listObject = AtIteratorNext(iterator)) != NULL)
        {
        if(listObject == object)
            {
            AtIteratorRemove(iterator);
            ret = cAtOk;
            break;
            }
        }
    AtObjectDelete((AtObject)iterator);

    return ret;
    }

/**
 * Get list's capacity
 *
 * @param self This list
 *
 * @return List's capacity
 */
uint32 AtListCapacityGet(AtList self)
    {
    if (!ListIsValid(self))
        return 0;

    return self->capacity;
    }

/**
 * Check if list contains an object
 *
 * @param self This list
 * @param object Object to check
 *
 * @return cAtTrue if object is in the list. Otherwise, cAtFalse is returned
 */
eBool AtListContainsObject(AtList self, AtObject object)
    {
    AtIterator iterator;
    eBool contain = cAtFalse;
    AtObject listObject = NULL;

    if (!ListIsValid(self))
        return cAtFalse;

    iterator = AtListIteratorCreate(self);
    if (NULL == iterator)
        return cAtFalse;

    /* Search until reach the first appearance of object then return */
    while((listObject = AtIteratorNext(iterator)) != NULL)
        {
        if(listObject == object)
            {
            contain = cAtTrue;
            break;
            }
        }

    /* Delete Iterator */
    AtObjectDelete((AtObject)iterator);

    return contain;
    }

/**
 * Create iterator to iterate all of objects
 *
 * @param self This list
 *
 * @return Iterator object
 */
AtIterator AtListIteratorCreate(AtList self)
    {
    if (self)
        return (AtIterator)AtListIteratorNew(self);

    return NULL;
    }

/**
 * Get built-in iterator
 *
 * @param self This list
 *
 * @return Built-in iterator
 */
AtIterator AtListIteratorGet(AtList self)
    {
    if (self)
        return IteratorGet(self);
    return NULL;
    }

/**
 * Flush all objects
 *
 * @param self This list
 *
 * @return None
 */
void AtListFlush(AtList self)
    {
    if (self)
        mMethodsGet(self)->Flush(self);
    }

/**
 * Add objects from other list
 *
 * @param self This list
 * @param list Other list
 *
 * @return Number of objects from other list have been added to this list
 */
uint32 AtListObjectsAddFromList(AtList self, AtList list)
    {
    if (self)
        return ObjectsAddFromList(self, list);
    return 0;
    }

/**
 * Delete list and give outside chance to handle object before it is deleted
 *
 * @param self This list
 * @param ObjectHandler Object handler
 */
void AtListDeleteWithObjectHandler(AtList self, void (*ObjectHandler)(AtObject object))
    {
    if (self)
        mMethodsGet(self)->DeleteWithObjectHandler(self, ObjectHandler);
    }

/**
 * @}
 */

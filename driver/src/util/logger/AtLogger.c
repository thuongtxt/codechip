/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtLogger.c
 *
 * Created Date: Mar 19, 2013
 *
 * Description : Logger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtLoggerInternal.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#define cTimeStampStringLength 32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtLoggerListenerWrapper
    {
    const tAtLoggerListener *callback;
    void *userData;
    }tAtLoggerListenerWrapper;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtLoggerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Init(AtLogger self)
    {
    AtLoggerEnable(self, cAtTrue);
    AtLoggerLevelEnable(self, cAtLogLevelCritical | cAtLogLevelWarning, cAtTrue);
    }

static void Log(AtLogger self, eAtLogLevel level, const char* prefix, const char *msgBuffer)
    {
	if (self->verbose)
	    {
	    AtPrintc(AtLoggerLevelColor(level), "%s", prefix);
	    AtPrintc(cSevNormal, "%s", msgBuffer);
	    }
    }

static void Show(AtLogger self)
    {
	AtUnused(self);
    /* Concrete class will do */
    return;
    }

static void Flush(AtLogger self)
    {
	AtOsalMemInit(&(self->counter), 0, sizeof(tAtLoggerCounters));
    }

static char* SharedMessageBufferCreate(AtLogger self)
    {
    uint32 maxMsgLength = AtLoggerMaxMessageLength(self);
    char *newBuffer = AtOsalMemAlloc(maxMsgLength);
    if (newBuffer)
        AtOsalMemInit(newBuffer, 0, maxMsgLength);
    return newBuffer;
    }

char* AtLoggerSharedBufferGet(AtLogger self, uint32* bufferLength)
    {
    if (self->sharedMsgBuffer == NULL)
        self->sharedMsgBuffer = SharedMessageBufferCreate(self);

    if (bufferLength)
        *bufferLength = AtLoggerMaxMessageLength(self);

    return self->sharedMsgBuffer;
    }

static uint32 NumMessages(AtLogger self)
    {
    AtUnused(self);
    return 0;
    }

static char* SharedPrefixBufferGet(AtLogger self, uint32* bufferLength)
    {
    if (self->sharedPrefixBuffer == NULL)
        self->sharedPrefixBuffer = AtOsalMemAlloc(cPrefixBufferLength);
    else
        AtOsalMemInit(self->sharedPrefixBuffer, 0, cPrefixBufferLength);

    if (bufferLength)
        *bufferLength = cPrefixBufferLength;

    return self->sharedPrefixBuffer;
    }

static const char* TimeStampString(AtLogger self)
    {
    static char timeStamp[cTimeStampStringLength];
    AtUnused(self);

    AtSnprintf(timeStamp, cTimeStampStringLength - 1, "[%s", AtOsalDateTimeGet());
    timeStamp[AtStrlen(timeStamp) - 1]    = ']';
    timeStamp[cTimeStampStringLength - 1] = '\0';

    return timeStamp;
    }

static void LevelEnable(AtLogger self, uint32 levelMask, eBool enable)
    {
    if (enable)
        self->enabledLevels = self->enabledLevels | levelMask;
    else
        self->enabledLevels = self->enabledLevels & (~levelMask);
    }

static eBool LevelIsEnabled(AtLogger self, eAtLogLevel logLevel)
    {
    return (self->enabledLevels & logLevel) ? cAtTrue : cAtFalse;
    }

static void ShowMessageContainSubString(AtLogger self, const char* pattern)
    {
    AtUnused(self);
    AtUnused(pattern);
    /* Concrete class will do */
    return;
    }

static void FileExport(AtLogger self, AtFile file)
    {
    AtUnused(self);
    AtUnused(file);
    }

static tAtLoggerListenerWrapper *ListenerFind(AtLogger self, const tAtLoggerListener *callback)
    {
    tAtLoggerListenerWrapper *wrapper;
    AtIterator iterator = AtListIteratorCreate(self->listeners);

    while ((wrapper = (tAtLoggerListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback == callback)
            break;
        }
    AtObjectDelete((AtObject)iterator);

    return wrapper;
    }

static tAtLoggerListenerWrapper *ListenerWrapperCreate(const tAtLoggerListener *callback, void *userData)
    {
    uint32 memorySize = sizeof(tAtLoggerListenerWrapper);
    tAtLoggerListenerWrapper *newWrapper = AtOsalMemAlloc(memorySize);

    AtOsalMemInit(newWrapper, 0, memorySize);
    newWrapper->callback = callback;
    newWrapper->userData = userData;

    return newWrapper;
    }

static AtList Listeners(AtLogger self)
    {
    if (self->listeners == NULL)
        self->listeners = AtListCreate(0);
    return self->listeners;
    }

static eAtRet ListenerAdd(AtLogger self, const tAtLoggerListener *callback, void *userData)
    {
    if (ListenerFind(self, callback))
        return cAtErrorDuplicatedEntries;

    return AtListObjectAdd(Listeners(self), (AtObject)ListenerWrapperCreate(callback, userData));
    }

static eAtRet ListenerRemove(AtLogger self, const tAtLoggerListener *callback)
    {
    tAtLoggerListenerWrapper *wrapper;

    if (callback == NULL)
        return cAtOk;

    if (self->listeners == NULL)
        return cAtErrorNotExist;

    wrapper = ListenerFind(self, callback);
    if (wrapper == NULL)
        return cAtErrorNotExist;

    if (wrapper->callback->WillRemoveListener)
        wrapper->callback->WillRemoveListener(self, wrapper->userData);

    AtListObjectRemove(self->listeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);

    return cAtOk;
    }

static void AllListenersDelete(AtLogger self)
    {
    if (self->listeners == NULL)
        return;

    while (AtListLengthGet(self->listeners) > 0)
        {
        tAtLoggerListenerWrapper *wrapper = (tAtLoggerListenerWrapper *)AtListObjectRemoveAtIndex(self->listeners, 0);
        if (wrapper->callback->WillRemoveListener)
            wrapper->callback->WillRemoveListener(self, wrapper->userData);
        AtOsalMemFree(wrapper);
        }

    AtObjectDelete((AtObject)self->listeners);
    }

static void Delete(AtObject self)
    {
    AtLogger logger = (AtLogger)self;

    AllListenersDelete(logger);

    if (logger->sharedMsgBuffer)
        {
        AtOsalMemFree(logger->sharedMsgBuffer);
        AtOsalMemFree(logger->sharedPrefixBuffer);
        logger->sharedMsgBuffer = NULL;
        logger->sharedPrefixBuffer = NULL;
        }

    AtLoggerUnLock(logger);
    AtOsalMutexDestroy(logger->loggerMutex);
    logger->loggerMutex = NULL;
    m_AtObjectMethods->Delete(self);
    }

static const char* LogLevel2String(eAtLogLevel level)
    {
    if (level == cAtLogLevelWarning)  return "[WARNING]";
    if (level == cAtLogLevelCritical) return "[CRITICAL]";
    if (level == cAtLogLevelInfo)     return "[INFO]";
    if (level == cAtLogLevelNormal)   return "[NORMAL]";
    if (level == cAtLogLevelNone)     return "";
    if (level == cAtLogLevelDebug)    return "[DEBUG]";
    return "[UNKNOWN]";
    }

static const char* PrefixBuildWithTimestamp(AtLogger self, eAtLogLevel level, eBool withTimestamp)
    {
    uint32 bufferLength;
    char* prefixBuffer = SharedPrefixBufferGet(self, &bufferLength);
    const char *levelString;

    if (prefixBuffer == NULL)
        return "";

    levelString = LogLevel2String(level);
    if (withTimestamp)
        {
        const char *timestamp = mMethodsGet(self)->TimeStampString(self);
        AtSnprintf(prefixBuffer, bufferLength - 1, "AT: %s %s ", timestamp, levelString);
        }
    else
        AtSnprintf(prefixBuffer, bufferLength - 1, "AT: %s ", levelString);

    return prefixBuffer;
    }

static eBool LoggerIsValid(AtLogger self)
    {
    return (self != NULL) ? cAtTrue : cAtFalse;
    }

static eBool CanLog(AtLogger self, eAtLogLevel level)
    {
    if (!LoggerIsValid(self) || !AtLoggerIsEnabled(self) || !AtLoggerLevelIsEnabled(self, level))
        return cAtFalse;

    return cAtTrue;
    }

static void Notify(AtLogger self, eAtLogLevel level, const char* message)
    {
    tAtLoggerListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorGet(self->listeners);
    AtIteratorRestart(iterator);
    while ((wrapper = (tAtLoggerListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->DidLogMessage)
            wrapper->callback->DidLogMessage(self, level, message, wrapper->userData);
        }
    }

static uint32 *CounterAddress(AtLogger self, eAtLogLevel level)
    {
    switch (level)
        {
        case cAtLogLevelNone    : return &(self->counter.levelNone);
        case cAtLogLevelNormal  : return &(self->counter.levelNormal);
        case cAtLogLevelInfo    : return &(self->counter.levelInfo);
        case cAtLogLevelWarning : return &(self->counter.levelWarning);
        case cAtLogLevelCritical: return &(self->counter.levelCritical);
        case cAtLogLevelDebug   : return &(self->counter.levelDebug);
        case cAtLogLevelAll     : return &(self->counter.levelOthers);
        default                 : return &(self->counter.levelOthers);
        }
    }

static void CounterUpdate(AtLogger self, eAtLogLevel level)
    {
    uint32 *address = CounterAddress(self, level);
    *address = *address + 1;
    }

static void DidLog(AtLogger self, eAtLogLevel level, const char* message)
    {
    Notify(self, level, message);
    CounterUpdate(self, level);
    }

static void CountersGet(AtLogger self, tAtLoggerCounters *counters)
    {
    if (counters)
        AtOsalMemCpy(counters, &(self->counter), sizeof(tAtLoggerCounters));
    }

static void CountersClear(AtLogger self, tAtLoggerCounters *counters)
    {
    if (counters)
        AtOsalMemCpy(counters, &(self->counter), sizeof(tAtLoggerCounters));
    AtOsalMemInit(&(self->counter), 0, sizeof(tAtLoggerCounters));
    }

static void OverrideAtObject(AtLogger self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtLogger self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtLogger self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Log);
        mMethodOverride(m_methods, Show);
        mMethodOverride(m_methods, Flush);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, NumMessages);
        mMethodOverride(m_methods, TimeStampString);
        mMethodOverride(m_methods, LevelEnable);
        mMethodOverride(m_methods, LevelIsEnabled);
        mMethodOverride(m_methods, ShowMessageContainSubString);
        mMethodOverride(m_methods, FileExport);
        }

    mMethodsGet(self) = &m_methods;
    }

/* Constructor of AtLogger */
AtLogger AtLoggerObjectInit(AtLogger self, uint32 maxMessageLength)
    {
    AtOsalMemInit(self, 0, sizeof(tAtLogger));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->maxMessageLength = maxMessageLength;
    self->loggerMutex = AtOsalMutexCreate();

    return self;
    }

eAtSevLevel AtLoggerLevelColor(eAtLogLevel level)
    {
    if (level == cAtLogLevelInfo)     return cSevInfo;
    if (level == cAtLogLevelWarning)  return cSevWarning;
    if (level == cAtLogLevelCritical) return cSevCritical;
    if (level == cAtLogLevelDebug)    return cSevDebug;

    return cSevNormal;
    }

/**
 * @addtogroup AtLogger
 * @{
 */

/**
 * Initialize logger
 *
 * @param self This logger
 */
void AtLoggerInit(AtLogger self)
    {
    if (LoggerIsValid(self))
        mMethodsGet(self)->Init(self);
    }

/**
 * Display all of logged messages of logger to console
 *
 * @param self This
 */
void AtLoggerShow(AtLogger self)
	{
	if (LoggerIsValid(self))
	    {
	    AtLoggerLock(self);
		mMethodsGet(self)->Show(self);
		AtLoggerUnLock(self);
	    }
	}

/**
 * Display logged message by level
 * @param self
 * @param level
 */
void AtLoggerShowByLevel(AtLogger self, eAtLogLevel level)
    {
    AtLoggerShowMessageContainSubString(self, LogLevel2String(level));
    }

/**
 * Flush all of logged messages
 *
 * @param self This logger
 */
void AtLoggerFlush(AtLogger self)
	{
	if (LoggerIsValid(self))
	    {
	    AtLoggerLock(self);
		mMethodsGet(self)->Flush(self);
		AtLoggerUnLock(self);
	    }
	}

/**
 * To verbose or silent when one message is logged. In verbose mode, message is
 * always displayed to console, then it is internally processed.
 *
 * @param self This logger
 * @param verbose Verbose.
 *                - cAtTrue: verbose
 *                - cAtFalse: silent
 */
void AtLoggerVerbose(AtLogger self, eBool verbose)
    {
    if (LoggerIsValid(self))
        self->verbose = verbose;
    }

/**
 * Get message max length
 *
 * @param self This logger
 * @return Message max length
 */
uint32 AtLoggerMaxMessageLength(AtLogger self)
    {
    return self->maxMessageLength;
    }

/**
 * Log a message
 *
 * @param self This logger
 * @param level Level of this message
 * @param format Format
 * @param args Arguments
 */
void AtLoggerLog(AtLogger self, eAtLogLevel level, const char *format, va_list args)
    {
    uint32 bufferSize;
    char *buffer;

    if (!CanLog(self, level))
        return;

    AtLoggerLock(self);

    buffer = AtLoggerSharedBufferGet(self, &bufferSize);
    if (buffer)
        {
        AtOsalMemInit(buffer, 0, bufferSize);

        /* Build message buffer */
        AtStdSnprintf(AtStdSharedStdGet(), buffer, bufferSize - 1, format, args);

        /* For better look, if created string from format and args is large than bufferSize */
        buffer[bufferSize - 3] = '\r';
        buffer[bufferSize - 2] = '\n';

        mMethodsGet(self)->Log(self, level, PrefixBuildWithTimestamp(self, level, cAtTrue), buffer);
        DidLog(self, level, buffer);
        }

    AtLoggerUnLock(self);
    }

/**
 * Log a message without lock the logger, outside need to lock logger before calling this function
 * and unlock after logging done
 *
 * @param self This logger
 * @param level Level of this message
 * @param message Message string
 */
void AtLoggerNoLockLog(AtLogger self, eAtLogLevel level, const char* message)
	{
    if (!CanLog(self, level))
        return;

    mMethodsGet(self)->Log(self, level, PrefixBuildWithTimestamp(self, level, cAtTrue), message);
    DidLog(self, level, message);
	}

/**
 * Enable/disable log level
 * @param self This logger
 * @param levelMask Mask of levels need to be enabled/disabled
 * @param enable Enable or disable
 */
void AtLoggerLevelEnable(AtLogger self, uint32 levelMask, eBool enable)
    {
    if (!LoggerIsValid(self))
        return;

    mMethodsGet(self)->LevelEnable(self, levelMask, enable);
    }

/**
 * Log level is enabled or disabled
 * @param self This logger
 * @param logLevel Log level
 * @return Log level is enabled or disabled
 */
eBool AtLoggerLevelIsEnabled(AtLogger self, eAtLogLevel logLevel)
    {
    if (!LoggerIsValid(self))
        return cAtFalse;

    return mMethodsGet(self)->LevelIsEnabled(self, logLevel);
    }

/**
 * Display logged messages which contain a pattern
 *
 * @param self This
 */

/**
 * Search and display logged messages which contain a pattern
 *
 * @param self This logger
 * @param subString Search pattern
 */
void AtLoggerShowMessageContainSubString(AtLogger self, const char* subString)
    {
    if (!LoggerIsValid(self))
        return;

    AtLoggerLock(self);
    mMethodsGet(self)->ShowMessageContainSubString(self, subString);
    AtLoggerUnLock(self);
    }

/**
 * Lock logger
 * @param self This logger
 * @return At return code
 */
eAtRet AtLoggerLock(AtLogger self)
    {
    return (AtOsalMutexLock(self->loggerMutex) == cAtOsalOk) ? cAtOk : cAtError;
    }

/**
 * Release logger
 * @param self This logger
 * @return At return code
 */
eAtRet AtLoggerUnLock(AtLogger self)
    {
    return (AtOsalMutexUnLock(self->loggerMutex) == cAtOsalOk) ? cAtOk : cAtError;
    }

/**
 * Enable/disable logging
 *
 * @param self This logger
 * @param enable Enable. cAtTrue to enable and cAtFalse to disable
 */
void AtLoggerEnable(AtLogger self, eBool enable)
    {
    if (self)
        self->enabled = enable;
    }

/**
 * Check if logger is enabled/disabled
 *
 * @param self This logger
 * @retval cAtTrue if logging is enabled
 * @retval cAtFalse if logging is disabled
 */
eBool AtLoggerIsEnabled(AtLogger self)
    {
    return (eBool)(self ? self->enabled : cAtFalse);
    }

/**
 * Get number of messages that have been logged.
 *
 * @param self This logger
 * @return Number of messages that have been logged.
 */
uint32 AtLoggerNumMessages(AtLogger self)
    {
    if (self)
        return mMethodsGet(self)->NumMessages(self);
    return 0;
    }

/**
 * Export all of messages to files
 *
 * @param self This logger
 * @param file File to export
 */
void AtLoggerFileExport(AtLogger self, AtFile file)
    {
    if (self)
        mMethodsGet(self)->FileExport(self, file);
    }

/**
 * Add listener
 *
 * @param self This logger
 * @param callback Callback
 * @param userData User data
 *
 * @return AT return code
 */
eAtRet AtLoggerListenerAdd(AtLogger self, const tAtLoggerListener *callback, void *userData)
    {
    if (self)
        return ListenerAdd(self, callback, userData);
    return cAtErrorNullPointer;
    }

/**
 * Remove listener
 *
 * @param self This logger
 * @param callback Callback
 *
 * @return AT return code
 */
eAtRet AtLoggerListenerRemove(AtLogger self, const tAtLoggerListener *callback)
    {
    if (self)
        return ListenerRemove(self, callback);
    return cAtErrorNullPointer;
    }

/**
 * Get counters
 *
 * @param self Logger
 * @param counters Counters
 */
void AtLoggerCountersGet(AtLogger self, tAtLoggerCounters *counters)
    {
    if (self)
        CountersGet(self, counters);
    }

/**
 * Clear counters
 *
 * @param self Logger
 * @param counters Counters before clearing
 */
void AtLoggerCountersClear(AtLogger self, tAtLoggerCounters *counters)
    {
    if (self)
        CountersClear(self, counters);
    }

/**
 * @}
 */

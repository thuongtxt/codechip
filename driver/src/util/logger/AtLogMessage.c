/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Logger
 *
 * File        : AtLogMessage.c
 *
 * Created Date: Apr 4, 2013
 *
 * Description : Message wrapper
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtLoggerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtObjectMethods m_AtObjectOverride;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtOsalMemFree(((AtLogMessage)self)->body);
    AtOsalMemFree(((AtLogMessage)self)->prefix);
    ((AtLogMessage)self)->body = NULL;
    ((AtLogMessage)self)->prefix = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtLogMessage self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride,m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static AtLogMessage MessageBodyCreate(AtLogMessage self, const char* prefix, const char* msgBuffer)
    {
    /* Create message body, assume that msgBuffer contains end of string,
     * so also copy it to message body */
    self->body = AtStrdup(msgBuffer);
    self->prefix = AtStrdup(prefix);
    if ((self->body == NULL) || (self->prefix == NULL))
        {
        AtObjectDelete((AtObject)self);
        return NULL;
        }

    return self;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLogMessage);
    }

static AtLogMessage AtLogMessageObjectInit(AtLogMessage self, eAtLogLevel level, const char *prefix, const char* msgBuffer)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtObject(self);
    m_methodsInit = 1;

    /* Private data */
    self->level = level;
    return MessageBodyCreate(self, prefix, msgBuffer);
    }

AtLogMessage AtLogMessageNew(eAtLogLevel level, const char *prefix, const char* msgBuffer)
    {
    AtLogMessage newMsg = AtOsalMemAlloc(ObjectSize());
    if (newMsg == NULL)
        return NULL;

    return AtLogMessageObjectInit(newMsg, level, prefix, msgBuffer);
    }

const char* AtLoggerLevel2String(eAtLogLevel level)
    {
    if (level == cAtLogLevelWarning)  return "[WARNING] ";
    if (level == cAtLogLevelCritical) return "[CRITICAL]";
    if (level == cAtLogLevelInfo)     return "[INFO]    ";
    if (level == cAtLogLevelNormal)   return "[NORMAL]  ";
    if (level == cAtLogLevelNone)     return "";
    return "[UNKNOWN] ";
    }

eBool AtLogMessageContainsSubString(AtLogMessage self, const char* subString)
    {
    if (self == NULL)
        return cAtFalse;

    if ((AtStrstr(self->body, subString)) || (AtStrstr(self->prefix, subString)))
        return cAtTrue;

    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtLoggerInternal.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : Logger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLOGGERINTERNAL_H_
#define _ATLOGGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "AtLogger.h"
#include "../../generic/common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cPrefixBufferLength 64
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtLoggerMethods
    {
    void  (*Init)(AtLogger self);
    void  (*Show)(AtLogger self);
    void  (*Flush)(AtLogger self);
    void  (*Log)(AtLogger self, eAtLogLevel level, const char* prefix, const char* msg);
    uint32 (*NumMessages)(AtLogger self);
    uint32 (*MaxMessageLengthGet)(AtLogger self);
    uint32 (*MaxMessageLengthSet)(AtLogger self);
    const char* (*TimeStampString)(AtLogger self);
    void  (*LevelEnable)(AtLogger self, uint32 levelMask, eBool enable);
    eBool (*LevelIsEnabled)(AtLogger self, eAtLogLevel logLevel);
    void  (*ShowMessageContainSubString)(AtLogger self, const char* pattern);
    void  (*FileExport)(AtLogger self, AtFile file);
    }tAtLoggerMethods;

typedef struct tAtLogger
    {
    tAtObject super;
    const tAtLoggerMethods *methods;

    /* Private data */
    eBool verbose;
    uint32 maxMessageLength;
    char* sharedMsgBuffer;
    char* sharedPrefixBuffer;
    AtOsalMutex loggerMutex;
    uint32 enabledLevels;
    eBool enabled;
    AtList listeners;
    tAtLoggerCounters counter;
    }tAtLogger;

typedef struct tAtLogMessage * AtLogMessage;

typedef struct tAtLogMessage
    {
    tAtObject super;

    /* Private data */
    eAtLogLevel level;
    char* body;
    char* prefix;
    }tAtLogMessage;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLogMessage AtLogMessageNew(eAtLogLevel level, const char* prefix, const char* msgBuffer);
AtLogger AtLoggerObjectInit(AtLogger self, uint32 maxMessageLength);
eBool AtLogMessageContainsSubString(AtLogMessage self, const char* subString);

#ifdef __cplusplus
}
#endif
#endif /* _ATLOGGERINTERNAL_H_ */


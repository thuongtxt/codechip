/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Logger
 *
 * File        : AtDefaultLogger.c
 *
 * Created Date: Mar 19, 2013
 *
 * Description : Default logger
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtLoggerInternal.h"
#include "AtList.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtDefaultLogger)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDefaultLogger * AtDefaultLogger;

typedef struct tAtDefaultLogger
    {
    tAtLogger super;

    /* Private data */
    AtList messages;
    uint32 maxNumMessages;
    }tAtDefaultLogger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtLoggerMethods m_AtLoggerOverride;
static const tAtLoggerMethods *m_AtLoggerMethods = NULL;

static tAtObjectMethods m_AtObjectOverride;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtList MessagesList(AtDefaultLogger self)
    {
    if (self->messages == NULL)
        self->messages = AtListCreate(self->maxNumMessages);

    return self->messages;
    }

static void Log(AtLogger self, eAtLogLevel level, const char* prefix, const char* msgBuffer)
    {
    AtLogMessage message;

    if (AtListLengthGet(mThis(self)->messages) >= mThis(self)->maxNumMessages)
        return;

    message = AtLogMessageNew(level, prefix, msgBuffer);
    m_AtLoggerMethods->Log((AtLogger)self, level, prefix, msgBuffer);

    if (AtListObjectAdd(MessagesList((AtDefaultLogger)self), (AtObject)message) != cAtOk)
        AtObjectDelete((AtObject)message);
    }

static void PrintMsgOnConsole(AtLogMessage msg)
    {
    AtPrintc(AtLoggerLevelColor(msg->level), "%s", msg->prefix);
    AtPrintc(cSevNormal, "%s", msg->body);
    }

static void Show(AtLogger self)
    {
    AtIterator msgIterator;
    AtLogMessage msg;
    AtDefaultLogger defaultLogger = (AtDefaultLogger)self;

    if (AtListLengthGet(defaultLogger->messages) == 0)
        return;

    msgIterator = AtListIteratorCreate(defaultLogger->messages);
    while((msg = (AtLogMessage)AtIteratorNext(msgIterator)) != NULL)
        PrintMsgOnConsole(msg);

    AtObjectDelete((AtObject)msgIterator);
    }

static uint32 NumMessages(AtLogger self)
    {
    AtDefaultLogger defaultLogger = (AtDefaultLogger)self;
    return AtListLengthGet(defaultLogger->messages);
    }

static void AllMessageDelete(AtDefaultLogger self)
    {
    AtIterator msgIterator = AtListIteratorCreate(self->messages);
    AtLogMessage msg;

    while((msg = (AtLogMessage)AtIteratorNext(msgIterator)) != NULL)
        AtObjectDelete((AtObject)msg);

    AtObjectDelete((AtObject)msgIterator);
    }

static void Flush(AtLogger self)
    {
    AtDefaultLogger defaultLogger = (AtDefaultLogger)self;

    if (defaultLogger->messages == NULL)
        return;

    AllMessageDelete(defaultLogger);
    AtListFlush(defaultLogger->messages);

    m_AtLoggerMethods->Flush(self);
    }

static void ShowMessageContainSubString(AtLogger self, const char* subString)
    {
    AtIterator msgIterator;
    AtLogMessage msg;
    AtDefaultLogger defaultLogger = (AtDefaultLogger)self;

    if (defaultLogger->messages == NULL)
       return;

    msgIterator = AtListIteratorCreate(defaultLogger->messages);
    while((msg = (AtLogMessage)AtIteratorNext(msgIterator)) != NULL)
        {
        if (AtLogMessageContainsSubString(msg, subString))
            PrintMsgOnConsole(msg);
        }

    AtObjectDelete((AtObject)msgIterator);
    }

static void FileExport(AtLogger self, AtFile file)
    {
    AtIterator msgIterator;
    AtLogMessage msg;
    AtDefaultLogger defaultLogger = (AtDefaultLogger)self;

    if (file == NULL)
        return;

    if (AtListLengthGet(defaultLogger->messages) == 0)
        return;

    msgIterator = AtListIteratorCreate(defaultLogger->messages);
    while((msg = (AtLogMessage)AtIteratorNext(msgIterator)) != NULL)
        AtFilePrintf(file, "%s%s", msg->prefix, msg->body);

    AtObjectDelete((AtObject)msgIterator);
    }

static void OverrideAtLogger(AtDefaultLogger self)
    {
    AtLogger logger = (AtLogger)self;
    if (!m_methodsInit)
        {
        m_AtLoggerMethods = mMethodsGet(logger);
        AtOsalMemCpy(&m_AtLoggerOverride, m_AtLoggerMethods, sizeof(m_AtLoggerOverride));

        mMethodOverride(m_AtLoggerOverride, Log);
        mMethodOverride(m_AtLoggerOverride, Show);
        mMethodOverride(m_AtLoggerOverride, Flush);
        mMethodOverride(m_AtLoggerOverride, NumMessages);
        mMethodOverride(m_AtLoggerOverride, ShowMessageContainSubString);
        mMethodOverride(m_AtLoggerOverride, FileExport);
        }

    mMethodsSet(logger, &m_AtLoggerOverride);
    }

static void MessageListDelete(AtDefaultLogger self)
    {
    if (self->messages == NULL)
        return;

    while (AtListLengthGet(self->messages) > 0)
        AtObjectDelete((AtObject)AtListObjectRemoveAtIndex(self->messages, 0));

    AtObjectDelete((AtObject)(self->messages));
    self->messages = NULL;
    }

static void Delete(AtObject self)
    {
    AtDefaultLogger logger = (AtDefaultLogger)self;

    AtLoggerLock((AtLogger)logger);

    /* Delete private data */
    MessageListDelete(logger);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtDefaultLogger self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtDefaultLogger self)
    {
    OverrideAtLogger(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultLogger);
    }

/* Constructor of AtDefaultLogger */
static AtLogger AtDefaultLoggerObjectInit(AtLogger self, uint32 maxNumMessages, uint32 maxMessageLength)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    if (AtLoggerObjectInit(self, maxMessageLength) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->maxNumMessages = maxNumMessages;

    return self;
    }

AtLogger AtDefaultLoggerNew(uint32 maxNumMessages, uint32 maxMessageLength)
    {
    AtLogger newLogger = AtOsalMemAlloc(ObjectSize());
    if (newLogger == NULL)
        return NULL;

    /* Construct it */
    if (AtDefaultLoggerObjectInit(newLogger, maxNumMessages, maxMessageLength) == NULL)
        {
        AtObjectDelete((AtObject)newLogger);
        return NULL;
        }

    AtLoggerInit(newLogger);
    return newLogger;
    }

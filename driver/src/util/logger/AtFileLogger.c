/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Logger
 *
 * File        : AtFileLogger.c
 *
 * Created Date: Apr 7, 2013
 *
 * Description : File logger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtLoggerInternal.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtFileLogger * AtFileLogger;

typedef struct tAtFileLogger
    {
    tAtLogger super;

    /* Private data */
    char* fileName;
    char* lineBuffer;
    uint32 numMessage;
    }tAtFileLogger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtLoggerMethods m_AtLoggerOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super's implementation */
static const tAtLoggerMethods *m_AtLoggerMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

static const char* m_LevelStr[] = {"[WARNING]", "[CRITICAL]", "[INFO]", "[NORMAL]", "[DEBUG]"};
static eAtLogLevel m_LevelVal[] = {cAtLogLevelWarning, cAtLogLevelCritical, cAtLogLevelInfo, cAtLogLevelNormal, cAtLogLevelDebug};

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void CloseFile(AtFile file)
    {
    AtFileClose(file);
    }

static AtFile OpenFile(const char* fileName)
    {
    AtStd std = AtStdSharedStdGet();
    uint8 mode = cAtFileOpenModeAppend | cAtFileOpenModeRead;
    AtFile file = AtStdFileOpen(std, fileName, mode);

    return file;
    }

static void Log(AtLogger self, eAtLogLevel level, const char* prefix, const char* message)
    {
    AtFile file;
    AtFileLogger fileLogger = (AtFileLogger)self;

    /* Super's job */
    m_AtLoggerMethods->Log((AtLogger)self, level, prefix, message);

	file = OpenFile(fileLogger->fileName);

    /* Need a file */
    if (file == NULL)
        return;

    /* Make buffer content before written to file */
    AtFileWrite(file, prefix, sizeof(char), AtStrlen(prefix));
    AtFileWrite(file, message, sizeof(char), AtStrlen(message));
    AtFileFlush(file);

    /* Close file so another task have change to write to log file */
    CloseFile(file);
    ((AtFileLogger)self)->numMessage++;
    }

static void FlushFile(const char* fileName)
    {
    AtFile file;
    AtStd std = AtStdSharedStdGet();
    uint8 mode = cAtFileOpenModeWrite;

    /* Open file as over write */
    file = AtStdFileOpen(std, fileName, mode);
    AtFileClose(file);
    }

static void Flush(AtLogger self)
    {
    AtFileLogger fileLogger = (AtFileLogger)self;
    if (fileLogger->fileName == NULL)
        return;

    FlushFile(fileLogger->fileName);
	fileLogger->numMessage = 0;

	m_AtLoggerMethods->Flush(self);
    }

static void Delete(AtObject self)
    {
    AtFileLogger logger = (AtFileLogger)self;

    AtLoggerLock((AtLogger)logger);
    if (logger->lineBuffer)
        {
        AtOsalMemFree(logger->lineBuffer);
        logger->lineBuffer = NULL;
        }

    AtOsalMemFree(logger->fileName);
    logger->fileName = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static char* LineBufferGet(AtFileLogger self, uint32 *bufferLength)
    {
    uint32 size = AtLoggerMaxMessageLength((AtLogger)self) + cPrefixBufferLength;
    if (self->lineBuffer == NULL)
        self->lineBuffer = AtOsalMemAlloc(size);

    *bufferLength = size;
    return self->lineBuffer;
    }

static void PrintMsgOnConsole(eAtLogLevel level, char* startOfMessage, const char* line)
    {
    AtPrintc(AtLoggerLevelColor(level), "%s ", line);

    if (startOfMessage)
        AtPrintc(cSevNormal, "%s", startOfMessage);
    }

static char* PrefixSplit(char* line, eAtLogLevel *level)
    {
    uint8 level_i;
    uint32 levelLength;
    char* startOfLevel;

    for (level_i = 0; level_i < mCount(m_LevelStr); level_i++)
        {
        if ((startOfLevel = AtStrstr(line, m_LevelStr[level_i])) != NULL)
            {
            levelLength = AtStrlen(m_LevelStr[level_i]);
            *(startOfLevel + levelLength) = '\0';

            *level = m_LevelVal[level_i];
            return startOfLevel + levelLength + 1;
            }
        }

    *level = cAtLogLevelNone;
    return NULL;
    }

static void SearchMessageInFile(AtFileLogger fileLogger, AtFile file, const char* subString)
    {
    uint32 bufferLength;
    eAtLogLevel level;
    char* startOfMessage;

    char *lineBuffer = LineBufferGet(fileLogger, &bufferLength);
    if (lineBuffer == NULL)
        return;

    while (AtFileGets(file, lineBuffer, bufferLength) != NULL)
        {
        if (AtStrstr(lineBuffer, subString))
            {
            startOfMessage = PrefixSplit(lineBuffer, &level);
            PrintMsgOnConsole(level, startOfMessage, lineBuffer);
            }
        }
    }

static void ShowMessageContainSubString(AtLogger self, const char* subString)
    {
    AtFile file;
    AtFileLogger fileLogger = (AtFileLogger)self;
    if (fileLogger->fileName == NULL)
        return;

    file = OpenFile(fileLogger->fileName);
    if (file == NULL)
        return;

    SearchMessageInFile(fileLogger, file, subString);
    CloseFile(file);
    }

static void Show(AtLogger self)
    {
    AtFile file;
    uint32 bufferLength;
    char *lineBuffer, *startOfMessage;
    AtFileLogger fileLogger = (AtFileLogger)self;
    eAtLogLevel level;

    if (fileLogger->fileName == NULL)
        return;

    file = OpenFile(fileLogger->fileName);
    if (file == NULL)
        return;

    lineBuffer = LineBufferGet(fileLogger, &bufferLength);
    while (AtFileGets(file, lineBuffer, bufferLength) != NULL)
        {
        startOfMessage = PrefixSplit(lineBuffer, &level);
        PrintMsgOnConsole(level, startOfMessage, lineBuffer);
        }

    CloseFile(file);
    }

static uint32 NumMessages(AtLogger self)
    {
    return ((AtFileLogger)self)->numMessage;
    }

static void OverrideAtObject(AtFileLogger self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtLogger(AtFileLogger self)
    {
    AtLogger logger = (AtLogger)self;
    if (!m_methodsInit)
        {
        m_AtLoggerMethods = mMethodsGet(logger);
        AtOsalMemCpy(&m_AtLoggerOverride, m_AtLoggerMethods, sizeof(m_AtLoggerOverride));
        mMethodOverride(m_AtLoggerOverride, Log);
        mMethodOverride(m_AtLoggerOverride, Flush);
        mMethodOverride(m_AtLoggerOverride, ShowMessageContainSubString);
        mMethodOverride(m_AtLoggerOverride, Show);
        mMethodOverride(m_AtLoggerOverride, NumMessages);
        }

    mMethodsSet(logger, &m_AtLoggerOverride);
    }

static void Override(AtFileLogger self)
    {
    OverrideAtLogger(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFileLogger);
    }

static eBool CanCreateFileWithName(const char* fileName)
    {
    AtStd std = AtStdSharedStdGet();
    uint8 mode = cAtFileOpenModeAppend | cAtFileOpenModeRead;
    AtFile file = AtStdFileOpen(std, fileName, mode);

    if (file == NULL)
        return cAtFalse;

    AtFileClose(file);
    return cAtTrue;
    }

static AtLogger ObjectInit(AtLogger self, const char* fileName, uint32 maxMessageLen)
    {
    AtFileLogger fileLogger = (AtFileLogger)self;
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtLoggerObjectInit(self, maxMessageLen) == NULL)
        return NULL;

    /* Setup class */
    Override(fileLogger);
    m_methodsInit = 1;

    /* Private data */
    fileLogger->fileName = AtStrdup(fileName);

    return self;
    }

AtLogger AtFileLoggerNew(const char* fileName, uint32 maxMessageLen)
    {
    AtLogger newLogger = AtOsalMemAlloc(ObjectSize());
    if (newLogger == NULL)
        return NULL;

    /* Construct it */
    if (ObjectInit(newLogger, fileName, maxMessageLen) == NULL)
        {
        AtObjectDelete((AtObject)newLogger);
        return NULL;
        }

    if (!CanCreateFileWithName(fileName))
        {
        AtObjectDelete((AtObject)newLogger);
        return NULL;
        }

    AtLoggerInit(newLogger);
    return newLogger;
    }

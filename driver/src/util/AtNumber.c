/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtNumber.c
 *
 * Created Date: Jun 16, 2015
 *
 * Description : Number utility
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtList.h"
#include "AtObject.h"
#include "AtNumber.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtNumber * AtNumber;
typedef struct tAtNumber
    {
    uint32 number;
    }tAtNumber;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtNumber AtNumberNew(uint32 number)
    {
    AtNumber newNumber = AtOsalMemAlloc(sizeof(tAtNumber));
    if (newNumber == NULL)
        return NULL;

    newNumber->number = number;
    return newNumber;
    }

static void AtNumberDelete(AtNumber self)
    {
    AtOsalMemFree(self);
    }

const char* AtNumberDigitGroupingString(uint32 number)
    {
    return AtNumberUInt64DigitGroupingString((uint64)number);
    }

const char* AtNumberUInt64DigitGroupingString(uint64 number)
    {
    static char numberString[16];
    char tmpStr[4];
    AtList digitGroupList;
    uint32 object_i;
    AtNumber aNumber;

    if (number == 0)
        return "0";

    digitGroupList = AtListCreate(0);
    while (number != 0)
        {
        AtListObjectAdd(digitGroupList, (AtObject)(AtNumberNew((uint32)(number % 1000UL))));
        number = number / 1000;
        }

    AtOsalMemInit(numberString, 0, 16);
    for (object_i = AtListLengthGet(digitGroupList); object_i > 0; object_i--)
        {
        aNumber = (AtNumber)(AtListObjectGet(digitGroupList, object_i - 1));
        if (object_i == AtListLengthGet(digitGroupList))
            AtSnprintf(tmpStr, 4, "%u", aNumber->number);
        else
            AtSnprintf(tmpStr, 4, "%03u", aNumber->number);
        tmpStr[3] = '\0';
        AtStrcat(numberString, tmpStr);
        if (object_i > 1)
            AtStrcat(numberString, ",");

        AtNumberDelete(aNumber);
        }

    numberString[15] = '\0';
    AtObjectDelete((AtObject)digitGroupList);
    return numberString;
    }

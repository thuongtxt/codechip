/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtLimitedList.c
 *
 * Created Date: Oct 5, 2012
 *
 * Description : Limitted list
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtListInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* To override super methods (require if need to override some Methods) */
static tAtObjectMethods m_AtObjectOverride;
static tAtListMethods   m_AtListOverride;

static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtListMethods   *m_AtListMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DeleteWithObjectHandler(AtList self, void (*ObjectHandler)(AtObject object))
    {
    uint32 i;
    AtObject *objects;

    if (ObjectHandler == NULL)
        {
        m_AtListMethods->DeleteWithObjectHandler(self, ObjectHandler);
        return;
        }

    objects = (((AtLimitedList)self)->objects);
    for (i = 0; i < self->length; i++)
        ObjectHandler(objects[i]);

    AtOsalMemFree((((AtLimitedList)self)->objects));

    m_AtListMethods->DeleteWithObjectHandler(self, ObjectHandler);
    }

static void Delete(AtObject self)
    {
    AtOsalMemFree((((AtLimitedList)self)->objects));
    ((AtLimitedList)self)->objects = NULL;
    m_AtObjectMethods->Delete(self);
    }

static AtObject ObjectAtIndexGet(AtList self, uint32 listIndex)
    {
    if (AtListIndexIsValid(self, listIndex))
         return (((AtLimitedList)self)->objects[listIndex]);
    return NULL;
    }

static AtObject RemoveAtIndex(AtList self, uint32 position)
    {
    AtObject removedObject;
    AtLimitedList limitedList = (AtLimitedList)self;
    uint32 i;
    if (AtListIndexIsValid(self, position))
        {
        removedObject = limitedList->objects[position];
        /* Re-arrange list */
        for (i = position; i < self->length - 1; i++)
            {
            limitedList->objects[i] = limitedList->objects[i+1];
            }
        self->length--;
        return removedObject;
        }
    return NULL;
    }

static void Flush(AtList self)
    {
    self->length = 0;
    }

static AtObject IteratorNextGet(AtList self, AtListIterator iterator)
    {
    AtObject anObject = NULL;

    anObject = mMethodsGet(self)->ObjectAtIndexGet(self, (uint32)(iterator->index + 1));
    if (anObject)
        iterator->index++; /* For next time get */

    iterator->current = anObject;
    return anObject;
    }

static AtObject IteratorRemove(AtList self, AtListIterator iterator)
    {
    AtObject anObject= NULL;
    if (iterator->current)
        {
        anObject = RemoveAtIndex(self, (uint32)iterator->index);
        iterator->current = ObjectAtIndexGet(self, (uint32)iterator->index);
        }
    return anObject;
    }

static uint8 ListIsFull(AtLimitedList self)
    {
    return ((((AtList)self)->length) == (((AtList)self)->capacity) ? 1 : 0);
    }

static eAtRet AddObjectToEnd(AtList self, AtObject object)
    {
    AtLimitedList limitedList = (AtLimitedList)self;
    if (!ListIsFull(limitedList))
        {
        limitedList->objects[self->length] = object;
        self->length++;
        return cAtOk;
        }

    return cAtError;
    }

static void OverrideAtObject(AtLimitedList self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }
    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtList(AtLimitedList self)
    {
    AtList list = (AtList)self;

    if (!m_methodsInit)
        {
        m_AtListMethods = mMethodsGet(list);
        AtOsalMemCpy(&m_AtListOverride, mMethodsGet(list), sizeof(m_AtListOverride));
        mMethodOverride(m_AtListOverride, ObjectAtIndexGet);
        mMethodOverride(m_AtListOverride, RemoveAtIndex);
        mMethodOverride(m_AtListOverride, AddObjectToEnd);
        mMethodOverride(m_AtListOverride, IteratorNextGet);
        mMethodOverride(m_AtListOverride, IteratorRemove);
        mMethodOverride(m_AtListOverride, Flush);
        mMethodOverride(m_AtListOverride, DeleteWithObjectHandler);
        }

    mMethodsSet(list, &m_AtListOverride);
    }

static void Override(AtLimitedList self)
    {
    OverrideAtList(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLimitedList);
    }

static AtLimitedList ObjectInit(AtLimitedList self, uint32 capacity)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    /* Super constructor */
    if (AtListObjectInit((AtList)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->objects = AtOsalMemAlloc(sizeof(AtObject) * capacity);
    AtOsalMemInit(self->objects, 0, sizeof(AtObject) * capacity);

    return self;
    }

AtLimitedList AtLimitedListNew(uint32 capacity)
    {
    AtLimitedList newList = AtOsalMemAlloc(ObjectSize());
    if (newList == NULL)
        return NULL;

    return ObjectInit(newList, capacity);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwGroupInternal.h
 * 
 * Created Date: May 5, 2015
 *
 * Description : PW Group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWGROUPINTERNAL_H_
#define _ATPWGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "AtPwGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwGroupMethods
    {
    eAtModulePwRet (*Enable)(AtPwGroup self, eBool enable);
    eBool (*IsEnabled)(AtPwGroup self);
    eAtModulePwRet (*PwAdd)(AtPwGroup self, AtPw pw);
    eAtModulePwRet (*PwRemove)(AtPwGroup self, AtPw pw);
    eBool (*ContainsPw)(AtPwGroup self, AtPw pw);
    uint32 (*NumPws)(AtPwGroup self);
    AtPw (*PwAtIndex)(AtPwGroup self, uint32 pwIndex);

    eAtModulePwRet (*TxLabelSetSelect)(AtPwGroup self, eAtPwGroupLabelSet labelSet);
    eAtPwGroupLabelSet (*TxSelectedLabelGet)(AtPwGroup self);
    eAtModulePwRet (*RxLabelSetSelect)(AtPwGroup self, eAtPwGroupLabelSet labelSet);
    eAtPwGroupLabelSet (*RxSelectedLabelGet)(AtPwGroup self);
    eAtPwGroupType (*TypeGet)(AtPwGroup self);

    eAtModulePwRet (*Init)(AtPwGroup self);
    const char *(*IdString)(AtPwGroup self);
    const char *(*TypeString)(AtPwGroup self);
    }tAtPwGroupMethods;

typedef struct tAtPwGroup
    {
    tAtObject super;
    const tAtPwGroupMethods *methods;

    /* Private data */
    uint32 groupId;
    AtModulePw module;
    AtList pws;
    }tAtPwGroup;

typedef struct tAtPwApsGroup
    {
    tAtPwGroup super;
    }tAtPwApsGroup;

typedef struct tAtPwHsGroup
    {
    tAtPwGroup super;
    }tAtPwHsGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePwRet AtPwGroupInit(AtPwGroup self);
AtPwGroup AtPwGroupObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module);
AtPwGroup AtPwApsGroupObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module);
AtPwGroup AtPwHsGroupObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module);

#endif /* _ATPWGROUPINTERNAL_H_ */


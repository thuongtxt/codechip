/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwKbyteChannelInternal.h
 * 
 * Created Date: Jun 8, 2018
 *
 * Description : Abstract PW K-byte channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWKBYTECHANNELINTERNAL_H_
#define _ATPWKBYTECHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelInternal.h"
#include "AtPwKbyteChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwKbyteChannelMethods
    {
    /* Line mapping */
    eAtRet (*SdhLineSet)(AtPwKbyteChannel self, AtSdhLine line);

    eAtRet (*ValidationEnable)(AtPwKbyteChannel self, eBool enable);
    eBool  (*ValidationIsEnabled)(AtPwKbyteChannel self);
    eAtRet (*AlarmSuppressionEnable)(AtPwKbyteChannel self, eBool enable);
    eBool  (*AlarmSuppressionIsEnabled)(AtPwKbyteChannel self);
    eAtRet (*OverrideEnable)(AtPwKbyteChannel self, eBool enable);
    eBool  (*OverrideIsEnabled)(AtPwKbyteChannel self);

    /* K-bytes */
    eAtRet (*TxK1Set)(AtPwKbyteChannel self, uint8 value);
    uint8  (*TxK1Get)(AtPwKbyteChannel self);
    eAtRet (*TxK2Set)(AtPwKbyteChannel self, uint8 value);
    uint8  (*TxK2Get)(AtPwKbyteChannel self);
    eAtRet (*TxEK1Set)(AtPwKbyteChannel self, uint8 value);
    uint8  (*TxEK1Get)(AtPwKbyteChannel self);
    eAtRet (*TxEK2Set)(AtPwKbyteChannel self, uint8 value);
    uint8  (*TxEK2Get)(AtPwKbyteChannel self);
    uint8  (*RxK1Get)(AtPwKbyteChannel self);
    uint8  (*RxK2Get)(AtPwKbyteChannel self);
    uint8  (*RxEK1Get)(AtPwKbyteChannel self);
    uint8  (*RxEK2Get)(AtPwKbyteChannel self);

    /* Channel mapping label */
    eAtRet (*ExpectedLabelSet)(AtPwKbyteChannel self, uint16 value);
    uint16 (*ExpectedLabelGet)(AtPwKbyteChannel self);
    eAtRet (*TxLabelSet)(AtPwKbyteChannel self, uint16 value);
    uint16 (*TxLabelGet)(AtPwKbyteChannel self);
    }tAtPwKbyteChannelMethods;

typedef struct tAtPwKbyteChannel
    {
    tAtChannel super;
    const tAtPwKbyteChannelMethods *methods;

    /* Private data */
    AtPw pw; /* K-byte pseudowire */
    AtSdhLine line; /* Mapping line */
    }tAtPwKbyteChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPwKbyteChannel AtPwKbyteChannelObjectInit(AtPwKbyteChannel self, uint32 channelId, AtPw pw, AtModulePw module);


#ifdef __cplusplus
}
#endif
#endif /* _ATPWKBYTECHANNELINTERNAL_H_ */


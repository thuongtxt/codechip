/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwCountersInternal.h
 *
 * Created Date: Apr 02, 2013
 *
 * Description : All pseudowire counter classes
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCOUNTERSINTERNAL_H_
#define _ATPWCOUNTERSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h"  /* Super class */
#include "AtPwCounters.h"

#include "../../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwCountersMethods
    {
    void (*Init)(AtPwCounters self);
    }tAtPwCountersMethods;

typedef struct tAtPwCounters
    {
    tAtObject super;
    const tAtPwCountersMethods *methods;

    /* Private */
    uint32 txPackets;
    uint32 txPayloadBytes;
    uint32 rxPackets;
    uint32 rxPayloadBytes;
    uint32 rxDiscardedPackets; /* same with OutOfSeqDropedPackets */
    uint32 rxMalformedPackets;
    uint32 rxReorderedPackets;
    uint32 rxLostPackets;
    uint32 rxOutOfSeqDropedPackets;
    uint32 rxOamPackets;
    uint32 rxStrayPackets;
    uint32 rxDuplicatedPackets;
    }tAtPwCounters;

typedef struct tAtPwTdmCounters
    {
    tAtPwCounters  super;

    /* Private data */
    uint32 txLbitPackets;
    uint32 txRbitPackets;
    uint32 rxLbitPackets;
    uint32 rxRbitPackets;
    uint32 rxJitBufOverrun;
    uint32 rxJitBufUnderrun;
    uint32 rxLops;
    uint32 rxPacketsSentToTdm;
    }tAtPwTdmCounters;

typedef struct tAtPwSAToPCounters
    {
    tAtPwTdmCounters  super;

    /* Private data */
    }tAtPwSAToPCounters;

typedef struct tAtPwCESoPCounters
    {
    /* super */
    tAtPwTdmCounters  super;

    /* Public data */
    uint32 txMbitPackets;
    uint32 rxMbitPackets;
    }tAtPwCESoPCounters;

typedef struct tAtPwCepCounters
    {
    /* super */
    tAtPwTdmCounters  super;

    /* private */
    uint32 txNbitPackets;
    uint32 txPbitPackets;
    uint32 rxNbitPackets;
    uint32 rxPbitPackets;
    }tAtPwCepCounters;

typedef struct tAtPwHdlcCounters
    {
    /* super */
    tAtPwCounters super;

    }tAtPwHdlcCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPwCounters AtPwCountersNew(void);
AtPwSAToPCounters AtPwSAToPCountersNew(void);
AtPwCESoPCounters AtPwCESoPCountersNew(void);
AtPwCepCounters AtPwCepCountersNew(void);
AtPwHdlcCounters AtPwHdlcCountersNew(void);

/* Constructor */
AtPwCounters AtPwCountersObjectInit(AtPwCounters self);
AtPwTdmCounters AtPwTdmCountersObjectInit(AtPwTdmCounters self);

void AtPwCountersInit(AtPwCounters self);

/* Access private data */
void AtPwCountersTxPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersTxPayloadBytesSet(AtPwCounters self, uint32 value);
void AtPwCountersRxPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxPayloadBytesSet(AtPwCounters self, uint32 value);
void AtPwCountersRxDiscardedPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxMalformedPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxReorderedPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxLostPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxOutOfSeqDropPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxOamPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxStrayPacketsSet(AtPwCounters self, uint32 value);
void AtPwCountersRxDuplicatedPacketsSet(AtPwCounters self, uint32 value);

void AtPwTdmCountersTxLbitPacketsSet(AtPwTdmCounters self, uint32 value);
void AtPwTdmCountersTxRbitPacketsSet(AtPwTdmCounters self, uint32 value);
void AtPwTdmCountersRxLbitPacketsSet(AtPwTdmCounters self, uint32 value);
void AtPwTdmCountersRxRbitPacketsSet(AtPwTdmCounters self, uint32 value);
void AtPwTdmCountersRxJitBufOverrunSet(AtPwTdmCounters self, uint32 value);
void AtPwTdmCountersRxJitBufUnderrunSet(AtPwTdmCounters self, uint32 value);
void AtPwTdmCountersRxLopsSet(AtPwTdmCounters self, uint32 value);
void AtPwTdmCountersRxPacketsSentToTdmSet(AtPwTdmCounters self, uint32 value);

void AtPwCESoPCountersTxMbitPacketsSet(AtPwCESoPCounters self, uint32 value);
void AtPwCESoPCountersRxMbitPacketsSet(AtPwCESoPCounters self, uint32 value);

void AtPwCepCountersTxNbitPacketsSet(AtPwCepCounters self, uint32 value);
void AtPwCepCountersTxPbitPacketsSet(AtPwCepCounters self, uint32 value);
void AtPwCepCountersRxNbitPacketsSet(AtPwCepCounters self, uint32 value);
void AtPwCepCountersRxPbitPacketsSet(AtPwCepCounters self, uint32 value);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCOUNTERSINTERNAL_H_ */

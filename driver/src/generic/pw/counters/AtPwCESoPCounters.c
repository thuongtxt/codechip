/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwTdmCounters.c
 *
 * Created Date: Apr 02, 2013
 *
 * Description : TDM Pseudowire counters
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwCESoPCounters)self)
#define mCEsopCounterIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwCountersMethods m_AtPwCountersOverride;

/* Save super implementation */
static const tAtPwCountersMethods *m_AtPwCountersMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwCESoPCounters);
    }

static void Init(AtPwCounters self)
    {
    AtPwCESoPCounters counters = mThis(self);

    m_AtPwCountersMethods->Init(self);

    counters->txMbitPackets = 0;
    counters->rxMbitPackets = 0;
    }

static void OverrideAtPwCounters(AtPwCounters self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwCountersMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwCountersOverride, m_AtPwCountersMethods, sizeof(m_AtPwCountersOverride));

        mMethodOverride(m_AtPwCountersOverride, Init);
        }

    mMethodsSet(self, &m_AtPwCountersOverride);
    }

static void Override(AtPwCounters self)
    {
    OverrideAtPwCounters(self);
    }

static AtPwCESoPCounters AtPwCESoPCountersObjectInit(AtPwCESoPCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwTdmCountersObjectInit((AtPwTdmCounters)self) == NULL)
        return NULL;

    /* Setup class  */
    Override((AtPwCounters)self);
    m_methodsInit = 1;

    return self;
    }

AtPwCESoPCounters AtPwCESoPCountersNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwCESoPCounters newCounters = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtPwCESoPCountersObjectInit(newCounters);
    }

void AtPwCESoPCountersTxMbitPacketsSet(AtPwCESoPCounters self, uint32 value)
    {
    if (self)
        self->txMbitPackets = value;
    }
    
void AtPwCESoPCountersRxMbitPacketsSet(AtPwCESoPCounters self, uint32 value)
    {
    if (self)
        self->rxMbitPackets = value;
    }

/**
 * @addtogroup AtPwCounters
 * @{
 */

/**
 * Get number of transmitted M Bit packets when circuit detects RDI.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCESoPCountersTxMbitPacketsGet(AtPwCESoPCounters self)
    {
    if (mCEsopCounterIsValid(self))
        return self->txMbitPackets;

    return 0;
    }

/**
 * Get number of received M Bit packets. This will include good /stray/malformed
 * packets.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCESoPCountersRxMbitPacketsGet(AtPwCESoPCounters self)
    {
    if (mCEsopCounterIsValid(self))
        return self->rxMbitPackets;

    return 0;
    }
/** @} */

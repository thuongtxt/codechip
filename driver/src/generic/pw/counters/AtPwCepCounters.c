/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwCepCounters.c
 *
 * Created Date: Apr 02, 2013
 *
 * Description : TDM Pseudowire counters
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwCepCounters)self)
#define mCepCounterIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwCountersMethods m_AtPwCountersOverride;

/* Save super implementation */
static const tAtPwCountersMethods *m_AtPwCountersMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwCepCounters);
    }

static void Init(AtPwCounters self)
    {
    AtPwCepCounters counters = mThis(self);

    m_AtPwCountersMethods->Init(self);

    counters->txNbitPackets = 0;
    counters->txPbitPackets = 0;
    counters->rxNbitPackets = 0;
    counters->rxPbitPackets = 0;
    }

static void OverrideAtPwCounters(AtPwCounters self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwCountersMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwCountersOverride, m_AtPwCountersMethods, sizeof(m_AtPwCountersOverride));

        mMethodOverride(m_AtPwCountersOverride, Init);
        }

    mMethodsSet(self, &m_AtPwCountersOverride);
    }

static void Override(AtPwCounters self)
    {
    OverrideAtPwCounters(self);
    }

static AtPwCepCounters AtPwCepCountersObjectInit(AtPwCepCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwTdmCountersObjectInit((AtPwTdmCounters)self) == NULL)
        return NULL;

    /* Setup class  */
    Override((AtPwCounters)self);
    m_methodsInit = 1;

    return self;
    }

AtPwCepCounters AtPwCepCountersNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwCepCounters newCounters = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtPwCepCountersObjectInit(newCounters);
    }

void AtPwCepCountersTxNbitPacketsSet(AtPwCepCounters self, uint32 value)
    {
    if (self)
        self->txNbitPackets = value;
    }

void AtPwCepCountersTxPbitPacketsSet(AtPwCepCounters self, uint32 value)
    {
    if (self)
        self->txPbitPackets = value;
    }

void AtPwCepCountersRxNbitPacketsSet(AtPwCepCounters self, uint32 value)
    {
    if (self)
        self->rxNbitPackets = value;
    }

void AtPwCepCountersRxPbitPacketsSet(AtPwCepCounters self, uint32 value)
    {
    if (self)
        self->rxPbitPackets = value;
    }

/**
 * @addtogroup AtPwCounters
 * @{
 */

/**
 * Get number of transmitted NP-bit packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCepCountersTxNbitPacketsGet(AtPwCepCounters self)
    {
    if (mCepCounterIsValid(self))
        return self->txNbitPackets;

    return 0;
    }

/**
 * Get number of transmitted NP-bit packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCepCountersTxPbitPacketsGet(AtPwCepCounters self)
    {
    if (mCepCounterIsValid(self))
        return self->txPbitPackets;

    return 0;
    }

/**
 * Get number of received NP-bit packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCepCountersRxNbitPacketsGet(AtPwCepCounters self)
    {
    if (mCepCounterIsValid(self))
        return self->rxNbitPackets;

    return 0;
    }

/**
 * Get number of received P-bit packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCepCountersRxPbitPacketsGet(AtPwCepCounters self)
    {
    if (mCepCounterIsValid(self))
        return self->rxPbitPackets;

    return 0;
    }

/** @} */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwCounters.c
 *
 * Created Date: Sep 18, 2012
 *
 * Description : Pseudowire counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mPwCounterIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwCountersMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwCounters);
    }

static void Init(AtPwCounters self)
    {
    self->txPackets               = 0;
    self->txPayloadBytes          = 0;
    self->rxPackets               = 0;
    self->rxPayloadBytes          = 0;
    self->rxDiscardedPackets      = 0;
    self->rxMalformedPackets      = 0;
    self->rxReorderedPackets      = 0;
    self->rxLostPackets           = 0;
    self->rxOutOfSeqDropedPackets = 0;
    self->rxOamPackets            = 0;
    self->rxStrayPackets          = 0;
    }

static void MethodsInit(AtPwCounters self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        }

    mMethodsSet(self, &m_methods);
    }

AtPwCounters AtPwCountersObjectInit(AtPwCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPwCounters AtPwCountersNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwCounters newCounters = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCounters == NULL)
        return NULL;

    /* Construct it */
    return AtPwCountersObjectInit(newCounters);
    }

void AtPwCountersRxPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxPackets = value;
    }

void AtPwCountersRxPayloadBytesSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxPayloadBytes = value;
    }

void AtPwCountersRxDiscardedPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxDiscardedPackets = value;
    }

void AtPwCountersRxMalformedPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxMalformedPackets = value;
    }

void AtPwCountersRxReorderedPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxReorderedPackets = value;
    }

void AtPwCountersRxLostPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxLostPackets = value;
    }

void AtPwCountersRxOutOfSeqDropPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxOutOfSeqDropedPackets = value;
    }

void AtPwCountersRxOamPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxOamPackets = value;
    }

void AtPwCountersTxPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->txPackets = value;
    }

void AtPwCountersTxPayloadBytesSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->txPayloadBytes = value;
    }

void AtPwCountersInit(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        mMethodsGet(self)->Init(self);
    }

void AtPwCountersRxStrayPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxStrayPackets = value;
    }

void AtPwCountersRxDuplicatedPacketsSet(AtPwCounters self, uint32 value)
    {
    if (self)
        self->rxDuplicatedPackets = value;
    }

/**
 * @addtogroup AtPwCounters
 * @{
 */

/**
 * Get number of packets transmitted to PSN side
 *
 * @param self This counter
 *
 * @return Number of transmitted packets
 */
uint32 AtPwCountersTxPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->txPackets;

    return 0;
    }

/**
 * Get number of transmitted bytes of PW payload, including PW Control Word, RTP
 * but not include PSN header.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersTxPayloadBytesGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->txPayloadBytes;

    return 0;
    }

/**
 * Get number of received packets from PSN including good/stray/malformed packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxPackets;

    return 0;
    }

/**
 * Get number of received bytes of PW payload, including PW Control Word, RTP
 * but not include PSN header. Payload bytes of good/stray/malformed packets are
 * also counted in this counter.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxPayloadBytesGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxPayloadBytes;

    return 0;
    }

/**
 * Get number of discarded packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxDiscardedPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxDiscardedPackets;

    return 0;
    }

/**
 * Get number of received malformed packets. Packet is malformed when RTP PT
 * field mismatch or payload length mismatch
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxMalformedPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxMalformedPackets;

    return 0;
    }

/**
 * Get number of successfully re-ordered packets. This is not error counter,
 * just to report there are re-sequence actions with packet stream.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxReorderedPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxReorderedPackets;

    return 0;
    }

/**
 * Get number of lost packets - packets with expected sequences do not come.
 * For example, packets with sequence 1,2,6,7 come but 3,4,5 do not. After
 * re-sequence timeout, this counter will increase 3 as 3 packets do not come.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxLostPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxLostPackets;

    return 0;
    }

/**
 * Get number of received packets are out of sequence but can not be re-ordered.
 * For detail, packet coming with (sequence - expected sequence > min{PDV, 32})
 * will be considered as out of sequence dropped packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxOutOfSeqDropPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxOutOfSeqDropedPackets;

    return 0;
    }

/**
 * Get number of PW OAM packets such VCCV packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxOamPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxOamPackets;

    return 0;
    }

/**
 * Get number of received stray packets. Packet is stray if RTP SSRC field
 * mismatch packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxStrayPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxStrayPackets;

    return 0;
    }

/**
 * Get number of received duplicated packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwCountersRxDuplicatedPacketsGet(AtPwCounters self)
    {
    if (mPwCounterIsValid(self))
        return self->rxDuplicatedPackets;

    return 0;
    }

/** @} */

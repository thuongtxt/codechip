/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwHdlcCounters.c
 *
 * Created Date: Apr 12, 2016
 *
 * Description : HDLC PW Counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwHdlcCounters);
    }

static AtPwHdlcCounters AtPwHdlcCountersObjectInit(AtPwHdlcCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwCountersObjectInit((AtPwCounters)self) == NULL)
        return NULL;

    /* Setup class  */

    return self;
    }

AtPwHdlcCounters AtPwHdlcCountersNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwHdlcCounters newCounters = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtPwHdlcCountersObjectInit(newCounters);
    }

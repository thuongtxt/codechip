/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwTdmCounters.c
 *
 * Created Date: Apr 02, 2013
 *
 * Description : TDM Pseudowire counters
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCountersInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwTdmCounters)self)
#define mTdmCounterIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwCountersMethods m_AtPwCountersOverride;

/* Save super implementation */
static const tAtPwCountersMethods *m_AtPwCountersMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwTdmCounters);
    }

static void Init(AtPwCounters self)
    {
    AtPwTdmCounters counters = mThis(self);

    m_AtPwCountersMethods->Init(self);

    counters->txLbitPackets      = 0;
    counters->txRbitPackets      = 0;
    counters->rxLbitPackets      = 0;
    counters->rxRbitPackets      = 0;
    counters->rxJitBufOverrun    = 0;
    counters->rxJitBufUnderrun   = 0;
    counters->rxLops             = 0;
    counters->rxPacketsSentToTdm = 0;
    }

static void OverrideAtPwCounters(AtPwCounters self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwCountersMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwCountersOverride, m_AtPwCountersMethods, sizeof(m_AtPwCountersOverride));

        mMethodOverride(m_AtPwCountersOverride, Init);
        }

    mMethodsSet(self, &m_AtPwCountersOverride);
    }

static void Override(AtPwCounters self)
    {
    OverrideAtPwCounters(self);
    }

AtPwTdmCounters AtPwTdmCountersObjectInit(AtPwTdmCounters self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPwCountersObjectInit((AtPwCounters)self) == NULL)
        return NULL;

    /* Setup class  */
    Override((AtPwCounters)self);
    m_methodsInit = 1;

    return self;
    }

void AtPwTdmCountersTxLbitPacketsSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->txLbitPackets = value;
    }
    
void AtPwTdmCountersTxRbitPacketsSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->txRbitPackets = value;
    }
    
void AtPwTdmCountersRxLbitPacketsSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->rxLbitPackets = value;
    }
    
void AtPwTdmCountersRxRbitPacketsSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->rxRbitPackets = value;
    }
    
void AtPwTdmCountersRxJitBufUnderrunSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->rxJitBufUnderrun = value;
    }
    
void AtPwTdmCountersRxLopsSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->rxLops = value;
    }

void AtPwTdmCountersRxPacketsSentToTdmSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->rxPacketsSentToTdm = value;
    }

void AtPwTdmCountersRxJitBufOverrunSet(AtPwTdmCounters self, uint32 value)
    {
    if (self)
        self->rxJitBufOverrun = value;
    }
    
/**
 * @addtogroup AtPwCounters
 * @{
 */

/**
 * Get number of transmitted L-Bit packets. This happens when TDM has critical
 * alarm (LOF/AIS/...)
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersTxLbitPacketsGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->txLbitPackets;

    return 0;
    }

/**
 * Get number of transmitted R-Bit packets. When PW is in lost-of-packet state
 * (SAToP term) or lost of packet synchronization (CEP term), RBit packets will
 * be transmitted.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersTxRbitPacketsGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->txRbitPackets;

    return 0;
    }

/**
 * Get number of received L-bit packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersRxLbitPacketsGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->rxLbitPackets;

    return 0;
    }

/**
 * Get number of received R-bit packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersRxRbitPacketsGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->rxRbitPackets;

    return 0;
    }

/**
 * Get Number of PW packets come when jitter buffer is in OVERRUN state.
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersRxJitBufOverrunGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->rxJitBufOverrun;

    return 0;
    }

/**
 * Get number of empty packets sent to TDM during UNDERRUN state
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersRxJitBufUnderrunGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->rxJitBufUnderrun;

    return 0;
    }

/**
 * Get number of transitions from normal to lost of packet synchronization condition
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersRxLopsGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->rxLops;

    return 0;
    }
    
/**
 * Get number of packets sent to SONET/SDH/TDM side, includes data packets,
 * replacing packets of L/R bits packets, and replacing packets of LOST packets
 *
 * @param self This counter
 *
 * @return Counter value
 */
uint32 AtPwTdmCountersRxPacketsSentToTdmGet(AtPwTdmCounters self)
    {
    if (mTdmCounterIsValid(self))
        return self->rxPacketsSentToTdm;

    return 0;
    }

/** @} */

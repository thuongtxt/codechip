/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwFr.c
 *
 * Created Date: May 24, 2016
 *
 * Description : FR PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "fr_pw";
    }

static void OverrideAtChannel(AtPwFr self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPwFr self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwFr);
    }

AtPwFr AtPwFrObjectInit(AtPwFr self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwHdlcObjectInit((AtPwHdlc)self, pwId, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    ((AtPw)self)->pwType = cAtPwTypeFr;

    return self;
    }

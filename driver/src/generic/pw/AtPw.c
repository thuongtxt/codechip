/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPw.c
 *
 * Created Date: Sep 18, 2012
 *
 * Description : Generic implementation of Pseudowire
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwInternal.h"
#include "AtModulePwInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h"
#include "../util/AtUtil.h"
#include "../sur/AtModuleSurInternal.h"
#include "../prbs/AtModulePrbsInternal.h"
#include "../lab/profiler/AtFailureProfilerFactory.h"
#include "psn/AtPwPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cVlanStringLen 32
#define cMacStringLen  32

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPw)self)
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

#define mMacAttributeSetWithCache(method, mac)                                 \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mMacAttributeApiLogStart(mac);                                             \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        Cache##method(self, mac);                                              \
        ret = mMethodsGet(self)->method(self, mac);                            \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s]) = %s\r\n",     \
                                    AtFunction, AtObjectToString((AtObject)self), AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPwMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Cache super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->prbsEngine));
    mThis(self)->prbsEngine = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void DefaultRtpSet(AtPw self)
    {
    AtModulePw modulePw = (AtModulePw)AtChannelModuleGet((AtChannel)self);
    eBool rtpIsEnabled = AtModulePwRtpIsEnabledAsDefault(modulePw);

    AtPwRtpEnable(self, rtpIsEnabled);
    AtPwRtpPayloadTypeSet(self, 98); /* Any value */
    AtPwRtpSsrcSet(self, 0);
    AtPwRtpSsrcCompare(self, cAtFalse);
    AtPwRtpPayloadTypeCompare(self, cAtFalse);
    AtPwRtpTimeStampModeSet(self, cAtPwRtpTimeStampModeDifferential);
    }

static eAtRet Init(AtChannel self)
    {
    AtPw pw = (AtPw)self;
    AtModulePw modulePw = (AtModulePw)AtChannelModuleGet(self);

    /* Super initialization */
    eAtModulePwRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return cAtOk;

    /* Common default configuration */
    if (AtPwNeedInitConfig((AtPw)self))
        {
        AtPwSuppressEnable(pw, cAtFalse);
        AtPwReorderingEnable(pw, cAtTrue);

        /* Control word default configuration */
        AtPwCwEnable(pw, cAtTrue);
        AtPwCwAutoRBitEnable(pw, cAtTrue);
        AtPwCwAutoTxLBitEnable(pw, cAtTrue);
        AtPwCwAutoRxLBitEnable(pw, cAtTrue);
        AtPwCwSequenceModeSet(pw, cAtPwCwSequenceModeWrapZero);
        AtPwCwPktReplaceModeSet(pw, cAtPwPktReplaceModeAis);
        AtPwCwLengthModeSet(pw, cAtPwCwLengthModePayload);
        AtPwTxActiveForce(pw, cAtFalse);

        if (mMethodsGet(pw)->CanControlLopsPktReplaceMode(pw))
            AtPwLopsPktReplaceModeSet(pw, cAtPwPktReplaceModeAis);

        /* Clear interrupt mask and alarm forced. */
        AtChannelInterruptMaskSet(self, AtChannelInterruptMaskGet(self), 0x0);
        AtChannelTxAlarmUnForce(self, AtChannelTxForcableAlarmsGet(self));

        /* And RTP */
        DefaultRtpSet(pw);

        /* Default LOPS threshold */
        AtPwLopsSetThresholdSet(pw, AtModulePwDefaultLopsSetThreshold(modulePw));
        if (AtPwLopsClearThresholdIsSupported(pw))
            AtPwLopsClearThresholdSet(pw, AtModulePwDefaultLopsClearThreshold(modulePw));
        }

    return AtChannelEnable(self, cAtFalse);
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    if (self)
        mThis(self)->prbsEngine = engine;
    }

static AtPrbsEngine PrbsEngineCreate(AtChannel self)
    {
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePrbs);

    if (AtModulePrbsAllPwsPrbsIsSupported(prbsModule))
        return AtModulePrbsPwPrbsEngineCreate(prbsModule, AtChannelIdGet(self), (AtPw)self);

    return NULL;
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (mThis(self)->prbsEngine == NULL)
        mThis(self)->prbsEngine = PrbsEngineCreate(self);
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPw object = (AtPw)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(pwType);
    mEncodeObjectDescription(prbsEngine);
    mEncodeObjectDescription(apsGroup);
    mEncodeObjectDescription(hsGroup);

    mEncodeNone(cache);
    }

static eBool PktReplaceModeIsValid(eAtPwPktReplaceMode pktReplaceMode)
    {
    return mOutOfRange(pktReplaceMode, cAtPwPktReplaceModePreviousGoodPacket, cAtPwPktReplaceModeIdleCode) ? cAtFalse : cAtTrue;
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtPwCache);
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static void CacheLopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->lopsSetThreshold = numPackets;
    }

static uint32 CacheLopsSetThresholdGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->lopsSetThreshold : 0;
    }

static void CacheLopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->lopsClearThreshold = numPackets;
    }

static uint32 CacheLopsClearThresholdGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->lopsClearThreshold : 0;
    }

static void CacheRtpEnable(AtPw self, eBool enable)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rtpEnabled = enable;
    }

static eBool CacheRtpIsEnabled(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return (eBool)(cache ? cache->rtpEnabled : cAtFalse);
    }

static void CacheRtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rtpTxPayloadType = payloadType;
    }

static uint8 CacheRtpTxPayloadTypeGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->rtpTxPayloadType : 0);
    }

static void CacheRtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rtpExpectedPayloadType = payloadType;
    }

static uint8 CacheRtpExpectedPayloadTypeGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->rtpExpectedPayloadType : 0);
    }

static void CacheRtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rtpTxSsrc = ssrc;
    }

static uint32 CacheRtpTxSsrcGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->rtpTxSsrc : 0;
    }

static void CacheRtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rtpExpectedSsrc = ssrc;
    }

static uint32 CacheRtpExpectedSsrcGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->rtpExpectedSsrc : 0;
    }

static void CacheRtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rtpTimeStampMode = timeStampMode;
    }

static eAtPwRtpTimeStampMode CacheRtpTimeStampModeGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->rtpTimeStampMode : 0;
    }

static void CacheCwAutoTxLBitEnable(AtPw self, eBool enable)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->cwAutoTxLBitEnabled = enable;
    }

static eBool CacheCwAutoTxLBitIsEnabled(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return (eBool)(cache ? cache->cwAutoTxLBitEnabled : cAtFalse);
    }

static void CacheCwAutoRxLBitEnable(AtPw self, eBool enable)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->cwAutoRxLBitEnabled = enable;
    }

static eBool CacheCwAutoRxLBitIsEnabled(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return (eBool)(cache ? cache->cwAutoRxLBitEnabled : cAtFalse);
    }

static void CacheCwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->cwPktReplaceMode = pktReplaceMode;
    }

static eAtPwPktReplaceMode CacheCwPktReplaceModeGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->cwPktReplaceMode : cAtPwPktReplaceModeInvalid;
    }

static void CacheLopsPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->lopsPktReplaceMode = pktReplaceMode;
    }

static eAtPwPktReplaceMode CacheLopsPktReplaceModeGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->lopsPktReplaceMode : cAtPwPktReplaceModeInvalid;
    }

static void CacheCwAutoRBitEnable(AtPw self, eBool enable)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->cwAutoRBitEnabled = enable;
    }

static eBool CacheCwAutoRBitIsEnabled(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    return (eBool)(cache ? cache->cwAutoRBitEnabled : cAtFalse);
    }

static void CacheEthDestMacSet(AtPw self, uint8 *destMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return;

    if (destMac)
        AtOsalMemCpy(cache->destMac, destMac, sizeof(cache->destMac));
    }

static eAtModulePwRet CacheEthDestMacGet(AtPw self, uint8 *destMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if ((cache == NULL) || (destMac == NULL))
        return cAtErrorNullPointer;

    AtOsalMemCpy(destMac, cache->destMac, sizeof(cache->destMac));
    return cAtOk;
    }

static void CacheEthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return;

    if (srcMac)
        AtOsalMemCpy(cache->srcMac, srcMac, sizeof(cache->srcMac));
    }

static eAtModulePwRet CacheEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if ((cache == NULL) || (srcMac == NULL))
        return cAtErrorNullPointer;

    AtOsalMemCpy(srcMac, cache->srcMac, sizeof(cache->srcMac));
    return cAtOk;
    }

static void CacheEthCVlanTpidSet(AtPw self, uint16 tpid)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return ;

    if (tpid)
        cache->cVlanTpid = tpid;
    }

static void CacheEthSVlanTpidSet(AtPw self, uint16 tpid)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return ;

    if (tpid)
        cache->sVlanTpid = tpid;
    }

static uint16 CacheEthCVlanTpidGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return 0;

    return cache->cVlanTpid;
    }

static uint16 CacheEthSVlanTpidGet(AtPw self)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return 0;

    return cache->sVlanTpid;
    }

static void CacheEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache == NULL)
        return;

    /* Cache C-VLAN */
    if (cVlan)
        {
        AtOsalMemCpy(&(cache->cVlan), cVlan, sizeof(cache->cVlan));
        cache->pCVlan = &(cache->cVlan);
        }
    else
        {
        cache->pCVlan = NULL;
        }

    /* Cache S-VLAN */
    if (sVlan)
        {
        AtOsalMemCpy(&(cache->sVlan), sVlan, sizeof(cache->sVlan));
        cache->pSVlan = &(cache->sVlan);
        }
    else
        {
        cache->pSVlan = NULL;
        }
    }

static tAtEthVlanTag *CacheEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return NULL;

    if ((cache->pCVlan == NULL) || (cVlan == NULL))
        return NULL;

    AtOsalMemCpy(cVlan, cache->pCVlan, sizeof(tAtEthVlanTag));
    return cVlan;
    }

static tAtEthVlanTag *CacheEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return NULL;

    if ((cache->pSVlan == NULL) || (sVlan == NULL))
        return NULL;

    AtOsalMemCpy(sVlan, cache->pSVlan, sizeof(tAtEthVlanTag));
    return sVlan;
    }

static void CacheBackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache == NULL)
        return;

    /* Cache C-VLAN */
    if (cVlan)
        {
        AtOsalMemCpy(&(cache->backupCVlan), cVlan, sizeof(cache->backupCVlan));
        cache->pBackupCVlan = &(cache->backupCVlan);
        }
    else
        {
        cache->pBackupCVlan = NULL;
        }

    /* Cache S-VLAN */
    if (sVlan)
        {
        AtOsalMemCpy(&(cache->backupSVlan), sVlan, sizeof(cache->backupSVlan));
        cache->pBackupSVlan = &(cache->backupSVlan);
        }
    else
        {
        cache->pBackupSVlan = NULL;
        }
    }

static tAtEthVlanTag *CacheBackupEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return NULL;

    if ((cache->pBackupCVlan == NULL) || (cVlan == NULL))
        return NULL;

    AtOsalMemCpy(cVlan, cache->pBackupCVlan, sizeof(tAtEthVlanTag));
    return cVlan;
    }

static tAtEthVlanTag *CacheBackupEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return NULL;

    if ((cache->pBackupSVlan == NULL) || (sVlan == NULL))
        return NULL;

    AtOsalMemCpy(sVlan, cache->pBackupSVlan, sizeof(tAtEthVlanTag));
    return sVlan;
    }

static void CacheBackupEthDestMacSet(AtPw self, uint8 *destMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return;

    if (destMac)
        AtOsalMemCpy(cache->backupDestMac, destMac, sizeof(cache->backupDestMac));
    }

static eAtModulePwRet CacheBackupEthDestMacGet(AtPw self, uint8 *destMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if ((cache == NULL) || (destMac == NULL))
        return cAtErrorNullPointer;

    AtOsalMemCpy(destMac, cache->backupDestMac, sizeof(cache->backupDestMac));
    return cAtOk;
    }

static void CacheBackupEthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return;

    if (srcMac)
        AtOsalMemCpy(cache->backupSrcMac, srcMac, sizeof(cache->backupSrcMac));
    }

static eAtModulePwRet CacheBackupEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    tAtPwCache *cache = AtChannelCacheGet((AtChannel)self);

    if ((cache == NULL) || (srcMac == NULL))
        return cAtErrorNullPointer;

    AtOsalMemCpy(srcMac, cache->backupSrcMac, sizeof(cache->backupSrcMac));
    return cAtOk;
    }

static void CacheEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    CacheEthDestMacSet(self, destMac);
    CacheEthVlanSet(self, cVlan, sVlan);
    }

static void CacheBackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    CacheBackupEthDestMacSet(self, destMac);
    CacheBackupEthVlanSet(self, cVlan, sVlan);
    }

static void CachePayloadSizeSet(AtPw self, uint16 sizeInBytes)
    {
    AtUnused(self);
    AtUnused(sizeInBytes);
    }

static uint16 CachePayloadSizeGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static AtModuleSur SurModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static AtSurEngine SurEngineObjectCreate(AtChannel self)
    {
    return AtModuleSurPwEngineCreate(SurModule(self), (AtPw)self);
    }

static void SurEngineObjectDelete(AtChannel self, AtSurEngine engine)
    {
    AtModuleSurEnginePwDelete(SurModule(self), engine);
    }

static AtFailureProfiler FailureProfilerObjectCreate(AtChannel self)
    {
    return AtFailureProfilerFactoryPwProfilerCreate(AtChannelProfilerFactory(self), self);
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtPwSharedQuerier();
    }

static eAtRet HardwareCleanup(AtChannel self)
    {
    eAtRet ret;
    ret  = AtChannelInterruptMaskSet(self, AtChannelInterruptMaskGet(self), 0x0);
    ret |= AtChannelSurEngineInterruptDisable(self);

    return ret;
    }

static eAtModulePwRet LopsPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtUnused(pktReplaceMode);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPwPktReplaceMode LopsPktReplaceModeGet(AtPw self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtPwPktReplaceModeAis;
    }

static eBool CanControlLopsPktReplaceMode(AtPw self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool PktReplaceModeIsSupported(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtUnused(self);
    AtUnused(pktReplaceMode);
    return cAtTrue;
    }

static void OverrideAtChannel(AtPw self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, CacheSize);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectCreate);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectDelete);
        mMethodOverride(m_AtChannelOverride, FailureProfilerObjectCreate);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, HardwareCleanup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtPw self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPw self)
    {
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static uint32 LopsThresholdMax(AtPw self)
    {
    /* Sub class must know */
    return AtPwLopsThresholdMin(self);
    }

static uint32 LopsThresholdMin(AtPw self)
    {
    /* Sub class must know. But normally, it is 1 as a minimum */
    AtUnused(self);
    return 1;
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
	AtUnused(numPackets);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 LopsSetThresholdGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
	AtUnused(numPackets);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 sizeInBytes)
    {
	AtUnused(sizeInBytes);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint16 PayloadSizeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet PrioritySet(AtPw self, uint8 priority)
    {
	AtUnused(priority);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 PriorityGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet ReorderingEnable(AtPw self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool ReorderingIsEnabled(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModulePwRet JitterBufferSizeSet(AtPw self, uint32 microseconds)
    {
	AtUnused(microseconds);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 JitterBufferSizeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet JitterBufferSizeInPacketSet(AtPw self, uint16 numPackets)
    {
	AtUnused(numPackets);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint16 JitterBufferSizeInPacketGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet JitterBufferDelaySet(AtPw self, uint32 microseconds)
    {
	AtUnused(microseconds);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 JitterBufferDelayGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet JitterBufferDelayInPacketSet(AtPw self, uint16 numPackets)
    {
	AtUnused(numPackets);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint16  JitterBufferDelayInPacketGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint32 NumCurrentPacketsInJitterBuffer(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint32 NumCurrentAdditionalBytesInJitterBuffer(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet JitterBufferCenter(AtPw self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtModulePwRet RtpEnable(AtPw self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool RtpIsEnabled(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModulePwRet RtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
	AtUnused(payloadType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint8 RtpTxPayloadTypeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet RtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
	AtUnused(payloadType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint8 RtpExpectedPayloadTypeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet RtpPayloadTypeCompare(AtPw self, eBool enableCompare)
	{
	AtUnused(enableCompare);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
	}

static eBool RtpPayloadTypeIsCompared(AtPw self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static eAtModulePwRet RtpPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    eAtModulePwRet ret = cAtOk;

    ret |= AtPwRtpTxPayloadTypeSet(self, payloadType);
    ret |= AtPwRtpExpectedPayloadTypeSet(self, payloadType);

    return ret;
    }

static uint8 RtpPayloadTypeGet(AtPw self)
    {
    return AtPwRtpTxPayloadTypeGet(self);
    }

static eAtModulePwRet RtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
	AtUnused(ssrc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint32 RtpTxSsrcGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet RtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
	AtUnused(ssrc);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint32 RtpExpectedSsrcGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet RtpSsrcCompare(AtPw self, eBool enableCompare)
	{
	AtUnused(enableCompare);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
	}

static eBool RtpSsrcIsCompared(AtPw self)
	{
	AtUnused(self);
    /* Let concrete class do */
	return cAtFalse;
	}

static eAtModulePwRet RtpSsrcSet(AtPw self, uint32 ssrc)
    {
    eAtModulePwRet ret = cAtOk;

    ret |= AtPwRtpTxSsrcSet(self, ssrc);
    ret |= AtPwRtpExpectedSsrcSet(self, ssrc);

    return ret;
    }

static uint32 RtpSsrcGet(AtPw self)
    {
    return AtPwRtpTxSsrcGet(self);
    }

static eAtModulePwRet RtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
	AtUnused(timeStampMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPwRtpTimeStampMode RtpTimeStampModeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtPwRtpTimeStampModeInvalid;
    }

static eBool RtpTimeStampModeIsSupported(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtUnused(self);
    AtUnused(timeStampMode);
    return cAtFalse;
    }

static eAtModulePwRet SuppressEnable(AtPw self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool SuppressIsEnabled(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModulePwRet CwEnable(AtPw self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool CwIsEnabled(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModulePwRet CwAutoTxLBitEnable(AtPw self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool CwAutoTxLBitIsEnabled(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModulePwRet CwAutoRxLBitEnable(AtPw self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool CwAutoRxLBitIsEnabled(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool CwAutoRxLBitCanEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static eAtModulePwRet CwAutoRBitEnable(AtPw self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool CwAutoRBitIsEnabled(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModulePwRet CwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
	AtUnused(sequenceMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPwCwSequenceMode CwSequenceModeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtPwCwSequenceModeInvalid;
    }

static eBool CwSequenceModeIsSupported(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(self);
    AtUnused(sequenceMode);
    return cAtFalse;
    }

static eAtModulePwRet CwLengthModeSet(AtPw self, eAtPwCwLengthMode lengthMode)
    {
	AtUnused(lengthMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPwCwLengthMode CwLengthModeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtPwCwLengthModeFullPacket;
    }

static eBool CwLengthModeIsSupported(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(self);
    return (lengthMode == cAtPwCwLengthModeFullPacket) ? cAtTrue : cAtFalse;
    }

static eAtModulePwRet CwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
	AtUnused(pktReplaceMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPwPktReplaceMode CwPktReplaceModeGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtPwPktReplaceModeAis;
    }

/* PSN */
static eAtModulePwRet PsnSet(AtPw self, AtPwPsn psn)
    {
	AtUnused(psn);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static AtPwPsn PsnGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

/* Ethernet */
static eAtModulePwRet EthPortSet(AtPw self, AtEthPort ethPort)
    {
	AtUnused(ethPort);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static AtEthPort EthPortGet(AtPw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtModulePwRet EthFlowSet(AtPw self, AtEthFlow ethFlow)
    {
    AtUnused(self);
    AtUnused(ethFlow);
    /* Let concrete class do */
    return cAtError;
    }

static AtEthFlow EthFlowGet(AtPw self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtModulePwRet EthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
	AtUnused(sVlan);
	AtUnused(cVlan);
	AtUnused(destMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePwRet EthDestMacSet(AtPw self, uint8 *destMac)
    {
	AtUnused(destMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePwRet EthDestMacGet(AtPw self, uint8 *destMac)
    {
	AtUnused(destMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePwRet EthSrcMacSet(AtPw self, uint8 *srcMac)
    {
	AtUnused(srcMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePwRet EthSrcMacGet(AtPw self, uint8 *srcMac)
    {
	AtUnused(srcMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePwRet EthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
	AtUnused(sVlan);
	AtUnused(cVlan);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static tAtEthVlanTag *EthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
	AtUnused(cVlan);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static tAtEthVlanTag *EthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
	AtUnused(sVlan);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtRet SubPortVlanIndexSet(AtPw self, uint32 subPortVlanIndex)
    {
    /* Let concrete class do */
    AtUnused(subPortVlanIndex);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 SubPortVlanIndexGet(AtPw self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cInvalidUint32;
    }

/* Circuit binding */
static eAtModulePwRet CircuitBind(AtPw self, AtChannel circuit)
    {
	AtUnused(circuit);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static AtChannel BoundCircuitGet(AtPw self)
    {
	AtUnused(self);
    return NULL;
    }

static eAtModulePwRet EthExpectedCVlanSet(AtPw self, tAtEthVlanTag *cVlan)
    {
	AtUnused(cVlan);
	AtUnused(self);
    /* Concrete class must do */
    return cAtErrorModeNotSupport;
    }

static tAtEthVlanTag *EthExpectedCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
	AtUnused(cVlan);
	AtUnused(self);
    /* Concrete class must do */
    return NULL;
    }

static eAtModulePwRet EthExpectedSVlanSet(AtPw self, tAtEthVlanTag *sVlan)
    {
	AtUnused(sVlan);
	AtUnused(self);
    /* Concrete class must do */
    return cAtErrorModeNotSupport;
    }

static tAtEthVlanTag *EthExpectedSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
	AtUnused(sVlan);
	AtUnused(self);
    /* Concrete class must do */
    return NULL;
    }

static uint32 MinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(circuit);
	AtUnused(self);
    /* Concrete class must do */
    return 0;
    }

static uint32 MaxJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(circuit);
	AtUnused(self);
    /* Concrete class must do */
    return 0;
    }

static uint32 MinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(circuit);
	AtUnused(self);
    /* Concrete class must do */
    return 0;
    }

static uint32 MaxJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(circuit);
	AtUnused(self);
    /* Concrete class must do */
    return 0;
    }

static uint32 MaxJitterDelayForJitterBufferSize(AtPw self, uint32 jitterBufferSize_us)
    {
    AtUnused(self);
    AtUnused(jitterBufferSize_us);
    /* Concrete class must do */
    return 0;
    }

static uint16 MinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
	AtUnused(jitterBufferSize);
	AtUnused(circuit);
	AtUnused(self);
    /* Concrete class must do */
    return 0;
    }

static uint16 MaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
	AtUnused(jitterBufferSize);
	AtUnused(circuit);
	AtUnused(self);
    /* Concrete class must do */
    return 0;
    }

static eAtModulePwRet JitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(jitterDelay_us);
	AtUnused(jitterBufferSize_us);
	AtUnused(self);
    /* Concrete class must do */
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet JitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
	AtUnused(payloadSize);
	AtUnused(jitterDelay_pkt);
	AtUnused(jitterBufferSize_pkt);
	AtUnused(self);
    /* Concrete class must do */
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet IdleCodeSet(AtPw self, uint8 idleCode)
    {
	AtUnused(idleCode);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8  IdleCodeGet(AtPw self)
    {
	AtUnused(self);
    return 0;
    }

static eAtModulePwRet MissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 MissingPacketDefectThresholdGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numPackets);
    AtUnused(timeInSecond);
    return cAtErrorModeNotSupport;
    }

static uint32 ExcessivePacketLossRateDefectThresholdGet(AtPw self, uint32* timeInSecond)
    {
    /* Concrete class must do */
    AtUnused(self);
    if (timeInSecond)
    *timeInSecond = 0;
    return 0;
    }

static eAtModulePwRet StrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 StrayPacketDefectThresholdGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet MalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 MalformedPacketDefectThresholdGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet RemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 RemotePacketLossDefectThresholdGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numOverrunEvent);
    return cAtErrorModeNotSupport;
    }

static uint32 JitterBufferOverrunDefectThresholdGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numUnderrunEvent);
    return cAtErrorModeNotSupport;
    }

static uint32 JitterBufferUnderrunDefectThresholdGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet MisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 MisConnectionDefectThresholdGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static AtPwGroup ApsGroupGet(AtPw self)
    {
    return self->apsGroup;
    }

static AtPwGroup HsGroupGet(AtPw self)
    {
    return self->hsGroup;
    }

static eAtModulePwRet BackupPsnSet(AtPw self, AtPwPsn backupPsn)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(backupPsn);
    return cAtErrorNotImplemented;
    }

static AtPwPsn BackupPsnGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return NULL;
    }

static eAtModulePwRet BackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(destMac);
    AtUnused(cVlan);
    AtUnused(sVlan);
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet BackupEthDestMacSet(AtPw self, uint8 *destMac)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(destMac);
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet BackupEthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(srcMac);
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet BackupEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(srcMac);
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet BackupEthDestMacGet(AtPw self, uint8 *destMac)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(destMac);
    return cAtErrorNotImplemented;
    }

static eAtModulePwRet BackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(cVlan);
    AtUnused(sVlan);
    return cAtErrorNotImplemented;
    }

static tAtEthVlanTag* BackupEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(cVlan);
    return NULL;
    }

static tAtEthVlanTag* BackupEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(sVlan);
    return NULL;
    }

static void InterruptProcess(AtPw self, uint32 hwPw, AtHal hal)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(hwPw);
    AtUnused(hal);
    }
    
static eBool LopsClearThresholdIsSupported(AtPw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool MissingPacketDefectThresholdIsSupported(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet EthPortQueueSet(AtPw self, uint8 queueId)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(queueId);
    return cAtErrorNotImplemented;
    }

static uint8 EthPortQueueGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return 0;
    }

static eAtRet PtchInsertionModeSet(AtPw self, eAtPtchMode insertMode)
    {
    /* Concrete class must do */
    AtUnused(self);
    AtUnused(insertMode);
    return cAtErrorNotImplemented;
    }

static eAtPtchMode PtchInsertionModeGet(AtPw self)
    {
    /* Concrete class must do */
    AtUnused(self);
    return cAtPtchModeUnknown;
    }

static eAtModulePwRet TxActiveForce(AtPw self, eBool active)
    {
    AtUnused(self);
    return active ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxActiveIsForced(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsTdmPw(AtPw self)
    {
    eAtPwType pwType = AtPwTypeGet(self);

    if ((pwType == cAtPwTypeSAToP) ||
        (pwType == cAtPwTypeCESoP) ||
        (pwType == cAtPwTypeCEP))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 EthPortMaxNumQueues(AtPw self)
    {
    AtUnused(self);
    return cAtError;
    }

static eAtModulePwRet EthCVlanTpidSet(AtPw self, uint16 tpid)
    {
    AtUnused(self);
    AtUnused(tpid);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePwRet EthSVlanTpidSet(AtPw self, uint16 tpid)
    {
    AtUnused(self);
    AtUnused(tpid);
    /* Let concrete class do */
    return 0;
    }

static uint16 EthCVlanTpidGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static uint16 EthSVlanTpidGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PtchServiceEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PtchServiceIsEnabled(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 JitterBufferWatermarkMinPackets(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 JitterBufferWatermarkMaxPackets(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eBool JitterBufferWatermarkIsSupported(AtPw self)
    {
    /* Just some recent products support this and concrete must know */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet JitterBufferWatermarkReset(AtPw self)
    {
    /* Nothing to reset */
    AtUnused(self);
    return cAtOk;
    }

static uint32 JitterBufferNumBlocksGet(AtPw self)
    {
    AtUnused(self);
    /* Let concrete class decide */
    return 0;
    }

static uint16 MinJitterBufferDelayInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(payloadSize);
    return 0;
    }

static uint16 MinJitterBufferSizeInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    AtUnused(self);
    AtUnused(circuit);
    AtUnused(payloadSize);
    return 0;
    }

static void EthHeaderSetApiLogStart(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan,
                                    const char *file, uint32 line, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize;
        char *buffer, *cVlanString, *sVlanString, *macString;

        AtDriverLogLock();
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);

        /* Plus 1 for trailer '\0' */
        macString   = buffer;
        cVlanString = macString + cMacStringLen;
        sVlanString = cVlanString + cVlanStringLen + 1;

        macString = AtUtilBytes2String(destMac, cAtMacAddressLen, 16, macString, cMacStringLen);
        cVlanString = AtUtilVlanTag2String(cVlan, cVlanString, cVlanStringLen + 1);
        sVlanString = AtUtilVlanTag2String(sVlan, sVlanString, cVlanStringLen + 1);

        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                file, line, "API: %s([%s], %s, %s, %s)\r\n",
                                function, AtObjectToString((AtObject)self), macString, cVlanString, sVlanString);

        AtDriverLogUnLock();
        }
    }

static void EthVlanSetApiLogStart(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan,
                                  const char *file, uint32 line, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize;
        char *buffer, *cVlanString, *sVlanString;

        AtDriverLogLock();
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);

        /* Plus 1 for trailer '\0' */
        cVlanString = buffer;
        sVlanString = cVlanString + cVlanStringLen + 1;
        cVlanString = AtUtilVlanTag2String(cVlan, cVlanString, cVlanStringLen + 1);
        sVlanString = AtUtilVlanTag2String(sVlan, sVlanString, cVlanStringLen + 1);

        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                file, line, "API: %s([%s], %s, %s)\r\n",
                                function, AtObjectToString((AtObject)self), cVlanString, sVlanString);

        AtDriverLogUnLock();
        }
    }

static void MethodsInit(AtPw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, LopsThresholdMax);
        mMethodOverride(m_methods, LopsThresholdMin);
        mMethodOverride(m_methods, LopsSetThresholdSet);
        mMethodOverride(m_methods, LopsSetThresholdGet);
        mMethodOverride(m_methods, LopsClearThresholdSet);
        mMethodOverride(m_methods, LopsClearThresholdGet);
        mMethodOverride(m_methods, PayloadSizeSet);
        mMethodOverride(m_methods, PayloadSizeGet);
        mMethodOverride(m_methods, MinPayloadSize);
        mMethodOverride(m_methods, MaxPayloadSize);
        mMethodOverride(m_methods, PrioritySet);
        mMethodOverride(m_methods, PriorityGet);
        mMethodOverride(m_methods, ReorderingEnable);
        mMethodOverride(m_methods, ReorderingIsEnabled);
        mMethodOverride(m_methods, JitterBufferSizeSet);
        mMethodOverride(m_methods, JitterBufferSizeGet);
        mMethodOverride(m_methods, MaxJitterBufferSize);
        mMethodOverride(m_methods, MinJitterBufferSize);
        mMethodOverride(m_methods, JitterBufferDelaySet);
        mMethodOverride(m_methods, JitterBufferAndPayloadSizeSet);
        mMethodOverride(m_methods, JitterBufferInPacketAndPayloadSizeSet);
        mMethodOverride(m_methods, MinJitterDelay);
        mMethodOverride(m_methods, MaxJitterDelay);
        mMethodOverride(m_methods, MaxJitterDelayForJitterBufferSize);
        mMethodOverride(m_methods, JitterBufferDelayGet);
        mMethodOverride(m_methods, NumCurrentPacketsInJitterBuffer);
        mMethodOverride(m_methods, NumCurrentAdditionalBytesInJitterBuffer);
        mMethodOverride(m_methods, JitterBufferCenter);
        mMethodOverride(m_methods, JitterBufferWatermarkMinPackets);
        mMethodOverride(m_methods, JitterBufferWatermarkMaxPackets);
        mMethodOverride(m_methods, JitterBufferWatermarkIsSupported);
        mMethodOverride(m_methods, JitterBufferWatermarkReset);
        mMethodOverride(m_methods, RtpEnable);
        mMethodOverride(m_methods, RtpIsEnabled);
        mMethodOverride(m_methods, RtpPayloadTypeSet);
        mMethodOverride(m_methods, RtpPayloadTypeGet);
        mMethodOverride(m_methods, RtpTxSsrcSet);
        mMethodOverride(m_methods, RtpTxSsrcGet);
        mMethodOverride(m_methods, RtpExpectedSsrcSet);
        mMethodOverride(m_methods, RtpExpectedSsrcGet);
        mMethodOverride(m_methods, RtpSsrcSet);
        mMethodOverride(m_methods, RtpSsrcGet);
        mMethodOverride(m_methods, RtpTimeStampModeSet);
        mMethodOverride(m_methods, RtpTimeStampModeGet);
        mMethodOverride(m_methods, RtpTimeStampModeIsSupported);
        mMethodOverride(m_methods, CwEnable);
        mMethodOverride(m_methods, CwIsEnabled);
        mMethodOverride(m_methods, CwAutoTxLBitEnable);
        mMethodOverride(m_methods, CwAutoTxLBitIsEnabled);
        mMethodOverride(m_methods, CwAutoRxLBitEnable);
        mMethodOverride(m_methods, CwAutoRxLBitIsEnabled);
        mMethodOverride(m_methods, CwAutoRxLBitCanEnable);
        mMethodOverride(m_methods, CwAutoRBitEnable);
        mMethodOverride(m_methods, CwAutoRBitIsEnabled);
        mMethodOverride(m_methods, CwSequenceModeSet);
        mMethodOverride(m_methods, CwSequenceModeGet);
        mMethodOverride(m_methods, CwSequenceModeIsSupported);
        mMethodOverride(m_methods, CwLengthModeSet);
        mMethodOverride(m_methods, CwLengthModeGet);
        mMethodOverride(m_methods, CwLengthModeIsSupported);
        mMethodOverride(m_methods, CwPktReplaceModeSet);
        mMethodOverride(m_methods, CwPktReplaceModeGet);
        mMethodOverride(m_methods, LopsPktReplaceModeSet);
        mMethodOverride(m_methods, LopsPktReplaceModeGet);
        mMethodOverride(m_methods, CanControlLopsPktReplaceMode);
        mMethodOverride(m_methods, PktReplaceModeIsSupported);
        mMethodOverride(m_methods, PsnSet);
        mMethodOverride(m_methods, PsnGet);
        mMethodOverride(m_methods, EthPortSet);
        mMethodOverride(m_methods, EthPortGet);
        mMethodOverride(m_methods, EthFlowSet);
        mMethodOverride(m_methods, EthFlowGet);
        mMethodOverride(m_methods, EthHeaderSet);
        mMethodOverride(m_methods, EthDestMacSet);
        mMethodOverride(m_methods, EthDestMacGet);
        mMethodOverride(m_methods, EthSrcMacSet);
        mMethodOverride(m_methods, EthSrcMacGet);
        mMethodOverride(m_methods, EthVlanSet);
        mMethodOverride(m_methods, EthCVlanGet);
        mMethodOverride(m_methods, EthSVlanGet);
        mMethodOverride(m_methods, SubPortVlanIndexSet);
        mMethodOverride(m_methods, SubPortVlanIndexGet);
        mMethodOverride(m_methods, EthCVlanTpidSet);
        mMethodOverride(m_methods, EthSVlanTpidSet);
        mMethodOverride(m_methods, EthCVlanTpidGet);
        mMethodOverride(m_methods, EthSVlanTpidGet);
        mMethodOverride(m_methods, EthExpectedCVlanSet);
        mMethodOverride(m_methods, EthExpectedCVlanGet);
        mMethodOverride(m_methods, EthExpectedSVlanSet);
        mMethodOverride(m_methods, EthExpectedSVlanGet);
        mMethodOverride(m_methods, CircuitBind);
        mMethodOverride(m_methods, CircuitUnbind);
        mMethodOverride(m_methods, BoundCircuitGet);
        mMethodOverride(m_methods, SuppressEnable);
        mMethodOverride(m_methods, SuppressIsEnabled);
        mMethodOverride(m_methods, JitterBufferSizeInPacketSet);
        mMethodOverride(m_methods, JitterBufferSizeInPacketGet);
        mMethodOverride(m_methods, JitterBufferDelayInPacketSet);
        mMethodOverride(m_methods, JitterBufferDelayInPacketGet);
        mMethodOverride(m_methods, MinJitterBufferDelayInPacketGet);
        mMethodOverride(m_methods, MinJitterBufferSizeInPacketGet);
        mMethodOverride(m_methods, RtpTxPayloadTypeSet);
        mMethodOverride(m_methods, RtpTxPayloadTypeGet);
        mMethodOverride(m_methods, RtpExpectedPayloadTypeSet);
        mMethodOverride(m_methods, RtpExpectedPayloadTypeGet);
        mMethodOverride(m_methods, RtpPayloadTypeCompare);
        mMethodOverride(m_methods, RtpPayloadTypeIsCompared);
        mMethodOverride(m_methods, RtpSsrcCompare);
        mMethodOverride(m_methods, RtpSsrcIsCompared);
        mMethodOverride(m_methods, IdleCodeSet);
        mMethodOverride(m_methods, IdleCodeGet);
        mMethodOverride(m_methods, MissingPacketDefectThresholdSet);
        mMethodOverride(m_methods, MissingPacketDefectThresholdGet);
        mMethodOverride(m_methods, ExcessivePacketLossRateDefectThresholdSet);
        mMethodOverride(m_methods, ExcessivePacketLossRateDefectThresholdGet);
        mMethodOverride(m_methods, StrayPacketDefectThresholdSet);
        mMethodOverride(m_methods, StrayPacketDefectThresholdGet);
        mMethodOverride(m_methods, MalformedPacketDefectThresholdSet);
        mMethodOverride(m_methods, MalformedPacketDefectThresholdGet);
        mMethodOverride(m_methods, RemotePacketLossDefectThresholdSet);
        mMethodOverride(m_methods, RemotePacketLossDefectThresholdGet);
        mMethodOverride(m_methods, JitterBufferUnderrunDefectThresholdSet);
        mMethodOverride(m_methods, JitterBufferUnderrunDefectThresholdGet);
        mMethodOverride(m_methods, JitterBufferOverrunDefectThresholdSet);
        mMethodOverride(m_methods, JitterBufferOverrunDefectThresholdGet);
        mMethodOverride(m_methods, MisConnectionDefectThresholdSet);
        mMethodOverride(m_methods, MisConnectionDefectThresholdGet);

        mMethodOverride(m_methods, ApsGroupGet);
        mMethodOverride(m_methods, HsGroupGet);
        mMethodOverride(m_methods, BackupPsnSet);
        mMethodOverride(m_methods, BackupPsnGet);
        mMethodOverride(m_methods, BackupEthHeaderSet);
        mMethodOverride(m_methods, BackupEthDestMacSet);
        mMethodOverride(m_methods, BackupEthDestMacGet);
        mMethodOverride(m_methods, BackupEthSrcMacSet);
        mMethodOverride(m_methods, BackupEthSrcMacGet);
        mMethodOverride(m_methods, BackupEthVlanSet);
        mMethodOverride(m_methods, BackupEthCVlanGet);
        mMethodOverride(m_methods, BackupEthSVlanGet);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, LopsClearThresholdIsSupported);
        mMethodOverride(m_methods, MissingPacketDefectThresholdIsSupported);

        /* For standby driver */
        mMethodOverride(m_methods, CachePayloadSizeSet);
        mMethodOverride(m_methods, CachePayloadSizeGet);

        mMethodOverride(m_methods, EthPortQueueSet);
        mMethodOverride(m_methods, EthPortQueueGet);
        mMethodOverride(m_methods, EthPortMaxNumQueues);

        mMethodOverride(m_methods, PtchInsertionModeSet);
        mMethodOverride(m_methods, PtchInsertionModeGet);
        mMethodOverride(m_methods, PtchServiceEnable);
        mMethodOverride(m_methods, PtchServiceIsEnabled);

        mMethodOverride(m_methods, TxActiveForce);
        mMethodOverride(m_methods, TxActiveIsForced);

        /* RAM blocks for jitter buffer */
        mMethodOverride(m_methods, JitterBufferNumBlocksGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtPw AtPwObjectInit(AtPw self, uint32 pwId, AtModulePw module, eAtPwType pwType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPw));

    /* Super constructor should be called first */
    if (AtChannelObjectInit((AtChannel)self, pwId, (AtModule)module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->pwType = (uint8)pwType;

    return self;
    }

eAtModulePwRet AtPwCircuitBindingCheck(AtPw self, AtChannel circuit)
    {
    AtPw boundPw;

    /* Check if it is busy */
    if (AtPwBoundCircuitGet(self) && (AtPwBoundCircuitGet(self) != circuit))
        return cAtErrorChannelBusy;

    /* The circuit is also busy */
    boundPw = AtChannelBoundPwGet(circuit);
    if (boundPw && (boundPw != self))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

void AtPwApsGroupSet(AtPw self, AtPwGroup apsGroup)
    {
    if (self)
        self->apsGroup = apsGroup;
    }

void AtPwHsGroupSet(AtPw self, AtPwGroup hsGroup)
    {
    if (self)
        self->hsGroup = hsGroup;
    }

void AtPwInterruptProcess(AtPw self, uint32 hwPw, AtHal hal)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self, hwPw, hal);
    }

eBool AtPwLopsClearThresholdIsSupported(AtPw self)
    {
    if (self)
        return mMethodsGet(self)->LopsClearThresholdIsSupported(self);
    return cAtFalse;
    }

eBool AtPwMissingPacketDefectThresholdIsSupported(AtPw self)
    {
    if (self)
        return mMethodsGet(self)->MissingPacketDefectThresholdIsSupported(self);
    return cAtFalse;
    }

eAtRet AtPwPtchInsertionModeSet(AtPw self, eAtPtchMode insertMode)
    {
    if (self)
        return mMethodsGet(self)->PtchInsertionModeSet(self, insertMode);
    return cAtErrorObjectNotExist;
    }

eAtPtchMode AtPwPtchInsertionModeGet(AtPw self)
    {
    if (self)
        return mMethodsGet(self)->PtchInsertionModeGet(self);
    return cAtPtchModeUnknown;
    }

eAtRet AtPwPtchServiceEnable(AtPw self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PtchServiceEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

eBool AtPwPtchServiceIsEnabled(AtPw self)
    {
    if (self)
        return mMethodsGet(self)->PtchServiceIsEnabled(self);
    return cAtFalse;
    }

eAtModulePwRet AtPwTxActiveForce(AtPw self, eBool active)
    {
    if (self)
        return mMethodsGet(self)->TxActiveForce(self, active);
    return cAtErrorNullPointer;
    }

eBool AtPwTxActiveIsForced(AtPw self)
    {
    if (self)
        return mMethodsGet(self)->TxActiveIsForced(self);
    return cAtFalse;
    }

/*
 * Set Subport VLAN Index.
 *
 * @param self This pseudowire
 * @param subPortVlanIndex Index select Subport VLAN from PW module
 *
 * @return AT return code
 */
eAtRet AtPwSubPortVlanIndexSet(AtPw self, uint32 subPortVlanIndex)
    {
    mNumericalAttributeSet(SubPortVlanIndexSet, subPortVlanIndex);
    }

/*
 * Get Subport VLAN Index.
 *
 * @param self This pseudowire
 *
 * @return Index selects Subport VLAN from PW module
 */
uint32 AtPwSubPortVlanIndexGet(AtPw self)
    {
    mAttributeGet(SubPortVlanIndexGet, uint32, cInvalidUint32);
    }

eBool AtPwNeedInitConfig(AtPw self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModulePrbs module = (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    if (module && (AtModulePrbsNeedPwInitConfig(module) == cAtFalse))
        return cAtFalse;
    return cAtTrue;
    }

/**
 * @addtogroup AtPw
 * @{
 */

/**
 * Get Pseudowire type
 *
 * @param self This pseudowire
 *
 * @return @ref eAtPwType "Pseudowire types"
 */
eAtPwType AtPwTypeGet(AtPw self)
    {
    if (self)
        return self->pwType;

    return cAtPwTypeInvalid;
    }

/**
 * Get max LOPS threshold
 *
 * @param self This pseudowire
 *
 * @return Max LOPS threshold
 */
uint32 AtPwLopsThresholdMax(AtPw self)
    {
    mAttributeGet(LopsThresholdMax, uint32, 0);
    }

/**
 * Get min LOPS threshold
 *
 * @param self This pseudowire
 *
 * @return Min LOPS threshold
 */
uint32 AtPwLopsThresholdMin(AtPw self)
    {
    mAttributeGet(LopsThresholdMin, uint32, 0);
    }

/**
 * Set number of error packets to declare lost of packet synchronization in case of CEP or
 * lost of packet state in case of CESoP
 *
 * @param self This pseudowire
 * @param numPackets Number of error packets to declare lost of packet synchronization
 *
 * @return AT return code
 */
eAtModulePwRet AtPwLopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    mNumericalAttributeSetWithCache(LopsSetThresholdSet, numPackets);
    }

/**
 * Get number of error packets that is used to declare lost of packet synchronization
 *
 * @param self This pseudowire
 *
 * @return Number of error packets to declare lost of packet synchronization
 */
uint32 AtPwLopsSetThresholdGet(AtPw self)
    {
    mAttributeGetWithCache(LopsSetThresholdGet, uint32, 0);
    }

/**
 * Set number of good packets to clear lost of packet synchronization condition
 *
 * @param self This pseudowire
 * @param numPackets Number of good packets to clear lost of packet synchronization condition
 *
 * @return AT return code
 */
eAtModulePwRet AtPwLopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    mNumericalAttributeSetWithCache(LopsClearThresholdSet, numPackets);
    }

/**
 * Get number of good packets that is used to clear lost of packet synchronization condition
 *
 * @param self This pseudowire
 *
 * @return Number of good packets to clear lost of packet synchronization condition
 */
uint32 AtPwLopsClearThresholdGet(AtPw self)
    {
    mAttributeGetWithCache(LopsClearThresholdGet, uint32, 0);
    }

/**
 * Get minimum payload size corresponding to circuit speed and jitter buffer size
 *
 * @param self This pseudowire
 * @param circuit Circuit
 * @param jitterBufferSize Jitter buffer size
 *
 * @return Minimum payload size
 */
uint16 AtPwMinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    if (self)
        return mMethodsGet(self)->MinPayloadSize(self, circuit, jitterBufferSize);
    return 0;
    }

/**
 * Get maximum payload size corresponding to circuit speed and jitter buffer size
 *
 * @param self This pseudowire
 * @param circuit Circuit
 * @param jitterBufferSize Jitter buffer size
 *
 * @return Maximum payload size
 */
uint16 AtPwMaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize)
    {
    if (self)
        return mMethodsGet(self)->MaxPayloadSize(self, circuit, jitterBufferSize);
    return 0;
    }

/**
 * Set payload size
 *
 * @param self This pseudowire
 * @param payloadSize Payload size:
 *                    - In case of SAToP, CEP basic: unit is in bytes
 *                    - In case of CESoP: unit is number of frames per packet
 *                    - In case of CEP fractional: unit is number of rows in 9-rows SPE
 *
 * @return AT return code
 */
eAtModulePwRet AtPwPayloadSizeSet(AtPw self, uint16 payloadSize)
    {
    mNumericalAttributeSetWithCacheMethod(PayloadSizeSet, payloadSize);
    }

/**
 * Get payload size
 *
 * @param self This pseudowire
 *
 * @return Payload size
 *         - In case of SAToP, CEP: unit is in bytes
 *         - In case of CESoP: unit is number of frames per packet
 */
uint16 AtPwPayloadSizeGet(AtPw self)
    {
    mAttributeGetWithCacheMethod(PayloadSizeGet, uint16, 0);
    }

/**
 * Set priority on direction to Ethernet
 *
 * @param self This pseudowire
 * @param priority Priority
 *
 * @return AT return code
 */
eAtModulePwRet AtPwPrioritySet(AtPw self, uint8 priority)
    {
    mNumericalAttributeSet(PrioritySet, priority);
    }

/**
 * Get priority on direction to Ethernet
 *
 * @param self This pseudowire
 *
 * @return Pseudowire priority
 */
uint8 AtPwPriorityGet(AtPw self)
    {
    mAttributeGet(PriorityGet, uint8, 0);
    }

/**
 * Enable/disable reordering
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwReorderingEnable(AtPw self, eBool enable)
    {
    mNumericalAttributeSet(ReorderingEnable, enable);
    }

/**
 * Check if reordering is enabled
 *
 * @param self This pseudowire
 *
 * @return cAtTrue if reordering is enabled, otherwise, cAtFalse is returned
 */
eBool AtPwReorderingIsEnabled(AtPw self)
    {
    mAttributeGet(ReorderingIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable suppression
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwSuppressEnable(AtPw self, eBool enable)
    {
    mNumericalAttributeSet(SuppressEnable, enable);
    }

/**
 * Check if suppression is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue Suppression is enabled
 * @retval cAtFalse Suppression is disabled
 */
eBool AtPwSuppressIsEnabled(AtPw self)
    {
    mAttributeGet(SuppressIsEnabled, eBool, cAtFalse);
    }

/**
 * Set jitter buffer size in microseconds
 *
 * @param self This pseudowire
 * @param microseconds Jitter buffer size in microseconds. It should be multiple
 *                     of 125us
 *
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferSizeSet(AtPw self, uint32 microseconds)
    {
    mNumericalAttributeSet(JitterBufferSizeSet, microseconds);
    }

/**
 * Get jitter buffer size in microseconds
 *
 * @param self This pseudowire
 *
 * @return Jitter buffer size in microseconds
 */
uint32 AtPwJitterBufferSizeGet(AtPw self)
    {
    mAttributeGet(JitterBufferSizeGet, uint32, 0);
    }

/**
 * Get the minimum jitter buffer size
 *
 * @param self This PW
 * @param circuit Circuit
 * @param payloadSize Payload size. Note, in case of CESoP, the payload size is
 *        in frame unit, not byte unit.
 *
 * @return Minimum jitter buffer size
 */
uint32 AtPwMinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinJitterBufferSize(self, circuit, payloadSize);

    return 0;
    }

/**
 * Get the maximum jitter buffer size
 *
 * @param self This PW
 * @param circuit Circuit
 * @param payloadSize Payload size. Note, in case of CESoP, the payload size is
 *        in frame unit, not byte unit.
 *
 * @return Maximum jitter buffer size
 */
uint32 AtPwMaxJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MaxJitterBufferSize(self, circuit, payloadSize);

    return 0;
    }

/**
 * Set jitter buffer size in number of packet
 *
 * @param self This pseudowire
 * @param numPackets Jitter buffer size in packet unit
 *
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferSizeInPacketSet(AtPw self, uint16 numPackets)
    {
    mNumericalAttributeSet(JitterBufferSizeInPacketSet, numPackets);
    }

/**
 * Get jitter buffer size in number of packet
 *
 * @param self This pseudowire
 *
 * @return Jitter buffer size in packet unit
 */
uint16 AtPwJitterBufferSizeInPacketGet(AtPw self)
    {
    mAttributeGet(JitterBufferSizeInPacketGet, uint16, 0);
    }

/**
 * Get the minimum jitter delay
 *
 * @param self This PW
 * @param circuit Circuit
 * @param payloadSize Payload size. Note, in case of CESoP, the payload size is
 *        in frame unit, not byte unit.
 *
 * @return Minimum jitter delay
 */
uint32 AtPwMinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinJitterDelay(self, circuit, payloadSize);

    return 0;
    }

/**
 * Get the maximum jitter delay
 *
 * @param self This PW
 * @param circuit Circuit
 * @param payloadSize Payload size. Note, in case of CESoP, the payload size is
 *        in frame unit, not byte unit.
 *
 * @return Maximum jitter delay
 */
uint32 AtPwMaxJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MaxJitterDelay(self, circuit, payloadSize);

    return 0;
    }

/**
 * Get the maximum jitter delay applicable for a given jitter buffer
 *
 * @param self This PW
 * @param jitterBufferSize_us Jitter buffer size in microseconds.
 *
 * @return Maximum jitter delay
 */
uint32 AtPwMaxJitterDelayForJitterBufferSize(AtPw self, uint32 jitterBufferSize_us)
    {
    if (self)
        return mMethodsGet(self)->MaxJitterDelayForJitterBufferSize(self, jitterBufferSize_us);
    return 0;
    }

/**
 * Set jitter buffer delay
 *
 * @param self This pseudowire
 * @param microseconds Jitter buffer delay in microsecond. Its value should be
 *        multiple of 125us
 *
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferDelaySet(AtPw self, uint32 microseconds)
    {
    mNumericalAttributeSet(JitterBufferDelaySet, microseconds);
    }

/**
 * Get jitter buffer delay
 *
 * @param self This pseudowire
 *
 * @return Jitter buffer delay in us
 */
uint32 AtPwJitterBufferDelayGet(AtPw self)
    {
    mAttributeGet(JitterBufferDelayGet, uint32, 0);
    }

/**
 * Set jitter buffer delay in number of packet
 *
 * @param self This pseudowire
 *
 * @param numPackets Jitter buffer delay in packet unit
 *
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferDelayInPacketSet(AtPw self, uint16 numPackets)
    {
    mNumericalAttributeSet(JitterBufferDelayInPacketSet, numPackets);
    }

/**
 * Get jitter buffer delay in number of packet
 *
 * @param self This pseudowire
 *
 * @return Jitter buffer delay in packet unit
 */
uint16 AtPwJitterBufferDelayInPacketGet(AtPw self)
    {
    mAttributeGet(JitterBufferDelayInPacketGet, uint16, 0);
    }

/**
 * Get minimum jitter buffer delay in number of packets
 *
 * @param self This pseudowire
 * @param circuit Circuit
 * @param payloadSize Payload size. Note, in case of CESoP, the payload size is
 *        in frame unit, not byte unit.
 *
 * @return Minimum jitter buffer delay in packet unit
 */
uint16 AtPwMinJitterBufferDelayInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinJitterBufferDelayInPacketGet(self, circuit, payloadSize);
    return 0;
    }

/**
 * Get minimum jitter buffer size in number of packets
 *
 * @param self This pseudowire
 * @param circuit Circuit
 * @param payloadSize Payload size. Note, in case of CESoP, the payload size is
 *        in frame unit, not byte unit.
 *
 * @return Minimum jitter buffer size in packet unit
 */
uint16 AtPwMinJitterBufferSizeInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize)
    {
    if (self)
        return mMethodsGet(self)->MinJitterBufferSizeInPacketGet(self, circuit, payloadSize);
    return 0;
    }

/**
 * Set jitter buffer size, delay in microseconds and payload size at a time
 *
 * @param self This pseudowire
 * @param jitterBufferSize_us Jitter buffer size in microseconds
 * @param jitterDelay_us Jitter delay in microseconds
 * @param payloadSize Payload size.
 *                    - In frame: for CESoP
 *                    - In byte: for SAToP and CEP
 *
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
    mThreeParamsAttributeSet(JitterBufferAndPayloadSizeSet, jitterBufferSize_us, jitterDelay_us, payloadSize);
    }

/**
 * Set jitter buffer size, delay in packets and payload size at a time
 *
 * @param self This pseudowire
 * @param jitterBufferSize_pkt Jitter buffer size in packets
 * @param jitterDelay_pkt Jitter delay in packets
 * @param payloadSize Payload size.
 *                    - In frame: for CESoP
 *                    - In byte: for SAToP and CEP
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
    mThreeParamsAttributeSet(JitterBufferInPacketAndPayloadSizeSet, jitterBufferSize_pkt, jitterDelay_pkt, payloadSize);
    }

/**
 * To get number of packets that are in jitter buffer
 *
 * @param self This pseudowire
 * @return Number of packets that are in jitter buffer
 */
uint32 AtPwNumCurrentPacketsInJitterBuffer(AtPw self)
    {
    mAttributeGet(NumCurrentPacketsInJitterBuffer, uint32, 0);
    }

/**
 * To get number of additional bytes that are in jitter buffer
 *
 * @param self This pseudowire
 * @return Number of additional bytes that are in jitter buffer
 */
uint32 AtPwNumCurrentAdditionalBytesInJitterBuffer(AtPw self)
    {
    mAttributeGet(NumCurrentAdditionalBytesInJitterBuffer, uint32, 0);
    }

/**
 * Make jitter buffer be center
 *
 * @param self This pseudowire
 *
 * @return AT return code
 *
 * @note This operation impacts running traffic on PW
 */
eAtRet AtPwJitterBufferCenter(AtPw self)
    {
    mNoParamCall(JitterBufferCenter, eAtRet, cAtErrorNullPointer);
    }

/**
 * Get minimum jitter buffer level
 *
 * @param self This pseudowire
 * @return Minimum jitter buffer level in packets
 */
uint32 AtPwJitterBufferWatermarkMinPackets(AtPw self)
    {
    mAttributeGet(JitterBufferWatermarkMinPackets, uint32, 0);
    }

/**
 * Get maximum jitter buffer level
 *
 * @param self This pseudowire
 * @return Maximum jitter buffer level in packets
 */
uint32 AtPwJitterBufferWatermarkMaxPackets(AtPw self)
    {
    mAttributeGet(JitterBufferWatermarkMaxPackets, uint32, 0);
    }

/**
 * Check if jitter buffer watermark is supported or not
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPwJitterBufferWatermarkIsSupported(AtPw self)
    {
    mAttributeGet(JitterBufferWatermarkIsSupported, eBool, cAtFalse);
    }

/**
 * Reset jitter buffer watermark. This should be done after pseudowire is up
 *
 * @param self This pseudowire
 *
 * @return AT return code
 */
eAtRet AtPwJitterBufferWatermarkReset(AtPw self)
    {
    mNoParamCall(JitterBufferWatermarkReset, eAtRet, cAtErrorObjectNotExist);
    }

/**
 * Enable/disable RTP
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwRtpEnable(AtPw self, eBool enable)
    {
    mNumericalAttributeSetWithCache(RtpEnable, enable);
    }

/**
 * Check if RTP is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue RTP is enabled
 * @retval cAtFalse RTP is disabled
 */
eBool AtPwRtpIsEnabled(AtPw self)
    {
    mAttributeGetWithCache(RtpIsEnabled, eBool, cAtFalse);
    }

/**
 * Set value to RTP payload type field. The transmitted value and expected value
 * are set to the same value.
 *
 * @param self This pseudowire
 * @param payloadType Payload type value
 *
 * @return AT return code
 *
 * @see AtPwRtpTxPayloadTypeSet(), AtPwRtpExpectedPayloadTypeSet()
 */
eAtModulePwRet AtPwRtpPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    mNumericalAttributeSet(RtpPayloadTypeSet, payloadType);
    }

/**
 * Get value of RTP payload type field
 *
 * @param self This pseudowire
 *
 * @return Value of RTP payload type field
 */
uint8 AtPwRtpPayloadTypeGet(AtPw self)
    {
    mAttributeGet(RtpPayloadTypeGet, uint8, 0);
    }

/**
 * Set value to RTP SSRC field
 *
 * @param self This pseudowire
 * @param ssrc SSRC value
 *
 * @return AT return code
 */
eAtModulePwRet AtPwRtpSsrcSet(AtPw self, uint32 ssrc)
    {
    mNumericalAttributeSet(RtpSsrcSet, ssrc);
    }

/**
 * Set TX RTP-SSRC
 *
 * @param self pseudowire
 * @param ssrc TX SSRC
 *
 * @return AT return code
 */
eAtModulePwRet AtPwRtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
    mNumericalAttributeSetWithCache(RtpTxSsrcSet, ssrc);
    }

/**
 * Get TX RTP-SSRC
 *
 * @param self pseudowire
 *
 * @return TX SSRC
 */
uint32 AtPwRtpTxSsrcGet(AtPw self)
    {
    mAttributeGetWithCache(RtpTxSsrcGet, uint32, 0);
    }

/**
 * Set expected RTP-SSRC
 *
 * @param self pseudowire
 * @param ssrc Expected SSRC
 *
 * @return AT return code
 */
eAtModulePwRet AtPwRtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
    mNumericalAttributeSetWithCache(RtpExpectedSsrcSet, ssrc);
    }

/**
 * Get expected RTP-SSRC
 *
 * @param self pseudowire
 *
 * @return expected SSRC
 */
uint32 AtPwRtpExpectedSsrcGet(AtPw self)
    {
    mAttributeGetWithCache(RtpExpectedSsrcGet, uint32, 0);
    }

/**
 * Get value of RTP SSRC field
 *
 * @param self This pseudowire
 *
 * @return SSRC value
 */
uint32 AtPwRtpSsrcGet(AtPw self)
    {
    mAttributeGet(RtpSsrcGet, uint32, 0);
    }

/**
 * Set RTP time stamp mode
 *
 * @param self This pseudowire
 * @param timeStampMode @ref eAtPwRtpTimeStampMode "RTP time stamp mode"
 *
 * @return AT return code
 */
eAtModulePwRet AtPwRtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    mNumericalAttributeSetWithCache(RtpTimeStampModeSet, timeStampMode);
    }

/**
 * Get RTP time stamp mode
 *
 * @param self This pseudowire
 *
 * @return @ref eAtPwRtpTimeStampMode "RTP time stamp mode"
 */
eAtPwRtpTimeStampMode AtPwRtpTimeStampModeGet(AtPw self)
    {
    mAttributeGetWithCache(RtpTimeStampModeGet, eAtPwRtpTimeStampMode, cAtPwRtpTimeStampModeInvalid);
    }

/**
 * Enable/disable control word.
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCwEnable(AtPw self, eBool enable)
    {
    mNumericalAttributeSet(CwEnable, enable);
    }

/**
 * Check if control word is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue Control word is enabled
 * @retval cAtFalse Control word is disabled
 */
eBool AtPwCwIsEnabled(AtPw self)
    {
    mAttributeGet(CwIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable auto transmitting TX L-bit packets when attachment circuit is fail
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCwAutoTxLBitEnable(AtPw self, eBool enable)
    {
    mNumericalAttributeSetWithCache(CwAutoTxLBitEnable, enable);
    }

/**
 * Check if auto transmitting TX L-bit packets is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue Auto transmitting TX L-bit packets is enabled
 * @retval cAtFalse Auto transmitting TX L-bit packets is disabled
 */
eBool AtPwCwAutoTxLBitIsEnabled(AtPw self)
    {
    mAttributeGetWithCache(CwAutoTxLBitIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable auto AIS generating to attachment circuit side when receiving
 * L-Bit packets
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCwAutoRxLBitEnable(AtPw self, eBool enable)
    {
    mNumericalAttributeSetWithCache(CwAutoRxLBitEnable, enable);
    }

/**
 * Check if auto AIS generating to attachment circuit side when receiving
 * L-Bit packets is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue Auto AIS generating is enabled
 * @retval cAtFalse Auto AIS generating is disabled
 */
eBool AtPwCwAutoRxLBitIsEnabled(AtPw self)
    {
    mAttributeGetWithCache(CwAutoRxLBitIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable auto transmitting TX R-Bit packets on entering lost of packet
 * condition
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCwAutoRBitEnable(AtPw self, eBool enable)
    {
    mNumericalAttributeSetWithCache(CwAutoRBitEnable, enable);
    }

/**
 * Check if auto transmitting TX R-Bit packets on entering lost of packet
 * condition is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue auto transmitting TX R-Bit is enabled
 * @retval cAtFalse auto transmitting TX R-Bit is disabled
 */
eBool AtPwCwAutoRBitIsEnabled(AtPw self)
    {
    mAttributeGetWithCache(CwAutoRBitIsEnabled, eBool, cAtFalse);
    }

/**
 * Set control word sequence mode
 *
 * @param self This pseudowire
 * @param sequenceMode @ref eAtPwCwSequenceMode "Control word sequence modes"
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    mNumericalAttributeSet(CwSequenceModeSet, sequenceMode);
    }

/**
 * Get control word sequence mode
 *
 * @param self This pseudowire
 *
 * @return @ref eAtPwCwSequenceMode "Control word sequence modes"
 */
eAtPwCwSequenceMode AtPwCwSequenceModeGet(AtPw self)
    {
    mAttributeGet(CwSequenceModeGet, eAtPwCwSequenceMode, cAtPwCwSequenceModeDisable);
    }

/**
 * Set control word length mode
 *
 * @param self This pseudowire
 * @param lengthMode @ref eAtPwCwLengthMode "Control word length modes"
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCwLengthModeSet(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    mNumericalAttributeSet(CwLengthModeSet, lengthMode);
    }

/**
 * Get control word length mode
 *
 * @param self This pseudowire
 *
 * @return @ref eAtPwCwLengthMode "Control word length modes"
 */
eAtPwCwLengthMode AtPwCwLengthModeGet(AtPw self)
    {
    mAttributeGet(CwLengthModeGet, eAtPwCwLengthMode, cAtPwCwLengthModePayload);
    }

/**
 * Set packet replacement mode when PW received packets with L-Bit set
 *
 * @param self This pseudowire
 * @param pktReplaceMode @ref eAtPwPktReplaceMode "Packet replacement modes"
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    if (!AtPwPktReplaceModeIsSupported(self, pktReplaceMode))
        return cAtErrorInvlParm;

    mNumericalAttributeSetWithCache(CwPktReplaceModeSet, pktReplaceMode);
    }

/**
 * Get packet replacement mode when PW received packets with L-Bit set
 *
 * @param self This pseudowire
 *
 * @return @ref eAtPwPktReplaceMode "Packet replacement modes"
 */
eAtPwPktReplaceMode AtPwCwPktReplaceModeGet(AtPw self)
    {
    mAttributeGetWithCache(CwPktReplaceModeGet, eAtPwPktReplaceMode, cAtPwPktReplaceModeAis);
    }

/**
 * Check if packet replacement mode is supported
 *
 * @param self This pseudowire
 * @param pktReplaceMode Packet replacement mode
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPwPktReplaceModeIsSupported(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    mOneParamAttributeGet(PktReplaceModeIsSupported, pktReplaceMode, eBool, cAtFalse);
    }

/**
 * Set PSN layer
 *
 * @param self This pseudowire
 * @param psn PSN layer
 *
 * @return AT return code
 */
eAtModulePwRet AtPwPsnSet(AtPw self, AtPwPsn psn)
    {
    mObjectSet(PsnSet, psn);
    }

/**
 * Get PSN layer
 *
 * @param self This pseudowire
 *
 * @return PSN layer
 */
AtPwPsn AtPwPsnGet(AtPw self)
    {
    mNoParamObjectGet(PsnGet, AtPwPsn);
    }

/**
 * Set Ethernet port
 *
 * @param self This pseudowire
 * @param ethPort Ethernet port
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthPortSet(AtPw self, AtEthPort ethPort)
    {
    mObjectSet(EthPortSet, ethPort);
    }

/**
 * Get Ethernet port
 *
 * @param self This pseudowire
 *
 * @return Ethernet port
 */
AtEthPort AtPwEthPortGet(AtPw self)
    {
    mNoParamObjectGet(EthPortGet, AtEthPort);
    }

/**
 * Set Ethernet flow
 *
 * @param self This pseudowire
 * @param ethFlow Ethernet flow
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthFlowSet(AtPw self, AtEthFlow ethFlow)
    {
    mObjectSet(EthFlowSet, ethFlow);
    }

/**
 * Get Ethernet flow
 *
 * @param self This pseudowire
 *
 * @return Ethernet flow
 */
AtEthFlow AtPwEthFlowGet(AtPw self)
    {
    mNoParamObjectGet(EthFlowGet, AtEthFlow);
    }

/**
 * Set Ethernet header
 *
 * @param self This pseudowire
 * @param destMac Destination MAC address, this must be an array of 6 bytes
 * @param cVlan C-VLAN. If it is NULL, sVlan parameter is ignored. No VLAN is
 *              used in this case
 * @param sVlan S-VLAN. Can be NULL, and only C-VLAN is used in this case
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    eAtRet ret = cAtErrorObjectNotExist;

    EthHeaderSetApiLogStart(self, destMac, cVlan, sVlan, AtSourceLocationNone, AtFunction);

    if (self)
        {
        CacheEthHeaderSet(self, destMac, cVlan, sVlan);
        ret = mMethodsGet(self)->EthHeaderSet(self, destMac, cVlan, sVlan);
        }

    AtDriverApiLogStop();

    return ret;
    }

/**
 * Set Ethernet destination MAC address
 *
 * @param self
 * @param [in] destMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthDestMacSet(AtPw self, uint8 *destMac)
    {
    mMacAttributeSetWithCache(EthDestMacSet, destMac);
    }

/**
 * Get Ethernet destination MAC address
 *
 * @param self
 * @param [out] destMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthDestMacGet(AtPw self, uint8 *destMac)
    {
    mGeneralAttributeGetWithCache(EthDestMacGet, destMac);
    }

/**
 * Set Ethernet source MAC address
 *
 * @param self
 * @param [out] srcMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    mMacAttributeSetWithCache(EthSrcMacSet, srcMac);
    }

/**
 * Get Ethernet source MAC address
 *
 * @param self
 * @param [out] srcMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    mGeneralAttributeGetWithCache(EthSrcMacGet, srcMac);
    }

/**
 * Set VLAN header
 *
 * @param self This pseudowire
 * @param cVlan C-VLAN. If it is NULL, sVlan parameter is ignored. No VLAN is
 *              used in this case
 * @param sVlan S-VLAN. Can be NULL, and only C-VLAN is used in this case
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    eAtModulePwRet ret = cAtErrorObjectNotExist;

    EthVlanSetApiLogStart(self, cVlan, sVlan, AtSourceLocationNone, AtFunction);

    if (self)
        {
        CacheEthVlanSet(self, cVlan, sVlan);
        ret = mMethodsGet(self)->EthVlanSet(self, cVlan, sVlan);
        }

    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get C-VLAN
 *
 * @param self This pseudowire
 * @param cVlan Structure to hold C-VLAN information
 *
 * @return cVlan if C-VLAN exists, otherwise NULL is returned
 */
tAtEthVlanTag *AtPwEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    mOneParamAttributeGetWithCache(EthCVlanGet, cVlan, tAtEthVlanTag *, NULL);
    }

/**
 * Set TPID for C-VLANs
 *
 * @param self This pseudowire
 * @param tpid Value TPID set for C-VLANs
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthCVlanTpidSet(AtPw self, uint16 tpid)
    {
    mNumericalAttributeSetWithCache(EthCVlanTpidSet, tpid);
    }

/**
 * Set TPID for S-VLANs
 *
 * @param self This pseudowire
 * @param tpid Value TPID set for S-VLANs
 *
 * @return AT return code
 */
eAtModulePwRet AtPwEthSVlanTpidSet(AtPw self, uint16 tpid)
    {
    mNumericalAttributeSetWithCache(EthSVlanTpidSet, tpid);
    }

/**
 * Get TPID C-VLANs
 *
 * @param self This pseudowire
 *
 * @return Value TPID of C-VLANs if C-VLAN exists, otherwise return 0
 */
uint16 AtPwEthCVlanTpidGet(AtPw self)
    {
    mAttributeGetWithCache(EthCVlanTpidGet, uint16, 0);
    }

/**
 * Get TPID S-VLANs
 *
 * @param self This pseudowire
 *
 * @return Value TPID of S-VLANs if S-VLAN exists, otherwise return 0
 */
uint16 AtPwEthSVlanTpidGet(AtPw self)
    {
    mAttributeGetWithCache(EthSVlanTpidGet, uint16, 0);
    }

/**
 * Get S-VLAN
 *
 * @param self This pseudowire
 * @param sVlan Structure to hold S-VLAN information
 *
 * @return sVlan if S-VLAN exists, otherwise NULL is returned
 */
tAtEthVlanTag *AtPwEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    mOneParamAttributeGetWithCache(EthSVlanGet, sVlan, tAtEthVlanTag *, NULL);
    }

/**
 * Set expected C-VLAN.
 *
 * @param self This pseudowire
 * @param cVlan Expected C-VLAN
 *
 * @return AT return code
 * @note It is only used for product that support PW VLAN lookup
 */
eAtModulePwRet AtPwEthExpectedCVlanSet(AtPw self, tAtEthVlanTag *cVlan)
    {
    mVlanTagAttributeSet(EthExpectedCVlanSet, cVlan);
    }

/**
 * Get expected C-VLAN.
 *
 * @param self This pseudowire
 * @param [out] cVlan Expected C-VLAN
 *
 * @return cVlan - Expected C-VLAN structure if VLAN lookup mode is supported.
 * Otherwise, NULL is returned
 */
tAtEthVlanTag *AtPwEthExpectedCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    mOneParamAttributeGet(EthExpectedCVlanGet, cVlan, tAtEthVlanTag *, NULL);
    }

/**
 * Set expected S-VLAN.
 *
 * @param self This pseudowire
 * @param sVlan Expected S-VLAN
 *
 * @return AT return code
 * @note It is only used for product that support PW VLAN lookup
 */
eAtModulePwRet AtPwEthExpectedSVlanSet(AtPw self, tAtEthVlanTag *sVlan)
    {
    mVlanTagAttributeSet(EthExpectedSVlanSet, sVlan);
    }

/**
 * Get expected S-VLAN.
 *
 * @param self This pseudowire
 * @param [out] sVlan Expected S-VLAN
 *
 * @return sVlan - Expected S-VLAN structure if VLAN lookup mode is supported.
 * Otherwise, NULL is returned
 */
tAtEthVlanTag *AtPwEthExpectedSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    mOneParamAttributeGet(EthExpectedSVlanGet, sVlan, tAtEthVlanTag *, NULL);
    }

/**
 * Bind circuit.
 *
 * @param self This pseudowire
 * @param circuit Circuit to bind
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCircuitBind(AtPw self, AtChannel circuit)
    {
    mObjectSet(CircuitBind, circuit);
    }

/**
 * Unbind circuit that bound before
 *
 * @param self This pseudowire
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCircuitUnbind(AtPw self)
    {
    mNoParamCall(CircuitUnbind, eAtModulePwRet, cAtErrorNullPointer);
    }

/**
 * Get bound circuit
 *
 * @param self This pseudowire
 *
 * @return Bound circuit
 */
AtChannel AtPwBoundCircuitGet(AtPw self)
    {
    mNoParamObjectGet(BoundCircuitGet, AtChannel);
    }

/**
 * Set TX RTP-PayloadType
 *
 * @param self pseudowire
 * @param payloadType TX PayloadType
 *
 * @return AT return code
 *
 * @see AtPwRtpPayloadTypeSet()
 */
eAtModulePwRet AtPwRtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    mNumericalAttributeSetWithCache(RtpTxPayloadTypeSet, payloadType);
    }

/**
 * Get TX RTP-PayloadType
 *
 * @param self pseudowire
 *
 * @return TX PayloadType
 *
 * @see AtPwRtpPayloadTypeGet()
 */
uint8 AtPwRtpTxPayloadTypeGet(AtPw self)
    {
    mAttributeGetWithCache(RtpTxPayloadTypeGet, uint8, 0);
    }

/**
 * Set expected RTP-PayloadType
 *
 * @param self pseudowire
 * @param payloadType Expected PayloadType
 *
 * @return AT return code
 *
 * @see AtPwRtpPayloadTypeSet()
 */
eAtModulePwRet AtPwRtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    mNumericalAttributeSetWithCache(RtpExpectedPayloadTypeSet, payloadType);
    }

/**
 * Get expected RTP-PayloadType
 *
 * @param self pseudowire
 *
 * @return expected PayloadType
 *
 * @see AtPwRtpPayloadTypeGet()
 */
uint8 AtPwRtpExpectedPayloadTypeGet(AtPw self)
    {
    mAttributeGetWithCache(RtpExpectedPayloadTypeGet, uint8, 0);
    }

/**
 * Enable/disable comparing the received RTP-PayloadType with the expected one
 *
 * @param self pseudowire
 * @param enableCompare Enable/disable comparing
 *
 * @return AT return code
 *
 * @see AtPwRtpExpectedPayloadTypeSet()
 */
eAtModulePwRet AtPwRtpPayloadTypeCompare(AtPw self, eBool enableCompare)
	{
    mNumericalAttributeSet(RtpPayloadTypeCompare, enableCompare);
	}

/**
 * Check if the received Payload type is compared with the expected one
 *
 * @param self pseudowire
 *
 * @return cAtTrue if they are compared, otherwise, cAtFalse is returned
 */
eBool AtPwRtpPayloadTypeIsCompared(AtPw self)
	{
    mAttributeGet(RtpPayloadTypeIsCompared, eBool, 0);
	}

/**
 * Enable/disable comparing RTP-SSRC with the expected one.
 *
 * @param self pseudowire
 * @param enableCompare Enable/disable comparing
 *
 * @return AT return code
 *
 * @see AtPwRtpExpectedSsrcSet()
 */
eAtModulePwRet AtPwRtpSsrcCompare(AtPw self, eBool enableCompare)
	{
    mNumericalAttributeSet(RtpSsrcCompare, enableCompare);
	}

/**
 * Check if the received SSRC is compared with the expected one
 *
 * @param self pseudowire
 *
 * @return cAtTrue if they are compared, otherwise, cAtFalse is returned
 */
eBool AtPwRtpSsrcIsCompared(AtPw self)
	{
    mAttributeGet(RtpSsrcIsCompared, eBool, cAtFalse);
	}

/**
 * Set PW IDLE code
 *
 * @param self This PW
 * @param idleCode IDLE code
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIdleCodeSet(AtPw self, uint8 idleCode)
    {
    mNumericalAttributeSet(IdleCodeSet, idleCode);
    }

/**
 * Get PW IDLE code
 *
 * @param self This PW
 * @return IDLE code
 */
uint8 AtPwIdleCodeGet(AtPw self)
    {
    mAttributeGet(IdleCodeGet, uint8, 0);
    }

/**
 * Set threshold for Missing Packet Defect in case of PW CEP
 *
 * @param self This PW
 * @param numPackets if number of lost packets (missing packets) above this
 *                   threshold, Missing Packet Defect is raised
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    mNumericalAttributeSet(MissingPacketDefectThresholdSet, numPackets);
    }

/**
 * Get threshold of Missing Packet Defect in case of PW CEP
 *
 * @param self This PW
 *
 * @return Threshold for missing packet defect in number of packet unit
 */
uint32 AtPwMissingPacketDefectThresholdGet(AtPw self)
    {
    mAttributeGet(MissingPacketDefectThresholdGet, uint32, 0);
    }

/**
 * Set threshold for Excessive Packet Loss Rate Defect
 *
 * @param self This PW
 * @param numPackets Number of packets. When number of lost packets above this
 *                   number of packet within a time window (timeInSecond), an
 *                   Excessive Packet Loss Rate Defect is raised
 * @param timeInSecond Time window in seconds
 *
 * @return AT return code
 */
eAtModulePwRet AtPwExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
    mTwoParamsAttributeSet(ExcessivePacketLossRateDefectThresholdSet, numPackets, timeInSecond);
    }

/**
 * Get threshold of Excessive Packet Loss Rate Defect
 *
 * @param self This PW
 * @param [out] timeInSecond Time window
 *
 * @return Number of packets as threshold for Excessive Packet Loss Rate Defect
 *         in time window timeInSecond
 */
uint32 AtPwExcessivePacketLossRateDefectThresholdGet(AtPw self, uint32* timeInSecond)
    {
    if (self)
        return mMethodsGet(self)->ExcessivePacketLossRateDefectThresholdGet(self, timeInSecond);
    return 0;
    }

/**
 * Set threshold for Stray Packet Defect
 *
 * @param self This PW
 * @param numPackets Number of packet. When number of stray packets above this
 *        number of packets, Stray Packet Defect is raised
 *
 * @return AT return code
 */
eAtModulePwRet AtPwStrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    mNumericalAttributeSet(StrayPacketDefectThresholdSet, numPackets);
    }

/**
 * Get threshold of Stray Packet Defect
 *
 * @param self This PW
 *
 * @return Number of packet as threshold for Stray Packet Defect
 */
uint32 AtPwStrayPacketDefectThresholdGet(AtPw self)
    {
    mAttributeGet(StrayPacketDefectThresholdGet, uint32, 0);
    }

/**
 * Set threshold for Malformed Packet Defect
 *
 * @param self This PW
 * @param numPackets Number of packet. When number of malformed packets above
 *        this number of packets, Malformed Packet Defect is raised
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    mNumericalAttributeSet(MalformedPacketDefectThresholdSet, numPackets);
    }

/**
 * Get threshold of Malformed Packet Defect
 *
 * @param self This PW
 *
 * @return Number of packets as threshold for Malformed Packet Defect
 */
uint32 AtPwMalformedPacketDefectThresholdGet(AtPw self)
    {
    mAttributeGet(MalformedPacketDefectThresholdGet, uint32, 0);
    }

/**
 * Set threshold for Remote Packet Loss Defect
 *
 * @param self This PW
 * @param numPackets Number of received R-bit packets. When number of R-bit
 *        packets above this number of packets, Remote Packet Loss Defect is raised
 *
 * @return AT return code
 */
eAtModulePwRet AtPwRemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    mNumericalAttributeSet(RemotePacketLossDefectThresholdSet, numPackets);
    }

/**
 * Get threshold of Remote Packet Loss Defect
 *
 * @param self This PW
 *
 * @return Number of packets as threshold for Remote Packet Loss Defect
 */
uint32 AtPwRemotePacketLossDefectThresholdGet(AtPw self)
    {
    mAttributeGet(RemotePacketLossDefectThresholdGet, uint32, 0);
    }

/**
 * Set threshold for Jitter Buffer Overrun Defect
 *
 * @param self This PW
 * @param numOverrunEvent Number of jitter buffer overrun events
 *
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    mNumericalAttributeSet(JitterBufferOverrunDefectThresholdSet, numOverrunEvent);
    }

/**
 * Get threshold of Jitter Buffer Overrun Defect
 *
 * @param self This PW
 *
 * @return Number of Overrun event as threshold for Jitter Buffer Overrun Defect
 */
uint32 AtPwJitterBufferOverrunDefectThresholdGet(AtPw self)
    {
    mAttributeGet(JitterBufferOverrunDefectThresholdGet, uint32, 0);
    }

/**
 * Set threshold for Jitter Buffer Underrun Defect
 *
 * @param self This PW
 * @param numUnderrunEvent Number of jitter buffer overrun event
 *
 * @return AT return code
 */
eAtModulePwRet AtPwJitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
    mNumericalAttributeSet(JitterBufferUnderrunDefectThresholdSet, numUnderrunEvent);
    }

/**
 * Get threshold of Jitter Buffer Underrun Defect
 *
 * @param self This PW
 *
 * @return Number of underrun event as threshold for Jitter Buffer Underrun Defect
 */
uint32 AtPwJitterBufferUnderrunDefectThresholdGet(AtPw self)
    {
    mAttributeGet(JitterBufferUnderrunDefectThresholdGet, uint32, 0);
    }

/**
 * Set threshold for MisConnection Defect
 *
 * @param self This PW
 * @param numPackets Number of packets that have fields ECID or DA in MEF header in case of MEFoMPLS mis-match with expected values
 * @return AT return code
 */
eAtModulePwRet AtPwMisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    mNumericalAttributeSet(MisConnectionDefectThresholdSet, numPackets);
    }

/**
 * Get threshold of MisConnection Defect
 *
 * @param self This PW
 * @return Number of packets as threshold for MisConnection Defect
 */
uint32 AtPwMisConnectionDefectThresholdGet(AtPw self)
    {
    mAttributeGet(MisConnectionDefectThresholdGet, uint32, 0);
    }

/**
 * Get APS group that PW belongs to
 *
 * @param self PW
 *
 * @return APS group that PW belongs to or NULL if it does not belong to any APS
 * group
 */
AtPwGroup AtPwApsGroupGet(AtPw self)
    {
    mNoParamObjectGet(ApsGroupGet, AtPwGroup);
    }

/**
 * Get Hot-Swap group that PW belongs to
 *
 * @param self PW
 *
 * @return Hot-Swap group that PW belongs to or NULL if it does not belong to any
 * Hot-Swap group.
 */
AtPwGroup AtPwHsGroupGet(AtPw self)
    {
    mNoParamObjectGet(HsGroupGet, AtPwGroup);
    }

/**
 * Set the backup PSN for PW
 *
 * @param self PW
 * @param backupPsn Backup PSN
 *
 * @return AT return code.
 */
eAtModulePwRet AtPwBackupPsnSet(AtPw self, AtPwPsn backupPsn)
    {
    mObjectSet(BackupPsnSet, backupPsn);
    }

/**
 * Get backup PSN object
 *
 * @param self PW
 *
 * @return Backup PSN object.
 */
AtPwPsn AtPwBackupPsnGet(AtPw self)
    {
    mNoParamObjectGet(BackupPsnGet, AtPwPsn);
    }

/**
 * Set backup Ethernet MAC header.
 *
 * @param self PW
 * @param destMac Destination MAC
 * @param cVlan C-VLAN
 * @param sVlan S-VLAN
 *
 * @return AT return code
 */
eAtModulePwRet AtPwBackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    eAtModulePwRet ret = cAtErrorObjectNotExist;

    EthHeaderSetApiLogStart(self, destMac, cVlan, sVlan, AtSourceLocationNone, AtFunction);

    if (self)
        {
        CacheBackupEthHeaderSet(self, destMac, cVlan, sVlan);
        ret = mMethodsGet(self)->BackupEthHeaderSet(self, destMac, cVlan, sVlan);
        }

    AtDriverApiLogStop();

    return ret;
    }

/**
 * Set backup Ethernet source MAC address
 *
 * @param self
 * @param [out] srcMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwBackupEthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    mMacAttributeSetWithCache(BackupEthSrcMacSet, srcMac);
    }

/**
 * Get backup Ethernet source MAC address
 *
 * @param self
 * @param [out] srcMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwBackupEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    mGeneralAttributeGetWithCache(BackupEthSrcMacGet, srcMac);
    }

/**
 * Set backup Ethernet destination MAC address
 *
 * @param self
 * @param [out] destMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwBackupEthDestMacSet(AtPw self, uint8 *destMac)
    {
    mMacAttributeSetWithCache(BackupEthDestMacSet, destMac);
    }

/**
 * Get backup Ethernet destination MAC address
 *
 * @param self
 * @param [out] destMac Destination MAC address. It must be an array with 6 bytes
 *
 * @return AT return code
 */
eAtModulePwRet AtPwBackupEthDestMacGet(AtPw self, uint8 *destMac)
    {
    mGeneralAttributeGetWithCache(BackupEthDestMacGet, destMac);
    }

/**
 * Set backup VLAN header
 *
 * @param self This pseudowire
 * @param cVlan C-VLAN. If it is NULL, sVlan parameter is ignored. No VLAN is
 *              used in this case
 * @param sVlan S-VLAN. Can be NULL, and only C-VLAN is used in this case
 *
 * @return AT return code
 */
eAtModulePwRet AtPwBackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    eAtModulePwRet ret = cAtErrorObjectNotExist;

    EthVlanSetApiLogStart(self, cVlan, sVlan, AtSourceLocationNone, AtFunction);

    if (self)
        {
        CacheBackupEthVlanSet(self, cVlan, sVlan);
        ret = mMethodsGet(self)->BackupEthVlanSet(self, cVlan, sVlan);
        }

    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get backup C-VLAN
 *
 * @param self This pseudowire
 * @param cVlan Structure to hold C-VLAN information
 *
 * @return cVlan if C-VLAN exists, otherwise NULL is returned
 */
tAtEthVlanTag *AtPwBackupEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    mOneParamAttributeGetWithCache(BackupEthCVlanGet, cVlan, tAtEthVlanTag *, NULL);
    }

/**
 * Get backup S-VLAN
 *
 * @param self This pseudowire
 * @param sVlan Structure to hold S-VLAN information
 *
 * @return sVlan if S-VLAN exists, otherwise NULL is returned
 */
tAtEthVlanTag *AtPwBackupEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    mOneParamAttributeGetWithCache(BackupEthSVlanGet, sVlan, tAtEthVlanTag *, NULL);
    }

/**
 * Set transmitted ethernet port queue

 * @param self This pseudowire
 * @param queueId Ethernet port queue ID

 * @return AT return code
 */
eAtRet AtPwEthPortQueueSet(AtPw self, uint8 queueId)
    {
    mNumericalAttributeSet(EthPortQueueSet, queueId);
    }

/**
 * Get transmitted ethernet port queue

 * @param self This PW

 * @return Ethernet port queue ID or cAtInvalidPwEthPortQueueId if this function is not supported
 */
uint8 AtPwEthPortQueueGet(AtPw self)
    {
    mAttributeGet(EthPortQueueGet, uint8, cAtInvalidPwEthPortQueueId);
    }

/**
 * Get maximum number of queues that PW can be assigned to one of them.
 *
 * @param self This PW
 * @return Maximum number of queues that PW can be assigned to one of them.
 */
uint8 AtPwEthPortMaxNumQueues(AtPw self)
    {
    mAttributeGet(EthPortMaxNumQueues, uint8, 0);
    }

/**
 * Check if input RTP timestamp mode is supported or not
 *
 * @param self This pseudowire
 * @param timeStampMode @ref eAtPwRtpTimeStampMode "RTP timestamp mode" to check
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPwRtpTimeStampModeIsSupported(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    if (self)
        return mMethodsGet(self)->RtpTimeStampModeIsSupported(self, timeStampMode);
    return cAtFalse;
    }

/**
 * Check specific control word sequence mode is supported or not
 *
 * @param self This pseudowire
 * @param sequenceMode @ref eAtPwCwSequenceMode "Control word sequence mode" to check
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPwCwSequenceModeIsSupported(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    if (self)
        return mMethodsGet(self)->CwSequenceModeIsSupported(self, sequenceMode);
    return cAtFalse;
    }

/**
 * Check if auto AIS generating to attachment circuit side when receiving
 * L-Bit packets can be enabled/disabled
 *
 * @param self This pseudowire
 * @param enable Enabling to check
 *               - cAtTrue to check if this feature can be enabled
 *               - cAtFalse to check if this feature can be disabled
 *
 * @retval cAtTrue if can enable/disable
 * @retval cAtFalse if cannot enable/disable
 */
eBool AtPwCwAutoRxLBitCanEnable(AtPw self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->CwAutoRxLBitCanEnable(self, enable);
    return cAtFalse;
    }

/**
 * Check if a specific control word length mode is supported or not
 *
 * @param self This pseudowire
 * @param lengthMode @ref eAtPwCwLengthMode "Control word length mode" to check
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPwCwLengthModeIsSupported(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    if (self)
        return mMethodsGet(self)->CwLengthModeIsSupported(self, lengthMode);
    return cAtFalse;
    }

/**
 * To check if a PW is TDM PW (CEP/SAToP/CESoP)
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue if it is TDM PW
 * @retval cAtFalse if it is not TDM PW
 */
eBool AtPwIsTdmPw(AtPw self)
    {
    if (self)
        return IsTdmPw(self);
    return cAtFalse;
    }

/**
 * Get number of RAM blocks used by PW's jitter buffer
 *
 * @param self This PW
 * @return Number of blocks used by PW
 */
uint32 AtPwJitterBufferNumBlocksGet(AtPw self)
    {
    mAttributeGet(JitterBufferNumBlocksGet, uint32, 0);
    }

/**
 * Set packet replacement mode when PSN side entering lost of packet state or loss of packet synchronization condition
 *
 * @param self This pseudowire
 * @param pktReplaceMode @ref eAtPwPktReplaceMode "Packet replacement modes"
 *
 * @return AT return code
 */
eAtModulePwRet AtPwLopsPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    if (!PktReplaceModeIsValid(pktReplaceMode))
        return cAtErrorInvlParm;

    mNumericalAttributeSetWithCache(LopsPktReplaceModeSet, pktReplaceMode);
    }

/**
 * Get packet replacement mode when PSN side entering lost of packet state or loss of packet synchronization condition
 *
 * @param self This pseudowire
 *
 * @return @ref eAtPwPktReplaceMode "Packet replacement modes"
 */
eAtPwPktReplaceMode AtPwLopsPktReplaceModeGet(AtPw self)
    {
    mAttributeGetWithCache(LopsPktReplaceModeGet, eAtPwPktReplaceMode, cAtPwPktReplaceModeAis);
    }

/** @} */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwGroup.c
 *
 * Created Date: Mar 24, 2015
 *
 * Description : PW Group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDriverInternal.h"
#include "../pw/AtModulePwInternal.h"
#include "AtPwGroupInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidUint32 0xFFFFFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwGroup)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwGroupMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePwRet Enable(AtPwGroup self, eBool enable)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool IsEnabled(AtPwGroup self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtFalse;
    }

static AtList PwList(AtPwGroup self)
    {
    if (self->pws == NULL)
        self->pws = AtListCreate(0);
    return self->pws;
    }

static eAtModulePwRet PwAdd(AtPwGroup self, AtPw pw)
    {
    if (pw == NULL)
        return cAtErrorNullPointer;
    return AtListObjectAdd(PwList(self), (AtObject)pw);
    }

static eAtModulePwRet PwRemove(AtPwGroup self, AtPw pw)
    {
    if (pw == NULL)
        return cAtOk;
    return AtListObjectRemove(PwList(self), (AtObject)pw);
    }

static eBool ContainsPw(AtPwGroup self, AtPw pw)
    {
    return AtListContainsObject(PwList(self), (AtObject)pw);
    }

static uint32 NumPws(AtPwGroup self)
    {
    return AtListLengthGet(PwList(self));
    }

static AtPw PwAtIndex(AtPwGroup self, uint32 pwIndex)
    {
    return (AtPw)AtListObjectGet(PwList(self), pwIndex);
    }

static eAtModulePwRet TxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(labelSet);
    return cAtErrorNotImplemented;
    }

static eAtPwGroupLabelSet TxSelectedLabelGet(AtPwGroup self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtPwGroupLabelSetUnknown;
    }

static eAtModulePwRet RxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(labelSet);
    return cAtErrorNotImplemented;
    }

static eAtPwGroupLabelSet RxSelectedLabelGet(AtPwGroup self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtPwGroupLabelSetUnknown;
    }

static eAtPwGroupType TypeGet(AtPwGroup self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtPwGroupTypeUnknown;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->pws);
    mThis(self)->pws = NULL;

    /* Call super to fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eAtModulePwRet Init(AtPwGroup self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwGroup object = (AtPwGroup)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(groupId);
    AtCoderEncodeObjectDescriptionInList(encoder, object->pws, "pws");
    mEncodeObjectDescription(module);
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtPwGroup group = (AtPwGroup)self;
    AtModule module = (AtModule)AtPwGroupModuleGet(group);
    AtDevice dev = AtModuleDeviceGet(module);

    AtSnprintf(description, sizeof(description) - 1, "%s%s.%s", AtDeviceIdToString(dev), AtPwGroupTypeString(group), AtPwGroupIdString(group));
    return description;
    }

static const char *IdString(AtPwGroup self)
    {
    static char idString[16];
    AtSprintf(idString, "%u", AtPwGroupIdGet(self) + 1);
    return idString;
    }

static const char *TypeString(AtPwGroup self)
    {
    eAtPwGroupType groupType = AtPwGroupTypeGet(self);

    switch (groupType)
        {
        case cAtPwGroupTypeUnknown: return "unknown";
        case cAtPwGroupTypeAps    : return "apsPwGroup";
        case cAtPwGroupTypeHs     : return "hsPwGroup";
        default:
            return NULL;
        }
    }

static void OverrideAtObject(AtPwGroup self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPwGroup self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPwGroup self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, PwAdd);
        mMethodOverride(m_methods, PwRemove);
        mMethodOverride(m_methods, ContainsPw);
        mMethodOverride(m_methods, NumPws);
        mMethodOverride(m_methods, PwAtIndex);
        mMethodOverride(m_methods, TxLabelSetSelect);
        mMethodOverride(m_methods, TxSelectedLabelGet);
        mMethodOverride(m_methods, RxLabelSetSelect);
        mMethodOverride(m_methods, RxSelectedLabelGet);
        mMethodOverride(m_methods, TypeGet);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, IdString);
        mMethodOverride(m_methods, TypeString);
        }

    mMethodsSet(self, &m_methods);
    }

AtPwGroup AtPwGroupObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwGroup));

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->groupId = groupId;
    self->module  = module;

    return self;
    }

eAtModulePwRet AtPwGroupInit(AtPwGroup self)
    {
    return (self) ? mMethodsGet(self)->Init(self) : cAtErrorNullPointer;
    }

/**
 * @addtogroup AtPwGroup
 * @{
 */

/**
 * Enable/disable group
 *
 * @param self This group
 * @param enable Enabling.
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtPwGroupEnable(AtPwGroup self, eBool enable)
    {
    mNumericalAttributeSet(Enable, enable);
    }

/**
 * Check if a group is enabled or not
 *
 * @param self This group
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPwGroupIsEnabled(AtPwGroup self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * Add a PW to a group
 *
 * @param self This group
 * @param pw PW to add
 *
 * @return AT return code.
 */
eAtRet AtPwGroupPwAdd(AtPwGroup self, AtPw pw)
    {
    mObjectSet(PwAdd, pw);
    }

/**
 * Remove a PW out of a group
 *
 * @param self This group
 * @param pw PW to remove
 *
 * @return AT return code.
 */
eAtRet AtPwGroupPwRemove(AtPwGroup self, AtPw pw)
    {
    mObjectSet(PwRemove, pw);
    }

/**
 * Check if a group contains a PW
 *
 * @param self This group
 * @param pw PW to check if it exists in group
 *
 * @retval cAtTrue if this group contains the input PW
 * @retval cAtFalse if this group does not contain the input PW
 */
eBool AtPwGroupContainsPw(AtPwGroup self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->ContainsPw(self, pw);
    return cAtFalse;
    }

/**
 * Get number of PWs have been added to group
 *
 * @param self This group
 *
 * @return Number of PWs have been added to group.
 */
uint32 AtPwGroupNumPws(AtPwGroup self)
    {
    mAttributeGet(NumPws, uint32, 0);
    }

/**
 * Get a PW at specified index in group
 *
 * @param self This group
 * @param pwIndex PW index
 *
 * @return PW instance
 */
AtPw AtPwGroupPwAtIndex(AtPwGroup self, uint32 pwIndex)
    {
    if (self)
        return mMethodsGet(self)->PwAtIndex(self, pwIndex);
    return NULL;
    }

/**
 * Select label set of a group in Tx direction
 *
 * @param self This group
 * @param labelSet Label set. See - @ref eAtPwGroupLabelSet "Group label set"
 *
 * @return AT return code.
 */
eAtRet AtPwGroupTxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    mNumericalAttributeSet(TxLabelSetSelect, labelSet);
    }

/**
 * Get label set of a group in Tx direction
 *
 * @param self This group
 *
 * @return label set.
 */
eAtPwGroupLabelSet AtPwGroupTxSelectedLabelGet(AtPwGroup self)
    {
    mAttributeGet(TxSelectedLabelGet, eAtPwGroupLabelSet, cAtPwGroupLabelSetUnknown);
    }

/**
 * Select label set of a group in Rx direction
 *
 * @param self This group
 * @param labelSet Label set. See @ref eAtPwGroupLabelSet "Group label set"
 *
 * @return AT return code.
 */
eAtRet AtPwGroupRxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet)
    {
    mNumericalAttributeSet(RxLabelSetSelect, labelSet);
    }

/**
 * Get label set of a group in Rx direction
 *
 * @param self This group
 *
 * @return label set.
 */
eAtPwGroupLabelSet AtPwGroupRxSelectedLabelGet(AtPwGroup self)
    {
    mAttributeGet(RxSelectedLabelGet, eAtPwGroupLabelSet, cAtPwGroupLabelSetUnknown);
    }

/**
 * Get ID of a group
 *
 * @param self This group
 *
 * @return group ID.
 */
uint32 AtPwGroupIdGet(AtPwGroup self)
    {
    return (self) ? self->groupId : 0xFFFFFFFF;
    }

/**
 * Get type of a group
 *
 * @param self This group
 *
 * @return group type.
 */
eAtPwGroupType AtPwGroupTypeGet(AtPwGroup self)
    {
    mAttributeGet(TypeGet, eAtPwGroupType, cAtPwGroupTypeUnknown);
    }

/**
 * Get PW module that this group belongs to
 *
 * @param self This group
 * @return PW module
 */
AtModulePw AtPwGroupModuleGet(AtPwGroup self)
    {
    return (self) ? self->module : NULL;
    }

/**
 * Get ID string
 *
 * @param self This group
 * @return ID string
 */
const char *AtPwGroupIdString(AtPwGroup self)
    {
    if (self)
        return mMethodsGet(self)->IdString(self);
    return NULL;
    }

/**
 * Get type string
 *
 * @param self This group
 * @return Type string
 */
const char *AtPwGroupTypeString(AtPwGroup self)
    {
    if (self)
        return mMethodsGet(self)->TypeString(self);
    return NULL;
    }

/**
 * @}
 */

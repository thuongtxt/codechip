/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwCESoP.c
 *
 * Created Date: Sep 18, 2012
 *
 * Description : CESoPSN pseudowire
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCESoP.h"
#include "AtPdhNxDs0.h"
#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "AtModulePwInternal.h"
#include "AtPwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mPwIsValid(self) ((self) != NULL) && (AtPwTypeGet((AtPw)self) == cAtPwTypeCESoP)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwCESoPMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    AtPw pw = (AtPw)self;
    AtPwCESoP cesop = (AtPwCESoP)self;

    /* Let its super do first */
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Additional initialization */
    if (AtPwNeedInitConfig((AtPw)self))
        {
        AtPwCwEnable(pw, cAtTrue);
        AtPwCESoPCwAutoTxMBitEnable(cesop, cAtTrue);
        if (AtPwCESoPCwAutoRxMBitIsConfigurable(cesop))
            {
            AtModulePw pwModule = (AtModulePw)AtChannelModuleGet(self);
            AtPwCESoPCwAutoRxMBitEnable(cesop, AtModulePwAutoRxMBitShouldEnable(pwModule));
            }
        }

    return cAtOk;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "cesop";
    }

static eAtModulePwRet CwAutoTxMBitEnable(AtPwCESoP self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool CwAutoTxMBitIsEnabled(AtPwCESoP self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModulePwRet CwAutoRxMBitEnable(AtPwCESoP self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool CwAutoRxMBitIsEnabled(AtPwCESoP self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool CwAutoRxMBitIsConfigurable(AtPwCESoP self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtFalse;
    }

static eAtPwCESoPMode ModeGet(AtPwCESoP self)
    {
    return self->mode;
    }

static eAtModulePwRet CasIdleCode1Set(AtPwCESoP self, uint8 abcd1)
    {
    AtUnused(self);
    AtUnused(abcd1);
    return cAtErrorNotImplemented;
    }

static uint8 CasIdleCode1Get(AtPwCESoP self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet CasIdleCode2Set(AtPwCESoP self, uint8 abcd2)
    {
    AtUnused(self);
    AtUnused(abcd2);
    return cAtErrorNotImplemented;
    }

static uint8 CasIdleCode2Get(AtPwCESoP self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet CasAutoIdleEnable(AtPwCESoP self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool CasAutoIdleIsEnabled(AtPwCESoP self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RxCasModeSet(AtPwCESoP self, eAtPwCESoPCasMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtErrorNotImplemented;
    }

static eAtPwCESoPCasMode RxCasModeGet(AtPwCESoP self)
    {
    AtUnused(self);
    return cAtPwCESoPCasModeUnknown;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwCESoP object = (AtPwCESoP)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(mode);
    }

static void OverrideAtObject(AtPwCESoP self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPwCESoP self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPwCESoP self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(AtPwCESoP self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, CwAutoTxMBitEnable);
        mMethodOverride(m_methods, CwAutoTxMBitIsEnabled);
        mMethodOverride(m_methods, CwAutoRxMBitEnable);
        mMethodOverride(m_methods, CwAutoRxMBitIsEnabled);
        mMethodOverride(m_methods, CwAutoRxMBitIsConfigurable);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, CasIdleCode1Set);
        mMethodOverride(m_methods, CasIdleCode1Get);
        mMethodOverride(m_methods, CasIdleCode2Set);
        mMethodOverride(m_methods, CasIdleCode2Get);
        mMethodOverride(m_methods, CasAutoIdleEnable);
        mMethodOverride(m_methods, CasAutoIdleIsEnabled);
        mMethodOverride(m_methods, RxCasModeSet);
        mMethodOverride(m_methods, RxCasModeGet);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize object */
AtPwCESoP AtPwCESoPObjectInit(AtPwCESoP self, uint32 pwId, AtModulePw module, eAtPwCESoPMode mode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwCESoP));

    /* Super constructor should be called first */
    if (AtPwObjectInit((AtPw)self, pwId, module, cAtPwTypeCESoP) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->mode = (uint8)mode;

    return self;
    }

/**
 * @addtogroup AtPwCESoP
 * @{
 */

/**
 * Enable/disable auto transmitting M-bit when DS1/E1 has RAI/RDI
 *
 * @param self This pseudowire
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCESoPCwAutoTxMBitEnable(AtPwCESoP self, eBool enable)
    {
    mNumericalAttributeSet(CwAutoTxMBitEnable, enable);
    }

/**
 * Check if auto transmitting M-bit when DS1/E1 has RAI/RDI is enabled or not
 *
 * @param self This PW
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPwCESoPCwAutoTxMBitIsEnabled(AtPwCESoP self)
    {
    mAttributeGet(CwAutoTxMBitIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable auto transmitting RDI upon receiving M-Bit
 *
 * @param self This pseudowire
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCESoPCwAutoRxMBitEnable(AtPwCESoP self, eBool enable)
    {
    mNumericalAttributeSet(CwAutoRxMBitEnable, enable);
    }

/**
 * Check if auto transmitting RDI upon receiving M-Bit is enabled or not
 *
 * @param self This PW
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPwCESoPCwAutoRxMBitIsEnabled(AtPwCESoP self)
    {
    mAttributeGet(CwAutoRxMBitIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if auto transmitting RDI upon receiving M-Bit can be enabled/disabled.
 * Products that do not support this will always playout RDI onto DS1/E1 when
 * receiving M-Bit.
 *
 * And not all products support this.
 *
 * @param self This PW
 *
 * @retval cAtTrue if configurable
 * @retval cAtFalse if not configurable
 */
eBool AtPwCESoPCwAutoRxMBitIsConfigurable(AtPwCESoP self)
    {
    if (self)
        return mMethodsGet(self)->CwAutoRxMBitIsConfigurable(self);
    return cAtFalse;
    }

/**
 * Get CESoP mode to check if it is basic or with CAS
 *
 * @param self This PW
 *
 * @return @ref eAtPwCESoPMode "CESoP modes"
 */
eAtPwCESoPMode AtPwCESoPModeGet(AtPwCESoP self)
    {
    mAttributeGet(ModeGet, eAtPwCESoPMode, cAtPwCESoPModeUnknown);
    }

/**
 * Set first ABCD idle pattern for CAS trunk condition
 *
 * @param self This PW
 * @param abcd1 The CAS idle code
 *
 * @return AT return code
  */
eAtModulePwRet AtPwCESoPCasIdleCode1Set(AtPwCESoP self, uint8 abcd1)
    {
    mNumericalAttributeSet(CasIdleCode1Set, abcd1);
    }

/**
 * Get first ABCD idle pattern configured for CAS trunk condition
 *
 * @param self This PW
 *
 * @return the first CAS idle pattern configuration
 */
uint8 AtPwCESoPCasIdleCode1Get(AtPwCESoP self)
    {
    mAttributeGet(CasIdleCode1Get, uint8, 0);
    }

/**
 * Set second ABCD idle pattern for CAS trunk condition
 *
 * @param self This PW
 * @param abcd2 The CAS idle pattern
 *
 * @return AT return code
  */
eAtModulePwRet AtPwCESoPCasIdleCode2Set(AtPwCESoP self, uint8 abcd2)
    {
    mNumericalAttributeSet(CasIdleCode2Set, abcd2);
    }

/**
 * Get the second ABCD idle pattern configured for CAS trunk condition
 *
 * @param self This PW
 *
 * @return the first CAS idle pattern configuration
 */
uint8 AtPwCESoPCasIdleCode2Get(AtPwCESoP self)
    {
    mAttributeGet(CasIdleCode2Get, uint8, 0);
    }

/**
 * Enable/Disable to generate CAS ABCD1 and ABCD2 automatically for CAS trunk condition
 *
 * @param self This PW
 * @param enable Enable/disable
 *
 * @return AT return code
  */
eAtModulePwRet AtPwCESoPCasAutoIdleEnable(AtPwCESoP self, eBool enable)
    {
    mNumericalAttributeSet(CasAutoIdleEnable, enable);
    }

/**
 * Get the configuration of enabling PW automactially generate ABCD1 and ABCD2 CAS
 * for CAS trunk condition
 *
 * @param self This PW
 *
 * @return cAtTrue: enabled, cAtFalse: disabled
 */
eBool AtPwCESoPCasAutoIdleIsEnabled(AtPwCESoP self)
    {
    mAttributeGet(CasAutoIdleIsEnabled, eBool, cAtFalse);
    }

/**
 * Set CESoP CAS mode at reception
 *
 * @param self This PW
 * @param mode CAS mode  @ref eAtPwCESoPCasMode "CESoP CAS modes"
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCESoPRxCasModeSet(AtPwCESoP self, eAtPwCESoPCasMode mode)
    {
    mNumericalAttributeSet(RxCasModeSet, mode);
    }

/**
 * Get CESoP CAS mode at reception
 *
 * @param self This PW
 *
 * @return @ref eAtPwCESoPCasMode "CESoP CAS modes"
 */
eAtPwCESoPCasMode AtPwCESoPRxCasModeGet(AtPwCESoP self)
    {
    mAttributeGet(RxCasModeGet, eAtPwCESoPCasMode, cAtPwCESoPCasModeUnknown);
    }

/** @} */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwInternal.h
 * 
 * Created Date: Nov 17, 2012
 *
 * Description : PW classes descriptors
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWINTERNAL_H_
#define _ATPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelInternal.h"
#include "AtModulePw.h"
#include "AtPw.h"
#include "AtPwAtm.h"
#include "AtPwCESoP.h"
#include "AtPwToh.h"
#include "AtPwHdlc.h"
#include "AtPtchService.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwCache
    {
    tAtChannelCache cache;

    /* Additional fields */
    uint32 lopsSetThreshold, lopsClearThreshold;

    /* RTP */
    uint8 rtpEnabled;
    uint8 rtpTxPayloadType, rtpExpectedPayloadType;
    uint32 rtpTxSsrc, rtpExpectedSsrc;
    uint8 rtpTimeStampMode;

    /* Control word */
    uint8 cwAutoTxLBitEnabled;
    uint8 cwAutoRxLBitEnabled;
    uint8 cwAutoRBitEnabled;
    uint8 cwPktReplaceMode;
    uint8 lopsPktReplaceMode;

    /* ETH */
    tAtEthVlanTag cVlan, sVlan, *pCVlan, *pSVlan;
    uint8 srcMac[6], destMac[6];
    uint16 sVlanTpid, cVlanTpid;

    /* Backup header */
    tAtEthVlanTag backupCVlan, backupSVlan, *pBackupCVlan, *pBackupSVlan;
    uint8 backupSrcMac[6], backupDestMac[6];
    }tAtPwCache;

/* Pseudowire methods */
typedef struct tAtPwMethods
    {
    /* Public methods */
    uint32 (*LopsThresholdMax)(AtPw self);
    uint32 (*LopsThresholdMin)(AtPw self);
    eAtModulePwRet (*LopsSetThresholdSet)(AtPw self, uint32 numPackets);
    uint32 (*LopsSetThresholdGet)(AtPw self);
    eAtModulePwRet (*LopsClearThresholdSet)(AtPw self, uint32 numPackets);
    uint32 (*LopsClearThresholdGet)(AtPw self);

    /* Payload size */
    eAtModulePwRet (*PayloadSizeSet)(AtPw self, uint16 payloadSize);
    uint16 (*PayloadSizeGet)(AtPw self);
    uint16 (*MinPayloadSize)(AtPw self, AtChannel circuit, uint32 jitterBufferSize);
    uint16 (*MaxPayloadSize)(AtPw self, AtChannel circuit, uint32 jitterBufferSize);

    /* Priority */
    eAtModulePwRet (*PrioritySet)(AtPw self, uint8 priority);
    uint8 (*PriorityGet)(AtPw self);

    /* Jitter buffer size */
    eAtModulePwRet (*JitterBufferSizeSet)(AtPw self, uint32 microseconds);
    uint32 (*JitterBufferSizeGet)(AtPw self);
    uint32 (*MinJitterBufferSize)(AtPw self, AtChannel circuit, uint16 payloadSize);
    uint32 (*MaxJitterBufferSize)(AtPw self, AtChannel circuit, uint16 payloadSize);

    /* Jitter buffer delay */
    eAtModulePwRet (*JitterBufferDelaySet)(AtPw self, uint32 microseconds);
    uint32 (*JitterBufferDelayGet)(AtPw self);
    uint32 (*MinJitterDelay)(AtPw self, AtChannel circuit, uint16 payloadSize);
    uint32 (*MaxJitterDelay)(AtPw self, AtChannel circuit, uint16 payloadSize);
    uint32 (*MaxJitterDelayForJitterBufferSize)(AtPw self, uint32 jitterBufferSize_us);

    /* Jitter buffer status */
    uint32 (*NumCurrentPacketsInJitterBuffer)(AtPw self);
    uint32 (*NumCurrentAdditionalBytesInJitterBuffer)(AtPw self);
    eAtRet (*JitterBufferCenter)(AtPw self);

    /* Control jitter buffer in packet unit */
    eAtModulePwRet (*JitterBufferSizeInPacketSet)(AtPw self, uint16 numPackets);
    uint16 (*JitterBufferSizeInPacketGet)(AtPw self);
    eAtModulePwRet (*JitterBufferDelayInPacketSet)(AtPw self, uint16 numPackets);
    uint16 (*JitterBufferDelayInPacketGet)(AtPw self);
    uint16 (*MinJitterBufferDelayInPacketGet)(AtPw self, AtChannel circuit, uint16 payloadSize);
    uint16 (*MinJitterBufferSizeInPacketGet)(AtPw self, AtChannel circuit, uint16 payloadSize);

    eAtModulePwRet (*JitterBufferAndPayloadSizeSet)(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize);
    eAtModulePwRet (*JitterBufferInPacketAndPayloadSizeSet)(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize);

    /* Jitter buffer water mark */
    uint32 (*JitterBufferWatermarkMinPackets)(AtPw self);
    uint32 (*JitterBufferWatermarkMaxPackets)(AtPw self);
    eBool (*JitterBufferWatermarkIsSupported)(AtPw self);
    eAtRet (*JitterBufferWatermarkReset)(AtPw self);

    /* RTP */
    eAtModulePwRet (*RtpEnable)(AtPw self, eBool enable);
    eBool (*RtpIsEnabled)(AtPw self);
    eAtModulePwRet (*RtpTimeStampModeSet)(AtPw self, eAtPwRtpTimeStampMode timeStampMode);
    eAtPwRtpTimeStampMode (*RtpTimeStampModeGet)(AtPw self);
    eBool (*RtpTimeStampModeIsSupported)(AtPw self, eAtPwRtpTimeStampMode timeStampMode);

    /* RTP-SSRC */
    eAtModulePwRet (*RtpSsrcSet)(AtPw self, uint32 ssrc);
    uint32 (*RtpSsrcGet)(AtPw self);
    eAtModulePwRet (*RtpTxSsrcSet)(AtPw self, uint32 ssrc);
    uint32 (*RtpTxSsrcGet)(AtPw self);
    eAtModulePwRet (*RtpExpectedSsrcSet)(AtPw self, uint32 ssrc);
    uint32 (*RtpExpectedSsrcGet)(AtPw self);
    eAtModulePwRet (*RtpSsrcCompare)(AtPw self, eBool enableCompare);
    eBool (*RtpSsrcIsCompared)(AtPw self);

    /* RTP-PayloadType */
    eAtModulePwRet (*RtpPayloadTypeSet)(AtPw self, uint8 payloadType);
    uint8 (*RtpPayloadTypeGet)(AtPw self);
    eAtModulePwRet (*RtpTxPayloadTypeSet)(AtPw self, uint8 payloadType);
    uint8 (*RtpTxPayloadTypeGet)(AtPw self);
    eAtModulePwRet (*RtpExpectedPayloadTypeSet)(AtPw self, uint8 payloadType);
    uint8 (*RtpExpectedPayloadTypeGet)(AtPw self);
    eAtModulePwRet (*RtpPayloadTypeCompare)(AtPw self, eBool enableCompare);
    eBool (*RtpPayloadTypeIsCompared)(AtPw self);

    /* IDLE code */
    eAtModulePwRet (*IdleCodeSet)(AtPw self, uint8 idleCode);
    uint8  (*IdleCodeGet)(AtPw self);

    /* Reordering */
    eAtModulePwRet (*ReorderingEnable)(AtPw self, eBool enable);
    eBool (*ReorderingIsEnabled)(AtPw self);

    /* Suppress */
    eAtModulePwRet (*SuppressEnable)(AtPw self, eBool enable);
    eBool (*SuppressIsEnabled)(AtPw self);

    /* Control word */
    eAtModulePwRet (*CwEnable)(AtPw self, eBool enable);
    eBool (*CwIsEnabled)(AtPw self);
    eAtModulePwRet (*CwAutoTxLBitEnable)(AtPw self, eBool enable);
    eBool (*CwAutoTxLBitIsEnabled)(AtPw self);
    eAtModulePwRet (*CwAutoRxLBitEnable)(AtPw self, eBool enable);
    eBool (*CwAutoRxLBitCanEnable)(AtPw self, eBool enable);
    eBool (*CwAutoRxLBitIsEnabled)(AtPw self);
    eAtModulePwRet (*CwAutoRBitEnable)(AtPw self, eBool enable);
    eBool (*CwAutoRBitIsEnabled)(AtPw self);
    eAtModulePwRet (*CwSequenceModeSet)(AtPw self, eAtPwCwSequenceMode sequenceMode);
    eAtPwCwSequenceMode (*CwSequenceModeGet)(AtPw self);
    eBool (*CwSequenceModeIsSupported)(AtPw self, eAtPwCwSequenceMode sequenceMode);
    eAtModulePwRet (*CwLengthModeSet)(AtPw self, eAtPwCwLengthMode lengthMode);
    eAtPwCwLengthMode (*CwLengthModeGet)(AtPw self);
    eBool (*CwLengthModeIsSupported)(AtPw self, eAtPwCwLengthMode lengthMode);
    eAtModulePwRet (*CwPktReplaceModeSet)(AtPw self, eAtPwPktReplaceMode pktReplaceMode);
    eAtPwPktReplaceMode (*CwPktReplaceModeGet)(AtPw self);
    eAtModulePwRet (*LopsPktReplaceModeSet)(AtPw self, eAtPwPktReplaceMode pktReplaceMode);
    eAtPwPktReplaceMode (*LopsPktReplaceModeGet)(AtPw self);
    eBool (*CanControlLopsPktReplaceMode)(AtPw self);
    eBool (*PktReplaceModeIsSupported)(AtPw self, eAtPwPktReplaceMode pktReplaceMode);

    /* PSN */
    eAtModulePwRet (*PsnSet)(AtPw self, AtPwPsn psn);
    AtPwPsn (*PsnGet)(AtPw self);

    /* Ethernet */
    eAtModulePwRet (*EthPortSet)(AtPw self, AtEthPort ethPort);
    AtEthPort (*EthPortGet)(AtPw self);
    eAtModulePwRet (*EthFlowSet)(AtPw self, AtEthFlow ethFlow);
    AtEthFlow (*EthFlowGet)(AtPw self);
    eAtModulePwRet (*EthHeaderSet)(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
    eAtModulePwRet (*EthDestMacSet)(AtPw self, uint8 *destMac);
    eAtModulePwRet (*EthDestMacGet)(AtPw self, uint8 *destMac);
    eAtModulePwRet (*EthSrcMacSet)(AtPw self, uint8 *srcMac);
    eAtModulePwRet (*EthSrcMacGet)(AtPw self, uint8 *srcMac);

    eAtModulePwRet (*EthVlanSet)(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
    tAtEthVlanTag* (*EthCVlanGet)(AtPw self, tAtEthVlanTag *cVlan);
    tAtEthVlanTag* (*EthSVlanGet)(AtPw self, tAtEthVlanTag *sVlan);
    eAtModulePwRet (*EthCVlanTpidSet)(AtPw self, uint16 tpid);
    eAtModulePwRet (*EthSVlanTpidSet)(AtPw self, uint16 tpid);
    uint16 (*EthCVlanTpidGet)(AtPw self);
    uint16 (*EthSVlanTpidGet)(AtPw self);

    /* Subport VLAN (only some products have) */
    eAtRet (*SubPortVlanIndexSet)(AtPw self, uint32 subPortVlanIndex);
    uint32 (*SubPortVlanIndexGet)(AtPw self);

    /* For VLAN lookup */
    eAtModulePwRet (*EthExpectedCVlanSet)(AtPw self, tAtEthVlanTag *cVlan);
    tAtEthVlanTag *(*EthExpectedCVlanGet)(AtPw self, tAtEthVlanTag *cVlan);
    eAtModulePwRet (*EthExpectedSVlanSet)(AtPw self, tAtEthVlanTag *sVlan);
    tAtEthVlanTag *(*EthExpectedSVlanGet)(AtPw self, tAtEthVlanTag *sVlan);

    /* Circuit binding */
    eAtModulePwRet (*CircuitBind)(AtPw self, AtChannel circuit);
    eAtModulePwRet (*CircuitUnbind)(AtPw self);
    AtChannel (*BoundCircuitGet)(AtPw self);

    /* Defect threshold */
    eAtModulePwRet (*MissingPacketDefectThresholdSet)(AtPw self, uint32 numPackets);
    uint32 (*MissingPacketDefectThresholdGet)(AtPw self);
    eAtModulePwRet (*ExcessivePacketLossRateDefectThresholdSet)(AtPw self, uint32 numPackets, uint32 timeInSecond);
    uint32 (*ExcessivePacketLossRateDefectThresholdGet)(AtPw self, uint32* timeInSecond);
    eAtModulePwRet (*StrayPacketDefectThresholdSet)(AtPw self, uint32 numPackets);
    uint32 (*StrayPacketDefectThresholdGet)(AtPw self);
    eAtModulePwRet (*MalformedPacketDefectThresholdSet)(AtPw self, uint32 numPackets);
    uint32 (*MalformedPacketDefectThresholdGet)(AtPw self);
    eAtModulePwRet (*RemotePacketLossDefectThresholdSet)(AtPw self, uint32 numPackets);
    uint32 (*RemotePacketLossDefectThresholdGet)(AtPw self);
    eAtModulePwRet (*JitterBufferUnderrunDefectThresholdSet)(AtPw self, uint32 numUnderrunEvent);
    uint32 (*JitterBufferUnderrunDefectThresholdGet)(AtPw self);
    eAtModulePwRet (*JitterBufferOverrunDefectThresholdSet)(AtPw self, uint32 numOverrunEvent);
    uint32 (*JitterBufferOverrunDefectThresholdGet)(AtPw self);
    eAtModulePwRet (*MisConnectionDefectThresholdSet)(AtPw self, uint32 numPackets);
    uint32 (*MisConnectionDefectThresholdGet)(AtPw self);

    AtPwGroup (*ApsGroupGet)(AtPw self);
    AtPwGroup (*HsGroupGet)(AtPw self);

    /* Backup header */
    eAtModulePwRet (*BackupPsnSet)(AtPw self, AtPwPsn backupPsn);
    AtPwPsn (*BackupPsnGet)(AtPw self);
    eAtModulePwRet (*BackupEthHeaderSet)(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
    eAtModulePwRet (*BackupEthDestMacSet)(AtPw self, uint8 *destMac);
    eAtModulePwRet (*BackupEthDestMacGet)(AtPw self, uint8 *destMac);
    eAtModulePwRet (*BackupEthSrcMacSet)(AtPw self, uint8 *srcMac);
    eAtModulePwRet (*BackupEthSrcMacGet)(AtPw self, uint8 *srcMac);
    eAtModulePwRet (*BackupEthVlanSet)(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
    tAtEthVlanTag* (*BackupEthCVlanGet)(AtPw self, tAtEthVlanTag *cVlan);
    tAtEthVlanTag* (*BackupEthSVlanGet)(AtPw self, tAtEthVlanTag *sVlan);

    /* Interrupt process */
    void (*InterruptProcess)(AtPw self, uint32 hwPw, AtHal hal);

    eBool (*LopsClearThresholdIsSupported)(AtPw self);
    eBool (*MissingPacketDefectThresholdIsSupported)(AtPw self);

    /* For standby driver */
    void (*CachePayloadSizeSet)(AtPw self, uint16 sizeInBytes);
    uint16 (*CachePayloadSizeGet)(AtPw self);

    /* Ethernet port queue selection */
    eAtRet (*EthPortQueueSet)(AtPw self, uint8 queueId);
    uint8 (*EthPortQueueGet)(AtPw self);
    uint8 (*EthPortMaxNumQueues)(AtPw self);

    /* PTCH */
    eAtRet (*PtchInsertionModeSet)(AtPw self, eAtPtchMode insertMode);
    eAtPtchMode (*PtchInsertionModeGet)(AtPw self);
    eAtRet (*PtchServiceEnable)(AtPw self, eBool enable);
    eBool (*PtchServiceIsEnabled)(AtPw self);

    /* Tx active forcing */
    eAtModulePwRet (*TxActiveForce)(AtPw self, eBool active);
    eBool (*TxActiveIsForced)(AtPw self);

    /* Number of blocks used by a PW */
    uint32 (*JitterBufferNumBlocksGet)(AtPw self);
    }tAtPwMethods;

/* Pseudowire object */
typedef struct tAtPw
    {
    tAtChannel super;
    const tAtPwMethods *methods;

    /* Private data */
    uint8 pwType;
    AtPrbsEngine prbsEngine;
    void *cache;
    AtPwGroup apsGroup;
    AtPwGroup hsGroup;
    }tAtPw;

/* SAToP pseudowire object */
typedef struct tAtPwSAToP
    {
    tAtPw super;

    /* Private attribute */
    }tAtPwSAToP;

/* CESoP methods */
typedef struct tAtPwCESoPMethods
    {
    /* M-Bit */
    eAtModulePwRet (*CwAutoTxMBitEnable)(AtPwCESoP self, eBool enable);
    eBool (*CwAutoTxMBitIsEnabled)(AtPwCESoP self);
    eAtModulePwRet (*CwAutoRxMBitEnable)(AtPwCESoP self, eBool enable);
    eBool (*CwAutoRxMBitIsEnabled)(AtPwCESoP self);
    eBool (*CwAutoRxMBitIsConfigurable)(AtPwCESoP self);

    eAtPwCESoPMode (*ModeGet)(AtPwCESoP self);
    eAtRet (*RxCasModeSet)(AtPwCESoP self, eAtPwCESoPCasMode mode);
    eAtPwCESoPCasMode (*RxCasModeGet)(AtPwCESoP self);

    /* CAS IDLE code */
    eAtModulePwRet (*CasIdleCode1Set)(AtPwCESoP self, uint8 abcd1);
    uint8 (*CasIdleCode1Get)(AtPwCESoP self);
    eAtModulePwRet (*CasIdleCode2Set)(AtPwCESoP self, uint8 abcd2);
    uint8 (*CasIdleCode2Get)(AtPwCESoP self);
    eAtModulePwRet (*CasAutoIdleEnable)(AtPwCESoP self, eBool enable);
    eBool (*CasAutoIdleIsEnabled)(AtPwCESoP self);

    }tAtPwCESoPMethods;

/* CESoP pseudowire object */
typedef struct tAtPwCESoP
    {
    tAtPw super;
    const tAtPwCESoPMethods* methods;

    /* Private attribute */
    uint8 mode; /* eAtPwCESoPMode */
    }tAtPwCESoP;

/* CEP methods */
typedef struct tAtPwCepMethods
    {
    eAtModulePwRet (*Equip)(AtPwCep self, AtChannel channel, eBool equip);
    eBool (*IsEquipped)(AtPwCep self, AtChannel channel);

    /* Equipped Bit Mask - EBM */
    eAtModulePwRet (*CwEbmEnable)(AtPwCep self, eBool enable);
    eBool (*CwEbmIsEnabled)(AtPwCep self);

    /* Explicit Pointer Adjustment Relay - EPAR */
    eAtModulePwRet (*EparEnable)(AtPwCep self, eBool enable);
    eBool (*EparIsEnabled)(AtPwCep self);

    /* Auto N-Bit, P-Bit */
    eAtModulePwRet (*CwAutoNBitEnable)(AtPwCep self, eBool enable);
    eAtModulePwRet (*CwAutoPBitEnable)(AtPwCep self, eBool enable);
    eBool (*CwAutoNBitIsEnabled)(AtPwCep self);
    eBool (*CwAutoPBitIsEnabled)(AtPwCep self);

    eAtPwCepMode (*ModeGet)(AtPwCep self);
    }tAtPwCepMethods;

/* CEP pseudowire object */
typedef struct tAtPwCep
    {
    tAtPw super;
    const tAtPwCepMethods* methods;

    /* Private attribute */
    eAtPwCepMode mode;
    AtList equippedChannels;
    }tAtPwCep;

/* HDLC PW methods */
typedef struct tAtPwHdlcMethods
    {
    eAtModulePwRet (*PayloadTypeSet)(AtPwHdlc self, eAtPwHdlcPayloadType payloadType);
    eAtPwHdlcPayloadType (*PayloadTypeGet)(AtPwHdlc self);
    }tAtPwHdlcMethods;

/* HDLC pseudowire object */
typedef struct tAtPwHdlc
    {
    tAtPw super;
    const tAtPwHdlcMethods * methods;
    }tAtPwHdlc;

/* PPP pseudowire object */
typedef struct tAtPwPpp
    {
    tAtPwHdlc super;
    }tAtPwPpp;

/* FR pseudowire object */
typedef struct tAtPwFr
    {
    tAtPwHdlc super;
    }tAtPwFr;
    
/* ATM pseudowire object */
typedef struct tAtPwAtm
    {
    tAtPw super;
    }tAtPwAtm;

/* TOH methods */
typedef struct tAtPwTohMethods
    {
    eAtModulePwRet (*TohByteEnable)(AtPwToh self, uint8 sts1, uint32 tohByteBitmap, eBool enable);
    eBool  (*TohByteIsEnabled)(AtPwToh self, uint8 sts1, eAtSdhLineOverheadByte tohByteBitmap);
    uint16 (*NumEnabledBytesGet)(AtPwToh self);
    uint16 (*AllEnabledBytesGet)(AtPwToh self, uint32* tohByteBitmap, uint8 numBitmap);
    uint8  (*EnabledBytesByStsGet)(AtPwToh self, uint8 sts1, uint32* tohByteBitmap);
    }tAtPwTohMethods;

typedef struct tAtPwToh
    {
    tAtPw super;
    const tAtPwTohMethods* methods;

    }tAtPwToh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtPw        AtPwObjectInit(AtPw self, uint32 pwId, AtModulePw module, eAtPwType pwType);
AtPwAtm     AtPwAtmObjectInit(AtPwAtm self, uint32 pwId, AtModulePw module);
AtPwCESoP   AtPwCESoPObjectInit(AtPwCESoP self, uint32 pwId, AtModulePw module, eAtPwCESoPMode mode);
AtPwSAToP   AtPwSAToPObjectInit(AtPwSAToP self, uint32 pwId, AtModulePw module);
AtPwCep     AtPwCepObjectInit(AtPwCep self, uint32 pwId, AtModulePw module, eAtPwCepMode mode);
AtPwHdlc    AtPwHdlcObjectInit(AtPwHdlc self, uint32 pwId, AtModulePw module);
AtPwToh     AtPwTohObjectInit(AtPwToh self, uint32 pwId, AtModulePw module);
AtPwPpp     AtPwPppObjectInit(AtPwPpp self, uint32 pwId, eAtPwType pwType, AtModulePw module);
AtPwFr      AtPwFrObjectInit(AtPwFr self, uint32 pwId, AtModulePw module);

/* Utils */
eBool AtPwIsTdmPw(AtPw self);
eAtModulePwRet AtPwCircuitBindingCheck(AtPw self, AtChannel circuit);
uint8 AtPwCepNumEquippedChannelGet(AtPwCep self);
AtChannel AtPwCepEquippedChannelAtIndexGet(AtPwCep self, uint8 channelIndex);
eBool AtPwLopsClearThresholdIsSupported(AtPw self);
eBool AtPwMissingPacketDefectThresholdIsSupported(AtPw self);

void AtPwApsGroupSet(AtPw self, AtPwGroup apsGroup);
void AtPwHsGroupSet(AtPw self, AtPwGroup hsGroup);

void AtPwInterruptProcess(AtPw self, uint32 hwPw, AtHal hal);

/* PTCH */
eAtRet AtPwPtchInsertionModeSet(AtPw self, eAtPtchMode insertMode);
eAtPtchMode AtPwPtchInsertionModeGet(AtPw self);
eAtRet AtPwPtchServiceEnable(AtPw self, eBool enable);
eBool AtPwPtchServiceIsEnabled(AtPw self);

/* Tx active forcing */
eAtModulePwRet AtPwTxActiveForce(AtPw self, eBool active);
eBool AtPwTxActiveIsForced(AtPw self);

/* Subport VLAN */
eAtRet AtPwSubPortVlanIndexSet(AtPw self, uint32 subPortVlanIndex);
uint32 AtPwSubPortVlanIndexGet(AtPw self);

eBool AtPwNeedInitConfig(AtPw self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWINTERNAL_H_ */


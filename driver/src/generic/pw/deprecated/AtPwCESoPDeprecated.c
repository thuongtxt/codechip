/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwCESoPDeprecated.c
 *
 * Created Date: Jun 1, 2017
 *
 * Description : CESoPSN pseudowire
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtPwInternal.h"
#include "AtPwCESoPDeprecated.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Enable/disable transmitting M-bit
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCESoPCwAutoMBitEnable(AtPwCESoP self, eBool enable)
    {
    return AtPwCESoPCwAutoTxMBitEnable(self, enable);
    }

/*
 * Check if transmitting M-bit is enabled or not
 *
 * @param self This PW
 *
 * @retval cAtTrue - Auto M-Bit is enabled
 * @retval cAtFalse - Auto M-Bit is disabled
 */
eBool AtPwCESoPCwAutoMBitIsEnabled(AtPwCESoP self)
    {
    return AtPwCESoPCwAutoTxMBitIsEnabled(self);
    }

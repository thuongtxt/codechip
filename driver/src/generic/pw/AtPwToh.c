/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwToh.c
 *
 * Created Date: Feb 28, 2014
 *
 * Description : TOH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtPwInternal.h"
#include "AtPwToh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwTohMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePwRet TohByteEnable(AtPwToh self, uint8 sts1, uint32 tohByteBitmap, eBool enable)
    {
	AtUnused(enable);
	AtUnused(tohByteBitmap);
	AtUnused(sts1);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool TohByteIsEnabled(AtPwToh self, uint8 sts1, eAtSdhLineOverheadByte tohByteBitmap)
    {
	AtUnused(tohByteBitmap);
	AtUnused(sts1);
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static uint16 NumEnabledBytesGet(AtPwToh self)
    {
    return AtPwTohAllEnabledBytesGet(self, NULL, 0);
    }

static uint16 AllEnabledBytesGet(AtPwToh self, uint32* tohByteBitmap, uint8 numBitmap)
    {
	AtUnused(tohByteBitmap);
	AtUnused(self);
	AtUnused(numBitmap);
    /* Let concrete class do */
    return 0;
    }

static uint8  EnabledBytesByStsGet(AtPwToh self, uint8 sts1, uint32* tohByteBitmap)
    {
	AtUnused(tohByteBitmap);
	AtUnused(sts1);
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "toh";
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwToh);
    }

static void OverrideAtChannel(AtPwToh self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPwToh self)
    {
    OverrideAtChannel(self);
    }

static void MethodsInit(AtPwToh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TohByteEnable);
        mMethodOverride(m_methods, TohByteIsEnabled);
        mMethodOverride(m_methods, NumEnabledBytesGet);
        mMethodOverride(m_methods, AllEnabledBytesGet);
        mMethodOverride(m_methods, EnabledBytesByStsGet);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize object */
AtPwToh AtPwTohObjectInit(AtPwToh self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwObjectInit((AtPw)self, pwId, module, cAtPwTypeToh) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwToh
 * @{
 */

/**
 * Enable/disable Overhead bytes to be carried on TOH pseudowire
 *
 * @param self This PW
 * @param sts1 STS ID
 * @param tohByteBitmap TOH bytes bitmap, bit position is defined by @ref eAtSdhLineOverheadByte
 * @param enable cAtTrue if Overhead bytes will be carried, cAtFalse if they will be not carried
 * @return AT return code
 */
eAtModulePwRet AtPwTohByteEnable(AtPwToh self, uint8 sts1, uint32 tohByteBitmap, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TohByteEnable(self, sts1, tohByteBitmap, enable);

    return cAtError;
    }

/**
 * Check if Overhead bytes are carried on Pseudowire
 *
 * @param self This PW
 * @param sts1 STS ID
 * @param ohByte @ref eAtSdhLineOverheadByte "SDH line overhead byte"
 * @retval cAtTrue Overhead byte is carried on Pseudowire
 * @retval cAtFalse Overhead byte is NOT carried on Pseudowire
 */
eBool AtPwTohByteIsEnabled(AtPwToh self, uint8 sts1, eAtSdhLineOverheadByte ohByte)
    {
    if (self)
        return mMethodsGet(self)->TohByteIsEnabled(self, sts1, ohByte);

    return cAtError;
    }

/**
 * Get number of carried bytes
 *
 * @param self This PW
 * @return Number of carried bytes
 */
uint16 AtPwTohNumEnabledBytesGet(AtPwToh self)
    {
    mAttributeGet(NumEnabledBytesGet, uint16, 0);
    }

/**
 * Get all of carried bytes
 *
 * @param self This PW
 * @param tohByteBitmaps Pointer to output array of TOH byte bitmaps. Number of item in array should
 *                       correspond with bound SDH line rate. For example, if bound line rate is STM-1,
 *                       number of item should be 3, corresponding to 3 STS-1s in that line.
 *                       This parameter may be null, if it is null, number of carried is returned only
 * @param numBitmap Number of item of array tohByteBitmaps
 *
 * @return Number of carried bytes
 */
uint16 AtPwTohAllEnabledBytesGet(AtPwToh self, uint32 *tohByteBitmaps, uint8 numBitmap)
    {
    if (self)
        return mMethodsGet(self)->AllEnabledBytesGet(self, tohByteBitmaps, numBitmap);

    return 0;
    }

/**
 * Get all of carried OH byte on one STS-1
 *
 * @param self This PW
 * @param sts1 STS-1 index
 * @param tohByteBitmap Pointer to output bitmap. This parameter may be null, if it
 *                      is null, number of carried is returned only
 * @return Number of carried bytes on this STS
 */
uint8 AtPwTohEnabledBytesByStsGet(AtPwToh self, uint8 sts1, uint32 * tohByteBitmap)
    {
    if (self)
        return mMethodsGet(self)->EnabledBytesByStsGet(self, sts1, tohByteBitmap);

    return 0;
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwMefPsn.c
 *
 * Created Date: Nov 22, 2012
 *
 * Description : MEF PSN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPwPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwMefPsn)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPwMefPsnMethods m_methods;

/* Override */
static tAtPwPsnMethods  m_AtPwPsnOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* MEF header structure

|    1  |    2  |   3   |   4   |   5   |   6   |    7  |    8  |
+-------+-------+-------+-------+-------+-------+-------+-------+
|                     ECID (20 bits)                            | 0
+                                                               +
|                                                               | 1
+                               +-------+-------+-------+-------+
|                               |                               | 2
+-------+-------+-------+-------+                               +
|                                0x102 (12 bits)                | 3
+-------+-------+-------+-------+-------+-------+-------+-------+
*/
static eBool IsMefOverMpls(AtPwPsn self)
    {
    eAtPwPsnType psnType = AtPwPsnTypeGet(self);
    if ((psnType == cAtPwPsnTypeMef) && (AtPwPsnTypeGet(AtPwPsnLowerPsnGet(self)) == cAtPwPsnTypeMpls))
        return cAtTrue;
    return cAtFalse;
    }

static uint8 HeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer)
    {
    uint32     ecid;
    uint32     dIndex = 0;
    AtPwMefPsn psn = (AtPwMefPsn)self;
    AtUnused(pw);

    if (IsMefOverMpls(self))
        {
        if ((psn->pDestMac != NULL) && (psn->pSourceMac != NULL))
            {
            uint8 vlanIndex, numVlan, maxNumVlan;
            const uint16 mefEthType = 0x88D8;
            AtOsal osal = AtSharedDriverOsalGet();
            mMethodsGet(osal)->MemCpy(osal, &buffer[dIndex], psn->destMac, sizeof(psn->destMac));
            dIndex = dIndex + cAtPwPsnMacAddrNumByte;
            mMethodsGet(osal)->MemCpy(osal, &buffer[dIndex], psn->sourceMac, sizeof(psn->sourceMac));
            dIndex = dIndex + cAtPwPsnMacAddrNumByte;

            numVlan = AtPwMefPsnNumVlanTags(psn);
            maxNumVlan = AtPwMefPsnMaxNumVlanTags(psn);
            if (numVlan > maxNumVlan)
                numVlan = maxNumVlan;

            for (vlanIndex = 0; vlanIndex < numVlan; vlanIndex++)
                {
                uint16 vlanType = psn->vlanTypes[vlanIndex];
                tAtEthVlanTag *vlanTag = &psn->vlans[vlanIndex];

                /* Construct VLAN Type */
                buffer[dIndex] = (uint8)(vlanType >> 8);
                dIndex++;
                buffer[dIndex] = vlanType & cBit7_0;
                dIndex++;

                /* Construct VLAN priority - cfi - vlan Id */
                buffer[dIndex] = (uint8)(((vlanTag->priority & cBit2_0) << 5) |
                                         ((vlanTag->cfi & cBit0) << 4) |
                                         ((vlanTag->vlanId >> 8) & cBit3_0));
                dIndex++;

                buffer[dIndex] = vlanTag->vlanId & cBit7_0;
                dIndex++;
                }

            /* Add MEF Ethernet type to buffer */
            buffer[dIndex] = (uint8)(mefEthType >> 8);
            dIndex++;
            buffer[dIndex] = mefEthType & cBit7_0;
            dIndex++;
            }
        else
            return cAtErrorModeNotSupport;
        }

    ecid = AtPwMefPsnTxEcIdGet((AtPwMefPsn)self);
    buffer[dIndex] = (ecid >> 12) & cBit7_0;
    dIndex = dIndex + 1;
    buffer[dIndex] = (ecid >> 4) & cBit7_0;
    dIndex = dIndex + 1;
    buffer[dIndex] = (uint8)(((ecid & cBit3_0) << 4) | (0x102 >> 8));
    dIndex = dIndex + 1;
    buffer[dIndex] = (0x102 & cBit7_0);
    dIndex = dIndex + 1;

    return (uint8)dIndex;
    }

static const char * TypeString(AtPwPsn self)
    {
	AtUnused(self);
    return "MEF";
    }

static const char *ToString(AtObject self)
    {
    static char buf[128];
    AtPwMefPsn mefPsn = (AtPwMefPsn)self;

    AtSprintf(buf,
              "%s: TxEcId.%u, ExpEcId.%u",
              AtPwPsnTypeString((AtPwPsn)self),
              AtPwMefPsnTxEcIdGet(mefPsn),
              AtPwMefPsnExpectedEcIdGet(mefPsn));

    return buf;
    }

static AtPwPsn Copy(AtPwPsn self, AtPwPsn replicaPsn)
    {
    AtPwMefPsn thisPsn = (AtPwMefPsn)self;
    AtPwMefPsn replicaMef = (AtPwMefPsn)replicaPsn;
    eAtRet ret;

    ret = AtPwMefPsnTxEcIdSet(replicaMef, AtPwMefPsnTxEcIdGet(thisPsn));
    ret |= AtPwMefPsnExpectedEcIdSet(replicaMef, AtPwMefPsnExpectedEcIdGet(thisPsn));

    if ((thisPsn->pDestMac != NULL) || (thisPsn->pSourceMac != NULL) || (thisPsn->pVlans != NULL))
        {
        uint8 vlanIndex;
        uint8 mac[cAtMacAddressLen];
        ret |= AtPwMefPsnSourceMacGet(thisPsn, mac);
        ret |= AtPwMefPsnSourceMacSet(replicaMef, mac);
        ret |= AtPwMefPsnDestMacGet(thisPsn, mac);
        ret |= AtPwMefPsnDestMacSet(replicaMef, mac);
        ret |= AtPwMefPsnExpectedMacGet(thisPsn, mac);
        ret |= AtPwMefPsnExpectedMacSet(replicaMef, mac);

        replicaMef->currentNumVlans = 0;
        for (vlanIndex = 0; vlanIndex < AtPwMefPsnNumVlanTags(thisPsn); vlanIndex++)
            {
            uint16 vlanType;
            tAtEthVlanTag vlanTag;
            AtPwMefPsnVlanTagAtIndex(thisPsn, vlanIndex, &vlanType, &vlanTag);
            AtPwMefPsnVlanTagAdd(replicaMef, vlanType, &vlanTag);
            }
        }

    if (ret != cAtOk)
        return NULL;

    return replicaPsn;
    }

static AtObject Clone(AtObject self)
    {
    AtPwPsn thisPsn = (AtPwPsn)self;

    /* Clone itself */
    AtPwPsn newPsn = (AtPwPsn)AtPwMefPsnNew();

    /* Clone lower PSN */
    if ((Copy(thisPsn, newPsn) == NULL) || (AtPwPsnLowerPsnClone(newPsn, thisPsn) == NULL))
        {
        AtObjectDelete((AtObject)newPsn);
        return NULL;
        }

    return (AtObject)newPsn;
    }

static eBool EcIdIsValid(uint32 ecId)
    {
    if (ecId > cBit19_0)
        return cAtFalse;

    return cAtTrue;
    }

static eAtModulePwRet DestMacSet(AtPwMefPsn self, uint8 *dmac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, self->destMac, dmac, sizeof(self->destMac));
    self->pDestMac = self->destMac;
    return cAtOk;
    }

static eAtModulePwRet DestMacGet(AtPwMefPsn self, uint8 *dmac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, dmac, self->destMac, sizeof(self->destMac));
    return cAtOk;
    }

static eAtModulePwRet SourceMacSet(AtPwMefPsn self, uint8 *smac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, self->sourceMac, smac, sizeof(self->sourceMac));
    self->pSourceMac = self->sourceMac;
    return cAtOk;
    }

static eAtModulePwRet SourceMacGet(AtPwMefPsn self, uint8 *smac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, smac, self->sourceMac, sizeof(self->sourceMac));
    return cAtOk;
    }

static eAtModulePwRet ExpectedMacSet(AtPwMefPsn self, uint8 *expectedMac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, self->expectedMac, expectedMac, sizeof(self->expectedMac));
    self->pExpectedMac = self->expectedMac;
    return cAtOk;
    }

static eAtModulePwRet ExpectedMacGet(AtPwMefPsn self, uint8 *expectedMac)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, expectedMac, self->expectedMac, sizeof(self->expectedMac));
    return cAtOk;
    }

static eAtModulePwRet VlanTagAdd(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 currentNumVlans = AtPwMefPsnNumVlanTags(self);

    if (currentNumVlans == AtPwMefPsnMaxNumVlanTags(self))
        return cAtErrorRsrcNoAvail;

    mMethodsGet(osal)->MemCpy(osal, &self->vlans[currentNumVlans], tag, sizeof(tAtEthVlanTag));
    self->vlanTypes[currentNumVlans] = vlanType;
    self->currentNumVlans++;
    self->pVlans = self->vlans;
    return cAtOk;
    }

static eBool SameVlan(AtPwMefPsn self, uint8 vlanIndex, uint16 vlanType, const tAtEthVlanTag *tag)
    {
    tAtEthVlanTag *thisTag = &self->vlans[vlanIndex];
    if (self->vlanTypes[vlanIndex] != vlanType)
        return cAtFalse;

    if (thisTag->cfi != tag->cfi)
        return cAtFalse;

    if (thisTag->priority != tag->priority)
        return cAtFalse;

    if (thisTag->vlanId != tag->vlanId)
        return cAtFalse;

    return cAtTrue;
    }

static eAtModulePwRet VlanTagRemove(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag)
    {
    uint8 vlanIndex;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 currentNumVlans = AtPwMefPsnNumVlanTags(self);
    uint8 maxNumVlan;

    if (currentNumVlans == 0)
        return cAtErrorRsrcNoAvail;

    /* FIXME: The following code only works with products that support max 2 VLANs
     * and it should be updated to support more than 2 VLANs */
    maxNumVlan = AtPwMefPsnMaxNumVlanTags(self);
    if (currentNumVlans > maxNumVlan)
        currentNumVlans = maxNumVlan;

    for (vlanIndex = 0; vlanIndex < currentNumVlans; vlanIndex++)
        {
        if (!SameVlan(self, vlanIndex, vlanType, tag))
            continue;

        if (vlanIndex < (currentNumVlans - 1))
            mMethodsGet(osal)->MemCpy(osal, &self->vlans[vlanIndex], &self->vlans[currentNumVlans - 1], sizeof(tAtEthVlanTag));
        self->currentNumVlans--;

        return cAtOk;
        }

    return cAtErrorRsrcNoAvail;
    }

static uint8 NumVlanTags(AtPwMefPsn self)
    {
    return self->currentNumVlans;
    }

static eAtModulePwRet VlanTagAtIndex(AtPwMefPsn self, uint8 tagIndex, uint16 *vlanType, tAtEthVlanTag *tag)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if ((tagIndex >= AtPwMefPsnNumVlanTags(self)) || (tagIndex >= AtPwMefPsnMaxNumVlanTags(self)))
        return cAtErrorOutOfRangParm;

    if (vlanType)
        *vlanType = self->vlanTypes[tagIndex];
    if (tag)
        mMethodsGet(osal)->MemCpy(osal, tag, &self->vlans[tagIndex], sizeof(tAtEthVlanTag));

    return cAtOk;
    }

static eBool VlanTagIsValid(const tAtEthVlanTag *tag)
    {
    if (tag->priority > 7)
        return cAtFalse;
    if (tag->cfi > 1)
        return cAtFalse;
    return cAtTrue;
    }

static eBool MacIsIdentical(uint8* macOfPsn1, uint8* macOfPsn2)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if ((macOfPsn1 == NULL) && (macOfPsn2 == NULL))
        return cAtTrue;

    if ((macOfPsn1 == NULL) || (macOfPsn2 == NULL))
        return cAtFalse;

    if (mMethodsGet(osal)->MemCmp(osal, macOfPsn1, macOfPsn2, cAtMacAddressLen))
        return cAtFalse;

    return cAtTrue;
    }

static eBool VlansAreIdentical(AtPwMefPsn self, AtPwMefPsn another)
    {
    uint8 vlan_i;

    if (AtPwMefPsnNumVlanTags(self) != AtPwMefPsnNumVlanTags(another))
        return cAtFalse;

    for (vlan_i = 0; vlan_i < AtPwMefPsnNumVlanTags(self); vlan_i++)
        {
        tAtEthVlanTag vlanTag1, vlanTag2;
        uint16 vlan1Type, vlan2Type;
        eAtModulePwRet ret;

        ret = AtPwMefPsnVlanTagAtIndex(self, vlan_i, &vlan1Type, &vlanTag1);
        ret |= AtPwMefPsnVlanTagAtIndex(another, vlan_i, &vlan2Type, &vlanTag2);

        if ((ret != cAtOk) || (vlan1Type != vlan2Type))
            return cAtFalse;

        if ((vlanTag1.cfi != vlanTag2.cfi)           ||
            (vlanTag1.priority != vlanTag2.priority) ||
            (vlanTag1.vlanId != vlanTag2.vlanId))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool IsIdentical(AtPwPsn self, AtPwPsn another)
    {
    if (AtPwPsnTypeGet(another) != cAtPwPsnTypeMef)
        return cAtFalse;

    if ((AtPwMefPsnTxEcIdGet(mThis(self))       != AtPwMefPsnTxEcIdGet(mThis(another))) ||
        (AtPwMefPsnExpectedEcIdGet(mThis(self)) != AtPwMefPsnExpectedEcIdGet(mThis(another))))
        return cAtFalse;

    if ((!MacIsIdentical(mThis(self)->pDestMac, mThis(another)->pDestMac)) ||
        (!MacIsIdentical(mThis(self)->pSourceMac, mThis(another)->pSourceMac)) ||
        (!MacIsIdentical(mThis(self)->pExpectedMac, mThis(another)->pExpectedMac)))
        return cAtFalse;

    return VlansAreIdentical(mThis(self), mThis(another));
    }

static void SerializeVlans(AtObject self, AtCoder encoder)
    {
    AtPwMefPsn object = (AtPwMefPsn)self;
    uint8 vlan_i;

    for (vlan_i = 0; vlan_i < object->currentNumVlans; vlan_i++)
        {
        tAtEthVlanTag *vlan = &(object->vlans[vlan_i]);
        uint16 vlanType = object->vlanTypes[vlan_i];
        char key[16];

        AtSnprintf(key, sizeof(key) - 1, "vlan[%u]", vlan_i);
        AtCoderEncodeUInt(encoder, vlan->priority, key);
        AtSnprintf(key, sizeof(key)- 1, "vlanTypes[%u]", vlan_i);
        AtCoderEncodeUInt(encoder, vlanType, key);
        }

    mEncodeUInt(currentNumVlans);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwMefPsn object = (AtPwMefPsn)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(txEcId);
    mEncodeUInt(expectedEcId);
    mEncodeUInt8Array(pSourceMac, cAtPwPsnMacAddrNumByte);
    mEncodeUInt8Array(pDestMac, cAtPwPsnMacAddrNumByte);
    mEncodeUInt8Array(pExpectedMac, cAtPwPsnMacAddrNumByte);

    SerializeVlans(self, encoder);
    }

static uint8 LengthInBytes(AtPwPsn self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 ExpectedLabelGet(AtPwPsn self)
    {
    return AtPwMefPsnExpectedEcIdGet(mThis(self));
    }

static void OverrideAtObject(AtPwMefPsn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Clone);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPwPsn(AtPwMefPsn self)
    {
    AtPwPsn psn = (AtPwPsn)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwPsnOverride, mMethodsGet(psn), sizeof(m_AtPwPsnOverride));

        mMethodOverride(m_AtPwPsnOverride, HeaderBuild);
        mMethodOverride(m_AtPwPsnOverride, TypeString);
        mMethodOverride(m_AtPwPsnOverride, Copy);
        mMethodOverride(m_AtPwPsnOverride, IsIdentical);
        mMethodOverride(m_AtPwPsnOverride, LengthInBytes);
        mMethodOverride(m_AtPwPsnOverride, ExpectedLabelGet);
        }

    mMethodsSet(psn, &m_AtPwPsnOverride);
    }

static void Override(AtPwMefPsn self)
    {
    OverrideAtPwPsn(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtPwMefPsn self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, DestMacSet);
        mMethodOverride(m_methods, DestMacGet);
        mMethodOverride(m_methods, SourceMacSet);
        mMethodOverride(m_methods, SourceMacGet);
        mMethodOverride(m_methods, VlanTagAdd);
        mMethodOverride(m_methods, VlanTagRemove);
        mMethodOverride(m_methods, NumVlanTags);
        mMethodOverride(m_methods, VlanTagAtIndex);
        mMethodOverride(m_methods, ExpectedMacSet);
        mMethodOverride(m_methods, ExpectedMacGet);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize object */
static AtPwMefPsn AtPwMefPsnObjectInit(AtPwMefPsn self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwMefPsn));

    /* Super constructor should be called first */
    if (AtPwPsnObjectInit((AtPwPsn)self, cAtPwPsnTypeMef) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwMefPsn
 * @{
 */
/**
 * Create new AtPwMefPsn object
 *
 * @return AtPwMefPsn object
 */
AtPwMefPsn AtPwMefPsnNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwMefPsn newPsn = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtPwMefPsn));
    if (newPsn == NULL)
        return NULL;

    /* Construct it */
    return AtPwMefPsnObjectInit(newPsn);
    }

/**
 * Set TX MEF EC ID
 *
 * @param self This PSN
 * @param ecId TX EC ID
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnTxEcIdSet(AtPwMefPsn self, uint32 ecId)
    {
    if (self == NULL)
        return cAtError;

    if (!EcIdIsValid(ecId))
        return cAtErrorInvlParm;

    self->txEcId = ecId;

    return cAtOk;
    }

/**
 * Get TX EC ID
 *
 * @param self This PSH
 *
 * @return TX EC ID
 */
uint32 AtPwMefPsnTxEcIdGet(AtPwMefPsn self)
    {
    if (self == NULL)
        return 0;

    return self->txEcId;
    }

/**
 * Set expected EC ID
 *
 * @param self This PSN
 * @param ecId Expected EC ID
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnExpectedEcIdSet(AtPwMefPsn self, uint32 ecId)
    {
    if (self == NULL)
        return cAtError;

    if (!EcIdIsValid(ecId))
        return cAtErrorInvlParm;

    self->expectedEcId = ecId;

    return cAtOk;
    }

/**
 * Get expected EC ID
 *
 * @param self This PSN
 *
 * @return Expected EC ID
 */
uint32 AtPwMefPsnExpectedEcIdGet(AtPwMefPsn self)
    {
    if (self == NULL)
        return 0;

    return self->expectedEcId;
    }

/**
 * Set Destination MAC
 *
 * @param self This PSN
 * @param dmac Destination MAC. It must be an array of 6 bytes.
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnDestMacSet(AtPwMefPsn self, uint8 *dmac)
    {
    if ((self == NULL) || (dmac == NULL))
        return cAtErrorNullPointer;
    return mMethodsGet(self)->DestMacSet(self, dmac);
    }

/**
 * Get Destination MAC
 *
 * @param self This PSN
 * @param [out] dmac Destination MAC. It must be an array of 6 bytes.
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnDestMacGet(AtPwMefPsn self, uint8 *dmac)
    {
    if ((self == NULL) || (dmac == NULL))
        return cAtErrorNullPointer;
    return mMethodsGet(self)->DestMacGet(self, dmac);
    }

/**
 * Set Source MAC
 *
 * @param self This PSN
 * @param smac Source MAC. It must be an array of 6 bytes.
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnSourceMacSet(AtPwMefPsn self, uint8 *smac)
    {
    if ((self == NULL) || (smac == NULL))
        return cAtErrorNullPointer;
    return mMethodsGet(self)->SourceMacSet(self, smac);
    }

/**
 * Get Source MAC
 *
 * @param self This PSN
 * @param [out] smac Source MAC. It must be an array of 6 bytes.
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnSourceMacGet(AtPwMefPsn self, uint8 *smac)
    {
    if ((self == NULL) || (smac == NULL))
        return cAtErrorNullPointer;
    return mMethodsGet(self)->SourceMacGet(self, smac);
    }

/**
 * Set Expected MAC
 *
 * @param self This PSN
 * @param expectedMac Expected MAC. It must be an array of 6 bytes.
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnExpectedMacSet(AtPwMefPsn self, uint8 *expectedMac)
    {
    if ((self == NULL) || (expectedMac == NULL))
        return cAtErrorNullPointer;

    return mMethodsGet(self)->ExpectedMacSet(self, expectedMac);
    }

/**
 * Get Expected MAC
 *
 * @param self This PSN
 * @param [out] expectedMac Expected MAC. It must be an array of 6 bytes.
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnExpectedMacGet(AtPwMefPsn self, uint8 *expectedMac)
    {
    if ((self == NULL) || (expectedMac == NULL))
        return cAtErrorNullPointer;
    return mMethodsGet(self)->ExpectedMacGet(self, expectedMac);
    }

/**
 * Add a VLAN tag with specified type
 *
 * @param self This PSN
 * @param vlanType Type. It can be 0x88a8, 0x8100
 * @param tag Tag
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnVlanTagAdd(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag)
    {
    if ((self == NULL) || (tag == NULL))
        return cAtErrorNullPointer;

    if (!VlanTagIsValid(tag))
        return cAtErrorInvlParm;

    return mMethodsGet(self)->VlanTagAdd(self, vlanType, tag);
    }

/**
 * Remove a VLAN tag with specified type
 *
 * @param self This PSN
 * @param vlanType Type. It can be 0x88a8, 0x8100
 * @param tag Tag to be removed
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnVlanTagRemove(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag)
    {
    if ((self == NULL) || (tag == NULL))
        return cAtErrorNullPointer;
    return mMethodsGet(self)->VlanTagRemove(self, vlanType, tag);
    }

/**
 * Get number of added VLAN tags
 *
 * @param self This PSN
 *
 * @return Number of added VLAN tags
 */
uint8 AtPwMefPsnNumVlanTags(AtPwMefPsn self)
    {
    if (self)
        return mMethodsGet(self)->NumVlanTags(self);
    return 0;
    }

/**
 * Get max number of VLAN tags can be added.
 *
 * @param self This PSN
 * @return Max number of VLAN tags can be added.
 */
uint8 AtPwMefPsnMaxNumVlanTags(AtPwMefPsn self)
    {
    AtUnused(self);
    return 2;
    }

/**
 * Get VLAN tag at specified index
 *
 * @param self This PSN
 * @param tagIndex VLAN tag index
 * @param [out] vlanType VLAN type
 * @param [out] tag VLAN tag
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMefPsnVlanTagAtIndex(AtPwMefPsn self, uint8 tagIndex, uint16 *vlanType, tAtEthVlanTag *tag)
    {
    if (self)
        return mMethodsGet(self)->VlanTagAtIndex(self, tagIndex, vlanType, tag);
    return cAtErrorNullPointer;
    }

/** @} */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwPsnInternal.h
 * 
 * Created Date: Nov 21, 2012
 *
 * Description : PW PSN classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWPSNINTERNAL_H_
#define _ATPWPSNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwPsn.h"
#include "../../man/AtDriverInternal.h"
#include "../../common/AtChannelInternal.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtPwPsnIpV4AddrNumByte 4
#define cAtPwPsnIpV6AddrNumByte 16
#define cAtPwPsnMacAddrNumByte  6
#define cAtPwPsnMaxNumVlans     2
#define cAtMplsPsnMaxNumOuterLabels 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwPsnMethods
    {
    uint8 (*HeaderBuild)(AtPwPsn self, AtPw pw, uint8 *buffer);
    const char * (*TypeString)(AtPwPsn self);
    eBool (*IsValid)(AtPwPsn self);
    AtPwPsn (*Copy)(AtPwPsn self, AtPwPsn replicaPsn);
    eBool (*IsIdentical)(AtPwPsn self, AtPwPsn another);
    uint8 (*LengthInBytes)(AtPwPsn self);
    uint32 (*ExpectedLabelGet)(AtPwPsn self);
    }tAtPwPsnMethods;

typedef struct tAtPwPsn
    {
    tAtObject super;

    /* PSN methods */
    const tAtPwPsnMethods *methods;

    /* Private attributes */
    AtPwPsn higherPsn;
    AtPwPsn lowerPsn;
    eAtPwPsnType type;

    /* Description */
    char *description;
    }tAtPwPsn;

typedef struct tAtPwMplsPsn
    {
    tAtPwPsn super;

    tAtPwMplsLabel innerLabel;
    AtList outerLabels;
    uint32 expectedLabel;
    }tAtPwMplsPsn;

typedef struct tAtPwMefPsnMethods
    {
    eAtModulePwRet (*DestMacSet)(AtPwMefPsn self, uint8 *dmac);
    eAtModulePwRet (*DestMacGet)(AtPwMefPsn self, uint8 *dmac);
    eAtModulePwRet (*SourceMacSet)(AtPwMefPsn self, uint8 *smac);
    eAtModulePwRet (*SourceMacGet)(AtPwMefPsn self, uint8 *smac);
    eAtModulePwRet (*ExpectedMacSet)(AtPwMefPsn self, uint8 *expectedMac);
    eAtModulePwRet (*ExpectedMacGet)(AtPwMefPsn self, uint8 *expectedMac);
    eAtModulePwRet (*VlanTagAdd)(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag);
    eAtModulePwRet (*VlanTagRemove)(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag);
    uint8 (*NumVlanTags)(AtPwMefPsn self);
    eAtModulePwRet (*VlanTagAtIndex)(AtPwMefPsn self, uint8 tagIndex, uint16 *vlanType, tAtEthVlanTag *tag);

    /* Internal methods */
    uint8 (*MaxNumVlanTags)(AtPwMefPsn self);
    }tAtPwMefPsnMethods;

typedef struct tAtPwMefPsn
    {
    tAtPwPsn super;
    const tAtPwMefPsnMethods *methods;

    uint32 txEcId;
    uint32 expectedEcId;
    uint8 sourceMac[cAtPwPsnMacAddrNumByte];
    uint8 *pSourceMac;
    uint8 destMac[cAtPwPsnMacAddrNumByte];
    uint8 *pDestMac;
    uint8 expectedMac[cAtPwPsnMacAddrNumByte];
    uint8 *pExpectedMac;
    tAtEthVlanTag vlans[cAtPwPsnMaxNumVlans];
    tAtEthVlanTag *pVlans;
    uint16 vlanTypes[cAtPwPsnMaxNumVlans];
    uint8 currentNumVlans;
    }tAtPwMefPsn;

typedef struct tAtPwUdpPsn
    {
    tAtPwPsn super;

    uint16 sourcePort;
    uint16 destPort;
    uint16 expectedPort;
    }tAtPwUdpPsn;

typedef struct tAtPwIpPsnMethods
    {
    eAtModulePwRet (*SourceAddressSet)(AtPwIpPsn self, const uint8 *sourceAddress);
    eAtModulePwRet (*SourceAddressGet)(AtPwIpPsn self, uint8 *sourceAddress);
    eAtModulePwRet (*DestAddressSet)(AtPwIpPsn self, const uint8 *destAddress);
    eAtModulePwRet (*DestAddressGet)(AtPwIpPsn self, uint8 *destAddress);
    }tAtPwIpPsnMethods;

typedef struct tAtPwIpPsn
    {
    tAtPwPsn super;
    const tAtPwIpPsnMethods *methods;
    }tAtPwIpPsn;

typedef struct tAtPwIpV4Psn
    {
    tAtPwIpPsn super;

    /* Private data */
    uint16 fragmentId;
    uint8 controlFlag;     /* [0..7] IP control flag */
    uint16 fragmentOffset; /* IP fragmentation offset */
    uint8 typeOfService;
    uint8 timeToLive;
    uint8 destAddress[cAtPwPsnIpV4AddrNumByte];
    uint8 sourceAddress[cAtPwPsnIpV4AddrNumByte];
    }tAtPwIpV4Psn;

typedef struct tAtPwIpV6Psn
    {
    tAtPwIpPsn super;

    uint8 destAddress[cAtPwPsnIpV6AddrNumByte];
    uint8 sourceAddress[cAtPwPsnIpV6AddrNumByte];
    uint8 trafficClass;
    uint32 flowLabel;
    uint8 hopLimit;
    }tAtPwIpV6Psn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtPwPsn AtPwPsnObjectInit(AtPwPsn self, eAtPwPsnType psnType);
AtPwIpPsn AtPwIpPsnObjectInit(AtPwIpPsn self, eAtPwPsnType psnType);

uint8 AtPwPsnHeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer);

/* Helpers */
AtPwPsn AtPwPsnLowerPsnClone(AtPwPsn self, AtPwPsn fromPsn);
eBool AtPwMplsPsnOuterLabelsAreIdentical(AtPwMplsPsn self, AtPwMplsPsn another);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWPSNINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwIpPsn.c
 *
 * Created Date: Feb 18, 2013
 *
 * Description : Pseudowire PSN layer
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPwIpPsnMethods m_methods;

/* Override */
static tAtPwPsnMethods m_AtPwPsnOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePwRet SourceAddressSet(AtPwIpPsn self, const uint8 *sourceAddress)
    {
	AtUnused(sourceAddress);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eAtModulePwRet SourceAddressGet(AtPwIpPsn self, uint8 *sourceAddress)
    {
	AtUnused(sourceAddress);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eAtModulePwRet DestAddressSet(AtPwIpPsn self, const uint8 *destAddress)
    {
	AtUnused(destAddress);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eAtModulePwRet DestAddressGet(AtPwIpPsn self, uint8 *destAddress)
    {
	AtUnused(destAddress);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static const char * TypeString(AtPwPsn self)
    {
	AtUnused(self);
    return "IP";
    }

static void MethodsInit(AtPwIpPsn self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, SourceAddressSet);
        mMethodOverride(m_methods, SourceAddressGet);
        mMethodOverride(m_methods, DestAddressSet);
        mMethodOverride(m_methods, DestAddressGet);
        }
    mMethodsSet(self, &m_methods);
    }

static void OverrideAtPwPsn(AtPwIpPsn self)
    {
    AtPwPsn psn = (AtPwPsn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwPsnOverride, mMethodsGet(psn), sizeof(m_AtPwPsnOverride));

        mMethodOverride(m_AtPwPsnOverride, TypeString);
        }

    mMethodsSet(psn, &m_AtPwPsnOverride);
    }

static void Override(AtPwIpPsn self)
    {
    OverrideAtPwPsn(self);
    }

/* To initialize object */
AtPwIpPsn AtPwIpPsnObjectInit(AtPwIpPsn self, eAtPwPsnType psnType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwIpPsn));

    /* Super constructor should be called first */
    if (AtPwPsnObjectInit((AtPwPsn)self, psnType) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwIpPsn
 * @{
 */

/**
 * Set source IP address
 *
 * @param self This PSN
 * @param sourceAddress Source IP address
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpPsnSourceAddressSet(AtPwIpPsn self, const uint8 *sourceAddress)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (sourceAddress == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->SourceAddressSet(self, sourceAddress);
    }

/**
 * Get source IP address
 *
 * @param self This PSN
 * @param [out] sourceAddress Source IP Address
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpPsnSourceAddressGet(AtPwIpPsn self, uint8 *sourceAddress)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    
    if (sourceAddress == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->SourceAddressGet(self, sourceAddress);
    }

/**
 * Set destination IP address
 *
 * @param self This PSN
 * @param destAddress Destination IP address
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpPsnDestAddressSet(AtPwIpPsn self, const uint8 *destAddress)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (destAddress == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->DestAddressSet(self, destAddress);
    }

/**
 * Get destination IP address
 *
 * @param self This PSN
 * @param [out] destAddress Destination IP address
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpPsnDestAddressGet(AtPwIpPsn self, uint8 *destAddress)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (destAddress == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->DestAddressGet(self, destAddress);
    }

/** @} */


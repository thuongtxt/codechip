/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtPwPsn.c
 *
 * Created Date: Sep 18, 2012
 *
 * Description : Pseudowire PSN layer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPsnInternal.h"
#include "../AtPwInternal.h"
#include "../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPwPsnMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 HeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer)
    {
	AtUnused(buffer);
	AtUnused(pw);
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static const char * TypeString(AtPwPsn self)
    {
	AtUnused(self);
    return "PSN";
    }

static void Delete(AtObject self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwPsn pwPsn = (AtPwPsn)self;

    AtObjectDelete((AtObject)(pwPsn->lowerPsn));
    pwPsn->lowerPsn = NULL;
    mMethodsGet(osal)->MemFree(osal, pwPsn->description);
    pwPsn->description = NULL;

    m_AtObjectMethods->Delete(self);
    }

static eBool IsValid(AtPwPsn self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtPwPsn Copy(AtPwPsn self, AtPwPsn dest)
    {
    /* Let sub-class do */
    AtUnused(self);
    AtUnused(dest);
    return NULL;
    }

static eBool IsIdentical(AtPwPsn self, AtPwPsn another)
    {
    /* Let sub-class do */
    AtUnused(self);
    AtUnused(another);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwPsn object = (AtPwPsn)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(higherPsn);
    mEncodeObjectDescription(lowerPsn);
    mEncodeUInt(type);
    }

static uint8 LengthInBytes(AtPwPsn self)
    {
    AtUnused(self);
    return 0;
    }

static AtPwPsn LowestPsn(AtPwPsn self)
    {
    AtPwPsn currentPsn = self;
    AtPwPsn nextPsn;

    while ((nextPsn = AtPwPsnLowerPsnGet(currentPsn)) != NULL)
        currentPsn = nextPsn;

    return currentPsn;
    }

static uint32 ExpectedLabelGet(AtPwPsn self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void OverrideAtObject(AtPwPsn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPwPsn self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPwPsn self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HeaderBuild);
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, IsValid);
        mMethodOverride(m_methods, Copy);
        mMethodOverride(m_methods, IsIdentical);
        mMethodOverride(m_methods, LengthInBytes);
        mMethodOverride(m_methods, ExpectedLabelGet);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize object */
AtPwPsn AtPwPsnObjectInit(AtPwPsn self, eAtPwPsnType psnType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwPsn));

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->type = psnType;

    return self;
    }

/*
 * Clone lower PSN
 *
 * @param self This PSN
 * @param fromPsn PSN want to clone
 *
 * @return self if clone successfully
 */
AtPwPsn AtPwPsnLowerPsnClone(AtPwPsn self, AtPwPsn fromPsn)
    {
    AtPwPsn lowerPsn;

    lowerPsn = AtPwPsnLowerPsnGet(fromPsn);
    if (lowerPsn == NULL)
        return self;

    lowerPsn = (AtPwPsn)AtObjectClone((AtObject)lowerPsn);
    AtPwPsnHigherPsnSet(lowerPsn, self);
    AtPwPsnLowerPsnSet(self, lowerPsn);

    return self;
    }

AtPwPsn AtPwPsnCopy(AtPwPsn self, AtPwPsn replicaPsn)
    {
    if (self)
        return mMethodsGet(self)->Copy(self, replicaPsn);

    return NULL;
    }

eBool AtPwPsnIsIdentical(AtPwPsn self, AtPwPsn another)
    {
    if ((self == NULL) && (another == NULL))
        return cAtTrue;

    if ((self == NULL) || (another == NULL))
        return cAtFalse;

    if (!mMethodsGet(self)->IsIdentical(self, another))
        return cAtFalse;

    return AtPwPsnIsIdentical(AtPwPsnLowerPsnGet(self), AtPwPsnLowerPsnGet(another));
    }

/**
 * @addtogroup AtPwPsn
 * @{
 */
/**
 * Set lower PSN
 *
 * @param self This PSN
 * @param lowerPsn Lower PSN
 *
 * @return AT return code
 */
eAtModulePwRet AtPwPsnLowerPsnSet(AtPwPsn self, AtPwPsn lowerPsn)
    {
    if (self == NULL)
        return cAtError;

    /* Lower PSN need to be configured */
    if (lowerPsn)
        {
        self->lowerPsn = lowerPsn;
        lowerPsn->higherPsn = self;
        }

    /* Lower PSN need to be set to NULL */
    else
        {
        if (self->lowerPsn)
            self->lowerPsn->higherPsn = NULL;
        self->lowerPsn = NULL;
        }

    return cAtOk;
    }

/**
 * Get lower PSN
 *
 * @param self This PSN
 *
 * @return Lower PSN
 */
AtPwPsn AtPwPsnLowerPsnGet(AtPwPsn self)
    {
    return self ? self->lowerPsn : NULL;
    }

/**
 * Set higher PSN
 *
 * @param self This PSN
 * @param higherPsn Higher PSN
 *
 * @return AT return code
 */
eAtRet AtPwPsnHigherPsnSet(AtPwPsn self, AtPwPsn higherPsn)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* Higher PSN need to be configured */
    if (higherPsn)
        {
        self->higherPsn = higherPsn;
        higherPsn->lowerPsn = self;
        }

    /* Higher PSN need to be set to NULL */
    else
        {
        if (self->higherPsn)
            self->higherPsn->lowerPsn = NULL;
        self->higherPsn = NULL;
        }

    return cAtOk;
    }

/**
 * Get higher PSN
 *
 * @param self This PSN
 *
 * @return Upper PSN
 */
AtPwPsn AtPwPsnHigherPsnGet(AtPwPsn self)
    {
    return self ? self->higherPsn : NULL;
    }

/**
 * Get PSN type
 *
 * @param self This PSN
 *
 * @return @ref eAtPwPsnType "PSN types"
 */
eAtPwPsnType AtPwPsnTypeGet(AtPwPsn self)
    {
    if (self)
        return self->type;

    return cAtPwPsnTypeUnknown;
    }

/**
 * Get PSN type string
 *
 * @param self This PSN
 *
 * @return PSN type string
 */
const char * AtPwPsnTypeString(AtPwPsn self)
    {
    if (self)
        return mMethodsGet(self)->TypeString(self);

    return "NULL";
    }

/**
 * Check if PSN is valid
 *
 * @param self This PSN
 * @return
 */
eBool AtPwPsnIsValid(AtPwPsn self)
    {
    AtPwPsn psn;

    if (self == NULL)
        return cAtFalse;

    /* The top PSN must be valid */
    if (!mMethodsGet(self)->IsValid(self))
        return cAtFalse;

    /* And all of lower PSN must also be valid */
    psn = self;
    while ((psn = AtPwPsnLowerPsnGet(psn)) != NULL)
        {
        if (!mMethodsGet(psn)->IsValid(psn))
            return cAtFalse;
        }

    return cAtTrue;
    }

/**
 * Build PSN header
 *
 * @param self This PSN
 * @param pw Pseudowire
 * @param buffer Buffer
 *
 * @return Header length
 */
uint8 AtPwPsnHeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer)
    {
    uint8 totalLength;
    AtPwPsn currentPsn, higherPsn;
    uint8 higherLayerLength;

    if (self == NULL)
        return 0;

    /* Start from the lowest PSN */
    currentPsn = LowestPsn(self);
    totalLength = mMethodsGet(currentPsn)->HeaderBuild(currentPsn, pw, buffer);

    /* And move up to higher PSNs */
    higherPsn = currentPsn;
    while ((higherPsn = AtPwPsnHigherPsnGet(higherPsn)) != NULL)
        {
        higherLayerLength = mMethodsGet(higherPsn)->HeaderBuild(higherPsn, pw, &(buffer[totalLength]));
        totalLength = (uint8)(totalLength + higherLayerLength);
        }

    return totalLength;
    }

/**
 * Return length in bytes of PSN, including all lower PSNs in chain
 *
 * @param self This PSN
 * @return Length in bytes of PSN
 */
uint32 AtPwPsnLengthInBytes(AtPwPsn self)
    {
    uint32 psnLength = 0;
    AtPwPsn psn = self;
    while (psn)
        {
        psnLength += mMethodsGet(psn)->LengthInBytes(psn);
        psn = AtPwPsnLowerPsnGet(psn);
        }

    return psnLength;
    }

/**
 * Get expected label
 *
 * @param self This PSN
 *
 * @return Expected label.
 */
uint32 AtPwPsnExpectedLabelGet(AtPwPsn self)
    {
    if (self)
        return mMethodsGet(self)->ExpectedLabelGet(self);
    return cInvalidUint32;
    }

/**
 * @}
 */

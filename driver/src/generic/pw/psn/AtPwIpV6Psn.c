/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwIpV6Psn.c
 *
 * Created Date: Nov 22, 2012
 *
 * Description : IPv6 PSN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/* IP-v6 structure
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |    1  |    2  |   3   |   4   |   5   |   6   |    7  |    8  |
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |            IPVER=6            |    Traffic Class (bits 1-4)   | 0
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |     Traffic Class (bits 5-8)  |                               | 1
    +-------+-------+-------+-------+            Flow Label         +
    |                                                               | 2
    +                                                               +
    |                                                               | 3
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 4
    +                        Payload Length                         +
    |                            (2 bytes)                          | 5
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                    Next header (8 bits)                       | 6
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                      Hop Limit (8 bits)                       | 7
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 8
    +                      Source IP Address                        + ...
    |                           (16 bytes)                          | 21
    |                                                               | 22
    |                                                               | 23
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 24
    +                    Destination IP Address                     + ...
    |                            (16 bytes)                         |
    |                                                               | 38
    |                                                               | 39
    +-------+-------+-------+-------+-------+-------+-------+-------+
*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPwPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtIpv6VerRstVal    6
#define cThaIpProtocolUdp   0x11
#define cThaIpProtocolMpls  0x89

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwIpV6Psn)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPwPsnMethods   m_AtPwPsnOverride;
static tAtPwIpPsnMethods m_AtPwIpPsnOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 UperPsnProtocolGet(AtPwPsn psn)
    {
    switch (AtPwPsnTypeGet(psn))
        {
        case cAtPwPsnTypeUdp:  return cThaIpProtocolUdp;
        case cAtPwPsnTypeMpls: return cThaIpProtocolMpls;

        case cAtPwPsnTypeUnknown:
        case cAtPwPsnTypeMef:
        case cAtPwPsnTypeIPv4:
        case cAtPwPsnTypeIPv6:
        default:
            return 0;
        }
    }

static uint8 HeaderArrayBuild(AtPwIpV6Psn self, AtPw pw, uint8 *buffer, AtPwPsn uperPsn)
    {
    uint8 byte_i = 0, protocol;
    uint8 srcAddress[cAtPwPsnIpV6AddrNumByte];
	AtUnused(pw);

    if (AtPwIpPsnSourceAddressGet((AtPwIpPsn)self, srcAddress) != cAtOk)
        return 0;

    buffer[byte_i++] = (cAtIpv6VerRstVal << 4 ) | (self->trafficClass >> 4);
    buffer[byte_i++] = (uint8)(((self->trafficClass & cBit3_0) << 4) | (self->flowLabel >> 16));
    buffer[byte_i++] = ((self->flowLabel >> 8) & cBit7_0);
    buffer[byte_i++] = (self->flowLabel & cBit7_0);
    
    /* Payload length will be updated by HW */
    buffer[byte_i++] = 0;
    buffer[byte_i++] = 0;

    /* Next header */
    protocol = UperPsnProtocolGet(uperPsn);
    buffer[byte_i++] = protocol;

    /* Hop limit */
    buffer[byte_i++] = self->hopLimit;

    /* Source address */
    AtOsalMemCpy((void*)(buffer + byte_i), (void*)srcAddress, cAtPwPsnIpV6AddrNumByte);
    byte_i =  (uint8)(byte_i + 16);

    /* Destination address */
    AtOsalMemCpy((void*)(buffer + byte_i), (void*)self->destAddress, cAtPwPsnIpV6AddrNumByte);
    byte_i =  (uint8)(byte_i + 16);

    return byte_i;
    }

static uint8 HeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer)
    {
    return HeaderArrayBuild((AtPwIpV6Psn)self, pw, buffer, AtPwPsnHigherPsnGet(self));
    }

static AtPwPsn Copy(AtPwPsn self, AtPwPsn replicaPsn)
    {
    AtPwIpV6Psn thisPsn = (AtPwIpV6Psn)self;
    eAtRet ret = cAtOk;
    AtPwIpV6Psn replicaIpv6 = (AtPwIpV6Psn)replicaPsn;
    uint8 destIp[cAtPwPsnIpV6AddrNumByte];

    /* IP addresses */
    ret |= AtPwIpPsnDestAddressGet((AtPwIpPsn)thisPsn, destIp);
    ret |= AtPwIpPsnDestAddressSet((AtPwIpPsn)replicaIpv6, destIp);
    ret |= AtPwIpPsnSourceAddressGet((AtPwIpPsn)thisPsn, destIp);
    ret |= AtPwIpPsnSourceAddressSet((AtPwIpPsn)replicaIpv6, destIp);

    /* Other fields */
    ret |= AtPwIpV6PsnTrafficClassSet(replicaIpv6, AtPwIpV6PsnTrafficClassGet(thisPsn));
    ret |= AtPwIpV6PsnFlowLabelSet(replicaIpv6, AtPwIpV6PsnFlowLabelGet(thisPsn));
    ret |= AtPwIpV6PsnHopLimitSet(replicaIpv6, AtPwIpV6PsnHopLimitGet(thisPsn));

    if (ret != cAtOk)
        return NULL;

    return replicaPsn;
    }

static AtObject Clone(AtObject self)
    {
    AtPwPsn thisPsn = (AtPwPsn)self;

    /* Clone itself */
    AtPwPsn newPsn = (AtPwPsn)AtPwIpV6PsnNew();

    /* Clone lower PSN */
    if ((Copy(thisPsn, newPsn) == NULL) || (AtPwPsnLowerPsnClone(newPsn, thisPsn) == NULL))
        {
        AtObjectDelete((AtObject)newPsn);
        return NULL;
        }

    return (AtObject)newPsn;
    }

static const char * TypeString(AtPwPsn self)
    {
	AtUnused(self);
    return "IPv6";
    }

static eBool IsIdentical(AtPwPsn self, AtPwPsn another)
    {
    uint8 myIp[cAtPwPsnIpV6AddrNumByte];
    uint8 anotherIp[cAtPwPsnIpV6AddrNumByte];
    AtOsal osal = AtSharedDriverOsalGet();
    eAtRet ret = cAtOk;

    if (AtPwPsnTypeGet(another) != cAtPwPsnTypeIPv6)
        return cAtFalse;

    /* IP addresses */
    ret |= AtPwIpPsnDestAddressGet((AtPwIpPsn)self, myIp);
    ret |= AtPwIpPsnDestAddressGet((AtPwIpPsn)another, anotherIp);
    if ((ret != cAtOk) ||
        (mMethodsGet(osal)->MemCmp(osal, myIp, anotherIp, sizeof(uint8) * cAtPwPsnIpV6AddrNumByte)))
        return cAtFalse;

    ret |= AtPwIpPsnSourceAddressGet((AtPwIpPsn)self, myIp);
    ret |= AtPwIpPsnSourceAddressGet((AtPwIpPsn)another, anotherIp);
    if ((ret != cAtOk) ||
        (mMethodsGet(osal)->MemCmp(osal, myIp, anotherIp, sizeof(uint8) * cAtPwPsnIpV6AddrNumByte)))
        return cAtFalse;

    if ((AtPwIpV6PsnTrafficClassGet(mThis(self)) != AtPwIpV6PsnTrafficClassGet(mThis(another))) ||
        (AtPwIpV6PsnFlowLabelGet(mThis(self))    != AtPwIpV6PsnFlowLabelGet(mThis(another)))    ||
        (AtPwIpV6PsnHopLimitGet(mThis(self))     != AtPwIpV6PsnHopLimitGet(mThis(another))))
        return cAtFalse;

    return cAtTrue;
    }

static uint8 LengthInBytes(AtPwPsn self)
    {
    AtUnused(self);
    return 40;
    }

static void OverrideAtPwPsn(AtPwIpV6Psn self)
    {
    AtPwPsn psn = (AtPwPsn)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwPsnOverride, mMethodsGet(psn), sizeof(m_AtPwPsnOverride));

        mMethodOverride(m_AtPwPsnOverride, HeaderBuild);
        mMethodOverride(m_AtPwPsnOverride, TypeString);
        mMethodOverride(m_AtPwPsnOverride, Copy);
        mMethodOverride(m_AtPwPsnOverride, IsIdentical);
        mMethodOverride(m_AtPwPsnOverride, LengthInBytes);
        }

    mMethodsSet(psn, &m_AtPwPsnOverride);
    }

static eAtModulePwRet DestAddressSet(AtPwIpPsn self, const uint8 *destAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV6AddrNumByte; i++)
        (((AtPwIpV6Psn)self)->destAddress[i]) = destAddress[i];

    return cAtOk;
    }

static eAtModulePwRet DestAddressGet(AtPwIpPsn self, uint8 *destAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV6AddrNumByte; i++)
        destAddress[i] = (((AtPwIpV6Psn)self)->destAddress[i]);

    return cAtOk;
    }

static eAtModulePwRet SourceAddressSet(AtPwIpPsn self, const uint8 *sourceAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV6AddrNumByte; i++)
        (((AtPwIpV6Psn)self)->sourceAddress[i]) = sourceAddress[i];

    return cAtOk;
    }

static eAtModulePwRet SourceAddressGet(AtPwIpPsn self, uint8 *sourceAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV6AddrNumByte; i++)
        sourceAddress[i] = (((AtPwIpV6Psn)self)->sourceAddress[i]);

    return cAtOk;
    }

static const char *ToString(AtObject self)
    {
    static char buf[128];
    AtPwIpV6Psn ipv6Psn = (AtPwIpV6Psn)self;

    AtSprintf(buf, "%s: FlowLabel.%u, HopLimit.%u, TrafficClass.%u",
              AtPwPsnTypeString((AtPwPsn)self),
              AtPwIpV6PsnFlowLabelGet(ipv6Psn),
              AtPwIpV6PsnHopLimitGet(ipv6Psn),
              AtPwIpV6PsnTrafficClassGet(ipv6Psn));

    return buf;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwIpV6Psn object = (AtPwIpV6Psn)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt8Array(destAddress, cAtPwPsnIpV6AddrNumByte);
    mEncodeUInt8Array(sourceAddress, cAtPwPsnIpV6AddrNumByte);
    mEncodeUInt(trafficClass);
    mEncodeUInt(flowLabel);
    mEncodeUInt(hopLimit);
    }

static void OverrideAtPwIpPsn(AtPwIpV6Psn self)
    {
    AtPwIpPsn ipPsn = (AtPwIpPsn)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwIpPsnOverride, mMethodsGet(ipPsn), sizeof(m_AtPwIpPsnOverride));

        mMethodOverride(m_AtPwIpPsnOverride, SourceAddressSet);
        mMethodOverride(m_AtPwIpPsnOverride, SourceAddressGet);
        mMethodOverride(m_AtPwIpPsnOverride, DestAddressSet);
        mMethodOverride(m_AtPwIpPsnOverride, DestAddressGet);
        }

    mMethodsSet(ipPsn, &m_AtPwIpPsnOverride);
    }

static void OverrideAtObject(AtPwIpV6Psn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Clone);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPwIpV6Psn self)
    {
    OverrideAtObject(self);
    OverrideAtPwIpPsn(self);
    OverrideAtPwPsn(self);
    }

/* To initialize object */
static AtPwIpV6Psn AtPwIpV6PsnObjectInit(AtPwIpV6Psn self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwIpV6Psn));

    /* Super constructor should be called first */
    if (AtPwIpPsnObjectInit((AtPwIpPsn)self, cAtPwPsnTypeIPv6) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPwIpPsn
 * @{
 */

/**
 * Create new AtPwIpV6Psn object
 *
 * @return AtPwIpV6Psn object
 */
AtPwIpV6Psn AtPwIpV6PsnNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwIpV6Psn newPsn = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtPwIpV6Psn));
    if (newPsn == NULL)
        return NULL;

    /* Construct it */
    return AtPwIpV6PsnObjectInit(newPsn);
    }

/**
 * Set IPv6 traffic class
 *
 * @param self This PSN
 * @param trafficClass Traffic class
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpV6PsnTrafficClassSet(AtPwIpV6Psn self, uint8 trafficClass)
    {
    if (NULL == self)
        return cAtError;

    self->trafficClass = trafficClass;

    return cAtOk;
    }

/**
 * Get IPv6 traffic class
 *
 * @param self This PSN
 *
 * @return Traffic class
 */
uint8 AtPwIpV6PsnTrafficClassGet(AtPwIpV6Psn self)
    {
    if (self)
        return self->trafficClass;

    return 0;
    }

/**
 * Set IPv6 flow label
 *
 * @param self This PSN
 * @param flowLabel Flow label. This flowlabel field is 20 bits in the IPv6 header.
 *
 * @return AT return code.
 */
eAtModulePwRet AtPwIpV6PsnFlowLabelSet(AtPwIpV6Psn self, uint32 flowLabel)
    {
    if (NULL == self)
        return cAtErrorNullPointer;

    if (flowLabel > 0xFFFFF)
        return cAtErrorOutOfRangParm;

    self->flowLabel = flowLabel;

    return cAtOk;
    }

/**
 * Get Ipv6 flow label
 *
 * @param self This PSN
 *
 * @return Flow label
 */
uint32 AtPwIpV6PsnFlowLabelGet(AtPwIpV6Psn self)
    {
    if (self)
        return self->flowLabel;

    return 0;
    }

/**
 * Set IPv6 hop limit
 *
 * @param self This PSN
 * @param hopLimit Hop limit
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpV6PsnHopLimitSet(AtPwIpV6Psn self, uint8 hopLimit)
    {
    if (NULL == self)
        return cAtError;

    self->hopLimit = hopLimit;

    return cAtOk;
    }

/**
 * Get IPV6 hop limit
 *
 * @param self This PSN
 *
 * @return Hop limit
 */
uint8 AtPwIpV6PsnHopLimitGet(AtPwIpV6Psn self)
    {
    if (self)
        return self->hopLimit;

    return 0;
    }

/**
 * @}
 */

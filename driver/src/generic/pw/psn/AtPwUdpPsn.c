/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwUdpPsn.c
 *
 * Created Date: Nov 22, 2012
 *
 * Description : PW PSN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPwPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cIpAddressMaxLength 16

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwUdpPsn)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPwPsnMethods m_AtPwPsnOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 SourceIpAddress(AtPwPsn self, uint8 *ipAddress)
    {
    AtPwPsn higherPsn = AtPwPsnLowerPsnGet(self);
    eAtPwPsnType psnType = AtPwPsnTypeGet(higherPsn);

    /* Not support */
    if ((psnType != cAtPwPsnTypeIPv4) && (psnType != cAtPwPsnTypeIPv6))
        return 0;

    AtPwIpPsnSourceAddressGet((AtPwIpPsn)higherPsn, ipAddress);
    return (psnType == cAtPwPsnTypeIPv4) ? 4 : 16;
    }

static uint8 DestIpAddress(AtPwPsn self, uint8 *ipAddress)
    {
    AtPwPsn lowerPsn = AtPwPsnLowerPsnGet(self);
    eAtPwPsnType psnType = AtPwPsnTypeGet(lowerPsn);

    /* Not support */
    if ((psnType != cAtPwPsnTypeIPv4) && (psnType != cAtPwPsnTypeIPv6))
        return 0;

    AtPwIpPsnDestAddressGet((AtPwIpPsn)lowerPsn, ipAddress);
    return (psnType == cAtPwPsnTypeIPv4) ? 4 : 16;
    }

static uint16 CheckSumCalculate(AtPwPsn self, uint16 *length)
    {
    uint32 summary = 0;
    uint16 checkSum;
    uint8 word_i;
    uint8 ipAddressLen;
    uint8 sourceIpAddress[cIpAddressMaxLength];
    uint8 destIpAddress[cIpAddressMaxLength];
    AtPwUdpPsn udpPsn = (AtPwUdpPsn)self;
    *length = 0;

    ipAddressLen = DestIpAddress(self, destIpAddress);
    for (word_i = 0; word_i < (ipAddressLen / 2); word_i++)
        summary = summary + (uint32)((destIpAddress[(2 * word_i)] << 8) | destIpAddress[(2 * word_i) + 1]);

    ipAddressLen = SourceIpAddress(self, sourceIpAddress);
    for (word_i = 0; word_i < (ipAddressLen / 2); word_i++)
        summary = summary + (uint32)((sourceIpAddress[(2 * word_i)] << 8) | sourceIpAddress[(2 * word_i) + 1]);

    summary  = summary + 0x11; /* Protocol of IPv4 or Next header of IPv6 */
    summary  = summary + AtPwUdpPsnSourcePortGet(udpPsn) + AtPwUdpPsnDestPortGet(udpPsn);
    checkSum = summary & cBit15_0;
    *length  = (uint16)(summary >> 16);

    return checkSum;
    }

static uint8 HeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer)
    {
    uint8 byte_i;
    uint16 udpLen = 0;
    uint16 udpCheckSum;
    AtPwUdpPsn udpPsn = (AtPwUdpPsn)self;
	AtUnused(pw);

    byte_i      = 0;
    udpCheckSum = 0;

    buffer[byte_i++] = (uint8)(AtPwUdpPsnSourcePortGet(udpPsn) >> 8);
    buffer[byte_i++] = AtPwUdpPsnSourcePortGet(udpPsn) & cBit7_0;
    buffer[byte_i++] = (uint8)(AtPwUdpPsnDestPortGet(udpPsn) >> 8);
    buffer[byte_i++] = AtPwUdpPsnDestPortGet(udpPsn) & cBit7_0;

    /* HW will calculate checksum by itself in case of UDP.IPv4 */
    if (AtPwPsnTypeGet(AtPwPsnLowerPsnGet(self)) == cAtPwPsnTypeIPv4)
        udpCheckSum = 0;
    else
        udpCheckSum = CheckSumCalculate(self, &udpLen);

    buffer[byte_i++] = (uint8)(udpLen >> 8);
    buffer[byte_i++] = udpLen & cBit7_0;

    buffer[byte_i++] = (uint8)(udpCheckSum >> 8);
    buffer[byte_i++] = udpCheckSum & cBit7_0;

    return byte_i;
    }

static const char * TypeString(AtPwPsn self)
    {
	AtUnused(self);
    return "UDP";
    }

static const char *PortToString(uint16 port, char *buffer)
    {
    if (port == cAtPwPsnUdpUnusedPort)
        AtSprintf(buffer, "unused");
    else
        AtSprintf(buffer, "%d", port);

    return buffer;
    }

static const char *ToString(AtObject self)
    {
    static char buf[64];
    static char source[7];
    static char dest[7];
    AtPwUdpPsn udpPsn = (AtPwUdpPsn)self;

    PortToString(AtPwUdpPsnSourcePortGet(udpPsn), source);
    PortToString(AtPwUdpPsnDestPortGet(udpPsn), dest);

    AtSprintf(buf,
              "%s: source.%s, dest.%s",
              AtPwPsnTypeString((AtPwPsn)self),
              source,
              dest);

    return buf;
    }

static AtPwPsn Copy(AtPwPsn self, AtPwPsn replicaPsn)
    {
    eAtRet ret = cAtOk;
    AtPwUdpPsn replicaUdp = (AtPwUdpPsn)replicaPsn;
    AtPwUdpPsn thisPsn = (AtPwUdpPsn)self;

    ret |= AtPwUdpPsnSourcePortSet(replicaUdp, AtPwUdpPsnSourcePortGet(thisPsn));
    ret |= AtPwUdpPsnDestPortSet(replicaUdp, AtPwUdpPsnDestPortGet(thisPsn));
    ret |= AtPwUdpPsnExpectedPortSet(replicaUdp, AtPwUdpPsnExpectedPortGet(thisPsn));

    if (ret != cAtOk)
        return NULL;

    return replicaPsn;
    }

static eBool IsIdentical(AtPwPsn self, AtPwPsn another)
    {
    if (AtPwPsnTypeGet(another) != cAtPwPsnTypeUdp)
        return cAtFalse;

    if ((AtPwUdpPsnSourcePortGet(mThis(self))   != AtPwUdpPsnSourcePortGet(mThis(another))) ||
        (AtPwUdpPsnDestPortGet(mThis(self))     != AtPwUdpPsnDestPortGet(mThis(another)))   ||
        (AtPwUdpPsnExpectedPortGet(mThis(self)) != AtPwUdpPsnExpectedPortGet(mThis(another))))
        return cAtFalse;

    return cAtTrue;
    }

static AtObject Clone(AtObject self)
    {
    AtPwPsn thisPsn = (AtPwPsn)self;
    AtPwPsn newPsn  = (AtPwPsn)AtPwUdpPsnNew();

    /* Clone lower PSN */
    if ((Copy(thisPsn, newPsn) == NULL) || (AtPwPsnLowerPsnClone(newPsn, thisPsn) == NULL))
        {
        AtObjectDelete((AtObject)newPsn);
        return NULL;
        }

    return (AtObject)newPsn;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwUdpPsn object = (AtPwUdpPsn)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(sourcePort);
    mEncodeUInt(destPort);
    mEncodeUInt(expectedPort);
    }

static uint8 LengthInBytes(AtPwPsn self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 ExpectedLabelGet(AtPwPsn self)
    {
    return AtPwUdpPsnExpectedPortGet(mThis(self));
    }

static void OverrideAtPwPsn(AtPwUdpPsn self)
    {
    AtPwPsn psn = (AtPwPsn)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwPsnOverride, mMethodsGet(psn), sizeof(m_AtPwPsnOverride));

        mMethodOverride(m_AtPwPsnOverride, HeaderBuild);
        mMethodOverride(m_AtPwPsnOverride, TypeString);
        mMethodOverride(m_AtPwPsnOverride, Copy);
        mMethodOverride(m_AtPwPsnOverride, IsIdentical);
        mMethodOverride(m_AtPwPsnOverride, LengthInBytes);
        mMethodOverride(m_AtPwPsnOverride, ExpectedLabelGet);
        }

    mMethodsSet(psn, &m_AtPwPsnOverride);
    }

static void OverrideAtObject(AtPwUdpPsn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Clone);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPwUdpPsn self)
    {
    OverrideAtObject(self);
    OverrideAtPwPsn(self);
    }

/* To initialize object */
static AtPwUdpPsn AtPwUdpPsnObjectInit(AtPwUdpPsn self)
    {
    static const uint16 cUnusedPort = 0x85E;

    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwUdpPsn));

    /* Super constructor should be called first */
    if (AtPwPsnObjectInit((AtPwPsn)self, cAtPwPsnTypeUdp) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->sourcePort = cUnusedPort;
    self->destPort   = cUnusedPort;

    return self;
    }

uint32 AtPwUdpPsnCheckSumCalculate(AtPwUdpPsn self)
    {
    uint16 length;
    uint16 sum;

    if (self == NULL)
        return 0;

    sum = CheckSumCalculate((AtPwPsn)self, &length);
    return (uint32)(length << 16 | sum);
    }

/**
 * @addtogroup AtPwUdpPsn
 * @{
 */
/**
 * Create new AtPwUdpPsn object
 *
 * @return AtPwUdpPsn object
 */
AtPwUdpPsn AtPwUdpPsnNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwUdpPsn newPsn = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtPwUdpPsn));
    if (newPsn == NULL)
        return NULL;

    /* Construct it */
    return AtPwUdpPsnObjectInit(newPsn);
    }

/**
 * Set UDP source port
 *
 * @param self This PSN
 * @param sourcePort Source port
 *
 * @return AT return code
 */
eAtModulePwRet AtPwUdpPsnSourcePortSet(AtPwUdpPsn self, uint16 sourcePort)
    {
    if (NULL == self)
        return cAtError;

    self->sourcePort = sourcePort;
    return cAtOk;
    }

/**
 * Get UDP source port
 *
 * @param self This PSN
 *
 * @return Source port
 */
uint16 AtPwUdpPsnSourcePortGet(AtPwUdpPsn self)
    {
    if (NULL == self)
        return 0;

    return self->sourcePort;
    }

/**
 * Set UDP destination port
 *
 * @param self This PSN
 * @param destPort Destination port
 *
 * @return AT return code
 */
eAtModulePwRet AtPwUdpPsnDestPortSet(AtPwUdpPsn self, uint16 destPort)
    {
    if (NULL == self)
        return cAtError;

    self->destPort = destPort;
    return cAtOk;
    }

/**
 * Get UDP destination port
 *
 * @param self This PSN
 *
 * @return AT return code
 */
uint16 AtPwUdpPsnDestPortGet(AtPwUdpPsn self)
    {
    if (self == NULL)
        return 0;

    return self->destPort;
    }

/**
 * Set expected port at receive direction
 *
 * @param self This PSN
 * @param port Expected port
 *
 * @return AT return code
 */
eAtModulePwRet AtPwUdpPsnExpectedPortSet(AtPwUdpPsn self, uint16 port)
    {
    if (self == NULL)
        return cAtError;

    self->expectedPort = port;

    return cAtOk;
    }

/**
 * Get expected port at receive direction
 *
 * @param self This PSN
 *
 * @return Expected port or unused port if PSN is NULL
 */
uint16 AtPwUdpPsnExpectedPortGet(AtPwUdpPsn self)
    {
    if (self == NULL)
        return cAtPwPsnUdpUnusedPort;

    return self->expectedPort;
    }

/** @} */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwIpV4Psn.c
 *
 * Created Date: Nov 22, 2012
 *
 * Description : PW IPv4 PSN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "atclib.h"
#include "AtPwPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtIpv4VerRstVal               4
#define cThaIpv4IhlRstVal              5
#define cThaIpProtocolUdp              0x11
#define cThaIpProtocolMpls             0x89
#define cThaNumByteIpv4Hdr             20

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwIpV4Psn)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPwPsnMethods   m_AtPwPsnOverride;
static tAtPwIpPsnMethods m_AtPwIpPsnOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static uint8 Ipv4HeaderArrayBuild(AtPwIpV4Psn self, AtPw pw, uint8 *buffer, AtPwPsn uperPsn, uint16 ipv4CheckSum, uint16 totalLength);

/*--------------------------- Implementation ---------------------------------*/
static AtPwPsn Copy(AtPwPsn self, AtPwPsn replicaPsn)
    {
    AtPwIpV4Psn thisPsn = (AtPwIpV4Psn)self;
    uint8 destIp[cAtPwPsnIpV4AddrNumByte];
    eAtRet ret = cAtOk;
    AtPwIpV4Psn replicaIpv4 = (AtPwIpV4Psn)replicaPsn;

    /* IP addresses */
    ret |= AtPwIpPsnDestAddressGet((AtPwIpPsn)thisPsn, destIp);
    ret |= AtPwIpPsnDestAddressSet((AtPwIpPsn)replicaIpv4, destIp);
    ret |= AtPwIpPsnSourceAddressGet((AtPwIpPsn)thisPsn, destIp);
    ret |= AtPwIpPsnSourceAddressSet((AtPwIpPsn)replicaIpv4, destIp);

    /* Other fields */
    ret |= AtPwIpV4PsnTypeOfServiceSet(replicaIpv4, AtPwIpV4PsnTypeOfServiceGet(thisPsn));
    ret |= AtPwIpV4PsnTimeToLiveSet(replicaIpv4, AtPwIpV4PsnTimeToLiveGet(thisPsn));

    if (ret != cAtOk)
        return NULL;

    return replicaPsn;
    }

static AtObject Clone(AtObject self)
    {
    AtPwPsn thisPsn = (AtPwPsn)self;

    /* Clone itself */
    AtPwPsn newPsn = (AtPwPsn)AtPwIpV4PsnNew();

    /* Clone lower PSN */
    if ((Copy(thisPsn, newPsn) == NULL) || (AtPwPsnLowerPsnClone(newPsn, thisPsn) == NULL))
        {
        AtObjectDelete((AtObject)newPsn);
        return NULL;
        }

    return (AtObject)newPsn;
    }

/*
 * This function is used to  calculate the checksum for uint8 array
 * with previous length (RFC 1071 (Computing the Internet Checksum))
 *
 * @param pData Array
 * @param len Length of uint8 array.
 * @param isInverted The check sum value should be inverted ager.
 * @return
 */
static uint32 EthCheckSumCalculate(const uint8 *pData, uint32 len, eBool isInverted)
    {
    uint32       sum = 0;
    uint32       i = 0;
    uint32       checksum = 0;

    /* This is used to validate the checksum
    0001-f203-f4f5-f6f7, checksum = ddf2
    */
    sum = 0;
    i   = 0;
    if ((len >> 1) > 0)
        {
        for (i = 0; i < (len >> 1); i++)
            sum = sum + (uint32)(pData[2*i]<<8) + (uint32)pData[2*i + 1];
        }

    if ((len & cBit0) == 1)
        sum = sum + pData[len -1];

    if (isInverted)
        {
        sum = (sum >> 16) + (sum & cBit15_0);
        checksum =(~sum) ;
        }
    else
        checksum = sum; /* 15_0: Write to IP header checksum, 31_16: Write to Total Length */

    return checksum;
    }

static const char *ToString(AtObject self)
    {
    static char buf[32];
    AtPwIpV4Psn ipv4Psn = (AtPwIpV4Psn)self;

    AtSprintf(buf,
              "%s: TTL.%d, TOS.%d",
              AtPwPsnTypeString((AtPwPsn)self),
              AtPwIpV4PsnTimeToLiveGet(ipv4Psn),
              AtPwIpV4PsnTypeOfServiceGet(ipv4Psn));

    return buf;
    }

/*
 * Calculate IPv4 Check sum
 *
 * @param pIpv4Hdr
 * @param pw
 * @param uperPsn
 * @return
 */
static void Ipv4CheckSumCalculate(AtPwIpV4Psn self, AtPw pw, AtPwPsn uperPsn, uint16 *checkSum, uint16 *totalLength)
    {
    uint32       dSum;
    uint8        pHdr[cThaNumByteIpv4Hdr];

    Ipv4HeaderArrayBuild(self, pw, pHdr, uperPsn, 0, 0);
    dSum = EthCheckSumCalculate(pHdr, cThaNumByteIpv4Hdr, cAtFalse);
    *checkSum = dSum & cBit15_0;
    *totalLength = (uint16)(dSum >> 16);
    }

static uint8 UperPsnProtocolGet(AtPwPsn psn)
    {
    switch (AtPwPsnTypeGet(psn))
        {
        case cAtPwPsnTypeUdp : return cThaIpProtocolUdp;
        case cAtPwPsnTypeMpls: return cThaIpProtocolMpls;

        case cAtPwPsnTypeUnknown:
        case cAtPwPsnTypeMef:
        case cAtPwPsnTypeIPv4:
        case cAtPwPsnTypeIPv6:
        default:
            return 0;
        }
    }

static uint8 Ipv4HeaderArrayBuild(AtPwIpV4Psn self, AtPw pw, uint8 *buffer, AtPwPsn uperPsn, uint16 ipv4CheckSum, uint16 totalLength)
    {
    uint8 byte_i = 0, protocol;
    uint8 srcAddress[cAtPwPsnIpV4AddrNumByte];
    uint8 destAddress[cAtPwPsnIpV4AddrNumByte];
    AtUnused(pw);

    if (AtPwIpPsnSourceAddressGet((AtPwIpPsn)self, srcAddress) != cAtOk)
        return 0;

    if (AtPwIpPsnDestAddressGet((AtPwIpPsn)self, destAddress) != cAtOk)
        return 0;

    /* IPv4 structure
    |    1  |    2  |   3   |   4   |   5   |   6   |    7  |    8  |
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |            IPVER=4            |          IHL= 5               | 0
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                             IP TOS                            | 1
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 2
    +                          Total Length                         +
    |                                                               | 3
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 4
    +                          Identification                       +
    |                                                               | 5
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |       Flags           |                                       | 6
    +-------+-------+-------+        Fragment Offset                +
    |                                                               | 7
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                    Time To Live (8 bits)                      | 8
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                      Protocol (8 bits)                        | 9
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 10
    +                      IP header checksum                       +
    |                            (2 bytes)                          | 11
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 12
    +                      Source IP Address                        +
    |                           (4 bytes)                           | 13
    |                                                               | 14
    |                                                               | 15
    +-------+-------+-------+-------+-------+-------+-------+-------+
    |                                                               | 16
    +                    Destination IP Address                     +
    |                            (4 bytes)                          | 17
    |                                                               | 18
    |                                                               | 19
    +-------+-------+-------+-------+-------+-------+-------+-------+
    */
    buffer[byte_i++] = (cAtIpv4VerRstVal << 4 ) | cThaIpv4IhlRstVal;
    buffer[byte_i++] = self->typeOfService;

    /* Total length will be updated by HW */
    buffer[byte_i++] = (uint8)(totalLength >> 8);
    buffer[byte_i++] = totalLength & cBit7_0;

    /*------------------------------------------------------------------------*/

    buffer[byte_i++] = (uint8)(self->fragmentId >> 8);
    buffer[byte_i++] = self->fragmentId & cBit7_0;

    /*  +-------+-------+-------+-------+-------+-------+-------+-------+
        |       Flags           |                                       | 6
        +-------+-------+-------+        Fragment Offset                +
        |                                                               | 7
        +-------+-------+-------+-------+-------+-------+-------+-------+ */
    /*  Flags Structure
        0   1   2
        +---+---+---+  Bit 0: reserved, must be zero
        |   | D | M |  Bit 1: (DF) 0 = May Fragment,  1 = Don't Fragment.
        | 0 | F | F |  Bit 2: (MF) 0 = Last Fragment, 1 = More Fragments.
        +---+---+---+
    */
    buffer[byte_i++] = (uint8)((uint32)(self->controlFlag << 5) | (uint32)((self->fragmentOffset >> 8) & cBit4_0));
    buffer[byte_i++] = self->fragmentOffset & cBit7_0;
    buffer[byte_i++] = self->timeToLive;

    protocol = UperPsnProtocolGet(uperPsn);
    buffer[byte_i++] = protocol;
    buffer[byte_i++] = (uint8)(ipv4CheckSum >> 8);
    buffer[byte_i++] = ipv4CheckSum &  cBit7_0;

    /* Source address */
    AtOsalMemCpy((void*)(buffer + byte_i), (void*)srcAddress, cAtPwPsnIpV4AddrNumByte);
    byte_i =  (uint8)(byte_i + 4);

    /* Destination address */
    AtOsalMemCpy((void*)(buffer + byte_i), (void*)destAddress, cAtPwPsnIpV4AddrNumByte);
    byte_i =  (uint8)(byte_i + 4);

    return byte_i;
    }

static uint8 HeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer)
    {
    uint16 checkSum, totalLength;
    Ipv4CheckSumCalculate((AtPwIpV4Psn)self, pw, AtPwPsnHigherPsnGet(self), &checkSum, &totalLength);
    return Ipv4HeaderArrayBuild((AtPwIpV4Psn)self, pw, buffer, AtPwPsnHigherPsnGet(self), checkSum, totalLength);
    }

static const char * TypeString(AtPwPsn self)
    {
	AtUnused(self);
    return "IPv4";
    }

static eBool IsIdentical(AtPwPsn self, AtPwPsn another)
    {
    uint8 myIp[cAtPwPsnIpV4AddrNumByte];
    uint8 anotherIp[cAtPwPsnIpV4AddrNumByte];
    AtOsal osal = AtSharedDriverOsalGet();
    eAtRet ret = cAtOk;

    if (AtPwPsnTypeGet(another) != cAtPwPsnTypeIPv4)
        return cAtFalse;

    /* IP addresses */
    ret |= AtPwIpPsnDestAddressGet((AtPwIpPsn)self, myIp);
    ret |= AtPwIpPsnDestAddressGet((AtPwIpPsn)another, anotherIp);
    if ((ret != cAtOk) ||
        (mMethodsGet(osal)->MemCmp(osal, myIp, anotherIp, sizeof(uint8) * cAtPwPsnIpV4AddrNumByte)))
        return cAtFalse;

    ret |= AtPwIpPsnSourceAddressGet((AtPwIpPsn)self, myIp);
    ret |= AtPwIpPsnSourceAddressGet((AtPwIpPsn)another, anotherIp);
    if ((ret != cAtOk) ||
        (mMethodsGet(osal)->MemCmp(osal, myIp, anotherIp, sizeof(uint8) * cAtPwPsnIpV4AddrNumByte)))
        return cAtFalse;

    /* Other fields */
    if ((AtPwIpV4PsnTypeOfServiceGet(mThis(self)) != AtPwIpV4PsnTypeOfServiceGet(mThis(another))) ||
        (AtPwIpV4PsnTimeToLiveGet(mThis(self))    != AtPwIpV4PsnTimeToLiveGet(mThis(another))))
        return cAtFalse;

    return cAtTrue;
    }

static uint8 LengthInBytes(AtPwPsn self)
    {
    AtUnused(self);
    return 20;
    }

static void OverrideAtPwPsn(AtPwIpV4Psn self)
    {
    AtPwPsn psn = (AtPwPsn)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwPsnOverride, mMethodsGet(psn), sizeof(m_AtPwPsnOverride));

        mMethodOverride(m_AtPwPsnOverride, HeaderBuild);
        mMethodOverride(m_AtPwPsnOverride, TypeString);
        mMethodOverride(m_AtPwPsnOverride, Copy);
        mMethodOverride(m_AtPwPsnOverride, IsIdentical);
        mMethodOverride(m_AtPwPsnOverride, LengthInBytes);
        }

    mMethodsSet(psn, &m_AtPwPsnOverride);
    }

static eAtModulePwRet DestAddressSet(AtPwIpPsn self, const uint8 *destAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV4AddrNumByte; i++)
        ((AtPwIpV4Psn)self)->destAddress[i] = destAddress[i];

    return cAtOk;
    }

static eAtModulePwRet DestAddressGet(AtPwIpPsn self, uint8 *destAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV4AddrNumByte; i++)
        destAddress[i] = ((AtPwIpV4Psn)self)->destAddress[i];

    return cAtOk;
    }

static eAtModulePwRet SourceAddressSet(AtPwIpPsn self, const uint8 *sourceAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV4AddrNumByte; i++)
        ((AtPwIpV4Psn)self)->sourceAddress[i] = sourceAddress[i];

    return cAtOk;
    }

static eAtModulePwRet SourceAddressGet(AtPwIpPsn self, uint8 *sourceAddress)
    {
    uint8 i;
    for (i = 0; i < cAtPwPsnIpV4AddrNumByte; i++)
        sourceAddress[i] = ((AtPwIpV4Psn)self)->sourceAddress[i];

    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwIpV4Psn object = (AtPwIpV4Psn)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(fragmentId);
    mEncodeUInt(controlFlag);
    mEncodeUInt(fragmentOffset);
    mEncodeUInt(typeOfService);
    mEncodeUInt(timeToLive);
    mEncodeUInt8Array(destAddress, cAtPwPsnIpV4AddrNumByte);
    mEncodeUInt8Array(sourceAddress, cAtPwPsnIpV4AddrNumByte);
    }

static void OverrideAtPwIpPsn(AtPwIpV4Psn self)
    {
    AtPwIpPsn ipPsn = (AtPwIpPsn)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwIpPsnOverride, mMethodsGet(ipPsn), sizeof(m_AtPwIpPsnOverride));

        mMethodOverride(m_AtPwIpPsnOverride, SourceAddressSet);
        mMethodOverride(m_AtPwIpPsnOverride, SourceAddressGet);
        mMethodOverride(m_AtPwIpPsnOverride, DestAddressSet);
        mMethodOverride(m_AtPwIpPsnOverride, DestAddressGet);
        }

    mMethodsSet(ipPsn, &m_AtPwIpPsnOverride);
    }

static void OverrideAtObject(AtPwIpV4Psn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Clone);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPwIpV4Psn self)
    {
    OverrideAtObject(self);
    OverrideAtPwIpPsn(self);
    OverrideAtPwPsn(self);
    }

/* To initialize object */
static AtPwIpV4Psn AtPwIpV4PsnObjectInit(AtPwIpV4Psn self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwIpV4Psn));

    /* Super constructor should be called first */
    if (AtPwIpPsnObjectInit((AtPwIpPsn)self, cAtPwPsnTypeIPv4) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private */
    self->timeToLive = 0xFF;

    return self;
    }

uint32 AtPwIpV4PsnCheckSumCalculate(AtPwIpV4Psn self)
    {
    uint16 checkSum, totalLength;
    if (self == NULL)
        return 0;

    Ipv4CheckSumCalculate(self, NULL, AtPwPsnHigherPsnGet((AtPwPsn)self), &checkSum, &totalLength);
    return (uint32)(totalLength << 16 | checkSum);
    }

/**
 * @addtogroup AtPwIpPsn
 * @{
 */

/**
 * Create new AtPwIpV4Psn object
 *
 * @return AtPwIpV4Psn object
 */
AtPwIpV4Psn AtPwIpV4PsnNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwIpV4Psn newPsn = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtPwIpV4Psn));
    if (newPsn == NULL)
        return NULL;

    /* Construct it */
    return AtPwIpV4PsnObjectInit(newPsn);
    }

/**
 * Set IPv4 Type of Service
 *
 * @param self This PSN
 * @param typeOfService Type of Service
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpV4PsnTypeOfServiceSet(AtPwIpV4Psn self, uint8 typeOfService)
    {
    if (self == NULL)
        return cAtError;

    self->typeOfService = typeOfService;
    return cAtOk;
    }

/**
 * Get IPv4 Type of Service
 *
 * @param self This PSN
 *
 * @return Type of Service
 */
uint8 AtPwIpV4PsnTypeOfServiceGet(AtPwIpV4Psn self)
    {
    if (self)
        return self->typeOfService;

    return 0;
    }

/**
 * Set IPv4 Time-to-Live
 *
 * @param self This PSN
 * @param timeToLive Time-to-Live
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpV4PsnTimeToLiveSet(AtPwIpV4Psn self, uint8 timeToLive)
    {
    if (NULL == self)
        return cAtError;

    self->timeToLive = timeToLive;
    return cAtOk;
    }

/**
 * Get IPv4 Time-to-Live
 *
 * @param self This PSN
 *
 * @return Time-to-Live
 */
uint8 AtPwIpV4PsnTimeToLiveGet(AtPwIpV4Psn self)
    {
    if (self)
        return self->timeToLive;

    return 0;
    }

/**
 * Set Flags
 *
 * @param self This PSN
 * @param flags Flags
 *
 * @return AT return code
 */
eAtModulePwRet AtPwIpV4PsnFlagsSet(AtPwIpV4Psn self, uint8 flags)
    {
    if (self == NULL)
        return cAtError;

    self->controlFlag = flags;
    return cAtOk;
    }

/**
 * Get Flags
 *
 * @param self This PSN
 *
 * @return  Flags
 */
uint8 AtPwIpV4PsnFlagsGet(AtPwIpV4Psn self)
    {
    if (self)
        return self->controlFlag;

    return 0;
    }

/**
 * @}
 */

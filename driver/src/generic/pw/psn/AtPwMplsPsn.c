/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwMplsPsn.c
 *
 * Created Date: Nov 22, 2012
 *
 * Description : MPLS PSN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPsnInternal.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPwMplsPsn)self)

#define mEncodeInnerLabel(fieldName) AtCoderEncodeUInt(encoder, object->innerLabel.fieldName, "innerLabel."#fieldName)

#define mEncodeOutterLabel(fieldName_)                                         \
        do                                                                     \
            {                                                                  \
            AtSnprintf(key, sizeof(key) - 1, "outerLabels[%u]."#fieldName_, label_i); \
            AtCoderEncodeUInt(encoder, labelWrap->label.fieldName_, key);        \
            }while(0)                                                          \

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwMplsLabelWrapper * AtPwMplsLabelWrapper;

typedef struct tAtPwMplsLabelWrapper
    {
    tAtObject super;

    tAtPwMplsLabel label;
    }tAtPwMplsLabelWrapper;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPwPsnMethods  m_AtPwPsnOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* MPLS header structure

|    1  |    2  |   3   |   4   |   5   |   6   |    7  |    8  |
+-------+-------+-------+-------+-------+-------+-------+-------+
|            Outer Label 1 (20 bits)                            | 0
+                                                               +
|                                                               | 1
+                               +-------+-------+-------+-------+
|                               |         EXP           |   S   | 2
+-------+-------+-------+-------+-------+-------+-------+-------+
|                             TTL                               | 3
+-------+-------+-------+-------+-------+-------+-------+-------+
|            Outer Label  2(20 bits)                            | 4
+                                                               +
|                                                               | 5
+                               +-------+-------+-------+-------+
|                               |         EXP           |   S   | 6
+-------+-------+-------+-------+-------+-------+-------+-------+
|                             TTL                               | 7
+-------+-------+-------+-------+-------+-------+-------+-------+
|            Inner Label (20 bits)                              | 8
+                                                               +
|                                                               | 9
+                               +-------+-------+-------+-------+
|                               |         EXP           |   S   | 10
+-------+-------+-------+-------+-------+-------+-------+-------+
|                             TTL                               | 11
+-------+-------+-------+-------+-------+-------+-------+-------+
*/
static uint8 HeaderBuild(AtPwPsn self, AtPw pw, uint8 *buffer)
    {
    tAtPwMplsLabel label;
    uint32 byte_i = 0;
    uint8 stackingBit;
    uint8 numOuterLabels, headerLength;
    AtPwMplsPsn mpls = (AtPwMplsPsn)self;

	AtUnused(pw);

    /* Put outer labels */
    numOuterLabels = AtPwMplsPsnNumberOfOuterLabelsGet((AtPwMplsPsn)self);
    if (numOuterLabels > 0)
        {
        AtIterator labelIterator = AtPwMplsPsnOuterLabelIteratorCreate(mpls);
        AtPwMplsLabelWrapper labelWrapper;
        tAtPwMplsLabel *pLabel;
        while ((labelWrapper = (AtPwMplsLabelWrapper)AtIteratorNext(labelIterator)) != NULL)
            {
            pLabel = &(labelWrapper->label);
            buffer[byte_i++] = (pLabel->label >> 12) & cBit7_0;
            buffer[byte_i++] = (pLabel->label >> 4) & cBit7_0;

            /* Stacking bit is 0 for Outer label */
            buffer[byte_i++] = (uint8)(((pLabel->label & cBit3_0) << 4) |
                                       ((pLabel->experimental & cBit2_0) << 1));
            buffer[byte_i++] = pLabel->timeToLive;
            }
        AtObjectDelete((AtObject)labelIterator);
        }

    /* Put inner label */
    AtPwMplsPsnInnerLabelGet(mpls, &label);
    buffer[byte_i++] = (label.label >> 12) & cBit7_0;
    buffer[byte_i++] = (label.label >> 4) & cBit7_0;

    /* Stacking bit is 1 for Inner label */
    stackingBit = 1;
    buffer[byte_i++] = (uint8)(((label.label & cBit3_0) << 4) | ((label.experimental & cBit2_0) << 1) | stackingBit);
    buffer[byte_i++] = label.timeToLive;
    headerLength = (uint8)(4 + (numOuterLabels * 4));

    return headerLength;
    }

static void AllLabelsDelete(AtPwMplsPsn self)
    {
    AtIterator labelIterator;
    AtPwMplsLabelWrapper labelWrapper;

    /* Delete outer labels */
    labelIterator = AtPwMplsPsnOuterLabelIteratorCreate(self);
    while ((labelWrapper = (AtPwMplsLabelWrapper)AtIteratorNext(labelIterator)) != NULL)
        AtObjectDelete((AtObject)labelWrapper);
    AtObjectDelete((AtObject)labelIterator);

    /* And the list that hold these labels */
    AtObjectDelete((AtObject)(self->outerLabels));
    self->outerLabels = NULL;
    }

static void Delete(AtObject self)
    {
    AllLabelsDelete((AtPwMplsPsn)self);

    /* And fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static const char * TypeString(AtPwPsn self)
    {
	AtUnused(self);
    return "MPLS";
    }

static char *Label2String(const tAtPwMplsLabel *label)
    {
    static char labelString[32];

    AtSprintf(labelString, "%u.%u.%u", label->label, label->experimental, label->timeToLive);

    return labelString;
    }

static const char *ToString(AtObject self)
    {
    AtPwMplsPsn mpls = (AtPwMplsPsn)self;
    static char allLabelString[128];
    char tempBuf[32];
    uint32 bufferSize = sizeof(allLabelString);
    uint32 remained = bufferSize;
    tAtPwMplsLabel label;
    AtPwMplsLabelWrapper labelWrapper;

    /* Zero string first */
    allLabelString[0] = '\0';

    /* Inner label */
    if (AtPwMplsPsnInnerLabelGet(mpls, &label) != cAtOk)
        return NULL;
    AtSnprintf(allLabelString, remained, "%s(label.exp.ttl): Inner.%s", AtPwPsnTypeString((AtPwPsn)self), Label2String(&label));
    remained = bufferSize - AtStrlen(allLabelString);

    /* Outer labels */
    AtStrcat(allLabelString, ", Outters.");
    if (AtPwMplsPsnNumberOfOuterLabelsGet(mpls) == 0)
        {
        AtStrncat(allLabelString, "none", remained);
        remained = bufferSize - AtStrlen(allLabelString);
        }
    else
        {
        AtIterator labelIterator = AtPwMplsPsnOuterLabelIteratorCreate(mpls);
        while ((labelWrapper = (AtPwMplsLabelWrapper)AtIteratorNext(labelIterator)) != NULL)
            {
            char *labelString = Label2String(&(labelWrapper->label));
            AtStrncat(allLabelString, labelString, remained);
            remained = bufferSize - AtStrlen(allLabelString);
            AtStrncat(allLabelString, ",", remained);
            remained = bufferSize - AtStrlen(allLabelString);
            }

        /* Remove the last ', */
        allLabelString[AtStrlen(allLabelString) - 1] = '\0';
        remained = bufferSize - AtStrlen(allLabelString);

        AtObjectDelete((AtObject)labelIterator);
        }

    /* Expected */
    AtStrncat(allLabelString, ". Expected label: ", remained);
    remained = bufferSize - AtStrlen(allLabelString);
    AtSnprintf(tempBuf, sizeof(tempBuf), "%d", AtPwMplsPsnExpectedLabelGet((AtPwMplsPsn)self));
    AtStrncat(allLabelString, tempBuf, remained);

    return allLabelString;
    }

static AtPwPsn Copy(AtPwPsn self, AtPwPsn replicaPsn)
    {
    eAtRet ret = cAtOk;
    AtIterator outLabelsIterator;
    AtPwMplsPsn thisPsn = (AtPwMplsPsn)self;
    tAtPwMplsLabel label;
    AtPwMplsLabelWrapper labelWrapper;
    AtPwMplsPsn replicaMpls = (AtPwMplsPsn)replicaPsn;

    /* Inner label */
    ret |= AtPwMplsPsnInnerLabelGet(thisPsn, &label);
    ret |= AtPwMplsPsnInnerLabelSet(replicaMpls, &label);

    /* Remove current outer labels of replica PSN */
    while (AtListLengthGet(replicaMpls->outerLabels) > 0)
        AtObjectDelete(AtListObjectRemoveAtIndex(replicaMpls->outerLabels, 0));

    /* Outer labels */
    outLabelsIterator = AtPwMplsPsnOuterLabelIteratorCreate(thisPsn);
    while ((labelWrapper = (AtPwMplsLabelWrapper)AtIteratorNext(outLabelsIterator)) != NULL)
        ret |= AtPwMplsPsnOuterLabelAdd(replicaMpls, &(labelWrapper->label));
    AtObjectDelete((AtObject)outLabelsIterator);

    /* Expected label */
    ret |= AtPwMplsPsnExpectedLabelSet(replicaMpls, AtPwMplsPsnExpectedLabelGet(thisPsn));

    if (ret != cAtOk)
        return NULL;

    return replicaPsn;
    }

static AtObject Clone(AtObject self)
    {
    AtPwPsn thisPsn = (AtPwPsn)self;

    /* Clone itself */
    AtPwPsn newPsn = (AtPwPsn)AtPwMplsPsnNew();
    if (newPsn == NULL)
        return NULL;

    /* Clone lower PSN */
    if ((Copy(thisPsn, newPsn) == NULL) || (AtPwPsnLowerPsnClone(newPsn, thisPsn) == NULL))
        {
        AtObjectDelete((AtObject)newPsn);
        return NULL;
        }

    return (AtObject)newPsn;
    }

static eBool LabelFieldIsValid(uint32 labelValue)
    {
    return (labelValue > cBit19_0) ? cAtFalse : cAtTrue;
    }

static eBool LabelIsValid(const tAtPwMplsLabel *label)
    {
    if ((label->experimental >= 8) || (!LabelFieldIsValid(label->label)))
        return cAtFalse;

    return cAtTrue;
    }

static eBool IsValid(AtPwPsn self)
    {
    AtPwMplsPsn mpls = (AtPwMplsPsn)self;
    tAtPwMplsLabel innerLabel;
    eAtRet ret;

    /* Need the inner label, so only check it */
    ret = AtPwMplsPsnInnerLabelGet(mpls, &innerLabel);
    if (ret != cAtOk)
        return cAtFalse;

    /* Check inner label */
    return LabelIsValid(&innerLabel);
    }

static eBool LabelIsIdentical(tAtPwMplsLabel *label1, tAtPwMplsLabel *label2)
    {
    if ((label1->experimental != label2->experimental) ||
        (label1->label        != label2->label)        ||
        (label1->timeToLive   != label2->timeToLive))
        return cAtFalse;

    return cAtTrue;
    }

static eBool OuterLabelsAreIdentical(AtPwMplsPsn self, AtPwMplsPsn another)
    {
    uint8 label_i;

    if (AtListLengthGet(self->outerLabels) != AtListLengthGet(another->outerLabels))
        return cAtFalse;

    for (label_i = 0; label_i < AtListLengthGet(self->outerLabels); label_i++)
        {
        AtPwMplsLabelWrapper label1, label2;
        label1 = (AtPwMplsLabelWrapper)AtListObjectGet(self->outerLabels, label_i);
        label2 = (AtPwMplsLabelWrapper)AtListObjectGet(another->outerLabels, label_i);

        if (!LabelIsIdentical(&(label1->label), &(label2->label)))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool IsIdentical(AtPwPsn self, AtPwPsn another)
    {
    eAtRet ret = cAtOk;
    tAtPwMplsLabel label1, label2;

    if (AtPwPsnTypeGet(another) != cAtPwPsnTypeMpls)
        return cAtFalse;

    if (AtPwMplsPsnExpectedLabelGet(mThis(self)) != AtPwMplsPsnExpectedLabelGet(mThis(another)))
        return cAtFalse;

    /* Inner label */
    ret |= AtPwMplsPsnInnerLabelGet(mThis(self), &label1);
    ret |= AtPwMplsPsnInnerLabelGet(mThis(another), &label2);
    if (!LabelIsIdentical(&label1, &label2))
        return cAtFalse;

    return OuterLabelsAreIdentical(mThis(self), mThis(another));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwMplsPsn object = (AtPwMplsPsn)self;
    uint32 label_i;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeInnerLabel(label);
    mEncodeInnerLabel(experimental);
    mEncodeInnerLabel(timeToLive);

    for (label_i = 0; label_i < AtListLengthGet(object->outerLabels); label_i++)
        {
        AtPwMplsLabelWrapper labelWrap = (AtPwMplsLabelWrapper)AtListObjectGet(object->outerLabels, label_i);
        char key[32];

        mEncodeOutterLabel(label);
        mEncodeOutterLabel(experimental);
        mEncodeOutterLabel(timeToLive);
        }

    mEncodeUInt(expectedLabel);
    }

static uint8 LengthInBytes(AtPwPsn self)
    {
    return (uint8)((AtPwMplsPsnNumberOfOuterLabelsGet((AtPwMplsPsn)self) + 1) * 4);
    }

static uint32 ExpectedLabelGet(AtPwPsn self)
    {
    return AtPwMplsPsnExpectedLabelGet(mThis(self));
    }

static void OverrideAtObject(AtPwMplsPsn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Clone);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPwPsn(AtPwMplsPsn self)
    {
    AtPwPsn psn = (AtPwPsn)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwPsnOverride, mMethodsGet(psn), sizeof(m_AtPwPsnOverride));

        mMethodOverride(m_AtPwPsnOverride, HeaderBuild);
        mMethodOverride(m_AtPwPsnOverride, TypeString);
        mMethodOverride(m_AtPwPsnOverride, IsValid);
        mMethodOverride(m_AtPwPsnOverride, Copy);
        mMethodOverride(m_AtPwPsnOverride, IsIdentical);
        mMethodOverride(m_AtPwPsnOverride, LengthInBytes);
        mMethodOverride(m_AtPwPsnOverride, ExpectedLabelGet);
        }

    mMethodsSet(psn, &m_AtPwPsnOverride);
    }

static void Override(AtPwMplsPsn self)
    {
    OverrideAtPwPsn(self);
    OverrideAtObject(self);
    }

static AtPwMplsLabelWrapper AtPwMplsLabelWrapperNew(const tAtPwMplsLabel *label)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwMplsLabelWrapper newLabel = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtPwMplsLabelWrapper));
    if (newLabel == NULL)
       return NULL;

    mMethodsGet(osal)->MemInit(osal, newLabel, 0, sizeof(tAtPwMplsLabelWrapper));

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)newLabel) == NULL)
        return NULL;

    mMethodsGet(osal)->MemCpy(osal, &(newLabel->label), label, sizeof(tAtPwMplsLabel));

    return newLabel;
    }

static eBool LabelIsSame(const tAtPwMplsLabel *label1, const tAtPwMplsLabel *label2)
    {
    if((label1->experimental != label2->experimental) ||
       (label1->label        != label2->label)        ||
       (label1->timeToLive   != label2->timeToLive))
        return cAtFalse;

    return cAtTrue;
    }

/* To initialize object */
static AtPwMplsPsn AtPwMplsPsnObjectInit(AtPwMplsPsn self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwMplsPsn));

    /* Super constructor should be called first */
    if (AtPwPsnObjectInit((AtPwPsn)self, cAtPwPsnTypeMpls) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Create list to hold all outer labels */
    self->outerLabels = AtListCreate(0);

    return self;
    }

eBool AtPwMplsPsnOuterLabelsAreIdentical(AtPwMplsPsn self, AtPwMplsPsn another)
    {
    if (self)
        return OuterLabelsAreIdentical(self, another);
    return cAtFalse;
    }

/**
 * @addtogroup AtPwMplsPsn
 * @{
 */
/**
 * Create AtPwMplsPsn object
 *
 * @return AtPwMplsPsn object
 */
AtPwMplsPsn AtPwMplsPsnNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwMplsPsn newPsn = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtPwMplsPsn));
    if (newPsn == NULL)
        return NULL;

    /* Construct it */
    return AtPwMplsPsnObjectInit(newPsn);
    }

/**
 * Set MPLS inner label
 *
 * @param self This PSN
 * @param label MPLS label
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMplsPsnInnerLabelSet(AtPwMplsPsn self, const tAtPwMplsLabel *label)
    {
    if (NULL == self || NULL == label)
        return cAtError;

    if (!LabelIsValid(label))
        return cAtErrorInvlParm;

    self->innerLabel.experimental = label->experimental;
    self->innerLabel.label = label->label;
    self->innerLabel.timeToLive = label->timeToLive;
    return cAtOk;
    }

/**
 * Get MPLS inner label
 *
 * @param self This PSN
 * @param [out] label Inner MPLS label
 * @return
 */
eAtModulePwRet AtPwMplsPsnInnerLabelGet(AtPwMplsPsn self, tAtPwMplsLabel *label)
    {
    AtOsal osal;

    if (NULL == self || NULL == label)
        return cAtError;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, label, &(self->innerLabel), sizeof(tAtPwMplsLabel));
    return cAtOk;
    }

/**
 * Add a outer label
 *
 * @param self This PSN
 * @param label Label to be added
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMplsPsnOuterLabelAdd(AtPwMplsPsn self, const tAtPwMplsLabel *label)
    {
    AtPwMplsLabelWrapper labelObject;
    eAtRet ret = cAtOk;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (label == NULL)
        return cAtErrorNullPointer;

    labelObject = AtPwMplsLabelWrapperNew(label);
    if (labelObject == NULL)
        return cAtErrorNullPointer;

    if (self->outerLabels == NULL)
        self->outerLabels = AtListCreate(AtPwMplsPsnMaxNumOuterLabels(self));

    if (self->outerLabels == NULL)
        return cAtErrorRsrcNoAvail;

    if (AtListLengthGet(self->outerLabels) >= AtPwMplsPsnMaxNumOuterLabels(self))
        {
        AtObjectDelete((AtObject)labelObject);
        return cAtErrorModeNotSupport;
        }

    ret = AtListObjectAdd(self->outerLabels, (AtObject)labelObject);
    if (ret != cAtOk)
        AtObjectDelete((AtObject)labelObject);

    return ret;
    }

/**
 * Remove an outer label
 *
 * @param self This PSN
 * @param label Outer label to be removed
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMplsPsnOuterLabelRemove(AtPwMplsPsn self, const tAtPwMplsLabel *label)
    {
    AtPwMplsLabelWrapper outerLabel;
    uint32 currentIndex = 0;

    if (self == NULL || self->outerLabels == NULL)
        return cAtError;

    while ((outerLabel = (AtPwMplsLabelWrapper)AtListObjectGet(self->outerLabels, currentIndex)) != NULL)
        {
        if (LabelIsSame(&(outerLabel->label), label))
            {
            AtListObjectRemove(self->outerLabels, (AtObject)outerLabel);
            AtObjectDelete((AtObject)outerLabel);
            return cAtOk;
            }

        currentIndex = currentIndex + 1;
        }

    return cAtError;
    }

/**
 * Get number of outer labels
 *
 * @param self This PSN
 *
 * @return Number of outer labels
 */
uint8 AtPwMplsPsnNumberOfOuterLabelsGet(AtPwMplsPsn self)
    {
    if (self == NULL || self->outerLabels == NULL)
        return 0;

    return (uint8)AtListLengthGet(self->outerLabels);
    }

/**
 * Get all of outer labels
 *
 * @param self This PSN
 * @param [out] labels Array of outer labels. Array capacity must greater than
 *              or equal to the number returned by the API
 *              AtPwMplsPsnNumberOfOuterLabelsGet(). It is ignored if NULL
 *
 * @return Number of outer label
 */
uint8 AtPwMplsPsnOuterLabelsGet(AtPwMplsPsn self, tAtPwMplsLabel *labels)
    {
    AtIterator iterator;
    AtPwMplsLabelWrapper outerLabel;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self == NULL || self->outerLabels == NULL)
        return 0;

    /* Assume input buffer is enough */
    iterator = AtPwMplsPsnOuterLabelIteratorCreate(self);
    if (iterator == NULL)
        return 0;

    while ((outerLabel = (AtPwMplsLabelWrapper)AtIteratorNext(iterator)) != NULL)
        {
        mMethodsGet(osal)->MemCpy(osal, labels, &(outerLabel->label), sizeof(tAtPwMplsLabel));
        labels++;
        }

    AtObjectDelete((AtObject)iterator);

    return (uint8)AtListLengthGet(self->outerLabels);
    }

/**
 * Get MPLS label by index
 *
 * @param self This PSN
 * @param labelIndex label inde
 * @param [out] label Structure to hold MPLS label information
 *
 * @return "label" if success or NULL on failure
 */
tAtPwMplsLabel * AtPwMplsPsnOuterLabelGetByIndex(AtPwMplsPsn self, uint8 labelIndex, tAtPwMplsLabel *label)
    {
    AtPwMplsLabelWrapper labelWraper;
    AtOsal osal;

    if (self == NULL || self->outerLabels == NULL)
        return NULL;

    labelWraper = (AtPwMplsLabelWrapper)AtListObjectGet(self->outerLabels, labelIndex);
    if (labelWraper == NULL)
        return NULL;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemCpy(osal, label, &(labelWraper->label), sizeof(tAtPwMplsLabel));
    return label;
    }

/**
 * Create an iterator to iterate all of outer labels
 *
 * @param self This PSN
 *
 * @return Outer label iterator
 */
AtIterator AtPwMplsPsnOuterLabelIteratorCreate(AtPwMplsPsn self)
    {
    return AtListIteratorCreate(self->outerLabels);
    }

/**
 * Get max number of outer labels can be added
 *
 * @param self This PSN
 *
 * @return Max number of outer labels can be added
 */
uint8 AtPwMplsPsnMaxNumOuterLabels(AtPwMplsPsn self)
    {
	AtUnused(self);
    return cAtMplsPsnMaxNumOuterLabels;
    }

/**
 * Set expected EC Label
 *
 * @param self This PSN
 * @param label Expected Label
 *
 * @return AT return code
 */
eAtModulePwRet AtPwMplsPsnExpectedLabelSet(AtPwMplsPsn self, uint32 label)
    {
    if (self == NULL)
        return cAtError;

    if (!LabelFieldIsValid(label))
        return cAtError;

    self->expectedLabel = label;

    return cAtOk;
    }

/**
 * Get expected EC ID
 *
 * @param self This PSN
 *
 * @return Expected Label
 */
uint32 AtPwMplsPsnExpectedLabelGet(AtPwMplsPsn self)
    {
    if (NULL == self)
        return 0;

    return self->expectedLabel;
    }


/**
 * Helper function to create a MPLS label
 *
 * @param label Label
 * @param experimetal Experimental
 * @param timeToLive Time to Live
 * @param [out] mplsLabel MPLS label
 *
 * @return mplsLabel
 */
tAtPwMplsLabel *AtPwMplsLabelMake(uint32 label, uint8 experimetal, uint8 timeToLive, tAtPwMplsLabel *mplsLabel)
    {
    if (mplsLabel == NULL)
        return NULL;

    /* Make sure that input is valid */
    if (!LabelFieldIsValid(label) || (experimetal > cBit2_0))
        return NULL;

    /* Construct */
    mplsLabel->experimental = experimetal;
    mplsLabel->label        = label;
    mplsLabel->timeToLive   = timeToLive;

    return mplsLabel;
    }

/** @} */

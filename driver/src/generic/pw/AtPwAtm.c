/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwAtm.c
 *
 * Created Date: Nov 19, 2012
 *
 * Description : ATM PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h"
#include "AtPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Override(AtPwAtm self)
    {
	AtUnused(self);
    }

static void MethodsInit(AtPwAtm self)
    {
	AtUnused(self);
    }

/* To initialize object */
AtPwAtm AtPwAtmObjectInit(AtPwAtm self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPwAtm));

    /* Super constructor should be called first */
    if (AtPwObjectInit((AtPw)self, pwId, module, cAtPwTypeATM) == NULL)
        return NULL;

    /* Override Delete method of AtObject class */
    Override(self);

    /* Initialize Methods */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

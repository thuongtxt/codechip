/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwHdlc.c
 *
 * Created Date: Nov 19, 2012
 *
 * Description : HDLC PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwHdlc.h"

#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h"
#include "AtModulePw.h"
#include "AtPwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwHdlcMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePwRet PayloadTypeSet(AtPwHdlc self, eAtPwHdlcPayloadType payloadType)
    {
    AtUnused(self);
    AtUnused(payloadType);
    return cAtErrorNotImplemented;
    }

static eAtPwHdlcPayloadType PayloadTypeGet(AtPwHdlc self)
    {
    AtUnused(self);
    return cAtPwHdlcPayloadTypeInvalid;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "hdlc_pw";
    }

static void OverrideAtChannel(AtPwHdlc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPwHdlc self)
    {
    OverrideAtChannel(self);
    }

static void MethodsInit(AtPwHdlc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, PayloadTypeSet);
        mMethodOverride(m_methods, PayloadTypeGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwHdlc);
    }

/* To initialize object */
AtPwHdlc AtPwHdlcObjectInit(AtPwHdlc self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwObjectInit((AtPw)self, pwId, module, cAtPwTypeHdlc) == NULL)
        return NULL;

    MethodsInit(self);
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * Set payload type
 *
 * @param self This Pseudowire
 * @param payloadType Payload type. Refer @ref eAtPwHdlcPayloadType "HDLC Pseudowire payload types".
 *
 * @return AT return code
 */
eAtModulePwRet AtPwHdlcPayloadTypeSet(AtPwHdlc self, eAtPwHdlcPayloadType payloadType)
    {
    mNumericalAttributeSet(PayloadTypeSet, payloadType);
    }

/**
 * Get payload type
 * @param self This Pseudowire
 *
 * @return Payload type. Refer @ref eAtPwHdlcPayloadType "HDLC Pseudowire payload types".
 */
eAtPwHdlcPayloadType AtPwHdlcPayloadTypeGet(AtPwHdlc self)
    {
    mAttributeGet(PayloadTypeGet, eAtPwHdlcPayloadType, cAtPwHdlcPayloadTypeInvalid);
    }

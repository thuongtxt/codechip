/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwHsGroup.c
 *
 * Created Date: May 7, 2015
 *
 * Description : Hot-swap protection group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwInternal.h"
#include "AtPwGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPwGroupMethods m_AtPwGroupOverride;

/* Save super implementation */
static const tAtPwGroupMethods *m_AtPwGroupMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePwRet PwAdd(AtPwGroup self, AtPw pw)
    {
    if (pw == NULL)
        return cAtErrorNullPointer;

    /* Return OK if this PW already added to this group */
    if (AtPwHsGroupGet(pw) == self)
        return cAtOk;

    /* Check if this PW belongs to another APS group */
    if (AtPwHsGroupGet(pw) != NULL)
        return cAtErrorChannelBusy;

    AtPwHsGroupSet(pw, self);
    return m_AtPwGroupMethods->PwAdd(self, pw);
    }

static eAtModulePwRet PwRemove(AtPwGroup self, AtPw pw)
    {
    if (pw == NULL)
        return cAtOk;

    /* Return error if this PW does not belong to this group */
    if (AtPwHsGroupGet(pw) != self)
        return cAtErrorInvlParm;

    AtPwHsGroupSet(pw, NULL);
    return m_AtPwGroupMethods->PwRemove(self, pw);
    }

static eAtPwGroupType TypeGet(AtPwGroup self)
    {
    AtUnused(self);
    return cAtPwGroupTypeHs;
    }

static const char *TypeString(AtPwGroup self)
    {
    AtUnused(self);
    return "pwHsGroup";
    }

static eAtModulePwRet Enable(AtPwGroup self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtPwGroup self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePwRet Init(AtPwGroup self)
    {
    eAtRet ret = cAtOk;
    ret |= AtPwGroupTxLabelSetSelect(self, cAtPwGroupLabelSetPrimary);
    ret |= AtPwGroupRxLabelSetSelect(self, cAtPwGroupLabelSetPrimary);
    return ret;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwHsGroup);
    }

static void OverrideAtPwGroup(AtPwGroup self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if (!m_methodsInit)
        {
        m_AtPwGroupMethods = mMethodsGet(self);

        mMethodsGet(osal)->MemCpy(osal, &m_AtPwGroupOverride, m_AtPwGroupMethods, sizeof(m_AtPwGroupOverride));
        mMethodOverride(m_AtPwGroupOverride, PwAdd);
        mMethodOverride(m_AtPwGroupOverride, PwRemove);
        mMethodOverride(m_AtPwGroupOverride, TypeGet);
        mMethodOverride(m_AtPwGroupOverride, Enable);
        mMethodOverride(m_AtPwGroupOverride, IsEnabled);
        mMethodOverride(m_AtPwGroupOverride, Init);
        mMethodOverride(m_AtPwGroupOverride, TypeString);
        }

    mMethodsSet(self, &m_AtPwGroupOverride);
    }

static void Override(AtPwGroup self)
    {
    OverrideAtPwGroup(self);
    }

AtPwGroup AtPwHsGroupObjectInit(AtPwGroup self, uint32 groupId, AtModulePw module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (AtPwGroupObjectInit(self, groupId, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

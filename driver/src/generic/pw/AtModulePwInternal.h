/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtModulePwInternal.h
 * 
 * Created Date: Nov 17, 2012
 *
 * Description : PW module class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPWINTERNAL_H_
#define _ATMODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include <AtPwHdlc.h>

#include "../man/AtModuleInternal.h"
#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h"
#include "../../util/AtIteratorInternal.h"

#include "AtModulePw.h"
#include "AtPwCep.h"
#include "AtPwSAToP.h"
#include "AtPwCESoP.h"
#include "AtPwAtm.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePwMethods
    {
    AtPwCep (*CepCreate)(AtModulePw self, uint32 pwId, eAtPwCepMode mode);
    AtPwSAToP (*SAToPCreate)(AtModulePw self, uint32 pwId);
    AtPwCESoP (*CESoPCreate)(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode);
    AtPwHdlc (*HdlcCreate)(AtModulePw self, AtHdlcChannel hdlcChannel);
    AtPwPpp (*PppCreate)(AtModulePw self, AtPppLink pppLink);
    AtPwPpp (*MlpppCreate)(AtModulePw self, AtMpBundle mpBundle);
    AtPwFr (*FrVcCreate)(AtModulePw self, AtFrVirtualCircuit frVc);
    AtPwAtm (*AtmCreate)(AtModulePw self, uint32 pwId, eAtAtmPwType atmPwType);
    AtPwToh (*TohCreate)(AtModulePw self, uint32 pwId);

    AtPw (*GetPw)(AtModulePw self, uint32 pwId);
    eAtRet (*DeletePw)(AtModulePw self, uint32 pw);
    uint32 (*FreePwGet)(AtModulePw self);
    eAtRet (*DeletePwObject)(AtModulePw self, AtPw pw);
    eAtRet (*DeleteHdlcPwObject)(AtModulePw self, AtPw pw);

    /* DCR processing */
    eAtModulePwRet (*DcrClockSourceSet)(AtModulePw self, eAtPwDcrClockSource clockSource);
    eAtPwDcrClockSource (*DcrClockSourceGet)(AtModulePw self);
    eAtModulePwRet (*DcrClockFrequencySet)(AtModulePw self, uint32 khz);
    uint32 (*DcrClockFrequencyGet)(AtModulePw self);
    eAtModulePwRet (*RtpTimestampFrequencySet)(AtModulePw self, uint32 khz);
    uint32 (*RtpTimestampFrequencyGet)(AtModulePw self);
    eAtModulePwRet (*CepRtpTimestampFrequencySet)(AtModulePw self, uint32 khz);
    uint32 (*CepRtpTimestampFrequencyGet)(AtModulePw self);
    eAtModulePwRet (*CesRtpTimestampFrequencySet)(AtModulePw self, uint32 khz);
    uint32 (*CesRtpTimestampFrequencyGet)(AtModulePw self);
    eBool (*RtpTimestampFrequencyIsSupported)(AtModulePw self, uint32 khz);
    uint32 (*DefaultDcrClockFrequency)(AtModulePw self);

    /* Return the number of pseudowire, don't mis-understand to "last available pseudowire ID" */
    uint32 (*MaxPwsGet)(AtModulePw self);

    /* Internal methods */
    AtPwCep (*CepObjectCreate)(AtModulePw self, uint32 pwId, eAtPwCepMode mode);
    AtPwSAToP (*SAToPObjectCreate)(AtModulePw self, uint32 pwId);
    AtPwCESoP (*CESoPObjectCreate)(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode);
    AtPwHdlc (*HdlcPwObjectCreate)(AtModulePw self, AtHdlcChannel hdlcChannel);
    AtPwPpp (*PppPwObjectCreate)(AtModulePw self, AtPppLink pppLink);
    AtPwPpp (*MlpppPwObjectCreate)(AtModulePw self, AtMpBundle mpBundle);
    AtPwFr (*FrPwObjectCreate)(AtModulePw self, AtFrVirtualCircuit frVc);
    AtPwAtm (*AtmPwObjectCreate)(AtModulePw self, uint32 pwId);
    AtPwToh (*TohPwObjectCreate)(AtModulePw self, uint32 pwId);
    eBool (*RtpIsEnabledAsDefault)(AtModulePw self);

    /* Idle code */
    eAtModulePwRet (*IdleCodeSet)(AtModulePw self, uint8 idleCode);
    uint8  (*IdleCodeGet)(AtModulePw self);
    eBool (*IdleCodeIsSupported)(AtModulePw self);
    eBool (*CasIdleCodeIsSupported)(AtModulePw self);

    /* PW grouping */
    /* PW APS group management */
    uint32 (*MaxApsGroupsGet)(AtModulePw self);
    AtPwGroup (*ApsGroupCreate)(AtModulePw self, uint32 groupId);
    eAtModulePwRet (*ApsGroupDelete)(AtModulePw self, uint32 groupId);
    AtPwGroup (*ApsGroupGet)(AtModulePw self, uint32 groupId);

    /* PW Hot-Standby group management */
    uint32 (*MaxHsGroupsGet)(AtModulePw self);
    AtPwGroup (*HsGroupCreate)(AtModulePw self, uint32 groupId);
    eAtModulePwRet (*HsGroupDelete)(AtModulePw self, uint32 groupId);
    AtPwGroup (*HsGroupGet)(AtModulePw self, uint32 groupId);

    AtPwGroup (*ApsGroupObjectCreate)(AtModulePw self, uint32 groupId);
    AtPwGroup (*HsGroupObjectCreate)(AtModulePw self, uint32 groupId);

    /* Jitter centering*/
    eAtModulePwRet (*JitterBufferCenteringEnable)(AtModulePw self, eBool enable);
    eBool (*JitterBufferCenteringIsEnabled)(AtModulePw self);
    eBool (*JitterBufferCenteringIsSupported)(AtModulePw self);

    uint32 (*DefaultLopsSetThreshold)(AtModulePw self);
    uint32 (*DefaultLopsClearThreshold)(AtModulePw self);
    eBool (*AutoRxMBitShouldEnable)(AtModulePw self);
    eBool (*DefaultCESoPPayloadSizeInBytes)(AtModulePw self);

    /* Jitter buffer RAM blocks resource management */
    uint32 (*JitterBufferMaxBlocksGet)(AtModulePw self);
    uint32 (*JitterBufferBlockSizeInBytesGet)(AtModulePw self);
    uint32 (*JitterBufferFreeBlocksGet)(AtModulePw self);
    }tAtModulePwMethods;

typedef struct tAtModulePw
    {
    tAtModule super;
    const tAtModulePwMethods *methods;

    AtPw *pseudowires; /* To cached all created pseudowires */
    AtPwGroup *pwApsGroups;
    AtPwGroup *pwHsGroups;
    eBool isCesopPayloadSizeInByte;
    }tAtModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw AtModulePwObjectInit(AtModulePw self, AtDevice device);

eBool AtModulePwRtpIsEnabledAsDefault(AtModulePw self);
uint32 AtModulePwDefaultLopsSetThreshold(AtModulePw self);
uint32 AtModulePwDefaultLopsClearThreshold(AtModulePw self);
eBool AtModulePwAutoRxMBitShouldEnable(AtModulePw self);
eBool AtModulePwCesopPayloadSizeInByteIsEnabled(AtModulePw self);
eAtRet AtModulePwCesopPayloadSizeInByteEnable(AtModulePw self, eBool enabled);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPWINTERNAL_H_ */


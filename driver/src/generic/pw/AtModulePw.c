/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pseudowire
 *
 * File        : AtModulePw.c
 *
 * Created Date: Sep 18, 2012
 *
 * Description : Pseudowire module generic implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModulePwInternal.h"
#include "AtPwInternal.h"
#include "AtPwGroupInternal.h"
#include "AtHdlcChannel.h"
#include "AtPppLink.h"
#include "AtFrVirtualCircuit.h"

#include "../../util/AtIteratorInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mModulePwIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtModulePwMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Original methods of object */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/* For PW iterator */
static char m_pwIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_pwAtArrayIteratorOverride;
static tAtIteratorMethods m_pwAtIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PwIdIsValid(AtModulePw self, uint32 pwId)
    {
    return (pwId < AtModulePwMaxPwsGet(self)) ? cAtTrue : cAtFalse;
    }

static eBool PwCanBeCreated(AtModulePw self, uint32 pwId)
    {
    if (!PwIdIsValid(self, pwId))
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "PW ID %d is out of range\r\n", pwId);
        return cAtFalse;
        }

    if (self->pseudowires == NULL)
        return cAtFalse;

    return cAtTrue;
    }

static AtPw CachePw(AtModulePw self, AtPw pw, eAtPwType pwType)
    {
	AtUnused(pwType);
    if ((pw == NULL) || (self->pseudowires == NULL))
        return NULL;

    self->pseudowires[AtChannelIdGet((AtChannel)pw)] = pw;
    return pw;
    }

static AtPwCep CepCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
    AtPw pw;

    if (!PwCanBeCreated(self, pwId))
        return NULL;

    /* If same pw has been created before, just return it */
    pw = AtModulePwGetPw(self, pwId);
    if (pw)
        {
        if ((AtPwTypeGet(pw) == cAtPwTypeCEP) && (AtPwCepModeGet((AtPwCep)pw) == mode))
            return (AtPwCep)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    pw = (AtPw)mMethodsGet(self)->CepObjectCreate(self, pwId, mode);
    if (pw == NULL)
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Cannot create CEP#%d, it may not be supported\r\n", pwId);
    return (AtPwCep)CachePw(self, pw, cAtPwTypeCEP);
    }

static AtPwSAToP SAToPCreate(AtModulePw self, uint32 pwId)
    {
    AtPw pw;

    if (!PwCanBeCreated(self, pwId))
        return NULL;

    /* If pw has been created before, just return it */
    pw = AtModulePwGetPw(self, pwId);
    if (pw)
        {
        if (AtPwTypeGet(pw) == cAtPwTypeSAToP)
            return (AtPwSAToP)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    pw = (AtPw)mMethodsGet(self)->SAToPObjectCreate(self, pwId);
    if (pw == NULL)
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Cannot create SAToP#%d, it may not be supported\r\n", pwId);

    return (AtPwSAToP)CachePw(self, pw, cAtPwTypeSAToP);
    }

static AtPwCESoP CESoPCreate(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode)
    {
    AtPw pw;

    if (!PwCanBeCreated(self, pwId))
        return NULL;

    /* If same pw has been created before, just return it */
    pw = AtModulePwGetPw(self, pwId);
    if (pw)
        {
        if ((AtPwTypeGet(pw) == cAtPwTypeCESoP) && (AtPwCESoPModeGet((AtPwCESoP)pw) == mode))
            return (AtPwCESoP)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    pw = (AtPw)mMethodsGet(self)->CESoPObjectCreate(self, pwId, mode);
    if (pw == NULL)
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Cannot create CESoP#%d, it may not be supported\r\n", pwId);

    return (AtPwCESoP)CachePw(self, pw, cAtPwTypeCESoP);
    }

static AtPwHdlc HdlcCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    AtPw pw;

    /* If pw has been created before, just return it */
    pw = AtChannelBoundPwGet((AtChannel)hdlcChannel);
    if (pw)
        {
        if (AtPwTypeGet(pw) == cAtPwTypeHdlc)
            return (AtPwHdlc)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    pw = (AtPw)mMethodsGet(self)->HdlcPwObjectCreate(self, hdlcChannel);
    if (pw == NULL)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot create HDLC PW# %s.%s, it may not be supported\r\n",
                    AtChannelTypeString((AtChannel)hdlcChannel), AtChannelIdString((AtChannel)hdlcChannel));
        }

    return (AtPwHdlc)pw;
    }

static AtPwAtm AtmCreate(AtModulePw self, uint32 pwId, eAtAtmPwType atmPwType)
    {
    AtPw pw;
	AtUnused(atmPwType);

    if (!PwCanBeCreated(self, pwId))
        return NULL;

    /* If pw has been created before, just return it */
    pw = AtModulePwGetPw(self, pwId);
    if (pw)
        {
        if (AtPwTypeGet(pw) == cAtPwTypeATM)
            return (AtPwAtm)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    /* Create new ATM pw */
    pw = (AtPw)mMethodsGet(self)->AtmPwObjectCreate(self, pwId);
    if (pw == NULL)
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Cannot create ATM PW#%d, it may not be supported\r\n", pwId);

    return (AtPwAtm)CachePw(self, pw, cAtPwTypeATM);
    }

static AtPwToh TohCreate(AtModulePw self, uint32 pwId)
    {
    AtPw pw;

    if (!PwCanBeCreated(self, pwId))
        return NULL;

    /* Create new TOH pw */
    pw = (AtPw)mMethodsGet(self)->TohPwObjectCreate(self, pwId);
    if (pw == NULL)
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Cannot create TOH#%d, it may not be supported\r\n", pwId);

    return (AtPwToh)CachePw(self, pw, cAtPwTypeToh);
    }

static AtPwPpp PppCreate(AtModulePw self, AtPppLink pppLink)
    {
    AtPw pw;

    /* If pw has been created before, just return it */
    pw = AtChannelBoundPwGet((AtChannel)pppLink);
    if (pw)
        {
        if (AtPwTypeGet(pw) == cAtPwTypePpp)
            return (AtPwPpp)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    pw = (AtPw)mMethodsGet(self)->PppPwObjectCreate(self, pppLink);
    if (pw == NULL)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot create PPP PW#%s.%s, it may not be supported\r\n",
                    AtChannelTypeString((AtChannel)pppLink), AtChannelIdString((AtChannel)pppLink));
        }

    return (AtPwPpp)pw;
    }

static AtPwPpp MlpppCreate(AtModulePw self, AtMpBundle mpBundle)
    {
    AtPw pw;

    /* If pw has been created before, just return it */
    pw = AtChannelBoundPwGet((AtChannel)mpBundle);
    if (pw)
        {
        if (AtPwTypeGet(pw) == cAtPwTypeMlppp)
            return (AtPwPpp)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    pw = (AtPw)mMethodsGet(self)->MlpppPwObjectCreate(self, mpBundle);
    if (pw == NULL)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot create Multilink PPP PW#%s.%s, it may not be supported\r\n",
                    AtChannelTypeString((AtChannel)mpBundle), AtChannelIdString((AtChannel)mpBundle));
        }

    return (AtPwPpp)pw;
    }

static AtPwFr FrVcCreate(AtModulePw self, AtFrVirtualCircuit frVc)
    {
    AtPw pw;

    /* If pw has been created before, just return it */
    pw = AtChannelBoundPwGet((AtChannel)frVc);
    if (pw)
        {
        if (AtPwTypeGet(pw) == cAtPwTypeFr)
            return (AtPwFr)pw;

        /* Another kind of PW is existing */
        return NULL;
        }

    pw = (AtPw)mMethodsGet(self)->FrPwObjectCreate(self, frVc);
    if (pw == NULL)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Cannot create Frame Relay Virtual Circuit PW#%s.%s, it may not be supported\r\n",
                    AtChannelTypeString((AtChannel)frVc), AtChannelIdString((AtChannel)frVc));
        }

    return (AtPwFr)pw;
    }

static AtPw GetPw(AtModulePw self, uint32 pwId)
    {
    if (self->pseudowires == NULL)
        return NULL;

    if (!PwIdIsValid(self, pwId))
        return NULL;

    return self->pseudowires[pwId];
    }

static eAtRet DeletePw(AtModulePw self, uint32 pwId)
    {
    AtPw pw;

    if (!PwIdIsValid(self, pwId) || (self->pseudowires == NULL))
        return cAtErrorInvlParm;

    pw = self->pseudowires[pwId];
    if (pw == NULL)
        return cAtErrorNullPointer;

    AtObjectDelete((AtObject)pw);
    self->pseudowires[pwId] = NULL;

    return cAtOk;
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static AtPwSAToP SAToPObjectCreate(AtModulePw self, uint32 pwId)
    {
	AtUnused(pwId);
	AtUnused(self);
    /* let concrete class do */
    return NULL;
    }

static AtPwCESoP CESoPObjectCreate(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode)
    {
	AtUnused(mode);
	AtUnused(pwId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPwHdlc HdlcPwObjectCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(hdlcChannel);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPwPpp PppPwObjectCreate(AtModulePw self, AtPppLink pppLink)
    {
    AtUnused(pppLink);
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPwPpp MlpppPwObjectCreate(AtModulePw self, AtMpBundle mpBundle)
    {
    AtUnused(mpBundle);
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPwAtm AtmPwObjectCreate(AtModulePw self, uint32 pwId)
    {
	AtUnused(pwId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPwToh TohPwObjectCreate(AtModulePw self, uint32 pwId)
    {
	AtUnused(pwId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtRet AllPwDelete(AtModulePw self)
    {
    uint32 maxNumOfPws;
    uint32 pwId;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->pseudowires == NULL)
        return cAtOk;

    /* Get maximum number of supported pseudowires */
    maxNumOfPws = mMethodsGet(self)->MaxPwsGet(self);

    /* Delete each pseudowire */
    for (pwId = 0; pwId < maxNumOfPws; pwId++)
        AtObjectDelete((AtObject)(self->pseudowires[pwId]));

    /* Delete memory that holds all PW Objects */
    mMethodsGet(osal)->MemFree(osal, self->pseudowires);
    self->pseudowires = NULL;

    return cAtOk;
    }

static eAtRet AllPwApsGroupDelete(AtModulePw self)
    {
    uint32 groupId;
    uint32 maxNumGroups;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->pwApsGroups == NULL)
        return cAtOk;

    maxNumGroups = AtModulePwMaxApsGroupsGet(self);

    /* Delete each pseudowire */
    for (groupId = 0; groupId < maxNumGroups; groupId++)
        AtObjectDelete((AtObject)(self->pwApsGroups[groupId]));

    /* Delete memory that holds all PW Objects */
    mMethodsGet(osal)->MemFree(osal, self->pwApsGroups);
    self->pwApsGroups = NULL;

    return cAtOk;
    }

static eAtRet AllPwHsGroupDelete(AtModulePw self)
    {
    uint32 groupId;
    uint32 maxNumGroups;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->pwHsGroups == NULL)
        return cAtOk;

    maxNumGroups = AtModulePwMaxHsGroupsGet(self);

    /* Delete each pseudowire */
    for (groupId = 0; groupId < maxNumGroups; groupId++)
        AtObjectDelete((AtObject)(self->pwHsGroups[groupId]));

    /* Delete memory that holds all PW Objects */
    mMethodsGet(osal)->MemFree(osal, self->pwHsGroups);
    self->pwHsGroups = NULL;

    return cAtOk;
    }

/* Delete module */
static void Delete(AtObject self)
    {
    /* Delete private data */
    AllPwApsGroupDelete((AtModulePw)self);
    AllPwHsGroupDelete((AtModulePw)self);
    AllPwDelete((AtModulePw)self);

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static eAtRet AllPwCreate(AtModulePw modulePw)
    {
    uint32 maxNumOfPws, memorySize;
    AtPw *allPws;

    AtOsal osal = AtSharedDriverOsalGet();

    /* Get maximum number of supported pws */
    maxNumOfPws = mMethodsGet(modulePw)->MaxPwsGet(modulePw);

    /* Initialize all pseudowires managed by this module */
    memorySize = sizeof(AtPw) * maxNumOfPws;
    allPws = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allPws == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allPws, 0, memorySize);

    /* Save */
    modulePw->pseudowires = allPws;

    return cAtOk;
    }

static eAtModulePwRet DcrClockSourceSet(AtModulePw self, eAtPwDcrClockSource clockSource)
    {
	AtUnused(clockSource);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPwDcrClockSource DcrClockSourceGet(AtModulePw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtPwDcrClockSourceUnknown;
    }

static eAtModulePwRet DcrClockFrequencySet(AtModulePw self, uint32 khz)
    {
	AtUnused(khz);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 DcrClockFrequencyGet(AtModulePw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet RtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
	AtUnused(khz);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 RtpTimestampFrequencyGet(AtModulePw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePwRet CepRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(khz);
    return cAtError;
    }

static uint32 CepRtpTimestampFrequencyGet(AtModulePw self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtModulePwRet CesRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(khz);
    return cAtError;
    }

static uint32 CesRtpTimestampFrequencyGet(AtModulePw self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 DefaultDcrClockFrequency(AtModulePw self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static eAtRet DefaultDcrSet(AtModulePw self)
    {
    eAtRet ret = cAtOk;

    /* Use PRC is a default choice (use system mode has not much meaning) */
    ret |= AtModulePwDcrClockSourceSet(self, cAtPwDcrClockSourcePrc);
    ret |= AtModulePwDcrClockFrequencySet(self, mMethodsGet(self)->DefaultDcrClockFrequency(self));
    ret |= AtModulePwRtpTimestampFrequencySet(self, 19440);

    return ret;
    }

static eAtRet DefaultSet(AtModulePw self)
    {
    eAtRet ret = cAtOk;

    ret |= DefaultDcrSet(self);

    if (mMethodsGet(self)->DefaultCESoPPayloadSizeInBytes(self))
        ret |= AtModulePwCesopPayloadSizeInByteEnable(self, cAtTrue);

    /* To be added more */

    return ret;
    }

static eAtRet AllPwApsGroupsCreate(AtModulePw self)
    {
    uint32 memSize;
    AtPwGroup *allPwGroups;
    AtOsal osal = AtSharedDriverOsalGet();

    memSize = AtModulePwMaxApsGroupsGet(self) * sizeof(AtPwGroup);
    if (memSize == 0)
        return cAtOk;

    allPwGroups = mMethodsGet(osal)->MemAlloc(osal, memSize);
    if (allPwGroups == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allPwGroups, 0, memSize);

    /* Save */
    self->pwApsGroups = allPwGroups;

    return cAtOk;
    }

static eAtRet AllPwHsGroupsCreate(AtModulePw self)
    {
    uint32 memSize;
    AtPwGroup *allPwGroups;
    AtOsal osal = AtSharedDriverOsalGet();

    memSize = AtModulePwMaxHsGroupsGet(self) * sizeof(AtPwGroup);
    if (memSize == 0)
        return cAtOk;

    allPwGroups = mMethodsGet(osal)->MemAlloc(osal, memSize);
    if (allPwGroups == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allPwGroups, 0, memSize);

    /* Save */
    self->pwHsGroups = allPwGroups;

    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    AtModulePw modulePw = (AtModulePw)self;
    eAtRet ret = cAtOk;

    /* Delete all pseudowire*/
    ret |= AllPwDelete(modulePw);
    ret |= AllPwApsGroupDelete((AtModulePw)self);
    ret |= AllPwHsGroupDelete((AtModulePw)self);

    /* Create all pseudowire */
    ret |= AllPwCreate(modulePw);
    ret |= AllPwApsGroupsCreate(modulePw);
    ret |= AllPwHsGroupsCreate(modulePw);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    return DefaultSet((AtModulePw)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "pseudowire";
    }

static const char *CapacityDescription(AtModule self)
    {
    static char capacity[32];

    AtSprintf(capacity, "pws: %u", AtModulePwMaxPwsGet((AtModulePw)self));

    return capacity;
    }

static void StatusClear(AtModule self)
    {
    AtIterator pwIterator = AtModulePwIteratorCreate((AtModulePw)self);
    AtChannel pw;

    m_AtModuleMethods->StatusClear(self);

    while ((pw = (AtChannel)AtIteratorNext(pwIterator)) != NULL)
        AtChannelStatusClear(pw);

    AtObjectDelete((AtObject)pwIterator);
    }

static uint32 FreePwGet(AtModulePw self)
    {
    uint32 pwId;

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(self); pwId++)
        {
        if (self->pseudowires[pwId] == NULL)
            break;
        }

    return pwId;
    }

static eAtModulePwRet IdleCodeSet(AtModulePw self, uint8 idleCode)
    {
	AtUnused(idleCode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 IdleCodeGet(AtModulePw self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eBool IdleCodeIsSupported(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CasIdleCodeIsSupported(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RtpIsEnabledAsDefault(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz)
    {
    /* Concrete will decide what value is supported */
    AtUnused(self);
    AtUnused(khz);
    return cAtTrue;
    }

static uint32 MaxApsGroupsGet(AtModulePw self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static uint8 ApsGroupIdIsValid(AtModulePw self, uint32 groupId)
    {
    return (groupId < AtModulePwMaxApsGroupsGet(self)) ? cAtTrue : cAtFalse;
    }

static eBool ApsGroupCanBeCreated(AtModulePw self, uint32 groupId)
    {
    if (!ApsGroupIdIsValid(self, groupId))
        return cAtFalse;

    if (self->pwApsGroups == NULL)
        return cAtFalse;

    return cAtTrue;
    }

static AtPwGroup CacheApsGroup(AtModulePw self, AtPwGroup pwGroup)
    {
    if ((pwGroup == NULL) || (self->pwApsGroups == NULL))
        return NULL;

    self->pwApsGroups[AtPwGroupIdGet(pwGroup)] = pwGroup;
    return pwGroup;
    }

static AtPwGroup ApsGroupCreate(AtModulePw self, uint32 groupId)
    {
    AtPwGroup pwGroup;

    if (!ApsGroupCanBeCreated(self, groupId))
        return NULL;

    /* If same APS group has been created before, just return it */
    pwGroup = AtModulePwApsGroupGet(self, groupId);
    if (pwGroup != NULL)
        return pwGroup;

    pwGroup = mMethodsGet(self)->ApsGroupObjectCreate(self, groupId);
    if (AtPwGroupInit(pwGroup) != cAtOk)
        return NULL;
    return CacheApsGroup(self, pwGroup);
    }

static eAtModulePwRet ApsGroupDelete(AtModulePw self, uint32 groupId)
    {
    AtPwGroup pwGroup;

    if (!ApsGroupIdIsValid(self, groupId))
        return cAtErrorInvlParm;

    pwGroup = self->pwApsGroups[groupId];
    if (pwGroup == NULL)
        return cAtOk;

    while (AtPwGroupNumPws(pwGroup) > 0)
        AtPwGroupPwRemove(pwGroup, AtPwGroupPwAtIndex(pwGroup, 0));

    AtObjectDelete((AtObject)pwGroup);
    self->pwApsGroups[groupId] = NULL;

    return cAtOk;
    }

static AtPwGroup ApsGroupGet(AtModulePw self, uint32 groupId)
    {
    if (self->pwApsGroups == NULL)
        return NULL;

    if (!ApsGroupIdIsValid(self, groupId))
        return NULL;

    return self->pwApsGroups[groupId];
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static uint8 HsGroupIdIsValid(AtModulePw self, uint32 groupId)
    {
    return (groupId < AtModulePwMaxHsGroupsGet(self)) ? cAtTrue : cAtFalse;
    }

static eBool HsGroupCanBeCreated(AtModulePw self, uint32 groupId)
    {
    if (!HsGroupIdIsValid(self, groupId))
        return cAtFalse;

    if (self->pwHsGroups == NULL)
        return cAtFalse;

    return cAtTrue;
    }

static AtPwGroup CacheHsGroup(AtModulePw self, AtPwGroup pwGroup)
    {
    if ((pwGroup == NULL) || (self->pwHsGroups == NULL))
        return NULL;

    self->pwHsGroups[AtPwGroupIdGet(pwGroup)] = pwGroup;
    return pwGroup;
    }

static AtPwGroup HsGroupCreate(AtModulePw self, uint32 groupId)
    {
    AtPwGroup pwGroup;

    if (!HsGroupCanBeCreated(self, groupId))
        return NULL;

    /* If same APS group has been created before, just return it */
    pwGroup = AtModulePwHsGroupGet(self, groupId);
    if (pwGroup != NULL)
        return pwGroup;

    pwGroup = mMethodsGet(self)->HsGroupObjectCreate(self, groupId);
    if (AtPwGroupInit(pwGroup) != cAtOk)
        return NULL;

    return CacheHsGroup(self, pwGroup);
    }

static eAtModulePwRet HsGroupDelete(AtModulePw self, uint32 groupId)
    {
    AtPwGroup pwGroup;

    if (!HsGroupIdIsValid(self, groupId))
        return cAtErrorInvlParm;

    pwGroup = self->pwHsGroups[groupId];
    if (pwGroup == NULL)
        return cAtOk;

    while (AtPwGroupNumPws(pwGroup) > 0)
        AtPwGroupPwRemove(pwGroup, AtPwGroupPwAtIndex(pwGroup, 0));
    AtObjectDelete((AtObject)pwGroup);
    self->pwHsGroups[groupId] = NULL;

    return cAtOk;
    }

static AtPwGroup HsGroupGet(AtModulePw self, uint32 groupId)
    {
    if (self->pwHsGroups == NULL)
        return NULL;

    if (!HsGroupIdIsValid(self, groupId))
        return NULL;

    return self->pwHsGroups[groupId];
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModulePw object = (AtModulePw)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(pseudowires, AtModulePwMaxPwsGet(object));
    mEncodeObjects(pwApsGroups, AtModulePwMaxApsGroupsGet(object));
    mEncodeObjects(pwHsGroups, AtModulePwMaxHsGroupsGet(object));
    mEncodeUInt(isCesopPayloadSizeInByte);
    }

static eBool PwIsValid(AtPw self)
    {
    return (self == NULL) ? cAtFalse : cAtTrue;
    }

static eBool PwAtIndexIsValid(AtArrayIterator self, uint32 pwIndex)
    {
    AtPw *pwList = self->array;
    return PwIsValid(pwList[pwIndex]);
    }

static AtObject PwAtIndex(AtArrayIterator self, uint32 pwIndex)
    {
    AtPw *pwList = self->array;
    return (AtObject)pwList[pwIndex];
    }

static void PwIteratorOverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_pwIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_pwAtArrayIteratorOverride, mMethodsGet(self), sizeof(m_pwAtArrayIteratorOverride));
        m_pwAtArrayIteratorOverride.DataAtIndexIsValid = PwAtIndexIsValid;
        m_pwAtArrayIteratorOverride.DataAtIndex = PwAtIndex;
        }
    mMethodsSet(self, &m_pwAtArrayIteratorOverride);
    }

static uint16 AtModulePwCountGet(AtArrayIterator self)
    {
    uint16 count = 0;
    uint16 i;
    AtPw *pwList = self->array;
    if (NULL == pwList)
        return 0;

    for (i = 0; i < self->capacity; i++)
        if (PwIsValid((AtPw)(pwList[i])))
            count++;
    return count;
    }

static uint32 pwCount(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count >= 0)
        return (uint32)arrayIterator->count;

    arrayIterator->count = (int32)AtModulePwCountGet(arrayIterator);
    return (uint32)arrayIterator->count;
    }

static eAtModulePwRet JitterBufferCenteringEnable(AtModulePw self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool JitterBufferCenteringIsEnabled(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool JitterBufferCenteringIsSupported(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void PwIteratorOverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_pwIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_pwAtIteratorOverride, mMethodsGet(iterator), sizeof(m_pwAtIteratorOverride));
        m_pwAtIteratorOverride.Count = pwCount;
        }
    mMethodsSet(iterator, &m_pwAtIteratorOverride);
    }

static void PwIteratorOverride(AtArrayIterator self)
    {
    PwIteratorOverrideAtArrayIterator(self);
    PwIteratorOverrideAtIterator(self);
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    uint32 pwId, apsGroupId, hsGroupId;
    AtModulePw pwModule = (AtModulePw)self;

    for (apsGroupId = 0; apsGroupId < AtModulePwMaxApsGroupsGet(pwModule); apsGroupId++)
        AtModulePwApsGroupDelete(pwModule, apsGroupId);

    for (hsGroupId = 0; hsGroupId < AtModulePwMaxHsGroupsGet(pwModule); hsGroupId++)
        AtModulePwHsGroupDelete(pwModule, hsGroupId);

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
        {
        AtPw pw = AtModulePwGetPw(pwModule, pwId);
        if (pw == NULL)
            continue;
            
        AtPwCircuitUnbind(pw);
        AtModulePwDeletePw(pwModule, pwId);
        }

    return cAtOk;
    }

static void AllChannelsListenedDefectClear(AtModule self)
    {
    uint32 pwId;
    AtModulePw pwModule = (AtModulePw)self;

    for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
        AtChannelListenedDefectClear((AtChannel)AtModulePwGetPw(pwModule, pwId), NULL);
    }

static uint32 DefaultLopsSetThreshold(AtModulePw self)
    {
    AtUnused(self);
    return 30;
    }

static uint32 DefaultLopsClearThreshold(AtModulePw self)
    {
    AtUnused(self);
    return 4;
    }

static eBool IsTdmPw(uint8 pwType)
    {
    if ((pwType == cAtPwTypeCEP)   ||
        (pwType == cAtPwTypeSAToP) ||
        (pwType == cAtPwTypeCESoP))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsHdlcPw(uint8 pwType)
    {
    if ((pwType == cAtPwTypeHdlc) ||
        (pwType == cAtPwTypePpp)  ||
        (pwType == cAtPwTypeFr)   ||
        (pwType == cAtPwTypeMlppp))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DeletePwObject(AtModulePw self, AtPw pw)
    {
    uint8 pwType = AtPwTypeGet(pw);

    if (pw == NULL)
        return cAtErrorNullPointer;

    if (IsTdmPw(pwType))
        return mMethodsGet(self)->DeletePw(self, AtChannelIdGet((AtChannel)pw));

    if (pwType == cAtPwTypeATM)
        return mMethodsGet(self)->DeletePw(self, AtChannelIdGet((AtChannel)pw));

    if (IsHdlcPw(pwType))
        return mMethodsGet(self)->DeleteHdlcPwObject(self, pw);

    return cAtErrorInvlParm;
    }

static eAtRet DeleteHdlcPwObject(AtModulePw self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    /* Let concrete class do it */
    return cAtOk;
    }

static eBool AutoRxMBitShouldEnable(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 JitterBufferMaxBlocksGet(AtModulePw self)
    {
    AtUnused(self);
    /* Let concrete class do it */
    return 0;
    }

static uint32 JitterBufferBlockSizeInBytesGet(AtModulePw self)
    {
    AtUnused(self);
    /* Let concrete class do it */
    return 0;
    }

static uint32 JitterBufferFreeBlocksGet(AtModulePw self)
    {
    AtUnused(self);
    /* Let concrete class do it */
    return 0;
    }

static AtQuerier QuerierGet(AtModule self)
    {
    AtUnused(self);
    return AtModulePwSharedQuerier();
    }

static eAtRet ModulePwCesopPayloadSizeInByteEnable(AtModulePw self, eBool enabled)
    {
    if (self)
        {
        self->isCesopPayloadSizeInByte = enabled;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

static eBool DefaultCESoPPayloadSizeInBytes(AtModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtModulePw self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtObject pwObject = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(pwObject);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    /* Change behavior of super class level 2 */
    mMethodsGet(pwObject) = &m_AtObjectOverride;
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        mMethodOverride(m_AtModuleOverride, AllServicesDestroy);
        mMethodOverride(m_AtModuleOverride, AllChannelsListenedDefectClear);
        mMethodOverride(m_AtModuleOverride, QuerierGet);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

static void MethodsInit(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, CepCreate);
        mMethodOverride(m_methods, SAToPCreate);
        mMethodOverride(m_methods, CESoPCreate);
        mMethodOverride(m_methods, HdlcCreate);
        mMethodOverride(m_methods, AtmCreate);
        mMethodOverride(m_methods, TohCreate);
        mMethodOverride(m_methods, PppCreate);
        mMethodOverride(m_methods, MlpppCreate);
        mMethodOverride(m_methods, FrVcCreate);
        mMethodOverride(m_methods, GetPw);
        mMethodOverride(m_methods, DeletePw);
        mMethodOverride(m_methods, FreePwGet);
        mMethodOverride(m_methods, MaxPwsGet);
        mMethodOverride(m_methods, SAToPObjectCreate);
        mMethodOverride(m_methods, CESoPObjectCreate);
        mMethodOverride(m_methods, HdlcPwObjectCreate);
        mMethodOverride(m_methods, PppPwObjectCreate);
        mMethodOverride(m_methods, MlpppPwObjectCreate);
        mMethodOverride(m_methods, AtmPwObjectCreate);
        mMethodOverride(m_methods, TohPwObjectCreate);
        mMethodOverride(m_methods, DeletePwObject);

        mMethodOverride(m_methods, DcrClockSourceSet);
        mMethodOverride(m_methods, DcrClockSourceGet);
        mMethodOverride(m_methods, DcrClockFrequencySet);
        mMethodOverride(m_methods, DcrClockFrequencyGet);
        mMethodOverride(m_methods, RtpTimestampFrequencySet);
        mMethodOverride(m_methods, RtpTimestampFrequencyGet);
        mMethodOverride(m_methods, CepRtpTimestampFrequencySet);
        mMethodOverride(m_methods, CepRtpTimestampFrequencyGet);
        mMethodOverride(m_methods, CesRtpTimestampFrequencySet);
        mMethodOverride(m_methods, CesRtpTimestampFrequencyGet);
        mMethodOverride(m_methods, RtpTimestampFrequencyIsSupported);
        mMethodOverride(m_methods, DefaultDcrClockFrequency);

        mMethodOverride(m_methods, IdleCodeSet);
        mMethodOverride(m_methods, IdleCodeGet);
        mMethodOverride(m_methods, IdleCodeIsSupported);
        mMethodOverride(m_methods, CasIdleCodeIsSupported);
        mMethodOverride(m_methods, RtpIsEnabledAsDefault);

        mMethodOverride(m_methods, MaxApsGroupsGet);
        mMethodOverride(m_methods, ApsGroupCreate);
        mMethodOverride(m_methods, ApsGroupGet);
        mMethodOverride(m_methods, ApsGroupDelete);
        mMethodOverride(m_methods, MaxHsGroupsGet);
        mMethodOverride(m_methods, HsGroupCreate);
        mMethodOverride(m_methods, HsGroupDelete);
        mMethodOverride(m_methods, HsGroupGet);
        mMethodOverride(m_methods, JitterBufferCenteringEnable);
        mMethodOverride(m_methods, JitterBufferCenteringIsEnabled);
        mMethodOverride(m_methods, JitterBufferCenteringIsSupported);
        mMethodOverride(m_methods, DefaultLopsSetThreshold);
        mMethodOverride(m_methods, DefaultLopsClearThreshold);
        mMethodOverride(m_methods, DeleteHdlcPwObject);
        mMethodOverride(m_methods, AutoRxMBitShouldEnable);
        mMethodOverride(m_methods, JitterBufferMaxBlocksGet);
        mMethodOverride(m_methods, JitterBufferBlockSizeInBytesGet);
        mMethodOverride(m_methods, JitterBufferFreeBlocksGet);
        mMethodOverride(m_methods, DefaultCESoPPayloadSizeInBytes);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize Module PW object */
AtModulePw AtModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModulePw));

    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModulePw, device) == NULL)
        return NULL;

    /* Override Delete method of AtObject class */
    Override(self);

    /* Initialize Methods */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

eBool AtModulePwRtpIsEnabledAsDefault(AtModulePw self)
    {
    if (self)
        return mMethodsGet(self)->RtpIsEnabledAsDefault(self);

    return cAtFalse;
    }

uint32 AtModulePwDefaultLopsSetThreshold(AtModulePw self)
    {
    if (self)
        return mMethodsGet(self)->DefaultLopsSetThreshold(self);
    return 0;
    }

uint32 AtModulePwDefaultLopsClearThreshold(AtModulePw self)
    {
    if (self)
        return mMethodsGet(self)->DefaultLopsClearThreshold(self);
    return 0;
    }

eBool AtModulePwAutoRxMBitShouldEnable(AtModulePw self)
    {
    if (self)
        return mMethodsGet(self)->AutoRxMBitShouldEnable(self);
    return cAtFalse;
    }

eAtRet AtModulePwCesopPayloadSizeInByteEnable(AtModulePw self, eBool enabled)
    {
    mOneParamWrapCall(enabled, eAtRet, ModulePwCesopPayloadSizeInByteEnable(self, enabled));
    }

eBool AtModulePwCesopPayloadSizeInByteIsEnabled(AtModulePw self)
    {
    if (self)
        return self->isCesopPayloadSizeInByte;
    return cAtFalse;
    }

/**
 * @addtogroup AtModulePw
 * @{
 */

/**
 * Create CEP Pseudowire
 *
 * @param self This module
 * @param pwId Pseudowire ID
 * @param mode @ref eAtPwCepMode "CEP mode"
 *
 * @return CEP Pseudowire
 */
AtPwCep AtModulePwCepCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
    mTwoParamsObjectCreate(CepCreate, pwId, mode, AtPwCep, NULL);
    }

/**
 * Create SAToP Pseudowire
 *
 * @param self This module
 * @param pwId Pseudowire ID
 *
 * @return SAToP Pseudowire
 */
AtPwSAToP AtModulePwSAToPCreate(AtModulePw self, uint32 pwId)
    {
    mOneParamObjectCreate(SAToPCreate, AtPwSAToP, pwId);
    }

/**
 * Create CESoPSN Pseudowire
 *
 * @param self This module
 * @param pwId Pseudowire ID
 * @param mode @ref eAtPwCESoPMode "CESoP modes"
 *
 * @return CESoPSN Pseudowire
 */
AtPwCESoP AtModulePwCESoPCreate(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode)
    {
    mTwoParamsObjectCreate(CESoPCreate, pwId, mode, AtPwCESoP, NULL);
    }

/**
 * Create ATM Pseudowire
 *
 * @param self This module
 * @param pwId Pseudowire ID
 * @param atmPwType @ref eAtAtmPwType "ATM Pseudowire types"
 *
 * @return ATM Pseudowire
 */
AtPwAtm AtModulePwAtmCreate(AtModulePw self, uint32 pwId, eAtAtmPwType atmPwType)
    {
    mTwoParamsObjectCreate(AtmCreate, pwId, atmPwType, AtPwAtm, NULL);
    }

/**
 * Create HDLC Pseudowire
 *
 * @param self This module
 * @param hdlcChannel Associated HDLC channel instance.
 *
 * @return HDLC Pseudowire
 */
AtPwHdlc AtModulePwHdlcCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    mOneObjectParamObjectCreate(HdlcCreate, AtPwHdlc, hdlcChannel);
    }

/**
 * Create PPP Pseudowire
 *
 * @param self This module
 * @param pppLink Associated PPP link instance.
 *
 * @return PPP Pseudowire
 */
AtPwPpp AtModulePwPppCreate(AtModulePw self, AtPppLink pppLink)
    {
    mOneObjectParamObjectCreate(PppCreate, AtPwPpp, pppLink);
    }

/**
 * Create MP Pseudowire
 *
 * @param self This module
 * @param mpBundle MP Associated Frame-Relay instance.
 *
 * @return MP Pseudowire
 */
AtPwPpp AtModulePwMlpppCreate(AtModulePw self, AtMpBundle mpBundle)
    {
    mOneObjectParamObjectCreate(MlpppCreate, AtPwPpp, mpBundle);
    }

/**
 * Create FR VC Pseudowire
 *
 * @param self This module
 * @param frVc Associated Frame-Relay Virtual Circuit instance.
 *
 * @return FR Pseudowire
 */
AtPwFr AtModulePwFrVcCreate(AtModulePw self, AtFrVirtualCircuit frVc)
    {
    mOneObjectParamObjectCreate(FrVcCreate, AtPwFr, frVc);
    }

/**
 * Create TOH Pseudowire
 *
 * @param self This module
 * @param pwId Pseudowire ID
 *
 * @return TOH Pseudowire
 */
AtPwToh AtModulePwTohCreate(AtModulePw self, uint32 pwId)
    {
    mOneParamObjectCreate(TohCreate, AtPwToh, pwId);
    }

/**
 * Get TDM (CEP/SAToP/CESoPSN/TOH) pseudowire object by its identifier
 *
 * @param self This module
 * @param pwId Pseudowire ID
 *
 * @return Pseudowire object on success or NULL on failure
 */
AtPw AtModulePwGetPw(AtModulePw self, uint32 pwId)
    {
    mOneParamObjectGet(GetPw, AtPw, pwId);
    }

/**
 * Get one PW that has not been created
 *
 * @param self This module
 *
 * @return Valid free PW ID if success.
 */
uint32 AtModulePwFreePwGet(AtModulePw self)
    {
    mAttributeGet(FreePwGet, uint32, AtModulePwMaxPwsGet(self))
    }

/**
 * Delete a TDM Pseudowire
 *
 * @param self This module
 * @param pwId Pseudowire ID
 *
 * @return AT return code
 */
eAtModulePwRet AtModulePwDeletePw(AtModulePw self, uint32 pwId)
    {
    eAtRet ret = cAtOk;

    if (!mModulePwIsValid(self))
        return cAtError;

    mOneParamApiLogStart(pwId);
    AtModuleLock((AtModule)self);
    ret = mMethodsGet(self)->DeletePw(self, pwId);
    AtModuleUnLock((AtModule)self);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get maximum number of TDM Pseudowires this module can support
 *
 * @param self This module
 *
 * @return Maximum number of supported Pseudowires
 */
uint32 AtModulePwMaxPwsGet(AtModulePw self)
    {
    mAttributeGet(MaxPwsGet, uint32, 0);
    }

/**
 * Create pseudowire iterator
 *
 * @param self This module
 *
 * @return AtIterator object
 */
AtIterator AtModulePwIteratorCreate(AtModulePw self)
    {
    uint32 maxNumOfPw;
    AtArrayIterator iterator;

    if (NULL == self)
        return NULL;

    maxNumOfPw = mMethodsGet(self)->MaxPwsGet(self);
    iterator = AtArrayIteratorNew(self->pseudowires, maxNumOfPw);

    if (NULL == iterator)
        return NULL;

    /* Override */
    PwIteratorOverride(iterator);

    m_pwIteratorMethodsInit = 1;

    return (AtIterator)iterator;
    }

/**
 * Delete a Pseudowire object
 *
 * @param self This module
 * @param pw Pseudowire object.
 *
 * @return AT return code
 */
eAtModulePwRet AtModulePwDeletePwObject(AtModulePw self, AtPw pw)
    {
    eAtRet ret = cAtOk;

    if (!mModulePwIsValid(self))
        return cAtError;

    mOneObjectApiLogStart(pw);
    AtModuleLock((AtModule)self);
    ret = mMethodsGet(self)->DeletePwObject(self, pw);
    AtModuleUnLock((AtModule)self);
    AtDriverApiLogStop();

    return ret;
}

/**
 * Set IDLE code
 *
 * @param self This module
 * @param idleCode IDLE code
 *
 * @return AT return code
 */
eAtModulePwRet AtModulePwIdleCodeSet(AtModulePw self, uint8 idleCode)
    {
    mNumericalAttributeSet(IdleCodeSet, idleCode);
    }

/**
 * Get IDLE code
 *
 * @param self This module
 *
 * @return IDLE code
 */
uint8 AtModulePwIdleCodeGet(AtModulePw self)
    {
    mAttributeGet(IdleCodeGet, uint8, 0);
    }

/**
 * Check if module IDLE code is supported or not. If module IDLE code is not
 * supported, then it may be supported for each PW. See AtPwIdleCodeSet().
 *
 * @param self This module
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtModulePwIdleCodeIsSupported(AtModulePw self)
    {
    mAttributeGet(IdleCodeIsSupported, eBool, cAtFalse);
    }

/**
 * Check if CAS IDLE code function is supported or not.
 *
 * @param self This module
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtModulePwCasIdleCodeIsSupported(AtModulePw self)
    {
    mAttributeGet(CasIdleCodeIsSupported, eBool, cAtFalse);
    }


/**
 * Set DCR clock source
 *
 * @param self This module
 * @param clockSource @ref eAtPwDcrClockSource "DCR clock sources"
 *
 * @return AT return code
 */
eAtModulePwRet AtModulePwDcrClockSourceSet(AtModulePw self, eAtPwDcrClockSource clockSource)
    {
    mNumericalAttributeSet(DcrClockSourceSet, clockSource);
    }

/**
 * Get DCR clock source
 *
 * @param self This module
 *
 * @return @ref eAtPwDcrClockSource "DCR clock sources"
 */
eAtPwDcrClockSource AtModulePwDcrClockSourceGet(AtModulePw self)
    {
    mAttributeGet(DcrClockSourceGet, eAtPwDcrClockSource, cAtPwDcrClockSourceUnknown);
    }

/**
 * Set DCR clock frequency
 *
 * @param self This module
 * @param khz DCR clock frequency in KHz
 *
 * @return AT return code
 */
eAtModulePwRet AtModulePwDcrClockFrequencySet(AtModulePw self, uint32 khz)
    {
    mNumericalAttributeSet(DcrClockFrequencySet, khz);
    }

/**
 * Get DCR clock frequency
 *
 * @param self This module
 *
 * @return DCR clock frequency in KHz
 */
uint32 AtModulePwDcrClockFrequencyGet(AtModulePw self)
    {
    mAttributeGet(DcrClockFrequencyGet, uint32, 0);
    }

/**
 * Set RTP Time-stamp frequency. This API is used for products that do not
 * support mix of CEP and CES or there is only one common setting used for both
 * of them.
 *
 * For products that support mix of CEP and CES, calling it will apply the same
 * configuration.
 *
 * @param self This module
 * @param khz RTP Time-stamp frequency in KHz and must be multiple of 8KHz
 *
 * @return AT return code
 * @note Product 60150011 only supports these value of time-stamp frequency
 *       - 155.52 MHz
 *       - 77.76 MHz
 *       - 38.88 MHz
 *       - 1.544 � 25 MHz
 */
eAtModulePwRet AtModulePwRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    if (!mModulePwIsValid(self))
        return cAtError;

    if (!mMethodsGet(self)->RtpTimestampFrequencyIsSupported(self, khz))
        return cAtErrorModeNotSupport;

    mNumericalAttributeSet(RtpTimestampFrequencySet, khz);
}

/**
 * Get RTP Time-stamp frequency. This API is used for products that do not
 * support mix of CEP and CES or there is only one common setting used for both
 * of them.
 *
 * Calling this API for products that support mix of CEP and CES may return
 * undetermined value.
 *
 * @param self This module
 *
 * @return RTP Time-stamp frequency in KHz
 *
 * @see AtModulePwCepRtpTimestampFrequencyGet(), AtModulePwCesRtpTimestampFrequencyGet()
 */
uint32 AtModulePwRtpTimestampFrequencyGet(AtModulePw self)
    {
    mAttributeGet(RtpTimestampFrequencyGet, uint32, 0);
    }

/**
 * Set common RTP timestamp frequency for all of CEP PWs.
 *
 * @param self This module
 * @param khz RTP Time-stamp frequency in KHz
 *
 * @return AT return code
 */
eAtModulePwRet AtModulePwCepRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    if (!mModulePwIsValid(self))
        return cAtErrorNullPointer;

    if (!mMethodsGet(self)->RtpTimestampFrequencyIsSupported(self, khz))
        return cAtErrorModeNotSupport;

    mNumericalAttributeSet(CepRtpTimestampFrequencySet, khz);
}

/**
 * Get common RTP timestamp frequency of all of CEP PWs.
 *
 * @param self This module
 *
 * @return RTP Time-stamp frequency in KHz
 */
uint32 AtModulePwCepRtpTimestampFrequencyGet(AtModulePw self)
    {
    mAttributeGet(CepRtpTimestampFrequencyGet, uint32, 0);
    }

/**
 * Set common RTP timestamp frequency for all of CES PWs (CESoP, SAToP).
 *
 * @param self This module
 * @param khz RTP Time-stamp frequency in KHz
 *
 * @return AT return code
 */
eAtModulePwRet AtModulePwCesRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    if (!mModulePwIsValid(self))
        return cAtErrorNullPointer;

    if (!mMethodsGet(self)->RtpTimestampFrequencyIsSupported(self, khz))
        return cAtErrorModeNotSupport;

    mNumericalAttributeSet(CesRtpTimestampFrequencySet, khz);
}

/**
 * Check if timestamp frequency is supported or not
 *
 * @param self This module
 * @param khz RTP Time-stamp frequency in KHz
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtModulePwRtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz)
    {
    mOneParamAttributeGet(RtpTimestampFrequencyIsSupported, khz, eBool, cAtFalse);
    }

/**
 * Get common RTP timestamp frequency of all of CES PWs (CESoP, SAToP).
 *
 * @param self This module
 *
 * @return RTP Time-stamp frequency in KHz
 */
uint32 AtModulePwCesRtpTimestampFrequencyGet(AtModulePw self)
    {
    mAttributeGet(CesRtpTimestampFrequencyGet, uint32, 0);
    }

/**
 * Get maximum number of APS group that PW module can support
 *
 * @param self PW module
 *
 * @return Maximum number of APS group that PW module can support
 */
uint32 AtModulePwMaxApsGroupsGet(AtModulePw self)
    {
    mAttributeGet(MaxApsGroupsGet, uint32, 0);
    }

/**
 * Create APS group by unique ID
 *
 * @param self PW module
 * @param groupId Group ID
 *
 * @return PW group instance if success or NULL if ID is invalid.
 */
AtPwGroup AtModulePwApsGroupCreate(AtModulePw self, uint32 groupId)
    {
    mOneParamObjectCreate(ApsGroupCreate, AtPwGroup, groupId);
    }

/**
 * Delete APS group
 *
 * @param self PW module
 * @param groupId Group ID to delete
 *
 * @return AT return code
 */
eAtRet AtModulePwApsGroupDelete(AtModulePw self, uint32 groupId)
    {
    mNumericalAttributeSet(ApsGroupDelete, groupId);
    }

/**
 * Get APS group by unique ID
 *
 * @param self PW module
 * @param groupId Group ID
 *
 * @return PW group instance if it has already been created. NULL if it has not
 * been created or ID is invalid.
 */
AtPwGroup AtModulePwApsGroupGet(AtModulePw self, uint32 groupId)
    {
    mOneParamObjectGet(ApsGroupGet, AtPwGroup, groupId);
    }

/**
 * Get maximum number of Hot-Swap group PW module can support
 *
 * @param self PW module
 *
 * @return Maximum number of Hot-Swap group PW module can support
 */
uint32 AtModulePwMaxHsGroupsGet(AtModulePw self)
    {
    mAttributeGet(MaxHsGroupsGet, uint32, 0);
    }

/**
 * Create a Hot-Swap group.
 *
 * @param self PW module
 * @param groupId Group ID
 *
 * @return Hot-Swap group instance if success or NULL if ID is invalid.
 */
AtPwGroup AtModulePwHsGroupCreate(AtModulePw self, uint32 groupId)
    {
    mOneParamObjectCreate(HsGroupCreate, AtPwGroup, groupId);
    }

/**
 * Delete a Hot-Swap group
 *
 * @param self PW module
 * @param groupId Group ID
 *
 * @return AT return code
 */
eAtRet AtModulePwHsGroupDelete(AtModulePw self, uint32 groupId)
    {
    mNumericalAttributeSet(HsGroupDelete, groupId);
    }

/**
 * Get a Hot-Swap group by unique ID.
 *
 * @param self PW module
 * @param groupId Group ID
 *
 * @return Hot-Swap group instance if success or NULL if ID is invalid or it has
 * not been created yet.
 */
AtPwGroup AtModulePwHsGroupGet(AtModulePw self, uint32 groupId)
    {
    mOneParamObjectGet(HsGroupGet, AtPwGroup, groupId);
    }

/**
 * Enable/disable jitter buffer centering for a period of time after LBIT is clear.
 * The period is depend on specific products:60210021 period is 1 minutes, 60210031 period is 4 seconds.
 *
 * @param self PW module
 * @param enable enable/disable
 *
 * @return cAtOk or cAtErrorModeNotSupport
 */
eAtModulePwRet AtModulePwJitterBufferCenteringEnable(AtModulePw self, eBool enable)
    {
    mNumericalAttributeSet(JitterBufferCenteringEnable, enable);
    }

/**
 * Check if jitter buffer centering is enabled or not.
 *
 * @param self PW module
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtModulePwJitterBufferCenteringIsEnabled(AtModulePw self)
    {
    mAttributeGet(JitterBufferCenteringIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if jitter buffer centering is supported or not
 *
 * @param self PW module
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtModulePwJitterBufferCenteringIsSupported(AtModulePw self)
    {
    mAttributeGet(JitterBufferCenteringIsSupported, eBool, cAtFalse);
    }

/**
 * Get maximum number of RAM blocks dedicated for PWs jitter buffer
 *
 * @param self PW module
 * @return Maximum number of RAM blocks dedicated for PWs jitter buffer
 */
uint32 AtModulePwJitterBufferMaxBlocksGet(AtModulePw self)
    {
    mAttributeGet(JitterBufferMaxBlocksGet, uint32, 0);
    }

/**
 * Get jitter buffer block size in number of bytes
 *
 * @param self PW module
 * @return Size in number of bytes of block
 */
uint32 AtModulePwJitterBufferBlockSizeInBytesGet(AtModulePw self)
    {
    mAttributeGet(JitterBufferBlockSizeInBytesGet, uint32, 0);
    }

/**
 * Get number of free jitter buffer RAM blocks resource
 *
 * @param self
 * @return Number of free blocks
 */
uint32 AtModulePwJitterBufferFreeBlocksGet(AtModulePw self)
    {
    mAttributeGet(JitterBufferFreeBlocksGet, uint32, 0);
    }

/**
 * @}
 */

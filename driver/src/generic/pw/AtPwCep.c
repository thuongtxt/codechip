/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwCep.c
 *
 * Created Date: Nov 17, 2012
 *
 * Description : CEP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "AtPwInternal.h"
#include "AtPwCep.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwCepMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    AtPwCep pw = (AtPwCep)self;

    /* Let its super do first */
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Additional initialization */
    if (AtPwNeedInitConfig((AtPw)self))
        {
        if (AtPwCepModeGet(pw) == cAtPwCepModeFractional)
            AtPwCepCwEbmEnable(pw, cAtTrue);

        AtPwCepEparEnable(pw, cAtFalse);
        }

    return cAtOk;
    }

static eBool NeedToEquip(AtPwCep self, AtChannel channel, eBool equip)
    {
    return (equip != AtPwCepIsEquipped(self, channel)) ? cAtTrue : cAtFalse;
    }

static eBool BoundCircuitHasSubChannel(AtChannel boundChannel, AtChannel subChannel)
    {
    AtSdhChannel parent;
    AtSdhChannel sdhChannel = (AtSdhChannel)subChannel;
    while ((parent = AtSdhChannelParentChannelGet(sdhChannel)) != NULL)
        {
        if (parent == (AtSdhChannel)boundChannel)
            return cAtTrue;

        sdhChannel = parent;
        }

    return cAtFalse;
    }

static eAtModulePwRet Equip(AtPwCep self, AtChannel channel, eBool equip)
    {
    static const uint8 cMaxNumOfEquipChannel = 84;

    AtChannel boundVc = AtPwBoundCircuitGet((AtPw)self);

    if (boundVc == NULL)
        return cAtError;

    if (!BoundCircuitHasSubChannel(boundVc, channel))
        return cAtErrorInvlParm;

    if (!NeedToEquip(self, channel, equip))
        return cAtOk;

    if (equip)
        {
        if (self->equippedChannels == NULL)
            self->equippedChannels = AtListCreate(cMaxNumOfEquipChannel);

        return AtListObjectAdd(self->equippedChannels, (AtObject)channel);
        }

    return AtListObjectRemove(self->equippedChannels, (AtObject)channel);
    }

static eBool IsEquipped(AtPwCep self, AtChannel channel)
    {
    if (self->equippedChannels == NULL)
        return cAtFalse;

    return (AtListContainsObject(self->equippedChannels, (AtObject)channel));
    }

static eAtModulePwRet CwEbmEnable(AtPwCep self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Concrete will do */
    return cAtError;
    }

static eBool CwEbmIsEnabled(AtPwCep self)
    {
	AtUnused(self);
    /* Concrete will do */
    return cAtFalse;
    }

static eAtModulePwRet EparEnable(AtPwCep self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Concrete will do */
    return cAtError;
    }

static eBool EparIsEnabled(AtPwCep self)
    {
	AtUnused(self);
    /* Concrete will do */
    return cAtFalse;
    }

/* Auto N-Bit, P-Bit */
static eAtModulePwRet CwAutoNBitEnable(AtPwCep self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePwRet CwAutoPBitEnable(AtPwCep self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool CwAutoNBitIsEnabled(AtPwCep self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool CwAutoPBitIsEnabled(AtPwCep self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "cep";
    }

static eAtPwCepMode ModeGet(AtPwCep self)
    {
    return self->mode;
    }

static void Delete(AtObject self)
    {
    AtPwCep pwCep = (AtPwCep)self;

    if (pwCep->equippedChannels)
        {
        AtObjectDelete((AtObject)(pwCep->equippedChannels));
        pwCep->equippedChannels = NULL;
        }

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwCep object = (AtPwCep)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(mode);
    mEncodeList(equippedChannels);
    }

static void OverrideAtObject(AtObject self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPwCep self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPwCep self)
    {
    OverrideAtChannel(self);
    OverrideAtObject((AtObject)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwCep);
    }

static void MethodsInit(AtPwCep self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Equip);
        mMethodOverride(m_methods, IsEquipped);
        mMethodOverride(m_methods, CwEbmEnable);
        mMethodOverride(m_methods, CwEbmIsEnabled);
        mMethodOverride(m_methods, EparEnable);
        mMethodOverride(m_methods, EparIsEnabled);
        mMethodOverride(m_methods, CwAutoNBitEnable);
        mMethodOverride(m_methods, CwAutoPBitEnable);
        mMethodOverride(m_methods, CwAutoNBitIsEnabled);
        mMethodOverride(m_methods, CwAutoPBitIsEnabled);
        mMethodOverride(m_methods, ModeGet);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize object */
AtPwCep AtPwCepObjectInit(AtPwCep self, uint32 pwId, AtModulePw module, eAtPwCepMode mode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwObjectInit((AtPw)self, pwId, module, cAtPwTypeCEP) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
	
	/* Private data */
    self->mode = mode;
    
    return self;
    }

AtChannel AtPwCepEquippedChannelAtIndexGet(AtPwCep self, uint8 channelIndex)
    {
    if (self == NULL)
        return NULL;

    return (AtChannel)AtListObjectGet(self->equippedChannels, channelIndex);
    }

/**
 * @addtogroup AtPwCep
 * @{
 */

/**
 * To equip a sub-channel for fractional mode
 *
 * @param self This pseudowire
 * @param subChannel Sub-channel to equip
 * @param equip cAtTrue to equip and cAtFalse to un-equip
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCepEquip(AtPwCep self, AtChannel subChannel, eBool equip)
    {
    eAtModulePwRet ret = cAtErrorObjectNotExist;

    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        const char* objectDesc;
        AtDriverLogLock();
        objectDesc = AtDriverObjectStringToSharedBuffer((AtObject)subChannel);
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                AtSourceLocationNone, "API: %s([%s], %s, %u)\r\n",
                                AtFunction, AtObjectToString((AtObject)self), objectDesc, equip);
        AtDriverLogUnLock();
        }

    if (self)
        ret = mMethodsGet(self)->Equip(self, subChannel, equip);

    AtDriverApiLogStop();

    return ret;
    }

/**
 * Check if a sub-channel is equipped
 *
 * @param self This pseudowire
 * @param subChannel Sub-channel to check
 *
 * @retval cAtTrue Sub-channel is equipped
 * @retval cAtFalse Sub-channel is unequipped
 */
eBool AtPwCepIsEquipped(AtPwCep self, AtChannel subChannel)
    {
    if (self)
        return mMethodsGet(self)->IsEquipped(self, subChannel);

    return cAtFalse;
    }

/**
 * Get iterator on equipped channels
 *
 * @param self This pseudowire
 * @return Equipped channel Iterator
 */
AtIterator AtPwCepEquippedChannelsIteratorCreate(AtPwCep self)
    {
    if (self)
        return AtListIteratorCreate(self->equippedChannels);

    return NULL;
    }

/**
 * Get number of equipped channels
 *
 * @param self This pseudowire
 * @return Number of equipped channels
 */
uint8 AtPwCepNumEquippedChannelsGet(AtPwCep self)
    {
    if (self == NULL)
        return 0;

    return (uint8)AtListLengthGet(self->equippedChannels);
    }

/**
 * Enable/disable EBM field in Control Word
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCepCwEbmEnable(AtPwCep self, eBool enable)
    {
    mNumericalAttributeSet(CwEbmEnable, enable);
    }

/**
 * Check if EBM field in Control Word is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue EBM is enabled
 * @retval cAtFalse EBM is disabled
 */
eBool AtPwCepCwEbmIsEnabled(AtPwCep self)
    {
    mAttributeGet(CwEbmIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable EPAR
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCepEparEnable(AtPwCep self, eBool enable)
    {
    mNumericalAttributeSet(EparEnable, enable);
    }

/**
 * Check if EPAR is enabled
 *
 * @param self This pseudowire
 *
 * @retval cAtTrue if EPAR is enabled
 * @retval cAtFalse EBM is disabled
 */
eBool AtPwCepEparIsEnabled(AtPwCep self)
    {
    mAttributeGet(EparIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable automatically transmitting N-Bit
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCepCwAutoNBitEnable(AtPwCep self, eBool enable)
    {
    mNumericalAttributeSet(CwAutoNBitEnable, enable);
    }

/**
 * Check if transmitting N-bit is enabled or not
 *
 * @param self This PW
 *
 * @retval cAtTrue - Auto N-Bit is enabled
 * @retval cAtFalse - Auto N-Bit is disabled
 */
eBool AtPwCepCwAutoNBitIsEnabled(AtPwCep self)
    {
    mAttributeGet(CwAutoNBitIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable automatically transmitting P-Bit
 *
 * @param self This pseudowire
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePwRet AtPwCepCwAutoPBitEnable(AtPwCep self, eBool enable)
    {
    mNumericalAttributeSet(CwAutoPBitEnable, enable);
    }

/**
 * Check if transmitting P-bit is enabled or not
 *
 * @param self This PW
 *
 * @retval cAtTrue - Auto P-Bit is enabled
 * @retval cAtFalse - Auto P-Bit is disabled
 */
eBool AtPwCepCwAutoPBitIsEnabled(AtPwCep self)
    {
    mAttributeGet(CwAutoPBitIsEnabled, eBool, cAtFalse);
    }

/**
 * Get CEP mode to check if it is basic or fractional
 *
 * @param self This PW
 *
 * @return @ref eAtPwCepMode "CEP modes"
 */
eAtPwCepMode AtPwCepModeGet(AtPwCep self)
    {
    mAttributeGet(ModeGet, eAtPwCepMode, cAtPwCepModeUnknown);
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : AtPwKbyteChannel.c
 *
 * Created Date: Jun 8, 2018
 *
 * Description : Abstract PW Kbyte channel.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPwKbyteChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtPwKbyteChannel)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPwKbyteChannelMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "kbyte_pw_channel";
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtPwKbyteChannel channel = (AtPwKbyteChannel)self;

    ret |= AtPwKbyteChannelValidationEnable(channel, cAtTrue);
    ret |= AtPwKbyteChannelOverrideEnable(channel, cAtFalse);

    /* Disable all interrupt mask */
    ret |= AtChannelInterruptMaskSet(self, AtChannelSupportedInterruptMasks(self), 0);

    return ret;
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtDevice dev = AtChannelDeviceGet((AtChannel)self);

    AtSprintf(description, "%s%s.%d", AtDeviceIdToString(dev), AtChannelTypeString((AtChannel)self), AtChannelIdGet((AtChannel)self));
    return description;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPwKbyteChannel object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(pw);
    mEncodeObjectDescription(line);
    }

static eAtRet SdhLineSet(AtPwKbyteChannel self, AtSdhLine line)
    {
    self->line = line;
    return cAtOk;
    }

static eAtRet ValidationEnable(AtPwKbyteChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool ValidationIsEnabled(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AlarmSuppressionEnable(AtPwKbyteChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool AlarmSuppressionIsEnabled(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet OverrideEnable(AtPwKbyteChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool OverrideIsEnabled(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxK1Set(AtPwKbyteChannel self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint8 TxK1Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet TxK2Set(AtPwKbyteChannel self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint8 TxK2Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet TxEK1Set(AtPwKbyteChannel self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint8 TxEK1Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet TxEK2Set(AtPwKbyteChannel self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint8 TxEK2Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 RxK1Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 RxK2Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 RxEK1Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 RxEK2Get(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet ExpectedLabelSet(AtPwKbyteChannel self, uint16 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint16 ExpectedLabelGet(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet TxLabelSet(AtPwKbyteChannel self, uint16 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint16 TxLabelGet(AtPwKbyteChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(AtChannel self)
    {
    AtPwKbyteChannel channel = (AtPwKbyteChannel)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, SdhLineSet);
        mMethodOverride(m_methods, ValidationEnable);
        mMethodOverride(m_methods, ValidationIsEnabled);
        mMethodOverride(m_methods, AlarmSuppressionEnable);
        mMethodOverride(m_methods, AlarmSuppressionIsEnabled);
        mMethodOverride(m_methods, OverrideEnable);
        mMethodOverride(m_methods, OverrideIsEnabled);
        mMethodOverride(m_methods, TxK1Set);
        mMethodOverride(m_methods, TxK1Get);
        mMethodOverride(m_methods, TxK2Set);
        mMethodOverride(m_methods, TxK2Get);
        mMethodOverride(m_methods, TxEK1Set);
        mMethodOverride(m_methods, TxEK1Get);
        mMethodOverride(m_methods, TxEK2Set);
        mMethodOverride(m_methods, TxEK2Get);
        mMethodOverride(m_methods, RxK1Get);
        mMethodOverride(m_methods, RxK2Get);
        mMethodOverride(m_methods, RxEK1Get);
        mMethodOverride(m_methods, RxEK2Get);
        mMethodOverride(m_methods, ExpectedLabelSet);
        mMethodOverride(m_methods, ExpectedLabelGet);
        mMethodOverride(m_methods, TxLabelSet);
        mMethodOverride(m_methods, TxLabelGet);
        }

    mMethodsSet(channel, &m_methods);
    }

static void OverrideAtChannel(AtChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(self, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtChannel self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwKbyteChannel);
    }

AtPwKbyteChannel AtPwKbyteChannelObjectInit(AtPwKbyteChannel self, uint32 channelId, AtPw pw, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtChannelObjectInit((AtChannel)self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((AtChannel)self);
    Override((AtChannel)self);
    m_methodsInit = 1;
    self->pw = pw;

    return self;
    }

AtPw AtPwKbyteChannelKbytePwGet(AtPwKbyteChannel self)
    {
    return (self) ? self->pw : NULL;
    }

AtSdhLine AtPwKbyteChannelSdhLineGet(AtPwKbyteChannel self)
    {
    return (self) ? self->line : NULL;
    }

eAtRet AtPwKbyteChannelSdhLineSet(AtPwKbyteChannel self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->SdhLineSet(self, line);
    return cAtErrorNullPointer;
    }

eAtRet AtPwKbyteChannelValidationEnable(AtPwKbyteChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->ValidationEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool AtPwKbyteChannelValidationIsEnabled(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->ValidationIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtPwKbyteChannelAlarmSuppressionEnable(AtPwKbyteChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->AlarmSuppressionEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool AtPwKbyteChannelAlarmSuppressionIsEnabled(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->AlarmSuppressionIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtPwKbyteChannelOverrideEnable(AtPwKbyteChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->OverrideEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool AtPwKbyteChannelOverrideIsEnabled(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->OverrideIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtPwKbyteChannelTxK1Set(AtPwKbyteChannel self, uint8 value)
    {
    if (self)
        return mMethodsGet(self)->TxK1Set(self, value);
    return cAtErrorNullPointer;
    }

uint8 AtPwKbyteChannelTxK1Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->TxK1Get(self);
    return 0;
    }

eAtRet AtPwKbyteChannelTxK2Set(AtPwKbyteChannel self, uint8 value)
    {
    if (self)
        return mMethodsGet(self)->TxK2Set(self, value);
    return cAtErrorNullPointer;
    }

uint8 AtPwKbyteChannelTxK2Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->TxK2Get(self);
    return 0;
    }

eAtRet AtPwKbyteChannelTxEK1Set(AtPwKbyteChannel self, uint8 value)
    {
    if (self)
        return mMethodsGet(self)->TxEK1Set(self, value);
    return cAtErrorNullPointer;
    }

uint8 AtPwKbyteChannelTxEK1Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->TxEK1Get(self);
    return 0;
    }

eAtRet AtPwKbyteChannelTxEK2Set(AtPwKbyteChannel self, uint8 value)
    {
    if (self)
        return mMethodsGet(self)->TxEK2Set(self, value);
    return cAtErrorNullPointer;
    }

uint8 AtPwKbyteChannelTxEK2Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->TxEK2Get(self);
    return 0;
    }

uint8 AtPwKbyteChannelRxK1Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->RxK1Get(self);
    return 0;
    }

uint8 AtPwKbyteChannelRxK2Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->RxK2Get(self);
    return 0;
    }

uint8 AtPwKbyteChannelRxEK1Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->RxEK1Get(self);
    return 0;
    }

uint8 AtPwKbyteChannelRxEK2Get(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->RxEK2Get(self);
    return 0;
    }

eAtRet AtPwKbyteChannelExpectedLabelSet(AtPwKbyteChannel self, uint16 value)
    {
    if (self)
        return mMethodsGet(self)->ExpectedLabelSet(self, value);
    return cAtErrorNullPointer;
    }

uint16 AtPwKbyteChannelExpectedLabelGet(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->ExpectedLabelGet(self);
    return cInvalidUint16;
    }

eAtRet AtPwKbyteChannelTxLabelSet(AtPwKbyteChannel self, uint16 value)
    {
    if (self)
        return mMethodsGet(self)->TxLabelSet(self, value);
    return cAtErrorNullPointer;
    }

uint16 AtPwKbyteChannelTxLabelGet(AtPwKbyteChannel self)
    {
    if (self)
        return mMethodsGet(self)->TxLabelGet(self);
    return cInvalidUint16;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : AtPowerSupplySensorInternal.h
 * 
 * Created Date: Oct 3, 2016
 *
 * Description : AtPowerSupplySensorInternal declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPOWERSUPPLYSENSORINTERNAL_H_
#define _ATPOWERSUPPLYSENSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSensorInternal.h"
#include "AtPowerSupplySensor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPowerSupplySensorMethods
    {
    uint32 (*VoltageGet)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);
    eAtRet (*AlarmSetThresholdSet)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage);
    uint32 (*AlarmSetThresholdGet)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);
    eAtRet (*AlarmClearThresholdSet)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage);
    uint32 (*AlarmClearThresholdGet)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);
    uint32 (*MinRecordedVoltageGet)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);
    uint32 (*MaxRecordedVoltageGet)(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);
    }tAtPowerSupplySensorMethods;

typedef struct tAtPowerSupplySensor
    {
    tAtSensor super;
    const tAtPowerSupplySensorMethods *methods;
    }tAtPowerSupplySensor;

/*--------------------------- Forward declarations ---------------------------*/
AtPowerSupplySensor AtPowerSupplySensorObjectInit(AtPowerSupplySensor self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATPOWERSUPPLYSENSORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtSerdesManager.c
 *
 * Created Date: Jun 17, 2016
 *
 * Description : Serdes manager implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSerdesControllerInternal.h"
#include "AtSerdesManagerInternal.h"
#include "AtSerdesControllerInternal.h"
#include "../man/AtDriverInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) (AtSerdesManager)self

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSerdesManagerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSerdesControllers(AtSerdesManager self)
    {
    AtUnused(self);
    /* Sub class must do */
    return 0;
    }

static eBool SerdesIdIsValid(AtSerdesManager self, uint32 serdesId)
    {
    return (eBool)((serdesId < mMethodsGet(self)->NumSerdesControllers(self)) ? cAtTrue : cAtFalse);
    }

static AtSerdesController SerdesControllerGet(AtSerdesManager self, uint32 serdesId)
    {
    if ((self->allSerdes == NULL) || (!SerdesIdIsValid(self, serdesId)))
        return NULL;

    return self->allSerdes[serdesId];
    }

static AtSerdesController SerdesControllerObjectCreate(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    /* Sub class must do */
    return NULL;
    }

static void AllSerdesControllersDelete(AtSerdesManager self, uint32 numSerdes)
    {
    uint32 serdes_i;
    if (self->allSerdes == NULL)
        return;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        if (self->allSerdes[serdes_i] == NULL)
            continue;

        AtObjectDelete((AtObject)self->allSerdes[serdes_i]);
        self->allSerdes[serdes_i] = NULL;
        }

    AtOsalMemFree(self->allSerdes);
    self->allSerdes = NULL;
    }

static eAtRet Setup(AtSerdesManager self)
    {
    uint32 numSerdes = mMethodsGet(self)->NumSerdesControllers(self);
    uint32 memSize = sizeof(AtSerdesController) * numSerdes;
    uint32 serdes_i;

    if (numSerdes == 0)
        return cAtOk;

    self->allSerdes = AtOsalMemAlloc(memSize);
    if (self->allSerdes == NULL)
        return cAtErrorRsrcNoAvail;

    AtOsalMemInit(self->allSerdes, 0, memSize);
    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdesController = mMethodsGet(self)->SerdesControllerObjectCreate(self, serdes_i);
        if (serdesController == NULL)
            {
            AllSerdesControllersDelete(self, serdes_i);
            return cAtErrorRsrcNoAvail;
            }

        AtSerdesControllerManagerSet(serdesController, self);
        self->allSerdes[serdes_i] = serdesController;
        }

    return cAtOk;
    }

static eAtRet SerdesIterateWithHandler(AtSerdesManager self, eAtRet (*Handler)(AtSerdesController self))
    {
    uint32 numSerdes = mMethodsGet(self)->NumSerdesControllers(self);
    uint32 serdes_i;
    eAtRet ret = cAtOk;

    if (numSerdes == 0)
        return cAtOk;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdesController = self->allSerdes[serdes_i];
        if (AtSerdesControllerIsControllable(serdesController))
            ret |= Handler(serdesController);
        }

    return ret;
    }

static eAtRet Init(AtSerdesManager self)
    {
    return SerdesIterateWithHandler(self, AtSerdesControllerInit);
    }

static void Delete(AtObject self)
    {
    AtSerdesManager serdesManage = (AtSerdesManager)self;
    uint32 numSerdes = mMethodsGet(serdesManage)->NumSerdesControllers(serdesManage);

    AllSerdesControllersDelete(serdesManage, numSerdes);
    m_AtObjectMethods->Delete(self);
    }

static eBool CanAccessRegister(AtSerdesManager self, uint32 serdesId, uint32 address, eAtModule moduleId)
    {
    /* Concrete should know */
    AtUnused(self);
    AtUnused(serdesId);
    AtUnused(address);
    AtUnused(moduleId);
    return cAtTrue;
    }

static eBool SerdesIsControllable(AtSerdesManager self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtTrue;
    }

static eAtRet SerdesControllerAsyncInit(AtSerdesManager self)
    {
    return AtSerdesControllerAsyncInit(self->allSerdes[self->asyncInitState]);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSerdesManager object = (AtSerdesManager)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(allSerdes, AtSerdesManagerNumSerdesControllers(object));
    mEncodeNone(asyncInitState);
    mEncodeNone(device);
    }

static const char *ToString(AtObject self)
    {
    AtDevice dev = AtSerdesManagerDeviceGet((AtSerdesManager)self);
    static char str[64];

    AtSprintf(str, "%sserdes_manager", AtDeviceIdToString(dev));
    return str;
    }

static AtQuerier QuerierGet(AtSerdesManager self)
    {
    AtUnused(self);
    return AtSerdesManagerSharedQuerier();
    }

static tAtAsyncOperationFunc NextInitOp(AtObject self, uint32 state)
    {
    AtSerdesManager manager = (AtSerdesManager)self;
    uint32 numSerdes = mMethodsGet(manager)->NumSerdesControllers(manager);
    if (state >= numSerdes)
        return NULL;

    return (tAtAsyncOperationFunc)SerdesControllerAsyncInit;
    }

static eAtRet AsyncInit(AtSerdesManager self)
    {
    return AtDeviceObjectAsyncProcess(AtSerdesManagerDeviceGet(self), (AtObject)self, &self->asyncInitState, NextInitOp);
    }

static eAtRet Reset(AtSerdesManager self)
    {
    if (AtDeviceSerdesResetIsSupportted(self->device))
        return SerdesIterateWithHandler(self, AtSerdesControllerReset);
    return cAtOk;
    }

static void OverrideAtObject(AtSerdesManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtSerdesManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NumSerdesControllers);
        mMethodOverride(m_methods, SerdesControllerGet);
        mMethodOverride(m_methods, SerdesControllerObjectCreate);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, CanAccessRegister);
        mMethodOverride(m_methods, SerdesIsControllable);
        mMethodOverride(m_methods, QuerierGet);
        mMethodOverride(m_methods, AsyncInit);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSerdesManager);
    }

AtSerdesManager AtSerdesManagerObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    self->device = device;
    return self;
    }

eAtRet AtSerdesManagerSetup(AtSerdesManager self)
    {
    mNoParamAttributeSet(Setup);
    }

eAtRet AtSerdesManagerInit(AtSerdesManager self)
    {
    mNoParamAttributeSet(Init);
    }

eBool AtSerdesManagerCanAccessRegister(AtSerdesManager self, uint32 serdesId, uint32 address, eAtModule moduleId)
    {
    if (self)
        return mMethodsGet(self)->CanAccessRegister(self, serdesId, address, moduleId);
    return cAtFalse;
    }

eBool AtSerdesManagerSerdesIsControllable(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return mMethodsGet(self)->SerdesIsControllable(self, serdesId);
    return cAtFalse;
    }

eAtRet AtSerdesManagerAsyncInit(AtSerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->AsyncInit(self);

    return cAtErrorNullPointer;
    }

AtQuerier AtSerdesManagerQuerierGet(AtSerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->QuerierGet(self);
    return NULL;
    }

eAtRet AtSerdesManagerReset(AtSerdesManager self)
    {
    if (self)
        return Reset(self);
    return cAtErrorNullPointer;
    }

/**
 * @addtogroup AtSerdesManager
 * @{
 */

/**
 * Get number of SERDES controller
 *
 * @param self This manager
 * @return Number of SERDES controller
 */
uint32 AtSerdesManagerNumSerdesControllers(AtSerdesManager self)
    {
    mAttributeGet(NumSerdesControllers, uint32, 0);
    }

/**
 * Get SERDES controller by controller ID
 *
 * @param self This manager
 * @param serdesId Controller ID
 * @return SERDES controller
 */
AtSerdesController AtSerdesManagerSerdesControllerGet(AtSerdesManager self, uint32 serdesId)
    {
    mOneParamAttributeGet(SerdesControllerGet, serdesId, AtSerdesController, NULL);
    }
/**
 * Get device that SERDES manager belongs to
 *
 * @param self This manager
 *
 * @return Device that SERDES manager belongs to
 */
AtDevice AtSerdesManagerDeviceGet(AtSerdesManager self)
    {
    if (self)
        return self->device;
    return NULL;
    }

/**
 * @}
 */

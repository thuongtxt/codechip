/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanLane.c
 *
 * Created Date: Dec 24, 2014
 *
 * Description : Eye scan lane
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h" /* For read/write */
#include "AtEyeScanLaneInternal.h"
#include "AtEyeScanControllerInternal.h"
#include "AtEyeScanDrpReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxVerticalOffset 127
#define cStepPrescale 3
#define cMinErrorCount 3
#define cStateTimeoutMs 1000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtEyeScanLane)self)

#define mFewErrors(errorCount)  (errorCount < (10 * cMinErrorCount))
#define mManyErrors(errorCount) (errorCount > (1000 * cMinErrorCount))
#define mDrpWrite(self, address, value) DrpWrite(self, address, (uint16)value)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eNextStateAction
    {
    cNextStateActionNone,
    cNextStateActionNextState
    }eNextStateAction;

typedef enum eAtEyeScanState
    {
    cAtEyeScanStateWait  = 0,
    cAtEyeScanStateReset = 16,
    cAtEyeScanStateSetup = 32,
    cAtEyeScanStateCount = 48
    }eAtEyeScanState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEyeScanLaneMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DrpWrite(AtEyeScanLane self, uint16 drpAddress, uint16 value)
    {
    AtEyeScanControllerDrpWrite(self->controller, self, drpAddress, value);
    }

static uint16 DrpRead(AtEyeScanLane self, uint16 drpAddress)
    {
    return AtEyeScanControllerDrpRead(self->controller, self, drpAddress);
    }

static void InitializeEsQualMasks(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_QUAL_MASK0_REG(controller), 0xFFFF);
    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_QUAL_MASK1_REG(controller), 0xFFFF);
    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_QUAL_MASK2_REG(controller), 0xFFFF);
    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_QUAL_MASK3_REG(controller), 0xFFFF);
    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_QUAL_MASK4_REG(controller), 0xFFFF);
    }

static void InitializeEsSDataMasks(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    switch (AtEyeScanLaneDataWidthGet(self))
        {
        case 40:
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK0_REG(controller), 0x0000);
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK1_REG(controller), 0x0000);
            break;

        case 32:
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK0_REG(controller), 0x00FF);
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK1_REG(controller), 0x0000);
            break;

        case 20:
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK0_REG(controller), 0xFFFF);
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK1_REG(controller), 0x000F);
            break;

        case 16:
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK0_REG(controller), 0xFFFF);
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK1_REG(controller), 0x00FF);
            break;

        default:
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK0_REG(controller), 0xFFFF);
            AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK1_REG(controller), 0xFFFF);
            break;
        }

    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK2_REG(controller), 0xFF00);
    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK3_REG(controller), 0xFFFF);
    AtEyeScanLaneDrpWrite(self, mMethodsGet(controller)->ES_SDATA_MASK4_REG(controller), 0xFFFF);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanLane);
    }

static void EyeScanDrpInit(AtEyeScanLane self)
    {
    AtEyeScanLaneInitializeEsSDataMasks(self);
    AtEyeScanLanePrescaleSet(self, 0x0);
    AtEyeScanLaneInitializeEsQualMasks(self);
    }

static uint32 MaxPixelCount(AtEyeScanLane self)
    {
    AtUnused(self);
    return 253;
    }

static uint32 EyeScanDataMemorySize(AtEyeScanLane self)
    {
    return sizeof(tAtEyeScanLanePixel) * MaxPixelCount(self);
    }

static tAtEyeScanLanePixel *CreateEyeScanData(AtEyeScanLane self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = EyeScanDataMemorySize(self);
    tAtEyeScanLanePixel *scanData = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (scanData)
        mMethodsGet(osal)->MemInit(osal, scanData, 0, memorySize);
    return scanData;
    }

static tAtEyeScanLanePixel *EyeScanData(AtEyeScanLane self)
    {
    if (self->pixels == NULL)
        self->pixels = CreateEyeScanData(self);
    return self->pixels;
    }

static void ResetPixelCount(AtEyeScanLane self)
    {
    self->pixelCount = 0;
    }

static void EyeScanInternalStateReset(AtEyeScanLane self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    self->state = cAtEyeScanStateReset;
    mMethodsGet(osal)->MemInit(osal, EyeScanData(self), 0, EyeScanDataMemorySize(self));
    ResetPixelCount(self);
    }

static uint16 EyeScanControlState(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_CONTROL_STATUS_REG(controller);
    uint32 fieldMask  = mMethodsGet(controller)->ES_CONTROL_CURRENT_STATE_MASK(controller);
    uint32 fieldShift = mMethodsGet(controller)->ES_CONTROL_CURRENT_STATE_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    return (uint16) mRegField(regVal, field);
    }

static uint16 MaxVerticalOffsetCalculate(AtEyeScanLane self)
    {
    uint32 maxVertOffset = 0;

    /* Find largest multiple of vert_step_size that is less than 127 */
    while (maxVertOffset < cMaxVerticalOffset)
        maxVertOffset = maxVertOffset + self->vertStepSize;
    maxVertOffset = maxVertOffset - self->vertStepSize;

    return (uint16)maxVertOffset;
    }

static eNextStateAction WaitStateHandle(AtEyeScanLane self)
    {
    /* Does nothing but return. Waits for RESET state to be written externally */
    AtUnused(self);
    return cNextStateActionNone;
    }

static eBool CanStartEyeScan(AtEyeScanLane self)
    {
    /* Eye Scan SM must be in WAIT state with RUN & ARM de-asserted */
    return (EyeScanControlState(self) == 0) ? cAtTrue : cAtFalse;
    }

static tAtEyeScanLanePixel *CurrentPixel(AtEyeScanLane self)
    {
    return &(self->currentPixel);
    }

static uint16 NextPixel(AtEyeScanLane self)
    {
    self->pixelCount = (uint16)(self->pixelCount + 1);
    return self->pixelCount;
    }

static eNextStateAction ResetStateHandle(AtEyeScanLane self)
    {
    tAtEyeScanLanePixel *currentPixel;

    if (!CanStartEyeScan(self))
        return cNextStateActionNone;

    currentPixel = CurrentPixel(self);
    currentPixel->utSign = (self->equalizerMode == cAtEyeScanEqualizerModeDfe) ? 1 : 0;

    /* Starts in center of eye */
    currentPixel->horzOffset = 0;
    currentPixel->vertOffset = (int16)(-self->maxVertOffset - self->vertStepSize); /* Incremented to -max_vert_offset in SETUP state */

    /* Gear Shifting start */
    AtEyeScanLanePrescaleSet(self, currentPixel->prescale);

    /* Gear Shifting end */
    self->state = cAtEyeScanStateSetup;

    return cNextStateActionNextState;
    }

static eAtRet EsControlSet(AtEyeScanLane self, uint16 value)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_CONTROL_REG(controller);
    uint32 controlMask  = mMethodsGet(controller)->ES_CONTROL_MASK(controller);
    uint32 controlShift = mMethodsGet(controller)->ES_CONTROL_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    mRegFieldSet(regVal, control, value);
    mDrpWrite(self, regAddress, regVal);
    return cAtOk;
    }

static eAtRet Move2Run(AtEyeScanLane self)
    {
    return EsControlSet(self, 1);
    }

static void LatchTime(AtEyeScanLane self)
    {
    AtOsal osal;

    if (!self->debugEnabled)
        return;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->CurTimeGet(osal, &(self->latchTime));
    }

static uint32 ElapseTimeInUs(AtEyeScanLane self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    tAtOsalCurTime curTime;

    if (!self->debugEnabled)
        return 0;

    mMethodsGet(osal)->CurTimeGet(osal, &curTime);
    return ((((curTime.sec - self->latchTime.sec) * 1000000) + curTime.usec) - self->latchTime.usec);
    }

static eAtRet VerticalOffsetSet(AtEyeScanLane self, uint16 value)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    return AtEyeScanControllerVerticalOffsetSet(controller, self, value);
    }

static uint16 VerticalValueCalculate(AtEyeScanLane self, int16 vertOffset, int16 utSign)
    {
    uint32 vertValue;

    AtUnused(self);

    /* 256 = 2^8 ut_sign is bit[8] */
    vertValue = (uint32)((int32)AtStdAbs(AtStdSharedStdGet(), vertOffset) + (int32)(utSign * 256));

    /* 128=2^7 sign is bit [7] */
    if (vertOffset < 0)
        vertValue = vertValue + 128;

    return (uint16)vertValue;
    }

static eNextStateAction SetupStateHandle(AtEyeScanLane self)
    {
    uint8 maxUtSign = (uint8)(1 - self->equalizerMode);
    uint16 horzValue = 0;
    uint16 phaseUnification = 0;
    tAtEyeScanLanePixel *currentPixel = CurrentPixel(self);

    /* Advance vert_offset (-max to max) */
    currentPixel->vertOffset = (int16)(currentPixel->vertOffset + self->vertStepSize);

    /* If done scanning pixel column */
    if (currentPixel->vertOffset > self->maxVertOffset)
        {
        /* Reset to -max_vert_offset */
        currentPixel->vertOffset = (int16)(-self->maxVertOffset);

        /* Advance(decrement) ut_sign */
        currentPixel->utSign = (int16)(currentPixel->utSign - 1);

        /* If completed 1 & 0 for DFE (or just 0 for LPM), reset to max */
        if(currentPixel->utSign < 0)
            {
            currentPixel->utSign = maxUtSign;

            /* Advance horz_offset */
            if(currentPixel->horzOffset < 0)
                {
                currentPixel->horzOffset = (int16)(-currentPixel->horzOffset);
                }
            else
                {
                /* Increments thru sequence: 0, -step, step, -2*step, 2*step, -3*step, 3*step, ... */
                currentPixel->horzOffset = (int16)(-(self->horzStepSize + currentPixel->horzOffset));
                }

            /* If incremented thru all pixel columns, reset to 0 and return to WAIT state, ending statistical eye acquisition */
            if (currentPixel->horzOffset < -AtEyeScanLaneMaxHorizontalOffsetGet(self))
                {
                currentPixel->horzOffset = 0;
                self->state = cAtEyeScanStateWait;
                return cNextStateActionNone;
                }
            }
        }

    /* Convert to 'phase_unification' + two's complement */
    if (currentPixel->horzOffset < 0)
        {
        horzValue = (uint16)(currentPixel->horzOffset + 2048); /* Generate 11-bit 2's complement number */
        phaseUnification = 2048; /* 12th bit i.e. bit[11] */
        }
    else
        {
        horzValue = (uint16)currentPixel->horzOffset;
        phaseUnification = 0;
        }
    horzValue  = (uint16)(phaseUnification + horzValue);

    /* Write horizontal offset */
    AtEyeScanLaneHorizontalOffsetSet(self, horzValue);
    AtEyeScanLaneVerticalOffsetSet(self, VerticalValueCalculate(self, currentPixel->vertOffset, currentPixel->utSign));
    Move2Run(self);

    self->state = cAtEyeScanStateCount;
    LatchTime(self);

    return cNextStateActionNone;
    }

static AtOsalMutex Locker(AtEyeScanLane self)
    {
    if (self->locker == NULL)
        self->locker = AtOsalMutexCreate();
    return self->locker;
    }

static void NotifyDidScanPixels(AtEyeScanLane self)
    {
    /* TODO: Need to consider how we should do with this */
    AtUnused(self);
    }

static tAtEyeScanLaneEventListener *EventListenerCreate(void* userData, const tAtEyeScanLaneListener *callbacks)
    {
    uint32 memorySize = sizeof(tAtEyeScanLaneEventListener);
    tAtEyeScanLaneEventListener *newListener = AtOsalMemAlloc((uint32)memorySize);
    if (newListener == NULL)
        return NULL;

    memorySize = sizeof(tAtEyeScanLaneListener);
    newListener->callbacks = AtOsalMemAlloc(memorySize);
    if (newListener->callbacks == NULL)
        {
        AtOsalMemFree(newListener);
        return NULL;
        }

    AtOsalMemCpy(newListener->callbacks, callbacks, memorySize);
    newListener->userData  = userData;

    return newListener;
    }

static eBool CallbacksAreSame(const tAtEyeScanLaneListener *callback1, const tAtEyeScanLaneListener *callback2)
    {
    return (AtOsalMemCmp(callback1, callback2, sizeof(tAtEyeScanLaneListener)) == 0) ? cAtTrue : cAtFalse;
    }

static tAtEyeScanLaneEventListener *EventListenerFind(AtEyeScanLane self, void* userData, const tAtEyeScanLaneListener *callbacks, uint32 *listenerIndex)
    {
    uint32 listener_i;

    if (self->listeners == NULL)
        return NULL;

    for (listener_i = 0; listener_i < AtListLengthGet(self->listeners); listener_i++)
        {
        tAtEyeScanLaneEventListener *aListener = (tAtEyeScanLaneEventListener *)AtListObjectGet(self->listeners, listener_i);

        if ((aListener->userData == userData) && CallbacksAreSame(aListener->callbacks, callbacks))
            {
            if (listenerIndex)
                *listenerIndex = listener_i;
            return aListener;
            }
        }

    return NULL;
    }

static eAtRet ListenerAdd(AtEyeScanLane self, void *userData, const tAtEyeScanLaneListener *callback)
    {
    eAtRet ret;

    if (callback == NULL)
        return cAtErrorNullPointer;

    AtEyeScanLaneLock(self);

    /* Already add, ignore */
    if (EventListenerFind(self, userData, callback, NULL))
        ret = cAtOk;
    else
        ret = AtListObjectAdd(self->listeners, (AtObject)EventListenerCreate(userData, callback));

    AtEyeScanLaneUnLock(self);

    return ret;
    }

static void EventListenerDelete(tAtEyeScanLaneEventListener* eventListener)
    {
    if (eventListener->callbacks)
        AtOsalMemFree((void*)(eventListener->callbacks));

    AtOsalMemFree(eventListener);
    }

static eAtRet ListenerRemove(AtEyeScanLane self, void *userData, const tAtEyeScanLaneListener *callback)
    {
    uint32 listenerIndex;

    if (callback == NULL)
        return cAtErrorNullPointer;

    AtEyeScanLaneLock(self);
    if (EventListenerFind(self, userData, callback, &listenerIndex))
        {
        tAtEyeScanLaneEventListener* wrapListener = (tAtEyeScanLaneEventListener *)AtListObjectRemoveAtIndex((AtList)(mThis(self)->listeners), listenerIndex);
        EventListenerDelete(wrapListener);
        }

    AtEyeScanLaneUnLock(self);
    return cAtOk;
    }

static void DeleteAllListeners(AtObject self)
    {
    AtEyeScanLane eyeScanLane = mThis(self);
    AtIterator iterator;

    if (eyeScanLane->listeners == NULL)
        return;

    /* Delete all listeners in list */
    iterator = AtListIteratorCreate(eyeScanLane->listeners);
    if (iterator)
        {
        tAtEyeScanLaneEventListener *aListener;
        while((aListener = (tAtEyeScanLaneEventListener *)AtIteratorNext(iterator)) != NULL)
            EventListenerDelete(aListener);
        }
    AtObjectDelete((AtObject)iterator);

    /* Delete event listener list */
    AtObjectDelete((AtObject)(eyeScanLane->listeners));
    mThis(self)->listeners = NULL;
    }

static void NotifyDidFinish(AtEyeScanLane self)
    {
    uint32 i;

    if (self->listeners == NULL)
        return;

    AtEyeScanLaneLock(self);

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tAtEyeScanLaneEventListener *listener = (tAtEyeScanLaneEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks->EyeScanDidFinish)
            listener->callbacks->EyeScanDidFinish(self, listener->userData);
        }

    AtEyeScanLaneUnLock(self);
    }

static void NotifyEyeScanWillStart(AtEyeScanLane self)
    {
    uint32 i;

    if (self->listeners == NULL)
        return;

    AtEyeScanLaneLock(self);

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tAtEyeScanLaneEventListener *listener = (tAtEyeScanLaneEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks->EyeScanWillStart)
            listener->callbacks->EyeScanWillStart(self, listener->userData);
        }

    AtEyeScanLaneUnLock(self);
    }

static eBool IsSimulated(AtEyeScanLane self)
    {
    AtEyeScanController eyeScanController = AtEyeScanLaneControllerGet(self);
    AtSerdesController controller = AtEyeScanControllerSerdesControllerGet(eyeScanController);
    AtDevice device = AtSerdesControllerDeviceGet(controller);
    return AtDeviceIsSimulated(device);
    }

static eAtRet StateWait(AtEyeScanLane self, uint32 expectedState, uint32 timeoutMs)
    {
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0;
    eBool isSimulated = IsSimulated(self);

    AtOsalCurTimeGet(&startTime);
    while (elapseTime < timeoutMs)
        {
        if (EyeScanControlState(self) == expectedState)
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);

        if (isSimulated)
            return cAtOk;
        }

    return cAtErrorEyeScanTimeout;
    }

static eAtRet Move2Wait(AtEyeScanLane self)
    {
    eAtRet ret;

    EsControlSet(self, 0);

    ret = StateWait(self, DRP_ADDR_ES_CONTROL_CURRENT_STATE_WAIT, cStateTimeoutMs);
    if (ret != cAtOk)
        AtEyeScanLaneLog(self, cAtLogLevelCritical, AtSourceLocation, "Move to wait state fail with ret = %s\r\n", AtRet2String(ret));

    return ret;
    }

static uint16 NextPrescaleCalculate(AtEyeScanLane self, uint16 prescale, uint16 errorCount)
    {
    int32 nextPrescale  = 0;

    if ((prescale < self->maxPrescale) && mFewErrors(errorCount))
        {
        nextPrescale = (int16)(prescale + cStepPrescale);
        if (nextPrescale > self->maxPrescale)
            nextPrescale = self->maxPrescale;
        return (uint16)nextPrescale;
        }

    if ((prescale > 0) && (errorCount > (10 * cMinErrorCount)))
        {
        if (mManyErrors(errorCount))
            nextPrescale = prescale - (2 * cStepPrescale);
        else
            nextPrescale = prescale - cStepPrescale;

        if (nextPrescale < 0)
            nextPrescale = 0;

        return (uint16)nextPrescale;
        }

    return prescale;
    }

static void NextPrescaleSet(AtEyeScanLane self, uint16 prescale, uint16 errorCount)
    {
    if (!mFewErrors(errorCount) && !mManyErrors(errorCount))
        return;

    AtEyeScanLanePrescaleSet(self, NextPrescaleCalculate(self, prescale, errorCount));
    }

static eNextStateAction CountStateHandle(AtEyeScanLane self)
    {
    uint16 errorCount    = 0;
    uint16 sampleCount   = 0;
    uint16 prescale      = 0;
    tAtEyeScanLanePixel *newPixel, *currentPixel;
    AtOsal osal;

    /* Not finished yet */
    if (!AtEyeScanLaneDrpEyeScanDone(self))
        return cNextStateActionNone;

    self->eyeScanDoneTimeUs = ElapseTimeInUs(self);

    /* Move to wait state and read current scanning result */
    Move2Wait(self);
    errorCount  = AtEyeScanLaneErrorCountGet(self);
    sampleCount = AtEyeScanLaneSampleCountGet(self);
    prescale    = AtEyeScanLanePrescaleGet(self);

    /* Adjust prescale if too few or many errors, in case of too few errors,
     * need restart scanning */
    NextPrescaleSet(self, prescale, errorCount);
    if ((errorCount < cMinErrorCount) && (prescale < self->maxPrescale))
        {
        Move2Run(self);
        self->currentEyeScanRestartTimes = self->currentEyeScanRestartTimes + 1;
        return cNextStateActionNone;
        }

    /* Update debug information */
    self->lastEyeScanRestartTimes    = self->currentEyeScanRestartTimes;
    self->currentEyeScanRestartTimes = 0;

    /* Save result */
    osal         = AtSharedDriverOsalGet();
    newPixel     = &(self->pixels[self->pixelCount]);
    currentPixel = CurrentPixel(self);
    mMethodsGet(osal)->MemCpy(osal, newPixel, currentPixel, sizeof(tAtEyeScanLanePixel));
    newPixel->errorCount  = errorCount;
    newPixel->sampleCount = sampleCount;
    newPixel->prescale    = (uint8)prescale;

    if (NextPixel(self) == MaxPixelCount(self))
        {
        NotifyDidScanPixels(self);
        ResetPixelCount(self);
        }

    self->state = cAtEyeScanStateSetup;

    return cNextStateActionNone;
    }

static void WillDeleteNotify(AtEyeScanLane self)
    {
    uint32 i;

    if (self->listeners == NULL)
        return;

    AtEyeScanLaneLock(self);

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tAtEyeScanLaneEventListener *listener = (tAtEyeScanLaneEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks->WillDelete)
            listener->callbacks->WillDelete(self, listener->userData);
        }

    AtEyeScanLaneUnLock(self);
    }

static void WillDelete(AtObject self)
    {
    WillDeleteNotify(mThis(self));
    }

static void Delete(AtObject self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    DeleteAllListeners(self);
    mMethodsGet(osal)->MemFree(osal, mThis(self)->pixels);
    mThis(self)->pixels = NULL;
    AtOsalMutexDestroy(mThis(self)->locker);
    mThis(self)->locker = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void EyeScanSetup(AtEyeScanLane self)
    {
    EyeScanInternalStateReset(self);
    AtEyeScanLaneEyeScanEnable(self, cAtTrue);
    EsControlSet(self, 0);
    EyeScanDrpInit(self);
    }

static eAtRet Acquire(AtEyeScanLane self)
    {
    if ((self->state == cAtEyeScanStateWait)  && (WaitStateHandle(self)  == cNextStateActionNone))
        return cAtOk;

    if ((self->state == cAtEyeScanStateReset) && (ResetStateHandle(self) == cNextStateActionNone))
        return cAtOk;

    if ((self->state == cAtEyeScanStateSetup) && (SetupStateHandle(self) == cNextStateActionNone))
        return cAtOk;

    if ((self->state == cAtEyeScanStateCount) && (CountStateHandle(self) == cNextStateActionNone))
        return cAtOk;

    return cAtOk;
    }

static const char *ToString(AtObject self)
    {
    AtEyeScanLane lane = mThis(self);
    AtEyeScanController controller = AtEyeScanLaneControllerGet(lane);
    AtChannel channel = AtEyeScanControllerPhysicalPort(controller);
    AtDevice dev = AtChannelDeviceGet(channel);
    static char str[128];

    if (!lane->pName)
        {
        AtSnprintf(lane->name,
                   sizeof(lane->name) - 1,
                   "%s.lane_%d",
                   AtObjectToString((AtObject)AtEyeScanLaneControllerGet(lane)),
                   AtEyeScanLaneIdGet(lane) + 1);

        lane->pName = lane->name;
        }
    AtSprintf(str, "%s%s", AtDeviceIdToString(dev), lane->pName);

    return str;
    }

static const char *CurrentStateString(AtEyeScanLane self)
    {
    switch (self->state)
        {
        case cAtEyeScanStateWait  : return "wait";
        case cAtEyeScanStateReset : return "reset";
        case cAtEyeScanStateSetup : return "setup";
        case cAtEyeScanStateCount : return "count";
        default                   : return "unknown";
        }
    }

static const char *BufferStatus(uint32 status)
    {
    if (status == 0) return "Normal";
    if (status == 1) return "Number of bytes in the buffer are less than CLK_COR_MIN_LAT";
    if (status == 2) return "Number of bytes in the buffer are greater than CLK_COR_MAX_LAT";
    if (status == 5) return "RX elastic buffer underflow";
    if (status == 6) return "RX elastic buffer overflow";
    return "unknown";
    }

static void ShowBufferStatus(uint32 value)
    {
    uint32 status = (value & ES_REG_SERDES_BUF_STATE_MASK) >> ES_REG_SERDES_BUF_STATE_SHIFT;
    const char *statusString = BufferStatus(status);
    AtPrintc(cSevNormal, "* RX buffer status: ");
    AtPrintc((status == 0) ? cSevInfo : cSevCritical, "%s\r\n", statusString);
    }

static void ShowLaneStatus(AtEyeScanLane self)
    {
    uint8 laneId = AtEyeScanLaneIdGet(self);
    AtChannel channel = AtEyeScanControllerPhysicalPort(AtEyeScanLaneControllerGet(self));
    eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(channel));
    uint32 regAddr = ES_REG_SERDES_LANE_STATUS(laneId);
    uint32 regVal = mChannelHwRead(channel, regAddr, moduleId);
    uint32 fieldVal;

    mFieldGet(regVal, ES_REG_SERDES_LANE_STATUS_FIELD_MASK(laneId), ES_REG_SERDES_LANE_STATUS_FIELD_SHIFT(laneId), uint32, &fieldVal);

    if ((fieldVal & ES_REG_SERDES_TX_RESET_DONE_MASK) == 0)
        AtPrintc(cSevCritical, "* TX reset has not finished\r\n");
    if ((fieldVal & ES_REG_SERDES_RX_RESET_DONE_MASK) == 0)
        AtPrintc(cSevCritical, "* RX reset has not finished\r\n");
    if ((fieldVal & ES_REG_SERDES_TX_PHASE_ALIGN_MASK) == 0)
        AtPrintc(cSevCritical, "* TX phase is not align\r\n");
    if ((fieldVal & ES_REG_SERDES_TX_PHY_INIT_DONE_MASK) == 0)
        AtPrintc(cSevCritical, "* TX PHY initializing has not finished\r\n");
    if ((fieldVal & ES_REG_SERDES_TX_DELAY_RESET_DONE_MASK) == 0)
        AtPrintc(cSevCritical, "* TX delay reset has not finished\r\n");
    if (fieldVal & ES_REG_SERDES_EYE_SCAN_DATA_ERROR_MASK)
        AtPrintc(cSevCritical, "* Eye-scan data error\r\n");
    if ((fieldVal & ES_REG_SERDES_RX_COMMA_DET_MASK) == 0)
        AtPrintc(cSevCritical, "* Comma has not been detected\r\n");

    ShowBufferStatus(fieldVal);

    if (fieldVal & ES_REG_SERDES_RX_DISP_ERROR_RXDATA_7_0_MASK)
        AtPrintc(cSevCritical, "* RXDATA[7:0]: disparity error\r\n");
    if (fieldVal & ES_REG_SERDES_RX_DISP_ERROR_RXDATA_15_8_MASK)
        AtPrintc(cSevCritical, "* RXDATA[15:8]: disparity error\r\n");

    if (fieldVal & ES_REG_SERDES_RX_NOT_INTABLE_RXDATA_7_0_MASK)
        AtPrintc(cSevCritical, "* RXDATA[7:0]: not intable");
    if (fieldVal & ES_REG_SERDES_RX_NOT_INTABLE_RXDATA_15_8_MASK)
        AtPrintc(cSevCritical, "* RXDATA[[15:8]: not intable");
    }

static eAtRet Debug(AtEyeScanLane self)
    {
    AtPrintf("* Scanning           : %s\r\n", self->scanning ? "yes" : "no");
    AtPrintf("* MaxHorzOffset      : %d\r\n", AtEyeScanLaneMaxHorizontalOffsetGet(self));
    AtPrintf("* MaxVertOffset      : %d\r\n", self->maxVertOffset);
    AtPrintf("* PixelCount         : %d\r\n", self->pixelCount);
    AtPrintf("* State              : %s\r\n", CurrentStateString(self));
    AtPrintf("* LastRestartTimes   : %d (times)\r\n", self->lastEyeScanRestartTimes);
    AtPrintf("* CurrentRestartTimes: %d (times)\r\n", self->currentEyeScanRestartTimes);
    AtPrintf("* HwEnabled          : %s\r\n", AtEyeScanLaneEyeScanIsEnabled(self) ? "enabled" : "disabled");

    /* Attributes that are only applicable if debug is enabled */
    if (self->debugEnabled)
        AtPrintf("* eyeScanDoneTimeUs  : %d.%d(ms)\r\n", (self->eyeScanDoneTimeUs / 1000), (self->eyeScanDoneTimeUs % 1000));

    mMethodsGet(self)->ShowLaneStatus(self);

    return cAtOk;
    }

static eBool MaxPrescaleIsInRange(AtEyeScanLane self, uint8 maxPrescale)
    {
    AtUnused(self);
    return maxPrescale < 32;
    }

static eBool ShouldAcquire(AtEyeScanLane self)
    {
    return (self->state == cAtEyeScanStateWait) ? cAtFalse : cAtTrue;
    }

static eBool FinishScanning(AtEyeScanLane self)
    {
    return (self->state == cAtEyeScanStateWait) ? cAtTrue : cAtFalse;
    }

static eBool NeedInitialize(AtEyeScanLane self)
    {
    uint16 regVal = AtEyeScanLaneDrpRead(self, DRP_ADDR_PMA_RSV2);
    return (regVal & DRP_ADDR_PMA_RSV2_ENABLE_MASK) ?  cAtFalse : cAtTrue;
    }

static eAtRet PmaSetup(AtEyeScanLane self)
    {
    uint32 regVal = AtEyeScanLaneDrpRead(self, DRP_ADDR_PMA_RSV2);
    mFieldIns(&regVal, DRP_ADDR_PMA_RSV2_ENABLE_MASK, DRP_ADDR_PMA_RSV2_ENABLE_SHIFT, 1);
    mDrpWrite(self, DRP_ADDR_PMA_RSV2, regVal);
    return cAtOk;
    }

static eAtRet Init(AtEyeScanLane self)
    {
    return mMethodsGet(self)->PmaSetup(self);
    }

static uint32 MeasuredWidth(AtEyeScanLane self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MeasuredSwing(AtEyeScanLane self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumHorizontalTaps(AtEyeScanLane self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ElapsedTimeMs(AtEyeScanLane self)
    {
    AtUnused(self);
    return 0;
    }

static eBool EyeScanIsEnabled(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_CONTROL_REG(controller);
    uint32 enableMask  = mMethodsGet(controller)->ES_EYESCAN_EN_MASK(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    return (regVal & enableMask) ? cAtTrue : cAtFalse;
    }

static void EyeScanEnable(AtEyeScanLane self, eBool enabled)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_CONTROL_REG(controller);
    uint32 eyeScanEnableMask  = mMethodsGet(controller)->ES_EYESCAN_EN_MASK(controller);
    uint32 eyeScanEnableShift = mMethodsGet(controller)->ES_EYESCAN_EN_SHIFT(controller);
    uint32 errDetMask  = mMethodsGet(controller)->ES_ERRDET_EN_MASK(controller);
    uint32 errDetShift = mMethodsGet(controller)->ES_ERRDET_EN_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    uint32 hwEnabled = enabled ? 1 : 0;
    mRegFieldSet(regVal, eyeScanEnable, hwEnabled);
    mRegFieldSet(regVal, errDet, hwEnabled);
    mDrpWrite(self, regAddress, regVal);
    }

static void HorizontalOffsetSet(AtEyeScanLane self, uint16 value)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_HORZ_OFFSET_REG(controller);
    uint32 horzOffsetMask  = mMethodsGet(controller)->ES_HORZ_OFFSET_MASK(controller);
    uint32 horzOffsetShift = mMethodsGet(controller)->ES_HORZ_OFFSET_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);

    mRegFieldSet(regVal, horzOffset, value);
    mDrpWrite(self, regAddress, regVal);
    }

static uint16 RxOutDiv(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->RXOUT_DIV_REG(controller);
    uint32 rxOutDivMask  = mMethodsGet(controller)->RXOUT_DIV_MASK(controller);
    uint32 rxOutDivShift = mMethodsGet(controller)->RXOUT_DIV_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    return (uint16) mRegField(regVal, rxOutDiv);
    }

static eAtRet StepSizeSetup(AtEyeScanLane self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet Setup(AtEyeScanLane self)
    {
    return mMethodsGet(self)->StepSizeSetup(self);
    }

static eAtRet DebugPixelScan(AtEyeScanLane self, int32 horzOffset, int32 vertOffset)
    {
    /* TODO: Should be supported */
    AtUnused(self);
    AtUnused(horzOffset);
    AtUnused(vertOffset);
    return cAtErrorNotImplemented;
    }

static eBool NeedEyeCenter(AtEyeScanLane self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Reset(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    return AtEyeScanControllerReset(controller, self);
    }

static void OverrideAtObject(AtEyeScanLane self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, WillDelete);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtEyeScanLane self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtEyeScanLane self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, EyeScanSetup);
        mMethodOverride(m_methods, ShouldAcquire);
        mMethodOverride(m_methods, FinishScanning);
        mMethodOverride(m_methods, Acquire);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, MeasuredWidth);
        mMethodOverride(m_methods, MeasuredSwing);
        mMethodOverride(m_methods, NumHorizontalTaps);
        mMethodOverride(m_methods, ElapsedTimeMs);
        mMethodOverride(m_methods, NeedInitialize);
        mMethodOverride(m_methods, InitializeEsQualMasks);
        mMethodOverride(m_methods, InitializeEsSDataMasks);
        mMethodOverride(m_methods, PmaSetup);
        mMethodOverride(m_methods, ShowLaneStatus);
        mMethodOverride(m_methods, HorizontalOffsetSet);
        mMethodOverride(m_methods, VerticalOffsetSet);
        mMethodOverride(m_methods, StepSizeSetup);
        mMethodOverride(m_methods, DebugPixelScan);
        mMethodOverride(m_methods, NeedEyeCenter);
        mMethodOverride(m_methods, Debug);
        }

    mMethodsSet(self, &m_methods);
    }

AtEyeScanLane AtEyeScanLaneObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->controller = controller;
    self->id         = laneId;

    /* Refer XILINX TCL script for these default values */
    self->equalizerMode  = cAtEyeScanEqualizerModeDfe;
    self->horzStepSize   = 1;
    self->vertStepSize   = 1;
    self->maxPrescale    = 5;
    self->eyeScanEnabled = cAtFalse;
    self->maxVertOffset = MaxVerticalOffsetCalculate(self);
    self->listeners = AtListCreate(0);

    return self;
    }

eAtRet AtEyeScanLaneInit(AtEyeScanLane self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtOk;
    }

eAtRet AtEyeScanLaneAcquire(AtEyeScanLane self)
    {
    eAtRet ret = cAtOk;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->finished)
        return cAtOk;

    if (!self->scanning)
        {
        NotifyEyeScanWillStart(self);
        mMethodsGet(self)->EyeScanSetup(self);
        self->finished = cAtFalse;
        self->scanning = cAtTrue;
        }

    if (mMethodsGet(self)->ShouldAcquire(self))
        ret = mMethodsGet(self)->Acquire(self);

    if (mMethodsGet(self)->FinishScanning(self))
        {
        self->finished = cAtTrue;
        NotifyDidFinish(self);
        ResetPixelCount(self);
        self->scanning = cAtFalse;
        }

    return ret;
    }

void AtEyeScanLaneEyeScanEnable(AtEyeScanLane self, eBool enabled)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    eBool shouldReset = cAtFalse;

    /* Eyescan is re-enabled */
    if (enabled && (!AtEyeScanLaneEyeScanIsEnabled(self)))
        shouldReset = cAtTrue;

    EyeScanEnable(self, enabled);

    if (shouldReset)
        AtEyeScanControllerPmaReset(controller);
    }

void AtEyeScanLanePrescaleSet(AtEyeScanLane self, uint16 value)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_PRESCALE_REG(controller);
    uint32 preScaleMask  = mMethodsGet(controller)->ES_PRESCALE_MASK(controller);
    uint32 preScaleShift = mMethodsGet(controller)->ES_PRESCALE_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    mRegFieldSet(regVal, preScale, value);
    mDrpWrite(self, regAddress, regVal);
    }

void AtEyeScanLaneInitializeEsQualMasks(AtEyeScanLane self)
    {
    if (self)
        mMethodsGet(self)->InitializeEsQualMasks(self);
    }

void AtEyeScanLaneInitializeEsSDataMasks(AtEyeScanLane self)
    {
    if (self)
        mMethodsGet(self)->InitializeEsSDataMasks(self);
    }

void AtEyeScanLaneHorizontalOffsetSet(AtEyeScanLane self, uint16 value)
    {
    if (self)
        mMethodsGet(self)->HorizontalOffsetSet(self, value);
    }

eAtRet AtEyeScanLaneVerticalOffsetSet(AtEyeScanLane self, uint16 value)
    {
    if (self)
        return mMethodsGet(self)->VerticalOffsetSet(self, value);
    return cAtErrorNullPointer;
    }

uint16 AtEyeScanLaneDataWidthRead(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->RX_DATA_WIDTH_REG(controller);
    uint32 rxDataWidthMask = mMethodsGet(controller)->RX_DATA_WIDTH_MASK(controller);
    uint32 rxDataWidthShift = mMethodsGet(controller)->RX_DATA_WIDTH_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    uint32 value = mRegField(regVal, rxDataWidth);

    switch (value)
        {
        case 2: return 16;
        case 3: return 20;
        case 4: return 32;
        case 5: return 40;
        case 6: return 64;
        case 7: return 80;
        default:
            return 0;
        }
    }

static eBool EyeScanDone(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_CONTROL_STATUS_REG(controller);
    uint32 controlMask  = mMethodsGet(controller)->ES_CONTROL_DONE_MASK(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    return (regVal & controlMask) ? cAtTrue : cAtFalse;
    }

eBool AtEyeScanLaneDrpEyeScanDone(AtEyeScanLane self)
    {
    if (self)
        return EyeScanDone(self);
    return cAtFalse;
    }

uint16 AtEyeScanLanePrescaleGet(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_PRESCALE_REG(controller);
    uint32 prescaleMask  = mMethodsGet(controller)->ES_PRESCALE_MASK(controller);
    uint32 prescaleShift = mMethodsGet(controller)->ES_PRESCALE_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    return (uint16) mRegField(regVal, prescale);
    }

uint16 AtEyeScanLaneErrorCountGet(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_ERROR_COUNT_REG(controller);
    return AtEyeScanLaneDrpRead(self, regAddress);
    }

uint16 AtEyeScanLaneSampleCountGet(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_SAMPLE_COUNT_REG(controller);
    return AtEyeScanLaneDrpRead(self, regAddress);
    }

uint16 AtEyeScanLaneRxOutDiv(AtEyeScanLane self)
    {
    if (self)
        return RxOutDiv(self);
    return cInvalidUint16;
    }

eAtRet AtEyeScanLaneLock(AtEyeScanLane self)
    {
    return (AtOsalMutexLock(Locker(self)) == cAtOsalOk) ? cAtOk : cAtErrorOsalFail;
    }

eAtRet AtEyeScanLaneUnLock(AtEyeScanLane self)
    {
    return (AtOsalMutexUnLock(Locker(self)) == cAtOsalOk) ? cAtOk : cAtErrorOsalFail;
    }

eBool AtEyeScanLaneNeedInitialize(AtEyeScanLane self)
    {
    if (self)
        return mMethodsGet(self)->NeedInitialize(self);
    return cAtFalse;
    }

static uint32 MeasuredSwingInMilliVolt(AtEyeScanLane self)
    {
    uint32 measuredSwing = AtEyeScanLaneMeasuredSwing(self);
    uint32 voltageSwing = AtEyeScanControllerVoltageSwingInMillivolt(AtEyeScanLaneControllerGet(self)) * 2;
    uint8 verticalOffset = AtEyeScanLaneVerticalStepSizeGet(self);
    return (uint32)((float)measuredSwing * ((float)voltageSwing / (float)verticalOffset));
    }

eAtRet AtEyeScanLaneMove2Wait(AtEyeScanLane self)
    {
    if (self)
        return Move2Wait(self);
    return cAtErrorNullPointer;
    }

eAtRet AtEyeScanLaneMove2Run(AtEyeScanLane self)
    {
    if (self)
        return Move2Run(self);
    return cAtErrorNullPointer;
    }

void AtEyeScanLaneLog(AtEyeScanLane self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...)
    {
    va_list args;
    if (self == NULL)
        return;

    va_start(args, format);
    AtDriverVaListLog(AtDriverSharedDriverGet(), level, file, line, format, args);
    va_end(args);
    }

eAtRet AtEyeScanLaneSetup(AtEyeScanLane self)
    {
    if (self)
        return Setup(self);
    return cAtErrorNullPointer;
    }

uint16 AtEyeScanLaneEyeScanControlState(AtEyeScanLane self)
    {
    if (self)
        return EyeScanControlState(self);
    return cInvalidUint16;
    }

eBool AtEyeScanLaneEyeScanIsEnabled(AtEyeScanLane self)
    {
    if (self)
        return EyeScanIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtEyeScanLaneDebugPixelScan(AtEyeScanLane self, int32 horzOffset, int32 vertOffset)
    {
    if (self)
        return mMethodsGet(self)->DebugPixelScan(self, horzOffset, vertOffset);
    return cAtErrorNullPointer;
    }

eAtRet AtEyeScanLaneReset(AtEyeScanLane self)
    {
    if (self)
        return Reset(self);
    return cAtErrorNullPointer;
    }

/**
 * @addtogroup AtEyeScanLane
 * @{
 */

/**
 * Read DRP register
 *
 * @param self This lane
 * @param drpAddress DRP register address
 *
 * @return Register value
 */
uint16 AtEyeScanLaneDrpRead(AtEyeScanLane self, uint16 drpAddress)
    {
    if (self)
        return DrpRead(self, drpAddress);
    return 0xDEAD;
    }

/**
 * Write DRP register
 *
 * @param self This lane
 * @param drpAddress DRP register address
 * @param value Value to write
 */
void AtEyeScanLaneDrpWrite(AtEyeScanLane self, uint16 drpAddress, uint16 value)
    {
    if (self)
        mDrpWrite(self, drpAddress, value);
    }

/**
 * Add listener to listen on eye scan events
 *
 * @param self This lane
 * @param listener Listener callback
 * @param userData User data. This will be input when callback is called.
 *
 * @return AT return code
 */
eAtRet AtEyeScanLaneListenerAdd(AtEyeScanLane self, const tAtEyeScanLaneListener *listener, void* userData)
    {
    if (self)
        return ListenerAdd(self, userData, listener);
    return cAtErrorNullPointer;
    }

/**
 * Remove eye scan listener
 *
 * @param self This lane
 * @param listener Listener to be removed
 * @param userData User data associated to this listener
 *
 * @param listener Listener to remove
 */
eAtRet AtEyeScanLaneListenerRemove(AtEyeScanLane self, const tAtEyeScanLaneListener *listener, void* userData)
    {
    if (self)
        return ListenerRemove(self, userData, listener);
    return cAtErrorNullPointer;
    }

/**
 * Check if eye scan state machine finish
 *
 * @param self This lane
 *
 * @retval cAtTrue if finished
 * @retval cAtFalse if not finish
 */
eBool AtEyeScanLaneScanFinished(AtEyeScanLane self)
    {
    return (eBool)(self ? self->finished : cAtFalse);
    }

/**
 * Get lane ID
 *
 * @param self This lane
 *
 * @return Lane ID.
 */
uint8 AtEyeScanLaneIdGet(AtEyeScanLane self)
    {
    return (uint8)(self ? self->id : cBit7_0);
    }

/**
 * Get eye scan controller
 *
 * @param self This lane
 *
 * @return Eye scan controller
 */
AtEyeScanController AtEyeScanLaneControllerGet(AtEyeScanLane self)
    {
    return self ? self->controller : NULL;
    }

/**
 * Set horizontal step size
 *
 * @param self This lane
 * @param horizontalStepSize Horizontal step size
 *
 * @return AT return code
 */
eAtRet AtEyeScanLaneHorizontalStepSizeSet(AtEyeScanLane self, uint8 horizontalStepSize)
    {
    if (self)
        self->horzStepSize = horizontalStepSize;
    return self ? cAtOk : cAtError;
    }

/**
 * Get horizontal step size
 *
 * @param self This lane
 *
 * @return horizontal step size
 */
uint8 AtEyeScanLaneHorizontalStepSizeGet(AtEyeScanLane self)
    {
    return (uint8)(self ? self->horzStepSize : 0);
    }

/**
 * Set vertical step size
 *
 * @param self This lane
 * @param verticalStepSize vertical step size
 *
 * @return AT return code
 */
eAtRet AtEyeScanLaneVerticalStepSizeSet(AtEyeScanLane self, uint8 verticalStepSize)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    self->vertStepSize  = verticalStepSize;
    self->maxVertOffset = MaxVerticalOffsetCalculate(self);

    return cAtOk;
    }

/**
 * Get vertical step size
 *
 * @param self This lane
 *
 * @return Vertical step size
 */
uint8 AtEyeScanLaneVerticalStepSizeGet(AtEyeScanLane self)
    {
    return (uint8)(self ? self->vertStepSize : 0);
    }

/**
 * Set max prescale
 *
 * @param self This lane
 * @param maxPrescale Max prescale
 *
 * @return AT return code
 */
eAtRet AtEyeScanLaneMaxPrescaleSet(AtEyeScanLane self, uint8 maxPrescale)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!MaxPrescaleIsInRange(self, maxPrescale))
        return cAtErrorOutOfRangParm;

    self->maxPrescale = maxPrescale;

    return cAtOk;
    }

/**
 * Get max prescale
 *
 * @param self This lane
 *
 * @return Max prescale
 */
uint8 AtEyeScanLaneMaxPrescaleGet(AtEyeScanLane self)
    {
    return (uint8)(self ? self->maxPrescale : 0);
    }

/**
 * Get max horizontal offset
 *
 * @param self This lane
 *
 * @return Max horizontal offset
 */
uint16 AtEyeScanLaneMaxHorizontalOffsetGet(AtEyeScanLane self)
    {
    if (self)
        return AtEyeScanControllerMaxHorizontalOffsetGet(AtEyeScanLaneControllerGet(self), self);
    return 0;
    }

/**
 * Get data width
 *
 * @param self This lane
 *
 * @return Data width
 */
uint16 AtEyeScanLaneDataWidthGet(AtEyeScanLane self)
    {
    return AtEyeScanControllerLaneDataWidth(AtEyeScanLaneControllerGet(self), self);
    }

/**
 * Enable eye scanning
 *
 * @param self This lane
 * @param enable Enable. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtEyeScanLaneEnable(AtEyeScanLane self, eBool enable)
    {
    if (self == NULL)
        return cAtError;

    self->eyeScanEnabled = enable;

    if (!enable)
        {
        self->scanning = cAtFalse;
        return cAtOk;
        }

    if (self->finished)
        self->finished = cAtFalse;

    return cAtOk;
    }

/**
 * Check if eye scanning is enabled
 *
 * @param self This lane
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtEyeScanLaneIsEnabled(AtEyeScanLane self)
    {
    return (eBool)(self ? self->eyeScanEnabled : cAtFalse);
    }

/**
 * To query last scan pixel. It is to used to check if eye scan state machine is
 * running or not. If state machine is running, this attribute will always be
 * updated.
 *
 * @param self This lane
 *
 * @return Last scan pixel or NULL if eye scan state machine is not running
 */
const tAtEyeScanLanePixel *AtEyeScanLaneLastScanPixel(AtEyeScanLane self)
    {
    uint16 currentPixel;

    if (self == NULL)
        return NULL;

    currentPixel = self->pixelCount;
    if (currentPixel == 0)
        return NULL;

    return &(self->pixels[currentPixel - 1]);
    }

/**
 * Show eye-scan lane debug information
 *
 * @param self This lane
 */
eAtRet AtEyeScanLaneDebug(AtEyeScanLane self)
    {
    if (self)
        return mMethodsGet(self)->Debug(self);
    return cAtErrorNullPointer;
    }

/**
 * Enable debugging to debug local state machine.
 *
 * @param self This lane
 * @param enable Enabling. cAtTrue to enable and cAtFalse to disable.
 */
eAtRet AtEyeScanLaneDebugEnable(AtEyeScanLane self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    self->debugEnabled = enable;
    return cAtOk;
    }

/**
 * Check if state machine debug is enabled or not.
 *
 * @param self This lane
 * @retval cAtTrue if debug is enabled
 * @retval cAtFalse if debug is disabled
 */
eBool AtEyeScanLaneDebugIsEnabled(AtEyeScanLane self)
    {
    return (eBool)(self ? self->debugEnabled : cAtFalse);
    }

/**
 * Get measured width
 *
 * @param self This lane
 *
 * @return Measured width
 */
uint32 AtEyeScanLaneMeasuredWidth(AtEyeScanLane self)
    {
    mAttributeGet(MeasuredWidth, uint32, 0);
    }

/**
 * Get measured swing
 *
 * @param self This lane
 *
 * @return Measured swing
 */
uint32 AtEyeScanLaneMeasuredSwing(AtEyeScanLane self)
    {
    mAttributeGet(MeasuredSwing, uint32, 0);
    }

/**
 * Get measured swing in millivolt
 *
 * @param self This lane
 *
 * @return Measured swing in millivolt
 */
uint32 AtEyeScanLaneMeasuredSwingInMilliVolt(AtEyeScanLane self)
    {
    if (self)
        return MeasuredSwingInMilliVolt(self);
    return 0;
    }

/**
 * Get number of horizontal taps
 *
 * @param self This lane
 * @return Number of horizontal taps
 */
uint32 AtEyeScanLaneNumHorizontalTaps(AtEyeScanLane self)
    {
    mAttributeGet(NumHorizontalTaps, uint32, 0);
    }

/**
 * Get elapsed time
 *
 * @param self This lane
 *
 * @return Elapsed time
 */
uint32 AtEyeScanLaneElapsedTimeMs(AtEyeScanLane self)
    {
    mAttributeGet(ElapsedTimeMs, uint32, 0);
    }

/**
 * Get lane speed in Gbps
 *
 * @param self This lane
 *
 * @return Lane speed in Gbps
 */
float AtEyeScanLaneSpeedInGbps(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    return AtEyeScanControllerSpeedInGbps(controller, self);
    }

/**
 * Convert vertical offset to voltage in mV
 *
 * @param self This lane
 * @param verticalOffset Vertical offset
 *
 * @return Voltage (in mV) corresponding to vertical offset
 */
int32 AtEyeScanLaneVerticalOffsetToMillivolt(AtEyeScanLane self, int32 verticalOffset)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    return AtEyeScanControllerVerticalOffsetToMillivolt(controller, self, verticalOffset);
    }

/**
 * @}
 */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : AtSemControllerInternal.h
 * 
 * Created Date: Dec 9, 2015
 *
 * Description : SEM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSEMCONTROLLERINTERNAL_H_
#define _ATSEMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "AtObject.h"
#include "AtSemController.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSemControllerMethods
    {
    eAtRet (*Init)(AtSemController self);
    eAtRet (*Validate)(AtSemController self);
    uint32 (*CounterGet)(AtSemController self, uint32 counterType);
    uint32 (*CounterClear)(AtSemController self, uint32 counterType);
    uint32 (*AlarmGet)(AtSemController self);
    uint32 (*AlarmHistoryGet)(AtSemController self);
    uint32 (*AlarmHistoryClear)(AtSemController self);
    eAtRet (*InterruptMaskSet)(AtSemController self, uint32 alarmMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtSemController self);
    uint32 (*SupportedAlarms)(AtSemController self);
    void (*InterruptProcess)(AtSemController self, AtIpCore ipCore);

    /* Deprecated */
    uint8 (*CorrectedErrorCounterGet)(AtSemController self);
    uint8 (*StatusGet)(AtSemController self);

    /* Internal */
    eBool (*IsActive)(AtSemController self);
    eAtRet (*MoveObservationModeBeforeActiveChecking)(AtSemController self);
    uint32 (*MaxElapseTimeToInsertError)(AtSemController self);
    uint32 (*MaxElapseTimeToMoveFsm)(AtSemController self);

    /* UART */
    AtUart (*UartGet)(AtSemController self);
    AtUart (*UartObjectCreate)(AtSemController self);
    eAtRet (*ValidateByUart)(AtSemController self);
    eAtRet (*ForceErrorByUart)(AtSemController self, eAtSemControllerCounterType errorType);
    eAtRet (*AsyncForceErrorByUart)(AtSemController self, eAtSemControllerCounterType errorType);

    /* Sem device information for essential error lookup in EBD files */
    const char* (*DeviceDescription)(AtSemController self);
    uint8 (*NumSlr)(AtSemController self);
    uint32 (*MaxLfa)(AtSemController self);
    uint32 (*FrameSize)(AtSemController self);
    const char* (*EbdFilePath)(AtSemController self, uint8 slrNum);
    eAtRet (*EbdFilePathSet)(AtSemController self, uint8 slrNum, const char* filePath);
    eBool (*ErrorIsEssential)(AtSemController self, const char *uartReport, uint32 reportLength);
    uint32 (*GlobalLfaToLocalLfa)(AtSemController self, uint32 globalLfa);
    uint32 (*GlobalLfaToSlrId)(AtSemController self, uint32 globalLfa);
    uint32 (*NumDummyWordsInEbd)(AtSemController self);
    }tAtSemControllerMethods;

typedef struct tAtSemController
    {
    tAtObject super;
    const tAtSemControllerMethods *methods;

    /* Private data. */
    AtDevice device;
    AtUart uart;
    uint32 semId;
    char **ebdFilePaths;
    
    /* For debugging */
    uint32 maxElapseTimeToMoveFsm;
    uint32 maxElapseTimeToInsertError;

    /* Listeners */
    AtList allEventListeners;
    AtOsalMutex listenerListMutex; /* Locker to protect list of event listener */
    }tAtSemController;

typedef struct tAtSemControllerEventListenerWrapper * AtSemControllerEventListener;

typedef struct tAtSemControllerEventListenerWrapper
    {
    tAtObject super;
    tAtSemControllerEventListener listener;
    void *userData;
    }tAtSemControllerEventListenerWrapper;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController AtSemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId);

uint32 AtSemControllerMaxElapseTimeToInsertError(AtSemController self);
uint32 AtSemControllerMaxElapseTimeToMoveFsm(AtSemController self);
eBool AtSemControllerIsActive(AtSemController self);
eAtRet AtSemControllerMoveObservationModeBeforeActiveChecking(AtSemController self);

void AtSemControllerInterruptProcess(AtSemController self, AtIpCore ipCore);
void AtSemControllerAllAlarmListenersCall(AtSemController self, uint32 changedAlarms, uint32 currentAlarms);

uint32 AtSemControllerGlobalLfaToLocalLfa(AtSemController self, uint32 globalLfa);
uint32 AtSemControllerGlobalLfaToSlrId(AtSemController self, uint32 globalLfa);
uint32 AtSemControllerNumDummyWordsInEbd(AtSemController self);
eBool AtUartRerportErrorIsEssential(AtSemController self, const char *uartReport, uint32 reportLength);
uint32 AtUartReportLineCount(const char *report, uint32 reportLength);
const char* AtUartReportAlineAtLineIndex(const char *report, uint32 reportLength, uint32 lineNum, uint32 *lineLength);

#ifdef __cplusplus
}
#endif
#endif /* _ATSEMCONTROLLERINTERNAL_H_ */


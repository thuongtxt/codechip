/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanLaneFastGty.c
 *
 * Created Date: Sep 1, 2016
 *
 * Description : Lane that use fast scan algorithm
 *
 * Notes       : GTY
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanLaneInternal.h"
#include "AtEyeScanControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define DRP_ADDR_ES_QUAL_MASK0    0x044
#define DRP_ADDR_ES_QUAL_MASK1    0x045
#define DRP_ADDR_ES_QUAL_MASK2    0x046
#define DRP_ADDR_ES_QUAL_MASK3    0x047
#define DRP_ADDR_ES_QUAL_MASK4    0x048
#define DRP_ADDR_ES_QUAL_MASK5    0x0EC
#define DRP_ADDR_ES_QUAL_MASK6    0x0ED
#define DRP_ADDR_ES_QUAL_MASK7    0x0EE
#define DRP_ADDR_ES_QUAL_MASK8    0x0EF
#define DRP_ADDR_ES_QUAL_MASK9    0x0F0

#define DRP_ADDR_ES_SDATA_MASK0   0x049
#define DRP_ADDR_ES_SDATA_MASK1   0x04A
#define DRP_ADDR_ES_SDATA_MASK2   0x04B
#define DRP_ADDR_ES_SDATA_MASK3   0x04C
#define DRP_ADDR_ES_SDATA_MASK4   0x04D
#define DRP_ADDR_ES_SDATA_MASK5   0x0F1
#define DRP_ADDR_ES_SDATA_MASK6   0x0F2
#define DRP_ADDR_ES_SDATA_MASK7   0x0F3
#define DRP_ADDR_ES_SDATA_MASK8   0x0F4
#define DRP_ADDR_ES_SDATA_MASK9   0x0F5

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanLaneMethods m_AtEyeScanLaneOverride;

/* Save super implementation */
static const tAtEyeScanLaneMethods *m_AtEyeScanLaneMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void InitializeEsQualMasks(AtEyeScanLane self)
    {
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK0, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK1, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK2, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK3, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK4, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK5, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK6, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK7, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK8, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_QUAL_MASK9, 0xFFFF);
    }

static void InitializeEsSDataMasksWithWidth(AtEyeScanLane self, uint32 width)
    {
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK5, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK6, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK7, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK8, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK9, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK4, 0x0000);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK0, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK1, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK2, 0xFFFF);
    AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK3, 0xFFFF);

    /* Override some settings */
    switch (width)
        {
        case 20:
            AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK3, 0x0FFF);
            break;

        case 64:
            AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK3, 0x0000);
            AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK2, 0x0000);
            AtEyeScanLaneDrpWrite(self, DRP_ADDR_ES_SDATA_MASK1, 0x0000);
            break;

        default:
            break;
        }
    }

static uint16 DataWidth(AtEyeScanLane self)
    {
    return AtEyeScanLaneDataWidthRead(self);
    }

static void InitializeEsSDataMasks(AtEyeScanLane self)
    {
    InitializeEsSDataMasksWithWidth(self, DataWidth(self));
    }

static eBool NeedEyeCenter(AtEyeScanLane self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void EyeScanSetup(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    m_AtEyeScanLaneMethods->EyeScanSetup(self);
    AtEyeScanControllerVsRangeSet(controller, self, AtEyeScanControllerDefaultVsRange(controller, self));
    }

static float VsRangeGet(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    return AtEyeScanControllerVsRangeInMillivoltGet(controller, self);
    }

static eAtRet Debug(AtEyeScanLane self)
    {
    AtPrintc(cSevNormal, "  * MaxHorizontalOffset: %u \r\n", AtEyeScanLaneMaxHorizontalOffsetGet(self));
    AtPrintc(cSevNormal, "  * HorizontalStepSize : %u \r\n", AtEyeScanLaneHorizontalStepSizeGet(self));
    AtPrintc(cSevNormal, "  * NumHorizontalTaps  : %u \r\n", AtEyeScanLaneNumHorizontalTaps(self));
    AtPrintc(cSevNormal, "  * VerticalStepSize   : %u \r\n", AtEyeScanLaneVerticalStepSizeGet(self));
    AtPrintc(cSevNormal, "  * Enabled            : %s \r\n", AtEyeScanLaneIsEnabled(self) ? "enable":"disable");

    /* DRP attributes */
    AtPrintc(cSevNormal, "  * RX_DATA_WIDTH      : %u \r\n", AtEyeScanLaneDataWidthRead(self));
    AtPrintc(cSevNormal, "  * ES_PRESCALE        : %u \r\n", AtEyeScanLanePrescaleGet(self));
    AtPrintc(cSevNormal, "  * RXOUT_DIV          : %u \r\n", AtEyeScanLaneRxOutDiv(self));
    AtPrintc(cSevNormal, "  * RX_EYESCAN_VS_RANGE: %.1f (mV/count) \r\n", VsRangeGet(self));

    mMethodsGet(self)->ShowLaneStatus(self);

    return cAtOk;
    }

static void OverrideAtEyeScanLane(AtEyeScanLane self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEyeScanLaneMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanLaneOverride, mMethodsGet(self), sizeof(m_AtEyeScanLaneOverride));

        mMethodOverride(m_AtEyeScanLaneOverride, InitializeEsQualMasks);
        mMethodOverride(m_AtEyeScanLaneOverride, InitializeEsSDataMasks);
        mMethodOverride(m_AtEyeScanLaneOverride, NeedEyeCenter);
        mMethodOverride(m_AtEyeScanLaneOverride, EyeScanSetup);
        mMethodOverride(m_AtEyeScanLaneOverride, Debug);
        }

    mMethodsSet(self, &m_AtEyeScanLaneOverride);
    }

static void Override(AtEyeScanLane self)
    {
    OverrideAtEyeScanLane(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanLaneFastGty);
    }

static AtEyeScanLane ObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanLaneFastV2ObjectInit(self, controller, laneId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanLane AtEyeScanLaneFastGtyNew(AtEyeScanController controller, uint8 laneId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanLane newLane = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newLane, controller, laneId);
    }

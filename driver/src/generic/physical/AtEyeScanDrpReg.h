/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtEyeScanDrpReg.h
 * 
 * Created Date: Feb 26, 2015
 *
 * Description : DRP register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEYESCANDRPREG_H_
#define _ATEYESCANDRPREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define DRP_ADDR_ES_CONTROL_REG       0x03D
#define DRP_ADDR_ES_CONTROL_MASK      cBit5_0
#define DRP_ADDR_ES_CONTROL_SHIFT     0
#define DRP_ADDR_ES_EYESCAN_EN_MASK   cBit8
#define DRP_ADDR_ES_EYESCAN_EN_SHIFT  8
#define DRP_ADDR_ES_ERRDET_EN_MASK    cBit9
#define DRP_ADDR_ES_ERRDET_EN_SHIFT   9

#define DRP_ADDR_ES_PRESCALE          0x03B
#define DRP_ADDR_ES_PRESCALE_MASK     cBit15_11
#define DRP_ADDR_ES_PRESCALE_SHIFT    11

#define DRP_ADDR_ES_VERT_OFFSET            0x03B

#define DRP_ADDR_ES_VS_NEG_DIR_MASK        cBit7
#define DRP_ADDR_ES_VS_NEG_DIR_SHIFT       7

#define DRP_ADDR_ES_VERT_OFFSET_MASK       cBit6_0
#define DRP_ADDR_ES_VERT_OFFSET_SHIFT      0

#define DRP_ADDR_ES_VERT_UTSIGN_MASK       cBit8
#define DRP_ADDR_ES_VERT_UTSIGN_SHIFT      8

#define DRP_ADDR_ES_HORZ_OFFSET       0x03C
#define DRP_ADDR_ES_HORZ_OFFSET_MASK  cBit11_0
#define DRP_ADDR_ES_HORZ_OFFSET_SHIFT 0

#define DRP_ADDR_ES_CONTROL_STATUS 0x151
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_MASK  cBit3_1
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_SHIFT 1
#define DRP_ADDR_ES_CONTROL_DONE_MASK  cBit0
#define DRP_ADDR_ES_CONTROL_DONE_SHIFT 0

#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_WAIT  0
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_RESET 1
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_COUN  3
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_END   2
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_ARMED 5
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_READ  4

#define DRP_ADDR_ES_ERROR_COUNT    0x14F
#define DRP_ADDR_ES_SAMPLE_COUNT   0x150
#define DRP_ADDR_ES_SDATA_MASK0    0x036
#define DRP_ADDR_ES_SDATA_MASK1    0x037
#define DRP_ADDR_ES_SDATA_MASK2    0x038
#define DRP_ADDR_ES_SDATA_MASK3    0x039
#define DRP_ADDR_ES_SDATA_MASK4    0x03A
#define DRP_ADDR_ES_QUAL_MASK0     0x031
#define DRP_ADDR_ES_QUAL_MASK1     0x032
#define DRP_ADDR_ES_QUAL_MASK2     0x033
#define DRP_ADDR_ES_QUAL_MASK3     0x034
#define DRP_ADDR_ES_QUAL_MASK4     0x035

#define DRP_ADDR_RX_DATA_WIDTH       0x011
#define DRP_ADDR_RX_DATA_WIDTH_MASK  cBit2_0
#define DRP_ADDR_RX_DATA_WIDTH_SHIFT 0

#define DRP_ADDR_RXOUT_DIV           0x088
#define DRP_ADDR_RXOUT_DIV_MASK      cBit2_0
#define DRP_ADDR_RXOUT_DIV_SHIFT     0

#define DRP_ADDR_PMA_RSV2              0x082
#define DRP_ADDR_PMA_RSV2_ENABLE_MASK  cBit5
#define DRP_ADDR_PMA_RSV2_ENABLE_SHIFT 5

/*------------------------------------------------------------------------------
Reg Name: SERDES control
Reg Addr: 0xf00044
Reg Desc:
------------------------------------------------------------------------------*/
#define ES_REG_SERDES_CONTROL_XAUI          0xf00044
#define ES_REG_SERDES_CONTROL_STM           0xf00042

#define ES_SERDES_CONTROL_RESET_ALL_MASK    cBit0
#define ES_SERDES_CONTROL_RESET_TX_PMA_MASK cBit1
#define ES_SERDES_CONTROL_RESET_TX_PCS_MASK cBit2

#define ES_SERDES_CONTROL_RESET_RX_PMA_MASK  cBit3
#define ES_SERDES_CONTROL_RESET_RX_PMA_SHIFT 3

#define ES_SERDES_CONTROL_RESET_RX_PCS_MASK cBit4
#define ES_SERDES_CONTROL_RESET_EYE_MASK    cBit6
#define ES_SERDES_CONTROL_RESET_ALL         cBit6_0

#define ES_SERDES_CONTROL_LPM_MASK          cBit7
#define ES_SERDES_CONTROL_LPM_SHIFT         7

/*------------------------------------------------------------------------------
Reg Name: SERDES lane status
Reg Addr: 0xf0006b
Reg Desc:
------------------------------------------------------------------------------*/
#define ES_REG_SERDES_LANE_STATUS(laneId)  (((laneId) < 2) ? 0xf0006b : 0xf0006c)

#define ES_REG_SERDES_LANE_STATUS_FIELD_MASK(laneId)  (((laneId) % 2) == 0) ? cBit15_0 : cBit31_16
#define ES_REG_SERDES_LANE_STATUS_FIELD_SHIFT(laneId) (((laneId) % 2) == 0) ? 0 : 16

#define ES_REG_SERDES_TX_RESET_DONE_MASK       cBit0
#define ES_REG_SERDES_RX_RESET_DONE_MASK       cBit1

#define ES_REG_SERDES_BUF_STATE_MASK           cBit4_2
#define ES_REG_SERDES_BUF_STATE_SHIFT          2

#define ES_REG_SERDES_TX_PHASE_ALIGN_MASK      cBit5
#define ES_REG_SERDES_TX_PHY_INIT_DONE_MASK    cBit6
#define ES_REG_SERDES_TX_DELAY_RESET_DONE_MASK cBit7
#define ES_REG_SERDES_EYE_SCAN_DATA_ERROR_MASK cBit8

#define ES_REG_SERDES_RX_DISP_ERROR_RXDATA_7_0_MASK    cBit9
#define ES_REG_SERDES_RX_DISP_ERROR_RXDATA_15_8_MASK   cBit10

#define ES_REG_SERDES_RX_NOT_INTABLE_RXDATA_7_0_MASK   cBit11
#define ES_REG_SERDES_RX_NOT_INTABLE_RXDATA_15_8_MASK  cBit12

#define ES_REG_SERDES_RX_COMMA_DET_MASK        cBit13

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATEYESCANDRPREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtSerdesController.c
 *
 * Created Date: Aug 16, 2013
 *
 * Description : SERDES controller. To configure physical parameters of STM
 *               port and GE port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSerdesControllerInternal.h"
#include "AtSerdesManagerInternal.h"
#include "../common/AtChannelInternal.h"
#include "../man/AtModuleInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSerdesController)self)
#define mInAccessible(self) AtSerdesControllerInAccessible(self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tListenerWraper
    {
    void* userData;
    tAtSerdesControllerPropertyListener listener;
    }tListenerWraper;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSerdesControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HwParameterId(AtSerdesController self, eAtSerdesParam parameterId)
    {
    AtUnused(self);
    return parameterId;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtSerdesMode defaultMode;

    /* Concrete may want to have default SERDES mode */
    defaultMode = mMethodsGet(self)->DefaultMode(self);
	if (defaultMode != cAtSerdesModeUnknown)
	    return AtSerdesControllerModeSet(self, defaultMode);

    /* Sub class must do */
    return cAtOk;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
	AtUnused(self);
    /* Sub class must do */
    return cAtTrue;
    }

static eBool PllChanged(AtSerdesController self)
    {
    AtUnused(self);
    /* Sub class must do */
    return cAtFalse;
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesLinkStatusNotSupported;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    /* Sub class must do */
    AtUnused(self);
    return enable ? cAtErrorNotImplemented : cAtOk;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    /* Sub class must do */
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet FpgaEnable(AtSerdesController self, eBool enable)
    {
    /* Sub class must do */
    AtUnused(self);
    return enable ? cAtErrorNotImplemented : cAtOk;
    }

static eBool FpgaIsEnabled(AtSerdesController self)
    {
    /* Sub class must do */
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    /* Sub class must do */
    AtUnused(self);
    return enable ? cAtFalse : cAtTrue;
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
	AtUnused(value);
	AtUnused(param);
	AtUnused(self);
    /* Sub class must do */
    return cAtErrorNotImplemented;
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
	AtUnused(param);
	AtUnused(self);
    /* Sub class must do */
    return 0;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
	AtUnused(enable);
	AtUnused(loopbackMode);
	AtUnused(self);
    /* Sub class must do */
    return cAtErrorNotImplemented;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
	AtUnused(loopbackMode);
	AtUnused(self);
    /* Sub class must do */
    return cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    /* Sub class must do */
    return cAtErrorNotImplemented;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeUnknown;
    }

static void Debug(AtSerdesController self)
    {
	AtUnused(self);
    }

static void StatusClear(AtSerdesController self)
    {
    AtSerdesControllerLinkStatus(self);
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
	AtUnused(enable);
	AtUnused(loopbackMode);
	AtUnused(self);
    return cAtTrue;
    }

static AtPrbsEngine PrbsEngine(AtSerdesController self)
    {
    if (self->prbsEngine == NULL)
        {
        self->prbsEngine = mMethodsGet(self)->PrbsEngineCreate(self);
        if (self->prbsEngine)
            AtPrbsEngineInit(self->prbsEngine);
        }

    return self->prbsEngine;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
	AtUnused(self);
    /* Concrete must know */
    return NULL;
    }

static eAtRet PrbsEngineTypeSet(AtSerdesController self, eAtSerdesPrbsEngineType type)
    {
    AtUnused(self);
    if (type == cAtSerdesPrbsEngineTypeFramed)
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtSerdesPrbsEngineType PrbsEngineTypeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesPrbsEngineTypeFramed;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSerdesController);
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
	AtUnused(self);
    /* Let sub class determine */
    return cAtFalse;
    }

static eAtRet TimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
	AtUnused(timingMode);
	AtUnused(self);
    /* Sub class must do */
    return cAtErrorModeNotSupport;
    }

static eAtSerdesTimingMode TimingModeGet(AtSerdesController self)
    {
	AtUnused(self);
    return cAtSerdesTimingModeAuto;
    }

static eAtRet HwIdSet(AtSerdesController self, uint32 hwId)
    {
    self->hwId = hwId;
    return cAtOk;
    }

static uint32 HwIdGet(AtSerdesController self)
    {
    if (self->hwId == cInvalidUint32)
        return AtSerdesControllerIdGet(self);
    return self->hwId;
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
	AtUnused(param);
	AtUnused(self);
    /* Let concrete determine */
    return cAtFalse;
    }

static uint32 AlarmGet(AtSerdesController self)
    {
	AtUnused(self);
    /* Let concrete determine */
    return cAtSerdesAlarmTypeLinkDown;
    }

static uint32 AlarmHistoryGet(AtSerdesController self)
    {
    AtUnused(self);
    /* Let concrete determine */
    return 0;
    }

static uint32 AlarmHistoryClear(AtSerdesController self)
    {
    AtUnused(self);
    /* Let concrete determine */
    return 0;
    }

static eAtRet InterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(alarmMask);
    AtUnused(enableMask);
    /* Let concrete class do */
    return 0;
    }

static uint32 InterruptMaskGet(AtSerdesController self)
    {
    AtUnused(self);
    /* Let concrete determine */
    return 0;
    }

static uint32 SupportedInterruptMask(AtSerdesController self)
    {
    AtUnused(self);
    return (cAtSerdesAlarmTypeLos        |
            cAtSerdesAlarmTypeTxFault    |
            cAtSerdesAlarmTypeRxFault    |
            cAtSerdesAlarmTypeNotAligned |
            cAtSerdesAlarmTypeNotSync    |
            cAtSerdesAlarmTypeLinkDown);
    }

static eAtRet RxAlarmForce(AtSerdesController self, uint32 alarmType)
    {
    AtUnused(alarmType);
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnforce(AtSerdesController self, uint32 alarmType)
    {
    AtUnused(alarmType);
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static uint32 RxForcedAlarmGet(AtSerdesController self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return no alarm */
    return 0;
    }

static uint32 RxForcableAlarmsGet(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static AtSerdesController *AllLanesCreate(AtSerdesController self, uint32 numLanes)
    {
    uint32 memorySize = sizeof(AtSerdesController) * numLanes;
    AtSerdesController *lanes;
    uint32 laneId;

    /* Allocate memory to hold all lanes */
    lanes = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(lanes, 0, memorySize);

    for (laneId = 0; laneId < numLanes; laneId++)
        {
        uint32 createdLane_i;

        /* Lane must be created successfully */
        lanes[laneId] = mMethodsGet(self)->LaneObjectCreate(self, laneId);
        if (lanes[laneId])
            {
            lanes[laneId]->parent = self;
            continue;
            }

        /* Cleanup on failure */
        for (createdLane_i = 0; createdLane_i < laneId; createdLane_i++)
            AtObjectDelete((AtObject)lanes[createdLane_i]);
        AtOsalMemFree(lanes);
        return NULL;
        }

    return lanes;
    }

static eAtRet LanesSetup(AtSerdesController self)
    {
    uint32 numLanes = AtSerdesControllerNumLanes(self);

    if (numLanes == 0)
        return cAtOk;

    self->lanes = AllLanesCreate(self, numLanes);
    if (self->lanes == NULL)
        return cAtErrorRsrcNoAvail;

    self->numLanes = numLanes;
    return cAtOk;
    }

static AtSerdesController LaneGet(AtSerdesController self, uint32 laneId)
    {
    if (self->lanes == NULL)
        LanesSetup(self);

    if (self->lanes == NULL)
        return NULL;

    if (laneId < self->numLanes)
        return self->lanes[laneId];

    return NULL;
    }

static uint32 NumLanes(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static AtSerdesController ParentGet(AtSerdesController self)
    {
    return self->parent;
    }

static const char *Description(AtSerdesController self)
    {
    return AtObjectToString((AtObject)self);
    }

static tSerdesControllerEventListener *EventListenerCreate(void* userData, const tAtSerdesControllerListener *callbacks)
    {
    uint32 memorySize = sizeof(tSerdesControllerEventListener);
    tSerdesControllerEventListener *newListener = AtOsalMemAlloc(memorySize);
    if (newListener == NULL)
        return NULL;

    /* Need to alloc memory to cache callback */
    memorySize = sizeof(tAtSerdesControllerListener);
    newListener->callbacks = AtOsalMemAlloc(memorySize);
    if (newListener->callbacks == NULL)
        {
        AtOsalMemFree(newListener);
        return NULL;
        }

    AtOsalMemCpy(newListener->callbacks, callbacks, memorySize);
    newListener->userData  = userData;

    return newListener;
    }

static eBool CallbacksAreSame(const tAtSerdesControllerListener *callback1, const tAtSerdesControllerListener *callback2)
    {
    return (AtOsalMemCmp(callback1, callback2, sizeof(tAtSerdesControllerListener)) == 0) ? cAtTrue : cAtFalse;
    }

static tSerdesControllerEventListener *EventListenerFind(AtSerdesController self, void* userData, const tAtSerdesControllerListener *callbacks, uint32 *listenerIndex)
    {
    uint32 listener_i;

    if (self->listeners == NULL)
        return NULL;

    for (listener_i = 0; listener_i < AtListLengthGet(self->listeners); listener_i++)
        {
        tSerdesControllerEventListener *aListener = (tSerdesControllerEventListener *)AtListObjectGet(self->listeners, listener_i);

        if ((aListener->userData == userData) && CallbacksAreSame(aListener->callbacks, callbacks))
            {
            if (listenerIndex)
                *listenerIndex = listener_i;
            return aListener;
            }
        }

    return NULL;
    }

static AtList Listeners(AtSerdesController self)
    {
    if (self->listeners == NULL)
        self->listeners = AtListCreate(0);
    return self->listeners;
    }

static eAtRet ListenerAdd(AtSerdesController self, void *userData, const tAtSerdesControllerListener *callback)
    {
    /* Already add, ignore */
    if (EventListenerFind(self, userData, callback, NULL))
        return cAtOk;

    return AtListObjectAdd(Listeners(self), (AtObject)EventListenerCreate(userData, callback));
    }

static void EventListenerDelete(tSerdesControllerEventListener* eventListener)
    {
    if (eventListener->callbacks)
        AtOsalMemFree((void*)(eventListener->callbacks));

    AtOsalMemFree(eventListener);
    }

static eAtRet ListenerRemove(AtSerdesController self, void *listener, const tAtSerdesControllerListener *callback)
    {
    uint32 listenerIndex;

    if (EventListenerFind(self, listener, callback, &listenerIndex))
        {
        tSerdesControllerEventListener* wrapListener = (tSerdesControllerEventListener *)AtListObjectRemoveAtIndex((AtList)(mThis(self)->listeners), listenerIndex);
        EventListenerDelete(wrapListener);
        }

    return cAtOk;
    }

static eAtRet Reset(AtSerdesController self)
    {
    /* Let concrete determine */
    AtUnused(self);
    return cAtOk;
    }

static eAtRet RxReset(AtSerdesController self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxReset(AtSerdesController self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtUnused(self);
    return NULL;
    }

static AtDrp DrpObjectCreate(AtSerdesController self)
    {
    AtUnused(self);
    return NULL;
    }

static AtDrp Drp(AtSerdesController self)
    {
    if (self->drp == NULL)
        self->drp = mMethodsGet(self)->DrpObjectCreate(self);
    return self->drp;
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeUnknown;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtErrorModeNotSupport;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeUnknown;
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtFalse;
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void DeleteAllListeners(AtObject self)
    {
    AtSerdesController engine = mThis(self);
    AtIterator iterator;

    if (engine->listeners == NULL)
        return;

    /* Delete all listeners in list */
    iterator = AtListIteratorCreate(engine->listeners);
    if (iterator)
        {
        tSerdesControllerEventListener *aListener;
        while((aListener = (tSerdesControllerEventListener *)AtIteratorNext(iterator)) != NULL)
            EventListenerDelete(aListener);
        }
    AtObjectDelete((AtObject)iterator);

    /* Delete event listener list */
    AtObjectDelete((AtObject)(engine->listeners));
    engine->listeners = NULL;
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    static char str[64];
    AtSerdesController serdes = (AtSerdesController)self;
    AtChannel channel = AtSerdesControllerPhysicalPortGet(serdes);
    AtDevice dev = AtChannelDeviceGet(channel);

    if (channel)
        {
        AtSnprintf(description, sizeof(description), "%s.%s_serdes",
                   AtChannelTypeString(channel),
                   AtChannelIdString(channel));
        }
    else
        AtSnprintf(description, sizeof(description), "serdes.%d", AtSerdesControllerIdGet(serdes) + 1);

    AtSprintf(str, "%s%s", AtDeviceIdToString(dev), description);
    return str;
    }

static eBool IsGood(AtSerdesController self)
    {
    return (AtSerdesControllerAlarmGet(self) == cAtSerdesAlarmTypeNone) ? cAtTrue : cAtFalse;
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    AtUnused(self);
    AtUnused(powerDown);
    return cAtErrorNotImplemented;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StandbyCacheSize(AtSerdesController self)
    {
    AtUnused(self);
    return sizeof(tAtSerdesControllerStandbyCache);
    }

static void **StandbyCacheMemoryAddress(AtSerdesController self)
    {
    AtUnused(self);
    return NULL;
    }

static void *StandbyCacheCreate(AtSerdesController self)
    {
    void *newCache;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 cacheSize = mMethodsGet(self)->StandbyCacheSize(self);

    if (cacheSize == 0)
        return NULL;

    newCache = mMethodsGet(osal)->MemAlloc(osal, cacheSize);
    if (newCache == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, newCache, 0, cacheSize);
    return newCache;
    }

static void StandbyCacheDelete(AtObject self)
    {
    void **cache = mMethodsGet(mThis(self))->StandbyCacheMemoryAddress(mThis(self));

    if (cache)
        {
        AtOsalMemFree(*cache);
        *cache = NULL;
        }
    }

static void *StandbyCacheGet(AtSerdesController self)
    {
    void **memory;

    if (AtSerdesControllerAccessible(self))
        return NULL;

    memory = mMethodsGet(self)->StandbyCacheMemoryAddress(self);
    if (memory == NULL)
        return NULL;

    if ((*memory) == NULL)
        *memory = StandbyCacheCreate(self);

    return *memory;
    }

static void CachePowerDown(AtSerdesController self, eBool powerDown)
    {
    tAtSerdesControllerStandbyCache *cache = AtSerdesControllerStandbyCacheGet(self);
    if (cache)
        cache->powerDown = powerDown;
    }

static eBool CachePowerIsDown(AtSerdesController self)
    {
    tAtSerdesControllerStandbyCache *cache = AtSerdesControllerStandbyCacheGet(self);
    return (eBool)(cache ? cache->powerDown : cAtFalse);
    }

static AtEyeScanController EyeScanControllerCreateWithLogic(AtSerdesController self)
    {
    uint32 drpBaseAddress = mMethodsGet(self)->EyeScanDrpBaseAddress(self);

    switch (self->eyescanLogic)
        {
        case cAtSerdesControllerEyescanStm:
            return AtEyeScanControllerStmNew(self, drpBaseAddress);
        case cAtSerdesControllerEyescanXaui:
            return AtEyeScanControllerXauiNew(self, drpBaseAddress);
        case cAtSerdesControllerEyescanGty:
            return AtEyeScanControllerGthE3New(self, drpBaseAddress);
        case cAtSerdesControllerEyescanGtyE3:
            return AtEyeScanControllerGtyE3New(self, drpBaseAddress);
        case cAtSerdesControllerEyescanGthE3:
            return AtEyeScanControllerGthE3New(self, drpBaseAddress);

        case cAtSerdesControllerEyescanAutoSelect:
        default:
            return mMethodsGet(self)->EyeScanControllerObjectCreate(self, drpBaseAddress);
        }
    }

static AtEyeScanController EyeScanControllerGet(AtSerdesController self)
    {
    if (self->eyeScanController)
        return self->eyeScanController;

    if (mMethodsGet(self)->EyeScanIsSupported(self))
        self->eyeScanController = EyeScanControllerCreateWithLogic(self);

    return self->eyeScanController;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 baseAddress)
    {
    AtUnused(self);
    AtUnused(baseAddress);
    /* Let subclass handle create eyescan controller. */
    return NULL;
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static void WillDeleteNotify(AtSerdesController self)
    {
    uint32 i;

    if ((self == NULL) || (self->listeners == NULL))
        return;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tSerdesControllerEventListener *listener = (tSerdesControllerEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks && listener->callbacks->WillDelete)
            listener->callbacks->WillDelete(self, listener->userData);
        }
    }

static void WillDelete(AtObject self)
    {
    WillDeleteNotify(mThis(self));
    }

static void DeleteAllLanes(AtSerdesController self)
    {
    uint32 lane_i;

    if (self->numLanes == 0)
        return;

    for (lane_i = 0; lane_i < self->numLanes; lane_i++)
        {
        AtObjectDelete((AtObject)self->lanes[lane_i]);
        self->lanes[lane_i] = NULL;
        }

    AtOsalMemFree(self->lanes);
    self->lanes = NULL;
    }

static void PropertyListenerDelete(AtObject listener)
    {
    AtOsalMemFree(listener);
    }

static void PropertyListenerListDelete(AtSerdesController self)
    {
    AtListDeleteWithObjectHandler(self->propertyListeners, PropertyListenerDelete);
    self->propertyListeners = NULL;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->prbsEngine);
    mThis(self)->prbsEngine = NULL;
    AtObjectDelete((AtObject)mThis(self)->eyeScanController);
    mThis(self)->eyeScanController = NULL;
    DeleteAllListeners(self);
    StandbyCacheDelete(self);
    DeleteAllLanes(mThis(self));
    AtDrpDelete(mThis(self)->drp);
    mThis(self)->drp = NULL;

    PropertyListenerListDelete((AtSerdesController)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSerdesController object = (AtSerdesController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeString(encoder, AtObjectToString(self), "self");
    mEncodeObjectDescription(physicalPort);
    mEncodeObjectDescription(prbsEngine);
    mEncodeUInt(serdesId);
    mEncodeUInt(hwId);
    mEncodeNone(listeners);
    mEncodeObject(eyeScanController);
    mEncodeObjectDescription(manager);
    mEncodeUInt(asyncInitState);
    mEncodeNone(propertyListeners);
    AtCoderEncodeString(encoder, AtDrpToString(object->drp), "drp");
    mEncodeUInt(eyescanLogic);
    mEncodeObjects(lanes, object->numLanes);
    mEncodeObjectDescription(parent);
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    /* Concrete class must be able to determine if value is in range */
    AtUnused(self);
    AtUnused(param);
    AtUnused(value);
    return cAtTrue;
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool Controllable(AtSerdesController self)
    {
    /* Trick here, its purpose is to reuse logger macro */
    if (self == NULL)
        return cAtTrue;

    return AtSerdesControllerIsControllable(self);
    }

static AtSerdesController LaneObjectCreate(AtSerdesController self, uint32 laneIdx)
    {
    AtUnused(self);
    AtUnused(laneIdx);
    return NULL;
    }

static eBool CanResetWhenPowerDown(AtSerdesController self)
    {
    /* Return cAtTrue to avoid affecting to existing projects */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 BaseAddress(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static AtSerdesManager ManagerGet(AtSerdesController self)
    {
    return self->manager;
    }

static eAtRet ManagerSet(AtSerdesController self, AtSerdesManager manager)
    {
    self->manager = manager;
    return cAtOk;
    }

static AtModule ModuleGet(AtSerdesController self, eAtModule moduleId)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    AtDevice device;

    if (manager == NULL)
        return NULL;

    device = AtSerdesManagerDeviceGet(manager);
    if (device)
        return AtDeviceModuleGet(device, moduleId);

    return NULL;
    }

static uint32 Read(AtSerdesController self, uint32 address, eAtModule moduleId)
    {
    AtChannel channel;
    AtModule module;

    channel = AtSerdesControllerPhysicalPortGet(self);
    if (channel)
        return mChannelHwRead(channel, address, moduleId);

    module = ModuleGet(self, moduleId);
    if (module)
        return mModuleHwRead(module, address);

    return cInvalidValue;
    }

static void Write(AtSerdesController self, uint32 address, uint32 value, eAtModule moduleId)
    {
    AtChannel channel;
    AtModule module;

    channel = AtSerdesControllerPhysicalPortGet(self);
    if (channel)
        {
        mChannelHwWrite(channel, address, value, moduleId);
        return;
        }

    module = ModuleGet(self, moduleId);
    if (module)
        mModuleHwWrite(module, address, value);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    return self->physicalPort;
    }

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeUnknown;
    }

static float SpeedInGbps(AtSerdesController self)
    {
    /* Let concrete class determinate, It will be apply for serdes multi-rate */
    AtUnused(self);
    return 0.0;
    }

static eAtRet LoopbackSet(AtSerdesController self, eAtLoopbackMode loopback)
    {
    eAtRet ret = cAtOk;

    /* Just one mode should be enabled at a time */
    switch (loopback)
        {
        case cAtLoopbackModeLocal:
            ret |= AtSerdesControllerLoopbackEnable(self, cAtLoopbackModeRemote, cAtFalse);
            ret |= AtSerdesControllerLoopbackEnable(self, cAtLoopbackModeLocal, cAtTrue);
            break;

        case cAtLoopbackModeRemote:
            if (!mMethodsGet(self)->CanLoopback(self, loopback, cAtTrue))
                return cAtErrorModeNotSupport;
            ret |= AtSerdesControllerLoopbackEnable(self, cAtLoopbackModeLocal, cAtFalse);
            ret |= AtSerdesControllerLoopbackEnable(self, cAtLoopbackModeRemote, cAtTrue);
            break;

        case cAtLoopbackModeRelease:
            ret |= AtSerdesControllerLoopbackEnable(self, cAtLoopbackModeLocal, cAtFalse);
            ret |= AtSerdesControllerLoopbackEnable(self, cAtLoopbackModeRemote, cAtFalse);
            break;

        case cAtLoopbackModeDual:
        default:
            return cAtErrorModeNotSupport;
        }

    return ret;
    }

static eAtLoopbackMode LoopbackGet(AtSerdesController self)
    {
    if (AtSerdesControllerLoopbackIsEnabled(self, cAtLoopbackModeLocal))
        return cAtLoopbackModeLocal;

    if (AtSerdesControllerLoopbackIsEnabled(self, cAtLoopbackModeRemote))
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static void EyescanLogicSet(AtSerdesController self, eAtSerdesControllerEyescanLogic logic)
    {
    if (logic == self->eyescanLogic)
        return;

    /* Delete eye-scan controller so that it can be recreated by new mode */
    AtObjectDelete((AtObject)self->eyeScanController);
    self->eyeScanController = NULL;
    self->eyescanLogic = logic;
    }

static eAtSerdesControllerEyescanLogic EyescanLogicGet(AtSerdesController self)
    {
    return self->eyescanLogic;
    }

static eAtRet LayerLoopbackSet(AtSerdesController self, eAtSerdesPhysicalLayer layer, eAtLoopbackMode loopbackMode)
    {
    AtUnused(self);
    AtUnused(layer);
    AtUnused(loopbackMode);
    return cAtErrorNotImplemented;
    }

static eAtLoopbackMode LayerLoopbackGet(AtSerdesController self, eAtSerdesPhysicalLayer layer)
    {
    AtUnused(self);
    AtUnused(layer);
    return cAtLoopbackModeRelease;
    }

static void CacheModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    tAtSerdesControllerStandbyCache *cache = AtSerdesControllerStandbyCacheGet(self);
    if (cache)
        cache->mode = mode;
    }

static tAtAsyncOperationFunc AsyncOperationGet(AtSerdesController self, uint32 state)
    {
    if (state >= 1)
        return NULL;

    return (tAtAsyncOperationFunc)(mMethodsGet(self)->Init);
    }

static AtList ListenerList(AtSerdesController self)
    {
    if (self->propertyListeners == NULL)
        self->propertyListeners = AtListCreate(0);

    return self->propertyListeners;
    }

static tListenerWraper* NewListenerWrapper(void* userData, const tAtSerdesControllerPropertyListener* listener)
    {
    tListenerWraper* newWrapper = AtOsalMemAlloc(sizeof(tListenerWraper));
    if (newWrapper == NULL)
        return NULL;

    AtOsalMemInit(newWrapper, 0, sizeof(tListenerWraper));
    newWrapper->userData = userData;
    newWrapper->listener.ModeDidChange = listener->ModeDidChange;
    return newWrapper;
    }

static tListenerWraper* ListenerWrapperSearch(AtSerdesController self, void* userData, const tAtSerdesControllerPropertyListener* listener)
    {
    AtIterator iterator;
    tListenerWraper* wrapper = NULL;

    if (self->propertyListeners == NULL)
        return NULL;

    iterator = AtListIteratorCreate(self->propertyListeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if ((wrapper->userData == userData) &&
            (wrapper->listener.ModeDidChange == listener->ModeDidChange))
            break;
        }

    AtObjectDelete((AtObject)iterator);
    return wrapper;
    }

static void ModeDidChangeNotify(AtSerdesController self, eAtSerdesMode mode)
    {
    tListenerWraper* wrapper;
    AtIterator iterator;

    if (self->propertyListeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->propertyListeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->listener.ModeDidChange)
            wrapper->listener.ModeDidChange(self, mode, wrapper->userData);
        }

    AtObjectDelete((AtObject)iterator);
    }

static AtDevice DeviceGet(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    AtChannel channel;

    if (manager)
        return AtSerdesManagerDeviceGet(manager);

    channel = AtSerdesControllerPhysicalPortGet(self);
    if (channel)
        return AtChannelDeviceGet(channel);

    return NULL;
    }

static AtQuerier QuerierGet(AtSerdesController self)
    {
    AtUnused(self);
    return AtSerdesControllerSharedQuerier();
    }

static const char *IdString(AtSerdesController self)
    {
    static char idString[32];
    AtSerdesController parent = AtSerdesControllerParentGet(self);

    if (parent)
        AtSnprintf(idString, sizeof(idString), "lane#%u.%u", AtSerdesControllerIdGet(parent) + 1, AtSerdesControllerIdGet(self) + 1);
    else
        AtSnprintf(idString, sizeof(idString), "%u", AtSerdesControllerIdGet(self) + 1);

    return idString;
    }

static eBool HasAutoResetPpmThreshold(AtSerdesController self)
    {
    /* Concrete should know */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 AutoResetPpmThresholdGet(AtSerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet AutoResetPpmThresholdSet(AtSerdesController self, uint32 ppmThreshold)
    {
    AtUnused(self);
    AtUnused(ppmThreshold);
    return cAtErrorModeNotSupport;
    }

static uint32 AutoResetPpmThresholdMax(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtSerdesMode OverSamplingMode(AtSerdesController self)
    {
    return AtSerdesControllerModeGet(self);
    }

static eAtRet DoModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    eAtRet ret = cAtErrorNullPointer;

    if (self)
        {
        CacheModeSet(self, mode);
        ret = (self)->methods->ModeSet(self, mode);

        if (AtDriverShouldLog(mLogLevel(ret)))
            {
            AtDriverLogLock();
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret),
                                    AtSourceLocation, "%s([%s], %u) = %s\r\n", AtFunction, AtObjectToString((AtObject)self), mode, AtRet2String(ret));
            AtDriverLogUnLock();
            }

        if (ret == cAtOk)
            ModeDidChangeNotify(self, mode);
        }
    else
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);

    return ret;
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtFalse;
    }

static eAtRet EyeScanReset(AtSerdesController self, uint32 laneId)
    {
    AtUnused(self);
    AtUnused(laneId);
    return cAtOk;
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, WillDelete);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, FpgaEnable);
        mMethodOverride(m_methods, FpgaIsEnabled);
        mMethodOverride(m_methods, PhysicalParamGet);
        mMethodOverride(m_methods, PhysicalParamSet);
        mMethodOverride(m_methods, HwParameterId);
        mMethodOverride(m_methods, LoopbackEnable);
        mMethodOverride(m_methods, LoopbackIsEnabled);
        mMethodOverride(m_methods, LoopbackSet);
        mMethodOverride(m_methods, LoopbackGet);
        mMethodOverride(m_methods, CanLoopback);
        mMethodOverride(m_methods, LayerLoopbackSet);
        mMethodOverride(m_methods, LayerLoopbackGet);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, StatusClear);
        mMethodOverride(m_methods, PllIsLocked);
        mMethodOverride(m_methods, PllChanged);
        mMethodOverride(m_methods, LinkStatus);
        mMethodOverride(m_methods, PrbsEngineCreate);
        mMethodOverride(m_methods, CanControlTimingMode);
        mMethodOverride(m_methods, TimingModeSet);
        mMethodOverride(m_methods, TimingModeGet);
        mMethodOverride(m_methods, PhysicalParamIsSupported);
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, SupportedInterruptMask);
        mMethodOverride(m_methods, Reset);
        mMethodOverride(m_methods, RxReset);
        mMethodOverride(m_methods, TxReset);
        mMethodOverride(m_methods, PrbsEngine);
        mMethodOverride(m_methods, Mdio);
        mMethodOverride(m_methods, Drp);
        mMethodOverride(m_methods, DrpObjectCreate);
        mMethodOverride(m_methods, DefaultEqualizerMode);
        mMethodOverride(m_methods, EqualizerModeSet);
        mMethodOverride(m_methods, EqualizerModeGet);
        mMethodOverride(m_methods, EqualizerModeIsSupported);
        mMethodOverride(m_methods, EqualizerCanControl);
        mMethodOverride(m_methods, CanEnable);
        mMethodOverride(m_methods, ListenerAdd);
        mMethodOverride(m_methods, ListenerRemove);
        mMethodOverride(m_methods, RxAlarmForce);
        mMethodOverride(m_methods, RxAlarmUnforce);
        mMethodOverride(m_methods, RxForcedAlarmGet);
        mMethodOverride(m_methods, RxForcableAlarmsGet);
        mMethodOverride(m_methods, HwIdGet);
        mMethodOverride(m_methods, IsGood);
        mMethodOverride(m_methods, PowerDown);
        mMethodOverride(m_methods, PowerIsDown);
        mMethodOverride(m_methods, PowerCanControl);
        mMethodOverride(m_methods, CanResetWhenPowerDown);
        mMethodOverride(m_methods, StandbyCacheSize);
        mMethodOverride(m_methods, StandbyCacheMemoryAddress);
        mMethodOverride(m_methods, PhysicalParamValueIsInRange);
        mMethodOverride(m_methods, IsControllable);
        mMethodOverride(m_methods, ModeSet);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, DefaultMode);
        mMethodOverride(m_methods, EyeScanControllerGet);
        mMethodOverride(m_methods, EyeScanControllerObjectCreate);
        mMethodOverride(m_methods, EyeScanIsSupported);
        mMethodOverride(m_methods, EyeScanDrpBaseAddress);
        mMethodOverride(m_methods, SpeedInGbps);
        mMethodOverride(m_methods, LaneGet);
        mMethodOverride(m_methods, NumLanes);
        mMethodOverride(m_methods, ParentGet);
        mMethodOverride(m_methods, LaneObjectCreate);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        mMethodOverride(m_methods, PhysicalPortGet);
        mMethodOverride(m_methods, Description);
        mMethodOverride(m_methods, AsyncOperationGet);
        mMethodOverride(m_methods, ParentGet);
        mMethodOverride(m_methods, QuerierGet);
        mMethodOverride(m_methods, IdString);
        mMethodOverride(m_methods, OverSamplingMode);
        mMethodOverride(m_methods, HasAutoResetPpmThreshold);
        mMethodOverride(m_methods, AutoResetPpmThresholdGet);
        mMethodOverride(m_methods, AutoResetPpmThresholdSet);
        mMethodOverride(m_methods, AutoResetPpmThresholdMax);
        mMethodOverride(m_methods, PrbsEngineTypeSet);
        mMethodOverride(m_methods, PrbsEngineTypeGet);
        mMethodOverride(m_methods, ModeIsSupported);
        mMethodOverride(m_methods, EyeScanReset);
        }

    mMethodsSet(self, &m_methods);
    }

AtSerdesController AtSerdesControllerObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    self->physicalPort = physicalPort;
    self->serdesId     = serdesId;
    self->hwId         = cInvalidUint32;

    return self;
    }

AtSerdesController AtSerdesControllerObjectInitWithManager(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    AtSerdesController controller = AtSerdesControllerObjectInit(self, physicalPort, serdesId);
    if (controller)
        controller->manager = manager;
    return controller;
    }

uint32 AtSerdesControllerRead(AtSerdesController self, uint32 address, eAtModule moduleId)
    {
    if (self)
        return mMethodsGet(self)->Read(self, address, moduleId);
    return cInvalidUint32;
    }

void AtSerdesControllerWrite(AtSerdesController self, uint32 address, uint32 value, eAtModule moduleId)
    {
    if (self)
        mMethodsGet(self)->Write(self, address, value, moduleId);
    }

uint32 AtSerdesControllerHwParameterId(AtSerdesController self, eAtSerdesParam parameterId)
    {
    if (self)
        return mMethodsGet(self)->HwParameterId(self, parameterId);
    return cInvalidValue;
    }

eAtRet AtSerdesControllerReset(AtSerdesController self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* Nothing to do */
    if (!Controllable(self))
        return cAtOk;

    if (mMethodsGet(self)->CanResetWhenPowerDown(self))
        return mMethodsGet(self)->Reset(self);

    if (AtSerdesControllerPowerIsDown(self))
        return cAtOk;

    return mMethodsGet(self)->Reset(self);
    }

const char *AtSerdesControllerDefaultIdString(AtSerdesController self)
    {
    if (self)
        return IdString(self);
    return NULL;
    }

/*
 * Reset rx SERDES
 *
 * @param self This controller
 *
 * @return AT return codes.
 */
eAtRet AtSerdesControllerRxReset(AtSerdesController self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->RxReset(self);
    }

/*
 * Reset tx SERDES
 *
 * @param self This controller
 *
 * @return AT return codes.
 */
eAtRet AtSerdesControllerTxReset(AtSerdesController self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->TxReset(self);
    }

eBool AtSerdesControllerCanEnable(AtSerdesController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->CanEnable(self, enable);
    return cAtFalse;
    }

void AtSerdesControllerAlarmNotify(AtSerdesController self, uint32 alarmType, uint32 currentStatus)
    {
    uint32 i;

    if ((self == NULL) || (self->listeners == NULL))
        return;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tSerdesControllerEventListener *listener = (tSerdesControllerEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks && listener->callbacks->AlarmNotify)
            listener->callbacks->AlarmNotify(self, listener->userData, alarmType, currentStatus);
        }
    }

void AtSerdesControllerStatusClear(AtSerdesController self)
    {
    if (self)
        mMethodsGet(self)->StatusClear(self);
    }

float AtSerdesControllerSpeedInGbps(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->SpeedInGbps(self);
    return 0.0;
    }

/*
 * Force RX alarm
 *
 * @param self This SERDES controller.
 * @param alarmType Alarm to force. @ref eAtSerdesAlarmType "SERDES alarm types"
 *
 * @return AT return codes
 */
eAtRet AtSerdesControllerRxAlarmForce(AtSerdesController self, uint32 alarmType)
    {
    mNumericalAttributeSet(RxAlarmForce, alarmType);
    }

/*
 * Un-force RX alarm
 *
 * @param self This SERDES controller.
 * @param alarmType Alarm to un-force. @ref eAtSerdesAlarmType "SERDES alarm types"
 *
 * @return AT return codes
 */
eAtRet AtSerdesControllerRxAlarmUnforce(AtSerdesController self, uint32 alarmType)
    {
    mNumericalAttributeSet(RxAlarmUnforce, alarmType);
    }

/*
 * Get alarms that are being forced at RX side
 *
 * @param self This SERDES controller.
 *
 * @return Alarms that are being forced. @ref eAtSerdesAlarmType "SERDES alarm types"
 */
uint32 AtSerdesControllerRxForcedAlarmGet(AtSerdesController self)
    {
    mAttributeGet(RxForcedAlarmGet, uint32, 0);
    }

/*
 * Get forcable alarms at RX side
 *
 * @param self This SERDES controller.
 *
 * @return Alarms that can be forced. @ref eAtSerdesAlarmType "SERDES alarm types"
 */
uint32 AtSerdesControllerRxForcableAlarmsGet(AtSerdesController self)
    {
    mAttributeGet(RxForcableAlarmsGet, uint32, 0);
    }

eBool AtSerdesControllerIsGood(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->IsGood(self);
    return cAtFalse;
    }

eAtRet AtSerdesControllerFpgaEnable(AtSerdesController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->FpgaEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool AtSerdesControllerFpgaIsEnabled(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->FpgaIsEnabled(self);
    return cAtFalse;
    }

void *AtSerdesControllerStandbyCacheGet(AtSerdesController self)
    {
    if (self)
        return StandbyCacheGet(self);
    return NULL;
    }

uint32 AtSerdesControllerHwIdGet(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->HwIdGet(self);
    return 0;
    }

void AtSerdesControllerPhysicalPortSet(AtSerdesController self, AtChannel physicalPort)
    {
    if (self)
        self->physicalPort = physicalPort;
    }

eBool AtSerdesControllerModeIsOcn(eAtSerdesMode mode)
    {
    switch ((uint32)mode)
        {
        case cAtSerdesModeStm0:   return cAtTrue;
        case cAtSerdesModeStm1:   return cAtTrue;
        case cAtSerdesModeStm4:   return cAtTrue;
        case cAtSerdesModeStm16:  return cAtTrue;
        case cAtSerdesModeStm64:  return cAtTrue;
        case cAtSerdesModeOcn:    return cAtTrue;
        default: return cAtFalse;
        }
    }

eBool AtSerdesControllerModeIsEthernet(eAtSerdesMode mode)
    {
    switch ((uint32)mode)
        {
        case cAtSerdesModeEth10M   : return cAtTrue;
        case cAtSerdesModeEth100M  : return cAtTrue;
        case cAtSerdesModeEth2500M : return cAtTrue;
        case cAtSerdesModeEth1G    : return cAtTrue;
        case cAtSerdesModeEth10G   : return cAtTrue;
        case cAtSerdesModeEth40G   : return cAtTrue;
        case cAtSerdesModeGe       : return cAtTrue;
        default: return cAtFalse;
        }
    }

eAtRet AtSerdesControllerHwIdSet(AtSerdesController self, uint32 hwId)
    {
    if (self)
        return HwIdSet(self, hwId);
    return cAtErrorNullPointer;
    }

eBool AtSerdesControllerPhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (self)
        return mMethodsGet(mThis(self))->PhysicalParamValueIsInRange(self, param, value);
    return cAtFalse;
    }

uint32 AtSerdesControllerBaseAddress(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0;
    }

eBool AtSerdesControllerModeIsValid(eAtSerdesMode mode)
    {
    switch (mode)
        {
        case cAtSerdesModeStm0     : return cAtTrue;
        case cAtSerdesModeStm1     : return cAtTrue;
        case cAtSerdesModeStm4     : return cAtTrue;
        case cAtSerdesModeStm16    : return cAtTrue;
        case cAtSerdesModeStm64    : return cAtTrue;
        case cAtSerdesModeEth10M   : return cAtTrue;
        case cAtSerdesModeEth100M  : return cAtTrue;
        case cAtSerdesModeEth2500M : return cAtTrue;
        case cAtSerdesModeEth1G    : return cAtTrue;
        case cAtSerdesModeEth10G   : return cAtTrue;
        case cAtSerdesModeEth40G   : return cAtTrue;
        case cAtSerdesModeOcn      : return cAtTrue;
        case cAtSerdesModeGe       : return cAtTrue;
        case cAtSerdesModeUnknown:
        default:
            return cAtFalse;
        }
    }

eAtRet AtSerdesControllerManagerSet(AtSerdesController self, AtSerdesManager manager)
    {
    if (self)
        return ManagerSet(self, manager);
    return cAtErrorNullPointer;
    }

void AtSerdesControllerEyescanLogicSet(AtSerdesController self, eAtSerdesControllerEyescanLogic logic)
    {
    if (self)
        EyescanLogicSet(self, logic);
    }

eAtSerdesControllerEyescanLogic AtSerdesControllerEyescanLogicGet(AtSerdesController self)
    {
    if (self)
        return EyescanLogicGet(self);
    return cAtSerdesControllerEyescanAutoSelect;
    }

eAtRet AtSerdesControllerLayerLoopbackSet(AtSerdesController self, eAtSerdesPhysicalLayer layer, eAtLoopbackMode loopbackMode)
    {
    if (self)
        return mMethodsGet(self)->LayerLoopbackSet(self, layer, loopbackMode);
    return cAtErrorNullPointer;
    }

eAtLoopbackMode AtSerdesControllerLayerLoopbackGet(AtSerdesController self, eAtSerdesPhysicalLayer layer)
    {
    if (self)
        return mMethodsGet(self)->LayerLoopbackGet(self, layer);
    return cAtLoopbackModeRelease;
    }

eAtRet AtSerdesControllerAsyncInit(AtSerdesController self)
    {
    uint32 state = self->asyncInitState;
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;
    tAtAsyncOperationFunc func;
    char stateString[32];

    func = mMethodsGet(self)->AsyncOperationGet(self, state);
    if (func == NULL)
        {
        self->asyncInitState = 0;
        return cAtOk;
        }

    AtSprintf(stateString, "Async state: %d", state);
    AtOsalCurTimeGet(&profileTime);
    ret = func((AtObject)self);

    AtDeviceAccessTimeInMsSinceLastProfileCheck(AtSerdesControllerDeviceGet(self), cAtTrue, &profileTime, AtSourceLocation, stateString);
    AtDeviceAccessTimeCountSinceLastProfileCheck(AtSerdesControllerDeviceGet(self), cAtTrue, AtSourceLocation, stateString);
    if (AtDeviceAsyncRetValIsInState(ret))
        return ret;

    if (ret != cAtOk)
        {
        self->asyncInitState = 0;
        return ret;
        }

    self->asyncInitState++;
    return cAtErrorAgain;
    }

uint32 AtSerdesControllerDrpBaseAddress(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->EyeScanDrpBaseAddress(self);

    return 0;
    }

eAtRet AtSerdesControllerPropertyListenerAdd(AtSerdesController self, const tAtSerdesControllerPropertyListener* listener, void* userData)
    {
    /* Identical listener is existing */
    if (ListenerWrapperSearch(self, userData, listener))
        return cAtOk;

    return AtListObjectAdd(ListenerList(self), (AtObject)NewListenerWrapper(userData, listener));
    }

eAtRet AtSerdesControllerPropertyListenerRemove(AtSerdesController self, const tAtSerdesControllerPropertyListener* listener, void* userData)
    {
    eAtRet ret;
    tListenerWraper* wrapper = ListenerWrapperSearch(self, userData, listener);
    if (wrapper == NULL)
        return cAtOk;

    ret = AtListObjectRemove(self->propertyListeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);
    return ret;
    }

eBool AtSerdesControllerInAccessible(AtSerdesController self)
    {
    if (self)
        return AtDeviceInAccessible(AtSerdesControllerDeviceGet(self));
    return cAtFalse;
    }

eBool AtSerdesControllerAccessible(AtSerdesController self)
    {
    if (self)
        return AtDeviceAccessible(AtSerdesControllerDeviceGet(self));
    return cAtFalse;
    }

eAtSerdesMode AtSerdesControllerOverSamplingMode(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->OverSamplingMode(self);
    return cAtSerdesModeUnknown;
    }

AtQuerier AtSerdesControllerQuerierGet(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->QuerierGet(self);
    return NULL;
    }

eAtRet AtSerdesControllerEyeScanReset(AtSerdesController self, uint32 laneId)
    {
    if (self)
        return mMethodsGet(self)->EyeScanReset(self, laneId);
    return cAtErrorNullPointer;
    }

/**
 * @addtogroup AtSerdesController
 * @{
 */

/**
 * Initialize SERDES controller
 *
 * @param self This controller
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerInit(AtSerdesController self)
    {
    /* Should not touch SERDES that cannot be controlled */
    if (!Controllable(self))
        return cAtOk;

    mNoParamCall(Init, eAtRet, cAtErrorNullPointer);
    }

/**
 * Get physical port of this SERDES
 *
 * @param self This controller
 *
 * @return Physical port
 */
AtChannel AtSerdesControllerPhysicalPortGet(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->PhysicalPortGet(self);
    return NULL;
    }

/**
 * Get controller ID
 *
 * @param self This controller
 *
 * @return Controller ID
 */
uint32 AtSerdesControllerIdGet(AtSerdesController self)
    {
    if (self == NULL)
        return 0xCAFECAFE;
    return self->serdesId;
    }

/**
 * Check if PLL is locked
 *
 * @param self This controller
 *
 * @retval cAtTrue  - If PLL is locked or PLL checking is not applicable
 * @retval cAtFalse - If PLL is not locked
 */
eBool AtSerdesControllerPllIsLocked(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtFalse;

    mAttributeGet(PllIsLocked, eBool, cAtFalse);
    }

/**
 * Enable/disable SERDES
 *
 * @param self This controller
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerEnable(AtSerdesController self, eBool enable)
    {
    if (!Controllable(self))
        return cAtErrorNotControllable;

    mNumericalAttributeSet(Enable, enable);
    }

/**
 * Check if SERDES is enabled or not
 *
 * @param self This controller
 *
 * @return cAtTrue if enable, cAtFalse if disable
 */
eBool AtSerdesControllerIsEnabled(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtFalse;

    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * Set physical parameter
 *
 * @param self This controller
 * @param param @ref eAtSerdesParam "SERDES parameters"
 * @param value Value for this parameter
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (self == NULL)
        return cAtError;

    if (!Controllable(self))
        return cAtErrorNotControllable;

    if (!AtSerdesControllerPhysicalParamIsSupported(self, param))
       return cAtErrorModeNotSupport;

    mTwoParamsAttributeSet(PhysicalParamSet, param, value)
    }

/**
 * Get configured value of physical parameter
 *
 * @param self This controller
 * @param param @ref eAtSerdesParam "SERDES parameters"
 *
 * @return Value of physical parameter
 */
uint32 AtSerdesControllerPhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    if (!Controllable(self))
        return 0;

    if (self)
        return mMethodsGet(self)->PhysicalParamGet(self, param);
    return 0;
    }

/**
 * Check if the input loopback mode is supported or not
 *
 * @param self This controller
 * @param loopbackMode @ref eAtLoopbackMode "Loopback modes"
 * @param enable Enabling
 *
 * @retval cAtTrue if can loopback
 * @retval cAtFalse if cannot loopback
 */
eBool AtSerdesControllerCanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (!Controllable(self))
        return cAtFalse;

    if (self)
        return mMethodsGet(self)->CanLoopback(self, loopbackMode, enable);

    return cAtFalse;
    }

/**
 * Enable/disable loopback
 *
 * @param self This controller
 * @param loopbackMode @ref eAtLoopbackMode "Loopback modes"
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerLoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (self == NULL)
        return cAtError;

    if (!Controllable(self))
        return cAtErrorNotControllable;

    if (!mMethodsGet(self)->CanLoopback(self, loopbackMode, enable))
        return cAtErrorModeNotSupport;

    mTwoParamsAttributeSet(LoopbackEnable, loopbackMode, enable);
    }

/**
 * Check if loopback is enabled or not
 *
 * @param self This controller
 * @param loopbackMode @ref eAtLoopbackMode "Loopback modes"
 *
 * @return cAtTrue if it is enabled, cAtFalse if it is disabled
 */
eBool AtSerdesControllerLoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    if (!Controllable(self))
        return cAtFalse;

    if (self)
        return mMethodsGet(self)->LoopbackIsEnabled(self, loopbackMode);
    return cAtFalse;
    }

/**
 * Show debugging information of SERDES
 *
 * @param self This controller
 */
void AtSerdesControllerDebug(AtSerdesController self)
    {
    if (!Controllable(self))
        return;

    if (self)
        mMethodsGet(self)->Debug(self);
    }

/**
 * Set loopback
 *
 * @param self This controller
 * @param loopback @ref eAtLoopbackMode "Loopback modes"
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerLoopbackSet(AtSerdesController self, eAtLoopbackMode loopback)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!Controllable(self))
        return (loopback == cAtLoopbackModeRelease) ? cAtOk : cAtErrorNotControllable;

    /* Check if loopback mode is supported */
    if ((loopback != cAtLoopbackModeRelease) &&
        (!mMethodsGet(self)->CanLoopback(self, loopback, cAtTrue)))
        return cAtErrorModeNotSupport;

    mNumericalAttributeSet(LoopbackSet, loopback);
    }

/**
 * Check if a specific loopback mode is supported
 *
 * @param self This controller
 * @param loopback @ref eAtLoopbackMode "Loopback modes"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtSerdesControllerLoopbackIsSupported(AtSerdesController self, eAtLoopbackMode loopback)
    {
    if (self == NULL)
        return cAtFalse;

    if (!Controllable(self))
        return cAtFalse;

    if (loopback == cAtLoopbackModeRelease)
        return cAtTrue;

    return mMethodsGet(self)->CanLoopback(self, loopback, cAtTrue);
    }

/**
 * Get current loopback mode
 *
 * @param self This controller
 *
 * @return @ref eAtLoopbackMode "Loopback modes"
 */
eAtLoopbackMode AtSerdesControllerLoopbackGet(AtSerdesController self)
    {
    if (self && Controllable(self))
        return mMethodsGet(self)->LoopbackGet(self);
    return cAtLoopbackModeRelease;
    }

/**
 * Get PRBS engine
 *
 * @param self This controller
 *
 * @return AtPrbsEngine object on success, NULL on failure
 */
AtPrbsEngine AtSerdesControllerPrbsEngine(AtSerdesController self)
    {
    if (!Controllable(self))
        return NULL;

    if (self)
        return mMethodsGet(self)->PrbsEngine(self);

    return NULL;
    }

/**
 * Set SERDES timing mode
 *
 * @param self This controller
 * @param timingMode See @ref eAtSerdesTimingMode "SERDES timing mode"
 * @return AT return code
 */
eAtRet AtSerdesControllerTimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    if (self == NULL)
        return cAtError;

    if (!Controllable(self))
        return cAtErrorNotControllable;

    if (mMethodsGet(self)->CanControlTimingMode(self))
        {
        mNumericalAttributeSet(TimingModeSet, timingMode);
        }

    return cAtErrorModeNotSupport;
    }

/**
 * Check if SERDES timing mode can be controlled
 *
 * @param self This SERDES
 *
 * @retval cAtTrue if this can be controlled
 * @retval cAtFalse if this cannot be controlled
 */
eBool AtSerdesControllerCanControlTimingMode(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtFalse;

    if (self)
        return mMethodsGet(self)->CanControlTimingMode(self);

    return cAtFalse;
    }

/**
 * Get SERDES timing mode
 *
 * @param self This controller
 * @return timingMode See @ref eAtSerdesTimingMode "SERDES timing mode"
 */
eAtSerdesTimingMode AtSerdesControllerTimingModeGet(AtSerdesController self)
    {
    if (self == NULL)
        return cAtSerdesTimingModeUnknown;

    if (!Controllable(self))
        return cAtSerdesTimingModeUnknown;

    if (mMethodsGet(self)->CanControlTimingMode(self))
        return mMethodsGet(self)->TimingModeGet(self);

    /* Normally, timing mode is auto by hardware */
    return cAtSerdesTimingModeAuto;
    }

/**
 * Check if a physical parameter is supported
 *
 * @param self This controller
 * @param param @ref eAtSerdesParam "SERDES parameters"
 *
 * @@return cAtTrue if supported, cAtFalse if not supported
 */
eBool AtSerdesControllerPhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    if (!Controllable(self))
        return cAtFalse;

    mOneParamAttributeGet(PhysicalParamIsSupported, param, eBool, cAtFalse);
    }

/**
 * Get SERDES alarm
 *
 * @param self This controller
 *
 * @return Alarm bit mask @ref eAtSerdesAlarmType "SERDES alarm types"
 */
uint32 AtSerdesControllerAlarmGet(AtSerdesController self)
    {
    if (!Controllable(self))
        return 0;

    mAttributeGet(AlarmGet, uint32, 0);
    }

/**
 * Get SERDES history alarm
 *
 * @param self This controller
 *
 * @return Alarm bit mask @ref eAtSerdesAlarmType "SERDES alarm types"
 */
uint32 AtSerdesControllerAlarmHistoryGet(AtSerdesController self)
    {
    if (!Controllable(self))
        return 0;

    mAttributeGet(AlarmHistoryGet, uint32, 0);
    }

/**
 * Get and clear SERDES alarms
 *
 * @param self This controller
 *
 * @return Alarm bit mask @ref eAtSerdesAlarmType "SERDES alarm types"
 */
uint32 AtSerdesControllerAlarmHistoryClear(AtSerdesController self)
    {
    if (!Controllable(self))
        return 0;
    mAttributeGet(AlarmHistoryClear, uint32, 0);
    }

/**
 * Set SERDES interrupt mask.
 *
 * @param self This controller
 * @param alarmMask Alarm mask @ref eAtSerdesAlarmType "SERDES alarm types"
 * @param enableMask Alarm enable mask @ref eAtSerdesAlarmType "SERDES alarm types"
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerInterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    if (!Controllable(self))
        return cAtErrorNotControllable;

    mTwoParamsAttributeSet(InterruptMaskSet, alarmMask, enableMask);
    }

/**
 * Get all of all interrupt mask that SERDES can support. Only supported
 * interrupt mask can generate interrupt to CPU.
 *
 * @param self This controller
 *
 * @return Interrupt mask that SERDES can support
 */
uint32 AtSerdesControllerSupportedInterruptMask(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtErrorNotControllable;

    if (self)
        return mMethodsGet(self)->SupportedInterruptMask(self);
    return 0;
    }

/**
 * Get SERDES interrupt mask.
 *
 * @param self This controller
 *
 * @return Alarm bit mask @ref eAtSerdesAlarmType "SERDES alarm types"
 */
uint32 AtSerdesControllerInterruptMaskGet(AtSerdesController self)
    {
    if (!Controllable(self))
        return 0;
    mAttributeGet(InterruptMaskGet, uint32, 0);
    }

/**
 * Get HAL to access MDIO
 *
 * @param self This controller
 *
 * @return MDIO
 */
AtMdio AtSerdesControllerMdio(AtSerdesController self)
    {
    if (!Controllable(self))
        return NULL;

    if (self)
        return mMethodsGet(self)->Mdio(self);

    return NULL;
    }

/**
 * Get HAL to access DRP
 *
 * @param self This controller
 *
 * @return HAL
 */
AtDrp AtSerdesControllerDrp(AtSerdesController self)
    {
    if (!Controllable(self))
        return NULL;

    if (self)
        return mMethodsGet(self)->Drp(self);

    return NULL;
    }

/**
 * Get link status
 *
 * @param self This controller
 *
 * @return @ref eAtSerdesLinkStatus "Serdes link status"
 */
eAtSerdesLinkStatus AtSerdesControllerLinkStatus(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtSerdesLinkStatusNotSupported;

    if (self)
        return mMethodsGet(self)->LinkStatus(self);
    return cAtSerdesLinkStatusUnknown;
    }

/**
 * Add listener to a SERDES controller.
 *
 * @param self This SERDES controller
 * @param userData Listening entity from application. When a callback is called,
 *                 this context will be input.
 * @param callback callback function
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerListenerAdd(AtSerdesController self, void *userData, const tAtSerdesControllerListener *callback)
    {
    if (!Controllable(self))
        return cAtErrorNotControllable;

    if (self)
        return mMethodsGet(self)->ListenerAdd(self, userData, callback);
    return cAtErrorNullPointer;
    }

/**
 * Remove listener from a SERDES controller
 *
 * @param self This SERDES controller
 * @param userData Listening entity. See AtSerdesControllerListenerAdd().
 * @param callback callback function
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerListenerRemove(AtSerdesController self, void *userData, const tAtSerdesControllerListener *callback)
    {
    if (!Controllable(self))
        return cAtErrorNotControllable;

    if (self)
        return mMethodsGet(self)->ListenerRemove(self, userData, callback);
    return cAtErrorNullPointer;
    }

/**
 * Set SERDES equalizer mode.
 *
 * @param self This controller
 * @param mode @ref eAtSerdesEqualizerMode "SERDES equalizer mode"
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerEqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    if (!Controllable(self))
        return cAtErrorNotControllable;

    if (!AtSerdesControllerEqualizerModeIsSupported(self, mode))
        return cAtErrorModeNotSupport;

    if (self)
        {
        mNumericalAttributeSet(EqualizerModeSet, mode);
        }

    return cAtErrorNullPointer;
    }

/**
 * Get SERDES equalizer mode.
 *
 * @param self This controller
 *
 * @return @ref eAtSerdesEqualizerMode "SERDES equalizer mode"
 */
eAtSerdesEqualizerMode AtSerdesControllerEqualizerModeGet(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtSerdesEqualizerModeUnknown;

    if (self)
        return mMethodsGet(self)->EqualizerModeGet(self);

    return cAtSerdesEqualizerModeUnknown;
    }

/**
 * Check SERDES equalizer is supported
 *
 * @param self This controller
 * @param mode equalizer mode @ref eAtSerdesEqualizerMode "SERDES equalizer mode"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtSerdesControllerEqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    if (!Controllable(self))
        return cAtFalse;

    if (self)
        return mMethodsGet(self)->EqualizerModeIsSupported(self, mode);

    return cAtFalse;
    }

/**
 * Check if equalizer can be controlled
 *
 * @param self This controller
 *
 * @retval cAtTrue if can control
 * @retval cAtFalse if cannot control
 */
eBool AtSerdesControllerEqualizerCanControl(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtFalse;

    if (self)
        return mMethodsGet(self)->EqualizerCanControl(self);

    return cAtFalse;
    }

/**
 * Power down/up SERDES
 *
 * @param self This controller
 * @param powerDown Power down
 *                  - cAtTrue: to power down
 *                  - cAtFalse: to power up
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerPowerDown(AtSerdesController self, eBool powerDown)
    {
    if (!Controllable(self))
        return cAtErrorNotControllable;

    mNumericalAttributeSetWithCache(PowerDown, powerDown);
    }

/**
 * Check if SERDES is power down
 *
 * @param self This controller
 *
 * @retval cAtTrue If serdes is power down
 * @retval cAtFalse If serdes is power up
 */
eBool AtSerdesControllerPowerIsDown(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtFalse;

    mAttributeGetWithCache(PowerIsDown, eBool, cAtFalse);
    }

/**
 * Check if power can be controlled
 *
 * @param self This controller
 *
 * @retval cAtTrue if can control
 * @retval cAtFalse if cannot control
 */
eBool AtSerdesControllerPowerCanControl(AtSerdesController self)
    {
    if (!Controllable(self))
        return cAtFalse;

    if (self)
        return mMethodsGet(self)->PowerCanControl(self);

    return cAtFalse;
    }

/**
 * Check if SERDES is controllable. For some products, although all of SERDESes
 * can be seen by application, but not all of them can be controlled. Accessing
 * SERDES parameters, power up/down, enable/disable, ... on not controllable
 * SERDES will have error returned.
 *
 * @param self This controller
 *
 * @retval cAtTrue if SERDES can be controlled
 * @retval cAtFalse if SERDES can not be controlled
 */
eBool AtSerdesControllerIsControllable(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->IsControllable(self);
    return cAtFalse;
    }

/**
 * Configure SERDES mode
 *
 * @param self This SERDES
 * @param mode SERDES mode @ref eAtSerdesMode "SERDES mode"
 * @return AT return code
 */
eAtRet AtSerdesControllerModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    mOneParamWrapCall(mode, eAtRet, DoModeSet(self, mode));
    }

/**
 * Get SERDES mode
 *
 * @param self This SERDES
 * @return SERDES mode @ref eAtSerdesMode "SERDES mode"
 */
eAtSerdesMode AtSerdesControllerModeGet(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->ModeGet(self);
    return cAtSerdesModeUnknown;
	}

/**
 * Access associated eye-scan controller
 *
 * @param self This controller
 *
 * @return AtEyeScanController object if exist, otherwise, NULL is returned.
 */
AtEyeScanController AtSerdesControllerEyeScanControllerGet(AtSerdesController self)
    {
    mNoParamObjectGet(EyeScanControllerGet, AtEyeScanController);
    }

/**
 * Check if a specific mode is supported
 *
 * @param self This SERDES
 * @param mode @ref eAtSerdesMode "Mode" to check
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtSerdesControllerModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    if (self)
        return mMethodsGet(self)->ModeIsSupported(self, mode);
    return cAtFalse;
    }

/**
 * Get serdes's sub lane (such as a SGMII in a QSGMII).
 *
 * @param self This controller
 * @param laneId Lane ID in this controller, e.g a SGMII index in a QSGMII serdes.
 *
 * @retval NULL if SERDES has not any sub lane or lane ID is out-of-range.
  */
AtSerdesController AtSerdesControllerLaneGet(AtSerdesController self, uint32 laneId)
    {
    if (self)
        return mMethodsGet(self)->LaneGet(self, laneId);
    return NULL;
    }

/**
 * Get number of sub lanes in a SERDES.
 *
 * @param self This controller
 *
 * @retval Number of lanes in this SERDES.
  */
uint32 AtSerdesControllerNumLanes(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->NumLanes(self);
    return 0;
    }

/**
 * Get parent
 *
 * @param self This controller
 *
 * @return Parent if this controller is a sub lane
 */
AtSerdesController AtSerdesControllerParentGet(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->ParentGet(self);
    return NULL;
    }

/**
 * Get description of a SERDES.
 *
 * @param self This controller
 *
 * @return SERDES.
  */
const char *AtSerdesControllerDescription(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->Description(self);
    return NULL;
    }

/**
 * Get SERDES manager
 *
 * @param self This controller
 *
 * @return SERDES manager if has.
 */
AtSerdesManager AtSerdesControllerManagerGet(AtSerdesController self)
    {
    if (self)
        return ManagerGet(self);
    return NULL;
    }

/**
 * Get device associated to SERDES
 *
 * @param self This controller
 *
 * @return Device associated to SERDES
 */
AtDevice AtSerdesControllerDeviceGet(AtSerdesController self)
    {
    if (self)
        return DeviceGet(self);
    return NULL;
    }

/**
 * Get ID string
 *
 * @param self This controller
 *
 * @return SERDES ID string
 */
const char *AtSerdesControllerIdString(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->IdString(self);
    return NULL;
    }

/**
 * Check if SERDES has a feature that when current PPM cross threshold, then
 * hardware will trigger SERDES reset
 *
 * @param self This SERDES
 *
 * @retval cAtTrue if has
 * @retval cAtTrue if does not have
 */
eBool AtSerdesControllerHasAutoResetPpmThreshold(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->HasAutoResetPpmThreshold(self);
    return cAtFalse;
    }

/**
 * Get PPM auto reset threshold. When SERDES PPM cross this threshold, it will be
 * reset automatically by hardware
 *
 * @param self This SERDES
 *
 * @return PPM auto reset threshold
 */
uint32 AtSerdesControllerAutoResetPpmThresholdGet(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->AutoResetPpmThresholdGet(self);
    return cInvalidUint32;
    }

/**
 * Set PPM auto reset threshold. When SERDES PPM cross this threshold, it will be
 * reset automatically by hardware
 *
 * @param self This SERDES
 * @param ppmThreshold PPM auto reset threshold
 *
 * @return AT return code
 */
eAtRet AtSerdesControllerAutoResetPpmThresholdSet(AtSerdesController self, uint32 ppmThreshold)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (ppmThreshold > mMethodsGet(self)->AutoResetPpmThresholdMax(self))
        return cAtErrorOutOfRangParm;

    mNumericalAttributeSet(AutoResetPpmThresholdSet, ppmThreshold);
    }

/**
 * Get maximum PPM auto reset threshold that can be specified.
 *
 * @param self This SERDES
 *
 * @return Maximum PPM auto reset threshold that can be specified.
 */
uint32 AtSerdesControllerAutoResetPpmThresholdMax(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->AutoResetPpmThresholdMax(self);
    return 0;
    }

/**
 * Configure SERDES prbs type
 *
 * @param self This SERDES
 * @param type SERDES prbs type @ref eAtSerdesPrbsEngineType "SERDES prbs type"
 * @return AT return code
 */
eAtRet AtSerdesControllerPrbsEngineTypeSet(AtSerdesController self, eAtSerdesPrbsEngineType type)
    {
    mNumericalAttributeSet(PrbsEngineTypeSet, type);
    }

/**
 * Get SERDES prbs type
 *
 * @param self This SERDES
 * @return SERDES prbs type @ref eAtSerdesPrbsEngineType "SERDES prbs type"
 */
eAtSerdesPrbsEngineType AtSerdesControllerPrbsEngineTypeGet(AtSerdesController self)
    {
    if (self)
        return mMethodsGet(self)->PrbsEngineTypeGet(self);
    return cAtSerdesPrbsEngineTypeFramed;
    }

/**
 * @}
 */

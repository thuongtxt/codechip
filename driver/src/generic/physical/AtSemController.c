/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : AtSemController.c
 *
 * Created Date: Dec 9, 2015
 *
 * Description : SEM controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDriverInternal.h"
#include "AtSemControllerInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtMaxNumEventListener 10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSemControllerMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtSemController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet Validate(AtSemController self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8 CorrectedErrorCounterGet(AtSemController self)
    {
    return (uint8)AtSemControllerCounterGet(self, cAtSemControllerCounterTypeCorrectable);
    }

static uint8 StatusGet(AtSemController self)
    {
    return (uint8)AtSemControllerAlarmGet(self);
    }

static uint32 CounterGet(AtSemController self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 CounterClear(AtSemController self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 AlarmGet(AtSemController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryGet(AtSemController self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 AlarmHistoryClear(AtSemController self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet InterruptMaskSet(AtSemController self, uint32 alarmMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(alarmMask);
    AtUnused(enableMask);
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtSemController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 SupportedAlarms(AtSemController self)
    {
    static uint32 supportedAlarms = cAtSemControllerAlarmActive
                                  | cAtSemControllerAlarmInitialization
                                  | cAtSemControllerAlarmObservation
                                  | cAtSemControllerAlarmCorrection
                                  | cAtSemControllerAlarmClassification
                                  | cAtSemControllerAlarmInjection
                                  | cAtSemControllerAlarmIdle
                                  | cAtSemControllerAlarmFatalError
                                  | cAtSemControllerAlarmErrorEssential
                                  | cAtSemControllerAlarmErrorCorrectable
                                  | cAtSemControllerAlarmErrorNoncorrectable
                                  | cAtSemControllerAlarmHeartbeat;

    AtUnused(self);
    return supportedAlarms;
    }

static void InterruptProcess(AtSemController self, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    }

static uint32 MaxElapseTimeToInsertError(AtSemController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxElapseTimeToMoveFsm(AtSemController self)
    {
    AtUnused(self);
    return 0;
    }

static AtUart UartObjectCreate(AtSemController self)
    {
    AtUnused(self);
    return NULL;
    }

static AtUart UartGet(AtSemController self)
    {
    if (self->uart == NULL)
        self->uart = mMethodsGet(self)->UartObjectCreate(self);
    return self->uart;
    }

static eBool IsActive(AtSemController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet MoveObservationModeBeforeActiveChecking(AtSemController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static AtSemControllerEventListener EventListenerObjectInit(AtSemControllerEventListener self, tAtSemControllerEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initialize memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSemControllerEventListenerWrapper));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize private data */
    mMethodsGet(osal)->MemCpy(osal, &(self->listener), callbacks, sizeof(tAtSemControllerEventListener));
    self->userData = userData;

    return self;
    }

static AtSemControllerEventListener EventListenerNew(tAtSemControllerEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemControllerEventListener newListener = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtSemControllerEventListenerWrapper));

    if (newListener)
        return EventListenerObjectInit(newListener, callbacks, userData);

    return NULL;
    }

static eBool TwoListenersAreIdenticalWithUserData(AtSemControllerEventListener self, tAtSemControllerEventListener * listener, void* userData)
    {
    if ((self->listener.AlarmChangeStateNotify != listener->AlarmChangeStateNotify) ||
        (self->userData != userData))
        return cAtFalse;

    return cAtTrue;
    }

static eBool TwoListenersAreIdentical(AtSemControllerEventListener self, tAtSemControllerEventListener * listener)
    {
    if (self->listener.AlarmChangeStateNotify != listener->AlarmChangeStateNotify)
        return cAtFalse;

    return cAtTrue;
    }

static eBool ListenerIsExistingInChannel(AtSemController self, tAtSemControllerEventListener *listener, void *userData)
    {
    AtIterator iterator = AtListIteratorCreate(self->allEventListeners);
    AtSemControllerEventListener aListener;
    eBool isExisting = cAtFalse;

    /* For all event listeners */
    while ((aListener = (AtSemControllerEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (TwoListenersAreIdenticalWithUserData(aListener, listener, userData))
            {
            isExisting = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    return isExisting;
    }

static eAtRet EventListenerAdd(AtSemController self, tAtSemControllerEventListener *listener, void *userData)
    {
    eAtRet atRet = cAtError;
    AtSemControllerEventListener newListener;

    if (self->allEventListeners == NULL)
        {
        self->listenerListMutex = AtOsalMutexCreate();
        self->allEventListeners = AtListCreate(cAtMaxNumEventListener);
        }

    if (ListenerIsExistingInChannel(self, listener, userData))
        return cAtOk;

    newListener = EventListenerNew(listener, userData);
    if (newListener == NULL)
        return cAtError;

    /* Only add a new listener */
    AtOsalMutexLock(self->listenerListMutex);
    atRet = AtListObjectAdd(self->allEventListeners, (AtObject)newListener);
    AtOsalMutexUnLock(self->listenerListMutex);

    return atRet;
    }

static void AlarmListenerCall(AtSemControllerEventListener self, AtSemController sensor, uint32 changedAlarms, uint32 currentAlarms)
    {
    if (self->listener.AlarmChangeStateNotify)
        self->listener.AlarmChangeStateNotify(sensor, changedAlarms, currentAlarms, self->userData);
    }

static void EbdFilesDelete(AtSemController controller)
    {
    uint8 slr, numSlr = AtSemControllerNumSlr(controller);

    if (controller->ebdFilePaths != NULL)
        {
        for (slr = 0; slr < numSlr; slr++)
            AtOsalMemFree(controller->ebdFilePaths[slr]);
        }

    if (numSlr)
        AtOsalMemFree(controller->ebdFilePaths);
    controller->ebdFilePaths = NULL;
    }

static void Delete(AtObject self)
    {
    AtSemController controller = (AtSemController)self;
    AtIterator iterator ;

    if(controller->allEventListeners)
        {
        AtOsalMutexLock(controller->listenerListMutex);

        /* Delete all listeners in list */
        iterator = AtListIteratorCreate(controller->allEventListeners);
        if (iterator)
            {
            AtSemControllerEventListener aListener;
            while((aListener = (AtSemControllerEventListener)AtIteratorNext(iterator)) != NULL)
                AtObjectDelete((AtObject)aListener);
            }

        AtObjectDelete((AtObject)iterator);

        /* Delete event listener list */
        AtObjectDelete((AtObject)(controller->allEventListeners));
        controller->allEventListeners = NULL;

        AtOsalMutexUnLock(controller->listenerListMutex);

        /* Delete private attribute */
        AtOsalMutexDestroy(controller->listenerListMutex);
        controller->listenerListMutex = NULL;
        }

    EbdFilesDelete(controller);

    /* Call object delete */
    AtObjectDelete((AtObject)controller->uart);
    controller->uart = NULL;
    m_AtObjectMethods->Delete(self);
    }

static const char *ToString(AtObject self)
    {
    AtDevice dev = AtSemControllerDeviceGet((AtSemController)self);
    static char str[64];

    AtSprintf(str, "%ssem_controller", AtDeviceIdToString(dev));
    return str;
    }

static void EbdFilesSerialize(AtSemController object, AtCoder encoder)
    {
    uint8 slr, numSlr = AtSemControllerNumSlr(object);

    if (object->ebdFilePaths == NULL)
        return;

    for (slr = 0; slr < numSlr; slr++)
        {
        char fieldName[32];
        AtSprintf(fieldName, "ebdFilePaths[%d]", slr);
        AtCoderEncodeString(encoder, object->ebdFilePaths[slr], fieldName);
        }
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSemController object = (AtSemController)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(device);
    mEncodeNone(maxElapseTimeToMoveFsm);
    mEncodeNone(maxElapseTimeToInsertError);
    mEncodeUInt(semId);
    mEncodeNone(allEventListeners);
    mEncodeNone(listenerListMutex);
    mEncodeNone(ebdFilePaths);

    EbdFilesSerialize(object, encoder);

    /* Create UART if it does not exist */
    AtSemControllerUartGet(object);
    mEncodeObject(uart);
    }

static eAtRet ValidateByUart(AtSemController self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet ForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static eAtRet AsyncForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorModeNotSupport;
    }

static const char* DeviceDescription(AtSemController self)
    {
    AtUnused(self);
    return NULL;
    }

static uint8 NumSlr(AtSemController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 FrameSize(AtSemController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 GlobalLfaToLocalLfa(AtSemController self, uint32 globalLfa)
    {
    AtUnused(self);
    AtUnused(globalLfa);

    return 0;
    }

static uint32 GlobalLfaToSlrId(AtSemController self, uint32 globalLfa)
    {
    AtUnused(self);
    AtUnused(globalLfa);

    return 0;
    }

static uint32 NumDummyWordsInEbd(AtSemController self)
    {
    AtUnused(self);

    return 0;
    }

static const char* EbdFilePath(AtSemController self, uint8 slrIndex)
    {
    uint8 numSlr = AtSemControllerNumSlr(self);

    if ((self->ebdFilePaths == NULL) || (numSlr == 0))
        return NULL;

    /* Just ignore input SLR index if there is only one SLR */
    if (numSlr == 1)
        return self->ebdFilePaths[0];

    /* For controllers that have more than one SRLs, the SLR index must be valid */
    if (slrIndex >= numSlr)
        return NULL;
    return self->ebdFilePaths[slrIndex];
    }

static eBool SlrIndexIsValid(AtSemController self, uint8 slrIndex)
    {
    uint8 numSlr;

    numSlr = AtSemControllerNumSlr(self);
    if (numSlr == 0)
        return cAtFalse;

    /* Always ignore input SLR index if there is only one of them */
    if (numSlr == 1)
        return cAtTrue;

    return (slrIndex < numSlr) ? cAtTrue : cAtFalse;
    }

static char **EbdFilePaths(AtSemController self)
    {
    uint8 numSlr = AtSemControllerNumSlr(self);
    uint32 memorySize;

    if (self->ebdFilePaths)
        return self->ebdFilePaths;

    memorySize = sizeof(char*) * numSlr;
    self->ebdFilePaths = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(self->ebdFilePaths, 0, memorySize);

    return self->ebdFilePaths;
    }

static eAtRet EbdFilePathSet(AtSemController self, uint8 slrIndex, const char* filePath)
    {
    uint8 numSlr = 0;
    char **ebdFilePaths;

    if (filePath == NULL)
        return cAtErrorInvlParm;

    if (!SlrIndexIsValid(self, slrIndex))
        return cAtErrorInvlParm;

    /* Always use first index if there is only one SLR */
    numSlr = AtSemControllerNumSlr(self);
    if (numSlr == 1)
        slrIndex = 0;

    ebdFilePaths = EbdFilePaths(self);
    if (ebdFilePaths[slrIndex])
        AtOsalMemFree(ebdFilePaths[slrIndex]);
    ebdFilePaths[slrIndex] = AtStrdup(filePath);

    return cAtOk;
    }

static eBool ErrorIsEssential(AtSemController self, const char *uartReport, uint32 reportLength)
    {
    uint8 numSlr = 0;

    if (uartReport == NULL)
        return cAtFalse;

    numSlr = AtSemControllerNumSlr(self);
    if (numSlr == 0)
        return cAtFalse;

    return AtUartRerportErrorIsEssential(self, uartReport, reportLength);
    }

static void OverrideAtObject(AtSemController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet((AtObject)self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        /* Setup methods */
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet((AtObject)self, &m_AtObjectOverride);
    }

static void Override(AtSemController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtSemController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Validate);
        mMethodOverride(m_methods, CorrectedErrorCounterGet);
        mMethodOverride(m_methods, StatusGet);
        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, SupportedAlarms);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, IsActive);
        mMethodOverride(m_methods, MoveObservationModeBeforeActiveChecking);
        mMethodOverride(m_methods, MaxElapseTimeToInsertError);
        mMethodOverride(m_methods, MaxElapseTimeToMoveFsm);
        mMethodOverride(m_methods, UartGet);
        mMethodOverride(m_methods, UartObjectCreate);
        mMethodOverride(m_methods, ValidateByUart);
        mMethodOverride(m_methods, ForceErrorByUart);
        mMethodOverride(m_methods, AsyncForceErrorByUart);
        mMethodOverride(m_methods, DeviceDescription);
        mMethodOverride(m_methods, NumSlr);
        mMethodOverride(m_methods, MaxLfa);
        mMethodOverride(m_methods, FrameSize);
        mMethodOverride(m_methods, GlobalLfaToLocalLfa);
        mMethodOverride(m_methods, GlobalLfaToSlrId);
        mMethodOverride(m_methods, NumDummyWordsInEbd);
        mMethodOverride(m_methods, EbdFilePath);
        mMethodOverride(m_methods, EbdFilePathSet);
        mMethodOverride(m_methods, ErrorIsEssential);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSemController);
    }

AtSemController AtSemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->device = device;
    self->uart = NULL;
    self->semId  = semId;

    return self;
    }

eBool AtSemControllerIsActive(AtSemController self)
    {
    mAttributeGet(IsActive, eBool, cAtFalse);
    }

eAtRet AtSemControllerMoveObservationModeBeforeActiveChecking(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->MoveObservationModeBeforeActiveChecking(self);
    return cAtErrorNullPointer;
    }

uint32 AtSemControllerMaxElapseTimeToInsertError(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->MaxElapseTimeToInsertError(self);
    return 0;
    }

uint32 AtSemControllerMaxElapseTimeToMoveFsm(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->MaxElapseTimeToMoveFsm(self);
    return 0;
    }

uint32 AtSemControllerGlobalLfaToLocalLfa(AtSemController self, uint32 globalLfa)
    {
    if (self)
        return mMethodsGet(self)->GlobalLfaToLocalLfa(self, globalLfa);

    return 0;
    }

uint32 AtSemControllerGlobalLfaToSlrId(AtSemController self, uint32 globalLfa)
    {
    if (self)
        return mMethodsGet(self)->GlobalLfaToSlrId(self, globalLfa);

    return 0;
    }

uint32 AtSemControllerNumDummyWordsInEbd(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->NumDummyWordsInEbd(self);

    return 0;
    }

/*
 * To get SEM corrected error counters
 *
 * @param self This controller
 *
 * @return number of SEM corrected error counters
 */
uint8 AtSemControllerCorrectedErrorCounterGet(AtSemController self)
    {
    mAttributeGet(CorrectedErrorCounterGet, uint8, 0);
    }

/*
 * To get SEM status
 *
 * @param self This controller
 *
 * @return @ref eAtSemControllerStatus "Multiple SEM status in a bitmap"
 */
uint8 AtSemControllerStatusGet(AtSemController self)
    {
    mAttributeGet(StatusGet, uint8, 0);
    }

void AtSemControllerAllAlarmListenersCall(AtSemController self, uint32 changedAlarms, uint32 currentAlarms)
    {
    AtSemControllerEventListener aListener;
    AtIterator iterator;

    if ((self == NULL) || (self->allEventListeners == NULL))
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(self->allEventListeners);
    while((aListener = (AtSemControllerEventListener)AtIteratorNext(iterator)) != NULL)
        AlarmListenerCall(aListener, self, changedAlarms, currentAlarms);

    AtObjectDelete((AtObject)iterator);
    }

void AtSemControllerInterruptProcess(AtSemController self, AtIpCore ipCore)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self, ipCore);
    }

/**
 * @addtogroup AtSemController
 * @{
 */

/**
 * To get device
 *
 * @param self This controller
 *
 * @return device object
 */
AtDevice AtSemControllerDeviceGet(AtSemController self)
    {
    return self ? self->device : NULL;
    }

/**
 * To get UART
 *
 * @param self This controller
 *
 * @return UART object
 */
AtUart AtSemControllerUartGet(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->UartGet(self);
    return NULL;
    }

/**
 * To validate SEM by UART interface, not via firmware interface
 *
 * @param self This controller
 *
 * @return AT code
 */
eAtRet AtSemControllerValidateByUart(AtSemController self)
    {
    mAttributeGet(ValidateByUart, eAtRet, cAtErrorNullPointer);
    }

/**
 * Force SEM correcable/uncorrectable error by UART interface
 *
 * @param self This controller
 * @param errorType @ref eAtSemControllerCounterType "Error type"
 *
 * @return AT code
 */
eAtRet AtSemControllerForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    mNumericalAttributeSet(ForceErrorByUart, errorType);
    }

/**
 * Force SEM correcable/uncorrectable error by UART interface in the async operation.
 * This function is to slice down the error force function into smaller parts to share CPU to another tasks.
 * This function should be called again and again until it return a code which is not cAtErrorAgain.
 *
 * @param self This controller
 * @param errorType @ref eAtSemControllerCounterType "Error type"
 *
 * @return AT code
 */
eAtRet AtSemControllerAsyncForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType)
    {
    if (self)
        return mMethodsGet(self)->AsyncForceErrorByUart(self, errorType);
    return cAtErrorNullPointer;
    }

/**
 * Get controller ID
 *
 * @param self This controller
 *
 * @return Its ID
 */
uint32 AtSemControllerIdGet(AtSemController self)
    {
    if (self)
        return self->semId;
    return 0;
    }

/**
 * To initialize SEM
 *
 * @param self This controller
 *
 * @return AT code
 */
eAtRet AtSemControllerInit(AtSemController self)
    {
    mAttributeGet(Init, eAtRet, cAtErrorNullPointer);
    }

/**
 * To validate SEM
 *
 * @param self This controller
 *
 * @return AT code
 */
eAtRet AtSemControllerValidate(AtSemController self)
    {
    mAttributeGet(Validate, eAtRet, cAtErrorNullPointer);
    }

/**
 * To get SEM counters
 *
 * @param self This controller
 * @param counterType Type of SEM counter @ref eAtSemControllerCounterType "SEM counter type"
 *
 * @return SEM counter number
 */
uint32 AtSemControllerCounterGet(AtSemController self, uint32 counterType)
    {
    mOneParamAttributeGet(CounterGet, counterType, uint32, 0);
    }

/**
 * To get and clear SEM counters
 *
 * @param self This controller
 * @param counterType Type of SEM counter @ref eAtSemControllerCounterType "SEM counter type"
 *
 * @return SEM counter number
 */
uint32 AtSemControllerCounterClear(AtSemController self, uint32 counterType)
    {
    mOneParamAttributeGet(CounterClear, counterType, uint32, 0);
    }

/**
 * To get SEM alarm
 *
 * @param self This controller
 *
 * @return @ref eAtSemControllerAlarmType "SEM alarm type in bitmap"
 */
uint32 AtSemControllerAlarmGet(AtSemController self)
    {
    mAttributeGet(AlarmGet, uint32, 0);
    }

/**
 * To get SEM alarm history in read only mode
 *
 * @param self This controller
 *
 * @return @ref eAtSemControllerAlarmType "Multiple SEM alarm in a bitmap"
 */
uint32 AtSemControllerAlarmHistoryGet(AtSemController self)
    {
    mAttributeGet(AlarmHistoryGet, uint32, 0);
    }

/**
 * To get SEM alarm history in read to clear mode
 *
 * @param self This controller
 *
 * @return @ref eAtSemControllerAlarmType "Multiple SEM alarm in a bitmap"
 */
uint32 AtSemControllerAlarmHistoryClear(AtSemController self)
    {
    mAttributeGet(AlarmHistoryClear, uint32, 0);
    }

/**
 * To set SEM interrupt mask
 *
 * @param self This controller
 * @param alarmMask Alarm mask @ref eAtSemControllerAlarmType "Multiple SEM alarm in a bitmap"
 * @param enableMask Enable mask @ref eAtSemControllerAlarmType "Multiple SEM alarm in a bitmap"
 *
 * @return AT return code
 */
eAtRet AtSemControllerInterruptMaskSet(AtSemController self, uint32 alarmMask, uint32 enableMask)
    {
    uint32 notSupportedAlarms;

    if (self == NULL)
        mRetCheck(cAtErrorObjectNotExist);

    notSupportedAlarms = ~(AtSemControllerSupportedAlarms(self));
    if (alarmMask & notSupportedAlarms)
        mRetCheck(cAtErrorModeNotSupport);

    mTwoParamsAttributeSet(InterruptMaskSet, alarmMask, enableMask);
    }

/**
 * To get SEM interrupt mask
 *
 * @param self This controller
 *
 * @return Interrupt mask @ref eAtSemControllerAlarmType SEM alarm in a bitmap"
 */
uint32 AtSemControllerInterruptMaskGet(AtSemController self)
    {
    mAttributeGet(InterruptMaskGet, uint32, 0);
    }

/**
 * Add event listener. When there is any alarm corresponding interface will be called.
 * Default listener interface is @ref tAtSemControllerEventListener
 * Note, each sensor type can have its own event listener interface, so this
 * default listener interface must not be used.
 *
 * Application just implemented functions defined in event listener interface in
 * order to receiving event.
 *
 * @param self This SEM controller
 * @param listener Event listener
 * @param userData User data that will be input when callback is called
 *
 * @return AT return code
 */
eAtRet AtSemControllerEventListenerAdd(AtSemController self, tAtSemControllerEventListener *listener, void *userData)
    {
    if (self && listener)
        return EventListenerAdd(self, listener, userData);
    return cAtError;
    }

/**
 * Remove event listener
 *
 * @param self This physical Sensor
 * @param listener Event listener
 *
 * @return AT return code
 */
eAtRet AtSemControllerEventListenerRemove(AtSemController self, tAtSemControllerEventListener *listener)
    {
    AtSemControllerEventListener aListener;
    AtIterator iterator;

    if ((self == NULL) || (listener == NULL))
        return cAtErrorNullPointer;

    if (self->allEventListeners == NULL)
        return cAtOk;

    AtOsalMutexLock(self->listenerListMutex);
    iterator = AtListIteratorCreate(self->allEventListeners);
    while((aListener = (AtSemControllerEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (!TwoListenersAreIdentical(aListener, listener))
            continue;

        /* Remove current object out of list */
        AtIteratorRemove(iterator);

        /* Delete listener object */
        AtObjectDelete((AtObject)aListener);
        }
    AtOsalMutexUnLock(self->listenerListMutex);

    AtObjectDelete((AtObject)iterator);

    return cAtOk;
    }

/**
 * Get all of supported alarms in this SEM controller.
 *
 * @param self This physical SEM
 *
 * @return Supported alarms
 */
uint32 AtSemControllerSupportedAlarms(AtSemController self)
    {
    mAttributeGet(SupportedAlarms, uint32, 0);
    }

/**
 * Get SEM device description
 *
 * @param self This physical SEM
 *
 * @return Device description example "XC7K325T-2FFG900I7002"
 */
const char* AtSemControllerDeviceDescription(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->DeviceDescription(self);
    return NULL;
    }

/**
 * Get number of SLRs which connects to this SEM controller
 *
 * @param self This physical SEM
 *
 * @return Number of SLRs
 */
uint8 AtSemControllerNumSlr(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->NumSlr(self);

    return 0;
    }

/**
 * Get maximum LFA
 *
 * @param self This physical SEM
 *
 * @return Maximum LFA
 */
uint32 AtSemControllerMaxLfa(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->MaxLfa(self);

    return 0;
    }

/**
 * Get size of a frame of a SLR that this SEM controller is connected
 *
 * @param self This physical SEM
 *
 * @return Number of word(32 bits) of a frame
 */
uint32 AtSemControllerFrameSize(AtSemController self)
    {
    if (self)
        return mMethodsGet(self)->FrameSize(self);

    return 0;
    }

/**
 * Get EBD file path of a SLR that this SEM controller is connected.
 * This EBD file is used to lookup to know if an LFA error is essential or non-essential.
 *
 * @param self This physical SEM
 * @param slrIndex Index of a SLR in this SEM controller
 *
 * @return File path
 */
const char* AtSemControllerEbdFilePathGet(AtSemController self, uint8 slrIndex)
    {
    if (self)
        return mMethodsGet(self)->EbdFilePath(self, slrIndex);
    return NULL;
    }

/**
 * Set EBD file path of a SLR that this SEM controller is connected
 * This EBD file is used to lookup to know if an LFA error is essential or non-essential.
 *
 * @param self This physical SEM
 * @param slrIndex Index of a SLR in this SEM controller
 * @param filePath EBD file path
 *
 * @return AT return code
 */
eAtRet AtSemControllerEbdFilePathSet(AtSemController self, uint8 slrIndex, const char* filePath)
    {
    if (self)
        return mMethodsGet(self)->EbdFilePathSet(self, slrIndex, filePath);
    return cAtErrorNullPointer;
    }

/**
 * Lookup LFA errors from the UART report to know if at least an error is essential.
 *
 * @param self This physical SEM
 * @param uartReport UART report buffer
 * @param reportLength UART report buffer length
 *
 * @retval cAtTrue if it is essential
 * @retval cAtFalse if it is not essential
 */
eBool AtSemControllerErrorIsEssential(AtSemController self, const char *uartReport, uint32 reportLength)
    {
    if (self)
        return mMethodsGet(self)->ErrorIsEssential(self, uartReport, reportLength);

    return cAtFalse;
    }

/**
 * @}
*/

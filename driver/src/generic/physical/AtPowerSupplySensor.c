/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : AtPowerSupplySensor.c
 *
 * Created Date: Oct 3, 2016
 *
 * Description : AtPowerSupplySensor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtPowerSupplySensorInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mSensorIsValid(self) (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPowerSupplySensorMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtObject self)
    {
    AtSensor sensor = (AtSensor)self;
    AtDevice dev = AtSensorDeviceGet(sensor);
    static char str[64];

    AtSprintf(str, "%spower_supply_sensor", AtDeviceIdToString(dev));
    return str;
    }

static uint32 VoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    AtUnused(self);
    AtUnused(voltageType);
    return 0x0;
    }

static eAtRet AlarmSetThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage)
    {
    AtUnused(self);
    AtUnused(voltageType);
    AtUnused(voltage);
    return cAtErrorNotImplemented;
    }

static uint32 AlarmSetThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    AtUnused(self);
    AtUnused(voltageType);
    return 0x0;
    }

static eAtRet AlarmClearThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage)
    {
    AtUnused(self);
    AtUnused(voltageType);
    AtUnused(voltage);
    return cAtErrorNotImplemented;
    }

static uint32 AlarmClearThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    AtUnused(self);
    AtUnused(voltageType);
    return 0x0;
    }

static uint32 MinRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    AtUnused(self);
    AtUnused(voltageType);
    return 0x0;
    }

static uint32 MaxRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    AtUnused(self);
    AtUnused(voltageType);
    return 0x0;
    }

static void OverrideAtObject(AtPowerSupplySensor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet((AtObject)self), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

static void Override(AtPowerSupplySensor self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPowerSupplySensor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, VoltageGet);
        mMethodOverride(m_methods, AlarmSetThresholdSet);
        mMethodOverride(m_methods, AlarmSetThresholdGet);
        mMethodOverride(m_methods, AlarmClearThresholdSet);
        mMethodOverride(m_methods, AlarmClearThresholdGet);
        mMethodOverride(m_methods, MinRecordedVoltageGet);
        mMethodOverride(m_methods, MaxRecordedVoltageGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPowerSupplySensor);
    }

AtPowerSupplySensor AtPowerSupplySensorObjectInit(AtPowerSupplySensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSensorObjectInit((AtSensor)self, device) == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPowerSupplySensor
 * @{
 */

/**
 * Get power supply sensor voltage
 *
 * @param self This sensor
 * @param voltageType @ref eAtPowerSupplyVoltage "Voltage type"
 *
 * @return Power supply sensor voltage value (in mV)
 */
uint32 AtPowerSupplySensorVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    if (self)
        return mMethodsGet(self)->VoltageGet(self, voltageType);
    return 0x0;
    }

/**
 * Set set threshold of power supply sensor voltage
 *
 * @param self This sensor
 * @param voltageType @ref eAtPowerSupplyVoltage "Voltage type"
 * @param voltage Voltage value (in mV)
 *
 * @return AT return code
 */
eAtRet AtPowerSupplySensorAlarmUpperThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage)
    {
    if (self)
        return mMethodsGet(self)->AlarmSetThresholdSet(self, voltageType, voltage);
    return cAtErrorNullPointer;
    }

/**
 * Get set threshold of power supply sensor voltage
 *
 * @param self This sensor
 * @param voltageType @ref eAtPowerSupplyVoltage "Voltage type"
 *
 * @return Power supply sensor voltage value  (in mV)
 */
uint32 AtPowerSupplySensorAlarmUpperThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    if (self)
        return mMethodsGet(self)->AlarmSetThresholdGet(self, voltageType);
    return 0x0;
    }

/**
 * Set clear threshold of power supply sensor voltage
 *
 * @param self This sensor
 * @param voltageType @ref eAtPowerSupplyVoltage "Voltage type"
 * @param voltage Voltage value (in mV)
 *
 * @return AT return code
 */
eAtRet AtPowerSupplySensorAlarmLowerThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage)
    {
    if (self)
        return mMethodsGet(self)->AlarmClearThresholdSet(self, voltageType, voltage);
    return cAtErrorNullPointer;
    }

/**
 * Get clear threshold of power supply sensor voltage
 *
 * @param self This sensor
 * @param voltageType @ref eAtPowerSupplyVoltage "Voltage type"
 *
 * @return Power supply sensor voltage value (in mV)
 */
uint32 AtPowerSupplySensorAlarmLowerThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    if (self)
        return mMethodsGet(self)->AlarmClearThresholdGet(self, voltageType);
    return 0x0;
    }

/**
 * Get minimum recorded power supply sensor voltage
 *
 * @param self This sensor
 * @param voltageType @ref eAtPowerSupplyVoltage "Voltage type"
 *
 * @return Power supply sensor voltage value (in mV)
 */
uint32 AtPowerSupplySensorMinRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    if (self)
        return mMethodsGet(self)->MinRecordedVoltageGet(self, voltageType);
    return 0x0;
    }

/**
 * Get maximum recorded power supply sensor voltage
 *
 * @param self This sensor
 * @param voltageType @ref eAtPowerSupplyVoltage "Voltage type"
 *
 * @return Power supply sensor voltage value (in mV)
 */
uint32 AtPowerSupplySensorMaxRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType)
    {
    if (self)
        return mMethodsGet(self)->MaxRecordedVoltageGet(self, voltageType);
    return 0x0;
    }

/**
 * @}
 */

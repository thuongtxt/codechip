/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : AtSensorInternal.h
 * 
 * Created Date: Dec 15, 2015
 *
 * Description : Generic physical sensor representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSENSORINTERNAL_H_
#define _ATSENSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Superclass. */
#include "AtOsal.h"
#include "AtSensor.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSensorMethods
    {
    eAtRet (*Init)(AtSensor self);
    uint32 (*AlarmGet)(AtSensor self);
    uint32 (*AlarmHistoryGet)(AtSensor self);
    uint32 (*AlarmHistoryClear)(AtSensor self);

    /* Interrupt. */
    eAtRet (*InterruptMaskSet)(AtSensor self, uint32 alarmMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtSensor self);
    void (*InterruptProcess)(AtSensor self, AtIpCore ipCore);
    eAtRet (*EventListenerAdd)(AtSensor self, tAtSensorEventListener *listener, void* userData);
    uint32 (*SupportedAlarms)(AtSensor self);
    }tAtSensorMethods;

typedef struct tAtSensor
    {
    tAtObject super;
    const tAtSensorMethods * methods;

    /* Private data */
    AtDevice device;
    /* Listeners */
    AtList allEventListeners;
    AtOsalMutex listenerListMutex; /* Locker to protect list of event listener */
    }tAtSensor;

typedef struct tAtSensorEventListenerWrapper * AtSensorEventListener;
typedef struct tAtSensorEventListenerWrapper
    {
    tAtObject super;
    tAtSensorEventListener listener;
    void *userData;
    }tAtSensorEventListenerWrapper;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSensor AtSensorObjectInit(AtSensor self, AtDevice device);

void AtSensorInterruptProcess(AtSensor self, AtIpCore ipCore);
void AtSensorAllAlarmListenersCall(AtSensor self, uint32 changedAlarms, uint32 currentAlarms);

#ifdef __cplusplus
}
#endif
#endif /* _ATSENSORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanControllerXaui.c
 *
 * Created Date: Feb 25, 2015
 *
 * Description : XAUI eye scan controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanControllerXauiInternal.h"
#include "AtEyeScanDrpReg.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanControllerMethods m_AtEyeScanControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 LaneDataWidth(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 20;
    }

static uint8 NumLanes(AtEyeScanController self)
    {
    AtUnused(self);
    return AtEyeScanControllerMaxNumLanes();
    }

static AtEyeScanLane LaneObjectCreate(AtEyeScanController self, uint8 laneId)
    {
    return AtEyeScanLaneFastNew(self, laneId);
    }

static eBool EqualizerModeIsSupported(AtEyeScanController self, eAtEyeScanEqualizerMode equalizerMode)
    {
    AtUnused(self);
    if ((equalizerMode == cAtEyeScanEqualizerModeDfe) ||
        (equalizerMode == cAtEyeScanEqualizerModeLpm))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 SerdesControlRegister(AtEyeScanController self)
    {
    AtUnused(self);
    return ES_REG_SERDES_CONTROL_XAUI;
    }

static eAtRet PmaReset(AtEyeScanController self)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
	return AtSerdesControllerReset(serdes);
    }

static float SpeedInGbps(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 3.125;
    }

static uint32 TotalWidth(AtEyeScanController self)
    {
    AtUnused(self);
    return 38;
    }

static void OverrideAtEyeScanController(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanControllerOverride, mMethodsGet(self), sizeof(m_AtEyeScanControllerOverride));

        mMethodOverride(m_AtEyeScanControllerOverride, NumLanes);
        mMethodOverride(m_AtEyeScanControllerOverride, LaneObjectCreate);
        mMethodOverride(m_AtEyeScanControllerOverride, LaneDataWidth);
        mMethodOverride(m_AtEyeScanControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtEyeScanControllerOverride, SerdesControlRegister);
        mMethodOverride(m_AtEyeScanControllerOverride, PmaReset);
        mMethodOverride(m_AtEyeScanControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtEyeScanControllerOverride, TotalWidth);
        }

    mMethodsSet(self, &m_AtEyeScanControllerOverride);
    }

static void Override(AtEyeScanController self)
    {
    OverrideAtEyeScanController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanControllerXaui);
    }

AtEyeScanController AtEyeScanControllerXauiObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanControllerObjectInit(self, serdesController, drpBaseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanController AtEyeScanControllerXauiNew(AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanControllerXauiObjectInit(newController, serdesController, drpBaseAddress);
    }

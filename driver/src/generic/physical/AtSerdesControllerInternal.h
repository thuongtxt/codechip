/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSerdesControllerInternal.h
 * 
 * Created Date: Aug 16, 2013
 *
 * Description : SERDES controller. To configure physical parameters of STM
 *               port and GE port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSERDESCONTROLLERINTERNAL_H_
#define _ATSERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtDriverInternal.h"
#include "../man/AtDeviceInternal.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtSerdesControllerEyescanLogic
    {
    cAtSerdesControllerEyescanAutoSelect,
    cAtSerdesControllerEyescanStm,
    cAtSerdesControllerEyescanXaui,
    cAtSerdesControllerEyescanGty,
    cAtSerdesControllerEyescanGtyE3,
    cAtSerdesControllerEyescanGthE3
    }eAtSerdesControllerEyescanLogic;

typedef enum eAtSerdesPhysicalLayer
    {
    cAtSerdesPhysicalLayerPcs = 1,
    cAtSerdesPhysicalLayerPma = 2
    }eAtSerdesPhysicalLayer;

typedef struct tAtSerdesControllerStandbyCache
    {
    uint8 powerDown;
    uint8 mode;
    }tAtSerdesControllerStandbyCache;

typedef struct tAtSerdesControllerMethods
    {
    eBool (*IsControllable)(AtSerdesController self);
    eAtRet (*Init)(AtSerdesController self);
    eAtRet (*Reset)(AtSerdesController self);
    eAtRet (*RxReset)(AtSerdesController self);
    eAtRet (*TxReset)(AtSerdesController self);
    eBool (*PllIsLocked)(AtSerdesController self);
    eBool (*PllChanged)(AtSerdesController self);
    eAtSerdesLinkStatus (*LinkStatus)(AtSerdesController self);
    uint32 (*BaseAddress)(AtSerdesController self);
    AtChannel (*PhysicalPortGet)(AtSerdesController self);
    const char *(*IdString)(AtSerdesController self);

    eAtRet (*Enable)(AtSerdesController self, eBool enable);
    eBool (*IsEnabled)(AtSerdesController self);
    eBool (*CanEnable)(AtSerdesController self, eBool enable);

    eAtRet (*ModeSet)(AtSerdesController self, eAtSerdesMode mode);
    eAtSerdesMode (*ModeGet)(AtSerdesController self);
    eBool (*ModeIsSupported)(AtSerdesController self, eAtSerdesMode mode);
    eAtSerdesMode (*DefaultMode)(AtSerdesController self);
    eAtSerdesMode (*OverSamplingMode)(AtSerdesController self);

    eAtRet (*PhysicalParamSet)(AtSerdesController self, eAtSerdesParam param, uint32 value);
    uint32 (*PhysicalParamGet)(AtSerdesController self, eAtSerdesParam param);
    eBool  (*PhysicalParamIsSupported)(AtSerdesController self, eAtSerdesParam param);
    eBool (*PhysicalParamValueIsInRange)(AtSerdesController self, eAtSerdesParam param, uint32 value);

    /* Loopback */
    eBool (*CanLoopback)(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable);
    eAtRet (*LoopbackEnable)(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable);
    eBool (*LoopbackIsEnabled)(AtSerdesController self, eAtLoopbackMode loopbackMode);
    eAtRet (*LayerLoopbackSet)(AtSerdesController self, eAtSerdesPhysicalLayer layer, eAtLoopbackMode loopbackMode);
    eAtLoopbackMode (*LayerLoopbackGet)(AtSerdesController self, eAtSerdesPhysicalLayer layer);

    /* For SERDES that cannot do local/remote loopback at the same time */
    eAtRet (*LoopbackSet)(AtSerdesController self, eAtLoopbackMode loopback);
    eAtLoopbackMode (*LoopbackGet)(AtSerdesController self);

    /* Alarms */
    uint32 (*AlarmGet)(AtSerdesController self);
    uint32 (*AlarmHistoryGet)(AtSerdesController self);
    uint32 (*AlarmHistoryClear)(AtSerdesController self);

    eAtRet (*InterruptMaskSet)(AtSerdesController self, uint32 alarmMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtSerdesController self);
    uint32 (*SupportedInterruptMask)(AtSerdesController self);

    /* RX alarm force. */
    eAtRet (*RxAlarmForce)(AtSerdesController self, uint32 alarmType);
    eAtRet (*RxAlarmUnforce)(AtSerdesController self, uint32 alarmType);
    uint32 (*RxForcedAlarmGet)(AtSerdesController self);
    uint32 (*RxForcableAlarmsGet)(AtSerdesController self);

    /* Listeners */
    eAtRet (*ListenerAdd)(AtSerdesController self, void *listener, const tAtSerdesControllerListener *callback);
    eAtRet (*ListenerRemove)(AtSerdesController self, void *listener, const tAtSerdesControllerListener *callback);

    /* Debug */
    void (*Debug)(AtSerdesController self);
    void (*StatusClear)(AtSerdesController self);
    AtQuerier (*QuerierGet)(AtSerdesController self);

    /* MDIO */
    AtMdio (*Mdio)(AtSerdesController self);

    /* DRP */
    AtDrp (*Drp)(AtSerdesController self);
    AtDrp (*DrpObjectCreate)(AtSerdesController self);

	/* Equalizer */
    eAtSerdesEqualizerMode (*DefaultEqualizerMode)(AtSerdesController self);
    eAtRet (*EqualizerModeSet)(AtSerdesController self, eAtSerdesEqualizerMode mode);
    eAtSerdesEqualizerMode (*EqualizerModeGet)(AtSerdesController self);
    eBool (*EqualizerModeIsSupported)(AtSerdesController self, eAtSerdesEqualizerMode mode);
    eBool (*EqualizerCanControl)(AtSerdesController self);

    /* Eye scan controller */
    AtEyeScanController (*EyeScanControllerGet)(AtSerdesController self);
    float (*SpeedInGbps)(AtSerdesController self);
    eAtRet (*EyeScanReset)(AtSerdesController self, uint32 laneId);

    /* Internal methods */
    uint32 (*HwParameterId)(AtSerdesController self, eAtSerdesParam param);
    AtPrbsEngine (*PrbsEngine)(AtSerdesController self);
    AtPrbsEngine (*PrbsEngineCreate)(AtSerdesController self);
    eAtRet (*PrbsEngineTypeSet)(AtSerdesController self, eAtSerdesPrbsEngineType type);
    eAtSerdesPrbsEngineType (*PrbsEngineTypeGet)(AtSerdesController self);
    uint32 (*Read)(AtSerdesController self, uint32 address, eAtModule moduleId);
    void (*Write)(AtSerdesController self, uint32 address, uint32 value, eAtModule moduleId);

    AtEyeScanController (*EyeScanControllerObjectCreate)(AtSerdesController self, uint32 baseAddress);
    eBool (*EyeScanIsSupported)(AtSerdesController self);
    uint32 (*EyeScanDrpBaseAddress)(AtSerdesController self);

    /* Some SERDESes can only be disabled in FPGA's Core context */
    eAtRet (*FpgaEnable)(AtSerdesController self, eBool enable);
    eBool (*FpgaIsEnabled)(AtSerdesController self);

    eBool (*CanControlTimingMode)(AtSerdesController self);
    eAtRet (*TimingModeSet)(AtSerdesController self, eAtSerdesTimingMode timingMode);
    eAtSerdesTimingMode (*TimingModeGet)(AtSerdesController self);

    uint32 (*HwIdGet)(AtSerdesController self);
    eBool (*IsGood)(AtSerdesController self);

    eAtRet (*PowerDown)(AtSerdesController self, eBool powerDown);
    eBool (*PowerIsDown)(AtSerdesController self);
    eBool (*PowerCanControl)(AtSerdesController self);
    eBool (*CanResetWhenPowerDown)(AtSerdesController self);

    /* SERDES auto reset when PPM cross threshold */
    eBool (*HasAutoResetPpmThreshold)(AtSerdesController self);
    uint32 (*AutoResetPpmThresholdGet)(AtSerdesController self);
    uint32 (*AutoResetPpmThresholdMax)(AtSerdesController self);
    eAtRet (*AutoResetPpmThresholdSet)(AtSerdesController self, uint32 ppmThreshold);

    /* For standby driver */
    uint32 (*StandbyCacheSize)(AtSerdesController self);
    void **(*StandbyCacheMemoryAddress)(AtSerdesController self);

    /* Sub lanes */
    AtSerdesController (*LaneGet)(AtSerdesController self, uint32 laneIdx);
    uint32 (*NumLanes)(AtSerdesController self);
    AtSerdesController (*LaneObjectCreate)(AtSerdesController self, uint32 laneIdx);
    AtSerdesController (*ParentGet)(AtSerdesController self);

    const char* (*Description)(AtSerdesController self);
    tAtAsyncOperationFunc (*AsyncOperationGet)(AtSerdesController self, uint32 state);
    }tAtSerdesControllerMethods;

typedef struct tAtSerdesControllerPropertyListener
    {
    void (*ModeDidChange)(AtSerdesController self, eAtSerdesMode mode, void *userData);
    }tAtSerdesControllerPropertyListener;

typedef struct tAtSerdesController
    {
    tAtObject super;
    const tAtSerdesControllerMethods *methods;

    /* Private data */
    AtChannel physicalPort;
    uint32 serdesId;
    uint32 hwId;
    AtPrbsEngine prbsEngine;
    AtEyeScanController eyeScanController;
    void *listeners;
    AtSerdesManager manager;
    uint32 asyncInitState;
    AtDrp drp;
    eAtSerdesControllerEyescanLogic eyescanLogic;

    /* Lanes */
    AtSerdesController *lanes;
    uint32 numLanes;
    AtSerdesController parent;

    AtList propertyListeners;
    }tAtSerdesController;

typedef struct tSerdesControllerEventListener
    {
    tAtSerdesControllerListener *callbacks;
    void *userData;
    }tSerdesControllerEventListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController AtSerdesControllerObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId);
AtSerdesController AtSerdesControllerObjectInitWithManager(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager);

eAtRet AtSerdesControllerHwIdSet(AtSerdesController self, uint32 hwId);
uint32 AtSerdesControllerHwParameterId(AtSerdesController self, eAtSerdesParam param);
eBool AtSerdesControllerCanEnable(AtSerdesController self, eBool enable);
void AtSerdesControllerAlarmNotify(AtSerdesController self, uint32 alarmType, uint32 currentStatus);
eBool AtSerdesControllerIsGood(AtSerdesController self);
void AtSerdesControllerStatusClear(AtSerdesController self);
eBool AtSerdesControllerInAccessible(AtSerdesController self);
eBool AtSerdesControllerAccessible(AtSerdesController self);
eBool AtSerdesControllerPhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value);
uint32 AtSerdesControllerBaseAddress(AtSerdesController self);
const char *AtSerdesControllerDefaultIdString(AtSerdesController self);

/* RX Alarms insertion. */
eAtRet AtSerdesControllerRxAlarmForce(AtSerdesController self, uint32 alarmType);
eAtRet AtSerdesControllerRxAlarmUnforce(AtSerdesController self, uint32 alarmType);
uint32 AtSerdesControllerRxForcedAlarmGet(AtSerdesController self);
uint32 AtSerdesControllerRxForcableAlarmsGet(AtSerdesController self);

eAtRet AtSerdesControllerFpgaEnable(AtSerdesController self, eBool enable);
eBool AtSerdesControllerFpgaIsEnabled(AtSerdesController self);

void *AtSerdesControllerStandbyCacheGet(AtSerdesController self);
uint32 AtSerdesControllerHwIdGet(AtSerdesController self);
void AtSerdesControllerPhysicalPortSet(AtSerdesController self, AtChannel physicalPort);
eBool AtSerdesControllerModeIsOcn(eAtSerdesMode mode);
eBool AtSerdesControllerModeIsEthernet(eAtSerdesMode mode);
eAtSerdesMode AtSerdesControllerOverSamplingMode(AtSerdesController self);

eAtRet AtSerdesControllerManagerSet(AtSerdesController self, AtSerdesManager manager);
eAtRet AtSerdesControllerAsyncInit(AtSerdesController self);

/* To determinate Speed of serdes in Gbps */
float AtSerdesControllerSpeedInGbps(AtSerdesController self);

void AtSerdesControllerEyescanLogicSet(AtSerdesController self, eAtSerdesControllerEyescanLogic logic);
eAtSerdesControllerEyescanLogic AtSerdesControllerEyescanLogicGet(AtSerdesController self);
eAtRet AtSerdesControllerEyeScanReset(AtSerdesController self, uint32 laneId);

eAtRet AtSerdesControllerLayerLoopbackSet(AtSerdesController self, eAtSerdesPhysicalLayer layer, eAtLoopbackMode loopbackMode);
eAtLoopbackMode AtSerdesControllerLayerLoopbackGet(AtSerdesController self, eAtSerdesPhysicalLayer layer);

uint32 AtSerdesControllerDrpBaseAddress(AtSerdesController self);
eAtRet AtSerdesControllerPropertyListenerAdd(AtSerdesController self, const tAtSerdesControllerPropertyListener* listener, void* userData);
eAtRet AtSerdesControllerPropertyListenerRemove(AtSerdesController self, const tAtSerdesControllerPropertyListener* listener, void* userData);

AtQuerier AtSerdesControllerQuerierGet(AtSerdesController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSERDESCONTROLLERINTERNAL_H_ */


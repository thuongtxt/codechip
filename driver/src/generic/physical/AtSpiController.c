/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtSpiController.c
 *
 * Created Date: Aug 11, 2014
 *
 * Description : SPI controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtOsal.h"
#include "AtSpiControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSpiControllerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ModeSet(AtSpiController self, eAtSpiControllerSpiMode mode)
    {
    AtUnused(mode);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtSpiControllerSpiMode ModeGet(AtSpiController self)
    {
    AtUnused(self);
    return cAtSpiControllerSpiModeUnknown;
    }

static uint32 NumPortsGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxCalendarPortIdSet(AtSpiController self, uint32 calendarIndex, uint32 portId)
    {
    AtUnused(portId);
    AtUnused(calendarIndex);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxCalendarPortIdGet(AtSpiController self, uint32 calendarIndex)
    {
    AtUnused(calendarIndex);
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxCalendarMaxSequenceLengthSet(AtSpiController self, uint32 length)
    {
    AtUnused(length);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxCalendarMaxSequenceLengthGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxCalendarSequenceLengthSet(AtSpiController self, uint32 length)
    {
    AtUnused(length);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxCalendarSequenceLengthGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxCalendarSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    AtUnused(numTimes);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxCalendarSequenceRepeatTimesGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxFifoStatusMaxBurst1Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    AtUnused(numBlocks);
    AtUnused(portId);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxFifoStatusMaxBurst1Get(AtSpiController self, uint32 portId)
    {
    AtUnused(portId);
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxFifoStatusMaxBurst2Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    AtUnused(numBlocks);
    AtUnused(portId);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxFifoStatusMaxBurst2Get(AtSpiController self, uint32 portId)
    {
    AtUnused(portId);
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxFifoStatusTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    AtUnused(numCycles);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxFifoStatusTrainingSequenceIntervalGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxDataPathTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    AtUnused(numCycles);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxDataPathTrainingSequenceIntervalGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet TxDataPathTrainingSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    AtUnused(numTimes);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TxDataPathTrainingSequenceRepeatTimesGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxCalendarPortIdSet(AtSpiController self, uint32 calendarIndex, uint32 portId)
    {
    AtUnused(portId);
    AtUnused(calendarIndex);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxCalendarPortIdGet(AtSpiController self, uint32 calendarIndex)
    {
    AtUnused(calendarIndex);
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxCalendarMaxSequenceLengthSet(AtSpiController self, uint32 length)
    {
    AtUnused(length);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxCalendarMaxSequenceLengthGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxCalendarSequenceLengthSet(AtSpiController self, uint32 length)
    {
    AtUnused(length);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxCalendarSequenceLengthGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxCalendarSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    AtUnused(numTimes);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxCalendarSequenceRepeatTimesGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxFifoStatusMaxBurst1Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    AtUnused(numBlocks);
    AtUnused(portId);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxFifoStatusMaxBurst1Get(AtSpiController self, uint32 portId)
    {
    AtUnused(portId);
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxFifoStatusMaxBurst2Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    AtUnused(numBlocks);
    AtUnused(portId);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxFifoStatusMaxBurst2Get(AtSpiController self, uint32 portId)
    {
    AtUnused(portId);
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxFifoStatusTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    AtUnused(numCycles);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxFifoStatusTrainingSequenceIntervalGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxDataPathTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    AtUnused(numCycles);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxDataPathTrainingSequenceIntervalGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet RxDataPathTrainingSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    AtUnused(numTimes);
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 RxDataPathTrainingSequenceRepeatTimesGet(AtSpiController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 Version(AtSpiController self)
    {
    AtUnused(self);
    return 0x04020000;
    }

static void MethodsInit(AtSpiController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Version);
        mMethodOverride(m_methods, ModeSet);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, NumPortsGet);

        /* TX direction */
        mMethodOverride(m_methods, TxCalendarPortIdSet);
        mMethodOverride(m_methods, TxCalendarPortIdGet);
        mMethodOverride(m_methods, TxCalendarMaxSequenceLengthSet);
        mMethodOverride(m_methods, TxCalendarMaxSequenceLengthGet);
        mMethodOverride(m_methods, TxCalendarSequenceLengthSet);
        mMethodOverride(m_methods, TxCalendarSequenceLengthGet);
        mMethodOverride(m_methods, TxCalendarSequenceRepeatTimesSet);
        mMethodOverride(m_methods, TxCalendarSequenceRepeatTimesGet);
        mMethodOverride(m_methods, TxFifoStatusMaxBurst1Set);
        mMethodOverride(m_methods, TxFifoStatusMaxBurst1Get);
        mMethodOverride(m_methods, TxFifoStatusMaxBurst2Set);
        mMethodOverride(m_methods, TxFifoStatusMaxBurst2Get);
        mMethodOverride(m_methods, TxFifoStatusTrainingSequenceIntervalSet);
        mMethodOverride(m_methods, TxFifoStatusTrainingSequenceIntervalGet);
        mMethodOverride(m_methods, TxDataPathTrainingSequenceIntervalSet);
        mMethodOverride(m_methods, TxDataPathTrainingSequenceIntervalGet);
        mMethodOverride(m_methods, TxDataPathTrainingSequenceRepeatTimesSet);
        mMethodOverride(m_methods, TxDataPathTrainingSequenceRepeatTimesGet);

        /* RX direction */
        mMethodOverride(m_methods, RxCalendarPortIdSet);
        mMethodOverride(m_methods, RxCalendarPortIdGet);
        mMethodOverride(m_methods, RxCalendarMaxSequenceLengthSet);
        mMethodOverride(m_methods, RxCalendarMaxSequenceLengthGet);
        mMethodOverride(m_methods, RxCalendarSequenceLengthSet);
        mMethodOverride(m_methods, RxCalendarSequenceLengthGet);
        mMethodOverride(m_methods, RxCalendarSequenceRepeatTimesSet);
        mMethodOverride(m_methods, RxCalendarSequenceRepeatTimesGet);
        mMethodOverride(m_methods, RxFifoStatusMaxBurst1Set);
        mMethodOverride(m_methods, RxFifoStatusMaxBurst1Get);
        mMethodOverride(m_methods, RxFifoStatusMaxBurst2Set);
        mMethodOverride(m_methods, RxFifoStatusMaxBurst2Get);
        mMethodOverride(m_methods, RxFifoStatusTrainingSequenceIntervalSet);
        mMethodOverride(m_methods, RxFifoStatusTrainingSequenceIntervalGet);
        mMethodOverride(m_methods, RxDataPathTrainingSequenceIntervalSet);
        mMethodOverride(m_methods, RxDataPathTrainingSequenceIntervalGet);
        mMethodOverride(m_methods, RxDataPathTrainingSequenceRepeatTimesSet);
        mMethodOverride(m_methods, RxDataPathTrainingSequenceRepeatTimesGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSpiController);
    }

AtSpiController AtSpiControllerObjectInit(AtSpiController self, AtModule module, uint32 phyId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->phyId = phyId;
    self->module = module;

    return self;
    }

/**
 * @addtogroup AtSpiController
 * @{
 */

/**
 * Set SPI mode
 *
 * @param self This controller
 * @param mode See @ref eAtSpiControllerSpiMode "SPI mode"
 *
 * @return AT return code
 */
eAtRet AtSpiControllerModeSet(AtSpiController self, eAtSpiControllerSpiMode mode)
    {
    mNumericalAttributeSet(ModeSet, mode);
    }

/**
 * Get SPI mode
 *
 * @param self This controller
 *
 * @return mode See @ref eAtSpiControllerSpiMode "SPI mode"
 */
eAtSpiControllerSpiMode AtSpiControllerModeGet(AtSpiController self)
    {
    mAttributeGet(ModeGet, eAtSpiControllerSpiMode, cAtSpiControllerSpiModeUnknown);
    }

/**
 * Get number of ports
 *
 * @param self This controller
 *
 * @return number of ports
 */
uint32 AtSpiControllerNumPortsGet(AtSpiController self)
    {
    mAttributeGet(NumPortsGet, uint32, cInvalidUint32);
    }

/**
 * Get physical ID
 *
 * @param self This controller
 *
 * @return Physical ID
 */
uint32 AtSpiControllerIdGet(AtSpiController self)
    {
    if (self)
        return self->phyId;
    return cInvalidUint32;
    }

/**
 * Set port Id for calendar element at TX direction
 *
 * @param self This controller
 * @param calendarIndex Calendar Index
 * @param portId Port Id
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxCalendarPortIdSet(AtSpiController self, uint32 calendarIndex, uint32 portId)
    {
    mTwoParamsAttributeSet(TxCalendarPortIdSet, calendarIndex, portId);
    }

/**
 * Get portId of calendar element at TX direction
 *
 * @param self This controller
 * @param calendarIndex Calendar Index
 *
 * @return Port Id of calendar element
 */
uint32 AtSpiControllerTxCalendarPortIdGet(AtSpiController self, uint32 calendarIndex)
    {
    if (self)
        return mMethodsGet(self)->TxCalendarPortIdGet(self, calendarIndex);
    return cInvalidUint32;
    }

/**
 * Set maximum length of the calendar sequence at TX direction
 *
 * @param self This controller
 * @param length Maximum length of calendar sequence
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxCalendarMaxSequenceLengthSet(AtSpiController self, uint32 length)
    {
    mNumericalAttributeSet(TxCalendarMaxSequenceLengthSet, length);
    }

/**
 * Get maximum length of the calendar sequence at TX direction
 *
 * @param self This controller
 *
 * @return maximum length of the calendar sequence
 */
uint32 AtSpiControllerTxCalendarMaxSequenceLengthGet(AtSpiController self)
    {
    mAttributeGet(TxCalendarMaxSequenceLengthGet, uint32, cInvalidUint32);
    }

/**
 * Set length of the calendar sequence at TX direction
 *
 * @param self This controller
 * @param length Sequence length of calendar
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxCalendarSequenceLengthSet(AtSpiController self, uint32 length)
    {
    mNumericalAttributeSet(TxCalendarSequenceLengthSet, length);
    }

/**
 * Get sequence length of calendar at TX direction
 *
 * @param self This controller
 *
 * @return Length of the calendar sequence of calendar
 */
uint32 AtSpiControllerTxCalendarSequenceLengthGet(AtSpiController self)
    {
    mAttributeGet(TxCalendarSequenceLengthGet, uint32, cInvalidUint32);
    }

/**
 * Set Number of times calendar sequence is repeated at TX direction
 *
 * @param self This controller
 * @param numTimes Number of times calendar sequence is repeated
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxCalendarSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    mNumericalAttributeSet(TxCalendarSequenceRepeatTimesSet, numTimes);
    }

/**
 * Get Number of times calendar sequence is repeated at TX direction
 *
 * @param self This controller
 *
 * @return Number of times calendar sequence is repeated
 */
uint32 AtSpiControllerTxCalendarSequenceRepeatTimesGet(AtSpiController self)
    {
    mAttributeGet(TxCalendarSequenceRepeatTimesGet, uint32, cInvalidUint32);
    }

/**
 * Set maximum number of 16 byte blocks that the FIFO can accept when FIFO Status
 * channel indicates Starving at TX direction
 *
 * @param self This controller
 * @param portId Port Id
 * @param numBlocks Number of 16 byte blocks
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxFifoStatusMaxBurst1Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    mTwoParamsAttributeSet(TxFifoStatusMaxBurst1Set, portId, numBlocks);
    }

/**
 * Get maximum number of 16 byte blocks that the FIFO can accept when FIFO Status
 * channel indicates Starving at TX direction
 *
 * @param self This controller
 * @param portId Port Id
 *
 * @return Number of 16 byte blocks
 */
uint32 AtSpiControllerTxFifoStatusMaxBurst1Get(AtSpiController self, uint32 portId)
    {
    if (self)
        return mMethodsGet(self)->TxFifoStatusMaxBurst1Get(self, portId);
    return cInvalidUint32;
    }

/**
 * Set maximum number of 16 byte blocks that the FIFO can accept when FIFO
 * Status channel indicates Hungry at TX direction
 *
 * @param self This controller
 * @param portId Port Id
 * @param numBlocks Number of 16 byte blocks
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxFifoStatusMaxBurst2Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    mTwoParamsAttributeSet(TxFifoStatusMaxBurst2Set, portId, numBlocks);
    }

/**
 * Get maximum number of 16 byte blocks that the FIFO can accept when FIFO
 * Status channel indicates Hungry at TX direction
 *
 * @param self This controller
 * @param portId Port Id
 *
 * @return Number of 16 byte blocks
 */
uint32 AtSpiControllerTxFifoStatusMaxBurst2Get(AtSpiController self, uint32 portId)
    {
    if (self)
        return mMethodsGet(self)->TxFifoStatusMaxBurst2Get(self, portId);
    return cInvalidUint32;
    }

/**
 * Set maximum interval between scheduling of training sequences on FIFO Status
 * Path interface at TX direction
 *
 * @param self This controller
 * @param numCycles Number of cycles
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxFifoStatusTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    mNumericalAttributeSet(TxFifoStatusTrainingSequenceIntervalSet, numCycles);
    }

/**
 * Get maximum interval between scheduling of training sequences on FIFO Status
 * Path interface at TX direction
 *
 * @param self This controller
 *
 * @return Number of cycles
 */
uint32 AtSpiControllerTxFifoStatusTrainingSequenceIntervalGet(AtSpiController self)
    {
    mAttributeGet(TxFifoStatusTrainingSequenceIntervalGet, uint32, cInvalidUint32);
    }

/**
 * Set maximum interval between scheduling of training sequences on Data Path
 * interface (DATA_MAX_T) at TX direction
 *
 * @param self This controller
 * @param numCycles Number of cycles
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxDataPathTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    mNumericalAttributeSet(TxDataPathTrainingSequenceIntervalSet, numCycles);
    }

/**
 * Get maximum interval between scheduling of training sequences on Data Path
 * interface at TX direction
 *
 * @param self This controller
 *
 * @return Number of cycles
 */
uint32 AtSpiControllerTxDataPathTrainingSequenceIntervalGet(AtSpiController self)
    {
    mAttributeGet(TxDataPathTrainingSequenceIntervalGet, uint32, cInvalidUint32);
    }

/**
 * Set number of repetitions of the data training sequence that must be scheduled
 * every DATA_MAX_T cycles at TX direction
 *
 * @param self This controller
 * @param numTimes Number of repetitions
 *
 * @return AT return code
 */
eAtRet AtSpiControllerTxDataPathTrainingSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    mNumericalAttributeSet(TxDataPathTrainingSequenceRepeatTimesSet, numTimes);
    }

/**
 * Get number of repetitions of the data training sequence that must be scheduled
 * every DATA_MAX_T cycles at TX direction
 *
 * @param self This controller
 *
 * @return Number of repetitions
 */
uint32 AtSpiControllerTxDataPathTrainingSequenceRepeatTimesGet(AtSpiController self)
    {
    mAttributeGet(TxDataPathTrainingSequenceRepeatTimesGet, uint32, cInvalidUint32);
    }

/**
 * Set port Id for calendar element at RX direction
 *
 * @param self This controller
 * @param calendarIndex Calendar Index
 * @param portId Port Id
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxCalendarPortIdSet(AtSpiController self, uint32 calendarIndex, uint32 portId)
    {
    mTwoParamsAttributeSet(RxCalendarPortIdSet, calendarIndex, portId);
    }

/**
 * Get portId of calendar element at RX direction
 *
 * @param self This controller
 * @param calendarIndex Calendar Index
 *
 * @return Port Id of calendar element
 */
uint32 AtSpiControllerRxCalendarPortIdGet(AtSpiController self, uint32 calendarIndex)
    {
    if (self)
        return mMethodsGet(self)->RxCalendarPortIdGet(self, calendarIndex);
    return cInvalidUint32;
    }

/**
 * Set maximum length of the calendar sequence at RX direction
 *
 * @param self This controller
 * @param length maximum length of calendar sequence
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxCalendarMaxSequenceLengthSet(AtSpiController self, uint32 length)
    {
    mNumericalAttributeSet(RxCalendarMaxSequenceLengthSet, length);
    }

/**
 * Get maximum length of the calendar sequence at RX direction
 *
 * @param self This controller
 *
 * @return maximum length of the calendar sequence
 */
uint32 AtSpiControllerRxCalendarMaxSequenceLengthGet(AtSpiController self)
    {
    mAttributeGet(RxCalendarMaxSequenceLengthGet, uint32, cInvalidUint32);
    }

/**
 * Set length of the calendar sequence at RX direction
 *
 * @param self This controller
 * @param length Sequence length of calendar
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxCalendarSequenceLengthSet(AtSpiController self, uint32 length)
    {
    mNumericalAttributeSet(RxCalendarSequenceLengthSet, length);
    }

/**
 * Get sequence length of calendar at RX direction
 *
 * @param self This controller
 *
 * @return Length of the calendar sequence of calendar
 */
uint32 AtSpiControllerRxCalendarSequenceLengthGet(AtSpiController self)
    {
    mAttributeGet(RxCalendarSequenceLengthGet, uint32, cInvalidUint32);
    }

/**
 * Set Number of times calendar sequence is repeated at RX direction
 *
 * @param self This controller
 * @param numTimes Number of times calendar sequence is repeated
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxCalendarSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    mNumericalAttributeSet(RxCalendarSequenceRepeatTimesSet, numTimes);
    }

/**
 * Get Number of times calendar sequence is repeated at RX direction
 *
 * @param self This controller
 *
 * @return Number of times calendar sequence is repeated
 */
uint32 AtSpiControllerRxCalendarSequenceRepeatTimesGet(AtSpiController self)
    {
    mAttributeGet(RxCalendarSequenceRepeatTimesGet, uint32, cInvalidUint32);
    }

/**
 * Set maximum number of 16 byte blocks that the FIFO can accept
 * when FIFO Status channel indicates Starving at RX direction
 *
 * @param self This controller
 * @param portId Port Id
 * @param numBlocks Number of 16 byte blocks
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxFifoStatusMaxBurst1Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    mTwoParamsAttributeSet(RxFifoStatusMaxBurst1Set, portId, numBlocks);
    }

/**
 * Get maximum number of 16 byte blocks that the FIFO can accept
 * when FIFO Status channel indicates Starving at RX direction
 *
 * @param self This controller
 * @param portId Port Id
 *
 * @return Number of 16 byte blocks
 */
uint32 AtSpiControllerRxFifoStatusMaxBurst1Get(AtSpiController self, uint32 portId)
    {
    if (self)
        return mMethodsGet(self)->RxFifoStatusMaxBurst1Get(self, portId);
    return cInvalidUint32;
    }

/**
 * Set maximum number of 16 byte blocks that the FIFO can accept
 * when FIFO Status channel indicates Hungry at RX direction
 *
 * @param self This controller
 * @param portId Port Id
 * @param numBlocks Number of 16 byte blocks
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxFifoStatusMaxBurst2Set(AtSpiController self, uint32 portId, uint32 numBlocks)
    {
    mTwoParamsAttributeSet(RxFifoStatusMaxBurst2Set, portId, numBlocks);
    }

/**
 * Get maximum number of 16 byte blocks that the FIFO can accept
 * when FIFO Status channel indicates Hungry at RX direction
 *
 * @param self This controller
 * @param portId Port Id
 *
 * @return Number of 16 byte blocks
 */
uint32 AtSpiControllerRxFifoStatusMaxBurst2Get(AtSpiController self, uint32 portId)
    {
    if (self)
        return mMethodsGet(self)->RxFifoStatusMaxBurst2Get(self, portId);
    return cInvalidUint32;
    }

/**
 * Set maximum interval between scheduling of training sequences on
 * FIFO Status Path interface at RX direction
 *
 * @param self This controller
 * @param numCycles Number of cycles
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxFifoStatusTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    mNumericalAttributeSet(RxFifoStatusTrainingSequenceIntervalSet, numCycles);
    }

/**
 * Get maximum interval between scheduling of training sequences on
 * FIFO Status Path interface at RX direction
 *
 * @param self This controller
 *
 * @return Number of cycles
 */
uint32 AtSpiControllerRxFifoStatusTrainingSequenceIntervalGet(AtSpiController self)
    {
    mAttributeGet(RxFifoStatusTrainingSequenceIntervalGet, uint32, cInvalidUint32);
    }

/**
 * Set maximum interval between scheduling of training sequences on
 * Data Path interface (DATA_MAX_T) at RX direction
 *
 * @param self This controller
 * @param numCycles Number of cycles
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxDataPathTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles)
    {
    mNumericalAttributeSet(RxDataPathTrainingSequenceIntervalSet, numCycles);
    }

/**
 * Get maximum interval between scheduling of training sequences on
 * Data Path interface at RX direction
 *
 * @param self This controller
 *
 * @return Number of cycles
 */
uint32 AtSpiControllerRxDataPathTrainingSequenceIntervalGet(AtSpiController self)
    {
    mAttributeGet(RxDataPathTrainingSequenceIntervalGet, uint32, cInvalidUint32);
    }

/**
 * Set number of repetitions of the data training sequence that must be
 * scheduled every DATA_MAX_T cycles at RX direction
 *
 * @param self This controller
 * @param numTimes Number of repetitions
 *
 * @return AT return code
 */
eAtRet AtSpiControllerRxDataPathTrainingSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes)
    {
    mNumericalAttributeSet(RxDataPathTrainingSequenceRepeatTimesSet, numTimes);
    }

/**
 * Get number of repetitions of the data training sequence that must be
 * scheduled every DATA_MAX_T cycles at RX direction
 *
 * @param self This controller
 *
 * @return Number of repetitions
 */
uint32 AtSpiControllerRxDataPathTrainingSequenceRepeatTimesGet(AtSpiController self)
    {
    mAttributeGet(RxDataPathTrainingSequenceRepeatTimesGet, uint32, cInvalidUint32);
    }

/**
 * Get SPI version
 *
 * @param self This controller
 *
 * @return Version description
 *         - major:    bit[31:24]
 *         - minor:    bit[23:16]
 *         - reserved: bit[15:0]
 */
uint32 AtSpiControllerVersion(AtSpiController self)
    {
    mAttributeGet(Version, uint32, cInvalidUint32);
    }

/**
 * @}
 */

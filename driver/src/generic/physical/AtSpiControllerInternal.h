/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSpiControllerInternal.h
 * 
 * Created Date: Aug 11, 2014
 *
 * Description : SPI controller. To configure parameters of SPI physical
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSPICONTROLLERINTERNAL_H_
#define _ATSPICONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSpiController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSpiControllerMethods
    {
    uint32 (*Version)(AtSpiController self);
    eAtRet (*ModeSet)(AtSpiController self, eAtSpiControllerSpiMode mode);
    eAtSpiControllerSpiMode (*ModeGet)(AtSpiController self);
    uint32 (*NumPortsGet)(AtSpiController self);

    /* Tx direction */
    eAtRet (*TxCalendarPortIdSet)(AtSpiController self, uint32 calendarIndex, uint32 portId);
    uint32 (*TxCalendarPortIdGet)(AtSpiController self, uint32 calendarIndex);
    eAtRet (*TxCalendarMaxSequenceLengthSet)(AtSpiController self, uint32 length);
    uint32 (*TxCalendarMaxSequenceLengthGet)(AtSpiController self);
    eAtRet (*TxCalendarSequenceLengthSet)(AtSpiController self, uint32 length);
    uint32 (*TxCalendarSequenceLengthGet)(AtSpiController self);
    eAtRet (*TxCalendarSequenceRepeatTimesSet)(AtSpiController self, uint32 numTimes);
    uint32 (*TxCalendarSequenceRepeatTimesGet)(AtSpiController self);
    eAtRet (*TxFifoStatusMaxBurst1Set)(AtSpiController self, uint32 portId, uint32 numBlocks);
    uint32 (*TxFifoStatusMaxBurst1Get)(AtSpiController self, uint32 portId);
    eAtRet (*TxFifoStatusMaxBurst2Set)(AtSpiController self, uint32 portId, uint32 numBlocks);
    uint32 (*TxFifoStatusMaxBurst2Get)(AtSpiController self, uint32 portId);
    eAtRet (*TxFifoStatusTrainingSequenceIntervalSet)(AtSpiController self, uint32 numCycles);
    uint32 (*TxFifoStatusTrainingSequenceIntervalGet)(AtSpiController self);
    eAtRet (*TxDataPathTrainingSequenceIntervalSet)(AtSpiController self, uint32 numCycles);
    uint32 (*TxDataPathTrainingSequenceIntervalGet)(AtSpiController self);
    eAtRet (*TxDataPathTrainingSequenceRepeatTimesSet)(AtSpiController self, uint32 numTimes);
    uint32 (*TxDataPathTrainingSequenceRepeatTimesGet)(AtSpiController self);

    /* Rx direction */
    eAtRet (*RxCalendarPortIdSet)(AtSpiController self, uint32 calendarIndex, uint32 portId);
    uint32 (*RxCalendarPortIdGet)(AtSpiController self, uint32 calendarIndex);
    eAtRet (*RxCalendarMaxSequenceLengthSet)(AtSpiController self, uint32 length);
    uint32 (*RxCalendarMaxSequenceLengthGet)(AtSpiController self);
    eAtRet (*RxCalendarSequenceLengthSet)(AtSpiController self, uint32 length);
    uint32 (*RxCalendarSequenceLengthGet)(AtSpiController self);
    eAtRet (*RxCalendarSequenceRepeatTimesSet)(AtSpiController self, uint32 numTimes);
    uint32 (*RxCalendarSequenceRepeatTimesGet)(AtSpiController self);
    eAtRet (*RxFifoStatusMaxBurst1Set)(AtSpiController self, uint32 portId, uint32 numBlocks);
    uint32 (*RxFifoStatusMaxBurst1Get)(AtSpiController self, uint32 portId);
    eAtRet (*RxFifoStatusMaxBurst2Set)(AtSpiController self, uint32 portId, uint32 numBlocks);
    uint32 (*RxFifoStatusMaxBurst2Get)(AtSpiController self, uint32 portId);
    eAtRet (*RxFifoStatusTrainingSequenceIntervalSet)(AtSpiController self, uint32 numCycles);
    uint32 (*RxFifoStatusTrainingSequenceIntervalGet)(AtSpiController self);
    eAtRet (*RxDataPathTrainingSequenceIntervalSet)(AtSpiController self, uint32 numCycles);
    uint32 (*RxDataPathTrainingSequenceIntervalGet)(AtSpiController self);
    eAtRet (*RxDataPathTrainingSequenceRepeatTimesSet)(AtSpiController self, uint32 numTimes);
    uint32 (*RxDataPathTrainingSequenceRepeatTimesGet)(AtSpiController self);
    }tAtSpiControllerMethods;

typedef struct tAtSpiController
    {
    tAtObject super;
    const tAtSpiControllerMethods *methods;

    uint32 phyId;
    AtModule module;
    }tAtSpiController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSpiController AtSpiControllerObjectInit(AtSpiController self, AtModule module, uint32 phyId);

#ifdef __cplusplus
}
#endif
#endif /* _ATSPICONTROLLERINTERNAL_H_ */


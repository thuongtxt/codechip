/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtEyeScanDrpRegV1.h
 * 
 * Created Date: Aug 24, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEYESCANDRPREGV1_H_
#define _ATEYESCANDRPREGV1_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
#define DRP_ADDR_ES_CONTROL_REG       0x03C
#define DRP_ADDR_ES_CONTROL_MASK      cBit15_10
#define DRP_ADDR_ES_CONTROL_SHIFT     10
#define DRP_ADDR_ES_ERRDET_EN_MASK    cBit9
#define DRP_ADDR_ES_ERRDET_EN_SHIFT   9
#define DRP_ADDR_ES_EYESCAN_EN_MASK   cBit8
#define DRP_ADDR_ES_EYESCAN_EN_SHIFT  8


#define DRP_ADDR_ES_PRESCALE          0x03C
#define DRP_ADDR_ES_PRESCALE_MASK     cBit4_0
#define DRP_ADDR_ES_PRESCALE_SHIFT    0

#define DRP_ADDR_ES_VERT_OFFSET            0x097
#define DRP_ADDR_ES_VS_NEG_DIR_MASK        cBit10
#define DRP_ADDR_ES_VS_NEG_DIR_SHIFT       10
#define DRP_ADDR_ES_VERT_OFFSET_MASK       cBit8_2
#define DRP_ADDR_ES_VERT_OFFSET_SHIFT      2
#define DRP_ADDR_ES_VERT_UTSIGN_MASK       cBit9
#define DRP_ADDR_ES_VERT_UTSIGN_SHIFT      9

#define DRP_ADDR_ES_HORZ_OFFSET       0x04F
#define DRP_ADDR_ES_HORZ_OFFSET_MASK  cBit15_4
#define DRP_ADDR_ES_HORZ_OFFSET_SHIFT 4

#define DRP_ADDR_ES_CONTROL_STATUS 0x153
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_MASK  cBit3_1
#define DRP_ADDR_ES_CONTROL_CURRENT_STATE_SHIFT 1
#define DRP_ADDR_ES_CONTROL_DONE_MASK  cBit0
#define DRP_ADDR_ES_CONTROL_DONE_SHIFT 0


#define DRP_ADDR_ES_ERROR_COUNT    0x151
#define DRP_ADDR_ES_SAMPLE_COUNT   0x152

#define DRP_ADDR_ES_QUAL_MASK0     0x044
#define DRP_ADDR_ES_QUAL_MASK1     0x045
#define DRP_ADDR_ES_QUAL_MASK2     0x046
#define DRP_ADDR_ES_QUAL_MASK3     0x047
#define DRP_ADDR_ES_QUAL_MASK4     0x048

#define DRP_ADDR_ES_SDATA_MASK0    0x049
#define DRP_ADDR_ES_SDATA_MASK1    0x04A
#define DRP_ADDR_ES_SDATA_MASK2    0x04B
#define DRP_ADDR_ES_SDATA_MASK3    0x04C
#define DRP_ADDR_ES_SDATA_MASK4    0x04D


#define DRP_ADDR_RX_DATA_WIDTH       0x03
#define DRP_ADDR_RX_DATA_WIDTH_MASK  cBit8_5
#define DRP_ADDR_RX_DATA_WIDTH_SHIFT 5

#define DRP_ADDR_RXOUT_DIV           0x063
#define DRP_ADDR_RXOUT_DIV_MASK      cBit2_0
#define DRP_ADDR_RXOUT_DIV_SHIFT     0


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATEYESCANDRPREGV1_H_ */


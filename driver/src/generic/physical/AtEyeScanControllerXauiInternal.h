/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtEyeScanControllerXauiInternal.h
 * 
 * Created Date: Jun 1, 2016
 *
 * Description : Eye scan controller for XAUI
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEYESCANCONTROLLERXAUIINTERNAL_H_
#define _ATEYESCANCONTROLLERXAUIINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEyeScanControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEyeScanControllerXaui
    {
    tAtEyeScanController super;
    }tAtEyeScanControllerXaui;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEyeScanController AtEyeScanControllerXauiObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _ATEYESCANCONTROLLERXAUIINTERNAL_H_ */


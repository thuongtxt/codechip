/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtUart.c
 *
 * Created Date: Apr 20, 2016
 *
 * Description : AtUart implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../util/AtUtil.h"
#include "AtUartInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidSlrId 0xFF
#define mUartIsValid(self) (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtUartMethods m_methods;

/* To override super implementation */
static tAtObjectMethods m_AtObjectOverride;

/* Super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtObject self)
    {
    AtDevice dev = AtUartDeviceGet((AtUart)self);
    static char str[64];

    AtSprintf(str, "%suart", AtDeviceIdToString(dev));
    return str;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtUart object = (AtUart)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(device);
    mEncodeUInt(slrId);
    }

static eAtRet Init(AtUart self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet Transmit(AtUart self, const char *buffer, uint32 bufferSize)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(bufferSize);
    return cAtErrorNotImplemented;
    }

static uint32 Receive(AtUart self, char *buffer, uint32 bufferSize)
    {
    AtUnused(self);
    AtUnused(buffer);
    AtUnused(bufferSize);
    return 0;
    }

static void Debug(AtUart self)
    {
    AtUnused(self);
    }

static void ReceiveDataWait(AtUart self)
    {
    AtUnused(self);
    }

static void OverrideAtObject(AtUart self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet((AtObject)self);

        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

static void Override(AtUart self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtUart self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Transmit);
        mMethodOverride(m_methods, Receive);
        mMethodOverride(m_methods, ReceiveDataWait);
        mMethodOverride(m_methods, Debug);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtUart);
    }

AtUart AtUartObjectInit(AtUart self, AtDevice device, uint8 slrId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);
    Override(self);

    self->device = device;
    self->slrId = slrId;
    m_methodsInit = 1;
    return self;
    }

/**
 * @addtogroup AtUart
 * @{
 */

/**
 * Transmit a UART command to SEM controller
 *
 * @param self This UART
 * @param buffer Buffer to contain UART command
 * @param bufferSize Buffer size
 *
 * @return AT return code
 */
eAtRet AtUartTransmit(AtUart self, const char *buffer, uint32 bufferSize)
    {
    mBufferAttributeSet(Transmit, buffer, bufferSize);
    }

/**
 * Receive buffer result sent from SEM controller
 *
 * @param self This UART
 * @param buffer Buffer to store result
 * @param bufferSize Buffer size
 *
 * @return 0 if no any data received, or number of characters of returned data
 */
uint32 AtUartReceive(AtUart self, char *buffer, uint32 bufferSize)
    {
    if (mUartIsValid(self))
        return mMethodsGet(self)->Receive(self, buffer, bufferSize);
    return 0;
    }

/**
 * Get Super Logic Region ID
 *
 * @param self UART
 *
 * @return Super Logic Region ID
 */
uint8 AtUartSuperLogicRegionIdGet(AtUart self)
    {
    if (mUartIsValid(self))
        return self->slrId;
    return cInvalidSlrId;
    }

/**
 * Get device associated to this UART
 *
 * @param self This UART
 *
 * @return Device
 */
AtDevice AtUartDeviceGet(AtUart self)
    {
    if (mUartIsValid(self))
        return self->device;
    return NULL;
    }

/**
 * Show debug information
 *
 * @param self This UART
 */
void AtUartDebug(AtUart self)
    {
    if (mUartIsValid(self))
        mMethodsGet(self)->Debug(self);
    }

/**
 * Wait to Receive data from UART
 *
 * @param self This UART
 */
void AtUartReceiveDataWait(AtUart self)
	{
	if (mUartIsValid(self))
		mMethodsGet(self)->ReceiveDataWait(self);
	}


/**
 * @}
 */

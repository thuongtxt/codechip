/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanLaneXaui.c
 *
 * Created Date: Feb 25, 2015
 *
 * Description : XAUI lane
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanLaneInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Override(AtEyeScanLane self)
    {
    AtUnused(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanLaneXaui);
    }

AtEyeScanLane AtEyeScanLaneXauiObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanLaneObjectInit(self, controller, laneId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanLane AtEyeScanLaneXauiNew(AtEyeScanController controller, uint8 laneId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanLane newLane = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanLaneXauiObjectInit(newLane, controller, laneId);
    }

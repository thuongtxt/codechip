/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : AtSensor.c
 *
 * Created Date: Dec 15, 2015
 *
 * Description : Generic implement of physical sensor.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDriverInternal.h"
#include "AtSensorInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mSensorIsValid(self) (self ? cAtTrue : cAtFalse)
#define cAtMaxNumEventListener 10

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (AtSensor)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSensorMethods m_methods;

/* To override super implementation */
static tAtObjectMethods m_AtObjectOverride;

/* Reference to super implementation (option) */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AlarmGet(AtSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 AlarmHistoryGet(AtSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 AlarmHistoryClear(AtSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet InterruptMaskSet(AtSensor self, uint32 alarmMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(alarmMask);
    AtUnused(enableMask);
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtSensor self)
    {
    AtUnused(self);
    return 0;
    }

static void InterruptProcess(AtSensor self, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(ipCore);
    }

static AtSensorEventListener EventListenerObjectInit(AtSensorEventListener self, tAtSensorEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initialize memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSensorEventListenerWrapper));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize private data */
    mMethodsGet(osal)->MemCpy(osal, &(self->listener), callbacks, sizeof(tAtSensorEventListener));
    self->userData = userData;

    return self;
    }

static AtSensorEventListener EventListenerNew(tAtSensorEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtSensorEventListener newListener = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtSensorEventListenerWrapper));

    if (newListener)
        return EventListenerObjectInit(newListener, callbacks, userData);

    return NULL;
    }

static eBool TwoListenersAreIdenticalWithUserData(AtSensorEventListener self, tAtSensorEventListener * listener, void* userData)
    {
    if ((self->listener.AlarmChangeStateNotify != listener->AlarmChangeStateNotify) ||
        (self->userData != userData))
        return cAtFalse;

    return cAtTrue;
    }

static eBool TwoListenersAreIdentical(AtSensorEventListener self, tAtSensorEventListener * listener)
    {
    if (self->listener.AlarmChangeStateNotify != listener->AlarmChangeStateNotify)
        return cAtFalse;

    return cAtTrue;
    }

static eBool ListenerIsExistingInChannel(AtSensor self, tAtSensorEventListener *listener, void *userData)
    {
    AtIterator iterator = AtListIteratorCreate(self->allEventListeners);
    AtSensorEventListener aListener;
    eBool isExisting = cAtFalse;

    /* For all event listeners */
    while ((aListener = (AtSensorEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (TwoListenersAreIdenticalWithUserData(aListener, listener, userData))
            {
            isExisting = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    return isExisting;
    }

static eAtRet EventListenerAdd(AtSensor self, tAtSensorEventListener *listener, void *userData)
    {
    eAtRet atRet = cAtError;
    AtSensorEventListener newListener;

    if (self->allEventListeners == NULL)
        {
        self->listenerListMutex = AtOsalMutexCreate();
        self->allEventListeners = AtListCreate(cAtMaxNumEventListener);
        }

    if (ListenerIsExistingInChannel(self, listener, userData))
        return cAtOk;

    newListener = EventListenerNew(listener, userData);
    if (newListener == NULL)
        return cAtError;

    /* Only add a new listener */
    AtOsalMutexLock(self->listenerListMutex);
    atRet = AtListObjectAdd(self->allEventListeners, (AtObject)newListener);
    AtOsalMutexUnLock(self->listenerListMutex);

    return atRet;
    }

static void AlarmListenerCall(AtSensorEventListener self, AtSensor sensor, uint32 changedAlarms, uint32 currentAlarms)
    {
    if (self->listener.AlarmChangeStateNotify)
        self->listener.AlarmChangeStateNotify(sensor, changedAlarms, currentAlarms, self->userData);
    }

static void Delete(AtObject self)
    {
    AtSensor controller = (AtSensor)self;
    AtIterator iterator ;

    if (controller->allEventListeners)
        {
        AtOsalMutexLock(controller->listenerListMutex);

        /* Delete all listeners in list */
        iterator = AtListIteratorCreate(controller->allEventListeners);
        if (iterator)
            {
            AtSensorEventListener aListener;
            while((aListener = (AtSensorEventListener)AtIteratorNext(iterator)) != NULL)
                AtObjectDelete((AtObject)aListener);
            }

        AtObjectDelete((AtObject)iterator);

        /* Delete event listener list */
        AtObjectDelete((AtObject)(controller->allEventListeners));
        controller->allEventListeners = NULL;

        AtOsalMutexUnLock(controller->listenerListMutex);

        /* Delete private attribute */
        AtOsalMutexDestroy(controller->listenerListMutex);
        controller->listenerListMutex = NULL;
        }

    /* Call object delete */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSensor object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(device);

    mEncodeNone(allEventListeners);
    mEncodeNone(listenerListMutex);
    }

static const char *ToString(AtObject self)
    {
    AtDevice dev = AtSensorDeviceGet((AtSensor)self);
    static char str[64];

    AtSprintf(str, "%ssensor", AtDeviceIdToString(dev));
    return str;
    }

static uint32 SupportedAlarms(AtSensor self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet Init(AtSensor self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtObject(AtSensor self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        /* Save reference to super implementation */
        m_AtObjectMethods = mMethodsGet((AtObject)self);

        /* Copy to reuse implementation of super class. But override some methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

static void Override(AtSensor self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtSensor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, EventListenerAdd);
        mMethodOverride(m_methods, SupportedAlarms);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, Init);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSensor);
    }

AtSensor AtSensorObjectInit(AtSensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    self->device = device;

    return self;
    }

void AtSensorAllAlarmListenersCall(AtSensor self, uint32 changedAlarms, uint32 currentAlarms)
    {
    AtSensorEventListener aListener;
    AtIterator iterator;

    if ((self == NULL) || (self->allEventListeners == NULL))
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(self->allEventListeners);
    while((aListener = (AtSensorEventListener)AtIteratorNext(iterator)) != NULL)
        AlarmListenerCall(aListener, self, changedAlarms, currentAlarms);

    AtObjectDelete((AtObject)iterator);
    }

void AtSensorInterruptProcess(AtSensor self, AtIpCore ipCore)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self, ipCore);
    }

/**
 * @addtogroup AtSensor
 * @{
 */

/**
 * To get current alarms
 *
 * @param self This physical Sensor
 *
 * @return sensor current alarm
 *
 * @note Each type of Sensor have predefined alarm masks. Refer:
 *       - @ref eAtThermalSensorAlarmType "Thermal Sensor alarm types"
 *       This API ORed all of happening alarms and return
 */
uint32 AtSensorAlarmGet(AtSensor self)
    {
    mAttributeGet(AlarmGet, uint32, 0);
    }

/**
 * To get history alarms
 *
 * @param self This physical Sensor
 *
 * @return sensor alarm history
 *
 * @note Each type of Sensor have predefined alarm masks. Refer:
 *       - @ref eAtThermalSensorAlarmType "Thermal Sensor alarm types"
 *       This API ORed all of happening alarms and return
 */
uint32 AtSensorAlarmHistoryGet(AtSensor self)
    {
    mAttributeGet(AlarmHistoryGet, uint32, 0);
    }

/**
 * To get and clear history alarms
 *
 * @param self This physical Sensor
 *
 * @return sensor alarm history
 *
 * @note Each type of Sensor have predefined alarm masks. Refer:
 *       - @ref eAtThermalSensorAlarmType "Thermal Sensor alarm types"
 *       This API ORed all of happening alarms and return
 */
uint32 AtSensorAlarmHistoryClear(AtSensor self)
    {
    mAttributeGet(AlarmHistoryClear, uint32, 0);
    }

/**
 * Set interrupt mask
 *
 * @param self This physical Sensor
 * @param alarmMask Mask of defects that will be configured
 * @param enableMask Interrupt mask of each defect
 *
 * @return AT return code
 *
 * @note Each type of Sensor have predefined alarm masks. Refer:
 *      - @ref eAtThermalSensorAlarmType "Thermal Sensor alarm types"
 */
eAtRet AtSensorInterruptMaskSet(AtSensor self, uint32 alarmMask, uint32 enableMask)
    {
    mTwoParamsAttributeSet(InterruptMaskSet, alarmMask, enableMask);
    }

/**
 * Get interrupt mask
 *
 * @param self This physical Sensor
 *
 * @return InterruptMask
 */
uint32 AtSensorInterruptMaskGet(AtSensor self)
    {
    mAttributeGet(InterruptMaskGet, uint32, 0);
    }

/**
 * Add event listener. When there is any alarm corresponding interface will be called.
 * Default listener interface is @ref tAtSensorEventListener
 * Note, each sensor type can have its own event listener interface, so this
 * default listener interface must not be used.
 *
 * Application just implemented functions defined in event listener interface in
 * order to receiving event.
 *
 * @param self This Sensor
 * @param listener Event listener
 * @param userData User data that will be input when callback is called
 *
 * @return AT return code
 */
eAtRet AtSensorEventListenerAdd(AtSensor self, tAtSensorEventListener *listener, void *userData)
    {
    if (mSensorIsValid(self))
        return EventListenerAdd(self, listener, userData);

    return cAtError;
    }

/**
 * Remove event listener
 *
 * @param self This physical Sensor
 * @param listener Event listener
 *
 * @return AT return code
 */
eAtRet AtSensorEventListenerRemove(AtSensor self, tAtSensorEventListener *listener)
    {
    /* Search to find listener match with input information */
    AtSensorEventListener aListener;
    AtIterator iterator;

    if (!mSensorIsValid(self))
        return cAtErrorNullPointer;

    if (self->allEventListeners == NULL)
        return cAtOk;

    iterator = AtListIteratorCreate(self->allEventListeners);

    AtOsalMutexLock(self->listenerListMutex);

    /* For all event listeners */
    while((aListener = (AtSensorEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (!TwoListenersAreIdentical(aListener, listener))
            continue;

        /* Remove current object out of list */
        AtIteratorRemove(iterator);

        /* Delete listener object */
        AtObjectDelete((AtObject)aListener);
        }

    AtOsalMutexUnLock(self->listenerListMutex);

    AtObjectDelete((AtObject)iterator);
    return cAtOk;
    }

/**
 * Get all of supported alarms
 *
 * @param self This sensor
 *
 * @return All of supported alarms
 */
uint32 AtSensorSupportedAlarms(AtSensor self)
    {
    if (self)
        return mMethodsGet(self)->SupportedAlarms(self);
    return 0;
    }

/**
 * Get device that this sensor is associated to
 *
 * @param self This sensor
 * @return Device that this sensor is associated to
 */
AtDevice AtSensorDeviceGet(AtSensor self)
    {
    if (self)
        return self->device;
    return NULL;
    }

/**
 * Initialize sensor to default state
 *
 * @param self This sensor
 *
 * @return AT return code
 */
eAtRet AtSensorInit(AtSensor self)
    {
    mNoParamAttributeSet(Init);
    }

/**
 * @}
 */

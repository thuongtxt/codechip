/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : AtThermalSensor.c
 *
 * Created Date: Dec 10, 2015
 *
 * Description : Generic thermal sensor implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtThermalSensorInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mSensorIsValid(self) (self ? cAtTrue : cAtFalse)
#define cAtMaxNumEventListener 10

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtThermalSensorMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtSensorMethods m_AtSensorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int32 CurrentTemperatureGet(AtThermalSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static int32 MinRecordedTemperatureGet(AtThermalSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static int32 MaxRecordedTemperatureGet(AtThermalSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet AlarmSetThresholdSet(AtThermalSensor self, int32 celsius)
    {
    AtUnused(self);
    AtUnused(celsius);
    return cAtErrorNotImplemented;
    }

static eAtRet AlarmClearThresholdSet(AtThermalSensor self, int32 celsius)
    {
    AtUnused(self);
    AtUnused(celsius);
    return cAtErrorNotImplemented;
    }

static int32 AlarmSetThresholdGet(AtThermalSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static int32 AlarmClearThresholdGet(AtThermalSensor self)
    {
    AtUnused(self);
    return 0x0;
    }

static const char *ToString(AtObject self)
    {
    AtDevice dev = AtSensorDeviceGet((AtSensor)self);
    static char str[64];

    AtSprintf(str, "%sthermal_sensor", AtDeviceIdToString(dev));
    return str;
    }

static uint32 SupportedAlarms(AtSensor self)
    {
    AtUnused(self);
    return cAtThermalSensorAlarmOverheat;
    }

static int32 AlarmSetThresholdMax(AtThermalSensor self)
    {
    AtUnused(self);
    return 0;
    }

static int32 AlarmSetThresholdMin(AtThermalSensor self)
    {
    AtUnused(self);
    return 0;
    }

static int32 AlarmClearThresholdMax(AtThermalSensor self)
    {
    AtUnused(self);
    return 0;
    }

static int32 AlarmClearThresholdMin(AtThermalSensor self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtSensor(AtThermalSensor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSensorOverride, mMethodsGet((AtSensor)self), sizeof(m_AtSensorOverride));

        mMethodOverride(m_AtSensorOverride, SupportedAlarms);
        }

    mMethodsSet(((AtSensor)self), &m_AtSensorOverride);
    }

static void OverrideAtObject(AtThermalSensor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet((AtObject)self), sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

static void Override(AtThermalSensor self)
    {
    OverrideAtObject(self);
    OverrideAtSensor(self);
    }

static void MethodsInit(AtThermalSensor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, CurrentTemperatureGet);
        mMethodOverride(m_methods, MinRecordedTemperatureGet);
        mMethodOverride(m_methods, MaxRecordedTemperatureGet);
        mMethodOverride(m_methods, AlarmSetThresholdSet);
        mMethodOverride(m_methods, AlarmClearThresholdSet);
        mMethodOverride(m_methods, AlarmSetThresholdGet);
        mMethodOverride(m_methods, AlarmClearThresholdGet);
        mMethodOverride(m_methods, AlarmSetThresholdMax);
        mMethodOverride(m_methods, AlarmSetThresholdMin);
        mMethodOverride(m_methods, AlarmClearThresholdMax);
        mMethodOverride(m_methods, AlarmClearThresholdMin);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtThermalSensor);
    }

AtThermalSensor AtThermalSensorObjectInit(AtThermalSensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSensorObjectInit((AtSensor)self, device) == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtThermalSensor
 * @{
 */

/**
 * To get current temperature
 *
 * @param self This thermal sensor
 *
 * @return The current temperature in Celsius
 */
int32 AtThermalSensorCurrentTemperatureGet(AtThermalSensor self)
    {
    mAttributeGet(CurrentTemperatureGet, int32, 0);
    }

/**
 * To get minimum recorded temperature
 *
 * @param self This sensor
 *
 * @return current temperature in Celsius
 */
int32 AtThermalSensorMinRecordedTemperatureGet(AtThermalSensor self)
    {
    mAttributeGet(MinRecordedTemperatureGet, int32, 0);
    }

/**
 * To get maximum recorded temperature
 *
 * @param self This sensor
 *
 * @return current temperature in Celsius
 */
int32 AtThermalSensorMaxRecordedTemperatureGet(AtThermalSensor self)
    {
    mAttributeGet(MaxRecordedTemperatureGet, int32, 0);
    }

/**
 * To set alarm SET threshold
 *
 * @param self This sensor
 * @param celsius Threshold to set alarm in Celsius
 *
 * @return AT return code
 */
eAtRet AtThermalSensorAlarmSetThresholdSet(AtThermalSensor self, int32 celsius)
    {
    mOneParamAttributeGet(AlarmSetThresholdSet, celsius, eAtRet, cAtErrorNullPointer);
    }

/**
 * To get threshold to set alarm
 *
 * @param self This sensor
 *
 * @return Alarm SET threshold
 */
int32 AtThermalSensorAlarmSetThresholdGet(AtThermalSensor self)
    {
    mAttributeGet(AlarmSetThresholdGet, int32, 0);
    }

/**
 * Get max threshold to set alarm
 *
 * @param self This sensor
 *
 * @return max threshold to set alarm
 */
int32 AtThermalSensorAlarmSetThresholdMax(AtThermalSensor self)
    {
    if (self)
        return mMethodsGet(self)->AlarmSetThresholdMax(self);
    return 0;
    }

/**
 * Get minimum threshold to set alarm
 *
 * @param self This sensor
 *
 * @return Minimum threshold to set alarm
 */
int32 AtThermalSensorAlarmSetThresholdMin(AtThermalSensor self)
    {
    if (self)
        return mMethodsGet(self)->AlarmSetThresholdMin(self);
    return 0;
    }

/**
 * To set alarm CLEAR threshold
 *
 * @param self This sensor
 * @param celsius Threshold to clear alarm in Celsius
 *
 * @return AT return code
 */
eAtRet AtThermalSensorAlarmClearThresholdSet(AtThermalSensor self, int32 celsius)
    {
    mOneParamAttributeGet(AlarmClearThresholdSet, celsius, eAtRet, cAtErrorNullPointer);
    }

/**
 * To get threshold to clear alarm
 *
 * @param self This sensor
 *
 * @return Alarm CLEAR threshold
 */
int32 AtThermalSensorAlarmClearThresholdGet(AtThermalSensor self)
    {
    mAttributeGet(AlarmClearThresholdGet, int32, 0);
    }

/**
 * Get max threshold to clear alarm
 *
 * @param self This sensor
 *
 * @return max threshold to clear alarm
 */
int32 AtThermalSensorAlarmClearThresholdMax(AtThermalSensor self)
    {
    if (self)
        return mMethodsGet(self)->AlarmClearThresholdMax(self);
    return 0;
    }

/**
 * Get minimum threshold to clear alarm
 *
 * @param self This sensor
 *
 * @return Minimum threshold to clear alarm
 */
int32 AtThermalSensorAlarmClearThresholdMin(AtThermalSensor self)
    {
    if (self)
        return mMethodsGet(self)->AlarmClearThresholdMin(self);
    return 0;
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtPtchService.c
 *
 * Created Date: May 7, 2016
 *
 * Description : PTCh service
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPtchServiceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static tAtPtchServiceMethods m_methods;
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtPtchServiceType TypeGet(AtPtchService self)
    {
    return self->serviceType;
    }

static AtModule ModuleGet(AtPtchService self)
    {
    return self->module;
    }

static eAtRet IngressPtchSet(AtPtchService self, uint16 ptch)
    {
    /* Sub class must do */
    AtUnused(self);
    AtUnused(ptch);
    return cAtErrorNotImplemented;
    }

static uint16 IngressPtchGet(AtPtchService self)
    {
    /* Sub class must do */
    AtUnused(self);
    return 0;
    }

static eAtRet EgressPtchSet(AtPtchService self, uint16 ptch)
    {
    /* Sub class must do */
    AtUnused(self);
    AtUnused(ptch);
    return cAtErrorNotImplemented;
    }

static uint16 EgressPtchGet(AtPtchService self)
    {
    /* Sub class must do */
    AtUnused(self);
    return 0;
    }

static eAtRet IngressOffsetSet(AtPtchService self, uint32 offset)
    {
    /* Sub class must do */
    AtUnused(self);
    AtUnused(offset);
    return cAtErrorNotApplicable;
    }

static uint32 IngressOffsetGet(AtPtchService self)
    {
    /* Sub class must do */
    AtUnused(self);
    return 0;
    }

static eAtRet EgressOffsetSet(AtPtchService self, uint32 offset)
    {
    /* Sub class must do */
    AtUnused(self);
    AtUnused(offset);
    return cAtErrorNotApplicable;
    }

static uint32 EgressOffsetGet(AtPtchService self)
    {
    /* Sub class must do */
    AtUnused(self);
    return 0;
    }

static eBool CounterIsSupported(AtPtchService self, eAtPtchServiceCounterType counterType)
    {
    /* Sub class must do */
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static uint32 CounterGet(AtPtchService self, eAtPtchServiceCounterType counterType)
    {
    /* Sub class must do */
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 CounterClear(AtPtchService self, eAtPtchServiceCounterType counterType)
    {
    /* Sub class must do */
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static eAtRet Enable(AtPtchService self, eBool enabled)
    {
    AtUnused(self);
    return enabled ? cAtErrorNotImplemented : cAtOk;
    }

static eBool IsEnabled(AtPtchService self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet IngressModeSet(AtPtchService self, eAtPtchMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtErrorNotImplemented;
    }

static eAtPtchMode IngressModeGet(AtPtchService self)
    {
    AtUnused(self);
    return cAtPtchModeUnknown;
    }

static void MethodsInit(AtPtchService self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TypeGet);
        mMethodOverride(m_methods, ModuleGet);
        mMethodOverride(m_methods, IngressPtchSet);
        mMethodOverride(m_methods, IngressPtchGet);
        mMethodOverride(m_methods, EgressPtchSet);
        mMethodOverride(m_methods, EgressPtchGet);
        mMethodOverride(m_methods, IngressOffsetSet);
        mMethodOverride(m_methods, IngressOffsetGet);
        mMethodOverride(m_methods, EgressOffsetSet);
        mMethodOverride(m_methods, EgressOffsetGet);
        mMethodOverride(m_methods, CounterIsSupported);
        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, IngressModeSet);
        mMethodOverride(m_methods, IngressModeGet);
        }

    mMethodsGet(self) = &m_methods;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPtchService object = (AtPtchService)(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    mEncodeUInt(serviceType);
    }

static void OverrideAtObject(AtPtchService self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPtchService self)
    {
    OverrideAtObject(self);
    }

AtPtchService AtPtchServiceObjectInit(AtPtchService self, uint32 serviceType, AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initialize memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPtchService));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize implementation */
    MethodsInit(self);
    Override(self);

    /* Initialize private data */
    self->module = module;
    self->serviceType = serviceType;

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPtchService
 * @{
 */

/**
 * Get service type
 *
 * @param self This service
 *
 * @return @ref eAtPtchServiceType "PTCH service type"
 */
eAtPtchServiceType AtPtchServiceTypeGet(AtPtchService self)
    {
    mAttributeGet(TypeGet, eAtPtchServiceType, cAtPtchServiceTypeUnknown);
    }

/**
 * Get associated module
 *
 * @param self This service
 *
 * @return Associated module
 */
AtModule AtPtchServiceModuleGet(AtPtchService self)
    {
    mAttributeGet(ModuleGet, AtModule, NULL);
    }

/**
 * Set two-bytes ingress PTCH (direction from TDM to XFI)
 *
 * @param self This service
 * @param ptch Two-bytes PTCH
 *
 * @return AT return code
 * @note Error code is returned when calling this on eXAUI#0 PTCH service
 */
eAtRet AtPtchServiceIngressPtchSet(AtPtchService self, uint16 ptch)
    {
    mNumericalAttributeSet(IngressPtchSet, ptch);
    }

/**
 * Get two-bytes ingress PTCH (direction from TDM to XFI)
 *
 * @param self This service
 * @return Two-bytes PTCH
 *
 * @note Invalid value is returned when calling this on eXAUI#0 PTCH service
 */
uint16 AtPtchServiceIngressPtchGet(AtPtchService self)
    {
    mAttributeGet(IngressPtchGet, uint16, 0);
    }

/**
 * Set egress one-byte PTCH (direction from XFI to TDM)
 *
 * @param self This service
 * @param ptch One-byte PTCH
 *
 * @return AT return code
 * @note Error code is returned when calling this on eXAUI#0 PTCH service
 */
eAtRet AtPtchServiceEgressPtchSet(AtPtchService self, uint16 ptch)
    {
    mNumericalAttributeSet(EgressPtchSet, ptch);
    }

/**
 * Get egress one-byte PTCH (direction from XFI to TDM)
 *
 * @param self This service
 *
 * @return One-byte PTCH
 */
uint16 AtPtchServiceEgressPtchGet(AtPtchService self)
    {
    mAttributeGet(EgressPtchGet, uint16, 0);
    }

/**
 * Set ingress offset for eXAUI#0 (direction from TDM to XFI)
 *
 * @param self This service
 * @param offset Offset
 *
 * @return AT return code
 * @note Calling this API on other service type will have error code returned
 */
eAtRet AtPtchServiceIngressOffsetSet(AtPtchService self, uint32 offset)
    {
    mNumericalAttributeSet(IngressOffsetSet, offset);
    }

/**
 * Get ingress offset for eXAUI#0 (direction from TDM to XFI)
 *
 * @param self This service
 *
 * @return Ingress offset
 * @note Calling this API on other service type will have invalid value returned.
 */
uint32 AtPtchServiceIngressOffsetGet(AtPtchService self)
    {
    mAttributeGet(IngressPtchGet, uint32, 0);
    }

/**
 * Set egress offset for eXAUI#0 (direction from XFI to TDM)
 *
 * @param self This service
 * @param offset Offset
 *
 * @return AT return code
 * @note Calling this API on other service type will have error code returned
 */
eAtRet AtPtchServiceEgressOffsetSet(AtPtchService self, uint32 offset)
    {
    mNumericalAttributeSet(EgressOffsetSet, offset);
    }

/**
 * Get egress offset for eXAUI#0 (direction from XFI to TDM)
 *
 * @param self This service
 *
 * @return egress offset
 * @note Calling this API on other service type will have invalid value returned.
 */
uint32 AtPtchServiceEgressOffsetGet(AtPtchService self)
    {
    mAttributeGet(EgressOffsetGet, uint32, 0);
    }

/**
 * Check if a specific counter type is supported
 *
 * @param self This service
 * @param counterType @ref eAtPtchServiceCounterType "Counter type"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPtchServiceCounterIsSupported(AtPtchService self, eAtPtchServiceCounterType counterType)
    {
    mOneParamAttributeGet(CounterIsSupported, counterType, eBool, cAtTrue);
    }

/**
 * Read-only a counter
 *
 * @param self This service
 * @param counterType @ref eAtPtchServiceCounterType "Counter type"
 *
 * @return Counter value or 0 if not supported.
 */
uint32 AtPtchServiceCounterGet(AtPtchService self, eAtPtchServiceCounterType counterType)
    {
    mOneParamAttributeGet(CounterGet, counterType, uint32, 0);
    }

/**
 * Read-then-clear a counter
 *
 * @param self This service
 * @param counterType @ref eAtPtchServiceCounterType "Counter type"
 *
 * @return Counter value (before clearing) or 0 if not supported.
 */
uint32 AtPtchServiceCounterClear(AtPtchService self, eAtPtchServiceCounterType counterType)
    {
    mOneParamAttributeGet(CounterClear, counterType, uint32, 0);
    }

/**
 * Enable/disable service
 *
 * @param self This service
 * @param enabled Enabling
 *                - cAtTrue: to enable
 *                - cAtFalse: to disable
 *
 * @return AT error code
 */
eAtRet AtPtchServiceEnable(AtPtchService self, eBool enabled)
    {
    mNumericalAttributeSet(Enable, enabled);
    }

/**
 * Check if service is enabled or disabled
 *
 * @param self This service
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPtchServiceIsEnabled(AtPtchService self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * Configure number byte PTCH at Ingress direction
 *
 * @param self This service
 * @param mode @ref eAtPtchMode "PTCH mode"
 *
 * @return AT return code
 */
eAtRet AtPtchServiceIngressModeSet(AtPtchService self, eAtPtchMode mode)
    {
    mNumericalAttributeSet(IngressModeSet, mode);
    }

/**
 * Get number byte PTCH inserted
 *
 * @param self This service
 *
 * @return @ref eAtPtchMode "PTCH mode"
 */
eAtPtchMode AtPtchServiceIngressModeGet(AtPtchService self)
    {
    mAttributeGet(IngressModeGet, eAtPtchMode, cAtPtchModeUnknown);
    }

/**
 * @}
 */

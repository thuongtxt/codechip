/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanLaneFastV2.c
 *
 * Created Date: Aug 24, 2016
 *
 * Description : Lane that use fast scan algorithm
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanLaneInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanLaneMethods m_AtEyeScanLaneOverride;

/* Save super implementation */
static const tAtEyeScanLaneMethods *m_AtEyeScanLaneMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PmaSetup(AtEyeScanLane self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool NeedInitialize(AtEyeScanLane self)
    {
    /* So that default eye-scan parameters can be setup */
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtEyeScanLane(AtEyeScanLane self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEyeScanLaneMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanLaneOverride, mMethodsGet(self), sizeof(m_AtEyeScanLaneOverride));

        mMethodOverride(m_AtEyeScanLaneOverride, PmaSetup);
        mMethodOverride(m_AtEyeScanLaneOverride, NeedInitialize);
        }

    mMethodsSet(self, &m_AtEyeScanLaneOverride);
    }

static void Override(AtEyeScanLane self)
    {
    OverrideAtEyeScanLane(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanLaneFastV2);
    }

AtEyeScanLane AtEyeScanLaneFastV2ObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanLaneFastObjectInit(self, controller, laneId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanLane AtEyeScanLaneFastV2New(AtEyeScanController controller, uint8 laneId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanLane newLane = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanLaneFastV2ObjectInit(newLane, controller, laneId);
    }

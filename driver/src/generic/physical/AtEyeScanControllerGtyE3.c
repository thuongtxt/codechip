/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanControllerGtyE3.c
 *
 * Created Date: Aug 24, 2016
 *
 * Description : GTYE3 Eye Scan Controller
 *
 * Notes       : GTY-578: GTYE3_CHANNEL
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanControllerInternal.h"
#include "AtEyeScanDrpRegV3.h"
#include "AtEyeScanLaneInternal.h"
#include "AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanControllerMethods m_AtEyeScanControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumLanes(AtEyeScanController self)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    uint8 numLanes = (uint8)AtSerdesControllerNumLanes(serdes);

    if (numLanes)
        return numLanes;

    return 1;
    }

static AtEyeScanLane LaneObjectCreate(AtEyeScanController self, uint8 laneId)
    {
    return AtEyeScanLaneFastGtyNew(self, laneId);
    }

static uint16 ES_CONTROL_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_REG;
    }

static uint32 ES_CONTROL_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_MASK;
    }

static uint32 ES_CONTROL_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_SHIFT;
    }

static uint32 ES_ERRDET_EN_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_ERRDET_EN_MASK;
    }

static uint32 ES_ERRDET_EN_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_ERRDET_EN_SHIFT;
    }

static uint32 ES_EYESCAN_EN_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_EYESCAN_EN_MASK;
    }

static uint32 ES_EYESCAN_EN_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_EYESCAN_EN_SHIFT;
    }

static uint16 ES_PRESCALE_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_PRESCALE;
    }

static uint32 ES_PRESCALE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_PRESCALE_MASK;
    }

static uint32 ES_PRESCALE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_PRESCALE_SHIFT;
    }

static uint16 ES_VERT_OFFSET_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_OFFSET;
    }

static uint32 RX_EYESCAN_VS_NEG_DIR_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VS_NEG_DIR_MASK;
    }

static uint32 RX_EYESCAN_VS_NEG_DIR_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VS_NEG_DIR_SHIFT;
    }

static uint32 ES_VERT_OFFSET_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_OFFSET_MASK;
    }

static uint32 ES_VERT_OFFSET_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_OFFSET_SHIFT;
    }

static uint32 ES_VERT_UTSIGN_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_UTSIGN_MASK;
    }

static uint32 ES_VERT_UTSIGN_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_UTSIGN_SHIFT;
    }

static uint16 ES_HORZ_OFFSET_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_HORZ_OFFSET;
    }

static uint32 ES_HORZ_OFFSET_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_HORZ_OFFSET_MASK;
    }

static uint32 ES_HORZ_OFFSET_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_HORZ_OFFSET_SHIFT;
    }

static uint16 ES_CONTROL_STATUS_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_STATUS;
    }

static uint32 ES_CONTROL_CURRENT_STATE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_CURRENT_STATE_MASK;
    }

static uint32 ES_CONTROL_CURRENT_STATE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_CURRENT_STATE_SHIFT;
    }

static uint32 ES_CONTROL_DONE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_DONE_MASK;
    }

static uint32 ES_CONTROL_DONE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_DONE_SHIFT;
    }

static uint16 ES_ERROR_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_ERROR_COUNT;
    }

static uint16 ES_SAMPLE_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SAMPLE_COUNT;
    }

static uint16 ES_SDATA_MASK0_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK0;
    }

static uint16 ES_SDATA_MASK1_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK1;
    }

static uint16 ES_SDATA_MASK2_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK2;
    }

static uint16 ES_SDATA_MASK3_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK3;
    }

static uint16 ES_SDATA_MASK4_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK4;
    }

static uint16 ES_QUAL_MASK0_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK0;
    }

static uint16 ES_QUAL_MASK1_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK1;
    }

static uint16 ES_QUAL_MASK2_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK2;
    }

static uint16 ES_QUAL_MASK3_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK3;
    }

static uint16 ES_QUAL_MASK4_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK4;
    }

static uint16 RX_DATA_WIDTH_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_DATA_WIDTH;
    }

static uint32 RX_DATA_WIDTH_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_DATA_WIDTH_MASK;
    }

static uint32 RX_DATA_WIDTH_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_DATA_WIDTH_SHIFT;
    }

static uint16 RXOUT_DIV_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RXOUT_DIV;
    }

static uint32 RXOUT_DIV_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RXOUT_DIV_MASK;
    }

static uint32 RXOUT_DIV_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RXOUT_DIV_SHIFT;
    }

static uint32 RX_EYESCAN_VS_RANGE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_EYESCAN_VS_RANGE_MASK;
    }

static uint32 RX_EYESCAN_VS_RANGE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_EYESCAN_VS_RANGE_SHIFT;
    }

static uint32 TotalWidth(AtEyeScanController self)
    {
    AtUnused(self);
    return 64;
    }

static float SpeedInGbps(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    AtSerdesController serdesLane = serdes;

    if (lane)
        serdesLane = AtSerdesControllerLaneGet(serdes, AtEyeScanLaneIdGet(lane));
    if (serdesLane == NULL)
        serdesLane = serdes;

    return AtSerdesControllerSpeedInGbps(serdesLane);
    }

static eAtRet PmaReset(AtEyeScanController self)
    {
    AtSerdesController serdesController = AtEyeScanControllerSerdesControllerGet(self);
    return AtSerdesControllerRxReset(serdesController);
    }

static float *VsRangeValuesInMillivolt(AtEyeScanController self)
    {
    static float GTYE3_VS_RANGES[] = {1.6f, 2.0f, 2.4f, 3.3f};
    AtUnused(self);
    return GTYE3_VS_RANGES;
    }

static uint8 DefaultVsRange(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 3;
    }

static const char *TypeString(AtEyeScanController self)
    {
    AtUnused(self);
    return "gty3_eyescan_controller";
    }

static void ShowSerdesInfo(AtEyeScanController self)
    {
    AtPrintc(cSevInfo,   "* SERDES:\r\n");
    AtPrintc(cSevNormal, "  * EqualizerMode         : %u \r\n", AtEyeScanControllerEqualizerModeGet(self));
    AtPrintc(cSevNormal, "  * TotalWidth            : %u \r\n", AtEyeScanControllerTotalWidth(self));
    AtPrintc(cSevNormal, "  * VoltageSwing          : %u (mV) \r\n", AtEyeScanControllerVoltageSwingInMillivolt(self));
    }

static uint8 DefaultEsPrescale(AtEyeScanController self, AtEyeScanLane lane)
    {
    uint16 dataWidth = AtEyeScanLaneDataWidthRead(lane);
    AtUnused(self);

    /* UG578 Table 4-18: ES_PRESCALE to confirm default BER 10e-6 at a given bus width */
    switch (dataWidth)
        {
        case 16:    return 2;
        case 20:    return 1;
        case 32:    return 1;
        case 40:    return 0;
        case 64:    return 0;
        case 80:    return 0;
        default:
            return 0;
        }
    }

static uint16 LaneDataWidth(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    return AtEyeScanLaneDataWidthRead(lane);
    }

static void OverrideAtEyeScanController(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanControllerOverride, mMethodsGet(self), sizeof(m_AtEyeScanControllerOverride));

        mMethodOverride(m_AtEyeScanControllerOverride, TypeString);
        mMethodOverride(m_AtEyeScanControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtEyeScanControllerOverride, TotalWidth);
        mMethodOverride(m_AtEyeScanControllerOverride, NumLanes);
        mMethodOverride(m_AtEyeScanControllerOverride, LaneObjectCreate);
        mMethodOverride(m_AtEyeScanControllerOverride, PmaReset);
        mMethodOverride(m_AtEyeScanControllerOverride, VsRangeValuesInMillivolt);
        mMethodOverride(m_AtEyeScanControllerOverride, DefaultVsRange);
        mMethodOverride(m_AtEyeScanControllerOverride, ShowSerdesInfo);
        mMethodOverride(m_AtEyeScanControllerOverride, DefaultEsPrescale);
        mMethodOverride(m_AtEyeScanControllerOverride, LaneDataWidth);

        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_ERRDET_EN_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_ERRDET_EN_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_EYESCAN_EN_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_EYESCAN_EN_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_PRESCALE_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_PRESCALE_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_PRESCALE_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_VERT_OFFSET_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, RX_EYESCAN_VS_NEG_DIR_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, RX_EYESCAN_VS_NEG_DIR_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_VERT_OFFSET_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_VERT_OFFSET_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_VERT_UTSIGN_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_VERT_UTSIGN_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_HORZ_OFFSET_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_HORZ_OFFSET_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_HORZ_OFFSET_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_STATUS_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_CURRENT_STATE_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_CURRENT_STATE_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_DONE_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_DONE_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_ERROR_COUNT_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SAMPLE_COUNT_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SDATA_MASK0_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SDATA_MASK1_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SDATA_MASK2_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SDATA_MASK3_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SDATA_MASK4_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_QUAL_MASK0_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_QUAL_MASK1_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_QUAL_MASK2_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_QUAL_MASK3_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_QUAL_MASK4_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, RX_DATA_WIDTH_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, RX_DATA_WIDTH_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, RX_DATA_WIDTH_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, RXOUT_DIV_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, RXOUT_DIV_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, RXOUT_DIV_SHIFT);
        mMethodOverride(m_AtEyeScanControllerOverride, RX_EYESCAN_VS_RANGE_MASK);
        mMethodOverride(m_AtEyeScanControllerOverride, RX_EYESCAN_VS_RANGE_SHIFT);
        }

    mMethodsSet(self, &m_AtEyeScanControllerOverride);
    }

static void Override(AtEyeScanController self)
    {
    OverrideAtEyeScanController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanControllerGtyE3);
    }

AtEyeScanController AtEyeScanControllerGtyE3ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanControllerObjectInit(self, serdesController, drpBaseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanController AtEyeScanControllerGtyE3New(AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanControllerGtyE3ObjectInit(newController, serdesController, drpBaseAddress);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtEyeScanLaneInternal.h
 * 
 * Created Date: Dec 24, 2014
 *
 * Description : Eye scan lane
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATXEYESCANLANEINTERNAL_H_
#define _ATXEYESCANLANEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEyeScanLane.h"
#include "AtList.h"
#include "../man/AtDriverInternal.h" /* For shared OSAL */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEyeScanLaneEventListener
    {
    tAtEyeScanLaneListener *callbacks;
    void *userData;
    }tAtEyeScanLaneEventListener;

typedef struct tAtEyeScanLaneMethods
    {
    eAtRet (*Init)(AtEyeScanLane self);
    eAtRet (*Debug)(AtEyeScanLane self);
    void (*EyeScanSetup)(AtEyeScanLane self);
    eBool (*ShouldAcquire)(AtEyeScanLane self);
    eBool (*FinishScanning)(AtEyeScanLane self);
    eAtRet (*Acquire)(AtEyeScanLane self);
    uint32 (*MeasuredWidth)(AtEyeScanLane self);
    uint32 (*MeasuredSwing)(AtEyeScanLane self);
    uint32 (*NumHorizontalTaps)(AtEyeScanLane self);
    uint32 (*ElapsedTimeMs)(AtEyeScanLane self);
    eBool (*NeedInitialize)(AtEyeScanLane self);
    void (*InitializeEsQualMasks)(AtEyeScanLane self);
    void (*InitializeEsSDataMasks)(AtEyeScanLane self);
    eAtRet (*PmaSetup)(AtEyeScanLane self);
    void (*ShowLaneStatus)(AtEyeScanLane self);
    void (*HorizontalOffsetSet)(AtEyeScanLane self, uint16 value);
    eAtRet (*VerticalOffsetSet)(AtEyeScanLane self, uint16 value);
    eAtRet (*StepSizeSetup)(AtEyeScanLane self);
    eAtRet (*DebugPixelScan)(AtEyeScanLane self, int32 horzOffset, int32 vertOffset);
    eBool (*NeedEyeCenter)(AtEyeScanLane self);
    }tAtEyeScanLaneMethods;

typedef struct tAtEyeScanLane
    {
    tAtObject super;
    const tAtEyeScanLaneMethods *methods;

    /* Private data */
    uint8 id;
    AtEyeScanController controller;
    AtList listeners;
    eBool eyeScanEnabled;
    eBool finished;
    eBool scanning;

    /* Eye scan */
    uint8 equalizerMode;  /* See eAtEyeScanEqualizerMode */
    uint8 horzStepSize;   /* Horizontal scan step size */
    uint8 vertStepSize;   /* Vertical scan step size */
    uint16 maxVertOffset; /* It will be calculated whenever vertStepSize is changed. */
    uint8 maxPrescale;    /* Maximum prescale value (for dynamic prescaling) */

    /* Eye scan data */
    tAtEyeScanLanePixel currentPixel;
    tAtEyeScanLanePixel *pixels;
    uint16 pixelCount;

    /* Status indicators: */
    uint16 state; /* See eAtEyeScanState */

    /* Name */
    char name[64];
    char *pName;

    /* For thread safe */
    AtOsalMutex locker;

    /* For debugging */
    eBool debugEnabled;
    tAtOsalCurTime latchTime;
    uint32 eyeScanDoneTimeUs;

    /* To know number of times eye scan restart measuring at a pixel */
    uint32 currentEyeScanRestartTimes;
    uint32 lastEyeScanRestartTimes;
    }tAtEyeScanLane;

typedef struct tAtEyeScanLaneXaui
    {
    tAtEyeScanLane super;
    }tAtEyeScanLaneXaui;

typedef struct tAtEyeScanLaneFast
    {
    tAtEyeScanLane super;

    /* Private data */
    int32 measuredWidth;
    int32 measuredSwing;
    uint32 elapseTimeMs;
    }tAtEyeScanLaneFast;

typedef struct tAtEyeScanLaneFastV2
    {
    tAtEyeScanLaneFast super;
    }tAtEyeScanLaneFastV2;

typedef struct tAtEyeScanLaneFastGty
    {
    tAtEyeScanLaneFastV2 super;
    }tAtEyeScanLaneFastGty;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtEyeScanLane AtEyeScanLaneObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId);
AtEyeScanLane AtEyeScanLaneFastObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId);
AtEyeScanLane AtEyeScanLaneXauiObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId);
AtEyeScanLane AtEyeScanLaneFastV2ObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId);

eAtRet AtEyeScanLaneInit(AtEyeScanLane self);
eAtRet AtEyeScanLaneSetup(AtEyeScanLane self);
eBool AtEyeScanLaneNeedInitialize(AtEyeScanLane self);
void AtEyeScanLaneLog(AtEyeScanLane self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...) AtAttributePrintf(5, 6);
uint16 AtEyeScanLaneEyeScanControlState(AtEyeScanLane self);
eAtRet AtEyeScanLaneDebugPixelScan(AtEyeScanLane self, int32 horzOffset, int32 vertOffset);

eAtRet AtEyeScanLaneReset(AtEyeScanLane self);
eAtRet AtEyeScanLaneAcquire(AtEyeScanLane self);
void AtEyeScanLaneEyeScanEnable(AtEyeScanLane self, eBool enabled);
eBool AtEyeScanLaneEyeScanIsEnabled(AtEyeScanLane self);
void AtEyeScanLanePrescaleSet(AtEyeScanLane self, uint16 value);
void AtEyeScanLaneInitializeEsQualMasks(AtEyeScanLane self);
void AtEyeScanLaneInitializeEsSDataMasks(AtEyeScanLane self);
void AtEyeScanLaneHorizontalOffsetSet(AtEyeScanLane self, uint16 value);
eAtRet AtEyeScanLaneVerticalOffsetSet(AtEyeScanLane self, uint16 value);
uint16 AtEyeScanLaneDataWidthRead(AtEyeScanLane self);
eBool AtEyeScanLaneDrpEyeScanDone(AtEyeScanLane self);
uint16 AtEyeScanLanePrescaleGet(AtEyeScanLane self);
uint16 AtEyeScanLaneErrorCountGet(AtEyeScanLane self);
uint16 AtEyeScanLaneSampleCountGet(AtEyeScanLane self);
uint16 AtEyeScanLaneRxOutDiv(AtEyeScanLane self);

eAtRet AtEyeScanLaneMove2Wait(AtEyeScanLane self);
eAtRet AtEyeScanLaneMove2Run(AtEyeScanLane self);

eAtRet AtEyeScanLaneLock(AtEyeScanLane self);
eAtRet AtEyeScanLaneUnLock(AtEyeScanLane self);

#endif /* _ATXEYESCANLANEINTERNAL_H_ */


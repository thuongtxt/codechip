/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtUartInternal.h
 * 
 * Created Date: Apr 20, 2016
 *
 * Description : AtUartInternal declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUARTINTERNAL_H_
#define _ATUARTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtUart.h"
#include "../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtUartMethods
    {
    eAtRet (*Init)(AtUart self);
    eAtRet (*Transmit)(AtUart self, const char *buffer, uint32 bufferSize);
    uint32 (*Receive)(AtUart self, char *buffer, uint32 bufferSize);
    void (*ReceiveDataWait)(AtUart self);
    void (*Debug)(AtUart self);
    }tAtUartMethods;

typedef struct tAtUart
    {
    tAtObject super;
    const tAtUartMethods *methods;

    /* Private data */
    AtDevice device;
    uint8 slrId; /* Super Logic Region ID */
    }tAtUart;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUart AtUartObjectInit(AtUart self, AtDevice device, uint8 slrId);

#ifdef __cplusplus
}
#endif
#endif /* _ATUARTINTERNAL_H_ */


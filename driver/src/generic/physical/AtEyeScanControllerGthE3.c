/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanControllerGthE3.c
 *
 * Created Date: Nov 3, 2016
 *
 * Description : GTHE3 Eye Scan Controller
 *
 * Notes       : GTH-576: GTHE3_CHANNEL
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanControllerInternal.h"
#include "AtEyeScanLaneInternal.h"
#include "AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define DRP_ADDR_ES_CONTROL_STATUS 0x153
#define DRP_ADDR_ES_ERROR_COUNT    0x151
#define DRP_ADDR_ES_SAMPLE_COUNT   0x152

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanControllerMethods m_AtEyeScanControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 ES_CONTROL_STATUS_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_STATUS;
    }

static uint16 ES_ERROR_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_ERROR_COUNT;
    }

static uint16 ES_SAMPLE_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SAMPLE_COUNT;
    }

static eAtRet VerticalOffsetSet(AtEyeScanController self, AtEyeScanLane lane, uint16 vertOffset)
    {
    return AtEyeScanControllerVerticalOffsetSetWithSignBit(self, lane, vertOffset);
    }

static AtEyeScanLane LaneObjectCreate(AtEyeScanController self, uint8 laneId)
    {
    return AtEyeScanLaneFastNew(self, laneId);
    }

static float *VsRangeValuesInMillivolt(AtEyeScanController self)
    {
    static float GTHE3_VS_RANGES[] = {1.5f, 1.8f, 2.2f, 2.8f};
    AtUnused(self);
    return GTHE3_VS_RANGES;
    }

static const char *TypeString(AtEyeScanController self)
    {
    AtUnused(self);
    return "gth3_eyescan_controller";
    }

static void OverrideAtEyeScanController(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanControllerOverride, mMethodsGet(self), sizeof(m_AtEyeScanControllerOverride));

        mMethodOverride(m_AtEyeScanControllerOverride, ES_CONTROL_STATUS_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_ERROR_COUNT_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, ES_SAMPLE_COUNT_REG);
        mMethodOverride(m_AtEyeScanControllerOverride, VerticalOffsetSet);
        mMethodOverride(m_AtEyeScanControllerOverride, LaneObjectCreate);
        mMethodOverride(m_AtEyeScanControllerOverride, VsRangeValuesInMillivolt);
        mMethodOverride(m_AtEyeScanControllerOverride, TypeString);
        }

    mMethodsSet(self, &m_AtEyeScanControllerOverride);
    }

static void Override(AtEyeScanController self)
    {
    OverrideAtEyeScanController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanControllerGthE3);
    }

AtEyeScanController AtEyeScanControllerGthE3ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanControllerGtyE3ObjectInit(self, serdesController, drpBaseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanController AtEyeScanControllerGthE3New(AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanControllerGthE3ObjectInit(newController, serdesController, drpBaseAddress);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtEyeScanControllerInternal.h
 * 
 * Created Date: Dec 24, 2014
 *
 * Description : Eye scan controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEYESCANCONTROLLERNTERNAL_H_
#define _ATEYESCANCONTROLLERNTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtEyeScanController.h"
#include "AtEyeScanLaneInternal.h"
#include "../physical/AtSerdesControllerInternal.h"
#include "../man/AtDriverInternal.h" /* For shared OSAL */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtEyeScanMode
    {
    cAtEyeScanModeQuick,
    cAtEyeScanModeFull
    }eAtEyeScanMode;

typedef struct tAtEyeScanControllerMethods
    {
    const char *(*TypeString)(AtEyeScanController self);
    eAtRet (*Init)(AtEyeScanController self);
    uint8 (*NumLanes)(AtEyeScanController self);
    uint16 (*LaneDataWidth)(AtEyeScanController self, AtEyeScanLane lane);
    eAtEyeScanEqualizerMode (*EqualizerModeGet)(AtEyeScanController self);
    eAtRet (*EqualizerModeSet)(AtEyeScanController self, eAtEyeScanEqualizerMode mode);
    eAtEyeScanEqualizerMode (*DefaultEqualizerModeGet)(AtEyeScanController self);
    eAtRet (*Debug)(AtEyeScanController self);
    eAtRet (*PmaReset)(AtEyeScanController self);
    float (*SpeedInGbps)(AtEyeScanController self, AtEyeScanLane lane);
    uint32 (*TotalWidth)(AtEyeScanController self);
    uint32 (*VoltageSwingInMillivolt)(AtEyeScanController self);
    eBool (*ShouldHaveDefaultEqualizer)(AtEyeScanController self);
    void (*ShowSerdesInfo)(AtEyeScanController self);
    eBool (*ShouldAutoReset)(AtEyeScanController self);
    uint16 (*MaxHorizontalOffsetGet)(AtEyeScanController self, AtEyeScanLane lane);
    eAtRet (*VerticalOffsetSet)(AtEyeScanController self, AtEyeScanLane lane, uint16 value);
    eAtEyeScanMode (*ScaneMode)(AtEyeScanController self);
    int32 (*VerticalOffsetToMillivolt)(AtEyeScanController self, AtEyeScanLane lane, int32 verticalOffset);
    eAtRet (*Reset)(AtEyeScanController self, AtEyeScanLane lane);
    float *(*VsRangeValuesInMillivolt)(AtEyeScanController self);
    uint8 (*DefaultVsRange)(AtEyeScanController self, AtEyeScanLane lane);
    uint8 (*DefaultEsPrescale)(AtEyeScanController self, AtEyeScanLane lane);

    /* DRP read/write */
    uint32 (*LaneOffset)(AtEyeScanController self);
    uint16 (*DrpRead)(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress);
    void (*DrpWrite)(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress, uint16 value);

    /* For read/write */
    uint32 (*Read)(AtEyeScanController self, uint32 address);
    void (*Write)(AtEyeScanController self, uint32 address, uint32 value);

    AtEyeScanLane (*LaneObjectCreate)(AtEyeScanController self, uint8 laneId);

    /* Registers */
    eBool (*EqualizerModeIsSupported)(AtEyeScanController self, eAtEyeScanEqualizerMode equalizerMode);
    uint32 (*SerdesControlRegister)(AtEyeScanController self);

    /* To override Address for each specific serdes */
    uint16 (*ES_CONTROL_REG)(AtEyeScanController self);
    uint32 (*ES_CONTROL_MASK)(AtEyeScanController self);
    uint32 (*ES_CONTROL_SHIFT)(AtEyeScanController self);
    uint32 (*ES_ERRDET_EN_MASK)(AtEyeScanController self);
    uint32 (*ES_ERRDET_EN_SHIFT)(AtEyeScanController self);
    uint32 (*ES_EYESCAN_EN_MASK)(AtEyeScanController self);
    uint32 (*ES_EYESCAN_EN_SHIFT)(AtEyeScanController self);
    uint16 (*ES_PRESCALE_REG)(AtEyeScanController self);
    uint32 (*ES_PRESCALE_MASK)(AtEyeScanController self);
    uint32 (*ES_PRESCALE_SHIFT)(AtEyeScanController self);
    uint16 (*ES_VERT_OFFSET_REG)(AtEyeScanController self);
    uint32 (*RX_EYESCAN_VS_NEG_DIR_MASK)(AtEyeScanController self);
    uint32 (*RX_EYESCAN_VS_NEG_DIR_SHIFT)(AtEyeScanController self);
    uint32 (*RX_EYESCAN_VS_RANGE_MASK)(AtEyeScanController self);
    uint32 (*RX_EYESCAN_VS_RANGE_SHIFT)(AtEyeScanController self);
    uint32 (*ES_VERT_OFFSET_MASK)(AtEyeScanController self);
    uint32 (*ES_VERT_OFFSET_SHIFT)(AtEyeScanController self);
    uint32 (*ES_VERT_UTSIGN_MASK)(AtEyeScanController self);
    uint32 (*ES_VERT_UTSIGN_SHIFT)(AtEyeScanController self);
    uint16 (*ES_HORZ_OFFSET_REG)(AtEyeScanController self);
    uint32 (*ES_HORZ_OFFSET_MASK)(AtEyeScanController self);
    uint32 (*ES_HORZ_OFFSET_SHIFT)(AtEyeScanController self);
    uint16 (*ES_CONTROL_STATUS_REG)(AtEyeScanController self);
    uint32 (*ES_CONTROL_CURRENT_STATE_MASK)(AtEyeScanController self);
    uint32 (*ES_CONTROL_CURRENT_STATE_SHIFT)(AtEyeScanController self);
    uint32 (*ES_CONTROL_DONE_MASK)(AtEyeScanController self);
    uint32 (*ES_CONTROL_DONE_SHIFT)(AtEyeScanController self);
    uint16 (*ES_ERROR_COUNT_REG)(AtEyeScanController self);
    uint16 (*ES_SAMPLE_COUNT_REG)(AtEyeScanController self);
    uint16 (*ES_SDATA_MASK0_REG)(AtEyeScanController self);
    uint16 (*ES_SDATA_MASK1_REG)(AtEyeScanController self);
    uint16 (*ES_SDATA_MASK2_REG)(AtEyeScanController self);
    uint16 (*ES_SDATA_MASK3_REG)(AtEyeScanController self);
    uint16 (*ES_SDATA_MASK4_REG)(AtEyeScanController self);
    uint16 (*ES_QUAL_MASK0_REG)(AtEyeScanController self);
    uint16 (*ES_QUAL_MASK1_REG)(AtEyeScanController self);
    uint16 (*ES_QUAL_MASK2_REG)(AtEyeScanController self);
    uint16 (*ES_QUAL_MASK3_REG)(AtEyeScanController self);
    uint16 (*ES_QUAL_MASK4_REG)(AtEyeScanController self);
    uint16 (*RX_DATA_WIDTH_REG)(AtEyeScanController self);
    uint32 (*RX_DATA_WIDTH_MASK)(AtEyeScanController self);
    uint32 (*RX_DATA_WIDTH_SHIFT)(AtEyeScanController self);
    uint16 (*RXOUT_DIV_REG)(AtEyeScanController self);
    uint32 (*RXOUT_DIV_MASK)(AtEyeScanController self);
    uint32 (*RXOUT_DIV_SHIFT)(AtEyeScanController self);
    }tAtEyeScanControllerMethods;

typedef struct tAtEyeScanController
    {
    tAtObject super;
    const tAtEyeScanControllerMethods *methods;

    /* Private data */
    uint32 drpBaseAddress;
    AtSerdesController serdesController;
    AtEyeScanLane *lanes;
    char name[64];
    char *pName;
    eBool initialized;
    eBool defaultSettingDisabled;
    }tAtEyeScanController;

typedef struct tAtEyeScanControllerStm
    {
    tAtEyeScanController super;
    }tAtEyeScanControllerStm;

typedef struct tAtEyeScanControllerGtyE3
    {
    tAtEyeScanController super;
    }tAtEyeScanControllerGtyE3;

typedef struct tAtEyeScanControllerGtyE4
    {
    tAtEyeScanControllerGtyE3 super;
    }tAtEyeScanControllerGtyE4;

typedef struct tAtEyeScanControllerGthE3
    {
    tAtEyeScanControllerGtyE3 super;
    }tAtEyeScanControllerGthE3;

typedef struct tAtEyeScanControllerGthE4
    {
    tAtEyeScanControllerGthE3 super;
    }tAtEyeScanControllerGthE4;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEyeScanController AtEyeScanControllerObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerStmObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerGtyE3ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerGtyE4ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerGthE3ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress);

eAtRet AtEyeScanControllerPmaReset(AtEyeScanController self);
eAtRet AtEyeScanControllerReset(AtEyeScanController self, AtEyeScanLane lane); /* EYESCANRESET */

/* Util */
uint16 AtEyeScanControllerLaneDataWidth(AtEyeScanController self, AtEyeScanLane lane);
uint32 AtEyeScanControllerRead(AtEyeScanController self, uint32 address);
void AtEyeScanControllerWrite(AtEyeScanController self, uint32 address, uint32 value);
uint16 AtEyeScanControllerDrpRead(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress);
void AtEyeScanControllerDrpWrite(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress, uint16 value);
uint32 AtEyeScanControllerLaneOffset(AtEyeScanController self);
uint32 AtEyeScanControllerDrpBaseAddress(AtEyeScanController self);

float AtEyeScanControllerSpeedInGbps(AtEyeScanController self, AtEyeScanLane lane);
uint32 AtEyeScanControllerTotalWidth(AtEyeScanController self);
uint32 AtEyeScanControllerVoltageSwingInMillivolt(AtEyeScanController self);
eAtEyeScanEqualizerMode AtEyeScanControllerDefaultEqualizerModeGet(AtEyeScanController self);
uint8 AtEyeScanControllerDefaultEsPrescale(AtEyeScanController self, AtEyeScanLane lane); /* ES_PRESCALE */
uint8 AtEyeScanControllerDefaultVsRange(AtEyeScanController self, AtEyeScanLane lane); /* RX_EYESCAN_VS_RANGE */

eAtRet AtEyeScanControllerDefaultSettingEnable(AtEyeScanController self, eBool enabled);
eBool AtEyeScanControllerDefaultSettingIsEnabled(AtEyeScanController self);
eAtRet AtEyeScanControllerShowDatabase(AtEyeScanController self);
eBool AtEyeScanControllerShouldAutoReset(AtEyeScanController  self);
uint16 AtEyeScanControllerMaxHorizontalOffsetGet(AtEyeScanController self, AtEyeScanLane lane);
eAtRet AtEyeScanControllerVerticalOffsetSet(AtEyeScanController self, AtEyeScanLane lane, uint16 value);
int32 AtEyeScanControllerVerticalOffsetToMillivolt(AtEyeScanController self, AtEyeScanLane lane, int32 verticalOffset);
float AtEyeScanControllerVsRangeInMillivoltGet(AtEyeScanController self, AtEyeScanLane lane);
void AtEyeScanControllerVsRangeInMillivoltSet(AtEyeScanController self, AtEyeScanLane lane, float mV);
void AtEyeScanControllerVsRangeSet(AtEyeScanController self, AtEyeScanLane lane, uint8 value);
eAtRet AtEyeScanControllerVerticalOffsetSetWithSignBit(AtEyeScanController self, AtEyeScanLane lane, uint16 vertOffset);

#endif /* _ATEYESCANCONTROLLERNTERNAL_H_ */

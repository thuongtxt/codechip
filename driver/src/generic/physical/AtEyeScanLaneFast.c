/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanLaneFast.c
 *
 * Created Date: Feb 25, 2015
 *
 * Description : Lane that use fast scan algorithm
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDeviceInternal.h"
#include "AtEyeScanControllerInternal.h"
#include "AtEyeScanLaneInternal.h"
#include "AtSerdesControllerInternal.h"
#include "AtEyeScanDrpReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mDrpWrite(self, address, value) AtEyeScanLaneDrpWrite(self, address, (uint16)value)
#define mThis(self) ((tAtEyeScanLaneFast *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanLaneMethods m_AtEyeScanLaneOverride;

/* Save super implementation */
static const tAtEyeScanLaneMethods *m_AtEyeScanLaneMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtEyeScanLane self)
    {
    AtEyeScanController eyeScanController = AtEyeScanLaneControllerGet(self);
    AtSerdesController controller = AtEyeScanControllerSerdesControllerGet(eyeScanController);
    return AtSerdesControllerDeviceGet(controller);
    }

static eBool IsSimulated(AtEyeScanLane self)
    {
    return AtDeviceIsSimulated(Device(self));
    }

static void EyeScanSetup(AtEyeScanLane self)
    {
    AtUnused(self);
    }

static eBool ShouldAcquire(AtEyeScanLane self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FinishScanning(AtEyeScanLane self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static int32 UI(AtEyeScanLane self)
    {
    /* Have not know the purpose of this attribute yet, it is now hardcode as 1 */
    AtUnused(self);
    return 1;
    }

static void UtSignSet(AtEyeScanLane self, int16 utSign)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddress = mMethodsGet(controller)->ES_VERT_OFFSET_REG(controller);
    uint32 utSignMask  = mMethodsGet(controller)->ES_VERT_UTSIGN_MASK(controller);
    uint32 utSignShift = mMethodsGet(controller)->ES_VERT_UTSIGN_SHIFT(controller);
    uint32 regVal = AtEyeScanLaneDrpRead(self, regAddress);
    mRegFieldSet(regVal, utSign, utSign);
    mDrpWrite(self, regAddress, regVal);
    }

static uint32 TimeoutMs(AtEyeScanLane self)
    {
    if (AtDeviceTestbenchIsEnabled(Device(self)))
        return AtDeviceTestbenchDefaultTimeoutMs();
    return 500;/* Increase to 500ms, Before is 200ms */
    }

static eBool SamplingFinished(AtEyeScanLane self)
    {
    uint32 timeoutMs = TimeoutMs(self);
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTime = 0;
    eBool isSimulated = IsSimulated(self);

    AtOsalCurTimeGet(&startTime);
    while (elapseTime < timeoutMs)
        {
        uint16 state = AtEyeScanLaneEyeScanControlState(self);
        eBool done = AtEyeScanLaneDrpEyeScanDone(self);
        if (done && (state == DRP_ADDR_ES_CONTROL_CURRENT_STATE_END))
            return cAtTrue;

        AtOsalCurTimeGet(&curTime);
        elapseTime = mTimeIntervalInMsGet(startTime, curTime);

        if (isSimulated)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool FloatIsZero(float value)
    {
    return (value <= 0.000000001) ? cAtTrue : cAtFalse;;
    }

static eAtRet RealignLane(AtEyeScanLane self)
    {
    eAtRet ret = cAtOk;
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint16 regAddr = mMethodsGet(controller)->ES_HORZ_OFFSET_REG(controller);
    uint16 regVal = AtEyeScanLaneDrpRead(self, regAddr);

    ret |= AtEyeScanLaneReset(self);
    AtEyeScanLaneDrpWrite(self, regAddr, (uint16)((regVal & 0xF000) | 0x880));
    AtOsalUSleep(1000);
    AtEyeScanLaneDrpWrite(self, regAddr, (uint16)((regVal & 0xF000) | 0x800));
    AtOsalUSleep(1000);
    ret |= AtEyeScanLaneReset(self);

    /* Restore value */
    AtEyeScanLaneDrpWrite(self, regAddr, regVal);

    return ret;
    }

static eAtRet BerCalculate(AtEyeScanLane self,
                           int32 horzOffset,
                           uint16 vertOffset,
                           float *ber,
                           uint32 *sampleCount,
                           uint32 *errorCount,
                           uint32 *maxError)
    {
    static const uint32 cMaxNumErrors = 65535;
    uint32 width = AtEyeScanLaneDataWidthRead(self);
    uint32 error1 = 0, error2 = 0;
    uint32 sample1 = 0, sample2 = 0;
    uint32 prescale;
    uint32 maxUtSign = 1 - AtEyeScanControllerEqualizerModeGet(AtEyeScanLaneControllerGet(self));
    uint32 utSign;
    float ber1 = 0.0, ber2 = 0.0;
    eAtRet ret;

    AtEyeScanLaneVerticalOffsetSet(self, vertOffset);
    AtEyeScanLaneHorizontalOffsetSet(self, (uint16)horzOffset);
    
    for (utSign = 0; utSign <= maxUtSign; utSign++)
        {
        uint32 tempErrorCount, tempSampleCount;
        uint32 totalSamples;
        float tempBer = 0.0f;

        UtSignSet(self, (int16)utSign);

        /* Move from END to WAIT state */
        ret = AtEyeScanLaneMove2Wait(self);
        if (ret != cAtOk)
            return ret;

        /* Move from WAIT to RUN state */
        ret = AtEyeScanLaneMove2Run(self);
        if (ret != cAtOk)
            return ret;

        if (!SamplingFinished(self))
            return cAtErrorEyeScanTimeout;

        prescale        = (uint32)(1 << (AtEyeScanLanePrescaleGet(self) + 1)); /* 2 ** (1 + ES_PRESCALE) */
        tempErrorCount  = AtEyeScanLaneErrorCountGet(self);
        tempSampleCount = AtEyeScanLaneSampleCountGet(self);

        totalSamples = tempSampleCount * prescale * width;
        tempBer = (tempErrorCount == 0) ? ((float)(tempErrorCount + 1) / (float)totalSamples) :
                                           ((float)tempErrorCount / (float)totalSamples);

        if (utSign == 1)
            {
            error2  = tempErrorCount;
            sample2 = totalSamples;
            ber2    = tempBer;
            }
        else
            {
            error1  = tempErrorCount;
            sample1 = totalSamples;
            ber1    = tempBer;
            }
        }

    if (FloatIsZero(ber1))
        {
        *ber         = ber2;
        *sampleCount = sample2;
        *errorCount  = error2;
        *maxError    = cMaxNumErrors;

        return cAtOk;
        }

    if (FloatIsZero(ber2))
        {
        *ber         = ber1;
        *sampleCount = sample1;
        *errorCount  = error1;
        *maxError    = cMaxNumErrors;

        return cAtOk;
        }

    *ber          = (ber1 + ber2) / 2;
    *sampleCount  = (sample1 + sample2);
    *errorCount   = (error1 + error2);
    *maxError     = cMaxNumErrors * 2;

    return cAtOk;
    }

static uint32 DefaultHorizontalStep(AtEyeScanLane self)
    {
    int32 horzMax = AtEyeScanLaneMaxHorizontalOffsetGet(self);
    uint32 horzStep = (uint32)(horzMax / 64); /* So that we can have 128 eye-width */
    if (horzStep == 0)
        horzStep = 1;
    return horzStep;
    }

static void NotifyRowWillScan(AtEyeScanLane self, int32 verticalOffset)
    {
    uint32 i;

    if (self->listeners == NULL)
        return;

    AtEyeScanLaneLock(self);

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tAtEyeScanLaneEventListener *listener = (tAtEyeScanLaneEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks->EyeScanWillScanRow)
            listener->callbacks->EyeScanWillScanRow(self, verticalOffset, listener->userData);
        }

    AtEyeScanLaneUnLock(self);
    }

static void NotifyRowDidScan(AtEyeScanLane self, int32 verticalOffset)
    {
    uint32 i;

    if (self->listeners == NULL)
        return;

    AtEyeScanLaneLock(self);

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tAtEyeScanLaneEventListener *listener = (tAtEyeScanLaneEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks->EyeScanDidScanRow)
            listener->callbacks->EyeScanDidScanRow(self, verticalOffset, listener->userData);

        }

    AtEyeScanLaneUnLock(self);
    }

static void NotifyPixelDidScan(AtEyeScanLane self,
                               int32 verticalOffset, int32 horizontalOffset,
                               uint32 numErrors, uint32 maxNumErrors, float ber)
    {
    uint32 i;

    if (self->listeners == NULL)
        return;

    AtEyeScanLaneLock(self);

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tAtEyeScanLaneEventListener *listener = (tAtEyeScanLaneEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks->EyeScanDidScanPixel)
            listener->callbacks->EyeScanDidScanPixel(self,
                                                     verticalOffset, horizontalOffset,
                                                     numErrors, maxNumErrors, ber, listener->userData);
        }

    AtEyeScanLaneUnLock(self);
    }

static int32 HorzMax(AtEyeScanLane self)
    {
    return AtEyeScanLaneMaxHorizontalOffsetGet(self) * UI(self);
    }

static int32 HorzMin(AtEyeScanLane self)
    {
    return -HorzMax(self);
    }

static eAtRet Eye1d(AtEyeScanLane self, uint16 vertOffset, uint32 prescale, int32 *eyeWidth)
    {
    eAtRet retCode = cAtOk;
    uint32 maxerrs, samples, errors;
    float ber;
    int32 horzMin;
    int32 horzOffset;
    uint32 horzStep = DefaultHorizontalStep(self);
    int32 horzMax = AtEyeScanLaneMaxHorizontalOffsetGet(self);

    *eyeWidth = 0;

    AtEyeScanLaneVerticalOffsetSet(self, vertOffset);

    if (AtEyeScanControllerDefaultSettingIsEnabled(AtEyeScanLaneControllerGet(self)))
        {
        AtEyeScanLanePrescaleSet(self, (uint16)prescale);
        AtEyeScanLaneInitializeEsQualMasks(self);
        AtEyeScanLaneInitializeEsSDataMasks(self);
        }

    horzMin = -horzMax;
    horzMin = (horzMin * UI(self));
    horzMax = (horzMax * UI(self));

    for (horzOffset = horzMin; horzOffset <= horzMax; horzOffset += (int32)horzStep)
        {
        retCode = BerCalculate(self, horzOffset, vertOffset, &ber, &samples, &errors, &maxerrs);
        if (retCode != cAtOk)
            return (retCode);

        NotifyPixelDidScan(self, vertOffset, horzOffset, errors, maxerrs, ber);

        if (errors == 0)
            *eyeWidth = *eyeWidth + 1;
        }

    if (*eyeWidth > 1)
        *eyeWidth = *eyeWidth - 1;

    return (retCode);
    }

static eAtRet EyeCenter(AtEyeScanLane self, uint32 prescale)
    {
    eAtRet ret = cAtOk;
    uint32 maxerrs, samples, errors;
    float ber;

    if (!mMethodsGet(self)->NeedEyeCenter(self))
        return cAtOk;

    if (AtEyeScanControllerDefaultSettingIsEnabled(AtEyeScanLaneControllerGet(self)))
        {
        AtEyeScanLanePrescaleSet(self, (uint16)prescale);
        AtEyeScanLaneInitializeEsQualMasks(self);
        AtEyeScanLaneInitializeEsSDataMasks(self);
        }

    ret = BerCalculate(self, 0, 0, &ber, &samples, &errors, &maxerrs);
    if (ret != cAtOk)
        return ret;

    /* Error count is saturate, need to realign the lane. */
    if (AtEyeScanLaneErrorCountGet(self) == maxerrs)
        {
        ret = RealignLane(self);
        if (ret != cAtOk)
            return ret;

        ret = BerCalculate(self, 0, 0, &ber, &samples, &errors, &maxerrs);
        if (ret != cAtOk)
            return ret;
        }

    if (errors)
        AtEyeScanLaneLog(self, cAtLogLevelCritical, AtSourceLocation,
                         "Cannot detect a good pixel at eye center (%d errors found)\r\n",
                         errors);

    return cAtOk;
    }

static int32 IntVertOffset(int32 vertical)
    {
    if (vertical / 128)
        return ((vertical == 128) ? 0 : ((vertical - 1) % 128));
    return (~(vertical % 128) & 0xff);
    }

static eAtRet Eye2d(AtEyeScanLane self, uint32 prescale, int32 *maxEyeWidth, int32 *maxEyeSwing)
    {
    eAtRet retCode = cAtOk;
    int32 vert_i;
    uint32 vertStep = AtEyeScanLaneVerticalStepSizeGet(self);

    *maxEyeWidth = 0;
    *maxEyeSwing = 0;

    for (vert_i = 256; vert_i >= 0; vert_i -= (int32)vertStep)
        {
        int32 vertOffset = IntVertOffset(vert_i);
        int32 eyeWidth;

        NotifyRowWillScan(self, vertOffset);
        retCode = Eye1d(self, (uint16)vertOffset, prescale, &eyeWidth);
        NotifyRowDidScan(self, vertOffset);
        if (retCode != cAtOk)
            return retCode;

        if (eyeWidth > *maxEyeWidth)
            *maxEyeWidth = eyeWidth;

        if (eyeWidth > 2)
            *maxEyeSwing = *maxEyeSwing + 1;
        }

    *maxEyeSwing = *maxEyeSwing - 1;

    return (retCode);
    }

static eAtRet Acquire(AtEyeScanLane self)
    {
    AtEyeScanController controller = AtEyeScanLaneControllerGet(self);
    uint32 prescale = AtEyeScanControllerDefaultEsPrescale(controller, self);
    int32 totalWidth = (int32)AtEyeScanControllerTotalWidth(controller);
    int32 expectedEyeWidth = (128 - 2) - totalWidth;
    int32 expectedEyeSwingMin = 4;
    eAtRet ret;
    int32 minEyeWidth = (expectedEyeWidth - totalWidth);
    int32 maxEyeWidth = (expectedEyeWidth + totalWidth);
    tAtOsalCurTime startTime, stopTime;

    /* Acquire and record elapse time */
    AtOsalCurTimeGet(&startTime);
    AtEyeScanLaneEyeScanEnable(self, cAtTrue);
    ret = EyeCenter(self, prescale);
    if (ret != cAtOk)
        return ret;

    ret = Eye2d(self, prescale, &(mThis(self)->measuredWidth), &(mThis(self)->measuredSwing));
    AtOsalCurTimeGet(&stopTime);
    mThis(self)->elapseTimeMs = mTimeIntervalInMsGet(startTime, stopTime);

    if (ret != cAtOk)
        return ret;

    if ((mThis(self)->measuredWidth < minEyeWidth) || (mThis(self)->measuredWidth > maxEyeWidth))
        {
        AtEyeScanLaneLog(self, cAtLogLevelCritical, AtSourceLocation,
                         "Eye-width %d is out of range [%d..%d]\r\n",
                         mThis(self)->measuredWidth, minEyeWidth, maxEyeWidth);
        return cAtErrorEyeWidthOutOfRange;
        }

    if (mThis(self)->measuredSwing < expectedEyeSwingMin)
        {
        AtEyeScanLaneLog(self, cAtLogLevelCritical, AtSourceLocation,
                         "Eye-swing %d is too small, it must be greater than %d\r\n",
                         (uint32)mThis(self)->measuredSwing, (uint32)expectedEyeSwingMin);
        return cAtErrorEyeSwingOutOfRange;
        }

    return cAtOk;
    }

static uint32 MeasuredWidth(AtEyeScanLane self)
    {
    return (uint32)mThis(self)->measuredWidth;
    }

static uint32 MeasuredSwing(AtEyeScanLane self)
    {
    return (uint32)mThis(self)->measuredSwing;
    }

static uint32 NumHorizontalTaps(AtEyeScanLane self)
    {
    uint32 horz_inc = AtEyeScanLaneHorizontalStepSizeGet(self);
    int32 horz_max  = HorzMax(self);
    int32 horz_min  = HorzMin(self);
    return (uint32)((horz_inc > 1) ? (horz_max - horz_min) / (int32)horz_inc : (horz_max - horz_min));
    }

static uint32 ElapsedTimeMs(AtEyeScanLane self)
    {
    return mThis(self)->elapseTimeMs;
    }

static eAtRet StepSizeSetup(AtEyeScanLane self)
    {
    /* To make eye-scan be fast */
    AtEyeScanLaneVerticalStepSizeSet(self, 16);
    AtEyeScanLaneHorizontalStepSizeSet(self, (uint8)DefaultHorizontalStep(self));
    return cAtOk;
    }

static void ShowLaneStatus(AtEyeScanLane self)
    {
    /* Let SERDES manage this */
    AtUnused(self);
    }

static eAtRet DebugPixelScan(AtEyeScanLane self, int32 horzOffset, int32 vertOffset)
    {
    float ber;
    uint32 sampleCount, errorCount, maxError;
    eAtRet ret;

    AtPrintf("* %s (%d, %d): ", AtObjectToString((AtObject)self), horzOffset, vertOffset);

    /* Scan */
    ret = BerCalculate(self, horzOffset, (uint16)vertOffset, &ber, &sampleCount, &errorCount, &maxError);
    if (ret != cAtOk)
        {
        AtPrintf("%s\r\n", AtRet2String(ret));
        return ret;
        }

    /* Display result */
    AtPrintf("\r\n");
    AtPrintf("  * Sample count: %u\r\n", sampleCount);
    AtPrintf("  * Error count : %u\r\n", errorCount);
    AtPrintf("  * Max error   : %u\r\n", maxError);

    return cAtOk;
    }

static void OverrideAtEyeScanLane(AtEyeScanLane self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEyeScanLaneMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanLaneOverride, mMethodsGet(self), sizeof(m_AtEyeScanLaneOverride));

        mMethodOverride(m_AtEyeScanLaneOverride, EyeScanSetup);
        mMethodOverride(m_AtEyeScanLaneOverride, ShouldAcquire);
        mMethodOverride(m_AtEyeScanLaneOverride, FinishScanning);
        mMethodOverride(m_AtEyeScanLaneOverride, Acquire);
        mMethodOverride(m_AtEyeScanLaneOverride, MeasuredWidth);
        mMethodOverride(m_AtEyeScanLaneOverride, MeasuredSwing);
        mMethodOverride(m_AtEyeScanLaneOverride, NumHorizontalTaps);
        mMethodOverride(m_AtEyeScanLaneOverride, ElapsedTimeMs);
        mMethodOverride(m_AtEyeScanLaneOverride, ShowLaneStatus);
        mMethodOverride(m_AtEyeScanLaneOverride, StepSizeSetup);
        mMethodOverride(m_AtEyeScanLaneOverride, DebugPixelScan);
        }

    mMethodsSet(self, &m_AtEyeScanLaneOverride);
    }

static void Override(AtEyeScanLane self)
    {
    OverrideAtEyeScanLane(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanLaneFast);
    }

AtEyeScanLane AtEyeScanLaneFastObjectInit(AtEyeScanLane self, AtEyeScanController controller, uint8 laneId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanLaneObjectInit(self, controller, laneId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanLane AtEyeScanLaneFastNew(AtEyeScanController controller, uint8 laneId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanLane newLane = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanLaneFastObjectInit(newLane, controller, laneId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanControllerStm.c
 *
 * Created Date: Feb 25, 2015
 *
 * Description : XAUI eye scan controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanControllerInternal.h"
#include "AtEyeScanDrpReg.h"
#include "AtSerdesController.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanControllerMethods m_AtEyeScanControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 LaneDataWidth(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 16;
    }

static uint8 NumLanes(AtEyeScanController self)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    uint32 numLanes = 0;

    if (serdes == NULL)
        return 1;

    numLanes = AtSerdesControllerNumLanes(serdes);
    if (numLanes == 0)
        numLanes = 1; /* SERDES must have itself as one lane */

    return (uint8)numLanes;
    }

static AtEyeScanLane LaneObjectCreate(AtEyeScanController self, uint8 laneId)
    {
    return AtEyeScanLaneFastNew(self, laneId);
    }

static eAtRet Debug(AtEyeScanController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint32 SerdesControlRegister(AtEyeScanController self)
    {
    AtUnused(self);
    return ES_REG_SERDES_CONTROL_STM;
    }

static eAtRet PmaReset(AtEyeScanController self)
    {
    AtSerdesController serdesController = AtEyeScanControllerSerdesControllerGet(self);
    return AtSerdesControllerReset(serdesController);
    }

static float SpeedInGbps(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 2.5;
    }

static uint32 TotalWidth(AtEyeScanController self)
    {
    AtUnused(self);
    return 128 / 4;
    }

static AtSerdesController Serdes(AtEyeScanController self)
    {
    return AtEyeScanControllerSerdesControllerGet(self);
    }

static eAtEyeScanEqualizerMode EqualizerModeGet(AtEyeScanController self)
    {
    AtSerdesController serdes = Serdes(self);

    if (serdes)
        {
        eAtSerdesEqualizerMode serdesMode = AtSerdesControllerEqualizerModeGet(serdes);
        return (serdesMode == cAtSerdesEqualizerModeDfe) ? cAtEyeScanEqualizerModeDfe : cAtEyeScanEqualizerModeLpm;
        }

    return cAtEyeScanEqualizerModeLpm;
    }

static eAtRet EqualizerModeSet(AtEyeScanController self, eAtEyeScanEqualizerMode mode)
    {
    AtSerdesController serdes = Serdes(self);

    if (serdes)
        {
        eAtSerdesEqualizerMode serdesMode = (mode == cAtEyeScanEqualizerModeLpm) ? cAtSerdesEqualizerModeLpm : cAtSerdesEqualizerModeDfe;
        eAtRet ret = AtSerdesControllerEqualizerModeSet(serdes, serdesMode);
        if (ret == cAtOk)
            ret = mMethodsGet(self)->PmaReset(self);
        return ret;
        }

    return (mode == cAtEyeScanEqualizerModeLpm) ? cAtOk : cAtErrorModeNotSupport;
    }

static void OverrideAtEyeScanController(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanControllerOverride, mMethodsGet(self), sizeof(m_AtEyeScanControllerOverride));

        mMethodOverride(m_AtEyeScanControllerOverride, NumLanes);
        mMethodOverride(m_AtEyeScanControllerOverride, LaneObjectCreate);
        mMethodOverride(m_AtEyeScanControllerOverride, LaneDataWidth);
        mMethodOverride(m_AtEyeScanControllerOverride, Debug);
        mMethodOverride(m_AtEyeScanControllerOverride, SerdesControlRegister);
        mMethodOverride(m_AtEyeScanControllerOverride, PmaReset);
        mMethodOverride(m_AtEyeScanControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtEyeScanControllerOverride, TotalWidth);
        mMethodOverride(m_AtEyeScanControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtEyeScanControllerOverride, EqualizerModeGet);
        }

    mMethodsSet(self, &m_AtEyeScanControllerOverride);
    }

static void Override(AtEyeScanController self)
    {
    OverrideAtEyeScanController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanControllerStm);
    }

AtEyeScanController AtEyeScanControllerStmObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanControllerObjectInit(self, serdesController, drpBaseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanController AtEyeScanControllerStmNew(AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanControllerStmObjectInit(newController, serdesController, drpBaseAddress);
    }

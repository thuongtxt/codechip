/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSerdesManagerInternal.h
 * 
 * Created Date: Jun 17, 2016
 *
 * Description : Serdes manager internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSERDESMANAGERINTERNAL_H_
#define _ATSERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtObjectInternal.h"
#include "../man/AtDeviceInternal.h"
#include "../diag/querier/common/AtQueriers.h"
#include "AtSerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSerdesManagerMethods
    {
    /* Public methods */
    uint32 (*NumSerdesControllers)(AtSerdesManager self);
    AtSerdesController (*SerdesControllerGet)(AtSerdesManager self, uint32 serdesId);

    /* Internal methods */
    eAtRet (*Setup)(AtSerdesManager self);
    eAtRet (*Init)(AtSerdesManager self);
    eAtRet (*AsyncInit)(AtSerdesManager self);
    AtSerdesController (*SerdesControllerObjectCreate)(AtSerdesManager self, uint32 serdesId);
    eBool (*CanAccessRegister)(AtSerdesManager self, uint32 serdesId, uint32 address, eAtModule moduleId);
    eBool (*SerdesIsControllable)(AtSerdesManager self, uint32 serdesId);

    /* Debugger */
    AtQuerier (*QuerierGet)(AtSerdesManager self);
    }tAtSerdesManagerMethods;

typedef struct tAtSerdesManager
    {
    tAtObject super;
    const tAtSerdesManagerMethods *methods;

    AtDevice device;
    AtSerdesController *allSerdes;
    uint32 asyncInitState;
    }tAtSerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager AtSerdesManagerObjectInit(AtSerdesManager self, AtDevice device);
eAtRet AtSerdesManagerSetup(AtSerdesManager self);
eAtRet AtSerdesManagerInit(AtSerdesManager self);
eAtRet AtSerdesManagerAsyncInit(AtSerdesManager self);
eAtRet AtSerdesManagerReset(AtSerdesManager self);

eBool AtSerdesManagerCanAccessRegister(AtSerdesManager self, uint32 serdesId, uint32 address, eAtModule moduleId);
eBool AtSerdesManagerSerdesIsControllable(AtSerdesManager self, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _ATSERDESMANAGERINTERNAL_H_ */


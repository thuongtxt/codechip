/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanController.c
 *
 * Created Date: Dec 24, 2014
 *
 * Description : Eye scan controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h" /* For read/write */
#include "AtEyeScanControllerInternal.h"
#include "AtEyeScanLaneInternal.h"
#include "AtEthPort.h"
#include "AtEyeScanDrpReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtEyeScanController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEyeScanControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumLanes(AtEyeScanController self)
    {
    /* Let concrete class determine */
    AtUnused(self);
    return 0;
    }

static AtEyeScanLane LaneObjectCreate(AtEyeScanController self, uint8 laneId)
    {
    /* Let concrete determine */
    AtUnused(self);
    AtUnused(laneId);
    return NULL;
    }

static AtEyeScanLane *CreateAllLanes(AtEyeScanController self)
    {
    uint8 lane_i;
    uint8 numLanes = mMethodsGet(self)->NumLanes(self);
    uint32 memorySize = sizeof(AtEyeScanLane) * numLanes;
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanLane *newLanes;

    /* Create memory to hold all Lane objects */
    newLanes = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (newLanes == NULL)
        return NULL;
    mMethodsGet(osal)->MemInit(osal, newLanes, 0, memorySize);

    /* Then create all of Lanes */
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        uint8 createdLane_i;
        AtEyeScanLane newLane;

        /* Try creating */
        newLane = mMethodsGet(self)->LaneObjectCreate(self, lane_i);
        if (newLane)
            {
            newLanes[lane_i] = newLane;
            continue;
            }

        /* Create fail, need to destroy all of allocated memory */
        for (createdLane_i = 0; createdLane_i < lane_i; createdLane_i++)
            AtObjectDelete((AtObject)newLanes[createdLane_i]);
        mMethodsGet(osal)->MemFree(osal, newLanes);

        return NULL;
        }

    return newLanes;
    }

static void DeleteAllLanes(AtEyeScanController self)
    {
    uint8 lane_i;
    uint8 numLanes = mMethodsGet(self)->NumLanes(self);
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->lanes == NULL)
        return;

    /* Delete all Lanes then memory that holds them */
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        AtObjectDelete((AtObject)self->lanes[lane_i]);
    mMethodsGet(osal)->MemFree(osal, self->lanes);
    self->lanes = NULL;
    }

static AtEyeScanLane *AllLanes(AtEyeScanController self)
    {
    if (self->lanes == NULL)
        self->lanes = CreateAllLanes(self);
    return self->lanes;
    }

static AtEyeScanLane Lane(AtEyeScanController self, uint8 laneId)
    {
    AtEyeScanLane *allLanes = AllLanes(self);
    return allLanes ? allLanes[laneId] : NULL;
    }

static void Delete(AtObject self)
    {
    DeleteAllLanes(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static eBool EyeScanFinished(AtEyeScanController self)
    {
    uint8 numLanes = mMethodsGet(self)->NumLanes(self);
    uint8 lane_i;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = Lane(self, lane_i);
        if (!AtEyeScanLaneIsEnabled(lane))
            continue;
        if (!AtEyeScanLaneScanFinished(lane))
            return cAtFalse;
        }

    return cAtTrue;
    }

static char *DescriptionBuild(AtObject self, char *buffer, uint32 bufferSize)
    {
    AtEyeScanController controller = mThis(self);
    AtChannel port;
    AtSerdesController serdes;

    /* Using SERDES description first as it has more detail */
    serdes = AtEyeScanControllerSerdesControllerGet(controller);
    if (serdes)
        {
        AtSnprintf(buffer, bufferSize, "%s._%s",
                   AtObjectToString((AtObject)serdes),
                   mMethodsGet(controller)->TypeString(controller));
        return buffer;
        }

    /* Use port description if there is no associated SERDES */
    port = AtEyeScanControllerPhysicalPort(controller);
    if (port)
        {
        AtSnprintf(buffer, bufferSize, "%s.%s_%s",
                   AtChannelTypeString(port), AtChannelIdString(port),
                   mMethodsGet(controller)->TypeString(controller));
        return buffer;
        }

    return NULL;
    }

static const char *ToString(AtObject self)
    {
    AtEyeScanController controller = mThis(self);
    AtChannel channel = AtEyeScanControllerPhysicalPort(controller);
    AtDevice dev = AtChannelDeviceGet(channel);
    static char str[128];

    if (!controller->pName)
        {
        DescriptionBuild(self, controller->name, sizeof(controller->name));
        controller->pName = controller->name;
        }

    AtSprintf(str, "%s%s", AtDeviceIdToString(dev), controller->pName);
    return str;
    }

static const char *TypeString(AtEyeScanController self)
    {
    AtUnused(self);
    return "eyescan_controller";
    }

static uint16 LaneDataWidth(AtEyeScanController self, AtEyeScanLane lane)
    {
    /* Concrete must know */
    AtUnused(self);
    AtUnused(lane);
    return 0;
    }

static uint32 SerdesControlRegister(AtEyeScanController self)
    {
    /* Sub class must know */
    AtUnused(self);
    return cBit31_0;
    }

static eBool EqualizerModeIsSupported(AtEyeScanController self, eAtEyeScanEqualizerMode equalizerMode)
    {
    /* Concrete must know */
    AtUnused(self);
    AtUnused(equalizerMode);
    return cAtFalse;
    }

static AtSerdesController Serdes(AtEyeScanController self)
	{
	return mThis(self)->serdesController;
	}

static eAtEyeScanEqualizerMode EqualizerModeGet(AtEyeScanController self)
    {
	eAtSerdesEqualizerMode serdesMode = AtSerdesControllerEqualizerModeGet(Serdes(self));
	if (serdesMode == cAtSerdesEqualizerModeDfe)
		return cAtEyeScanEqualizerModeDfe;
	return cAtEyeScanEqualizerModeLpm;
    }

static eAtEyeScanEqualizerMode DefaultEqualizerModeGet(AtEyeScanController self)
    {
    AtUnused(self);
    return cAtEyeScanEqualizerModeLpm;
    }

static eAtRet EqualizerModeSet(AtEyeScanController self, eAtEyeScanEqualizerMode mode)
    {
	AtSerdesController serdes = Serdes(self);
	eAtSerdesEqualizerMode serdesMode = cAtSerdesEqualizerModeDfe;

	if (mode == cAtEyeScanEqualizerModeLpm)
		serdesMode = cAtEyeScanEqualizerModeDfe;

	AtSerdesControllerEqualizerModeSet(serdes, serdesMode);
    return mMethodsGet(self)->PmaReset(self);
    }

static void ShowResetControl(uint32 regVal)
    {
    AtPrintc(cSevNormal, "* Reset: ");

    /* There is no reset in active */
    if ((regVal & ES_SERDES_CONTROL_RESET_ALL) == 0)
        {
        AtPrintc(cSevInfo, "No resets are in process\r\n");
        return;
        }

    /* At least one reset is in active */
    if (regVal & ES_SERDES_CONTROL_RESET_ALL_MASK)
        AtPrintc(cSevCritical, " SERDES is being reset");
    if (regVal & ES_SERDES_CONTROL_RESET_TX_PMA_MASK)
        AtPrintc(cSevCritical, " TX PMA is being reset");
    if (regVal & ES_SERDES_CONTROL_RESET_TX_PCS_MASK)
        AtPrintc(cSevCritical, " TX PCS is being reset");
    if (regVal & ES_SERDES_CONTROL_RESET_RX_PMA_MASK)
        AtPrintc(cSevCritical, " RX PMA is being reset");
    if (regVal & ES_SERDES_CONTROL_RESET_RX_PCS_MASK)
        AtPrintc(cSevCritical, " RX PCS is being reset");
    if (regVal & ES_SERDES_CONTROL_RESET_EYE_MASK)
        AtPrintc(cSevCritical, " Eye-scan is being reset");

    AtPrintc(cSevNormal, "\r\n");
    }

static void ShowSerdesInfo(AtEyeScanController self)
    {
    uint32 regVal = AtEyeScanControllerRead(self, mMethodsGet(self)->SerdesControlRegister(self));
    ShowResetControl(regVal);
    }

static void ShowLanesInfo(AtEyeScanController self)
    {
    uint8 numLanes = AtEyeScanControllerNumLanes(self);
    uint8 lane_i;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(self, lane_i);
        AtPrintc(cSevInfo, "\r\n");
        AtPrintc(cSevInfo, "* lane.%d:\r\n", lane_i + 1);
        AtEyeScanLaneDebug(lane);
        }
    }

static eAtRet ShowDatabase(AtEyeScanController self)
    {
    AtPrintc(cSevInfo,   "* Database:\r\n");
    AtPrintc(cSevNormal, "  * DRP base address      : 0x%08x\r\n", self->drpBaseAddress);
    AtPrintc(cSevNormal, "  * SERDES                : %s\r\n", AtObjectToString((AtObject)self->serdesController));
    AtPrintc(cSevNormal, "  * LanesInitialized      : %s\r\n", self->initialized ? "yes" : "no");
    AtPrintc(cSevNormal, "  * DefaultSettingDisabled: %s\r\n", self->defaultSettingDisabled ? "yes" : "no");
    return cAtOk;
    }

static eAtRet Debug(AtEyeScanController self)
    {
    mMethodsGet(self)->ShowSerdesInfo(self);
    AtEyeScanControllerShowDatabase(self);
    ShowLanesInfo(self);
    return cAtOk;
    }

static eAtRet PmaReset(AtEyeScanController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool LanesNeedInit(AtEyeScanController self)
    {
    uint8 numLanes = AtEyeScanControllerNumLanes(self);
    uint8 lane_i;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(self, lane_i);
        if (AtEyeScanLaneNeedInitialize(lane))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet AllLanesInit(AtEyeScanController self)
    {
    uint8 numLanes = AtEyeScanControllerNumLanes(self);
    eAtRet ret = cAtOk;
    uint8 lane_i;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(self, lane_i);
        if (AtEyeScanLaneNeedInitialize(lane))
            ret |= AtEyeScanLaneInit(lane);
        }

    return ret;
    }

static eAtRet AllLanesReset(AtEyeScanController self)
    {
    eAtRet ret = cAtOk;
    eBool lanesNeedInit = LanesNeedInit(self);

    if (!lanesNeedInit)
        {
        self->initialized = cAtTrue;
        return cAtOk;
        }

    ret |= AllLanesInit(self);
    if (AtEyeScanControllerShouldAutoReset(self))
        ret |= mMethodsGet(self)->PmaReset(self);
    if (ret == cAtOk)
        self->initialized = cAtTrue;

    return ret;
    }

static eBool ShouldHaveDefaultEqualizer(AtEyeScanController self)
    {
    /* For recent products, this is managed by SERDES which already has this
     * default attribute */
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Init(AtEyeScanController self)
    {
    eAtRet ret = cAtOk;

    ret |= AllLanesReset(self);
    if (mMethodsGet(self)->ShouldHaveDefaultEqualizer(self))
        ret |= AtEyeScanControllerEqualizerModeSet(self, AtEyeScanControllerDefaultEqualizerModeGet(self));

    return ret;
    }

static eAtRet ScanLane(AtEyeScanController self, uint8 laneId)
    {
    AtEyeScanLane lane = Lane(self, laneId);

    if (!AtEyeScanLaneIsEnabled(lane))
        return cAtOk;

    return AtEyeScanLaneAcquire(lane);
    }

static uint32 TotalWidth(AtEyeScanController self)
    {
    AtUnused(self);
    return 0;
    }

static float SpeedInGbps(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 0;
    }

static uint32 VoltageSwingInMillivolt(AtEyeScanController self)
    {
    eAtEyeScanEqualizerMode equalizerMode = AtEyeScanControllerEqualizerModeGet(self);

    if (equalizerMode == cAtEyeScanEqualizerModeDfe)
        return 247;
    if (equalizerMode == cAtEyeScanEqualizerModeLpm)
        return 380;

    return 0;
    }

static AtHal Hal(AtEyeScanController self)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    AtDevice device = AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(serdes));
    return AtDeviceIpCoreHalGet(device, 0);
    }

static uint32 Read(AtEyeScanController self, uint32 address)
    {
    AtChannel channel = AtEyeScanControllerPhysicalPort(self);

    if (channel)
        {
        eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(channel));
        return mChannelHwRead(channel, address, moduleId);
        }

    return AtHalRead(Hal(self), address);
    }

static void Write(AtEyeScanController self, uint32 address, uint32 value)
    {
    AtChannel channel = AtEyeScanControllerPhysicalPort(self);

    if (channel)
        {
        eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(channel));
        mChannelHwWrite(channel, address, value, moduleId);
        return;
        }

    AtHalWrite(Hal(self), address, value);
    }

static uint32 LaneOffset(AtEyeScanController self)
    {
    AtUnused(self);
    return 1024UL;
    }

static uint32 DrpLaneBaseAddress(AtEyeScanController self, AtEyeScanLane lane)
    {
    uint32 lanOffset = AtEyeScanControllerLaneOffset(self);
    return self->drpBaseAddress + (AtEyeScanLaneIdGet(lane) * lanOffset);
    }

static AtSerdesController LaneSerdes(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtSerdesController laneSerdes = NULL;
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    if (serdes == NULL)
        return NULL;

    if (lane)
    	laneSerdes = AtSerdesControllerLaneGet(serdes, AtEyeScanLaneIdGet(lane));

    if (laneSerdes)
    	return laneSerdes;

    return serdes;
    }

static AtDrp LaneDrp(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtSerdesController serdes = LaneSerdes(self, lane);
    if (serdes == NULL)
        return NULL;

    return AtSerdesControllerDrp(serdes);
    }

static uint16 DrpRead(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress)
    {
    AtDrp drp = LaneDrp(self, lane);

    if (drp)
        return (uint16)AtDrpRead(drp, drpAddress);

    else
        {
        AtChannel port = AtEyeScanControllerPhysicalPort(self);
        eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(port));
        return (uint16)mChannelHwRead(port, DrpLaneBaseAddress(self, lane) + drpAddress, moduleId) & cBit15_0;
        }
    }

static void DrpWrite(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress, uint16 value)
    {
    AtDrp drp = LaneDrp(self, lane);

    if (drp)
        {
        AtDrpWrite(drp, drpAddress, value);
        return;
        }
    else
        {
        AtChannel port = AtEyeScanControllerPhysicalPort(self);
        eAtModule moduleId = AtModuleTypeGet(AtChannelModuleGet(port));
        mChannelHwWrite(port, DrpLaneBaseAddress(self, lane) + drpAddress, value, moduleId);
        }
    }

static uint16 ES_CONTROL_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_REG;
    }

static uint32 ES_CONTROL_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_MASK;
    }

static uint32 ES_CONTROL_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_SHIFT;
    }

static uint32 ES_ERRDET_EN_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_ERRDET_EN_MASK;
    }

static uint32 ES_ERRDET_EN_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_ERRDET_EN_SHIFT;
    }

static uint32 ES_EYESCAN_EN_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_EYESCAN_EN_MASK;
    }

static uint32 ES_EYESCAN_EN_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_EYESCAN_EN_SHIFT;
    }

static uint16 ES_PRESCALE_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_PRESCALE;
    }

static uint32 ES_PRESCALE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_PRESCALE_MASK;
    }

static uint32 ES_PRESCALE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_PRESCALE_SHIFT;
    }

static uint16 ES_VERT_OFFSET_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_OFFSET;
    }

static uint32 RX_EYESCAN_VS_NEG_DIR_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VS_NEG_DIR_MASK;
    }

static uint32 RX_EYESCAN_VS_NEG_DIR_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VS_NEG_DIR_SHIFT;
    }

static uint32 RX_EYESCAN_VS_RANGE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RX_EYESCAN_VS_RANGE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ES_VERT_OFFSET_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_OFFSET_MASK;
    }

static uint32 ES_VERT_OFFSET_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_OFFSET_SHIFT;
    }

static uint32 ES_VERT_UTSIGN_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_UTSIGN_MASK;
    }

static uint32 ES_VERT_UTSIGN_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_VERT_UTSIGN_SHIFT;
    }

static uint16 ES_HORZ_OFFSET_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_HORZ_OFFSET;
    }

static uint32 ES_HORZ_OFFSET_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_HORZ_OFFSET_MASK;
    }

static uint32 ES_HORZ_OFFSET_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_HORZ_OFFSET_SHIFT;
    }

static uint16 ES_CONTROL_STATUS_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_STATUS;
    }

static uint32 ES_CONTROL_CURRENT_STATE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_CURRENT_STATE_MASK;
    }

static uint32 ES_CONTROL_CURRENT_STATE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_CURRENT_STATE_SHIFT;
    }

static uint32 ES_CONTROL_DONE_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_DONE_MASK;
    }

static uint32 ES_CONTROL_DONE_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_CONTROL_DONE_SHIFT;
    }

static uint16 ES_ERROR_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_ERROR_COUNT;
    }

static uint16 ES_SAMPLE_COUNT_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SAMPLE_COUNT;
    }

static uint16 ES_SDATA_MASK0_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK0;
    }

static uint16 ES_SDATA_MASK1_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK1;
    }

static uint16 ES_SDATA_MASK2_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK2;
    }

static uint16 ES_SDATA_MASK3_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK3;
    }

static uint16 ES_SDATA_MASK4_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_SDATA_MASK4;
    }

static uint16 ES_QUAL_MASK0_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK0;
    }

static uint16 ES_QUAL_MASK1_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK1;
    }

static uint16 ES_QUAL_MASK2_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK2;
    }

static uint16 ES_QUAL_MASK3_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK3;
    }

static uint16 ES_QUAL_MASK4_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_ES_QUAL_MASK4;
    }

static uint16 RX_DATA_WIDTH_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_DATA_WIDTH;
    }

static uint32 RX_DATA_WIDTH_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_DATA_WIDTH_MASK;
    }

static uint32 RX_DATA_WIDTH_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RX_DATA_WIDTH_SHIFT;
    }

static uint16 RXOUT_DIV_REG(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RXOUT_DIV;
    }

static uint32 RXOUT_DIV_MASK(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RXOUT_DIV_MASK;
    }

static uint32 RXOUT_DIV_SHIFT(AtEyeScanController self)
    {
    AtUnused(self);
    return DRP_ADDR_RXOUT_DIV_SHIFT;
    }

static eBool ControllerShouldInit(AtEyeScanController self)
    {
    uint8 numLanes, lane_i;

    /* Already did */
    if (self->initialized)
        return cAtFalse;

    /* Only initialize when there is at least one lane needs to be scanned */
    numLanes = AtEyeScanControllerNumLanes(self);
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(self, lane_i);

        if (AtEyeScanLaneIsEnabled(lane))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet DefaultSettingEnable(AtEyeScanController self, eBool enabled)
    {
    self->defaultSettingDisabled = enabled ? cAtFalse : cAtTrue;
    return cAtOk;
    }

static eBool DefaultSettingIsEnabled(AtEyeScanController self)
    {
    return self->defaultSettingDisabled ? cAtFalse : cAtTrue;
    }

static eAtRet Setup(AtEyeScanController self)
    {
    uint8 numLanes = AtEyeScanControllerNumLanes(self);
    eAtRet ret = cAtOk;
    uint8 lane_i;

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(self, lane_i);
        ret |= AtEyeScanLaneSetup(lane);
        }

    return ret;
    }

static eBool ShouldAutoReset(AtEyeScanController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint16 MaxHorizontalOffsetGet(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);

    switch (AtEyeScanLaneRxOutDiv(lane))
        {
        case 1: return 64;
        case 2: return 128;
        case 3: return 256;
        case 4: return 512;
        default: return 32;
        }
    }

static eAtRet FastVerticalOffsetSet(AtEyeScanController self, AtEyeScanLane lane, uint16 value)
    {
    uint16 regAddress = mMethodsGet(self)->ES_VERT_OFFSET_REG(self);
    uint32 offsetMask  = mMethodsGet(self)->ES_VERT_OFFSET_MASK(self);
    uint32 offsetShift = mMethodsGet(self)->ES_VERT_OFFSET_SHIFT(self);
    uint32 regVal = AtEyeScanLaneDrpRead(lane, regAddress);
    mRegFieldSet(regVal, offset, (value & 0xFFF));
    AtEyeScanLaneDrpWrite(lane, regAddress, (uint16)regVal);
    return cAtOk;
    }

static eAtRet FullVerticalOffsetSet(AtEyeScanController self, AtEyeScanLane lane, uint16 value)
    {
    uint16 regAddress = mMethodsGet(self)->ES_VERT_OFFSET_REG(self);
    uint32 ut_signMask = mMethodsGet(self)->ES_VERT_UTSIGN_MASK(self);
    uint32 ut_signShift = mMethodsGet(self)->ES_VERT_UTSIGN_SHIFT(self);
    uint32 negMask = mMethodsGet(self)->RX_EYESCAN_VS_NEG_DIR_MASK(self);
    uint32 negShift = mMethodsGet(self)->RX_EYESCAN_VS_NEG_DIR_SHIFT(self);
    uint32 fieldMask  = mMethodsGet(self)->ES_VERT_OFFSET_MASK(self);
    uint32 fieldShift = mMethodsGet(self)->ES_VERT_OFFSET_SHIFT(self);
    uint32 regVal = AtEyeScanLaneDrpRead(lane, regAddress);
    mRegFieldSet(regVal, ut_sign, ((value & cBit8) >> 8));
    mRegFieldSet(regVal, neg, ((value & cBit7) >> 7));
    mRegFieldSet(regVal, field, (value & cBit6_0));
    AtEyeScanLaneDrpWrite(lane, regAddress, (uint16)regVal);
    return cAtOk;
    }

static eAtRet VerticalOffsetSet(AtEyeScanController self, AtEyeScanLane lane, uint16 value)
    {
    if (mMethodsGet(self)->ScaneMode(self) == cAtEyeScanModeFull)
        return FullVerticalOffsetSet(self, lane, value);
    return FastVerticalOffsetSet(self, lane, value);
    }

static eAtEyeScanMode ScaneMode(AtEyeScanController self)
    {
    AtUnused(self);
    return cAtEyeScanModeQuick;
    }

static int32 VerticalOffsetToMillivolt(AtEyeScanController self, AtEyeScanLane lane, int32 verticalOffset)
    {
    uint32 voltageSwing = AtEyeScanControllerVoltageSwingInMillivolt(self);
    int32 millivolt = (int)voltageSwing * (verticalOffset & 0x7f) / 127;
    AtUnused(lane);
    return ((verticalOffset) & 0x80) ? (-millivolt) : millivolt;
    }

static float VsRangeInMillivoltGet(AtEyeScanController self, AtEyeScanLane lane)
    {
    uint16 regAddr = mMethodsGet(self)->ES_VERT_OFFSET_REG(self);
    uint32 regVal = AtEyeScanLaneDrpRead(lane, regAddr);
    uint32 vsRangeMask  = mMethodsGet(self)->RX_EYESCAN_VS_RANGE_MASK(self);
    uint32 vsRangeShift = mMethodsGet(self)->RX_EYESCAN_VS_RANGE_SHIFT(self);
    uint32 fieldVal;
    float *vsRanges = mMethodsGet(self)->VsRangeValuesInMillivolt(self);

    if (vsRangeMask == 0) /* May be not available */
        return 0.0f;

    if (vsRanges == NULL)
        return 0.0f;

    mFieldGet(regVal, vsRangeMask, vsRangeShift, uint32, &fieldVal);
    if (fieldVal < 4)
        return vsRanges[fieldVal];

    /* Impossible, just to make compiler happy */
    return vsRanges[0];
    }

static eBool FloatIsEqual(float value1, float value2)
    {
    float diff = value1 - value2;

    if (diff < 0)
        diff = -diff;

    return (diff <= 0.000000001) ? cAtTrue : cAtFalse;
    }

static uint32 VsRangeInMillivolt2Int(AtEyeScanController self, float mV)
    {
    float *vsRanges = mMethodsGet(self)->VsRangeValuesInMillivolt(self);
    uint32 vsRange_i;

    if (vsRanges == NULL)
        return 0;

    for (vsRange_i = 0; vsRange_i < 4; vsRange_i++)
        {
        if (FloatIsEqual(mV, vsRanges[vsRange_i]))
            return vsRange_i;
        }

    return 0;
    }

static void VsRangeSet(AtEyeScanController self, AtEyeScanLane lane, uint8 vsRange)
    {
    uint16 regAddr = mMethodsGet(self)->ES_VERT_OFFSET_REG(self);
    uint32 vsRangeMask  = mMethodsGet(self)->RX_EYESCAN_VS_RANGE_MASK(self);
    uint32 vsRangeShift = mMethodsGet(self)->RX_EYESCAN_VS_RANGE_SHIFT(self);
    uint32 regVal;

    if (vsRangeMask == 0) /* May be not available */
        return;

    regVal = AtEyeScanLaneDrpRead(lane, regAddr);
    mRegFieldSet(regVal, vsRange, vsRange);
    AtEyeScanLaneDrpWrite(lane, regAddr, (uint16)regVal);
    }

static void VsRangeInMillivoltSet(AtEyeScanController self, AtEyeScanLane lane, float mV)
    {
    AtEyeScanControllerVsRangeSet(self, lane, (uint8)VsRangeInMillivolt2Int(self, mV));
    }

static float *VsRangeValuesInMillivolt(AtEyeScanController self)
    {
    AtUnused(self);
    return NULL;
    }

static uint8 DefaultVsRange(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 0;
    }

static eAtRet Reset(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    return AtSerdesControllerEyeScanReset(serdes, AtEyeScanLaneIdGet(lane));
    }

static uint8 DefaultEsPrescale(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtUnused(self);
    AtUnused(lane);
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtEyeScanController object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(drpBaseAddress);
    mEncodeObjectDescription(serdesController);
    mEncodeObjects(lanes, AtEyeScanControllerNumLanes(object));
    AtCoderEncodeString(encoder, object->pName, "name");
    mEncodeNone(name);
    mEncodeUInt(initialized);
    mEncodeUInt(defaultSettingDisabled);
    }

static void OverrideAtObject(AtEyeScanController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtEyeScanController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NumLanes);
        mMethodOverride(m_methods, LaneDataWidth);
        mMethodOverride(m_methods, LaneObjectCreate);
        mMethodOverride(m_methods, SerdesControlRegister);
        mMethodOverride(m_methods, EqualizerModeIsSupported);
        mMethodOverride(m_methods, EqualizerModeSet);
        mMethodOverride(m_methods, EqualizerModeGet);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, PmaReset);
        mMethodOverride(m_methods, TotalWidth);
        mMethodOverride(m_methods, SpeedInGbps);
        mMethodOverride(m_methods, VoltageSwingInMillivolt);
        mMethodOverride(m_methods, DefaultEqualizerModeGet);
        mMethodOverride(m_methods, LaneOffset);
        mMethodOverride(m_methods, DrpRead);
        mMethodOverride(m_methods, DrpWrite);
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        mMethodOverride(m_methods, ShouldHaveDefaultEqualizer);
        mMethodOverride(m_methods, ShowSerdesInfo);
        mMethodOverride(m_methods, ShouldAutoReset);
        mMethodOverride(m_methods, MaxHorizontalOffsetGet);
        mMethodOverride(m_methods, VerticalOffsetSet);
        mMethodOverride(m_methods, ScaneMode);
        mMethodOverride(m_methods, VerticalOffsetToMillivolt);
        mMethodOverride(m_methods, VsRangeValuesInMillivolt);
        mMethodOverride(m_methods, DefaultVsRange);
        mMethodOverride(m_methods, Reset);
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, DefaultEsPrescale);

        mMethodOverride(m_methods, ES_CONTROL_REG);
        mMethodOverride(m_methods, ES_CONTROL_MASK);
        mMethodOverride(m_methods, ES_CONTROL_SHIFT);
        mMethodOverride(m_methods, ES_ERRDET_EN_MASK);
        mMethodOverride(m_methods, ES_ERRDET_EN_SHIFT);
        mMethodOverride(m_methods, ES_EYESCAN_EN_MASK);
        mMethodOverride(m_methods, ES_EYESCAN_EN_SHIFT);
        mMethodOverride(m_methods, ES_PRESCALE_REG);
        mMethodOverride(m_methods, ES_PRESCALE_MASK);
        mMethodOverride(m_methods, ES_PRESCALE_SHIFT);
        mMethodOverride(m_methods, ES_VERT_OFFSET_REG);
        mMethodOverride(m_methods, RX_EYESCAN_VS_NEG_DIR_MASK);
        mMethodOverride(m_methods, RX_EYESCAN_VS_NEG_DIR_SHIFT);
        mMethodOverride(m_methods, RX_EYESCAN_VS_RANGE_MASK);
        mMethodOverride(m_methods, RX_EYESCAN_VS_RANGE_SHIFT);
        mMethodOverride(m_methods, ES_VERT_OFFSET_MASK);
        mMethodOverride(m_methods, ES_VERT_OFFSET_SHIFT);
        mMethodOverride(m_methods, ES_VERT_UTSIGN_MASK);
        mMethodOverride(m_methods, ES_VERT_UTSIGN_SHIFT);
        mMethodOverride(m_methods, ES_HORZ_OFFSET_REG);
        mMethodOverride(m_methods, ES_HORZ_OFFSET_MASK);
        mMethodOverride(m_methods, ES_HORZ_OFFSET_SHIFT);
        mMethodOverride(m_methods, ES_CONTROL_STATUS_REG);
        mMethodOverride(m_methods, ES_CONTROL_CURRENT_STATE_MASK);
        mMethodOverride(m_methods, ES_CONTROL_CURRENT_STATE_SHIFT);
        mMethodOverride(m_methods, ES_CONTROL_DONE_MASK);
        mMethodOverride(m_methods, ES_CONTROL_DONE_SHIFT);
        mMethodOverride(m_methods, ES_ERROR_COUNT_REG);
        mMethodOverride(m_methods, ES_SAMPLE_COUNT_REG);
        mMethodOverride(m_methods, ES_SDATA_MASK0_REG);
        mMethodOverride(m_methods, ES_SDATA_MASK1_REG);
        mMethodOverride(m_methods, ES_SDATA_MASK2_REG);
        mMethodOverride(m_methods, ES_SDATA_MASK3_REG);
        mMethodOverride(m_methods, ES_SDATA_MASK4_REG);
        mMethodOverride(m_methods, ES_QUAL_MASK0_REG);
        mMethodOverride(m_methods, ES_QUAL_MASK1_REG);
        mMethodOverride(m_methods, ES_QUAL_MASK2_REG);
        mMethodOverride(m_methods, ES_QUAL_MASK3_REG);
        mMethodOverride(m_methods, ES_QUAL_MASK4_REG);
        mMethodOverride(m_methods, RX_DATA_WIDTH_REG);
        mMethodOverride(m_methods, RX_DATA_WIDTH_MASK);
        mMethodOverride(m_methods, RX_DATA_WIDTH_SHIFT);
        mMethodOverride(m_methods, RXOUT_DIV_REG);
        mMethodOverride(m_methods, RXOUT_DIV_MASK);
        mMethodOverride(m_methods, RXOUT_DIV_SHIFT);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanController);
    }

AtEyeScanController AtEyeScanControllerObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    mThis(self)->serdesController = serdesController;
    mThis(self)->drpBaseAddress = drpBaseAddress;

    return self;
    }

uint16 AtEyeScanControllerLaneDataWidth(AtEyeScanController self, AtEyeScanLane lane)
    {
    if (self)
        return mMethodsGet(self)->LaneDataWidth(self, lane);
    return 0x0;
    }

uint32 AtEyeScanControllerRead(AtEyeScanController self, uint32 address)
    {
    if (self)
        return mMethodsGet(self)->Read(self, address);
    return 0xDEADCAFE;
    }

void AtEyeScanControllerWrite(AtEyeScanController self, uint32 address, uint32 value)
    {
    if (self)
        mMethodsGet(self)->Write(self, address, value);
    }

float AtEyeScanControllerSpeedInGbps(AtEyeScanController self, AtEyeScanLane lane)
    {
    if (self)
        return mMethodsGet(self)->SpeedInGbps(self, lane);
    return 0.0;
    }

uint32 AtEyeScanControllerTotalWidth(AtEyeScanController self)
    {
    if (self)
        return mMethodsGet(self)->TotalWidth(self);
    return 0;
    }

uint32 AtEyeScanControllerVoltageSwingInMillivolt(AtEyeScanController self)
    {
    if (self)
        return mMethodsGet(self)->VoltageSwingInMillivolt(self);
    return 0;
    }

eAtEyeScanEqualizerMode AtEyeScanControllerDefaultEqualizerModeGet(AtEyeScanController self)
    {
    if (self)
        return mMethodsGet(self)->DefaultEqualizerModeGet(self);

    return cAtEyeScanEqualizerModeLpm;
    }

AtChannel AtEyeScanControllerPhysicalPort(AtEyeScanController self)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    if (serdes)
        return AtSerdesControllerPhysicalPortGet(serdes);
    return NULL;
    }

uint16 AtEyeScanControllerDrpRead(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress)
    {
    uint16 value;

    if (self == NULL)
        return 0xDEAD;

    value = mMethodsGet(self)->DrpRead(self, lane, drpAddress);
    if (value == 0xCAFE)
        {
        AtPrintc(cSevCritical, "%s, %d: Read 0x%04x, value = 0x%04x\r\n",
                 AtSourceLocation,
                 drpAddress, value);
        }

    return value;
    }

void AtEyeScanControllerDrpWrite(AtEyeScanController self, AtEyeScanLane lane, uint16 drpAddress, uint16 value)
    {
    if (self)
        mMethodsGet(self)->DrpWrite(self, lane, drpAddress, value);
    }

uint32 AtEyeScanControllerLaneOffset(AtEyeScanController self)
    {
    if (self)
        return mMethodsGet(self)->LaneOffset(self);
    return cInvalidUint32;
    }

uint32 AtEyeScanControllerDrpBaseAddress(AtEyeScanController self)
    {
    if (self)
        return self->drpBaseAddress;
    return cInvalidUint32;
    }

/*
 * Get equalizer mode
 *
 * @param self This controller
 *
 * @return @ref eAtEyeScanEqualizerMode "Equalizer modes"
 */
eAtEyeScanEqualizerMode AtEyeScanControllerEqualizerModeGet(AtEyeScanController self)
    {
    mAttributeGet(EqualizerModeGet, eAtEyeScanEqualizerMode, cAtEyeScanEqualizerModeUnknown);
    }

/*
 * Set equalizer mode
 *
 * @param self This controller
 * @param mode @ref eAtEyeScanEqualizerMode "Equalizer modes"
 *
 * @return AT return code
 */
eAtRet AtEyeScanControllerEqualizerModeSet(AtEyeScanController self, eAtEyeScanEqualizerMode mode)
    {
    mNumericalAttributeSet(EqualizerModeSet, mode);
    }

eAtRet AtEyeScanControllerPmaReset(AtEyeScanController self)
    {
    if (self)
        return mMethodsGet(self)->PmaReset(self);
    return cAtErrorObjectNotExist;
    }

eAtRet AtEyeScanControllerDefaultSettingEnable(AtEyeScanController self, eBool enabled)
    {
    if (self)
        return DefaultSettingEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool AtEyeScanControllerDefaultSettingIsEnabled(AtEyeScanController self)
    {
    if (self)
        return DefaultSettingIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtEyeScanControllerShowDatabase(AtEyeScanController self)
    {
    if (self)
        return ShowDatabase(self);
    return cAtErrorNullPointer;
    }

eBool AtEyeScanControllerShouldAutoReset(AtEyeScanController  self)
    {
    if (self)
        return mMethodsGet(self)->ShouldAutoReset(self);
    return cAtFalse;
    }

uint16 AtEyeScanControllerMaxHorizontalOffsetGet(AtEyeScanController self, AtEyeScanLane lane)
    {
    if (self)
        return mMethodsGet(self)->MaxHorizontalOffsetGet(self, lane);
    return cInvalidUint16;
    }

eAtRet AtEyeScanControllerVerticalOffsetSet(AtEyeScanController self, AtEyeScanLane lane, uint16 value)
    {
    if (self)
        return mMethodsGet(self)->VerticalOffsetSet(self, lane, value);
    return cAtErrorObjectNotExist;
    }

int32 AtEyeScanControllerVerticalOffsetToMillivolt(AtEyeScanController self, AtEyeScanLane lane, int32 verticalOffset)
    {
    if (self)
        return mMethodsGet(self)->VerticalOffsetToMillivolt(self, lane, verticalOffset);
    return 0;
    }

float AtEyeScanControllerVsRangeInMillivoltGet(AtEyeScanController self, AtEyeScanLane lane)
    {
    if (self)
        return VsRangeInMillivoltGet(self, lane);
    return 0.0;
    }

eAtRet AtEyeScanControllerVerticalOffsetSetWithSignBit(AtEyeScanController self, AtEyeScanLane lane, uint16 vertOffset)
    {
    uint32 fieldMask, fieldShift;
    uint32 vsCode, sign;
    uint16 regAddress = mMethodsGet(self)->ES_VERT_OFFSET_REG(self);
    uint32 regVal = AtEyeScanLaneDrpRead(lane, regAddress);

    /* Vertical offset saved in bit6_0 */
    fieldMask = cBit6_0;
    fieldShift = 0;
    vsCode = mRegField(vertOffset, field);
    fieldMask = mMethodsGet(self)->ES_VERT_OFFSET_MASK(self);
    fieldShift = mMethodsGet(self)->ES_VERT_OFFSET_SHIFT(self);
    mRegFieldSet(regVal, field, vsCode);

    /* Bit 7 is for sign */
    fieldMask = cBit7;
    fieldShift = 7;
    sign = mRegField(vertOffset, field);
    fieldMask = mMethodsGet(self)->RX_EYESCAN_VS_NEG_DIR_MASK(self);
    fieldShift = mMethodsGet(self)->RX_EYESCAN_VS_NEG_DIR_SHIFT(self);
    mRegFieldSet(regVal, field, sign);

    AtEyeScanLaneDrpWrite(lane, regAddress, (uint16)regVal);
    return cAtOk;
    }

void AtEyeScanControllerVsRangeInMillivoltSet(AtEyeScanController self, AtEyeScanLane lane, float mV)
    {
    if (self)
        VsRangeInMillivoltSet(self, lane, mV);
    }

void AtEyeScanControllerVsRangeSet(AtEyeScanController self, AtEyeScanLane lane, uint8 value)
    {
    if (self)
        VsRangeSet(self, lane, value);
    }

eAtRet AtEyeScanControllerReset(AtEyeScanController self, AtEyeScanLane lane)
    {
    if (self)
        return mMethodsGet(self)->Reset(self, lane);
    return cAtErrorNullPointer;
    }

uint8 AtEyeScanControllerDefaultEsPrescale(AtEyeScanController self, AtEyeScanLane lane)
    {
    if (self)
        return mMethodsGet(self)->DefaultEsPrescale(self, lane);
    return cInvalidUint8;
    }

uint8 AtEyeScanControllerDefaultVsRange(AtEyeScanController self, AtEyeScanLane lane)
    {
    if (self)
        return mMethodsGet(self)->DefaultVsRange(self, lane);
    return cInvalidUint8;
    }

/**
 * @addtogroup AtEyeScanController
 * @{
 */

/**
 * Initialize eye-scan controller.
 *
 * @param self This controller
 * @return AT return code
 */
eAtRet AtEyeScanControllerInit(AtEyeScanController self)
    {
    mAttributeGet(Init, eAtRet, cAtErrorNullPointer);
    }

/**
 * Run eye-scan on all lanes. In general, this API only update state machine of
 * all enabled lanes only one time and then return complete status. If one of
 * lanes has not finished scanning, application need to recall to update eye
 * scan state machine. So application code would be:
 *
 * @code{.c}
 * while (!AtEyeScanControllerFinished(eyeScanController))
 *    {
 *    AtEyeScanControllerScan(eyeScanController);
 *    }
 * @endcode
 *
 * And not to block the caller, the above code should be done in a task (thread).
 *
 * @param self This controller
 *
 * @return AT return code.
 *
 * @note For product 60150011, when this API is called, all of Lanes are scanned
 * sequentially until finish and eye data is displayed while this API is running.
 */
eAtRet AtEyeScanControllerScan(AtEyeScanController self)
    {
    uint8 numLanes, lane_i;
    eAtRet ret = cAtOk;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (ControllerShouldInit(self))
        AtEyeScanControllerInit(self);

    Setup(self);

    numLanes = mMethodsGet(self)->NumLanes(self);
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        ret |= ScanLane(self, lane_i);

    return ret;
    }

/**
 * Check if eye-scan finishes for all lanes
 *
 * @param self This controller
 *
 * @retval cAtTrue if eye scan finished
 * @retval cAtFalse if eye scan has not finished yet and caller must recall
 *         AtEyeScanControllerScan() to continue updating eye scan state machine.
 */
eBool AtEyeScanControllerFinished(AtEyeScanController self)
    {
    if (self == NULL)
        return cAtTrue;
    return EyeScanFinished(self);
    }

/**
 * Get associated SERDES that this controller is working on
 *
 * @param self This eye-scan controller
 *
 * @return SERDES controller
 */
AtSerdesController AtEyeScanControllerSerdesControllerGet(AtEyeScanController self)
    {
    return self ? self->serdesController : NULL;
    }

/**
 * Number of lanes this controller has
 *
 * @param self This controller
 *
 * @return Number of lanes
 */
uint8 AtEyeScanControllerNumLanes(AtEyeScanController self)
    {
    mAttributeGet(NumLanes, uint8, 0);
    }

/**
 * Get eye scan lane
 *
 * @param self This controller
 * @param laneId Lane ID
 *
 * @return Eye scan lane or NULL if lane ID is out of range
 */
AtEyeScanLane AtEyeScanControllerLaneGet(AtEyeScanController self, uint8 laneId)
    {
    if (laneId >= AtEyeScanControllerNumLanes(self))
        return NULL;
    return Lane(self, laneId);
    }

/**
 * A utility function to get maximum number of Lanes that one eye scan controller
 * can have
 *
 * @return Maximum number of lanes
 */
uint8 AtEyeScanControllerMaxNumLanes(void)
    {
    return 4;
    }

/**
 * Show eye-scan debug information
 *
 * @param self This controller
 *
 * @return AT return code
 */
eAtRet AtEyeScanControllerDebug(AtEyeScanController self)
    {
    mAttributeGet(Debug, eAtRet, cAtErrorNullPointer);
    }

/**
 * @}
 */

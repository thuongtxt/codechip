/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtUartReport.c
 *
 * Created Date: Aug 25, 2016
 *
 * Description : Uart report
 *
 * Notes       : see http://www.xilinx.com/support/answers/67086.html to
 *               understand the source code
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"
#include "AtOsal.h"
#include "AtList.h"
#include "AtSemController.h"
#include "AtDriver.h"
#include "../physical/AtSemControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cHeaderLineNum 8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtUartReportAddressEntry
    {
    uint32 LA; /* Linear address */
    uint32 WD; /* Word index */
    uint32 BT; /* Bit index */
    eBool  isCorrectable;
    } tAtUartReportAddressEntry;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 UART report:
 O
SC 02
O>
SC 04
SED OK
PA 0000121E
LA 000004F8
WD 05 BT 0E
COR
WD 05 BT 0E
END
FC 40
SC 08
FC 40
SC 02
O>
SC 04
DED
PA 00001223
LA 000004FD
COR
END
FC 60
SC 08
FC 60
SC 00
I>
 * */

static const char* FirstLineOf(const char **remainingReport, uint32 *remainingLength, uint32* firstLineLength)
    {
    uint32 i = 0;
    const char* firstLine = NULL;
    int32 remainingNumChar;

    for (i = 0; i < *remainingLength; i++)
        {
        if ((*remainingReport)[i] == '\r')
            break;
        if ((*remainingReport)[i] == '\0')
            break;
        }

    *firstLineLength = i;
    firstLine = (const char*)*remainingReport;
    remainingNumChar = (int32)(*remainingLength - (*firstLineLength + 1));
    if (remainingNumChar > 0)
        {
        *remainingLength = (uint32)remainingNumChar;
        *remainingReport = &(*remainingReport)[*firstLineLength + 1];
        }
    else
        *remainingLength = 0;

    return firstLine;
    }

static uint32 MaxLength(uint32 strBufLength, uint32 contentLength)
    {
    if ((strBufLength - 1) > contentLength)
        return contentLength;

    return (uint32)(strBufLength - 1);
    }

static tAtUartReportAddressEntry* AfterCORlineToUpdateLfaEntry(const char ** nextLine, uint32 *length, tAtUartReportAddressEntry* entry)
    {
    uint32 lineLength = 0;
    char aline[64], *ptoken;
    const char *delim = " ";
    uint32 alineMaxSize = sizeof(aline), wd = 0, bt = 0;
    const char* alinePos;

    if (*length == 0)
        return entry;
    alinePos = FirstLineOf(nextLine, length, &lineLength);
    if (lineLength == 0)
        return entry;
    AtOsalMemInit(aline, 0, alineMaxSize);
    AtOsalMemCpy(aline, alinePos, MaxLength(alineMaxSize, lineLength));
    ptoken = AtStrtok(aline, delim);
    if (ptoken == NULL)
        return entry;

    if (AtStrcmp(ptoken, "END") == 0)
        entry->isCorrectable = cAtFalse;
    else if (AtStrcmp(ptoken, "WD") == 0)
        {
        ptoken = AtStrtok(NULL, delim);
        if (ptoken == NULL)
            return entry;

        wd = AtStrtoul((const char*)ptoken, NULL, 16);

        ptoken = AtStrtok(NULL, delim);
        if (ptoken == NULL)
            return entry;

        if (AtStrcmp(ptoken, "BT") == 0)
            {
            ptoken = AtStrtok(NULL, delim);
            if (ptoken == NULL)
                return entry;

            bt = AtStrtoul((const char*)ptoken, NULL, 16);
            entry->BT = bt;
            entry->WD = wd;
            entry->isCorrectable = cAtTrue;
            }
        }

    return entry;
    }

static tAtUartReportAddressEntry* AlineToLfaEntry(const char ** nextLine, uint32 *length)
    {
    uint32 lineLength = 0;
    char aline[64], *ptoken;
    const char *delim = " ";
    uint32 alineMaxSize = sizeof(aline), lfa = 0, wd = 0, bt = 0;
    const char* alinePos;
    tAtUartReportAddressEntry* entry = NULL;

    if (*length == 0)
        return entry;
    alinePos = FirstLineOf(nextLine, length, &lineLength);
    if (lineLength == 0)
        return entry;
    AtOsalMemInit(aline, 0, alineMaxSize);
    AtOsalMemCpy(aline, alinePos, MaxLength(alineMaxSize, lineLength));

    ptoken = AtStrtok(aline, delim);
    if (ptoken == NULL)
        return entry;

    if (AtStrcmp(ptoken, "LA") == 0)
        {
        ptoken = AtStrtok(NULL, delim);
        if (ptoken == NULL)
            return entry;

        lfa = AtStrtoul((const char*)ptoken, NULL, 16);
        entry = (tAtUartReportAddressEntry*)AtOsalMemAlloc(sizeof(tAtUartReportAddressEntry));
        AtOsalMemInit(entry, 0, sizeof(tAtUartReportAddressEntry));
        entry->LA = lfa;
        }
    else
        return entry;

    if (*length == 0)
        return entry;
    alinePos = FirstLineOf(nextLine, length, &lineLength);
    if (lineLength == 0)
        return entry;
    AtOsalMemInit(aline, 0, alineMaxSize);
    AtOsalMemCpy(aline, alinePos, MaxLength(alineMaxSize, lineLength));
    ptoken = AtStrtok(aline, delim);
    if (ptoken == NULL)
        return entry;

    if (AtStrcmp(ptoken, "WD") == 0)
        {
        ptoken = AtStrtok(NULL, delim);
        if (ptoken == NULL)
            return entry;

        wd = AtStrtoul((const char*)ptoken, NULL, 16);
        ptoken = AtStrtok(NULL, delim);
        if (ptoken == NULL)
            return entry;

        if (AtStrcmp(ptoken, "BT") == 0)
            {
            ptoken = AtStrtok(NULL, delim);
            if (ptoken == NULL)
                return entry;

            bt = AtStrtoul((const char*)ptoken, NULL, 16);
            entry->BT = bt;
            entry->WD = wd;
            }

        /*COR after the line WD*/
        if (*length == 0)
            return entry;
        alinePos = FirstLineOf(nextLine, length, &lineLength);
        if (lineLength == 0)
            return entry;
        AtOsalMemInit(aline, 0, alineMaxSize);
        AtOsalMemCpy(aline, alinePos, MaxLength(alineMaxSize, lineLength));
        ptoken = AtStrtok(aline, delim);
        if (ptoken == NULL)
            return entry;

        if (AtStrcmp(ptoken, "COR") == 0)
            entry = AfterCORlineToUpdateLfaEntry(nextLine, length, entry);
        }
    else if (AtStrcmp(ptoken, "COR") == 0) /*COR after the LA*/
        entry = AfterCORlineToUpdateLfaEntry(nextLine, length, entry);

    return entry;
    }

static AtList UartReportToLfaEntries(const char* uartReport, uint32 reportLength)
    {
    const char *remainingLine = uartReport;
    uint32 remainingLength = reportLength;
    AtList list = AtListCreate(0);

    while (remainingLength != 0)
        {
        tAtUartReportAddressEntry* entry = AlineToLfaEntry(&remainingLine, &remainingLength);
        if (entry != NULL)
            AtListObjectAdd(list, (AtObject)entry);
        }

    return list;
    }

static eBool IsEbdFilePathValid(AtSemController self, const char *filePath, uint8 slrNum)
    {
    if (filePath)
        return cAtTrue;

    AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                "SEM#%d SLR#%d Essential error Lookup at NULL file path\r\n",
                AtSemControllerIdGet(self), slrNum);
    return cAtFalse;
    }

static eBool IsEntryValid(AtSemController self, tAtUartReportAddressEntry *entry)
    {
    uint32 localLfa = AtSemControllerGlobalLfaToLocalLfa(self, entry->LA);
    uint32 slrNum = AtSemControllerGlobalLfaToSlrId(self, entry->LA);

    if ((entry->BT >= 32) ||
        (entry->WD >= AtSemControllerFrameSize(self)) ||
        (localLfa > AtSemControllerMaxLfa(self)) ||
        (slrNum >= AtSemControllerNumSlr(self)))
        return cAtFalse;

    return cAtTrue;
    }

static uint32 LineNumberCalculate(AtSemController self, tAtUartReportAddressEntry *entry)
    {
    uint32 frameSize = AtSemControllerFrameSize(self);
    uint32 numDummyWords = AtSemControllerNumDummyWordsInEbd(self);
    uint32 localLfa =  AtSemControllerGlobalLfaToLocalLfa(self, entry->LA);
    return cHeaderLineNum + (frameSize * (localLfa + 1)) + entry->WD + numDummyWords;
    }

static AtFile EbdFileOpen(AtSemController self, tAtUartReportAddressEntry *entry)
    {
    AtFile ebdFile = NULL;
    uint32 slrIndex = AtSemControllerGlobalLfaToSlrId(self, entry->LA);
    const char *ebdFilePath = AtSemControllerEbdFilePathGet(self, (uint8)slrIndex);

    if (!IsEbdFilePathValid(self, ebdFilePath, (uint8)slrIndex) || !IsEntryValid(self, entry))
        return NULL;

    ebdFile = AtStdFileOpen(AtStdSharedStdGet(), ebdFilePath, cAtFileOpenModeRead);
    if (ebdFile == NULL)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "Open the EDB file failed at: %s\r\n", ebdFilePath);
        return NULL;
        }

    return ebdFile;
    }

static void HeaderPartSkip(AtFile ebdFile, char *buffer, uint32 bufferSize)
    {
    uint32 lineIndex = 0;

    while (AtFileGets(ebdFile, buffer, bufferSize) != NULL)
        {
        lineIndex = lineIndex + 1;
        if (lineIndex == (cHeaderLineNum + 1))
            break;
        }
    }

static eBool EdbFileEssentialLookup(AtSemController self, tAtUartReportAddressEntry *entry)
    {
    uint32 offset = 0, lineNum = 0;
    char line[256];
    AtFile ebdFile = NULL;

    /* Try opening EBD file */
    ebdFile = EbdFileOpen(self, entry);
    if (ebdFile == NULL)
        return cAtFalse;
    AtFileSeek(ebdFile, 0, cAtFileSeekPositionAt_SEEK_SET);

    HeaderPartSkip(ebdFile, line, sizeof(line));

    /* Seek to expected offset corresponding to the reported entry */
    lineNum = LineNumberCalculate(self, entry);
    offset = (lineNum - (cHeaderLineNum + 1)) * AtStrlen(line);
    AtFileSeek(ebdFile, (long int)offset, cAtFileSeekPositionAt_SEEK_CUR);

    /* And read it */
    AtOsalMemInit(line, 0, sizeof(line));
    AtFileGets(ebdFile, line, sizeof(line));
    AtStdFileClose(ebdFile);

    /* See if it is essential */
    if (line[31 - entry->BT] == '1')
        return cAtTrue;

    return cAtFalse;
    }

static void LfaEntryDelete(AtObject entry)
    {
    AtOsalMemFree(entry);
    }

uint32 AtUartReportLineCount(const char *report, uint32 reportLength)
    {
    const char *remainingLine = report;
    uint32 remainingLength = reportLength, firstLineLength = 0, count = 0;

    while (remainingLength > 0)
        {
        firstLineLength = 0;
        FirstLineOf(&remainingLine, &remainingLength, &firstLineLength);
        if (firstLineLength > 0)
            count++;
        }
    return count;
    }

const char* AtUartReportAlineAtLineIndex(const char *report, uint32 reportLength, uint32 lineNum, uint32 *lineLength)
    {
    const char *remainingLine = report;
    const char *aline = NULL;
    uint32 remainingLength = reportLength, count = 0;

    *lineLength = 0;
    while (remainingLength > 0)
        {
        *lineLength = 0;
        aline = FirstLineOf(&remainingLine, &remainingLength, lineLength);
        if (*lineLength > 0)
            count++;
        if (count > 0 && ((count - 1) == lineNum))
            return aline;
        }

    return aline;
    }

/*
 * Lookup an LFA error to see if it is essential or non-essential
 *
 * @param ebdFilePath Path to the EBD file
 * @param uartReport UART report buffer, which store LFA information
 * @param reportLength UART report buffer length
 *
 * @return cAtTrue if lookup result is essential, cAtFalse if lookup result is non-essential
 */
eBool AtUartRerportErrorIsEssential(AtSemController self, const char *uartReport, uint32 reportLength)
    {
    AtList lfaList;
    AtIterator iterator;
    tAtUartReportAddressEntry *entry;
    eBool ret = cAtFalse;

    lfaList = UartReportToLfaEntries(uartReport, reportLength);
    iterator = AtListIteratorGet(lfaList);
    while ((entry = (tAtUartReportAddressEntry*)AtIteratorNext(iterator)) != NULL)
        {
        if (EdbFileEssentialLookup(self, entry))
            {
            ret = cAtTrue;
            break;
            }
        }

    AtListDeleteWithObjectHandler(lfaList, LfaEntryDelete);
    return ret;
    }

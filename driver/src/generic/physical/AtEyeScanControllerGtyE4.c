/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : AtEyeScanControllerGtyE4.c
 *
 * Created Date: Nov 6, 2018
 *
 * Description : GTYE4 Eye Scan Controller
 *
 * Notes       : GTY-578: GTYE4_CHANNEL
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEyeScanControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanControllerMethods m_AtEyeScanControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtEyeScanController self)
    {
    AtUnused(self);
    return "gty4_eyescan_controller";
    }

static eAtRet VerticalOffsetSet(AtEyeScanController self, AtEyeScanLane lane, uint16 vertOffset)
    {
    return AtEyeScanControllerVerticalOffsetSetWithSignBit(self, lane, vertOffset);
    }

static int32 VerticalOffsetToMillivolt(AtEyeScanController self, AtEyeScanLane lane, int32 verticalOffset)
    {
    float rangeInMillivolt = AtEyeScanControllerVsRangeInMillivoltGet(self, lane);
    float voltage = (float)((uint32)verticalOffset & cBit6_0) * rangeInMillivolt;
    eBool negative = ((uint32)verticalOffset & cBit7) ? cAtTrue : cAtFalse;
    return (int32)(negative ? (-voltage) : voltage);
    }

static void OverrideAtEyeScanController(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanControllerOverride, mMethodsGet(self), sizeof(m_AtEyeScanControllerOverride));

        mMethodOverride(m_AtEyeScanControllerOverride, TypeString);
        mMethodOverride(m_AtEyeScanControllerOverride, VerticalOffsetSet);
        mMethodOverride(m_AtEyeScanControllerOverride, VerticalOffsetToMillivolt);
        }

    mMethodsSet(self, &m_AtEyeScanControllerOverride);
    }

static void Override(AtEyeScanController self)
    {
    OverrideAtEyeScanController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEyeScanControllerGtyE4);
    }

AtEyeScanController AtEyeScanControllerGtyE4ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanControllerGtyE3ObjectInit(self, serdesController, drpBaseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanController AtEyeScanControllerGtyE4New(AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtEyeScanControllerGtyE4ObjectInit(newController, serdesController, drpBaseAddress);
    }

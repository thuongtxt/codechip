/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtPtchServiceInternal.h
 * 
 * Created Date: May 7, 2016
 *
 * Description : PTCH service
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTCHSERVICEINTERNAL_H_
#define _ATPTCHSERVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPtchService.h"
#include "atclib.h"
#include "../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPtchServiceMethods
    {
    /* Read-only attributes */
    eAtPtchServiceType (*TypeGet)(AtPtchService self);
    AtModule (*ModuleGet)(AtPtchService self);

    /* Ingress two-bytes PTCH (direction from HyPhy to XFI). Note, this is
     * not applicable for eXAUI#0 */
    eAtRet (*IngressPtchSet)(AtPtchService self, uint16 ptch);
    uint16 (*IngressPtchGet)(AtPtchService self);

    /* Egress one-byte PTCH (direction from XFI to HyPhy). */
    eAtRet (*EgressPtchSet)(AtPtchService self, uint16 ptch);
    uint16 (*EgressPtchGet)(AtPtchService self);

    /* Enabling */
    eAtRet (*Enable)(AtPtchService self, eBool enabled);
    eBool (*IsEnabled)(AtPtchService self);

    /* Offset attribute is only applicable for eXAUI#0.
     * - Direction from eXAUI --> XFI:   PTCH = eXAUIChannelId + offset
     * - Direction from XFI   --> eXAUI: eXAUIChannelId = channelId - offset
     * Calling these APIs on other service type will lead to error code or
     * invalid value are returned */
    eAtRet (*IngressOffsetSet)(AtPtchService self, uint32 offset);
    uint32 (*IngressOffsetGet)(AtPtchService self);
    eAtRet (*IngressModeSet)(AtPtchService self, eAtPtchMode mode);
    eAtPtchMode (*IngressModeGet)(AtPtchService self);

    eAtRet (*EgressOffsetSet)(AtPtchService self, uint32 offset);
    uint32 (*EgressOffsetGet)(AtPtchService self);

    /* Counters */
    eBool (*CounterIsSupported)(AtPtchService self, eAtPtchServiceCounterType counterType);
    uint32 (*CounterGet)(AtPtchService self, eAtPtchServiceCounterType counterType);
    uint32 (*CounterClear)(AtPtchService self, eAtPtchServiceCounterType counterType);
    }tAtPtchServiceMethods;

typedef struct tAtPtchService
    {
    tAtObject super;
    const tAtPtchServiceMethods * methods;

    AtModule module;
    uint32 serviceType;
    }tAtPtchService;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtchService AtPtchServiceObjectInit(AtPtchService self, uint32 serviceType, AtModule module);


#ifdef __cplusplus
}
#endif
#endif /* _ATPTCHSERVICEINTERNAL_H_ */


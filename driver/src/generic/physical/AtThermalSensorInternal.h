/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : AtThermalSensorInternal.h
 * 
 * Created Date: Dec 10, 2015
 *
 * Description : AtThermalSensor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTHERMALSENSORINTERNAL_H_
#define _ATTHERMALSENSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSensorInternal.h"
#include "AtThermalSensor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtThermalSensorMethods
    {
    int32 (*CurrentTemperatureGet)(AtThermalSensor self);
    int32 (*MinRecordedTemperatureGet)(AtThermalSensor self);
    int32 (*MaxRecordedTemperatureGet)(AtThermalSensor self);
    eAtRet (*AlarmSetThresholdSet)(AtThermalSensor self, int32 celsius);
    eAtRet (*AlarmClearThresholdSet)(AtThermalSensor self, int32 celsius);
    int32 (*AlarmSetThresholdGet)(AtThermalSensor self);
    int32 (*AlarmClearThresholdGet)(AtThermalSensor self);
    int32 (*AlarmSetThresholdMax)(AtThermalSensor self);
    int32 (*AlarmSetThresholdMin)(AtThermalSensor self);
    int32 (*AlarmClearThresholdMax)(AtThermalSensor self);
    int32 (*AlarmClearThresholdMin)(AtThermalSensor self);
    }tAtThermalSensorMethods;

typedef struct tAtThermalSensor
    {
    tAtSensor super;
    const tAtThermalSensorMethods *methods;
    }tAtThermalSensor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtThermalSensor AtThermalSensorObjectInit(AtThermalSensor self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATTHERMALSENSORINTERNAL_H_ */

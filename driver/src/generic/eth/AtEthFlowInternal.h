/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : ThaEthFlowInternal.h
 *
 * Created Date: Aug 31, 2012
 *
 * Description : ETH Flow
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATETHFLOWINTERNAL_H_
#define _ATETHFLOWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthFlow.h"
#include "AtModuleEth.h"
#include "AtPtchService.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtEthFlowMethods
    {
    /* Address attributes */
	eAtModuleEthRet (*EgressDestMacSet)(AtEthFlow self, uint8 *egressDestMac);
    eAtModuleEthRet (*EgressDestMacGet)(AtEthFlow self, uint8 *egressDestMac);

    eAtModuleEthRet (*EgressSourceMacSet)(AtEthFlow self, uint8 *egressSourceMac);
    eAtModuleEthRet (*EgressSourceMacGet)(AtEthFlow self, uint8 *egressSourceMac);
    eBool (*EgressMacIsProgrammable)(AtEthFlow self);
    
    /* Egress VLAN attribute */
    eAtModuleEthRet (*EgressVlanSet)(AtEthFlow self, const tAtEthVlanDesc *egressVlan);
 
	/* Ethernet Header */
    eAtRet (*EgressHeaderSet)(AtEthFlow self, uint8 *egressSourceMac, uint8 *egressDestMac, const tAtEthVlanDesc *egressVlan);
    
    /* VLAN type */
    eAtRet (*EgressCVlanTpIdSet)(AtEthFlow self, uint16 tpid);
    uint16 (*EgressCVlanTpIdGet)(AtEthFlow self);
    eAtRet (*EgressSVlanTpIdSet)(AtEthFlow self, uint16 tpid);
    uint16 (*EgressSVlanTpIdGet)(AtEthFlow self);
    uint16 (*EgressCVlanDefaultTpId)(AtEthFlow self);
    uint16 (*EgressSVlanDefaultTpId)(AtEthFlow self);

    /* Manage Ingress VLAN traffic descriptors */
    eAtModuleEthRet (*IngressVlanAdd)(AtEthFlow self, const tAtEthVlanDesc *desc);
    eAtModuleEthRet (*IngressVlanRemove)(AtEthFlow self, const tAtEthVlanDesc *desc);
    uint32 (*IngressAllVlansGet)(AtEthFlow self, tAtEthVlanDesc *descs);
    uint32 (*IngressNumVlansGet)(AtEthFlow self);
    tAtEthVlanDesc *(*IngressVlanAtIndex)(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs);
    eBool (*IngressVlanCanUse)(AtEthFlow self, const tAtEthVlanDesc *desc);

    /* Manage egress VLAN traffic descriptors */
    eAtModuleEthRet (*EgressVlanAdd)(AtEthFlow self, const tAtEthVlanDesc *desc);
    eAtModuleEthRet (*EgressVlanRemove)(AtEthFlow self, const tAtEthVlanDesc *desc);
    uint32 (*EgressNumVlansGet)(AtEthFlow self);
    tAtEthVlanDesc *(*EgressVlanAtIndex)(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs);
    eBool (*EgressVlanCanUse)(AtEthFlow self, const tAtEthVlanDesc *desc);

    /* Attribute */
    AtHdlcLink (*HdlcLinkGet)(AtEthFlow self);
    AtMpBundle (*MpBundleGet)(AtEthFlow self);
    eAtModuleEthRet (*FrVirtualCircuitSet)(AtEthFlow self, AtFrVirtualCircuit circuit);
    AtFrVirtualCircuit (*FrVirtualCircuitGet)(AtEthFlow self);
    AtPw (*PwGet)(AtEthFlow self);

    eBool (*IsBusy)(AtEthFlow self);
	
	/* Flow control */
    AtEthFlowControl (*FlowControlCreate)(AtEthFlow self);
    AtEthFlowControl (*FlowControlGet)(AtEthFlow self);

    /* PTCH */
    eAtRet (*PtchInsertionModeSet)(AtEthFlow self, eAtPtchMode insertMode);
    eAtPtchMode (*PtchInsertionModeGet)(AtEthFlow self);
    eAtRet (*PtchServiceEnable)(AtEthFlow self, eBool enable);
    eBool (*PtchServiceIsEnabled)(AtEthFlow self);
    } tAtEthFlowMethods;

/* This structure is to represent for Ethernet Flow */
typedef struct tAtEthFlow
    {
    /* Inherit AtChannel */
    tAtChannel super;

    /* Implementation */
    const tAtEthFlowMethods *methods;
    
    /* Private attribute */
    uint8 flowType;
    eBool flowIsUsed;
    AtList ingressVlans;
    AtList egressVlans;

    /* Associations */
    AtEthFlowControl flowControl;
    AtEncapChannel encapChannel;
    AtVcg vcg;
    AtHdlcBundle bundle;        /* The bundle that this flow is bound */
    AtHdlcLink hdlcLink;        /* The link that this flow is bound */
    AtFrVirtualCircuit circuit; /* The Circuit that this flow is bound */
    AtPw pw;                    /* The PW that this flow is bound to */
    } tAtEthFlow;

/**
 * @brief Flow type
 */
typedef enum eAtEthFlowType
    {
    cAtEthFlowTypeAny,
    cAtEthFlowTypePw,       /**< TDM Circuit Emulation (over Ethernet) */
    cAtEthFlowTypePos,      /**< Packet over SONET/SDH/PDH (PoS) */
    cAtEthFlowTypeEos,      /**< Ethernet over SONET/SDH (EoS) */
    cAtEthFlowTypeEop,      /**< Ethernet over PDH (EoP) */

    /* Deprecated flows */
    cAtEthFlowTypeNpoPpp = 16,
    cAtEthFlowTypeEthoPpp,
    cAtEthFlowTypePcpoEth,
    cAtEthFlowTypePppoEth,
    } eAtEthFlowType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow AtEthFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module);

/* Access private data */
void AtEthFlowTypeSet(AtEthFlow self, uint8 flowType);
uint8 AtEthFlowTypeGet(AtEthFlow self);
void AtEthFlowUse(AtEthFlow self);
eBool AtEthFlowIsUsed(AtEthFlow self);
eBool AtEthFlowIsBusy(AtEthFlow self);

eAtRet AtEthFlowHdlcLinkSet(AtEthFlow self, AtHdlcLink hdlcLink);
eAtRet AtEthFlowFrVirtualCircuitSet(AtEthFlow self, AtFrVirtualCircuit frVirtualCircuit);

void AtEthFlowFlowControlSet(AtEthFlow self, AtEthFlowControl flowCtrlController);
void AtEthFlowBoundEncapChannelSet(AtEthFlow self, AtEncapChannel encapChannel);

void AtEthFlowVcgSet(AtEthFlow self, AtVcg vcg);
AtVcg AtEthFlowVcgGet(AtEthFlow self);
eBool AtEthVlanDescIsValid(const tAtEthVlanDesc *vlan);
void AtUtilVlanDescriptorSerialize(AtObject descriptor, AtCoder encoder);

/* PTCH */
eAtRet AtEthFlowPtchInsertionModeSet(AtEthFlow self, eAtPtchMode insertMode);
eAtPtchMode AtEthFlowPtchInsertionModeGet(AtEthFlow self);
eAtRet AtEthFlowPtchServiceEnable(AtEthFlow self, eBool enable);
eBool AtEthFlowPtchServiceIsEnabled(AtEthFlow self);

void AtEthFlowHdlcBundleSet(AtEthFlow self, AtHdlcBundle bundle);
AtHdlcBundle AtEthFlowHdlcBundleGet(AtEthFlow self);

void AtEthFlowPwSet(AtEthFlow self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHFLOWINTERNAL_H_ */


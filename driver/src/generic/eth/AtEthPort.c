/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * At information contained herein is confidential property of At Arrive
 * Technologies. At use, copying, transfer or disclosure of such information is
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : AtEthPort.c
 *
 * Created Date: Aug 10, 2012
 *
 * Description : 
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../util/AtUtil.h"
#include "../../util/coder/AtCoderUtil.h"
#include "AtEthFlowControl.h"
#include "AtEthPortInternal.h"
#include "AtModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) 		((AtEthPort)self)
#define mPortIsValid(self) 	(self ? cAtTrue : cAtFalse)
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

#define mPutCounter(counterField, counterType) \
    value = CounterGetFunc(self, counterType); \
    if (counters)                              \
        counters->counterField = value;

#define mIpV4AttributeSet(method, address)                                     \
    do                                                                         \
    {                                                                          \
    eAtRet ret_ = cAtErrorNullPointer;                                         \
                                                                               \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *ipString;                                               \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize); \
        ipString = AtUtilBytes2String(address, cAtIpv4AddressLen, 10, buffer, bufferSize); \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n", \
                                AtFunction,                                    \
                                AtObjectToString((AtObject)self), ipString);   \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
                                                                               \
    if (self)                                                                  \
        ret_ = mMethodsGet(self)->method(self, address);                       \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret_;                                                               \
    }while(0)

#define mIpV6AttributeSet(method, address)                                     \
    do                                                                         \
    {                                                                          \
    eAtRet ret_ = cAtErrorNullPointer;                                         \
                                                                               \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *ipString;                                               \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize); \
        ipString = AtUtilBytes2String(address, cAtIpv6AddressLen, 10, buffer, bufferSize); \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n", \
                                AtFunction,                                    \
                                AtObjectToString((AtObject)self), ipString);   \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
                                                                               \
    if (self)                                                                  \
        ret_ = mMethodsGet(self)->method(self, address);                       \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret_;                                                               \
    }while(0)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEthPortMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "eport";
    }

static eAtRet FlowControlDefaultSet(AtEthPort self)
    {
    return AtEthPortFlowControlEnable(self, cAtFalse);
    }

static eAtRet Init(AtChannel self)
    {
    AtEthPort ethPort = (AtEthPort)self;
    AtSerdesController serdes;

    /* Super */
    eAtRet ret = m_AtChannelMethods->Init(self);

    /* Set default for Port Speed. Because not all port can support these default
     * settings, just silently ignore return codes */
    eAtEthPortInterface defaultIntf = mMethodsGet(ethPort)->DefaultInterface(ethPort);
    AtEthPortInterfaceSet(ethPort, defaultIntf);

    AtEthPortSpeedSet(ethPort, cAtEthPortSpeed1000M);
    AtEthPortAutoNegEnable(ethPort, cAtFalse);
    AtEthPortDuplexModeSet(ethPort, cAtEthPortWorkingModeFullDuplex);
    mMethodsGet(mThis(self))->FlowControlDefaultSet(mThis(self));
    AtEthPortMacCheckingEnable(ethPort, cAtFalse);
    AtEthPortPauseFrameExpireTimeSet(ethPort, 10);
    AtEthPortPauseFrameIntervalSet(ethPort, 10);
    if (mMethodsGet(ethPort)->RxIpgIsConfigurable(ethPort))
        AtEthPortRxIpgSet(ethPort, AtEthPortDefaultIpg(ethPort));
    if (mMethodsGet(ethPort)->TxIpgIsConfigurable(ethPort))
        AtEthPortTxIpgSet(ethPort, AtEthPortTxDefaultIpg(ethPort));
    AtChannelLoopbackSet(self, cAtLoopbackModeRelease);

    serdes = AtEthPortSerdesController(ethPort);
    if (serdes)
        AtSerdesControllerInit(serdes);

    return ret;
    }

static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eBool HasDestMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static eAtModuleEthRet SourceMacAddressGet(AtEthPort port, uint8 *address)
    {
	AtUnused(address);
	AtUnused(port);
    /* Let concrete class do */
    return cAtError;
    }
    
static eAtModuleEthRet MacCheckingEnable(AtEthPort port, eBool enable)
    {
	AtUnused(enable);
	AtUnused(port);
    /* Let concrete class do */
    return cAtError;
    }
    
static eBool MacCheckingIsEnabled(AtEthPort port)
    {
	AtUnused(port);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
	AtUnused(speed);
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

/* Ethernet port speed */
static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
	AtUnused(speed);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtEthPortSpeedUnknown;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
	AtUnused(duplexMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtEthPortWorkingModeAutoDetect;
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
	AtUnused(txIpg);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static uint8 TxIpgGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eBool TxIpgIsConfigurable(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
	AtUnused(rxIpg);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static uint8 RxIpgGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eBool RxIpgIsConfigurable(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static eBool AutoNegIsEnabled(AtEthPort self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtEthPortSpeed AutoNegSpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeedUnknown;
    }

static eAtEthPortDuplexMode AutoNegDuplexModeGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortWorkingModeUnknown;
    }

static eBool AutoNegIsComplete(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
	AtUnused(interface);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtEthPortAutoNegState AutoNegStateGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModuleEthRet AutoNegRestartOn(AtEthPort self, eBool on)
    {
    AtUnused(self);
    return on ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AutoNegRestartIsOn(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
    AtEthPort port = (AtEthPort)self;
    tAtEthPortAllConfig *configs = (tAtEthPortAllConfig *)pAllConfig;

    if (pAllConfig == NULL)
        return cAtErrorNullPointer;

    configs->numTxIpg = AtEthPortTxIpgGet(port);
    AtEthPortIpV4AddressGet(port, configs->srcIpv4Addr);
    AtEthPortIpV6AddressGet(port, configs->srcIpv6Addr);
    AtEthPortSourceMacAddressGet(port, configs->srcMacAddr);
    configs->interface = AtEthPortInterfaceGet(port);
    configs->speed = AtEthPortSpeedGet(port);
    configs->workingMd = AtEthPortDuplexModeGet(port);
    configs->autoNegEn = AtEthPortAutoNegIsEnabled(port);
    configs->numRxIpg = AtEthPortRxIpgGet(port);
    configs->flowCtrlEn = AtEthPortFlowControlIsEnabled(port);
    configs->pauFrmInterval = AtEthPortPauseFrameIntervalGet(port);
    configs->pauFrmExpTime = AtEthPortPauseFrameExpireTimeGet(port);
    configs->macChkEn = AtEthPortMacCheckingIsEnabled(port);

    return cAtOk;
    }

static eAtEthPortInterface DefaultInterface(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return AtModuleEthPortDefaultInterface(ethModule, self);
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
	AtUnused(self);
    /* Let sub class do */
    return NULL;
    }

static eAtModuleEthRet MaxPacketSizeSet(AtEthPort self, uint32 maxPacketSize)
    {
	AtUnused(maxPacketSize);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static uint32 MaxPacketSizeGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static eAtModuleEthRet MinPacketSizeSet(AtEthPort self, uint32 minPacketSize)
    {
	AtUnused(minPacketSize);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static uint32 MinPacketSizeGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static eAtModuleEthRet Switch(AtEthPort self, AtEthPort toPort)
    {
	AtUnused(toPort);
	AtUnused(self);
    return cAtError;
    }

static AtEthPort SwitchedPortGet(AtEthPort self)
    {
	AtUnused(self);
    return NULL;
    }

static eAtModuleEthRet SwitchRelease(AtEthPort self)
    {
	AtUnused(self);
    return cAtError;
    }

static eAtModuleEthRet Bridge(AtEthPort self, AtEthPort toPort)
    {
	AtUnused(toPort);
	AtUnused(self);
    return cAtError;
    }

static AtEthPort BridgedPortGet(AtEthPort self)
    {
	AtUnused(self);
    return NULL;
    }

static eAtModuleEthRet BridgeRelease(AtEthPort self)
    {
	AtUnused(self);
    return cAtError;
    }

static uint32 ProvisionedBandwidthInKbpsGet(AtEthPort self)
    {
	AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static uint32 RunningBandwidthInKbpsGet(AtEthPort self)
    {
    AtUnused(self);
    /* Let sub class do */
    return 0;
    }

static eAtModuleEthRet HiGigEnable(AtEthPort self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtError;
    }

static eBool HiGigIsEnabled(AtEthPort self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static AtEthFlowControl FlowControlCreate(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtModuleEthPortFlowControlCreate((AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth), self);
    }

static AtEthFlowControl FlowControlGet(AtEthPort self)
    {
    if (self->flowControl == NULL)
        self->flowControl = FlowControlCreate(self);

    return self->flowControl;
    }

static eAtModuleEthRet DicEnable(AtEthPort self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtError;
    }

static eBool DicIsEnabled(AtEthPort self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }
    
static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }
    
static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }
    
static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
	AtUnused(self);
    if ((loopbackMode == cAtLoopbackModeLocal)  ||
        (loopbackMode == cAtLoopbackModeRemote) ||
        (loopbackMode == cAtLoopbackModeRelease))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsFlowControlCounter(AtChannel self, uint16 counterType)
    {
	AtUnused(self);
    if ((counterType == cAtEthPortCounterTxPausePackets)    ||
        (counterType == cAtEthPortCounterRxPausePackets)    ||
        (counterType == cAtEthPortCounterRxErrPausePackets))
        return cAtTrue;
    return cAtFalse;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    return AtEthPortCounterIsSupported(self, counterType);
    }

eBool AtEthPortCounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (IsFlowControlCounter(self, counterType))
        return AtEthPortFlowControlIsEnabled((AtEthPort)self);
    return cAtTrue;
    }

static eAtRet LedStateSet(AtEthPort self, eAtLedState ledState)
    {
	AtUnused(ledState);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtLedState LedStateGet(AtEthPort self)
    {
	AtUnused(self);
    return cAtLedStateOff;
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtEthPortCache);
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static void CacheSourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache && address)
        AtOsalMemCpy(cache->sourceMac, address, sizeof(cache->sourceMac));
    }

static eAtRet CacheSourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache && address)
        AtOsalMemCpy(address, cache->sourceMac, sizeof(cache->sourceMac));
    return cAtOk;
    }

static void CacheDestMacAddressSet(AtEthPort self, uint8 *address)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache && address)
        AtOsalMemCpy(cache->destMac, address, sizeof(cache->destMac));
    }

static eAtRet CacheDestMacAddressGet(AtEthPort self, uint8 *address)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache && address)
        AtOsalMemCpy(address, cache->destMac, sizeof(cache->destMac));
    return cAtOk;
    }

static void CacheMacCheckingEnable(AtEthPort self, eBool enable)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->macCheckingEnabled = enable;
    }

static eBool CacheMacCheckingIsEnabled(AtEthPort self)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    return (eBool)(cache ? cache->macCheckingEnabled : cAtFalse);
    }

static void CacheTxIpgSet(AtEthPort self, uint8 txIpg)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->txIpg = txIpg;
    }

static uint8 CacheTxIpgGet(AtEthPort self)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->txIpg : 0);
    }

static void CacheRxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rxIpg = rxIpg;
    }

static uint8 CacheRxIpgGet(AtEthPort self)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->rxIpg : 0);
    }

static void CacheInterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->interface = interface;
    }

static eAtEthPortInterface CacheInterfaceGet(AtEthPort self)
    {
    tAtEthPortCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->interface : cAtEthPortInterfaceUnknown;
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    /* By default, each port will have one associated controller */
    if (serdes == AtEthPortSerdesController(self))
        return cAtOk;
    return cAtErrorNotImplemented;
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    /* By default, each port will have its associated SERDES. So sure, it is
     * receiving traffic from this associated SERDES */
    return AtEthPortSerdesController(self);
    }

static eAtModuleEthRet TxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    /* By default, each port will have one associated controller */
    if (serdes == AtEthPortSerdesController(self))
        return cAtOk;
    return cAtErrorNotImplemented;
    }

static AtSerdesController TxSerdesGet(AtEthPort self)
    {
    /* By default, each port will have its associated SERDES */
    return AtEthPortSerdesController(self);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtFalse;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eBool SerdesApsSupported(AtEthPort self)
    {
    return AtModuleEthSerdesApsSupported((AtModuleEth)AtChannelModuleGet((AtChannel)self));
    }

static uint32 QueueCurrentBandwidthInBpsGet(AtEthPort self, uint8 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);

    /* Let concrete class do */
    return 0;
    }

static AtList QueueAllPwsGet(AtEthPort self, uint8 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);

    /* Let concrete class do */
    return NULL;
    }

static uint32 MaxQueuesGet(AtEthPort self)
    {
    AtUnused(self);

    /* Let concrete class do */
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtEthPort object = (AtEthPort)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(flowControl);
    mEncodeNone(cache);
    mEncodeObjectDescription(parent);
    mEncodeObjects(subPorts, object->numCreatedSubPorts);
    }

static AtErrorGenerator TxErrorGeneratorGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator RxErrorGeneratorGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static uint32 MaxSubPortsGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet ParentPortSet(AtEthPort self, AtEthPort parent)
    {
    self->parent = parent;
    return cAtOk;
    }

static AtEthPort ParentPortGet(AtEthPort self)
    {
    return self->parent;
    }

static AtEthPort SubPortObjectCreate(AtEthPort self, uint8 subPortId)
    {
    /* Concrete should know */
    AtUnused(self);
    AtUnused(subPortId);
    return NULL;
    }

static eAtRet SubPortsSetup(AtEthPort self)
    {
    uint8 subport_i;
    uint32 numSubPorts = AtEthPortMaxSubPortsGet(self);
    uint32 memorySize;
    AtEthPort *subPorts;

    /* Allocate memory for all of subports */
    memorySize = sizeof(AtEthPort) * numSubPorts;
    subPorts = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(subPorts, 0, memorySize);
    if (subPorts == NULL)
        return cAtErrorRsrcNoAvail;

    /* Create all of subports */
    for (subport_i = 0; subport_i < numSubPorts; subport_i++)
        {
        uint32 createdPort_i;

        subPorts[subport_i] = mMethodsGet(self)->SubPortObjectCreate(self, subport_i);
        if (subPorts[subport_i])
            {
            AtEthPortParentPortSet(subPorts[subport_i], self);
            continue;
            }

        /* Cleanup on failure */
        for (createdPort_i = 0; createdPort_i < subport_i; createdPort_i++)
            AtObjectDelete((AtObject)subPorts[subport_i]);
        AtOsalMemFree(subPorts);
        return cAtErrorRsrcNoAvail;
        }

    self->subPorts = subPorts;
    self->numCreatedSubPorts = numSubPorts;

    return cAtOk;
    }

static eAtRet SubPortsCleanup(AtEthPort self)
    {
    uint8 subport_i;

    /* Nothing to cleanup */
    if (self->subPorts == NULL)
        return cAtOk;

    /* Delete all of subports */
    for (subport_i = 0; subport_i < self->numCreatedSubPorts; subport_i++)
        AtObjectDelete((AtObject)self->subPorts[subport_i]);

    /* And memory that holds them */
    AtOsalMemFree(self->subPorts);
    self->subPorts = NULL;

    return cAtOk;
    }

static AtEthPort SubPortGet(AtEthPort self, uint32 subPortId)
    {
    if (subPortId >= AtEthPortMaxSubPortsGet(self))
        return NULL;

    /* Sub ports have not been setup yet */
    if (mThis(self)->subPorts == NULL)
        {
        eAtRet ret = SubPortsSetup(self);
        if (ret != cAtOk)
            return NULL;
        }

    /* There may be cases that internal implementation create less than
     * max number of subports. So, need the following check */
    if (subPortId >= mThis(self)->numCreatedSubPorts)
        return NULL;

    return mThis(self)->subPorts[subPortId];
    }

static const char *SubPortIdString(AtChannel self)
    {
    static char idString[32];
    AtChannel parent = (AtChannel)AtEthPortParentPortGet((AtEthPort)self);
    uint32 laneId = AtChannelIdGet(self);
    AtSnprintf(idString, sizeof(idString), "%d.%d", AtChannelIdGet(parent) + 1, laneId + 1);
    return idString;
    }

static const char *IdString(AtChannel self)
    {
    if (AtEthPortParentPortGet((AtEthPort)self))
        return SubPortIdString(self);
    return m_AtChannelMethods->IdString(self);
    }

static eAtRet AllSubPortsInit(AtEthPort self)
    {
    uint32 numSubPorts = AtEthPortMaxSubPortsGet(self);
    uint32 subPort_i;
    eAtRet ret = cAtOk;

    for (subPort_i = 0; subPort_i < numSubPorts; subPort_i++)
        {
        AtChannel subPort = (AtChannel)AtEthPortSubPortGet(self, subPort_i);
        ret |= AtChannelInit(subPort);
        }

    return ret;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->flowControl);
    mThis(self)->flowControl = NULL;
    SubPortsCleanup((AtEthPort)self);
    m_AtObjectMethods->Delete(self);
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortLinkStatusNotSupported;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    if (alarmType & AtChannelSupportedInterruptMasks(self))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 SimulationDatabaseSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtEthPortSimulationDb);
    }

static eBool HasMacFunctionality(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ExpectedCVlanIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ExpectedCVlanIdSet(AtEthPort self, uint16 vlanId)
    {
    AtUnused(self);
    AtUnused(vlanId);
    return cAtErrorModeNotSupport;
    }

static uint16 ExpectedCVlanIdGet(AtEthPort self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet DropPacketConditionMaskSet(AtEthPort self, uint32 conditionMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(conditionMask);
    AtUnused(enableMask);
    return cAtErrorModeNotSupport;
    }

static uint32 DropPacketConditionMaskGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortDropConditionNone;
    }

static uint32 *CounterField(AtEthPort self, uint16 counterType, tAtEthPortCounters *counters)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets               : return &(counters->txPackets);
        case cAtEthPortCounterTxBytes                 : return &(counters->txBytes);
        case cAtEthPortCounterTxGoodPackets           : return &(counters->txGoodPackets);
        case cAtEthPortCounterTxGoodBytes             : return &(counters->txGoodBytes);
        case cAtEthPortCounterTxOamPackets            : return &(counters->txOamPackets);
        case cAtEthPortCounterTxPeriodOamPackets      : return &(counters->txPeriodOamPackets);
        case cAtEthPortCounterTxPwPackets             : return &(counters->txPwPackets);
        case cAtEthPortCounterTxPacketsLen0_64        : return &(counters->txPacketsLen0_64);
        case cAtEthPortCounterTxPacketsLen65_127      : return &(counters->txPacketsLen65_127);
        case cAtEthPortCounterTxPacketsLen128_255     : return &(counters->txPacketsLen128_255);
        case cAtEthPortCounterTxPacketsLen256_511     : return &(counters->txPacketsLen256_511);
        case cAtEthPortCounterTxPacketsLen512_1024    : return &(counters->txPacketsLen512_1024);
        case cAtEthPortCounterTxPacketsLen1025_1528   : return &(counters->txPacketsLen1025_1528);
        case cAtEthPortCounterTxPacketsLen1529_2047   : return &(counters->txPacketsLen1529_2047);
        case cAtEthPortCounterTxPacketsJumbo          : return &(counters->txPacketsJumbo);
        case cAtEthPortCounterTxTopPackets            : return &(counters->txTopPackets);
        case cAtEthPortCounterTxPausePackets          : return &(counters->txPausePackets);
        case cAtEthPortCounterTxBrdCastPackets        : return &(counters->txBrdCastPackets);
        case cAtEthPortCounterTxMultCastPackets       : return &(counters->txMultCastPackets);
        case cAtEthPortCounterTxUniCastPackets        : return &(counters->txUniCastPackets);
        case cAtEthPortCounterTxOversizePackets       : return &(counters->txOversizePackets);
        case cAtEthPortCounterTxUndersizePackets      : return &(counters->txUndersizePackets);
        case cAtEthPortCounterTxDiscardedPackets      : return &(counters->txDiscardedPackets);
        case cAtEthPortCounterTxOverFlowDroppedPackets: return &(counters->txOverFlowDroppedPackets);
        case cAtEthPortCounterRxPackets               : return &(counters->rxPackets);
        case cAtEthPortCounterRxBytes                 : return &(counters->rxBytes);
        case cAtEthPortCounterRxGoodPackets           : return &(counters->rxGoodPackets);
        case cAtEthPortCounterRxGoodBytes             : return &(counters->rxGoodBytes);
        case cAtEthPortCounterRxErrEthHdrPackets      : return &(counters->rxErrEthHdrPackets);
        case cAtEthPortCounterRxErrBusPackets         : return &(counters->rxErrBusPackets);
        case cAtEthPortCounterRxErrFcsPackets         : return &(counters->rxErrFcsPackets);
        case cAtEthPortCounterRxOversizePackets       : return &(counters->rxOversizePackets);
        case cAtEthPortCounterRxUndersizePackets      : return &(counters->rxUndersizePackets);
        case cAtEthPortCounterRxPacketsLen0_64        : return &(counters->rxPacketsLen0_64);
        case cAtEthPortCounterRxPacketsLen65_127      : return &(counters->rxPacketsLen65_127);
        case cAtEthPortCounterRxPacketsLen128_255     : return &(counters->rxPacketsLen128_255);
        case cAtEthPortCounterRxPacketsLen256_511     : return &(counters->rxPacketsLen256_511);
        case cAtEthPortCounterRxPacketsLen512_1024    : return &(counters->rxPacketsLen512_1024);
        case cAtEthPortCounterRxPacketsLen1025_1528   : return &(counters->rxPacketsLen1025_1528);
        case cAtEthPortCounterRxPacketsLen1529_2047   : return &(counters->rxPacketsLen1529_2047);
        case cAtEthPortCounterRxPacketsJumbo          : return &(counters->rxPacketsJumbo);
        case cAtEthPortCounterRxPwUnsupportedPackets  : return &(counters->rxPwUnsupportedPackets);
        case cAtEthPortCounterRxErrPwLabelPackets     : return &(counters->rxErrPwLabelPackets);
        case cAtEthPortCounterRxDiscardedPackets      : return &(counters->rxDiscardedPackets);
        case cAtEthPortCounterRxPausePackets          : return &(counters->rxPausePackets);
        case cAtEthPortCounterRxErrPausePackets       : return &(counters->rxErrPausePackets);
        case cAtEthPortCounterRxUniCastPackets        : return &(counters->rxUniCastPackets);
        case cAtEthPortCounterRxBrdCastPackets        : return &(counters->rxBrdCastPackets);
        case cAtEthPortCounterRxMultCastPackets       : return &(counters->rxMultCastPackets);
        case cAtEthPortCounterRxArpPackets            : return &(counters->rxArpPackets);
        case cAtEthPortCounterRxOamPackets            : return &(counters->rxOamPackets);
        case cAtEthPortCounterRxEfmOamPackets         : return &(counters->rxEfmOamPackets);
        case cAtEthPortCounterRxErrEfmOamPackets      : return &(counters->rxErrEfmOamPackets);
        case cAtEthPortCounterRxEthOamType1Packets    : return &(counters->rxEthOamType1Packets);
        case cAtEthPortCounterRxEthOamType2Packets    : return &(counters->rxEthOamType2Packets);
        case cAtEthPortCounterRxIpv4Packets           : return &(counters->rxIpv4Packets);
        case cAtEthPortCounterRxErrIpv4Packets        : return &(counters->rxErrIpv4Packets);
        case cAtEthPortCounterRxIcmpIpv4Packets       : return &(counters->rxIcmpIpv4Packets);
        case cAtEthPortCounterRxIpv6Packets           : return &(counters->rxIpv6Packets);
        case cAtEthPortCounterRxErrIpv6Packets        : return &(counters->rxErrIpv6Packets);
        case cAtEthPortCounterRxIcmpIpv6Packets       : return &(counters->rxIcmpIpv6Packets);
        case cAtEthPortCounterRxMefPackets            : return &(counters->rxMefPackets);
        case cAtEthPortCounterRxErrMefPackets         : return &(counters->rxErrMefPackets);
        case cAtEthPortCounterRxMplsPackets           : return &(counters->rxMplsPackets);
        case cAtEthPortCounterRxErrMplsPackets        : return &(counters->rxErrMplsPackets);
        case cAtEthPortCounterRxMplsErrOuterLblPackets: return &(counters->rxMplsErrOuterLblPackets);
        case cAtEthPortCounterRxMplsDataPackets       : return &(counters->rxMplsDataPackets);
        case cAtEthPortCounterRxLdpIpv4Packets        : return &(counters->rxLdpIpv4Packets);
        case cAtEthPortCounterRxLdpIpv6Packets        : return &(counters->rxLdpIpv6Packets);
        case cAtEthPortCounterRxMplsIpv4Packets       : return &(counters->rxMplsIpv4Packets);
        case cAtEthPortCounterRxMplsIpv6Packets       : return &(counters->rxMplsIpv6Packets);
        case cAtEthPortCounterRxErrL2tpv3Packets      : return &(counters->rxErrL2tpv3Packets);
        case cAtEthPortCounterRxL2tpv3Packets         : return &(counters->rxL2tpv3Packets);
        case cAtEthPortCounterRxL2tpv3Ipv4Packets     : return &(counters->rxL2tpv3Ipv4Packets);
        case cAtEthPortCounterRxL2tpv3Ipv6Packets     : return &(counters->rxL2tpv3Ipv6Packets);
        case cAtEthPortCounterRxUdpPackets            : return &(counters->rxUdpPackets);
        case cAtEthPortCounterRxErrUdpPackets         : return &(counters->rxErrUdpPackets);
        case cAtEthPortCounterRxUdpIpv4Packets        : return &(counters->rxUdpIpv4Packets);
        case cAtEthPortCounterRxUdpIpv6Packets        : return &(counters->rxUdpIpv6Packets);
        case cAtEthPortCounterRxErrPsnPackets         : return &(counters->rxErrPsnPackets);
        case cAtEthPortCounterRxPacketsSendToCpu      : return &(counters->rxPacketsSendToCpu);
        case cAtEthPortCounterRxPacketsSendToPw       : return &(counters->rxPacketsSendToPw);
        case cAtEthPortCounterRxTopPackets            : return &(counters->rxTopPackets);
        case cAtEthPortCounterRxPacketDaMis           : return &(counters->rxPacketDaMis);
        case cAtEthPortCounterRxPhysicalError         : return &(counters->rxPhysicalError);
        case cAtEthPortCounterRxBip8Errors            : return &(counters->rxBip8Errors);
        case cAtEthPortCounterRxOverFlowDroppedPackets: return &(counters->rxOverFlowDroppedPackets);
        case cAtEthPortCounterRxFragmentPackets       : return &(counters->rxFragmentPackets);
        case cAtEthPortCounterRxJabberPackets         : return &(counters->rxJabberPackets);
        case cAtEthPortCounterRxLoopDaPackets         : return &(counters->rxLoopDaPackets);
        case cAtEthPortCounterRxPcsErrorPackets       : return &(counters->rxPcsErrorPackets);
        case cAtEthPortCounterRxPcsInvalidCount       : return &(counters->rxPcsInvalidCount);
        case cAtEthPortCounterTxPacketsLen64          : return &(counters->txPacketsLen64);
        case cAtEthPortCounterTxPacketsLen512_1023    : return &(counters->txPacketsLen512_1023);
        case cAtEthPortCounterTxPacketsLen1024_1518   : return &(counters->txPacketsLen1024_1518);
        case cAtEthPortCounterTxPacketsLen1519_1522   : return &(counters->txPacketsLen1519_1522);
        case cAtEthPortCounterTxPacketsLen1523_1548   : return &(counters->txPacketsLen1523_1548);
        case cAtEthPortCounterTxPacketsLen1549_2047   : return &(counters->txPacketsLen1549_2047);
        case cAtEthPortCounterTxPacketsLen2048_4095   : return &(counters->txPacketsLen2048_4095);
        case cAtEthPortCounterTxPacketsLen4096_8191   : return &(counters->txPacketsLen4096_8191);
        case cAtEthPortCounterTxPacketsLen8192_9215   : return &(counters->txPacketsLen8192_9215);
        case cAtEthPortCounterTxPacketsLarge          : return &(counters->txPacketsLarge);
        case cAtEthPortCounterTxPacketsSmall          : return &(counters->txPacketsSmall);
        case cAtEthPortCounterTxErrFcsPackets         : return &(counters->txErrFcsPackets);
        case cAtEthPortCounterTxErrPackets            : return &(counters->txErrPackets);
        case cAtEthPortCounterTxVlanPackets           : return &(counters->txVlanPackets);
        case cAtEthPortCounterTxUserPausePackets      : return &(counters->txUserPausePackets);
        case cAtEthPortCounterRxPacketsLen64          : return &(counters->rxPacketsLen64);
        case cAtEthPortCounterRxPacketsLen512_1023    : return &(counters->rxPacketsLen512_1023);
        case cAtEthPortCounterRxPacketsLen1024_1518   : return &(counters->rxPacketsLen1024_1518);
        case cAtEthPortCounterRxPacketsLen1519_1522   : return &(counters->rxPacketsLen1519_1522);
        case cAtEthPortCounterRxPacketsLen1523_1548   : return &(counters->rxPacketsLen1523_1548);
        case cAtEthPortCounterRxPacketsLen1549_2047   : return &(counters->rxPacketsLen1549_2047);
        case cAtEthPortCounterRxPacketsLen2048_4095   : return &(counters->rxPacketsLen2048_4095);
        case cAtEthPortCounterRxPacketsLen4096_8191   : return &(counters->rxPacketsLen4096_8191);
        case cAtEthPortCounterRxPacketsLen8192_9215   : return &(counters->rxPacketsLen8192_9215);
        case cAtEthPortCounterRxPacketsLarge          : return &(counters->rxPacketsLarge);
        case cAtEthPortCounterRxPacketsSmall          : return &(counters->rxPacketsSmall);
        case cAtEthPortCounterRxErrPackets            : return &(counters->rxErrPackets);
        case cAtEthPortCounterRxVlanPackets           : return &(counters->rxVlanPackets);
        case cAtEthPortCounterRxUserPausePackets      : return &(counters->rxUserPausePackets);
        case cAtEthPortCounterRxPacketsTooLong        : return &(counters->rxPacketsTooLong);
        case cAtEthPortCounterRxStompedFcsPackets     : return &(counters->rxStompedFcsPackets);
        case cAtEthPortCounterRxInRangeErrPackets     : return &(counters->rxInRangeErrPackets);
        case cAtEthPortCounterRxTruncatedPackets      : return &(counters->rxTruncatedPackets);
        case cAtEthPortCounterRxFecIncCorrectCount    : return &(counters->rxFecIncCorrectCount);
        case cAtEthPortCounterRxFecIncCantCorrectCount: return &(counters->rxFecIncCantCorrectCount);
        case cAtEthPortCounterRxFecLockErrorCount     : return &(counters->rxFecLockErrorCount);
        default:
            return NULL;
        }
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool clear)
    {
    uint32 value;
    tAtEthPortCounters *counters;
    uint32 (*CounterGetFunc)(AtChannel, uint16) = AtChannelCounterGet;

    /* Initialize counters */
    counters = (tAtEthPortCounters *)pAllCounters;
    if (counters)
        AtOsalMemInit(counters, 0, sizeof(tAtEthPortCounters));

    /* Put all of supported counters */
    AtChannelAllCountersLatchAndClear(self, clear);

    if (clear)
        CounterGetFunc = AtChannelCounterClear;

    mPutCounter(txPackets, cAtEthPortCounterTxPackets);
    mPutCounter(txBytes, cAtEthPortCounterTxBytes);
    mPutCounter(txGoodPackets, cAtEthPortCounterTxGoodPackets);
    mPutCounter(txGoodBytes, cAtEthPortCounterTxGoodBytes);
    mPutCounter(txOamPackets, cAtEthPortCounterTxOamPackets);
    mPutCounter(txPeriodOamPackets, cAtEthPortCounterTxPeriodOamPackets);
    mPutCounter(txPwPackets, cAtEthPortCounterTxPwPackets);
    mPutCounter(txPacketsLen0_64, cAtEthPortCounterTxPacketsLen0_64);
    mPutCounter(txPacketsLen65_127, cAtEthPortCounterTxPacketsLen65_127);
    mPutCounter(txPacketsLen128_255, cAtEthPortCounterTxPacketsLen128_255);
    mPutCounter(txPacketsLen256_511, cAtEthPortCounterTxPacketsLen256_511);
    mPutCounter(txPacketsLen512_1024, cAtEthPortCounterTxPacketsLen512_1024);
    mPutCounter(txPacketsLen1025_1528, cAtEthPortCounterTxPacketsLen1025_1528);
    mPutCounter(txPacketsLen1529_2047, cAtEthPortCounterTxPacketsLen1529_2047);
    mPutCounter(txPacketsJumbo, cAtEthPortCounterTxPacketsJumbo);
    mPutCounter(txTopPackets, cAtEthPortCounterTxTopPackets);
    mPutCounter(txPausePackets, cAtEthPortCounterTxPausePackets);
    mPutCounter(txBrdCastPackets, cAtEthPortCounterTxBrdCastPackets);
    mPutCounter(txMultCastPackets, cAtEthPortCounterTxMultCastPackets);
    mPutCounter(txUniCastPackets, cAtEthPortCounterTxUniCastPackets);
    mPutCounter(txOversizePackets, cAtEthPortCounterTxOversizePackets);
    mPutCounter(txUndersizePackets, cAtEthPortCounterTxUndersizePackets);
    mPutCounter(txDiscardedPackets, cAtEthPortCounterTxDiscardedPackets);
    mPutCounter(txOverFlowDroppedPackets, cAtEthPortCounterTxOverFlowDroppedPackets);
    mPutCounter(rxPackets, cAtEthPortCounterRxPackets);
    mPutCounter(rxBytes, cAtEthPortCounterRxBytes);
    mPutCounter(rxGoodPackets, cAtEthPortCounterRxGoodPackets);
    mPutCounter(rxGoodBytes, cAtEthPortCounterRxGoodBytes);
    mPutCounter(rxErrEthHdrPackets, cAtEthPortCounterRxErrEthHdrPackets);
    mPutCounter(rxErrBusPackets, cAtEthPortCounterRxErrBusPackets);
    mPutCounter(rxErrFcsPackets, cAtEthPortCounterRxErrFcsPackets);
    mPutCounter(rxOversizePackets, cAtEthPortCounterRxOversizePackets);
    mPutCounter(rxUndersizePackets, cAtEthPortCounterRxUndersizePackets);
    mPutCounter(rxPacketsLen0_64, cAtEthPortCounterRxPacketsLen0_64);
    mPutCounter(rxPacketsLen65_127, cAtEthPortCounterRxPacketsLen65_127);
    mPutCounter(rxPacketsLen128_255, cAtEthPortCounterRxPacketsLen128_255);
    mPutCounter(rxPacketsLen256_511, cAtEthPortCounterRxPacketsLen256_511);
    mPutCounter(rxPacketsLen512_1024, cAtEthPortCounterRxPacketsLen512_1024);
    mPutCounter(rxPacketsLen1025_1528, cAtEthPortCounterRxPacketsLen1025_1528);
    mPutCounter(rxPacketsLen1529_2047, cAtEthPortCounterRxPacketsLen1529_2047);
    mPutCounter(rxPacketsJumbo, cAtEthPortCounterRxPacketsJumbo);
    mPutCounter(rxPwUnsupportedPackets, cAtEthPortCounterRxPwUnsupportedPackets);
    mPutCounter(rxErrPwLabelPackets, cAtEthPortCounterRxErrPwLabelPackets);
    mPutCounter(rxDiscardedPackets, cAtEthPortCounterRxDiscardedPackets);
    mPutCounter(rxPausePackets, cAtEthPortCounterRxPausePackets);
    mPutCounter(rxErrPausePackets, cAtEthPortCounterRxErrPausePackets);
    mPutCounter(rxUniCastPackets, cAtEthPortCounterRxUniCastPackets);
    mPutCounter(rxBrdCastPackets, cAtEthPortCounterRxBrdCastPackets);
    mPutCounter(rxMultCastPackets, cAtEthPortCounterRxMultCastPackets);
    mPutCounter(rxArpPackets, cAtEthPortCounterRxArpPackets);
    mPutCounter(rxOamPackets, cAtEthPortCounterRxOamPackets);
    mPutCounter(rxEfmOamPackets, cAtEthPortCounterRxEfmOamPackets);
    mPutCounter(rxErrEfmOamPackets, cAtEthPortCounterRxErrEfmOamPackets);
    mPutCounter(rxEthOamType1Packets, cAtEthPortCounterRxEthOamType1Packets);
    mPutCounter(rxEthOamType2Packets, cAtEthPortCounterRxEthOamType2Packets);
    mPutCounter(rxIpv4Packets, cAtEthPortCounterRxIpv4Packets);
    mPutCounter(rxErrIpv4Packets, cAtEthPortCounterRxErrIpv4Packets);
    mPutCounter(rxIcmpIpv4Packets, cAtEthPortCounterRxIcmpIpv4Packets);
    mPutCounter(rxIpv6Packets, cAtEthPortCounterRxIpv6Packets);
    mPutCounter(rxErrIpv6Packets, cAtEthPortCounterRxErrIpv6Packets);
    mPutCounter(rxIcmpIpv6Packets, cAtEthPortCounterRxIcmpIpv6Packets);
    mPutCounter(rxMefPackets, cAtEthPortCounterRxMefPackets);
    mPutCounter(rxErrMefPackets, cAtEthPortCounterRxErrMefPackets);
    mPutCounter(rxMplsPackets, cAtEthPortCounterRxMplsPackets);
    mPutCounter(rxErrMplsPackets, cAtEthPortCounterRxErrMplsPackets);
    mPutCounter(rxMplsErrOuterLblPackets, cAtEthPortCounterRxMplsErrOuterLblPackets);
    mPutCounter(rxMplsDataPackets, cAtEthPortCounterRxMplsDataPackets);
    mPutCounter(rxLdpIpv4Packets, cAtEthPortCounterRxLdpIpv4Packets);
    mPutCounter(rxLdpIpv6Packets, cAtEthPortCounterRxLdpIpv6Packets);
    mPutCounter(rxMplsIpv4Packets, cAtEthPortCounterRxMplsIpv4Packets);
    mPutCounter(rxMplsIpv6Packets, cAtEthPortCounterRxMplsIpv6Packets);
    mPutCounter(rxErrL2tpv3Packets, cAtEthPortCounterRxErrL2tpv3Packets);
    mPutCounter(rxL2tpv3Packets, cAtEthPortCounterRxL2tpv3Packets);
    mPutCounter(rxL2tpv3Ipv4Packets, cAtEthPortCounterRxL2tpv3Ipv4Packets);
    mPutCounter(rxL2tpv3Ipv6Packets, cAtEthPortCounterRxL2tpv3Ipv6Packets);
    mPutCounter(rxUdpPackets, cAtEthPortCounterRxUdpPackets);
    mPutCounter(rxErrUdpPackets, cAtEthPortCounterRxErrUdpPackets);
    mPutCounter(rxUdpIpv4Packets, cAtEthPortCounterRxUdpIpv4Packets);
    mPutCounter(rxUdpIpv6Packets, cAtEthPortCounterRxUdpIpv6Packets);
    mPutCounter(rxErrPsnPackets, cAtEthPortCounterRxErrPsnPackets);
    mPutCounter(rxPacketsSendToCpu, cAtEthPortCounterRxPacketsSendToCpu);
    mPutCounter(rxPacketsSendToPw, cAtEthPortCounterRxPacketsSendToPw);
    mPutCounter(rxTopPackets, cAtEthPortCounterRxTopPackets);
    mPutCounter(rxPacketDaMis, cAtEthPortCounterRxPacketDaMis);
    mPutCounter(rxPhysicalError, cAtEthPortCounterRxPhysicalError);
    mPutCounter(rxBip8Errors, cAtEthPortCounterRxBip8Errors);
    mPutCounter(rxOverFlowDroppedPackets, cAtEthPortCounterRxOverFlowDroppedPackets);
    mPutCounter(rxFragmentPackets, cAtEthPortCounterRxFragmentPackets);
    mPutCounter(rxJabberPackets, cAtEthPortCounterRxJabberPackets);
    mPutCounter(rxLoopDaPackets, cAtEthPortCounterRxLoopDaPackets);
    mPutCounter(rxPcsErrorPackets, cAtEthPortCounterRxPcsErrorPackets);
    mPutCounter(rxPcsInvalidCount, cAtEthPortCounterRxPcsInvalidCount);
    mPutCounter(txPacketsLen64, cAtEthPortCounterTxPacketsLen64);
    mPutCounter(txPacketsLen512_1023, cAtEthPortCounterTxPacketsLen512_1023);
    mPutCounter(txPacketsLen1024_1518, cAtEthPortCounterTxPacketsLen1024_1518);
    mPutCounter(txPacketsLen1519_1522, cAtEthPortCounterTxPacketsLen1519_1522);
    mPutCounter(txPacketsLen1523_1548, cAtEthPortCounterTxPacketsLen1523_1548);
    mPutCounter(txPacketsLen1549_2047, cAtEthPortCounterTxPacketsLen1549_2047);
    mPutCounter(txPacketsLen2048_4095, cAtEthPortCounterTxPacketsLen2048_4095);
    mPutCounter(txPacketsLen4096_8191, cAtEthPortCounterTxPacketsLen4096_8191);
    mPutCounter(txPacketsLen8192_9215, cAtEthPortCounterTxPacketsLen8192_9215);
    mPutCounter(txPacketsLarge, cAtEthPortCounterTxPacketsLarge);
    mPutCounter(txPacketsSmall, cAtEthPortCounterTxPacketsSmall);
    mPutCounter(txErrFcsPackets, cAtEthPortCounterTxErrFcsPackets);
    mPutCounter(txErrPackets, cAtEthPortCounterTxErrPackets);
    mPutCounter(txVlanPackets, cAtEthPortCounterTxVlanPackets);
    mPutCounter(txUserPausePackets, cAtEthPortCounterTxUserPausePackets);
    mPutCounter(rxPacketsLen64, cAtEthPortCounterRxPacketsLen64);
    mPutCounter(rxPacketsLen512_1023, cAtEthPortCounterRxPacketsLen512_1023);
    mPutCounter(rxPacketsLen1024_1518, cAtEthPortCounterRxPacketsLen1024_1518);
    mPutCounter(rxPacketsLen1519_1522, cAtEthPortCounterRxPacketsLen1519_1522);
    mPutCounter(rxPacketsLen1523_1548, cAtEthPortCounterRxPacketsLen1523_1548);
    mPutCounter(rxPacketsLen1549_2047, cAtEthPortCounterRxPacketsLen1549_2047);
    mPutCounter(rxPacketsLen2048_4095, cAtEthPortCounterRxPacketsLen2048_4095);
    mPutCounter(rxPacketsLen4096_8191, cAtEthPortCounterRxPacketsLen4096_8191);
    mPutCounter(rxPacketsLen8192_9215, cAtEthPortCounterRxPacketsLen8192_9215);
    mPutCounter(rxPacketsLarge, cAtEthPortCounterRxPacketsLarge);
    mPutCounter(rxPacketsSmall, cAtEthPortCounterRxPacketsSmall);
    mPutCounter(rxErrPackets, cAtEthPortCounterRxErrPackets);
    mPutCounter(rxVlanPackets, cAtEthPortCounterRxVlanPackets);
    mPutCounter(rxUserPausePackets, cAtEthPortCounterRxUserPausePackets);
    mPutCounter(rxPacketsTooLong, cAtEthPortCounterRxPacketsTooLong);
    mPutCounter(rxStompedFcsPackets, cAtEthPortCounterRxStompedFcsPackets);
    mPutCounter(rxInRangeErrPackets, cAtEthPortCounterRxInRangeErrPackets);
    mPutCounter(rxTruncatedPackets, cAtEthPortCounterRxTruncatedPackets);
    mPutCounter(rxFecIncCorrectCount, cAtEthPortCounterRxFecIncCorrectCount);
    mPutCounter(rxFecIncCantCorrectCount, cAtEthPortCounterRxFecIncCantCorrectCount);
    mPutCounter(rxFecLockErrorCount, cAtEthPortCounterRxFecLockErrorCount);

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static void StatusClear(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint8 subPort_i;

    /* All of sub ports */
    for (subPort_i = 0; subPort_i < AtEthPortMaxSubPortsGet(port); subPort_i++)
        {
        AtEthPort subPort = AtEthPortSubPortGet(port, subPort_i);
        AtChannelStatusClear((AtChannel)subPort);
        }

    /* And status of itself */
    m_AtChannelMethods->StatusClear(self);
    }

static eAtEthPortRemoteFault RemoteFaultGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortRemoteFaultNotSupported;
    }

static eAtRet DisparityForce(AtEthPort self, eBool forced)
    {
    AtUnused(self);
    return forced ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool DisparityIsForced(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool DisparityCanForce(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RxFecEnable(AtEthPort self, eBool enabled)
    {
    AtUnused(self);
    return enabled ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool RxFecIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxFecEnable(AtEthPort self, eBool enabled)
    {
    AtUnused(self);
    return enabled ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool TxFecIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool FecIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ExpectedDestMacSet(AtEthPort self, const uint8 *mac)
    {
    AtUnused(self);
    AtUnused(mac);
    return cAtErrorModeNotSupport;
    }

static eAtRet ExpectedDestMacGet(AtEthPort self, uint8 *mac)
    {
    AtUnused(self);
    AtUnused(mac);
    return cAtErrorModeNotSupport;
    }

static eBool HasExpectedDestMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool SohTransparentIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanSelectNullSerdes(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet K30_7Force(AtEthPort self, eBool forced)
    {
    AtUnused(self);
    return forced ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool K30_7IsForced(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool K30_7CanForce(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtClockMonitor ClockMonitorGet(AtEthPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtEthPortSharedQuerier();
    }

static eBool HasSourceMac(AtEthPort self)
    {
    /* Let's concrete determine. Just return true to not affect anything */
    AtUnused(self);
    return cAtTrue;
    }

static eBool MacCheckingCanEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static uint8 TxDefaultIpg(AtEthPort self)
    {
    return AtEthPortDefaultIpg(self);
    }

static eBool IsBackplaneMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool TxIpgIsInRange(AtEthPort self, uint8 ipg)
    {
    AtUnused(self);
    if ((ipg < 4) || (ipg > 31))
        return cAtFalse;

    return cAtTrue;
    }

static eBool RxIpgIsInRange(AtEthPort self, uint8 rxIpg)
    {
    AtUnused(self);
    if ((rxIpg < 4) || (rxIpg > 15))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet ExpectedCVlanSet(AtEthPort self, const tAtVlan *cVlan)
    {
    /* Let's concrete determine */
    AtUnused(self);
    AtUnused(cVlan);
    return cAtErrorModeNotSupport;
    }

static tAtVlan *ExpectedCVlanGet(AtEthPort self, tAtVlan *cVlan)
    {
    /* Let's concrete determine */
    AtUnused(self);
    AtUnused(cVlan);
    return NULL;
    }

static eAtRet TxCVlanSet(AtEthPort self, const tAtVlan *cVlan)
    {
    /* Let's concrete determine */
    AtUnused(self);
    AtUnused(cVlan);
    return cAtErrorModeNotSupport;
    }

static tAtVlan *TxCVlanGet(AtEthPort self, tAtVlan *cVlan)
    {
    /* Let's concrete determine */
    AtUnused(self);
    AtUnused(cVlan);
    return NULL;
    }

static eBool HasCVlan(AtEthPort self)
    {
    /* Let's concrete determine */
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet LinkTrainingEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return (enable) ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool LinkTrainingIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet LinkTrainingRestart(AtEthPort self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool LinkTrainingIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, DestMacAddressSet);
        mMethodOverride(m_methods, DestMacAddressGet);
        mMethodOverride(m_methods, HasDestMac);
        mMethodOverride(m_methods, SourceMacAddressSet);
        mMethodOverride(m_methods, SourceMacAddressGet);
        mMethodOverride(m_methods, HasSourceMac);
        mMethodOverride(m_methods, MacCheckingEnable);
        mMethodOverride(m_methods, MacCheckingIsEnabled);
        mMethodOverride(m_methods, MacCheckingCanEnable);
        mMethodOverride(m_methods, SpeedIsSupported);
        mMethodOverride(m_methods, SpeedSet);
        mMethodOverride(m_methods, SpeedGet);
        mMethodOverride(m_methods, DuplexModeSet);
        mMethodOverride(m_methods, DuplexModeGet);
        mMethodOverride(m_methods, TxIpgSet);
        mMethodOverride(m_methods, TxIpgGet);
        mMethodOverride(m_methods, TxIpgIsConfigurable);
        mMethodOverride(m_methods, TxIpgIsInRange);
        mMethodOverride(m_methods, RxIpgSet);
        mMethodOverride(m_methods, RxIpgGet);
        mMethodOverride(m_methods, RxIpgIsConfigurable);
        mMethodOverride(m_methods, RxIpgIsInRange);
        mMethodOverride(m_methods, AutoNegEnable);
        mMethodOverride(m_methods, AutoNegIsEnabled);
        mMethodOverride(m_methods, AutoNegStateGet);
        mMethodOverride(m_methods, AutoNegIsSupported);
        mMethodOverride(m_methods, AutoNegSpeedGet);
        mMethodOverride(m_methods, AutoNegDuplexModeGet);
        mMethodOverride(m_methods, AutoNegIsComplete);
        mMethodOverride(m_methods, AutoNegRestartOn);
        mMethodOverride(m_methods, AutoNegRestartIsOn);
        mMethodOverride(m_methods, InterfaceSet);
        mMethodOverride(m_methods, InterfaceGet);
        mMethodOverride(m_methods, DefaultInterface);
        mMethodOverride(m_methods, SerdesController);
        mMethodOverride(m_methods, MaxPacketSizeSet);
        mMethodOverride(m_methods, MaxPacketSizeGet);
        mMethodOverride(m_methods, MinPacketSizeSet);
        mMethodOverride(m_methods, MinPacketSizeGet);
        mMethodOverride(m_methods, Switch);
        mMethodOverride(m_methods, SwitchedPortGet);
        mMethodOverride(m_methods, SwitchRelease);
        mMethodOverride(m_methods, Bridge);
        mMethodOverride(m_methods, BridgedPortGet);
        mMethodOverride(m_methods, BridgeRelease);
        mMethodOverride(m_methods, ProvisionedBandwidthInKbpsGet);
        mMethodOverride(m_methods, RunningBandwidthInKbpsGet);
        mMethodOverride(m_methods, HiGigEnable);
        mMethodOverride(m_methods, HiGigIsEnabled);
        mMethodOverride(m_methods, DicEnable);
        mMethodOverride(m_methods, DicIsEnabled);
        mMethodOverride(m_methods, IpV4AddressSet);
        mMethodOverride(m_methods, IpV4AddressGet);
        mMethodOverride(m_methods, IpV6AddressSet);
        mMethodOverride(m_methods, IpV6AddressGet);
        mMethodOverride(m_methods, LedStateSet);
        mMethodOverride(m_methods, LedStateGet);
        mMethodOverride(m_methods, RxSerdesSelect);
        mMethodOverride(m_methods, RxSelectedSerdes);
        mMethodOverride(m_methods, CanSelectNullSerdes);
        mMethodOverride(m_methods, TxSerdesGet);
        mMethodOverride(m_methods, TxSerdesSet);
        mMethodOverride(m_methods, TxSerdesBridge);
        mMethodOverride(m_methods, TxBridgedSerdes);
        mMethodOverride(m_methods, TxSerdesCanBridge);
        mMethodOverride(m_methods, QueueCurrentBandwidthInBpsGet);
        mMethodOverride(m_methods, QueueAllPwsGet);
        mMethodOverride(m_methods, MaxQueuesGet);
        mMethodOverride(m_methods, TxErrorGeneratorGet);
        mMethodOverride(m_methods, RxErrorGeneratorGet);
        mMethodOverride(m_methods, SubPortGet);
        mMethodOverride(m_methods, MaxSubPortsGet);
        mMethodOverride(m_methods, ParentPortGet);
        mMethodOverride(m_methods, SubPortObjectCreate);
        mMethodOverride(m_methods, LinkStatus);
        mMethodOverride(m_methods, FlowControlDefaultSet);
        mMethodOverride(m_methods, HasMacFunctionality);
        mMethodOverride(m_methods, RemoteFaultGet);
        mMethodOverride(m_methods, ClockMonitorGet);
        mMethodOverride(m_methods, ClockMonitorGet);

        /* Inernal Method to set expected C-VlanId */
        mMethodOverride(m_methods, ExpectedCVlanIsSupported);
        mMethodOverride(m_methods, ExpectedCVlanIdSet);
        mMethodOverride(m_methods, ExpectedCVlanIdGet);

        /* VLAN */
        mMethodOverride(m_methods, ExpectedCVlanSet);
        mMethodOverride(m_methods, ExpectedCVlanGet);
        mMethodOverride(m_methods, TxCVlanSet);
        mMethodOverride(m_methods, TxCVlanGet);
        mMethodOverride(m_methods, HasCVlan);

        /* To control Drop Packet per port */
        mMethodOverride(m_methods, DropPacketConditionMaskSet);
        mMethodOverride(m_methods, DropPacketConditionMaskGet);

        /* Disparity forcing */
        mMethodOverride(m_methods, DisparityForce);
        mMethodOverride(m_methods, DisparityIsForced);
        mMethodOverride(m_methods, DisparityCanForce);

        /* FEC */
        mMethodOverride(m_methods, RxFecEnable);
        mMethodOverride(m_methods, RxFecIsEnabled);
        mMethodOverride(m_methods, TxFecEnable);
        mMethodOverride(m_methods, TxFecIsEnabled);
        mMethodOverride(m_methods, FecIsSupported);

        /* Some products have expected DMAC */
        mMethodOverride(m_methods, ExpectedDestMacSet);
        mMethodOverride(m_methods, ExpectedDestMacGet);
        mMethodOverride(m_methods, HasExpectedDestMac);
        mMethodOverride(m_methods, SohTransparentIsSupported);

        /* K30.7 on 1000basex */
        mMethodOverride(m_methods, K30_7Force);
        mMethodOverride(m_methods, K30_7IsForced);
        mMethodOverride(m_methods, K30_7CanForce);

        mMethodOverride(m_methods, IsBackplaneMac);
        mMethodOverride(m_methods, TxDefaultIpg);

        /* Link training */
        mMethodOverride(m_methods, LinkTrainingEnable);
        mMethodOverride(m_methods, LinkTrainingIsEnabled);
        mMethodOverride(m_methods, LinkTrainingRestart);
        mMethodOverride(m_methods, LinkTrainingIsSupported);
        }
        
    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AllConfigGet);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, CacheSize);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        mMethodOverride(m_AtChannelOverride, SimulationDatabaseSize);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

AtEthPort AtEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtEthPort));
    
    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, portId, (AtModule)module) == NULL)
        return NULL;
        
    /* Override */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;
    
    return self;
    }

eBool AtEthPortExpectedCVlanIsSupported(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->ExpectedCVlanIsSupported(self);
    return cAtFalse;
    }

eAtRet AtEthPortExpectedCVlanIdSet(AtEthPort self, uint16 vlanId)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (vlanId > cBit11_0)
        return cAtErrorOutOfRangParm;

    return mMethodsGet(self)->ExpectedCVlanIdSet(self, vlanId);
    }

uint16 AtEthPortExpectedCVlanIdGet(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->ExpectedCVlanIdGet(self);
    return 0x0;
    }

eBool AtEthPortIsBackplaneMac(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->IsBackplaneMac(self);
    return cAtFalse;
    }

/*
 * Enable/disable DIC mode
 *
 * @param self This port
 * @param enable Enable/Disable
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortDicEnable(AtEthPort self, eBool enable)
    {
    mNumericalAttributeSet(DicEnable, enable);
    }

/*
 * Check if DIC is enabled/disabled
 *
 * @param self This port
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtEthPortDicIsEnabled(AtEthPort self)
    {
    mAttributeGet(DicIsEnabled, eBool, cAtFalse);
    }

eBool AtEthPortTxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (self)
        return mMethodsGet(self)->TxSerdesCanBridge(self, serdes);
    return cAtFalse;
    }

/*
 * Enable/disable flow control
 *
 * @param self This port
 * @param enable Enable/disable
 * @return AT return code
 */
eAtModuleEthRet AtEthPortFlowControlEnable(AtEthPort self, eBool enable)
    {
    return AtEthFlowControlEnable(AtEthPortFlowControlGet(self), enable);
    }

/*
 * Get Flow control is enabled or disabled
 *
 * @param self This port
 * @return Enabled/disabled
 */
eBool AtEthPortFlowControlIsEnabled(AtEthPort self)
    {
    return AtEthFlowControlIsEnabled(AtEthPortFlowControlGet(self));
    }

/*
 * Set Pause Frame Interval
 *
 * @param self This port
 * @param interval Interval
 * @return AT return code
 */
eAtModuleEthRet AtEthPortPauseFrameIntervalSet(AtEthPort self, uint32 interval)
    {
    return AtEthFlowControlPauseFramePeriodSet(AtEthPortFlowControlGet(self), interval);
    }

/*
 * Get Pause Frame Interval
 *
 * @param self This port
 * @return Interval
 */
uint32 AtEthPortPauseFrameIntervalGet(AtEthPort self)
    {
    return (uint16)AtEthFlowControlPauseFramePeriodGet(AtEthPortFlowControlGet(self));
    }

/*
 * Set Pause Frame expire time
 *
 * @param self This port
 * @param expireTime Expire time
 * @return AT return code
 */
eAtModuleEthRet AtEthPortPauseFrameExpireTimeSet(AtEthPort self, uint32 expireTime)
    {
    return AtEthFlowControlPauseFrameQuantaSet(AtEthPortFlowControlGet(self), expireTime);
    }

/*
 * Get Pause Frame expire time
 * @param self This port
 * @return Expire time
 */
uint32 AtEthPortPauseFrameExpireTimeGet(AtEthPort self)
    {
    return (uint16)AtEthFlowControlPauseFrameQuantaGet(AtEthPortFlowControlGet(self));
    }

eBool AtEthPortTxIpgIsInRange(AtEthPort self, uint8 ipg)
    {
    if (self)
        return mMethodsGet(self)->TxIpgIsInRange(self, ipg);

    return cAtFalse;
    }

eBool AtEthPortRxIpgIsInRange(AtEthPort self, uint8 rxIpg)
    {
    if (self)
        return mMethodsGet(self)->RxIpgIsInRange(self, rxIpg);

    return cAtFalse;
    }

eAtEthPortInterface AtEthPortDefaultInterface(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->DefaultInterface(self);
    return cAtEthPortInterfaceUnknown;
    }

/*
 * Access associated eye scan controller
 *
 * @param self This port
 *
 * @return AtEyeScanController object if exist, otherwise, NULL is returned.
 */
AtEyeScanController AtEthPortEyeScanControllerGet(AtEthPort self)
    {
    AtSerdesController serdesController = AtEthPortSerdesController(self);
    if (serdesController)
        return AtSerdesControllerEyeScanControllerGet(serdesController);
    return NULL;
    }
 
eAtRet AtEthPortParentPortSet(AtEthPort self, AtEthPort parent)
    {
    if (self)
        return ParentPortSet(self, parent);
    return cAtErrorNullPointer;
    }

eAtRet AtEthPortAllSubPortsInit(AtEthPort self)
    {
    if (self)
        return AllSubPortsInit(self);
    return cAtErrorNullPointer;
    }

eBool AtEthPortHasMacFunctionality(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->HasMacFunctionality(self);
    return cAtFalse;
    }

uint32 *AtEthPortCounterField(AtEthPort self, uint16 counterType, tAtEthPortCounters *counters)
    {
    if (self && counters)
        return CounterField(self, counterType, counters);
    return NULL;
    }

eBool AtEthPortMacCheckingCanEnable(AtEthPort self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->MacCheckingCanEnable(self, enable);
    return cAtFalse;
    }

uint8 AtEthPortDefaultIpg(AtEthPort self)
    {
    AtUnused(self);
    return 10;
    }

uint8 AtEthPortTxDefaultIpg(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->TxDefaultIpg(self);
    return 0;
    }

/**
 * @addtogroup AtEthPort
 * @{
 */

/**
 * Check if this port has configurable source MAC address
 * @param self This port
 * @return cAtTrue: the port has configurable SA, cAtFalse: the port don't have configurable SA
 */
eBool AtEthPortHasSourceMac(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->HasSourceMac(self);
    return cAtFalse;
    }

/**
 * Set source MAC address
 * @param self This port
 * @param mac Pointer to input MAC address array
 * @return AT return code
 */
eAtModuleEthRet AtEthPortSourceMacAddressSet(AtEthPort self, uint8 *mac)
    {
    if (mPortIsValid(self))
        {
        eAtRet ret = cAtOk;

        if (!AtEthPortHasSourceMac(self))
            return cAtErrorModeNotSupport;

        mMacAttributeApiLogStart(mac);
        ret = mMethodsGet(self)->SourceMacAddressSet(self, mac);
        CacheSourceMacAddressSet(self, mac);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtError;
    }

/**
 * Get source MAC address
 * @param self This port
 * @param address Pointer to output MAC address array
 * @return AT return code
 */
eAtModuleEthRet AtEthPortSourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    if (!mPortIsValid(self))
        return cAtError;

    if (!AtEthPortHasSourceMac(self))
        return cAtErrorModeNotSupport;

    if (AtDriverIsStandby())
        return CacheSourceMacAddressGet(self, address);

    return mMethodsGet(self)->SourceMacAddressGet(self, address);
    }

/**
 * Set dest MAC address
 *
 * @param self This port
 * @param address Pointer to input MAC address array
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortDestMacAddressSet(AtEthPort self, uint8 *address)
    {
    eAtRet ret;

    if (!mPortIsValid(self))
        return cAtErrorNullPointer;

    if (AtEthPortHasDestMac(self) == cAtFalse)
        return cAtErrorModeNotSupport;

    mMacAttributeApiLogStart(address);
    ret = mMethodsGet(self)->DestMacAddressSet(self, address);
    CacheDestMacAddressSet(self, address);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get dest MAC address
 *
 * @param self This port
 * @param address Pointer to output MAC address array
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortDestMacAddressGet(AtEthPort self, uint8 *address)
    {
    if (!mPortIsValid(self))
        return cAtError;

    if (!AtEthPortHasDestMac(self))
        return cAtErrorModeNotSupport;

    if (mInAccessible(self))
        return CacheDestMacAddressGet(self, address);

    return mMethodsGet(self)->DestMacAddressGet(self, address);
    }

/**
 * Check if DMAC is configurable
 *
 * @param self This port
 *
 * @retval cAtTrue  supported
 * @retval cAtFalse not supported
 */
eBool AtEthPortHasDestMac(AtEthPort self)
    {
    if (self)
        return mMethodsGet(self)->HasDestMac(self);
    return cAtFalse;
    }

/**
 * Enable/disable MAC address checking
 *
 * @param self This port
 * @param enable Enable/disable MAC address checking
 * @return AT return code
 */
eAtModuleEthRet AtEthPortMacCheckingEnable(AtEthPort self, eBool enable)
    {
    mNumericalAttributeSetWithCache(MacCheckingEnable, enable);
    }

/**
 * Get MAC address checking is enabled/disabled
 *
 * @param self This port
 * @return Enabled/disabled
 */
eBool AtEthPortMacCheckingIsEnabled(AtEthPort self)
    {
    mAttributeGetWithCache(MacCheckingIsEnabled, eBool, cAtFalse);
    }

/**
 * Set expected C-VLAN
 *
 * @param self This port
 * @param cVlan Pointer to C-VLAN @ref tAtVlan "VLAN Structure". If cVlan is
 *              NULL, the expected check shall be disabled.
 *
 * @return AT return code
 */
eAtRet AtEthPortExpectedCVlanSet(AtEthPort self, const tAtVlan *cVlan)
    {
    if (!mPortIsValid(self))
        return cAtErrorNullPointer;

    if (!AtEthPortHasCVlan(self))
        return (cVlan) ? cAtErrorModeNotSupport : cAtOk;

    mVlanAttributeSet(ExpectedCVlanSet, cVlan);
    }

/**
 * Get expected C-VLAN
 *
 * @param self This port
 * @param cVlan Pointer to C-VLAN @ref tAtVlan "VLAN Structure" to be returned
 *
 * @return Pointer to C-VLAN @ref tAtVlan "VLAN Structure". If the expected
 *         C-VLAN check is disabled, the NULL pointer shall be returned.
 */
tAtVlan *AtEthPortExpectedCVlanGet(AtEthPort self, tAtVlan *cVlan)
    {
    if (!mPortIsValid(self) || (cVlan == NULL))
        return NULL;

    if (!AtEthPortHasCVlan(self))
        return NULL;

    return mMethodsGet(self)->ExpectedCVlanGet(self, cVlan);
    }

/**
 * Set transmitted C-VLAN
 *
 * @param self This port
 * @param cVlan Pointer to C-VLAN @ref tAtVlan "VLAN Structure". If cVlan is
 *              NULL, the C-VLAN shall not be inserted for transmission.
 *
 * @return AT return code
 */
eAtRet AtEthPortTxCVlanSet(AtEthPort self, const tAtVlan *cVlan)
    {
    if (!mPortIsValid(self))
        return cAtErrorNullPointer;

    if (!AtEthPortHasCVlan(self))
        return (cVlan) ? cAtErrorModeNotSupport : cAtOk;

    mVlanAttributeSet(TxCVlanSet, cVlan);
    }

/**
 * Get transmitted C-VLAN
 *
 * @param self This port
 * @param cVlan Pointer to C-VLAN @ref tAtVlan "VLAN Structure" to be returned
 *
 * @return Pointer to C-VLAN @ref tAtVlan "VLAN Structure". If the C-VLAN
 *         insertion is disabled, the NULL pointer shall be returned.
 */
tAtVlan *AtEthPortTxCVlanGet(AtEthPort self, tAtVlan *cVlan)
    {
    if (!mPortIsValid(self) || (cVlan == NULL))
        return NULL;

    if (!AtEthPortHasCVlan(self))
        return NULL;

    return mMethodsGet(self)->TxCVlanGet(self, cVlan);
    }

/**
 * Check if C-VLAN is configurable
 *
 * @param self This port
 *
 * @retval cAtTrue  supported
 * @retval cAtFalse not supported
 */
eBool AtEthPortHasCVlan(AtEthPort self)
    {
    if (!mPortIsValid(self))
        return cAtFalse;

    return mMethodsGet(self)->HasCVlan(self);
    }

/**
 * Set Interface for Ethernet port
 *
 * @param self This port
 * @param interface Interface mode. Refer
 *                  - @ref eAtEthPortInterface "Ethernet port interface"
 * @return AT return code
 */
eAtModuleEthRet AtEthPortInterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    mNumericalAttributeSetWithCache(InterfaceSet, interface);
    }

/**
 * Get interface mode of Ethernet port
 *
 * @param self This port
 * @return Interface mode.  Refer
 *                  - @ref eAtEthPortInterface "Ethernet port interface"
 */
eAtEthPortInterface AtEthPortInterfaceGet(AtEthPort self)
    {
    mAttributeGetWithCache(InterfaceGet, eAtEthPortInterface, cAtEthPortInterfaceUnknown);
    }

/**
 * Check if this port can support input speed
 *
 * @param self This port
 * @param speed @ref eAtEthPortSpeed "Speed to check"
 *
 * @retval cAtTrue if this speed can be supported, cAtFalse for otherwise
 */
eBool AtEthPortSpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    mOneParamAttributeGet(SpeedIsSupported, speed, eBool, cAtFalse);
    }

/**
 * Set Speed for Ethernet port
 *
 * @param self This port
 * @param speed Port speed. Refer
 *                  - @ref eAtEthPortSpeed "Ethernet port speed"
 * @return AT return code
 */
eAtModuleEthRet AtEthPortSpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    if (!mPortIsValid(self))
        return cAtError;
    if (!AtEthPortSpeedIsSupported(self, speed))
        return cAtErrorModeNotSupport;

    mNumericalAttributeSet(SpeedSet, speed);
    }

/**
 * Get Ethernet port's speed
 * @param self This port
 * @return Speed. Refer
 *                  - @ref eAtEthPortSpeed "Ethernet port speed"
 */
eAtEthPortSpeed AtEthPortSpeedGet(AtEthPort self)
    {
    mAttributeGet(SpeedGet, eAtEthPortSpeed, cAtEthPortSpeedUnknown);
    }

/**
 * Set working mode for Ethernet port
 *
 * @param self This port
 * @param duplexMode Duplex mode
 *                 - @ref eAtEthPortDuplexMode "Ethernet port duplex mode"
 * @return AT return code
 */
eAtModuleEthRet AtEthPortDuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
    mNumericalAttributeSet(DuplexModeSet, duplexMode);
    }

/**
 * Get Ethernet port working mode
 *
 * @param self This port
 * @return Working mode.  Refer
 *                 - @ref eAtEthPortDuplexMode "Ethernet port working mode"
 */
eAtEthPortDuplexMode AtEthPortDuplexModeGet(AtEthPort self)
    {
    mAttributeGet(DuplexModeGet, eAtEthPortDuplexMode, cAtEthPortWorkingModeAutoDetect);
    }

/**
 * Enable/disable Ethernet port auto-negotiation
 *
 * @param self This port
 * @param enable Enable/disable
 * @return AT return code
 */
eAtModuleEthRet AtEthPortAutoNegEnable(AtEthPort self, eBool enable)
    {
    mNumericalAttributeSet(AutoNegEnable, enable);
    }

/**
 * Get Ethernet port auto-negotiation is enabled/disabled
 *
 * @param self This port
 * @return Enabled/disabled
 */
eBool AtEthPortAutoNegIsEnabled(AtEthPort self)
    {
    mAttributeGet(AutoNegIsEnabled, eBool, cAtFalse);
    }

/**
 * To control autoneg restarting
 *
 * @param self This port
 * @param on Restarting
 *           - cAtTrue: restart ON
 *           - cAtFalse: restart OFF
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortAutoNegRestartOn(AtEthPort self, eBool on)
    {
    mNumericalAttributeSet(AutoNegRestartOn, on);
    }

/**
 * Check if autoneg restart is ON
 *
 * @param self This port
 *
 * @retval cAtTrue if restarting is ON
 * @retval cAtFalse if restarting is OFF
 */
eBool AtEthPortAutoNegRestartIsOn(AtEthPort self)
    {
    mAttributeGet(AutoNegRestartIsOn, eBool, cAtFalse);
    }

/**
 * Set transmit Inter-packet gap
 *
 * @param self This port
 * @param ipg Inter-packet gap (in bytes)
 * @return AT return code
 */
eAtModuleEthRet AtEthPortTxIpgSet(AtEthPort self, uint8 ipg)
    {
    mNumericalAttributeSetWithCache(TxIpgSet, ipg);
    }

/**
 * Get transmit Inter-packet gap
 *
 * @param self This port
 * @return Inter-packet gap (in bytes)
 */
uint8 AtEthPortTxIpgGet(AtEthPort self)
    {
    mAttributeGetWithCache(TxIpgGet, uint8, 0);
    }

/**
 * Check if TX IPG is configurable
 *
 * @param self This port
 *
 * @retval cAtTrue if configurable
 * @retval cAtFalse if not configurable
 */
eBool AtEthPortTxIpgIsConfigurable(AtEthPort self)
    {
    mAttributeGet(TxIpgIsConfigurable, eBool, cAtFalse);
    }

/**
 * Set receive Inter-packet gap
 *
 * @param self This port
 * @param ipg Inter-packet gap (in bytes)
 * @return AT return code
 */
eAtModuleEthRet AtEthPortRxIpgSet(AtEthPort self, uint8 ipg)
    {
    mNumericalAttributeSetWithCache(RxIpgSet, ipg);
    }

/**
 * Get receive Inter-packet gap
 *
 * @param self This port
 * @return Inter-packet gap (in bytes)
 */
uint8 AtEthPortRxIpgGet(AtEthPort self)
    {
    mAttributeGetWithCache(RxIpgGet, uint8, 0);
    }

/**
 * Check if RX IPG is configurable
 *
 * @param self This port
 *
 * @retval cAtTrue if configurable
 * @retval cAtFalse if not configurable
 */
eBool AtEthPortRxIpgIsConfigurable(AtEthPort self)
    {
    mAttributeGet(RxIpgIsConfigurable, eBool, cAtFalse);
    }

/**
 * Set LED state. Turn it ON/OFF or make it blink
 *
 * @param self This port
 * @param ledState @ref eAtLedState "LED states"
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortLedStateSet(AtEthPort self, eAtLedState ledState)
    {
    mNumericalAttributeSet(LedStateSet, ledState);
    }

/**
 * Get LED state to determine it is ON/OFF or blinking
 *
 * @param self This port
 *
 * @return @ref eAtLedState "LED state"
 */
eAtLedState AtEthPortLedStateGet(AtEthPort self)
    {
    mAttributeGet(LedStateGet, eAtLedState, cAtLedStateOff);
    }

/**
 * Get autoneg state
 *
 * @param self This port
 *
 * @return @ref eAtEthPortAutoNegState "Autoneg state"
 */
eAtEthPortAutoNegState AtEthPortAutoNegStateGet(AtEthPort self)
    {
    mAttributeGet(AutoNegStateGet, eAtEthPortAutoNegState, cAtEthPortAutoNegInvalid);
    }

/**
 * Check if autoneg feature is supported
 *
 * @param self This port
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtEthPortAutoNegIsSupported(AtEthPort self)
    {
    mAttributeGet(AutoNegIsSupported, eBool, cAtFalse);
    }

/**
 * Get autoneg detected speed
 *
 * @param self This port
 *
 * @return @ref eAtEthPortSpeed "Detected speed"
 */
eAtEthPortSpeed AtEthPortAutoNegSpeedGet(AtEthPort self)
    {
    mAttributeGet(AutoNegSpeedGet, eAtEthPortSpeed, cAtEthPortSpeedUnknown);
    }

/**
 * Get autoneg detected duplex mode
 *
 * @param self This port
 *
 * @return @ref eAtEthPortDuplexMode "Detected duplex mode"
 */
eAtEthPortDuplexMode AtEthPortAutoNegDuplexModeGet(AtEthPort self)
    {
    mAttributeGet(AutoNegDuplexModeGet, eAtEthPortDuplexMode, cAtEthPortWorkingModeUnknown);
    }

/**
 * Check if autoneg is complete or not
 *
 * @param self This port
 *
 * @retval cAtTrue if complete
 * @retval cAtFalse if not complete
 */
eBool AtEthPortAutoNegIsComplete(AtEthPort self)
    {
    mAttributeGet(AutoNegIsComplete, eBool, cAtFalse);
    }

/**
 * Get link status
 *
 * @param self This port
 *
 * @return @ref eAtEthPortLinkStatus "Link status"
 */
eAtEthPortLinkStatus AtEthPortLinkStatus(AtEthPort self)
    {
    mAttributeGet(LinkStatus, eAtEthPortLinkStatus, cAtEthPortLinkStatusUnknown);
    }

/**
 * Ger remote fault code
 *
 * @param self This port
 *
 * @return @ref eAtEthPortRemoteFault "Remote fault"
 */
eAtEthPortRemoteFault AtEthPortRemoteFaultGet(AtEthPort self)
    {
    mAttributeGet(RemoteFaultGet, eAtEthPortRemoteFault, cAtEthPortRemoteFaultUnknown);
    }

/**
 * Get SERDES controller
 *
 * @param self This port
 *
 * @return AtSerdesController object on success, NULL if port does not have
 *         SERDES controller
 */
AtSerdesController AtEthPortSerdesController(AtEthPort self)
    {
    mNoParamObjectGet(SerdesController, AtSerdesController);
    }

/**
 * Set maximum packet size
 *
 * @param self This port
 * @param maxPacketSize Maximum packet size in bytes
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortMaxPacketSizeSet(AtEthPort self, uint32 maxPacketSize)
    {
    mNumericalAttributeSet(MaxPacketSizeSet, maxPacketSize);
    }

/**
 * Get maximum packet size
 *
 * @param self This port
 * @return Maximum packet size in bytes
 */
uint32 AtEthPortMaxPacketSizeGet(AtEthPort self)
    {
    mAttributeGet(MaxPacketSizeGet, uint32, 0);
    }

/**
 * Set minimum packet size in bytes
 *
 * @param self This port
 * @param minPacketSize Minimum packet size in bytes
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortMinPacketSizeSet(AtEthPort self, uint32 minPacketSize)
    {
    mNumericalAttributeSet(MinPacketSizeSet, minPacketSize);
    }

/**
 * Get minimum packet size in bytes
 *
 * @param self This port
 * @return Minimum packet size in bytes
 */
uint32 AtEthPortMinPacketSizeGet(AtEthPort self)
    {
    mAttributeGet(MinPacketSizeGet, uint32, 0);
    }

/**
 * Switch traffic which is selected from this port to another port
 *
 * @param self This port
 * @param toPort Another port
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortSwitch(AtEthPort self, AtEthPort toPort)
    {
    mObjectSet(Switch, toPort);
    }

/**
 * Get the port that traffic of this port is now switched to
 *
 * @param self This port
 *
 * @return The port that traffic of this port is now switched to. If traffic
 * has not been switched, NULL object is returned.
 */
AtEthPort AtEthPortSwitchedPortGet(AtEthPort self)
    {
    mNoParamObjectGet(SwitchedPortGet, AtEthPort);
    }

/**
 * Switch traffic back to this port.
 *
 * @param self This port
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortSwitchRelease(AtEthPort self)
    {
    mNoParamCall(SwitchRelease, eAtModuleEthRet, cAtErrorNullPointer);
    }

/**
 * Bridge traffic which is currently transmitted by this port to another port
 *
 * @param self This port
 * @param toPort Another port that traffic will be bridged to.
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortBridge(AtEthPort self, AtEthPort toPort)
    {
    mObjectSet(Bridge, toPort);
    }

/**
 * Get the port that traffic of this port is currently bridged to.
 *
 * @param self This port
 *
 * @return The port that traffic of this port is currently bridged to. If
 * traffic has not been bridged, NULL object is returned.
 */
AtEthPort AtEthPortBridgedPortGet(AtEthPort self)
    {
    mNoParamObjectGet(BridgedPortGet, AtEthPort);
    }

/**
 * Stop bridging traffic of this port to another port
 *
 * @param self This port
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortBridgeRelease(AtEthPort self)
    {
    mNoParamCall(BridgeRelease, eAtModuleEthRet, cAtErrorNullPointer);
    }

/**
 * Get provisioned bandwidth
 *
 * @param self This port
 * @return Provisioned bandwidth in Kbit-per-second unit
 */
uint32 AtEthPortProvisionedBandwidthInKbpsGet(AtEthPort self)
    {
    mAttributeGet(ProvisionedBandwidthInKbpsGet, uint32, 0);
    }

/**
 * Get running bandwidth
 *
 * @param self This port
 * @return Running bandwidth in Kbit-per-second unit
 */
uint32 AtEthPortRunningBandwidthInKbpsGet(AtEthPort self)
    {
    mAttributeGet(RunningBandwidthInKbpsGet, uint32, 0);
    }

/**
 * Enable/disable HiGig
 *
 * @param self This port
 * @param enable Enable/Disable
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortHiGigEnable(AtEthPort self, eBool enable)
    {
    mNumericalAttributeSet(HiGigEnable, enable);
    }

/**
 * Check if HiGig is enabled/disabled
 *
 * @param self This port
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtEthPortHiGigIsEnabled(AtEthPort self)
    {
    mAttributeGet(HiGigIsEnabled, eBool, cAtFalse);
    }

/**
 * Get the flow control engine associated with this port
 *
 * @param self This Port
 * @return Ethernet Flow Control if hardware supports flow control. Otherwise,
 * NULL object is returned
 */
AtEthFlowControl AtEthPortFlowControlGet(AtEthPort self)
    {
    return mPortIsValid(self) ? FlowControlGet(self) : NULL;
    }
    
/**
 * Set IPv4 address
 *
 * @param self This port
 * @param address IPv4 address. Must be at least 4-bytes array
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortIpV4AddressSet(AtEthPort self, uint8 *address)
    {
    mIpV4AttributeSet(IpV4AddressSet, address);
    }

/**
 * Get IPv4 address
 *
 * @param self This port
 * @param [out] address Buffer to hold IPv4 address. Its size must >= 4
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortIpV4AddressGet(AtEthPort self, uint8 *address)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->IpV4AddressGet(self, address);
        
    return cAtError;
    }

/**
 * Set IPv6 address
 *
 * @param self This port
 * @param address IPv6 address. Must be at least 16-bytes array.
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortIpV6AddressSet(AtEthPort self, uint8 *address)
    {
    mIpV6AttributeSet(IpV6AddressSet, address);
    }

/**
 * Get IPv6 address
 *
 * @param self This port
 * @param [out] address Buffer to hold IPv6 address. Its size must >= 16
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortIpV6AddressGet(AtEthPort self, uint8 *address)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->IpV6AddressGet(self, address);
        
    return cAtError;
    }

/**
 * Select SERDES that the ETH Port will receive traffic.
 *
 * @param self This port
 * @param serdes SERDES that ETH Port will receive traffic.
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortRxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    if (!mPortIsValid(self))
        return cAtErrorNullPointer;

    if ((serdes == NULL) && (!mMethodsGet(self)->CanSelectNullSerdes(self)))
        return cAtErrorInvlParm;

    if (SerdesApsSupported(self))
        {
        mObjectSet(RxSerdesSelect, serdes);
        }

    return cAtErrorModeNotSupport;
    }

/**
 * Get the SERDES that ETH Port is receiving traffic.
 *
 * @param self This port
 *
 * @return The SERDES that ETH Port is receiving traffic.
 */
AtSerdesController AtEthPortRxSelectedSerdes(AtEthPort self)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->RxSelectedSerdes(self);
    return NULL;
    }

/**
 * Set SERDES that the ETH Port will transmit traffic.
 *
 * @param self This port
 * @param serdes SERDES that ETH Port will transmit traffic.
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortTxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    if (!mPortIsValid(self))
        return cAtErrorNullPointer;

    if (serdes == NULL)
        return cAtErrorInvlParm;

    if (SerdesApsSupported(self))
        {
        mObjectSet(TxSerdesSet, serdes);
        }

    return cAtErrorModeNotSupport;
    }

/**
 * Get the SERDES that ETH Port is transmitting traffic.
 *
 * @param self This port
 *
 * @return The SERDES that ETH Port is transmitting traffic.
 */
AtSerdesController AtEthPortTxSerdesGet(AtEthPort self)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->TxSerdesGet(self);
    return NULL;
    }

/**
 * Bridge (duplicate) traffic to specified SERDES
 *
 * @param self This port
 * @param serdes SERDES to bridge traffic
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortTxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    if (!mPortIsValid(self))
        return cAtErrorNullPointer;

    if (SerdesApsSupported(self))
        {
        mObjectSet(TxSerdesBridge, serdes);
        }

    return cAtErrorModeNotSupport;
    }

/**
 * Get the SERDES that traffic of this port is currently bridged to.
 *
 * @param self This port
 *
 * @return The SERDES that traffic of this port is currently bridged to.
 */
AtSerdesController AtEthPortTxBridgedSerdes(AtEthPort self)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->TxBridgedSerdes(self);
    return NULL;
    }

/**
 * Get current bandwidth of queue
 *
 * @param self This port
 * @param queueId Queue Id
 *
 * @return Current queue's bandwidth in bit per second
 */
uint32 AtEthPortQueueCurrentBandwidthInBpsGet(AtEthPort self, uint8 queueId)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->QueueCurrentBandwidthInBpsGet(self, queueId);
    return 0;
    }

/**
 * Get maximum number of queue
 *
 * @param self This port
 * @return Number of queue
 */
uint32 AtEthPortMaxQueuesGet(AtEthPort self)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->MaxQueuesGet(self);

    return 0;
    }

/**
 * Get list of all PWs running on queue
 *
 * @param self This port
 * @param queueId Queue Id
 *
 * @return List of all PWs running on queue
 */
AtList AtEthPortQueueAllPwsGet(AtEthPort self, uint8 queueId)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->QueueAllPwsGet(self, queueId);
    return NULL;
    }

/**
 * Get TX error generator
 *
 * @param self This port
 *
 * @return TX error generator
 */
AtErrorGenerator AtEthPortTxErrorGeneratorGet(AtEthPort self)
    {
    mAttributeGet(TxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

/**
 * Get RX error generator
 *
 * @param self This port
 *
 * @return RX error generator
 */
AtErrorGenerator AtEthPortRxErrorGeneratorGet(AtEthPort self)
    {
    mAttributeGet(RxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

/**
 * Get sub port of ethernet port
 *
 * @param self This port
 * @param subPortId
 *
 * @return sub ethernet port
 */
AtEthPort AtEthPortSubPortGet(AtEthPort self, uint32 subPortId)
    {
    mOneParamAttributeGet(SubPortGet, subPortId, AtEthPort, NULL);
    }

/**
 * Get maximum number of sub-ports
 *
 * @param self This port
 * @return Maximum number of sub-ports
 */
uint32 AtEthPortMaxSubPortsGet(AtEthPort self)
    {
    mAttributeGet(MaxSubPortsGet, uint32, 0);
    }

/**
 * Get the parent port that this port belongs to
 *
 * @param self This port
 *
 * @return Parent port
 */
AtEthPort AtEthPortParentPortGet(AtEthPort self)
    {
    mAttributeGet(ParentPortGet, AtEthPort, NULL);
    }

/**
 * Set conditions that will drop packet
 *
 * @param self This port
 * @param conditionMask conditions mask. @ref eAtEthPortDropCondition "Drop Packet Conditions"
 * @param enableMask Drop indication. For each bit:
 *                   - 1: to drop
 *                   - 0: do not drop
 *
 * @return AT return code
 */
eAtRet AtEthPortDropPacketConditionMaskSet(AtEthPort self, uint32 conditionMask, uint32 enableMask)
    {
    mTwoParamsAttributeSet(DropPacketConditionMaskSet, conditionMask, enableMask);
    }

/**
 * Get Condition that cause drop packet
 *
 * @param self This port
 * @return @ref eAtEthPortDropCondition "Drop Packet Conditions"
 */
uint32 AtEthPortDropPacketConditionMaskGet(AtEthPort self)
    {
    if (mPortIsValid(self))
        return mMethodsGet(self)->DropPacketConditionMaskGet(self);

    return 0;
    }

/**
 * Force disparity
 *
 * @param self This port
 * @param forced Force enabling.
 *               - cAtTrue to enable forcing
 *               - cAtFalse to disable forcing
 *
 * @return AT return code
 */
eAtRet AtEthPortDisparityForce(AtEthPort self, eBool forced)
    {
    mNumericalAttributeSet(DisparityForce, forced);
    }

/**
 * Check if disparity forcing is enabled or disabled
 *
 * @param self This port
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtEthPortDisparityIsForced(AtEthPort self)
    {
    mAttributeGet(DisparityIsForced, eBool, cAtFalse);
    }

/**
 * Check if disparity forcing can be forced
 *
 * @param self This port
 *
 * @retval cAtTrue if can be forced
 * @retval cAtFalse if cannot be forced
 */
eBool AtEthPortDisparityCanForce(AtEthPort self)
    {
    mAttributeGet(DisparityCanForce, eBool, cAtFalse);
    }

/**
 * Enable RX FEC
 *
 * @param self This port
 * @param enabled Enabling. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortRxFecEnable(AtEthPort self, eBool enabled)
    {
    mNumericalAttributeSet(RxFecEnable, enabled);
    }

/**
 * Check if RX FEC is enabled or not
 *
 * @param self This port
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtEthPortRxFecIsEnabled(AtEthPort self)
    {
    mAttributeGet(RxFecIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable TX FEC
 *
 * @param self This port
 * @param enabled Enabling. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortTxFecEnable(AtEthPort self, eBool enabled)
    {
    mNumericalAttributeSet(TxFecEnable, enabled);
    }

/**
 * Check if TX FEC is enabled or not
 *
 * @param self This port
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtEthPortTxFecIsEnabled(AtEthPort self)
    {
    mAttributeGet(TxFecIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if FEC feature is supported
 *
 * @param self This port
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtEthPortFecIsSupported(AtEthPort self)
    {
    mAttributeGet(FecIsSupported, eBool, cAtFalse);
    }

/**
 * Set expected DMAC
 *
 * @param self This port
 * @param mac Expected DMAC
 *
 * @return AT return code
 * @see
 */
eAtRet AtEthPortExpectedDestMacSet(AtEthPort self, const uint8 *mac)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorObjectNotExist;
    if (mac == NULL)
        return cAtErrorNullPointer;

    mMacAttributeApiLogStart(mac);
    ret = mMethodsGet(self)->ExpectedDestMacSet(self, mac);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get expected DMAC
 *
 * @param self This port
 * @param [out] mac Expected DMAC
 *
 * @return AT return code
 */
eAtRet AtEthPortExpectedDestMacGet(AtEthPort self, uint8 *mac)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;
    if (mac == NULL)
        return cAtErrorNullPointer;

    return mMethodsGet(self)->ExpectedDestMacGet(self, mac);
    }

/**
 * Check if the port has expected DMAC attribute
 *
 * @param self This port
 *
 * @retval cAtTrue if has
 * @retval cAtFalse if does not have
 */
eBool AtEthPortHasExpectedDestMac(AtEthPort self)
    {
    mAttributeGet(HasExpectedDestMac, eBool, cAtFalse);
    }

/**
 * Check if SONET/SDH overhead transparent is supported
 *
 * @param self This port
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtEthPortSohTransparentIsSupported(AtEthPort self)
    {
    mAttributeGet(SohTransparentIsSupported, eBool, cAtFalse);
    }

/**
 * Force K30.7 on 1000BaseX
 * @param self This port
 * @param forced Forcing
 *               - cAtTrue to force
 *               - cAtFalse to unforce
 *
 * @return AT return code
 */
eAtRet AtEthPortK30_7Force(AtEthPort self, eBool forced)
    {
    mNumericalAttributeSet(K30_7Force, forced);
    }

/**
 * Check if K30.7 is forcing on a port
 *
 * @param self This port
 *
 * @retval cAtTrue if forcing
 * @retval cAtFalse if not forcing
 */
eBool AtEthPortK30_7IsForced(AtEthPort self)
    {
    mAttributeGet(K30_7IsForced, eBool, cAtFalse);
    }

/**
 * Check if K30.7 can be forced
 *
 * @param self This port
 *
 * @retval cAtTrue if can force
 * @retval cAtFalse if can not force
 */
eBool AtEthPortK30_7CanForce(AtEthPort self)
    {
    mAttributeGet(K30_7CanForce, eBool, cAtFalse);
    }

/**
 * Get associated clock monitor
 *
 * @param self This port
 *
 * @return Clock monitor if have. Otherwise, NULL is returned.
 */
AtClockMonitor AtEthPortClockMonitorGet(AtEthPort self)
    {
    mNoParamObjectGet(ClockMonitorGet, AtClockMonitor);
    }

/**
 * Enable Link training to tune transceiver equalization settings
 *
 * @param self This port
 * @param enable Enabling. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortLinkTrainingEnable(AtEthPort self, eBool enable)
    {
    mNumericalAttributeSet(LinkTrainingEnable, enable);
    }

/**
 * Check if Link training is enabled or not
 *
 * @param self This port
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtEthPortLinkTrainingIsEnabled(AtEthPort self)
    {
    mAttributeGet(LinkTrainingIsEnabled, eBool, cAtFalse);
    }

/**
 * To restart link training
 *
 * @param self This port
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthPortLinkTrainingRestart(AtEthPort self)
    {
    mNoParamAttributeSet(LinkTrainingRestart);
    }

/**
 * Check if link training feature is supported
 *
 * @param self This port
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtEthPortLinkTrainingIsSupported(AtEthPort self)
    {
    mAttributeGet(LinkTrainingIsSupported, eBool, cAtFalse);
    }

/**
 * @}
 */

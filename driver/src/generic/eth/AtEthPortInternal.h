/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : ThaEthPortInternal.h
 *
 * Created Date: Aug 31, 2012
 *
 * Description : AtEthPort class representation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATETHPORTINTERNAL_H_
#define _ATETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEthPortSimulationDb
    {
    tAtChannelSimulationDb super;

    uint32 maxPacketSize;
    uint32 minPacketSize;
    }tAtEthPortSimulationDb;

typedef struct tAtEthPortCache
    {
    tAtChannel super;

    uint8 sourceMac[6];
    uint8 destMac[6];
    uint8 txIpg, rxIpg;
    uint8 macCheckingEnabled;
    uint8 interface;
    }tAtEthPortCache;

/* Methods */
typedef struct tAtEthPortMethods
    {
    /* MAC */
    eAtModuleEthRet (*SourceMacAddressSet)(AtEthPort self, uint8 *address);
    eAtModuleEthRet (*SourceMacAddressGet)(AtEthPort self, uint8 *address);
    eBool (*HasSourceMac)(AtEthPort self);
    eAtModuleEthRet (*MacCheckingEnable)(AtEthPort self, eBool enable);
    eBool (*MacCheckingIsEnabled)(AtEthPort self);
    eBool (*MacCheckingCanEnable)(AtEthPort self, eBool enable);
    eAtModuleEthRet (*DestMacAddressSet)(AtEthPort self, uint8 *address);
    eAtModuleEthRet (*DestMacAddressGet)(AtEthPort self, uint8 *address);
    eBool (*HasDestMac)(AtEthPort self);
    eBool (*HasMacFunctionality)(AtEthPort self);
    uint8 (*TxDefaultIpg)(AtEthPort self);

    /* VLAN */
    eAtRet (*ExpectedCVlanSet)(AtEthPort self, const tAtVlan *cVlan);
    tAtVlan *(*ExpectedCVlanGet)(AtEthPort self, tAtVlan *cVlan);
    eAtRet (*TxCVlanSet)(AtEthPort self, const tAtVlan *cVlan);
    tAtVlan *(*TxCVlanGet)(AtEthPort self, tAtVlan *cVlan);
    eBool (*HasCVlan)(AtEthPort self);

    /* Ethernet port speed */
    eBool (*SpeedIsSupported)(AtEthPort self, eAtEthPortSpeed speed);
    eAtModuleEthRet (*SpeedSet)(AtEthPort self, eAtEthPortSpeed speed);
    eAtEthPortSpeed (*SpeedGet)(AtEthPort self);
        
    /* Interface */
    eAtModuleEthRet (*InterfaceSet)(AtEthPort self, eAtEthPortInterface interface);
    eAtEthPortInterface (*InterfaceGet)(AtEthPort self);
    eAtEthPortInterface (*DefaultInterface)(AtEthPort self);
    AtSerdesController (*SerdesController)(AtEthPort self);
        
    /* Duplex mode */
    eAtModuleEthRet (*DuplexModeSet)(AtEthPort self, eAtEthPortDuplexMode duplexMode);
    eAtEthPortDuplexMode (*DuplexModeGet)(AtEthPort self);
        
    /* Inter packet gap */
    eAtModuleEthRet (*TxIpgSet)(AtEthPort self, uint8 txIpg);
    uint8 (*TxIpgGet)(AtEthPort self);
    eBool (*TxIpgIsConfigurable)(AtEthPort self);
    eAtModuleEthRet (*RxIpgSet)(AtEthPort self, uint8 rxIpg);
    uint8 (*RxIpgGet)(AtEthPort self);
    eBool (*RxIpgIsConfigurable)(AtEthPort self);
    eAtModuleEthRet (*TxIpgSetWithCache)(AtEthPort self, uint8 ipg);
    eBool (*RxIpgIsInRange)(AtEthPort self, uint8 rxIpg);
    eBool (*TxIpgIsInRange)(AtEthPort self, uint8 txIpg);

    /* Methods */
    /* Auto Negotiation */
    eAtModuleEthRet (*AutoNegEnable)(AtEthPort self, eBool enable);
    eBool (*AutoNegIsEnabled)(AtEthPort self);
    eAtEthPortAutoNegState (*AutoNegStateGet)(AtEthPort self);
    eBool (*AutoNegIsSupported)(AtEthPort self);
    eAtEthPortLinkStatus (*LinkStatus)(AtEthPort self);
    eAtEthPortSpeed (*AutoNegSpeedGet)(AtEthPort self);
    eAtEthPortDuplexMode (*AutoNegDuplexModeGet)(AtEthPort self);
    eBool (*AutoNegIsComplete)(AtEthPort self);
    eAtModuleEthRet (*AutoNegRestartOn)(AtEthPort self, eBool on);
    eBool (*AutoNegRestartIsOn)(AtEthPort self);
    eAtEthPortRemoteFault (*RemoteFaultGet)(AtEthPort self);

    /* Max/Min packet size */
    eAtModuleEthRet (*MaxPacketSizeSet)(AtEthPort self, uint32 maxPacketSize);
    uint32 (*MaxPacketSizeGet)(AtEthPort self);
    eAtModuleEthRet (*MinPacketSizeSet)(AtEthPort self, uint32 maxPacketSize);
    uint32 (*MinPacketSizeGet)(AtEthPort self);

    /* APS */
    eAtModuleEthRet (*Switch)(AtEthPort self, AtEthPort toPort);
    AtEthPort (*SwitchedPortGet)(AtEthPort self);
    eAtModuleEthRet (*SwitchRelease)(AtEthPort self);
    eAtModuleEthRet (*Bridge)(AtEthPort self, AtEthPort toPort);
    AtEthPort (*BridgedPortGet)(AtEthPort self);
    eAtModuleEthRet (*BridgeRelease)(AtEthPort self);

    /* HiGig */
    eAtModuleEthRet (*HiGigEnable)(AtEthPort self, eBool enable);
    eBool (*HiGigIsEnabled)(AtEthPort self);

    /* Current in-used bandwidth */
    uint32 (*ProvisionedBandwidthInKbpsGet)(AtEthPort self);
    uint32 (*RunningBandwidthInKbpsGet)(AtEthPort self);

    /* DIC */
    eAtModuleEthRet (*DicEnable)(AtEthPort self, eBool enable);
    eBool  (*DicIsEnabled)(AtEthPort self);
	
	/* IPv4/v6 */
    eAtModuleEthRet (*IpV4AddressSet)(AtEthPort self, uint8 *address);
    eAtModuleEthRet (*IpV4AddressGet)(AtEthPort self, uint8 *address);
    eAtModuleEthRet (*IpV6AddressSet)(AtEthPort self, uint8 *address);
    eAtModuleEthRet (*IpV6AddressGet)(AtEthPort self, uint8 *address);

    /* Control LED */
    eAtRet (*LedStateSet)(AtEthPort self, eAtLedState ledState);
    eAtLedState (*LedStateGet)(AtEthPort self);

    /* APS */
    eAtModuleEthRet (*RxSerdesSelect)(AtEthPort self, AtSerdesController serdes);
    AtSerdesController (*RxSelectedSerdes)(AtEthPort self);
    eBool (*CanSelectNullSerdes)(AtEthPort self);
    eAtModuleEthRet (*TxSerdesBridge)(AtEthPort self, AtSerdesController serdes);
    AtSerdesController (*TxBridgedSerdes)(AtEthPort self);
    eBool (*TxSerdesCanBridge)(AtEthPort self, AtSerdesController serdes);
    eAtModuleEthRet (*TxSerdesSet)(AtEthPort self, AtSerdesController serdes);
    AtSerdesController (*TxSerdesGet)(AtEthPort self);

    /* Queue */
    uint32 (*QueueCurrentBandwidthInBpsGet)(AtEthPort self, uint8 queueId);
    AtList (*QueueAllPwsGet)(AtEthPort self, uint8 queueId);
    uint32 (*MaxQueuesGet)(AtEthPort self);

    /* Error generator */
    AtErrorGenerator (*TxErrorGeneratorGet)(AtEthPort self);
    AtErrorGenerator (*RxErrorGeneratorGet)(AtEthPort self);

    /* Sub port */
    AtEthPort (*SubPortGet)(AtEthPort self, uint32 subPortId);
    uint32 (*MaxSubPortsGet)(AtEthPort self);
    AtEthPort (*ParentPortGet)(AtEthPort self);
    AtEthPort (*SubPortObjectCreate)(AtEthPort self, uint8 subPortId);

    /* Flow control */
    eAtRet (*FlowControlDefaultSet)(AtEthPort self);

    /* VLan-Expected */
    eBool (*ExpectedCVlanIsSupported)(AtEthPort self);
    eAtRet (*ExpectedCVlanIdSet)(AtEthPort self, uint16 vlanId);
    uint16 (*ExpectedCVlanIdGet)(AtEthPort self);

    /* Drop Packet Condition */
    eAtRet (*DropPacketConditionMaskSet)(AtEthPort self, uint32 conditionMask, uint32 enableMask);
    uint32 (*DropPacketConditionMaskGet)(AtEthPort self);

    /* Disparity force */
    eAtRet (*DisparityForce)(AtEthPort self, eBool forced);
    eBool (*DisparityIsForced)(AtEthPort self);
    eBool (*DisparityCanForce)(AtEthPort self);

    /* K30.7 force */
    eAtRet (*K30_7Force)(AtEthPort self, eBool forced);
    eBool (*K30_7IsForced)(AtEthPort self);
    eBool (*K30_7CanForce)(AtEthPort self);

    /* FEC */
    eAtRet (*RxFecEnable)(AtEthPort self, eBool enabled);
    eBool (*RxFecIsEnabled)(AtEthPort self);
    eAtRet (*TxFecEnable)(AtEthPort self, eBool enabled);
    eBool (*TxFecIsEnabled)(AtEthPort self);
    eBool (*FecIsSupported)(AtEthPort self);

    /* Some products have expected DMAC */
    eAtRet (*ExpectedDestMacSet)(AtEthPort self, const uint8 *mac);
    eAtRet (*ExpectedDestMacGet)(AtEthPort self, uint8 *mac);
    eBool (*HasExpectedDestMac)(AtEthPort self);

    /* Link training */
    eAtModuleEthRet (*LinkTrainingEnable)(AtEthPort self, eBool enable);
    eBool (*LinkTrainingIsEnabled)(AtEthPort self);
    eAtModuleEthRet (*LinkTrainingRestart)(AtEthPort self);
    eBool (*LinkTrainingIsSupported)(AtEthPort self);

    /* SONET/SDH overhead transparent */
    eBool (*SohTransparentIsSupported)(AtEthPort self);

    /* Clock monitor */
    AtClockMonitor (*ClockMonitorGet)(AtEthPort self);

    /* Back plane MAC */
    eBool (*IsBackplaneMac)(AtEthPort self);
    } tAtEthPortMethods;

/* This structure is to represent for Ethernet Port */
typedef struct tAtEthPort
    {
    tAtChannel super;

    /* Implementation */
    const tAtEthPortMethods *methods;

    /* Private data */
    AtEthFlowControl flowControl;
    void *cache;

    /* Subport management */
    AtEthPort parent;
    AtEthPort *subPorts;
    uint32 numCreatedSubPorts;
    }tAtEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort AtEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

eAtEthPortInterface AtEthPortDefaultInterface(AtEthPort self);
eAtModuleEthRet AtEthPortDicEnable(AtEthPort self, eBool enable);
eBool AtEthPortDicIsEnabled(AtEthPort self);
eBool AtEthPortCounterIsSupported(AtChannel self, uint16 counterType);
eBool AtEthPortTxSerdesCanBridge(AtEthPort self, AtSerdesController serdes);
eAtRet AtEthPortParentPortSet(AtEthPort self, AtEthPort parent);
eAtRet AtEthPortAllSubPortsInit(AtEthPort self);
uint32 *AtEthPortCounterField(AtEthPort self, uint16 counterType, tAtEthPortCounters *counters);
uint8 AtEthPortDefaultIpg(AtEthPort self);
uint8 AtEthPortTxDefaultIpg(AtEthPort self);

eBool AtEthPortTxIpgIsInRange(AtEthPort self, uint8 ipg);
eBool AtEthPortRxIpgIsInRange(AtEthPort self, uint8 ipg);
eBool AtEthPortHasMacFunctionality(AtEthPort self);
eBool AtEthPortMacCheckingCanEnable(AtEthPort self, eBool enable);

/* For Internal Expected C-Vlan */
eBool AtEthPortExpectedCVlanIsSupported(AtEthPort self);
eAtRet AtEthPortExpectedCVlanIdSet(AtEthPort self, uint16 vlanId);
uint16 AtEthPortExpectedCVlanIdGet(AtEthPort self);

eBool AtEthPortIsBackplaneMac(AtEthPort self);
#ifdef __cplusplus
}
#endif
#endif /* _ATETHPORTINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtEthFlow.c
 *
 * Created Date: Aug 7, 2012
 *
 * Description : ETH Flow
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "atclib.h"
#include "AtEthFlowInternal.h"
#include "AtModuleEthInternal.h"
#include "AtEncapBundle.h"

/*--------------------------- Define -----------------------------------------*/
#define mFlowIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtEthFlow)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEthFlowMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "flow";
    }

static AtList IngressVlans(AtEthFlow self)
    {
    if (self->ingressVlans == NULL)
        self->ingressVlans = AtListCreate(0);
    return self->ingressVlans;
    }

static AtList EgressVlans(AtEthFlow self)
    {
    if (self->egressVlans == NULL)
        self->egressVlans = AtListCreate(0);
    return self->egressVlans;
    }

static void AllVlanDelete(AtList vlans)
    {
    AtObject object;
    AtOsal osal = AtSharedDriverOsalGet();
    while ((object = AtListObjectRemoveAtIndex(vlans, 0)) != NULL)
        mMethodsGet(osal)->MemFree(osal, object);
    }

static eAtRet Init(AtChannel self)
    {
    AtEthFlow flow = (AtEthFlow)self;

    /* Super */
    eAtRet ret = m_AtChannelMethods->Init(self);

    /* Create lists to hold VLANs */
    AllVlanDelete(flow->ingressVlans);
    AllVlanDelete(flow->egressVlans);
    if (flow->ingressVlans == NULL)
        flow->ingressVlans = AtListCreate(0);
    if (flow->egressVlans == NULL)
        flow->egressVlans = AtListCreate(0);

    return ret;
    }

static eBool EgressMacIsProgrammable(AtEthFlow self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEthRet EgressDestMacSet(AtEthFlow self, uint8 *egressDestMac)
    {
	AtUnused(egressDestMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static eAtModuleEthRet EgressDestMacGet(AtEthFlow self, uint8 *egressDestMac)
    {
	AtUnused(egressDestMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleEthRet EgressSourceMacSet(AtEthFlow self, uint8 *egressSourceMac)
    {
	AtUnused(egressSourceMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static eAtModuleEthRet EgressSourceMacGet(AtEthFlow self, uint8 *egressSourceMac)
    {
	AtUnused(egressSourceMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleEthRet EgressVlanSet(AtEthFlow self, const tAtEthVlanDesc *egressVlan)
    {
    if (!AtEthVlanDescIsValid(egressVlan))
        return cAtErrorInvlParm;

    while (AtEthFlowEgressNumVlansGet(self) > 0)
        {
        tAtEthVlanDesc vlanDesc;
        AtEthFlowEgressVlanAtIndex(self, 0, &vlanDesc);
        AtEthFlowEgressVlanRemove(self,  &vlanDesc);
        }

    return AtEthFlowEgressVlanAdd(self, egressVlan);
    }
    
static uint32 IngressAllVlansGet(AtEthFlow self, tAtEthVlanDesc *descs)
    {
    uint16 idx;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 igVlanNum = AtEthFlowIngressNumVlansGet(self);

    if (descs == NULL)
        return igVlanNum;

    for (idx = 0; idx < igVlanNum; idx++)
        {
        tAtEthVlanDesc vlanDesc;

        AtEthFlowIngressVlanAtIndex(self, idx, &vlanDesc);
        mMethodsGet(osal)->MemCpy(osal, &descs[idx], &vlanDesc, sizeof(tAtEthVlanDesc));
        }

    return igVlanNum;
    }

static eBool IngressVlanCanUse(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    /* Sub class should know */
    AtUnused(self);
    AtUnused(desc);
    return cAtTrue;
    }

static eBool SameVlanDesc(const tAtEthVlanDesc *desc1, const tAtEthVlanDesc *desc2)
    {
    uint8 i = 0;

    if (desc1->ethPortId != desc2->ethPortId)
        return cAtFalse;
    if (desc1->numberOfVlans != desc2->numberOfVlans)
        return cAtFalse;

    for (i = 0; i < cAtMaxNumVlanTag; i++)
        {
        if (desc1->vlans[i].cfi != desc2->vlans[i].cfi)
            return cAtFalse;
        if (desc1->vlans[i].priority != desc2->vlans[i].priority)
            return cAtFalse;
        if (desc1->vlans[i].vlanId != desc2->vlans[i].vlanId)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool VlanExist(AtList vlans, const tAtEthVlanDesc *desc, uint32 *vlanIndex)
    {
    uint32 i;

    /* Can not add VLAN which exists */
    for (i = 0; i < AtListLengthGet(vlans); i++)
        {
        tAtEthVlanDesc *existDesc = (tAtEthVlanDesc *)AtListObjectGet(vlans, i);

        if (SameVlanDesc(desc, existDesc))
            {
            if (vlanIndex != NULL)
                *vlanIndex = i;
            return cAtTrue;
            }
        }

    return cAtFalse;
    }

static eBool VlanIsValid(const tAtEthVlanDesc *vlan)
    {
    if (AtEthVlanDescIsValid(vlan) == cAtFalse)
        return cAtFalse;

    if (vlan->numberOfVlans == 0)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet VlanAdd(AtList vlans, const tAtEthVlanDesc *desc)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    tAtEthVlanDesc *descriptor = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtEthVlanDesc));
    mMethodsGet(osal)->MemCpy(osal, descriptor, desc, sizeof(tAtEthVlanDesc));
    return AtListObjectAdd(vlans, (AtObject)(AtSize)descriptor);
    }

static eAtRet VlanRemove(AtList vlans, const tAtEthVlanDesc *desc)
    {
    uint16 idx;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!VlanIsValid(desc))
        return cAtErrorInvlParm;

    for (idx = 0; idx < AtListLengthGet(vlans); idx++)
        {
        tAtEthVlanDesc *existingDescriptor = (tAtEthVlanDesc *)AtListObjectGet(vlans, idx);

        /* Remove VLAN if it exists */
        if (!SameVlanDesc(desc, existingDescriptor))
            continue;

        mMethodsGet(osal)->MemFree(osal, existingDescriptor);
        return AtListObjectRemove(vlans, (AtObject)(AtSize)existingDescriptor);
        }

    return cAtModuleEthErrorVlanNotExist;
    }

static tAtEthVlanDesc *VlanAtIndex(AtList vlans, uint32 vlanIndex, tAtEthVlanDesc *descs)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    tAtEthVlanDesc *existingDescriptor = (tAtEthVlanDesc *)AtListObjectGet(vlans, vlanIndex);

    if (existingDescriptor == NULL)
        return NULL;

    mMethodsGet(osal)->MemCpy(osal, descs, existingDescriptor, sizeof(tAtEthVlanDesc));
    return descs;
    }

static eAtModuleEthRet IngressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    if (!VlanIsValid(desc))
        return cAtErrorInvlParm;

    if (VlanExist(self->ingressVlans, desc, NULL))
        return cAtModuleEthErrorVlanAlreadyExist;

    if (!mMethodsGet(self)->IngressVlanCanUse(self, desc))
        return cAtErrorChannelBusy;

    return VlanAdd(IngressVlans(self), desc);
    }

static eAtModuleEthRet IngressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    return VlanRemove(self->ingressVlans, desc);
    }

static uint32 IngressNumVlansGet(AtEthFlow self)
    {
    return AtListLengthGet(self->ingressVlans);
    }

static tAtEthVlanDesc *IngressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs)
    {
    return VlanAtIndex(self->ingressVlans, vlanIndex, descs);
    }

static eBool EgressVlanCanUse(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    /* Sub class should know */
    AtUnused(self);
    AtUnused(desc);
    return cAtTrue;
    }

static eAtModuleEthRet EgressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    if (!VlanIsValid(desc))
        return cAtErrorInvlParm;

    if (VlanExist(self->egressVlans, desc, NULL))
        return cAtModuleEthErrorVlanAlreadyExist;

    if (!mMethodsGet(self)->EgressVlanCanUse(self, desc))
        return cAtErrorChannelBusy;

    return VlanAdd(EgressVlans(self), desc);
    }

static eAtModuleEthRet EgressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc)
    {
    return VlanRemove(self->egressVlans, desc);
    }

static uint32 EgressNumVlansGet(AtEthFlow self)
    {
    return AtListLengthGet(self->egressVlans);
    }

static tAtEthVlanDesc *EgressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs)
    {
    return VlanAtIndex(self->egressVlans, vlanIndex, descs);
    }

static AtHdlcLink HdlcLinkGet(AtEthFlow self)
    {
    return self->hdlcLink;
    }

static AtMpBundle MpBundleGet(AtEthFlow self)
    {
    if (self->bundle == NULL)
        return NULL;

    if (AtEncapBundleTypeGet((AtEncapBundle)self->bundle) == cAtEncapBundleTypeMlppp)
        return (AtMpBundle)(self->bundle);

    return NULL;
    }

static eAtModuleEthRet FrVirtualCircuitSet(AtEthFlow self, AtFrVirtualCircuit circuit)
    {
    if (self->circuit == circuit)
        return cAtOk;

    if ((circuit) && (self->circuit))
        return cAtErrorRsrcNoAvail;

    self->circuit = circuit;

    return cAtOk;
    }

static AtFrVirtualCircuit FrVirtualCircuitGet(AtEthFlow self)
    {
    return self->circuit;
    }

static void FlowControlDelete(AtEthFlow self)
    {
    AtObjectDelete((AtObject)(self->flowControl));
    self->flowControl = NULL;
    }

static void DeleteVlans(AtList *vlans)
    {
    AllVlanDelete(*vlans);
    AtObjectDelete((AtObject)*vlans);
    *vlans = NULL;
    }

static void Delete(AtObject self)
    {
    AtEthFlow object = (AtEthFlow)self;

    /* Delete all VLANs */
    DeleteVlans(&object->ingressVlans);
    DeleteVlans(&object->egressVlans);
    FlowControlDelete((AtEthFlow)self);

    /* Fully delete it */
    m_AtObjectMethods->Delete(self);
    }

static void VlanTagSerialize(AtObject vlanTag, AtCoder encoder)
    {
    tAtEthVlanTag *object = (tAtEthVlanTag *)vlanTag;

    mEncodeUInt(priority);
    mEncodeUInt(cfi);
    mEncodeUInt(vlanId);
    }

static AtObject SerializeVlanIndexAtIndex(AtCoder self, void *objects, uint32 objectIndex)
    {
    tAtEthVlanTag *vlans = (tAtEthVlanTag *)objects;
    AtObject object = (AtObject)((void *)&(vlans[objectIndex]));
    AtUnused(self);
    return object;
    }

static void SerializeVlanDescriptor(AtObject descriptor, AtCoder encoder)
    {
    tAtEthVlanDesc *object = (tAtEthVlanDesc *)descriptor;
    mEncodeUInt(ethPortId);
    AtCoderEncodeObjectsWithHandlers(encoder,
                                     object->vlans, object->numberOfVlans,
                                     SerializeVlanIndexAtIndex, "vlans", VlanTagSerialize);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtEthFlow object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(flowType);
    mEncodeUInt(flowIsUsed);
    mEncodeListWithHandler(ingressVlans, AtUtilVlanDescriptorSerialize);
    mEncodeListWithHandler(egressVlans, AtUtilVlanDescriptorSerialize);
    mEncodeObjectDescription(flowControl);
    mEncodeObjectDescription(encapChannel);
    mEncodeObjectDescription(vcg);
    mEncodeObjectDescription(bundle);
    mEncodeObjectDescription(hdlcLink);
    mEncodeObjectDescription(circuit);
    mEncodeObjectDescription(pw);
    }

static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
    eAtRet ret = cAtOk;
    tAtEthVlanFlowConfig *configs = (tAtEthVlanFlowConfig *)pAllConfig;
    uint32 vlan_i;

    if (pAllConfig == NULL)
        return cAtErrorNullPointer;

    /* Get all VLAN at IG direction */
    configs->numberOfIgTrafficDesc = mMin(AtEthFlowIngressNumVlansGet(mThis(self)), cAtMaxIgTrafficDesc);
    for (vlan_i = 0; vlan_i < configs->numberOfIgTrafficDesc; vlan_i++)
        AtEthFlowIngressVlanAtIndex(mThis(self), vlan_i, &(configs->igTrafficDesc[vlan_i]));

    configs->enable = AtChannelIsEnabled(self);
    ret |= AtEthFlowEgressVlanGet((AtEthFlow)self, &(configs->egTrafficDesc));
    ret |= AtEthFlowEgressDestMacGet((AtEthFlow)self, (configs->destMacAddress));
    ret |= AtEthFlowEgressSourceMacGet((AtEthFlow)self, (configs->srcMacAddress));

    return ret;
    }

static eBool IsBusy(AtEthFlow self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void FlowControlSet(AtEthFlow self, AtEthFlowControl flowControl)
    {
    if (mFlowIsValid(self))
        self->flowControl = flowControl;
    }

static AtEthFlowControl FlowControlGet(AtEthFlow self)
    {
    AtEthFlowControl newController;

    if (self->flowControl)
        return self->flowControl;

    newController = mMethodsGet(self)->FlowControlCreate(self);
    if (newController == NULL)
        return NULL;

    FlowControlSet(self, newController);
    return self->flowControl;
    }

static AtEthFlowControl FlowControlCreate(AtEthFlow self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtModuleEthFlowControlCreate((AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth), self);
    }

static eAtRet EgressHeaderSet(AtEthFlow self, uint8 *egressSourceMac, uint8 *egressDestMac, const tAtEthVlanDesc *egressVlan)
    {
	AtUnused(egressVlan);
	AtUnused(egressDestMac);
	AtUnused(egressSourceMac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet EgressCVlanTpIdSet(AtEthFlow self, uint16 tpid)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(tpid);
    return cAtErrorNotImplemented;
    }

static uint16 EgressCVlanTpIdGet(AtEthFlow self)
    {
    return mMethodsGet(self)->EgressCVlanDefaultTpId(self);
    }

static eAtRet EgressSVlanTpIdSet(AtEthFlow self, uint16 tpid)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(tpid);
    return cAtErrorNotImplemented;
    }

static uint16 EgressSVlanTpIdGet(AtEthFlow self)
    {
    return mMethodsGet(self)->EgressSVlanDefaultTpId(self);
    }

static uint16 EgressCVlanDefaultTpId(AtEthFlow self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return AtModuleEthDefaultCVlanTpIdGet(ethModule);
    }

static uint16 EgressSVlanDefaultTpId(AtEthFlow self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return AtModuleEthDefaultSVlanTpIdGet(ethModule);
    }

static eAtRet PtchInsertionModeSet(AtEthFlow self, eAtPtchMode insertMode)
    {
    AtUnused(self);
    AtUnused(insertMode);
    return cAtErrorNotImplemented;
    }

static eAtPtchMode PtchInsertionModeGet(AtEthFlow self)
    {
    AtUnused(self);
    return cAtPtchModeUnknown;
    }

static eAtRet PtchServiceEnable(AtEthFlow self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PtchServiceIsEnabled(AtEthFlow self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPw PwGet(AtEthFlow self)
    {
    return self->pw;
    }

static void MethodsInit(AtEthFlow self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, EgressHeaderSet);
        mMethodOverride(m_methods, EgressDestMacSet);
        mMethodOverride(m_methods, EgressDestMacGet);
        mMethodOverride(m_methods, EgressSourceMacSet);
        mMethodOverride(m_methods, EgressSourceMacGet);
    	mMethodOverride(m_methods, EgressVlanSet);
        mMethodOverride(m_methods, EgressMacIsProgrammable);
        mMethodOverride(m_methods, IngressVlanAdd);
        mMethodOverride(m_methods, IngressVlanRemove);
        mMethodOverride(m_methods, IngressAllVlansGet);
        mMethodOverride(m_methods, IngressNumVlansGet);
        mMethodOverride(m_methods, IngressVlanAtIndex);
        mMethodOverride(m_methods, IngressVlanCanUse);
        mMethodOverride(m_methods, EgressVlanAdd);
        mMethodOverride(m_methods, EgressVlanRemove);
        mMethodOverride(m_methods, EgressNumVlansGet);
        mMethodOverride(m_methods, EgressVlanAtIndex);
        mMethodOverride(m_methods, EgressVlanCanUse);
        mMethodOverride(m_methods, IsBusy);
        mMethodOverride(m_methods, HdlcLinkGet);
        mMethodOverride(m_methods, MpBundleGet);
        mMethodOverride(m_methods, FrVirtualCircuitSet);
        mMethodOverride(m_methods, FrVirtualCircuitGet);
        mMethodOverride(m_methods, PwGet);
        mMethodOverride(m_methods, FlowControlCreate);
        mMethodOverride(m_methods, FlowControlGet);
        mMethodOverride(m_methods, EgressCVlanTpIdSet);
        mMethodOverride(m_methods, EgressCVlanTpIdGet);
        mMethodOverride(m_methods, EgressCVlanDefaultTpId);
        mMethodOverride(m_methods, EgressSVlanTpIdSet);
        mMethodOverride(m_methods, EgressSVlanTpIdGet);
        mMethodOverride(m_methods, EgressSVlanDefaultTpId);
        mMethodOverride(m_methods, PtchInsertionModeSet);
        mMethodOverride(m_methods, PtchInsertionModeGet);
        mMethodOverride(m_methods, PtchServiceEnable);
        mMethodOverride(m_methods, PtchServiceIsEnabled);
        }
        
    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtEthFlow self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthFlow self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AllConfigGet);
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthFlow self)
    {
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

AtEthFlow AtEthFlowObjectInit(AtEthFlow self, uint32 flowId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtEthFlow));
    
    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, flowId, (AtModule)module) == NULL)
        return NULL;
        
    /* Override */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

void AtEthFlowTypeSet(AtEthFlow self, uint8 flowType)
    {
    self->flowType = flowType;
    }

uint8 AtEthFlowTypeGet(AtEthFlow self)
    {
    if (self)
        return self->flowType;
    return cAtEthFlowTypeAny;
    }

void AtEthFlowUse(AtEthFlow self)
    {
    if (self)
        self->flowIsUsed = cAtTrue;
    }

eBool AtEthFlowIsUsed(AtEthFlow self)
    {
    if (self)
        return self->flowIsUsed;

    return cAtFalse;
    }

void AtEthFlowFlowControlSet(AtEthFlow self, AtEthFlowControl flowControl)
    {
    if (mFlowIsValid(self))
        self->flowControl = flowControl;
    }

eBool AtEthVlanDescIsValid(const tAtEthVlanDesc *vlan)
    {
    if (vlan == NULL)
        return cAtFalse;

    if ((vlan->numberOfVlans > 2))
        return cAtFalse;

    return cAtTrue;
    }

void AtEthFlowVcgSet(AtEthFlow self, AtVcg vcg)
    {
    if (self)
        self->vcg = vcg;
    }

AtVcg AtEthFlowVcgGet(AtEthFlow self)
    {
    return self ? self->vcg : NULL;
    }

eBool AtEthFlowIsBusy(AtEthFlow self)
    {
    if (self)
        return mMethodsGet(self)->IsBusy(self);
    return cAtFalse;
    }

void AtEthFlowBoundEncapChannelSet(AtEthFlow self, AtEncapChannel encapChannel)
    {
    if (self)
        self->encapChannel = encapChannel;
    }

eAtRet AtEthFlowHdlcLinkSet(AtEthFlow self, AtHdlcLink hdlcLink)
    {
    if (self == NULL)
        return cAtErrorNotExist;

    if (self->hdlcLink == hdlcLink)
        return cAtOk;

    if ((hdlcLink) && (self->hdlcLink))
        return cAtErrorRsrcNoAvail;

    self->hdlcLink = hdlcLink;
    return cAtOk;
    }

eAtRet AtEthFlowPtchInsertionModeSet(AtEthFlow self, eAtPtchMode insertMode)
    {
    if (self)
        return mMethodsGet(self)->PtchInsertionModeSet(self, insertMode);
    return cAtErrorObjectNotExist;
    }

eAtPtchMode AtEthFlowPtchInsertionModeGet(AtEthFlow self)
    {
    if (self)
        return mMethodsGet(self)->PtchInsertionModeGet(self);
    return cAtPtchModeUnknown;
    }

eAtRet AtEthFlowPtchServiceEnable(AtEthFlow self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PtchServiceEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

eBool AtEthFlowPtchServiceIsEnabled(AtEthFlow self)
    {
    if (self)
        return mMethodsGet(self)->PtchServiceIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtEthFlowFrVirtualCircuitSet(AtEthFlow self, AtFrVirtualCircuit circuit)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->FrVirtualCircuitSet(self, circuit);

    return cAtErrorNullPointer;
	}

void AtEthFlowHdlcBundleSet(AtEthFlow self, AtHdlcBundle bundle)
    {
    if (self)
        self->bundle = bundle;
    }

AtHdlcBundle AtEthFlowHdlcBundleGet(AtEthFlow self)
    {
    if (self)
        return self->bundle;
    return NULL;
    }

void AtEthFlowPwSet(AtEthFlow self, AtPw pw)
    {
    if (self)
        self->pw = pw;
    }

/*
 * Set VLAN traffic descriptor at Egress direction
 *
 * @param self This flow
 * @param desc VLAN traffic descriptor
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressVlanSet(AtEthFlow self, const tAtEthVlanDesc * desc)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressVlanSet(self, desc);

    return cAtError;
    }

/*
 * Get Egress traffic descriptor
 *
 * @param self This flow
 * @param desc LAN traffic descriptor
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressVlanGet(AtEthFlow self, tAtEthVlanDesc *desc)
    {
    if (!mFlowIsValid(self) || (desc == NULL))
        return cAtErrorNullPointer;

    AtOsalMemInit(desc, 0, sizeof(tAtEthVlanDesc));
    if (AtEthFlowEgressNumVlansGet(self) == 0)
        return cAtOk;

    if (AtEthFlowEgressVlanAtIndex(self, 0, desc) == NULL)
        return cAtError;

    return cAtOk;
    }

void AtUtilVlanDescriptorSerialize(AtObject descriptor, AtCoder encoder)
    {
    if (descriptor)
        SerializeVlanDescriptor(descriptor, encoder);
    }

/**
 * @addtogroup AtEthFlow
 * @{
 */

/**
 * Add VLAN traffic descriptor at Ingress direction
 *
 * @param self This flow
 * @param desc VLAN traffic descriptor
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowIngressVlanAdd(AtEthFlow self, const tAtEthVlanDesc * desc)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->IngressVlanAdd(self, desc);

    return cAtError;
    }

/**
 * Remove VLAN traffic descriptor at Ingress direction
 *
 * @param self This flow
 * @param desc VLAN traffic descriptor
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowIngressVlanRemove(AtEthFlow self, const tAtEthVlanDesc * desc)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->IngressVlanRemove(self, desc);
        
    return cAtError;
    }

/**
 * Get number of VLANs are added at ingress direction
 *
 * @param self This flow
 *
 * @return Number of VLANs
 */
uint32 AtEthFlowIngressNumVlansGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->IngressNumVlansGet(self);
    return 0;
    }

/**
 * To get VLAN description at specified index at ingress direction
 *
 * @param self This flow
 * @param vlanIndex VLAN index
 * @param [out] descs VLAN descriptor
 *
 * @return descs on success or NULL on failure
 *
 * @see AtEthFlowIngressNumVlansGet()
 */
tAtEthVlanDesc *AtEthFlowIngressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->IngressVlanAtIndex(self, vlanIndex, descs);
    return NULL;
    }

/**
 * Add VLAN traffic descriptor at Egress direction
 *
 * @param self This flow
 * @param desc VLAN traffic descriptor
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressVlanAdd(AtEthFlow self, const tAtEthVlanDesc * desc)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressVlanAdd(self, desc);

    return cAtError;
    }

/**
 * Remove VLAN traffic descriptor at Egress direction
 *
 * @param self This flow
 * @param desc VLAN traffic descriptor
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressVlanRemove(AtEthFlow self, const tAtEthVlanDesc * desc)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressVlanRemove(self, desc);

    return cAtError;
    }

/**
 * Get number of VLANs are added at egress direction
 *
 * @param self This flow
 *
 * @return Number of VLANs
 */
uint32 AtEthFlowEgressNumVlansGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressNumVlansGet(self);
    return 0;
    }

/**
 * To get VLAN description at specified index at egress direction
 *
 * @param self This flow
 * @param vlanIndex VLAN index
 * @param [out] descs VLAN descriptor
 *
 * @return descs on success or NULL on failure
 *
 * @see AtEthFlowEgressNumVlansGet()
 */
tAtEthVlanDesc *AtEthFlowEgressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressVlanAtIndex(self, vlanIndex, descs);
    return NULL;
    }

/**
 * Check if DA/SA can be programmed
 *
 * @param self This flow
 *
 * @return cAtTrue if MACs can be programmed, cAtFalse if MACs can not be programmed
 */
eBool AtEthFlowEgressMacIsProgrammable(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressMacIsProgrammable(self);

    return cAtFalse;
    }

/**
 * Set egress destination MAC address
 *
 * @param self This flow
 * @param address Pointer to input MAC address array
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressDestMacSet(AtEthFlow self, uint8 * address)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressDestMacSet(self, address);

    return cAtError;
    }

/**
 * Get flow's egress destination MAC address
 *
 * @param self This flow
 * @param address Pointer to output MAC address array
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressDestMacGet(AtEthFlow self, uint8 * address)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressDestMacGet(self, address);
        
    return cAtError;
    }

/**
 * Change Egress Source MAC not to use Port Source MAC (as default)
 *
 * @param self This flow
 * @param address Source MAC address
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressSourceMacSet(AtEthFlow self, uint8 *address)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressSourceMacSet(self, address);

    return cAtError;
    }

/**
 * Get Egress Source MAC
 *
 * @param self This flow
 * @param address Source MAC address
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressSourceMacGet(AtEthFlow self, uint8 *address)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressSourceMacGet(self, address);

    return cAtError;
    }

/**
 * Get Hdlc link
 *
 * @param self This flow
 *
 * @return Hdlc Link bound to flow
 */
AtHdlcLink AtEthFlowHdlcLinkGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->HdlcLinkGet(self);

    return NULL;
    }

/**
 * Get mp bundle
 *
 * @param self This flow
 *
 * @return mp bundle bound to flow
 */
AtMpBundle AtEthFlowMpBundleGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->MpBundleGet(self);

    return NULL;
    }

/**
 * Get Fr Virtual Circuit bound to flow
 *
 * @param self This flow
 *
 * @return Fr Virtual Circuit
 */
AtFrVirtualCircuit AtEthFlowFrVirtualCircuitGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->FrVirtualCircuitGet(self);

    return NULL;
    }

/**
 * Get bound encap channel
 *
 * @param self This flow
 *
 * @return bound encap channel
 */
AtEncapChannel AtEthFlowBoundEncapChannelGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return self->encapChannel;

    return NULL;
    }

/**
 * Get Pseudowire
 *
 * @param self This flow
 *
 * @return Pseudowire bound to flow
 */
AtPw AtEthFlowPwGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->PwGet(self);

    return NULL;
    }

/**
 * Get the flow control engine associated with this flow
 *
 * @param self This flow
 *
 * @return Ethernet Flow Control if hardware supports flow control. Otherwise,
 * NULL object is returned
 */
AtEthFlowControl AtEthFlowFlowControlGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->FlowControlGet(self);

    return NULL;
    }

/**
 * Set egress header
 *
 * @param self This flow
 * @param egressSourceMac Pointer to input Source MAC address array
 * @param egressDestMac Pointer to input Dest MAC address array
 * @param egressVlan VLAN traffic descriptor
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressHeaderSet(AtEthFlow self, uint8 *egressSourceMac, uint8 *egressDestMac, const tAtEthVlanDesc *egressVlan)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressHeaderSet(self, egressSourceMac, egressDestMac, egressVlan);

    return cAtErrorNullPointer;
    }

/**
 * Set type of CVLAN
 *
 * @param self This flow
 * @param tpid TPID
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressCVlanTpIdSet(AtEthFlow self, uint16 tpid)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressCVlanTpIdSet(self, tpid);

    return cAtErrorNullPointer;
    }

/**
 * Get type of CVLAN
 *
 * @param self This flow
 *
 * @return type of CVLAN
 */
uint16 AtEthFlowEgressCVlanTpIdGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressCVlanTpIdGet(self);

    return 0x0;
    }

/**
 * Set SVLAN TPID
 *
 * @param self This flow
 * @param tpid TPID
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowEgressSVlanTpIdSet(AtEthFlow self, uint16 tpid)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressSVlanTpIdSet(self, tpid);

    return cAtErrorNullPointer;
    }

/**
 * Get SVLAN TPID
 *
 * @param self This flow
 *
 * @return type of SVLAN
 */
uint16 AtEthFlowEgressSVlanTpIdGet(AtEthFlow self)
    {
    if (mFlowIsValid(self))
        return mMethodsGet(self)->EgressSVlanTpIdGet(self);

    return 0x0;
    }

/**
 * @}
 */

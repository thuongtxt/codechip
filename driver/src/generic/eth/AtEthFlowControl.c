/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtEthFlowControl.c
 *
 * Created Date: Oct 4, 2013
 *
 * Description : Flow control
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthFlowControlInternal.h"

#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h" /* For read/write */
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mFlowControlIsValid(self) (self ? cAtTrue : cAtFalse)
#define cInvalidRegAddr 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (AtEthFlowControl)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEthFlowControlMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEthRet HighThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
	AtUnused(threshold);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 HighThresholdGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static eAtModuleEthRet LowThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
	AtUnused(threshold);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 LowThresholdGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static eAtModuleEthRet DestMacSet(AtEthFlowControl self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleEthRet DestMacGet(AtEthFlowControl self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleEthRet SourceMacSet(AtEthFlowControl self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleEthRet SourceMacGet(AtEthFlowControl self, uint8 *address)
    {
	AtUnused(address);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleEthRet EthTypeSet(AtEthFlowControl self, uint32 ethType)
    {
	AtUnused(ethType);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 EthTypeGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static eAtModuleEthRet PauseFramePeriodSet(AtEthFlowControl self, uint32 timeInterval)
    {
	AtUnused(timeInterval);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 PauseFramePeriodGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static uint16 TxPauseFrameCounterGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static uint16 TxPauseFrameCounterClear(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static uint16 RxPauseFrameCounterGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static uint16 RxPauseFrameCounterClear(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 TxFifoWaterMarkMinGet(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TxFifoWaterMarkMinClear(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TxFifoWaterMarkMaxGet(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TxFifoWaterMarkMaxClear(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TxFifoLevelGet(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet Enable(AtEthFlowControl self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eBool IsEnabled(AtEthFlowControl self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet PauseFrameQuantaSet(AtEthFlowControl self, uint32 quanta)
    {
	AtUnused(quanta);
	AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 PauseFrameQuantaGet(AtEthFlowControl self)
    {
	AtUnused(self);
    return 0;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtEthFlowControl object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(channel);
    }

static void OverrideAtObject(AtEthFlowControl self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtEthFlowControl self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtEthFlowControl self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HighThresholdSet);
        mMethodOverride(m_methods, HighThresholdGet);
        mMethodOverride(m_methods, LowThresholdSet);
        mMethodOverride(m_methods, LowThresholdGet);
        mMethodOverride(m_methods, DestMacSet);
        mMethodOverride(m_methods, DestMacGet);
        mMethodOverride(m_methods, SourceMacSet);
        mMethodOverride(m_methods, SourceMacGet);
        mMethodOverride(m_methods, EthTypeSet);
        mMethodOverride(m_methods, EthTypeGet);
        mMethodOverride(m_methods, PauseFramePeriodSet);
        mMethodOverride(m_methods, PauseFramePeriodGet);
        mMethodOverride(m_methods, PauseFrameQuantaSet);
        mMethodOverride(m_methods, PauseFrameQuantaGet);
        mMethodOverride(m_methods, TxPauseFrameCounterGet);
        mMethodOverride(m_methods, TxPauseFrameCounterClear);
        mMethodOverride(m_methods, RxPauseFrameCounterGet);
        mMethodOverride(m_methods, RxPauseFrameCounterClear);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, TxFifoWaterMarkMinGet);
        mMethodOverride(m_methods, TxFifoWaterMarkMinClear);
        mMethodOverride(m_methods, TxFifoWaterMarkMaxGet);
        mMethodOverride(m_methods, TxFifoWaterMarkMaxClear);
        mMethodOverride(m_methods, TxFifoLevelGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEthFlowControl);
    }

AtEthFlowControl AtEthFlowControlObjectInit(AtEthFlowControl self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->channel = channel;

    return self;
    }

uint32 AtEthFlowControlRead(AtEthFlowControl self, uint32 address, eAtModule moduleId)
    {
    if (mFlowControlIsValid(self))
    	return mChannelHwRead(self->channel, address, moduleId);
    return cInvalidRegAddr;
    }

void AtEthFlowControlWrite(AtEthFlowControl self, uint32 address, uint32 value, eAtModule moduleId)
    {
    if (mFlowControlIsValid(self))
    	mChannelHwWrite(self->channel, address, value, moduleId);
    }

/**
 * Set Quanta time of Pause Frame
 *
 * @param self This port flow Control
 * @param quanta Pause Frame Quata
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlPauseFrameQuantaSet(AtEthFlowControl self, uint32 quanta)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->PauseFrameQuantaSet(self, quanta);

    return cAtError;
    }

/**
 * Get Quanta time of Pause Frame
 *
 * @param self This flow Control
 *
 * @return The time that is required for transmitting 512 bits the speed of the port
 */
uint32 AtEthFlowControlPauseFrameQuantaGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->PauseFrameQuantaGet(self);

    return 0;
    }

/**
 * Configuration buffer threshold full, unit is in percent
 *
 * @param self This port flow Control
 * @param threshold Value threshold
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlHighThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->HighThresholdSet(self, threshold);

    return cAtError;
    }

/**
 * Get value buffer threshold full, unit is in percent
 * @param self This port flow Control
 *
 * @return Value threshold
 */
uint32 AtEthFlowControlHighThresholdGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->HighThresholdGet(self);

    return 0;
    }

/**
 * Configuration buffer empty threshold, unit is in percent
 * @param self This port flow Control
 * @param threshold Value threshold
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlLowThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->LowThresholdSet(self, threshold);

    return cAtError;
    }

/**
 * Get buffer empty threshold, unit is in percent
 *
 * @param self This port flow Control
 *
 * @return Value threshold
 */
uint32 AtEthFlowControlLowThresholdGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->LowThresholdGet(self);

    return 0;
    }

/**
 * Get the channel that this flow control is working on
 *
 * @param self This flow control
 *
 * @return Channel that this flow control is working on
 */
AtChannel AtEthFlowControlChannelGet(AtEthFlowControl self)
    {
    if (!mFlowControlIsValid(self))
        return NULL;

    return self->channel;
    }

/**
 * Set Ethernet Flow Control Destination MAC address
 *
 * @param self This flow control engine
 * @param address Pointer to input MAC address array
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlDestMacSet(AtEthFlowControl self, uint8 *address)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->DestMacSet(self, address);

    return cAtError;
    }

/**
 * Get Ethernet Flow Control Destination MAC address
 * @param self This flow control engine
 * @param address Pointer to get MAC address array
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlDestMacGet(AtEthFlowControl self, uint8 *address)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->DestMacGet(self, address);

    return cAtError;
    }

/**
 * Set Ethernet Flow Control Source MAC address
 * @param self This flow control engine
 * @param address Pointer to input MAC address array
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlSourceMacSet(AtEthFlowControl self, uint8 *address)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->SourceMacSet(self, address);

    return cAtError;
    }

/**
 * Get Ethernet Flow Control Source MAC address
 * @param self This flow control engine
 * @param address Pointer to get MAC address array
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlSourceMacGet(AtEthFlowControl self, uint8 *address)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->SourceMacGet(self, address);

    return cAtError;
    }

/**
 * Set Ethernet Flow Control ETH type
 * @param self This flow control engine
 * @param ethType value Ethernet type
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlEthTypeSet(AtEthFlowControl self, uint32 ethType)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->EthTypeSet(self, ethType);

    return cAtError;
    }

/**
 * Get Ethernet Flow Control ETH type configuration
 * @param self This flow control engine
 *
 * @return ethType Value Ethernet type
 */
uint32 AtEthFlowControlEthTypeGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->EthTypeGet(self);

    return 0;
    }

/**
 * Set PAUSE frame period
 *
 * @param self This port flow Control
 * @param periodMs PAUSE frame period in milliseconds
 *
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlPauseFramePeriodSet(AtEthFlowControl self, uint32 periodMs)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->PauseFramePeriodSet(self, periodMs);

    return cAtError;
    }

/**
 * Get PAUSE frame period
 *
 * @param self This flow Control
 *
 * @return PAUSE frame period in milliseconds
 */
uint32 AtEthFlowControlPauseFramePeriodGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->PauseFramePeriodGet(self);

    return 0;
    }

/**
 * Get performance counter at transmit of ethernet port
 *
 * @param self This Flow
 * @return Counter value
 */
uint16 AtEthFlowControlTxPauseFrameCounterGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->TxPauseFrameCounterGet(self);

    return 0;
    }

/**
 * Clear performance counter at transmit of ethernet port
 *
 * @param self This Flow
 * @return Value of counter before clearing
 */
uint16 AtEthFlowControlTxPauseFrameCounterClear(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->TxPauseFrameCounterClear(self);

    return 0;
    }

/**
 * Get performance counter at receive of ethernet port
 *
 * @param self This Flow
 * @return Counter value
 */
uint16 AtEthFlowControlRxPauseFrameCounterGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->RxPauseFrameCounterGet(self);

    return 0;
    }

/**
 * Clear performance counter at receive of ethernet port
 *
 * @param self This Flow
 * @return Value of counter before clearing
 */
uint16 AtEthFlowControlRxPauseFrameCounterClear(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->RxPauseFrameCounterClear(self);

    return 0;
    }

/**
 * Enable/disable flow control
 *
 * @param self This Flow
 * @param enable Enable/disable
 * @return AT return code
 */
eAtModuleEthRet AtEthFlowControlEnable(AtEthFlowControl self, eBool enable)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->Enable(self, enable);

    return cAtError;
    }

/**
 * Get Flow control is enabled or disabled
 *
 * @param self This Flow
 * @return Enabled/disabled
 */
eBool AtEthFlowControlIsEnabled(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->IsEnabled(self);

    return cAtFalse;
    }

/**
 * Get FIFO max watermark at transmit of ethernet port
 *
 * @param self This Flow
 * @return fifo max watermark
 */
uint32 AtEthFlowControlTxFifoWaterMarkMaxGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->TxFifoWaterMarkMaxGet(self);

    return 0;
    }

/**
 * Get FIFO max watermark at transmit of ethernet port and clear the wartermark
 *
 * @param self This Flow
 * @return fifo max watermark
 */
uint32 AtEthFlowControlTxFifoWaterMarkMaxClear(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->TxFifoWaterMarkMaxClear(self);

    return 0;
    }

/**
 * Get FIFO min watermark at transmit of ethernet port
 *
 * @param self This Flow
 * @return fifo min watermark
 */
uint32 AtEthFlowControlTxFifoWaterMarkMinGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->TxFifoWaterMarkMinGet(self);

    return 0;
    }

/**
 * Get FIFO min watermark at transmit of ethernet port and clear the wartermark
 *
 * @param self This Flow
 * @return fifo min watermark
 */
uint32 AtEthFlowControlTxFifoWaterMarkMinClear(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->TxFifoWaterMarkMinClear(self);

    return 0;
    }

/**
 * Get FIFO fill level of Ethernet port at transmit direction
 *
 * @param self This Flow
 *
 * @return TX FIFO fill level
 */
uint32 AtEthFlowControlTxFifoLevelGet(AtEthFlowControl self)
    {
    if (mFlowControlIsValid(self))
        return mMethodsGet(self)->TxFifoLevelGet(self);
    return 0;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Platform
 *
 * File        : AtXauiMdio.c
 *
 * Created Date: Jul 17, 2015
 *
 * Description : MDIO
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"
#include "commacro.h"
#include "AtHal.h"
#include "AtXauiMdio.h"
#include "AtMdio.h"
#include "AtModuleEth.h"
#include "../AtModuleEthInternal.h"
#include "../../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cXauiResetRegister        0xf00044
#define cXauiResetMask            cBit0
#define cXauiResetShift           0
#define cXauiResetLocalFaultMask  cBit10
#define cXauiResetLocalFaultShift 10
#define cXauiResetLinkStateMask   cBit11
#define cXauiResetLinkStateShift  11
#define cXauiLoopBackMask         cBit18_16 /* 0: normal/1:PCS loop-in/2:PMA loop-in/4:PMA loop-out/6:PCS loop-out */
#define cXauiLoopBackShift        16
#define cXauiRxPolarityMask       cBit23_20 /* should be 4'b1000 in ALU system version A */
#define cXauiRxPolarityShift      20
#define cXauiTxPolarityMask       cBit27_24 /* should be 4'b1111 in ALU system version A */
#define cXauiTxPolarityShift      24

#define cXauiStatusRegister       0xf0006a
#define cXauiTxReadyMask          cBit1
#define cXauiTxReadyShift         1
#define cXauiTxLocalFaultMask     cBit8 /* need to reset this alarm at the first time after de-reset XAUI */
#define cXauiTxLocalFaultShift    8
#define cXauiRxLocalFaultMask     cBit9 /* need to reset this alarm at the first time after de-reset XAUI */
#define cXauiRxLocalFaultShift    9
#define cXauiRxSynMask            cBit13_10
#define cXauiRxSynShift           10
#define cXauiRxAlignMask          cBit14
#define cXauiRxAlignShift         14
#define cXauiLinkStateMask        cBit15
#define cXauiLinkStateShift       15 /* link state 1:UP, 0:DOWN */

#define cXauiTxNotReady   cBit0
#define cXauiTxLocalFault cBit1
#define cXauiRxLocalFault cBit2
#define cXauiRxNotSyn     cBit3
#define cXauiRxNotAlign   cBit4
#define cXauiLinkDown     cBit5

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AlarmReset(AtHal hal)
    {
    uint32 regVal = AtHalRead(hal, cXauiResetRegister);
    mRegFieldSet(regVal, cXauiResetLocalFault, 1);
    mRegFieldSet(regVal, cXauiResetLinkState, 1);
    AtHalWrite(hal, cXauiResetRegister, regVal);
    }

static AtMdio Mdio(void)
    {
    AtDevice device = AtDriverDeviceGet(AtDriverSharedDriverGet(), 0);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthSerdesMdio(ethModule, NULL);
    }

uint32 AtXauiMdioRead(AtHal hal, uint32 page, uint32 address)
    {
    AtUnused(hal);
    AtMdioPortSelect(Mdio(), 0);
    return AtMdioRead(Mdio(), page, address);
    }

void AtXauiMdioWrite(AtHal hal, uint32 page, uint32 address, uint32 value)
    {
    AtUnused(hal);
    AtMdioPortSelect(Mdio(), 0);
    AtMdioWrite(Mdio(), page, address, value);
    }

/*
 * Reset XAUI
 *
 * @param hal Hal for read/write
 * @param reset
 *         - cAtTrue Start reset
 *         - cAtFalse Stop reset
 *
 * @return At return code
 */
eAtRet AtXauiReset(AtHal hal, eBool reset)
    {
    const uint8 cDefaultRxPolarity = 0x8;
    const uint8 cDefaultTxPolarity = 0xF;
    const uint8 cNoLoop = 0;
    uint32 regVal = 0;

    mRegFieldSet(regVal, cXauiReset,      (reset) ? 1 : 0);
    mRegFieldSet(regVal, cXauiLoopBack,   cNoLoop);
    mRegFieldSet(regVal, cXauiRxPolarity, cDefaultRxPolarity);
    mRegFieldSet(regVal, cXauiTxPolarity, cDefaultTxPolarity);

    AtHalWrite(hal, cXauiResetRegister, regVal);

    return cAtOk;
    }

/*
 * Get XAUI status
 *
 * @param hal Hal for read/write
 * @param clear Reading mode
 *         - cAtTrue Read then clear
 *         - cAtFalse Read only
 *
 * @return XAUI status, return 0 when everything is good. If return value is larger than 0
 *         - bit 0 : Tx not ready
 *         - bit 1 : Tx local fault
 *         - bit 2 : Rx local fault
 *         - bit 3 : Rx not SYN
 *         - bit 4 : RX not align
 *         - bit 5 : Link down
 */
uint32 AtXauiStatusRead2Clear(AtHal hal, eBool clear)
    {
    uint32 regVal, status = 0;

    regVal = AtHalRead(hal, cXauiStatusRegister);

    if ((regVal & cXauiTxReadyMask) == 0)
        status |= cXauiTxNotReady;
    if (regVal & cXauiTxLocalFaultMask)
        status |= cXauiTxLocalFault;
    if (regVal & cXauiRxLocalFaultMask)
        status |= cXauiRxLocalFault;
    if ((regVal & cXauiRxSynMask) != cXauiRxSynMask)
        status |= cXauiRxNotSyn;
    if ((regVal & cXauiRxAlignMask) == 0)
        status |= cXauiRxNotAlign;
    if ((regVal & cXauiLinkStateMask) == 0)
        status |= cXauiLinkDown;

    if (clear)
        AlarmReset(hal);

    return status;
    }

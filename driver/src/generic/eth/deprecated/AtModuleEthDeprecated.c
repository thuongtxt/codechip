/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtModuleEthDeprecated.c
 *
 * Created Date: Jul 31, 2014
 *
 * Description : ETH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Create Network protocol (IP, MPLS,...) over packet flow
 * @param self This module
 * @param flowId Flow ID
 * @return Instance of Ethernet flow
 */
AtEthFlow AtModuleEthNopFlowCreate(AtModuleEth self, uint16 flowId)
    {
    if (self)
        return mMethodsGet(self)->NopFlowCreate(self, flowId);

    return NULL;
    }

/*
 * Create Ethernet over packet flow
 * @param self This module
 * @param flowId Flow ID
 * @return Instance of Ethernet flow
 */
AtEthFlow AtModuleEthEopFlowCreate(AtModuleEth self, uint16 flowId)
    {
    if (self)
        return mMethodsGet(self)->EopFlowCreate(self, flowId);

    return NULL;
    }

/*
 * Create Ethernet over PDH
 * @param self This module
 * @param flowId Flow ID
 * @return Instance of Ethernet flow
 */
AtEthFlow AtModuleEthEoPDHFlowCreate(AtModuleEth self, uint16 flowId)
    {
    return AtModuleEthFlowCreate(self, flowId);
    }

/*
 * Create Ethernet over SONET/SDH
 * @param self This module
 * @param flowId Flow ID
 * @return Instance of Ethernet flow
 */
AtEthFlow AtModuleEthEoSFlowCreate(AtModuleEth self, uint16 flowId)
    {
    return AtModuleEthFlowCreate(self, flowId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : AtEthFlowDeprecated.c
 *
 * Created Date: Sep 3, 2015
 *
 * Description : For deprecated functions and APIs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtEthFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Get all VLAN traffic descriptors at Ingress direction
 *
 * @param self This flow
 * @param descs Pointer to output array of descriptors. This parameter may be null, if it
 *              is null, number of descriptors is returned only
 *
 * @return Number of VLAN traffic descriptors
 */
uint32 AtEthFlowIngressAllVlansGet(AtEthFlow self, tAtEthVlanDesc * descs)
    {
    if (self)
        return mMethodsGet(self)->IngressAllVlansGet(self, descs);

    return 0;
    }

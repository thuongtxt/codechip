/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Ethernet
 *
 * File        : AtModuleEth.c
 *
 * Created Date: Aug 7, 2012
 *
 * Description : Ethernet module
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../platform/include/AtDrp.h"
#include "../../util/AtIteratorInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "AtModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModuleIsValid(self) (self ? cAtTrue : cAtFalse)
#define mThis(self) ((AtModuleEth)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleEthMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;
static const tAtObjectMethods *m_AtObjectMethods;
static const tAtModuleMethods *m_AtModuleMethods;

/* Array Iterator initialize */
static char m_flowIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_flowAtArrayIteratorOverride;
static tAtIteratorMethods m_flowAtIteratorOverride;

static char m_portIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_portAtArrayIteratorOverride;
static tAtIteratorMethods m_portAtIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/
static eBool FlowIsValid(AtModuleEth self, uint16 flowId);

/*--------------------------- Implementation ---------------------------------*/
static eBool FlowAtIndexIsValid(AtArrayIterator self, uint32 flowIndex)
    {
    AtEthFlow *flows = self->array;
    return AtEthFlowIsUsed(flows[flowIndex]);
    }

static AtObject FlowAtIndex(AtArrayIterator self, uint32 flowIndex)
    {
    AtEthFlow *flows = self->array;
    return (AtObject)flows[flowIndex];
    }

static uint16 AtModuleEthFlowCountGet(AtArrayIterator self)
    {
    uint16 count = 0;
    uint16 i;
    AtEthFlow *flows = self->array;

    if (NULL == flows)
        return 0;

    for (i = 0; i < self->capacity; i++)
        if (AtEthFlowIsUsed(flows[i]))
            count++;
    return count;
    }

static uint32 FlowCount(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count >= 0)
        return (uint32)arrayIterator->count;

    arrayIterator->count = (int32)AtModuleEthFlowCountGet(arrayIterator);
    return (uint32)arrayIterator->count;
    }

static void FlowIteratorOverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_flowIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_flowAtArrayIteratorOverride, mMethodsGet(self), sizeof(m_flowAtArrayIteratorOverride));
        m_flowAtArrayIteratorOverride.DataAtIndexIsValid = FlowAtIndexIsValid;
        m_flowAtArrayIteratorOverride.DataAtIndex = FlowAtIndex;
        }
    mMethodsSet(self, &m_flowAtArrayIteratorOverride);
    }

static void FlowIteratorOverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_flowIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_flowAtIteratorOverride, mMethodsGet(iterator), sizeof(m_flowAtIteratorOverride));
        m_flowAtIteratorOverride.Count = FlowCount;
        }
    mMethodsSet(iterator, &m_flowAtIteratorOverride);
    }

static void FlowIteratorOverride(AtArrayIterator self)
    {
    FlowIteratorOverrideAtArrayIterator(self);
    FlowIteratorOverrideAtIterator(self);
    }

static eBool PortAtIndexIsValid(AtArrayIterator self, uint32 portIndex)
    {
	AtUnused(portIndex);
	AtUnused(self);
    return cAtTrue;
    }

static AtObject PortAtIndex(AtArrayIterator self, uint32 portIndex)
    {
    AtEthFlow *ports = self->array;
    if (ports == NULL)
        return NULL;

    return (AtObject)ports[portIndex];
    }

static uint32 PortCount(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count >= 0)
        return (uint32)arrayIterator->count;

    arrayIterator->count = (int32)arrayIterator->capacity;
    return (uint32)arrayIterator->count;
    }

static void PortIteratorOverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_portIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_portAtArrayIteratorOverride, mMethodsGet(self), sizeof(m_portAtArrayIteratorOverride));
        m_portAtArrayIteratorOverride.DataAtIndexIsValid = PortAtIndexIsValid;
        m_portAtArrayIteratorOverride.DataAtIndex = PortAtIndex;
        }
    mMethodsSet(self, &m_portAtArrayIteratorOverride);
    }

static void PortIteratorOverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_portIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_portAtIteratorOverride, mMethodsGet(iterator), sizeof(m_portAtIteratorOverride));
        m_portAtIteratorOverride.Count = PortCount;
        }
    mMethodsSet(iterator, &m_portAtIteratorOverride);
    }

static void PortIteratorOverride(AtArrayIterator self)
    {
    PortIteratorOverrideAtArrayIterator(self);
    PortIteratorOverrideAtIterator(self);
    }

static eAtRet AllPortsInit(AtModuleEth self)
    {
    eAtRet ret = cAtOk;
    uint8 numPorts = (uint8)AtModuleEthNumberOfCreatedPortsGet(self);
    uint8 i;

    if (self->ethPorts == NULL)
        return cAtOk;

    for (i = 0; i < numPorts; i++)
        {
        AtEthPort port = self->ethPorts[i];

        if (!mMethodsGet(self)->PortCanBeUsed(self, i))
            continue;

        ret |= AtChannelInit((AtChannel)port);

        /* Reset SERDES APS */
        if (mMethodsGet(self)->SerdesApsSupportedOnPort(self, port))
            {
            AtSerdesController defaultSerdes;

            /* Some products do not support release bridging */
            if (AtEthPortTxSerdesCanBridge(port, NULL))
                ret |= AtEthPortTxSerdesBridge(port, NULL);

            defaultSerdes = AtEthPortSerdesController(port);
            if (defaultSerdes)
                ret |= AtEthPortRxSerdesSelect(port, defaultSerdes);
            }
        }

    return ret;
    }

static eAtRet AllPortsCreate(AtModuleEth self)
    {
    /* Allocate memory to hold all port objects */
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 numPorts = mMethodsGet(self)->MaxPortsGet(self);
    uint8 port_i;
    AtEthPort *allPorts;
    uint32 memorySize;

    if (numPorts == 0)
        return cAtOk;

    memorySize = sizeof(AtEthPort) * numPorts;
    allPorts = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allPorts == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allPorts, 0, memorySize);

    /* Create all port objects */
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        uint8 createdPort_i;

        /* If cannot create one port, free all allocated resources */
        allPorts[port_i] = mMethodsGet(self)->PortCreate(self, port_i);
        if (allPorts[port_i] == NULL)
            {
            for (createdPort_i = 0; createdPort_i < port_i; createdPort_i++)
                AtObjectDelete((AtObject)allPorts[createdPort_i]);
            mMethodsGet(osal)->MemFree(osal, allPorts);

            return cAtErrorRsrcNoAvail;
            }
        }

    /* Update database */
    self->ethPorts = allPorts;
    self->numPorts = numPorts;

    return cAtOk;
    }

static eAtRet AllPortsDelete(AtModuleEth self)
    {
    uint32 i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Do nothing if there is no port */
    if (self->ethPorts == NULL)
        return cAtOk;

    /* Delete all ports first */
    for (i = 0; i < self->numPorts; i++)
        AtObjectDelete((AtObject)(self->ethPorts[i]));

    /* Free memory that hold all ports */
    mMethodsGet(osal)->MemFree(osal, self->ethPorts);
    self->ethPorts = NULL;

    return cAtOk;
    }

static eAtRet AllFlowsInit(AtModuleEth self)
    {
    uint16 i;
    eAtRet ret = cAtOk;
    uint16 numFlows = mMethodsGet(self)->MaxFlowsGet(self);

    if (self->ethFlows == NULL)
        return cAtOk;

    for (i = 0; i < numFlows; i++)
        ret |= AtChannelInit((AtChannel)(self->ethFlows[i]));

    return ret;
    }

static eAtRet AllFlowsCreate(AtModuleEth self)
    {
    /* Allocate memory to hold all port objects */
    AtOsal osal = AtSharedDriverOsalGet();
    uint16 numFlows = mMethodsGet(self)->MaxFlowsGet(self);
    uint16 i, j;
    AtEthFlow *allFlows;

    if (numFlows == 0)
        return cAtOk;

    allFlows = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtEthFlow) * numFlows);
    if (allFlows == NULL)
        return cAtErrorRsrcNoAvail;

    /* Create all port objects */
    for (i = 0; i < numFlows; i++)
        {
        /* If cannot create one port, free all allocated resources */
        allFlows[i] = mMethodsGet(self)->FlowObjectCreate(self, i);
        if (allFlows[i] == NULL)
            {
            for (j = 0; j < i; j++)
                AtObjectDelete((AtObject)allFlows[j]);

            mMethodsGet(osal)->MemFree(osal, allFlows);
            return cAtErrorRsrcNoAvail;
            }

        if (mMethodsGet(self)->StaticFlowManagement(self))
            AtEthFlowUse(allFlows[i]);
        }

    /* Update database */
    self->ethFlows = allFlows;

    return cAtOk;
    }

static eAtRet AllFlowsDelete(AtModuleEth self)
    {
    uint16 numFlows, i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Do nothing if there is no flow */
    if (self->ethFlows == NULL)
        return cAtOk;

    /* Delete all ports first */
    numFlows = mMethodsGet(self)->MaxFlowsGet(self);
    for (i = 0; i < numFlows; i++)
        AtObjectDelete((AtObject)(self->ethFlows[i]));

    /* Free memory that hold all ports */
    mMethodsGet(osal)->MemFree(osal, self->ethFlows);
    self->ethFlows = NULL;

    return cAtOk;
    }

static eAtRet AllChannelsCreate(AtModuleEth self)
    {
    return AllPortsCreate(self) | AllFlowsCreate(self);
    }

static eAtRet AllChannelsDelete(AtModuleEth self)
    {
    return AllPortsDelete(self) | AllFlowsDelete(self);
    }

static eAtRet DefaultVlanTpidSetup(AtModuleEth self)
    {
    self->cVlanTpid = mMethodsGet(self)->CVlanDefaultTpid(self);
    self->sVlanTpid = mMethodsGet(self)->SVlanDefaultTpid(self);
    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    AtModuleEth ethModule;
    eAtRet ret;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Just destroy and create all channels again to have a fresh memory */
    ethModule = (AtModuleEth)self;
    DefaultVlanTpidSetup(ethModule);
    return AllChannelsDelete(ethModule) | AllChannelsCreate(ethModule);
    }

static eAtRet Init(AtModule self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;
    eAtRet ret;

    /* Super init */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Initialize all managed objects */
    ret |= AllFlowsInit(ethModule);
    ret |= AllPortsInit(ethModule);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    AllChannelsDelete(mThis(self));
    AtMdioDelete(mThis(self)->serdesMdio);
    mThis(self)->serdesMdio = NULL;
    AtDrpDelete(mThis(self)->serdesDrp);
    mThis(self)->serdesDrp = NULL;

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eBool PortIsValid(AtModuleEth self, uint8 portId)
    {
    return portId < self->numPorts;
    }

static AtEthPort EthPortGet(AtModuleEth self, uint8 portId)
    {
    /* Database has not been allocated */
    if (self->ethPorts == NULL)
        return NULL;

    /* Get corresponding object from database */
    return PortIsValid(self, portId) ? self->ethPorts[portId] : NULL;
    }
    
static uint16 MaxFlowsGet(AtModuleEth self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static AtEthFlow CreateFlowByType(AtModuleEth self, uint16 flowId, eAtEthFlowType flowType)
    {
    AtEthFlow flow = self->ethFlows[flowId];

    /* Flow is busy */
    if (AtEthFlowIsUsed(flow))
        return NULL;

    /* Set flow type */
    AtEthFlowTypeSet(flow, (uint8)flowType);

    /* Get resource */
    AtEthFlowUse(flow);

    AtChannelInit((AtChannel)flow);
    return flow;
    }

static eBool FlowIsValid(AtModuleEth self, uint16 flowId)
    {
    return (flowId < mMethodsGet(self)->MaxFlowsGet(self));
    }

static AtEthFlow NopFlowCreate(AtModuleEth self, uint16 flowId)
    {
    if (FlowIsValid(self, flowId))
        return CreateFlowByType(self, flowId, cAtEthFlowTypeNpoPpp);
    return NULL;
    }
    
static AtEthFlow EopFlowCreate(AtModuleEth self, uint16 flowId)
    {
    if (FlowIsValid(self, flowId))
        return CreateFlowByType(self, flowId, cAtEthFlowTypeEthoPpp);
    return NULL;
    }

static AtEthFlow FlowCreate(AtModuleEth self, uint16 flowId)
    {
    if (FlowIsValid(self, flowId))
        return CreateFlowByType(self, flowId, cAtEthFlowTypeAny);
    return NULL;
    }

static AtEthFlow FlowGet(AtModuleEth self, uint16 flowId)
    {
	AtEthFlow flow;
	
    /* Database has not been created */
    if (self->ethFlows == NULL)
        return NULL;

    /* Return corresponding object in database */
    flow = FlowIsValid(self, flowId) ? self->ethFlows[flowId] : NULL;
    if ((flow == NULL) || (!AtEthFlowIsUsed(flow)))
        return NULL;

    return flow;
    }

static void AllIngressVlanRemove(AtEthFlow flow)
    {
    while (AtEthFlowIngressNumVlansGet(flow) > 0)
        {
        tAtEthVlanDesc descriptor;
        AtEthFlowIngressVlanAtIndex(flow, 0, &descriptor);
        AtEthFlowIngressVlanRemove(flow, &descriptor);
        }
    }

static eAtModuleEthRet FlowDelete(AtModuleEth self, uint16 flowId)
    {
    /* Do nothing if it has not been used */
    AtEthFlow flow = self->ethFlows[flowId];
    if (!AtEthFlowIsUsed(flow))
        return cAtOk;

    /* If flow is added to a link or bundle, do not allow to delete it, return error */
    if (mMethodsGet(flow)->IsBusy(flow))
        return cAtErrorChannelBusy;

    /* Remove all vlans to make them available for another flow */
    AllIngressVlanRemove(flow);

    /* Delete and create it again to have a refresh state */
    AtObjectDelete((AtObject)flow);
    self->ethFlows[flowId] = mMethodsGet(self)->FlowObjectCreate(self, flowId);

    return cAtOk;
    }
    
static uint16 FreeFlowIdGet(AtModuleEth self)
    {
    uint16 numFlows, i;

    /* Just scan the list and find one flow that has not been used */
    numFlows = mMethodsGet(self)->MaxFlowsGet(self);
    for (i = 0; i < numFlows; i++)
        {
        if (!AtEthFlowIsUsed(self->ethFlows[i]))
            return i;
        }

    /* Return invalid flow ID */
    return numFlows;
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
	AtUnused(portId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtEthFlow FlowObjectCreate(AtModuleEth self, uint16 flowId)
    {
	AtUnused(flowId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

/* ISIS MAC */
static eAtModuleEthRet ISISMacSet(AtModuleEth self, const uint8 *mac)
    {
	AtUnused(mac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleEthRet ISISMacGet(AtModuleEth self, uint8 *mac)
    {
	AtUnused(mac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "ethernet";
    }

static const char *CapacityDescription(AtModule self)
    {
    AtModuleEth moduleEth = (AtModuleEth)self;
    static char string[64];

    AtSnprintf(string, sizeof(string) - 1,
               "ports: %d, flows: %d, serdes: %d",
               AtModuleEthMaxPortsGet(moduleEth),
               AtModuleEthMaxFlowsGet(moduleEth),
               AtModuleEthNumSerdesControllers(moduleEth));

    return string;
    }

static void ChannelsStatusClear(AtModule self, AtIterator (*ChannelIteratorCreate)(AtModuleEth self))
    {
    AtIterator channelIterator = ChannelIteratorCreate((AtModuleEth)self);
    AtChannel channel;

    while ((channel = (AtChannel)AtIteratorNext(channelIterator)) != NULL)
        AtChannelStatusClear(channel);

    AtObjectDelete((AtObject)channelIterator);
    }

static void AllFlowStatusClear(AtModule self)
    {
    ChannelsStatusClear(self, AtModuleEthFlowIteratorCreate);
    }

static void AllPortsStatusClear(AtModule self)
    {
    ChannelsStatusClear(self, AtModuleEthPortIteratorCreate);
    }

static void AllSerdesStatusClear(AtModule self)
    {
    uint32 i;
    AtModuleEth ethModule = (AtModuleEth)self;

    for (i = 0; i < AtModuleEthNumSerdesControllers(ethModule); i++)
        AtSerdesControllerStatusClear(AtModuleEthSerdesController(ethModule, i));
    }

static void StatusClear(AtModule self)
    {
    m_AtModuleMethods->StatusClear(self);

    AllFlowStatusClear(self);
    AllPortsStatusClear(self);
    AllSerdesStatusClear(self);
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
	AtUnused(self);
	AtUnused(port);
    return cAtEthPortInterface1000BaseX;
    }

static AtEthFlowControl FlowControlCreate(AtModuleEth module, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(module);
    return NULL;
    }

static AtEthFlowControl PortFlowControlCreate(AtModuleEth module, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(module);
    return NULL;
    }

static AtSpiController SpiController(AtModuleEth self, uint32 phyId)
    {
    AtUnused(phyId);
    AtUnused(self);

    /* Let concrete class do */
    return NULL;
    }

static uint32 NumSpiControllers(AtModuleEth self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    AtIterator flowIterator;
    AtEthFlow flow;
    eAtRet ret = cAtOk;
    AtModuleEth ethModule = (AtModuleEth)self;

    flowIterator = AtModuleEthFlowIteratorCreate(ethModule);
    while ((flow = (AtEthFlow)AtIteratorNext(flowIterator)) != NULL)
        ret |= AtModuleEthFlowDelete(ethModule, (uint16)AtChannelIdGet((AtChannel)flow));

    AtObjectDelete((AtObject)flowIterator);

    return ret;
    }

static eAtRet WarmRestore(AtModule self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;
    uint8 port_i;
    eAtRet ret = cAtOk;

    for (port_i = 0; port_i < AtModuleEthMaxPortsGet(ethModule); port_i++)
        {
        AtEthPort port = AtModuleEthPortGet(ethModule, port_i);
        ret |= AtChannelWarmRestore((AtChannel)port);
        }

    return ret;
    }

static uint32 NumSerdesControllers(AtModuleEth self)
    {
    if (mMethodsGet(self)->HasSerdesControllers(self))
        return AtModuleEthMaxPortsGet(self);
    return 0;
    }

static AtSerdesController SerdesController(AtModuleEth self, uint32 serdesId)
    {
    if (mMethodsGet(self)->HasSerdesControllers(self))
        {
        AtEthPort port = AtModuleEthPortGet(self, (uint8)serdesId);
        return AtEthPortSerdesController(port);
        }

    return NULL;
    }

static eBool HasSerdesControllers(AtModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 SerdesMdioBaseAddress(AtModuleEth self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cBit31_0;
    }

static AtMdio SerdesMdioObjectCreate(AtModuleEth self, AtSerdesController serdes, uint32 baseAddress, AtHal hal)
    {
    AtUnused(self);
    AtUnused(serdes);
    AtUnused(baseAddress);
    return AtMdioNew(baseAddress, hal);
    }

static eBool SerdesHasMdio(AtModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtMdio SerdesMdio(AtModuleEth self, AtSerdesController serdes)
    {
    if (!mMethodsGet(self)->SerdesHasMdio(self))
        return NULL;

    if (self->serdesMdio == NULL)
        self->serdesMdio = mMethodsGet(self)->SerdesMdioCreate(self, serdes);

    AtMdioPortSelect(self->serdesMdio, AtSerdesControllerHwIdGet(serdes));
    return self->serdesMdio;
    }

static AtMdio SerdesMdioCreate(AtModuleEth self, AtSerdesController serdes)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    uint32 baseAddress = mMethodsGet(self)->SerdesMdioBaseAddress(self, serdes);
    AtMdio mdio = mMethodsGet(self)->SerdesMdioObjectCreate(self, serdes, baseAddress, hal);
    AtMdioSimulateEnable(mdio, AtDeviceIsSimulated(device));
    return mdio;
    }

static AtDrp SerdesDrp(AtModuleEth self, uint32 serdesId)
    {
    if (!mMethodsGet(self)->SerdesHasDrp(self, serdesId))
        return NULL;

    if (self->serdesDrp == NULL)
        self->serdesDrp = mMethodsGet(self)->SerdesDrpCreate(self, serdesId);

    return self->serdesDrp;
    }

static AtDrp DefaultSerdesDrpCreate(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    AtDrp drp = AtDrpNew(mMethodsGet(self)->SerdesDrpBaseAddress(self), hal);
    AtDrpNumberOfSerdesControllersSet(drp, AtModuleEthNumSerdesControllers(self));
    return drp;
    }

static AtDrp SerdesDrpCreate(AtModuleEth self, uint32 serdesId)
    {
    AtUnused(serdesId);
    return DefaultSerdesDrpCreate(self);
    }

static uint32 SerdesDrpBaseAddress(AtModuleEth self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool SerdesHasDrp(AtModuleEth self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtFalse;
    }

static eBool SerdesApsSupported(AtModuleEth self)
    {
    /* Let concrete determine */
    AtUnused(self);
    return cAtFalse;
    }

static eBool StaticFlowManagement(AtModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint16 CVlanDefaultTpid(AtModuleEth self)
    {
    AtUnused(self);
    return cAtModuleEthCVlanTpid;
    }

static uint16 SVlanDefaultTpid(AtModuleEth self)
    {
    AtUnused(self);
    return cAtModuleEthSVlanTpid;
    }

static eAtModuleEthRet DefaultCVlanTpIdSet(AtModuleEth self, uint16 tpid)
    {
    self->cVlanTpid = tpid;
    return cAtOk;
    }

static uint16 DefaultCVlanTpIdGet(AtModuleEth self)
    {
    return self->cVlanTpid;
    }

static eAtModuleEthRet DefaultSVlanTpIdSet(AtModuleEth self, uint16 tpid)
    {
    self->sVlanTpid = tpid;
    return cAtOk;
    }

static uint16 DefaultSVlanTpIdGet(AtModuleEth self)
    {
    return self->sVlanTpid;
    }

static eBool SerdesApsSupportedOnPort(AtModuleEth self, AtEthPort port)
    {
    AtUnused(port);
    return AtModuleEthSerdesApsSupported(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleEth object = (AtModuleEth)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(ethPorts, object->numPorts);
    mEncodeObjects(ethFlows, AtModuleEthMaxFlowsGet(object));
    mEncodeUInt(numPorts);
    mEncodeUInt(sVlanTpid);
    mEncodeUInt(cVlanTpid);

    /* Since MDIO and DRP are lazy created */
    AtModuleEthSerdesMdio(object, NULL);
    AtModuleEthSerdesDrp(object, 0);
    AtCoderEncodeString(encoder, AtMdioToString(object->serdesMdio), "serdesMdio");
    AtCoderEncodeString(encoder, AtDrpToString(object->serdesDrp), "serdesDrp");
    }

static eBool PortCanBeUsed(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtTrue;
    }

static AtQuerier QuerierGet(AtModule self)
    {
    AtUnused(self);
    return AtModuleEthSharedQuerier();
    }

static void MethodsInit(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxPortsGet);
        mMethodOverride(m_methods, EthPortGet);
        mMethodOverride(m_methods, PortCanBeUsed);
        mMethodOverride(m_methods, MaxFlowsGet);
        mMethodOverride(m_methods, FlowCreate);
        mMethodOverride(m_methods, NopFlowCreate);
        mMethodOverride(m_methods, EopFlowCreate);
        mMethodOverride(m_methods, FlowGet);
        mMethodOverride(m_methods, FlowDelete);
        mMethodOverride(m_methods, FreeFlowIdGet);
        mMethodOverride(m_methods, PortCreate);
        mMethodOverride(m_methods, FlowObjectCreate);
        mMethodOverride(m_methods, ISISMacSet);
        mMethodOverride(m_methods, ISISMacGet);
        mMethodOverride(m_methods, PortDefaultInterface);
        mMethodOverride(m_methods, FlowControlCreate);
        mMethodOverride(m_methods, PortFlowControlCreate);
        mMethodOverride(m_methods, SpiController);
        mMethodOverride(m_methods, NumSpiControllers);
        mMethodOverride(m_methods, NumSerdesControllers);
        mMethodOverride(m_methods, SerdesController);
        mMethodOverride(m_methods, HasSerdesControllers);
        mMethodOverride(m_methods, SerdesMdio);
        mMethodOverride(m_methods, SerdesMdioCreate);
        mMethodOverride(m_methods, SerdesMdioBaseAddress);
        mMethodOverride(m_methods, SerdesHasMdio);
        mMethodOverride(m_methods, SerdesMdioObjectCreate);
        mMethodOverride(m_methods, SerdesDrp);
        mMethodOverride(m_methods, SerdesDrpCreate);
        mMethodOverride(m_methods, SerdesDrpBaseAddress);
        mMethodOverride(m_methods, SerdesHasDrp);
        mMethodOverride(m_methods, SerdesApsSupported);
        mMethodOverride(m_methods, StaticFlowManagement);
        mMethodOverride(m_methods, CVlanDefaultTpid);
        mMethodOverride(m_methods, SVlanDefaultTpid);
        mMethodOverride(m_methods, DefaultCVlanTpIdSet);
        mMethodOverride(m_methods, DefaultCVlanTpIdGet);
        mMethodOverride(m_methods, DefaultSVlanTpIdSet);
        mMethodOverride(m_methods, DefaultSVlanTpIdGet);
        mMethodOverride(m_methods, SerdesApsSupportedOnPort);
        }
    
    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        mMethodOverride(m_AtModuleOverride, AllServicesDestroy);
        mMethodOverride(m_AtModuleOverride, WarmRestore);
        mMethodOverride(m_AtModuleOverride, QuerierGet);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModuleEth AtModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModuleEth));
    
    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleEth, device) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;
    
    return self;
    }

AtEthFlowControl AtModuleEthFlowControlCreate(AtModuleEth self, AtEthFlow flow)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->FlowControlCreate(self, flow);

    return NULL;
    }

eAtEthPortInterface AtModuleEthPortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->PortDefaultInterface(self, port);

    return cAtEthPortInterfaceUnknown;
    }

AtEthFlowControl AtModuleEthPortFlowControlCreate(AtModuleEth self, AtEthPort port)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->PortFlowControlCreate(self, port);

    return NULL;
    }

AtMdio AtModuleEthSerdesMdio(AtModuleEth self, AtSerdesController serdes)
    {
    if (self)
        return mMethodsGet(self)->SerdesMdio(self, serdes);
    return NULL;
    }

AtDrp AtModuleEthSerdesDrp(AtModuleEth self, uint32 serdesId)
    {
    if (self)
        return mMethodsGet(self)->SerdesDrp(self, serdesId);
    return NULL;
    }

eBool AtModuleEthSerdesApsSupported(AtModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->SerdesApsSupported(self);
    return cAtFalse;
    }

uint32 AtModuleEthNumberOfCreatedPortsGet(AtModuleEth self)
    {
    if (self)
        return self->numPorts;

    return 0;
    }

eBool AtModuleEthPortCanBeUsed(AtModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(self)->PortCanBeUsed(self, portId);
    return cAtFalse;
    }

/**
 * @addtogroup AtModuleEth
 * @{
 */

/**
 * Get maximum number of ports this module can support
 *
 * @param self This module
 *
 * @return Maximum number of ports this module can support
 */
uint8 AtModuleEthMaxPortsGet(AtModuleEth self)
    {
    mAttributeGet(MaxPortsGet, uint8, 0);
    }

/**
 * Get Ethernet port
 *
 * @param self This module
 * @param portId Port ID
 * @return An instance of Ethernet port
 */
AtEthPort AtModuleEthPortGet(AtModuleEth self, uint8 portId)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->EthPortGet(self, portId);
        
    return NULL;
    }

/**
 * Get maximum number of traffic flows that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of traffic flows that this module can support
 */
uint16 AtModuleEthMaxFlowsGet(AtModuleEth self)
    {
    mAttributeGet(MaxFlowsGet, uint16, 0);
    }

/**
 * Create a flow
 *
 * @param self This module
 * @param flowId Flow ID
 *
 * @return Instance of Ethernet flow
 */
AtEthFlow AtModuleEthFlowCreate(AtModuleEth self, uint16 flowId)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->FlowCreate(self, flowId);
    return NULL;
    }

/**
 * Get Ethernet flow
 * @param self This module
 * @param flowId Flow ID
 * @return Instance of Ethernet flow
 */
AtEthFlow AtModuleEthFlowGet(AtModuleEth self, uint16 flowId)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->FlowGet(self, flowId);
    
    return NULL;
    }

/**
 * Delete Ethernet flow
 *
 * @param self This module
 * @param flowId Flow Id
 *
 * @return AT return code
 */
eAtModuleEthRet AtModuleEthFlowDelete(AtModuleEth self, uint16 flowId)
    {
    eAtRet ret;

    if (!mModuleIsValid(self))
        return cAtError;

    AtModuleLock((AtModule)self);
    ret = mMethodsGet(self)->FlowDelete(self, flowId);
    AtModuleUnLock((AtModule)self);

    return ret;
    }

/**
 * To get a next free Ethernet flow resource
 *
 * @param self This module
 *
 * @return ID of free Ethernet flow
 */
uint16 AtModuleEthFreeFlowIdGet(AtModuleEth self)
    {
    mAttributeGet(FreeFlowIdGet, uint16, 0);
    }

/**
 * Create Iterator for ports
 *
 * @param self This module
 *
 * @return Iterator object
 */
AtIterator AtModuleEthPortIteratorCreate(AtModuleEth self)
    {
    AtArrayIterator iterator;

    if (self == NULL)
        return NULL;

    iterator = AtArrayIteratorNew(self->ethPorts, self->numPorts);

    if (iterator == NULL)
        return NULL;

    /* Override */
    PortIteratorOverride(iterator);
    m_portIteratorMethodsInit = 1;

    return (AtIterator)iterator;
    }

/**
 * Create Iterator for flows
 *
 * @param self This module
 *
 * @return Iterator object
 */
AtIterator AtModuleEthFlowIteratorCreate(AtModuleEth self)
    {
    if (self)
        {
        uint16 maxFlow = mMethodsGet(self)->MaxFlowsGet(self);
        AtArrayIterator iterator = AtArrayIteratorNew(self->ethFlows, maxFlow);

        if (iterator)
            {
            /* Override */
            FlowIteratorOverride(iterator);

            m_flowIteratorMethodsInit = 1;

            return (AtIterator)iterator;
            }
        }

    return NULL;
    }

/**
 * Set destination MAC will be inserted to Ethernet side for ISIS packet
 *
 * @param self This module
 * @param mac Destination MAC to be inserted
 *
 * @return AT return code
 */
eAtModuleEthRet AtModuleEthISISMacSet(AtModuleEth self, const uint8 *mac)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->ISISMacSet(self, mac);
    return cAtErrorNullPointer;
    }

/**
 * Get destination MAC that will be inserted to Ethernet side for ISIS packet
 *
 * @param self This module
 * @param [out] mac Destination MAC inserted to Ethernet side
 *
 * @return AT return code
 */
eAtModuleEthRet AtModuleEthISISMacGet(AtModuleEth self, uint8 *mac)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->ISISMacGet(self, mac);
    return cAtErrorNullPointer;
    }

/**
 * Get number of SPI controllers this module can support
 *
 * @param self This module
 *
 * @return Number of supported SPI controllers
 */
uint32 AtModuleEthNumSpiControllers(AtModuleEth self)
    {
    mAttributeGet(NumSpiControllers, uint32, 0);
    }

/**
 * Get SPI controller
 *
 * @param self This module
 * @param phyId Physical ID
 *
 * @return AtSpiController if the physical ID is in range, otherwise, NULL is returned.
 */
AtSpiController AtModuleEthSpiController(AtModuleEth self, uint32 phyId)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->SpiController(self, phyId);
    return NULL;
    }

/**
 * Get number of SERDES controllers
 *
 * @param self This module
 *
 * @return Number of SERDES controllers
 */
uint32 AtModuleEthNumSerdesControllers(AtModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->NumSerdesControllers(self);
    return 0;
    }

/**
 * Get SERDES controller
 *
 * @param self This module
 * @param serdesId SERDES ID
 *
 * @return SERDES controller if ID is valid. Otherwise, NULL is returned.
 */
AtSerdesController AtModuleEthSerdesController(AtModuleEth self, uint32 serdesId)
    {
    if (self)
        return mMethodsGet(self)->SerdesController(self, serdesId);
    return NULL;
    }

/**
 * Change default C-VLAN TPID
 *
 * @param self This module
 * @param tpid C-VLAN TPID
 *
 * @return AT return code
 */
eAtModuleEthRet AtModuleEthDefaultCVlanTpIdSet(AtModuleEth self, uint16 tpid)
    {
    if (self)
        return mMethodsGet(self)->DefaultCVlanTpIdSet(self, tpid);

    return cAtErrorObjectNotExist;
    }

/**
 * Get default C-VLAN TPID
 *
 * @param self This module
 *
 * @return Default C-VLAN TPID
 */
uint16 AtModuleEthDefaultCVlanTpIdGet(AtModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->DefaultCVlanTpIdGet(self);
    return cAtModuleEthCVlanTpid;
    }

/**
 * Set default S-VLAN TPID
 *
 * @param self This module
 * @param tpid S-VLAN TPID
 *
 * @return AT return code
 */
eAtModuleEthRet AtModuleEthDefaultSVlanTpIdSet(AtModuleEth self, uint16 tpid)
    {
    if (self)
        return mMethodsGet(self)->DefaultSVlanTpIdSet(self, tpid);
    return cAtErrorObjectNotExist;
    }

/**
 * Get default S-VLAN TPID
 *
 * @param self This module
 *
 * @return Default S-VLAN TPID
 */
uint16 AtModuleEthDefaultSVlanTpIdGet(AtModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->DefaultSVlanTpIdGet(self);
    return cAtModuleEthSVlanTpid;
    }

/**
 * @}
 */

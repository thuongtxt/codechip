/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : ThaModuleEthInternal.h
 * 
 * Created Date: Aug 7, 2012
 *
 * Description : ETH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEETHINTERNAL_H_
#define _ATMODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"

#include "../man/AtModuleInternal.h"
#include "AtEthPortInternal.h"
#include "AtEthFlowInternal.h"
#include "AtModuleConcate.h"
#include "../physical/AtSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtModuleEthMethods
    {
    /* Manage Ethernet port */
    uint8 (*MaxPortsGet)(AtModuleEth self);
    AtEthPort (*EthPortGet)(AtModuleEth self, uint8 portId);
    eBool (*PortCanBeUsed)(AtModuleEth self, uint8 portId);

    /* Manage Flow */
    uint16 (*MaxFlowsGet)(AtModuleEth self);
    AtEthFlow (*FlowCreate)(AtModuleEth self, uint16 flowId);
    AtEthFlow (*FlowGet)(AtModuleEth self, uint16 flowId);
    eAtModuleEthRet (*FlowDelete)(AtModuleEth self, uint16 flowId);
    uint16 (*FreeFlowIdGet)(AtModuleEth self);

    /* Internal methods */
    eBool (*StaticFlowManagement)(AtModuleEth self);
    AtEthFlow (*FlowObjectCreate)(AtModuleEth self, uint16 flowId);
    AtEthPort (*PortCreate)(AtModuleEth self, uint8 portId);
    eAtEthPortInterface (*PortDefaultInterface)(AtModuleEth self, AtEthPort port);
    AtEthFlowControl (*FlowControlCreate)(AtModuleEth self, AtEthFlow flow);
    AtEthFlowControl (*PortFlowControlCreate)(AtModuleEth self, AtEthPort port);

    /* ISIS MAC */
    eAtModuleEthRet (*ISISMacSet)(AtModuleEth self, const uint8 *mac);
    eAtModuleEthRet (*ISISMacGet)(AtModuleEth self, uint8 *mac);

    /* SPI controller */
    uint32 (*NumSpiControllers)(AtModuleEth self);
    AtSpiController (*SpiController)(AtModuleEth self, uint32 phyId);

    /* SERDES controllers */
    uint32 (*NumSerdesControllers)(AtModuleEth self);
    AtSerdesController (*SerdesController)(AtModuleEth self, uint32 serdesId);
    eBool (*HasSerdesControllers)(AtModuleEth self);
    AtMdio (*SerdesMdio)(AtModuleEth self, AtSerdesController serdes);
    AtMdio (*SerdesMdioCreate)(AtModuleEth self, AtSerdesController serdes);
    uint32 (*SerdesMdioBaseAddress)(AtModuleEth self, AtSerdesController serdes);
    AtMdio (*SerdesMdioObjectCreate)(AtModuleEth self, AtSerdesController serdes, uint32 baseAddress, AtHal hal);
    eBool (*SerdesHasMdio)(AtModuleEth self);
    eBool (*SerdesApsSupported)(AtModuleEth self);
    eBool (*SerdesApsSupportedOnPort)(AtModuleEth self, AtEthPort port);

    /* VLAN TPIDs */
    eAtModuleEthRet (*DefaultCVlanTpIdSet)(AtModuleEth self, uint16 tpid);
    uint16 (*DefaultCVlanTpIdGet)(AtModuleEth self);
    eAtModuleEthRet (*DefaultSVlanTpIdSet)(AtModuleEth self, uint16 tpid);
    uint16 (*DefaultSVlanTpIdGet)(AtModuleEth self);
    uint16 (*CVlanDefaultTpid)(AtModuleEth self);
    uint16 (*SVlanDefaultTpid)(AtModuleEth self);

    /* To be deprecated */
    AtEthFlow (*NopFlowCreate)(AtModuleEth self, uint16 flowId);
    AtEthFlow (*EopFlowCreate)(AtModuleEth self, uint16 flowId);

    AtDrp (*SerdesDrp)(AtModuleEth self, uint32 serdesId);
    AtDrp (*SerdesDrpCreate)(AtModuleEth self, uint32 serdesId);
    uint32 (*SerdesDrpBaseAddress)(AtModuleEth self);
    eBool (*SerdesHasDrp)(AtModuleEth self, uint32 serdesId);
    } tAtModuleEthMethods;

/* This structure is to represent for Ethernet Module */
typedef struct tAtModuleEth
    {
    tAtModule super;

    /* Implementation */
    const tAtModuleEthMethods *methods;

    /* Ports */
    AtEthPort *ethPorts;
    uint32 numPorts;

    /* Flows */
    AtEthFlow *ethFlows;

    /* Physical */
    AtMdio serdesMdio;

    /* Default settings */
    uint16 sVlanTpid;
    uint16 cVlanTpid;

    AtDrp serdesDrp;

    } tAtModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth AtModuleEthObjectInit(AtModuleEth self, AtDevice device);
AtModuleEth ThaModuleEthPwObjectInit(AtModuleEth self, AtDevice device);

eAtEthPortInterface AtModuleEthPortDefaultInterface(AtModuleEth self, AtEthPort port);
AtMdio AtModuleEthSerdesMdio(AtModuleEth self, AtSerdesController serdes);
AtDrp AtModuleEthSerdesDrp(AtModuleEth self, uint32 serdesId);
eBool AtModuleEthSerdesApsSupported(AtModuleEth self);
uint32 AtModuleEthNumberOfCreatedPortsGet(AtModuleEth self);

/* Flow control factory APIs */
AtEthFlowControl AtModuleEthFlowControlCreate(AtModuleEth self, AtEthFlow flow);
AtEthFlowControl AtModuleEthPortFlowControlCreate(AtModuleEth self, AtEthPort port);

/* Ethernet flow factory */
AtEthFlow AtModuleEthGfpEthFlowCreate(AtModuleEth self, AtGfpChannel gfpChannel);
eAtRet AtModuleEthGfpEthFlowDelete(AtModuleEth self, AtEthFlow flow);

eBool AtModuleEthPortCanBeUsed(AtModuleEth self, uint8 portId);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEETHINTERNAL_H_ */


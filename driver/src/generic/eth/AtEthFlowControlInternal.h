/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtEthFlowControlInternal.h
 * 
 * Created Date: Oct 4, 2013
 *
 * Description : ETH flow control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHFLOWCONTROLINTERNAL_H_
#define _ATETHFLOWCONTROLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtEthFlowControl.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEthFlowControlMethods
    {
    /* Thresholds */
	eAtModuleEthRet (*HighThresholdSet)(AtEthFlowControl self, uint32 threshold);
    uint32 (*HighThresholdGet)(AtEthFlowControl self);
    eAtModuleEthRet (*LowThresholdSet)(AtEthFlowControl self, uint32 threshold);
    uint32 (*LowThresholdGet)(AtEthFlowControl self);

    /* Ethernet header */
    eAtModuleEthRet (*DestMacSet)(AtEthFlowControl self, uint8 *address);
    eAtModuleEthRet (*DestMacGet)(AtEthFlowControl self, uint8 *address);
    eAtModuleEthRet (*SourceMacSet)(AtEthFlowControl self, uint8 *address);
    eAtModuleEthRet (*SourceMacGet)(AtEthFlowControl self, uint8 *address);
    eAtModuleEthRet (*EthTypeSet)(AtEthFlowControl self, uint32 ethType);
    uint32 (*EthTypeGet)(AtEthFlowControl self);

    /* PAUSE frame period */
    eAtModuleEthRet (*PauseFramePeriodSet)(AtEthFlowControl self, uint32 period);
    uint32 (*PauseFramePeriodGet)(AtEthFlowControl self);

    /* Quanta */
    eAtModuleEthRet (*PauseFrameQuantaSet)(AtEthFlowControl self, uint32 quanta);
    uint32 (*PauseFrameQuantaGet)(AtEthFlowControl self);

    /* Counter */
    uint16 (*TxPauseFrameCounterGet)(AtEthFlowControl self);
    uint16 (*TxPauseFrameCounterClear)(AtEthFlowControl self);
    uint16 (*RxPauseFrameCounterGet)(AtEthFlowControl self);
    uint16 (*RxPauseFrameCounterClear)(AtEthFlowControl self);

    uint32 (*TxFifoWaterMarkMinGet)(AtEthFlowControl self);
    uint32 (*TxFifoWaterMarkMinClear)(AtEthFlowControl self);
    uint32 (*TxFifoWaterMarkMaxGet)(AtEthFlowControl self);
    uint32 (*TxFifoWaterMarkMaxClear)(AtEthFlowControl self);

    /* Buffer */
    uint32 (*TxFifoLevelGet)(AtEthFlowControl self);

    /* Enable/Disable */
    eAtModuleEthRet (*Enable)(AtEthFlowControl self, eBool enable);
    eBool (*IsEnabled)(AtEthFlowControl self);
    }tAtEthFlowControlMethods;

typedef struct tAtEthFlowControl
    {
    tAtObject super;
    const tAtEthFlowControlMethods *methods;

    /* Private */
    AtChannel channel; /* Channel that need flow control (Ethernet port or Ethernet flow, ...) */
    }tAtEthFlowControl;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlowControl AtEthFlowControlObjectInit(AtEthFlowControl self, AtChannel channel);

uint32 AtEthFlowControlRead(AtEthFlowControl self, uint32 address, eAtModule moduleId);
void AtEthFlowControlWrite(AtEthFlowControl self, uint32 address, uint32 value, eAtModule moduleId);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHFLOWCONTROLINTERNAL_H_ */


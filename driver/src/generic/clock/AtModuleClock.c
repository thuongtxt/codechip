/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : AtModuleClock.c
 *
 * Created Date: Aug 27, 2013
 *
 * Description : Clock module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleClockMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleClock);
    }

static uint8 NumExtractors(AtModuleClock self)
    {
	AtUnused(self);
    /* Sub class knows */
    return 0;
    }

static AtClockExtractor ExtractorGet(AtModuleClock self, uint8 extractorId)
    {
	AtUnused(extractorId);
	AtUnused(self);
    /* Sub class knows */
    return NULL;
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "clock";
    }

static const char *CapacityDescription(AtModule self)
    {
    static char string[32];

    AtSprintf(string, "extractor: %d", AtModuleClockNumExtractors((AtModuleClock)self));
    return string;
    }

static uint32 AllClockCheck(AtModuleClock self)
    {
	AtUnused(self);
    /* Let sub class do this */
    return cAtOk;
    }

static eBool AllClockCheckIsSupported(AtModuleClock self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void MethodsInit(AtModuleClock self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NumExtractors);
        mMethodOverride(m_methods, ExtractorGet);
        mMethodOverride(m_methods, AllClockCheck);
        mMethodOverride(m_methods, AllClockCheckIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModuleClock self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleClock self)
    {
    OverrideAtModule(self);
    }

AtModuleClock AtModuleClockObjectInit(AtModuleClock self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleClock, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModuleClock
 * @{
 */

/**
 * Get number of clock extractors that this module can support
 *
 * @param self This module
 *
 * @return Number of clock extractors.
 */
uint8 AtModuleClockNumExtractors(AtModuleClock self)
    {
    if (self)
        return mMethodsGet(self)->NumExtractors(self);
    return 0;
    }

/**
 * Get clock extractor
 *
 * @param self This module
 * @param extractorId Clock extractor ID
 *
 * @return Clock extractor
 */
AtClockExtractor AtModuleClockExtractorGet(AtModuleClock self, uint8 extractorId)
    {
    if (self)
        return mMethodsGet(self)->ExtractorGet(self, extractorId);
    return NULL;
    }

/**
 * Check if all clocks are good
 *
 * @param self This module
 *
 * @return 0 if success or other value if checking fail. For one product, there
 * are more than one clock sources come to AT device and they are different among
 * products. So this function will return a mask of fail clock sources. And this
 * mask is defined for each product.
 */
uint32 AtModuleClockAllClockCheck(AtModuleClock self)
    {
    if (self)
        return mMethodsGet(self)->AllClockCheck(self);
    return 0;
    }

/**
 * Check if all clock checking is supported or not
 *
 * @param self This module
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtModuleClockAllClockCheckIsSupported(AtModuleClock self)
    {
    if (self)
        return mMethodsGet(self)->AllClockCheckIsSupported(self);
    return cAtFalse;
    }

/**
 * @}
 */

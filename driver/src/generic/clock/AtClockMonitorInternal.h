/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : AtClockMonitorInternal.h
 * 
 * Created Date: Aug 7, 2017
 *
 * Description : Clock monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLOCKMONITORINTERNAL_H_
#define _ATCLOCKMONITORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtObjectInternal.h"
#include "../common/AtChannelInternal.h" /* For read/write */
#include "../man/AtModuleInternal.h"
#include "AtModuleClockInternal.h"
#include "AtClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtClockMonitorMethods
    {
    eAtRet (*Init)(AtClockMonitor self);
    uint32 (*CurrentPpmGet)(AtClockMonitor self);
    eAtRet (*ThresholdSet)(AtClockMonitor self, eAtClockMonitorThreshold threshold, uint32 value);
    uint32 (*ThresholdGet)(AtClockMonitor self, eAtClockMonitorThreshold threshold);
    eBool (*ThresholdIsSupported)(AtClockMonitor self, eAtClockMonitorThreshold threshold);
    uint32 (*ThresholdMax)(AtClockMonitor self, eAtClockMonitorThreshold threshold);
    uint32 (*AlarmGet)(AtClockMonitor self);
    uint32 (*AlarmHistoryGet)(AtClockMonitor self);
    uint32 (*AlarmHistoryClear)(AtClockMonitor self);
    eAtRet (*InterruptMaskSet)(AtClockMonitor self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtClockMonitor self);
    eBool (*InterruptMaskIsSupported)(AtClockMonitor self, uint32 defectMask);
    }tAtClockMonitorMethods;

typedef struct tAtClockMonitor
    {
    tAtObject super;
    const tAtClockMonitorMethods *methods;

    /* Private data */
    AtModuleClock module;
    AtObject objectMonitor;
    }tAtClockMonitor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockMonitor AtClockMonitorObjectInit(AtClockMonitor self, AtModuleClock clockModule, AtObject objectMonitor);
AtModuleClock AtClockMonitorModuleGet(AtClockMonitor self);
AtDevice AtClockMonitorDeviceGet(AtClockMonitor self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLOCKMONITORINTERNAL_H_ */


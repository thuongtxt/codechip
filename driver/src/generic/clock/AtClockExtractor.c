/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : AtClockExtractor.c
 *
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtModuleInternal.h"
#include "AtClockExtractorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidExternalId 0xFF

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtClockExtractorMethods m_methods;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtClockExtractor);
    }

static uint8 IdGet(AtClockExtractor self)
    {
    return self->extractorId;
    }

static const char *ToString(AtObject self)
    {
    static char buf[64];
    AtModule module = (AtModule)AtClockExtractorModuleGet((AtClockExtractor)self);
    AtDevice dev = AtModuleDeviceGet(module);

    AtSnprintf(buf,
               sizeof(buf), "%sclock_extractor#%d",
               AtDeviceIdToString(dev), AtClockExtractorIdGet((AtClockExtractor)self));

    return buf;
    }

static eBool NeedDisableClockOutput(AtClockExtractor self, eAtTimingMode clockSourceType, AtChannel clockSource)
    {
	AtUnused(self);
    return ((clockSourceType == cAtTimingModeUnknown) && (clockSource == NULL)) ? cAtTrue : cAtFalse;
    }

static eAtModuleClockRet HwClockExtract(AtClockExtractor self, eAtTimingMode clockSourceType, AtChannel clockSource)
    {
    if (clockSourceType == cAtTimingModeSys)
        return AtClockExtractorSystemClockExtract(self);

    if (clockSourceType == cAtTimingModeSdhSys)
        return AtClockExtractorSdhSystemClockExtract(self);

    if (clockSourceType == cAtTimingModeSdhLineRef)
        return AtClockExtractorSdhLineClockExtract(self, (AtSdhLine)clockSource);

    if (clockSourceType == cAtTimingModeExt1Ref)
        return AtClockExtractorExternalClockExtract(self, 0);
    if (clockSourceType == cAtTimingModeExt2Ref)
        return AtClockExtractorExternalClockExtract(self, 1);

    return AtClockExtractorPdhDe1ClockExtract(self, (AtPdhDe1)clockSource);
    }

static eAtModuleClockRet Extract(AtClockExtractor self, eAtTimingMode clockSourceType, AtChannel clockSource)
    {
    eAtRet ret = cAtOk;

    if (NeedDisableClockOutput(self, clockSourceType, clockSource))
        return AtClockExtractorEnable(self, cAtFalse);

    /* Extract clock */
    ret |= HwClockExtract(self, clockSourceType, clockSource);
    ret |= AtClockExtractorEnable(self, cAtTrue);

    return ret;
    }

static eAtTimingMode SourceTypeGet(AtClockExtractor self)
    {
    if (AtClockExtractorSystemClockIsExtracted(self))
        return cAtTimingModeSys;
    if (AtClockExtractorSdhSystemClockIsExtracted(self))
        return cAtTimingModeSdhSys;
    if (AtClockExtractorSdhLineGet(self))
        return cAtTimingModeSdhLineRef;
    if (AtClockExtractorExternalIdIsValid(self, AtClockExtractorExternalClockGet(self)))
        return (AtClockExtractorExternalClockGet(self) == 0) ? cAtTimingModeExt1Ref : cAtTimingModeExt2Ref;
    if (AtClockExtractorPdhDe1LiuClockIsExtracted(self) || AtClockExtractorPdhDe1Get(self))
        return AtChannelTimingModeGet((AtChannel)AtClockExtractorPdhDe1Get(self));

    return cAtTimingModeUnknown;
    }

static AtChannel SourceGet(AtClockExtractor self)
    {
    if (AtClockExtractorSystemClockIsExtracted(self))
        return NULL;
    if (AtClockExtractorSdhSystemClockIsExtracted(self))
        return NULL;
    if (AtClockExtractorSdhLineGet(self))
        return (AtChannel)AtClockExtractorSdhLineGet(self);
    if (AtClockExtractorExternalIdIsValid(self, AtClockExtractorExternalClockGet(self)))
        return NULL;
    if (AtClockExtractorPdhDe1LiuClockIsExtracted(self) || AtClockExtractorPdhDe1Get(self))
        return (AtChannel)AtClockExtractorPdhDe1Get(self);

    return NULL;
    }

static eAtModuleClockRet SystemClockExtract(AtClockExtractor self)
    {
	AtUnused(self);
    /* Subclass will do */
    return cAtErrorNotImplemented;
    }

static eBool SystemClockIsExtracted(AtClockExtractor self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtModuleClockRet SdhLineClockExtract(AtClockExtractor self, AtSdhLine line)
    {
	AtUnused(line);
	AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet SdhSystemClockExtract(AtClockExtractor self)
    {
	AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eBool SdhSystemClockIsExtracted(AtClockExtractor self)
    {
	AtUnused(self);
    /* Sub class should do */
    return cAtFalse;
    }

static eAtModuleClockRet ExternalClockExtract(AtClockExtractor self, uint8 externalId)
    {
	AtUnused(externalId);
	AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet PdhDe1ClockExtract(AtClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet PdhDe1LiuClockExtract(AtClockExtractor self, AtPdhDe1 de1)
    {
	AtUnused(de1);
	AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
	AtUnused(self);
    /* Sub class should do */
    return cAtFalse;
    }

static AtSdhLine SdhLineGet(AtClockExtractor self)
    {
	AtUnused(self);
    /* Sub class should do */
    return NULL;
    }

static AtPdhDe1 PdhDe1Get(AtClockExtractor self)
    {
	AtUnused(self);
    /* Sub class should do */
    return NULL;
    }

static uint8 ExternalClockGet(AtClockExtractor self)
    {
	AtUnused(self);
    /* Sub class should do */
    return cInvalidExternalId;
    }

static eBool ExternalIdIsValid(AtClockExtractor self, uint8 externalId)
    {
	AtUnused(self);
    return (externalId == cInvalidExternalId) ? cAtFalse : cAtTrue;
    }

static eBool PdhDe1LiuClockIsExtracted(AtClockExtractor self)
    {
	AtUnused(self);
    /* Sub class should do */
    return cAtFalse;
    }

static uint8 CoreId(AtClockExtractor self)
    {
    return AtModuleDefaultCoreGet((AtModule)AtClockExtractorModuleGet(self));
    }

static AtHal LookupHal(AtClockExtractor self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)AtClockExtractorModuleGet(self));
    return AtDeviceIpCoreHalGet(device, CoreId(self));
    }

static AtHal Hal(AtClockExtractor self)
    {
    if (self->hal == NULL)
        self->hal = LookupHal(self);
    return self->hal;
    }

static void Debug(AtClockExtractor self)
    {
	AtUnused(self);
    /* Let concrete class do */
    }

static eAtModuleClockRet SquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    AtUnused(self);
    return (options == cAtClockExtractorSquelchingNone) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtClockExtractorSquelching SquelchingOptionGet(AtClockExtractor self)
    {
    AtUnused(self);
    return cAtClockExtractorSquelchingNone;
    }

static eBool SquelchingIsSupported(AtClockExtractor self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 OutputCounterGet(AtClockExtractor self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleClockRet PdhDe3ClockExtract(AtClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eAtModuleClockRet PdhDe3LiuClockExtract(AtClockExtractor self, AtPdhDe3 de3)
    {
    AtUnused(de3);
    AtUnused(self);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static eBool PdhDe3LiuClockIsExtracted(AtClockExtractor self)
    {
    AtUnused(self);
    /* Sub class should do */
    return cAtFalse;
    }

static eAtModuleClockRet SerdesClockExtract(AtClockExtractor self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    /* Sub class should do */
    return cAtErrorNotImplemented;
    }

static AtSerdesController SerdesGet(AtClockExtractor self)
    {
    AtUnused(self);
    /* Sub class should do */
    return NULL;
    }

static AtPdhDe3 PdhDe3Get(AtClockExtractor self)
    {
    AtUnused(self);
    /* Sub class should do */
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtClockExtractor object = (AtClockExtractor)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(extractorId);
    mEncodeObjectDescription(module);
    mEncodeNone(hal);
    }

static void MethodsInit(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, IdGet);

        /* Will be deprecated */
        mMethodOverride(m_methods, Extract);
        mMethodOverride(m_methods, SourceGet);
        mMethodOverride(m_methods, SourceTypeGet);

        /* Control global clock extraction */
        mMethodOverride(m_methods, SystemClockExtract);
        mMethodOverride(m_methods, SystemClockIsExtracted);

        /* Control SDH clock extraction */
        mMethodOverride(m_methods, SdhSystemClockExtract);
        mMethodOverride(m_methods, SdhSystemClockIsExtracted);
        mMethodOverride(m_methods, SdhLineClockExtract);
        mMethodOverride(m_methods, SdhLineGet);

        /* Control external clock extraction */
        mMethodOverride(m_methods, ExternalClockExtract);
        mMethodOverride(m_methods, ExternalClockGet);
        mMethodOverride(m_methods, ExternalIdIsValid);

        /* Control PDH clock extraction */
        mMethodOverride(m_methods, PdhDe1ClockExtract);
        mMethodOverride(m_methods, PdhDe1LiuClockExtract);
        mMethodOverride(m_methods, PdhDe1Get);
        mMethodOverride(m_methods, PdhDe1LiuClockIsExtracted);
        mMethodOverride(m_methods, PdhDe3ClockExtract);
        mMethodOverride(m_methods, PdhDe3LiuClockExtract);
        mMethodOverride(m_methods, PdhDe3Get);
        mMethodOverride(m_methods, PdhDe3LiuClockIsExtracted);
        mMethodOverride(m_methods, SerdesClockExtract);
        mMethodOverride(m_methods, SerdesGet);

        /* Enabling */
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, Debug);

        /* Squelching */
        mMethodOverride(m_methods, SquelchingIsSupported);
        mMethodOverride(m_methods, SquelchingOptionSet);
        mMethodOverride(m_methods, SquelchingOptionGet);
        mMethodOverride(m_methods, OutputCounterGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtClockExtractor self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtObject(self);
    }

AtClockExtractor AtClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->extractorId = extractorId;
    self->module      = clockModule;

    return self;
    }

uint32 AtClockExtractorRead(AtClockExtractor self, uint32 address)
    {
    return AtHalRead(Hal(self), address);
    }

void AtClockExtractorWrite(AtClockExtractor self, uint32 address, uint32 value)
    {
    AtHalWrite(Hal(self), address, value);
    }

uint32 AtClockExtractorOutputCounterGet(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->OutputCounterGet(self);
    return 0;
    }

/**
 * @addtogroup AtClockExtractor
 * @{
 */

/**
 * Get ID of clock extractor
 *
 * @param self This clock extractor
 *
 * @return ID of this extractor
 */
uint8 AtClockExtractorIdGet(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->IdGet(self);
    return 0;
    }

/**
 * Get module of clock extractor
 *
 * @param self This clock extractor
 *
 * @return Its module
 */
AtModuleClock AtClockExtractorModuleGet(AtClockExtractor self)
    {
    if (self)
        return self->module;
    return NULL;
    }

/**
 * Extract clock
 *
 * @param self This clock extractor
 * @param clockSourceType @ref eAtTimingMode "Clock timing sources".
 * @param clockSource Channel to extract clock. This parameter is ignored if
 *        clockSourceType is:
          - cAtTimingModeSys
          - cAtTimingModeSdhSys
          - cAtTimingModeExt1Ref
          - cAtTimingModeExt2Ref

 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorExtract(AtClockExtractor self, eAtTimingMode clockSourceType, AtChannel clockSource)
    {
    eAtModuleClockRet ret = cAtErrorObjectNotExist;

    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        const char* objectDesc;
        AtDriverLogLock();
        objectDesc = AtDriverObjectStringToSharedBuffer((AtObject)clockSource);
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                AtSourceLocationNone, "API: %s([%s], %d, %s)\r\n",
                                AtFunction, AtObjectToString((AtObject)self), clockSourceType, objectDesc);
        AtDriverLogUnLock();
        }

    if (self)
        ret = mMethodsGet(self)->Extract(self, clockSourceType, clockSource);

    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get type of source that this extractor extracts clock
 *
 * @param self This clock extractor
 *
 * @return Clock source type
 */
eAtTimingMode AtClockExtractorSourceTypeGet(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->SourceTypeGet(self);
    return cAtTimingModeUnknown;
    }

/**
 * Get source channel that this extractor extract clock
 *
 * @param self This clock extractor
 *
 * @return Channel that this extractor extract clock
 */
AtChannel AtClockExtractorSourceGet(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->SourceGet(self);
    return NULL;
    }

/**
 * Extract clock from system clock
 *
 * @param self This clock extractor
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorSystemClockExtract(AtClockExtractor self)
    {
    mNoParamCall(SystemClockExtract, eAtModuleClockRet, cAtErrorObjectNotExist);
    }

/**
 * Check if system clock is extracted
 *
 * @param self This clock extractor
 *
 * @return cAtTrue if system clock is extracted, otherwise, cAtFalse is returned
 */
eBool AtClockExtractorSystemClockIsExtracted(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->SystemClockIsExtracted(self);
    return cAtFalse;
    }

/**
 * Extract clock from SDH line
 *
 * @param self This clock extractor
 * @param line SDH Line that clock will be extracted
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorSdhLineClockExtract(AtClockExtractor self, AtSdhLine line)
    {
    if (line == NULL)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "NULL clock source is not allowed\r\n");
        return cAtErrorNullPointer;
        }

    mObjectSet(SdhLineClockExtract, line);
    }

/**
 * Extract clock from system clock provided for SDH module
 *
 * @param self This clock extractor
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorSdhSystemClockExtract(AtClockExtractor self)
    {
    mNoParamCall(SdhSystemClockExtract, eAtModuleClockRet, cAtErrorObjectNotExist);
    }

/**
 * Check if system clock provided for SDH module is extracted
 *
 * @param self This clock extractor
 * @return cAtTrue SDH system clock is extracted, otherwise, cAtFalse is returned.
 */
eBool AtClockExtractorSdhSystemClockIsExtracted(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->SdhSystemClockIsExtracted(self);
    return cAtFalse;
    }

/**
 * Get SDH Line being extracted clock
 *
 * @param self This clock extractor
 *
 * @return AtSdhLine object if this extractor is extracting clock from SDH Line,
 *         or NULL if it is extracting from another source
 */
AtSdhLine AtClockExtractorSdhLineGet(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->SdhLineGet(self);
    return NULL;
    }

/**
 * Extract clock from external clock source
 *
 * @param self This clock extractor
 * @param externalId External clock source ID
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorExternalClockExtract(AtClockExtractor self, uint8 externalId)
    {
    mNumericalAttributeSet(ExternalClockExtract, externalId);
    }

/**
 * Get external clock ID being extracted clock
 *
 * @param self This clock extractor
 * @return Valid external clock ID if this extractor is extracting on. Otherwise,
 *         invalid ID is returned. See AtClockExtractorExternalIdIsValid() to
 *         check if external clock ID is valid or not.
 */
uint8 AtClockExtractorExternalClockGet(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->ExternalClockGet(self);
    return 0;
    }

/**
 * Check if external clock ID is valid
 *
 * @param self This extractor
 * @param externalId External clock ID
 *
 * @return cAtTrue if the input ID is correct, otherwise, cAtFalse is returned
 */
eBool AtClockExtractorExternalIdIsValid(AtClockExtractor self, uint8 externalId)
    {
    if (self)
        return mMethodsGet(self)->ExternalIdIsValid(self, externalId);
    return cAtFalse;
    }

/**
 * Extract clock from DS1/E1 channel
 *
 * @param self This clock extractor
 * @param de1 DS1/E1 object needs to extract clock
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorPdhDe1ClockExtract(AtClockExtractor self, AtPdhDe1 de1)
    {
    if (de1 == NULL)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "NULL clock source is not allowed\r\n");
        return cAtErrorNullPointer;
        }

    mObjectSet(PdhDe1ClockExtract, de1);
    }

/**
 * Extract clock from RX LIU of DS1/E1
 *
 * @param self This clock extractor
 * @param de1 DS1/E1 object needs to extract clock from RX LIU
 * @return
 */
eAtModuleClockRet AtClockExtractorPdhDe1LiuClockExtract(AtClockExtractor self, AtPdhDe1 de1)
    {
    if (de1 == NULL)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "NULL clock source is not allowed\r\n");
        return cAtErrorNullPointer;
        }

    mObjectSet(PdhDe1LiuClockExtract, de1);
    }

/**
 * Get DS1/E1 object being extracted clock
 *
 * @param self This clock extractor
 *
 * @return DS1/E1 object being extracted clock
 */
AtPdhDe1 AtClockExtractorPdhDe1Get(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->PdhDe1Get(self);
    return NULL;
    }

/**
 * Check if clock is extracted from LIU of DS1/E1
 *
 * @param self This clock extractor
 *
 * @return cAtTrue if clock is extracted from DS1/E1 LIU, cAtFalse for other case.
 */
eBool AtClockExtractorPdhDe1LiuClockIsExtracted(AtClockExtractor self)
    {
    mAttributeGet(PdhDe1LiuClockIsExtracted, eBool, cAtFalse);
    }

/**
 * Enable/disable clock output
 *
 * @param self This clock extractor
 * @param enable cAtTrue to enable clock output, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorEnable(AtClockExtractor self, eBool enable)
    {
    mNumericalAttributeSet(Enable, enable);
    }

/**
 * Check if clock output is enabled
 *
 * @param self This clock extractor
 *
 * @return cAtTrue if clock output is enabled, otherwise, cAtFalse is returned
 */
eBool AtClockExtractorIsEnabled(AtClockExtractor self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * Show debug information
 *
 * @param self This clock extractor
 */
void AtClockExtractorDebug(AtClockExtractor self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

/**
 * Extract clock from DS3/E3 channel
 *
 * @param self This clock extractor
 * @param de3 DS3/E3 object needs to extract clock
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorPdhDe3ClockExtract(AtClockExtractor self, AtPdhDe3 de3)
    {
    if (de3 == NULL)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "NULL clock source is not allowed\r\n");
        return cAtErrorNullPointer;
        }

    mObjectSet(PdhDe3ClockExtract, de3);
    }

/**
 * Extract clock from RX LIU of DS3/E3
 *
 * @param self This clock extractor
 * @param de3 DS3/E3 object needs to extract clock from RX LIU
 * @return
 */
eAtModuleClockRet AtClockExtractorPdhDe3LiuClockExtract(AtClockExtractor self, AtPdhDe3 de3)
    {
    if (de3 == NULL)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "NULL clock source is not allowed\r\n");
        return cAtErrorNullPointer;
        }

    mObjectSet(PdhDe3LiuClockExtract, de3);
    }

/**
 * Get DS3/E3 object being extracted clock
 *
 * @param self This clock extractor
 *
 * @return DS3/E3 object being extracted clock
 */
AtPdhDe3 AtClockExtractorPdhDe3Get(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->PdhDe3Get(self);
    return NULL;
    }

/**
 * Check if clock is extracted from LIU of DS3/E3
 *
 * @param self This clock extractor
 *
 * @return cAtTrue if clock is extracted from DS3/E3 LIU, cAtFalse for other case.
 */
eBool AtClockExtractorPdhDe3LiuClockIsExtracted(AtClockExtractor self)
    {
    mAttributeGet(PdhDe3LiuClockIsExtracted, eBool, cAtFalse);
    }

/**
 * Extract clock from serdes controller
 *
 * @param self This clock extractor
 * @param serdes SERDES object needs to extract clock from Rx SERDES(OCN/GE)
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorSerdesClockExtract(AtClockExtractor self, AtSerdesController serdes)
    {
    mObjectSet(SerdesClockExtract, serdes);
    }

/**
 * Get serdes controller from clock extractor
 *
 * @param self This clock extractor
 *
 * @return serdes Serdes controller
 */
AtSerdesController AtClockExtractorSerdesGet(AtClockExtractor self)
    {
    mNoParamObjectGet(SerdesGet, AtSerdesController);
    }

/**
 * Check if clock squelching is supported
 *
 * @param self This clock extractor
 *
 * @return cAtTrue if clock squelching is supported, cAtFalse for other case.
 */
eBool AtClockExtractorSquelchingIsSupported(AtClockExtractor self)
    {
    mAttributeGet(SquelchingIsSupported, eBool, cAtFalse);
    }

/**
 * Set squelching options for a clock extractor
 *
 * @param self This clock extractor
 * @param options Options for squelching: None, LOF | LOF | AIS. See @ref eAtClockExtractorSquelching "squelching option"
 *
 * @return AT return code
 */
eAtModuleClockRet AtClockExtractorSquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options)
    {
    mNumericalAttributeSet(SquelchingOptionSet, options);
    }

/**
 * Get squelching options for a clock extractor
 *
 * @param self This clock extractor
 *
 * @return Options for squelching: None, LOF | LOF | AIS. See @ref eAtClockExtractorSquelching "squelching option".
 */
eAtClockExtractorSquelching AtClockExtractorSquelchingOptionGet(AtClockExtractor self)
    {
    if (self)
        return mMethodsGet(self)->SquelchingOptionGet(self);
    return cAtClockExtractorSquelchingNone;
    }

/**
 * @}
 */

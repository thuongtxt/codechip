/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : AtModuleClockInternal.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULECLOCKINTERNAL_H_
#define _ATMODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtModuleInternal.h"
#include "AtModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleClockMethods
    {
    uint8 (*NumExtractors)(AtModuleClock self);
    AtClockExtractor (*ExtractorGet)(AtModuleClock self, uint8 extractorId);
    uint32 (*AllClockCheck)(AtModuleClock self);
    eBool (*AllClockCheckIsSupported)(AtModuleClock self);
    }tAtModuleClockMethods;

typedef struct tAtModuleClock
    {
    tAtModule super;
    const tAtModuleClockMethods *methods;

    /* Private data */
    }tAtModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock AtModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULECLOCKINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : AtClockExtractorInternal.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLOCKEXTRACTORINTERNAL_H_
#define _ATCLOCKEXTRACTORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtClockExtractor.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDriverInternal.h"
#include "../common/AtChannelInternal.h" /* For read/write */
#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtClockExtractorMethods
    {
    uint8 (*IdGet)(AtClockExtractor self);

    eAtModuleClockRet (*Extract)(AtClockExtractor self, eAtTimingMode clockSourceType, AtChannel clockSource);
    AtChannel (*SourceGet)(AtClockExtractor self);
    eAtTimingMode (*SourceTypeGet)(AtClockExtractor self);

    /* Control global clock extraction */
    eAtModuleClockRet (*SystemClockExtract)(AtClockExtractor self);
    eBool (*SystemClockIsExtracted)(AtClockExtractor self);

    /* Control SDH clock extraction */
    eAtModuleClockRet (*SdhSystemClockExtract)(AtClockExtractor self);
    eBool (*SdhSystemClockIsExtracted)(AtClockExtractor self);
    eAtModuleClockRet (*SdhLineClockExtract)(AtClockExtractor self, AtSdhLine line);
    AtSdhLine (*SdhLineGet)(AtClockExtractor self);

    /* Control external clock extraction */
    eAtModuleClockRet (*ExternalClockExtract)(AtClockExtractor self, uint8 externalId);
    uint8 (*ExternalClockGet)(AtClockExtractor self);
    eBool (*ExternalIdIsValid)(AtClockExtractor self, uint8 externalId);

    /* Control PDH clock extraction */
    eAtModuleClockRet (*PdhDe1ClockExtract)(AtClockExtractor self, AtPdhDe1 de1);
    eAtModuleClockRet (*PdhDe1LiuClockExtract)(AtClockExtractor self, AtPdhDe1 de1);
    AtPdhDe1 (*PdhDe1Get)(AtClockExtractor self);
    eBool (*PdhDe1LiuClockIsExtracted)(AtClockExtractor self);

    /* Control PDH clock extraction */
    eAtModuleClockRet (*PdhDe3ClockExtract)(AtClockExtractor self, AtPdhDe3 de3);
    eAtModuleClockRet (*PdhDe3LiuClockExtract)(AtClockExtractor self, AtPdhDe3 de3);
    AtPdhDe3 (*PdhDe3Get)(AtClockExtractor self);
    eBool (*PdhDe3LiuClockIsExtracted)(AtClockExtractor self);

    /* Serdes clock extractor */
    eAtModuleClockRet (*SerdesClockExtract)(AtClockExtractor self, AtSerdesController serdes);
    AtSerdesController (*SerdesGet)(AtClockExtractor self);

    /* Enabling */
    eAtModuleClockRet (*Enable)(AtClockExtractor self, eBool enable);
    eBool (*IsEnabled)(AtClockExtractor self);

    /* Debug */
    void (*Debug)(AtClockExtractor self);

    /* Squelching */
    eAtModuleClockRet (*SquelchingOptionSet)(AtClockExtractor self, eAtClockExtractorSquelching options);
    eAtClockExtractorSquelching (*SquelchingOptionGet)(AtClockExtractor self);
    eBool (*SquelchingIsSupported)(AtClockExtractor self);
    uint32 (*OutputCounterGet)(AtClockExtractor self);
    }tAtClockExtractorMethods;

typedef struct tAtClockExtractor
    {
    tAtObject super;
    const tAtClockExtractorMethods *methods;

    /* Private data */
    uint8 extractorId;
    AtModuleClock module;
    AtHal hal;
    }tAtClockExtractor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor AtClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLOCKEXTRACTORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : AtClockMonitor.c
 *
 * Created Date: Aug 7, 2017
 *
 * Description : Clock monitoring
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtClockMonitorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtClockMonitor)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
static uint8 m_methodsInit = 0;
static tAtClockMonitorMethods m_methods;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtObject self)
    {
    static char description[64];
    AtObject monitoredObject = AtClockMonitorMonitoredObjectGet(mThis(self));
    AtModule module = (AtModule)AtClockMonitorModuleGet(mThis(self));
    AtDevice dev = AtModuleDeviceGet(module);

    AtSnprintf(description, sizeof(description), "%sclock_monitor_%s", AtDeviceIdToString(dev), AtObjectToString(monitoredObject));
    return description;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtClockMonitor object = (AtClockMonitor)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(objectMonitor);
    mEncodeObjectDescription(module);
    }

static uint32 CurrentPpmGet(AtClockMonitor self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet ThresholdSet(AtClockMonitor self, eAtClockMonitorThreshold threshold, uint32 value)
    {
    AtUnused(self);
    AtUnused(threshold);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint32 ThresholdGet(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return 0;
    }

static uint32 ThresholdMax(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return 0;
    }

static eBool ThresholdIsSupported(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtFalse;
    }

static uint32 AlarmGet(AtClockMonitor self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryGet(AtClockMonitor self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtClockMonitor self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtClockMonitor self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    AtUnused(enableMask);
    return cAtErrorNotImplemented;
    }

static uint32 InterruptMaskGet(AtClockMonitor self)
    {
    AtUnused(self);
    return 0;
    }

static eBool InterruptMaskIsSupported(AtClockMonitor self, uint32 defectMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    return 0;
    }

static eAtRet Init(AtClockMonitor self)
    {
    uint32 allAlarms = cAtClockMonitorAlarmLossOfClock |
                       cAtClockMonitorFrequencyOutOfRange;
    return AtClockMonitorInterruptMaskSet(self, allAlarms, 0);
    }

static void OverrideAtObject(AtClockMonitor self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtClockMonitor self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtClockMonitor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, CurrentPpmGet);
        mMethodOverride(m_methods, ThresholdSet);
        mMethodOverride(m_methods, ThresholdGet);
        mMethodOverride(m_methods, ThresholdMax);
        mMethodOverride(m_methods, ThresholdIsSupported);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, InterruptMaskIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtClockMonitor);
    }

AtClockMonitor AtClockMonitorObjectInit(AtClockMonitor self, AtModuleClock clockModule, AtObject objectMonitor)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->objectMonitor = objectMonitor;
    self->module        = clockModule;

    return self;
    }

AtModuleClock AtClockMonitorModuleGet(AtClockMonitor self)
    {
    if (self)
        return self->module;
    return NULL;
    }

AtDevice AtClockMonitorDeviceGet(AtClockMonitor self)
    {
    return AtModuleDeviceGet((AtModule)AtClockMonitorModuleGet(self));
    }

/**
 * @addtogroup AtClockMonitor
 * @{
 */

/**
 * Initialize clock monitor to default state
 *
 * @param self This clock monitor
 *
 * @return AT return code
 */
eAtRet AtClockMonitorInit(AtClockMonitor self)
    {
    mNoParamAttributeSet(Init);
    }

/**
 * Get object that is being associated to this clock monitor
 *
 * @param self This clock monitor
 *
 * @return Associated object
 */
AtObject AtClockMonitorMonitoredObjectGet(AtClockMonitor self)
    {
    if (self)
        return self->objectMonitor;
    return NULL;
    }

/**
 * Get current PPM
 *
 * @param self This clock monitor
 *
 * @return Current PPM
 */
uint32 AtClockMonitorCurrentPpmGet(AtClockMonitor self)
    {
    mAttributeGet(CurrentPpmGet, uint32, 0);
    }

/**
 * Configure threshold to declare alarm
 *
 * @param self This clock monitor
 * @param threshold @ref eAtClockMonitorThreshold "threshold type"
 * @param value threshold value
 *
 * @return AT return code
 */
eAtRet AtClockMonitorThresholdSet(AtClockMonitor self, eAtClockMonitorThreshold threshold, uint32 value)
    {
    if (value > AtClockMonitorThresholdMax(self, threshold))
        return cAtErrorOutOfRangParm;

    mTwoParamsAttributeSet(ThresholdSet, threshold, value);
    }

/**
 * Get threshold
 *
 * @param self This clock monitor
 * @param threshold @ref eAtClockMonitorThreshold "threshold type"
 *
 * @return Threshold value
 */
uint32 AtClockMonitorThresholdGet(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    mOneParamAttributeGet(ThresholdGet, threshold, uint32, 0);
    }

/**
 * Get max value of input threshold
 *
 * @param self This clock monitor
 * @param threshold @ref eAtClockMonitorThreshold "threshold type"
 *
 * @return Threshold max value
 */
uint32 AtClockMonitorThresholdMax(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    mOneParamAttributeGet(ThresholdMax, threshold, uint32, 0);
    }

/**
 * Check if threshold type is support or not
 *
 * @param self This clock monitor
 * @param threshold @ref eAtClockMonitorThreshold "threshold type"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtClockMonitorThresholdIsSupported(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    mOneParamAttributeGet(ThresholdIsSupported, threshold, eBool, cAtFalse);
    }

/**
 * Get current alarm
 *
 * @param self This clock monitor
 *
 * @return @ref eAtClockMonitorAlarmType "Current alarm"
 */
uint32 AtClockMonitorAlarmGet(AtClockMonitor self)
    {
    mAttributeGet(AlarmGet, uint32, 0);
    }

/**
 * Get alarm change history
 *
 * @param self This clock monitor
 *
 * @return @ref eAtClockMonitorAlarmType "Alarm change history"
 */
uint32 AtClockMonitorAlarmHistoryGet(AtClockMonitor self)
    {
    mAttributeGet(AlarmHistoryGet, uint32, 0);
    }

/**
 * Read then clear alarm change history
 *
 * @param self This clock monitor
 *
 * @return @ref eAtClockMonitorAlarmType "Alarm change history"
 */
uint32 AtClockMonitorAlarmHistoryClear(AtClockMonitor self)
    {
    mAttributeGet(AlarmHistoryClear, uint32, 0);
    }

/**
 * Set interrupt mask
 *
 * @param self This clock monitor
 * @param defectMask @ref eAtClockMonitorAlarmType "Mask" of defects need to be configured
 * @param enableMask @ref eAtClockMonitorAlarmType "Enabled mask"
 *
 * @return AT return code.
 */
eAtRet AtClockMonitorInterruptMaskSet(AtClockMonitor self, uint32 defectMask, uint32 enableMask)
    {
    mTwoParamsAttributeSet(InterruptMaskSet, defectMask, enableMask);
    }

/**
 * Get interrupt mask
 *
 * @param self This clock monitor
 *
 * @return @ref eAtClockMonitorAlarmType "Interrupt mask"
 */
uint32 AtClockMonitorInterruptMaskGet(AtClockMonitor self)
    {
    mAttributeGet(InterruptMaskGet, uint32, 0);
    }

/**
 * Check if an interrupt mask is supported or not.
 *
 * @param self This clock monitor
 * @param defectMask @ref eAtClockMonitorAlarmType "Mask" of defect needs to be checked
 *
 * @retval cAtTrue if the interrupt mask is supported.
 *         cAtFalse if the interrupt mask is not supported.
 *
 * @note This API expects only one mask to be input.
 */
eBool AtClockMonitorInterruptMaskIsSupported(AtClockMonitor self, uint32 defectMask)
    {
    mOneParamAttributeGet(InterruptMaskIsSupported, defectMask, eBool, cAtFalse);
    }
/**
 * @}
 */

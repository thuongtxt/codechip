/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtDeviceQuerierInternal.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : Device querier
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICEQUERIERINTERNAL_H_
#define _ATDEVICEQUERIERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtQuerierInternal.h"
#include "AtClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDeviceQuerier
    {
    tAtQuerier super;
    }tAtDeviceQuerier;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtQuerier AtDeviceQuerierObjectInit(AtQuerier self);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEVICEQUERIERINTERNAL_H_ */


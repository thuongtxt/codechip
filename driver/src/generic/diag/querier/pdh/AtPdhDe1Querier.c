/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtPdhDe1Querier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : DE1 querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../pdh/AtPdhDe1Internal.h"
#include "AtPdhChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhDe1Querier
    {
    tAtPdhChannelQuerier super;
    }tAtPdhDe1Querier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtChannelQuerierMethods *m_AtChannelQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtPdhDe1AlarmLos, "los"},
                                           {cAtPdhDe1AlarmLof, "lof"},
                                           {cAtPdhDe1AlarmAis, "ais"},
                                           {cAtPdhDe1AlarmRai, "rai"},
                                           {cAtPdhDe1AlarmLomf, "lomf"},
                                           {cAtPdhDe1AlarmSfBer, "sf-ber"},
                                           {cAtPdhDe1AlarmSdBer, "sd-ber"},
                                           {cAtPdhDe1AlarmSigLof, "sig-lof"},
                                           {cAtPdhDe1AlarmSigRai, "sig-rai"},
                                           {cAtPdhDe1AlarmEbit, "ebit"},
                                           {cAtPdhDs1AlarmBomChange, "bom-change"},
                                           {cAtPdhDs1AlarmLapdChange, "lapd-change"},
                                           {cAtPdhDs1AlarmInbandLoopCodeChange, "inband-loopcode-change"},
                                           {cAtPdhDe1AlarmAisCi, "ais-ci"},
                                           {cAtPdhDe1AlarmRaiCi, "rai-ci"},
                                           {cAtPdhDs1AlarmPrmLBBitChange, "prm-lbbit-change"},
                                           {cAtPdhE1AlarmSsmChange, "ssm-change"},
                                           {cAtPdhDe1AlarmBerTca, "ber-tca"},
                                           {cAtPdhDe1AlarmClockStateChange, "clock-state-change"}};
    AtUnused(self);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static tAtTypeNamePair *CounterNames(AtChannelQuerier self, uint32 *numCounters)
    {
    static tAtTypeNamePair counters[] = {{cAtPdhDe1CounterBpvExz, "bpvexz"},
                                         {cAtPdhDe1CounterCrc, "crc"},
                                         {cAtPdhDe1CounterFe, "fe"},
                                         {cAtPdhDe1CounterRei, "rei"},
                                         {cAtPdhDe1CounterTxCs, "txcs"}};
    AtUnused(self);
    if (numCounters)
        *numCounters = mCount(counters);
    return counters;
    }

static eAtCounterKind CounterKind(AtChannelQuerier self, AtChannel channel, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(counterType);
    return cAtCounterKindError;
    }

static eBool ProblemsShouldQuery(AtChannelQuerier self, AtChannel channel)
    {
    AtPdhDe1 de1 = (AtPdhDe1)channel;

    AtUnused(self);

    if (AtPdhDe1Ds0MaskGet(de1))
        return cAtTrue;

    return m_AtChannelQuerierMethods->ProblemsShouldQuery(self, channel);
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, m_AtChannelQuerierMethods, sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, CounterNames);
        mMethodOverride(m_AtChannelQuerierOverride, CounterKind);
        mMethodOverride(m_AtChannelQuerierOverride, ProblemsShouldQuery);
        mMethodOverride(m_AtChannelQuerierOverride, AlarmNames);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe1Querier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtPdhDe1SharedQuerier(void)
    {
    static tAtPdhDe1Querier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

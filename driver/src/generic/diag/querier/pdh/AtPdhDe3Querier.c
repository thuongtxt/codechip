/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtPdhDe3Querier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : DE3 querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe3.h"
#include "AtPdhChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhDe3Querier
    {
    tAtPdhChannelQuerier super;
    }tAtPdhDe3Querier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtChannelQuerierMethods *m_AtChannelQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtPdhDe3AlarmLos, "los"},
                                           {cAtPdhDe3AlarmLof, "lof"},
                                           {cAtPdhDe3AlarmAis, "ais"},
                                           {cAtPdhDe3AlarmRai, "rai"},
                                           {cAtPdhDe3AlarmSfBer, "sf-ber"},
                                           {cAtPdhDe3AlarmSdBer, "sd-ber"},
                                           {cAtPdhDs3AlarmIdle, "idle"},
                                           {cAtPdhDs3AlarmAicChange, "aic-change"},
                                           {cAtPdhDs3AlarmFeacChange, "feac-change"},
                                           {cAtPdhE3AlarmTim, "tim"},
                                           {cAtPdhE3AlarmPldTypeChange, "pldtype-change"},
                                           {cAtPdhE3AlarmTmChange, "tm-change"},
                                           {cAtPdhDe3AlarmSsmChange, "ssm-change"},
                                           {cAtPdhDs3AlarmMdlChange, "mdl-change"},
                                           {cAtPdhDe3AlarmBerTca, "ber-tca"},
                                           {cAtPdhDe3AlarmClockStateChange, "clock-state-change"}};
    AtUnused(self);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static tAtTypeNamePair *CounterNames(AtChannelQuerier self, uint32 *numCounters)
    {
    static tAtTypeNamePair counters[] = {{cAtPdhDe3CounterBip8, "bip8"},
                                         {cAtPdhDe3CounterRei, "rei"},
                                         {cAtPdhDe3CounterFBit, "f-bit"},
                                         {cAtPdhDe3CounterPBit, "p-bit"},
                                         {cAtPdhDe3CounterCPBit, "cp-bit"},
                                         {cAtPdhDe3CounterBpvExz, "bpv-exz"},
                                         {cAtPdhDe3CounterTxCs, "tx-cs"}};
    AtUnused(self);
    if (numCounters)
        *numCounters = mCount(counters);
    return counters;
    }

static eAtCounterKind CounterKind(AtChannelQuerier self, AtChannel channel, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(counterType);
    return cAtCounterKindError;
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, m_AtChannelQuerierMethods, sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, CounterNames);
        mMethodOverride(m_AtChannelQuerierOverride, CounterKind);
        mMethodOverride(m_AtChannelQuerierOverride, AlarmNames);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe3Querier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtPdhDe3SharedQuerier(void)
    {
    static tAtPdhDe3Querier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

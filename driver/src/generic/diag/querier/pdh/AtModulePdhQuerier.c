/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtModulePdhQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : PDH querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdh.h"
#include "../common/AtQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtModulePdhQuerier
    {
    tAtQuerier super;
    }tAtModulePdhQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtQuerierMethods m_AtQuerierOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtModulePdh pdhModule = (AtModulePdh)object;
    uint32 numChannels, channel_i;

    AtUnused(self);

    numChannels = AtModulePdhNumberOfDe1sGet(pdhModule);
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtPdhDe1 de1 = AtModulePdhDe1Get(pdhModule, channel_i);
        AtQuerier querier = AtChannelQuerierGet((AtChannel)de1);
        AtQuerierProblemsQuery(querier, (AtObject)de1, debugger);
        }

    numChannels = AtModulePdhNumberOfDe3sGet(pdhModule);
    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        AtPdhDe3 de3 = AtModulePdhDe3Get(pdhModule, channel_i);
        AtQuerier querier = AtChannelQuerierGet((AtChannel)de3);
        AtQuerierProblemsQuery(querier, (AtObject)de3, debugger);
        }
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, mMethodsGet(self), sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(self, &m_AtQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePdhQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtModulePdhSharedQuerier(void)
    {
    static tAtModulePdhQuerier querier;
    static AtQuerier pQuerier = NULL;

    if (pQuerier == NULL)
        pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

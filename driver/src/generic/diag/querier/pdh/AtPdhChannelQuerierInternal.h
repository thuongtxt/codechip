/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtPdhChannelQuerierInternal.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : PDH chanel querier
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHCHANNELQUERIERINTERNAL_H_
#define _ATPDHCHANNELQUERIERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhChannelQuerier
    {
    tAtChannelQuerier super;
    }tAtPdhChannelQuerier;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtQuerier AtPdhChannelQuerierObjectInit(AtQuerier self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHCHANNELQUERIERINTERNAL_H_ */


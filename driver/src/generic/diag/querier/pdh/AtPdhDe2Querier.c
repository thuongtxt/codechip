/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *.
 * File        : AtPdhDe2Querier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : DE3 querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe2.h"
#include "AtPdhChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhDe2Querier
    {
    tAtPdhChannelQuerier super;
    }tAtPdhDe2Querier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtChannelQuerierMethods *m_AtChannelQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtPdhDe2AlarmLof, "lof"},
                                           {cAtPdhDe2AlarmAis, "ais"},
                                           {cAtPdhDe2AlarmRai, "rai"}};
    AtUnused(self);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, m_AtChannelQuerierMethods, sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, AlarmNames);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe2Querier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtPdhDe2SharedQuerier(void)
    {
    static tAtPdhDe2Querier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

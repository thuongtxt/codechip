/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtPwQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : ETH Port querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPw.h"
#include "AtPwCounters.h"
#include "../common/AtChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mProblemsQueryPwCounterPut(counterType)      ProblemsQueryCounterPut(self, channel, debugger, cAtPwCounterType##counterType, #counterType, counter, AtPwCounters##counterType##Get)
#define mProblemsQueryTdmPwCounterPut(counterType)   ProblemsQueryCounterPut(self, channel, debugger, cAtPwTdmCounterType##counterType, #counterType, counter, (uint32 (*)(AtPwCounters self))AtPwTdmCounters##counterType##Get)
#define mProblemsQueryCESoPPwCounterPut(counterType) ProblemsQueryCounterPut(self, channel, debugger, cAtPwCESoPCounterType##counterType, #counterType, counter, (uint32 (*)(AtPwCounters self))AtPwCESoPCounters##counterType##Get)
#define mProblemsQueryCepPwCounterPut(counterType)   ProblemsQueryCounterPut(self, channel, debugger, cAtPwCepCounterType##counterType, #counterType, counter, (uint32 (*)(AtPwCounters self))AtPwCepCounters##counterType##Get)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwQuerier
    {
    tAtChannelQuerier super;
    }tAtPwQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtChannelQuerierMethods *m_AtChannelQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtPwAlarmTypeLops, "lops"},
                                           {cAtPwAlarmTypeJitterBufferOverrun, "jitter-buffer-overrun"},
                                           {cAtPwAlarmTypeJitterBufferUnderrun, "jitter-buffer-underrun"},
                                           {cAtPwAlarmTypeLBit, "l-bit"},
                                           {cAtPwAlarmTypeRBit, "r-bit"},
                                           {cAtPwAlarmTypeMBit, "m-bit"},
                                           {cAtPwAlarmTypeNBit, "n-bit"},
                                           {cAtPwAlarmTypePBit, "p-bit"},
                                           {cAtPwAlarmTypeMissingPacket, "missing-packet"},
                                           {cAtPwAlarmTypeExcessivePacketLossRate, "excessive-packet-loss-rate"},
                                           {cAtPwAlarmTypeStrayPacket, "stray-packet"},
                                           {cAtPwAlarmTypeMalformedPacket, "malformed-packet"},
                                           {cAtPwAlarmTypeRemotePacketLoss, "remote-packet-loss"},
                                           {cAtpwAlarmTypeMisConnection, "misconnection"}};
    AtUnused(self);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static eAtCounterKind CounterKind(AtChannelQuerier self, AtChannel channel, uint32 counterType)
    {
    if (!AtChannelCounterIsSupported(channel, (uint16)counterType))
        return cAtCounterKindNone;

    AtUnused(self);

    switch (counterType)
        {
        case cAtPwCounterTypeTxPackets             : return cAtCounterKindGood;
        case cAtPwCounterTypeTxPayloadBytes        : return cAtCounterKindGood;
        case cAtPwCounterTypeRxPackets             : return cAtCounterKindGood;
        case cAtPwCounterTypeRxPayloadBytes        : return cAtCounterKindGood;
        case cAtPwCounterTypeRxDiscardedPackets    : return cAtCounterKindError;
        case cAtPwCounterTypeRxMalformedPackets    : return cAtCounterKindError;
        case cAtPwCounterTypeRxReorderedPackets    : return cAtCounterKindInfo;
        case cAtPwCounterTypeRxLostPackets         : return cAtCounterKindError;
        case cAtPwCounterTypeRxOutOfSeqDropPackets : return cAtCounterKindError;
        case cAtPwCounterTypeRxOamPackets          : return cAtCounterKindInfo;
        case cAtPwCounterTypeRxStrayPackets        : return cAtCounterKindError;
        case cAtPwCounterTypeRxDuplicatedPackets   : return cAtCounterKindError;
        case cAtPwTdmCounterTypeTxLbitPackets      : return cAtCounterKindError;
        case cAtPwTdmCounterTypeTxRbitPackets      : return cAtCounterKindError;
        case cAtPwTdmCounterTypeRxLbitPackets      : return cAtCounterKindError;
        case cAtPwTdmCounterTypeRxRbitPackets      : return cAtCounterKindError;
        case cAtPwTdmCounterTypeRxJitBufOverrun    : return cAtCounterKindError;
        case cAtPwTdmCounterTypeRxJitBufUnderrun   : return cAtCounterKindError;
        case cAtPwTdmCounterTypeRxLops             : return cAtCounterKindError;
        case cAtPwTdmCounterTypeRxPacketsSentToTdm : return cAtCounterKindInfo;
        case cAtPwCESoPCounterTypeTxMbitPackets    : return cAtCounterKindError;
        case cAtPwCESoPCounterTypeRxMbitPackets    : return cAtCounterKindError;
        case cAtPwCepCounterTypeTxNbitPackets      : return cAtCounterKindInfo;
        case cAtPwCepCounterTypeTxPbitPackets      : return cAtCounterKindInfo;
        case cAtPwCepCounterTypeRxNbitPackets      : return cAtCounterKindInfo;
        case cAtPwCepCounterTypeRxPbitPackets      : return cAtCounterKindInfo;
        default                                    : return cAtCounterKindError;
        }
    }

static void ProblemsQueryCounterPut(AtChannelQuerier self, AtChannel channel, AtDebugger debugger,
                                    eAtPwCounterType counterType, const char *counterName,
                                    AtPwCounters counter,
                                    uint32 (*CounterGet)(AtPwCounters channel))
    {
    eAtCounterKind kind = mMethodsGet(self)->CounterKind(self, channel, counterType);
    AtQuerierUtilProblemsQueryCounterPut((AtObject)channel, debugger,
                                  counterName, CounterGet(counter), kind);
    }

static void ProblemsQueryCounters(AtChannelQuerier self, AtChannel channel, AtDebugger debugger)
    {
    AtPw pw = (AtPw)channel;
    eAtPwType pwType = AtPwTypeGet(pw);
    AtPwCounters counter = NULL;
    eAtRet ret;

    AtUnused(self);

    ret = AtChannelAllCountersClear(channel, &counter);
    if (ret != cAtOk)
        return;

    mProblemsQueryPwCounterPut(TxPackets);
    mProblemsQueryPwCounterPut(TxPayloadBytes);
    mProblemsQueryPwCounterPut(RxPackets);
    mProblemsQueryPwCounterPut(RxPayloadBytes);
    mProblemsQueryPwCounterPut(RxDiscardedPackets);
    mProblemsQueryPwCounterPut(RxMalformedPackets);
    mProblemsQueryPwCounterPut(RxReorderedPackets);
    mProblemsQueryPwCounterPut(RxLostPackets);
    mProblemsQueryPwCounterPut(RxOutOfSeqDropPackets);
    mProblemsQueryPwCounterPut(RxOamPackets);
    mProblemsQueryPwCounterPut(RxStrayPackets);
    mProblemsQueryPwCounterPut(RxDuplicatedPackets);

    if (AtPwIsTdmPw(pw))
        {
        mProblemsQueryTdmPwCounterPut(TxLbitPackets);
        mProblemsQueryTdmPwCounterPut(TxLbitPackets);
        mProblemsQueryTdmPwCounterPut(TxRbitPackets);
        mProblemsQueryTdmPwCounterPut(RxLbitPackets);
        mProblemsQueryTdmPwCounterPut(RxRbitPackets);
        mProblemsQueryTdmPwCounterPut(RxJitBufOverrun);
        mProblemsQueryTdmPwCounterPut(RxJitBufUnderrun);
        mProblemsQueryTdmPwCounterPut(RxLops);
        mProblemsQueryTdmPwCounterPut(RxPacketsSentToTdm);
        }

    if (pwType == cAtPwTypeCESoP)
        {
        mProblemsQueryCESoPPwCounterPut(TxMbitPackets);
        mProblemsQueryCESoPPwCounterPut(RxMbitPackets);
        }

    if (pwType == cAtPwTypeCEP)
        {
        mProblemsQueryCepPwCounterPut(TxNbitPackets);
        mProblemsQueryCepPwCounterPut(TxPbitPackets);
        mProblemsQueryCepPwCounterPut(RxNbitPackets);
        mProblemsQueryCepPwCounterPut(RxPbitPackets);
        }
    }

static eBool ProblemsShouldQuery(AtChannelQuerier self, AtChannel channel)
    {
    AtPw pw = (AtPw)channel;

    AtUnused(self);

    if (!AtChannelIsEnabled(channel))
        return cAtFalse;

    if (AtPwBoundCircuitGet(pw) == NULL)
        return cAtFalse;

    if (AtPwEthPortGet(pw) == NULL)
        return cAtFalse;

    return cAtTrue;
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, m_AtChannelQuerierMethods, sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, CounterKind);
        mMethodOverride(m_AtChannelQuerierOverride, ProblemsShouldQuery);
        mMethodOverride(m_AtChannelQuerierOverride, AlarmNames);
        mMethodOverride(m_AtChannelQuerierOverride, ProblemsQueryCounters);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtPwSharedQuerier(void)
    {
    static tAtPwQuerier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

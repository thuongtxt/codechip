/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtSdhChannelQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : SDH channel querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtSdhChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtQuerierMethods        m_AtQuerierOverride;
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtQuerierMethods *m_AtQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)object;
    uint8 numSubChannels;
    uint8 subChannel_i;

    m_AtQuerierMethods->ProblemsQuery(self, object, debugger);

    numSubChannels = AtSdhChannelNumberOfSubChannelsGet(sdhChannel);

    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(sdhChannel, subChannel_i);
        AtQuerier querier = AtChannelQuerierGet((AtChannel)subChannel);
        if (querier)
            AtQuerierProblemsQuery(querier, (AtObject)subChannel, debugger);
        }
    }

static eBool ProblemsShouldQuery(AtChannelQuerier self, AtChannel channel)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)channel;

    AtUnused(self);

    if (!AtChannelIsEnabled(channel))
        return cAtFalse;

    if (AtChannelPrbsEngineGet(channel))
        return cAtTrue;

    if (AtChannelBoundPwGet(channel))
        return cAtTrue;

    if (AtChannelBoundEncapChannelGet(channel))
        return cAtTrue;

    if (AtSdhChannelNumberOfSubChannelsGet(sdhChannel) > 0)
        return cAtTrue;

    if (AtSdhChannelMapChannelGet(sdhChannel))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    AtQuerier querier = (AtQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, m_AtQuerierMethods, sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(querier, &m_AtQuerierOverride);
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, mMethodsGet(querier), sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, ProblemsShouldQuery);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhChannelQuerier);
    }

AtQuerier AtSdhChannelQuerierObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtSdhChannelSharedQuerier(void)
    {
    static tAtSdhChannelQuerier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = AtSdhChannelQuerierObjectInit((AtQuerier)&querier);
    return pQuerier;
    }


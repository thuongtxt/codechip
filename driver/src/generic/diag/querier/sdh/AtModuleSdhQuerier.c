/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtModuleSdhQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : SDH querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleSdh.h"
#include "../common/AtQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtModuleSdhQuerier
    {
    tAtQuerier super;
    }tAtModuleSdhQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtQuerierMethods m_AtQuerierOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)object;
    uint8 numLines = AtModuleSdhMaxLinesGet(sdhModule);
    uint8 line_i;

    AtUnused(self);

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, line_i);
        AtQuerier querier = AtChannelQuerierGet((AtChannel)line);
        AtQuerierProblemsQuery(querier, (AtObject)line, debugger);
        }
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, mMethodsGet(self), sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(self, &m_AtQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleSdhQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtModuleSdhSharedQuerier(void)
    {
    static tAtModuleSdhQuerier querier;
    static AtQuerier pQuerier = NULL;

    if (pQuerier == NULL)
        pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

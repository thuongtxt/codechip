/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtSdhChannelQuerierInternal.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : SDH channel querier
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHCHANNELQUERIERINTERNAL_H_
#define _ATSDHCHANNELQUERIERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhChannelQuerier
    {
    tAtChannelQuerier super;
    }tAtSdhChannelQuerier;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtQuerier AtSdhChannelQuerierObjectInit(AtQuerier self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHCHANNELQUERIERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtSdhPathQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : SDH Line querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "AtSdhChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSdhPathQuerier
    {
    tAtSdhChannelQuerier super;
    }tAtSdhPathQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtChannelQuerierMethods *m_AtChannelQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtSdhPathAlarmAis             , "ais"},
                                           {cAtSdhPathAlarmLop             , "lop"},
                                           {cAtSdhPathAlarmTim             , "tim"},
                                           {cAtSdhPathAlarmUneq            , "uneq"},
                                           {cAtSdhPathAlarmPlm             , "plm"},
                                           {cAtSdhPathAlarmRdi             , "rdi"},
                                           {cAtSdhPathAlarmErdiS           , "erdi-s"},
                                           {cAtSdhPathAlarmErdiP           , "erdi-p"},
                                           {cAtSdhPathAlarmErdiC           , "erdi-c"},
                                           {cAtSdhPathAlarmBerSd           , "ber-sd"},
                                           {cAtSdhPathAlarmBerSf           , "ber-sf"},
                                           {cAtSdhPathAlarmLom             , "lom"},
                                           {cAtSdhPathAlarmRfi             , "rfi"},
                                           {cAtSdhPathAlarmPayloadUneq     , "payload-uneq"},
                                           {cAtSdhPathAlarmBerTca          , "ber-tca"},
                                           {cAtSdhPathAlarmRfiS            , "rfi-s"},
                                           {cAtSdhPathAlarmRfiC            , "rfi-c"},
                                           {cAtSdhPathAlarmRfiP            , "rfi-p"},
                                           {cAtSdhPathAlarmClockStateChange, "clock-state-change"},
                                           {cAtSdhPathAlarmTtiChange       , "tti-change"},
                                           {cAtSdhPathAlarmPslChange       , "psl-change "}};
    AtUnused(self);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static tAtTypeNamePair *CounterNames(AtChannelQuerier self, uint32 *numCounters)
    {
    static tAtTypeNamePair counters[] = {{cAtSdhPathCounterTypeBip, "bip"},
                                         {cAtSdhPathCounterTypeRei, "rei"},
                                         {cAtSdhPathCounterTypeRxPPJC, "rx-ppjc"},
                                         {cAtSdhPathCounterTypeRxNPJC, "rx-npjc"},
                                         {cAtSdhPathCounterTypeTxPPJC, "tx-ppjc"},
                                         {cAtSdhPathCounterTypeTxNPJC, "tx-npjc"}};
    AtUnused(self);
    if (numCounters)
        *numCounters = mCount(counters);
    return counters;
    }

static eAtCounterKind CounterKind(AtChannelQuerier self, AtChannel channel, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(channel);

    switch (counterType)
        {
        case cAtSdhPathCounterTypeRxPPJC: return cAtCounterKindInfo;
        case cAtSdhPathCounterTypeRxNPJC: return cAtCounterKindInfo;
        case cAtSdhPathCounterTypeTxPPJC: return cAtCounterKindInfo;
        case cAtSdhPathCounterTypeTxNPJC: return cAtCounterKindInfo;
        default                         : return cAtCounterKindError;
        }
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, m_AtChannelQuerierMethods, sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, CounterNames);
        mMethodOverride(m_AtChannelQuerierOverride, CounterKind);
        mMethodOverride(m_AtChannelQuerierOverride, AlarmNames);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhPathQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtSdhPathSharedQuerier(void)
    {
    static tAtSdhPathQuerier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

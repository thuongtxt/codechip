/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtSdhLineQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : SDH Line querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "AtSdhChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSdhLineQuerier
    {
    tAtSdhChannelQuerier super;
    }tAtSdhLineQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtChannelQuerierMethods *m_AtChannelQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtSdhLineAlarmLos, "los"},
                                           {cAtSdhLineAlarmOof, "oof"},
                                           {cAtSdhLineAlarmLof, "lof"},
                                           {cAtSdhLineAlarmTim, "tim"},
                                           {cAtSdhLineAlarmAis, "ais"},
                                           {cAtSdhLineAlarmRdi, "rdi"},
                                           {cAtSdhLineAlarmBerSd, "ber-sd"},
                                           {cAtSdhLineAlarmBerSf, "ber-sf"},
                                           {cAtSdhLineAlarmS1Change, "s1-change"},
                                           {cAtSdhLineAlarmRsBerSd, "ber-sd"},
                                           {cAtSdhLineAlarmRsBerSf, "ber-sf"},
                                           {cAtSdhLineAlarmK1Change, "k1-change"},
                                           {cAtSdhLineAlarmK2Change, "k2-change"},
                                           {cAtSdhLineAlarmBerTca, "ber-tca"},
                                           {cAtSdhLineAlarmRfi, "rfi"}};
    AtUnused(self);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static tAtTypeNamePair *CounterNames(AtChannelQuerier self, uint32 *numCounters)
    {
    static tAtTypeNamePair counters[] = {{cAtSdhLineCounterTypeB1, "b1"},
                                         {cAtSdhLineCounterTypeB2, "b2"},
                                         {cAtSdhLineCounterTypeRei, "b3"}};
    AtUnused(self);
    if (numCounters)
        *numCounters = mCount(counters);
    return counters;
    }

static eAtCounterKind CounterKind(AtChannelQuerier self, AtChannel channel, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(counterType);
    return cAtCounterKindError;
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, m_AtChannelQuerierMethods, sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, CounterNames);
        mMethodOverride(m_AtChannelQuerierOverride, CounterKind);
        mMethodOverride(m_AtChannelQuerierOverride, AlarmNames);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhLineQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtSdhLineSharedQuerier(void)
    {
    static tAtSdhLineQuerier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

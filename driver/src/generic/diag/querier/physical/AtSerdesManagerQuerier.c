/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtSerdesManagerQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : SERDES manager querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSerdesManager.h"
#include "../common/AtQueriers.h"
#include "../common/AtQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSerdesManagerQuerier
    {
    tAtQuerier super;
    }tAtSerdesManagerQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtQuerierMethods m_AtQuerierOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtSerdesManager manager = (AtSerdesManager)object;
    uint32 numSerdes = AtSerdesManagerNumSerdesControllers(manager);
    uint32 serdes_i;

    AtUnused(self);

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController controller = AtSerdesManagerSerdesControllerGet(manager, serdes_i);
        AtQuerier querier = AtSerdesControllerQuerierGet(controller);
        if (querier)
            AtQuerierProblemsQuery(querier, (AtObject)controller, debugger);
        }
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, mMethodsGet(self), sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(self, &m_AtQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSerdesManagerQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtSerdesManagerSharedQuerier(void)
    {
    static tAtSerdesManagerQuerier querier;
    static AtQuerier pQuerier = NULL;

    if (pQuerier == NULL)
        pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

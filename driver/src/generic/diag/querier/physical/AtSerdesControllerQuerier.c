/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtSerdesControllerQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : SDH querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSerdesController.h"
#include "../common/AtQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSerdesControllerQuerier
    {
    tAtQuerier super;
    }tAtSerdesControllerQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtQuerierMethods m_AtQuerierOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtSerdesController serdes, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtSerdesAlarmTypeLos, "los"},
                                           {cAtSerdesAlarmTypeTxFault, "tx-fault"},
                                           {cAtSerdesAlarmTypeRxFault, "rx-fault"},
                                           {cAtSerdesAlarmTypeNotAligned, "not-aligned"},
                                           {cAtSerdesAlarmTypeNotSync, "not-sync"},
                                           {cAtSerdesAlarmTypeLinkDown, "link-down"}};
    AtUnused(serdes);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static char *AlarmToString(AtSerdesController serdes, uint32 alarm, char *buffer, uint32 bufferSize)
    {
    uint32 numAlarms;
    tAtTypeNamePair *alarmNames = AlarmNames(serdes, &numAlarms);
    return AtQuerierUtilAlarmToString(alarm, alarmNames, numAlarms, buffer, bufferSize);
    }

static void ProblemsQueryDefectWithHandler(AtSerdesController serdes, AtDebugger debugger,
                                           const char *defectKind,
                                           uint32 (*DefectGet)(AtSerdesController))
    {
    uint32 alarms;
    AtDebugEntry entry;
    uint32 bufferSize;
    char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);

    alarms = DefectGet(serdes);
    if (alarms == 0)
        return;

    entry = AtQuerierUtilProblemEntryCreate((AtObject)serdes, debugger);
    AtSnprintf(buffer, bufferSize, "%s: %s", defectKind, AlarmToString(serdes, alarms, buffer, bufferSize));
    AtDebuggerEntryValueSet(entry, buffer);
    AtDebuggerEntryAdd(debugger, entry);
    }

static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtSerdesController serdes = (AtSerdesController)object;
    AtUnused(self);
    ProblemsQueryDefectWithHandler(serdes, debugger, "history", AtSerdesControllerAlarmHistoryClear);
    ProblemsQueryDefectWithHandler(serdes, debugger, "alarm", AtSerdesControllerAlarmGet);
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, mMethodsGet(self), sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(self, &m_AtQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSerdesControllerQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtSerdesControllerSharedQuerier(void)
    {
    static tAtSerdesControllerQuerier querier;
    static AtQuerier pQuerier = NULL;

    if (pQuerier == NULL)
        pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

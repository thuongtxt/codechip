/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtPrbsEngineQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : PRBS engine querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPrbsEngine.h"
#include "../common/AtQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPrbsEngineQuerier
    {
    tAtQuerier super;
    }tAtPrbsEngineQuerier;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtQuerierMethods m_AtQuerierOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtPrbsEngine engine, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtPrbsEngineAlarmTypeError, "error"},
                                           {cAtPrbsEngineAlarmTypeLossSync, "loss-sync"}};
    AtUnused(engine);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static void ProblemsQueryDefectWithHandler(AtPrbsEngine engine, AtDebugger debugger,
                                           const char *defectKind,
                                           uint32 (*DefectGet)(AtPrbsEngine engine))
    {
    uint32 alarms;
    AtDebugEntry entry;
    uint32 bufferSize;
    char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
    uint32 numAlarms;
    tAtTypeNamePair *alarmNames;

    alarms = DefectGet(engine);
    if (alarms == 0)
        return;

    entry = AtQuerierUtilProblemEntryCreate((AtObject)engine, debugger);
    alarmNames = AlarmNames(engine, &numAlarms);
    AtSnprintf(buffer, bufferSize, "%s: %s", defectKind, AtQuerierUtilAlarmToString(alarms, alarmNames, numAlarms, buffer, bufferSize));
    AtDebuggerEntryValueSet(entry, buffer);
    AtDebuggerEntryAdd(debugger, entry);
    }

static tAtTypeNamePair *CounterNames(AtPrbsEngine self, uint32 *numCounters)
    {
    static tAtTypeNamePair counters[] = {{cAtPrbsEngineCounterTxFrame, "TxFrame"},
                                         {cAtPrbsEngineCounterRxFrame, "RxFrame"},
                                         {cAtPrbsEngineCounterTxBit, "TxBit"},
                                         {cAtPrbsEngineCounterRxBit, "RxBit"},
                                         {cAtPrbsEngineCounterRxBitError, "RxBitError"},
                                         {cAtPrbsEngineCounterRxBitLastSync, "RxBitLastSync"},
                                         {cAtPrbsEngineCounterRxBitErrorLastSync, "RxBitErrorLastSync"},
                                         {cAtPrbsEngineCounterRxBitLoss, "RxBitLoss"},
                                         {cAtPrbsEngineCounterRxSync, "RxSync"},
                                         {cAtPrbsEngineCounterRxLostFrame, "RxLostFrame"},
                                         {cAtPrbsEngineCounterRxErrorFrame, "RxErrorFrame"}};
    AtUnused(self);
    if (numCounters)
        *numCounters = mCount(counters);
    return counters;
    }

static eAtCounterKind CounterKind(AtPrbsEngine engine, uint32 counterType)
    {
    if (!AtPrbsEngineCounterIsSupported(engine, (uint16)counterType))
        return cAtCounterKindNone;

    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame           : return cAtCounterKindGood;
        case cAtPrbsEngineCounterRxFrame           : return cAtCounterKindGood;
        case cAtPrbsEngineCounterTxBit             : return cAtCounterKindGood;
        case cAtPrbsEngineCounterRxBit             : return cAtCounterKindGood;
        case cAtPrbsEngineCounterRxBitError        : return cAtCounterKindGood;
        case cAtPrbsEngineCounterRxBitLastSync     : return cAtCounterKindInfo;
        case cAtPrbsEngineCounterRxBitErrorLastSync: return cAtCounterKindError;
        case cAtPrbsEngineCounterRxBitLoss         : return cAtCounterKindError;
        case cAtPrbsEngineCounterRxSync            : return cAtCounterKindGood;
        case cAtPrbsEngineCounterRxLostFrame       : return cAtCounterKindError;
        case cAtPrbsEngineCounterRxErrorFrame      : return cAtCounterKindError;
        default                                    : return cAtCounterKindError;
        }
    }

static void ProblemsQueryCounters(AtPrbsEngine engine, AtDebugger debugger)
    {
    uint32 numCounters, counter_i;
    tAtTypeNamePair *counterNames = CounterNames(engine, &numCounters);

    for (counter_i = 0; counter_i < numCounters; counter_i++)
        {
        tAtTypeNamePair *counterName = &(counterNames[counter_i]);
        uint32 counterValue = AtPrbsEngineCounterClear(engine, (uint16)counterName->type);
        eAtCounterKind kind = CounterKind(engine, counterName->type);
        AtQuerierUtilProblemsQueryCounterPut((AtObject)engine, debugger, counterName->name, counterValue, kind);
        }
    }

static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtPrbsEngine engine = (AtPrbsEngine)object;

    if (!AtPrbsEngineIsEnabled(engine))
        return;

    AtUnused(self);

    ProblemsQueryDefectWithHandler(engine, debugger, "history", AtPrbsEngineAlarmHistoryClear);
    ProblemsQueryDefectWithHandler(engine, debugger, "alarm", AtPrbsEngineAlarmGet);
    ProblemsQueryCounters(engine, debugger);
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, mMethodsGet(self), sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(self, &m_AtQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPrbsEngineQuerier);
    }

static AtQuerier ObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtPrbsEngineSharedQuerier(void)
    {
    static tAtPrbsEngineQuerier querier;
    static AtQuerier pQuerier = NULL;

    if (pQuerier == NULL)
        pQuerier = ObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

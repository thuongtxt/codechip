/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtEthPortQuerierInternal.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : ETH Port querier
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHPORTQUERIERINTERNAL_H_
#define _ATETHPORTQUERIERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEthPortQuerier
    {
    tAtChannelQuerier super;
    }tAtEthPortQuerier;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtQuerier AtEthPortQuerierObjectInit(AtQuerier self);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHPORTQUERIERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtEthPortQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : ETH Port querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthPort.h"
#include "AtEthPortQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelQuerierMethods m_AtChannelQuerierOverride;

/* Save super implementation */
static const tAtChannelQuerierMethods *m_AtChannelQuerierMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *CounterNames(AtChannelQuerier self, uint32 *numCounters)
    {
    static tAtTypeNamePair counters[] = {{cAtEthPortCounterTxPackets, "TxPackets"},
                                         {cAtEthPortCounterTxBytes, "TxBytes"},
                                         {cAtEthPortCounterTxOamPackets, "TxOamPackets"},
                                         {cAtEthPortCounterTxPeriodOamPackets, "TxPeriodOamPackets"},
                                         {cAtEthPortCounterTxPwPackets, "TxPwPackets"},
                                         {cAtEthPortCounterTxPacketsLen0_64, "TxPacketsLen0_64"},
                                         {cAtEthPortCounterTxPacketsLen65_127, "TxPacketsLen65_127"},
                                         {cAtEthPortCounterTxPacketsLen128_255, "TxPacketsLen128_255"},
                                         {cAtEthPortCounterTxPacketsLen256_511, "TxPacketsLen256_511"},
                                         {cAtEthPortCounterTxPacketsLen512_1024, "TxPacketsLen512_1024"},
                                         {cAtEthPortCounterTxPacketsLen1025_1528, "TxPacketsLen1025_1528"},
                                         {cAtEthPortCounterTxPacketsLen1529_2047, "TxPacketsLen1529_2047"},
                                         {cAtEthPortCounterTxPacketsJumbo, "TxPacketsJumbo"},
                                         {cAtEthPortCounterTxTopPackets, "TxTopPackets"},
                                         {cAtEthPortCounterTxPauFrames, "TxPauFrames"},
                                         {cAtEthPortCounterTxBrdCastPackets, "TxBrdCastPackets"},
                                         {cAtEthPortCounterTxMultCastPackets, "TxMultCastPackets"},
                                         {cAtEthPortCounterTxUniCastPackets, "TxUniCastPackets"},
                                         {cAtEthPortCounterRxPackets, "RxPackets"},
                                         {cAtEthPortCounterRxBytes, "RxBytes"},
                                         {cAtEthPortCounterRxErrEthHdrPackets, "RxErrEthHdrPackets"},
                                         {cAtEthPortCounterRxErrBusPackets, "RxErrBusPackets"},
                                         {cAtEthPortCounterRxErrFcsPackets, "RxErrFcsPackets"},
                                         {cAtEthPortCounterRxOversizePackets, "RxOversizePackets"},
                                         {cAtEthPortCounterRxUndersizePackets, "RxUndersizePackets"},
                                         {cAtEthPortCounterRxPacketsLen0_64, "RxPacketsLen0_64"},
                                         {cAtEthPortCounterRxPacketsLen65_127, "RxPacketsLen65_127"},
                                         {cAtEthPortCounterRxPacketsLen128_255, "RxPacketsLen128_255"},
                                         {cAtEthPortCounterRxPacketsLen256_511, "RxPacketsLen256_511"},
                                         {cAtEthPortCounterRxPacketsLen512_1024, "RxPacketsLen512_1024"},
                                         {cAtEthPortCounterRxPacketsLen1025_1528, "RxPacketsLen1025_1528"},
                                         {cAtEthPortCounterRxPacketsLen1529_2047, "RxPacketsLen1529_2047"},
                                         {cAtEthPortCounterRxPacketsJumbo, "RxPacketsJumbo"},
                                         {cAtEthPortCounterRxPwUnsupportedPackets, "RxPwUnsupportedPackets"},
                                         {cAtEthPortCounterRxErrPwLabelPackets, "RxErrPwLabelPackets"},
                                         {cAtEthPortCounterRxDiscardedPackets, "RxDiscardedPackets"},
                                         {cAtEthPortCounterRxPausePackets, "RxPausePackets"},
                                         {cAtEthPortCounterRxErrPausePackets, "RxErrPausePackets"},
                                         {cAtEthPortCounterRxUniCastPackets, "RxUniCastPackets"},
                                         {cAtEthPortCounterRxBrdCastPackets, "RxBrdCastPackets"},
                                         {cAtEthPortCounterRxMultCastPackets, "RxMultCastPackets"},
                                         {cAtEthPortCounterRxArpPackets, "RxArpPackets"},
                                         {cAtEthPortCounterRxOamPackets, "RxOamPackets"},
                                         {cAtEthPortCounterRxEfmOamPackets, "RxEfmOamPackets"},
                                         {cAtEthPortCounterRxErrEfmOamPackets, "RxErrEfmOamPackets"},
                                         {cAtEthPortCounterRxEthOamType1Packets, "RxEthOamType1Packets"},
                                         {cAtEthPortCounterRxEthOamType2Packets, "RxEthOamType2Packets"},
                                         {cAtEthPortCounterRxIpv4Packets, "RxIpv4Packets"},
                                         {cAtEthPortCounterRxErrIpv4Packets, "RxErrIpv4Packets"},
                                         {cAtEthPortCounterRxIcmpIpv4Packets, "RxIcmpIpv4Packets"},
                                         {cAtEthPortCounterRxIpv6Packets, "RxIpv6Packets"},
                                         {cAtEthPortCounterRxErrIpv6Packets, "RxErrIpv6Packets"},
                                         {cAtEthPortCounterRxIcmpIpv6Packets, "RxIcmpIpv6Packets"},
                                         {cAtEthPortCounterRxMefPackets, "RxMefPackets"},
                                         {cAtEthPortCounterRxErrMefPackets, "RxErrMefPackets"},
                                         {cAtEthPortCounterRxMplsPackets, "RxMplsPackets"},
                                         {cAtEthPortCounterRxErrMplsPackets, "RxErrMplsPackets"},
                                         {cAtEthPortCounterRxMplsErrOuterLblPackets, "RxMplsErrOuterLblPackets"},
                                         {cAtEthPortCounterRxMplsDataPackets, "RxMplsDataPackets"},
                                         {cAtEthPortCounterRxLdpIpv4Packets, "RxLdpIpv4Packets"},
                                         {cAtEthPortCounterRxLdpIpv6Packets, "RxLdpIpv6Packets"},
                                         {cAtEthPortCounterRxMplsIpv4Packets, "RxMplsIpv4Packets"},
                                         {cAtEthPortCounterRxMplsIpv6Packets, "RxMplsIpv6Packets"},
                                         {cAtEthPortCounterRxErrL2tpv3Packets, "RxErrL2tpv3Packets"},
                                         {cAtEthPortCounterRxL2tpv3Packets, "RxL2tpv3Packets"},
                                         {cAtEthPortCounterRxL2tpv3Ipv4Packets, "RxL2tpv3Ipv4Packets"},
                                         {cAtEthPortCounterRxL2tpv3Ipv6Packets, "RxL2tpv3Ipv6Packets"},
                                         {cAtEthPortCounterRxUdpPackets, "RxUdpPackets"},
                                         {cAtEthPortCounterRxErrUdpPackets, "RxErrUdpPackets"},
                                         {cAtEthPortCounterRxUdpIpv4Packets, "RxUdpIpv4Packets"},
                                         {cAtEthPortCounterRxUdpIpv6Packets, "RxUdpIpv6Packets"},
                                         {cAtEthPortCounterRxErrPsnPackets, "RxErrPsnPackets"},
                                         {cAtEthPortCounterRxPacketsSendToCpu, "RxPacketsSendToCpu"},
                                         {cAtEthPortCounterRxPacketsSendToPw, "RxPacketsSendToPw"},
                                         {cAtEthPortCounterRxTopPackets, "RxTopPackets"},
                                         {cAtEthPortCounterRxPacketDaMis, "RxPacketDaMis"},
                                         {cAtEthPortCounterRxPauFrames, "RxPauFrames"},
                                         {cAtEthPortCounterRxPhysicalError, "RxPhysicalError"}};
    AtUnused(self);
    if (numCounters)
        *numCounters = mCount(counters);
    return counters;
    }

static eAtCounterKind CounterKind(AtChannelQuerier self, AtChannel channel, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(channel);

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets               : return cAtCounterKindGood;
        case cAtEthPortCounterTxBytes                 : return cAtCounterKindGood;
        case cAtEthPortCounterTxOamPackets            : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPeriodOamPackets      : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPwPackets             : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsLen0_64        : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsLen65_127      : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsLen128_255     : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsLen256_511     : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsLen512_1024    : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsLen1025_1528   : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsLen1529_2047   : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPacketsJumbo          : return cAtCounterKindInfo;
        case cAtEthPortCounterTxTopPackets            : return cAtCounterKindInfo;
        case cAtEthPortCounterTxPauFrames             : return cAtCounterKindInfo;
        case cAtEthPortCounterTxBrdCastPackets        : return cAtCounterKindInfo;
        case cAtEthPortCounterTxMultCastPackets       : return cAtCounterKindInfo;
        case cAtEthPortCounterTxUniCastPackets        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPackets               : return cAtCounterKindGood;
        case cAtEthPortCounterRxBytes                 : return cAtCounterKindGood;
        case cAtEthPortCounterRxErrEthHdrPackets      : return cAtCounterKindError;
        case cAtEthPortCounterRxErrBusPackets         : return cAtCounterKindError;
        case cAtEthPortCounterRxErrFcsPackets         : return cAtCounterKindError;
        case cAtEthPortCounterRxOversizePackets       : return cAtCounterKindError;
        case cAtEthPortCounterRxUndersizePackets      : return cAtCounterKindError;
        case cAtEthPortCounterRxPacketsLen0_64        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsLen65_127      : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsLen128_255     : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsLen256_511     : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsLen512_1024    : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsLen1025_1528   : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsLen1529_2047   : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsJumbo          : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPwUnsupportedPackets  : return cAtCounterKindError;
        case cAtEthPortCounterRxErrPwLabelPackets     : return cAtCounterKindError;
        case cAtEthPortCounterRxDiscardedPackets      : return cAtCounterKindError;
        case cAtEthPortCounterRxPausePackets          : return cAtCounterKindError;
        case cAtEthPortCounterRxErrPausePackets       : return cAtCounterKindError;
        case cAtEthPortCounterRxUniCastPackets        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxBrdCastPackets        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxMultCastPackets       : return cAtCounterKindInfo;
        case cAtEthPortCounterRxArpPackets            : return cAtCounterKindInfo;
        case cAtEthPortCounterRxOamPackets            : return cAtCounterKindInfo;
        case cAtEthPortCounterRxEfmOamPackets         : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrEfmOamPackets      : return cAtCounterKindError;
        case cAtEthPortCounterRxEthOamType1Packets    : return cAtCounterKindInfo;
        case cAtEthPortCounterRxEthOamType2Packets    : return cAtCounterKindInfo;
        case cAtEthPortCounterRxIpv4Packets           : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrIpv4Packets        : return cAtCounterKindError;
        case cAtEthPortCounterRxIcmpIpv4Packets       : return cAtCounterKindInfo;
        case cAtEthPortCounterRxIpv6Packets           : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrIpv6Packets        : return cAtCounterKindError;
        case cAtEthPortCounterRxIcmpIpv6Packets       : return cAtCounterKindInfo;
        case cAtEthPortCounterRxMefPackets            : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrMefPackets         : return cAtCounterKindError;
        case cAtEthPortCounterRxMplsPackets           : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrMplsPackets        : return cAtCounterKindError;
        case cAtEthPortCounterRxMplsErrOuterLblPackets: return cAtCounterKindError;
        case cAtEthPortCounterRxMplsDataPackets       : return cAtCounterKindInfo;
        case cAtEthPortCounterRxLdpIpv4Packets        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxLdpIpv6Packets        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxMplsIpv4Packets       : return cAtCounterKindInfo;
        case cAtEthPortCounterRxMplsIpv6Packets       : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrL2tpv3Packets      : return cAtCounterKindError;
        case cAtEthPortCounterRxL2tpv3Packets         : return cAtCounterKindInfo;
        case cAtEthPortCounterRxL2tpv3Ipv4Packets     : return cAtCounterKindInfo;
        case cAtEthPortCounterRxL2tpv3Ipv6Packets     : return cAtCounterKindInfo;
        case cAtEthPortCounterRxUdpPackets            : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrUdpPackets         : return cAtCounterKindError;
        case cAtEthPortCounterRxUdpIpv4Packets        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxUdpIpv6Packets        : return cAtCounterKindInfo;
        case cAtEthPortCounterRxErrPsnPackets         : return cAtCounterKindError;
        case cAtEthPortCounterRxPacketsSendToCpu      : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketsSendToPw       : return cAtCounterKindInfo;
        case cAtEthPortCounterRxTopPackets            : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPacketDaMis           : return cAtCounterKindInfo;
        case cAtEthPortCounterRxPhysicalError         : return cAtCounterKindError;
        default                                       : return cAtCounterKindError;
        }
    }

static eBool ProblemsShouldQuery(AtChannelQuerier self, AtChannel channel)
    {
    AtUnused(self);
    return AtChannelIsEnabled(channel);
    }

static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    static tAtTypeNamePair alarmNames[] = {{cAtEthPortAlarmLinkDown, "link-down"}};
    AtUnused(self);
    if (numAlarms)
        *numAlarms = mCount(alarmNames);
    return alarmNames;
    }

static void OverrideAtChannelQuerier(AtQuerier self)
    {
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelQuerierMethods = mMethodsGet(querier);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelQuerierOverride, m_AtChannelQuerierMethods, sizeof(m_AtChannelQuerierOverride));

        mMethodOverride(m_AtChannelQuerierOverride, CounterNames);
        mMethodOverride(m_AtChannelQuerierOverride, CounterKind);
        mMethodOverride(m_AtChannelQuerierOverride, ProblemsShouldQuery);
        mMethodOverride(m_AtChannelQuerierOverride, AlarmNames);
        }

    mMethodsSet(querier, &m_AtChannelQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtChannelQuerier(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEthPortQuerier);
    }

AtQuerier AtEthPortQuerierObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtQuerier AtEthPortSharedQuerier(void)
    {
    static tAtEthPortQuerier querier;
    static AtQuerier pQuerier = NULL;
    if (pQuerier == NULL)
         pQuerier = AtEthPortQuerierObjectInit((AtQuerier)&querier);
    return pQuerier;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : Querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtQuerierMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtUnused(self);
    AtUnused(object);
    AtUnused(debugger);
    }

static void MethodsInit(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ProblemsQuery);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtQuerier);
    }

AtQuerier AtQuerierObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtQuerierProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    if (self)
        mMethodsGet(self)->ProblemsQuery(self, object, debugger);
    }

char *AtQuerierUtilAlarmToString(uint32 alarm, tAtTypeNamePair *alarmNames, uint32 numAlarms, char *buffer, uint32 bufferSize)
    {
    uint32 alarm_i;
    uint32 remainingBuffer = bufferSize;

    if (alarm == 0)
        {
        AtSnprintf(buffer, bufferSize, "none");
        return buffer;
        }

    if (numAlarms == 0)
        return NULL;

    for (alarm_i = 0; alarm_i < numAlarms; alarm_i++)
        {
        tAtTypeNamePair *alarmName = &(alarmNames[alarm_i]);
        if (alarm & alarmName->type)
            {
            uint32 printed = AtSnprintf(buffer, remainingBuffer, "%s|", alarmName->name);
            remainingBuffer = remainingBuffer - printed;
            }
        }

    /* Just to remove the last '|' and return */
    buffer[AtStrlen(buffer) - 1] = '\0';

    return buffer;
    }

void AtQuerierUtilProblemsQueryCounterPut(AtObject object, AtDebugger debugger, const char *name, uint32 counterValue, eAtCounterKind kind)
    {
    uint32 bufferSize;
    char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);

    switch (kind)
        {
        case cAtCounterKindNone:
            break;

        case cAtCounterKindGood :
            if (counterValue == 0)
                {
                AtDebugEntry entry = AtQuerierUtilProblemEntryCreate(object, debugger);
                AtSnprintf(buffer, bufferSize, "%s: %u", name, counterValue);
                AtDebuggerEntryValueSet(entry, buffer);
                AtDebuggerEntryAdd(debugger, entry);
                }
            break;

        case cAtCounterKindError:
            if (counterValue > 0)
                {
                AtDebugEntry entry = AtQuerierUtilProblemEntryCreate(object, debugger);
                AtSnprintf(buffer, bufferSize, "%s: %u", name, counterValue);
                AtDebuggerEntryValueSet(entry, buffer);
                AtDebuggerEntryAdd(debugger, entry);
                }
            break;

        case cAtCounterKindInfo:
            if (counterValue > 0)
                {
                AtDebugEntry entry = AtQuerierUtilProblemEntryCreate(object, debugger);
                AtDebuggerEntrySeveritySet(entry, cSevInfo);
                AtSnprintf(buffer, bufferSize, "%s: %u", name, counterValue);
                AtDebuggerEntryValueSet(entry, buffer);
                AtDebuggerEntryAdd(debugger, entry);
                }
            break;

        default:
            AtAssert(0);
            break;
        }
    }

AtDebugEntry AtQuerierUtilProblemEntryCreate(AtObject object, AtDebugger debugger)
    {
    uint32 bufferSize;
    char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
    AtDebugEntry entry = AtDebugEntryNew(cSevCritical, NULL, NULL);
    AtSnprintf(buffer, bufferSize, "%s", AtObjectToString(object));
    AtDebuggerEntryNameSet(entry, buffer);
    return entry;
    }

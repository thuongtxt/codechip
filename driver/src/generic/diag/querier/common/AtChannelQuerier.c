/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Diagnostic
 *
 * File        : AtChannelQuerier.c
 *
 * Created Date: Jun 17, 2017
 *
 * Description : ETH querier
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "AtChannelQuerierInternal.h"
#include "../../../prbs/AtPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtChannelQuerier)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtChannelQuerierMethods m_methods;

/* Override */
static tAtQuerierMethods m_AtQuerierOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtTypeNamePair *AlarmNames(AtChannelQuerier self, uint32 *numAlarms)
    {
    AtUnused(self);
    if (numAlarms)
        *numAlarms = 0;
    return NULL;
    }

static char *AlarmToString(AtChannelQuerier self, uint32 alarm, char *buffer, uint32 bufferSize)
    {
    uint32 numAlarms;
    tAtTypeNamePair *alarmNames = mMethodsGet(self)->AlarmNames(self, &numAlarms);
    return AtQuerierUtilAlarmToString(alarm, alarmNames, numAlarms, buffer, bufferSize);
    }

static void ProblemsQueryDefectWithHandler(AtChannelQuerier self, AtChannel channel, AtDebugger debugger,
                                           const char *defectKind,
                                           uint32 (*DefectGetFunc)(AtChannel))
    {
    uint32 alarms;
    AtDebugEntry entry;
    uint32 bufferSize;
    char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
    char *alarmString;
    uint32 numPrintedChars;

    alarms = DefectGetFunc(channel);
    if (alarms == 0)
        return;

    entry = AtQuerierUtilProblemEntryCreate((AtObject)channel, debugger);

    /* Put channel ID and define kinds */
    numPrintedChars = AtSnprintf(buffer, bufferSize, "%s: ", defectKind);

    /* Move buffer up for printing alarms */
    alarmString = AlarmToString(self, alarms, buffer + numPrintedChars, bufferSize - numPrintedChars);
    AtAssert(alarmString);

    /* Now, add it */
    AtDebuggerEntryValueSet(entry, buffer);
    AtDebuggerEntryAdd(debugger, entry);
    }

static tAtTypeNamePair *CounterNames(AtChannelQuerier self, uint32 *numCounters)
    {
    AtUnused(self);
    if (numCounters)
        *numCounters = 0;
    return NULL;
    }

static void ProblemsQueryCounters(AtChannelQuerier self, AtChannel channel, AtDebugger debugger)
    {
    uint32 numCounters, counter_i;
    tAtTypeNamePair *counterNames = mMethodsGet(self)->CounterNames(self, &numCounters);

    for (counter_i = 0; counter_i < numCounters; counter_i++)
        {
        tAtTypeNamePair *counterName = &(counterNames[counter_i]);
        eAtCounterKind kind = mMethodsGet(self)->CounterKind(self, channel, counterName->type);

        if (AtChannelCounterIsSupported(channel, (uint16)counterName->type))
            {
            uint32 counterValue = AtChannelCounterClear(channel, (uint16)counterName->type);
            AtQuerierUtilProblemsQueryCounterPut((AtObject)channel, debugger, counterName->name, counterValue, kind);
            }
        }
    }

static void ProblemsQueryPrbs(AtQuerier self, AtChannel channel, AtDebugger debugger)
    {
    AtPrbsEngine engine = AtChannelPrbsEngineGet(channel);
    AtQuerier querier = AtPrbsEngineQuerierGet(engine);
    AtUnused(self);
    if (querier)
        AtQuerierProblemsQuery(querier, (AtObject)engine, debugger);
    }

static eBool ProblemsShouldQuery(AtChannelQuerier self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtTrue;
    }

static void ProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger)
    {
    AtChannel channel = (AtChannel)object;
    AtChannelQuerier querier = (AtChannelQuerier)self;

    if (!mMethodsGet(mThis(self))->ProblemsShouldQuery(mThis(self), channel))
        return;

    ProblemsQueryDefectWithHandler(querier, channel, debugger, "history", AtChannelDefectInterruptClear);
    ProblemsQueryDefectWithHandler(querier, channel, debugger, "alarm", AtChannelDefectGet);
    mMethodsGet(mThis(self))->ProblemsQueryCounters(mThis(self), channel, debugger);
    ProblemsQueryPrbs(self, channel, debugger);
    }

static eAtCounterKind CounterKind(AtChannelQuerier self, AtChannel channel, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(channel);
    AtUnused(counterType);
    return cAtCounterKindInfo;
    }

static void OverrideAtQuerier(AtQuerier self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtQuerierOverride, mMethodsGet(self), sizeof(m_AtQuerierOverride));

        mMethodOverride(m_AtQuerierOverride, ProblemsQuery);
        }

    mMethodsSet(self, &m_AtQuerierOverride);
    }

static void Override(AtQuerier self)
    {
    OverrideAtQuerier(self);
    }

static void MethodsInit(AtChannelQuerier self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ProblemsQueryCounters);
        mMethodOverride(m_methods, ProblemsShouldQuery);
        mMethodOverride(m_methods, CounterKind);
        mMethodOverride(m_methods, AlarmNames);
        mMethodOverride(m_methods, CounterNames);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtChannelQuerier);
    }

AtQuerier AtChannelQuerierObjectInit(AtQuerier self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtQuerierObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtQueriers.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : All of queriers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATQUERIERS_H_
#define _ATQUERIERS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPhysicalClasses.h"
#include "AtQuerier.h"
#include "AtModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Associated querier */
AtQuerier AtChannelQuerierGet(AtChannel self);
AtQuerier AtModuleQuerierGet(AtModule self);
AtQuerier AtSerdesManagerQuerierGet(AtSerdesManager self);
AtQuerier AtSerdesControllerQuerierGet(AtSerdesController self);
AtQuerier AtPrbsEngineQuerierGet(AtPrbsEngine self);

/* Device concretes */
AtQuerier AtDeviceSharedQuerier(void);

/* ETH */
AtQuerier AtModuleEthSharedQuerier(void);
AtQuerier AtEthPortSharedQuerier(void);

/* PDH */
AtQuerier AtModulePdhSharedQuerier(void);
AtQuerier AtPdhChannelSharedQuerier(void);
AtQuerier AtPdhDe1SharedQuerier(void);
AtQuerier AtPdhDe2SharedQuerier(void);
AtQuerier AtPdhDe3SharedQuerier(void);

/* PW */
AtQuerier AtModulePwSharedQuerier(void);
AtQuerier AtPwSharedQuerier(void);

/* SDH */
AtQuerier AtModuleSdhSharedQuerier(void);
AtQuerier AtSdhChannelSharedQuerier(void);
AtQuerier AtSdhLineSharedQuerier(void);
AtQuerier AtSdhPathSharedQuerier(void);

/* SERDES manager */
AtQuerier AtSerdesManagerSharedQuerier(void);
AtQuerier AtSerdesControllerSharedQuerier(void);

/* PRBS */
AtQuerier AtPrbsEngineSharedQuerier(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATQUERIERS_H_ */


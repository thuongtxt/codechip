/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtQuerierInternal.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : Querier
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATQUERIERINTERNAL_H_
#define _ATQUERIERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtQuerier.h"
#include "AtQueriers.h"
#include "../../../man/AtDriverInternal.h" /* For OSAL macros */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtCounterKind
    {
    cAtCounterKindNone,  /* Do not need to check this */
    cAtCounterKindGood,  /* Good counter and must be > 0 */
    cAtCounterKindError, /* Error counter and must be == 0 */
    cAtCounterKindInfo   /* Info counter, any value */
    }eAtCounterKind;

typedef struct tAtTypeNamePair
    {
    uint32 type;
    const char *name;
    }tAtTypeNamePair;

typedef struct tAtQuerierMethods
    {
    void (*ProblemsQuery)(AtQuerier self, AtObject object, AtDebugger debugger);
    }tAtQuerierMethods;

typedef struct tAtQuerier
    {
    tAtObject super;
    const tAtQuerierMethods *methods;

    /* Private data */
    AtObject object;
    }tAtQuerier;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtQuerier AtQuerierObjectInit(AtQuerier self);

char *AtQuerierUtilAlarmToString(uint32 alarm, tAtTypeNamePair *alarmNames, uint32 numAlarms, char *buffer, uint32 bufferSize);
AtDebugEntry AtQuerierUtilProblemEntryCreate(AtObject object, AtDebugger debugger);
void AtQuerierUtilProblemsQueryCounterPut(AtObject object, AtDebugger debugger, const char *name, uint32 counterValue, eAtCounterKind kind);

#ifdef __cplusplus
}
#endif
#endif /* _ATQUERIERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtChannelQuerierInternal.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : Channel querier
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELQUERIERINTERNAL_H_
#define _ATCHANNELQUERIERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtQuerierInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelQuerier * AtChannelQuerier;

typedef struct tAtChannelQuerierMethods
    {
    eBool (*ProblemsShouldQuery)(AtChannelQuerier self, AtChannel channel);
    tAtTypeNamePair *(*AlarmNames)(AtChannelQuerier self, uint32 *numAlarms);
    tAtTypeNamePair *(*CounterNames)(AtChannelQuerier self, uint32 *numCounters);
    eAtCounterKind (*CounterKind)(AtChannelQuerier self, AtChannel channel, uint32 counterType);
    void (*ProblemsQueryCounters)(AtChannelQuerier self, AtChannel channel, AtDebugger debugger);
    }tAtChannelQuerierMethods;

typedef struct tAtChannelQuerier
    {
    tAtQuerier super;
    const tAtChannelQuerierMethods *methods;
    }tAtChannelQuerier;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtQuerier AtChannelQuerierObjectInit(AtQuerier self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELQUERIERINTERNAL_H_ */


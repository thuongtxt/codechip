/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : AtDiagErrorGenerator Module Name
 *
 * File        : AtDiagErrorGenerator.c
 *
 * Created Date: Mar 21, 2016
 *
 * Description : Error generator for physical interfaces such as DS1/E1, DS3/E3,
 *               ETH port or internal RAM
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtStd.h"
#include "AtOsal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtDriverInternal.h"
#include "../man/AtDriverLogMacro.h"
#include "AtErrorGeneratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtErrorGeneratorMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtErrorGenerator self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet SourceSet(AtErrorGenerator self, AtObject source)
    {
    if (self->source == source)
        return cAtOk;

    if (self->source != NULL && self->source != source)
        AtErrorGeneratorStop(self);

    self->source = source;
    return cAtOk;
    }

static AtObject SourceGet(AtErrorGenerator self)
    {
    return self->source;
    }

static eAtRet ErrorTypeSet(AtErrorGenerator self, uint32 mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtErrorModeNotSupport;
    }

static uint32 ErrorTypeGet(AtErrorGenerator self)
    {
    AtUnused(self);
    return 0;
    }

static eBool ModeIsSupported(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    AtUnused(self);
    if (mode == cAtErrorGeneratorModeOneshot)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet ModeSet(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtErrorModeNotSupport;
    }

static eAtErrorGeneratorMode ModeGet(AtErrorGenerator self)
    {
    AtUnused(self);
    return cAtErrorGeneratorModeInvalid;
    }

static eAtRet ErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    AtUnused(self);
    AtUnused(errorNum);
    return cAtErrorModeNotSupport;
    }

static uint32 ErrorNumGet(AtErrorGenerator self)
    {
    return self->errorNum;
    }

static eAtRet ErrorRateSet(AtErrorGenerator self, eAtBerRate errorRate)
    {
    AtUnused(self);
    AtUnused(errorRate);
    return cAtErrorModeNotSupport;
    }

static eAtBerRate ErrorRateGet(AtErrorGenerator self)
    {
    return self->errorRate;
    }

static eAtRet Start(AtErrorGenerator self)
    {
    AtUnused(self);

    return cAtErrorModeNotSupport;
    }

static eAtRet Stop(AtErrorGenerator self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool IsStarted(AtErrorGenerator self)
    {
    if (AtErrorGeneratorModeIsSupported(self, cAtErrorGeneratorModeContinuous))
        return self->continuousStarted;

    return cAtFalse;
    }

static uint32 ErrorRateToErrorNum(AtErrorGenerator self, eAtBerRate errorRate)
    {
    AtUnused(self);
    return AtErrorGeneratorErrorBitRateToErrorNum(errorRate);
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "error_generator";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtErrorGenerator object = (AtErrorGenerator)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(source);
    mEncodeUInt(errorNum);
    mEncodeUInt(errorRate);
    mEncodeUInt(continuousStarted);
    }

static void DbInit(AtErrorGenerator self)
    {
    AtErrorGeneratorSourceSet(self, NULL);
    AtErrorGeneratorErrorNumDbSet(self, 0);
    AtErrorGeneratorErrorRateDbSet(self, cAtBerRateUnknown);
    AtErrorGeneratorContinuousStartedDbSet(self, cAtFalse);
    }

static void OverrideAtObject(AtErrorGenerator self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtErrorGenerator self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtErrorGenerator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, SourceSet);
        mMethodOverride(m_methods, SourceGet);
        mMethodOverride(m_methods, ErrorTypeSet);
        mMethodOverride(m_methods, ErrorTypeGet);
        mMethodOverride(m_methods, ModeIsSupported);
        mMethodOverride(m_methods, ModeSet);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, ErrorNumSet);
        mMethodOverride(m_methods, ErrorRateSet);
        mMethodOverride(m_methods, ErrorNumGet);
        mMethodOverride(m_methods, ErrorRateGet);
        mMethodOverride(m_methods, ErrorRateToErrorNum);
        mMethodOverride(m_methods, Start);
        mMethodOverride(m_methods, Stop);
        mMethodOverride(m_methods, IsStarted);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtErrorGenerator);
    }

AtErrorGenerator AtErrorGeneratorObjectInit(AtErrorGenerator self, AtObject source)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    self->source = source;

    return self;
    }

eAtRet AtErrorGeneratorInit(AtErrorGenerator self)
    {
    mNoParamAttributeSet(Init);
    }

eAtRet AtErrorGeneratorSourceSet(AtErrorGenerator self, AtObject source)
    {
    mObjectSet(SourceSet, source);
    }

void AtErrorGeneratorRefSourceRelease(AtErrorGenerator self)
    {
    if (self)
        self->source = NULL;
    }

AtObject AtErrorGeneratorSourceGet(AtErrorGenerator self)
    {
    mAttributeGet(SourceGet, AtObject, NULL);
    }

eBool AtErrorGeneratorModeIsSupported(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    mOneParamAttributeGet(ModeIsSupported, mode, eBool, cAtFalse);
    }

eAtRet AtErrorGeneratorErrorNumDbSet(AtErrorGenerator self, uint32 errorNum)
    {
    if (self != NULL)
        {
        self->errorNum = errorNum;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eAtRet AtErrorGeneratorErrorRateDbSet(AtErrorGenerator self, eAtBerRate errorRate)
    {
    if (self != NULL)
        {
        self->errorRate = errorRate;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eAtRet AtErrorGeneratorContinuousStartedDbSet(AtErrorGenerator self, eBool isStared)
    {
    if (self != NULL)
        {
        self->continuousStarted = isStared;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eBool AtErrorGeneratorContinuousStartedDbGet(AtErrorGenerator self)
    {
    if (self != NULL)
        {
        return self->continuousStarted ;
        }

    return cAtFalse;
    }

uint32 AtErrorGeneratorErrorBitRateToErrorNum(eAtBerRate errorRate)
    {
    switch ((uint32) errorRate)
        {
        case cAtBerRate1E3: return 1000;
        case cAtBerRate1E4: return 10000;
        case cAtBerRate1E5: return 100000;
        case cAtBerRate1E6: return 1000000;
        case cAtBerRate1E7: return 10000000;
        case cAtBerRate1E8: return 100000000;
        case cAtBerRate1E9: return 1000000000;

        case cAtBerRate1E10:
        case cAtBerRateUnknown:
        default: return 0;
        }
    }

eBool AtErrorGeneratorErrorRateIsSupported(AtErrorGenerator self, eAtBerRate errorRate)
    {
    AtUnused(self);
    switch ((uint32) errorRate)
        {
        case cAtBerRate1E3: return cAtTrue;
        case cAtBerRate1E4: return cAtTrue;
        case cAtBerRate1E5: return cAtTrue;
        case cAtBerRate1E6: return cAtTrue;
        case cAtBerRate1E7: return cAtTrue;
        case cAtBerRate1E8: return cAtTrue;
        case cAtBerRate1E9: return cAtTrue;
        default: return cAtFalse;
        }
    }

uint32 AtErrorGeneratorErrorRateToErrorNum(AtErrorGenerator self, eAtBerRate errorRate)
    {
    if (!AtErrorGeneratorErrorRateIsSupported(self, errorRate))
        return cInvalidUint32;
    mOneParamAttributeGet(ErrorRateToErrorNum, errorRate, uint32, 0);
    }

void AtErrorGeneratorDbInit(AtErrorGenerator self)
    {
    if (self)
        DbInit(self);
    }

/**
 * @addtogroup AtErrorGenerator
 * @{
 */

/**
 * Set error type
 *
 * @param self This error generator
 * @param errorType Error type to generate. Each class that supports error
 *        generator will have its own error definition.
 *
 * @return AT return code
 */
eAtRet AtErrorGeneratorErrorTypeSet(AtErrorGenerator self, uint32 errorType)
    {
    mNumericalAttributeSet(ErrorTypeSet, errorType);
    }

/**
 * Get error type
 *
 * @param self This error generator
 *
 * @return Error type
 */
uint32 AtErrorGeneratorErrorTypeGet(AtErrorGenerator self)
    {
    mAttributeGet(ErrorTypeGet, uint32, 0);
    }

/**
 * Set error generating mode, one-shot or contiguous, ...
 *
 * @param self This error generator
 * @param mode @ref eAtErrorGeneratorMode "Error generating mode"
 *
 * @return AT return code
 */
eAtRet AtErrorGeneratorModeSet(AtErrorGenerator self, eAtErrorGeneratorMode mode)
    {
    mNumericalAttributeSet(ModeSet, mode);
    }

/**
 * Get error generating mode
 *
 * @param self This error generator
 *
 * @return @ref eAtErrorGeneratorMode "Error generating mode"
 */
eAtErrorGeneratorMode AtErrorGeneratorModeGet(AtErrorGenerator self)
    {
    mAttributeGet(ModeGet, uint32, 0);
    }

/**
 * Set number of errors to generate in case of one-shot
 *
 * @param self This error generator
 * @param errorNum Number of error to generate in the one-shot mode
 *
 * @return AT return code
 */
eAtRet AtErrorGeneratorErrorNumSet(AtErrorGenerator self, uint32 errorNum)
    {
    mNumericalAttributeSet(ErrorNumSet, errorNum);
    }

/**
 * Get number of errors to be generated in one-shot
 *
 * @param self This error generator
 *
 * @return Number of errors to be generated in one-shot
 */
uint32 AtErrorGeneratorErrorNumGet(AtErrorGenerator self)
    {
    mAttributeGet(ErrorNumGet, uint32, 0);
    }

/**
 * Set error rate to generate for contiguous mode.
 *
 * @param self This error generator
 * @param errorRate @ref eAtBerRate "BER rate" to generate in the continuous mode
 *
 * @return AT return code
 */
eAtRet AtErrorGeneratorErrorRateSet(AtErrorGenerator self, eAtBerRate errorRate)
    {
    mNumericalAttributeSet(ErrorRateSet, errorRate);
    }

/**
 * Get error rate that is being generated in contiguous mode.
 *
 * @param self This error generator
 *
 * @return @ref eAtBerRate "BER rate" that is being generated in contiguous mode.
 */
eAtBerRate AtErrorGeneratorErrorRateGet(AtErrorGenerator self)
    {
    mAttributeGet(ErrorRateGet, uint32, cAtBerRateUnknown);
    }

/**
 * Start generating errors
 *
 * @param self This error generator
 *
 * @return AT return code
 */
eAtRet AtErrorGeneratorStart(AtErrorGenerator self)
    {
    mNoParamAttributeSet(Start);
    }

/**
 * Stop generating errors
 *
 * @param self This error generator
 *
 * @return AT return code
 */
eAtRet AtErrorGeneratorStop(AtErrorGenerator self)
    {
    mNoParamAttributeSet(Stop);
    }

/**
 * Check if error generating is started or not.
 *
 * @param self This error generator
 *
 * @retval cAtTrue if the error generator is started
 * @retval cAtFalse if the error generator is stopped
 */
eBool AtErrorGeneratorIsStarted(AtErrorGenerator self)
    {
    mAttributeGet(IsStarted, eBool, cAtFalse);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : AtDiagErrorGenerator
 * 
 * File        : AtDiagErrorGeneratorInternal.h
 * 
 * Created Date: Mar 21, 2016
 *
 * Description : Error generator internal data and methods
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDIAGERRORGENERATORINTERNAL_H_
#define _ATDIAGERRORGENERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtErrorGenerator.h"
#include "AtObject.h"
#include "AtClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtErrorGeneratorMethods
    {
    eAtRet (*Init)(AtErrorGenerator self);
    eAtRet (*SourceSet)(AtErrorGenerator self, AtObject channel);
    AtObject (*SourceGet)(AtErrorGenerator self);
    eAtRet (*ErrorTypeSet)(AtErrorGenerator self, uint32 errorType);
    uint32 (*ErrorTypeGet)(AtErrorGenerator self);
    eBool (*ModeIsSupported)(AtErrorGenerator self, eAtErrorGeneratorMode mode);
    eAtRet (*ModeSet)(AtErrorGenerator self, eAtErrorGeneratorMode mode);
    eAtErrorGeneratorMode (*ModeGet)(AtErrorGenerator self);
    eAtRet (*ErrorNumSet)(AtErrorGenerator self, uint32 errorNum);
    uint32 (*ErrorNumGet)(AtErrorGenerator self);
    eAtRet (*ErrorRateSet)(AtErrorGenerator self, eAtBerRate errorRate);
    eAtBerRate (*ErrorRateGet)(AtErrorGenerator self);
    uint32 (*ErrorRateToErrorNum)(AtErrorGenerator self, eAtBerRate errorRate);
    eAtRet (*Start)(AtErrorGenerator self);
    eAtRet (*Stop)(AtErrorGenerator self);
    eBool (*IsStarted)(AtErrorGenerator self);
    }tAtErrorGeneratorMethods;

typedef struct tAtErrorGenerator
    {
    tAtObject super;
    const tAtErrorGeneratorMethods *methods;

    /* Private data */
    AtObject        source;
    uint32          errorNum;
    eAtBerRate      errorRate;
    eBool           continuousStarted;
    }tAtErrorGenerator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtErrorGenerator AtErrorGeneratorObjectInit(AtErrorGenerator self, AtObject source);
eAtRet AtErrorGeneratorErrorNumDbSet(AtErrorGenerator self, uint32 errorNum);
eAtRet AtErrorGeneratorErrorRateDbSet(AtErrorGenerator self, eAtBerRate errorRate);
eBool AtErrorGeneratorErrorRateIsSupported(AtErrorGenerator self, eAtBerRate errorRate);
uint32 AtErrorGeneratorErrorRateToErrorNum(AtErrorGenerator self, eAtBerRate errorRate);
uint32 AtErrorGeneratorErrorBitRateToErrorNum(eAtBerRate errorRate);
eAtRet AtErrorGeneratorContinuousStartedDbSet(AtErrorGenerator self, eBool isStared);
eBool AtErrorGeneratorContinuousStartedDbGet(AtErrorGenerator self);
void AtErrorGeneratorDbInit(AtErrorGenerator self);

eAtRet AtErrorGeneratorSourceSet(AtErrorGenerator self, AtObject source);
AtObject AtErrorGeneratorSourceGet(AtErrorGenerator self);
void AtErrorGeneratorRefSourceRelease(AtErrorGenerator self);
eAtRet AtErrorGeneratorInit(AtErrorGenerator self);
eBool AtErrorGeneratorModeIsSupported(AtErrorGenerator self, eAtErrorGeneratorMode mode);

#ifdef __cplusplus
}
#endif
#endif /* _ATDIAGERRORGENERATORINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Sdh
 *
 * File        : AtFailureProfiler.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : Sdh path failure profiler
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFailureProfilerInternal.h"
#include "AtSdhPath.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtSdhPathFailureProfiler *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSdhPathFailureProfiler
    {
    tAtFailureProfiler super;

    /* Private data */
    tAtProfilerDefect ais;
    tAtProfilerDefect lop;
    tAtProfilerDefect tim;
    tAtProfilerDefect uneq;
    tAtProfilerDefect plm;
    tAtProfilerDefect rdi;
    tAtProfilerDefect erdis;
    tAtProfilerDefect erdip;
    tAtProfilerDefect erdic;
    tAtProfilerDefect berSd;
    tAtProfilerDefect berSf;
    tAtProfilerDefect berTca;
    tAtProfilerDefect lom;
    tAtProfilerDefect rfi;
    tAtProfilerDefect rfiS;
    tAtProfilerDefect rfiP;
    tAtProfilerDefect rfiC;
    tAtProfilerDefect payloadUneq;
    tAtProfilerDefect clockStateChange;
    tAtProfilerDefect ttiChange;
    tAtProfilerDefect pslChange;
    }tAtSdhPathFailureProfiler;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtFailureProfilerMethods m_AtFailureProfilerOverride;

/* Save super implementation */
static const tAtFailureProfilerMethods *m_AtFailureProfilerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtProfilerDefect DefectInfo(AtFailureProfiler self, uint32 defectType)
    {
    switch (defectType)
        {
        case cAtSdhPathAlarmAis             : return &(mThis(self)->ais);
        case cAtSdhPathAlarmLop             : return &(mThis(self)->lop);
        case cAtSdhPathAlarmTim             : return &(mThis(self)->tim);
        case cAtSdhPathAlarmUneq            : return &(mThis(self)->uneq);
        case cAtSdhPathAlarmPlm             : return &(mThis(self)->plm);
        case cAtSdhPathAlarmRdi             : return &(mThis(self)->rdi);
        case cAtSdhPathAlarmErdiS           : return &(mThis(self)->erdis);
        case cAtSdhPathAlarmErdiP           : return &(mThis(self)->erdip);
        case cAtSdhPathAlarmErdiC           : return &(mThis(self)->erdic);
        case cAtSdhPathAlarmBerSd           : return &(mThis(self)->berSd);
        case cAtSdhPathAlarmBerSf           : return &(mThis(self)->berSf);
        case cAtSdhPathAlarmLom             : return &(mThis(self)->lom);
        case cAtSdhPathAlarmRfi             : return &(mThis(self)->rfi);
        case cAtSdhPathAlarmPayloadUneq     : return &(mThis(self)->payloadUneq);
        case cAtSdhPathAlarmBerTca          : return &(mThis(self)->berTca);
        case cAtSdhPathAlarmRfiS            : return &(mThis(self)->rfiS);
        case cAtSdhPathAlarmRfiC            : return &(mThis(self)->rfiC);
        case cAtSdhPathAlarmRfiP            : return &(mThis(self)->rfiP);
        case cAtSdhPathAlarmClockStateChange: return &(mThis(self)->clockStateChange);
        case cAtSdhPathAlarmTtiChange       : return &(mThis(self)->ttiChange);
        case cAtSdhPathAlarmPslChange       : return &(mThis(self)->pslChange);

        default:
            AtAssert(0);
            return NULL;
        }
    }

static const uint32 *DefectTypes(AtFailureProfiler self, uint32 *numDefects)
    {
    static const uint32 cDefectTypes[] =
        {
         cAtSdhPathAlarmAis,
         cAtSdhPathAlarmLop,
         cAtSdhPathAlarmTim,
         cAtSdhPathAlarmUneq,
         cAtSdhPathAlarmPlm,
         cAtSdhPathAlarmRdi,
         cAtSdhPathAlarmErdiS,
         cAtSdhPathAlarmErdiP,
         cAtSdhPathAlarmErdiC,
         cAtSdhPathAlarmBerSd,
         cAtSdhPathAlarmBerSf,
         cAtSdhPathAlarmLom,
         cAtSdhPathAlarmRfi,
         cAtSdhPathAlarmPayloadUneq,
         cAtSdhPathAlarmBerTca,
         cAtSdhPathAlarmRfiS,
         cAtSdhPathAlarmRfiC,
         cAtSdhPathAlarmRfiP,
         cAtSdhPathAlarmClockStateChange,
         cAtSdhPathAlarmTtiChange,
         cAtSdhPathAlarmPslChange,
         };

    AtUnused(self);
    if (numDefects)
        *numDefects = mCount(cDefectTypes);
    return cDefectTypes;
    }

static eAtRet Setup(AtFailureProfiler self)
    {
    AtProfilerDefectInit(&(mThis(self)->ais), cAtSdhPathAlarmAis);
    AtProfilerDefectInit(&(mThis(self)->lop), cAtSdhPathAlarmLop);
    AtProfilerDefectInit(&(mThis(self)->tim), cAtSdhPathAlarmTim);
    AtProfilerDefectInit(&(mThis(self)->uneq), cAtSdhPathAlarmUneq);
    AtProfilerDefectInit(&(mThis(self)->plm), cAtSdhPathAlarmPlm);
    AtProfilerDefectInit(&(mThis(self)->rdi), cAtSdhPathAlarmRdi);
    AtProfilerDefectInit(&(mThis(self)->erdis), cAtSdhPathAlarmErdiS);
    AtProfilerDefectInit(&(mThis(self)->erdip), cAtSdhPathAlarmErdiP);
    AtProfilerDefectInit(&(mThis(self)->erdic), cAtSdhPathAlarmErdiC);
    AtProfilerDefectInit(&(mThis(self)->berSd), cAtSdhPathAlarmBerSd);
    AtProfilerDefectInit(&(mThis(self)->berSf), cAtSdhPathAlarmBerSf);
    AtProfilerDefectInit(&(mThis(self)->berTca), cAtSdhPathAlarmBerTca);
    AtProfilerDefectInit(&(mThis(self)->lom), cAtSdhPathAlarmLom);
    AtProfilerDefectInit(&(mThis(self)->rfi), cAtSdhPathAlarmRfi);
    AtProfilerDefectInit(&(mThis(self)->rfiS), cAtSdhPathAlarmRfiS);
    AtProfilerDefectInit(&(mThis(self)->rfiP), cAtSdhPathAlarmRfiC);
    AtProfilerDefectInit(&(mThis(self)->rfiC), cAtSdhPathAlarmRfiP);
    AtProfilerDefectInit(&(mThis(self)->payloadUneq), cAtSdhPathAlarmPayloadUneq);
    AtProfilerDefectInit(&(mThis(self)->clockStateChange), cAtSdhPathAlarmClockStateChange);
    AtProfilerDefectInit(&(mThis(self)->ttiChange), cAtSdhPathAlarmTtiChange);
    AtProfilerDefectInit(&(mThis(self)->pslChange), cAtSdhPathAlarmPslChange);

    return cAtOk;
    }

static uint32 DefectForFailure(AtFailureProfiler self, uint32 failureType)
    {
    if (failureType & cAtSdhPathAlarmRfi)  return cAtSdhPathAlarmRdi;
    if (failureType & cAtSdhPathAlarmRfiS) return cAtSdhPathAlarmErdiS;
    if (failureType & cAtSdhPathAlarmRfiC) return cAtSdhPathAlarmErdiC;
    if (failureType & cAtSdhPathAlarmRfiP) return cAtSdhPathAlarmErdiP;

    return m_AtFailureProfilerMethods->DefectForFailure(self, failureType);
    }

static void OverrideFailureProfiler(AtFailureProfiler self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtFailureProfilerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtFailureProfilerOverride, mMethodsGet(self), sizeof(m_AtFailureProfilerOverride));

        mMethodOverride(m_AtFailureProfilerOverride, DefectInfo);
        mMethodOverride(m_AtFailureProfilerOverride, DefectTypes);
        mMethodOverride(m_AtFailureProfilerOverride, Setup);
        mMethodOverride(m_AtFailureProfilerOverride, DefectForFailure);
        }

    mMethodsSet(self, &m_AtFailureProfilerOverride);
    }

static void Override(AtFailureProfiler self)
    {
    OverrideFailureProfiler(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhPathFailureProfiler);
    }

static AtFailureProfiler ObjectInit(AtFailureProfiler self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtFailureProfilerObjectInit((AtFailureProfiler)self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFailureProfiler AtSdhPathFailureProfilerNew(AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFailureProfiler NewProfiler = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (NewProfiler == NULL)
        return NULL;

    return ObjectInit(NewProfiler, channel);
    }

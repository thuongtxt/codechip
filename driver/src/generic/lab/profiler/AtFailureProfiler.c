/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profile
 *
 * File        : AtFailureProfiler.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : Failure profiler
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "AtFailureProfilerInternal.h"
#include "AtModuleSur.h"
#include "AtSurEngine.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtFailureProfiler)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtFailureProfilerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtProfilerDefect DefectInfo(AtFailureProfiler self, uint32 defectType)
    {
    AtUnused(self);
    AtUnused(defectType);
    return NULL;
    }

static tAtFailureProfilerEvent *FailureEventCreate(uint32 type, uint32 status)
    {
    uint32 memorySize = sizeof(tAtFailureProfilerEvent);
    tAtFailureProfilerEvent* newEvent = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(newEvent, 0, memorySize);

    newEvent->status = status & type;
    newEvent->type = type;
    AtOsalCurTimeGet(&(newEvent->reportedTime));
    newEvent->profiledTimeMs = cInvalidUint32;

    return newEvent;
    }

static void TimeInvalidate(tAtOsalCurTime *time)
    {
    AtOsalMemInit(time, cInvalidUint8, sizeof(tAtOsalCurTime));
    }

static eBool IsValidTime(tAtOsalCurTime *time)
    {
    if ((time->sec  == cInvalidUint32) &&
        (time->usec == cInvalidUint32))
        return cAtFalse;
    return cAtTrue;
    }

static uint32 DefectForFailure(AtFailureProfiler self, uint32 failureType)
    {
    AtUnused(self);
    return failureType;
    }

static eAtRet FailureInject(AtFailureProfiler self, uint32 failures, uint32 currentStatus)
    {
    uint32 alarm_i;
    eAtRet ret = cAtOk;
    uint32 numDefects;
    const uint32 *defectTypes = mMethodsGet(self)->DefectTypes(self, &numDefects);

    for (alarm_i = 0; alarm_i < numDefects; alarm_i++)
        {
        tAtFailureProfilerEvent *newEvent;
        AtProfilerDefect defectInfo = NULL;
        uint32 failureType = defectTypes[alarm_i];
        uint32 defectType;

        if ((failures & failureType) == 0)
            continue;

        newEvent = FailureEventCreate(failureType, currentStatus);
        defectType = mMethodsGet(self)->DefectForFailure(self, failureType);
        defectInfo = AtFailureProfilerDefectInfo(self, defectType);
        AtAssert(defectInfo);
        AtOsalMemCpy(&(newEvent->defect), defectInfo, sizeof(tAtProfilerDefect));

        if (failures & currentStatus)
            {
            if (IsValidTime(&(defectInfo->raiseTime)))
                newEvent->profiledTimeMs = mTimeIntervalInMsGet(defectInfo->raiseTime, newEvent->reportedTime);
            }
        else
            {
            if (IsValidTime(&(defectInfo->clearTime)))
                newEvent->profiledTimeMs = mTimeIntervalInMsGet(defectInfo->clearTime, newEvent->reportedTime);
            }

        ret |= AtListObjectAdd(self->events, (AtObject)newEvent);
        }

    return ret;
    }

static eAtRet DefectInject(AtFailureProfiler self, uint32 changedAlarms, uint32 currentStatus)
    {
    uint8 alarm_i;
    uint32 numDefects;
    const uint32 *defectTypes = mMethodsGet(self)->DefectTypes(self, &numDefects);

    for (alarm_i = 0; alarm_i < numDefects; alarm_i++)
         {
         uint32 alarmType = defectTypes[alarm_i];
         AtProfilerDefect defect;
         eBool invalidated = cAtFalse;

         if ((changedAlarms & alarmType) == 0)
             continue;

         defect = AtFailureProfilerDefectInfo(self, alarmType);
         AtAssert(defect);

         /* Just because defect is invalidated at the first time */
         if (defect->currentStatus == cInvalidUint32)
             {
             defect->currentStatus = 0;
             invalidated = cAtTrue;
             }

         if (currentStatus & alarmType)
             {
             if (invalidated || ((defect->currentStatus & alarmType) == 0))
                 AtOsalCurTimeGet(&(defect->raiseTime));
             TimeInvalidate(&(defect->clearTime));
             }
         else
             {
             if (invalidated || (defect->currentStatus & alarmType))
                 AtOsalCurTimeGet(&(defect->clearTime));
             TimeInvalidate(&(defect->raiseTime));
             }

         defect->currentStatus = currentStatus & alarmType;
         }

     return cAtOk;
    }

static void Lock(AtFailureProfiler self)
    {
    AtOsalMutexLock(self->mutex);
    }

static void Unlock(AtFailureProfiler self)
    {
    AtOsalMutexUnLock(self->mutex);
    }

static void Delete(AtObject self)
    {
    AtFailureProfiler profiler = mThis(self);

    /* Delete all events */
    Lock(profiler);
    AtFailureProfilerFailuresClear(profiler);
    AtObjectDelete((AtObject)profiler->events);
    profiler->events = NULL;
    Unlock(profiler);

    /* Clean up OSAL resource */
    AtOsalMutexDestroy(profiler->mutex);

    m_AtObjectMethods->Delete(self);
    }

static eAtRet FailuresClear(AtFailureProfiler self)
    {
    AtFailureProfilerEvent event;

    if (self->events == NULL)
        return cAtOk;

    while ((event = (AtFailureProfilerEvent)AtListObjectRemoveAtIndex(self->events, 0)) != NULL)
        AtOsalMemFree(event);

    return cAtOk;
    }

static const uint32 *DefectTypes(AtFailureProfiler self, uint32 *numDefects)
    {
    AtUnused(self);
    AtUnused(numDefects);
    return NULL;
    }

static void FailureNotify(AtSurEngine surEngine,
                          void *listener,
                          uint32 failureType,
                          uint32 currentStatus)
    {
    AtFailureProfiler self = listener;
    AtUnused(surEngine);
    AtFailureProfilerFailureInject(self, failureType, currentStatus);
    }

static tAtSurEngineListener *FailureListener(void)
    {
    static tAtSurEngineListener listener;
    static tAtSurEngineListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.FailureNotify = FailureNotify;
    pListener = &listener;

    return pListener;
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    AtFailureProfiler profiler = AtChannelFailureProfilerGet(channel);
    AtFailureProfilerDefectInject(profiler, changedAlarms, currentStatus);
    }

static tAtChannelEventListener *DefectListener(void)
    {
    static tAtChannelEventListener listener;
    static tAtChannelEventListener *pListener = NULL;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.AlarmChangeState = AlarmChangeState;
    pListener = &listener;

    return pListener;
    }

static eAtRet Setup(AtFailureProfiler self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet Start(AtFailureProfiler self, eBool started)
    {
    AtChannel channel = AtFailureProfilerChannelGet(self);
    AtSurEngine surEngine = AtChannelSurEngineGet(channel);
    eAtRet ret = cAtOk;

    if (!started)
        self->injectionEnabled = cAtFalse;

    if (started)
        {
        mMethodsGet(self)->Setup(self);
        ret |= AtChannelEventListenerAddWithUserData(channel, DefectListener(), self);
        if (surEngine)
            ret |= AtSurEngineListenerAdd(surEngine, self, FailureListener());
        }
    else
        {
        ret |= AtChannelEventListenerRemove(channel, DefectListener());
        if (surEngine)
            ret |= AtSurEngineListenerRemove(surEngine, self, FailureListener());
        }

    if (started)
        self->injectionEnabled = cAtTrue;

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtFailureProfiler object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(mutex);
    AtCoderEncodeUInt(encoder, AtListLengthGet(object->events), "events");
    mEncodeObjectDescription(channel);
    mEncodeUInt(injectionEnabled);
    }

static void OverrideAtObject(AtFailureProfiler self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtFailureProfiler self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtFailureProfiler self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefectInfo);
        mMethodOverride(m_methods, FailureInject);
        mMethodOverride(m_methods, DefectInject);
        mMethodOverride(m_methods, DefectTypes);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, DefectForFailure);
        }

    mMethodsGet(self) = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFailureProfiler);
    }

AtFailureProfiler AtFailureProfilerObjectInit(AtFailureProfiler self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor to reuse all of its implementation */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->channel = channel;
    self->mutex = AtOsalMutexCreate();
    self->events = AtListCreate(0);

    return self;
    }

eAtRet AtFailureProfilerStart(AtFailureProfiler self)
    {
    if (self)
        return Start(self, cAtTrue);

    return cAtError;
    }

eAtRet AtFailureProfilerStop(AtFailureProfiler self)
	{
    if (self)
        return Start(self, cAtFalse);

    return cAtErrorObjectNotExist;
	}

AtChannel AtFailureProfilerChannelGet(AtFailureProfiler self)
    {
    return self ? self->channel : NULL;
    }

eAtRet AtFailureProfilerFailuresClear(AtFailureProfiler self)
	{
    if (self)
        return FailuresClear(self);
    return cAtErrorObjectNotExist;
	}

AtList AtFailureProfilerFailuresGet(AtFailureProfiler self)
    {
    return self ? self->events : NULL;
    }

AtProfilerDefect AtFailureProfilerDefectInfo(AtFailureProfiler self, uint32 defectType)
    {
    if (self)
        return mMethodsGet(self)->DefectInfo(self, defectType);

    return NULL;
    }

eAtRet AtFailureProfilerDefectInject(AtFailureProfiler self, uint32 defects, uint32 currentStatus)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorNullPointer;

    Lock(self);
    if (self->injectionEnabled)
        ret = mMethodsGet(self)->DefectInject(self, defects, currentStatus);
    Unlock(self);

    return ret;
    }

eAtRet AtFailureProfilerFailureInject(AtFailureProfiler self, uint32 failures, uint32 currentStatus)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorNullPointer;

    Lock(self);
    if (self->injectionEnabled)
        ret = mMethodsGet(self)->FailureInject(self, failures, currentStatus);
    Unlock(self);

    return ret;
    }

tAtOsalCurTime *AtFailureProfilerEventReportedTime(AtFailureProfilerEvent self)
    {
    if (self)
        return &(self->reportedTime);
    return NULL;
    }

uint32 AtFailureProfilerEventStatus(AtFailureProfilerEvent self)
    {
    if (self)
        return self->status;
    return cInvalidUint32;
    }

uint32 AtFailureProfilerEventProfiledTimeMs(AtFailureProfilerEvent self)
    {
    if (self)
        return self->profiledTimeMs;
    return cInvalidUint32;
    }

uint32 AtFailureProfilerEventType(AtFailureProfilerEvent self)
    {
    return self ? self->type : cInvalidUint32;
    }

void AtFailureProfilerLock(AtFailureProfiler self)
    {
    if (self)
        Lock(self);
    }

void AtFailureProfilerUnlock(AtFailureProfiler self)
    {
    if (self)
        Unlock(self);
    }

void AtProfilerDefectInit(AtProfilerDefect defectInfo, uint32 type)
    {
    AtOsalMemInit(defectInfo, cInvalidUint8, sizeof(tAtProfilerDefect));
    TimeInvalidate(&(defectInfo->clearTime));
    TimeInvalidate(&(defectInfo->raiseTime));
    defectInfo->type = type;
    }

AtProfilerDefect AtFailureProfilerEventDefect(AtFailureProfilerEvent self)
    {
    if (self)
        return &(self->defect);
    return NULL;
    }

uint32 AtProfilerDefectType(AtProfilerDefect self)
    {
    return self ? self->type : cInvalidUint32;
    }

tAtOsalCurTime *AtProfilerDefectClearTime(AtProfilerDefect self)
    {
    return self ? &(self->clearTime) : NULL;
    }

tAtOsalCurTime *AtProfilerDefectRaiseTime(AtProfilerDefect self)
    {
    return self ? &(self->raiseTime) : NULL;
    }

uint32 AtProfilerDefectCurrentStatus(AtProfilerDefect self)
    {
    return self ? self->currentStatus : cInvalidUint32;
    }

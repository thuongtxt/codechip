/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : AtFailureProfiler.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : PW failure profiler
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFailureProfilerInternal.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtPwFailureProfiler *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwFailureProfiler
    {
    tAtFailureProfiler super;

    /* Private data */
    tAtProfilerDefect lbit;
    tAtProfilerDefect rbit;
    tAtProfilerDefect mbit;
    tAtProfilerDefect nbit;
    tAtProfilerDefect pbit;
    tAtProfilerDefect lops;
    tAtProfilerDefect jitterBufferOverrun;
    tAtProfilerDefect jitterBufferUnderrun;
    tAtProfilerDefect missingPacket;
    tAtProfilerDefect excessivePacketLossRate;
    tAtProfilerDefect strayPacket;
    tAtProfilerDefect malformedPacket;
    tAtProfilerDefect remotePacketLoss;
    tAtProfilerDefect misConnection;
    }tAtPwFailureProfiler;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtFailureProfilerMethods m_AtFailureProfilerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtProfilerDefect DefectInfo(AtFailureProfiler self, uint32 defectType)
    {
    AtUnused(self);

    switch (defectType)
        {
        case cAtPwAlarmTypeLops                   : return &(mThis(self)->lops);
        case cAtPwAlarmTypeJitterBufferOverrun    : return &(mThis(self)->jitterBufferOverrun);
        case cAtPwAlarmTypeJitterBufferUnderrun   : return &(mThis(self)->jitterBufferUnderrun);
        case cAtPwAlarmTypeLBit                   : return &(mThis(self)->lbit);
        case cAtPwAlarmTypeRBit                   : return &(mThis(self)->rbit);
        case cAtPwAlarmTypeMBit                   : return &(mThis(self)->mbit);
        case cAtPwAlarmTypeNBit                   : return &(mThis(self)->nbit);
        case cAtPwAlarmTypePBit                   : return &(mThis(self)->pbit);
        case cAtPwAlarmTypeMissingPacket          : return &(mThis(self)->missingPacket);
        case cAtPwAlarmTypeExcessivePacketLossRate: return &(mThis(self)->excessivePacketLossRate);
        case cAtPwAlarmTypeStrayPacket            : return &(mThis(self)->strayPacket);
        case cAtPwAlarmTypeMalformedPacket        : return &(mThis(self)->malformedPacket);
        case cAtPwAlarmTypeRemotePacketLoss       : return &(mThis(self)->remotePacketLoss);
        case cAtpwAlarmTypeMisConnection          : return &(mThis(self)->misConnection);
        default:
            AtAssert(0);
            return NULL;
        }
    }

static const uint32 *DefectTypes(AtFailureProfiler self, uint32 *numDefects)
    {
    static const uint32 cAtPwAlarmTypeVal[] =
        {
        cAtPwAlarmTypeLBit,
        cAtPwAlarmTypeRBit,
        cAtPwAlarmTypeMBit,
        cAtPwAlarmTypeLops,
        cAtPwAlarmTypeJitterBufferOverrun,
        cAtPwAlarmTypeJitterBufferUnderrun,
        cAtPwAlarmTypeMissingPacket,
        cAtPwAlarmTypeExcessivePacketLossRate,
        cAtPwAlarmTypeStrayPacket,
        cAtPwAlarmTypeMalformedPacket,
        cAtpwAlarmTypeMisConnection
        };

    AtUnused(self);
    if (numDefects)
        *numDefects = mCount(cAtPwAlarmTypeVal);
    return cAtPwAlarmTypeVal;
    }

static eAtRet Setup(AtFailureProfiler self)
    {
    AtProfilerDefectInit(&(mThis(self)->lbit), cAtPwAlarmTypeLBit);
    AtProfilerDefectInit(&(mThis(self)->rbit), cAtPwAlarmTypeRBit);
    AtProfilerDefectInit(&(mThis(self)->mbit), cAtPwAlarmTypeMBit);
    AtProfilerDefectInit(&(mThis(self)->nbit), cAtPwAlarmTypeNBit);
    AtProfilerDefectInit(&(mThis(self)->pbit), cAtPwAlarmTypePBit);
    AtProfilerDefectInit(&(mThis(self)->lops), cAtPwAlarmTypeLops);
    AtProfilerDefectInit(&(mThis(self)->jitterBufferOverrun), cAtPwAlarmTypeJitterBufferOverrun);
    AtProfilerDefectInit(&(mThis(self)->jitterBufferUnderrun), cAtPwAlarmTypeJitterBufferUnderrun);
    AtProfilerDefectInit(&(mThis(self)->missingPacket), cAtPwAlarmTypeMissingPacket);
    AtProfilerDefectInit(&(mThis(self)->excessivePacketLossRate), cAtPwAlarmTypeExcessivePacketLossRate);
    AtProfilerDefectInit(&(mThis(self)->strayPacket), cAtPwAlarmTypeStrayPacket);
    AtProfilerDefectInit(&(mThis(self)->malformedPacket), cAtPwAlarmTypeMalformedPacket);
    AtProfilerDefectInit(&(mThis(self)->remotePacketLoss), cAtPwAlarmTypeRemotePacketLoss);
    AtProfilerDefectInit(&(mThis(self)->misConnection), cAtpwAlarmTypeMisConnection);

    return cAtOk;
    }

static void OverrideFailureProfiler(AtFailureProfiler self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtFailureProfilerOverride, mMethodsGet(self), sizeof(m_AtFailureProfilerOverride));

        mMethodOverride(m_AtFailureProfilerOverride, DefectInfo);
        mMethodOverride(m_AtFailureProfilerOverride, DefectTypes);
        mMethodOverride(m_AtFailureProfilerOverride, Setup);
        }

    mMethodsSet(self, &m_AtFailureProfilerOverride);
    }

static void Override(AtFailureProfiler self)
    {
    OverrideFailureProfiler(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwFailureProfiler);
    }

static AtFailureProfiler ObjectInit(AtFailureProfiler self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtFailureProfilerObjectInit((AtFailureProfiler)self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFailureProfiler AtPwFailureProfilerNew(AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFailureProfiler newProfiler = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProfiler == NULL)
        return NULL;

    return ObjectInit(newProfiler,channel);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : AtFailureProfiler.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : PDH DE3 failure profiler
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFailureProfilerInternal.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtPdhDe3FailureProfiler *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhDe3FailureProfiler
    {
    tAtFailureProfiler super;

    /* Private data */
    tAtProfilerDefect los;
    tAtProfilerDefect lof;
    tAtProfilerDefect ais;
    tAtProfilerDefect rai;
    tAtProfilerDefect sfBer;
    tAtProfilerDefect sdBer;
    tAtProfilerDefect berTca;
    tAtProfilerDefect idle;
    tAtProfilerDefect aicChange;
    tAtProfilerDefect feacChange;
    tAtProfilerDefect tim;
    tAtProfilerDefect pldTypeChange;
    tAtProfilerDefect tmChange;
    tAtProfilerDefect ssmChange;
    tAtProfilerDefect mdlChange;
    tAtProfilerDefect clockStateChange;
    }tAtPdhDe3FailureProfiler;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtFailureProfilerMethods m_AtFailureProfilerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtProfilerDefect DefectInfo(AtFailureProfiler self, uint32 defectType)
    {
    AtUnused(self);

    switch (defectType)
        {
        case cAtPdhDe3AlarmLos             : return &(mThis(self)->los);
        case cAtPdhDe3AlarmLof             : return &(mThis(self)->lof);
        case cAtPdhDe3AlarmAis             : return &(mThis(self)->ais);
        case cAtPdhDe3AlarmRai             : return &(mThis(self)->rai);
        case cAtPdhDe3AlarmSfBer           : return &(mThis(self)->sfBer);
        case cAtPdhDe3AlarmSdBer           : return &(mThis(self)->sdBer);
        case cAtPdhDs3AlarmIdle            : return &(mThis(self)->idle);
        case cAtPdhDs3AlarmAicChange       : return &(mThis(self)->aicChange);
        case cAtPdhDs3AlarmFeacChange      : return &(mThis(self)->feacChange);
        case cAtPdhE3AlarmTim              : return &(mThis(self)->tim);
        case cAtPdhE3AlarmPldTypeChange    : return &(mThis(self)->pldTypeChange);
        case cAtPdhE3AlarmTmChange         : return &(mThis(self)->tmChange);
        case cAtPdhDe3AlarmSsmChange       : return &(mThis(self)->ssmChange);
        case cAtPdhDs3AlarmMdlChange       : return &(mThis(self)->mdlChange);
        case cAtPdhDe3AlarmBerTca          : return &(mThis(self)->berTca);
        case cAtPdhDe3AlarmClockStateChange: return &(mThis(self)->clockStateChange);

        default:
            AtAssert(0);
            return NULL;
        }
    }

static const uint32 *DefectTypes(AtFailureProfiler self, uint32 *numDefects)
    {
    static const uint32 cAtPdhDe3AlarmTypeVal[] =
        {
        cAtPdhDe3AlarmLos,
        cAtPdhDe3AlarmLof,
        cAtPdhDe3AlarmAis,
        cAtPdhDe3AlarmRai,
        cAtPdhDe3AlarmSfBer,
        cAtPdhDe3AlarmSdBer,
        cAtPdhDe3AlarmBerTca,
        cAtPdhDs3AlarmIdle,
        cAtPdhDs3AlarmAicChange,
        cAtPdhDs3AlarmFeacChange,
        cAtPdhE3AlarmTim,
        cAtPdhE3AlarmPldTypeChange,
        cAtPdhE3AlarmTmChange,
        cAtPdhDe3AlarmSsmChange,
        cAtPdhDs3AlarmMdlChange,
        cAtPdhDe3AlarmClockStateChange
        };

    AtUnused(self);
    if (numDefects)
        *numDefects = mCount(cAtPdhDe3AlarmTypeVal);
    return cAtPdhDe3AlarmTypeVal;
    }

static eAtRet Setup(AtFailureProfiler self)
    {
    AtProfilerDefectInit(&(mThis(self)->los), cAtPdhDe3AlarmLos);
    AtProfilerDefectInit(&(mThis(self)->lof), cAtPdhDe3AlarmLof);
    AtProfilerDefectInit(&(mThis(self)->ais), cAtPdhDe3AlarmAis);
    AtProfilerDefectInit(&(mThis(self)->rai), cAtPdhDe3AlarmRai);
    AtProfilerDefectInit(&(mThis(self)->sfBer), cAtPdhDe3AlarmSfBer);
    AtProfilerDefectInit(&(mThis(self)->sdBer), cAtPdhDe3AlarmSdBer);
    AtProfilerDefectInit(&(mThis(self)->idle), cAtPdhDs3AlarmIdle);
    AtProfilerDefectInit(&(mThis(self)->berTca), cAtPdhDe3AlarmBerTca);
    AtProfilerDefectInit(&(mThis(self)->aicChange), cAtPdhDs3AlarmAicChange);
    AtProfilerDefectInit(&(mThis(self)->feacChange), cAtPdhDs3AlarmFeacChange);
    AtProfilerDefectInit(&(mThis(self)->tim), cAtPdhE3AlarmTim);
    AtProfilerDefectInit(&(mThis(self)->pldTypeChange), cAtPdhE3AlarmPldTypeChange);
    AtProfilerDefectInit(&(mThis(self)->tmChange), cAtPdhE3AlarmTmChange);
    AtProfilerDefectInit(&(mThis(self)->ssmChange), cAtPdhDe3AlarmSsmChange);
    AtProfilerDefectInit(&(mThis(self)->mdlChange), cAtPdhDs3AlarmMdlChange);
    AtProfilerDefectInit(&(mThis(self)->clockStateChange), cAtPdhDe3AlarmClockStateChange);

    return cAtOk;
    }

static void OverrideAtFailureProfiler(AtFailureProfiler self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtFailureProfilerOverride, mMethodsGet(self), sizeof(m_AtFailureProfilerOverride));

        mMethodOverride(m_AtFailureProfilerOverride, DefectInfo);
        mMethodOverride(m_AtFailureProfilerOverride, DefectTypes);
        mMethodOverride(m_AtFailureProfilerOverride, Setup);
        }

    mMethodsSet(self, &m_AtFailureProfilerOverride);
    }

static void Override(AtFailureProfiler self)
    {
    OverrideAtFailureProfiler(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe3FailureProfiler);
    }

static AtFailureProfiler ObjectInit(AtFailureProfiler self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtFailureProfilerObjectInit((AtFailureProfiler)self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFailureProfiler AtPdhDe3FailureProfilerNew(AtChannel de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFailureProfiler newProfiler = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProfiler == NULL)
        return NULL;

    return ObjectInit(newProfiler, de3);
    }

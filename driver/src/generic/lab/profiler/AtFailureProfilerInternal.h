/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : All module
 * 
 * File        : AtFailureProfilerInternal.h
 * 
 * Created Date: Jul 18, 2017
 *
 * Description : AtFailure profiler
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFAILUREPROFILERINTERNAL_H_
#define _ATFAILUREPROFILERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtOsal.h"
#include "AtChannelClasses.h"
#include "AtChannel.h"
#include "AtFailureProfiler.h"
#include "../../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtProfilerDefect
    {
    uint32 type;
    tAtOsalCurTime clearTime;
    tAtOsalCurTime raiseTime;
    uint32 currentStatus;
    }tAtProfilerDefect;

typedef struct tAtFailureProfilerEvent
    {
    uint32 type;

    /* Defect information */
    tAtProfilerDefect defect;

    /* Failure information */
    tAtOsalCurTime reportedTime;
    uint32 status;
    uint32 profiledTimeMs;
    }tAtFailureProfilerEvent;

typedef struct tAtFailureProfilerMethods
    {
    AtProfilerDefect (*DefectInfo)(AtFailureProfiler self, uint32 defectType);
    eAtRet (*DefectInject)(AtFailureProfiler self, uint32 defects, uint32 currentStatus);
    eAtRet (*FailureInject)(AtFailureProfiler self, uint32 failures, uint32 currentStatus);
    const uint32 *(*DefectTypes)(AtFailureProfiler self, uint32 *numDefects);
    eAtRet (*Setup)(AtFailureProfiler self);
    uint32 (*DefectForFailure)(AtFailureProfiler self, uint32 failureType);
    }tAtFailureProfilerMethods;

typedef struct tAtFailureProfiler
    {
    tAtObject super;
    const tAtFailureProfilerMethods *methods;

    /* Private data */
    AtOsalMutex mutex;
    AtList events;
    AtChannel channel;
    eBool injectionEnabled;
    }tAtFailureProfiler;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFailureProfiler AtFailureProfilerObjectInit(AtFailureProfiler self, AtChannel channel);

/* Concrete profilers */
AtFailureProfiler AtSdhLineFailureProfilerNew(AtChannel channel);
AtFailureProfiler AtSdhPathFailureProfilerNew(AtChannel channel);
AtFailureProfiler AtPdhDe1FailureProfilerNew(AtChannel channel);
AtFailureProfiler AtPdhDe3FailureProfilerNew(AtChannel channel);
AtFailureProfiler AtPwFailureProfilerNew(AtChannel channel);

void AtProfilerDefectInit(AtProfilerDefect defectInfo, uint32 type);

#ifdef __cplusplus
}
#endif
#endif /* _ATFAILUREPROFILERINTERNAL_H_ */


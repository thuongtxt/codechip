/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : All module
 * 
 * File        : AtFailureProfilerFactoryInternal.h
 * 
 * Created Date: Jul 18, 2017
 *
 * Description : Failure profiler
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFAILUREPROFILERFACTORYINTERNAL_H_
#define _ATFAILUREPROFILERFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../man/AtDriverInternal.h"
#include "AtFailureProfilerFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFailureProfilerFactoryMethods
    {
    AtFailureProfiler (*SdhLineProfilerCreate)(AtFailureProfilerFactory self, AtChannel channel);
    AtFailureProfiler (*SdhPathProfilerCreate)(AtFailureProfilerFactory self, AtChannel channel);
    AtFailureProfiler (*PdhDe1ProfilerCreate)(AtFailureProfilerFactory self, AtChannel channel);
    AtFailureProfiler (*PdhDe3ProfilerCreate)(AtFailureProfilerFactory self, AtChannel channel);
    AtFailureProfiler (*PwProfilerCreate)(AtFailureProfilerFactory self, AtChannel channel);
    }tAtFailureProfilerFactoryMethods;

typedef struct tAtFailureProfilerFactory
    {
    tAtObject super;
    const tAtFailureProfilerFactoryMethods *methods;
    }tAtFailureProfilerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus

}
#endif
#endif /* _ATFAILUREPROFILERFACTORYINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : AtFailureProfiler.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : Pdh De1 failure profiler
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFailureProfilerInternal.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtPdhDe1FailureProfiler *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPdhDe1FailureProfiler
    {
    tAtFailureProfiler super;

    /* Private data */
    tAtProfilerDefect los;
    tAtProfilerDefect lof;
    tAtProfilerDefect ais;
    tAtProfilerDefect rai;
    tAtProfilerDefect lomf;
    tAtProfilerDefect sfBer;
    tAtProfilerDefect sdBer;
    tAtProfilerDefect sigLof;
    tAtProfilerDefect sigRai;
    tAtProfilerDefect eBit;
    tAtProfilerDefect bomChange;
    tAtProfilerDefect lapdChange;
    tAtProfilerDefect inbandLoopCodeChange;
    tAtProfilerDefect aisCi;
    tAtProfilerDefect raiCi;
    tAtProfilerDefect prmLBBitChange;
    tAtProfilerDefect ssmChange;
    tAtProfilerDefect berTca;
    tAtProfilerDefect clockStateChange;
    }tAtPdhDe1FailureProfiler;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtFailureProfilerMethods m_AtFailureProfilerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtProfilerDefect DefectInfo(AtFailureProfiler self, uint32 defectType)
    {
    switch (defectType)
        {
        case cAtPdhDe1AlarmLos                 : return &(mThis(self)->los);
        case cAtPdhDe1AlarmLof                 : return &(mThis(self)->lof);
        case cAtPdhDe1AlarmAis                 : return &(mThis(self)->ais);
        case cAtPdhDe1AlarmRai                 : return &(mThis(self)->rai);
        case cAtPdhDe1AlarmLomf                : return &(mThis(self)->lomf);
        case cAtPdhDe1AlarmSfBer               : return &(mThis(self)->sfBer);
        case cAtPdhDe1AlarmSdBer               : return &(mThis(self)->sdBer);
        case cAtPdhDe1AlarmSigLof              : return &(mThis(self)->sigLof);
        case cAtPdhDe1AlarmSigRai              : return &(mThis(self)->sigRai);
        case cAtPdhDe1AlarmEbit                : return &(mThis(self)->eBit);
        case cAtPdhDs1AlarmBomChange           : return &(mThis(self)->bomChange);
        case cAtPdhDs1AlarmLapdChange          : return &(mThis(self)->lapdChange);
        case cAtPdhDs1AlarmInbandLoopCodeChange: return &(mThis(self)->inbandLoopCodeChange);
        case cAtPdhDe1AlarmAisCi               : return &(mThis(self)->aisCi);
        case cAtPdhDe1AlarmRaiCi               : return &(mThis(self)->raiCi);
        case cAtPdhDs1AlarmPrmLBBitChange      : return &(mThis(self)->prmLBBitChange);
        case cAtPdhE1AlarmSsmChange            : return &(mThis(self)->ssmChange);
        case cAtPdhDe1AlarmBerTca              : return &(mThis(self)->berTca);
        case cAtPdhDe1AlarmClockStateChange    : return &(mThis(self)->clockStateChange);

        default:
            return NULL;
        }
    }

static const uint32 *DefectTypes(AtFailureProfiler self, uint32 *numDefects)
    {
    static const uint32 cAtPdhDe1AlarmTypeVal[] =
        {
        cAtPdhDe1AlarmLos,
        cAtPdhDe1AlarmLof,
        cAtPdhDe1AlarmAis,
        cAtPdhDe1AlarmRai,
        cAtPdhDe1AlarmLomf,
        cAtPdhDe1AlarmSfBer,
        cAtPdhDe1AlarmSdBer,
        cAtPdhDe1AlarmSigLof,
        cAtPdhDe1AlarmSigRai,
        cAtPdhDe1AlarmEbit,
        cAtPdhDs1AlarmBomChange,
        cAtPdhDs1AlarmInbandLoopCodeChange,
        cAtPdhDs1AlarmPrmLBBitChange,
        cAtPdhE1AlarmSsmChange,
        cAtPdhDe1AlarmBerTca,
        cAtPdhDe1AlarmClockStateChange
        };

    AtUnused(self);
    if (numDefects)
        *numDefects = mCount(cAtPdhDe1AlarmTypeVal);
    return cAtPdhDe1AlarmTypeVal;
    }

static eAtRet Setup(AtFailureProfiler self)
    {
    AtProfilerDefectInit(&(mThis(self)->los), cAtPdhDe1AlarmLos);
    AtProfilerDefectInit(&(mThis(self)->lof), cAtPdhDe1AlarmLof);
    AtProfilerDefectInit(&(mThis(self)->ais), cAtPdhDe1AlarmAis);
    AtProfilerDefectInit(&(mThis(self)->rai), cAtPdhDe1AlarmRai);
    AtProfilerDefectInit(&(mThis(self)->lomf), cAtPdhDe1AlarmLomf);
    AtProfilerDefectInit(&(mThis(self)->sfBer), cAtPdhDe1AlarmSfBer);
    AtProfilerDefectInit(&(mThis(self)->sdBer), cAtPdhDe1AlarmSdBer);
    AtProfilerDefectInit(&(mThis(self)->sigLof), cAtPdhDe1AlarmSigLof);
    AtProfilerDefectInit(&(mThis(self)->sigRai), cAtPdhDe1AlarmSigRai);
    AtProfilerDefectInit(&(mThis(self)->eBit), cAtPdhDe1AlarmEbit);
    AtProfilerDefectInit(&(mThis(self)->bomChange), cAtPdhDs1AlarmBomChange);
    AtProfilerDefectInit(&(mThis(self)->lapdChange), cAtPdhDs1AlarmLapdChange);
    AtProfilerDefectInit(&(mThis(self)->inbandLoopCodeChange), cAtPdhDs1AlarmInbandLoopCodeChange);
    AtProfilerDefectInit(&(mThis(self)->aisCi), cAtPdhDe1AlarmAisCi);
    AtProfilerDefectInit(&(mThis(self)->raiCi), cAtPdhDe1AlarmRaiCi);
    AtProfilerDefectInit(&(mThis(self)->prmLBBitChange), cAtPdhDs1AlarmPrmLBBitChange);
    AtProfilerDefectInit(&(mThis(self)->ssmChange), cAtPdhE1AlarmSsmChange);
    AtProfilerDefectInit(&(mThis(self)->berTca), cAtPdhDe1AlarmBerTca);
    AtProfilerDefectInit(&(mThis(self)->clockStateChange), cAtPdhDe1AlarmClockStateChange);

    return cAtOk;
    }

static void OverrideFailureProfiler(AtFailureProfiler self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtFailureProfilerOverride, mMethodsGet(self), sizeof(m_AtFailureProfilerOverride));

        mMethodOverride(m_AtFailureProfilerOverride, DefectInfo);
        mMethodOverride(m_AtFailureProfilerOverride, DefectTypes);
        mMethodOverride(m_AtFailureProfilerOverride, Setup);
        }

    mMethodsSet(self, &m_AtFailureProfilerOverride);
    }

static void Override(AtFailureProfiler self)
    {
    OverrideFailureProfiler(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe1FailureProfiler);
    }

static AtFailureProfiler ObjectInit(AtFailureProfiler self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtFailureProfilerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFailureProfiler AtPdhDe1FailureProfilerNew(AtChannel de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFailureProfiler newProfiler = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProfiler == NULL)
        return NULL;

    return ObjectInit(newProfiler, de1);
    }

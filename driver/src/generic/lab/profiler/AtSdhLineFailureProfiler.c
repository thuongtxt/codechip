/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : AtSdhLineFailureProfiler.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : SDH line failure profiler
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFailureProfilerInternal.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtSdhLineFailureProfiler *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtSdhLineFailureProfiler
    {
    tAtFailureProfiler super;

    /* Private data */
    tAtProfilerDefect los;
    tAtProfilerDefect oof;
    tAtProfilerDefect lof;
    tAtProfilerDefect tim;
    tAtProfilerDefect ais;
    tAtProfilerDefect rdi;
    tAtProfilerDefect rfi;
    tAtProfilerDefect berSd;
    tAtProfilerDefect berSf;
    tAtProfilerDefect berTca;
    tAtProfilerDefect s1Changed;
    tAtProfilerDefect rsBerSd;
    tAtProfilerDefect rsBerSf;
    tAtProfilerDefect k1Changed;
    tAtProfilerDefect k2Changed;
    }tAtSdhLineFailureProfiler;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtFailureProfilerMethods m_AtFailureProfilerOverride;

/* Save super implementation */
static const tAtFailureProfilerMethods *m_AtFailureProfilerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtProfilerDefect DefectInfo(AtFailureProfiler self, uint32 defectType)
    {
    switch (defectType)
        {
        case cAtSdhLineAlarmLos     : return &(mThis(self)->los);
        case cAtSdhLineAlarmOof     : return &(mThis(self)->oof);
        case cAtSdhLineAlarmLof     : return &(mThis(self)->lof);
        case cAtSdhLineAlarmTim     : return &(mThis(self)->tim);
        case cAtSdhLineAlarmAis     : return &(mThis(self)->ais);
        case cAtSdhLineAlarmRdi     : return &(mThis(self)->rdi);
        case cAtSdhLineAlarmBerSd   : return &(mThis(self)->berSd);
        case cAtSdhLineAlarmBerSf   : return &(mThis(self)->berSf);
        case cAtSdhLineAlarmS1Change: return &(mThis(self)->s1Changed);
        case cAtSdhLineAlarmRsBerSd : return &(mThis(self)->rsBerSd);
        case cAtSdhLineAlarmRsBerSf : return &(mThis(self)->rsBerSf);
        case cAtSdhLineAlarmK1Change: return &(mThis(self)->k1Changed);
        case cAtSdhLineAlarmK2Change: return &(mThis(self)->k2Changed);
        case cAtSdhLineAlarmBerTca  : return &(mThis(self)->berTca);
        case cAtSdhLineAlarmRfi     : return &(mThis(self)->rfi);
        default:
            return NULL;
        }
    }

static const uint32 *DefectTypes(AtFailureProfiler self, uint32 *numDefects)
    {
    static const uint32 cAtSdhLineAlarmTypeVal[] =
        {
        cAtSdhLineAlarmLos,
        cAtSdhLineAlarmOof,
        cAtSdhLineAlarmLof,
        cAtSdhLineAlarmTim,
        cAtSdhLineAlarmAis,
        cAtSdhLineAlarmRdi,
        cAtSdhLineAlarmBerSd,
        cAtSdhLineAlarmBerSf,
        cAtSdhLineAlarmBerTca,
        cAtSdhLineAlarmS1Change,
        cAtSdhLineAlarmRsBerSd,
        cAtSdhLineAlarmRsBerSf,
        cAtSdhLineAlarmK1Change,
        cAtSdhLineAlarmK2Change,
        cAtSdhLineAlarmRfi
        };

    AtUnused(self);
    if (numDefects)
        *numDefects = mCount(cAtSdhLineAlarmTypeVal);
    return cAtSdhLineAlarmTypeVal;
    }

static eAtRet Setup(AtFailureProfiler self)
    {
    AtProfilerDefectInit(&(mThis(self)->los), cAtSdhLineAlarmLos);
    AtProfilerDefectInit(&(mThis(self)->oof), cAtSdhLineAlarmOof);
    AtProfilerDefectInit(&(mThis(self)->lof), cAtSdhLineAlarmLof);
    AtProfilerDefectInit(&(mThis(self)->tim), cAtSdhLineAlarmTim);
    AtProfilerDefectInit(&(mThis(self)->ais), cAtSdhLineAlarmAis);
    AtProfilerDefectInit(&(mThis(self)->rdi), cAtSdhLineAlarmRdi);
    AtProfilerDefectInit(&(mThis(self)->rfi), cAtSdhLineAlarmRfi);
    AtProfilerDefectInit(&(mThis(self)->berSd), cAtSdhLineAlarmBerSd);
    AtProfilerDefectInit(&(mThis(self)->berSf), cAtSdhLineAlarmBerSf);
    AtProfilerDefectInit(&(mThis(self)->berTca), cAtSdhLineAlarmBerTca);
    AtProfilerDefectInit(&(mThis(self)->s1Changed), cAtSdhLineAlarmS1Change);
    AtProfilerDefectInit(&(mThis(self)->rsBerSd), cAtSdhLineAlarmRsBerSd);
    AtProfilerDefectInit(&(mThis(self)->rsBerSf), cAtSdhLineAlarmRsBerSf);
    AtProfilerDefectInit(&(mThis(self)->k1Changed), cAtSdhLineAlarmK1Change);
    AtProfilerDefectInit(&(mThis(self)->k2Changed), cAtSdhLineAlarmK2Change);

    return cAtOk;
    }

static uint32 DefectForFailure(AtFailureProfiler self, uint32 failureType)
    {
    if (failureType & cAtSdhLineAlarmRfi)
        return cAtSdhLineAlarmRdi;
    return m_AtFailureProfilerMethods->DefectForFailure(self, failureType);
    }

static void OverrideFailureProfiler(AtFailureProfiler self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtFailureProfilerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtFailureProfilerOverride, m_AtFailureProfilerMethods, sizeof(m_AtFailureProfilerOverride));

        mMethodOverride(m_AtFailureProfilerOverride, DefectInfo);
        mMethodOverride(m_AtFailureProfilerOverride, DefectTypes);
        mMethodOverride(m_AtFailureProfilerOverride, Setup);
        mMethodOverride(m_AtFailureProfilerOverride, DefectForFailure);
        }

    mMethodsSet(self, &m_AtFailureProfilerOverride);
    }

static void Override(AtFailureProfiler self)
    {
    OverrideFailureProfiler(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhLineFailureProfiler);
    }

static AtFailureProfiler ObjectInit(AtFailureProfiler self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtFailureProfilerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtFailureProfiler AtSdhLineFailureProfilerNew(AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtFailureProfiler newProfiler = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProfiler == NULL)
        return NULL;

    return ObjectInit(newProfiler, channel);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Profiler
 * 
 * File        : AtFailureProfilerFactory.h
 * 
 * Created Date: Jul 31, 2017
 *
 * Description : Failure profiler factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFAILUREPROFILERFACTORY_H_
#define _ATFAILUREPROFILERFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtFailureProfiler.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFailureProfilerFactory * AtFailureProfilerFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Factory */
AtFailureProfilerFactory AtFailureProfilerDefaultFactory(void);

AtFailureProfilerFactory AtDeviceProfilerFactory(AtDevice device);
AtFailureProfilerFactory AtChannelProfilerFactory(AtChannel channel);

/* Factory methods */
AtFailureProfiler AtFailureProfilerFactorySdhLineProfilerCreate(AtFailureProfilerFactory self, AtChannel channel);
AtFailureProfiler AtFailureProfilerFactorySdhPathProfilerCreate(AtFailureProfilerFactory self, AtChannel channel);
AtFailureProfiler AtFailureProfilerFactoryPwProfilerCreate(AtFailureProfilerFactory self, AtChannel channel);
AtFailureProfiler AtFailureProfilerFactoryPdhDe1ProfilerCreate(AtFailureProfilerFactory self, AtChannel channel);
AtFailureProfiler AtFailureProfilerFactoryPdhDe3ProfilerCreate(AtFailureProfilerFactory self, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _ATFAILUREPROFILERFACTORY_H_ */


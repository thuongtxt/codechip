/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Profiler
 *
 * File        : AtFailureProfilerFactory.c
 *
 * Created Date: Jul 18, 2017
 *
 * Description : Failure profiler factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFailureProfilerFactoryInternal.h"
#include "AtFailureProfilerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtFailureProfilerFactoryMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtFailureProfiler PdhDe1ProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    AtUnused(self);
    return AtPdhDe1FailureProfilerNew(channel);
    }

static AtFailureProfiler PdhDe3ProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    AtUnused(self);
    return AtPdhDe3FailureProfilerNew(channel);
    }

static AtFailureProfiler PwProfilerCreate(AtFailureProfilerFactory self,  AtChannel channel)
    {
    AtUnused(self);
    return AtPwFailureProfilerNew(channel);
    }

static AtFailureProfiler SdhLineProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    AtUnused(self);
    return AtSdhLineFailureProfilerNew(channel);
    }

static AtFailureProfiler SdhPathProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    AtUnused(self);
    return AtSdhPathFailureProfilerNew(channel);
    }

static void MethodsInit(AtFailureProfilerFactory self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SdhLineProfilerCreate);
        mMethodOverride(m_methods, SdhPathProfilerCreate);
        mMethodOverride(m_methods, PdhDe1ProfilerCreate);
        mMethodOverride(m_methods, PdhDe3ProfilerCreate);
        mMethodOverride(m_methods, PwProfilerCreate);
        }

    mMethodsGet(self) = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFailureProfilerFactory);
    }

static AtFailureProfilerFactory ObjectInit(AtFailureProfilerFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtFailureProfilerFactory AtFailureProfilerDefaultFactory(void)
    {
    static tAtFailureProfilerFactory factory;
    static AtFailureProfilerFactory pFactory = NULL;

    if (pFactory == NULL)
        pFactory = ObjectInit((AtFailureProfilerFactory)&factory);

    return pFactory;
    }

AtFailureProfiler AtFailureProfilerFactorySdhLineProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->SdhLineProfilerCreate(self, channel);

    return NULL;
    }

AtFailureProfiler AtFailureProfilerFactorySdhPathProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->SdhPathProfilerCreate(self, channel);

    return NULL;
    }

AtFailureProfiler AtFailureProfilerFactoryPdhDe1ProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->PdhDe1ProfilerCreate(self, channel);

    return NULL;
    }

AtFailureProfiler AtFailureProfilerFactoryPdhDe3ProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->PdhDe3ProfilerCreate(self, channel);

    return NULL;
    }

AtFailureProfiler AtFailureProfilerFactoryPwProfilerCreate(AtFailureProfilerFactory self, AtChannel channel)
    {
    if (self)
        return mMethodsGet(self)->PwProfilerCreate(self, channel);

    return NULL;
    }

AtFailureProfilerFactory AtDeviceProfilerFactory(AtDevice device)
    {
    AtUnused(device);
    return AtFailureProfilerDefaultFactory();
    }

AtFailureProfilerFactory AtChannelProfilerFactory(AtChannel channel)
    {
    AtDevice device = AtChannelDeviceGet(channel);
    return AtDeviceProfilerFactory(device);
    }

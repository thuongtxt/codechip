/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpPort.c
 *
 * Created Date: Jun 27, 2018
 *
 * Description : Generic PTP port implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#include "AtPtpPsn.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../util/AtUtil.h"
#include "AtPtpPortInternal.h"


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPtpPortMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;

/* Save superclass implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "ptp_port";
    }

static eAtModulePtpRet AsymmetryDelaySet(AtPtpPort self, uint32 nanoseconds)
    {
    AtUnused(self);
    AtUnused(nanoseconds);
    return cAtErrorNotImplemented;
    }

static uint32 AsymmetryDelayGet(AtPtpPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet StateSet(AtPtpPort self, eAtPtpPortState state)
    {
    AtUnused(self);
    AtUnused(state);
    return cAtErrorNotImplemented;
    }

static eAtPtpPortState StateGet(AtPtpPort self)
    {
    AtUnused(self);
    return cAtPtpPortStateUnknown;
    }

static eAtModulePtpRet StepModeSet(AtPtpPort self, eAtPtpStepMode stepMode)
    {
    AtUnused(self);
    AtUnused(stepMode);
    return cAtErrorNotImplemented;
    }

static eAtPtpStepMode StepModeGet(AtPtpPort self)
    {
    AtUnused(self);
    return cAtPtpStepModeUnknown;
    }

static eBool IsIpV4Transport(eAtPtpTransportType type)
    {
    return ((type == cAtPtpTransportTypeIpV4) || (type == cAtPtpTransportTypeIpV4Vpn)) ? cAtTrue : cAtFalse;
    }

static eBool IsIpV6Transport(eAtPtpTransportType type)
    {
    return ((type == cAtPtpTransportTypeIpV6) || (type == cAtPtpTransportTypeIpV6Vpn)) ? cAtTrue : cAtFalse;
    }

static eBool IpPsnIsChanged(AtPtpPort self, eAtPtpTransportType type)
    {
    if (self->ipv4Psn && !IsIpV4Transport(type))
        return cAtTrue;

    if (self->ipv6Psn && !IsIpV6Transport(type))
        return cAtTrue;

    return cAtFalse;
    }

static void IpPsnDelete(AtPtpPort self)
    {
    AtObjectDelete((AtObject)self->ipv4Psn);
    AtObjectDelete((AtObject)self->ipv6Psn);
    self->ipv4Psn = NULL;
    self->ipv6Psn = NULL;
    }

static eAtModulePtpRet TransportTypeSet(AtPtpPort self, eAtPtpTransportType type)
    {
    if ((self->ipv4Psn == NULL) && (self->ipv6Psn == NULL))
        return cAtOk;

    if ((type != cAtPtpTransportTypeAny) && IpPsnIsChanged(self, type))
        IpPsnDelete(self);

    return cAtOk;
    }

static eAtPtpTransportType TransportTypeGet(AtPtpPort self)
    {
    AtUnused(self);
    return cAtPtpTransportTypeUnknown;
    }

static AtPtpPsn L2PsnGet(AtPtpPort self)
    {
    if (self->l2Psn == NULL)
        self->l2Psn = mMethodsGet(self)->L2PsnObjectCreate(self);
    return self->l2Psn;
    }

static AtPtpPsn Ipv4PsnGet(AtPtpPort self, AtPtpPsn (*ipPsnCreate)(AtPtpPort))
    {
    if (self->ipv4Psn)
        return self->ipv4Psn;

    self->ipv4Psn = ipPsnCreate(self);
    return self->ipv4Psn;
    }

static AtPtpPsn IpV4PsnGet(AtPtpPort self)
    {
    eAtPtpTransportType transportType = AtPtpPortTransportTypeGet(self);

    if (IsIpV4Transport(transportType) || (transportType == cAtPtpTransportTypeAny))
        return Ipv4PsnGet(self, mMethodsGet(self)->IpV4PsnObjectCreate);

    return NULL;
    }

static AtPtpPsn Ipv6PsnGet(AtPtpPort self, AtPtpPsn (*ipPsnCreate)(AtPtpPort))
    {
    if (self->ipv6Psn)
        return self->ipv6Psn;

    self->ipv6Psn = ipPsnCreate(self);
    return self->ipv6Psn;
    }

static AtPtpPsn IpV6PsnGet(AtPtpPort self)
    {
    eAtPtpTransportType transportType = AtPtpPortTransportTypeGet(self);

    if (IsIpV6Transport(transportType) || (transportType == cAtPtpTransportTypeAny))
        return Ipv6PsnGet(self, mMethodsGet(self)->IpV6PsnObjectCreate);

    return NULL;
    }

static AtPtpPsn PsnGet(AtPtpPort self, eAtPtpPsnType layer)
    {
    if (layer == cAtPtpPsnTypeLayer2)
        return L2PsnGet(self);

    if (layer == cAtPtpPsnTypeIpV4)
        return IpV4PsnGet(self);

    if (layer == cAtPtpPsnTypeIpV6)
        return IpV6PsnGet(self);

    return NULL;
    }

static AtPtpPsn L2PsnObjectCreate(AtPtpPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPtpPsn IpV4PsnObjectCreate(AtPtpPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPtpPsn IpV6PsnObjectCreate(AtPtpPort self)
    {
    AtUnused(self);
    return NULL;
    }

static AtEthPort EthPortGet(AtPtpPort self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet PsnGroupJoin(AtPtpPort self, AtPtpPsnGroup group)
    {
    AtUnused(self);
    AtUnused(group);
    return cAtErrorNotImplemented;
    }

static eAtRet PsnGroupLeave(AtPtpPort self, AtPtpPsnGroup group)
    {
    AtUnused(self);
    AtUnused(group);
    return cAtErrorNotImplemented;
    }

static eAtModulePtpRet TxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds)
    {
    AtUnused(self);
    AtUnused(nanoSeconds);
    return cAtErrorModeNotSupport;
    }

static uint32 TxDelayAdjustGet(AtPtpPort self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet RxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds)
    {
    AtUnused(self);
    AtUnused(nanoSeconds);
    return cAtErrorModeNotSupport;
    }

static uint32 RxDelayAdjustGet(AtPtpPort self)
    {
    AtUnused(self);
    return 0;
    }

static void DeleteAllPsn(AtPtpPort self)
    {
    AtObjectDelete((AtObject)self->l2Psn);
    self->l2Psn = NULL;
    IpPsnDelete(self);
    }

static void Delete(AtObject self)
    {
    DeleteAllPsn((AtPtpPort)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPtpPort object = (AtPtpPort)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(l2Psn);
    mEncodeObject(ipv4Psn);
    mEncodeObject(ipv6Psn);
    }

static eBool TransportTypeIsSupported(AtPtpPort self, eAtPtpTransportType type)
    {
    AtUnused(self);
    AtUnused(type);
    return cAtFalse;
    }

static void OverrideAtObject(AtPtpPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(tAtObjectMethods));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPtpPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void MethodsInit(AtPtpPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, AsymmetryDelaySet);
        mMethodOverride(m_methods, AsymmetryDelayGet);
        mMethodOverride(m_methods, StateSet);
        mMethodOverride(m_methods, StateGet);
        mMethodOverride(m_methods, StepModeSet);
        mMethodOverride(m_methods, StepModeGet);
        mMethodOverride(m_methods, EthPortGet);
        mMethodOverride(m_methods, TransportTypeSet);
        mMethodOverride(m_methods, TransportTypeGet);
        mMethodOverride(m_methods, PsnGet);
        mMethodOverride(m_methods, L2PsnObjectCreate);
        mMethodOverride(m_methods, IpV4PsnObjectCreate);
        mMethodOverride(m_methods, IpV6PsnObjectCreate);
        mMethodOverride(m_methods, PsnGroupJoin);
        mMethodOverride(m_methods, PsnGroupLeave);
        mMethodOverride(m_methods, TxDelayAdjustSet);
        mMethodOverride(m_methods, TxDelayAdjustGet);
        mMethodOverride(m_methods, RxDelayAdjustSet);
        mMethodOverride(m_methods, RxDelayAdjustGet);
        mMethodOverride(m_methods, TransportTypeIsSupported);
        }
    mMethodsSet(self, &m_methods);
    }

static void Override(AtPtpPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpPort);
    }

AtPtpPort AtPtpPortObjectInit(AtPtpPort self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet AtPtpPortPsnGroupJoin(AtPtpPort self, AtPtpPsnGroup group)
    {
    if (self)
        return mMethodsGet(self)->PsnGroupJoin(self, group);
    return cAtErrorNullPointer;
    }

eAtRet AtPtpPortPsnGroupLeave(AtPtpPort self, AtPtpPsnGroup group)
    {
    if (self)
        return mMethodsGet(self)->PsnGroupLeave(self, group);
    return cAtErrorNullPointer;
    }

eBool AtPtpPortTransportTypeIsSupported(AtPtpPort self, eAtPtpTransportType type)
    {
    if (self)
        return mMethodsGet(self)->TransportTypeIsSupported(self, type);
    return cAtFalse;
    }

/**
 * @addtogroup AtPtpPort
 * @{
 */

/**
 * Set asymmetry delay of the link between this PTP port and upstream PTP port,
 * which may be practically measured by application.
 *
 * @param self This PTP port
 * @param nanoseconds Asymmetry link delay in nanoseconds
 *
 * @return AT return code.
 */
eAtModulePtpRet AtPtpPortAsymmetryDelaySet(AtPtpPort self, uint32 nanoseconds)
    {
    mNumericalAttributeSet(AsymmetryDelaySet, nanoseconds);
    }

/**
 * Get asymmetry link delay between this PTP port and upstream PTP port
 *
 * @param self This PTP port
 *
 * @return Asymmetry link delay in nanoseconds
 */
uint32 AtPtpPortAsymmetryDelayGet(AtPtpPort self)
    {
    mAttributeGet(AsymmetryDelayGet, uint32, 0);
    }

/**
 * Set transport protocol type of a PTP port
 *
 * @param self This PTP port
 * @param type Transport protocol type @ref  eAtPtpTransportType "PTP Transport protocol type"
 *
 * @return AT return code.
 */
eAtModulePtpRet AtPtpPortTransportTypeSet(AtPtpPort self, eAtPtpTransportType type)
    {
    mNumericalAttributeSet(TransportTypeSet, type);
    }

/**
 * Get transport protocol type of a PTP port
 *
 * @param self This PTP port
 *
 * @return Transport protocol type @ref eAtPtpTransportType "PTP Transport protocol type"
 */
eAtPtpTransportType AtPtpPortTransportTypeGet(AtPtpPort self)
    {
    mAttributeGet(TransportTypeGet, eAtPtpPsnType, cAtPtpPsnTypeUnknown);
    }

/**
 * Get PSN layer
 *
 * @param self This port
 * @param layer Type of PSN layer @ref eAtPtpPsnType "PTP PSN type"
 *
 * @return PSN layer
 */
AtPtpPsn AtPtpPortPsnGet(AtPtpPort self, eAtPtpPsnType layer)
    {
    mOneParamObjectGet(PsnGet, AtPtpPsn, layer);
    }

/**
 * Set state for a PTP port
 *
 * @param self This PTP port
 * @param state Port state @ref eAtPtpPortState "PTP port state"
 *
 * @return AT return code.
 */
eAtModulePtpRet AtPtpPortStateSet(AtPtpPort self, eAtPtpPortState state)
    {
    mNumericalAttributeSet(StateSet, state);
    }

/**
 * Get state of a PTP port
 *
 * @param self This PTP port
 *
 * @return Port state @ref eAtPtpPortState "PTP port state"
 */
eAtPtpPortState AtPtpPortStateGet(AtPtpPort self)
    {
    mAttributeGet(StateGet, eAtPtpPortState, cAtPtpPortStateUnknown);
    }

/**
 * Set step mode for a PTP port
 *
 * @param self This PTP port
 * @param stepMode Step mode @ref eAtPtpStepMode "PTP step mode"
 *
 * @return AT return code.
 */
eAtModulePtpRet AtPtpPortStepModeSet(AtPtpPort self, eAtPtpStepMode stepMode)
    {
    mNumericalAttributeSet(StepModeSet, stepMode);
    }

/**
 * Get step mode of a PTP port
 *
 * @param self This PTP port
 *
 * @return PTP step mode @ref eAtPtpStepMode "PTP port step mode"
 */
eAtPtpStepMode AtPtpPortStepModeGet(AtPtpPort self)
    {
    mAttributeGet(StepModeGet, eAtPtpStepMode, cAtPtpStepModeUnknown);
    }

/**
 * Get reference Ethernet port which the PTP port uses as its physical port
 *
 * @param self This PTP port
 *
 * @return Ethernet port @ref AtEthPort "Ethernet port".
 */
AtEthPort AtPtpPortEthPortGet(AtPtpPort self)
    {
    mAttributeGet(EthPortGet, AtEthPort, NULL);
    }

/**
 * Set TX round trip delay compensation for a PTP port
 *
 * @param self This PTP port
 * @param nanoseconds nanoseconds configured
 *
 * @return AT return code.
 */
eAtModulePtpRet AtPtpPortTxDelayAdjustSet(AtPtpPort self, uint32 nanoseconds)
    {
    mNumericalAttributeSet(TxDelayAdjustSet, nanoseconds);
    }

/**
 * Get TX round trip delay compensation configuration of a PTP port
 *
 * @param self This PTP port
 *
 * @return number of nanoseconds configured
 */
uint32 AtPtpPortTxDelayAdjustGet(AtPtpPort self)
    {
    mAttributeGet(TxDelayAdjustGet, uint32, 0);
    }

/**
 * Set RX round trip delay compensation for a PTP port
 *
 * @param self This PTP port
 * @param nanoseconds nanoseconds configured
 *
 * @return AT return code.
 */
eAtModulePtpRet AtPtpPortRxDelayAdjustSet(AtPtpPort self, uint32 nanoseconds)
    {
    mNumericalAttributeSet(RxDelayAdjustSet, nanoseconds);
    }

/**
 * Get RX round trip delay compensation configuration of a PTP port
 *
 * @param self This PTP port
 *
 * @return number of nanoseconds configured
 */
uint32 AtPtpPortRxDelayAdjustGet(AtPtpPort self)
    {
    mAttributeGet(RxDelayAdjustGet, uint32, 0);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtModulePtpInternal.h
 * 
 * Created Date: Jun 27, 2018
 *
 * Description : Internal data for the AtModulePtp
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPTPINTERNAL_H_
#define _ATMODULEPTPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtModuleInternal.h"
#include "AtModulePtp.h"
#include "AtPtpPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePtpMethods
    {
    /* Port management */
    uint8 (*NumPtpPorts)(AtModulePtp self);
    AtPtpPort (*PtpPortGet)(AtModulePtp self, uint32 portId);
    AtPtpPort (*PtpPortObjectCreate)(AtModulePtp self, uint32 portId);

    /* Device property */
    eAtModulePtpRet (*DeviceTypeSet)(AtModulePtp self, eAtPtpDeviceType deviceType);
    eAtPtpDeviceType (*DeviceTypeGet)(AtModulePtp self);
    eAtModulePtpRet (*DeviceMacAddressSet)(AtModulePtp self, const uint8 *address);
    eAtModulePtpRet (*DeviceMacAddressGet)(AtModulePtp self, uint8 *address);
    eAtModulePtpRet (*DeviceMacAddressEnable)(AtModulePtp self, eBool enable);
    eBool (*DeviceMacAddressIsEnabled)(AtModulePtp self);
    eAtModulePtpRet (*CorrectionModeSet)(AtModulePtp self, eAtPtpCorrectionMode correctMode);
    eAtPtpCorrectionMode (*CorrectionModeGet)(AtModulePtp self);
    eAtModulePtpRet (*T1T3CaptureModeSet)(AtModulePtp self, eAtPtpT1T3TimeStampCaptureMode mode);
    eAtPtpT1T3TimeStampCaptureMode (*T1T3CaptureModeGet)(AtModulePtp self);
    uint32 (*T1T3CaptureContentGet)(AtModulePtp self, tAtPtpT1T3TimestampContent *timestampInfoBuffer, uint32 numberOfTimestamp);
    eBool (*DeviceTypeIsSupported)(AtModulePtp self, eAtPtpDeviceType deviceType);
    eBool (*CorrectionModeIsSupported)(AtModulePtp self, eAtPtpCorrectionMode correctionMode);
    eBool (*PpsSourceIsSupported)(AtModulePtp self, eAtPtpPpsSource source);

    /* Time property */
    eAtModulePtpRet (*PpsRouteTripDelaySet)(AtModulePtp self, uint32 nanoseconds);
    uint32 (*PpsRouteTripDelayGet)(AtModulePtp self);
    eAtModulePtpRet (*TimeOfDaySet)(AtModulePtp self, uint64 seconds);
    uint64 (*TimeOfDayGet)(AtModulePtp self);
    eAtModulePtpRet (*PpsSourceSet)(AtModulePtp self, eAtPtpPpsSource source);
    eAtPtpPpsSource (*PpsSourceGet)(AtModulePtp self);

    /* Multicast group */
    uint32 (*NumL2PsnGroups)(AtModulePtp self);
    AtPtpPsnGroup (*L2PsnGroupGet)(AtModulePtp self, uint32 groupId);
    uint32 (*NumIpV4PsnGroups)(AtModulePtp self);
    AtPtpPsnGroup (*IpV4PsnGroupGet)(AtModulePtp self, uint32 groupId);
    uint32 (*NumIpV6PsnGroups)(AtModulePtp self);
    AtPtpPsnGroup (*IpV6PsnGroupGet)(AtModulePtp self, uint32 groupId);
    AtPtpPsnGroup (*PsnGroupObjectCreate)(AtModulePtp self, uint32 groupId, eAtPtpPsnType psnType);

    /* expected vlan */
    eAtModulePtpRet (*ExpectedVlanSet)(AtModulePtp self, uint8 index, tAtEthVlanTag *atag);
    tAtEthVlanTag* (*ExpectedVlanGet)(AtModulePtp self, uint8 index, tAtEthVlanTag *atag);
    uint8 (*NumExpectedVlan)(AtModulePtp self);

    /* Timestamp bypass for debugging */
    eAtModulePtpRet (*TimestampBypassEnable)(AtModulePtp self, eBool enabled);
    eBool (*TimestampBypassIsEnabled)(AtModulePtp self);
    }tAtModulePtpMethods;

typedef struct tAtModulePtp
    {
    tAtModule super;
    const tAtModulePtpMethods *methods;

    /* Private data */
    AtPtpPort *ptpPorts;
    AtPtpPsnGroup *l2Groups;
    AtPtpPsnGroup *ipv4Groups;
    AtPtpPsnGroup *ipv6Groups;
    }tAtModulePtp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePtp AtModulePtpObjectInit(AtModulePtp self, AtDevice device);

/* Capacity */
eBool AtModulePtpDeviceTypeIsSupported(AtModulePtp self, eAtPtpDeviceType deviceType);
eBool AtModulePtpCorrectionModeIsSupported(AtModulePtp self, eAtPtpCorrectionMode correctionMode);
eBool AtModulePtpPpsSourceIsSupported(AtModulePtp self, eAtPtpPpsSource source);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPTPINTERNAL_H_ */


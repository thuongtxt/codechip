/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPortInternal.h
 * 
 * Created Date: Jun 27, 2018
 *
 * Description : Internal data for the AtPtpPort
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPORTINTERNAL_H_
#define _ATPTPPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelInternal.h"
#include "AtModulePtp.h"
#include "AtPtpPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPtpPortMethods
    {
    /* Asymmetry link delay */
    eAtModulePtpRet (*AsymmetryDelaySet)(AtPtpPort self, uint32 nanoseconds);
    uint32 (*AsymmetryDelayGet)(AtPtpPort self);

    /* State */
    eAtModulePtpRet (*StateSet)(AtPtpPort self, eAtPtpPortState state);
    eAtPtpPortState (*StateGet)(AtPtpPort self);

    /* Step mode */
    eAtModulePtpRet (*StepModeSet)(AtPtpPort self, eAtPtpStepMode stepMode);
    eAtPtpStepMode (*StepModeGet)(AtPtpPort self);

    /* Ethernet */
    AtEthPort (*EthPortGet)(AtPtpPort self);

    /* Transport protocol */
    eAtModulePtpRet (*TransportTypeSet)(AtPtpPort self, eAtPtpTransportType type);
    eAtPtpTransportType (*TransportTypeGet)(AtPtpPort self);
    eBool (*TransportTypeIsSupported)(AtPtpPort self, eAtPtpTransportType type);

    /* PSN */
    AtPtpPsn (*PsnGet)(AtPtpPort self, eAtPtpPsnType layer);
    AtPtpPsn (*L2PsnObjectCreate)(AtPtpPort self);
    AtPtpPsn (*IpV4PsnObjectCreate)(AtPtpPort self);
    AtPtpPsn (*IpV6PsnObjectCreate)(AtPtpPort self);

    /* PSN multicast group */
    eAtRet (*PsnGroupJoin)(AtPtpPort self, AtPtpPsnGroup group);
    eAtRet (*PsnGroupLeave)(AtPtpPort self, AtPtpPsnGroup group);

    /* Round trip delay compensation */
    eAtModulePtpRet (*TxDelayAdjustSet)(AtPtpPort self, uint32 nanoSeconds);
    uint32 (*TxDelayAdjustGet)(AtPtpPort self);
    eAtModulePtpRet (*RxDelayAdjustSet)(AtPtpPort self, uint32 nanoSeconds);
    uint32 (*RxDelayAdjustGet)(AtPtpPort self);
    }tAtPtpPortMethods;

typedef struct tAtPtpPort
    {
    tAtChannel super;
    const tAtPtpPortMethods *methods;

    /* Private data */
    AtPtpPsn l2Psn;
    AtPtpPsn ipv4Psn; /* L3 PSN */
    AtPtpPsn ipv6Psn; /* L3 PSN */
    }tAtPtpPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtpPort AtPtpPortObjectInit(AtPtpPort self, uint32 channelId, AtModule module);

eAtRet AtPtpPortPsnGroupJoin(AtPtpPort self, AtPtpPsnGroup group);
eAtRet AtPtpPortPsnGroupLeave(AtPtpPort self, AtPtpPsnGroup group);

/* Capacity */
eBool AtPtpPortTransportTypeIsSupported(AtPtpPort self, eAtPtpTransportType type);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPORTINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpIpV4Psn.c
 *
 * Created Date: Jul 9, 2018
 *
 * Description : Abstract PTP IPv4 PSN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPtpPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPtpPsnMethods         m_AtPtpPsnOverride;

/* Save superclass implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtPtpPsn self)
    {
    AtUnused(self);
    return "ptp_ipv4_psn";
    }

static uint8 AddressLengthInBytes(AtPtpPsn self)
    {
    AtUnused(self);
    return 4;
    }

static void OverrideAtPtpPsn(AtPtpIpPsn self)
    {
    AtPtpPsn psn = (AtPtpPsn)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpPsnOverride, mMethodsGet(psn), sizeof(m_AtPtpPsnOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpPsnOverride, TypeString);
        mMethodOverride(m_AtPtpPsnOverride, AddressLengthInBytes);
        }

    mMethodsSet(psn, &m_AtPtpPsnOverride);
    }

static void Override(AtPtpIpPsn self)
    {
    OverrideAtPtpPsn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpIpV4Psn);
    }

AtPtpIpPsn AtPtpIpV4PsnObjectInit(AtPtpIpPsn self, AtPtpPort ptpPort)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPtpIpPsnObjectInit(self, cAtPtpPsnTypeIpV4, ptpPort) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpPsn.c
 *
 * Created Date: Jul 4, 2018
 *
 * Description : Abstract PTP PSN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../util/AtUtil.h"
#include "AtPtpPsnInternal.h"
#include "../../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPtpPsnMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save superclass implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtPtpPsn self)
    {
    AtUnused(self);
    return "ptp_psn";
    }

static uint8 AddressLengthInBytes(AtPtpPsn self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet ExpectedUnicastDestAddressSet(AtPtpPsn self, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static eAtModulePtpRet ExpectedUnicastDestAddressGet(AtPtpPsn self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtError;
    }

static eBool ExpectedUnicastDestAddressIsEnabled(AtPtpPsn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePtpRet ExpectedMcastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtError;
    }

static eBool ExpectedMcastDestAddressIsEnabled(AtPtpPsn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePtpRet ExpectedMcastDestAddressSet(AtPtpPsn self, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static eAtModulePtpRet ExpectedMcastDestAddressGet(AtPtpPsn self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static eAtModulePtpRet ExpectedAnycastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtError;
    }

static eBool ExpectedAnycastDestAddressIsEnabled(AtPtpPsn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressSet(AtPtpPsn self, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressGet(AtPtpPsn self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEnable(AtPtpPsn self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtError;
    }

static eBool ExpectedUnicastSourceAddressIsEnabled(AtPtpPsn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePtpRet ExpectedAnycastSourceAddressEnable(AtPtpPsn self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtError;
    }

static eBool ExpectedAnycastSourceAddressIsEnabled(AtPtpPsn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32          MaxNumSourceAddressEntryGet(AtPtpPsn self)
    {
    AtUnused(self);
    return 0;
    }

static uint32          MaxNumDestAddressEntryGet(AtPtpPsn self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet ExpectedSourceAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModulePtpRet ExpectedSourceAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModulePtpRet ExpectedDestAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModulePtpRet ExpectedDestAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModulePtpRet ExpectedDestAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool ExpectedDestAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtFalse;
    }

static eAtModulePtpRet ExpectedSourceAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool ExpectedSourceAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtFalse;
    }

static eAtModulePtpRet ExpectedCVlanSet(AtPtpPsn self, tAtEthVlanTag *cTag)
    {
    AtUnused(self);
    AtUnused(cTag);
    return cAtErrorModeNotSupport;
    }

static tAtEthVlanTag* ExpectedCVlanGet(AtPtpPsn self, tAtEthVlanTag *cTag)
    {
    AtUnused(self);
    AtUnused(cTag);
    return NULL;
    }

static eAtModulePtpRet ExpectedSVlanSet(AtPtpPsn self, tAtEthVlanTag *sTag)
    {
    AtUnused(self);
    AtUnused(sTag);
    return cAtErrorModeNotSupport;
    }

static tAtEthVlanTag* ExpectedSVlanGet(AtPtpPsn self, tAtEthVlanTag *sTag)
    {
    AtUnused(self);
    AtUnused(sTag);
    return NULL;
    }

static void Delete(AtObject self)
    {
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPtpPsn object = (AtPtpPsn)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(ptpPort);
    mEncodeUInt(type);
    }

static const char *ToString(AtObject self)
    {
    static char buff[64];
    AtPtpPsn psn = (AtPtpPsn)self;
    AtChannel channel = (AtChannel)AtPtpPsnPortGet(psn);
    AtDevice dev = AtChannelDeviceGet(channel);
    AtSprintf(buff, "%s%s.%s", AtDeviceIdToString(dev), AtPtpPsnTypeString(psn), AtChannelIdString(channel));
    return buff;
    }

static void OverrideAtObject(AtPtpPsn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(tAtObjectMethods));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(AtPtpPsn self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressSet);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressGet);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressEnable);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressIsEnabled);
        mMethodOverride(m_methods, ExpectedMcastDestAddressSet);
        mMethodOverride(m_methods, ExpectedMcastDestAddressGet);
        mMethodOverride(m_methods, ExpectedMcastDestAddressEnable);
        mMethodOverride(m_methods, ExpectedMcastDestAddressIsEnabled);
        mMethodOverride(m_methods, ExpectedAnycastDestAddressEnable);
        mMethodOverride(m_methods, ExpectedAnycastDestAddressIsEnabled);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressSet);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressGet);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressEnable);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressIsEnabled);
        mMethodOverride(m_methods, ExpectedAnycastSourceAddressEnable);
        mMethodOverride(m_methods, ExpectedAnycastSourceAddressIsEnabled);
        mMethodOverride(m_methods, AddressLengthInBytes);

        mMethodOverride(m_methods, MaxNumDestAddressEntryGet);
        mMethodOverride(m_methods, MaxNumSourceAddressEntryGet);
        mMethodOverride(m_methods, ExpectedDestAddressEntrySet);
        mMethodOverride(m_methods, ExpectedDestAddressEntryGet);
        mMethodOverride(m_methods, ExpectedSourceAddressEntrySet);
        mMethodOverride(m_methods, ExpectedSourceAddressEntryGet);
        mMethodOverride(m_methods, ExpectedDestAddressEntryEnable);
        mMethodOverride(m_methods, ExpectedDestAddressEntryIsEnabled);
        mMethodOverride(m_methods, ExpectedSourceAddressEntryEnable);
        mMethodOverride(m_methods, ExpectedSourceAddressEntryIsEnabled);

        mMethodOverride(m_methods, ExpectedCVlanSet);
        mMethodOverride(m_methods, ExpectedCVlanGet);
        mMethodOverride(m_methods, ExpectedSVlanSet);
        mMethodOverride(m_methods, ExpectedSVlanGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtPtpPsn self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpPsn);
    }

AtPtpPsn AtPtpPsnObjectInit(AtPtpPsn self, eAtPtpPsnType psnType, AtPtpPort ptpPort)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
    self->type = psnType;
    self->ptpPort = ptpPort;

    return self;
    }

AtModulePtp AtPtpPsnModuleGet(AtPtpPsn self)
    {
    return (AtModulePtp)AtChannelModuleGet((AtChannel)AtPtpPsnPortGet(self));
    }

uint8 AtPtpPsnAddressLengthInBytes(AtPtpPsn self)
    {
    if (self)
        return mMethodsGet(self)->AddressLengthInBytes(self);
    return 0;
    }

/**
 * @addtogroup AtPtpPsn
 * @{
 */

/**
 * Get PTP port
 *
 * @param self This PSN
 *
 * @return PTP port @ref AtPtpPort "PTP port".
 */
AtPtpPort AtPtpPsnPortGet(AtPtpPsn self)
    {
    return (self) ? self->ptpPort : NULL;
    }

/**
 * Get PSN type
 *
 * @param self This PSN
 *
 * @return PTP PSN type @ref eAtPtpPsnType "PTP PSN type".
 */
eAtPtpPsnType AtPtpPsnTypeGet(AtPtpPsn self)
    {
    return (self) ? self->type : cAtPtpPsnTypeUnknown;
    }

/**
 * Get PSN type string
 *
 * @param self This PSN
 *
 * @return PSN type string
 */
const char * AtPtpPsnTypeString(AtPtpPsn self)
    {
    if (self)
        return mMethodsGet(self)->TypeString(self);
    return "NULL";
    }

/**
 * Set expected destination address
 *
 * @param self This PSN
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedUnicastDestAddressSet(AtPtpPsn self, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnAddressLengthInBytes(self));
        ret = mMethodsGet(self)->ExpectedUnicastDestAddressSet(self, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected destination address
 *
 * @param self This PSN
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedUnicastDestAddressGet(AtPtpPsn self, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->ExpectedUnicastDestAddressGet(self, address);
    return cAtErrorNullPointer;
    }

/**
 * Enable/disable expected destination address for classifying incoming PTP packets
 *
 * @param self This PSN
 * @param enable Enable/disable expected destination unicast address.
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedUnicastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    mNumericalAttributeSet(ExpectedUnicastDestAddressEnable, enable);
    }

/**
 * Get enabling state of expected destination address for classifying incoming PTP packets
 *
 * @param self This PSN
 *
 * @return Enable state of expected destination unicast address.
 */
eBool AtPtpPsnExpectedUnicastDestAddressIsEnabled(AtPtpPsn self)
    {
    mAttributeGet(ExpectedUnicastDestAddressIsEnabled, eBool, cAtFalse);
    }

/**
 * Set expected destination multicast address
 *
 * @param self This PSN
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedMcastDestAddressSet(AtPtpPsn self, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnAddressLengthInBytes(self));
        ret = mMethodsGet(self)->ExpectedMcastDestAddressSet(self, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected destination multicast address
 *
 * @param self This PSN
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedMcastDestAddressGet(AtPtpPsn self, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->ExpectedMcastDestAddressGet(self, address);
    return cAtErrorNullPointer;
    }

/**
 * Enable/disable expected destination multicast address for classifying incoming PTP packets
 *
 * @param self This PSN
 * @param enable Enable/disable expected destination multicast address.
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedMcastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    mNumericalAttributeSet(ExpectedMcastDestAddressEnable, enable);
    }

/**
 * Get enabling state of expected destination multicast address for classifying incoming PTP packets
 *
 * @param self This PSN
 *
 * @return Enable state of expected destination multicast address.
 */
eBool AtPtpPsnExpectedMcastDestAddressIsEnabled(AtPtpPsn self)
    {
    mAttributeGet(ExpectedMcastDestAddressIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable expected destination anycast address for classifying incoming PTP packets
 *
 * @param self This PSN
 * @param enable Enable/disable expected destination anycast address.
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedAnycastDestAddressEnable(AtPtpPsn self, eBool enable)
    {
    mNumericalAttributeSet(ExpectedAnycastDestAddressEnable, enable);
    }

/**
 * Get enabling state of expected destination anycast address for classifying incoming PTP packets
 *
 * @param self This PSN
 *
 * @return Enable state of expected destination anycast address.
 */
eBool AtPtpPsnExpectedAnycastDestAddressIsEnabled(AtPtpPsn self)
    {
    mAttributeGet(ExpectedAnycastDestAddressIsEnabled, eBool, cAtFalse);
    }

/**
 * Set expected source address
 *
 * @param self This PSN
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedUnicastSourceAddressSet(AtPtpPsn self, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnAddressLengthInBytes(self));
        ret = mMethodsGet(self)->ExpectedUnicastSourceAddressSet(self, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected source address
 *
 * @param self This PSN
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedUnicastSourceAddressGet(AtPtpPsn self, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->ExpectedUnicastSourceAddressGet(self, address);
    return cAtErrorNullPointer;
    }

/**
 * Enable/disable expected source address for classifying incoming PTP packets
 *
 * @param self This PSN
 * @param enable Enable/disable expected source unicast address.
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedUnicastSourceAddressEnable(AtPtpPsn self, eBool enable)
    {
    mNumericalAttributeSet(ExpectedUnicastSourceAddressEnable, enable);
    }

/**
 * Get enabling state of expected source address for classifying incoming PTP packets
 *
 * @param self This PSN
 *
 * @return Enable state of expected source unicast address.
 */
eBool AtPtpPsnExpectedUnicastSourceAddressIsEnabled(AtPtpPsn self)
    {
    mAttributeGet(ExpectedUnicastSourceAddressIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable expected source anycast address for classifying incoming PTP packets
 *
 * @param self This PSN
 * @param enable Enable/disable expected source anycast address.
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedAnycastSourceAddressEnable(AtPtpPsn self, eBool enable)
    {
    mNumericalAttributeSet(ExpectedAnycastSourceAddressEnable, enable);
    }

/**
 * Get enabling state of expected source anycast address for classifying incoming PTP packets
 *
 * @param self This PSN
 *
 * @return Enable state of expected source anycast address.
 */
eBool AtPtpPsnExpectedAnycastSourceAddressIsEnabled(AtPtpPsn self)
    {
    mAttributeGet(ExpectedAnycastSourceAddressIsEnabled, eBool, cAtFalse);
    }

/**
 * Get the number of source address entries that are supported.
 *
 * @param self This PSN
 *
 * @return Number of source address.
 */
uint32 AtPtpPsnMaxNumSourceAddressEntryGet(AtPtpPsn self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumSourceAddressEntryGet(self);
    return 0;
    }

/**
 * Get the number of dest address entries that are supported.
 *
 * @param self This PSN
 *
 * @return Number of dest address.
 */
uint32 AtPtpPsnMaxNumDestAddressEntryGet(AtPtpPsn self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumDestAddressEntryGet(self);
    return 0;
    }

/**
 * Set expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedSourceAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnAddressLengthInBytes(self));
        ret = mMethodsGet(self)->ExpectedSourceAddressEntrySet(self, entryIndex, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedSourceAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->ExpectedSourceAddressEntryGet(self, entryIndex, address);
    return cAtErrorNullPointer;
    }

/**
 * Set expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedDestAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnAddressLengthInBytes(self));
        ret = mMethodsGet(self)->ExpectedDestAddressEntrySet(self, entryIndex, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedDestAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->ExpectedDestAddressEntryGet(self, entryIndex, address);
    return cAtErrorNullPointer;
    }

/**
 * Enable expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param enable Enable/disable expected source address entry
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedSourceAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    mTwoParamsAttributeSet(ExpectedSourceAddressEntryEnable, entryIndex, enable);
    }

/**
 * Get enable status of expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 *
 * @return cAtTrue if expected source address entry is enabled, otherwise, return cAtFalse
 */
eBool AtPtpPsnExpectedSourceAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    mOneParamAttributeGet(ExpectedSourceAddressEntryIsEnabled, entryIndex, eBool, cAtFalse);
    }

/**
 * Enable expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param enable Enable/disable expected destination address entry
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedDestAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable)
    {
    mTwoParamsAttributeSet(ExpectedDestAddressEntryEnable, entryIndex, enable);
    }

/**
 * Get enable status of expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 *
 * @return cAtTrue if expected destination address entry is enabled, otherwise, return cAtFalse
 */
eBool AtPtpPsnExpectedDestAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex)
    {
    mOneParamAttributeGet(ExpectedDestAddressEntryIsEnabled, entryIndex, eBool, cAtFalse);
    }

/**
 * Set expected C-VLAN
 *
 * @param self This PSN
 * @param cVlan Pointer to input C-VLAN tag (only vlanId is interested)
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedCVlanSet(AtPtpPsn self, tAtEthVlanTag *cVlan)
    {
    mVlanTagAttributeSet(ExpectedCVlanSet, cVlan);
    }

/**
 * Get expected C-VLAN
 *
 * @param self This PTP PSN
 * @param cVlan Structure to hold C-VLAN information
 *
 * @return cVlan if C-VLAN exists, otherwise NULL is returned
 */
tAtEthVlanTag* AtPtpPsnExpectedCVlanGet(AtPtpPsn self, tAtEthVlanTag *cVlan)
    {
    mOneParamAttributeGet(ExpectedCVlanGet, cVlan, tAtEthVlanTag*, NULL);
    }

/**
 * Set expected S-VLAN
 *
 * @param self This PSN
 * @param sVlan Pointer to input S-VLAN (only vlanId is interested)
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnExpectedSVlanSet(AtPtpPsn self, tAtEthVlanTag *sVlan)
    {
    mVlanTagAttributeSet(ExpectedSVlanSet, sVlan);
    }

/**
 * Get expected S-VLAN
 *
 * @param self This PTP PSN
 * @param sVlan Structure to hold S-VLAN information
 *
 * @return sVlan if S-VLAN exists, otherwise NULL is returned
 */
tAtEthVlanTag* AtPtpPsnExpectedSVlanGet(AtPtpPsn self, tAtEthVlanTag *sVlan)
    {
    mOneParamAttributeGet(ExpectedSVlanGet, sVlan, tAtEthVlanTag*, NULL);
    }

/**
 * @}
 */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPsnInternal.h
 * 
 * Created Date: Jul 4, 2018
 *
 * Description : Abstract PTP PSN representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPSNINTERNAL_H_
#define _ATPTPPSNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtPtpPsn.h"
#include "../../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPtpPsnMethods
    {
    const char * (*TypeString)(AtPtpPsn self);

    /* Unicast dest. address */
    eAtModulePtpRet (*ExpectedUnicastDestAddressEnable)(AtPtpPsn self, eBool enable);
    eBool (*ExpectedUnicastDestAddressIsEnabled)(AtPtpPsn self);
    eAtModulePtpRet (*ExpectedUnicastDestAddressSet)(AtPtpPsn self, const uint8 *address);
    eAtModulePtpRet (*ExpectedUnicastDestAddressGet)(AtPtpPsn self, uint8 *address);

    /* Multicast dest. address */
    eAtModulePtpRet (*ExpectedMcastDestAddressEnable)(AtPtpPsn self, eBool enable);
    eBool (*ExpectedMcastDestAddressIsEnabled)(AtPtpPsn self);
    eAtModulePtpRet (*ExpectedMcastDestAddressSet)(AtPtpPsn self, const uint8 *address);
    eAtModulePtpRet (*ExpectedMcastDestAddressGet)(AtPtpPsn self, uint8 *address);

    /* Anycast dest. address */
    eAtModulePtpRet (*ExpectedAnycastDestAddressEnable)(AtPtpPsn self, eBool enable);
    eBool (*ExpectedAnycastDestAddressIsEnabled)(AtPtpPsn self);

    /* Unicast source address */
    eAtModulePtpRet (*ExpectedUnicastSourceAddressEnable)(AtPtpPsn self, eBool enable);
    eBool (*ExpectedUnicastSourceAddressIsEnabled)(AtPtpPsn self);
    eAtModulePtpRet (*ExpectedUnicastSourceAddressSet)(AtPtpPsn self, const uint8 *address);
    eAtModulePtpRet (*ExpectedUnicastSourceAddressGet)(AtPtpPsn self, uint8 *address);

    /* Anycast source address */
    eAtModulePtpRet (*ExpectedAnycastSourceAddressEnable)(AtPtpPsn self, eBool enable);
    eBool (*ExpectedAnycastSourceAddressIsEnabled)(AtPtpPsn self);

    /* Internal */
    uint8 (*AddressLengthInBytes)(AtPtpPsn self);

    /* Specifically for backplane/system side port which was not explicitly indicate unicast or multicast address */
    uint32          (*MaxNumSourceAddressEntryGet)(AtPtpPsn self);
    uint32          (*MaxNumDestAddressEntryGet)(AtPtpPsn self);
    eAtModulePtpRet (*ExpectedSourceAddressEntrySet)(AtPtpPsn self, uint32 entryIndex, const uint8 *address);
    eAtModulePtpRet (*ExpectedSourceAddressEntryGet)(AtPtpPsn self, uint32 entryIndex, uint8 *address);
    eAtModulePtpRet (*ExpectedDestAddressEntrySet)(AtPtpPsn self, uint32 entryIndex, const uint8 *address);
    eAtModulePtpRet (*ExpectedDestAddressEntryGet)(AtPtpPsn self, uint32 entryIndex, uint8 *address);
    eAtModulePtpRet (*ExpectedSourceAddressEntryEnable)(AtPtpPsn self, uint32 entryIndex, eBool enable);
    eBool (*ExpectedSourceAddressEntryIsEnabled)(AtPtpPsn self, uint32 entryIndex);
    eAtModulePtpRet (*ExpectedDestAddressEntryEnable)(AtPtpPsn self, uint32 entryIndex, eBool enable);
    eBool (*ExpectedDestAddressEntryIsEnabled)(AtPtpPsn self, uint32 entryIndex);

    /* expected vlan */
    eAtModulePtpRet (*ExpectedCVlanSet)(AtPtpPsn self, tAtEthVlanTag *cTag);
    tAtEthVlanTag* (*ExpectedCVlanGet)(AtPtpPsn self, tAtEthVlanTag *cTag);
    eAtModulePtpRet (*ExpectedSVlanSet)(AtPtpPsn self, tAtEthVlanTag *sTag);
    tAtEthVlanTag* (*ExpectedSVlanGet)(AtPtpPsn self, tAtEthVlanTag *sTag);
    }tAtPtpPsnMethods;

typedef struct tAtPtpPsn
    {
    tAtObject super;
    const tAtPtpPsnMethods *methods;

    /* Private attributes */
    AtPtpPort ptpPort;
    eAtPtpPsnType type;
    }tAtPtpPsn;

typedef struct tAtPtpL2Psn
    {
    tAtPtpPsn super;
    }tAtPtpL2Psn;

typedef struct tAtPtpIpPsnMethods
    {
    /* Destination IP */
    uint32 (*MaxNumUnicastDestAddressEntryGet)(AtPtpIpPsn self);
    eAtModulePtpRet (*ExpectedUnicastDestAddressEntrySet)(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address);
    eAtModulePtpRet (*ExpectedUnicastDestAddressEntryGet)(AtPtpIpPsn self, uint32 entryIndex, uint8 *address);
    eAtModulePtpRet (*ExpectedUnicastDestAddressEntryEnable)(AtPtpIpPsn self, uint32 entryIndex, eBool enable);
    eBool (*ExpectedUnicastDestAddressEntryIsEnabled)(AtPtpIpPsn self, uint32 entryIndex);

    /* Source IP */
    uint32 (*MaxNumUnicastSourceAddressEntryGet)(AtPtpIpPsn self);
    eAtModulePtpRet (*ExpectedUnicastSourceAddressEntrySet)(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address);
    eAtModulePtpRet (*ExpectedUnicastSourceAddressEntryGet)(AtPtpIpPsn self, uint32 entryIndex, uint8 *address);
    eAtModulePtpRet (*ExpectedUnicastSourceAddressEntryEnable)(AtPtpIpPsn self, uint32 entryIndex, eBool enable);
    eBool (*ExpectedUnicastSourceAddressEntryIsEnabled)(AtPtpIpPsn self, uint32 entryIndex);
    }tAtPtpIpPsnMethods;

typedef struct tAtPtpIpPsn
    {
    tAtPtpPsn super;
    const tAtPtpIpPsnMethods *methods;
    }tAtPtpIpPsn;

typedef struct tAtPtpIpV4Psn
    {
    tAtPtpIpPsn super;
    }tAtPtpIpV4Psn;

typedef struct tAtPtpIpV6Psn
    {
    tAtPtpIpPsn super;
    }tAtPtpIpV6Psn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtpPsn AtPtpPsnObjectInit(AtPtpPsn self, eAtPtpPsnType psnType, AtPtpPort ptpPort);
AtPtpL2Psn AtPtpL2PsnObjectInit(AtPtpL2Psn self, AtPtpPort ptpPort);
AtPtpIpPsn AtPtpIpPsnObjectInit(AtPtpIpPsn self, eAtPtpPsnType psnType, AtPtpPort ptpPort);
AtPtpIpPsn AtPtpIpV4PsnObjectInit(AtPtpIpPsn self, AtPtpPort ptpPort);
AtPtpIpPsn AtPtpIpV6PsnObjectInit(AtPtpIpPsn self, AtPtpPort ptpPort);

uint8 AtPtpPsnAddressLengthInBytes(AtPtpPsn self);

/* Utility */
AtModulePtp AtPtpPsnModuleGet(AtPtpPsn self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPSNINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpIpPsn.c
 *
 * Created Date: Jul 4, 2018
 *
 * Description : Abstract PTP IP PSN implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../util/AtUtil.h"
#include "AtPtpPsnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPtpIpPsnMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtPtpPsnMethods         m_AtPtpPsnOverride;

/* Save superclass implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtPtpPsn self)
    {
    AtUnused(self);
    return "ptp_ip_psn";
    }

static uint32 MaxNumUnicastDestAddressEntryGet(AtPtpIpPsn self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorNotImplemented;
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorNotImplemented;
    }

static eAtModulePtpRet ExpectedUnicastDestAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool ExpectedUnicastDestAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtFalse;
    }

static uint32 MaxNumUnicastSourceAddressEntryGet(AtPtpIpPsn self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorNotImplemented;
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(address);
    return cAtErrorNotImplemented;
    }

static eAtModulePtpRet ExpectedUnicastSourceAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool ExpectedUnicastSourceAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    /*AtPtpIpPsn object = (AtPtpPsn)self;*/

    m_AtObjectMethods->Serialize(self, encoder);
    }

static void OverrideAtObject(AtPtpIpPsn self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(tAtObjectMethods));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPtpPsn(AtPtpIpPsn self)
    {
    AtPtpPsn psn = (AtPtpPsn)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPtpPsnOverride, mMethodsGet(psn), sizeof(m_AtPtpPsnOverride));

        /* Setup methods */
        mMethodOverride(m_AtPtpPsnOverride, TypeString);
        }

    mMethodsSet(psn, &m_AtPtpPsnOverride);
    }

static void Override(AtPtpIpPsn self)
    {
    OverrideAtObject(self);
    OverrideAtPtpPsn(self);
    }

static void MethodsInit(AtPtpIpPsn self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxNumUnicastDestAddressEntryGet);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressEntrySet);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressEntryGet);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressEntryEnable);
        mMethodOverride(m_methods, ExpectedUnicastDestAddressEntryIsEnabled);
        mMethodOverride(m_methods, MaxNumUnicastSourceAddressEntryGet);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressEntrySet);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressEntryGet);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressEntryEnable);
        mMethodOverride(m_methods, ExpectedUnicastSourceAddressEntryIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpIpPsn);
    }

AtPtpIpPsn AtPtpIpPsnObjectInit(AtPtpIpPsn self, eAtPtpPsnType psnType, AtPtpPort ptpPort)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPtpPsnObjectInit((AtPtpPsn)self, psnType, ptpPort) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPtpIpPsn
 * @{
 */

/**
 * Get the number of unicast destination IP address entries that are supported.
 *
 * @param self This PSN
 *
 * @return Number of unicast destination IP address.
 */
uint32 AtPtpIpPsnMaxNumUnicastDestAddressEntryGet(AtPtpIpPsn self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumUnicastDestAddressEntryGet(self);
    return 0;
    }

/**
 * Set expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpIpPsnExpectedUnicastDestAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnAddressLengthInBytes((AtPtpPsn)self));
        ret = mMethodsGet(self)->ExpectedUnicastDestAddressEntrySet(self, entryIndex, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpIpPsnExpectedUnicastDestAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->ExpectedUnicastDestAddressEntryGet(self, entryIndex, address);
    return cAtErrorNullPointer;
    }

/**
 * Enable expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param enable Enable/disable expected destination address entry
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpIpPsnExpectedUnicastDestAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable)
    {
    mTwoParamsAttributeSet(ExpectedUnicastDestAddressEntryEnable, entryIndex, enable);
    }

/**
 * Get enable status of expected destination address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 *
 * @return cAtTrue if expected destination address entry is enabled, otherwise, return cAtFalse
 */
eBool AtPtpIpPsnExpectedUnicastDestAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex)
    {
    mOneParamAttributeGet(ExpectedUnicastDestAddressEntryIsEnabled, entryIndex, eBool, cAtFalse);
    }

/**
 * Get the number of unicast source IP address entries that are supported.
 *
 * @param self This PSN
 *
 * @return Number of unicast source IP address.
 */
uint32 AtPtpIpPsnMaxNumUnicastSourceAddressEntryGet(AtPtpIpPsn self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumUnicastSourceAddressEntryGet(self);
    return 0;
    }

/**
 * Set expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpIpPsnExpectedUnicastSourceAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnAddressLengthInBytes((AtPtpPsn)self));
        ret = mMethodsGet(self)->ExpectedUnicastSourceAddressEntrySet(self, entryIndex, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpIpPsnExpectedUnicastSourceAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->ExpectedUnicastSourceAddressEntryGet(self, entryIndex, address);
    return cAtErrorNullPointer;
    }

/**
 * Enable expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 * @param enable Enable/disable expected source address entry
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpIpPsnExpectedUnicastSourceAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable)
    {
    mTwoParamsAttributeSet(ExpectedUnicastSourceAddressEntryEnable, entryIndex, enable);
    }

/**
 * Get enable status of expected source address entry
 *
 * @param self This PSN
 * @param entryIndex Address entry index
 *
 * @return cAtTrue if expected source address entry is enabled, otherwise, return cAtFalse
 */
eBool AtPtpIpPsnExpectedUnicastSourceAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex)
    {
    mOneParamAttributeGet(ExpectedUnicastSourceAddressEntryIsEnabled, entryIndex, eBool, cAtFalse);
    }

/**
 * @}
 */


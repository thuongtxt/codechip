/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPsnGroupInternal.h
 * 
 * Created Date: Aug 2, 2018
 *
 * Description : PTP PSN multicast group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPSNGROUPINTERNAL_H_
#define _ATPTPPSNGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtModulePtp.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPtpPsnGroupMethods
    {
    const char * (*TypeString)(AtPtpPsnGroup self);
    eAtRet (*Init)(AtPtpPsnGroup self);
    eAtModulePtpRet (*AddressSet)(AtPtpPsnGroup self, const uint8 *address);
    eAtModulePtpRet (*AddressGet)(AtPtpPsnGroup self, uint8 *address);
    eAtModulePtpRet (*PortAdd)(AtPtpPsnGroup self, AtPtpPort port);
    eAtModulePtpRet (*PortRemove)(AtPtpPsnGroup self, AtPtpPort port);
    }tAtPtpPsnGroupMethods;

typedef struct tAtPtpPsnGroup
    {
    tAtObject super;
    const tAtPtpPsnGroupMethods *methods;

    /* Private attributes */
    AtModulePtp module;
    eAtPtpPsnType type;
    uint32 groupId;
    AtList portList;
    }tAtPtpPsnGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPtpPsnGroup AtPtpPsnGroupObjectInit(AtPtpPsnGroup self, uint32 groupId, eAtPtpPsnType psnType, AtModulePtp module);

uint8 AtPtpPsnGroupAddressLengthInBytes(AtPtpPsnGroup self);
eAtRet AtPtpPsnGroupInit(AtPtpPsnGroup self);
eBool AtPtpPsnGroupContainsPtpPort(AtPtpPsnGroup self, AtPtpPort ptpPort);
uint32 AtPtpPsnGroupNumPtpPorts(AtPtpPsnGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPSNGROUPINTERNAL_H_ */


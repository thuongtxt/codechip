/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtPtpPsnGroup.c
 *
 * Created Date: Aug 2, 2018
 *
 * Description : Abstract PTP PSN multicast group implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../../man/AtDriverInternal.h"
#include "../../util/AtUtil.h"
#include "../AtPtpPortInternal.h"
#include "AtPtpPsnGroupInternal.h"
#include "AtPtpPsnGroup.h"
#include "../../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPtpPsnGroupMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save superclass implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtPtpPsnGroup self)
    {
    switch (AtPtpPsnGroupTypeGet(self))
        {
        case cAtPtpPsnTypeLayer2:   return "ptp_l2_group";
        case cAtPtpPsnTypeIpV4:     return "ptp_ipv4_group";
        case cAtPtpPsnTypeIpV6:     return "ptp_ipv6_group";
        case cAtPtpPsnTypeUnknown:  return "unknown";
        default:                    return NULL;
        }
    }

static eAtRet Init(AtPtpPsnGroup self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint8 AddressLengthInBytes(AtPtpPsnGroup self)
    {
    switch (AtPtpPsnGroupTypeGet(self))
        {
        case cAtPtpPsnTypeLayer2:   return 6;
        case cAtPtpPsnTypeIpV4:     return 4;
        case cAtPtpPsnTypeIpV6:     return 16;
        case cAtPtpPsnTypeUnknown:  return 0;
        default:                    return 0;
        }
    }

static eAtModulePtpRet AddressSet(AtPtpPsnGroup self, const uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static eAtModulePtpRet AddressGet(AtPtpPsnGroup self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtError;
    }

static AtList PortListGet(AtPtpPsnGroup self)
    {
    if (self->portList)
        return self->portList;

    self->portList = AtListCreate(0);
    return self->portList;
    }

static eAtModulePtpRet PortAdd(AtPtpPsnGroup self, AtPtpPort port)
    {
    eAtRet ret = AtPtpPortPsnGroupJoin(port, self);
    if (ret != cAtOk)
        return ret;

    if (AtListContainsObject(PortListGet(self), (AtObject)port))
        return cAtOk;

    return AtListObjectAdd(PortListGet(self), (AtObject)port);
    }

static eAtModulePtpRet PortRemove(AtPtpPsnGroup self, AtPtpPort port)
    {
    eAtRet ret;

    if (port == NULL)
        return cAtOk;

    if (!AtListContainsObject(PortListGet(self), (AtObject)port))
        return cAtErrorRsrcNoAvail;

    ret = AtPtpPortPsnGroupLeave(port, self);
    ret |= AtListObjectRemove(PortListGet(self), (AtObject)port);

    return ret;
    }

static void PortListDelete(AtPtpPsnGroup self)
    {
    AtObjectDelete((AtObject)self->portList);
    self->portList = NULL;
    }

static void Delete(AtObject self)
    {
    PortListDelete((AtPtpPsnGroup)self);

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPtpPsnGroup object = (AtPtpPsnGroup)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(module);
    mEncodeUInt(type);
    mEncodeUInt(groupId);
    mEncodeObjectDescriptionList(portList);
    }

static const char *ToString(AtObject self)
    {
    static char buff[64];
    AtPtpPsnGroup group = (AtPtpPsnGroup)self;
    AtModule module = (AtModule)AtPtpPsnGroupModuleGet(group);
    AtDevice dev = AtModuleDeviceGet(module);

    AtSprintf(buff, "%s%s.%d", AtDeviceIdToString(dev), AtPtpPsnGroupTypeString(group), AtPtpPsnGroupIdGet(group) + 1);
    return buff;
    }

static void OverrideAtObject(AtPtpPsnGroup self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(tAtObjectMethods));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(AtPtpPsnGroup self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, AddressSet);
        mMethodOverride(m_methods, AddressGet);
        mMethodOverride(m_methods, PortAdd);
        mMethodOverride(m_methods, PortRemove);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtPtpPsnGroup self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpPsnGroup);
    }

AtPtpPsnGroup AtPtpPsnGroupObjectInit(AtPtpPsnGroup self, uint32 groupId, eAtPtpPsnType psnType, AtModulePtp module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
    self->type = psnType;
    self->module = module;
    self->groupId = groupId;

    return self;
    }

uint8 AtPtpPsnGroupAddressLengthInBytes(AtPtpPsnGroup self)
    {
    if (self)
        return AddressLengthInBytes(self);
    return 0;
    }

eAtRet AtPtpPsnGroupInit(AtPtpPsnGroup self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }

eBool AtPtpPsnGroupContainsPtpPort(AtPtpPsnGroup self, AtPtpPort ptpPort)
    {
    if (self)
        return AtListContainsObject(PortListGet(self), (AtObject)ptpPort);
    return cAtFalse;
    }

uint32 AtPtpPsnGroupNumPtpPorts(AtPtpPsnGroup self)
    {
    if (self)
        return AtListLengthGet(PortListGet(self));
    return 0;
    }

/**
 * @addtogroup AtPtpPsnGroup
 * @{
 */

/**
 * Get PTP module
 *
 * @param self This PSN group
 *
 * @return PTP module @ref AtModulePtp "PTP module".
 */
AtModulePtp AtPtpPsnGroupModuleGet(AtPtpPsnGroup self)
    {
    return (self) ? self->module : NULL;
    }

/**
 * Get PSN group identifier number
 *
 * @param self This PSN group
 *
 * @return PTP PSN group ID number
 */
uint32 AtPtpPsnGroupIdGet(AtPtpPsnGroup self)
    {
    return (self) ? self->groupId : cInvalidUint32;
    }

/**
 * Get PSN type
 *
 * @param self This PSN group
 *
 * @return PTP PSN type @ref eAtPtpPsnType "PTP PSN type".
 */
eAtPtpPsnType AtPtpPsnGroupTypeGet(AtPtpPsnGroup self)
    {
    return (self) ? self->type : cAtPtpPsnTypeUnknown;
    }

/**
 * Get PSN type string
 *
 * @param self This PSN group
 *
 * @return PSN type string
 */
const char * AtPtpPsnGroupTypeString(AtPtpPsnGroup self)
    {
    if (self)
        return mMethodsGet(self)->TypeString(self);
    return "NULL";
    }

/**
 * Set expected multicast address
 *
 * @param self This PSN
 * @param address Pointer to input address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnGroupAddressSet(AtPtpPsnGroup self, const uint8 *address)
    {
    if (self && address)
        {
        eAtRet ret;

        mAddressAttributeApiLogStart(address, AtPtpPsnGroupAddressLengthInBytes(self));
        ret = mMethodsGet(self)->AddressSet(self, address);
        AtDriverApiLogStop();

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get expected multicast address
 *
 * @param self This PSN group
 * @param [out] address Pointer to output address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnGroupAddressGet(AtPtpPsnGroup self, uint8 *address)
    {
    if (self && address)
        return mMethodsGet(self)->AddressGet(self, address);
    return cAtErrorNullPointer;
    }

/**
 * Add a PTP port into PSN multicast group
 *
 * @param self This PSN group
 * @param port PTP port
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnGroupPortAdd(AtPtpPsnGroup self, AtPtpPort port)
    {
    mObjectSet(PortAdd, port);
    }

/**
 * Remove a PTP port from PSN multicast group
 *
 * @param self This PSN group
 * @param port PTP port
 *
 * @return AT return code
 */
eAtModulePtpRet AtPtpPsnGroupPortRemove(AtPtpPsnGroup self, AtPtpPort port)
    {
    mObjectSet(PortRemove, port);
    }

/**
 * Get a PTP port in PSN multicast group
 *
 * @param self This PSN group
 * @param atIndex Index
 *
 * @return PTP port at if it was added to group, otherwise, NULL
 */
AtPtpPort AtPtpPsnGroupPortGet(AtPtpPsnGroup self, uint32 atIndex)
    {
    if (self)
        return (AtPtpPort)AtListObjectGet(PortListGet(self), atIndex);
    return NULL;
    }

/**
 * Create an iterator of PTP ports
 *
 * @param self This PSN group
 *
 * @return Iterator object
 */
AtIterator AtPtpPsnGroupPortIteratorCreate(AtPtpPsnGroup self)
    {
    if (self)
        return AtListIteratorCreate(PortListGet(self));
    return NULL;
    }

/**
 * @}
 */


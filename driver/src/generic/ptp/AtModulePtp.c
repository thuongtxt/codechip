/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PTP
 *
 * File        : AtModulePtp.c
 *
 * Created Date: Jun 27, 2018
 *
 * Description : Implementation for the AtModulePtp
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../util/AtUtil.h"
#include "../../util/coder/AtCoderUtil.h"
#include "psn/AtPtpPsnGroupInternal.h"
#include "AtModulePtpInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)         ((AtModulePtp)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModulePtpMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtModuleMethods         m_AtModuleOverride;

/* Save superclass implementation */
static const tAtObjectMethods   *m_AtObjectMethods = NULL;
static const tAtModuleMethods   *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumPtpPorts(AtModulePtp self)
    {
    AtUnused(self);
    return 0;
    }

static eBool PortIsValid(AtModulePtp self, uint32 portId)
    {
    return portId < AtModulePtpNumPortsGet(self) ? cAtTrue : cAtFalse;
    }

static AtPtpPort PtpPortGet(AtModulePtp self, uint32 portId)
    {
    /* Database has not been allocated */
    if (self->ptpPorts == NULL)
        return NULL;

    /* Get corresponding object from database */
    return PortIsValid(self, portId) ? self->ptpPorts[portId] : NULL;
    }

static AtPtpPort PtpPortObjectCreate(AtModulePtp self, uint32 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return NULL;
    }

static eAtModulePtpRet DeviceTypeSet(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    AtUnused(self);
    AtUnused(deviceType);
    return cAtError;
    }

static eAtPtpDeviceType DeviceTypeGet(AtModulePtp self)
    {
    AtUnused(self);
    return cAtPtpDeviceTypeUnknown;
    }

static eAtModulePtpRet DeviceMacAddressSet(AtModulePtp self, const uint8 *destMac)
    {
    AtUnused(self);
    AtUnused(destMac);
    return cAtError;
    }

static eAtModulePtpRet DeviceMacAddressGet(AtModulePtp self, uint8 *destMac)
    {
    AtUnused(self);
    AtUnused(destMac);
    return cAtError;
    }

static eAtModulePtpRet DeviceMacAddressEnable(AtModulePtp self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool DeviceMacAddressIsEnabled(AtModulePtp self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePtpRet PpsRouteTripDelaySet(AtModulePtp self, uint32 nanoseconds)
    {
    AtUnused(self);
    AtUnused(nanoseconds);
    return cAtError;
    }

static uint32 PpsRouteTripDelayGet(AtModulePtp self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet TimeOfDaySet(AtModulePtp self, uint64 seconds)
    {
    AtUnused(self);
    AtUnused(seconds);
    return cAtError;
    }

static uint64 TimeOfDayGet(AtModulePtp self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet PpsSourceSet(AtModulePtp self, eAtPtpPpsSource source)
    {
    AtUnused(self);
    AtUnused(source);
    return cAtErrorNotImplemented;
    }

static eAtPtpPpsSource PpsSourceGet(AtModulePtp self)
    {
    AtUnused(self);
    return cAtPtpPpsSourceUnknown;
    }

static eBool GroupIsValid(uint32 groupId, uint32 maxNumGroups)
    {
    return groupId < maxNumGroups ? cAtTrue : cAtFalse;
    }

static AtPtpPsnGroup PsnGroupGet(AtModulePtp self, AtPtpPsnGroup *group, uint32 groupId, eAtPtpPsnType psnType)
    {
    if (group == NULL)
        return NULL;

    if (group[groupId] == NULL)
        {
        group[groupId] = mMethodsGet(self)->PsnGroupObjectCreate(self, groupId, psnType);
        if (group[groupId])
            AtPtpPsnGroupInit(group[groupId]);
        }

    return group[groupId];
    }

static uint32 NumL2PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 0;
    }

static AtPtpPsnGroup L2PsnGroupGet(AtModulePtp self, uint32 groupId)
    {
    if (!GroupIsValid(groupId, AtModulePtpNumL2PsnGroupsGet(self)))
        return NULL;

    return PsnGroupGet(self, self->l2Groups, groupId, cAtPtpPsnTypeLayer2);
    }

static uint32 NumIpV4PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 0;
    }

static AtPtpPsnGroup IpV4PsnGroupGet(AtModulePtp self, uint32 groupId)
    {
    if (!GroupIsValid(groupId, AtModulePtpNumIpV4PsnGroupsGet(self)))
        return NULL;

    return PsnGroupGet(self, self->ipv4Groups, groupId, cAtPtpPsnTypeIpV4);
    }

static uint32 NumIpV6PsnGroups(AtModulePtp self)
    {
    AtUnused(self);
    return 0;
    }

static AtPtpPsnGroup IpV6PsnGroupGet(AtModulePtp self, uint32 groupId)
    {
    if (!GroupIsValid(groupId, AtModulePtpNumIpV6PsnGroupsGet(self)))
        return NULL;

    return PsnGroupGet(self, self->ipv6Groups, groupId, cAtPtpPsnTypeIpV6);
    }

static AtPtpPsnGroup PsnGroupObjectCreate(AtModulePtp self, uint32 groupId, eAtPtpPsnType psnType)
    {
    AtUnused(self);
    AtUnused(groupId);
    AtUnused(psnType);
    return NULL;
    }

static eAtModulePtpRet CorrectionModeSet(AtModulePtp self, eAtPtpCorrectionMode correctMode)
    {
    AtUnused(self);
    AtUnused(correctMode);
    return cAtErrorNotImplemented;
    }

static eAtPtpCorrectionMode CorrectionModeGet(AtModulePtp self)
    {
    AtUnused(self);
    return cAtPtpCorrectionModeUnknown;
    }

static eAtModulePtpRet T1T3CaptureModeSet(AtModulePtp self, eAtPtpT1T3TimeStampCaptureMode mode)
    {
    AtUnused(self);
    if (mode != cAtPtpT1T3TimestampCaptureModeNone)
        return cAtErrorModeNotSupport;
    return cAtOk;
    }

static eAtPtpT1T3TimeStampCaptureMode T1T3CaptureModeGet(AtModulePtp self)
    {
    AtUnused(self);
    return cAtPtpT1T3TimestampCaptureModeNone;
    }

static uint32 T1T3CaptureContentGet(AtModulePtp self, tAtPtpT1T3TimestampContent *timestampInfoBuffer, uint32 numberOfTimestamp)
    {
    AtUnused(self);
    AtUnused(timestampInfoBuffer);
    AtUnused(numberOfTimestamp);
    return 0;
    }

static eAtModulePtpRet ExpectedVlanSet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    AtUnused(self);
    AtUnused(index);
    AtUnused(atag);
    return cAtErrorModeNotSupport;
    }

static tAtEthVlanTag* ExpectedVlanGet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    AtUnused(self);
    AtUnused(index);
    AtUnused(atag);
    return NULL;
    }

static uint8 NumExpectedVlan(AtModulePtp self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePtpRet TimestampBypassEnable(AtModulePtp self, eBool enabled)
    {
    AtUnused(self);
    if (!enabled)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eBool TimestampBypassIsEnabled(AtModulePtp self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AllPortsInit(AtModulePtp self)
    {
    eAtRet ret = cAtOk;
    uint32 numPorts = AtModulePtpNumPortsGet(self);
    uint8 i;

    if (self->ptpPorts == NULL)
        return cAtOk;

    for (i = 0; i < numPorts; i++)
        {
        AtPtpPort port = self->ptpPorts[i];
        ret |= AtChannelInit((AtChannel)port);
        }

    return ret;
    }

static eAtRet AllPortsCreate(AtModulePtp self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 numPorts = mMethodsGet(self)->NumPtpPorts(self);
    uint8 port_i;
    AtPtpPort *allPorts;
    uint32 memorySize;

    if (numPorts == 0)
        return cAtOk;

    memorySize = sizeof(AtPtpPort) * numPorts;
    allPorts = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allPorts == NULL)
        return cAtErrorRsrcNoAvail;

    mMethodsGet(osal)->MemInit(osal, allPorts, 0, memorySize);

    /* Create all port objects */
    for (port_i = 0; port_i < numPorts; port_i++)
        {
        uint8 createdPort_i;

        /* If cannot create one port, free all allocated resources */
        allPorts[port_i] = mMethodsGet(self)->PtpPortObjectCreate(self, port_i);
        if (allPorts[port_i] == NULL)
            {
            for (createdPort_i = 0; createdPort_i < port_i; createdPort_i++)
                AtObjectDelete((AtObject)allPorts[createdPort_i]);
            mMethodsGet(osal)->MemFree(osal, allPorts);

            return cAtErrorRsrcNoAvail;
            }
        }

    /* Update database */
    self->ptpPorts = allPorts;

    return cAtOk;
    }

static eAtRet AllPortsDelete(AtModulePtp self)
    {
    uint32 i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 numPorts = AtModulePtpNumPortsGet(self);

    /* Do nothing if there is no port */
    if (self->ptpPorts == NULL)
        return cAtOk;

    /* Delete all ports first */
    for (i = 0; i < numPorts; i++)
        AtObjectDelete((AtObject)(self->ptpPorts[i]));

    /* Free memory that hold all ports */
    mMethodsGet(osal)->MemFree(osal, self->ptpPorts);
    self->ptpPorts = NULL;

    return cAtOk;
    }

static AtPtpPsnGroup *PsnGroupsSetup(AtModulePtp self, uint32 maxNumGroups)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPtpPsnGroup *allGroups;
    uint32 memorySize;
    AtUnused(self);

    if (maxNumGroups == 0)
        return cAtOk;

    memorySize = sizeof(AtPtpPsnGroup) * maxNumGroups;
    allGroups = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allGroups == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, allGroups, 0, memorySize);
    return allGroups;
    }

static eAtRet AllPsnGroupsSetup(AtModulePtp self)
    {
    self->l2Groups = PsnGroupsSetup(self, mMethodsGet(self)->NumL2PsnGroups(self));
    self->ipv4Groups = PsnGroupsSetup(self, mMethodsGet(self)->NumIpV4PsnGroups(self));
    self->ipv6Groups = PsnGroupsSetup(self, mMethodsGet(self)->NumIpV6PsnGroups(self));
    return cAtOk;
    }

static eAtRet PsnGroupsDelete(AtPtpPsnGroup *groups, uint32 maxNumGroups)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 i;

    if (groups == NULL)
        return cAtOk;

    /* Delete all ports first */
    for (i = 0; i < maxNumGroups; i++)
        AtObjectDelete((AtObject)(groups[i]));

    /* Free memory that hold all group */
    mMethodsGet(osal)->MemFree(osal, groups);
    groups = NULL;

    return cAtOk;
    }

static eAtRet AllPsnGroupsDelete(AtModulePtp self)
    {
    PsnGroupsDelete(self->l2Groups, mMethodsGet(self)->NumL2PsnGroups(self));
    PsnGroupsDelete(self->ipv4Groups, mMethodsGet(self)->NumIpV4PsnGroups(self));
    PsnGroupsDelete(self->ipv6Groups, mMethodsGet(self)->NumIpV6PsnGroups(self));
    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    AtModulePtp ptpModule;
    eAtRet ret;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Just destroy and create all channels again to have a fresh memory */
    ptpModule = (AtModulePtp)self;
    ret |= AllPortsDelete(ptpModule);
    ret |= AllPortsCreate(ptpModule);

    ret |= AllPsnGroupsDelete(ptpModule);
    ret |= AllPsnGroupsSetup(ptpModule);

    return ret;
    }

static eAtRet TimestampInit(AtModulePtp self)
    {
    tAtOsalCurTime curTime;
    AtOsalCurTimeGet(&curTime);

    return AtModulePtpTimeOfDaySet(self, curTime.sec);
    }

static eAtRet Init(AtModule self)
    {
    AtModulePtp ptpModule = (AtModulePtp)self;
    eAtRet ret;

    /* Super init */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Initialize all managed objects */
    ret |= AllPortsInit(ptpModule);
    ret |= TimestampInit(ptpModule);

    return ret;
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "ptp";
    }

static const char *CapacityDescription(AtModule self)
    {
    static char string[128] = {0};

    if (AtStrlen(string))
        return string;

    AtSprintf(string, "ports: %d", AtModulePtpNumPortsGet((AtModulePtp)self));
    AtSprintf(&string[AtStrlen(string)], ", l2 mac groups: %d", AtModulePtpNumL2PsnGroupsGet((AtModulePtp)self));
    AtSprintf(&string[AtStrlen(string)], ", ipv4 groups: %d", AtModulePtpNumIpV4PsnGroupsGet((AtModulePtp)self));
    AtSprintf(&string[AtStrlen(string)], ", ipv6 groups: %d", AtModulePtpNumIpV6PsnGroupsGet((AtModulePtp)self));

    return string;
    }

static void Delete(AtObject self)
    {
    AllPortsDelete(mThis(self));
    AllPsnGroupsDelete(mThis(self));

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModulePtp object = (AtModulePtp)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjects(ptpPorts, AtModulePtpNumPortsGet(object));
    mEncodeObjects(l2Groups, AtModulePtpNumL2PsnGroupsGet(object));
    mEncodeObjects(ipv4Groups, AtModulePtpNumIpV4PsnGroupsGet(object));
    mEncodeObjects(ipv6Groups, AtModulePtpNumIpV4PsnGroupsGet(object));
    }

static eBool DeviceTypeIsSupported(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    /* Let concrete class do it */
    AtUnused(self);
    AtUnused(deviceType);
    return cAtFalse;
    }

static eBool PpsSourceIsSupported(AtModulePtp self, eAtPtpPpsSource source)
    {
    /* Let concrete class do it */
    AtUnused(self);
    AtUnused(source);
    return cAtFalse;
    }

static eBool CorrectionModeIsSupported(AtModulePtp self, eAtPtpCorrectionMode correctionMode)
    {
    /* Let concrete class do it */
    AtUnused(self);
    AtUnused(correctionMode);
    return cAtFalse;
    }

static void MethodsInit(AtModulePtp self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NumPtpPorts);
        mMethodOverride(m_methods, PtpPortGet);
        mMethodOverride(m_methods, PtpPortObjectCreate);
        mMethodOverride(m_methods, DeviceTypeSet);
        mMethodOverride(m_methods, DeviceTypeGet);
        mMethodOverride(m_methods, DeviceMacAddressSet);
        mMethodOverride(m_methods, DeviceMacAddressGet);
        mMethodOverride(m_methods, DeviceMacAddressEnable);
        mMethodOverride(m_methods, DeviceMacAddressIsEnabled);
        mMethodOverride(m_methods, DeviceTypeIsSupported);
        mMethodOverride(m_methods, CorrectionModeIsSupported);
        mMethodOverride(m_methods, PpsSourceIsSupported);

        mMethodOverride(m_methods, PpsRouteTripDelaySet);
        mMethodOverride(m_methods, PpsRouteTripDelayGet);
        mMethodOverride(m_methods, TimeOfDaySet);
        mMethodOverride(m_methods, TimeOfDayGet);
        mMethodOverride(m_methods, PpsSourceSet);
        mMethodOverride(m_methods, PpsSourceGet);

        mMethodOverride(m_methods, NumL2PsnGroups);
        mMethodOverride(m_methods, L2PsnGroupGet);
        mMethodOverride(m_methods, NumIpV4PsnGroups);
        mMethodOverride(m_methods, IpV4PsnGroupGet);
        mMethodOverride(m_methods, NumIpV6PsnGroups);
        mMethodOverride(m_methods, IpV6PsnGroupGet);
        mMethodOverride(m_methods, PsnGroupObjectCreate);
        mMethodOverride(m_methods, CorrectionModeSet);
        mMethodOverride(m_methods, CorrectionModeGet);
        mMethodOverride(m_methods, T1T3CaptureModeSet);
        mMethodOverride(m_methods, T1T3CaptureModeGet);
        mMethodOverride(m_methods, T1T3CaptureContentGet);

        mMethodOverride(m_methods, ExpectedVlanSet);
        mMethodOverride(m_methods, ExpectedVlanGet);
        mMethodOverride(m_methods, NumExpectedVlan);

        mMethodOverride(m_methods, TimestampBypassEnable);
        mMethodOverride(m_methods, TimestampBypassIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModulePtp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModulePtp self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModulePtp self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePtp);
    }

AtModulePtp AtModulePtpObjectInit(AtModulePtp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModulePtp, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

eBool AtModulePtpDeviceTypeIsSupported(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    if (self)
        return mMethodsGet(self)->DeviceTypeIsSupported(self, deviceType);
    return cAtFalse;
    }

eBool AtModulePtpCorrectionModeIsSupported(AtModulePtp self, eAtPtpCorrectionMode correctionMode)
    {
    if (self)
        return mMethodsGet(self)->CorrectionModeIsSupported(self, correctionMode);
    return cAtFalse;
    }

eBool AtModulePtpPpsSourceIsSupported(AtModulePtp self, eAtPtpPpsSource source)
    {
    if (self)
        return mMethodsGet(self)->PpsSourceIsSupported(self, source);
    return cAtFalse;
    }

/**
 * @addtogroup AtModulePtp
 * @{
 */

/**
 * Get number of PTP ports
 *
 * @param self This module
 *
 * @return Number of PTP ports.
 */
uint8 AtModulePtpNumPortsGet(AtModulePtp self)
    {
    mAttributeGet(NumPtpPorts, uint8, 0);
    }

/**
 * Get a PTP port
 *
 * @param self This module
 *
 * @return PTP port.
 */
AtPtpPort AtModulePtpPortGet(AtModulePtp self, uint32 portId)
    {
    mOneParamAttributeGet(PtpPortGet, portId, AtPtpPort, NULL);
    }

/**
 * Set PTP device type
 *
 * @param self This module
 * @param deviceType @ref eAtPtpDeviceType "PTP device type"
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpDeviceTypeSet(AtModulePtp self, eAtPtpDeviceType deviceType)
    {
    mNumericalAttributeSet(DeviceTypeSet, deviceType);
    }

/**
 * Get PTP device type
 *
 * @param self This module
 *
 * @return @ref eAtPtpDeviceType "PTP device type"
 */
eAtPtpDeviceType AtModulePtpDeviceTypeGet(AtModulePtp self)
    {
    mAttributeGet(DeviceTypeGet, eAtPtpDeviceType, cAtPtpDeviceTypeUnknown);
    }

/**
 * Set PTP device MAC address
 *
 * @param self This module
 * @param address Pointer to input MAC address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpDeviceMacAddressSet(AtModulePtp self, const uint8 *address)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorNullPointer;

    mMacAttributeApiLogStart(address);
    ret = mMethodsGet(self)->DeviceMacAddressSet(self, address);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get PTP device MAC address
 *
 * @param self This port
 * @param address Pointer to output MAC address array
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpDeviceMacAddressGet(AtModulePtp self, uint8 *address)
    {
    if (self)
        return mMethodsGet(self)->DeviceMacAddressGet(self, address);
    return cAtErrorNullPointer;
    }

/**
 * Enable/disable PTP device MAC address used for incoming PTP packet classifying
 *
 * @param self This module
 * @param enable Enable device MAC address classifying
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpDeviceMacAddressEnable(AtModulePtp self, eBool enable)
    {
    mNumericalAttributeSet(DeviceMacAddressEnable, enable);
    }

/**
 * Get enable state of PTP device MAC address classifying
 *
 * @param self This port
 *
 * @return cAtTrue if PTP device MAC address classifying is enabled
 * @return cAtFalse if PTP device MAC address classifying is disabled
 */
eBool AtModulePtpDeviceMacAddressIsEnabled(AtModulePtp self)
    {
    mAttributeGet(DeviceMacAddressIsEnabled, eBool, cAtFalse);
    }

/**
 * Set PPS route-trip delay time in nanoseconds
 *
 * @param self This module
 * @param nanoseconds PPS route-trip delay time in nanoseconds
 *
 * @return AT return code.
 */
eAtModulePtpRet AtModulePtpPpsRouteTripDelaySet(AtModulePtp self, uint32 nanoseconds)
    {
    mNumericalAttributeSet(PpsRouteTripDelaySet, nanoseconds);
    }

/**
 * Get PPS route-trip delay time in nanoseconds
 *
 * @param self This module
 *
 * @return PPS route-trip delay time in nanoseconds
 */
uint32 AtModulePtpPpsRouteTripDelayGet(AtModulePtp self)
    {
    mAttributeGet(PpsRouteTripDelayGet, uint32, 0);
    }

/**
 * Set time-of-day
 *
 * @param self This module
 * @param seconds Time-of-day in seconds
 *
 * @return AT return code.
 */
eAtModulePtpRet AtModulePtpTimeOfDaySet(AtModulePtp self, uint64 seconds)
    {
    mUint64AttributeSet(TimeOfDaySet, seconds);
    }

/**
 * Get time-of-day
 *
 * @param self This module
 *
 * @return Time-of-day in seconds
 */
uint64 AtModulePtpTimeOfDayGet(AtModulePtp self)
    {
    mAttributeGet(TimeOfDayGet, uint64, 0);
    }

/**
 * Set PPS time source
 *
 * @param self This module
 * @param source PPS time source @ref eAtPtpPpsSource "PTP PPS time source"
 *
 * @return AT return code.
 */
eAtModulePtpRet AtModulePtpPpsSourceSet(AtModulePtp self, eAtPtpPpsSource source)
    {
    mNumericalAttributeSet(PpsSourceSet, source);
    }

/**
 * Get PPS time source
 *
 * @param self This module
 *
 * @return PPS time source @ref eAtPtpPpsSource "PTP PPS time source"
 */
eAtPtpPpsSource AtModulePtpPpsSourceGet(AtModulePtp self)
    {
    mAttributeGet(PpsSourceGet, eAtPtpPpsSource, cAtPtpPpsSourceUnknown);
    }

/**
 * Get number of Ethernet MAC (L2) multicast groups
 *
 * @param self This module
 *
 * @return Number of MAC multicast groups.
 */
uint32 AtModulePtpNumL2PsnGroupsGet(AtModulePtp self)
    {
    mAttributeGet(NumL2PsnGroups, uint32, 0);
    }

/**
 * Get a Ethernet MAC (L2) multicast groups
 *
 * @param self This module
 * @param groupId Multicast group ID
 *
 * @return MAC multicast groups.
 */
AtPtpPsnGroup AtModulePtpL2PsnGroupGet(AtModulePtp self, uint32 groupId)
    {
    mOneParamAttributeGet(L2PsnGroupGet, groupId, AtPtpPsnGroup, NULL);
    }

/**
 * Get number of IPv4 multicast groups
 *
 * @param self This module
 *
 * @return Number of IPv4 multicast groups.
 */
uint32 AtModulePtpNumIpV4PsnGroupsGet(AtModulePtp self)
    {
    mAttributeGet(NumIpV4PsnGroups, uint32, 0);
    }

/**
 * Get an IPv4 multicast groups
 *
 * @param self This module
 * @param groupId Multicast group ID
 *
 * @return IPv4 multicast groups.
 */
AtPtpPsnGroup AtModulePtpIpV4PsnGroupGet(AtModulePtp self, uint32 groupId)
    {
    mOneParamAttributeGet(IpV4PsnGroupGet, groupId, AtPtpPsnGroup, NULL);
    }

/**
 * Get number of IPv6 multicast groups
 *
 * @param self This module
 *
 * @return Number of IPv6 multicast groups.
 */
uint32 AtModulePtpNumIpV6PsnGroupsGet(AtModulePtp self)
    {
    mAttributeGet(NumIpV6PsnGroups, uint32, 0);
    }

/**
 * Get an IPv6 multicast groups
 *
 * @param self This module
 * @param groupId Multicast group ID
 *
 * @return IPv6 multicast groups.
 */
AtPtpPsnGroup AtModulePtpIpV6PsnGroupGet(AtModulePtp self, uint32 groupId)
    {
    mOneParamAttributeGet(IpV6PsnGroupGet, groupId, AtPtpPsnGroup, NULL);
    }

/**
 * Set PTP correction mode in Transparent Clock mode
 *
 * @param self This module
 * @param correctionMode @ref eAtPtpCorrectionMode "PTP correction mode"
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpCorrectionModeSet(AtModulePtp self, eAtPtpCorrectionMode correctionMode)
    {
    mNumericalAttributeSet(CorrectionModeSet, correctionMode);
    }

/**
 * Get PTP correction mode in Transparent Clock mode
 *
 * @param self This module
 *
 * @return @ref eAtPtpCorrectionMode "PTP correction mode"
 */
eAtPtpCorrectionMode AtModulePtpCorrectionModeGet(AtModulePtp self)
    {
    mAttributeGet(CorrectionModeGet, eAtPtpCorrectionMode, cAtPtpCorrectionModeUnknown);
    }

/**
 * Set PTP T1' and T3' timestamp capture for CPU or for packet processor mode
 *
 * @param self This module
 * @param mode Timestamp capture mode
 *             @ref eAtPtpT1T3TimeStampCaptureMode "PTP T1' and T3' timestamp mode"
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpT1T3CaptureModeSet(AtModulePtp self, eAtPtpT1T3TimeStampCaptureMode mode)
    {
    mNumericalAttributeSet(T1T3CaptureModeSet, mode);
    }

/**
 * Get PTP T1' and T3' timestamp capture for CPU or for packet processor mode
 *
 * @param self This module
 *
 * @return Timestamp capture mode
 *         @ref eAtPtpT1T3TimeStampCaptureMode "PTP T1' and T3' timestamp mode"
 */
eAtPtpT1T3TimeStampCaptureMode AtModulePtpT1T3CaptureModeGet(AtModulePtp self)
    {
    mAttributeGet(T1T3CaptureModeGet, eAtPtpCorrectionMode, cAtPtpT1T3TimestampCaptureModeNone);
    }

/**
 * Get PTP T1' and T3' timestamp capture for CPU or for packet processor
 *
 * @param self This module
 * @param timestampInfoBuffer Buffer
 * @param numberOfTimestamp Number of information elements that the buffer can store.
 *
 * @return number of timestamp information elements. Each element is formated as @ref tAtPtpT1T3TimestampContent
 */
uint32 AtModulePtpT1T3CaptureContentGet(AtModulePtp self, tAtPtpT1T3TimestampContent *timestampInfoBuffer, uint32 numberOfTimestamp)
    {
    mTwoParamAttributeGet(T1T3CaptureContentGet, timestampInfoBuffer, numberOfTimestamp, uint32, 0);
    }

/**
 * Set PTP expected vlan for the module PTP
 *
 * @param self This module
 * @param index Index to set expected global vlan ID
 * @param atag expected Vlan tag (only vlanID is involved)
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpExpectedVlanSet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    mVlanTagAttributeAtIndexSet(ExpectedVlanSet, index, atag);
    }

/**
 * Get PTP expected vlan configured for the module PTP
 *
 * @param self This module
 * @param index Index to set expected global vlan ID
 * @param atag buffer to store expected Vlan tag (only vlanID is involved)
 *
 * @return the buffer of expected vlan tag (only vlanID is involved). NULL if expected vlanID is NULL
 */
tAtEthVlanTag* AtModulePtpExpectedVlanGet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag)
    {
    mTwoParamAttributeGet(ExpectedVlanGet, index, atag, tAtEthVlanTag*, NULL);
    }

/**
 * Get Number of global support expected vlanID PTP for the module PTP
 *
 * @param self This module
 *
 * @return the number of global supported expected vlanId PTP for the module PTP
 */
uint8 AtModulePtpNumExpectedVlan(AtModulePtp self)
    {
    mAttributeGet(NumExpectedVlan, uint8, 0);
    }

/**
 * Enable timestamp bypass for debug purpose
 *
 * @param self This module
 * @param enabled Enabled this debug feature
 *
 * @return AT return code
 */
eAtModulePtpRet AtModulePtpTimestampBypassEnable(AtModulePtp self, eBool enabled)
    {
    mNumericalAttributeSet(TimestampBypassEnable, enabled);
    }

/**
 * Get enabled debug configuration of the PTP timestamp bypass
 *
 * @param self This module
 *
 * @return cAtTrue: the timestamp bypass for debugging is enabled; cAtFalse: the timestamp bypass for debugging is disabled.
 */
eBool AtModulePtpTimestampBypassIsEnabled(AtModulePtp self)
    {
    mAttributeGet(TimestampBypassIsEnabled, eBool, cAtFalse);
    }

/**
 * @}
 */

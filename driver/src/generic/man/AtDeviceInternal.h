/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtDeviceInternal.h
 * 
 * Created Date: Jul 26, 2012
 *
 * Description : Device abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICEINTERNAL_H_
#define _ATDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtSSKeyChecker.h"
#include "../common/AtObjectInternal.h"
#include "../util/AtLongRegisterAccess.h"
#include "binder/AtEncapBinder.h"
#include "AtDeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mDeviceError(self_, ret_)                                              \
    do                                                                         \
        {                                                                      \
        uint32 saveRet__ = ret_;                                               \
        AtDeviceLog((AtDevice)self_, cAtLogLevelCritical, AtSourceLocation, "Error code = %s\r\n", AtRet2String(saveRet__)); \
        return saveRet__;                                                      \
        }while(0)

#define mDeviceSuccessAssert(self_, ret_)                                      \
    do                                                                         \
        {                                                                      \
        uint32 saveRet__ = ret_;                                               \
        if ((saveRet__) != cAtOk)                                              \
            mDeviceError(self_, saveRet__);                                    \
        }while(0)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDeviceEventListenerWrapper * AtDeviceEventListener;

typedef struct tAtDeviceEventListenerWrapper
    {
    tAtObject super;
    tAtDeviceEventListener listener;
    void *userData;
    }tAtDeviceEventListenerWrapper;

/* Methods */
typedef struct tAtDeviceMethods
    {
    /* Public methods */
    uint32 (*ProductCodeGet)(AtDevice self);
    AtDriver (*DriverGet)(AtDevice self);
    eAtRet (*Init)(AtDevice self);
    eAtRet (*AsyncInit)(AtDevice self);
    const char *(*Version)(AtDevice self);
    uint32 (*VersionNumber)(AtDevice self);
    uint32 (*BuiltNumber)(AtDevice self);
    uint8 (*ModuleVersion)(AtDevice self, eAtModule moduleId);
    eAtRet (*VersionNumberSet)(AtDevice self, uint32 versionNumber);
    eAtRet (*BuiltNumberSet)(AtDevice self, uint32 builtNumber);
    uint32 (*VersionNumberFromHwGet)(AtDevice self);
    uint32 (*BuiltNumberFromHwGet)(AtDevice self);
    eBool (*Accessible)(AtDevice self);

    /* To control reset */
    eAtRet (*Reset)(AtDevice self);   /* Deal with HW & SW */
    eAtRet (*AsyncReset)(AtDevice self);
    eAtRet (*HwReset)(AtDevice self); /* Just only HW */
    eAtRet (*AsyncHwReset)(AtDevice self);
    eBool (*ShouldResetBeforeInitializing)(AtDevice self);
    void (*ShowResetInfo)(AtDevice self);

    /* Warm restore */
    eAtRet (*WarmRestoreStart)(AtDevice self);
    eBool (*WarmRestoreIsStarted)(AtDevice self);
    eAtRet (*WarmRestoreStop)(AtDevice self);
    eBool (*WarmRestoreReadOperationDisabled)(AtDevice self);
    eBool (*ShouldRestoreDatabaseOnWarmRestoreStop)(AtDevice self);

    /* IP Cores */
    uint8 (*NumIpCoresGet)(AtDevice self);
    AtIpCore (*IpCoreGet)(AtDevice self, uint8 coreId);

    /* Access supported modules */
    const eAtModule* (*AllSupportedModulesGet)(AtDevice self, uint8 *numModules);
    eBool (*ModuleIsSupported)(AtDevice self, eAtModule moduleId);
    AtModule (*ModuleGet)(AtDevice self, eAtModule moduleId);

    /* Long register accessing methods */
    uint16 (*LongReadOnCore)(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);
    uint16 (*LongWriteOnCore)(AtDevice self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);
    eAtRet (*MemoryTest)(AtDevice self);

    /* Internal functions */
    eAtRet (*Setup)(AtDevice self); /* To create IP Cores and modules */
    AtModule (*ModuleCreate)(AtDevice self, eAtModule moduleId);
    void (*AllModulesCreate)(AtDevice self);
    void (*AllModulesDelete)(AtDevice self);
    void (*DidDeleteAllManagedObjects)(AtDevice self);
    eAtRet (*AllModulesInit)(AtDevice self);
    eAtRet (*AllModulesAsyncInit)(AtDevice self);
    AtIpCore (*IpCoreCreate)(AtDevice self, uint8 coreId);
    eAtRet (*DefaultIpCoreSetup)(AtDevice self);
    eBool (*NeedCheckSSKey)(AtDevice self);
    AtSSKeyChecker (*SSKeyCheckerCreate)(AtDevice self);
    eAtRet (*SSKeyCheck)(AtDevice self);
    char *(*VersionRead)(AtDevice self, char *buffer, uint32 bufferLen);
    AtLongRegisterAccess (*LongRegisterAccessCreate)(AtDevice self, AtModule module);
    eAtRet (*AllCoresTest)(AtDevice self);
    eAtRet (*AllModulesHwResourceCheck)(AtDevice self);
    eBool  (*ShouldCheckHwResource)(AtDevice self);

    /* Simulation */
    uint32 (*SimulationCode)(AtDevice self);
    eBool (*HwReady)(AtDevice self);
    eBool (*ModuleHwReady)(AtDevice self, eAtModule moduleId);
    eBool (*ShouldDisableHwAccessBeforeDeleting)(AtDevice self);
    eBool (*ShouldCacheHalOnChannel)(AtDevice self);

    uint32 (*NumInterruptPins)(AtDevice self);
    AtInterruptPin (*InterruptPinGet)(AtDevice self, uint32 pinId);
    AtInterruptPin (*InterruptPinObjectCreate)(AtDevice self, uint32 pinId);
    eAtTriggerMode (*InterruptPinDefaultTriggerMode)(AtDevice self, AtInterruptPin pin);
    void (*InterruptProcess)(AtDevice self);
    eAtRet (*InterruptRestore)(AtDevice self);
    void (*PeriodicProcess)(AtDevice self, uint32 periodInMs);
    eBool (*ShouldEnableInterruptAfterProcessing)(AtDevice self);

    void (*Debug)(AtDevice self);
    uint32 (*ProductCodeRead)(AtDevice self);
    eBool (*IsEjected)(AtDevice self);
    void (*StatusClear)(AtDevice self);
    AtLongRegisterAccess (*GlobalLongRegisterAccessCreate)(AtDevice self);
    eAtRet (*WarmRestore)(AtDevice self);
    eBool (*WarmRestoreIsSupported)(AtDevice self);
    void (*WarmRestoreFinish)(AtDevice self);
    eBool (*ShouldCheckRegisterAccess)(AtDevice self);
    AtIterator (*AllModulesIteratorCreate)(AtDevice self);
    eBool (*ShouldCheckVersionInSimulation)(AtDevice self);
    AtQuerier (*QuerierGet)(AtDevice self);
    void (*DebuggerFill)(AtDevice self, AtDebugger debugger);

    /* Binders to link channels of relate modules */
    AtEncapBinder (*EncapBinder)(AtDevice self);

    /* SEM */
    AtSemController (*SemControllerObjectCreate)(AtDevice self, uint32 semId);
    eBool (*SemControllerIsSupported)(AtDevice self);
    uint8 (*NumSemControllersGet)(AtDevice self);
    void (*SemControllerSerialize)(AtDevice self, AtCoder encoder);

    /* Sensor */
    AtThermalSensor (*ThermalSensorObjectCreate)(AtDevice self);
    eBool (*ThermalSensorIsSupported)(AtDevice self);
    AtPowerSupplySensor (*PowerSupplySensorObjectCreate)(AtDevice self);
    eBool (*PowerSupplySensorIsSupported)(AtDevice self);

    /* To destroy all of running services */
    eAtRet (*AllServicesDestroy)(AtDevice self);

    eAtRet (*AllCoresReset)(AtDevice self);
    eAtRet (*AllCoresAsyncReset)(AtDevice self);

    /* For diagnostic */
    eAtRet (*DiagnosticModeEnable)(AtDevice self, eBool enable);
    eBool (*DiagnosticModeIsEnabled)(AtDevice self);
    AtUart (*DiagnosticUartGet)(AtDevice self);
    AtUart (*DiagnosticUartObjectCreate)(AtDevice self);

    /* For standby driver */
    AtIterator (*RestoredModulesIteratorCreate)(AtDevice self);

    /* For SERDES manager */
    AtSerdesManager (*SerdesManagerObjectCreate)(AtDevice self);

    /* Role */
    eAtRet (*RoleSet)(AtDevice self, eAtDeviceRole role);
    eAtDeviceRole (*RoleGet)(AtDevice self);
    eBool (*HasRole)(AtDevice self);
    eAtDeviceRole (*AutoRoleGet)(AtDevice self);

    /* Card protection */
    AtDevice (*ParentDeviceGet)(AtDevice self);
    uint8 (*NumSubDevices)(AtDevice self);
    AtDevice (*SubDeviceGet)(AtDevice self, uint8 subDeviceId);
    AtDevice* (*AllSubDevicesGet)(AtDevice self, uint8 *numSubDevices);
    uint16 (*ReadOnDdrOffload)(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);
    uint16 (*WriteOnDdrOffload)(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);
    eAtRet (*Simulate)(AtDevice self, eBool simulated);
    eBool (*IsSimulated)(AtDevice self);

    /* Alarms */
    uint32 (*AlarmGet)(AtDevice self);
    uint32 (*AlarmHistoryGet)(AtDevice self);
    uint32 (*AlarmHistoryClear)(AtDevice self);
    eAtRet (*EventListenerAdd)(AtDevice self, tAtDeviceEventListener *listener);
    eBool  (*AlarmIsSupported)(AtDevice self, uint32 alarmType);
    void (*ListenerDidAdd)(AtDevice self, tAtDeviceEventListener *listener, void *userData);
    uint32 (*SupportedInterruptMasks)(AtDevice self);
    eAtRet (*InterruptMaskSet)(AtDevice self, uint32 alarmMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtDevice self);
    eAtRet (*InterruptEnable)(AtDevice self);
    eAtRet (*InterruptDisable)(AtDevice self);
    eBool (*InterruptIsEnabled)(AtDevice self);
    void  (*DevAlarmInterruptProcess)(AtDevice self);
    /* For standby driver */
    void *(*CacheGet)(AtDevice self);
    uint32 (*CacheSize)(AtDevice self);
    void **(*CacheMemoryAddress)(AtDevice self);
    eBool (*SerdesResetIsSupported)(AtDevice self);
    eBool (*RamIdIsValid)(AtDevice self, uint32 ramId);

    /*ID, support logger object to string to generate CLI in sub device */
    uint32 (*DeviceId)(AtDevice self);
    const char* (*DeviceIdToString)(AtDevice self);
    }tAtDeviceMethods;

/*
 * AT device structure. All of concrete AT must implement all of
 * interface defined in implementation structure
 */
typedef struct tAtDevice
    {
    tAtObject super;
    tAtDeviceMethods *methods;

    uint32 productCode;
    AtDriver driver;
    char version[128];
    eBool resetDone;
    eBool allModulesDidSetup;

    /* Supported modules */
    AtModule sdhModule;
    AtModule pdhModule;
    AtModule encapModule;
    AtModule pppModule;
    AtModule ethModule;
    AtModule ramModule;
    AtModule berModule;
    AtModule pktAnalyzerModule;
    AtModule pwModule;
    AtModule clockModule;
    AtModule prbsModule;
    AtModule apsModule;
    AtModule surModule;
    AtModule xcModule;
    AtModule concateModule;
    AtModule frModule;
    AtModule ptpModule;
    
    /* Binders */
    AtEncapBinder encapBinder;

    /* SEM */
    AtSemController *semControllers;

    /* Sensor */
    AtThermalSensor thermalSensor;
    AtPowerSupplySensor powerSupplySensor;

    /* Serdes manager */
    AtSerdesManager serdesManager;

    /* Cores */
    AtIpCore *allIpCores; /* All IP cores */

    /* SSKey checking */
    AtSSKeyChecker sskeyChecker;

    /* Interrupt PINs */
    AtInterruptPin *interruptPins;
    uint32 numInterruptPins;

    /* User task's period in Ms */
    uint8 isEjected;
    eBool needCheckEjected;

    /* Warm restore */
    eBool inWarmRestore;
    eBool warmRestoreStarted;

    /* Long register access */
    AtLongRegisterAccess defaultGlobalLongRegisterAccess;
    AtLongRegisterAccess installedGlobalLongRegisterAccess;

    /* For simulation */
    eBool isSimulated;
    eBool testbenchEnabled;

    /* Register Audit listener */
    tAtRegisterWriteEventListener *registerListener;
    eBool isDiagnostic;
    eBool diagnosticCheckDisabled;
    AtUart diagUart;

    /* For debugging */
    AtDebugger debugger;
    AtList listeners;
    uint32 targetPlatformProductCode;

    /* Async init */
    uint32 devAsyncResetState;
    uint32 asyncInitState;
    uint32 nextModulePos;
    uint32 previousTimeCount;
    uint32 asyncInitEntranceState;
    tAtOsalCurTime ssKeyStartTime;
    uint32 asyncCurrentCore;
    uint32 asyncInitRemainingUsNeedDelay;

    /* To control deleting process easily */
    eBool deleting;

    /* alarm Listeners */
    AtList allEventListeners;
    AtOsalMutex listenerListMutex; /* Locker to protect list of event listener */
    void *cache;
    }tAtDevice;

typedef eAtRet (*tAtAsyncOperationFunc)(AtObject self);
typedef tAtAsyncOperationFunc (*tAtNextAsyncOperation)(AtObject, uint32 state);

typedef struct tAtDeviceCache
    {
    uint32 interruptMask;
    }tAtDeviceCache;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice AtDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);
eAtRet AtDeviceSetup(AtDevice self);
eBool AtDeviceIsEjected(AtDevice self);
eBool AtDeviceIsDeleting(AtDevice self);
AtLongRegisterAccess AtDeviceLongRegisterAccessCreate(AtDevice self, AtModule module);
AtLongRegisterAccess AtDeviceGlobalLongRegisterAccess(AtDevice self);

eAtRet AtDeviceWillDelete(AtDevice self);
eAtRet AtDeviceAllCoresReset(AtDevice self);
eAtRet AtDeviceAllCoresTest(AtDevice self);
eAtRet AtDeviceAllModulesSetup(AtDevice self);
eAtRet AtDeviceAllCoresActivate(AtDevice self, eBool activate);
eBool AtDeviceAllCoresAreActivated(AtDevice self);
eAtRet AtDeviceModuleReactivate(AtDevice self, uint32 moduleId);
eBool AtDeviceModuleIsActive(AtDevice self, uint32 moduleId);
eBool AtDeviceShouldEnableInterruptAfterProcessing(AtDevice self);

eAtRet AtDeviceIpCoreHalDidSet(AtDevice self, AtIpCore core, AtHal hal);
eAtRet AtDeviceModuleInit(AtDevice self, eAtModule moduleId);
eAtRet AtDeviceModuleAsyncInit(AtDevice self, eAtModule moduleId);
uint8 AtDeviceModuleVersion(AtDevice self, eAtModule moduleId);
eBool AtDeviceModuleHwReady(AtDevice self, eAtModule moduleId);
void AtDeviceLog(AtDevice self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...) AtAttributePrintf(5, 6);
void AtDeviceAllDiagServiceStop(AtDevice self);

eAtRet AtDeviceSimulate(AtDevice self, eBool simulate);
eBool AtDeviceIsSimulated(AtDevice self);
eBool AtDeviceAllFeaturesAvailableInSimulation(AtDevice self);
eAtRet AtDeviceTestbenchEnable(AtDevice self, eBool enabled);
eBool AtDeviceTestbenchIsEnabled(AtDevice self);
uint32 AtDeviceTestbenchDefaultTimeoutMs(void);
eBool AtDeviceInWarmRestore(AtDevice self);
eBool AtDeviceHwReady(AtDevice self);
eBool AtDeviceShouldCheckRegisterAccess(AtDevice self);
eBool AtDeviceAccessible(AtDevice self);
eBool AtDeviceInAccessible(AtDevice self);
eBool AtDeviceShouldPreventReconfigure(AtDevice self);
eBool AtDeviceShouldCacheHalOnChannel(AtDevice self);

uint32 AtDeviceAccessTimeCountSinceLastProfileCheck(AtDevice self, eBool isLogged, const char *file, uint32 line, const char *ObjMsg);
uint32 AtDeviceAccessTimeInMsSinceLastProfileCheck(AtDevice self, eBool isLogged, tAtOsalCurTime *prevTime, const char *file, uint32 line, const char *ObjMsg);

AtEncapBinder AtDeviceEncapBinder(AtDevice self);

eAtRet AtDeviceGlobalLongRegisterAccessSet(AtDevice self, AtLongRegisterAccess registerAccess);
AtLongRegisterAccess AtDeviceInstalledGlobalLongRegisterAccessGet(AtDevice self);

uint32 AtDeviceRestore(AtDevice self);

void AtDeviceShortWriteRegisterListenerCall(AtDevice self, uint32 address, uint32 value, uint8 coreId);
void AtDeviceShortReadRegisterListenerCall(AtDevice self, uint32 address, uint32 value, uint8 coreId);
void AtDeviceLongWriteRegisterListenerCall(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId);
void AtDeviceLongReadRegisterListenerCall(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId);

AtSerdesManager AtDeviceSerdesManagerSetup(AtDevice self);
eAtRet AtDeviceAsyncProcess(AtDevice self, uint32 *fsmState, tAtAsyncOperationFunc (*NextOperation)(AtDevice, uint32 state));
eAtRet AtDeviceObjectAsyncProcess(AtDevice self, AtObject object, uint32 *fsmState, tAtNextAsyncOperation nextOp);

/* For debugging */
void AtDeviceReadNotify(AtDevice self, uint32 address, uint32 value, uint8 coreId);
void AtDeviceWriteNotify(AtDevice self, uint32 address, uint32 value, uint8 coreId);
void AtDeviceLongReadNotify(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId);
void AtDeviceLongWriteNotify(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId);
void AtDeviceWriteOnCore(AtDevice self, uint32 address, uint32 value, uint8 coreId);
eAtRet AtDeviceDebuggerFill(AtDevice self, AtDebugger debugger);
void AtDeviceRoleChangeNotify(AtDevice self, uint32 role);

/* To test one product on another platform */
void AtDevicePlatformSet(AtDevice self, uint32 targetPlatformProductCode);
uint32 AtDevicePlatformGet(AtDevice self);

/* Async Init */
eAtRet AtDeviceAsyncResetIfNecessary(AtDevice self);
eBool AtDeviceShouldResetBeforeInitializing(AtDevice self);
void AtDeviceAsyncInitNeedFixDelayTimeSet(AtDevice self, uint32 delayInUs);
eBool AtDeviceAsyncRetValIsInState(eAtRet ret);

/* To check HW resource */
eAtRet AtDeviceAllModulesHwResourceCheck(AtDevice self);
eBool AtDeviceShouldCheckHwResource(AtDevice self);

/* Alarm/interrupt */
uint32 AtDeviceSupportedInterruptMasks(AtDevice self);
void *AtDeviceCacheGet(AtDevice self);
void AtDeviceAllAlarmListenersCall(AtDevice self, uint32 changedAlarms, uint32 currentStatus);
eBool AtDeviceRamIdIsValid(AtDevice self, uint32 ramId);

uint32 AtDeviceIdGet(AtDevice self);
const char* AtDeviceIdToString(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEVICEINTERNAL_H_ */

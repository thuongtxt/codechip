/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtModuleInternal.h
 * 
 * Created Date: Jul 26, 2012
 *
 * Author      : namnn
 * 
 * Description : Module abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEINTERNAL_H_
#define _ATMODULEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "AtDriverInternal.h"
#include "AtModule.h"
#include "AtIterator.h"
#include "AtLogger.h"
#include "AtDeviceInternal.h"
#include "AtDebugger.h"
#include "../ram/AtInternalRamInternal.h"
#include "../diag/querier/common/AtQueriers.h"
#include "interrupt/AtInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtMaxNumOfHoldRegister 16

/*--------------------------- Macros -----------------------------------------*/
#define mModuleHwRead(self, address)                                           \
    AtModuleHwRead((AtModule)self, (uint32)(address), NULL)
#define mModuleHwWrite(self, address, value)                                   \
    AtModuleHwWrite((AtModule)self, (uint32)address, (uint32)value, NULL)

#define mModuleHwReadByHal(self, address, hal)                                 \
    AtModuleHwRead((AtModule)self, (uint32)(address), hal)
#define mModuleHwWriteByHal(self, address, value, hal)                         \
    AtModuleHwWrite((AtModule)self, (uint32)address, (uint32)value, hal)

#define mModuleHwLongRead(self, address, dataBuffer, bufferLen, core)          \
    AtModuleHwLongReadOnCore((AtModule)self, (uint32)address, dataBuffer, bufferLen, core)
#define mModuleHwLongWrite(self, address, dataBuffer, bufferLen, core)         \
    AtModuleHwLongWriteOnCore((AtModule)self, (uint32)address, dataBuffer, bufferLen, core)

#define mModuleAttributeSet(method, value)                                     \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, value);                          \
        AtModuleLog((AtModule)self,  (ret == cAtOk) ? cAtLogLevelInfo : cAtLogLevelCritical, \
                     AtSourceLocation, NULL,                                 \
                     "%s(%u) = %s\r\n",                                        \
                     AtFunction, value, AtRet2String(ret));                  \
        }                                                                      \
    else                                                                       \
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, NULL, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return ret;                                                                \
    }

#define mModuleAttributeGet(method, returnType, invalidValue)                  \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value = mMethodsGet(self)->method(self);                    \
        AtModuleLog((AtModule)self, cAtLogLevelInfo, AtSourceLocation, NULL, "%s() = %u\r\n", \
                     AtFunction, value);                                     \
        return value;                                                          \
        }                                                                      \
                                                                               \
    AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical,  AtSourceLocation, NULL, "%s is called with NULL object\r\n", AtFunction); \
                                                                               \
    return invalidValue;                                                       \
    }

#define mModuleErrorLog(self, returnCode, message)               \
    {                                                                          \
    if (returnCode != cAtOk)                                                   \
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "%s call %s: error code = %d\r\n", AtFunction, message, returnCode); \
    }

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleEventListenerWrapper * AtModuleEventListenerWrapper;

/* Methods */
typedef struct tAtModuleMethods
    {
    eAtModule (*TypeGet)(AtModule self);
    const char *(*TypeString)(AtModule self);
    const char *(*CapacityDescription)(AtModule self);
    AtDevice (*DeviceGet)(AtModule self);
    eAtRet (*Init)(AtModule self);
    uint32 (*Restore)(AtModule self);
    eAtRet (*AsyncInit)(AtModule self);
    eBool (*HwIsReady)(AtModule self);

    /* Activation */
    eAtRet (*Activate)(AtModule self);
    eAtRet (*Deactivate)(AtModule self);
    eBool (*IsActive)(AtModule self);

    /* For standby module */
    void *(*CacheGet)(AtModule self);
    uint32 (*CacheSize)(AtModule self);
    void **(*CacheMemoryAddress)(AtModule self);

    /* Internal methods */
    eAtRet (*Setup)(AtModule self);       /* To create all managed objects */
    eAtRet (*HwResourceCheck)(AtModule self);
    eBool (*ShouldSetupWhenNoHal)(AtModule self);

    /* To access registers of a module */
    eBool (*HasRegister)(AtModule self, uint32 localAddress);
    uint32 (*HwRead)(AtModule self, uint32 localAddress, AtHal hal);
    void (*HwWrite)(AtModule self, uint32 localAddress, uint32 value, AtHal hal);

    /* To access long register of a module on a specified core */
    uint32 *(*HoldRegistersGet)(AtModule self, uint16 *numberOfHoldRegisters);
    uint32 *(*HoldReadRegistersGet)(AtModule self, uint16 *numberOfHoldRegisters);
    uint32 *(*HoldWriteRegistersGet)(AtModule self, uint16 *numberOfHoldRegisters);
    uint16 (*HwLongReadOnCore)(AtModule self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
    uint16 (*HwLongWriteOnCore)(AtModule self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
    AtLongRegisterAccess (*LongRegisterAccessCreate)(AtModule self);
    AtLongRegisterAccess (*LongRegisterAccess)(AtModule self, uint32 localAddress);

    /* Memory testing */
    eBool (*MemoryCanBeTested)(AtModule self);
    eAtRet (*MemoryTest)(AtModule self);
    AtIterator (*RegisterIteratorCreate)(AtModule self);

    /* Interrupt enable */
    eAtRet (*InterruptEnable)(AtModule self, eBool enable);
    eBool (*InterruptIsEnabled)(AtModule self);

    /* Locking */
    eAtRet (*Lock)(AtModule self);
    eAtRet (*UnLock)(AtModule self);

    /* Interrupt process */
    void (*InterruptProcess)(AtModule self, uint32 globalStatus, AtIpCore ipCore);
    void (*InterruptOnIpCoreEnable)(AtModule self, eBool enable, AtIpCore ipCore);
    AtInterruptManager (*InterruptManagerCreate)(AtModule self, uint32 managerIndex);
    uint32 (*NumInterruptManagers)(AtModule self); /* The number of interrupt managers is equal to the number of interrupt trees of that module. */

    /* Polling */
    uint32 (*PeriodicProcess)(AtModule self, uint32 periodInMs);
    uint32 (*RecommendedPeriodInMs)(AtModule self);
    uint32 (*ForcePointerPeriodicProcess)(AtModule self, uint32 periodInMs);

    /* OAM process */
    void (*OamProcessOnCore)(AtModule self, AtIpCore ipCore, uint16 numServedOamsAtOneTime);

	/* To handle interrupt context */
    void (*IsrContextEnter)(AtModule self);
    void (*IsrContextExit)(AtModule self);

    /* For debugging purpose, to show all of information of a module */
    void (*StatusClear)(AtModule self);
    eAtRet (*Debug)(AtModule self);
    eAtRet (*DebugEnable)(AtModule self, eBool enable);
    eBool (*DebugIsEnabled)(AtModule self);
    eAtRet (*WarmRestore)(AtModule self);

    /* Debugger */
    AtDebugger (*DebuggerCreate)(AtModule self);
    AtDebugger (*DebuggerObjectCreate)(AtModule self);
    void (*DebuggerEntriesFill)(AtModule self, AtDebugger debugger);
    AtQuerier (*QuerierGet)(AtModule self);
    
    /* To loopback at module level */
    eAtRet (*LoopbackSet)(AtModule self, uint8 loopbackMode);
    uint8 (*LoopbackGet)(AtModule self);
    eBool (*LoopbackIsSupported)(AtModule self, uint8 loopbackMode);

    /* To destroy all of running services */
    eAtRet (*AllServicesDestroy)(AtModule self);
    eAtRet (*AllChannelsInterruptDisable)(AtModule self);

    /* Listener */
    AtModuleEventListenerWrapper (*ListenerWrapperCreate)(AtModule self, void* listener, void* userData);
    eBool (*EventListenersAreIdentical)(AtModule self, AtModuleEventListenerWrapper registeredListener, void* listener);

    /* Alarms */
    uint32 (*AlarmGet)(AtModule self);
    uint32 (*AlarmHistoryGet)(AtModule self);
    uint32 (*AlarmHistoryClear)(AtModule self);
    eBool  (*AlarmIsSupported)(AtModule self, uint32 alarmType);

    /* Interrupt mask */
    uint32 (*SupportedInterruptMasks)(AtModule self);
    eAtRet (*InterruptMaskSet)(AtModule self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtModule self);

    /* Internal RAM management. */
    uint32 (*NumInternalRamsGet)(AtModule self);
    const char **(*AllInternalRamsDescription)(AtModule self, uint32 *numRams);
    AtInternalRam (*InternalRamCreate)(AtModule self, uint32 ramId, uint32 localRamId);
    eBool (*InternalRamIsReserved)(AtModule self, AtInternalRam ram);

    /* For debugging purpose and should not merge to main branch. This may affect performance */
    eBool (*RegisterIsInRange)(AtModule self, uint32 address);
    void (*WillAccessRegister)(AtModule self, uint32 address);
    void (*AllChannelsListenedDefectClear)(AtModule self);

    /* Role input */
    eBool (*HasRole)(AtModule self);
    eAtRet (*RoleSet)(AtModule self, eAtDeviceRole role);
    eAtDeviceRole (*RoleGet)(AtModule self);
    eAtDeviceRole (*RoleInputStatus)(AtModule self);
    eAtRet (*RoleActiveForce)(AtModule self, eBool forced);
    eBool (*RoleActiveIsForced)(AtModule self);
    eAtRet (*RoleInputEnable)(AtModule self, eBool enable);
    eBool (*RoleInputIsEnabled)(AtModule self);
    }tAtModuleMethods;

/* Module class */
typedef struct tAtModule
    {
    tAtObject super;
    const tAtModuleMethods *methods;

    /* Private data */
    eAtModule moduleId;
    AtDevice device;     /* Device that this module belongs to */
    uint8 defaultCoreId; /* Default core ID used when module belongs to only one core */
    AtLongRegisterAccess moduleLongRegisterAccess;
    AtLongRegisterAccess longRegisterAccess; /* Cache for fast accessing */

    /* To handle interrupt context */
    uint8 isIsrContext;

    /* Locker */
    AtOsalMutex mutex;
    eBool locked;
    AtList allEventListeners;

    /* For debugging */
    eBool debugEnabled;

    /* Internal RAM */
    AtInternalRam* internalRams;

    /* Interrupt managers.  */
    AtInterruptManager* interruptManagers;
    }tAtModule;

typedef struct tAtModuleEventListenerWrapper
    {
    tAtObject super;
    void * userData;
    AtModule module;
    }tAtModuleEventListenerWrapper;

typedef struct tAtModuleAlarmListenerWrapper * AtModuleAlarmListenerWrapper;
typedef struct tAtModuleAlarmListenerWrapper
    {
    tAtModuleEventListenerWrapper super;
    tAtModuleAlarmListener listener;
    }tAtModuleAlarmListenerWrapper;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule AtModuleObjectInit(AtModule module, eAtModule moduleId, AtDevice device);

eAtRet AtModuleSetup(AtModule module);
AtIterator AtModuleRegisterIteratorCreate(AtModule self);
eAtRet AtModuleReactivate(AtModule self);
eAtRet AtModuleActivate(AtModule self);
eAtRet AtModuleDeactivate(AtModule self);
eBool AtModuleIsActive(AtModule self);
eAtRet AtModuleLoopbackShow(AtModule self);
eBool AtModuleHwIsReady(AtModule self);

eAtRet AtModuleAsyncInit(AtModule self);

/* Default core is used for module that belongs to only core. All of objects it
 * manages refer to this default core */
eAtRet AtModuleDefaultCoreSet(AtModule self, uint8 coreId);
uint8 AtModuleDefaultCoreGet(AtModule self);

/* Helper */
eAtRet HelperAtModuleMemoryTest(AtModule self);
eAtRet AtModulesMemoryTest(AtIterator moduleIterator);
void AtModuleIsrContextExit(AtModule self);
void AtModuleIsrContextEnter(AtModule self);
uint32 *AtModuleHoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters);
uint32 *AtModuleHoldReadRegistersGet(AtModule self, uint16 *numberOfHoldRegisters);
uint32 *AtModuleHoldWriteRegistersGet(AtModule self, uint16 *numberOfHoldRegisters);
eAtRet AtModuleAllServicesDestroy(AtModule self);

/* Version */
uint8 AtModuleVersion(AtModule self);
void AtModuleLog(AtModule self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...) AtAttributePrintf(5, 6);

/* Access hardware */
uint32 AtModuleHwRead(AtModule self, uint32 localAddress, AtHal hal);
void AtModuleHwWrite(AtModule self, uint32 localAddress, uint32 value, AtHal hal);
uint16 AtModuleHwLongReadOnCore(AtModule self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
uint16 AtModuleHwLongWriteOnCore(AtModule self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
AtLongRegisterAccess AtModuleLongRegisterAccess(AtModule self, uint32 localAddress);
eBool AtModuleRegisterIsInRange(AtModule self, uint32 address);

/* Internal RAM */
uint32 AtModuleNumInternalRamsGet(AtModule self);
AtInternalRam AtModuleInternalRamGet(AtModule self, uint32 ramId, uint32 localRamId);
const char **AtModuleAllInternalRamsDescription(AtModule self, uint32 *numRams);
eBool AtModuleInternalRamIsReserved(AtModule self, AtInternalRam ram);

/* Warm restore */
eAtRet AtModuleWarmRestore(AtModule self);
eBool AtModuleDeviceInWarmRestore(AtModule self);
eBool AtModuleAccessible(AtModule self);
eBool AtModuleInAccessible(AtModule self);

/* For standby driver */
uint32 AtModuleRestore(AtModule self);
void *AtModuleCacheGet(AtModule self);

AtModuleEventListenerWrapper AtModuleEventListenerWrapperObjectInit(AtModuleEventListenerWrapper self, AtModule module, void* listener, void* userData);
void AtModuleAllAlarmListenersCall(AtModule self, uint32 changedAlarms, uint32 currentStatus);
/* Interrupt managers.
 * One module usually contains one interrupt tree. Some module can contains more
 * than one interrupt tree. And they are identified by index. */
AtInterruptManager AtModuleInterruptManagerGet(AtModule self);
AtInterruptManager AtModuleInterruptManagerAtIndexGet(AtModule self, uint32 index);

void AtModuleInterruptProcess(AtModule self, uint32 globalIntr, AtIpCore ipCore);
eAtRet AtModuleHwResourceCheck(AtModule self);
void AtModuleResourceDiminishedLog(AtModule self, const char* name, uint32 free, uint32 max);

void AtModuleAllChannelsListenedDefectClear(AtModule self);
uint32 AtModuleSupportedInterruptMasks(AtModule self);

/* Input role */
void AtModuleRoleDebug(AtModule self);
eAtRet AtModuleRoleSet(AtModule self, eAtDeviceRole role);
eAtDeviceRole AtModuleRoleGet(AtModule self);
eBool AtModuleHasRole(AtModule self);
eAtRet AtModuleRoleActiveForce(AtModule self, eBool forced);
eBool AtModuleRoleActiveIsForced(AtModule self);
eAtRet AtModuleRoleInputEnable(AtModule self, eBool enable);
eBool AtModuleRoleInputIsEnabled(AtModule self);
eAtDeviceRole AtModuleRoleInputStatus(AtModule self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEINTERNAL_H_ */

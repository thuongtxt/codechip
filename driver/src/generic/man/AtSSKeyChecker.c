/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtSSKeyChecker.c
 *
 * Created Date: Sep 18, 2013
 *
 * Description : SSKey validator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSSKeyCheckerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidRegAddress 0xCAFECAFE

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSSKeyChecker)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSSKeyCheckerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtSSKeyChecker);
    }

static uint32 StickyAddress(AtSSKeyChecker self)
    {
	AtUnused(self);
    /* Concrete product must override */
    return cInvalidRegAddress;
    }

static uint32 CodeMsbAddress(AtSSKeyChecker self)
    {
	AtUnused(self);
    /* Concrete product must override */
    return cInvalidRegAddress;
    }

static uint32 CodeLsbAddress(AtSSKeyChecker self)
    {
	AtUnused(self);
    /* Concrete product must override */
    return cInvalidRegAddress;
    }

static uint32 CodeExpectedMsb(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x0;
    }

static uint32 CodeExpectedLsb(AtSSKeyChecker self)
    {
	AtUnused(self);
    return 0x0;
    }

static eBool KeyExist(AtSSKeyChecker self)
    {
    return (mMethodsGet(self)->KeyMode(self) == cAtSSKeyModeRealKey) ? cAtTrue : cAtFalse;
    }

static eBool CodeMatch(AtSSKeyChecker self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self->device, 0);

    /* Cannot check if key code match if key does exist */
    if (!KeyExist(self))
        return cAtTrue;

    /* Check if code match */
    if ((AtHalRead(hal, mMethodsGet(self)->CodeMsbAddress(self)) == mMethodsGet(self)->CodeExpectedMsb(self)) &&
        (AtHalRead(hal, mMethodsGet(self)->CodeLsbAddress(self)) == mMethodsGet(self)->CodeExpectedLsb(self)))
        return cAtTrue;

    return cAtFalse;
    }

/* As recommended by HW:
 *
 * 1. Just need to check the SSKey 's sticky when sticky reports fail, we will
 *    consider product code for debugging only
 *
 * 2. SSKey sticky is always on core 0
 */
static eAtRet Check(AtSSKeyChecker self)
    {
    const uint8 cCheckingTime = 20;
    AtHal hal = AtDeviceIpCoreHalGet(self->device, 0);
    uint32 address = mMethodsGet(self)->StickyAddress(self);
    uint8 i;

    if (!CodeMatch(self))
        return cAtErrorInvalidSSkey;

    /* Ignore the first time */
    AtHalRead(hal, address);

    /* Read many times and make sure that the key is stable (0) */
    for (i = 0; i < cCheckingTime; i++)
        {
        if (AtHalRead(hal, address) != 0)
            return cAtErrorInvalidSSkey;
        }

    return cAtOk;
    }

static eAtSSKeyMode KeyMode(AtSSKeyChecker self)
    {
	AtUnused(self);
    /* All of product should use real key when startup any project */
    return cAtSSKeyModeRealKey;
    }

static void MethodsInit(AtSSKeyChecker self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StickyAddress);
        mMethodOverride(m_methods, CodeMsbAddress);
        mMethodOverride(m_methods, CodeLsbAddress);
        mMethodOverride(m_methods, CodeExpectedMsb);
        mMethodOverride(m_methods, CodeExpectedLsb);
        mMethodOverride(m_methods, Check);
        mMethodOverride(m_methods, KeyMode);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtSSKeyChecker self)
    {
	AtUnused(self);
    /* May be added soon */
    }

AtSSKeyChecker AtSSKeyCheckerObjectInit(AtSSKeyChecker self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->device = device;

    return self;
    }

eAtRet AtSSKeyCheckerCheck(AtSSKeyChecker self)
    {
    if (self)
        return mMethodsGet(self)->Check(self);
    return cAtError;
    }

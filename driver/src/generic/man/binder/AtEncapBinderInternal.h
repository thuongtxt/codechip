/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtEncapBinderInternal.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : Encapsulation binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATENCAPBINDERINTERNAL_H_
#define _ATENCAPBINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../AtDriverInternal.h" /* For shared OSAL */
#include "AtEncapBinder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEncapBinderMethods
    {
    eAtRet (*BindVc1xToEncapChannel)(AtEncapBinder self, AtChannel vc1x, AtEncapChannel encapChannel);
    eAtRet (*BindHoVcToEncapChannel)(AtEncapBinder self, AtChannel hoVc, AtEncapChannel encapChannel);
    eAtRet (*BindTu3VcToEncapChannel)(AtEncapBinder self, AtChannel tu3Vc, AtEncapChannel encapChannel);
    eAtRet (*BindDe3ToEncapChannel)(AtEncapBinder self, AtChannel de3, AtEncapChannel encapChannel);
    eAtRet (*BindDe1ToEncapChannel)(AtEncapBinder self, AtChannel de1, AtEncapChannel encapChannel);
    eAtRet (*BindNxDs0ToEncapChannel)(AtEncapBinder self, AtChannel nxDs0, AtEncapChannel encapChannel);
    eAtRet (*BindConcateGroupToEncapChannel)(AtEncapBinder self, AtChannel concateGroup, AtEncapChannel encapChannel);

    uint32 (*HoVcBoundEncapHwIdGet)(AtEncapBinder self, AtChannel hoVc);
    uint32 (*Vc1xBoundEncapHwIdGet)(AtEncapBinder self, AtChannel vc1x);
    uint32 (*De1BoundEncapHwIdGet)(AtEncapBinder self, AtChannel de1);
    uint32 (*De3BoundEncapHwIdGet)(AtEncapBinder self, AtChannel de3);
    uint32 (*NxDs0BoundEncapHwIdGet)(AtEncapBinder self, AtChannel nxDs0);

    eAtRet (*AugEncapChannelRestore)(AtEncapBinder self, AtChannel aug, AtEncapChannel encapChannel);
    eAtRet (*De1EncapChannelRestore)(AtEncapBinder self, AtChannel de1, AtEncapChannel encapChannel);
    eAtRet (*EncapChannelPhysicalChannelSet)(AtEncapBinder self, AtEncapChannel encapChannel, AtChannel physicalChannel);

    tDe1Backup* (*De1EncapBackup)(AtEncapBinder self, AtPdhChannel de1);
    eAtRet (*De1EncapRestore)(AtEncapBinder self, AtPdhChannel de1, tDe1Backup *backup);

    /* For dynamic module encap */
    uint32 (*De1EncapHwIdAllocate)(AtEncapBinder self, AtPdhDe1 de1);
    uint32 (*De3EncapHwIdAllocate)(AtEncapBinder self, AtPdhDe3 de3);
    uint32 (*NxDS0EncapHwIdAllocate)(AtEncapBinder self, AtPdhNxDS0 ds0);
    uint32 (*AuVcEncapHwIdAllocate)(AtEncapBinder self, AtSdhVc auvc);
    uint32 (*Tu3VcEncapHwIdAllocate)(AtEncapBinder self, AtSdhVc tu3vc);
    uint32 (*Vc1xEncapHwIdAllocate)(AtEncapBinder self, AtSdhVc vc1x);
    void (*EncapHwIdDeallocate)(AtEncapBinder self, uint32 hwId);
    }tAtEncapBinderMethods;

typedef struct tAtEncapBinder
    {
    tAtObject super;
    const tAtEncapBinderMethods *methods;

    AtDevice device;
    }tAtEncapBinder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEncapBinder AtEncapBinderObjectInit(AtEncapBinder self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATENCAPBINDERINTERNAL_H_ */


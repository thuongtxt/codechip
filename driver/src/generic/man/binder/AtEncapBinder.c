/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtEncapBinder.c
 *
 * Created Date: Apr 5, 2015
 *
 * Description : Encapsulation binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "AtEncapBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEncapBinderMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet BindVc1xToEncapChannel(AtEncapBinder self, AtChannel vc1x, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(vc1x);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet BindHoVcToEncapChannel(AtEncapBinder self, AtChannel hoVc, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(hoVc);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet BindTu3VcToEncapChannel(AtEncapBinder self, AtChannel tu3Vc, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(tu3Vc);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet BindDe3ToEncapChannel(AtEncapBinder self, AtChannel de3, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(de3);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet BindDe1ToEncapChannel(AtEncapBinder self, AtChannel de1, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(de1);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet BindNxDs0ToEncapChannel(AtEncapBinder self, AtChannel nxDs0, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(nxDs0);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet BindConcateGroupToEncapChannel(AtEncapBinder self, AtChannel concateGroup, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(concateGroup);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet AugEncapChannelRestore(AtEncapBinder self, AtChannel aug, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(aug);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet De1EncapChannelRestore(AtEncapBinder self, AtChannel de1, AtEncapChannel encapChannel)
    {
    AtUnused(self);
    AtUnused(de1);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet EncapChannelPhysicalChannelSet(AtEncapBinder self, AtEncapChannel encapChannel, AtChannel physicalChannel)
    {
    AtUnused(self);
    AtUnused(physicalChannel);
    AtUnused(encapChannel);
    return cAtErrorNotImplemented;
    }

static tDe1Backup* De1EncapBackup(AtEncapBinder self, AtPdhChannel de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return NULL;
    }

static eAtRet De1EncapRestore(AtEncapBinder self, AtPdhChannel de1, tDe1Backup *backup)
    {
    AtUnused(self);
    AtUnused(de1);
    AtUnused(backup);
    return cAtErrorNotImplemented;
    }

static uint32 De1EncapHwIdAllocate(AtEncapBinder self, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cInvalidUint32;
    }

static uint32 De3EncapHwIdAllocate(AtEncapBinder self, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return cInvalidUint32;
    }

static uint32 NxDS0EncapHwIdAllocate(AtEncapBinder self, AtPdhNxDS0 ds0)
    {
    AtUnused(self);
    AtUnused(ds0);
    return cInvalidUint32;
    }

static uint32 AuVcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc auvc)
    {
    AtUnused(self);
    AtUnused(auvc);
    return cInvalidUint32;
    }

static uint32 Tu3VcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc tu3vc)
    {
    AtUnused(self);
    AtUnused(tu3vc);
    return cInvalidUint32;
    }

static uint32 Vc1xEncapHwIdAllocate(AtEncapBinder self, AtSdhVc vc1x)
    {
    AtUnused(self);
    AtUnused(vc1x);
    return cInvalidUint32;
    }

static void EncapHwIdDeallocate(AtEncapBinder self, uint32 hwId)
    {
    AtUnused(self);
    AtUnused(hwId);
    }

static uint32 HoVcBoundEncapHwIdGet(AtEncapBinder self, AtChannel hoVc)
    {
    AtUnused(self);
    AtUnused(hoVc);
    return cInvalidUint32;
    }

static uint32 Vc1xBoundEncapHwIdGet(AtEncapBinder self, AtChannel vc1x)
    {
    AtUnused(self);
    AtUnused(vc1x);
    return cInvalidUint32;
    }

static uint32 De1BoundEncapHwIdGet(AtEncapBinder self, AtChannel de1)
    {
    AtUnused(self);
    AtUnused(de1);
    return cInvalidUint32;
    }

static uint32 De3BoundEncapHwIdGet(AtEncapBinder self, AtChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return cInvalidUint32;
    }

static uint32 NxDs0BoundEncapHwIdGet(AtEncapBinder self, AtChannel nxDs0)
    {
    AtUnused(self);
    AtUnused(nxDs0);
    return cInvalidUint32;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtEncapBinder object = (AtEncapBinder)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(device);
    }

static void OverrideAtObject(AtEncapBinder self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtEncapBinder self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtEncapBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BindVc1xToEncapChannel);
        mMethodOverride(m_methods, BindHoVcToEncapChannel);
        mMethodOverride(m_methods, BindTu3VcToEncapChannel);
        mMethodOverride(m_methods, BindDe3ToEncapChannel);
        mMethodOverride(m_methods, BindDe1ToEncapChannel);
        mMethodOverride(m_methods, BindNxDs0ToEncapChannel);
        mMethodOverride(m_methods, BindConcateGroupToEncapChannel);
        mMethodOverride(m_methods, AugEncapChannelRestore);
        mMethodOverride(m_methods, De1EncapChannelRestore);
        mMethodOverride(m_methods, EncapChannelPhysicalChannelSet);
        mMethodOverride(m_methods, De1EncapBackup);
        mMethodOverride(m_methods, De1EncapRestore);
        mMethodOverride(m_methods, De1EncapHwIdAllocate);
        mMethodOverride(m_methods, De3EncapHwIdAllocate);
        mMethodOverride(m_methods, NxDS0EncapHwIdAllocate);
        mMethodOverride(m_methods, AuVcEncapHwIdAllocate);
        mMethodOverride(m_methods, Tu3VcEncapHwIdAllocate);
        mMethodOverride(m_methods, Vc1xEncapHwIdAllocate);
        mMethodOverride(m_methods, EncapHwIdDeallocate);
        mMethodOverride(m_methods, HoVcBoundEncapHwIdGet);
        mMethodOverride(m_methods, Vc1xBoundEncapHwIdGet);
        mMethodOverride(m_methods, De1BoundEncapHwIdGet);
        mMethodOverride(m_methods, De3BoundEncapHwIdGet);
        mMethodOverride(m_methods, NxDs0BoundEncapHwIdGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEncapBinder);
    }

AtEncapBinder AtEncapBinderObjectInit(AtEncapBinder self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    self->device = device;

    return self;
    }

eAtRet AtEncapBinderBindVc1xToEncapChannel(AtEncapBinder self, AtChannel vc1x, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindVc1xToEncapChannel(self, vc1x, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderBindHoVcToEncapChannel(AtEncapBinder self, AtChannel hoVc, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindHoVcToEncapChannel(self, hoVc, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderBindTu3VcToEncapChannel(AtEncapBinder self, AtChannel tu3Vc, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindTu3VcToEncapChannel(self, tu3Vc, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderBindDe3ToEncapChannel(AtEncapBinder self, AtChannel de3, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindDe3ToEncapChannel(self, de3, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderBindDe1ToEncapChannel(AtEncapBinder self, AtChannel de1, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindDe1ToEncapChannel(self, de1, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderBindNxDs0ToEncapChannel(AtEncapBinder self, AtChannel nxDs0, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindNxDs0ToEncapChannel(self, nxDs0, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderBindConcateGroupToEncapChannel(AtEncapBinder self, AtChannel concateGroup, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindConcateGroupToEncapChannel(self, concateGroup, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderAugEncapChannelRestore(AtEncapBinder self, AtChannel aug, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->AugEncapChannelRestore(self, aug, encapChannel);
    return cAtError;
    }

eAtRet AtEncapBinderEncapChannelPhysicalChannelSet(AtEncapBinder self, AtEncapChannel encapChannel, AtChannel physicalChannel)
    {
    if (self)
        return mMethodsGet(self)->EncapChannelPhysicalChannelSet(self, encapChannel, physicalChannel);
    return cAtError;
    }

tDe1Backup* AtEncapBinderDe1EncapBackup(AtEncapBinder self, AtPdhChannel de1)
    {
    if (self)
        return mMethodsGet(self)->De1EncapBackup(self, de1);
    return NULL;
    }

eAtRet AtEncapBinderDe1EncapRestore(AtEncapBinder self, AtPdhChannel de1, tDe1Backup *backup)
    {
    if (self)
        return mMethodsGet(self)->De1EncapRestore(self, de1, backup);
    return cAtErrorNullPointer;
    }

uint32 AtEncapBinderDe1EncapHwIdAllocate(AtEncapBinder self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->De1EncapHwIdAllocate(self, de1);
    return cInvalidUint32;
    }

uint32 AtEncapBinderDe3EncapHwIdAllocate(AtEncapBinder self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->De3EncapHwIdAllocate(self, de3);
    return cInvalidUint32;
    }

uint32 AtEncapBinderNxDS0EncapHwIdAllocate(AtEncapBinder self, AtPdhNxDS0 ds0)
    {
    if (self)
        return mMethodsGet(self)->NxDS0EncapHwIdAllocate(self, ds0);
    return cInvalidUint32;
    }

uint32 AtEncapBinderAuVcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc auvc)
    {
    if (self)
        return mMethodsGet(self)->AuVcEncapHwIdAllocate(self, auvc);
    return cInvalidUint32;
    }

uint32 AtEncapBinderTu3VcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc tu3vc)
    {
    if (self)
        return mMethodsGet(self)->Tu3VcEncapHwIdAllocate(self, tu3vc);
    return cInvalidUint32;
    }

uint32 AtEncapBinderVc1xEncapHwIdAllocate(AtEncapBinder self, AtSdhVc vc1x)
    {
    if (self)
        return mMethodsGet(self)->Vc1xEncapHwIdAllocate(self, vc1x);
    return cInvalidUint32;
    }

void AtEncapBinderEncapHwIdDeallocate(AtEncapBinder self, uint32 hwId)
    {
    if (self)
        mMethodsGet(self)->EncapHwIdDeallocate(self, hwId);
    }

uint32 AtEncapBinderHoVcBoundEncapHwIdGet(AtEncapBinder self, AtChannel hoVc)
    {
    if (self)
        return mMethodsGet(self)->HoVcBoundEncapHwIdGet(self, hoVc);
    return cInvalidUint32;
    }

uint32 AtEncapBinderVc1xBoundEncapHwIdGet(AtEncapBinder self, AtChannel vc1x)
    {
    if (self)
        return mMethodsGet(self)->Vc1xBoundEncapHwIdGet(self, vc1x);
    return cInvalidUint32;
    }

uint32 AtEncapBinderDe1BoundEncapHwIdGet(AtEncapBinder self, AtChannel de1)
    {
    if (self)
        return mMethodsGet(self)->De1BoundEncapHwIdGet(self, de1);
    return cInvalidUint32;
    }

uint32 AtEncapBinderDe3BoundEncapHwIdGet(AtEncapBinder self, AtChannel de3)
    {
    if (self)
        return mMethodsGet(self)->De3BoundEncapHwIdGet(self, de3);
    return cInvalidUint32;
    }

uint32 AtEncapBinderNxDs0BoundEncapHwIdGet(AtEncapBinder self, AtChannel nxDs0)
    {
    if (self)
        return mMethodsGet(self)->NxDs0BoundEncapHwIdGet(self, nxDs0);
    return cInvalidUint32;
    }

AtDevice AtEncapBinderDeviceGet(AtEncapBinder self)
    {
    if (self)
        return self->device;

    return NULL;
    }

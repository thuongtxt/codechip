/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtEncapBinder.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : Encapsulation binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATENCAPBINDER_H_
#define _ATENCAPBINDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtRet.h"
#include "AtChannelClasses.h"
#include "AtModulePdh.h"
#include "AtModuleEncap.h"
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEncapBinder * AtEncapBinder;

/* To store binding information */
typedef struct tDe1Backup
    {
    uint32 masks[32]; /* In-used NxDs0s */
    uint8 numNxDs0s;  /* Number of NxDs0 groups  */
    AtChannel nxDs0BoundChannels[32]; /* Bound channels of all NxDs0 groups */
    AtChannel de1BoundChannel; /* Bound channel of DE1 itself */
    uint32 enabledPwsMask;     /* Enabled PWs */
    uint32 enabledPrbsMask;    /* Enabled PRBS engines */
    }tDe1Backup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtEncapBinderBindVc1xToEncapChannel(AtEncapBinder self, AtChannel vc1x, AtEncapChannel encapChannel);
eAtRet AtEncapBinderBindHoVcToEncapChannel(AtEncapBinder self, AtChannel hoVc, AtEncapChannel encapChannel);
eAtRet AtEncapBinderBindTu3VcToEncapChannel(AtEncapBinder self, AtChannel tu3Vc, AtEncapChannel encapChannel);
eAtRet AtEncapBinderBindDe3ToEncapChannel(AtEncapBinder self, AtChannel de3, AtEncapChannel encapChannel);
eAtRet AtEncapBinderBindDe1ToEncapChannel(AtEncapBinder self, AtChannel de1, AtEncapChannel encapChannel);
eAtRet AtEncapBinderBindNxDs0ToEncapChannel(AtEncapBinder self, AtChannel nxDs0, AtEncapChannel encapChannel);
eAtRet AtEncapBinderBindConcateGroupToEncapChannel(AtEncapBinder self, AtChannel concateGroup, AtEncapChannel encapChannel);

eAtRet AtEncapBinderAugEncapChannelRestore(AtEncapBinder self, AtChannel aug, AtEncapChannel encapChannel);
eAtRet AtEncapBinderEncapChannelPhysicalChannelSet(AtEncapBinder self, AtEncapChannel encapChannel, AtChannel physicalChannel);

tDe1Backup* AtEncapBinderDe1EncapBackup(AtEncapBinder self, AtPdhChannel de1);
eAtRet AtEncapBinderDe1EncapRestore(AtEncapBinder self, AtPdhChannel de1, tDe1Backup *backup);

uint32 AtEncapBinderDe1EncapHwIdAllocate(AtEncapBinder self, AtPdhDe1 de1);
uint32 AtEncapBinderDe3EncapHwIdAllocate(AtEncapBinder self, AtPdhDe3 de3);
uint32 AtEncapBinderNxDS0EncapHwIdAllocate(AtEncapBinder self, AtPdhNxDS0 ds0);
uint32 AtEncapBinderAuVcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc auvc);
uint32 AtEncapBinderTu3VcEncapHwIdAllocate(AtEncapBinder self, AtSdhVc tu3vc);
uint32 AtEncapBinderVc1xEncapHwIdAllocate(AtEncapBinder self, AtSdhVc vc1x);
void AtEncapBinderEncapHwIdDeallocate(AtEncapBinder self, uint32 hwId);

uint32 AtEncapBinderHoVcBoundEncapHwIdGet(AtEncapBinder self, AtChannel hoVc);
uint32 AtEncapBinderVc1xBoundEncapHwIdGet(AtEncapBinder self, AtChannel vc1x);
uint32 AtEncapBinderDe1BoundEncapHwIdGet(AtEncapBinder self, AtChannel de1);
uint32 AtEncapBinderDe3BoundEncapHwIdGet(AtEncapBinder self, AtChannel de3);
uint32 AtEncapBinderNxDs0BoundEncapHwIdGet(AtEncapBinder self, AtChannel nxDs0);

AtDevice AtEncapBinderDeviceGet(AtEncapBinder self);

#ifdef __cplusplus
}
#endif
#endif /* _ATENCAPBINDER_H_ */


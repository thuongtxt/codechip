/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Management
 * 
 * File        : AtDriverApiLogMacro.h
 * 
 * Created Date: Apr 8, 2018
 *
 * Description : API log macros
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDRIVERAPILOGMACRO_H_
#define _ATDRIVERAPILOGMACRO_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cApiLogSeverity cAtLogLevelDebug

#define mOneParamApiLogStart(value)                                            \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        AtDriverLogLock();                                                     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %u)\r\n",    \
                                AtFunction, AtObjectToString((AtObject)self), value); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mOneUint64ParamApiLogStart(value)                                      \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        AtDriverLogLock();                                                     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %llu)\r\n",    \
                                AtFunction, AtObjectToString((AtObject)self), value); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mOneObjectApiLogStart(object)                                          \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        const char* objectDesc;                                                \
        AtDriverLogLock();                                                     \
        objectDesc = AtDriverObjectStringToSharedBuffer((AtObject)object);     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n",     \
                                AtFunction, AtObjectToString((AtObject)self), objectDesc); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mTwoParamsApiLogStart(param1, param2)                                  \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        AtDriverLogLock();                                                     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %u, %u)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), param1, param2); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mTwoGenericParamsApiLogStart(param1, param2)                           \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        AtDriverLogLock();                                                     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %d, %d)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), (uint32)(param1), (uint32)(param2)); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mThreeParamsApiLogStart(param1, param2, param3)                        \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        AtDriverLogLock();                                                     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %u, %u, %u)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), param1, param2, param3); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mNoParamApiLogStart()                                                  \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        AtDriverLogLock();                                                     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s])\r\n",     \
                                AtFunction, AtObjectToString((AtObject)self)); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mMacAttributeApiLogStart(mac)                                          \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *macString;                                              \
                                                                               \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);   \
        macString = AtUtilBytes2String(mac, cAtMacAddressLen, 16, buffer, bufferSize); \
                                                                               \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n",     \
                                AtFunction, AtObjectToString((AtObject)self), macString); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mTwoParamsWrapCall(param1, param2, returnType, call)                   \
do                                                                             \
    {                                                                          \
    returnType ret;                                                            \
                                                                               \
    mTwoParamsApiLogStart(param1, param2);                                     \
    ret = call;                                                                \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }while(0)

#define mThreeParamsWrapCall(param1, param2, param3, returnType, call)         \
do                                                                             \
    {                                                                          \
    returnType ret;                                                            \
                                                                               \
    mThreeParamsApiLogStart(param1, param2, param3);                           \
    ret = call;                                                                \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }while(0)

#define mNoParamWrapCall(returnType, call)                                     \
do                                                                             \
    {                                                                          \
    returnType ret;                                                            \
    mNoParamApiLogStart();                                                     \
    ret = call;                                                                \
    AtDriverApiLogStop();                                                      \
    return ret;                                                                \
    }while(0)

#define mOneParamWrapCall(param, returnType, call)                             \
    do                                                                         \
    {                                                                          \
    returnType ret;                                                            \
                                                                               \
    mOneParamApiLogStart(param);                                               \
    ret = call;                                                                \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }while(0)

#define mOneObjectWrapCall(object, returnType, call)                           \
    do                                                                         \
    {                                                                          \
    returnType ret;                                                            \
                                                                               \
    mOneObjectApiLogStart(object);                                             \
    ret = call;                                                                \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }while(0)

#define mMacAttributeWrapCall(mac, call)                                       \
    do                                                                         \
    {                                                                          \
    eAtRet ret;                                                                \
                                                                               \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *macString;                                              \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);   \
        macString = AtUtilBytes2String(mac, cAtMacAddressLen, 16, buffer, bufferSize); \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n",     \
                                AtFunction,                                    \
                                AtObjectToString((AtObject)self),              \
                                macString);                                    \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
                                                                               \
    ret = call;                                                                \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }while(0)

#define mBufferAttributeApiLogStart(buffer, bufferLength)                      \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 charBufferSize;                                                 \
        char *charBuffer, *bufferString;                                       \
                                                                               \
        AtDriverLogLock();                                                     \
        charBuffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &charBufferSize);\
        bufferString = AtUtilBytes2String((const uint8 *)buffer, bufferLength, 16, charBuffer, charBufferSize);\
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,      \
                                AtSourceLocationNone, "API: %s([%s], [%s], %u)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), bufferString, bufferLength);\
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mAddressAttributeApiLogStart(address, addressLength)                   \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 charBufferSize;                                                 \
        char *charBuffer, *bufferString;                                       \
                                                                               \
        AtDriverLogLock();                                                     \
        charBuffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &charBufferSize);\
        bufferString = AtUtilBytes2String((const uint8 *)address, addressLength, 16, charBuffer, charBufferSize);\
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,      \
                                AtSourceLocationNone, "API: %s([%s], [%s], %u)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), bufferString, addressLength);\
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mVlanTagAttributeApiLogStart(vlanTag)                                  \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *vlanString;                                             \
                                                                               \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);\
        vlanString = AtUtilVlanTag2String(vlanTag, buffer, bufferSize);        \
                                                                               \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), vlanString);\
                                                                               \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mVlanAttributeApiLogStart(vlan)                                        \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *vlanString;                                             \
                                                                               \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);\
        vlanString = AtUtilVlan2String(vlan, buffer, bufferSize);              \
                                                                               \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), vlanString);\
                                                                               \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mVlanTagAttributeSet(method, vlanTag)                                  \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mVlanTagAttributeApiLogStart(vlanTag);                                     \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, vlanTag);                        \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s]) = %s\r\n",   \
                                    AtFunction, AtObjectToString((AtObject)self), AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mVlanAttributeSet(method, vlan)                                        \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mVlanAttributeApiLogStart(vlan);                                           \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, vlan);                           \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s]) = %s\r\n",     \
                                    AtFunction, AtObjectToString((AtObject)self), AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mVlanTagAttributeAtIndexApiLogStart(index, vlanTag)                    \
do                                                                             \
    {                                                                          \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *vlanString;                                             \
                                                                               \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);\
        vlanString = AtUtilVlanTag2String(vlanTag, buffer, bufferSize);        \
                                                                               \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %d, %s)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), index, vlanString);\
                                                                               \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
    }while(0)

#define mVlanTagAttributeAtIndexSet(method, index, vlanTag)                    \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mVlanTagAttributeAtIndexApiLogStart(index, vlanTag);                       \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, index, vlanTag);                 \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s]) = %s\r\n",     \
                                    AtFunction, AtObjectToString((AtObject)self), AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }
/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATDRIVERAPILOGMACRO_H_ */


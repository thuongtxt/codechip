/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtDevice.c
 *
 * Created Date: Jul 26, 2012
 *
 * Author      : namnn
 *
 * Description : Default implementation of AT device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDriver.h"
#include "AtModuleRam.h"
#include "AtRam.h"

#include "AtModuleInternal.h"
#include "AtDeviceInternal.h"
#include "AtIpCoreInternal.h"
#include "AtInterruptPinInternal.h"

#include "../../util/AtIteratorInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../physical/AtSerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtMaxNumEventListener 32
#define mInAccessible(self) AtDeviceInAccessible(self)

/*--------------------------- Macros -----------------------------------------*/
/* Delete module */
#define mDeleteModule(self, moduleName)                                        \
    do                                                                         \
        {                                                                      \
        if (self->moduleName)                                                  \
            {                                                                  \
            AtObjectDelete((AtObject)(self->moduleName));                        \
            self->moduleName = NULL;                                           \
            }                                                                  \
        }while(0)

#define mCreateModule(moduleId, moduleRef)                                     \
    if (moduleRef == NULL)                                                     \
        moduleRef = self->methods->ModuleCreate(self, (moduleId));             \
    return moduleRef;

#define mCoreIdIsValid(self, coreId) (coreId < AtDeviceNumIpCoresGet(self))
#define mDeviceIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDeviceListenerWrapper
    {
    const tAtDeviceListener *callback;
    void *userData;
    }tAtDeviceListenerWrapper;

typedef struct tAtDeviceModuleIterator
    {
    /* Inherit AtIterator */
    tAtIterator super;

    /* Private data */
    uint8 count;
    const eAtModule *modules;
    AtDevice device;
    int8 index;
    }tAtDeviceModuleIterator;

typedef struct tAtDeviceModuleIterator * AtDeviceModuleIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtDeviceMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/* Modules Iterator initialize */
static char m_iteratorMethodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;

static char m_coreIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_coreAtArrayIteratorOverride;
static tAtIteratorMethods m_coreAtIteratorOverride;

static tAtHalListener m_halListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Get next element, return null if there is no next element */
static AtObject NextGet(AtIterator self)
    {
    AtModule module;
    AtDeviceModuleIterator iterator = (AtDeviceModuleIterator)self;

    /* When index points to last element, there no next element to get */
    if(iterator->index >= (int8)(iterator->count - 1))
        return NULL;

    /* If last time, index does not point to last element */
    module = AtDeviceModuleGet(iterator->device, iterator->modules[iterator->index + 1]);
    iterator->index++;
    return (AtObject)module;
    }

/* Delete current element */
static AtObject Remove(AtIterator self)
    {
	AtUnused(self);
    /* Do not allow to remove object in this case */
    return NULL;
    }

/* Count */
static uint32 Count(AtIterator self)
    {
    return ((AtDeviceModuleIterator)self)->count;
    }

static void Restart(AtIterator self)
    {
    ((AtDeviceModuleIterator)self)->index = -1;
    }

static uint32 DeviceId(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static const char* DeviceIdToString(AtDevice self)
    {
    AtUnused(self);
    return "";
    }

static void SemControllerSerialize(AtDevice object, AtCoder encoder)
    {
    mEncodeObjects(semControllers, AtDeviceNumSemControllersGet(object));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtDevice object = (AtDevice)self;

    mEncodeUInt(productCode);
    mEncodeUInt(isSimulated);
    mEncodeUInt(testbenchEnabled);

    (void)AtDeviceVersion(object); /* To have valid version in database for serializing */
    mEncodeString(version);

    mEncodeNone(resetDone);
    mEncodeUInt(isEjected);
    mEncodeUInt(needCheckEjected);
    mEncodeUInt(inWarmRestore);
    mEncodeUInt(isDiagnostic);
    mEncodeNone(diagnosticCheckDisabled);

    mEncodeObjectDescription(driver);
    mEncodeNone(listeners);
    mEncodeNone(haHal);

    mEncodeObject(sdhModule);
    mEncodeObject(pdhModule);
    mEncodeObject(encapModule);
    mEncodeObject(pppModule);
    mEncodeObject(ethModule);
    mEncodeObject(ramModule);
    mEncodeObject(berModule);
    mEncodeObject(pktAnalyzerModule);
    mEncodeObject(pwModule);
    mEncodeObject(clockModule);
    mEncodeObject(prbsModule);
    mEncodeObject(apsModule);
    mEncodeObject(surModule);
    mEncodeObject(xcModule);
    mEncodeNone(semControllers); /* To satisfy the serialize check. */
    mMethodsGet(object)->SemControllerSerialize(object, encoder);
    mEncodeObject(thermalSensor);
    mEncodeObject(powerSupplySensor);
    mEncodeObject(sskeyChecker);
    mEncodeObject(defaultGlobalLongRegisterAccess);
    mEncodeObject(installedGlobalLongRegisterAccess);
    mEncodeObject(encapBinder);
    mEncodeObject(concateModule);
    mEncodeObject(frModule);
    mEncodeObject(serdesManager);
    mEncodeNone(debugger);
    mEncodeObject(ptpModule);

    AtCoderEncodeObjects(encoder, (AtObject *)object->allIpCores, AtDeviceNumIpCoresGet(object), "allIpCores");

    mEncodeUInt(devAsyncResetState);
    mEncodeUInt(asyncInitState);
    mEncodeUInt(nextModulePos);
    mEncodeNone(previousTimeCount);
    mEncodeUInt(asyncInitEntranceState);
    mEncodeNone(ssKeyStartTime);
    mEncodeUInt(asyncCurrentCore);
    mEncodeNone(deleting);
    mEncodeUInt(targetPlatformProductCode);
    mEncodeNone(registerListener);
    mEncodeNone(asyncInitRemainingUsNeedDelay);
    mEncodeObjects(interruptPins, object->numInterruptPins);
    mEncodeObject(diagUart);
    mEncodeUInt(allModulesDidSetup);
    mEncodeUInt(warmRestoreStarted);

    AtCoderEncodeUInt(encoder, AtListLengthGet(object->allEventListeners), "allEventListeners");
    mEncodeNone(listenerListMutex);
    mEncodeNone(cache);
    }

static eAtRet DiagnosticModeEnable(AtDevice self, eBool enable)
    {
    self->isDiagnostic = enable;
    return cAtOk;
    }

static eBool DiagnosticModeIsEnabled(AtDevice self)
    {
    return self->isDiagnostic;
    }

static tAtDeviceListenerWrapper *ListenerFind(AtDevice self, const tAtDeviceListener *callback)
    {
    tAtDeviceListenerWrapper *wrapper;
    AtIterator iterator = AtListIteratorCreate(self->listeners);

    while ((wrapper = (tAtDeviceListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback == callback)
            break;
        }
    AtObjectDelete((AtObject)iterator);

    return wrapper;
    }

static tAtDeviceListenerWrapper *ListenerWrapperCreate(const tAtDeviceListener *callback, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tAtDeviceListenerWrapper);
    tAtDeviceListenerWrapper *newWrapper = mMethodsGet(osal)->MemAlloc(osal, memorySize);

    mMethodsGet(osal)->MemInit(osal, newWrapper, 0, memorySize);
    newWrapper->callback = callback;
    newWrapper->userData = userData;

    return newWrapper;
    }

static AtList Listeners(AtDevice self)
    {
    if (self->listeners == NULL)
        self->listeners = AtListCreate(0);
    return self->listeners;
    }

static eAtRet ListenerAdd(AtDevice self, const tAtDeviceListener *callback, void *userData)
    {
    if (ListenerFind(self, callback))
        return cAtErrorDuplicatedEntries;

    return AtListObjectAdd(Listeners(self), (AtObject)ListenerWrapperCreate(callback, userData));
    }

static eAtRet ListenerRemove(AtDevice self, const tAtDeviceListener *callback)
    {
    tAtDeviceListenerWrapper *wrapper;

    if (callback == NULL)
        return cAtOk;

    if (self->listeners == NULL)
        return cAtErrorNotExist;

    wrapper = ListenerFind(self, callback);
    if (wrapper == NULL)
        return cAtErrorNotExist;

    if (wrapper->callback->WillRemoveListener)
        wrapper->callback->WillRemoveListener(self, wrapper->userData);

    AtListObjectRemove(self->listeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);

    return cAtOk;
    }

static void ReadNotify(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    tAtDeviceListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tAtDeviceListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->DidReadRegister)
            wrapper->callback->DidReadRegister(self, address, value, coreId, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void WriteNotify(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    tAtDeviceListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tAtDeviceListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->DidWriteRegister)
            wrapper->callback->DidWriteRegister(self, address, value, coreId, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void LongReadNotify(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId)
    {
    tAtDeviceListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tAtDeviceListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->DidLongReadOnCore)
            wrapper->callback->DidLongReadOnCore(self, address, dataBuffer, numDwords, coreId, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void LongWriteNotify(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId)
    {
    tAtDeviceListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tAtDeviceListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->DidLongWriteOnCore)
            wrapper->callback->DidLongWriteOnCore(self, address, dataBuffer, numDwords, coreId, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void RoleChangeNotify(AtDevice self, uint32 role)
    {
    tAtDeviceListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tAtDeviceListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->DidChangeRole)
            wrapper->callback->DidChangeRole(self, role, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static eBool Accessible(AtDevice self)
    {
    AtUnused(self);
    return AtDriverIsActive();
    }

static eBool ShouldRestoreDatabaseOnWarmRestoreStop(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldEnableInterruptAfterProcessing(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtIterator(AtDeviceModuleIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_iteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(iterator), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        mMethodOverride(m_AtIteratorOverride, Remove);
        mMethodOverride(m_AtIteratorOverride, Count);
        mMethodOverride(m_AtIteratorOverride, Restart);
        }
    mMethodsSet(iterator, &m_AtIteratorOverride);
    }

static AtDeviceModuleIterator AtDeviceModuleIteratorObjectInit(AtDeviceModuleIterator self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtDeviceModuleIterator));

    /* Super constructor */
    if (AtIteratorObjectInit((AtIterator)self) == NULL)
        return NULL;

    /* Override */
    OverrideAtIterator(self);
    m_iteratorMethodsInit = 1;

    return self;
    }

static AtDeviceModuleIterator AtDeviceModuleIteratorNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDeviceModuleIterator newIterator = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtDeviceModuleIterator));

    if (newIterator)
        {
        /* Construct it */
        AtDeviceModuleIteratorObjectInit(newIterator);

        /* Init it */
        newIterator->modules = mMethodsGet(device)->AllSupportedModulesGet(device, &(newIterator->count));
        newIterator->device = device;
        newIterator->index = -1;
        }

    return newIterator;
    }

/* To get device type */
static uint32 ProductCodeGet(AtDevice self)
    {
    return self->productCode;
    }

static eAtRet CreateAllCores(AtDevice self)
    {
    uint8 numCores, i;
    uint32 size;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Do nothing if they were created */
    if (self->allIpCores)
        return cAtOk;

    /* Allocate memory for all core objects */
    numCores = mMethodsGet(self)->NumIpCoresGet(self);
    size = sizeof(AtIpCore) * numCores;
    self->allIpCores = mMethodsGet(osal)->MemAlloc(osal, size);
    if (self->allIpCores == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, self->allIpCores, 0, size);

    /* Create all core objects */
    for (i = 0; i < numCores; i++)
        {
        self->allIpCores[i] = mMethodsGet(self)->IpCoreCreate(self, i);
        if (self->allIpCores[i] == NULL)
            return cAtErrorRsrcNoAvail;
        }

    return cAtOk;
    }

static void DeleteAllCores(AtDevice self)
    {
    uint8 numCores, i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Do nothing if cores have not been created */
    if (self->allIpCores == NULL)
        return;

    /* Delete all core objects */
    numCores = mMethodsGet(self)->NumIpCoresGet(self);
    for (i = 0; i < numCores; i++)
        AtObjectDelete((AtObject)(self->allIpCores[i]));

    /* Delete memory for all cores */
    mMethodsGet(osal)->MemFree(osal, self->allIpCores);
    self->allIpCores = NULL;
    }

static void ModuleSet(AtDevice self, eAtModule moduleId, AtModule module)
    {
    if (moduleId == cAtModuleSdh)   self->sdhModule   = module;
    if (moduleId == cAtModulePdh)   self->pdhModule   = module;
    if (moduleId == cAtModuleEncap) self->encapModule = module;
    if (moduleId == cAtModulePpp)   self->pppModule   = module;
    if (moduleId == cAtModuleEth)   self->ethModule   = module;
    if (moduleId == cAtModuleRam)   self->ramModule   = module;
    if (moduleId == cAtModuleBer)   self->berModule   = module;
    if (moduleId == cAtModuleClock) self->clockModule = module;
    if (moduleId == cAtModulePrbs)  self->prbsModule  = module;
    if (moduleId == cAtModuleAps)   self->apsModule   = module;
    if (moduleId == cAtModuleXc)    self->xcModule    = module;
    if (moduleId == cAtModuleSur)   self->surModule   = module;
    if (moduleId == cAtModuleFr)    self->frModule    = module;
    if (moduleId == cAtModulePktAnalyzer) self->pktAnalyzerModule = module;
    if (moduleId == cAtModuleConcate)   self->concateModule   = module;
    if (moduleId == cAtModulePtp) self->ptpModule = module;
    if (moduleId == cAtModulePw) self->pwModule = module;
    }

static void AllModulesCreate(AtDevice self)
    {
    uint8 numModules, i;
    const eAtModule* modules = mMethodsGet(self)->AllSupportedModulesGet(self, &numModules);

    for (i = 0 ; i < numModules; i++)
        {
        if (mMethodsGet(self)->ModuleGet(self, modules[i]))
            continue;

        ModuleSet(self, modules[i], self->methods->ModuleCreate(self, modules[i]));
        }
    }

static void AllModulesDelete(AtDevice self)
    {
    mDeleteModule(self, apsModule);
    mDeleteModule(self, sdhModule);
    mDeleteModule(self, pdhModule);
    mDeleteModule(self, pppModule);
    mDeleteModule(self, ethModule);
    mDeleteModule(self, encapModule);
    mDeleteModule(self, ramModule);
    mDeleteModule(self, berModule);
    mDeleteModule(self, pktAnalyzerModule);
    mDeleteModule(self, pwModule);
    mDeleteModule(self, clockModule);
    mDeleteModule(self, prbsModule);
    mDeleteModule(self, surModule);
    mDeleteModule(self, xcModule);
    mDeleteModule(self, concateModule);
    mDeleteModule(self, frModule);
    mDeleteModule(self, ptpModule);
    }

static eAtTriggerMode InterruptPinDefaultTriggerMode(AtDevice self, AtInterruptPin pin)
    {
    AtUnused(self);
    AtUnused(pin);
    return cAtTriggerModeLevelHigh;
    }

static eAtRet InterruptPinsInit(AtDevice self)
    {
    uint32 pin_i;
    uint32 numPins = self->numInterruptPins;
    eAtRet ret = cAtOk;

    if ((numPins == 0) || (AtDeviceNumInterruptPins(self) == 0))
        return cAtOk;

    if (self->interruptPins == NULL)
        return cAtOk;

    /* Delete all created PINs */
    for (pin_i = 0; pin_i < numPins; pin_i++)
        {
        AtInterruptPin pin = self->interruptPins[pin_i];
        eAtTriggerMode triggerMode = mMethodsGet(self)->InterruptPinDefaultTriggerMode(self, pin);

        ret |= AtInterruptPinInit(pin);
        ret |= AtInterruptPinTriggerModeSet(pin, triggerMode);
        }

    return ret;
    }

static eAtRet AllModulesInit(AtDevice self)
    {
    uint32 ret = cAtOk;

    ret |= InterruptPinsInit(self);

    ret |= AtDeviceModuleInit(self, cAtModuleBer);
    ret |= AtDeviceModuleInit(self, cAtModuleXc);
    ret |= AtDeviceModuleInit(self, cAtModuleSdh);
    ret |= AtDeviceModuleInit(self, cAtModulePdh);
    ret |= AtDeviceModuleInit(self, cAtModuleEncap);
    ret |= AtDeviceModuleInit(self, cAtModulePpp);
    ret |= AtDeviceModuleInit(self, cAtModuleEth);
    ret |= AtDeviceModuleInit(self, cAtModuleRam);
    ret |= AtDeviceModuleInit(self, cAtModulePktAnalyzer);
    ret |= AtDeviceModuleInit(self, cAtModulePw);
    ret |= AtDeviceModuleInit(self, cAtModuleClock);
    ret |= AtDeviceModuleInit(self, cAtModulePrbs);
    ret |= AtDeviceModuleInit(self, cAtModuleAps);
    ret |= AtDeviceModuleInit(self, cAtModuleSur);
    ret |= AtDeviceModuleInit(self, cAtModuleConcate);
    ret |= AtDeviceModuleInit(self, cAtModuleFr);
    ret |= AtDeviceModuleInit(self, cAtModulePtp);

    if (self->serdesManager)
        ret |= AtSerdesManagerInit(self->serdesManager);

    return (eAtRet)ret;
    }

static uint32 SimulationCode(AtDevice self)
    {
    uint32 productCode = self->productCode;
    return (productCode & cBit31_28) >> 28;
    }

static eBool IsSimulationCode(AtDevice self, uint32 productCode)
    {
    uint32 simulationCode = mMethodsGet(self)->SimulationCode(self);
    return (((productCode & cBit31_28) >> 28) == simulationCode) ? cAtTrue : cAtFalse;
    }

static void SerdesManagerDelete(AtDevice self)
    {
    AtObjectDelete((AtObject)self->serdesManager);
    self->serdesManager = NULL;
    }

static AtSerdesManager SerdesManagerCreate(AtDevice self)
    {
    AtSerdesManager manager;

    manager = mMethodsGet(self)->SerdesManagerObjectCreate(self);
    if (manager == NULL)
        return NULL;

    if (AtSerdesManagerSetup(manager) != cAtOk)
        {
        AtObjectDelete((AtObject)self->serdesManager);
        return NULL;
        }

    return manager;
    }

static AtSerdesManager SerdesManagerSetup(AtDevice self)
    {
    AtObjectDelete((AtObject)self->serdesManager);
    self->serdesManager = SerdesManagerCreate(self);
    return self->serdesManager;
    }

static eAtRet Setup(AtDevice self)
    {
    eAtRet ret = cAtOk;
    eBool isSimulated;

    /* Need to update simulate indication for further initialization */
    isSimulated = IsSimulationCode(self, AtDeviceProductCodeGet(self)) ? cAtTrue : cAtFalse;
    AtDeviceSimulate(self, isSimulated);

    /* Create all cores */
    if (self->allIpCores == NULL)
        ret = CreateAllCores(self);

    /* To setup modules, must have HAL to have hardware version so that modules
     * can be setup correctly with right resources */
    if (AtDeviceIpCoreHalGet(self, 0) == NULL)
        return ret;

    /* Create and setup all modules */
    self->methods->AllModulesCreate(self);
    AtDeviceAllModulesSetup(self);

    /* Setup binders */
    if (AtDeviceIpCoreHalGet(self, 0))
        self->encapBinder = mMethodsGet(self)->EncapBinder(self);

    /* This is fore devices that have more than one cores, to avoid module setup
     * be called every time HAL for each core is setup */
    self->allModulesDidSetup = cAtTrue;

    return ret;
    }

static eAtRet AllCoresActivate(AtDevice self, eBool activate)
    {
    uint8 core_i;
    eAtRet ret = cAtOk;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        ret |= AtIpCoreActivate(core, activate);
        }

    return ret;
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet AllSemControllersReset(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint8 i;

    for (i = 0; i < AtDeviceNumSemControllersGet(self); i++)
        ret |= AtSemControllerInit(AtDeviceSemControllerGetByIndex(self, i));

    return ret;
    }

static eAtRet EntranceToInit(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret  = AtDeviceAllCoresReset(self);

    /* Check SSKey, give the key engine a moment so that it can run and
     * update status */
    if (mMethodsGet(self)->NeedCheckSSKey(self))
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->Sleep(osal, 3);
        ret |= self->methods->SSKeyCheck(self);
        }

    if (ret != cAtOk)
        return ret;

    /* Need to check if cores work */
    ret = AtDeviceAllCoresTest(self);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static eAtRet InModulesInit(AtDevice self)
    {
    eBool shouldInit = AtDeviceTestbenchIsEnabled(self) ? cAtFalse : cAtTrue;
    eAtRet ret = cAtOk;

    /* Just try to re-initialize all modules to stop all traffics to hardware
     * before starting initializing sequence. After soft reset, modules can be
     * in unknown state. It's time to give all of modules a fresh state */
    if (shouldInit)
        self->methods->AllModulesInit(self);

    ret |= AtDeviceAllModulesSetup(self);

    if (shouldInit)
        ret |= self->methods->AllModulesInit(self);

    return ret;
    }

static eAtRet PostInit(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= AllCoresActivate(self, cAtTrue);
    ret |= AllSemControllersReset(self);

    return ret;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret = AtDeviceEntranceToInit(self);
    if (ret != cAtOk)
        return ret;

    ret = InModulesInit(self);
    if (ret != cAtOk)
        return ret;

    return AtDevicePostInit(self);
    }

static eAtRet DefaultIpCoreSetup(AtDevice self)
    {
	AtUnused(self);
    /* Let concrete class do, and all of module will have core 0 as default core */
    return cAtOk;
    }

static AtIpCore IpCoreCreate(AtDevice self, uint8 coreId)
    {
	AtUnused(coreId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

/* To get driver that this device belong to */
static AtDriver DriverGet(AtDevice self)
    {
    if (mDeviceIsValid(self))
        return self->driver;

    return NULL;
    }

static uint8 NumIpCoresGet(AtDevice self)
    {
	AtUnused(self);
    return 1;
    }

static AtIpCore IpCoreGet(AtDevice self, uint8 coreId)
    {
    if (self->allIpCores == NULL)
        return NULL;

    return self->allIpCores[coreId];
    }

/* To get all supported modules */
static const eAtModule* AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
	AtUnused(self);
    /* Let concrete class do */
    if (numModules)
        *numModules = 0;

    return NULL;
    }

/* Create new module instance */
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
	AtUnused(moduleId);
	AtUnused(self);
    return NULL;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
	AtUnused(moduleId);
	AtUnused(self);
    /* The first version of this function use the for loop to determine if a
     * module is supported. It just loops on all supported modules return by
     * AtDeviceAllSupportedModulesGet. This way can produce a clean code but
     * slower than and take CPU more than using switch case.
     *
     * So, let concrete devices do switch case to determine if a module is
     * supported. */
    return cAtFalse;
    }

/* To get a specified module. Generic device does not know what module it
 * supports. Let concrete device do this */
static AtModule ModuleGet(AtDevice self, eAtModule moduleId)
    {
    if (!mMethodsGet(self)->ModuleIsSupported(self, moduleId))
        return NULL;

    switch (moduleId)
        {
        case cAtModuleSdh:
            mCreateModule(moduleId, self->sdhModule)
        case cAtModulePdh:
            mCreateModule(moduleId, self->pdhModule)
        case cAtModulePpp:
            mCreateModule(moduleId, self->pppModule)
        case cAtModuleEncap:
            mCreateModule(moduleId, self->encapModule)
        case cAtModuleEth:
            mCreateModule(moduleId, self->ethModule)
        case cAtModulePw:
            mCreateModule(moduleId, self->pwModule)
        case cAtModuleRam:
            mCreateModule(moduleId, self->ramModule)
        case cAtModuleBer:
            mCreateModule(moduleId, self->berModule)
        case cAtModulePrbs:
            mCreateModule(moduleId, self->prbsModule)
        case cAtModuleClock:
            mCreateModule(moduleId, self->clockModule)
        case cAtModulePktAnalyzer:
            mCreateModule(moduleId, self->pktAnalyzerModule)
        case cAtModuleAps:
            mCreateModule(moduleId, self->apsModule)
        case cAtModuleSur:
            mCreateModule(moduleId, self->surModule)
        case cAtModuleXc :
            mCreateModule(moduleId, self->xcModule)
        case cAtModuleConcate:
            mCreateModule(moduleId, self->concateModule)    
        case cAtModuleFr :
            mCreateModule(moduleId, self->frModule)
        case cAtModulePtp:
            mCreateModule(moduleId, self->ptpModule)
        case cAtModuleIma:
        case cAtModuleAtm:
        case cAtModuleInvalid:
        default:
            return NULL;
        }
    }

static void InterruptProcess(AtDevice self)
    {
	AtUnused(self);
    /* Let concrete device do */
    }

static eAtRet InterruptRestore(AtDevice self)
    {
    /* Let concrete device do */
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static void PeriodicProcess(AtDevice self, uint32 periodInMs)
    {
	AtUnused(periodInMs);
	AtUnused(self);
    /* Let concrete device do */
    }

static uint16 LongReadOnCore(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
	AtUnused(coreId);
	AtUnused(bufferLen);
	AtUnused(dataBuffer);
	AtUnused(localAddress);
	AtUnused(self);
    /* Let concrete device do */
    return 0;
    }

static uint16 LongWriteOnCore(AtDevice self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
	AtUnused(coreId);
	AtUnused(bufferLen);
	AtUnused(dataBuffer);
	AtUnused(localAddress);
	AtUnused(self);
    /* Let concrete device do */
    return 0;
    }

static uint16 ReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    AtUnused(coreId);
    AtUnused(bufferLen);
    AtUnused(dataBuffer);
    AtUnused(localAddress);
    AtUnused(subDeviceId);
    AtUnused(self);
    /* Let concrete device do */
    return 0;
    }

static uint16 WriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    AtUnused(coreId);
    AtUnused(bufferLen);
    AtUnused(dataBuffer);
    AtUnused(ddrAddress);
    AtUnused(regAddress);
    AtUnused(subDeviceId);
    AtUnused(self);
    /* Let concrete device do */
    return 0;
    }

static const char *Version(AtDevice self)
    {
    return (const char *)mMethodsGet(self)->VersionRead(self, self->version, sizeof(self->version));
    }

static uint32 VersionNumber(AtDevice self)
    {
	AtUnused(self);
    /* Concrete classes know how to read */
    return 0;
    }

static uint32 BuiltNumber(AtDevice self)
    {
    /* Concretes should know */
    AtUnused(self);
    return 0;
    }

static void ShowResetInfo(AtDevice self)
    {
    AtPrintc(cSevNormal, "* Reset: ");
    AtPrintc(self->resetDone ? cSevInfo : cSevWarning, "%s", self->resetDone ? "Done" : "Not yet");
    AtPrintc(cSevNormal, "\r\n");
    }

static void Debug(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    if (debugger)
        {
        uint32 bufferSize;
        char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(self->isEjected ? cSevCritical : cSevNormal,
                           "Device is ejected", self->isEjected ? "yes" : "no"));
        AtDebuggerEntryAdd(debugger, AtDebugEntryStringFormatNew(cSevNormal, buffer, bufferSize,
                           "Simulation on platform", self->targetPlatformProductCode ? "0x%08x" : "none",
                           self->targetPlatformProductCode));
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(self->warmRestoreStarted ? cSevCritical : cSevNormal,
                           "Warm restore started", self->warmRestoreStarted ? "yes" : "no"));
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(self->resetDone ? cSevInfo : cSevWarning,
                           "Reset", self->resetDone ? "Done" : "Not yet"));
        return;
        }

    if (self->isEjected)
        AtPrintc(cSevCritical, "* Device 0x%08x is ejected\r\n", AtDeviceProductCodeGet(self));

    if (self->targetPlatformProductCode)
        {
        AtPrintc(cSevNormal, "* Simulate on platform: ");
        AtPrintc(cSevInfo, "0x%08x\r\n", self->targetPlatformProductCode);
        }

    if (mMethodsGet(self)->ShouldResetBeforeInitializing(self))
        mMethodsGet(self)->ShowResetInfo(self);

    AtPrintc(cSevNormal, "* Warm restore started: %s\r\n", self->warmRestoreStarted ? "yes" : "no");
    }

static AtDebugger DebuggerCreate(AtDevice self)
    {
    if (self->debugger == NULL)
        self->debugger = AtDebuggerNew();
    return self->debugger;
    }

static void DebuggerDelete(AtDevice self)
    {
    AtObjectDelete((AtObject)self->debugger);
    self->debugger = NULL;
    }

static void DebuggerFill(AtDevice self, AtDebugger debugger)
    {
    AtUnused(self);
    AtUnused(debugger);
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
	AtUnused(self);
    /* Concrete product should do */
    return NULL;
    }

static AtSSKeyChecker SSKeyChecker(AtDevice self)
    {
    if (self->sskeyChecker == NULL)
        self->sskeyChecker = mMethodsGet(self)->SSKeyCheckerCreate(self);
    return self->sskeyChecker;
    }

static eAtRet SSKeyCheck(AtDevice self)
    {
    static const uint8 cCheckingRetryTimes = 10;
    uint8 time_i;
    AtSSKeyChecker sskeyChecker = SSKeyChecker(self);
    eAtRet ret = cAtOk;
    AtOsal osal = AtSharedDriverOsalGet();

    for (time_i = 0; time_i < cCheckingRetryTimes; time_i++)
        {
        ret = AtSSKeyCheckerCheck(sskeyChecker);
        if (ret == cAtOk)
            {
            if (time_i > 0)
                AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "SSKey checking pass after retrying %d times\r\n", time_i + 1);
            return ret;
            }

        if (AtDeviceIsSimulated(self))
            return cAtOk;

        /* SSKey checking fail, need to give hardware a moment for stabling */
        mMethodsGet(osal)->Sleep(osal, 1);
        }

    AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "SSKey checking fail after retrying %d times\r\n", cCheckingRetryTimes);

    /* Return the last error */
    return ret;
    }

static void AllListenersDelete(AtDevice self)
    {
    if (self->listeners == NULL)
        return;

    while (AtListLengthGet(self->listeners) > 0)
        {
        tAtDeviceListenerWrapper *wrapper = (tAtDeviceListenerWrapper *)AtListObjectRemoveAtIndex(self->listeners, 0);
        if (wrapper->callback->WillRemoveListener)
            wrapper->callback->WillRemoveListener(self, wrapper->userData);
        AtOsalMemFree(wrapper);
        }

    AtObjectDelete((AtObject)self->listeners);
    }

static eAtRet MemoryTest(AtDevice self)
    {
    eAtRet ret;
    AtIterator moduleIterator = AtDeviceModuleIteratorCreate(self);

    ret = AtModulesMemoryTest(moduleIterator);
    AtObjectDelete((AtObject)moduleIterator);

    return ret;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtDevice self, AtModule module)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(module, &numHoldRegisters);
	AtUnused(self);
    if (holdRegisters == NULL)
        return NULL;

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static const char *ToString(AtObject self)
    {
    static char string[64];
    AtDevice dev = (AtDevice)self;

    AtSprintf(string, "%sdevice_%08x", AtDeviceIdToString(dev), AtDeviceProductCodeGet(dev));
    return string;
    }

static uint32 ProductCodeRead(AtDevice self)
    {
	AtUnused(self);
    return 0;
    }

static eBool CanReadProductCode(AtDevice self)
    {
    return AtDeviceIpCoreHalGet(self, 0) ? cAtTrue : cAtFalse;
    }

static eBool ProductCodeMatch(AtDevice self)
    {
    uint32 productCode = mMethodsGet(self)->ProductCodeRead(self);
    return ((productCode & cBit27_0) == (AtDeviceProductCodeGet(self) & cBit27_0)) ? cAtTrue : cAtFalse;
    }

static eBool IsEjected(AtDevice self)
    {
    if (self->isEjected)
        return cAtTrue;

    if (!CanReadProductCode(self))
        return cAtFalse;

    if (!ProductCodeMatch(self))
        self->isEjected = cAtTrue;

    return self->isEjected;
    }

static AtLongRegisterAccess GlobalLongRegisterAccessCreate(AtDevice self)
    {
	AtUnused(self);
    /* Sub class must do */
    return NULL;
    }

static AtIterator AllModulesIteratorCreate(AtDevice self)
    {
    return AtDeviceModuleIteratorCreate(self);
    }

static void StatusClear(AtDevice self)
    {
    AtIterator moduleIterator = mMethodsGet(self)->AllModulesIteratorCreate(self);
    AtModule module;
    eBool enabled = AtPrintIsEnabled();

    /* Clear sticky of device */
    AtPrintEnable(cAtFalse);
    AtDeviceDebug(self);
    AtPrintEnable(enabled);

    /* Clear status of modules */
    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        AtModuleStatusClear(module);

    AtObjectDelete((AtObject)moduleIterator);
    }

static void AllHalsWriteOperationEnable(AtDevice self, eBool enable)
    {
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(self, core_i);
        AtHalWriteOperationEnable(hal, enable);
        }
    }

static void AllHalsReadOperationEnable(AtDevice self, eBool enable)
    {
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(self, core_i);
        AtHalReadOperationEnable(hal, enable);
        }
    }

static eBool WarmRestoreReadOperationDisabled(AtDevice self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet WarmRestoreStart(AtDevice self)
    {
    AllHalsWriteOperationEnable(self, cAtFalse);
    if (mMethodsGet(self)->WarmRestoreReadOperationDisabled(self))
        AllHalsReadOperationEnable(self, cAtFalse);

    self->warmRestoreStarted = cAtTrue;

    return cAtOk;
    }

static eBool WarmRestoreIsStarted(AtDevice self)
    {
    return self->warmRestoreStarted;
    }

static eAtRet WarmRestoreStop(AtDevice self)
    {
    eAtRet ret = cAtOk;
    eBool started = AtDeviceWarmRestoreIsStarted(self);

    AllHalsWriteOperationEnable(self, cAtTrue);
    if (mMethodsGet(self)->WarmRestoreReadOperationDisabled(self))
        AllHalsReadOperationEnable(self, cAtTrue);

    self->warmRestoreStarted = cAtFalse;

    if (started && mMethodsGet(self)->ShouldRestoreDatabaseOnWarmRestoreStop(self))
        ret |= AtDeviceRestore(self);

    return ret;
    }

static void HalReadFail(AtHal self, void *listener)
    {
    AtDevice device = (AtDevice)listener;
	AtUnused(self);
    device->needCheckEjected = cAtTrue;
    }

static void IpCoreHalSet(AtDevice self, uint8 coreId, AtHal hal)
    {
    m_halListener.ReadFail  = HalReadFail;
    m_halListener.WriteFail = NULL;

    AtIpCoreHalSet(AtDeviceIpCoreGet(self, coreId), hal);
    AtHalListenerAdd(hal, self, &m_halListener);
    }

static uint8 ModuleVersion(AtDevice self, eAtModule moduleId)
    {
	AtUnused(moduleId);
	AtUnused(self);
    return 0;
    }

static eBool AllCoresAreActivated(AtDevice self)
    {
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        if (!AtIpCoreIsActivated(core))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool WarmRestoreIsSupported(AtDevice self)
    {
    /* Not all products support this feature
     * A product wants to support this feature, it is necessary to make sure that
     * we can get enough information from the running FPGA */
    AtUnused(self);
    return cAtFalse;
    }

static void WarmRestoreFinish(AtDevice self)
    {
    self->inWarmRestore = cAtFalse;
    }

static eAtRet WarmRestore(AtDevice self)
    {
    AtIterator moduleIterator = AtDeviceModuleIteratorCreate(self);
    eAtRet ret = cAtOk;
    AtModule module;

    if (!mMethodsGet(self)->WarmRestoreIsSupported(self))
        return cAtErrorModeNotSupport;

    if (!AllCoresAreActivated(self))
        return cAtOk;

    self->inWarmRestore = cAtTrue;

    /* Re-setup all modules to have fresh database before restoring */
    AtDeviceAllModulesSetup(self);

    /* Iterate and restore all modules */
    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        ret |= AtModuleWarmRestore(module);

    AtObjectDelete((AtObject)moduleIterator);

    mMethodsGet(self)->WarmRestoreFinish(self);
    return ret;
    }

static AtEncapBinder EncapBinder(AtDevice self)
    {
    /* Sub class must know */
    AtUnused(self);
    return NULL;
    }

static eAtRet AllServicesDestroy(AtDevice self)
    {
    uint8 numModules = 0;
    uint8 module_i;
    const eAtModule *modules = AtDeviceAllSupportedModulesGet(self, &numModules);
    eAtRet ret = cAtOk;

    for (module_i = 0; module_i < numModules; module_i++)
        {
        AtModule module = AtDeviceModuleGet(self, modules[module_i]);
        if (module)
            ret |= AtModuleAllServicesDestroy(module);
        }

    return ret;
    }

static void ShortRegisterReadListenerCall(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    if ((self->registerListener) && (self->registerListener->ShortRead))
        self->registerListener->ShortRead(self, address, value, coreId);
    }

static void ShortRegisterWriteListenerCall(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    if ((self->registerListener) && (self->registerListener->ShortWrite))
        self->registerListener->ShortWrite(self, address, value, coreId);
    }

static void LongRegisterReadListenerCall(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId)
    {
    if ((self->registerListener) && (self->registerListener->LongRead))
        self->registerListener->LongRead(self, address, dataBuffer, length, moduleId, coreId);
    }

static void LongRegisterWriteListenerCall(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId)
    {
    if ((self->registerListener) && (self->registerListener->LongWrite))
        self->registerListener->LongWrite(self, address, dataBuffer, length, moduleId, coreId);
    }

static eAtRet RegisterEventListenerAdd(AtDevice self, tAtRegisterWriteEventListener *listener)
    {
    if (listener == NULL)
        return cAtErrorNullPointer;

    if (self->registerListener != NULL)
        return cAtErrorRsrcNoAvail;

    self->registerListener = listener;
    return cAtOk;
    }

static eAtRet RegisterEventListenerRemove(AtDevice self)
    {
    self->registerListener = NULL;
    return cAtOk;
    }

static eBool RegisterEventListenerIsExisted(AtDevice self)
    {
    return (self->registerListener) ? cAtTrue : cAtFalse;
    }

static eAtRet AllCoresReset(AtDevice self)
    {
    uint8 core_i;
    eAtRet ret = cAtOk;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        ret |= AtIpCoreSoftReset(core);
        }

    return ret;
    }

static eAtRet AllCoresAsyncReset(AtDevice self)
    {
    return AtDeviceAsyncAllCoresAsyncReset(self);
    }

static void DidDeleteAllManagedObjects(AtDevice self)
    {
    AtUnused(self);
    }

static eAtRet AllCoresTest(AtDevice self)
    {
    uint8 core_i;
    eAtRet ret = cAtOk;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        ret |= AtIpCoreTest(core);
        }

    return ret;
    }

static eAtRet Reset(AtDevice self)
    {
    /* Concrete may one to put more logic, but at this context, simply
     * hardware reset is enough */
    return mMethodsGet(self)->HwReset(self);
    }

static eAtRet AsyncReset(AtDevice self)
    {
    return mMethodsGet(self)->AsyncHwReset(self);
    }

static eAtRet HwReset(AtDevice self)
    {
    return mMethodsGet(self)->AllCoresReset(self);
    }

static eAtRet AsyncHwReset(AtDevice self)
    {
    return mMethodsGet(self)->HwReset(self);
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
    AtUnused(self);
    AtUnused(semId);
    return NULL;
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumSemControllersGet(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static AtThermalSensor ThermalSensorObjectCreate(AtDevice self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool ThermalSensorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPowerSupplySensor PowerSupplySensorObjectCreate(AtDevice self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool PowerSupplySensorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldResetBeforeInitializing(AtDevice self)
    {
    /* Just a few products need this, let them determine */
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedReset(AtDevice self)
    {
    if (AtDeviceInAccessible(self))
        return cAtFalse;

    /* Do not require */
    if (!mMethodsGet(self)->ShouldResetBeforeInitializing(self))
        return cAtFalse;

    /* Already did */
    if (self->resetDone)
        return cAtFalse;

    /* The FPGA may already be programmed and configured before */
    if (AllCoresAreActivated(self))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet ResetIfNecessary(AtDevice self)
    {
    eAtRet ret;

    if (!NeedReset(self))
        return cAtOk;

    ret = AtDeviceReset(self);
    if (ret == cAtOk)
        self->resetDone = cAtTrue;

    return ret;
    }

static eAtRet AsyncResetIfNecessary(AtDevice self)
    {
    eAtRet ret;

    if (!NeedReset(self))
        return cAtOk;

    ret = mMethodsGet(self)->AsyncReset(self);
    if (ret == cAtOk)
        self->resetDone = cAtTrue;

    return ret;
    }

static eAtRet ModuleReactivate(AtDevice self, uint32 moduleId)
    {
    uint8 core_i;
    eAtRet ret = cAtOk;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        ret |= AtIpCoreModuleReactivate(core, moduleId);
        }

    return ret;
    }

static eBool ModuleIsActive(AtDevice self, uint32 moduleId)
    {
    uint8 core_i;

    for (core_i = 0; core_i < AtDeviceNumIpCoresGet(self); core_i++)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        if (!AtIpCoreModuleIsActive(core, moduleId))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool ShouldDisableHwAccessBeforeDeleting(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void DisableHwAccess(AtDevice self)
    {
    uint8 core_i, numCores = AtDeviceNumIpCoresGet(self);

    for (core_i = 0; core_i < numCores; core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(self, core_i);
        AtHalReadOperationEnable(hal, cAtFalse);
        AtHalWriteOperationEnable(hal, cAtFalse);
        }
    }

static eAtRet WillDelete(AtDevice self)
    {
    if (mMethodsGet(self)->ShouldDisableHwAccessBeforeDeleting(self))
        DisableHwAccess(self);
    self->deleting = cAtTrue;
    return cAtOk;
    }

static eBool CoreAtIndexIsValid(AtArrayIterator self, uint32 coreIndex)
    {
	AtUnused(coreIndex);
	AtUnused(self);
    return cAtTrue;
    }

static AtObject CoreAtIndex(AtArrayIterator self, uint32 coreIndex)
    {
    AtIpCore *cores = self->array;
    return (AtObject)cores[coreIndex];
    }

static uint32 CoreCount(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count != -1)
        return (uint32)arrayIterator->count;

    arrayIterator->count = (int32)arrayIterator->capacity;
    return (uint32)arrayIterator->count;
    }

static const uint32 *ProductCodeRegisters(uint8 *numRegister)
    {
    static const uint32 cActiveCodeRegisters[]  = {0, 0xf00001};
    static const uint32 cStandbyCodeRegisters[] = {0xf00001, 0};

    /* In standby driver, it would be better to read the TOP register */
    if (AtDriverIsStandby())
        {
        *numRegister = mCount(cStandbyCodeRegisters);
        return cStandbyCodeRegisters;
        }

    /* Full access on active driver */
    *numRegister = mCount(cActiveCodeRegisters);
    return cActiveCodeRegisters;
    }

static char* FormatWithDeviceDescription(AtDevice self, const char *format)
    {
    static char m_FormatWithDeviceDesc[256];
    AtSnprintf(m_FormatWithDeviceDesc, 256, "%s: %s", AtObjectToString((AtObject)self), format);

    m_FormatWithDeviceDesc[253] = '\r';
    m_FormatWithDeviceDesc[254] = '\n';
    m_FormatWithDeviceDesc[255] = '\0';

    return m_FormatWithDeviceDesc;
    }

static eAtRet DiagnosticCheckEnable(AtDevice self, eBool enable)
    {
    self->diagnosticCheckDisabled = enable ? cAtFalse : cAtTrue;
    return cAtOk;
    }

static eBool DiagnosticCheckIsEnabled(AtDevice self)
    {
    return self->diagnosticCheckDisabled ? cAtFalse : cAtTrue;
    }

static AtSemController* AllSemControllersCreate(AtDevice self)
    {
    uint32 sem_i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 semNum = AtDeviceNumSemControllersGet(self);
    uint32 memorySize = semNum * sizeof(AtSemController);

    if (memorySize == 0)
        return NULL;

    /* Allocate memory to store SEM controllers */
    self->semControllers = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (self->semControllers == NULL)
        return NULL;
    mMethodsGet(osal)->MemInit(osal, self->semControllers, 0, memorySize);

    /* Create controllers */
    for (sem_i = 0; sem_i < semNum; sem_i++)
        self->semControllers[sem_i] = mMethodsGet(self)->SemControllerObjectCreate(self, sem_i);

    return self->semControllers;
    }

static eAtRet AllSemControllersDelete(AtDevice self)
    {
    uint32 sem_i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 semNum = AtDeviceNumSemControllersGet(self);

    if (self->semControllers == NULL)
        return cAtOk;

    /* Delete controllers */
    for (sem_i = 0; sem_i < semNum; sem_i++)
        {
        AtObjectDelete((AtObject)self->semControllers[sem_i]);
        self->semControllers[sem_i] = NULL;
        }

    /* And the memory holding them */
    mMethodsGet(osal)->MemFree(osal, self->semControllers);
    self->semControllers = NULL;

    return cAtOk;
    }

static AtSemController SemControllerGet(AtDevice self, uint32 semId)
    {
    if (self == NULL)
        return NULL;

    if (!mMethodsGet(self)->SemControllerIsSupported(self))
        return NULL;

    if (semId >= AtDeviceNumSemControllersGet(self))
        return NULL;

    /* Already created */
    if (self->semControllers)
        return self->semControllers[semId];

    /* Create all SEM controllers */
    if (AllSemControllersCreate(self) == NULL)
        return NULL;

    return self->semControllers[semId];
    }

static void CoreIteratorOverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_coreIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_coreAtArrayIteratorOverride, mMethodsGet(self), sizeof(m_coreAtArrayIteratorOverride));
        m_coreAtArrayIteratorOverride.DataAtIndexIsValid = CoreAtIndexIsValid;
        m_coreAtArrayIteratorOverride.DataAtIndex = CoreAtIndex;
        }
    mMethodsSet(self, &m_coreAtArrayIteratorOverride);
    }

static void CoreIteratorOverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_coreIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_coreAtIteratorOverride, mMethodsGet(iterator), sizeof(m_coreAtIteratorOverride));
        m_coreAtIteratorOverride.Count = CoreCount;
        }
    mMethodsSet(iterator, &m_coreAtIteratorOverride);
    }

static void CoreIteratorOverride(AtArrayIterator self)
    {
    CoreIteratorOverrideAtArrayIterator(self);
    CoreIteratorOverrideAtIterator(self);
    }

static eAtRet InterruptPinsDelete(AtDevice self)
    {
    uint32 pin_i;
    uint32 numPins = self->numInterruptPins;

    if (numPins == 0)
        return cAtOk;

    if (self->interruptPins == NULL)
        return cAtOk;

    /* Delete all created PINs */
    for (pin_i = 0; pin_i < numPins; pin_i++)
        {
        AtObjectDelete((AtObject)self->interruptPins[pin_i]);
        self->interruptPins[pin_i] = NULL;
        }

    /* And memory that holds them */
    AtOsalMemFree(self->interruptPins);
    self->interruptPins = NULL;
    self->numInterruptPins = 0;

    return cAtOk;
    }

static void CacheDelete(AtObject self)
    {
    void **cache = mMethodsGet((AtDevice)(self))->CacheMemoryAddress((AtDevice)(self));

    if (cache)
        {
        AtOsalMemFree(*cache);
        *cache = NULL;
        }
    }

static void Delete(AtObject self)
    {
    AtDevice device = (AtDevice)self;

    if(device->allEventListeners)
        {
        AtOsalMutexLock(device->listenerListMutex);
        AtListDeleteWithObjectHandler(device->allEventListeners, AtObjectDelete);
        AtOsalMutexUnLock(device->listenerListMutex);

        /* Delete private attribute */
        AtOsalMutexDestroy(device->listenerListMutex);
        }

    AtObjectDelete((AtObject)device->sskeyChecker);
    device->sskeyChecker = NULL;
    AllSemControllersDelete(device);
    AtObjectDelete((AtObject)device->thermalSensor);
    device->thermalSensor = NULL;
    AtObjectDelete((AtObject)device->powerSupplySensor);
    device->powerSupplySensor = NULL;

    mMethodsGet(device)->AllModulesDelete(device);
    DeleteAllCores(device);
    AtObjectDelete((AtObject)(device->defaultGlobalLongRegisterAccess));
    device->defaultGlobalLongRegisterAccess = NULL;
    mMethodsGet(device)->DidDeleteAllManagedObjects(device);
    AllListenersDelete(device);
    AtObjectDelete((AtObject)device->encapBinder);
    device->encapBinder = NULL;
    SerdesManagerDelete(device);
    AtObjectDelete((AtObject)device->diagUart);
    device->diagUart = NULL;
    InterruptPinsDelete(device);
    AtObjectDelete((AtObject)device->debugger);

    CacheDelete(self);
    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static AtIterator RestoredModulesIteratorCreate(AtDevice self)
    {
    return AtDeviceModuleIteratorCreate(self);
    }

static uint32 Restore(AtDevice self)
    {
    AtIterator moduleIterator;
    uint32 remained = 0;
    AtModule module;

    moduleIterator = mMethodsGet(self)->RestoredModulesIteratorCreate(self);
    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        remained += AtModuleRestore(module);
    AtObjectDelete((AtObject)moduleIterator);

    return remained;
    }

static eAtRet VersionNumberSet(AtDevice self, uint32 versionNumber)
    {
    AtUnused(self);
    return (versionNumber == 0) ? cAtOk : cAtErrorNotImplemented;
    }

static eAtRet BuiltNumberSet(AtDevice self, uint32 builtNumber)
    {
    AtUnused(self);
    return (builtNumber == 0) ? cAtOk : cAtErrorNotImplemented;
    }

static AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    AtUnused(self);
    /* Concrete class will determine if it supports or not */
    return NULL;
    }

static eBool HasRole(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RoleSet(AtDevice self, eAtDeviceRole role)
    {
    AtUnused(self);
    return (role == cAtDeviceRoleAuto) ? cAtOk : cAtErrorNotImplemented;
    }

static eAtDeviceRole RoleGet(AtDevice self)
    {
    AtUnused(self);
    return cAtDeviceRoleAuto;
    }

static eAtDeviceRole AutoRoleGet(AtDevice self)
    {
    AtUnused(self);
    return cAtDeviceRoleUnknown;
    }

static AtDevice ParentDeviceGet(AtDevice self)
    {
    AtUnused(self);
    return NULL;
    }

static uint8 NumSubDevices(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static AtDevice SubDeviceGet(AtDevice self, uint8 subDeviceIndex)
    {
    AtUnused(subDeviceIndex);
    return self;
    }

static AtDevice* AllSubDevicesGet(AtDevice self, uint8 *numSubDevices)
    {
    AtUnused(self);
    *numSubDevices = 0;
    return NULL;
    }

static AtSerdesManager SerdesManagerGet(AtDevice self)
    {
    return self->serdesManager;
    }

static eBool HwReady(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ModuleHwReady(AtDevice self, eAtModule moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return cAtTrue;
    }

static uint32 VersionNumberFromHwGet(AtDevice self)
    {
    return AtDeviceVersionNumber(self);
    }

static uint32 BuiltNumberFromHwGet(AtDevice self)
    {
    return AtDeviceBuiltNumber(self);
    }

static eBool ShouldCheckRegisterAccess(AtDevice self)
    {
    /* This is really useful when starting a new product. Turn this to cAtTrue
     * to enforce access checking logic */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AllModulesAsyncInit(AtDevice self)
    {
    return AtDeviceAsyncAllModulesInit(self);
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return AtDeviceAsyncInitMain(self);
    }

static AtUart DiagnosticUartObjectCreate(AtDevice self)
    {
    AtUnused(self);
    return NULL;
    }

static AtUart DiagnosticUartGet(AtDevice self)
    {
    if (self->diagUart == NULL)
        self->diagUart = mMethodsGet(self)->DiagnosticUartObjectCreate(self);
    return self->diagUart;
    }

static void PlatformSet(AtDevice self, uint32 targetPlatformProductCode)
    {
    self->targetPlatformProductCode = targetPlatformProductCode;
    }

static uint32 PlatformGet(AtDevice self)
    {
    return self->targetPlatformProductCode;
    }

static uint32 NumInterruptPins(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static AtInterruptPin InterruptPinGet(AtDevice self, uint32 pinId)
    {
    if (self->interruptPins)
        return self->interruptPins[pinId];
    return NULL;
    }

static AtInterruptPin InterruptPinObjectCreate(AtDevice self, uint32 pinId)
    {
    AtUnused(self);
    AtUnused(pinId);
    return NULL;
    }

static AtInterruptPin *InterruptPinsCreate(AtDevice self)
    {
    uint32 pin_i;
    uint32 numPins = mMethodsGet(self)->NumInterruptPins(self);
    uint32 memorySize = sizeof(AtInterruptPin) * numPins;
    AtInterruptPin *pins;

    /* Memory to hold all PINs */
    pins = AtOsalMemAlloc(memorySize);
    if (pins == NULL)
        return NULL;
    AtOsalMemInit(pins, 0, memorySize);

    /* Create all PINs */
    for (pin_i = 0; pin_i < numPins; pin_i++)
        {
        uint32 createdPin_i;
        AtInterruptPin newPin = mMethodsGet(self)->InterruptPinObjectCreate(self, pin_i);
        if (newPin)
            {
            pins[pin_i] = newPin;
            continue;
            }

        /* Clean up on failure */
        for (createdPin_i = 0; createdPin_i < pin_i; createdPin_i++)
            AtObjectDelete((AtObject)pins[createdPin_i]);
        AtOsalMemFree(pins);

        return NULL;
        }

    return pins;
    }

static eAtRet InterruptPinsSetup(AtDevice self)
    {
    uint32 numPins;

    if (self->interruptPins)
        InterruptPinsDelete(self);

    numPins = mMethodsGet(self)->NumInterruptPins(self);
    if (numPins == 0)
        return cAtOk;

    self->interruptPins = InterruptPinsCreate(self);
    if (self->interruptPins == NULL)
        return cAtErrorRsrcNoAvail;
    self->numInterruptPins = numPins;

    return cAtOk;
    }

static eBool ShouldCheckVersionInSimulation(AtDevice self)
    {
    /* So far, version is not checked in simulation */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AllModulesHwResourceCheck(AtDevice self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool ShouldCheckHwResource(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void PrbsEngineStop(AtPrbsEngine engine)
    {
    AtTimer timer = AtPrbsEngineGatingTimer(engine);

    if (timer && AtTimerIsRunning(timer))
        AtTimerStop(timer);

    if (AtPrbsEngineIsEnabled(engine))
        AtPrbsEngineEnable(engine, cAtFalse);
    }

static void DiagPrbsServiceStop(AtDevice self)
    {
    AtSerdesManager manager = AtDeviceSerdesManagerGet(self);
    uint32 serdesIdx, numSerdes = AtSerdesManagerNumSerdesControllers(manager);

    for (serdesIdx = 0; serdesIdx < numSerdes; serdesIdx++)
        {
        AtSerdesController serdes = AtSerdesManagerSerdesControllerGet(manager, serdesIdx);
        AtPrbsEngine prbs = AtSerdesControllerPrbsEngine(serdes);
        if (prbs)
            PrbsEngineStop(prbs);
        }
    }

static void AllHwRamTestStop( AtDevice device)
    {
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(device, cAtModuleRam);
    uint8 ddr_i, qdr_i;

    /* Test all of DDRs */
    for (ddr_i = 0; ddr_i < AtModuleRamNumDdrGet(ramModule); ddr_i++)
        {
        AtRam ram = AtModuleRamDdrGet(ramModule, ddr_i);
        if (AtRamHwTestIsStarted(ram))
            AtRamHwTestStop(ram);
        }

    for (qdr_i = 0; qdr_i < AtModuleRamNumQdrGet(ramModule); qdr_i++)
        {
        AtRam ram = AtModuleRamQdrGet(ramModule, qdr_i);
        if (AtRamHwTestIsStarted(ram))
            AtRamHwTestStop(ram);
        }
    }

static void DiagServiceStop(AtDevice self)
    {
    if (self == NULL)
        return;

    if (AtDeviceDiagnosticModeIsEnabled(self))
        {
        DiagPrbsServiceStop(self);
        AllHwRamTestStop(self);
        AtDeviceDiagnosticModeEnable(self, cAtFalse);
        }
    }

static eAtRet IpCoreHalDidSet(AtDevice self, AtIpCore core, AtHal hal)
    {
    uint32 version;
    AtUnused(core);
    AtUnused(hal);
    if (self->allModulesDidSetup)
        return cAtOk;

    /* Cache version number and setup the device */
    version = AtDeviceVersionNumber(self);
    if (version == 0)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation,
                    "Setup device with version number 0.0.0000 can introduce a potential crashed\r\n");

    return AtDeviceSetup(self);
    }

static AtQuerier QuerierGet(AtDevice self)
    {
    AtUnused(self);
    return AtDeviceSharedQuerier();
    }

static eAtRet Simulate(AtDevice self, eBool simulate)
    {
    if (self)
        self->isSimulated = simulate;
    return cAtOk;
    }

static eBool IsSimulated(AtDevice self)
    {
    if (AtDeviceInAccessible(self))
        return cAtFalse;

    if (self)
        return self->isSimulated;
    return cAtFalse;
    }

static void ListenerDidAdd(AtDevice self, tAtDeviceEventListener *listener, void *userData)
    {
    AtUnused(self);
    AtUnused(listener);
    AtUnused(userData);
    }

static uint32 AlarmGet(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryGet(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool AlarmIsSupported(AtDevice self, uint32 alarmType)
    {
    AtUnused(alarmType);
    AtUnused(self);

    /* Let concrete class determine */
    return cAtFalse;
    }

static AtDeviceEventListener EventListenerObjectInit(AtDeviceEventListener self, tAtDeviceEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initialize memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtDeviceEventListenerWrapper));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize private data */
    mMethodsGet(osal)->MemCpy(osal, &(self->listener), callbacks, sizeof(tAtDeviceEventListener));
    self->userData = userData;

    return self;
    }

static AtDeviceEventListener EventListenerNew(tAtDeviceEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDeviceEventListener newListener = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtDeviceEventListenerWrapper));

    if (newListener)
        return EventListenerObjectInit(newListener, callbacks, userData);

    return NULL;
    }

static eBool TwoListenersAreIdentical(AtDeviceEventListener self, tAtDeviceEventListener * listener)
    {
    if (self->listener.AlarmChangeStateWithUserData != listener->AlarmChangeStateWithUserData)
        return cAtFalse;

    return cAtTrue;
    }

static eBool ListenerIsExistingInDevice(AtDevice self, tAtDeviceEventListener *listener)
    {
    AtIterator iterator = AtListIteratorCreate(self->allEventListeners);
    AtDeviceEventListener aListener;
    eBool isExisting = cAtFalse;

    /* For all event listeners */
    while ((aListener = (AtDeviceEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (TwoListenersAreIdentical(aListener, listener))
            {
            isExisting = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    return isExisting;
    }

static eAtRet EventListenerAddWithUserData(AtDevice self, tAtDeviceEventListener *listener, void *userData)
    {
    eAtRet ret;
    AtDeviceEventListener newListener;

    if (self->allEventListeners == NULL)
        {
        self->listenerListMutex = AtOsalMutexCreate();
        self->allEventListeners = AtListCreate(cAtMaxNumEventListener);
        }

    if (ListenerIsExistingInDevice(self, listener))
        return cAtOk;

    newListener = EventListenerNew(listener, userData);
    if (newListener == NULL)
        return cAtErrorRsrcNoAvail;

    /* Only add a new listener */
    AtOsalMutexLock(self->listenerListMutex);
    ret = AtListObjectAdd(self->allEventListeners, (AtObject)newListener);
    AtOsalMutexUnLock(self->listenerListMutex);

    /* To give concrete class chance to have more further handling on this operation */
    mMethodsGet(self)->ListenerDidAdd(self, listener, userData);

    return ret;
    }

static uint32 SupportedInterruptMasks(AtDevice self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return none */
    return 0x0;
    }

static eAtRet InterruptMaskSet(AtDevice self, uint32 alarmMask, uint32 enableMask)
    {
    AtUnused(enableMask);
    AtUnused(alarmMask);
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtDevice self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return no mask */
    return 0;
    }

static eAtRet InterruptEnable(AtDevice self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet InterruptDisable(AtDevice self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eBool InterruptIsEnabled(AtDevice self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return no mask */
    return cAtFalse;
    }

static eBool SerdesResetIsSupported(AtDevice self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return no mask */
    return cAtTrue;
    }

static eBool RamIdIsValid(AtDevice self, uint32 ramId)
    {
    AtUnused(self);
    AtUnused(ramId);
    return cAtTrue;
    }

static void  DevAlarmInterruptProcess(AtDevice self)
    {
    AtUnused(self);
    }

static void **CacheMemoryAddress(AtDevice self)
    {
    return &(self->cache);
    }

static void CacheSetup(AtDevice self, void *cache)
    {
    tAtDeviceCache *devCache = cache;
    AtUnused(self);
    devCache->interruptMask = 0;
    }

static void *CacheCreate(AtDevice self)
    {
    void *newCache;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 cacheSize = mMethodsGet(self)->CacheSize(self);

    if (cacheSize == 0)
        return NULL;

    newCache = mMethodsGet(osal)->MemAlloc(osal, cacheSize);
    if (newCache == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, newCache, 0, cacheSize);
    CacheSetup(self, newCache);

    return newCache;
    }

static void *CacheGet(AtDevice self)
    {
    void **memory;

    if (AtDeviceAccessible(self))
        return NULL;

    memory = mMethodsGet(self)->CacheMemoryAddress(self);
    if (memory == NULL)
        return NULL;

    if ((*memory) == NULL)
        *memory = CacheCreate(self);

    return *memory;
    }

static uint32 CacheSize(AtDevice self)
    {
    AtUnused(self);
    return sizeof(tAtDeviceCache);
    }

static void CacheInterruptMaskSet(AtDevice self, uint32 alarmMask, uint32 enableMask)
    {
    tAtDeviceCache *cache = AtDeviceCacheGet(self);

    if (cache)
        {
        uint32 enabledBits  = alarmMask &  enableMask;
        uint32 disabledBits = alarmMask & (~enableMask);

        cache->interruptMask |=   enabledBits;
        cache->interruptMask &= (~disabledBits);
        }
    }

static uint32 CacheInterruptMaskGet(AtDevice self)
    {
    tAtDeviceCache *cache = AtDeviceCacheGet(self);
    return cache ? cache->interruptMask : 0;
    }

static void AlarmListenerCall(AtDeviceEventListener self, AtDevice device, uint32 changedAlarms, uint32 currentStatus)
    {
    if (self->listener.AlarmChangeStateWithUserData)
        self->listener.AlarmChangeStateWithUserData(device, changedAlarms, currentStatus, self->userData);
    }

static eBool ShouldCacheHalOnChannel(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet DoRoleSet(AtDevice self, eAtDeviceRole role)
    {
    eAtRet ret;

    if (!mDeviceIsValid(self))
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    if (!mMethodsGet(self)->HasRole(self))
        return (role == cAtDeviceRoleAuto) ? cAtOk : cAtErrorModeNotSupport;

    /* No change of role. */
    if (AtDeviceRoleGet(self) == role)
        return cAtOk;

    ret = mMethodsGet(self)->RoleSet(self, role);

    if (AtDriverShouldLog(mLogLevel(ret)))
        {
        AtDriverLogLock();
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret),
                                AtSourceLocation, "%s([%s], %u) = %s\r\n",
                                AtFunction, AtObjectToString((AtObject)self),
                                role, AtRet2String(ret));
        AtDriverLogUnLock();
        }

    /* Query if the new role was successfully applied. */
    if (role == AtDeviceRoleGet(self))
        AtDeviceRoleChangeNotify(self, role);

    return ret;
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        /* Save reference to super implementation */
        m_AtObjectMethods = mMethodsGet(object);

        /* Copy to reuse implementation of super class. But override some methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    /* Change behavior of super class level 1 */
    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtDevice self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ProductCodeGet);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, DriverGet);
        mMethodOverride(m_methods, AllSupportedModulesGet);
        mMethodOverride(m_methods, ModuleGet);
        mMethodOverride(m_methods, ModuleIsSupported);
        mMethodOverride(m_methods, ModuleCreate);
        mMethodOverride(m_methods, AllModulesInit);
        mMethodOverride(m_methods, AllModulesCreate);
        mMethodOverride(m_methods, AllModulesDelete);
        mMethodOverride(m_methods, DidDeleteAllManagedObjects);
        mMethodOverride(m_methods, IpCoreCreate);
        mMethodOverride(m_methods, DefaultIpCoreSetup);
        mMethodOverride(m_methods, NumIpCoresGet);
        mMethodOverride(m_methods, IpCoreGet);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, InterruptRestore);
        mMethodOverride(m_methods, LongReadOnCore);
        mMethodOverride(m_methods, LongWriteOnCore);
        mMethodOverride(m_methods, PeriodicProcess);
        mMethodOverride(m_methods, MemoryTest);
        mMethodOverride(m_methods, Version);
        mMethodOverride(m_methods, VersionNumber);
        mMethodOverride(m_methods, BuiltNumber);
        mMethodOverride(m_methods, SSKeyCheck);
        mMethodOverride(m_methods, NeedCheckSSKey);
        mMethodOverride(m_methods, SSKeyCheckerCreate);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, DebuggerFill);
        mMethodOverride(m_methods, LongRegisterAccessCreate);
        mMethodOverride(m_methods, ProductCodeRead);
        mMethodOverride(m_methods, IsEjected);
        mMethodOverride(m_methods, StatusClear);
        mMethodOverride(m_methods, GlobalLongRegisterAccessCreate);
        mMethodOverride(m_methods, WarmRestoreStart);
        mMethodOverride(m_methods, WarmRestoreStop);
        mMethodOverride(m_methods, WarmRestoreIsStarted);
        mMethodOverride(m_methods, WarmRestoreReadOperationDisabled);
        mMethodOverride(m_methods, ShouldRestoreDatabaseOnWarmRestoreStop);
        mMethodOverride(m_methods, ModuleVersion);
        mMethodOverride(m_methods, WarmRestore);
        mMethodOverride(m_methods, WarmRestoreIsSupported);
        mMethodOverride(m_methods, WarmRestoreFinish);
        mMethodOverride(m_methods, EncapBinder);
        mMethodOverride(m_methods, AllServicesDestroy);
        mMethodOverride(m_methods, AllCoresReset);
        mMethodOverride(m_methods, AllCoresAsyncReset);
        mMethodOverride(m_methods, DiagnosticModeEnable);
        mMethodOverride(m_methods, DiagnosticModeIsEnabled);
        mMethodOverride(m_methods, AllCoresTest);
        mMethodOverride(m_methods, RestoredModulesIteratorCreate);
        mMethodOverride(m_methods, ShouldResetBeforeInitializing);
        mMethodOverride(m_methods, ShowResetInfo);
        mMethodOverride(m_methods, Reset);
        mMethodOverride(m_methods, HwReset);
        mMethodOverride(m_methods, SemControllerObjectCreate);
        mMethodOverride(m_methods, ThermalSensorObjectCreate);
        mMethodOverride(m_methods, PowerSupplySensorObjectCreate);
        mMethodOverride(m_methods, SemControllerIsSupported);
        mMethodOverride(m_methods, NumSemControllersGet);
        mMethodOverride(m_methods, ThermalSensorIsSupported);
        mMethodOverride(m_methods, PowerSupplySensorIsSupported);
        mMethodOverride(m_methods, SimulationCode);
        mMethodOverride(m_methods, ShouldDisableHwAccessBeforeDeleting);
        mMethodOverride(m_methods, VersionNumberSet);
        mMethodOverride(m_methods, BuiltNumberSet);
        mMethodOverride(m_methods, SerdesManagerObjectCreate);
        mMethodOverride(m_methods, HwReady);
        mMethodOverride(m_methods, ModuleHwReady);
        mMethodOverride(m_methods, VersionNumberFromHwGet);
        mMethodOverride(m_methods, BuiltNumberFromHwGet);
        mMethodOverride(m_methods, AsyncInit);
        mMethodOverride(m_methods, AllModulesAsyncInit);
        mMethodOverride(m_methods, ShouldCheckRegisterAccess);
        mMethodOverride(m_methods, AsyncReset);
        mMethodOverride(m_methods, AsyncHwReset);
        mMethodOverride(m_methods, QuerierGet);
        mMethodOverride(m_methods, AllModulesHwResourceCheck);
        mMethodOverride(m_methods, ShouldCheckHwResource);
        mMethodOverride(m_methods, Accessible);
        mMethodOverride(m_methods, ShouldCheckVersionInSimulation);
        mMethodOverride(m_methods, AllModulesIteratorCreate);
        mMethodOverride(m_methods, ShouldEnableInterruptAfterProcessing);
        mMethodOverride(m_methods, SemControllerSerialize);

        /* Interrupt PINs */
        mMethodOverride(m_methods, NumInterruptPins);
        mMethodOverride(m_methods, InterruptPinGet);
        mMethodOverride(m_methods, InterruptPinObjectCreate);
        mMethodOverride(m_methods, InterruptPinDefaultTriggerMode);

        /* UART Function */
        mMethodOverride(m_methods, DiagnosticUartGet);
        mMethodOverride(m_methods, DiagnosticUartObjectCreate);
        
        /* Role */
        mMethodOverride(m_methods, RoleSet);
        mMethodOverride(m_methods, RoleGet);
        mMethodOverride(m_methods, HasRole);
        mMethodOverride(m_methods, AutoRoleGet);

        /* Card Protection */
        mMethodOverride(m_methods, ParentDeviceGet);
        mMethodOverride(m_methods, NumSubDevices);
        mMethodOverride(m_methods, SubDeviceGet);
        mMethodOverride(m_methods, AllSubDevicesGet);
        mMethodOverride(m_methods, ReadOnDdrOffload);
        mMethodOverride(m_methods, WriteOnDdrOffload);
        mMethodOverride(m_methods, Simulate);
        mMethodOverride(m_methods, IsSimulated);

        /* Alarm */
        mMethodOverride(m_methods, ListenerDidAdd);
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, AlarmIsSupported);
        mMethodOverride(m_methods, SupportedInterruptMasks);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, InterruptEnable);
        mMethodOverride(m_methods, InterruptDisable);
        mMethodOverride(m_methods, InterruptIsEnabled);
        mMethodOverride(m_methods, DevAlarmInterruptProcess);
        mMethodOverride(m_methods, CacheSize);
        mMethodOverride(m_methods, CacheMemoryAddress);
        mMethodOverride(m_methods, CacheGet);
        mMethodOverride(m_methods, SerdesResetIsSupported);
        mMethodOverride(m_methods, RamIdIsValid);
        mMethodOverride(m_methods, ShouldCacheHalOnChannel);

        mMethodOverride(m_methods, DeviceId);
        mMethodOverride(m_methods, DeviceIdToString);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDevice);
    }

AtDevice AtDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->driver      = driver;
    self->productCode = productCode;
    AtDeviceAsyncInitSetup(self);

    return self;
    }

uint32 AtDeviceIdGet(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->DeviceId(self);

    return 0;
    }

const char* AtDeviceIdToString(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->DeviceIdToString(self);

    return "";
    }

eAtRet AtDeviceSetup(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->Setup(self);

    return cAtError;
    }

AtLongRegisterAccess AtDeviceLongRegisterAccessCreate(AtDevice self, AtModule module)
    {
    if (self)
        return mMethodsGet(self)->LongRegisterAccessCreate(self, module);
    return NULL;
    }

eBool AtDeviceIsEjected(AtDevice self)
    {
    eBool ejected;

    if (self == NULL)
        return cAtTrue;

    if (!self->needCheckEjected)
        return cAtFalse;

    ejected = mMethodsGet(self)->IsEjected(self);
    if (!ejected)
        self->needCheckEjected = cAtFalse;

    return ejected;
    }

AtLongRegisterAccess AtDeviceGlobalLongRegisterAccess(AtDevice self)
    {
    if (self == NULL)
        return NULL;

    if (self->defaultGlobalLongRegisterAccess != NULL)
        return self->defaultGlobalLongRegisterAccess;

    if (self->installedGlobalLongRegisterAccess != NULL)
        return self->installedGlobalLongRegisterAccess;

    self->defaultGlobalLongRegisterAccess = mMethodsGet(self)->GlobalLongRegisterAccessCreate(self);
    return self->defaultGlobalLongRegisterAccess;
    }

eAtRet AtDeviceAllCoresReset(AtDevice self)
    {
    if (AtDeviceInAccessible(self))
        return cAtOk;

    if (self)
        return mMethodsGet(self)->AllCoresReset(self);

    return cAtErrorNullPointer;
    }

eAtRet AtDeviceAllCoresTest(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->AllCoresTest(self);
    return cAtErrorNullPointer;
    }

eAtRet AtDeviceAllModulesSetup(AtDevice self)
    {
    AtIterator moduleIterator = AtDeviceModuleIteratorCreate(self);
    uint32 ret = cAtOk;
    AtModule module;

    SerdesManagerDelete(self);

    /* Iterate and setup all modules */
    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        ret |= AtModuleSetup(module);

    AtObjectDelete((AtObject)moduleIterator);

    /* Setup default core for all of modules */
    mMethodsGet(self)->DefaultIpCoreSetup(self);

    SerdesManagerSetup(self);
    InterruptPinsSetup(self);

    return (eAtRet)ret;
    }

uint8 AtDeviceModuleVersion(AtDevice self, eAtModule moduleId)
    {
    if (self)
        return mMethodsGet(self)->ModuleVersion(self, moduleId);
    return 0x0;
    }

void AtDeviceLog(AtDevice self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...)
    {
    va_list args;
    if (self == NULL)
        return;

    AtDriverLogLock();

    va_start(args, format);
    AtDriverVaListLog(AtDriverSharedDriverGet(), level, file, line, FormatWithDeviceDescription(self, format), args);
    va_end(args);

    AtDriverLogUnLock();
    }

eAtRet AtDeviceSimulate(AtDevice self, eBool simulate)
    {
    if (self)
        return mMethodsGet(self)->Simulate(self, simulate);
    return cAtOk;
    }

eBool AtDeviceIsSimulated(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->IsSimulated(self);
    return cAtFalse;
    }

eAtRet AtDeviceTestbenchEnable(AtDevice self, eBool enabled)
    {
    if (self)
        self->testbenchEnabled = enabled;
    return cAtOk;
    }

eBool AtDeviceTestbenchIsEnabled(AtDevice self)
    {
    if (self)
        return self->testbenchEnabled;
    return cAtFalse;
    }

uint32 AtDeviceTestbenchDefaultTimeoutMs(void)
    {
    return 10 * 60 * 1000;
    }

eAtRet AtDeviceAllCoresActivate(AtDevice self, eBool activate)
    {
    if (self)
        return AllCoresActivate(self, activate);
    return cAtErrorNullPointer;
    }

eBool AtDeviceAllCoresAreActivated(AtDevice self)
    {
    if (self)
        return AllCoresAreActivated(self);
    return cAtFalse;
    }

eBool AtDeviceInWarmRestore(AtDevice self)
    {
    if (self)
        return self->inWarmRestore;

    return cAtFalse;
    }

AtEncapBinder AtDeviceEncapBinder(AtDevice self)
    {
    if (self == NULL)
        return NULL;

    if (self->encapBinder == NULL)
        self->encapBinder = mMethodsGet(self)->EncapBinder(self);
    return self->encapBinder;
    }

eAtRet AtDeviceAllServicesDestroy(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->AllServicesDestroy(self);
    return cAtErrorNullPointer;
    }

eAtRet AtDeviceGlobalLongRegisterAccessSet(AtDevice self, AtLongRegisterAccess registerAccess)
    {
    if ((self == NULL) || (registerAccess == NULL))
        return cAtErrorNullPointer;

    if (self->defaultGlobalLongRegisterAccess != NULL)
        AtObjectDelete((AtObject)self->defaultGlobalLongRegisterAccess);

    if (self->installedGlobalLongRegisterAccess != NULL)
        return cAtErrorDevBusy;

    self->installedGlobalLongRegisterAccess = registerAccess;
    return cAtOk;
    }

AtLongRegisterAccess AtDeviceInstalledGlobalLongRegisterAccessGet(AtDevice self)
    {
    return (self) ? self->installedGlobalLongRegisterAccess : NULL;
    }

eAtRet AtDeviceModuleInit(AtDevice self, eAtModule moduleId)
    {
    AtModule module = AtDeviceModuleGet(self, moduleId);
    eAtRet ret = cAtOk;

    if (module == NULL)
        return cAtOk;

    if (AtModuleHwIsReady(module))
        ret = AtModuleInit(module);

    if (ret != cAtOk)
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "Module %s init fail\r\n", AtObjectToString((AtObject)module));

    return ret;
    }

void AtDeviceReadNotify(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    if (self)
        ReadNotify(self, address, value, coreId);
    }

void AtDeviceWriteNotify(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    if (self)
        WriteNotify(self, address, value, coreId);
    }

void AtDeviceLongReadNotify(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId)
    {
    if (self)
        LongReadNotify(self, address, dataBuffer, numDwords, coreId);
    }

void AtDeviceLongWriteNotify(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId)
    {
    if (self)
        LongWriteNotify(self, address, dataBuffer, numDwords, coreId);
    }

void AtDeviceWriteOnCore(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    AtHal hal;

    if (self == NULL)
        return;

    hal = AtDeviceIpCoreHalGet((AtDevice)self, coreId);
    if (hal)
        {
        AtHalWrite(hal, address, value);
        if (AtDriverDebugIsEnabled())
            AtDeviceWriteNotify(self, address, value, coreId);
        }
    }

void AtDeviceRoleChangeNotify(AtDevice self, uint32 role)
    {
    if (self)
        RoleChangeNotify(self, role);
    }

eAtRet AtDeviceModuleReactivate(AtDevice self, uint32 moduleId)
    {
    if (self)
        return ModuleReactivate(self, moduleId);
    return cAtErrorNullPointer;
    }

eBool AtDeviceModuleIsActive(AtDevice self, uint32 moduleId)
    {
    if (self)
        return ModuleIsActive(self, moduleId);
    return cAtFalse;
    }

eAtRet AtDeviceWillDelete(AtDevice self)
    {
    if (self)
        return WillDelete(self);
    return cAtErrorNullPointer;
    }

void AtDeviceAllChannelsListenedDefectClear(AtDevice self)
    {
    AtIterator moduleIterator = AtDeviceModuleIteratorCreate(self);
    AtModule module;

    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        AtModuleAllChannelsListenedDefectClear(module);

    AtObjectDelete((AtObject)moduleIterator);
    }

/*
 * To get SEM controller
 *
 * @param self This device
 *
 * @return SEM controller object if it is supported.
 * @return NULL if it is not supported.
 */
AtSemController AtDeviceSemControllerGet(AtDevice self)
    {
    return SemControllerGet(self, 0);
    }

uint32 AtDeviceRestore(AtDevice self)
    {
    if (self)
        return Restore(self);
    return 0;
    }

eBool AtDeviceIsDeleting(AtDevice self)
    {
    return (eBool)(self ? self->deleting : cAtFalse);
    }

AtSerdesManager AtDeviceSerdesManagerSetup(AtDevice self)
    {
    if (self)
        return SerdesManagerSetup(self);
    return NULL;
    }

eBool AtDeviceHwReady(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->HwReady(self);
    return cAtFalse;
    }

eBool AtDeviceModuleHwReady(AtDevice self, eAtModule moduleId)
    {
    if (self)
        return mMethodsGet(self)->ModuleHwReady(self, moduleId);
    return cAtFalse;
    }

eAtRet AtDeviceResetIfNecessary(AtDevice self)
    {
    if (self)
        return ResetIfNecessary(self);
    return cAtErrorNullPointer;
    }

eAtRet AtDeviceAsyncResetIfNecessary(AtDevice self)
    {
    if (self)
        return AsyncResetIfNecessary(self);
    return cAtErrorNullPointer;
    }

eAtRet AtDeviceEntranceToInit(AtDevice self)
    {
    if (self)
        return EntranceToInit(self);
    return cAtErrorNullPointer;
    }

eAtRet AtDevicePostInit(AtDevice self)
    {
    return PostInit(self);
    }

eBool AtDeviceShouldCheckRegisterAccess(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->ShouldCheckRegisterAccess(self);
    return cAtFalse;
    }

void AtDevicePlatformSet(AtDevice self, uint32 targetPlatformProductCode)
    {
    if (self)
        PlatformSet(self, targetPlatformProductCode);
    }

uint32 AtDevicePlatformGet(AtDevice self)
    {
    if (self)
        return PlatformGet(self);
    return 0;
    }

eBool AtDeviceAccessible(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->Accessible(self);
    return cAtFalse;
    }

eBool AtDeviceInAccessible(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->Accessible(self) ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

eBool AtDeviceShouldPreventReconfigure(AtDevice self)
    {
    return AtDeviceAccessible(self) ? cAtTrue : cAtFalse;
    }

eBool AtDeviceAllFeaturesAvailableInSimulation(AtDevice self)
    {
    /* Only check in simulation mode */
    if (!AtDeviceIsSimulated(self))
        return cAtFalse;

    /* Version has to be checked to see if a feature is available */
    if (mMethodsGet(self)->ShouldCheckVersionInSimulation(self))
        return cAtFalse;

    return cAtTrue;
    }

void AtDeviceAllDiagServiceStop(AtDevice self)
    {
    DiagServiceStop(self);
    }

eBool AtDeviceShouldResetBeforeInitializing(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->ShouldResetBeforeInitializing(self);
    return cAtFalse;
    }

eAtRet AtDeviceIpCoreHalDidSet(AtDevice self, AtIpCore core, AtHal hal)
    {
    if (self)
        return IpCoreHalDidSet(self, core, hal);
    return cAtErrorNullPointer;
    }

eBool AtDeviceShouldEnableInterruptAfterProcessing(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->ShouldEnableInterruptAfterProcessing(self);
    return cAtFalse;
    }

uint32 AtDeviceSupportedInterruptMasks(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->SupportedInterruptMasks(self);
    return 0x0;
    }

void *AtDeviceCacheGet(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->CacheGet(self);
    return NULL;
    }

/* Call all alarm listeners of this device */
void AtDeviceAllAlarmListenersCall(AtDevice self, uint32 changedAlarms, uint32 currentStatus)
    {
    AtDeviceEventListener aListener;
    AtIterator iterator;

    if ((self == NULL) || (self->allEventListeners == NULL))
        return;

    /* Opps, nothing to be report. */
    if (changedAlarms == 0)
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(self->allEventListeners);
    while((aListener = (AtDeviceEventListener)AtIteratorNext(iterator)) != NULL)
        AlarmListenerCall(aListener, self, changedAlarms, currentStatus);

    AtObjectDelete((AtObject)iterator);
    }

eBool AtDeviceShouldCacheHalOnChannel(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->ShouldCacheHalOnChannel(self);
    return cAtFalse;
    }

eAtRet AtDeviceAllModulesHwResourceCheck(AtDevice self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!mMethodsGet(self)->ShouldCheckHwResource(self))
        return cAtOk;

    return mMethodsGet(self)->AllModulesHwResourceCheck(self);
    }

eBool AtDeviceShouldCheckHwResource(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->ShouldCheckHwResource(self);
    return cAtFalse;
    }

void AtDeviceShortWriteRegisterListenerCall(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    if (mDeviceIsValid(self))
          ShortRegisterWriteListenerCall(self, address, value, coreId);
    }

void AtDeviceShortReadRegisterListenerCall(AtDevice self, uint32 address, uint32 value, uint8 coreId)
    {
    if (mDeviceIsValid(self))
          ShortRegisterReadListenerCall(self, address, value, coreId);
    }

void AtDeviceLongWriteRegisterListenerCall(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId)
    {
    if (mDeviceIsValid(self))
        LongRegisterWriteListenerCall(self, address, dataBuffer, length, moduleId, coreId);
    }

void AtDeviceLongReadRegisterListenerCall(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId)
    {
    if (mDeviceIsValid(self))
        LongRegisterReadListenerCall(self, address, dataBuffer, length, moduleId, coreId);
    }

eAtRet AtDeviceRegisterEventListenerAdd(AtDevice self, tAtRegisterWriteEventListener *listener)
    {
    if (mDeviceIsValid(self))
        return RegisterEventListenerAdd(self, listener);
    return cAtError;
    }

eAtRet AtDeviceRegisterEventListenerRemove(AtDevice self)
    {
    if (mDeviceIsValid(self))
        return RegisterEventListenerRemove(self);
    return cAtError;
    }

eBool AtDeviceRegisterEventListenerIsExisted(AtDevice self)
    {
    if (mDeviceIsValid(self))
        return RegisterEventListenerIsExisted(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtDevice
 * @{
 */

/**
 * Initialize device
 *
 * @param self This device
 *
 * @return AT return code
 */
eAtRet AtDeviceInit(AtDevice self)
    {
    eAtRet ret = cAtOk;

    if (self == NULL)
        return cAtErrorNullPointer;

    mNoParamApiLogStart();
    ret = AtDeviceResetIfNecessary(self);
    if (ret == cAtOk)
        ret = mMethodsGet(self)->Init(self);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * To get product code
 *
 * @param self This device
 *
 * @return Product code
 */
uint32 AtDeviceProductCodeGet(AtDevice self)
    {
    mAttributeGet(ProductCodeGet, uint32, 0);
    }

/**
 * Get Device Driver that manages this AT Device
 *
 * @param self AT Device
 *
 * @return Device Driver
 */
AtDriver AtDeviceDriverGet(AtDevice self)
    {
    mNoParamObjectGet(DriverGet, AtDriver);
    }

/**
 * To get number of IP cores
 *
 * @param self This device
 *
 * @return Number of IP cores.
 */
uint8 AtDeviceNumIpCoresGet(AtDevice self)
    {
    mAttributeGet(NumIpCoresGet, uint8, 0);
    }

/**
 * Get IP core object by its ID
 *
 * @param self This device
 * @param coreId IP core ID
 *
 * @return IP core object if success or NULL if fail
 */
AtIpCore AtDeviceIpCoreGet(AtDevice self, uint8 coreId)
    {
    if (!mCoreIdIsValid(self, coreId))
        return NULL;

    mOneParamObjectGet(IpCoreGet, AtIpCore, coreId);
    }

/**
 * Set HAL for an IP Core
 *
 * @param self This device
 * @param coreId IP Core ID
 * @param hal HAL layer
 *
 * @return AT return code
 */
eAtRet AtDeviceIpCoreHalSet(AtDevice self, uint8 coreId, AtHal hal)
    {
    if ((self == NULL) || (!mCoreIdIsValid(self, coreId)))
        return cAtErrorInvlParm;
    IpCoreHalSet(self, coreId, hal);
    return cAtOk;
    }

/**
 * Get HAL that is installed to an IP Core
 *
 * @param self This device
 * @param coreId IP core
 *
 * @return HAL that is installed to an IP Core. NULL is returned on failue
 */
AtHal AtDeviceIpCoreHalGet(AtDevice self, uint8 coreId)
    {
    if ((self == NULL) || (!mCoreIdIsValid(self, coreId)))
        return NULL;

    return AtIpCoreHalGet(AtDeviceIpCoreGet(self, coreId));
    }

/**
 * Get module types that this device can support
 *
 * @param self AT Device
 * @param [out] numModules Number of modules this device can support
 *
 * @return Array of supported module types
 */
const eAtModule *AtDeviceAllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    if (self)
        return mMethodsGet(self)->AllSupportedModulesGet(self, numModules);

    return NULL;
    }

/**
 * Get a module of this device
 *
 * @param self AT Device
 * @param moduleId Module ID
 *
 * @return Module instance
 */
AtModule AtDeviceModuleGet(AtDevice self, eAtModule moduleId)
    {
    mOneParamObjectGet(ModuleGet, AtModule, moduleId);
    }

/**
 * Check if a module is supported
 *
 * @param self AT device
 * @param moduleId Module ID
 *
 * @retval cAtTrue if module is supported
 * @retval cAtFalse if module is not supported
 */
eBool AtDeviceModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    mOneParamAttributeGet(ModuleIsSupported, moduleId, eBool, cAtFalse);
    }

/**
 * Process interrupt that raises on global interrupt pin. In order to receive
 * interrupt event from running channels, application must:
 * - Call this API in interrupt context or polling
 * - Register event listener on channels it want to monitor.
 *
 * @param self This device
 */
void AtDeviceInterruptProcess(AtDevice self)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self);
    }

/**
 * Request the hardware re-update its interrupt tree base on current status of 
 * all channels.
 *
 * @param self This device
 *
 * @return AT return code
 */
eAtRet AtDeviceInterruptRestore(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->InterruptRestore(self);
    return cAtErrorNullPointer;
    }

/**
 * There may be some software engines need to run periodically for some purposes,
 * for example, monitor BER, monitor special events that are not supported by
 * hardware, ... For products that need software engines, application should
 * call this API to help them accomplish their tasks.
 *
 * @param self This device
 * @param periodInMs Calling period in milliseconds. It is required to calculate
 *        relative time.
 */
void AtDevicePeriodicProcess(AtDevice self, uint32 periodInMs)
    {
    if (self)
        mMethodsGet(self)->PeriodicProcess(self, periodInMs);
    }

/**
 * Default function to get product code. This function is implemented as a
 * utility function (not a method of AtDevice class). It uses default HAL
 * implementation to access local registers. If application has different way on
 * accessing register, it should not use this function. Refer AtProductCodeGetByHal()
 * to read product code by specific HAL
 *
 * @param baseAddressOfCore Base address of core
 * @see AtProductCodeGetByHal()
 */
uint32 AtProductCodeGet(uint32 baseAddressOfCore)
    {
    AtHal hal = AtHalNew(baseAddressOfCore);
    uint32 code = AtHalRead(hal, 0x0); /* Product code is usually stored at the local address 0x0 */
    AtHalDelete(hal);

    return code;
    }

/**
 * Get device product code by specified HAL
 *
 * @param hal HAL object used to read product code. It should be HAL object of first Core
 *
 * @return Device product code.
 */
uint32 AtProductCodeGetByHal(AtHal hal)
    {
    static const uint32 cTimeoutMs = 1000;
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, currentTime;
    AtOsal osal = AtSharedDriverOsalGet();

    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        uint8 i, numCodeRegister;
        const uint32 *codeRegisters = ProductCodeRegisters(&numCodeRegister);
        uint32 code;

        for (i = 0; i < numCodeRegister; i++)
            {
            code = AtHalRead(hal, codeRegisters[i]);
            if (AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), code))
                return code;
            }

        /* Calculate elapse time */
        mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
        elapseTimeMs = mMethodsGet(osal)->DifferenceTimeInMs(osal, &currentTime, &startTime);
        }

    return 0;
    }

/**
 * Get version directly from the hardware
 *
 * @param self This Device
 *
 * @return HW version
 */
uint32 AtDeviceVersionNumberFromHwGet(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->VersionNumberFromHwGet(self);
    return 0;
    }

/**
 * Get build number directly from the hardware
 *
 * @param self This Device
 *
 * @return Hardware built number
 */
uint32 AtDeviceBuiltNumberFromHwGet(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->BuiltNumberFromHwGet(self);
    return 0;
    }

/**
 * Create Iterator for cores
 *
 * @param self This device
 *
 * @return AtIterator object
 */
AtIterator AtDeviceCoreIteratorCreate(AtDevice self)
    {
    uint8 numCores;
    AtArrayIterator iterator;

    if (NULL == self)
        return NULL;

    numCores = mMethodsGet(self)->NumIpCoresGet(self);
    iterator = AtArrayIteratorNew(self->allIpCores, (uint32)numCores);

    if (NULL == iterator)
        return NULL;

    /* Override */
    CoreIteratorOverride(iterator);

    m_coreIteratorMethodsInit = 1;

    return (AtIterator)iterator;
    }

/**
 * Create modules iterator
 *
 * @param self This device
 *
 * @return AtIterator object
 */
AtIterator AtDeviceModuleIteratorCreate(AtDevice self)
    {
    if (self)
        return (AtIterator)AtDeviceModuleIteratorNew(self);

    return NULL;
    }

/**
 * Read a long register on specified core
 *
 * @param self This device
 * @param localAddress Local address
 * @param dataBuffer Buffer to hold read data
 * @param bufferLen Buffer length to prevent overflow
 * @param coreId Core ID
 *
 * @return Number dwords are read
 */
uint16 AtDeviceLongReadOnCore(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    uint16 numDwords;

    if (self == NULL)
    return 0;

    numDwords = mMethodsGet(self)->LongReadOnCore(self, localAddress, dataBuffer, bufferLen, coreId);
    AtDeviceLongReadNotify(self, localAddress, dataBuffer, numDwords, coreId);

    return numDwords;
    }

/**
 * Write a long register
 *
 * @param self This device
 * @param localAddress Local address
 * @param dataBuffer Buffer that holds data to write
 * @param bufferLen Buffer length, it is number of dwords need to be written
 * @param coreId Core ID
 *
 * @return Number of dwords are written
 */
uint16 AtDeviceLongWriteOnCore(AtDevice self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    uint16 numDwords;

    if (self == NULL)
    return 0;

    numDwords = mMethodsGet(self)->LongWriteOnCore(self, localAddress, dataBuffer, bufferLen, coreId);
    AtDeviceLongWriteNotify(self, localAddress, dataBuffer, numDwords, coreId);

    return numDwords;
    }

/**
 * Test memory of device
 *
 * @param self This device
 *
 * @return AT return code
 */
eAtRet AtDeviceMemoryTest(AtDevice self)
    {
    mNoParamCall(MemoryTest, eAtRet, cAtErrorNullPointer);
    }

/**
 * Get version description of device
 *
 * @param self This device
 *
 * @return Device version description
 *
 * @see AtDeviceVersionNumber
 */
const char *AtDeviceVersion(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->Version(self);

    return "Unknown";
    }

/**
 * Get version number
 *
 * @param self This device
 *
 * @return Version number
 *
 * @see AtDeviceVersion
 */
uint32 AtDeviceVersionNumber(AtDevice self)
    {
    mAttributeGet(VersionNumber, uint32, 0);
    }

/**
 * Get internal built number
 *
 * @param self This device
 *
 * @return Internal built number
 *
 * @see AtDeviceVersionNumber()
 */
uint32 AtDeviceBuiltNumber(AtDevice self)
    {
    mAttributeGet(BuiltNumber, uint32, 0);
    }

/**
 * Show device debug information
 *
 * @param self This device
 */
void AtDeviceDebug(AtDevice self)
    {
    if (!mDeviceIsValid(self))
        return;

    /* Device debug information */
    mMethodsGet(self)->Debug(self);
    }

/**
 * Get device debugger
 *
 * @param self This device
 */
AtDebugger AtDeviceDebuggerGet(AtDevice self)
    {
    return self ? self->debugger : NULL;
    }

/**
 * To clear status of all modules and channels
 *
 * @param self This device
 */
void AtDeviceStatusClear(AtDevice self)
    {
    if (mDeviceIsValid(self))
        mMethodsGet(self)->StatusClear(self);
    }

/**
 * Start warm restore process.
 *
 * @param self This device
 *
 * @return AT return code
 */
eAtRet AtDeviceWarmRestoreStart(AtDevice self)
    {
    mNoParamCall(WarmRestoreStart, eAtRet, cAtErrorNullPointer);
    }

/**
 * Stop warm restore process
 *
 * @param self This device
 *
 * @return AT return code
 */
eAtRet AtDeviceWarmRestoreStop(AtDevice self)
    {
    mNoParamCall(WarmRestoreStop, eAtRet, cAtErrorNullPointer);
    }

/**
 * Check if warm restore process is started
 *
 * @param self This device
 *
 * @retval cAtTrue if it is started
 * @retval cAtFalse if it is stopped
 */
eBool AtDeviceWarmRestoreIsStarted(AtDevice self)
    {
    mAttributeGet(WarmRestoreIsStarted, eBool, cAtFalse);
    }

/**
 * To check SSKey
 *
 * @param self This device
 * @return AT return code
 */
eAtRet AtDeviceSSKeyCheck(AtDevice self)
    {
    mNoParamCall(SSKeyCheck, eAtRet, cAtErrorNullPointer);
    }

/**
 * Restore all previous configuration base on Hardware scanning
 *
 * @param self This device
 * @return AT return code
 */
eAtRet AtDeviceWarmRestore(AtDevice self)
    {
    mNoParamCall(WarmRestore, eAtRet, cAtErrorNullPointer);
    }

/**
 * Enable/disable diagnostic mode. When the device is moved to diagnostic mode,
 * datapath may not run properly. After finishing diagnostic activity, the device
 * should be moved to normal mode.
 *
 * Most of products so far do not need this to call APIs before starting diagnostic.
 * And this API just does nothing. So, it would be best practice to always call
 * this API before starting diagnostic activity.
 *
 * By default, the device is in normal mode.
 *
 * @param self This device
 * @param enable Enabling.
 *               - cAtTrue to enable diagnostic mode.
 *               - cAtFalse to disable diagnostic mode and datapath can run in
 *                 this mode
 *
 * @return AT return code
 */
eAtRet AtDeviceDiagnosticModeEnable(AtDevice self, eBool enable)
    {
    mNumericalAttributeSet(DiagnosticModeEnable, enable);
    }

/**
 * To check if device is moved to diagnostic mode
 *
 * @param self This device
 *
 * @retval cAtTrue if device is in diagnostic mode
 * @retval cAtFalse if device is in normal mode
 */
eBool AtDeviceDiagnosticModeIsEnabled(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->DiagnosticModeIsEnabled(self);
    return cAtFalse;
    }

/**
 * Enable/disable diagnostic checking. This API is used only for bring up phase
 * when full core is not available. With diagnostic image, device initializing
 * would be always fail because of status checking.
 *
 * And not to block diagnostic activity and to bring the device database to normal
 * state, this API is used to disable status checking.
 *
 * @param self This device
 * @param enable Enable/disable status checking
 *               - cAtTrue to enable status checking
 *               - cAtFalse to disable status checking
 *
 * @return AT return code.
 */
eAtRet AtDeviceDiagnosticCheckEnable(AtDevice self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtRet, DiagnosticCheckEnable(self, enable));
    }

/**
 * Check if status checking on device initialization is enabled or not
 *
 * @param self This device
 *
 * @retval cAtTrue to enable
 * @retval cAtFalse to disable
 */
eBool AtDeviceDiagnosticCheckIsEnabled(AtDevice self)
    {
    if (self)
        return DiagnosticCheckIsEnabled(self);
    return cAtFalse;
    }

/**
 * Reset device
 *
 * @param self This device
 * @return AT return code
 */
eAtRet AtDeviceReset(AtDevice self)
    {
    eAtRet ret;

    if (self == NULL)
    return cAtErrorNullPointer;

    mNoParamApiLogStart();
    self->resetDone = cAtFalse;
    ret = mMethodsGet(self)->Reset(self);
    if (ret == cAtOk)
        self->resetDone = cAtTrue;
    AtDriverApiLogStop();

    return ret;
    }

/**
 * To get SEM controller by index ID
 *
 * @param self This device
 * @param semId SEM controller ID
 *
 * @return SEM controller object if it is supported.
 * @return NULL if it is not supported.
 */
AtSemController AtDeviceSemControllerGetByIndex(AtDevice self, uint8 semId)
    {
    return SemControllerGet(self, semId);
    }

/**
 * To get number of SEM controllers
 *
 * @param self This device
 *
 * @return number of SEM controllers
 */
uint8 AtDeviceNumSemControllersGet(AtDevice self)
    {
    if (self == NULL)
        return 0;

    if (mMethodsGet(self)->SemControllerIsSupported(self))
        return mMethodsGet(self)->NumSemControllersGet(self);

    return 0;
    }

/**
 * Check if SEM controller is supported
 *
 * @param self This device
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtDeviceSemControllerIsSupported(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->SemControllerIsSupported(self);
    return cAtFalse;
    }

/**
 * To get thermal sensor
 *
 * @param self This device
 *
 * @return Thermal sensor object if it is supported.
 * @return NULL if it is not supported.
 */
AtThermalSensor AtDeviceThermalSensorGet(AtDevice self)
    {
    if (self == NULL)
        return NULL;

    if (!mMethodsGet(self)->ThermalSensorIsSupported(self))
        return NULL;

    /* Already there */
    if (self->thermalSensor)
        return self->thermalSensor;

    /* Just create and initialize it */
    self->thermalSensor = mMethodsGet(self)->ThermalSensorObjectCreate(self);
    if (self->thermalSensor)
        AtSensorInit((AtSensor)self->thermalSensor);

    return self->thermalSensor;
    }

/**
 * Check if thermal sensor is supported
 *
 * @param self This device
 *
 * @retval cAtTrue if thermal sensor is supported
 * @retval cAtFalse thermal sensor is not supported
 */
eBool AtDeviceThermalSensorIsSupported(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->ThermalSensorIsSupported(self);
    return cAtFalse;
    }

/**
 * Add listener
 *
 * @param self This device
 * @param callback Callback which is implemented by application
 * @param userData Userdata which will be input when callback is called
 *
 * @return AT return code
 */
eAtRet AtDeviceListenerAdd(AtDevice self, const tAtDeviceListener *callback, void *userData)
    {
    if (self)
        return ListenerAdd(self, callback, userData);
    return cAtErrorNullPointer;
    }

/**
 * Remove listener
 *
 * @param self This device
 * @param callback Callback to remove
 *
 * @return AT return code
 */
eAtRet AtDeviceListenerRemove(AtDevice self, const tAtDeviceListener *callback)
    {
    if (self)
        return ListenerRemove(self, callback);
    return cAtErrorNullPointer;
    }

/**
 * To get power supply sensor
 *
 * @param self This device
 *
 * @return Power supply sensor object if it is supported.
 * @return NULL if it is not supported.
 */
AtPowerSupplySensor AtDevicePowerSupplySensorGet(AtDevice self)
    {
    if (self == NULL)
        return NULL;

    if (!mMethodsGet(self)->PowerSupplySensorIsSupported(self))
        return NULL;

    /* Already created */
    if (self->powerSupplySensor)
        return self->powerSupplySensor;

    /* Just create and initialize it */
    self->powerSupplySensor = mMethodsGet(self)->PowerSupplySensorObjectCreate(self);
    if (self->powerSupplySensor)
        AtSensorInit((AtSensor)self->powerSupplySensor);

    return self->powerSupplySensor;
    }

/**
 * Set version number. This is used when the SDK cannot access hardware.
 *
 * @param self This device
 * @param versionNumber Version number
 *
 * @return AT return code
 */
eAtRet AtDeviceVersionNumberSet(AtDevice self, uint32 versionNumber)
    {
    mNumericalAttributeSet(VersionNumberSet, versionNumber);
    }

/**
 * Set hardware built number. This is used when the SDK cannot access hardware.
 *
 * @param self This device
 * @param builtNumber Hardware built number
 *
 * @return AT return code
 */
eAtRet AtDeviceBuiltNumberSet(AtDevice self, uint32 builtNumber)
    {
    mNumericalAttributeSet(BuiltNumberSet, builtNumber);
    }

/**
 * Get SERDES manager
 *
 * @param self This device
 * @return SERDES manager or NULL if not support
 */
AtSerdesManager AtDeviceSerdesManagerGet(AtDevice self)
    {
    if (self)
        return SerdesManagerGet(self);
    return NULL;
    }
    
/**
 * Get diagnostic UART
 *
 * @param self This device
 *
 * @return UART
 */
AtUart AtDeviceDiagnosticUartGet(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->DiagnosticUartGet(self);
    return NULL;
    }

/**
 * Set device role
 *
 * @param self This device
 * @param role @ref eAtDeviceRole "Device role"
 *
 * @return AT return code.
 */
eAtRet AtDeviceRoleSet(AtDevice self, eAtDeviceRole role)
    {
    eAtRet ret;

    mOneParamApiLogStart(role);
    ret = DoRoleSet(self, role);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get device role
 *
 * @param self This device
 * @return @ref eAtDeviceRole "Device Role"
 */
eAtDeviceRole AtDeviceRoleGet(AtDevice self)
    {
    if (self == NULL)
        return cAtDeviceRoleUnknown;

    if (mMethodsGet(self)->HasRole(self))
        return mMethodsGet(self)->RoleGet(self);

    return cAtDeviceRoleAuto;
    }

/**
 * Check if device has role
 *
 * @param self This device
 *
 * @retval cAtTrue if it has
 * @retval cAtFalse if it does not have
 */
eBool AtDeviceHasRole(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->HasRole(self);
    return cAtFalse;
    }

/**
 * Get role description
 *
 * @param role Role
 *
 * @return Role description string
 */
const char *AtDeviceRole2String(eAtDeviceRole role)
    {
    switch (role)
        {
        case cAtDeviceRoleActive : return "active";
        case cAtDeviceRoleStandby: return "standby";
        case cAtDeviceRoleAuto   : return "auto";

        case cAtDeviceRoleUnknown:
        default:
            return "unknown";
        }
    }

/**
 * Get automatic device role. It is applicable when device role is set to
 * cAtDeviceRoleAuto.
 *
 * @param self This device
 * @return @ref eAtDeviceRole "Device Role"
 */
eAtDeviceRole AtDeviceAutoRoleGet(AtDevice self)
    {
    if (self == NULL)
        return cAtDeviceRoleUnknown;

    if (AtDeviceRoleGet(self) != cAtDeviceRoleAuto)
        return cAtDeviceRoleUnknown;

    return mMethodsGet(self)->AutoRoleGet(self);
    }

/**
 * Get number of interrupt PINs device can support
 *
 * @param self This device
 *
 * @return Number of interrupt PINs
 */
uint32 AtDeviceNumInterruptPins(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->NumInterruptPins(self);
    return 0;
    }

/**
 * Get an interrupt PIN
 *
 * @param self This device
 * @param pinId Interrupt PIN ID
 *
 * @return Interrupt PIN object
 */
AtInterruptPin AtDeviceInterruptPinGet(AtDevice self, uint32 pinId)
    {
    if (self == NULL)
        return NULL;

    if (pinId < AtDeviceNumInterruptPins(self))
        return mMethodsGet(self)->InterruptPinGet(self, pinId);

    return NULL;
    }

/**
 * Reset device asynchronously. Application may need to call periodically until return code is
 * different with cAtErrorAgain.
 *
 * @param self This device
 * @return cAtErrorAgain This API needs to be called again
 * @retval cAtOk The device is fully reset with no errors.
 * @retval eAtRet Other error codes if device reset is stopped with error.
 */
eAtRet AtDeviceAsyncReset(AtDevice self)
    {
    if (self)
        {
        eAtRet ret;
        AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtFalse, AtSourceLocation, NULL);
        ret = mMethodsGet(self)->AsyncReset(self);
        if ((ret == cAtRetNeedDelay) && (!AtDeviceShouldReturnNeedDelay(self)))
            ret = cAtErrorAgain;

        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * Get querier object to query problems or other things from device.
 *
 * @param self This device
 *
 * @return Querier object.
 */
AtQuerier AtDeviceQuerierGet(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->QuerierGet(self);
    return NULL;
    }

/**
 * Create debugger
 *
 * @param self This device
 *
 * @return Device instance
 */
AtDebugger AtDeviceDebuggerCreate(AtDevice self)
    {
    if (self)
        return DebuggerCreate(self);
    return NULL;
    }

/**
 * Delete debugger
 *
 * @param self This device
 *
 * @return Device instance
 */
void AtDeviceDebuggerDelete(AtDevice self)
    {
    if (self)
        DebuggerDelete(self);
    }

/**
 * Get the parent device.
 *
 * @param self This device
 * @return The parent device
 */
AtDevice AtDeviceParentDeviceGet(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->ParentDeviceGet(self);
    return NULL;
    }

/**
 * Get number of sub devices.
 *
 * @param self This device
 * @return A number of its sub-devices
 */
uint8 AtDeviceNumSubDeviceGet(AtDevice self)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->NumSubDevices(self);
    }

/**
 * Get a sub device at an index.
 *
 * @param self This device
 * @param subDeviceId Index of the sub-device application wants to retrieve.
 *
 * @return A sub device at the index
 */
AtDevice AtDeviceSubDeviceGet(AtDevice self, uint8 subDeviceId)
    {
    if (self == NULL)
        return NULL;

    return mMethodsGet(self)->SubDeviceGet(self, subDeviceId);
    }

/**
 * Get all sub devices of this device.
 *
 * @param self This device
 * @param numSubDevice Output number of sub devices
 *
 * @return An array of sub devices
 */
AtDevice *AtDeviceAllSubDevicesGet(AtDevice self, uint8 *numSubDevices)
    {
    if (self == NULL)
        return NULL;
    return mMethodsGet(self)->AllSubDevicesGet(self, numSubDevices);
    }

/**
 * Read a DDR offload address on a specified core IP
 *
 * @param self This device
 * @param subDeviceId A sub-device ID
 * @param ddrAddress DDR address
 * @param dataBuffer Buffer to hold read data
 * @param bufferLen Buffer length to prevent overflow
 * @param coreId Core ID
 *
 * @return Number dwords are read
 */
uint16 AtDeviceReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    if (self)
        return mMethodsGet(self)->ReadOnDdrOffload(self, subDeviceId, ddrAddress, dataBuffer, bufferLen, coreId);
    return 0;
    }

/**
 * Write a DDR offload address on a specified core IP
 *
 * @param self This device
 * @param subDeviceId A sub-device ID
 * @param ddrAddress DDR address
 * @param regAddress Register address to write
 * @param dataBuffer Buffer that holds data to write
 * @param bufferLen Buffer length, it is number of dwords needs to be written
 * @param coreId Core ID
 *
 * @return Number of dwords are written
 */
uint16 AtDeviceWriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId)
    {
    uint16 numDwords;

    if (self == NULL)
        return 0;

    numDwords = mMethodsGet(self)->WriteOnDdrOffload(self, subDeviceId, ddrAddress, regAddress, dataBuffer, bufferLen, coreId);

    return numDwords;
    }

/**
 * Add event listener with associated user data. When there is any alarm or OAM
 * message come, corresponding interface will be called and user data will be
 * input when callback get called.
 *
 * Application just implemented functions defined in event listener interface in
 * order to receiving event.
 *
 * @param self This device
 * @param listener Event listener
 * @param userData User data that will be input when callback is called.
 *
 * @return AT return code
 */
eAtRet AtDeviceEventListenerAddWithUserData(AtDevice self, tAtDeviceEventListener *listener, void *userData)
    {
    if (self)
        return EventListenerAddWithUserData(self, listener, userData);
    return cAtErrorNullPointer;
    }

/**
 * Remove event listener
 *
 * @param self This device
 * @param listener Event listener
 *
 * @return AT return code
 */
eAtRet AtDeviceEventListenerRemove(AtDevice self, tAtDeviceEventListener *listener)
    {
    /* Search to find listener match with input information */
    AtDeviceEventListener aListener;
    AtIterator iterator;

    if (!self)
        return cAtErrorNullPointer;

    if (self->allEventListeners == NULL)
        return cAtOk;

    iterator = AtListIteratorCreate(self->allEventListeners);

    AtOsalMutexLock(self->listenerListMutex);

    /* For all event listeners */
    while((aListener = (AtDeviceEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (!TwoListenersAreIdentical(aListener, listener))
            continue;

        /* Remove current object out of list */
        AtIteratorRemove(iterator);

        /* Delete listener object */
        AtObjectDelete((AtObject)aListener);
        }

    AtOsalMutexUnLock(self->listenerListMutex);

    AtObjectDelete((AtObject)iterator);
    return cAtOk;
    }

/**
 * Get alarm of this device
 *
 * @param self This device
 *
 * @return Current alarms.
 *         masks. Refer:
 *         - @ref eAtDeviceAlarmType "Device alarm types"
 *         This API ORed all of happening alarms and return
 */
uint32 AtDeviceAlarmGet(AtDevice self)
    {
    if (AtDeviceIsEjected(self))
        return 0;

    return mMethodsGet(self)->AlarmGet(self);
    }

/**
 * Get alarm interrupt change status. When there is any change in alarm from clear to
 * raise or vice versa from raise to clear, corresponding alarm bit is set to 1
 * and kept that state until AtDeviceAlarmInterruptClear() is called. If
 * interrupt is enabled by AtDeviceInterruptMaskSet(), the global interrupt pin
 * will be triggered to let CPU know
 *
 * @param self This device
 *
 * @return Alarm interrupt change status.
 *         masks. Refer:
 *         - @ref eAtDeviceAlarmType "Device alarm types"
 *         This API ORed all of alarms that changed their state in the past
 */
uint32 AtDeviceAlarmInterruptGet(AtDevice self)
    {
    if (AtDeviceIsEjected(self))
        return 0;

    return mMethodsGet(self)->AlarmHistoryGet(self);
    }

/**
 * Clear alarm history
 *
 * @param self This device
 *
 * @return Alarm history before clearing
 */
uint32 AtDeviceAlarmInterruptClear(AtDevice self)
    {
    if (AtDeviceIsEjected(self))
        return 0;

    return mMethodsGet(self)->AlarmHistoryClear(self);
    }

/**
 * Check if an alarm type is supported
 *
 * @param self This device
 * @param alarmType Alarm to check. Channels of each module have predefined
 *        alarm types
 * @return cAtTrue if alarm is supported or cAtFalse if it is not supported
 */
eBool AtDeviceAlarmIsSupported(AtDevice self, uint32 alarmType)
    {
    mOneParamAttributeGet(AlarmIsSupported, alarmType, eBool, cAtFalse);
    }

/**
 * Set interrupt mask.
 *
 * @param self This device
 * @param alarmMask Mask of defects that will be configured
 * @param enableMask Interrupt mask of each defect
 *
 * @return AT return code.
 *
 * @note Refer:
 *       - @ref eAtDeviceAlarmType "Device alarm types"
 */
eAtRet AtDeviceInterruptMaskSet(AtDevice self, uint32 alarmMask, uint32 enableMask)
    {
    uint32 unsupportedMasks;

    if (alarmMask == 0)
        return cAtOk;

    if (!self)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    unsupportedMasks = ~AtDeviceSupportedInterruptMasks(self);
    unsupportedMasks &= (alarmMask & enableMask);
    if (unsupportedMasks)
        {
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation,
                     "does not support interrupt mask 0x%x\r\n", unsupportedMasks);
        return cAtErrorModeNotSupport;
        }

    mTwoParamsAttributeSetWithCache(InterruptMaskSet, alarmMask, enableMask);
    }

/**
 * Get interrupt mask
 *
 * @param self This device
 *
 * @return Interrupt mask
 */
uint32 AtDeviceInterruptMaskGet(AtDevice self)
    {
    mAttributeGetWithCache(InterruptMaskGet, uint32, 0);
    }

/**
 * Check if an interrupt mask is supported or not.
 *
 * @param self This device
 * @param alarmMask Mask of defects that will be configured
 *
 * @retval cAtTrue if the interrupt mask is supported.
 *         cAtFalse if the interrupt mask is not supported.
 *
 * @note Refer:
 *       - @ref eAtDeviceAlarmType "Device alarm types"
 */
eBool AtDeviceInterruptMaskIsSupported(AtDevice self, uint32 alarmMask)
    {
    if (!self)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    if (alarmMask == 0)
        return cAtTrue;

    if (alarmMask & AtDeviceSupportedInterruptMasks(self))
        return cAtTrue;

    return cAtFalse;
    }

/**
 * Enable interrupt.
 *
 * @param self This device
 *
 * @return AT return code.
 */
eAtRet AtDeviceInterruptEnable(AtDevice self)
    {
    mNoParamCall(InterruptEnable, eAtRet, cAtErrorObjectNotExist);
    }

/**
 * Disable interrupt.
 *
 * @param self This device
 *
 * @return AT return code.
 */
eAtRet AtDeviceInterruptDisable(AtDevice self)
    {
    mNoParamCall(InterruptDisable, eAtRet, cAtErrorObjectNotExist);
    }

/**
 * Get interrupt enabled state
 *
 * @param self This device
 *
 * @return Interrupt enabled state
 */
eBool AtDeviceInterruptIsEnabled(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->InterruptIsEnabled(self);

    return cAtFalse;
    }

eBool AtDeviceSerdesResetIsSupportted(AtDevice self)
    {
    if (self)
        return mMethodsGet(self)->SerdesResetIsSupported(self);

    return cAtFalse;
    }

eBool AtDeviceRamIdIsValid(AtDevice self, uint32 ramId)
    {
    if (self)
        return mMethodsGet(self)->RamIdIsValid(self, ramId);
    return cAtFalse;
    }

/**
 * @}
 */

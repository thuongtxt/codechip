/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtInterruptPinInternal.h
 * 
 * Created Date: Oct 1, 2016
 *
 * Description : Interrupt PIN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATINTERRUPTPININTERNAL_H_
#define _ATINTERRUPTPININTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDriverInternal.h"
#include "AtInterruptPin.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtInterruptPinMethods
    {
    eAtRet (*Init)(AtInterruptPin self);

    /* Trigger mode */
    eAtRet (*TriggerModeSet)(AtInterruptPin self, eAtTriggerMode triggerMode);
    eAtTriggerMode (*TriggerModeGet)(AtInterruptPin self);

    /* Trigger */
    eAtRet (*Trigger)(AtInterruptPin self, eBool triggered);
    eBool (*IsTriggered)(AtInterruptPin self);
    eBool (*IsAsserted)(AtInterruptPin self);
    }tAtInterruptPinMethods;

typedef struct tAtInterruptPin
    {
    tAtObject super;
    const tAtInterruptPinMethods *methods;

    /* Private */
    AtDevice device;
    uint32 id;
    }tAtInterruptPin;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptPin AtInterruptPinObjectInit(AtInterruptPin self, AtDevice device, uint32 pinId);

eAtRet AtInterruptPinInit(AtInterruptPin self);

uint32 AtInterruptPinRead(AtInterruptPin self, uint32 regAddr);
void AtInterruptPinWrite(AtInterruptPin self, uint32 regAddr, uint32 value);

#ifdef __cplusplus
}
#endif
#endif /* _ATINTERRUPTPININTERNAL_H_ */


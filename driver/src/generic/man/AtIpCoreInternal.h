/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : AT Device
 * 
 * File        : AtIpCoreInternal.h
 * 
 * Created Date: Aug 6, 2012
 *
 * Author      : namnn
 * 
 * Description : IP Core
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIPCOREINTERNAL_H_
#define _ATIPCOREINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtHal.h"
#include "AtList.h"
#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtIpCoreMethods
    {
    eAtRet (*HalSet)(AtIpCore self, AtHal hal);
    AtHal (*HalGet)(AtIpCore self);

    uint8 (*IdGet)(AtIpCore self);
    AtDevice (*DeviceGet)(AtIpCore self);
    eAtRet (*InterruptEnable)(AtIpCore self, eBool enable);
    eBool (*InterruptIsEnabled)(AtIpCore self);
    eAtRet (*SemInterruptEnable)(AtIpCore self, eBool isEnabled);
    eBool  (*SemInterruptIsEnabled)(AtIpCore self);
    eAtRet (*SysmonInterruptEnable)(AtIpCore self, eBool isEnabled);
    eBool  (*SysmonInterruptIsEnabled)(AtIpCore self);

    /* Debug only. */
    uint32 (*InterruptCountRead2Clear)(AtIpCore self, eBool clear);
    uint32 (*InterruptContiguousCountRead2Clear)(AtIpCore self, eBool clear);
    uint32 (*InterruptMaxProcessingTimeInUsRead2Clear)(AtIpCore self, eBool clear);
    uint32 (*InterruptLastProcessingTimeInUsRead2Clear)(AtIpCore self, eBool clear);
    void (*Debug)(AtIpCore self);

    /* Internal methods */
    eAtRet (*Init)(AtIpCore self);
    eAtRet (*Test)(AtIpCore self);
    eAtRet (*SoftReset)(AtIpCore self);
    eAtRet (*AsyncSoftReset)(AtIpCore self);
    eAtRet (*AsyncReset)(AtIpCore self);
    eAtRet (*Activate)(AtIpCore self, eBool activate);
    eBool (*IsActivated)(AtIpCore self);
    void (*HalConfigure)(AtIpCore self, AtHal hal);
    eAtRet (*ModuleReactivate)(AtIpCore self, uint32 moduleId);
    eBool (*ModuleIsActive)(AtIpCore self, uint32 moduleId);
    }tAtIpCoreMethods;

/* Representation */
typedef struct tAtIpCore
    {
    tAtObject super;
    const tAtIpCoreMethods *methods;

    /* Private data */
    AtDevice device;
    uint8 coreId;
    AtHal hal;
    AtList listeners;
    }tAtIpCore;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIpCore AtIpCoreObjectInit(AtIpCore self, uint8 coreId, AtDevice device);

eAtRet AtIpCoreActivate(AtIpCore self, eBool activate);
eBool AtIpCoreIsActivated(AtIpCore self);
eAtRet AtIpCoreSoftReset(AtIpCore self);
eAtRet AtIpCoreAsyncSoftReset(AtIpCore self);
eAtRet AtIpCoreAsyncReset(AtIpCore self);
eAtRet AtIpCoreTest(AtIpCore self);
eAtRet AtIpCoreModuleReactivate(AtIpCore self, uint32 moduleId);
eBool AtIpCoreModuleIsActive(AtIpCore self, uint32 moduleId);

AtHal IpCoreHalGet(AtIpCore self);
uint32 AtIpCoreRead(AtIpCore self, uint32 address);
void AtIpCoreWrite(AtIpCore self, uint32 address, uint32 value);

void AtIpCoreInterruptHappenedNotify(AtIpCore self);
void AtIpCoreInterruptResolvedNotify(AtIpCore self);
uint32 AtIpCoreInterruptCountRead2Clear(AtIpCore self, eBool clear);
uint32 AtIpCoreInterruptContiguousCountRead2Clear(AtIpCore self, eBool clear);
uint32 AtIpCoreInterruptMaxProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear);
uint32 AtIpCoreInterruptLastProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear);

#ifdef __cplusplus
}
#endif
#endif /* _ATIPCOREINTERNAL_H_ */


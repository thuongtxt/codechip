/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : AT Device
 *
 * File        : AtIpCore.c
 *
 * Created Date: Aug 6, 2012
 *
 * Author      : namnn
 *
 * Description : IP Core
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtIpCoreInternal.h"
#include "AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mIpCoreIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtIpCoreListenerWrapper
    {
    const tAtIpCoreListener *callback;
    void *userData;
    }tAtIpCoreListenerWrapper;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIpCoreMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void HalConfigure(AtIpCore self, AtHal hal)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    eBool accessEnabled = (AtDriverRoleGet(driver) == cAtDriverRoleActive) ? cAtTrue : cAtFalse;
    AtUnused(self);
    AtHalReadOperationEnable(hal, accessEnabled);
    AtHalWriteOperationEnable(hal, accessEnabled);
    }

static eAtRet HalSet(AtIpCore self, AtHal hal)
    {
    /* Internal modules and channels may cache HAL object. So changing HAL
     * object is not allowed */
    if (self->hal)
        return cAtErrorModeNotSupport;

    if (hal)
        mMethodsGet(self)->HalConfigure(self, hal);
    self->hal = hal;

    return AtDeviceIpCoreHalDidSet(AtIpCoreDeviceGet(self), self, hal);
    }

static AtHal HalGet(AtIpCore self)
    {
    return self->hal;
    }

static uint8 IdGet(AtIpCore self)
    {
    return self->coreId;
    }

static AtDevice DeviceGet(AtIpCore self)
    {
    return self->device;
    }

static eAtRet InterruptEnable(AtIpCore self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool InterruptIsEnabled(AtIpCore self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet SemInterruptEnable(AtIpCore self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtErrorNotImplemented;
    }

static eBool  SemInterruptIsEnabled(AtIpCore self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SysmonInterruptEnable(AtIpCore self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtErrorNotImplemented;
    }

static eBool  SysmonInterruptIsEnabled(AtIpCore self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Init(AtIpCore self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet Activate(AtIpCore self, eBool activate)
    {
	AtUnused(activate);
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eBool IsActivated(AtIpCore self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SoftReset(AtIpCore self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet AsyncSoftReset(AtIpCore self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static eAtRet AsyncReset(AtIpCore self)
    {
    return mMethodsGet(self)->AsyncSoftReset(self);
    }

static eAtRet Test(AtIpCore self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtIpCore object = (AtIpCore)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(coreId);
    mEncodeObjectDescription(device);
    mEncodeNone(hal);
    mEncodeNone(listeners);
    }

static const char *ToString(AtObject self)
    {
    static char buffer[16];
    AtDevice dev = AtIpCoreDeviceGet((AtIpCore)self);

    AtSnprintf(buffer, sizeof(buffer) - 1, "%sipcore_%u", AtDeviceIdToString(dev), AtIpCoreIdGet((AtIpCore)self));
    return buffer;
    }

static eAtRet ModuleReactivate(AtIpCore self, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return cAtOk;
    }

static eBool ModuleIsActive(AtIpCore self, uint32 moduleId)
    {
    /* So far, module active has not been fully controlled and all of modules
     * are activated after device initialization. Let concrete modules determine */
    AtUnused(self);
    AtUnused(moduleId);
    return cAtTrue;
    }

static AtList Listeners(AtIpCore self)
    {
    if (self->listeners == NULL)
        self->listeners = AtListCreate(0);
    return self->listeners;
    }

static tAtIpCoreListenerWrapper *ListenerFind(AtIpCore self, const tAtIpCoreListener *callback)
    {
    tAtIpCoreListenerWrapper *wrapper;
    AtIterator iterator = AtListIteratorCreate(self->listeners);

    while ((wrapper = (tAtIpCoreListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback == callback)
            break;
        }
    AtObjectDelete((AtObject)iterator);

    return wrapper;
    }

static tAtIpCoreListenerWrapper *ListenerWrapperCreate(const tAtIpCoreListener *callback, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tAtIpCoreListenerWrapper);
    tAtIpCoreListenerWrapper *newWrapper = mMethodsGet(osal)->MemAlloc(osal, memorySize);

    mMethodsGet(osal)->MemInit(osal, newWrapper, 0, memorySize);
    newWrapper->callback = callback;
    newWrapper->userData = userData;

    return newWrapper;
    }

static eAtRet ListenerAdd(AtIpCore self, const tAtIpCoreListener *callback, void *userData)
    {
    if (ListenerFind(self, callback))
        return cAtErrorDuplicatedEntries;

    return AtListObjectAdd(Listeners(self), (AtObject)ListenerWrapperCreate(callback, userData));
    }

static eAtRet ListenerRemove(AtIpCore self, const tAtIpCoreListener *callback)
    {
    tAtIpCoreListenerWrapper *wrapper;

    if (callback == NULL)
        return cAtOk;

    if (self->listeners == NULL)
        return cAtErrorNotExist;

    wrapper = ListenerFind(self, callback);
    if (wrapper == NULL)
        return cAtErrorNotExist;

    if (wrapper->callback->WillRemoveListener)
        wrapper->callback->WillRemoveListener(self, wrapper->userData);

    AtListObjectRemove(self->listeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);

    return cAtOk;
    }

static void AllListenersDelete(AtIpCore self)
    {
    if (self->listeners == NULL)
        return;

    while (AtListLengthGet(self->listeners) > 0)
        {
        tAtIpCoreListenerWrapper *wrapper = (tAtIpCoreListenerWrapper *)AtListObjectRemoveAtIndex(self->listeners, 0);
        if (wrapper->callback->WillRemoveListener)
            wrapper->callback->WillRemoveListener(self, wrapper->userData);
        AtOsalMemFree(wrapper);
        }

    AtObjectDelete((AtObject)self->listeners);
    }

static void InterruptHappenedNotify(AtIpCore self)
    {
    tAtIpCoreListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tAtIpCoreListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->InterruptHappened)
            wrapper->callback->InterruptHappened(self, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void InterruptResolvedNotify(AtIpCore self)
    {
    tAtIpCoreListenerWrapper *wrapper;
    AtIterator iterator;

    if (self->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(self->listeners);
    while ((wrapper = (tAtIpCoreListenerWrapper *)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->callback->InterruptResolved)
            wrapper->callback->InterruptResolved(self, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void Delete(AtObject self)
    {
    AllListenersDelete((AtIpCore)self);
    m_AtObjectMethods->Delete(self);
    }

static uint32 InterruptCountRead2Clear(AtIpCore self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return 0;
    }

static uint32 InterruptContiguousCountRead2Clear(AtIpCore self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return 0;
    }

static uint32 InterruptMaxProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return 0;
    }

static uint32 InterruptLastProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return 0;
    }

static void Debug(AtIpCore self)
    {
    AtUnused(self);
    AtPrintc(cSevNormal, "No debug information to display\r\n");
    }

static void OverrideAtObject(AtIpCore self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtIpCore self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtIpCore self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HalSet);
        mMethodOverride(m_methods, HalGet);
        mMethodOverride(m_methods, IdGet);
        mMethodOverride(m_methods, DeviceGet);
        mMethodOverride(m_methods, InterruptIsEnabled);
        mMethodOverride(m_methods, InterruptEnable);
        mMethodOverride(m_methods, SemInterruptIsEnabled);
        mMethodOverride(m_methods, SemInterruptEnable);
        mMethodOverride(m_methods, SysmonInterruptIsEnabled);
        mMethodOverride(m_methods, SysmonInterruptEnable);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Activate);
        mMethodOverride(m_methods, IsActivated);
        mMethodOverride(m_methods, SoftReset);
        mMethodOverride(m_methods, AsyncSoftReset);
        mMethodOverride(m_methods, Test);
        mMethodOverride(m_methods, HalConfigure);
        mMethodOverride(m_methods, ModuleReactivate);
        mMethodOverride(m_methods, ModuleIsActive);
        mMethodOverride(m_methods, InterruptCountRead2Clear);
        mMethodOverride(m_methods, InterruptContiguousCountRead2Clear);
        mMethodOverride(m_methods, InterruptMaxProcessingTimeInUsRead2Clear);
        mMethodOverride(m_methods, InterruptLastProcessingTimeInUsRead2Clear);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, AsyncReset);
        }

    mMethodsSet(self, &m_methods);
    }

AtIpCore AtIpCoreObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtIpCore));

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->device = device;
    self->coreId = coreId;

    return self;
    }

eAtRet AtIpCoreActivate(AtIpCore self, eBool activate)
    {
    if (mIpCoreIsValid(self))
        return mMethodsGet(self)->Activate(self, activate);

    return cAtError;
    }

eBool AtIpCoreIsActivated(AtIpCore self)
    {
    if (mIpCoreIsValid(self))
        return mMethodsGet(self)->IsActivated(self);

    return cAtFalse;
    }

eAtRet AtIpCoreSoftReset(AtIpCore self)
    {
    if (mIpCoreIsValid(self))
        return mMethodsGet(self)->SoftReset(self);

    return cAtError;
    }

eAtRet AtIpCoreAsyncReset(AtIpCore self)
    {
    if (mIpCoreIsValid(self))
        return mMethodsGet(self)->AsyncReset(self);

    return cAtError;
    }

eAtRet AtIpCoreAsyncSoftReset(AtIpCore self)
    {
    if (mIpCoreIsValid(self))
        return mMethodsGet(self)->AsyncSoftReset(self);

    return cAtError;
    }

eAtRet AtIpCoreTest(AtIpCore self)
    {
    if (mIpCoreIsValid(self))
        return mMethodsGet(self)->Test(self);

    return cAtError;
    }

uint32 AtIpCoreRead(AtIpCore self, uint32 address)
    {
    return AtHalRead(AtIpCoreHalGet(self), address);
    }

void AtIpCoreWrite(AtIpCore self, uint32 address, uint32 value)
    {
    AtHalWrite(AtIpCoreHalGet(self), address, value);
    }

eAtRet AtIpCoreModuleReactivate(AtIpCore self, uint32 moduleId)
    {
    if (self)
        return mMethodsGet(self)->ModuleReactivate(self, moduleId);
    return cAtErrorNullPointer;
    }

eBool AtIpCoreModuleIsActive(AtIpCore self, uint32 moduleId)
    {
    if (self)
        return mMethodsGet(self)->ModuleIsActive(self, moduleId);
    return cAtFalse;
    }

void AtIpCoreInterruptHappenedNotify(AtIpCore self)
    {
    if (self)
        InterruptHappenedNotify(self);
    }

void AtIpCoreInterruptResolvedNotify(AtIpCore self)
    {
    if (self)
        InterruptResolvedNotify(self);
    }

/*
 * To read the maximum execution time when a interrupt trigger is completely processed.
 *
 * @param self This core
 * @param clear To clear after reading.
 *              - cAtTrue to clear after reading
 *              - cAtFalse to read-only
 *
 * @return Maximum execution time of an interrupt processing.
 */
uint32 AtIpCoreInterruptMaxProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaxProcessingTimeInUsRead2Clear(self, clear);
    return 0;
    }

/*
 * To read the execution time of the last interrupt processing.
 *
 * @param self This core
 * @param clear To clear after reading.
 *              - cAtTrue to clear after reading
 *              - cAtFalse to read-only
 *
 * @return Last execution time of an interrupt processing.
 */
uint32 AtIpCoreInterruptLastProcessingTimeInUsRead2Clear(AtIpCore self, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->InterruptLastProcessingTimeInUsRead2Clear(self, clear);
    return 0;
    }

AtHal IpCoreHalGet(AtIpCore self)
    {
    if (self)
        return HalGet(self);
    return NULL;
    }

/**
 * @addtogroup AtIpCore
 * @{
 */
/**
 * Install Hardware Abstraction Layer
 *
 * @param self This IP core
 * @param hal HAL layer
 */
eAtRet AtIpCoreHalSet(AtIpCore self, AtHal hal)
    {
    if (mIpCoreIsValid(self))
        return mMethodsGet(self)->HalSet(self, hal);
    return cAtError;
    }

/**
 * Get installed Hardware Abstraction Layer
 *
 * @param self This IP core
 *
 * @return HAL layer on success or NULL if failure
 */
AtHal AtIpCoreHalGet(AtIpCore self)
    {
    mNoParamObjectGet(HalGet, AtHal);
    }

/**
 * Get ID of this core
 *
 * @param self This core
 *
 * @return Core ID
 */
uint8 AtIpCoreIdGet(AtIpCore self)
    {
    mAttributeGet(IdGet, uint8, 0);
    }

/**
 * Get AT device that this core belongs to
 *
 * @param self This core
 *
 * @return AT device object on success or NULL on failure
 */
AtDevice AtIpCoreDeviceGet(AtIpCore self)
    {
    mNoParamObjectGet(DeviceGet, AtDevice);
    }

/**
 * Enable/disable interrupt on this core
 *
 * @param self This core
 * @param enable Enable/disable
 *
 * @return AT return code
 */
eAtRet AtIpCoreInterruptEnable(AtIpCore self, eBool enable)
    {
    mNonApiLogNumericalAttributeSet(InterruptEnable, enable);
    }

/**
 * Get interrupt on this core is enabled or disabled
 *
 * @param self This core
 *
 * @return Enabled or disabled
 */
eBool AtIpCoreInterruptIsEnabled(AtIpCore self)
    {
    mAttributeGet(InterruptIsEnabled, eBool, cAtFalse);
    }

/**
 * Add listener
 *
 * @param self This core
 * @param callback Callback which is implemented by application
 * @param userData Userdata which will be input when callback is called
 *
 * @return AT return code
 */
eAtRet AtIpCoreListenerAdd(AtIpCore self, const tAtIpCoreListener *callback, void *userData)
    {
    if (self)
        return ListenerAdd(self, callback, userData);
    return cAtErrorNullPointer;
    }

/**
 * Remove listener
 *
 * @param self This core
 * @param callback Callback to remove
 *
 * @return AT return code
 */
eAtRet AtIpCoreListenerRemove(AtIpCore self, const tAtIpCoreListener *callback)
    {
    if (self)
        return ListenerRemove(self, callback);
    return cAtErrorNullPointer;
    }

/**
 * To read number of times the interrupt PIN on this core is triggered
 *
 * @param self This core
 * @param clear To clear after reading.
 *              - cAtTrue to clear after reading
 *              - cAtFalse to read-only
 *
 * @return Number of times the interrupt PIN on this core is triggered
 */
uint32 AtIpCoreInterruptCountRead2Clear(AtIpCore self, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->InterruptCountRead2Clear(self, clear);
    return 0;
    }

/**
 * To read number of times the interrupt PIN on this core is triggered contiguously.
 *
 * @param self This core
 * @param clear To clear after reading.
 *              - cAtTrue to clear after reading
 *              - cAtFalse to read-only
 *
 * @return number of times the interrupt PIN on this core is triggered contiguously.
 */
uint32 AtIpCoreInterruptContiguousCountRead2Clear(AtIpCore self, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->InterruptContiguousCountRead2Clear(self, clear);
    return 0;
    }

/**
 * Show debug information
 *
 * @param self This core
 */
void AtIpCoreDebug(AtIpCore self)
    {
    if (self)
        mMethodsGet(self)->Debug(self);
    }

/**
 * Enable/disable SEM interrupt on this core
 *
 * @param self This core
 * @param enable Enable/disable
 *
 * @return AT return code
 */
eAtRet AtIpCoreSemInterruptEnable(AtIpCore self, eBool enable)
    {
    mNumericalAttributeSet(SemInterruptEnable, enable);
    }

/**
 * Get SEM interrupt on this core is enabled or disabled
 *
 * @param self This core
 *
 * @return Enabled or disabled
 */
eBool AtIpCoreSemInterruptIsEnabled(AtIpCore self)
    {
    mAttributeGet(SemInterruptIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable Sysmon(thermal and voltage mornitoring) interrupt on this core
 *
 * @param self This core
 * @param enable Enable/disable
 *
 * @return AT return code
 */
eAtRet AtIpCoreSysmonInterruptEnable(AtIpCore self, eBool enable)
    {
    mNumericalAttributeSet(SysmonInterruptEnable, enable);
    }

/**
 * Get Sysmon interrupt on this core is enabled or disabled
 *
 * @param self This core
 *
 * @return Enabled or disabled
 */
eBool AtIpCoreSysmonInterruptIsEnabled(AtIpCore self)
    {
    mAttributeGet(SysmonInterruptIsEnabled, eBool, cAtFalse);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtDriverInternal.h
 * 
 * Created Date: Jul 25, 2012
 *
 * Author      : namnn
 * 
 * Description : Generic Device class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDRIVERINTERNAL_H_
#define _ATDRIVERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "commacro.h"
#include "AtDriver.h"
#include "../common/AtObjectInternal.h"
#include "AtLogger.h"
#include "AtDriverLogMacro.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mRetCodeStr(retCode) if ((uint32)ret == (uint32)retCode) return ""#retCode""
#define mDriverErrorLog(ret, msg) {\
  if (ret != cAtOk) \
     AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, __FILE__, __LINE__, "%s got ERROR: %s\r\n", msg, AtDriverRet2String(ret));\
  }
#define mDriverInfoLog(format, msg) {\
  if (ret != cAtOk) \
     AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelInfo, __FILE__, __LINE__, format, msg);\
  }
/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtDriverMethods
    {
    /* Methods */
    uint8 (*MaxNumberOfDevicesGet)(AtDriver self);
    AtDevice* (*AddedDevicesGet)(AtDriver self, uint8 *numberOfAddedDevice);
    eBool (*ProductCodeIsValid)(AtDriver self, uint32 productCode);
    AtDevice (*CreateDevice)(AtDriver self, uint32 productCode);
    void (*DeleteDevice)(AtDriver self, AtDevice device);

    /* OSAL */
    void (*OsalSet)(AtDriver self, AtOsal osal);
    AtOsal (*OsalGet)(AtDriver self);
    }tAtDriverMethods;

/* * Representation */
typedef struct tAtDriver
    {
    tAtObject super;
    const tAtDriverMethods *methods;

    AtOsal osal;
    AtDevice *devices;
    uint8 maxDevices;
    uint8 numAddedDevices;
    AtOsalMutex mutex;

    /* Loggers */
    AtLogger defaultLogger;   /* Default logger */
    AtLogger installedLogger; /* Logger that set by application */
    char* sharedBuffer;
    AtOsalMutex logMutex;

    /* API logging */
    eBool apiLogAllApiEnabled;
    eBool apiLogEnabled;
    eBool apiLogEnabledBackup;
    uint32 apiLogCounter; /* To prevent nested logs */
    eBool apiLogMultiDeviceEnabled;

    /* Role controlling */
    uint8 role;
    uint32 roleSwitchTimes;
    uint32 maxRoleSwitchTimeInMs;
    uint32 maxDatabaseRestoreTimeInMs;
    eBool haDebugIsEnabled;
    eBool keepDatabaseAfterRestore;

    /* Shared buffer used for all objects */
    char buffer[1024];
    eBool debugEnabled;
    }tAtDriver;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
AtDriver AtDriverObjectInit(AtDriver self, uint8 maxNumberOfDevices, AtOsal osal);

/* For role switching */
eAtRet AtDriverDevicesAllocate(AtDriver self, uint8 maxNumberOfDevices);
eBool AtDriverIsStandby(void);
eBool AtDriverIsActive(void);
uint32 AtDriverRestoreAndKeepDatabase(AtDriver self, eBool keepDatabase);
eBool AtDriverKeepDatabaseAfterRestore(void);

/* Helpers */
AtOsal AtSharedDriverOsalGet(void);
char *AtSharedDriverSharedBuffer(uint32 *bufferSize);

eAtRet AtDriverLock(void);
eAtRet AtDriverUnLock(void);
void AtDriverVaListLog(AtDriver self, eAtLogLevel level, const char *file, uint32 line, const char *format, va_list args);
void AtDriverLogWithFileLine(AtDriver self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...) AtAttributePrintf(5, 6);
eBool AtDriverShouldLog(eAtLogLevel level);
char* AtDriverLogSharedBufferGet(AtDriver self, uint32 *bufferLength);

eAtRet AtDriverLogLock(void);
eAtRet AtDriverLogUnLock(void);
const char* AtDriverObjectStringToSharedBuffer(AtObject object);

eBool AtDriverApiLogStart(void);
eAtRet AtDriverApiLogStop(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATDRIVERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtDeviceAsyncInit.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : To control Async init
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDeviceInternal.h"
#include "AtIpCoreInternal.h"
#include "AtDeviceAsyncInit.h"
#include "AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cSSKeyWaitMaxInMs   3000
#define cNumUsPerMs         1000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eAtDeviceState
    {
    cAtDeviceStateUninitialized,
    cAtDeviceStateFirstInitializing,
    cAtDeviceStateSetupInitializing,
    cAtDeviceStateSecondInitializing,
    cAtDeviceStatePostInitializing,
    cAtDeviceStateInited
    } eAtDeviceState;

typedef enum eAtDeviceEntranceToAsyncInitState
    {
    cAtDeviceEntranceToAsyncInitStateAllCoreReset,
    cAtDeviceEntranceToAsyncInitStateSSKey,
    cAtDeviceEntranceToAsyncInitStateSSKeyWait,
    cAtDeviceEntranceToAsyncInitStateSSKeyCheck,
    cAtDeviceEntranceToAsyncInitStateAllCoreTest
    } eAtDeviceEntranceToAsyncInitState;


typedef enum eAsyncStates
    {
    cAsyncStateReset,
    cAsyncStateInit
    } eAsyncStates;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static void EntranceCoreAsyncStateIncrease(AtDevice self)
    {
    self->asyncCurrentCore++;
    }

static void EntranceCoreAsyncStateReset(AtDevice self)
    {
    self->asyncCurrentCore = 0;
    }

static uint32 EntranceCoreAsyncStateGet(AtDevice self)
    {
    return self->asyncCurrentCore;
    }

eAtRet AtDeviceAsyncAllCoresAsyncReset(AtDevice self)
    {
    uint8 core_i = (uint8)EntranceCoreAsyncStateGet(self);
    eAtRet ret = cAtOk;
    uint8 maxCore = AtDeviceNumIpCoresGet(self);
    tAtOsalCurTime prevTime;

    AtOsalCurTimeGet(&prevTime);
    if (core_i < maxCore)
        {
        AtIpCore core = AtDeviceIpCoreGet(self, core_i);
        ret = AtIpCoreAsyncSoftReset(core);
        AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AtIpCoreSoftReset");
        AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "AtIpCoreSoftReset");

        if (ret == cAtOk)
            {
            EntranceCoreAsyncStateIncrease(self);
            ret = cAtErrorAgain;
            }
        else if (!AtDeviceAsyncRetValIsInState(ret))
            EntranceCoreAsyncStateReset(self);

        }

    if (EntranceCoreAsyncStateGet(self) == maxCore)
        {
        ret = cAtOk;
        EntranceCoreAsyncStateReset(self);
        }

    return ret;
    }

static void EntranceStateSet(AtDevice self, uint32 newState)
    {
    self->asyncInitEntranceState = newState;
    }

static void EntranceStateReset(AtDevice self)
    {
    self->asyncInitEntranceState = cAtDeviceEntranceToAsyncInitStateAllCoreReset;
    }

static uint32 EntranceStateGet(AtDevice self)
    {
    return self->asyncInitEntranceState;
    }

static eAtRet EntranceToAsyncInitStateAllCoreReset(AtDevice self)
    {
    eAtRet ret = AtDeviceAllCoresAsyncReset(self);

    if (ret == cAtOk)
        {
        EntranceStateSet(self, cAtDeviceEntranceToAsyncInitStateSSKey);
        ret = cAtErrorAgain;
        }

    return ret;
    }

static eAtRet EntranceToAsyncInitStateSSKey(AtDevice self)
    {
    eAtRet ret = cAtOk;

    /* Check SSKey, give the key engine a moment so that it can run and
     * update status */
    if (mMethodsGet(self)->NeedCheckSSKey(self))
        {
        EntranceStateSet(self, cAtDeviceEntranceToAsyncInitStateSSKeyWait);
        AtOsalCurTimeGet(&self->ssKeyStartTime);
        AtDeviceAsyncInitNeedFixDelayTimeSet(self, cSSKeyWaitMaxInMs * cNumUsPerMs);
        ret = cAtRetNeedDelay;
        }
    else
        {
        EntranceStateSet(self, cAtDeviceEntranceToAsyncInitStateAllCoreTest);
        ret = cAtErrorAgain;
        }

    return ret;
    }

static eAtRet EntranceToAsyncInitStateSSKeyWait(AtDevice self)
    {
    eAtRet ret = cAtOk;
    tAtOsalCurTime curTime;
    uint32 diffTimeInMs;

    AtOsalCurTimeGet(&curTime);
    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, &self->ssKeyStartTime);

    if (diffTimeInMs >= cSSKeyWaitMaxInMs)
        {
        AtDeviceAsyncInitNeedFixDelayTimeSet(self, 0);
        EntranceStateSet(self, cAtDeviceEntranceToAsyncInitStateSSKeyCheck);
        ret = cAtErrorAgain;
        }
    else
        {
        AtDeviceAsyncInitNeedFixDelayTimeSet(self, (cSSKeyWaitMaxInMs - diffTimeInMs) * cNumUsPerMs);
        ret = cAtRetNeedDelay;
        }

    return ret;
    }

static eAtRet EntranceToAsyncInitStateSSKeyCheck(AtDevice self)
    {
    eAtRet ret = self->methods->SSKeyCheck(self);

    if (ret == cAtOk)
        {
        EntranceStateSet(self, cAtDeviceEntranceToAsyncInitStateAllCoreTest);
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        EntranceStateReset(self);

    return ret;
    }

static eAtRet EntranceToAsyncInitStateAllCoreTest(AtDevice self)
    {
    /* Need to check if cores work */
    eAtRet ret = AtDeviceAllCoresTest(self);
    if (!AtDeviceAsyncRetValIsInState(ret))
        EntranceStateReset(self);

    return ret;
    }

static eAtRet EntranceToAsyncInit(AtDevice self)
    {
    eAtRet ret = cAtOk;
    uint32 state = EntranceStateGet(self);

    switch (state)
        {
        case cAtDeviceEntranceToAsyncInitStateAllCoreReset:
            ret = EntranceToAsyncInitStateAllCoreReset(self);
            break;
        case cAtDeviceEntranceToAsyncInitStateSSKey:
            ret = EntranceToAsyncInitStateSSKey(self);
            break;
        case cAtDeviceEntranceToAsyncInitStateSSKeyWait:
            ret = EntranceToAsyncInitStateSSKeyWait(self);
            break;
        case cAtDeviceEntranceToAsyncInitStateSSKeyCheck:
            ret = EntranceToAsyncInitStateSSKeyCheck(self);
            break;
        case cAtDeviceEntranceToAsyncInitStateAllCoreTest:
            ret = EntranceToAsyncInitStateAllCoreTest(self);
            break;
        default:
            EntranceStateReset(self);
            ret = cAtErrorDevFail;
            break;
        }

    return ret;
    }

static void AsyncInitStateSet(AtDevice self, uint32 newState)
    {
    self->asyncInitState = newState;
    }

static void AsyncInitStateReset(AtDevice self)
    {
    self->asyncInitState = cAtDeviceStateUninitialized;
    }

static uint32 AsyncInitStateGet(AtDevice self)
    {
    return self->asyncInitState;
    }

static void AsyncInitNextModulePositionSet(AtDevice self, uint32 nextModulePos)
    {
    self->nextModulePos = nextModulePos;
    }

static uint32 AsyncInitNextModulePositionGet(AtDevice self)
    {
    return self->nextModulePos;
    }

static void AsyncStateSet(AtDevice self, uint32 state)
    {
    self->devAsyncResetState = state;
    }

static void AsyncStateReset(AtDevice self)
    {
    self->devAsyncResetState = cAsyncStateReset;
    }

static uint32 AsyncStateGet(AtDevice self)
    {
    return self->devAsyncResetState;
    }

eAtRet AtDeviceAsyncInitMain(AtDevice self)
    {
    eAtRet ret = cAtOk;
    eAtDeviceState state = AsyncInitStateGet(self);
    tAtOsalCurTime prevTime;

    AtOsalCurTimeGet(&prevTime);
    switch(state)
        {
        case cAtDeviceStateUninitialized:
            ret = EntranceToAsyncInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "EntranceToAsyncInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "EntranceToAsyncInit");
            if (ret == cAtOk)
                {
        	    AsyncInitStateSet(self, cAtDeviceStateFirstInitializing);
                ret = cAtErrorAgain;
                }
            break;

        case cAtDeviceStateFirstInitializing:
            ret = mMethodsGet(self)->AllModulesAsyncInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AllModulesAsyncInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "AllModulesAsyncInit");
            if (ret == cAtOk)
                {
        	    AsyncInitStateSet(self, cAtDeviceStateSetupInitializing);
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
        	    AsyncInitStateReset(self);
            break;

        case cAtDeviceStateSetupInitializing:
            ret = AtDeviceAllModulesSetup(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AtDeviceAllModulesSetup");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "AtDeviceAllModulesSetup");
            if (ret == cAtOk)
                {
        	    AsyncInitStateSet(self, cAtDeviceStateSecondInitializing);
                ret = cAtErrorAgain;
                }
            else
        	    AsyncInitStateReset(self);
            break;

        case cAtDeviceStateSecondInitializing:
            ret = mMethodsGet(self)->AllModulesAsyncInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AllModulesAsyncInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "AllModulesAsyncInit");
            if (ret == cAtOk)
                {
        	    AsyncInitStateSet(self, cAtDeviceStatePostInitializing);
                ret = cAtErrorAgain;
                }
            else if (!AtDeviceAsyncRetValIsInState(ret))
        	    AsyncInitStateReset(self);
            break;

        case cAtDeviceStatePostInitializing:
            ret = AtDevicePostInit(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "PostInit");
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "PostInit");
            if (ret == cAtOk)
        	    AsyncInitStateSet(self, cAtDeviceStateInited);
            else if (!AtDeviceAsyncRetValIsInState(ret))
        	    AsyncInitStateReset(self);
            break;

        case cAtDeviceStateInited:
            AsyncInitStateReset(self);
            ret = cAtErrorAgain;
            break;

        default:
            AsyncInitStateReset(self);
            ret = cAtErrorDevFail;
        }

    return ret;
    }

uint32 AtDeviceAccessTimeCountSinceLastProfileCheck(AtDevice self, eBool isLogged, const char *file, uint32 line, const char *ObjMsg)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 numRead = AtHalNumReadGet(hal);
    uint32 numWrite = AtHalNumWriteGet(hal);
    uint32 timeCount = (numRead * 5) + (numWrite * 10);/*assumed 5us for read operation, 10us for write operation*/
    int32  timeDelta = (int32)(timeCount - self->previousTimeCount);
    uint32 maxUint32 = 0xFFFFFFFF;

    if (timeDelta < 0)
        timeDelta = (int32)(maxUint32 + timeCount - self->previousTimeCount);

    self->previousTimeCount = timeCount;

    if (!isLogged)
        return (uint32)timeDelta;

    if (timeDelta >= 2000000)
            AtDeviceLog(self, cAtLogLevelCritical, file, line,
                        "Accessing %s takes longer than 2s because of total number of timecount(ms) = %d\r\n", ObjMsg, (uint32)(timeDelta/1000));

    else if (timeDelta >= 1000000)
        AtDeviceLog(self, cAtLogLevelWarning, file, line,
                    "Accessing %s takes longer than 1s for: total number of timecount(ms) = %d\r\n", ObjMsg, (uint32)(timeDelta/1000));

    return (uint32)timeDelta;
    }

uint32 AtDeviceAccessTimeInMsSinceLastProfileCheck(AtDevice self, eBool isLogged, tAtOsalCurTime *prevTime, const char *file, uint32 line, const char *ObjMsg)
    {
    tAtOsalCurTime curTime;
    uint32  timeDelta;

    AtOsalCurTimeGet(&curTime);
    timeDelta = AtOsalDifferenceTimeInMs(&curTime, prevTime);

    if (!isLogged)
        return timeDelta;

    if (timeDelta >= 2000)
            AtDeviceLog(self, cAtLogLevelCritical, file, line,
                        "Accessing %s takes longer than 2s because of total time (ms) = %d\r\n", ObjMsg, timeDelta);

    else if (timeDelta >= 1000)
        AtDeviceLog(self, cAtLogLevelWarning, file, line,
                    "Accessing %s takes longer than 1s because of total time (ms) = %d\r\n", ObjMsg, timeDelta);

    return timeDelta;
    }

void AtDeviceAsyncInitSetup(AtDevice self)
    {
    AsyncInitStateReset(self);
    }

eAtRet AtDeviceAsyncAllModulesInit(AtDevice self)
    {
    uint32 nextModulePos = AsyncInitNextModulePositionGet(self);
    uint32 ret = cAtOk;
    static uint32 allModules[] = {cAtModuleBer,
                                  cAtModuleXc,
                                  cAtModuleSdh,
                                  cAtModulePdh,
                                  cAtModuleEncap,
                                  cAtModulePpp,
                                  cAtModuleEth,
                                  cAtModuleRam,
                                  cAtModulePktAnalyzer,
                                  cAtModulePw,
                                  cAtModuleClock,
                                  cAtModulePrbs,
                                  cAtModuleAps,
                                  cAtModuleSur,
                                  cAtModuleConcate,
                                  cAtModuleFr};
    AtModule module = AtDeviceModuleGet(self, allModules[nextModulePos]);
    tAtOsalCurTime prevTime;

    AtOsalCurTimeGet(&prevTime);
    ret = AtDeviceModuleAsyncInit(self, allModules[nextModulePos]);
    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation,
                                               AtObjectToString((AtObject)module));
    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, AtObjectToString((AtObject)module));

    if (ret == cAtOk)
        {
        nextModulePos += 1;
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        nextModulePos = 0;

    if (nextModulePos == mCount(allModules))/*last module*/
        {
        nextModulePos = 0;
        ret = cAtOk;
        }

    AsyncInitNextModulePositionSet(self, nextModulePos);
    return (eAtRet)ret;
    }

eBool AtOsalSleepInMsIsExpired(tAtOsalCurTime *prevTime, uint32 timeout)
    {
    tAtOsalCurTime currentTime;
    AtOsalCurTimeGet(&currentTime);
    if (AtOsalDifferenceTimeInMs(&currentTime, prevTime) > timeout)
        return cAtTrue;
    return cAtFalse;
    }

eAtRet AtDeviceModuleAsyncInit(AtDevice self, eAtModule moduleId)
    {
    AtModule module = AtDeviceModuleGet(self, moduleId);
    eAtRet ret = module ? AtModuleAsyncInit(module) : cAtOk;
    if (ret != cAtOk && !AtDeviceAsyncRetValIsInState(ret))
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "Module %d init fail\r\n", moduleId);

    return ret;
    }

eAtRet AtDeviceAllCoresAsyncReset(AtDevice self)
    {
    if (AtDeviceInAccessible(self))
        return cAtOk;

    if (self)
        return mMethodsGet(self)->AllCoresAsyncReset(self);

    return cAtErrorNullPointer;
    }

eAtRet AtDeviceAsyncProcess(AtDevice self, uint32 *fsmState, tAtAsyncOperationFunc (*NextOperation)(AtDevice, uint32 state))
    {
    return AtDeviceObjectAsyncProcess(self, (AtObject)self, fsmState, (tAtNextAsyncOperation)NextOperation);
    }

eAtRet AtDeviceObjectAsyncProcess(AtDevice self, AtObject object, uint32 *fsmState, tAtNextAsyncOperation nextOp)
    {
    uint32 state = *fsmState;
    eAtRet ret = cAtOk;
    tAtOsalCurTime profileTime;
    tAtAsyncOperationFunc func;
    char stateString[32];

    func = nextOp(object, state);
    if (func == NULL)
        {
        *fsmState = 0;
        return cAtOk;
        }

    AtSnprintf(stateString, sizeof(stateString), "Async state: %d", state);
    AtOsalCurTimeGet(&profileTime);
    ret = func(object);

    AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, stateString);
    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, stateString);
    if (AtDeviceAsyncRetValIsInState(ret))
        return ret;

    if (ret != cAtOk)
        {
        *fsmState = 0;
        return ret;
        }

    *fsmState = *fsmState + 1;
    return cAtErrorAgain;
    }

void AtDeviceAsyncInitNeedFixDelayTimeSet(AtDevice self, uint32 delayInUs)
    {
    self->asyncInitRemainingUsNeedDelay = delayInUs;
    }

eBool AtDeviceAsyncRetValIsInState(eAtRet ret)
    {
    if ((ret == cAtErrorAgain) || (ret == cAtRetNeedDelay))
        return cAtTrue;
    return cAtFalse;
    }

eBool AtDeviceShouldReturnNeedDelay(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

/**
 * @addtogroup AtDevice
 * @{
 */

/**
 * Initialize device asynchronously. Application may need to call this API more
 * than one times to completely initialize the device. Each call takes maximum
 * two seconds.
 *
 * @param self This device
 *
 * @retval cAtErrorAgain This API needs to be called again
 * @retval cAtRetNeedDelay The application should use CPU for other tasks in a period,
 *                         which return from the AtDeviceAsyncDelayTimeUsGet, then
 *                         back to execute next AtDeviceAsyncInit call.
 * @retval cAtOk The device is fully initialized with no errors.
 * @retval eAtRet Other error codes if device initializing is stopped with error.
 */
eAtRet AtDeviceAsyncInit(AtDevice self)
    {
    eAtRet ret = cAtOk;
    tAtOsalCurTime prevTime;
    uint32 state;

    if (self == NULL)
        return cAtErrorNullPointer;

    state = AsyncStateGet(self);

    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtFalse, AtSourceLocation, "AtDeviceAsyncInit");
    AtOsalCurTimeGet(&prevTime);

    switch (state)
        {
        case cAsyncStateReset:
            ret = AtDeviceAsyncResetIfNecessary(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "AtDeviceAsyncReset");
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AtDeviceAsyncReset");
            if (ret == cAtOk)
                {
                AsyncStateSet(self, cAsyncStateInit);
                ret = cAtErrorAgain;
                }
            break;
        case cAsyncStateInit:
            ret = mMethodsGet(self)->AsyncInit(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &prevTime, AtSourceLocation, "AtDeviceAsyncInit");
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "AtDeviceAsyncInit");
            if (!AtDeviceAsyncRetValIsInState(ret))
                AsyncStateReset(self);
            break;
        default:
            AsyncStateReset(self);
            ret = cAtErrorDevFail;
        }

    if (!AtDeviceShouldReturnNeedDelay(self) && (ret == cAtRetNeedDelay))
        ret = cAtErrorAgain;

    return ret;
    }

/**
 * Get remaining fixed delay of a fixed delay state of the AtDeviceAsyncInit.
 *
 * @param self This device
 *
 * @retval Delay time in micro second that the application have to wait for the AtDeviceAsyncInit exit the fixed delay state.
 */
uint32 AtDeviceAsyncDelayTimeUsGet(AtDevice self)
    {
    if (self)
	    return self->asyncInitRemainingUsNeedDelay;
    return 0;
    }

/**
 * @}
 */

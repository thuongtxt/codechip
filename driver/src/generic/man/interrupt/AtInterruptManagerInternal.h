/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : INTERRUPT
 * 
 * File        : AtInterruptManagerInternal.h
 * 
 * Created Date: Dec 12, 2015
 *
 * Description : Generic module interrupt manager representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATINTERRUPTMANAGERINTERNAL_H_
#define _ATINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
#include "AtRet.h"
#include "AtObject.h" /* Superclass. */
#include "AtInterruptManager.h"
#include "AtHal.h"
#include "../AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtInterruptManagerMethods
    {
    void   (*InterruptProcess)(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore);
    void   (*InterruptOnIpCoreEnable)(AtInterruptManager self, eBool enable, AtIpCore ipCore);
    eAtRet (*InterruptEnable)(AtInterruptManager self, eBool enable);
    eBool  (*InterruptIsEnabled)(AtInterruptManager self);
    eBool  (*HasInterrupt)(AtInterruptManager self, uint32 glbIntr, AtHal hal);
    }tAtInterruptManagerMethods;

typedef struct tAtInterruptManager
    {
    tAtObject super;
    const tAtInterruptManagerMethods* methods;

    /* Private data. */
    AtModule module;
    }tAtInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptManager AtInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATINTERRUPTMANAGERINTERNAL_H_ */

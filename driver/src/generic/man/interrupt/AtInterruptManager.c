/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : INTERRUPT
 *
 * File        : AtInterruptManager.c
 *
 * Created Date: Dec 12, 2015
 *
 * Description : Generic module interrupt manager implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtDriverInternal.h"
#include "AtInterruptManagerInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtInterruptManagerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(ipCore);
    }

static eAtRet InterruptEnable(AtInterruptManager self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtInterruptManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void InterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtUnused(self);
    AtUnused(enable);
    AtUnused(ipCore);
    }

static const char *ToString(AtObject self)
    {
    AtModule module = AtInterruptManagerModuleGet((AtInterruptManager)self);
    AtDevice dev = AtModuleDeviceGet(module);
    static char str[64];

    AtSprintf(str, "%sAtInterruptManager", AtDeviceIdToString(dev));
    return str;
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(glbIntr);
    AtUnused(hal);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtInterruptManager object = (AtInterruptManager)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    }

static void OverrideAtObject(AtInterruptManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, InterruptEnable);
        mMethodOverride(m_methods, InterruptIsEnabled);
        mMethodOverride(m_methods, InterruptOnIpCoreEnable);
        mMethodOverride(m_methods, HasInterrupt);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtInterruptManager);
    }

AtInterruptManager AtInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->module = module;

    return self;
    }

void AtInterruptManagerInterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    if (hal == NULL || self == NULL)
        return;

    if (mMethodsGet(self)->HasInterrupt(self, glbIntr, hal))
        mMethodsGet(self)->InterruptProcess(self, glbIntr, ipCore);
    }

void AtInterruptManagerInterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    if (self)
        mMethodsGet(self)->InterruptOnIpCoreEnable(self, enable, ipCore);
    }

eAtRet AtInterruptManagerInterruptEnable(AtInterruptManager self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->InterruptEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool AtInterruptManagerInterruptIsEnabled(AtInterruptManager self)
    {
    if (self)
        return mMethodsGet(self)->InterruptIsEnabled(self);
    return cAtFalse;
    }

AtModule AtInterruptManagerModuleGet(AtInterruptManager self)
    {
    return (self) ? self->module : NULL;
    }

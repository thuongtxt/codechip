/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : INTERRUPT
 * 
 * File        : AtInterruptManager.h
 * 
 * Created Date: Dec 12, 2015
 *
 * Description : Generic module interrupt manager interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATINTERRUPTMANAGER_H_
#define _ATINTERRUPTMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtIpCore.h"
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtInterruptManager * AtInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtInterruptManagerInterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore);
eAtRet AtInterruptManagerInterruptEnable(AtInterruptManager self, eBool enable);
eBool AtInterruptManagerInterruptIsEnabled(AtInterruptManager self);
void AtInterruptManagerInterruptOnIpCoreEnable(AtInterruptManager self, eBool enable, AtIpCore ipCore);

AtModule AtInterruptManagerModuleGet(AtInterruptManager self);

#ifdef __cplusplus
}
#endif
#endif /* _ATINTERRUPTMANAGER_H_ */


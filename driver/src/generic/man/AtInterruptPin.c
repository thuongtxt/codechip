/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtInterruptPin.c
 *
 * Created Date: Oct 1, 2016
 *
 * Description : Interrupt PIN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtInterruptPinInternal.h"
#include "AtIpCoreInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (AtInterruptPin)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtInterruptPinMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TriggerModeSet(AtInterruptPin self, eAtTriggerMode triggerMode)
    {
    AtUnused(self);
    AtUnused(triggerMode);
    return cAtErrorNotImplemented;
    }

static eAtTriggerMode TriggerModeGet(AtInterruptPin self)
    {
    AtUnused(self);
    return cAtTriggerModeUnknown;
    }

static eAtRet Trigger(AtInterruptPin self, eBool triggered)
    {
    AtUnused(self);
    AtUnused(triggered);
    return cAtErrorNotImplemented;
    }

static eBool IsTriggered(AtInterruptPin self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsAsserted(AtInterruptPin self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtDevice DeviceGet(AtInterruptPin self)
    {
    return self->device;
    }

static uint32 IdGet(AtInterruptPin self)
    {
    return self->id;
    }

static eAtRet Init(AtInterruptPin self)
    {
    if (AtInterruptPinIsTriggered(self))
        AtInterruptPinTrigger(self, cAtFalse);
    (void)AtInterruptPinIsAsserted(self);

    return cAtOk;
    }

static AtIpCore Core(AtInterruptPin self)
    {
    return AtDeviceIpCoreGet(AtInterruptPinDeviceGet(self), 0);
    }

static uint32 Read(AtInterruptPin self, uint32 regAddr)
    {
    return AtIpCoreRead(Core(self), regAddr);
    }

static void Write(AtInterruptPin self, uint32 regAddr, uint32 value)
    {
    AtIpCoreWrite(Core(self), regAddr, value);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtInterruptPin object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(device);
    mEncodeUInt(id);
    }

static void OverrideAtObject(AtInterruptPin self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtInterruptPin self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtInterruptPin self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TriggerModeSet);
        mMethodOverride(m_methods, TriggerModeGet);
        mMethodOverride(m_methods, Trigger);
        mMethodOverride(m_methods, IsTriggered);
        mMethodOverride(m_methods, IsAsserted);
        mMethodOverride(m_methods, Init);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtInterruptPin);
    }

AtInterruptPin AtInterruptPinObjectInit(AtInterruptPin self, AtDevice device, uint32 pinId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->device = device;
    self->id = pinId;

    return self;
    }

eAtRet AtInterruptPinInit(AtInterruptPin self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorObjectNotExist;
    }

uint32 AtInterruptPinRead(AtInterruptPin self, uint32 regAddr)
    {
    if (self)
        return Read(self, regAddr);
    return 0;
    }

void AtInterruptPinWrite(AtInterruptPin self, uint32 regAddr, uint32 value)
    {
    if (self)
        Write(self, regAddr, value);
    }

/**
 * @addtogroup AtInterruptPin
 * @{
 */

/**
 * Get associated device
 *
 * @param self This PIN
 *
 * @return Associated device
 */
AtDevice AtInterruptPinDeviceGet(AtInterruptPin self)
    {
    if (self)
        return DeviceGet(self);
    return NULL;
    }

/**
 * Get PIN ID
 *
 * @param self This PIN
 *
 * @return PIN ID
 */
uint32 AtInterruptPinIdGet(AtInterruptPin self)
    {
    if (self)
        return IdGet(self);
    return cInvalidUint32;
    }

/**
 * Set trigger mode
 *
 * @param self This PIN
 * @param triggerMode @ref eAtTriggerMode "Trigger mode"
 *
 * @return AT return code
 */
eAtRet AtInterruptPinTriggerModeSet(AtInterruptPin self, eAtTriggerMode triggerMode)
    {
    if (self)
        return mMethodsGet(self)->TriggerModeSet(self, triggerMode);
    return cAtErrorObjectNotExist;
    }

/**
 * Get trigger mode
 *
 * @param self This PIN
 *
 * @return @ref eAtTriggerMode "Trigger mode"
 */
eAtTriggerMode AtInterruptPinTriggerModeGet(AtInterruptPin self)
    {
    if (self)
        return mMethodsGet(self)->TriggerModeGet(self);
    return cAtTriggerModeUnknown;
    }

/**
 * Trigger interrupt PIN
 *
 * @param self This PIN
 * @param triggered Trigger
 *                  - cAtTrue to start triggering
 *                  - cAtFalse to stop triggering
 *
 * @return AT return code
 */
eAtRet AtInterruptPinTrigger(AtInterruptPin self, eBool triggered)
    {
    if (self)
        return mMethodsGet(self)->Trigger(self, triggered);
    return cAtErrorObjectNotExist;
    }

/**
 * Check if interrupt PIN is being triggered
 *
 * @param self This PIN
 * @return
 */
eBool AtInterruptPinIsTriggered(AtInterruptPin self)
    {
    if (self)
        return mMethodsGet(self)->IsTriggered(self);
    return cAtFalse;
    }

/**
 * Check if interrupt PIN is being asserted after triggering
 *
 * @param self
 * @return
 */
eBool AtInterruptPinIsAsserted(AtInterruptPin self)
    {
    if (self)
        return mMethodsGet(self)->IsAsserted(self);
    return cAtFalse;
    }

/**
 * @}
 */

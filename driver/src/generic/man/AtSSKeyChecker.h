/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtSSKeyChecker.h
 * 
 * Created Date: Sep 18, 2013
 *
 * Description : SS Key checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSSKEYCHECKER_H_
#define _ATSSKEYCHECKER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSSKeyChecker * AtSSKeyChecker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtSSKeyCheckerCheck(AtSSKeyChecker self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSSKEYCHECKER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device Driver Management
 *
 * File        : AtDriver.c
 *
 * Created Date: Jul 25, 2012
 *
 * Author      : namnn
 *
 * Description : Device Driver Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDeviceInternal.h"

#include "../../util/AtIteratorInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../../implement/default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModuleRet2String(ret, moduleName)                                     \
    do                                                                         \
        {                                                                      \
        const char *moduleRetString = AtModule ## moduleName ## Ret2String(ret); \
        if (moduleRetString)                                                   \
            return moduleRetString;                                            \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Only support creating one driver. This variable is to hold this singleton driver */
static AtDriver  m_driver = NULL;
static AtOsal    m_osal   = NULL;

/* Implementation */
static tAtDriverMethods m_methods;

/* To override Delete method of AtObject class */
static tAtObjectMethods m_AtObjectOverride;

/* Reuse Delete implementation of AtObject class */
static const tAtObjectMethods *m_AtObjectMethods;

/* To prevent initializing implementation structures more than one times */
static char m_methodsInit = 0;

static char m_deviceIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_deviceAtArrayIteratorOverride;
static tAtIteratorMethods m_deviceAtIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool DeviceAtIndexIsValid(AtArrayIterator self, uint32 deviceIndex)
    {
	AtUnused(deviceIndex);
	AtUnused(self);
    /* Always return true because index is always less then number of device */
    return cAtTrue;
    }

static AtObject DeviceAtIndex(AtArrayIterator self, uint32 deviceIndex)
    {
    AtDevice *devices = self->array;
    return (AtObject)devices[deviceIndex];
    }

static uint32 DeviceCount(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count != -1)
        return (uint32)arrayIterator->count;

    arrayIterator->count = (int32)arrayIterator->capacity;
    return (uint32)arrayIterator->count;
    }

static void DeviceIteratorOverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_deviceIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_deviceAtArrayIteratorOverride, mMethodsGet(self), sizeof(m_deviceAtArrayIteratorOverride));
        m_deviceAtArrayIteratorOverride.DataAtIndexIsValid = DeviceAtIndexIsValid;
        m_deviceAtArrayIteratorOverride.DataAtIndex = DeviceAtIndex;
        }
    mMethodsSet(self, &m_deviceAtArrayIteratorOverride);
    }

static void DeviceIteratorOverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_deviceIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_deviceAtIteratorOverride, mMethodsGet(iterator), sizeof(m_deviceAtIteratorOverride));
        m_deviceAtIteratorOverride.Count = DeviceCount;
        }
    mMethodsSet(iterator, &m_deviceAtIteratorOverride);
    }

static void DeviceIteratorOverride(AtArrayIterator self)
    {
    DeviceIteratorOverrideAtArrayIterator(self);
    DeviceIteratorOverrideAtIterator(self);
    }

/*
 * To check if driver is valid
 * @param self
 * @return
 */
static eBool DriverIsValid(AtDriver self)
    {
    if (self == NULL)
        return cAtFalse;
    return cAtTrue;
    }

/* Get maximum number of devices that can be added to this driver */
static uint8 MaxNumberOfDevicesGet(AtDriver self)
    {
    return self->maxDevices;
    }

/* Get all added device */
static AtDevice* AddedDevicesGet(AtDriver self, uint8 *numberOfAddedDevice)
    {
    if (numberOfAddedDevice)
        *numberOfAddedDevice = self->numAddedDevices;
    return self->devices;
    }

/* To check if there is enough space for new device */
static eBool SpaceIsEnoughForNewDevice(AtDriver self)
    {
    return self->numAddedDevices < self->maxDevices;
    }

static eBool ProductCodeIsValid(AtDriver self, uint32 productCode)
    {
    return ThaProductCodeIsValid(self, productCode);
    }

/* To create a device */
static AtDevice CreateDevice(AtDriver self, uint32 productCode)
    {
    AtDevice device;
    eAtRet ret;

    /* Check if this type can be supported or enough space for new device */
    if (!SpaceIsEnoughForNewDevice(self))
        return NULL;

    /* Call Thalassa device factory method to create concrete device */
    device = ThaDeviceCreate(self, productCode);
    if (device == NULL)
        {
        /* Code to create other product family is here.... */
        return NULL;
        }

    /* Setup this device */
    ret = AtDeviceSetup(device);
    if (ret != cAtOk)
        {
        AtObjectDelete((AtObject)device);
        return NULL;
        }

    /* Cache it */
    self->devices[self->numAddedDevices] = device;
    self->numAddedDevices = (uint8)(self->numAddedDevices + 1);

    return device;
    }

/* Delete a device */
static void DeleteDevice(AtDriver self, AtDevice device)
    {
    uint8 i, numDevs;

    /* Find this device in device list */
    numDevs = self->numAddedDevices;
    for (i = 0; i < numDevs; i++)
        {
        if (self->devices[i] == device)
            break;
        }

    /* Do nothing if that device is not found */
    if (i == numDevs)
        return;

    /* Delete this device but give it a chance to prepair before deleting */
    AtDeviceWillDelete(device);
    AtObjectDelete((AtObject)device);

    /* Rearrange the list */
    for (; i < numDevs - 1; i++)
        self->devices[i] = self->devices[i + 1];
    self->devices[i] = NULL;
    self->numAddedDevices = (uint8)(numDevs - 1);
    }

static void AllDevicesDelete(AtDriver self)
    {
    AtOsal osal = self->osal;

    while (self->numAddedDevices > 0)
        DeleteDevice(self, self->devices[0]);
    mMethodsGet(osal)->MemFree(osal, self->devices);
    }

static void DeleteLoggingResource(AtDriver self)
    {
    /* Delete logger if it is existing */
    AtObjectDelete((AtObject)self->defaultLogger);
    AtOsalMutexDestroy(self->logMutex);

    if (self->sharedBuffer == NULL)
        return;

    AtOsalMemFree(self->sharedBuffer);
    self->sharedBuffer = NULL;
    }

static void Delete(AtObject self)
    {
    AtDriver driver = (AtDriver)self;

    AllDevicesDelete(driver);

    DeleteLoggingResource(driver);

    /* Delete OS resources */
    AtOsalMutexDestroy(driver->mutex);

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete(self);
    }

static void OsalSet(AtDriver self, AtOsal osal)
    {
    m_osal = osal;
    self->osal = osal;
    }

static AtOsal OsalGet(AtDriver self)
    {
    return self->osal;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtDriver object = (AtDriver)self;

    /* Version information */
    AtCoderEncodeString(encoder, AtDriverVersion(object), "version");

    /* Serialize all devices */
    mEncodeUInt(maxDevices);
    mEncodeUInt(numAddedDevices);
    AtCoderEncodeObjects(encoder, (AtObject *)object->devices, object->numAddedDevices, "devices");
    }

static AtLogger DefautlLogger(AtDriver self)
    {
    static const uint32 cMaxMessageLen  = 500;
    static const uint32 cMaxNumMessages = 1024;

    if (self->defaultLogger == NULL)
        {
        self->defaultLogger = AtDefaultLoggerNew(cMaxNumMessages, cMaxMessageLen);
        AtLoggerEnable(self->defaultLogger, cAtTrue);
        }

    return self->defaultLogger;
    }

static eAtRet LoggerSet(AtDriver self, AtLogger logger)
    {
    if (logger && (logger == self->defaultLogger))
        return cAtOk;

    self->installedLogger = logger;
    return cAtOk;
    }

static AtLogger LoggerGet(AtDriver self)
    {
    if (self->installedLogger)
        return self->installedLogger;
    return DefautlLogger(self);
    }

static void Debug(AtDriver self)
    {
    eBool enabled = AtDriverDebugIsEnabled();
    AtPrintc(cSevInfo, "* Common:\r\n");
    AtPrintc(cSevNormal, "    * Debug: "); AtPrintc(enabled ? cSevInfo : cSevNormal, "%s\r\n", enabled ? "enable" : "disable");

    /* Role switching */
    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo,   "* Role switching: \r\n");
    AtPrintc(cSevNormal, "* Switching: %u times\r\n", self->roleSwitchTimes);
    AtPrintc(cSevNormal, "* Max switching time: %u (ms)\r\n", self->maxRoleSwitchTimeInMs);
    AtPrintc(cSevNormal, "* Max database restore time: %u (ms)\r\n", self->maxDatabaseRestoreTimeInMs);
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "AtDriver";
    }

static eBool IsValidRole(eAtDriverRole role)
    {
    if ((role == cAtDriverRoleActive) ||
        (role == cAtDriverRoleStandby))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet DeviceHwAccessAllow(AtDevice device, eBool allowRead, eBool allowWrite)
    {
    uint8 core_i, numCores = AtDeviceNumIpCoresGet(device);

    for (core_i = 0; core_i < numCores; core_i++)
        {
        AtHal hal = AtDeviceIpCoreHalGet(device, core_i);
        AtHalReadOperationEnable(hal, allowRead);
        AtHalWriteOperationEnable(hal, allowWrite);
        }

    return cAtOk;
    }

static eAtRet HwAccessAllow(AtDriver self, eBool allowRead, eBool allowWrite)
    {
    eAtRet ret = cAtOk;
    uint8 device_i, numDevices;
    AtDevice *addedDevices;

    AtDriverLock();
    addedDevices = AtDriverAddedDevicesGet(self, &numDevices);
    for (device_i = 0; device_i < numDevices; device_i++)
        ret |= DeviceHwAccessAllow(addedDevices[device_i], allowRead, allowWrite);
    AtDriverUnLock();

    return ret;
    }

static eAtRet RoleSet(AtDriver self, eAtDriverRole role)
    {
    eBool allowAccess;
    eAtRet ret = cAtOk;
    tAtOsalCurTime startTime, currentTime;
    uint32 elapseTime;

    if (!IsValidRole(role))
        return cAtErrorInvlParm;

    AtOsalCurTimeGet(&startTime);

    /* Role switching */
    allowAccess = (role == cAtDriverRoleActive) ? cAtTrue : cAtFalse;
    self->role = role;
    ret = HwAccessAllow(self, allowAccess, allowAccess);

    /* For debugging */
    AtOsalCurTimeGet(&currentTime);
    self->roleSwitchTimes++;
    elapseTime = mTimeIntervalInMsGet(startTime, currentTime);
    if (elapseTime > self->maxRoleSwitchTimeInMs)
        self->maxRoleSwitchTimeInMs = elapseTime;

    return ret;
    }

static eAtDriverRole RoleGet(AtDriver self)
    {
    return self->role;
    }

static eBool KeepDatabaseAfterRestore(AtDriver self)
    {
    return self->keepDatabaseAfterRestore;
    }

static uint32 RestoreAndKeepDatabase(AtDriver self, eBool keepDatabase)
    {
    uint32 remained = 0;
    uint8 device_i, numDevices;
    AtDevice *addedDevices;
    uint32 restoreTime;
    tAtOsalCurTime startTime, currentTime;

    AtOsalCurTimeGet(&startTime);
    AtDriverLock();
    self->keepDatabaseAfterRestore = keepDatabase;
    addedDevices = AtDriverAddedDevicesGet(self, &numDevices);
    for (device_i = 0; device_i < numDevices; device_i++)
        remained += AtDeviceRestore(addedDevices[device_i]);
    AtDriverUnLock();
    AtOsalCurTimeGet(&currentTime);

    /* Profile the time */
    restoreTime = mTimeIntervalInMsGet(startTime, currentTime);
    if (self->maxDatabaseRestoreTimeInMs < restoreTime)
        self->maxDatabaseRestoreTimeInMs = restoreTime;

    return remained;
    }

static eBool ApiLogStart(AtDriver self)
    {
    eBool enabled = self->apiLogEnabled;

    /* This is the first time of logging, backup enabling */
    if (self->apiLogCounter == 0)
        {
        self->apiLogEnabledBackup = self->apiLogEnabled;
        self->apiLogEnabled = cAtFalse;
        }

    /* Increase counter to avoid nested logs */
    self->apiLogCounter = self->apiLogCounter + 1;

    return enabled;
    }

static eAtRet ApiLogStop(AtDriver self)
    {
    if (self->apiLogCounter == 0)
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Un-balance API logging start/stop\t\n");
    else
        self->apiLogCounter = self->apiLogCounter - 1;

    /* Restore API log enabling */
    if (self->apiLogCounter == 0)
        self->apiLogEnabled = self->apiLogEnabledBackup;

    return cAtOk;
    }

static void OverrideAtObject(AtDriver self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(m_osal)->MemCpy(m_osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    /* Change behavior of super class level 1 */
    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtDriver self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtDriver self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxNumberOfDevicesGet);
        mMethodOverride(m_methods, AddedDevicesGet);
        mMethodOverride(m_methods, OsalGet);
        mMethodOverride(m_methods, OsalSet);
        mMethodOverride(m_methods, ProductCodeIsValid);
        mMethodOverride(m_methods, CreateDevice);
        mMethodOverride(m_methods, DeleteDevice);
        }

    mMethodsGet(self) = &m_methods;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDriver);
    }

AtDriver AtDriverObjectInit(AtDriver self, uint8 maxNumberOfDevices, AtOsal osal)
    {
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Create mutex to protect database then save this OSAL */
    self->mutex = mMethodsGet(osal)->MutexCreate(osal);
    self->logMutex = mMethodsGet(osal)->MutexCreate(osal);
    self->osal = osal;

    if (AtDriverDevicesAllocate(self, maxNumberOfDevices) != cAtOk)
        return NULL;

    return self;
    }

/*
 * Get OSAL layer of the singleton driver
 *
 * @return OSAL layer
 */
AtOsal AtSharedDriverOsalGet(void)
    {
    return m_osal;
    }

const char *AtRet2String(eAtRet ret)
    {
    return AtDriverRet2String(ret);
    }

static uint32 MessagePrefixBuild(const char *file, uint32 line, char* buffer, uint32 bufferLength)
    {
    if (file)
    AtSnprintf(buffer, bufferLength - 1, "%s:%d, ", file, line);
    return AtStrlen(buffer);
    }

static uint32 LogMessageBuild(const char *file, uint32 line, char* buffer, uint32 bufferLength, const char* format, va_list args)
    {
    uint32 prefixLength = MessagePrefixBuild(file, line, buffer, bufferLength);
    if (bufferLength > prefixLength)
        AtStdSnprintf(AtStdSharedStdGet(), buffer + prefixLength, bufferLength - prefixLength - 1, format, args);

    buffer[bufferLength - 3] = '\r';
    buffer[bufferLength - 2] = '\n';
    buffer[bufferLength - 1] = '\0';

    return AtStrlen(buffer);
    }

void AtDriverVaListLog(AtDriver self, eAtLogLevel level, const char *file, uint32 line, const char *format, va_list args)
    {
    char* sharedBuffer;
    uint32 bufferLength;

    AtLogger logger = AtDriverLoggerGet(self);
    if (!AtLoggerLevelIsEnabled(logger, level))
        return;

    AtLoggerLock(logger);
    sharedBuffer = AtLoggerSharedBufferGet(logger, &bufferLength);
    if (sharedBuffer == NULL)
        {
        AtLoggerUnLock(logger);
        return;
        }

    AtOsalMemInit(sharedBuffer, 0, bufferLength);
    LogMessageBuild(file, line, sharedBuffer, bufferLength, format, args);
    AtLoggerNoLockLog(logger, level, sharedBuffer);
    AtLoggerUnLock(logger);
    }

void AtDriverLogWithFileLine(AtDriver self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...)
    {
    va_list args;
    if (self == NULL)
        return;

    va_start(args, format);
    AtDriverVaListLog(self, level, file, line, format, args);
    va_end(args);
    }

eAtRet AtDriverLock(void)
    {
    eAtOsalRet ret;
    AtDriver driver = AtDriverSharedDriverGet();

    if (driver == NULL)
        return cAtErrorNullPointer;

    ret = AtOsalMutexLock(driver->mutex);
    if (ret != cAtOsalOk)
        return cAtErrorOsalFail;

    return ret;
    }

eAtRet AtDriverUnLock(void)
    {
    eAtOsalRet ret;
    AtDriver driver = AtDriverSharedDriverGet();

    if (driver == NULL)
        return cAtErrorNullPointer;

    ret = AtOsalMutexUnLock(driver->mutex);
    if (ret != cAtOsalOk)
        return cAtErrorOsalFail;

    return ret;
    }

eAtRet AtDriverLogLock(void)
    {
    eAtOsalRet ret;
    AtDriver driver = AtDriverSharedDriverGet();

    if (driver == NULL)
        return cAtErrorNullPointer;

    ret = AtOsalMutexLock(driver->logMutex);
    if (ret != cAtOsalOk)
        return cAtErrorOsalFail;

    return ret;
    }

eAtRet AtDriverLogUnLock(void)
    {
    eAtOsalRet ret;
    AtDriver driver = AtDriverSharedDriverGet();

    if (driver == NULL)
        return cAtErrorNullPointer;

    ret = AtOsalMutexUnLock(driver->logMutex);
    if (ret != cAtOsalOk)
        return cAtErrorOsalFail;

    return ret;
    }

static char* SharedBufferGet(AtDriver self, uint32 *bufferLength)
    {
    const uint32 cSharedBufferLength = 512;
    *bufferLength = cSharedBufferLength;

    if (self->sharedBuffer)
        return self->sharedBuffer;

    self->sharedBuffer = AtOsalMemAlloc(cSharedBufferLength);
    return self->sharedBuffer;
    }

const char* AtDriverObjectStringToSharedBuffer(AtObject object)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    uint32 bufferLength;
    char* sharedBuffer;

    if (driver == NULL)
        return "NULL";

    sharedBuffer = SharedBufferGet(driver, &bufferLength);
    if (sharedBuffer == NULL)
        return "NULL";

    if (object == NULL)
        return "NULL";

    AtSnprintf(sharedBuffer, bufferLength - 1, "[%s]", AtObjectToString(object));
    sharedBuffer[bufferLength - 1] = '\0';
    return sharedBuffer;
    }

eAtRet AtDriverDevicesAllocate(AtDriver self, uint8 maxNumberOfDevices)
    {
    uint32 size;
    AtOsal osal = self->osal;

    if (maxNumberOfDevices == 0)
        return cAtOk;

    if (self->devices)
        AllDevicesDelete(self);

    size = sizeof(AtDevice) * maxNumberOfDevices;
    self->devices = mMethodsGet(osal)->MemAlloc(osal, size);
    if (self->devices == NULL)
        {
        mMethodsGet(osal)->MemFree(osal, self);
        return cAtErrorRsrcNoAvail;
        }

    mMethodsGet(osal)->MemInit(osal, self->devices, 0, size);
    self->maxDevices = maxNumberOfDevices;

    return cAtOk;
    }

char *AtSharedDriverSharedBuffer(uint32 *bufferSize)
    {
    AtDriver driver = AtDriverSharedDriverGet();
    if (driver == NULL)
        return NULL;
    if (bufferSize)
        *bufferSize = sizeof(driver->buffer);
    return driver->buffer;
    }

eBool AtDriverIsStandby(void)
    {
    return (AtDriverRoleGet(AtDriverSharedDriverGet()) == cAtDriverRoleStandby) ? cAtTrue : cAtFalse;
    }

eBool AtDriverIsActive(void)
    {
    return (AtDriverRoleGet(AtDriverSharedDriverGet()) == cAtDriverRoleActive) ? cAtTrue : cAtFalse;
    }

eBool AtDriverShouldLog(eAtLogLevel level)
    {
    return AtLoggerLevelIsEnabled(AtSharedDriverLoggerGet(), level);
    }

uint32 AtDriverRestoreAndKeepDatabase(AtDriver self, eBool keepDatabase)
    {
    if (self)
        return RestoreAndKeepDatabase(self, keepDatabase);
    return 0;
    }

eBool AtDriverKeepDatabaseAfterRestore(void)
    {
    AtDriver self = AtDriverSharedDriverGet();
    if (self)
        return KeepDatabaseAfterRestore(self);
    return cAtFalse;
    }

eBool AtDriverApiLogStart(void)
    {
    if (m_driver)
        return ApiLogStart(m_driver);
    return cAtFalse;
    }

eAtRet AtDriverApiLogStop(void)
    {
    if (m_driver)
        return ApiLogStop(m_driver);
    return cAtErrorObjectNotExist;
    }

char* AtDriverLogSharedBufferGet(AtDriver self, uint32 *bufferLength)
    {
    if (self)
        return SharedBufferGet(self, bufferLength);
    return NULL;
    }

/**
 * @addtogroup AtDriver
 * @{
 */

/**
 * Create Device Driver. Only one Device Driver can be created so this function
 * returns the singleton driver if it is called for more than one times
 *
 * @param maxNumberOfDevices
 * @param osal OSAL layer
 *
 * @return Device Driver instance on success or NULL if driver is already created
 */
AtDriver AtDriverCreate(uint8 maxNumberOfDevices, AtOsal osal)
    {
    AtDriver newDriver = NULL;

    /* Only one driver instance can be created */
    if (m_driver != NULL)
        return NULL;

    /* Check if new driver cannot be created with the input */
    if (maxNumberOfDevices == 0)
        return NULL;
    if (osal == NULL)
        return NULL;

    /* Save this OSAL */
    AtOsalSharedSet(osal);
    m_osal = osal;

    /* Allocate memory */
    newDriver = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDriver == NULL)
        return NULL;

    /* Construct and save this driver */
    m_driver = AtDriverObjectInit(newDriver, maxNumberOfDevices, osal);
    AtDriverDebugEnable(cAtTrue);

    return m_driver;
    }

/**
 * Get the singleton driver object
 *
 * @return The shared driver
 */
AtDriver AtDriverSharedDriverGet(void)
    {
    return m_driver;
    }

/**
 * Delete a driver
 *
 * @param self
 */
void AtDriverDelete(AtDriver self)
    {
    if (DriverIsValid(self))
        AtObjectDelete((AtObject)self);

    if (m_driver == self)
        m_driver = NULL;
    }

/**
 * Get maximum number of devices that can be added to this driver
 *
 * @param self AT Device Driver
 * @return Maximum number of devices
 */
uint8 AtDriverMaxNumberOfDevicesGet(AtDriver self)
    {
    mAttributeGet(MaxNumberOfDevicesGet, uint8, 0);
    }

/**
 * Get installed OSAL
 *
 * @param self This driver
 *
 * @return installed OSAL
 */
AtOsal AtDriverOsalGet(AtDriver self)
    {
    mNoParamObjectGet(OsalGet, AtOsal);
    }

/**
 * Get number of added device
 *
 * @param [in] self AT Device Driver
 * @param [out] numberOfAddedDevice
 *
 * @return Array of added devices
 */
AtDevice *AtDriverAddedDevicesGet(AtDriver self, uint8 *numberOfAddedDevice)
    {
    if (DriverIsValid(self))
        return mMethodsGet(self)->AddedDevicesGet(self, numberOfAddedDevice);

    /* Return no device */
    if (numberOfAddedDevice)
        *numberOfAddedDevice = 0;

    return NULL;
    }

/**
 * Get number of added devices
 *
 * @param self This driver
 *
 * @return Number of added devices
 */
uint8 AtDriverNumAddedDeviceGet(AtDriver self)
    {
    if (self)
        return self->numAddedDevices;
    return 0;
    }

/**
 * Check if device product code is valid
 *
 * @param self This driver
 * @param productCode Product code to check
 *
 * @retval cAtTrue - If product code is valid
 * @retval cAtFalse - If product code is invalid
 */
eBool AtDriverProductCodeIsValid(AtDriver self, uint32 productCode)
    {
    mOneParamAttributeGet(ProductCodeIsValid, productCode, eBool, cAtFalse);
    }

/**
 * Create new device specified by device type
 *
 * @param self AT Device Driver
 * @param productCode Product code
 *
 * @return A device instance on success or NULL if driver does not support this
 *         device
 */
AtDevice AtDriverDeviceCreate(AtDriver self, uint32 productCode)
    {
    AtDevice newDevice = NULL;

    if (!DriverIsValid(self))
        return NULL;

    mOneParamApiLogStart(productCode);
    AtDriverLock();
    newDevice = mMethodsGet(self)->CreateDevice(self, productCode);
    AtDriverUnLock();
    AtDriverApiLogStop();

    return newDevice;
    }

/**
 * Get device by its index
 *
 * @param self AT Device Driver
 * @param deviceIndex Device index
 *
 * @return Valid AtDevice objec on success or NULL if out of range
 */
AtDevice AtDriverDeviceGet(AtDriver self, uint8 deviceIndex)
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices;
    AtDevice device = NULL;

    AtDriverLock();
    addedDevices = AtDriverAddedDevicesGet(self, &numAddedDevices);
    if (addedDevices && (deviceIndex < numAddedDevices))
        device = addedDevices[deviceIndex];
    AtDriverUnLock();

    return device;
    }

/**
 * Get index of a device
 *
 * @param self This driver
 * @param device Device to retrieve its index
 *
 * @return Device index if success. Otherwise, invalid value is returned.
 */
uint8 AtDriverDeviceIndexGet(AtDriver self, AtDevice device)
    {
    uint8 numAddedDevices;
    AtDevice *addedDevices;
    uint8 device_i;

    AtDriverLock();
    addedDevices = AtDriverAddedDevicesGet(self, &numAddedDevices);
    for (device_i = 0; device_i < numAddedDevices; device_i++)
        {
        if (addedDevices[device_i] == device)
            break;
        }
    AtDriverUnLock();

    return (uint8)((device_i == numAddedDevices) ? cBit7_0 : device_i);
    }

/**
 * Delete a device
 *
 * @param self AT Device Driver
 * @param device Device to delete
 */
void AtDriverDeviceDelete(AtDriver self, AtDevice device)
    {
    if (!DriverIsValid(self))
        return;

    mOneObjectApiLogStart(device);
    AtDriverLock();
    mMethodsGet(self)->DeleteDevice(self, device);
    AtDriverUnLock();
    AtDriverApiLogStop();
    }

/**
 * Create Devices Iterator
 *
 * @param self AT Device Driver
 *
 * @return AtIterator object
 */
AtIterator AtDriverDeviceIteratorCreate(AtDriver self)
    {
    AtArrayIterator iterator;

    if (!DriverIsValid(self))
        return NULL;

    iterator = AtArrayIteratorNew(self->devices, self->numAddedDevices);
    if (NULL == iterator)
        return NULL;

    /* Setup class */
    DeviceIteratorOverride(iterator);
    m_deviceIteratorMethodsInit = 1;

    return (AtIterator)iterator;
    }

/**
 * Get Shared logger
 *
 * @return AtLogger
 */
AtLogger AtDriverLoggerGet(AtDriver self)
    {
    if (self)
        return LoggerGet(self);
    return NULL;
    }

/**
 * Set logger. If the input logger is NULL, the default logger will be used.
 *
 * @param self This driver
 * @param logger Logger
 *
 * @return AT return code.
 */
eAtRet AtDriverLoggerSet(AtDriver self, AtLogger logger)
    {
    if (self)
        return LoggerSet(self, logger);
    return cAtErrorNullPointer;
    }

/**
 * Log a message
 *
 * @param self This driver
 * @param level @ref eAtLogLevel "Log levels"
 * @param format Format
 */
void AtDriverLog(AtDriver self, eAtLogLevel level, const char *format, ...)
    {
    va_list args;

    if (self == NULL)
        return;

    va_start(args, format);
    AtLoggerLog(AtDriverLoggerGet(self), level, format, args);
    va_end(args);
    }

/**
 * Get logger of the driver singleton
 *
 * @return The logger of this driver if exist
 */
AtLogger AtSharedDriverLoggerGet()
    {
    return AtDriverLoggerGet(AtDriverSharedDriverGet());
    }

/**
 * Translate return code to string
 *
 * @param ret return code
 *
 * @return String that describes return code
 */
const char *AtDriverRet2String(eAtRet ret)
    {
    /* Common error code */
    mRetCodeStr(cAtOk);
    mRetCodeStr(cAtError);
    mRetCodeStr(cAtErrorModeNotSupport);
    mRetCodeStr(cAtErrorOutOfRangParm);
    mRetCodeStr(cAtErrorRsrcNoAvail);
    mRetCodeStr(cAtErrorDevBusy);
    mRetCodeStr(cAtErrorChannelBusy);
    mRetCodeStr(cAtErrorResourceBusy);
    mRetCodeStr(cAtErrorIndrAcsTimeOut);
    mRetCodeStr(cAtErrorInvlParm);
    mRetCodeStr(cAtErrorDevFail);
    mRetCodeStr(cAtErrorNullPointer);
    mRetCodeStr(cAtErrorDevicePllNotLocked);
    mRetCodeStr(cAtErrorMemoryTestFail);
    mRetCodeStr(cAtErrorNotImplemented);
    mRetCodeStr(cAtErrorNotEditable);
    mRetCodeStr(cAtErrorOsalFail);
    mRetCodeStr(cAtErrorBerEngineFail);
    mRetCodeStr(cAtErrorDdrCalibFail);
    mRetCodeStr(cAtErrorDdrInitFail);
    mRetCodeStr(cAtErrorInvalidSSkey);
    mRetCodeStr(cAtErrorEyeWidthOutOfRange);
    mRetCodeStr(cAtErrorEyeSwingOutOfRange);
    mRetCodeStr(cAtErrorEyeScanTimeout);
    mRetCodeStr(cAtErrorClockUnstable);
    mRetCodeStr(cAtErrorNotActivate);
    mRetCodeStr(cAtErrorNotApplicable);
    mRetCodeStr(cAtErrorObjectNotExist);
    mRetCodeStr(cAtErrorIdConvertFail);
    mRetCodeStr(cAtErrorDiagnosticFail);
    mRetCodeStr(cAtErrorRamAccessTimeout);
    mRetCodeStr(cAtErrorQdrCalibFail);
    mRetCodeStr(cAtErrorNotReady);
    mRetCodeStr(cAtErrorNotFound);
    mRetCodeStr(cAtErrorDuplicatedEntries);
    mRetCodeStr(cAtErrorNotExist);
    mRetCodeStr(cAtModuleXcErrorConnectionNotExist);

    mRetCodeStr(cAtErrorSerdesResetTimeout);
    mRetCodeStr(cAtErrorSerdesTxUnderFlow);
    mRetCodeStr(cAtErrorSerdesTxOverFlow);
    mRetCodeStr(cAtErrorSerdesRxUnderFlow);
    mRetCodeStr(cAtErrorSerdesRxOverFlow);
    mRetCodeStr(cAtErrorSerdesFail);
    mRetCodeStr(cAtErrorSerdesMdioFail);

    mRetCodeStr(cAtErrorXfiPcsNotLocked);

    mRetCodeStr(cAtModuleEncapErrorNoBoundChannel);

    mRetCodeStr(cAtModuleXcErrorConnectionBusy);
    mRetCodeStr(cAtModuleXcErrorDifferentChannelType);

    mRetCodeStr(cAtModulePrbsErrorNoBoundPw);
    mRetCodeStr(cAtErrorBandwidthExceeded);
    mRetCodeStr(cAtErrorInvalidAddress);
    mRetCodeStr(cAtErrorNotControllable);
    mRetCodeStr(cAtErrorInvalidOperation);
    mRetCodeStr(cAtErrorOperationTimeout);
    mRetCodeStr(cAtErrorAgain);

    mRetCodeStr(cAtErrorSemNotActive);
    mRetCodeStr(cAtErrorSemFsmFailed);
    mRetCodeStr(cAtErrorSemFatalError);
    mRetCodeStr(cAtErrorSemUncorrectable);
    mRetCodeStr(cAtErrorSemAddressNotFound);
    mRetCodeStr(cAtErrorSemStateMoveFail);
    mRetCodeStr(cAtErrorSemValidateFail);
    mRetCodeStr(cAtErrorSemTimeOut);
    mRetCodeStr(cAtErrorSemError);

    mRetCodeStr(cAtModuleApsErrorExtCmdLowPriority);
    mRetCodeStr(cAtModuleSdhErrorInvalidConcatenation);
    mRetCodeStr(cAtModuleSdhErrorTtiModeMismatch);

    return "Unknown error";
    }

/**
 * Get the last error code.
 *
 * @return Last error code
 * @note The internal implementation will clear this last error code after
 *       reading.
 */
eAtRet AtDriverLastErrorCode(void)
    {
    /* TODO: will be supported in next version */
    return cAtOk;
    }

/**
 * Show driver debug information
 *
 * @param self This driver
 */
void AtDriverDebug(AtDriver self)
    {
    if (self)
        Debug(self);
    }

/**
 * Get driver version
 *
 * @param self This driver
 * @return Version description
 */
const char *AtDriverVersion(AtDriver self)
    {
    AtUnused(self);
    return "2.5.2";
    }

/**
 * To have more detail version information
 *
 * @param self This driver
 *
 * @return Version detail
 */
const char *AtDriverVersionDescription(AtDriver self)
    {
    AtUnused(self);
    return "Include atsdk.2.4.6.5";
    }

/**
 * Get built number.
 *
 * @param self This driver
 * @return Built number
 */
uint32 AtDriverBuiltNumber(AtDriver self)
    {
    /* Do not need to reset this built number, just simply increase whenever
     * there are any updates and need to release to SVT team */
    AtUnused(self);
    return 9;
    }

/**
 * Enable/disable debugging.
 *
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 * @note Enable debugging may slow down software processing.
 */
eAtRet AtDriverDebugEnable(eBool enable)
    {
    if (m_driver == NULL)
        return cAtErrorNullPointer;

    m_driver->debugEnabled = enable;
    return cAtOk;
    }

/**
 * Check if debugging is enabled or not
 *
 * @return
 */
eBool AtDriverDebugIsEnabled(void)
    {
    return (eBool)(m_driver ? m_driver->debugEnabled : cAtFalse);
    }

/**
 * Set driver's role to active or standby.
 *
 * When the role is switched from active to standby or vice versa from standby
 * to active. The internal sync task and sync connection will be deleted.
 * - For new active card: application need to listen again, task and
 *   connection will be created by this operation
 * - For new standby card: application need to explicitly connect to active
 *   driver. Sync task and connection will be created by this operation
 *
 * @param self This driver
 * @param role @ref eAtDriverRole "Driver's role"
 *
 * @return AT return code
 */
eAtRet AtDriverRoleSet(AtDriver self, eAtDriverRole role)
    {
    if (self)
        {
        mOneParamWrapCall(role, eAtRet, RoleSet(self, role));
        }
    return cAtErrorNullPointer;
    }

/**
 * Get driver's role
 *
 * @param self This driver
 *
 * @return @ref eAtDriverRole "Driver's role"
 */
eAtDriverRole AtDriverRoleGet(AtDriver self)
    {
    if (self)
        return RoleGet(self);
    return cAtDriverRoleUnknown;
    }

/**
 * To restore driver database after switching role from standby to active. This
 * API is used when driver sync mode is cAtDriverSyncModeNone. After switching
 * the driver role to active, application will keep calling this API until it
 * return 0 to rebuild necessary database by reading registers from hardware.
 *
 * @param self This driver
 *
 * @retval None zero if database has not been completely rebuilt
 * @retval Zero if database has been completely rebuilt
 */
uint32 AtDriverRestore(AtDriver self)
    {
    mNoParamWrapCall(uint32, AtDriverRestoreAndKeepDatabase(self, cAtFalse));
    }

/**
 * Enable/disable API logging. When API logging is enabled, all of API callings
 * will be logged to driver logger.
 *
 * @param enabled API logging enabling.
 *                - cAtTrue: to enable
 *                - cAtFalse: to disable
 *
 * @return AT return code
 */
eAtRet AtDriverApiLogEnable(eBool enabled)
    {
    if (m_driver)
        {
        m_driver->apiLogEnabled = enabled;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

/**
 * Check if API logging is enabled
 *
 * @retval cAtTrue if API logging is enabled
 * @retval cAtFalse if API logging is disabled
 */
eBool AtDriverApiLogIsEnabled(void)
    {
    return (eBool)(m_driver ? m_driver->apiLogEnabled : cAtFalse);
    }

/**
 * Enable/disable All API logging. When all API logging is enabled, all of API callings
 * will be logged to driver logger. This API is to allow flexible to disable API logging
 * for so frequently API calls: PM TCA threshold APIs.
 *
 * @param enabled API logging enabling.
 *                - cAtTrue: to enable
 *                - cAtFalse: to disable
 *
 * @return AT return code
 */
eAtRet AtDriverAllApiLogEnable(eBool enabled)
    {
    if (m_driver)
        {
        m_driver->apiLogAllApiEnabled = enabled;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

/**
 * Check if All API logging is enabled
 *
 * @retval cAtTrue if API logging is enabled
 * @retval cAtFalse if API logging is disabled
 */
eBool AtDriverAllApiLogIsEnabled(void)
    {
    return (eBool)(m_driver ? m_driver->apiLogAllApiEnabled : cAtFalse);
    }

/**
 * Enable/disable Multi device API logging. When all this kind of API logging is enabled, AtObjectToString result
 * is prefixed with mdev_x_ for the object of the master device x and sdev_x.y_ the objects of sub-device y in the master device x.
 * This feature is disabled in default to keep the testing system test safely.
 *
 * @param enabled Allow mdev_/sdev_ prefix of ObjectToString in API logging.
 *                - cAtTrue: to enable
 *                - cAtFalse: to disable
 *
 * @return AT return code
 */
eAtRet AtDriverMultiDevPrefixApiLogEnable(eBool enabled)
    {
    if (m_driver)
        {
        m_driver->apiLogMultiDeviceEnabled = enabled;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

/**
 * Check if mdev_/sdev_ prefix in API logging is enabled
 *
 * @retval cAtTrue if the "mdev_"/"sdev_" prefix is enabled
 * @retval cAtFalse if the "mdev_"/"sdev_" prefix is disabled
 */
eBool AtDriverMultiDevPrefixApiLogIsEnabled(void)
    {
    return (eBool)(m_driver ? m_driver->apiLogMultiDeviceEnabled : cAtFalse);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtDeviceAsyncInit.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : Async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICEASYNCINIT_H_
#define _ATDEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRet.h"
#include "AtDevice.h"
#include "AtOsal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtDeviceAsyncAllCoresAsyncReset(AtDevice self);
eAtRet AtDeviceAsyncInitMain(AtDevice self);
uint32 AtDeviceAccessTimeCountSinceLastProfileCheck(AtDevice self, eBool isLogged, const char *file, uint32 line, const char *ObjMsg);
uint32 AtDeviceAccessTimeInMsSinceLastProfileCheck(AtDevice self, eBool isLogged, tAtOsalCurTime *prevTime, const char *file, uint32 line, const char *ObjMsg);
void AtDeviceAsyncInitSetup(AtDevice self);
eAtRet AtDeviceAsyncAllModulesInit(AtDevice self);
eAtRet AtDeviceEntranceToInit(AtDevice self);
eAtRet AtDeviceResetIfNecessary(AtDevice self);
eAtRet AtDevicePostInit(AtDevice self);
eAtRet AtDeviceAllCoresAsyncReset(AtDevice self);

/* util */
eBool AtOsalSleepInMsIsExpired(tAtOsalCurTime *prevTime, uint32 timeout);
eBool AtDeviceShouldReturnNeedDelay(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEVICEASYNCINIT_H_ */


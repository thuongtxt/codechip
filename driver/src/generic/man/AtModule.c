/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : AtModule.c
 *
 * Created Date: Jul 26, 2012
 *
 * Author      : namnn
 *
 * Description : Module abstract class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "AtModuleInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtMaxNumEventListener 32

/*--------------------------- Macros -----------------------------------------*/
#define mModuleIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* To store implementation of this class (require) */
static char m_methodsInit = 0;
static tAtModuleMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return NULL;
    }

static const char *CapacityDescription(AtModule self)
    {
    return AtObjectToString((AtObject)self);
    }

/* Get module type */
static eAtModule TypeGet(AtModule self)
    {
    if (mModuleIsValid(self))
        return self->moduleId;
    return cAtModuleInvalid;
    }

static AtDevice DeviceGet(AtModule self)
    {
    if (mModuleIsValid(self))
        return self->device;
    return NULL;
    }

static eAtRet RoleDefaultSetup(AtModule self)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(self)->HasRole(self))
        return cAtOk;

    ret |= mMethodsGet(self)->RoleInputEnable(self, cAtTrue);
    ret |= mMethodsGet(self)->RoleActiveForce(self, cAtFalse);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret |= mMethodsGet(self)->AllChannelsInterruptDisable(self);
    ret |= AtModuleSetup(self);

    /* Disable interrupt as default */
    if (ret == cAtOk)
        mMethodsGet(self)->InterruptEnable(self, cAtFalse);

    return RoleDefaultSetup(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eAtRet Setup(AtModule self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtOk;
    }

static void InterruptProcess(AtModule self, uint32 glbIntrStatus, AtIpCore ipCore)
    {
    uint32 manager_i, numManagers = mMethodsGet(self)->NumInterruptManagers(self);

    for (manager_i = 0; manager_i < numManagers; manager_i++)
        AtInterruptManagerInterruptProcess(AtModuleInterruptManagerAtIndexGet(self, manager_i), glbIntrStatus, ipCore);
    }

static void InterruptOnIpCoreEnable(AtModule self, eBool enable, AtIpCore ipCore)
    {
    uint32 manager_i, numManagers = mMethodsGet(self)->NumInterruptManagers(self);

    for (manager_i = 0; manager_i < numManagers; manager_i++)
        AtInterruptManagerInterruptOnIpCoreEnable(AtModuleInterruptManagerAtIndexGet(self, manager_i), enable, ipCore);
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(self);
    AtUnused(managerIndex);
    /* Let sub-class do their job. */
    return NULL;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    /* Let sub-class do their job. */
    return 0;
    }

static void OamProcessOnCore(AtModule self, AtIpCore ipCore, uint16 numServedOamsAtOneTime)
    {
	AtUnused(numServedOamsAtOneTime);
	AtUnused(ipCore);
	AtUnused(self);
    /* Let concrete class do */
    }

/*
 * Read a module register. One module can have more than one cores implement it.
 * The default implementation always access the first core. Concrete module
 * should override this method to handle its cores properly
 *
 * @param self This module
 * @param localAddress Local address of this module
 *
 * @return Register value
 */
static uint32 HwRead(AtModule self, uint32 localAddress, AtHal hal)
    {
    AtHal usedHal = hal;

    /* If input HAL is NULL, try default one */
    if (usedHal == NULL)
        usedHal = AtIpCoreHalGet(AtDeviceIpCoreGet(self->device, AtModuleDefaultCoreGet(self)));

    if (usedHal)
        {
        mMethodsGet(self)->WillAccessRegister(self, localAddress);
        return AtHalRead(usedHal, localAddress);
        }

    return 0xcafecafe;
    }

/*
 * Write a module register. One module can have more than one cores implement it.
 * The default implementation always access the first core. Concrete module
 * should override this method to handle its cores properly
 *
 * @param self This module
 * @param localAddress Local address of this module
 * @param value Value
 */
static void HwWrite(AtModule self, uint32 localAddress, uint32 value, AtHal hal)
    {
    AtHal usedHal = hal;

    /* If input HAL is NULL, try default one */
    if (usedHal == NULL)
        usedHal = AtIpCoreHalGet(AtDeviceIpCoreGet(self->device, AtModuleDefaultCoreGet(self)));

    if (usedHal)
        {
        mMethodsGet(self)->WillAccessRegister(self, localAddress);
        AtHalWrite(usedHal, localAddress, value);
        }
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
	AtUnused(self);
    /* Default implementation just return no hold register */
    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = 0;

    return NULL;
    }

static uint32 *HoldReadRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    return mMethodsGet(self)->HoldRegistersGet(self, numberOfHoldRegisters);
    }

static uint32 *HoldWriteRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    return mMethodsGet(self)->HoldRegistersGet(self, numberOfHoldRegisters);
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceLongRegisterAccessCreate(device, self);
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    AtDevice device = AtModuleDeviceGet(self);
	AtUnused(localAddress);

    if (self->longRegisterAccess)
        return self->longRegisterAccess;

    /* If module has its own long register access, use it */
    if (self->moduleLongRegisterAccess == NULL)
        self->moduleLongRegisterAccess = mMethodsGet(self)->LongRegisterAccessCreate(self);
    if (self->moduleLongRegisterAccess)
        self->longRegisterAccess = self->moduleLongRegisterAccess;

    /* Or use global register access of device */
    if (self->longRegisterAccess == NULL)
        self->longRegisterAccess = AtDeviceGlobalLongRegisterAccess(device);

    return self->longRegisterAccess;
    }

static uint16 HwLongReadOnCore(AtModule self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numReg;
    AtLongRegisterAccess registerAccess = mMethodsGet(self)->LongRegisterAccess(self, localAddress);

    mMethodsGet(self)->WillAccessRegister(self, localAddress);
    numReg = AtLongRegisterAccessRead(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);

    AtDeviceLongReadRegisterListenerCall(AtModuleDeviceGet(self), localAddress, dataBuffer, bufferLen, self->moduleId, AtIpCoreIdGet(core));
    return numReg;
    }

static uint16 HwLongWriteOnCore(AtModule self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numReg;
    AtLongRegisterAccess registerAccess = mMethodsGet(self)->LongRegisterAccess(self, localAddress);
    numReg = AtLongRegisterAccessWrite(registerAccess, AtIpCoreHalGet(core), localAddress, dataBuffer, bufferLen);

    AtDeviceLongWriteRegisterListenerCall(AtModuleDeviceGet(self), localAddress, dataBuffer, bufferLen, self->moduleId, AtIpCoreIdGet(core));
    return numReg;
    }

static eAtRet InterruptEnable(AtModule self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32 atindex, numManagers = mMethodsGet(self)->NumInterruptManagers(self);

    for (atindex = 0; atindex < numManagers; atindex++)
        ret |= AtInterruptManagerInterruptEnable(AtModuleInterruptManagerAtIndexGet(self, atindex), enable);

    return ret;
    }

static eBool InterruptIsEnabled(AtModule self)
    {
    eBool isEnabled = cAtFalse;
    uint32 atindex, numManagers = mMethodsGet(self)->NumInterruptManagers(self);

    for (atindex = 0; atindex < numManagers; atindex++)
        {
        isEnabled = AtInterruptManagerInterruptIsEnabled(AtModuleInterruptManagerAtIndexGet(self, atindex));
        if (isEnabled)
            return isEnabled;
        }

    return isEnabled;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
	AtUnused(localAddress);
	AtUnused(self);
    /* Concrete module must implement this function to check whether it has the
     * input register */
    return cAtFalse;
    }

static eBool MemoryCanBeTested(AtModule self)
    {
	AtUnused(self);
    /* Let concrete class determine whether its registers can be tested */
    return cAtFalse;
    }

static eAtRet MemoryTest(AtModule self)
    {
    return HelperAtModuleMemoryTest(self);
    }

static AtIterator RegisterIteratorCreate(AtModule self)
    {
	AtUnused(self);
    /* Do not know to create, just let the concrete class do */
    return NULL;
    }

static void IsrContextEnter(AtModule self)
    {
    self->isIsrContext = 1;
    }

static void IsrContextExit(AtModule self)
    {
    self->isIsrContext = 0;
    }

static const char *LoopbackMode2String(AtModule self, eAtLoopbackMode loopbackMode)
    {
	AtUnused(self);
    if (loopbackMode == cAtLoopbackModeRelease) return "release";
    if (loopbackMode == cAtLoopbackModeRemote)  return "remote";
    if (loopbackMode == cAtLoopbackModeLocal)   return "local";

    return "N/A";
    }

static eAtRet LoopbackShow(AtModule self)
    {
    eAtLoopbackMode loopbackMode;

    loopbackMode = AtModuleDebugLoopbackGet(self);
    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtOk;

    AtPrintc(cSevNormal, "- Loopback mode: ");
    AtPrintc(cSevInfo, "%s\r\n", LoopbackMode2String(self, loopbackMode));

    return cAtOk;
    }

static eAtRet Debug(AtModule self)
    {
    AtModuleRoleDebug(self);
    AtModuleLoopbackShow(self);

    return cAtOk;
    }

static eAtRet DebugEnable(AtModule self, eBool enable)
    {
    self->debugEnabled = enable;
    return cAtOk;
    }

static eBool DebugIsEnabled(AtModule self)
    {
    return self->debugEnabled;
    }

static eAtRet LoopbackSet(AtModule self, uint8 loopbackMode)
    {
	AtUnused(loopbackMode);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint8 LoopbackGet(AtModule self)
    {
	AtUnused(self);
    return 0;
    }

static eBool LoopbackIsSupported(AtModule self, uint8 loopbackMode)
    {
	AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;
    }

static void StatusClear(AtModule self)
    {
    eBool enabled = AtPrintIsEnabled();

    AtPrintEnable(cAtFalse);
    AtModuleDebug(self);
    AtPrintEnable(enabled);
    }

static AtOsalMutex Locker(AtModule self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->mutex == NULL)
        self->mutex = mMethodsGet(osal)->MutexCreate(osal);

    return self->mutex;
    }

static eAtRet Lock(AtModule self)
    {
    if (AtOsalMutexLock(Locker(self)) != cAtOsalOk)
        return cAtErrorOsalFail;

    self->locked = cAtTrue;
    return cAtOk;
    }

static eAtRet UnLock(AtModule self)
    {
    if (AtOsalMutexUnLock(Locker(self)) != cAtOsalOk)
        return cAtErrorOsalFail;

    self->locked = cAtFalse;
    return cAtOk;
    }

static eAtRet WarmRestore(AtModule self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool RegisterIsInRange(AtModule self, uint32 address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtTrue;
    }

static eBool ShouldCheckRegisterAccess(AtModule self)
    {
    /* This debug feature should be only enabled at the first phase when a
     * new product is defined. Or it can be enabled on existing products for
     * debugging invalid access. */
    AtUnused(self);
    return cAtFalse;
    }

static void WillAccessRegister(AtModule self, uint32 address)
    {
    if (!ShouldCheckRegisterAccess(self))
        return;

    if (mMethodsGet(self)->RegisterIsInRange(self, address))
        return;

    AtModuleLog(self, cAtLogLevelCritical,
                __FILE__, __LINE__,
                "Wrong register at address 0x%08x is accessed\r\n", address);
    }

static uint32 PeriodicProcess(AtModule self, uint32 periodInMs)
    {
    AtUnused(self);
    AtUnused(periodInMs);
    return 0;
    }

static uint32 ForcePointerPeriodicProcess(AtModule self, uint32 periodInMs)
    {
    AtUnused(self);
    AtUnused(periodInMs);
    return 0;
    }

static uint32 RecommendedPeriodInMs(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static AtModuleEventListenerWrapper EventListenerWrapperObjectInit(AtModuleEventListenerWrapper wrapper, AtModule module, void* listener, void* userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleAlarmListenerWrapper thisWrapper = (AtModuleAlarmListenerWrapper)wrapper;

    mMethodsGet(osal)->MemInit(osal, wrapper, 0, sizeof(tAtModuleAlarmListenerWrapper));

    AtModuleEventListenerWrapperObjectInit(wrapper, module, listener, userData);
    mMethodsGet(osal)->MemCpy(osal, &(thisWrapper->listener), listener, sizeof(tAtModuleAlarmListener));

    return wrapper;
    }

static AtModuleEventListenerWrapper EventListenerWrapperNew(AtModule self, void* listener, void* userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEventListenerWrapper newListener = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtModuleAlarmListenerWrapper));

    if (newListener)
        return EventListenerWrapperObjectInit(newListener, self, listener, userData);

    return NULL;
    }

static AtModuleEventListenerWrapper ListenerWrapperCreate(AtModule self, void* listener, void* userData)
    {
    return EventListenerWrapperNew(self, listener, userData);
    }

static eBool EventListenersAreIdentical(AtModule self, AtModuleEventListenerWrapper registeredListener, void* listener)
    {
    tAtModuleAlarmListener *input = (tAtModuleAlarmListener *)listener;
    AtModuleAlarmListenerWrapper local = (AtModuleAlarmListenerWrapper)registeredListener;
    AtUnused(self);

    if (input->AlarmChangeStateWithUserData != local->listener.AlarmChangeStateWithUserData)
        return cAtFalse;

    return cAtTrue;
    }

static void AlarmListenerCall(AtModuleEventListenerWrapper self, AtModule module, uint32 changedAlarms, uint32 currentStatus)
    {
    AtModuleAlarmListenerWrapper local = (AtModuleAlarmListenerWrapper)self;
    if (local->listener.AlarmChangeStateWithUserData)
        local->listener.AlarmChangeStateWithUserData(module, changedAlarms, currentStatus, self->userData);
    }

static eBool ListenerExist(AtModule self, void *listener)
    {
    AtIterator iterator = AtListIteratorCreate(self->allEventListeners);
    AtModuleEventListenerWrapper aListener;
    eBool exist = cAtFalse;

    /* For all event listeners */
    while ((aListener = (AtModuleEventListenerWrapper)AtIteratorNext(iterator)) != NULL)
        {
        if (mMethodsGet(self)->EventListenersAreIdentical(self, aListener, listener))
            {
            exist = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    return exist;
    }

static eAtRet EventListenerAdd(AtModule self, void *listener, void *userData)
    {
    eAtRet atRet = cAtError;
    AtModuleEventListenerWrapper newListener;

    if (self->allEventListeners == NULL)
        self->allEventListeners = AtListCreate(cAtMaxNumEventListener);

    if (ListenerExist(self, listener))
        return cAtOk;

    newListener = mMethodsGet(self)->ListenerWrapperCreate(self, listener, userData);
    if (newListener == NULL)
        return cAtError;

    /* Only add a new listener */
    AtOsalMutexLock(self->mutex);
    atRet = AtListObjectAdd(self->allEventListeners, (AtObject)newListener);
    AtOsalMutexUnLock(self->mutex);

    return atRet;
    }

static eAtRet EventListenerRemove(AtModule self, void *listener)
    {
    AtModuleEventListenerWrapper aListener;
    AtIterator iterator;

    if (self->allEventListeners == NULL)
        return cAtOk;

    iterator = AtListIteratorCreate(self->allEventListeners);

    AtOsalMutexLock(self->mutex);

    while((aListener = (AtModuleEventListenerWrapper)AtIteratorNext(iterator)) != NULL)
        {
        if (mMethodsGet(self)->EventListenersAreIdentical(self, aListener, listener))
            break;
        }

    /* Remove current object out of list */
    AtIteratorRemove(iterator);
    AtObjectDelete((AtObject)aListener);

    AtOsalMutexUnLock(self->mutex);
    AtObjectDelete((AtObject)iterator);

    return cAtOk;
    }

static uint32 AlarmGet(AtModule self)
    {
    AtUnused(self);

    return 0;
    }

static uint32 AlarmHistoryGet(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 SupportedInterruptMasks(AtModule self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return none */
    return 0x0;
    }

static eAtRet InterruptMaskSet(AtModule self, uint32 eventMask, uint32 enableMask)
    {
    AtUnused(enableMask);
    AtUnused(eventMask);
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtModule self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return no mask */
    return 0;
    }

static uint32 NumInternalRamsGet(AtModule self)
    {
    uint32 numRams;
    (void)mMethodsGet(self)->AllInternalRamsDescription(self, &numRams);
    return numRams;
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    AtUnused(self);
    if (numRams)
        *numRams = 0;
    return NULL;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    AtUnused(self);
    AtUnused(ramId);
    AtUnused(localRamId);
    return NULL;
    }

static eAtRet Reactivate(AtModule self)
    {
    uint32 moduleId = AtModuleTypeGet(self);
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceModuleReactivate(device, moduleId);
    }

static eAtRet Activate(AtModule self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet Deactivate(AtModule self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eBool IsActive(AtModule self)
    {
    uint32 moduleId = AtModuleTypeGet(self);
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceModuleIsActive(device, moduleId);
    }

static void AllChannelsListenedDefectClear(AtModule self)
    {
    AtUnused(self);
    }

static uint32 Restore(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet AllChannelsInterruptDisable(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool HwIsReady(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);

    if (!AtDeviceHwReady(device))
        return cAtFalse;

    return AtDeviceModuleHwReady(device, AtModuleTypeGet(self));
    }

static eBool HasRole(AtModule self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RoleActiveForce(AtModule self, eBool forced)
    {
    AtUnused(self);
    AtUnused(forced);
    return cAtErrorModeNotSupport;
    }

static eBool RoleActiveIsForced(AtModule self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RoleInputEnable(AtModule self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool RoleInputIsEnabled(AtModule self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RoleSet(AtModule self, eAtDeviceRole role)
    {
    eAtRet ret = cAtOk;

    /* Release forcing */
    if (role == cAtDeviceRoleAuto)
        {
        ret |= mMethodsGet(self)->RoleInputEnable(self, cAtTrue);
        ret |= mMethodsGet(self)->RoleActiveForce(self, cAtFalse);
        return ret;
        }

    /* Force to specified role */
    ret |= mMethodsGet(self)->RoleInputEnable(self, cAtFalse);
    ret |= mMethodsGet(self)->RoleActiveForce(self, (role == cAtDeviceRoleActive) ? cAtTrue : cAtFalse);

    return ret;
    }

static eAtDeviceRole RoleInputStatus(AtModule self)
    {
    /* Concrete should know */
    AtUnused(self);
    return cAtDeviceRoleUnknown;
    }

static eAtDeviceRole RoleGet(AtModule self)
    {
    /* If forcing is disabled, return auto role */
    if (mMethodsGet(self)->RoleInputIsEnabled(self))
        return cAtDeviceRoleAuto;

    return mMethodsGet(self)->RoleActiveIsForced(self) ? cAtDeviceRoleActive : cAtDeviceRoleStandby;
    }

static void RoleDebug(AtModule self)
    {
    if (!mMethodsGet(self)->HasRole(self))
        return;

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevInfo,   "Role: %s\r\n", AtDeviceRole2String(AtModuleRoleGet(self)));
    AtPrintc(cSevNormal, "- Input : %s\r\n", mMethodsGet(self)->RoleInputIsEnabled(self) ? "Enabled" : "Disabled");
    AtPrintc(cSevNormal, "- Forced: %s\r\n", mMethodsGet(self)->RoleActiveIsForced(self) ? "Yes" : "No");
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    /* Sub modules must know */
    AtUnused(self);
    AtUnused(ram);
    return cAtFalse;
    }

static AtDebugger DebuggerObjectCreate(AtModule self)
    {
    AtUnused(self);
    return AtDebuggerNew();
    }

static AtDebugger DebuggerCreate(AtModule self)
    {
    AtDebugger debugger = mMethodsGet(self)->DebuggerObjectCreate(self);
    mMethodsGet(self)->DebuggerEntriesFill(self, debugger);
    return debugger;
    }

static void DebuggerEntriesFill(AtModule self, AtDebugger debugger)
    {
    AtUnused(self);
    AtUnused(debugger);
    }

static eAtRet HwResourceCheck(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static AtQuerier QuerierGet(AtModule self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool ShouldSetupWhenNoHal(AtModule self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void CacheDelete(AtModule self)
    {
    void **cache = mMethodsGet(self)->CacheMemoryAddress(self);

    if (cache)
        {
        AtOsalMemFree(*cache);
        *cache = NULL;
        }
    }

static void *CacheCreate(AtModule self)
    {
    void *newCache;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 cacheSize = mMethodsGet(self)->CacheSize(self);

    if (cacheSize == 0)
        return NULL;

    newCache = mMethodsGet(osal)->MemAlloc(osal, cacheSize);
    if (newCache == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, newCache, 0, cacheSize);

    return newCache;
    }

static void **CacheMemoryAddress(AtModule self)
    {
    AtUnused(self);
    return NULL;
    }

static void *CacheGet(AtModule self)
    {
    void **memory;

    if (AtModuleAccessible(self))
        return NULL;

    memory = mMethodsGet(self)->CacheMemoryAddress(self);
    if (memory == NULL)
        return NULL;

    if ((*memory) == NULL)
        *memory = CacheCreate(self);

    return *memory;
    }

static uint32 CacheSize(AtModule self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TypeGet);
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, CapacityDescription);
        mMethodOverride(m_methods, DeviceGet);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, AsyncInit);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, HwRead);
        mMethodOverride(m_methods, HwWrite);
        mMethodOverride(m_methods, HoldRegistersGet);
        mMethodOverride(m_methods, HoldReadRegistersGet);
        mMethodOverride(m_methods, HoldWriteRegistersGet);
        mMethodOverride(m_methods, HwLongReadOnCore);
        mMethodOverride(m_methods, HwLongWriteOnCore);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, InterruptOnIpCoreEnable);
        mMethodOverride(m_methods, InterruptEnable);
        mMethodOverride(m_methods, InterruptIsEnabled);
        mMethodOverride(m_methods, HasRegister);
        mMethodOverride(m_methods, OamProcessOnCore);
        mMethodOverride(m_methods, MemoryCanBeTested);
        mMethodOverride(m_methods, MemoryTest);
        mMethodOverride(m_methods, RegisterIteratorCreate);
        mMethodOverride(m_methods, IsrContextEnter);
        mMethodOverride(m_methods, IsrContextExit);
        mMethodOverride(m_methods, StatusClear);
        mMethodOverride(m_methods, LongRegisterAccessCreate);
        mMethodOverride(m_methods, LongRegisterAccess);
        mMethodOverride(m_methods, Lock);
        mMethodOverride(m_methods, UnLock);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, DebugEnable);
        mMethodOverride(m_methods, DebugIsEnabled);
        mMethodOverride(m_methods, WarmRestore);
        mMethodOverride(m_methods, LoopbackSet);
        mMethodOverride(m_methods, LoopbackGet);
        mMethodOverride(m_methods, LoopbackIsSupported);
        mMethodOverride(m_methods, AllServicesDestroy);
        mMethodOverride(m_methods, WillAccessRegister);
        mMethodOverride(m_methods, RegisterIsInRange);
        mMethodOverride(m_methods, PeriodicProcess);
        mMethodOverride(m_methods, ForcePointerPeriodicProcess);
        mMethodOverride(m_methods, RecommendedPeriodInMs);
        mMethodOverride(m_methods, Restore);
        mMethodOverride(m_methods, EventListenersAreIdentical);
        mMethodOverride(m_methods, NumInternalRamsGet);
        mMethodOverride(m_methods, AllInternalRamsDescription);
        mMethodOverride(m_methods, InternalRamCreate);
        mMethodOverride(m_methods, ListenerWrapperCreate);
        mMethodOverride(m_methods, InterruptManagerCreate);
        mMethodOverride(m_methods, NumInterruptManagers);
        mMethodOverride(m_methods, AllChannelsListenedDefectClear);
        mMethodOverride(m_methods, AllChannelsInterruptDisable);
        mMethodOverride(m_methods, DebuggerObjectCreate);
        mMethodOverride(m_methods, DebuggerCreate);
        mMethodOverride(m_methods, DebuggerEntriesFill);
        mMethodOverride(m_methods, HwIsReady);
        mMethodOverride(m_methods, HasRole);
        mMethodOverride(m_methods, RoleSet);
        mMethodOverride(m_methods, RoleGet);
        mMethodOverride(m_methods, RoleInputStatus);
        mMethodOverride(m_methods, RoleActiveForce);
        mMethodOverride(m_methods, RoleActiveIsForced);
        mMethodOverride(m_methods, RoleInputEnable);
        mMethodOverride(m_methods, RoleInputIsEnabled);
        mMethodOverride(m_methods, InternalRamIsReserved);
        mMethodOverride(m_methods, HwResourceCheck);
        mMethodOverride(m_methods, QuerierGet);
        mMethodOverride(m_methods, ShouldSetupWhenNoHal);
        mMethodOverride(m_methods, CacheGet);
        mMethodOverride(m_methods, CacheSize);
        mMethodOverride(m_methods, CacheMemoryAddress);
        mMethodOverride(m_methods, Activate);
        mMethodOverride(m_methods, Deactivate);
        mMethodOverride(m_methods, IsActive);
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, SupportedInterruptMasks);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void DeleteAllListeners(AtModule self)
    {
    if (self->allEventListeners)
        {
        AtOsalMutexLock(self->mutex);

        /* Delete all listeners in list */
        while (AtListLengthGet(self->allEventListeners) > 0)
            {
            AtObject listenerWrapper = AtListObjectRemoveAtIndex(self->allEventListeners, 0);
            AtObjectDelete(listenerWrapper);
            }

        /* Delete event listener list */
        AtObjectDelete((AtObject)(self->allEventListeners));
        self->allEventListeners = NULL;

        AtOsalMutexUnLock(self->mutex);
        }
    }

static void DeleteAllInternalRam(AtModule self)
    {
    if (self->internalRams)
        {
        uint32 ram_i, numRams;
        numRams = AtModuleNumInternalRamsGet(self);
        for (ram_i = 0; ram_i < numRams; ram_i++)
            AtObjectDelete((AtObject)self->internalRams[ram_i]);
        AtOsalMemFree(self->internalRams);
        self->internalRams = NULL;
        }
    }

static void InterruptManagerListCreate(AtModule self)
    {
    uint32 size = sizeof (AtInterruptManager) * mMethodsGet(self)->NumInterruptManagers(self);
    if (size > 0)
        {
        self->interruptManagers = AtOsalMemAlloc(size);
        AtOsalMemInit(self->interruptManagers, 0, size);
        }
    }

static void DeleteAllInterruptManagers(AtModule self)
    {
    if (self->interruptManagers)
        {
        uint32 manager_i;
        uint32 numManagers = mMethodsGet(self)->NumInterruptManagers(self);
        for (manager_i = 0; manager_i < numManagers; manager_i++)
            AtObjectDelete((AtObject)self->interruptManagers[manager_i]);
        AtOsalMemFree(self->interruptManagers);
        self->interruptManagers = NULL;
        }
    }

static void Delete(AtObject self)
    {
    AtModule module = (AtModule)self;

    DeleteAllListeners(module);
    AtObjectDelete((AtObject)(module->moduleLongRegisterAccess));
    module->moduleLongRegisterAccess = NULL;
    AtOsalMutexDestroy(module->mutex);
    module->mutex = NULL;
    DeleteAllInternalRam(module);
    DeleteAllInterruptManagers(module);
    CacheDelete(module);

    /* Call super to fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static const char *ToString(AtObject self)
    {
    static char str[128];
    AtDevice dev = AtModuleDeviceGet((AtModule)self);

    AtSprintf(str, "%s%s", AtDeviceIdToString(dev), AtModuleTypeString((AtModule)self));
    return str;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModule object = (AtModule)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(moduleId);
    mEncodeUInt(isIsrContext);
    mEncodeUInt(locked);
    mEncodeUInt(defaultCoreId);
    mEncodeUInt(debugEnabled);

    mEncodeObjectDescription(device);
    mEncodeObject(moduleLongRegisterAccess);
    mEncodeObject(longRegisterAccess);

    mEncodeObjects(internalRams, AtModuleNumInternalRamsGet(object));
    mEncodeObjects(interruptManagers, mMethodsGet(object)->NumInterruptManagers(object));

    mEncodeNone(allEventListeners);
    mEncodeNone(mutex);
    mEncodeNone(haHal);
    }

static void Override(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

/* Constructor */
AtModule AtModuleObjectInit(AtModule self, eAtModule moduleId, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModule));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Override */
    Override(self);

    /* Initialize implement */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    /* Initialize private variables (if have) */
    self->moduleId = moduleId;
    self->device   = device;

    return self;
    }

eAtRet AtModuleDefaultCoreSet(AtModule self, uint8 coreId)
    {
    if (self)
        self->defaultCoreId = coreId;

    return cAtOk;
    }

uint8 AtModuleDefaultCoreGet(AtModule self)
    {
    if (self)
        return self->defaultCoreId;

    return 0;
    }

eBool AtModuleDeviceInWarmRestore(AtModule self)
    {
    return AtDeviceInWarmRestore(AtModuleDeviceGet(self));
    }

eBool AtModuleRegisterIsInRange(AtModule self, uint32 address)
    {
    if (self)
        return mMethodsGet(self)->RegisterIsInRange(self, address);

    return cAtFalse;
    }

static eBool NoHal(AtModule self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtModuleDeviceGet(self), AtModuleDefaultCoreGet(self));
    return (hal == NULL) ? cAtTrue : cAtFalse;
    }

/*
 * To create all managed objects and other internal data structures
 *
 * @param module Module
 *
 * @return AT return code
 */
eAtRet AtModuleSetup(AtModule self)
    {
    if (mModuleIsValid(self))
        {
        if (NoHal(self) && !mMethodsGet(self)->ShouldSetupWhenNoHal(self))
            return cAtOk;

        return mMethodsGet(self)->Setup(self);
        }

    return cAtError;
    }

eAtRet AtModulesMemoryTest(AtIterator moduleIterator)
    {
    AtModule module;
    eAtRet ret = cAtOk;

    /* Iterate all modules and test all of them */
    while ((module = (AtModule)AtIteratorNext(moduleIterator)) != NULL)
        {
        if (!AtModuleMemoryCanBeTested(module))
            continue;

        /* Memory testing may take a long time to complete testing all module. So
         * when one module is fail, need to exit to resolve it */
        ret = AtModuleMemoryTest(module);
        if (ret != cAtOk)
            break;
        }

    return ret;
    }

/*
 * To enter ISR context
 *
 * @param self
 */
void AtModuleIsrContextEnter(AtModule self)
    {
    if (self == NULL)
        return;

    self->methods->IsrContextEnter(self);
    }

/*
 * Exit ISR context, back to normal context
 * @param self
 */
void AtModuleIsrContextExit(AtModule self)
    {
    if (self == NULL)
        return;

    self->methods->IsrContextExit(self);
    }

/*
 * Create a register iterator to iterate all valid registers of modules
 *
 * @param self This module
 *
 * @return Register iterator
 */
AtIterator AtModuleRegisterIteratorCreate(AtModule self)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->RegisterIteratorCreate(self);

    return NULL;
    }

uint32 *AtModuleHoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    if (self)
        return mMethodsGet(self)->HoldRegistersGet(self, numberOfHoldRegisters);

    return NULL;
    }

uint32 *AtModuleHoldReadRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    if (self)
        return mMethodsGet(self)->HoldReadRegistersGet(self, numberOfHoldRegisters);

    return NULL;
    }

uint32 *AtModuleHoldWriteRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    if (self)
        return mMethodsGet(self)->HoldWriteRegistersGet(self, numberOfHoldRegisters);

    return NULL;
    }

uint32 AtModuleHwRead(AtModule self, uint32 localAddress, AtHal hal)
    {
    uint32 value;

    if (self == NULL)
    return 0;

    value = mMethodsGet(self)->HwRead(self, localAddress, hal);
    if (AtDriverDebugIsEnabled())
        AtDeviceReadNotify(AtModuleDeviceGet(self), localAddress, value, 0);

    return value;
    }

void AtModuleHwWrite(AtModule self, uint32 localAddress, uint32 value, AtHal hal)
    {
    if (self == NULL)
        return;

    mMethodsGet(self)->HwWrite(self, localAddress, value, hal);
    if (AtDriverDebugIsEnabled())
        AtDeviceWriteNotify(AtModuleDeviceGet(self), localAddress, value, 0);
    }

uint16 AtModuleHwLongReadOnCore(AtModule self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numDwords;

    if (self == NULL)
    return 0;

    numDwords = mMethodsGet(self)->HwLongReadOnCore(self, localAddress, dataBuffer, bufferLen, core);
    if (AtDriverDebugIsEnabled())
        AtDeviceLongReadNotify(AtModuleDeviceGet(self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

uint16 AtModuleHwLongWriteOnCore(AtModule self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
    {
    uint16 numDwords;

    if (self == NULL)
    return 0;

    numDwords = mMethodsGet(self)->HwLongWriteOnCore(self, localAddress, dataBuffer, bufferLen, core);
    if (AtDriverDebugIsEnabled())
        AtDeviceLongWriteNotify(AtModuleDeviceGet(self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

AtLongRegisterAccess AtModuleLongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (self)
        return mMethodsGet(self)->LongRegisterAccess(self, localAddress);
    return NULL;
    }

eAtRet AtModuleWarmRestore(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->WarmRestore(self);
    return cAtErrorNullPointer;
    }

eAtRet AtModuleAllServicesDestroy(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->AllServicesDestroy(self);
    return cAtErrorNullPointer;
    }

void AtModuleLog(AtModule self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...)
    {
    va_list args;
    if (self == NULL)
        return;

    va_start(args, format);
    AtDriverVaListLog(AtDriverSharedDriverGet(), level, file, line, format, args);
    va_end(args);
    }

uint32 AtModuleNumInternalRamsGet(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->NumInternalRamsGet(self);
    return 0;
    }

AtInternalRam AtModuleInternalRamGet(AtModule self, uint32 ramId, uint32 localRamId)
    {
    uint32 numInternalRam;

    if (self == NULL)
        return NULL;

    numInternalRam = AtModuleNumInternalRamsGet(self);
    if (localRamId >= numInternalRam)
        return NULL;

    if (self->internalRams == NULL)
        {
        uint32 memSize = numInternalRam * sizeof(AtInternalRam);
        self->internalRams = AtOsalMemAlloc(memSize);
        if (self->internalRams)
            AtOsalMemInit(self->internalRams, 0, memSize);
        }

    if (self->internalRams)
        {
        if (self->internalRams[localRamId] == NULL)
            self->internalRams[localRamId] = mMethodsGet(self)->InternalRamCreate(self, ramId, localRamId);
        return self->internalRams[localRamId];
        }

    return NULL;
    }

AtModuleEventListenerWrapper AtModuleEventListenerWrapperObjectInit(AtModuleEventListenerWrapper wrapper, AtModule module, void* listener, void* userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtUnused(listener);

    mMethodsGet(osal)->MemInit(osal, wrapper, 0, sizeof(tAtModuleEventListenerWrapper));

    AtObjectInit((AtObject)wrapper);

    wrapper->userData = userData;
    wrapper->module = module;

    return wrapper;
    }

AtInterruptManager AtModuleInterruptManagerGet(AtModule self)
    {
    return AtModuleInterruptManagerAtIndexGet(self, 0);
    }

AtInterruptManager AtModuleInterruptManagerAtIndexGet(AtModule self, uint32 managerIndex)
    {
    if (self == NULL)
        return NULL;

    if (self->interruptManagers == NULL)
        {
        InterruptManagerListCreate(self);
        if (self->interruptManagers == NULL)
            return NULL;
        }

    if (managerIndex >= mMethodsGet(self)->NumInterruptManagers(self))
        return NULL;

    if (self->interruptManagers[managerIndex] == NULL)
        self->interruptManagers[managerIndex] = mMethodsGet(self)->InterruptManagerCreate(self, managerIndex);

    return self->interruptManagers[managerIndex];
    }

const char **AtModuleAllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    if (self)
        return mMethodsGet(self)->AllInternalRamsDescription(self, numRams);
        
    return NULL;
    }

eAtRet AtModuleReactivate(AtModule self)
    {
    if (self)
        return Reactivate(self);
    return cAtErrorNullPointer;
    }

eAtRet AtModuleActivate(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->Activate(self);
    return cAtErrorNullPointer;
    }

eAtRet AtModuleDeactivate(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->Deactivate(self);
    return cAtErrorNullPointer;
    }

eBool AtModuleIsActive(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->IsActive(self);
    return cAtFalse;
    }

void AtModuleAllChannelsListenedDefectClear(AtModule self)
    {
    if (self)
        mMethodsGet(self)->AllChannelsListenedDefectClear(self);
    }

void AtModuleInterruptProcess(AtModule self, uint32 globalIntr, AtIpCore ipCore)
    {
    if (self == NULL)
        return;

    AtModuleLock(self);
    mMethodsGet(self)->InterruptProcess(self, globalIntr, ipCore);
    AtModuleUnLock(self);
    }

uint32 AtModuleRestore(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->Restore(self);
    return 0;
    }

eAtRet AtModuleLoopbackShow(AtModule self)
    {
    if (self)
        return LoopbackShow(self);
    return cAtErrorNotExist;
    }

eAtRet AtModuleAsyncInit(AtModule self)
    {
    mAttributeGet(AsyncInit, eAtRet, cAtErrorNullPointer);
    }

eBool AtModuleHwIsReady(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->HwIsReady(self);
    return cAtFalse;
    }

void AtModuleRoleDebug(AtModule self)
    {
    if (self)
        RoleDebug(self);
    }

eAtRet AtModuleRoleSet(AtModule self, eAtDeviceRole role)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (mMethodsGet(self)->HasRole(self))
        return mMethodsGet(self)->RoleSet(self, role);

    return (role == cAtDeviceRoleAuto) ? cAtOk : cAtErrorModeNotSupport;
    }

eAtDeviceRole AtModuleRoleGet(AtModule self)
    {
    if (self == NULL)
        return cAtDeviceRoleUnknown;

    if (mMethodsGet(self)->HasRole(self))
        return mMethodsGet(self)->RoleGet(self);

    return cAtDeviceRoleAuto;
    }

eBool AtModuleHasRole(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->HasRole(self);
    return cAtFalse;
    }

eAtRet AtModuleRoleActiveForce(AtModule self, eBool forced)
    {
    if (self)
        return mMethodsGet(self)->RoleActiveForce(self, forced);

    return cAtErrorNullPointer;
    }

eBool AtModuleRoleActiveIsForced(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->RoleActiveIsForced(self);

    return cAtFalse;
    }

eAtRet AtModuleRoleInputEnable(AtModule self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->RoleInputEnable(self, enable);

    return cAtErrorNullPointer;
    }

eBool AtModuleRoleInputIsEnabled(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->RoleInputIsEnabled(self);

    return cAtFalse;
    }

eAtDeviceRole AtModuleRoleInputStatus(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->RoleInputStatus(self);

    return cAtDeviceRoleUnknown;
    }

eBool AtModuleInternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (self)
        return mMethodsGet(self)->InternalRamIsReserved(self, ram);
    return cAtFalse;
    }

uint32 AtModuleSupportedInterruptMasks(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->SupportedInterruptMasks(self);
    return 0x0;
    }

/*
 * Create debugger
 *
 * @param self This module
 *
 * @return Debugger instance
 */
AtDebugger AtModuleDebuggerCreate(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->DebuggerCreate(self);
    return NULL;
    }

eBool AtModuleAccessible(AtModule self)
    {
    return AtDeviceAccessible(AtModuleDeviceGet(self));
    }

eBool AtModuleInAccessible(AtModule self)
    {
    return AtDeviceAccessible(AtModuleDeviceGet(self)) ? cAtFalse : cAtTrue;
    }

eAtRet AtModuleHwResourceCheck(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->HwResourceCheck(self);
    return cAtErrorNullPointer;
    }

void AtModuleResourceDiminishedLog(AtModule self, const char* name, uint32 free, uint32 max)
    {
    if (free != max)
        AtModuleLog(self, cAtLogLevelWarning, AtSourceLocation, "%s: Resource '%s' is diminished, free %u, max %u.\r\n",
                    AtObjectToString((AtObject)self),
                    name, free, max);
    }

AtQuerier AtModuleQuerierGet(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->QuerierGet(self);
    return NULL;
    }

void *AtModuleCacheGet(AtModule self)
    {
    if (self)
        return mMethodsGet(self)->CacheGet(self);
    return NULL;
    }

/* Call all alarm listeners of this channel */
void AtModuleAllAlarmListenersCall(AtModule self, uint32 changedAlarms, uint32 currentStatus)
    {
    AtModuleEventListenerWrapper aListener;
    AtIterator iterator;

    if ((self == NULL) || (self->allEventListeners == NULL))
        return;

    /* Opps, nothing to be report. */
    if (changedAlarms == 0)
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(self->allEventListeners);
    while((aListener = (AtModuleEventListenerWrapper)AtIteratorNext(iterator)) != NULL)
        AlarmListenerCall(aListener, self, changedAlarms, currentStatus);

    AtObjectDelete((AtObject)iterator);
    }

/**
 * @addtogroup AtModule
 * @{
 */

/**
 * Get type of this module
 *
 * @param self This module
 *
 * @return Module type
 */
eAtModule AtModuleTypeGet(AtModule self)
    {
    mAttributeGet(TypeGet, eAtModule, cAtModuleInvalid);
    }

/**
 * Get module type string
 *
 * @param self This module
 *
 * @return Module type string
 */
const char *AtModuleTypeString(AtModule self)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->TypeString(self);

    return "NULL";
    }

/**
 * Get module capacity descriptions
 *
 * @param self This module
 * @return Capacity description
 */
const char *AtModuleCapacityDescription(AtModule self)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->CapacityDescription(self);

    return NULL;
    }

/**
 * Get device that this module belongs to
 *
 * @param self This module
 *
 * @return Device that this module belongs to
 */
AtDevice AtModuleDeviceGet(AtModule self)
    {
    mNoParamObjectGet(DeviceGet, AtDevice);
    }

/**
 * Initialize this module
 *
 * @param self This module
 */
eAtRet AtModuleInit(AtModule self)
    {
    mNoParamCall(Init, eAtRet, cAtErrorNullPointer);
    }

/**
 * Enable/disable interrupt on this module
 *
 * @param self This module
 * @param enable Enable/disable
 *
 * @return AT return code
 */
eAtRet AtModuleInterruptEnable(AtModule self, eBool enable)
    {
    mNumericalAttributeSet(InterruptEnable, enable);
    }

/**
 * Get interrupt on this module is enabled or disabled
 *
 * @param self This module
 *
 * @return Enabled or disabled
 */
eBool AtModuleInterruptIsEnabled(AtModule self)
    {
    mAttributeGet(InterruptIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if registers of a module can be tested
 *
 * @param self This module
 *
 * @return cAtTrue if memory testing is appicable. cAtFalse is returned for otherwise
 */
eBool AtModuleMemoryCanBeTested(AtModule self)
    {
    mAttributeGet(MemoryCanBeTested, eBool, cAtFalse);
    }

/**
 * Test memory of a module
 *
 * @param self This module
 *
 * @return AT return code
 */
eAtRet AtModuleMemoryTest(AtModule self)
    {
    mNoParamCall(MemoryTest, eAtRet, cAtErrorNullPointer);
    }

/**
 * Clear module status and all of its channels
 *
 * @param self This module
 */
void AtModuleStatusClear(AtModule self)
    {
    if (mModuleIsValid(self))
        mMethodsGet(self)->StatusClear(self);
    }

/**
 * Lock a module. When a module is locked, deleting operation on any objects
 * that it manages will pend caller until it is unlocked.
 *
 * @param self This module
 *
 * @return AT return code
 */
eAtRet AtModuleLock(AtModule self)
    {
    mAttributeGet(Lock, eAtRet, cAtErrorNullPointer);
    }

/**
 * Unlock a module. When a module is unlocked, deleting operation on any objects
 * will not pend caller.
 *
 * @param self This module
 * @return AT return code.
 */
eAtRet AtModuleUnLock(AtModule self)
    {
    mAttributeGet(UnLock, eAtRet, cAtErrorNullPointer);
    }

/**
 * Show module debug information
 *
 * @param self This module
 *
 * @return AT return code
 */
eAtRet AtModuleDebug(AtModule self)
    {
    mNoParamCall(Debug, eAtRet, cAtErrorNullPointer);
    }

/**
 * Enter debug mode
 *
 * @param self This module
 * @param enable cAtTrue to enter debug mode. cAtFalse to exit that mode.
 *
 * @return AT return code
 */
eAtRet AtModuleDebugEnable(AtModule self, eBool enable)
    {
    mNumericalAttributeSet(DebugEnable, enable);
    }

/**
 * Check if a module is in debug mode
 *
 * @param self This module
 *
 * @retval cAtTrue if this module is in debug mode
 * @retval cAtFalse if this module is not in debug mode
 */
eBool AtModuleDebugIsEnabled(AtModule self)
    {
    mAttributeGet(DebugIsEnabled, eBool, cAtFalse);
    }

/**
 * Loopback at module level
 *
 * @param self This module
 * @param loopMode Loopback mode
 *
 * @return AT return code
 */
eAtRet AtModuleDebugLoopbackSet(AtModule self, uint8 loopMode)
    {
    mNumericalAttributeSet(LoopbackSet, loopMode);
    }

/**
 * Get loopback at module level
 *
 * @param self This module
 *
 * @return Loopback mode
 */
uint8 AtModuleDebugLoopbackGet(AtModule self)
    {
    mAttributeGet(LoopbackGet, uint8, cAtError);
    }

/**
 * Check if module loopback is supported
 *
 * @param self This module
 * @param loopMode Loopback mode
 *
 * @retval cAtTrue if module loopback is supported
 * @retval cAtFalse if module loopback is not supported
 */
eBool AtModuleDebugLoopbackIsSupported(AtModule self, uint8 loopMode)
    {
    mOneParamAttributeGet(LoopbackIsSupported, loopMode, eBool, cAtFalse);
    }

/**
 * Some modules need this API be called every N(ms). This should be done in
 * application task (thread).
 *
 * @param self This module
 * @param periodInMs Polling period. It would be better to call this API every
 *        N(ms) returned by AtModuleRecommendedPeriodInMs().
 *
 * @return Time(ms) takes by this API.
 *
 * @see AtModuleRecommendedPeriodInMs().
 */
uint32 AtModulePeriodicProcess(AtModule self, uint32 periodInMs)
    {
    if (self)
        return mMethodsGet(self)->PeriodicProcess(self, periodInMs);
    return 0;
    }

/**
 * Get recommended polling period that application should invoke
 * AtModulePeriodicProcess().
 *
 * @param self This module
 *
 * @retval Recommended polling period in milliseconds if module needs to poll.
 * @retval 0 If module does not need to poll.
 *
 * @see AtModulePeriodicProcess()
 */
uint32 AtModuleRecommendedPeriodInMs(AtModule self)
    {
    mAttributeGet(RecommendedPeriodInMs, uint32, 0);
    }

/**
 * Add event listener. Application just implemented functions defined in event
 * listener interface in order to receiving event.
 *
 * @param self This module
 * @param listener Event listener
 *                 @ref tAtModuleRamEventListener "Default RAM listener."
 *                 @ref tAtModuleSurEventListener "Default SUR listener."
 * @param userData Input user data. It will returned once callback is called.
 *
 * @return AT return code
 */
eAtRet AtModuleEventListenerAdd(AtModule self, void *listener, void *userData)
    {
    if (mModuleIsValid(self) && listener)
        return EventListenerAdd(self, listener, userData);

    return cAtErrorNullPointer;
    }

/**
 * Remove event listener
 *
 * @param self This channel
 * @param listener Event listener
 *
 * @return AT return code
 */
eAtRet AtModuleEventListenerRemove(AtModule self, void *listener)
    {
    if (!mModuleIsValid(self))
        return cAtErrorNullPointer;

    if (listener == NULL)
        return cAtOk;

    return EventListenerRemove(self, listener);
    }

/**
 * Get alarm of this module
 *
 * @param self This module
 *
 * @return Current alarms. Note, each module have predefined alarm type
 *         masks. Refer:
 *         - @ref eAtModulePtpAlarmType "PTP module alarm types"
 *         This API ORed all of happening alarms and return
 */
uint32 AtModuleAlarmGet(AtModule self)
    {
    if (!self)
        return 0;

    return mMethodsGet(self)->AlarmGet(self);
    }

/**
 * Get alarm interrupt change status. When there is any change in alarm from clear to
 * raise or vice versa from raise to clear, corresponding alarm bit is set to 1
 * and kept that state until AtModuleAlarmInterruptClear() is called. If
 * interrupt is enabled by AtModuleInterruptMaskSet(), the global interrupt pin
 * will be triggered to let CPU know
 *
 * @param self This module
 *
 * @return Alarm interrupt change status. Note, channels of each module have predefined alarm
 *         masks. Refer:
 *         - @ref eAtModulePtpAlarmType "PTP module alarm types"
 *         This API ORed all of alarms that changed their state in the past
 */
uint32 AtModuleAlarmInterruptGet(AtModule self)
    {
    if (!self)
        return 0;

    return mMethodsGet(self)->AlarmHistoryGet(self);
    }

/**
 * Clear alarm history
 *
 * @param self This module
 *
 * @return Alarm history before clearing
 */
uint32 AtModuleAlarmInterruptClear(AtModule self)
    {
    if (!self)
        return 0;

    return mMethodsGet(self)->AlarmHistoryClear(self);
    }

/**
 * Set interrupt mask.
 *
 * @param self This module
 * @param eventMask Mask of events that will be configured
 * @param enableMask Interrupt mask of each defect
 *
 * @return AT return code.
 *
 * @note each module have predefined alarm masks. Refer:
 *       - @ref eAtModulePtpAlarmType "PTP module alarm types"
 */
eAtRet AtModuleInterruptMaskSet(AtModule self, uint32 eventMask, uint32 enableMask)
    {
    uint32 unsupportedMasks;

    if (eventMask == 0)
        return cAtOk;

    if (!self)
        {
        AtDeviceLog(AtModuleDeviceGet(self), cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    unsupportedMasks = ~AtModuleSupportedInterruptMasks(self);
    unsupportedMasks &= (eventMask & enableMask);
    if (unsupportedMasks)
        {
        AtDeviceLog(AtModuleDeviceGet(self), cAtLogLevelCritical, AtSourceLocation,
                     "does not support interrupt mask 0x%x\r\n", unsupportedMasks);
        return cAtErrorModeNotSupport;
        }

    mTwoParamsAttributeSet(InterruptMaskSet, eventMask, enableMask);
    }

/**
 * Get interrupt mask
 *
 * @param self This module
 *
 * @return Interrupt mask
 */
uint32 AtModuleInterruptMaskGet(AtModule self)
    {
    mAttributeGet(InterruptMaskGet, uint32, 0);
    }

/**
 * Check if an interrupt mask is supported or not.
 *
 * @param self This module
 * @param eventMask Mask of events that will be configured
 *
 * @retval cAtTrue if the interrupt mask is supported.
 *         cAtFalse if the interrupt mask is not supported.
 *
 * @note each module have predefined alarm masks. Refer:
 *       - @ref eAtModulePtpAlarmType "Module PTP alarm types"
 */
eBool AtModuleInterruptMaskIsSupported(AtModule self, uint32 eventMask)
    {
    if (!self)
        {
        AtDeviceLog(AtModuleDeviceGet(self), cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    if (eventMask == 0)
        return cAtTrue;

    if (eventMask & AtModuleSupportedInterruptMasks(self))
        return cAtTrue;

    return cAtFalse;
    }

/**
 * @}
 */

uint32 AtModuleForcePointerPeriodicProcess(AtModule self, uint32 periodInMs)
    {
    if (self)
        return mMethodsGet(self)->ForcePointerPeriodicProcess(self, periodInMs);
    return 0;
    }

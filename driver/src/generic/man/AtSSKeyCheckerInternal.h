/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaSSKeyCheckerInternal.h
 * 
 * Created Date: Sep 18, 2013
 *
 * Description : SSKey checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSSKEYCHECKERINTERNAL_H_
#define _ATSSKEYCHECKERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "AtSSKeyChecker.h"
#include "AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtSSKeyMode
    {
    cAtSSKeyModeUnknown, /* Unknown mode */
    cAtSSKeyModeTimeout, /* Timeout mode (no key) */
    cAtSSKeyModeRealKey  /* Real key is used */
    }eAtSSKeyMode;

typedef struct tAtSSKeyCheckerMethods
    {
    uint32 (*StickyAddress)(AtSSKeyChecker self);
    uint32 (*CodeMsbAddress)(AtSSKeyChecker self);
    uint32 (*CodeLsbAddress)(AtSSKeyChecker self);
    uint32 (*CodeExpectedMsb)(AtSSKeyChecker self);
    uint32 (*CodeExpectedLsb)(AtSSKeyChecker self);
    eAtRet (*Check)(AtSSKeyChecker self);
    eAtSSKeyMode (*KeyMode)(AtSSKeyChecker self);
    }tAtSSKeyCheckerMethods;

typedef struct tAtSSKeyChecker
    {
    tAtObject super;
    const tAtSSKeyCheckerMethods *methods;

    /* Private data */
    AtDevice device;
    }tAtSSKeyChecker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSSKeyChecker AtSSKeyCheckerObjectInit(AtSSKeyChecker self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATSSKEYCHECKERINTERNAL_H_ */


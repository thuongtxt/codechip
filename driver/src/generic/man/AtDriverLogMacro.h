/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Driver
 *
 * File        : AtDriverLogMacros.h
 *
 * Created Date: Jul 29, 2015
 *
 * Description : Log macros
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATDRIVERLOGMACROS_H_
#define _ATDRIVERLOGMACROS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDriverApiLogMacro.h"
#include "../util/AtUtil.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mNullObjectCheck(returnValue)                                          \
    {                                                                          \
    if (self == NULL)                                                          \
        {                                                                      \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
        return returnValue;                                                    \
        }                                                                      \
    }
 
#define mRetCheck(returnValue)                                                 \
    do                                                                         \
        {                                                                      \
        if (returnValue != cAtOk)                                              \
            {                                                                  \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                    AtSourceLocation, "%s fail with ret = %s\r\n", \
                                    AtFunction,                                \
                                    AtRet2String(returnValue));                \
                                                                               \
            return returnValue;                                                \
            }                                                                  \
        }while(0)

#define mLogLevel(ret) (((ret) == cAtOk) ? cAtLogLevelNone : cAtLogLevelCritical)

#define mNumericalAttributeSetWithCacheMethod(method, value)                   \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mOneParamApiLogStart(value);                                               \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        mMethodsGet(self)->Cache##method(self, value);                         \
        ret = mMethodsGet(self)->method(self, value);                          \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), value, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
    return ret;                                                                \
    }

#define mNumericalAttributeSetWithCache(method, value)                         \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mOneParamApiLogStart(value);                                               \
    if (self)                                                                  \
        {                                                                      \
        Cache##method(self, value);                                            \
        ret = mMethodsGet(self)->method(self, value);                          \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u) = %s\r\n",  \
                                    AtFunction, AtObjectToString((AtObject)self), value, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mNumericalAttributeSetThenCache(method, value)                         \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mOneParamApiLogStart(value);                                               \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, value);                          \
        if (ret == cAtOk)                                                      \
            Cache##method(self, value);                                        \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u) = %s\r\n",  \
                                    AtFunction, AtObjectToString((AtObject)self), value, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mGeneralAttributeGetWithCache(method, value)                           \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        if (mInAccessible(self))                                               \
            return Cache##method(self, value);                                 \
        ret = mMethodsGet(self)->method(self, value);                          \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s]) = %s\r\n",   \
                                    AtFunction, AtObjectToString((AtObject)self), AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return ret;                                                                \
    }

#define mNumericalAttributeSet(method, value)                                  \
    {                                                                          \
    eAtRet ret_ = cAtErrorObjectNotExist;                                      \
                                                                               \
    mOneParamApiLogStart(value);                                               \
    if (self)                                                                  \
        {                                                                      \
        ret_ = mMethodsGet(self)->method(self, value);                         \
        if (AtDriverShouldLog(mLogLevel(ret_)))                                \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret_),\
                                    AtSourceLocation, "%s([%s], %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), value, AtRet2String(ret_)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret_;                                                               \
    }

#define mUint64AttributeSet(method, value)                                     \
    {                                                                          \
    eAtRet ret_ = cAtErrorObjectNotExist;                                      \
                                                                               \
    mOneUint64ParamApiLogStart(value);                                         \
    if (self)                                                                  \
        {                                                                      \
        ret_ = mMethodsGet(self)->method(self, value);                         \
        if (AtDriverShouldLog(mLogLevel(ret_)))                                \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret_),\
                                    AtSourceLocation, "%s([%s], %llu) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), value, AtRet2String(ret_)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret_;                                                               \
    }

#define mNoParamAttributeSet(method)                                           \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mNoParamApiLogStart();                                                     \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self);                                 \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s]) = %s\r\n",     \
                                    AtFunction, AtObjectToString((AtObject)self), AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mObjectSet(method, object)                                             \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mOneObjectApiLogStart(object);                                             \
                                                                               \
    if (self)                                                                  \
        {                                                                      \
        const char* objectDesc;                                                \
        ret = mMethodsGet(self)->method(self, object);                         \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            objectDesc = AtDriverObjectStringToSharedBuffer((AtObject)object); \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %s) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), objectDesc, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                               \
    }

#define mTwoParamsAttributeSet(method, param1, param2)                         \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mTwoParamsApiLogStart(param1, param2);                                     \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, param1, param2);                 \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u, %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), param1, param2, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mTwoParamsAttributeSetWithMoreLogOption(method, param1, param2)        \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
    eBool isApiLogEnabled = AtDriverApiLogIsEnabled();                         \
    if(!AtDriverAllApiLogIsEnabled() && isApiLogEnabled)                        \
        AtDriverApiLogEnable(cAtFalse);                                        \
    mTwoParamsApiLogStart(param1, param2);                                     \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, param1, param2);                 \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u, %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), param1, param2, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
    if(!AtDriverAllApiLogIsEnabled() && isApiLogEnabled)                        \
        AtDriverApiLogEnable(cAtTrue);                                         \
                                                                               \
    return ret;                                                                \
    }

#define mTwoParamsAttributeSetWithCache(method, param1, param2)                \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mTwoParamsApiLogStart(param1, param2);                                     \
    if (self)                                                                  \
        {                                                                      \
        Cache##method(self, param1, param2);                                   \
        ret = mMethodsGet(self)->method(self, param1, param2);                 \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u, %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), param1, param2, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mThreeParamsAttributeSet(method, param1, param2, param3)               \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mThreeParamsApiLogStart(param1, param2, param3);                           \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, param1, param2, param3);         \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u, %u, %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), param1, param2, param3, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mDirectAttributeGet(attr, returnType, invalidValue)                    \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        return (returnType)self->attr;                                         \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mAttributeGet(method, returnType, invalidValue)                        \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value = mMethodsGet(self)->method(self);                    \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,   \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mAttributeGetIfCondition(method, returnType, invalidValue, condition)  \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value;                                                      \
                                                                               \
        if (!(condition))                                                      \
            return invalidValue;                                               \
                                                                               \
        value = mMethodsGet(self)->method(self);                               \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mNonApiLogNumericalAttributeSet(method, param)                         \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        eAtRet ret = mMethodsGet(self)->method(self, param);                   \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), param, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        return ret;                                                            \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return cAtErrorNullPointer;                                                \
    }

#define mOneParamAttributeGet(method, param, returnType, invalidValue)         \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value = mMethodsGet(self)->method(self, param);             \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mTwoParamAttributeGet(method, param1, param2, returnType, invalidValue)\
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value = mMethodsGet(self)->method(self, param1, param2);    \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mThreeParamAttributeGet(method, param1, param2, param3, returnType, invalidValue)\
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value = mMethodsGet(self)->method(self, param1, param2, param3);    \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mOneParamAttributeGetWithCache(method, param, returnType, invalidValue) \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value;                                                      \
        if (mInAccessible(self))                                               \
            return Cache##method(self, param);                                 \
        value = mMethodsGet(self)->method(self, param);                        \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mNoParamObjectGet(method, returnType)                                  \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType object = mMethodsGet(self)->method(self);                   \
        return object;                                                         \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return NULL;                                                               \
    }

#define mOneParamObjectGet(method, returnType, value)                          \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType object = mMethodsGet(self)->method(self, value);            \
        return object;                                                         \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return NULL;                                                               \
    }

#define mAttributeGetWithCache(method, returnType, invalidValue)               \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value;                                                      \
        if (mInAccessible(self))                                               \
            return Cache##method(self);                                        \
        value = mMethodsGet(self)->method(self);                               \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mAttributeGetWithCacheMethod(method, returnType, invalidValue)         \
    {                                                                          \
    if (self)                                                                  \
        {                                                                      \
        returnType value;                                                      \
        if (mInAccessible(self))                                               \
            return mMethodsGet(self)->Cache##method(self);                     \
        value = mMethodsGet(self)->method(self);                               \
        return value;                                                          \
        }                                                                      \
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,    \
                            AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    return invalidValue;                                                       \
    }

#define mLogNullPointer()                                                      \
    do                                                                         \
        {                                                                      \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction); \
        return cAtErrorNullPointer;                                            \
        }while(0)

#define mAttributeErrorLog(value, returnCode)                                  \
    do                                                                         \
        {                                                                      \
        AtDriverLogLock();                                                     \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, \
                                AtSourceLocation, "%s([%s], %u) = %s\r\n",   \
                                AtFunction, AtObjectToString((AtObject)self), value, AtRet2String(returnCode)); \
        AtDriverLogUnLock();                                                   \
        return returnCode;                                                     \
        }while(0)

#define mDoNothingOnStandby(returnVal)                                         \
    do                                                                         \
        {                                                                      \
        if (mInAccessible(self))                                               \
            return returnVal;                                                  \
        }while(0)

#define mNoParamCall(method, returnType, invalidValue)                         \
    {                                                                          \
    returnType value;                                                          \
                                                                               \
    mNoParamApiLogStart();                                                     \
    if (self)                                                                  \
        value = mMethodsGet(self)->method(self);                               \
    else                                                                       \
        {                                                                      \
        value = invalidValue;                                                  \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
        }                                                                      \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return value;                                                              \
    }

#define mOneParamObjectCreate(method, returnType, value)                       \
    {                                                                          \
    returnType object;                                                         \
                                                                               \
    mOneParamApiLogStart(value);                                               \
                                                                               \
    if (self)                                                                  \
        object = mMethodsGet(self)->method(self, value);                       \
    else                                                                       \
        {                                                                      \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
        object = NULL;                                                         \
        }                                                                      \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return object;                                                             \
    }

#define mOneObjectParamObjectCreate(method, returnType, inObject)              \
    {                                                                          \
    returnType object;                                                         \
                                                                               \
    mOneObjectApiLogStart(inObject);                                           \
                                                                               \
    if (self)                                                                  \
        object = mMethodsGet(self)->method(self, inObject);                    \
    else                                                                       \
        {                                                                      \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
        object = NULL;                                                         \
        }                                                                      \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return object;                                                             \
    }

#define mTwoParamsObjectCreate(method, param1, param2, returnType, invalidValue)\
    {                                                                          \
    returnType value;                                                          \
                                                                               \
    mTwoParamsApiLogStart(param1, param2);                                     \
                                                                               \
    if (self)                                                                  \
        value = mMethodsGet(self)->method(self, param1, param2);               \
    else                                                                       \
        {                                                                      \
        value = invalidValue;                                                  \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
        }                                                                      \
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return value;                                                              \
    }

#define mBufferAttributeSet(method, buffer, bufferLength)                      \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mBufferAttributeApiLogStart(buffer, bufferLength);                         \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, buffer, bufferLength);           \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %p, %u) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), buffer, bufferLength, AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

#define mTwoGenericParamsAttributeSet(method, param1, param2)                  \
    {                                                                          \
    eAtRet ret = cAtErrorNullPointer;                                          \
                                                                               \
    mTwoGenericParamsApiLogStart(param1, param2);                              \
    if (self)                                                                  \
        {                                                                      \
        ret = mMethodsGet(self)->method(self, param1, param2);                 \
        if (AtDriverShouldLog(mLogLevel(ret)))                                 \
            {                                                                  \
            AtDriverLogLock();                                                 \
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(), mLogLevel(ret), \
                                    AtSourceLocation, "%s([%s], %d, %d) = %s\r\n", \
                                    AtFunction, AtObjectToString((AtObject)self), (uint32)(param1), (uint32)(param2), AtRet2String(ret)); \
            AtDriverLogUnLock();                                               \
            }                                                                  \
        }                                                                      \
    else                                                                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,\
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);\
                                                                               \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATDRIVERLOGMACROS_H_ */


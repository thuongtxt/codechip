/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : AtDebugEntry.c
 *
 * Created Date: May 18, 2017
 *
 * Description : Debug entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDebugger.h"
#include "../man/AtDriverInternal.h" /* For OSAL */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtDebugEntry)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDebugEntry
    {
    tAtObject super;

    /* Private data */
    eAtSevLevel severity;
    char *name;
    char *value;
    }tAtDebugEntry;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtOsalMemFree(mThis(self)->name);
    mThis(self)->name = NULL;
    AtOsalMemFree(mThis(self)->value);
    mThis(self)->value = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void NameSet(AtDebugEntry self, const char *name)
    {
    if (self->name)
        {
        AtOsalMemFree(self->name);
        self->name = NULL;
        }

    if (name)
        self->name = AtStrdup(name);
    }

static void ValueSet(AtDebugEntry self, const char *value)
    {
    if (self->value)
        {
        AtOsalMemFree(self->value);
        self->value = NULL;
        }

    if (value)
        self->value = AtStrdup(value);
    }

static void OverrideAtObject(AtDebugEntry self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtDebugEntry self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDebugEntry);
    }

static AtDebugEntry ObjectInit(AtDebugEntry self, eAtSevLevel severity, const char *name, const char *value)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->severity = severity;
    if (name)
    self->name = AtStrdup(name);
    if (value)
    self->value = AtStrdup(value);

    return self;
    }

AtDebugEntry AtDebugEntryNew(eAtSevLevel severity, const char *name, const char *value)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDebugEntry newEntry = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEntry == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEntry, severity, name, value);
    }

AtDebugEntry AtDebugEntryStringFormatNew(eAtSevLevel severity, char *buffer, uint32 bufferSize, const char *name, const char *valueFormat, ...)
    {
    AtDebugEntry newEntry = AtDebugEntryNew(severity, name, NULL);
    va_list args;

    /* Build value */
    va_start(args, valueFormat);
    AtStdSnprintf(AtStdSharedStdGet(), buffer, bufferSize, valueFormat, args);
    va_end(args);

    AtDebuggerEntryValueSet(newEntry, buffer);
    return newEntry;
    }

AtDebugEntry AtDebugEntryErrorBitNew(const char* name, uint32 value, uint32 bitMask)
    {
    eAtSevLevel severity = (value & bitMask) ? cSevCritical : cSevInfo;
    const char *stringValue = (value & bitMask) ? "set" : "clear";
    return AtDebugEntryNew(severity, name, stringValue);
    }

AtDebugEntry AtDebugEntryWarningBitNew(const char* name, uint32 value, uint32 bitMask)
    {
    eAtSevLevel severity = (value & bitMask) ? cSevWarning : cSevNormal;
    const char *stringValue = (value & bitMask) ? "set" : "clear";
    return AtDebugEntryNew(severity, name, stringValue);
    }

AtDebugEntry AtDebugEntryInfoBitNew(const char* name, uint32 hwValue, uint32 mask)
    {
    eAtSevLevel severity = (hwValue & mask) ? cSevDebug : cSevNormal;
    const char *stringValue = (hwValue & mask) ? "ok" : "clear";
    return AtDebugEntryNew(severity, name, stringValue);
    }

AtDebugEntry AtDebugEntryInfoResourceNew(const char* name, uint32 max, uint32 free)
    {
    eAtSevLevel severity = cSevNormal;
    char stringValue[128];
    AtSnprintf(stringValue, sizeof(stringValue), "max: %u, free: %u", max, free);
    return AtDebugEntryNew(severity, name, stringValue);
    }

eAtSevLevel AtDebuggerEntrySeverityGet(AtDebugEntry self)
    {
    return self ? self->severity : cSevNormal;
    }

void AtDebuggerEntrySeveritySet(AtDebugEntry self, eAtSevLevel severity)
    {
    if (self)
        self->severity = severity;
    }

const char *AtDebuggerEntryNameGet(AtDebugEntry self)
    {
    return self ? self->name : NULL;
    }

void AtDebuggerEntryNameSet(AtDebugEntry self, const char *name)
    {
    if (self)
        NameSet(self, name);
    }

const char *AtDebuggerEntryValueGet(AtDebugEntry self)
    {
    return self ? self->value : NULL;
    }

void AtDebuggerEntryValueSet(AtDebugEntry self, const char *value)
    {
    if (self)
        ValueSet(self, value);
    }

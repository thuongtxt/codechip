/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : AtDefaultLongRegisterAccess.c
 *
 * Created Date: May 10, 2013
 *
 * Description : Default long register access
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtDefaultLongRegisterAccessInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtDefaultLongRegisterAccess)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtLongRegisterAccessMethods m_AtLongRegisterAccessOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super's implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, mThis(self)->holdReadRegisters);
    mMethodsGet(osal)->MemFree(osal, mThis(self)->holdWriteRegisters);
    mThis(self)->holdReadRegisters = NULL;
    mThis(self)->holdWriteRegisters = NULL;

    m_AtObjectMethods->Delete(self);
    }

static uint16 ReadByHandle(AtLongRegisterAccess self,
                           AtHal hal,
                           uint32 localAddress,
                           uint32 *dataBuffer,
                           uint16 bufferLen,
                           uint32 (*NoLockReadFunc)(AtHal self, uint32 address),
                           uint32 (*HoldRegReadFunc)(AtHal self, uint32 address))
    {
    uint16 i, numberOfReadDwords;
    uint32 offset = localAddress & cBit24;
    uint32 holdReg;

    /* Lock long read/write action */
    AtHalLock(hal);

    /* Read data */
    dataBuffer[0] = NoLockReadFunc(hal, localAddress);
    numberOfReadDwords = 1;
    for (i = 0; i < mThis(self)->numHoldReadRegisters; i++)
        {
        /* Buffer has no space left */
        if (bufferLen <= numberOfReadDwords)
            break;

        holdReg = (mThis(self)->holdReadRegisters[i] + AtLongRegisterHoldRegisterOffset(self)) | offset;
        dataBuffer[i + 1] = HoldRegReadFunc(hal, holdReg);
        numberOfReadDwords = (uint16)(numberOfReadDwords + 1);
        }

    if (bufferLen < mThis(self)->numHoldReadRegisters)
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning,
                    "Register buffer is not enough "
                    "space to hold all values, MSB values will be stripped\r\n");

    /* Unlock long read/write action */
    AtHalUnLock(hal);

    return numberOfReadDwords;
    }

static uint16 Read(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
    return ReadByHandle(self, hal, localAddress, dataBuffer, bufferLen, AtHalNoLockRead, AtHalNoLockRead);
    }

static uint16 Write(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen)
    {
    uint16 i, numberOfWrittenDwords;
    uint32 offset = localAddress & cBit24;
    uint32 holdReg;

    /* Lock long read/write action */
    AtHalLock(hal);

    /* Write data to hold register first */
    numberOfWrittenDwords = 1;
    for (i = 0; i < mThis(self)->numHoldWriteRegisters; i++)
        {
        /* Enough */
        if (bufferLen <= numberOfWrittenDwords)
            break;

        holdReg = (mThis(self)->holdWriteRegisters[i] + AtLongRegisterHoldRegisterOffset(self)) | offset;
        AtHalNoLockWrite(hal, holdReg, dataBuffer[i + 1]);
        numberOfWrittenDwords = (uint16)(numberOfWrittenDwords + 1);
        }

    /* Then write data to this register to activate hardware writing sequence */
    AtHalNoLockWrite(hal, localAddress, dataBuffer[0]);
    numberOfWrittenDwords = (uint16)(numberOfWrittenDwords + 1);

    if (bufferLen < mThis(self)->numHoldWriteRegisters)
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelWarning,
                    "Register buffer does not hold enough values "
                    "of long register. MSB values may be undetermined\r\n");

    /* Unlock long read/write action */
    AtHalUnLock(hal);

    return numberOfWrittenDwords;
    }

static uint32 HoldRegisterOffset(AtLongRegisterAccess self)
    {
    return mThis(self)->holdRegisterOffset;
    }

static eAtRet HoldRegisterOffsetSet(AtLongRegisterAccess self, uint32 offset)
    {
    mThis(self)->holdRegisterOffset = offset;
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtDefaultLongRegisterAccess object = (AtDefaultLongRegisterAccess)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt32Array(holdReadRegisters, object->numHoldReadRegisters);
    mEncodeUInt32Array(holdWriteRegisters, object->numHoldWriteRegisters);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDefaultLongRegisterAccess);
    }

static void OverrideAtLongRegisterAccess(AtLongRegisterAccess self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtLongRegisterAccessOverride, mMethodsGet(self), sizeof(m_AtLongRegisterAccessOverride));

        mMethodOverride(m_AtLongRegisterAccessOverride, Read);
        mMethodOverride(m_AtLongRegisterAccessOverride, Write);
        mMethodOverride(m_AtLongRegisterAccessOverride, HoldRegisterOffsetSet);
        mMethodOverride(m_AtLongRegisterAccessOverride, HoldRegisterOffset);
        }

    mMethodsSet(self, &m_AtLongRegisterAccessOverride);
    }

static void OverrideAtObject(AtLongRegisterAccess self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtLongRegisterAccess self)
    {
    OverrideAtLongRegisterAccess(self);
    OverrideAtObject(self);
    }

static void HoldRegistersSave(AtLongRegisterAccess self,
                              uint32 *holdReadRegisters,
                              uint16 numHoldReadRegisters,
                              uint32 *holdWriteRegisters,
                              uint16 numHoldWriteRegisters)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize;

    if ((numHoldReadRegisters == 0) || (numHoldWriteRegisters == 0))
        return;

    memorySize = sizeof(uint32) * numHoldReadRegisters;
    mThis(self)->holdReadRegisters = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    mMethodsGet(osal)->MemCpy(osal, mThis(self)->holdReadRegisters, holdReadRegisters, memorySize);
    mThis(self)->numHoldReadRegisters = numHoldReadRegisters;

    memorySize = sizeof(uint32) * numHoldWriteRegisters;
    mThis(self)->holdWriteRegisters = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    mMethodsGet(osal)->MemCpy(osal, mThis(self)->holdWriteRegisters, holdWriteRegisters, memorySize);
    mThis(self)->numHoldWriteRegisters = numHoldWriteRegisters;
    }

static AtLongRegisterAccess ObjectInit(AtLongRegisterAccess self,
                                       uint32 *holdReadRegisters,
                                       uint16 numHoldReadRegisters,
                                       uint32 *holdWriteRegisters,
                                       uint16 numHoldWriteRegisters)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtLongRegisterAccessObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    HoldRegistersSave(self, holdReadRegisters, numHoldReadRegisters, holdWriteRegisters, numHoldWriteRegisters);

    return self;
    }

AtLongRegisterAccess AtDefaultLongRegisterAccessNew(uint32 *holdReadRegisters,
                                                    uint16 numHoldReadRegisters,
                                                    uint32 *holdWriteRegisters,
                                                    uint16 numHoldWriteRegisters)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtLongRegisterAccess newAccess = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAccess == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newAccess, holdReadRegisters, numHoldReadRegisters, holdWriteRegisters, numHoldWriteRegisters);
    }

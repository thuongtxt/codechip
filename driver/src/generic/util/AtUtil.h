/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtUtil.h
 * 
 * Created Date: Jan 12, 2017
 *
 * Description : Util functions
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUTIL_H_
#define _ATUTIL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "AtChannel.h"
#include "AtEthVlanTag.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannel *AtUtilSortedChannelsCreate(AtList channelList);
void AtUtilLongFieldSet(uint32 *longReg, uint32 startDword, uint32 value,
                        uint32 lsbMask, uint32 lsbShift,
                        uint32 msbMask, uint32 msbShift);
uint32 AtUtilLongFieldGet(uint32 *longReg, uint32 startDword,
                          uint32 lsbMask, uint32 lsbShift,
                          uint32 msbMask, uint32 msbShift);
void AtUtilLongRegisterFieldSet(uint32 *longReg, uint32 numRegs, uint32 value, uint32 bitStart, uint32 bitEnd);
uint32 AtUtilLongRegisterFieldGet(uint32 *longReg, uint32 numRegs, uint32 bitStart, uint32 bitEnd);
char *AtObjectArrayToString(AtObject *objects, uint32 numObjects, char *buffer, uint32 bufferSize);
char *AtUtilBytes2String(const uint8 *bytes, uint32 numBytes, uint8 base, char *charBuffer, uint32 bufferSize);
char *AtUtilVlanTag2String(const tAtEthVlanTag *vlanTag, char *charBuffer, uint32 bufferSize);
char *AtUtilVlan2String(const tAtVlan *vlan, char *charBuffer, uint32 bufferSize);
uint8 AtUtilNumBitMaskGet(uint32 bitmask);

#ifdef __cplusplus
}
#endif
#endif /* _ATUTIL_H_ */


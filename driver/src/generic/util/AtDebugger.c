/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : AtDebugger.c
 *
 * Created Date: May 18, 2017
 *
 * Description : Generic debugger
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h" /* For OSAL */
#include "AtList.h"
#include "AtDebugger.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtDebugger)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtDebugger
    {
    tAtObject super;

    /* Private data */
    AtList entries;
    char buffer[256];

    /* Profile */
    uint32 numCriticalEntries;
    uint32 numInfoEntries;
    uint32 numWarningEntries;
    uint32 numOtherSeverityEntries;
    AtDevice device;
    }tAtDebugger;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtListDeleteWithObjectHandler(mThis(self)->entries, AtObjectDelete);
    mThis(self)->entries = NULL;
    m_AtObjectMethods->Delete(self);
    }

static AtIterator EntryIteratorCreate(AtDebugger self)
    {
    return AtListIteratorCreate(mThis(self)->entries);
    }

static void Profile(AtDebugger self, AtDebugEntry entry)
    {
    uint32 severity = AtDebuggerEntrySeverityGet(entry);

    switch (severity)
        {
        case cSevCritical:
            self->numCriticalEntries = self->numCriticalEntries + 1;
            break;

        case cSevWarning:
            self->numWarningEntries = self->numWarningEntries + 1;
            break;

        case cSevInfo:
            self->numInfoEntries = self->numInfoEntries + 1;
            break;

        default:
            self->numOtherSeverityEntries = self->numOtherSeverityEntries + 1;
            break;
        }
    }

static eAtRet EntryAdd(AtDebugger self, AtDebugEntry entry)
    {
    eAtRet ret = AtListObjectAdd(self->entries, (AtObject)entry);
    if (ret == cAtOk)
        Profile(self, entry);
    return ret;
    }

static char *CharBuffer(AtDebugger self, uint32 *bufferSize)
    {
    if (bufferSize)
        *bufferSize = sizeof(self->buffer);
    return self->buffer;
    }

static void OverrideAtObject(AtDebugger self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtDebugger self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtDebugger);
    }

static AtDebugger ObjectInit(AtDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->entries = AtListCreate(0);

    return self;
    }

AtDebugger AtDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDebugger newDebugger = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDebugger == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDebugger);
    }

AtIterator AtDebuggerEntryIteratorCreate(AtDebugger self)
    {
    if (self)
        return EntryIteratorCreate(self);
    return NULL;
    }

eAtRet AtDebuggerEntryAdd(AtDebugger self, AtDebugEntry entry)
    {
    if (self)
        return EntryAdd(self, entry);
    return cAtErrorNullPointer;
    }

char *AtDebuggerCharBuffer(AtDebugger self, uint32 *bufferSize)
    {
    if (self)
        return CharBuffer(self, bufferSize);
    return NULL;
    }

uint32 AtDebuggerNumCriticalEntries(AtDebugger self)
    {
    return self ? self->numCriticalEntries : 0;
    }

uint32 AtDebuggerNumInfoEntries(AtDebugger self)
    {
    return self ? self->numInfoEntries : 0;
    }

uint32 AtDebuggerNumWarningEntries(AtDebugger self)
    {
    return self ? self->numWarningEntries : 0;
    }

uint32 AtDebuggerNumOtherSeverityEntries(AtDebugger self)
    {
    return self ? self->numOtherSeverityEntries : 0;
    }

uint32 AtDebuggerNumEntries(AtDebugger self)
    {
    return self ? AtListLengthGet(self->entries) : 0;
    }

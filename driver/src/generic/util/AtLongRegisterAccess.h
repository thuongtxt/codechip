/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtLongRegisterAccess.h
 * 
 * Created Date: May 10, 2013
 *
 * Description : Long register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLONGREGISTER_H_
#define _ATLONGREGISTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtLongRegisterAccess * AtLongRegisterAccess;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concretes */
AtLongRegisterAccess AtDefaultLongRegisterAccessNew(uint32 *holdReadRegisters,
                                                    uint16 numHoldReadRegisters,
                                                    uint32 *holdWriteRegisters,
                                                    uint16 numHoldWriteRegisters);

/* APIs */
uint16 AtLongRegisterAccessRead(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen);
uint16 AtLongRegisterAccessWrite(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen);
uint32 AtLongRegisterHoldRegisterOffset(AtLongRegisterAccess self);
eAtRet AtLongRegisterHoldRegisterOffsetSet(AtLongRegisterAccess self, uint32 offset);

#ifdef __cplusplus
}
#endif
#endif /* _ATLONGREGISTER_H_ */


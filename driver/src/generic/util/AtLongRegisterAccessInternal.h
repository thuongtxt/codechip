/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtLongRegisterAccessInternal.h
 * 
 * Created Date: May 10, 2013
 *
 * Description : Long register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLONGREGISTERINTERNAL_H_
#define _ATLONGREGISTERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtLongRegisterAccess.h"
#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtLongRegisterAccessMethods
    {
    uint16 (*Read)(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen);
    uint16 (*Write)(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen);
    uint32 (*HoldRegisterOffset)(AtLongRegisterAccess self);
    eAtRet (*HoldRegisterOffsetSet)(AtLongRegisterAccess self, uint32 offset);
    }tAtLongRegisterAccessMethods;

typedef struct tAtLongRegisterAccess
    {
    tAtObject super;
    const tAtLongRegisterAccessMethods *methods;

    /* Private data */
    }tAtLongRegisterAccess;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLongRegisterAccess AtLongRegisterAccessObjectInit(AtLongRegisterAccess self);

#ifdef __cplusplus
}
#endif
#endif /* _ATLONGREGISTERINTERNAL_H_ */


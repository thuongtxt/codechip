/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : UTIL
 *
 * File        : AtLongRegisterAccess.c
 *
 * Created Date: May 10, 2013
 *
 * Description : Long register
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtLongRegisterAccessInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtLongRegisterAccessMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 Read(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
	AtUnused(bufferLen);
	AtUnused(dataBuffer);
	AtUnused(localAddress);
	AtUnused(hal);
	AtUnused(self);
    /* Sub class will do */
    return 0;
    }

static uint16 Write(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen)
    {
	AtUnused(bufferLen);
	AtUnused(dataBuffer);
	AtUnused(localAddress);
	AtUnused(hal);
	AtUnused(self);
    /* Sub class will do */
    return 0;
    }

static uint32 HoldRegisterOffset(AtLongRegisterAccess self)
    {
    AtUnused(self);
    /* Sub class will do */
    return 0;
    }

static eAtRet HoldRegisterOffsetSet(AtLongRegisterAccess self, uint32 offset)
    {
    AtUnused(self);
    AtUnused(offset);
    /* Sub class will do */
    return cAtOk;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "long_access";
    }

static void OverrideAtObject(AtLongRegisterAccess self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtLongRegisterAccess self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtLongRegisterAccess self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        mMethodOverride(m_methods, HoldRegisterOffsetSet);
        mMethodOverride(m_methods, HoldRegisterOffset);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLongRegisterAccess);
    }

AtLongRegisterAccess AtLongRegisterAccessObjectInit(AtLongRegisterAccess self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

uint16 AtLongRegisterAccessRead(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen)
    {
    if (self)
        return mMethodsGet(self)->Read(self, hal, localAddress, dataBuffer, bufferLen);
    return 0;
    }

uint16 AtLongRegisterAccessWrite(AtLongRegisterAccess self, AtHal hal, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen)
    {
    if (self)
        return mMethodsGet(self)->Write(self, hal, localAddress, dataBuffer, bufferLen);
    return 0;
    }

uint32 AtLongRegisterHoldRegisterOffset(AtLongRegisterAccess self)
    {
    if (self)
        return mMethodsGet(self)->HoldRegisterOffset(self);
    return 0;
    }

eAtRet AtLongRegisterHoldRegisterOffsetSet(AtLongRegisterAccess self, uint32 offset)
    {
    if (self)
        return mMethodsGet(self)->HoldRegisterOffsetSet(self, offset);
    return cAtErrorNullPointer;
    }

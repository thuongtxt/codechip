/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtHwTimer.h
 * 
 * Created Date: Apr 6, 2017
 *
 * Description : PRBS gating timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHWTIMER_H_
#define _ATHWTIMER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"
#include "AtTimer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHwTimer * AtHwTimer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtHwTimerRead(AtHwTimer self, uint32 address, eAtModule module);
void AtHwTimerWrite(AtHwTimer self, uint32 address, uint32 value, eAtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATHWTIMER_H_ */


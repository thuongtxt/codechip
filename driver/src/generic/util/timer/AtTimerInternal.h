/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtTimerInternal.h
 * 
 * Created Date: Apr 6, 2017
 *
 * Description : Abstract timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTIMERINTERNAL_H_
#define _ATTIMERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTimer.h"
#include "../../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtTimerMethods
    {
    eAtRet (*DurationSet)(AtTimer self, uint32 durationInMs);
    uint32 (*DurationGet)(AtTimer self);
    uint32 (*MaxDuration)(AtTimer self);
    uint32 (*ElapsedTimeGet)(AtTimer self);
    eAtRet (*Start)(AtTimer self);
    eAtRet (*Stop)(AtTimer self);
    eBool (*IsStarted)(AtTimer self);
    eBool  (*IsRunning)(AtTimer self);
    }tAtTimerMethods;

typedef struct tAtTimer
    {
    tAtObject super;
    const tAtTimerMethods *methods;
    }tAtTimer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTimer AtTimerObjectInit(AtTimer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATTIMERINTERNAL_H_ */


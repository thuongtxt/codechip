/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtHwTimerInternal.h
 * 
 * Created Date: Apr 7, 2017
 *
 * Description : HW timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHWTIMERINTERNAL_H_
#define _ATHWTIMERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "commacro.h"
#include "AtHwTimer.h"
#include "AtTimerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtHwTimerMethods
    {
    eAtRet (*HwDurationSet)(AtHwTimer self, uint32 hwDuration);
    uint32 (*HwDurationGet)(AtHwTimer self);
    uint32 (*HwDurationMax)(AtHwTimer self);
    uint32 (*HwElapsedTimeGet)(AtHwTimer self);
    eAtRet (*HwStart)(AtHwTimer self, eBool start);
    eBool (*HwIsRunning)(AtHwTimer self);
    eBool (*IsSimulated)(AtHwTimer self);
    uint32 (*Read)(AtHwTimer self, uint32 address, eAtModule module);
    void (*Write)(AtHwTimer self, uint32 address, uint32 value, eAtModule module);
    }tAtHwTimerMethods;

typedef struct tAtHwTimer
    {
    tAtTimer super;
    const tAtHwTimerMethods *methods;
    }tAtHwTimer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTimer AtHwTimerObjectInit(AtTimer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHWTIMERINTERNAL_H_ */


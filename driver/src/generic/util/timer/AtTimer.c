/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtTimer.c
 *
 * Created Date: Apr 6, 2017
 *
 * Description : Abstract timer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtTimerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtTimerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DurationSet(AtTimer self, uint32 durationInMs)
    {
    AtUnused(self);
    AtUnused(durationInMs);
    return cAtErrorNotImplemented;
    }

static uint32 DurationGet(AtTimer self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxDuration(AtTimer self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 ElapsedTimeGet(AtTimer self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eAtRet Start(AtTimer self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet Stop(AtTimer self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eBool IsStarted(AtTimer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsRunning(AtTimer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "timer";
    }

static void OverrideAtObject(AtTimer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtTimer self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtTimer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DurationSet);
        mMethodOverride(m_methods, DurationGet);
        mMethodOverride(m_methods, MaxDuration);
        mMethodOverride(m_methods, ElapsedTimeGet);
        mMethodOverride(m_methods, Start);
        mMethodOverride(m_methods, Stop);
        mMethodOverride(m_methods, IsStarted);
        mMethodOverride(m_methods, IsRunning);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtTimer);
    }

AtTimer AtTimerObjectInit(AtTimer self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtTimer
 * @{
 */

/**
 * Set duration
 *
 * @param self This timer
 * @param durationInMs Duration in milliseconds
 *
 * @return AT return code
 */
eAtRet AtTimerDurationSet(AtTimer self, uint32 durationInMs)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (durationInMs > AtTimerMaxDuration(self))
        return cAtErrorOutOfRangParm;

    return mMethodsGet(self)->DurationSet(self, durationInMs);
    }

/**
 * Get duration
 *
 * @param self This timer
 *
 * @return Duration in milliseconds
 */
uint32 AtTimerDurationGet(AtTimer self)
    {
    if (self)
        return mMethodsGet(self)->DurationGet(self);
    return 0;
    }

/**
 * Get max duration
 *
 * @param self This timer
 *
 * @return Max duration in milliseconds
 */
uint32 AtTimerMaxDuration(AtTimer self)
    {
    if (self)
        return mMethodsGet(self)->MaxDuration(self);
    return 0;
    }

/**
 * Get elapsed time
 *
 * @param self This timer
 *
 * @return Elapsed time in milliseconds
 */
uint32 AtTimerElapsedTimeGet(AtTimer self)
    {
    if (self)
        return mMethodsGet(self)->ElapsedTimeGet(self);
    return 0;
    }

/**
 * Start timer
 *
 * @param self This timer
 *
 * @return AT return code
 */
eAtRet AtTimerStart(AtTimer self)
    {
    if (self)
        return mMethodsGet(self)->Start(self);
    return cAtErrorObjectNotExist;
    }

/**
 * Stop timer
 *
 * @param self This timer
 *
 * @return AT return code
 */
eAtRet AtTimerStop(AtTimer self)
    {
    if (self)
        return mMethodsGet(self)->Stop(self);
    return cAtErrorObjectNotExist;
    }

/**
 * Check if timer has already started
 *
 * @param self This timer
 *
 * @retval cAtTrue if started
 * @retval cAtFalse if not started
 */
eBool AtTimerIsStarted(AtTimer self)
    {
    if (self)
        return mMethodsGet(self)->IsStarted(self);
    return cAtFalse;
    }

/**
 * Check if timer is still running
 *
 * @param self This timer
 *
 * @retval cAtTrue if running
 * @retval cAtFalse if stopped
 */
eBool AtTimerIsRunning(AtTimer self)
    {
    if (self)
        return mMethodsGet(self)->IsRunning(self);
    return cAtFalse;
    }

/**
 * @}
 */

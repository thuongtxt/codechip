/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtHwTimer.c
 *
 * Created Date: Apr 6, 2017
 *
 * Description : PRBS gating timer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHwTimerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtHwTimer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHwTimerMethods m_methods;

/* Override */
static tAtTimerMethods m_AtTimerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DurationHwUnit(void)
    {
    return 1000; /* 1s */
    }

static uint32 TimeSw2Hw(uint32 timeMs)
    {
    uint32 unit = DurationHwUnit();

    /* Must be in second unit */
    if (timeMs % unit)
        return cInvalidUint32;

    return timeMs / unit;
    }

static uint32 TimeHw2Sw(uint32 hwDuration)
    {
    return hwDuration * DurationHwUnit();
    }

static eAtRet HwDurationSet(AtHwTimer self, uint32 hwDuration)
    {
    AtUnused(self);
    AtUnused(hwDuration);
    return cAtErrorNotImplemented;
    }

static uint32 HwDurationGet(AtHwTimer self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 HwDurationMax(AtHwTimer self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 HwElapsedTimeGet(AtHwTimer self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet HwStart(AtHwTimer self, eBool start)
    {
    AtUnused(self);
    AtUnused(start);
    return cAtErrorNotImplemented;
    }

static eBool HwIsRunning(AtHwTimer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DurationSet(AtTimer self, uint32 durationInMs)
    {
    return mMethodsGet(mThis(self))->HwDurationSet(mThis(self), TimeSw2Hw(durationInMs));
    }

static uint32 DurationGet(AtTimer self)
    {
    return TimeHw2Sw(mMethodsGet(mThis(self))->HwDurationGet(mThis(self)));
    }

static uint32 MaxDuration(AtTimer self)
    {
    return TimeHw2Sw(mMethodsGet(mThis(self))->HwDurationMax(mThis(self)));
    }

static uint32 ElapsedTimeGet(AtTimer self)
    {
    return TimeHw2Sw(mMethodsGet(mThis(self))->HwElapsedTimeGet(mThis(self)));
    }

static eAtRet Start(AtTimer self)
    {
    return mMethodsGet(mThis(self))->HwStart(mThis(self), cAtTrue);
    }

static eAtRet Stop(AtTimer self)
    {
    return mMethodsGet(mThis(self))->HwStart(mThis(self), cAtFalse);
    }

static eBool IsSimulated(AtHwTimer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsRunning(AtTimer self)
    {
    eBool started = AtTimerIsStarted(self);

    if (started)
        {
        eBool hwIsRunning = mMethodsGet(mThis(self))->HwIsRunning(mThis(self));

        if (mMethodsGet(mThis(self))->IsSimulated(mThis(self)))
            return started;

        return hwIsRunning;
        }

    return cAtFalse;
    }

static uint32 Read(AtHwTimer self, uint32 address, eAtModule module)
    {
    AtUnused(self);
    AtUnused(address);
    AtUnused(module);
    return 0;
    }

static void Write(AtHwTimer self, uint32 address, uint32 value, eAtModule module)
    {
    AtUnused(self);
    AtUnused(address);
    AtUnused(value);
    AtUnused(module);
    }

static void OverrideTimer(AtTimer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtTimerOverride, mMethodsGet(self), sizeof(m_AtTimerOverride));

        mMethodOverride(m_AtTimerOverride, DurationSet);
        mMethodOverride(m_AtTimerOverride, DurationGet);
        mMethodOverride(m_AtTimerOverride, MaxDuration);
        mMethodOverride(m_AtTimerOverride, ElapsedTimeGet);
        mMethodOverride(m_AtTimerOverride, Start);
        mMethodOverride(m_AtTimerOverride, Stop);
        mMethodOverride(m_AtTimerOverride, IsRunning);
        }

    mMethodsSet(self, &m_AtTimerOverride);
    }

static void MethodsInit(AtHwTimer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwDurationSet);
        mMethodOverride(m_methods, HwDurationGet);
        mMethodOverride(m_methods, HwDurationMax);
        mMethodOverride(m_methods, HwElapsedTimeGet);
        mMethodOverride(m_methods, HwStart);
        mMethodOverride(m_methods, HwIsRunning);
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);
        mMethodOverride(m_methods, IsSimulated);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtTimer self)
    {
    OverrideTimer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtHwTimer);
    }

AtTimer AtHwTimerObjectInit(AtTimer self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtTimerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

uint32 AtHwTimerRead(AtHwTimer self, uint32 address, eAtModule module)
    {
    if (self)
        return mMethodsGet(self)->Read(self, address, module);
    return cInvalidUint32;
    }

void AtHwTimerWrite(AtHwTimer self, uint32 address, uint32 value, eAtModule module)
    {
    if (self)
        mMethodsGet(self)->Write(self, address, value, module);
    }

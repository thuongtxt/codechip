/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtDefaultLongRegisterAccessInternal.h
 * 
 * Created Date: May 10, 2013
 *
 * Description : Default long register access
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEFAULTLONGREGISTERACCESSINTERNAL_H_
#define _ATDEFAULTLONGREGISTERACCESSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtLongRegisterAccessInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDefaultLongRegisterAccess
    {
    tAtLongRegisterAccess super;

    /* Private data */
    uint16 numHoldReadRegisters;
    uint32 *holdReadRegisters;
    uint16 numHoldWriteRegisters;
    uint32 *holdWriteRegisters;
    uint32 holdRegisterOffset;
    }tAtDefaultLongRegisterAccess;

typedef struct tAtDefaultLongRegisterAccess * AtDefaultLongRegisterAccess;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATDEFAULTLONGREGISTERACCESSINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Util
 *
 * File        : AtUtil.c
 *
 * Created Date: Jan 12, 2017
 *
 * Description : Util function
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "AtUtil.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtStdCompareResult ChannelIdCompareFunc(const void *value1, const void *value2)
    {
    AtChannel channel1 = *((const AtChannel *)value1);
    AtChannel channel2 = *((const AtChannel *)value2);

    if (AtChannelIdGet(channel1) == AtChannelIdGet(channel2))
        return cAtStdCompareResultEqual;

    if (AtChannelIdGet(channel1) > AtChannelIdGet(channel2))
        return cAtStdCompareResultGreaterThan;

    return cAtStdCompareResultLessThan;
    }

AtChannel *AtUtilSortedChannelsCreate(AtList channelList)
    {
    uint32 channel_i;
    AtChannel *channelArray = NULL;
    uint32 memorySize = sizeof(AtChannel) * AtListLengthGet(channelList);

    if (memorySize == 0)
        return NULL;

    /* Have array of channels for sorting */
    channelArray = AtOsalMemAlloc(memorySize);
    for (channel_i = 0; channel_i < AtListLengthGet(channelList); channel_i++)
        channelArray[channel_i] = (AtChannel)AtListObjectGet(channelList, channel_i);

    AtStdSort(AtStdSharedStdGet(), channelArray, AtListLengthGet(channelList), sizeof(AtChannel), ChannelIdCompareFunc);
    return channelArray;
    }

void AtUtilLongFieldSet(uint32 *longReg, uint32 startDword, uint32 value,
                        uint32 lsbMask, uint32 lsbShift,
                        uint32 msbMask, uint32 msbShift)
    {
    uint32 msb, lsb;
    uint32 mask = lsbMask >> lsbShift;

    lsb = value & mask;
    mRegFieldSet(longReg[startDword], lsb, lsb);

    msb = (value & (~mask)) >> (32 - lsbShift);
    mRegFieldSet(longReg[startDword + 1], msb, msb);
    }

uint32 AtUtilLongFieldGet(uint32 *longReg, uint32 startDword,
                          uint32 lsbMask, uint32 lsbShift,
                          uint32 msbMask, uint32 msbShift)
    {
    uint32 msb, lsb;
    uint32 value = 0;

    lsb = mRegField(longReg[startDword], lsb);
    mFieldIns(&value, lsbMask >> lsbShift, 0, lsb);

    msb = mRegField(longReg[startDword + 1], msb);
    value = value | (msb << (32 - lsbShift));

    return value;
    }

static uint32 BitMaskBuild(uint32 startBit, uint32 endBit)
    {
    uint32 mask = 0;
    uint32 bit_i;
    for (bit_i = startBit; bit_i <= endBit; bit_i++)
        mask |= (1U << bit_i);
    return mask;
    }

void AtUtilLongRegisterFieldSet(uint32 *longReg, uint32 numRegs, uint32 value, uint32 bitStart, uint32 bitEnd)
    {
    uint32 wordStart = bitStart / 32;
    uint32 wordEnd = bitEnd / 32;

    if ((bitStart > bitEnd) || (wordEnd >= numRegs))
        return;

    /* Same word */
    if (wordStart == wordEnd)
        {
        uint32 fieldShift = bitStart % 32;
        uint32 fieldMask = BitMaskBuild(bitStart % 32, bitEnd % 32);
        mRegFieldSet(longReg[wordStart], field, value);
        }
    else
        {
        uint32 lsbShift = bitStart % 32;
        uint32 lsbMask = BitMaskBuild(lsbShift, 31);
        uint32 msbShift = 0;
        uint32 msbMask = BitMaskBuild(0, bitEnd % 32);
        AtUtilLongFieldSet(longReg, wordStart, value, lsbMask, lsbShift, msbMask, msbShift);
        }
    }

uint32 AtUtilLongRegisterFieldGet(uint32 *longReg, uint32 numRegs, uint32 bitStart, uint32 bitEnd)
    {
    uint32 wordStart = bitStart / 32;
    uint32 wordEnd = bitEnd / 32;

    if ((bitStart > bitEnd) || (wordEnd >= numRegs))
        return 0xdeadcafe;

    /* Same word */
    if (wordStart == wordEnd)
        {
        uint32 fieldShift = bitStart % 32;
        uint32 fieldMask = BitMaskBuild(bitStart % 32, bitEnd % 32);
        return mRegField(longReg[wordStart], field);
        }
    else
        {
        uint32 lsbShift = bitStart % 32;
        uint32 lsbMask = BitMaskBuild(lsbShift, 31);
        uint32 msbShift = 0;
        uint32 msbMask = BitMaskBuild(0, bitEnd % 32);
        return AtUtilLongFieldGet(longReg, wordStart, lsbMask, lsbShift, msbMask, msbShift);
        }
    }

char *AtObjectArrayToString(AtObject *objects, uint32 numObjects, char *buffer, uint32 bufferSize)
    {
    uint32 object_i;
    uint32 remained = bufferSize;

    /* Make an empty string */
    buffer[0] = '\0';

    for (object_i = 0; object_i < numObjects; object_i++)
        {
        const char *objectString = AtObjectToString(objects[object_i]);

        if (object_i == 0)
            {
            AtStrcat(buffer, objectString);
            remained = remained - AtStrlen(objectString);
            }

        else
            {
            AtStrcat(buffer, ",");
            remained = remained - 1;
            AtStrcat(buffer, objectString);
            remained = remained - AtStrlen(objectString);
            }
        }

    return buffer;
    }

char *AtUtilBytes2String(const uint8 *bytes, uint32 numBytes, uint8 base, char *charBuffer, uint32 bufferSize)
    {
    uint32 byte_i;

    if ((bytes == NULL) || (numBytes == 0))
        {
        AtSnprintf(charBuffer, bufferSize, "%s", "none");
        return charBuffer;
        }

    AtOsalMemInit(charBuffer, 0, bufferSize);

    if (numBytes > bufferSize)
        numBytes = bufferSize;

    for (byte_i = 0; byte_i < numBytes; byte_i++)
        {
        char number[16];
        AtOsalMemInit(number, 0, sizeof(number));

        if (base == 10) AtSprintf(number, "%d", bytes[byte_i]);
        if (base == 16) AtSprintf(number, "%02x", bytes[byte_i]);

        AtStrcat(charBuffer, ".");
        AtStrcat(charBuffer, number);
        }

    return &charBuffer[1];
    }

char *AtUtilVlanTag2String(const tAtEthVlanTag *vlanTag, char *charBuffer, uint32 bufferSize)
    {
    if (vlanTag)
        AtSnprintf(charBuffer, bufferSize, "%u.%u.%u", vlanTag->priority, vlanTag->cfi, vlanTag->vlanId);
    else
        AtSnprintf(charBuffer, bufferSize, "%s", "none");

    return charBuffer;
    }

char *AtUtilVlan2String(const tAtVlan *vlan, char *charBuffer, uint32 bufferSize)
    {
    if (vlan)
        AtSnprintf(charBuffer, bufferSize, "0x%04x.%u.%u.%u", vlan->tpid, vlan->priority, vlan->cfi, vlan->vlanId);
    else
        AtSnprintf(charBuffer, bufferSize, "%s", "none");

    return charBuffer;
    }

uint8 AtUtilNumBitMaskGet(uint32 bitmask)
    {
    uint8 i;
    uint8 numBits = 0;

    for (i = 0; i < 32; i++)
        {
        if (bitmask & (cBit0 << i))
            numBits = (uint8)(numBits + 1);
        }

    return numBits;
    }


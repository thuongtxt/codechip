/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IMA
 * 
 * File        : AtModuleImaInternal.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : IMA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEIMAINTERNAL_H_
#define _ATMODULEIMAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleIma.h"
#include "AtImaGroupInternal.h"
#include "AtImaLinkInternal.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleImaMethods
    {
    /* IMA group management */
	uint32 (*MaxGroupsGet)(AtModuleIma self);
	uint32 (*FreeGroupGet)(AtModuleIma self);
    AtImaGroup (*ImaGroupCreate)(AtModuleIma self, uint32 groupId);
    AtImaGroup (*ImaGroupGet)(AtModuleIma self, uint32 groupId);
    eAtImaRet  (*ImaGroupDelete)(AtModuleIma self, uint32 groupId);

    /* IMA Link management */
    uint32 (*MaxLinksGet)(AtModuleIma self);
	uint32 (*FreeLinkGet)(AtModuleIma self);
    AtImaLink (*ImaLinkCreate)(AtModuleIma self, uint32 linkId);
    AtImaLink (*ImaLinkGet)(AtModuleIma self, uint32 linkId);
    eAtImaRet (*ImaLinkDelete)(AtModuleIma self, uint32 linkId);
    }tAtModuleImaMethods;

/**
 * @brief IMA Module class
 * */
typedef struct tAtModuleIma
    {
    /* Inherit from tAtModule */
    tAtModule super;

    /* Methods */
    const tAtModuleImaMethods *methods;
    }tAtModuleIma;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleIma AtModuleImaObjectInit(AtModuleIma self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEIMAINTERNAL_H_ */


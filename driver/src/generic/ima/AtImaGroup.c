/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IMA
 *
 * File        : AtImaGroup.c
 *
 * Created Date: Sep 27, 2012
 *
 * Author      : thuynt
 *
 * Description : IMA group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleImaInternal.h"
#include "AtImaGroupInternal.h"
#include "AtImaGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mGroupIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtImaGroupMethods  m_methods;

static uint8 m_methodsInit = 0;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Set Group properties */
/* IMA group version */
static eAtImaRet VersionSet(AtImaGroup self, eAtImaVersion version)
    {
	AtUnused(version);
	AtUnused(self);
    return cAtImaRetError;
    }
static eAtImaVersion VersionGet(AtImaGroup self)
    {
	AtUnused(self);
    return cAtImaRetError;
    }

/* IMA ID */
static eAtImaRet ImaIdSet(AtImaGroup self, uint32 imaId)
    {
	AtUnused(imaId);
	AtUnused(self);
    return cAtImaRetError;
    }
static uint32 ImaIdGet(AtImaGroup self)
    {
	AtUnused(self);
    return 0;
    }

/* Symmetric working mode */
static eAtImaRet SymmetryModeSet(AtImaGroup self, eAtImaSymmetryMode symmetryMode)
    {
	AtUnused(symmetryMode);
	AtUnused(self);
    return cAtImaRetError;
    }
static eAtImaSymmetryMode SymmetryModeGet(AtImaGroup self)
    {
	AtUnused(self);
    return cAtImaASymmetryModeInvalid;
    }

/* TX Clock source */
static eAtImaRet TxClockModeSet(AtImaGroup self, eAtImaTxClockMode txClockMd)
    {
	AtUnused(txClockMd);
	AtUnused(self);
    return cAtImaRetError;
    }
static eAtImaTxClockMode TxClockModeGet(AtImaGroup self)
    {
	AtUnused(self);
    return cAtImaTxClockModeInvalid;
    }

/* TRL ID */
static eAtImaRet TrlIdSet(AtImaGroup self, uint8 trlId)
    {
	AtUnused(trlId);
	AtUnused(self);
    return cAtImaRetError;
    }
static uint8 TrlIdGet(AtImaGroup self)
    {
	AtUnused(self);
    return 0;
    }

/* Minimum TX link in "Active" state required for group change to "Operation" */
static eAtImaRet TxMinNumLinkSet(AtImaGroup self, uint8 txMinNumLink)
    {
	AtUnused(txMinNumLink);
	AtUnused(self);
    return cAtImaRetError;
    }
static uint8 TxMinNumLinkGet(AtImaGroup self)
    {
	AtUnused(self);
    return 0;
    }

/* Minimum RX link in "Active" state required for group change to "Operation" */
static eAtImaRet RxMinNumLinkSet(AtImaGroup self, uint8 rxMinNumLink)
    {
	AtUnused(rxMinNumLink);
	AtUnused(self);
    return cAtImaRetError;
    }
static uint8 RxMinNumLinkGet(AtImaGroup self)
    {
	AtUnused(self);
    return 0;
    }

/* Frame length */
static eAtImaRet FrameLenSet(AtImaGroup self, eAtImaFrameLength frameLen)
    {
	AtUnused(frameLen);
	AtUnused(self);
    return cAtImaRetError;
    }
static eAtImaFrameLength FrameLenGet(AtImaGroup self)
    {
	AtUnused(self);
    return cAtImaFrameLengthInvalid;
    }

/* Maximum differential delay */
static eAtImaRet MaxDifferentialLinkDelaySet(AtImaGroup self, uint32 maxDifferentialLinkDelay)
    {
	AtUnused(maxDifferentialLinkDelay);
	AtUnused(self);
    return cAtImaRetError;
    }
static uint32 MaxDifferentialLinkDelayGet(AtImaGroup self)
    {
	AtUnused(self);
    return 0;
    }

/* Group management */
static eAtImaRet Inhibit(AtImaGroup self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtImaRetError;
    }
static eBool IsInhibited(AtImaGroup self)
    {
	AtUnused(self);
    return cAtFalse;
    }

/* Restart group */
static eAtImaRet Restart(AtImaGroup self)
    {
	AtUnused(self);
    return cAtImaRetError;
    }

/* Link management */
/* Add link to group */
static eAtImaRet LinkAdd(AtImaGroup self, AtImaLink txLink, AtImaLink rxLink)
    {
	AtUnused(rxLink);
	AtUnused(txLink);
	AtUnused(self);
    return cAtImaRetError;
    }

/* Get TX & RX links in group */
static uint8 TxLinksGet(AtImaGroup self, AtImaLink *links)
    {
	AtUnused(links);
	AtUnused(self);
    return 0;
    }
static uint8 RxLinksGet(AtImaGroup self, AtImaLink *links)
    {
	AtUnused(links);
	AtUnused(self);
    return 0;
    }

/* Status */
static eAtImaRet StatusGet(AtImaGroup self, tAtImaGroupStatus *status)
    {
	AtUnused(status);
	AtUnused(self);
    return cAtImaRetError;
    }

/* Test pattern for link in group */
static eAtImaRet TestEnable(AtImaGroup self, AtImaLink link, eBool enable, uint8 pattern)
    {
	AtUnused(pattern);
	AtUnused(enable);
	AtUnused(link);
	AtUnused(self);
    return cAtImaRetError;
    }
static eBool TestIsEnabled(AtImaGroup self)
    {
	AtUnused(self);
    return cAtFalse;
    }
static eBool TestResultGet(AtImaGroup self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtImaGroup self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, VersionSet);
        mMethodOverride(m_methods, VersionGet);
        mMethodOverride(m_methods, ImaIdSet);
        mMethodOverride(m_methods, ImaIdGet);
        mMethodOverride(m_methods, SymmetryModeSet);
        mMethodOverride(m_methods, SymmetryModeGet);
        mMethodOverride(m_methods, TxClockModeSet);
        mMethodOverride(m_methods, TxClockModeGet);
        mMethodOverride(m_methods, TrlIdSet);
        mMethodOverride(m_methods, TrlIdGet);
        mMethodOverride(m_methods, TxMinNumLinkSet);
        mMethodOverride(m_methods, TxMinNumLinkGet);
        mMethodOverride(m_methods, RxMinNumLinkSet);
        mMethodOverride(m_methods, RxMinNumLinkGet);
        mMethodOverride(m_methods, FrameLenSet);
        mMethodOverride(m_methods, FrameLenGet);
        mMethodOverride(m_methods, MaxDifferentialLinkDelaySet);
        mMethodOverride(m_methods, MaxDifferentialLinkDelayGet);
        mMethodOverride(m_methods, Inhibit);
        mMethodOverride(m_methods, IsInhibited);
        mMethodOverride(m_methods, Restart);
        mMethodOverride(m_methods, LinkAdd);
        mMethodOverride(m_methods, TxLinksGet);
        mMethodOverride(m_methods, RxLinksGet);
        mMethodOverride(m_methods, StatusGet);
        mMethodOverride(m_methods, TestEnable);
        mMethodOverride(m_methods, TestIsEnabled);
        mMethodOverride(m_methods, TestResultGet);
        }
    mMethodsSet(self, &m_methods);
    }

/* Class constructor */
AtImaGroup AtImaGroupObjectInit(AtImaGroup self, uint32 groupId, AtModuleIma module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(AtImaGroup));

    if (AtChannelObjectInit((AtChannel)self, groupId, (AtModule)module) == NULL)
        return NULL;

    MethodsInit(self);

    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtImaGroup
 * @{
 */

/**
 * Set IMA group version
 *
 * @param self This module
 * @param version IMA version
 * @return IMA return code
 */
eAtImaRet AtImaGroupVersionSet(AtImaGroup self, eAtImaVersion version)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->VersionSet(self, version);

    return cAtImaRetError;
    }
/**
 * Get IMA group version
 *
 * @param self This module
 * @return IMA version. Refer
 * 						@ref eAtImaVersion "IMA version"
 */
eAtImaVersion AtImaGroupVersionGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->VersionGet(self);

    return cAtImaVersionInvalid;
    }
/**
 * Set IMA ID for group
 *
 * @param self This module
 * @param imaId IMA ID
 * @return IMA return code
 */
eAtImaRet AtImaGroupImaIdSet(AtImaGroup self, uint32 imaId)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->ImaIdSet(self, imaId);

    return cAtImaRetError;
    }
/**
 * Get IMA ID for group
 *
 * @param self This module
 * @return IMA ID
 */
uint32 AtImaGroupImaIdGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->ImaIdGet(self);

    return 0;
    }
/**
 * Set Symmetric mode for group
 *
 * @param self This module
 * @param symmetryMode Symmetric mode
 * @return IMA return code
 */
eAtImaRet AtImaGroupSymmetryModeSet(AtImaGroup self, eAtImaSymmetryMode symmetryMode)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->SymmetryModeSet(self, symmetryMode);

    return cAtImaRetError;
    }
/**
 * Get group symmetric mode
 *
 * @param self This module
 * @return Symmetric mode. Refer
 * 							@ref eAtImaSymmetryMode "Group symmetric mode"
 */
eAtImaSymmetryMode AtImaGroupSymmetryModeGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->SymmetryModeGet(self);

    return cAtImaASymmetryModeInvalid;
    }
/**
 * Set TX clock mode for group
 *
 * @param self This module
 * @param txClockMd Clock mode
 * @return IMA return code
 */
eAtImaRet AtImaGroupTxClockModeSet(AtImaGroup self, eAtImaTxClockMode txClockMd)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TxClockModeSet(self, txClockMd);

    return cAtImaRetError;
    }
/**
 * Get TX clock mode for group
 *
 * @param self This module
 * @return IMA version. Refer
 * 						@ref eAtImaTxClockMode "Clock mode"
 */
eAtImaTxClockMode AtImaGroupTxClockModeGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TxClockModeGet(self);

    return cAtImaTxClockModeInvalid;
    }
/**
 * Set TRL for group
 *
 * @param self This module
 * @param trlId TRL ID
 * @return IMA return code
 */
eAtImaRet AtImaGroupTrlIdSet(AtImaGroup self, uint8 trlId)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TrlIdSet(self, trlId);

    return cAtImaRetError;
    }
/**
 * Get TRL for group
 *
 * @param self This module
 * @return TRL ID for group
 */
uint8 AtImaGroupTrlIdGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TrlIdGet(self);

    return 0;
    }
/**
 * Set minimum number of TX active link
 *
 * @param self This module
 * @param txMinNumLink Minimum required active link
 * @return IMA return code
 */
eAtImaRet AtImaGroupTxMinNumLinkSet(AtImaGroup self, uint8 txMinNumLink)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TxMinNumLinkSet(self, txMinNumLink);

    return cAtImaRetError;
    }
/**
 * Get minimum number of TX active link
 *
 * @param self This module
 * @return Minimum TX active link
 */
uint8 AtImaGroupTxMinNumLinkGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TxMinNumLinkGet(self);

    return 0;
    }
/**
 * Set minimum number of RX active link
 *
 * @param self This module
 * @param rxMinNumLink Minimum required active link
 * @return IMA return code
 */
eAtImaRet AtImaGroupRxMinNumLinkSet(AtImaGroup self, uint8 rxMinNumLink)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->RxMinNumLinkSet(self, rxMinNumLink);

    return cAtImaRetError;
    }
/**
 * Get minimum number of RX active link
 *
 * @param self This module
 * @return Minimum RX active link
 */
uint8 AtImaGroupRxMinNumLinkGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->RxMinNumLinkGet(self);

    return 0;
    }
/**
 * Set IMA frame length for group
 *
 * @param self This module
 * @param frameLen Frame length
 * @return IMA return code
 */
eAtImaRet AtImaGroupFrameLenSet(AtImaGroup self, eAtImaFrameLength frameLen)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->FrameLenSet(self, frameLen);

    return cAtImaRetError;
    }
/**
 * Get IMA frame length for group
 *
 * @param self This module
 * @return Frame length. Refer
 * 						@ref eAtImaFrameLength "IMA frame length"
 */
eAtImaFrameLength AtImaGroupFrameLenGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->FrameLenGet(self);

    return cAtImaFrameLengthInvalid;
    }
/**
 * Set maximum differential links delay in group
 *
 * @param self This module
 * @param maxDifferentialLinkDelay Maximum delay
 * @return IMA return code
 */
eAtImaRet AtImaGroupMaxDifferentialLinkDelaySet(AtImaGroup self, uint32 maxDifferentialLinkDelay)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->MaxDifferentialLinkDelaySet(self, maxDifferentialLinkDelay);

    return cAtImaRetError;
    }
/**
 * Get maximum differential links delay in group
 *
 * @param self This module
 * @return Maximum delay
 */
uint32 AtImaGroupMaxDifferentialLinkDelayGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->MaxDifferentialLinkDelayGet(self);

    return 0;
    }
/**
 * Inhibit an IMA group
 *
 * @param self This module
 * @param enable Enable/disable inhibition
 * @return IMA return code
 */
eAtImaRet AtImaGroupInhibit(AtImaGroup self, eBool enable)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->Inhibit(self, enable);

    return cAtImaRetError;
    }
/**
 * Get inhibition state of IMA group
 *
 * @param self This module
 * @return Inhibition status(enable/disable)
 */
eBool AtImaGroupIsInhibited(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->IsInhibited(self);

    return cAtFalse;
    }
/**
 * Restart an IMA group
 *
 * @param self This module
 * @return IMA return code
 */
eAtImaRet AtImaGroupRestart(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->Restart(self);

    return cAtImaRetError;
    }
/**
 * Add links to group
 *
 * @param self This module
 * @param txLink TX link
 * @param rxLink RX link
 *
 * @return IMA return code
 */
eAtImaRet AtImaGroupLinkAdd(AtImaGroup self, AtImaLink txLink, AtImaLink rxLink)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->LinkAdd(self, txLink, rxLink);

    return cAtImaRetError;
    }

/**
 * Get TX links configuration
 *
 * @param self This module
 * @param links Buffer to store all links
 *
 * @return Number of TX links in group and configuration of each links
 */
uint8 AtImaGroupTxLinksGet(AtImaGroup self, AtImaLink *links)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TxLinksGet(self, links);

    return 0;
    }

/**
 * Get RX links configuration
 *
 * @param self This module
 * @param links Buffer to store all links.
 *
 * @return Number of TX links in group and configuration of each links
 */
uint8 AtImaGroupRxLinksGet(AtImaGroup self, AtImaLink *links)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->RxLinksGet(self, links);

    return 0;
    }

/**
 * Get Group status
 *
 * @param self This module
 * @param status @ref tAtImaGroupStatus "IMA Group Status"
 *
 * @return IMA return code
 */
eAtImaRet AtImaGroupStatusGet(AtImaGroup self, tAtImaGroupStatus *status)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->StatusGet(self, status);

    return cAtImaRetError;
    }

/**
 * Processing test pattern for link in group
 *
 * @param self This module
 * @param link IMA link to start test pattern
 * @param enable Enable/disable test process
 * @param pattern Test pattern in case enable this process
 *
 * @return IMA return code
 */
eAtImaRet AtImaGroupTestEnable(AtImaGroup self, AtImaLink link, eBool enable, uint8 pattern)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TestEnable(self, link, enable, pattern);

    return cAtImaRetError;
    }
/**
 * Get test pattern status
 *
 * @param self This module
 *
 * @return cAtTrue if enabled, cAtFalse if disabled
 */
eBool AtImaGroupTestIsEnabled(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TestIsEnabled(self);

    return cAtFalse;
    }
/**
 * Get test pattern result
 *
 * @param self This module
 * @return Test result
 */
eBool AtImaGroupTestResultGet(AtImaGroup self)
    {
    if (mGroupIsValid(self))
        return mMethodsGet(self)->TestResultGet(self);

    return cAtFalse;
    }

/**
 * @}
 */

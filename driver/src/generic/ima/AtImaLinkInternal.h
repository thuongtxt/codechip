/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IMA
 * 
 * File        : AtImaLinkInternal.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : IMA Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIMALINKINTERNAL_H_
#define _ATIMALINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleIma.h"
#include "AtImaLink.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtImaLinkMethods
    {
    /* Physical bind */
    eAtImaRet (*TxBind)(AtImaLink self, AtAtmTc atmTc);
    eAtImaRet (*RxBind)(AtImaLink self, AtAtmTc atmTc);

    /* Inhibit */
    eAtImaRet (*Inhibit)(AtImaLink self, eBool enable);
    eBool (*IsInhibited)(AtImaLink self);

    /* Status */
    eAtImaRet (*StatusGet)(AtImaLink self, tAtImaLinkStatus *status);
    }tAtImaLinkMethods;

typedef struct tAtImaLink
    {
    /* Supper class */
    tAtChannel          super;

    /* Implements */
    const tAtImaLinkMethods   *methods;
    }tAtImaLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtImaLink AtImaLinkObjectInit(AtImaLink self, uint16 linkId, AtModuleIma module);

#ifdef __cplusplus
}
#endif
#endif /* _ATIMALINKINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IMA
 *
 * File        : AtModuleIma.c
 *
 * Created Date: Sep 27, 2012
 *
 * Description : IMA module
 *
 * Notes       : 
 *----------------------------------t------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleImaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* To store implementation of this class (require) */
static tAtModuleImaMethods m_methods;

/* To override super methods (require if need to override some Methods) */
static tAtObjectMethods m_AtObjectOverride;

/* To cache method of AtObject */
static const tAtObjectMethods *m_AtObjectMethods;

/* To prevent initializing implementation structures more than one times */
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxGroupsGet(AtModuleIma self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 FreeGroupGet(AtModuleIma self)
    {
	AtUnused(self);
    return 0;
    }

static AtImaGroup ImaGroupCreate(AtModuleIma self, uint32 groupId)
    {
	AtUnused(groupId);
	AtUnused(self);
    return NULL;
    }

static AtImaGroup ImaGroupGet(AtModuleIma self, uint32 groupId)
    {
	AtUnused(groupId);
	AtUnused(self);
    return NULL;
    }

static eAtImaRet ImaGroupDelete(AtModuleIma self, uint32 groupId)
    {
	AtUnused(groupId);
	AtUnused(self);
    return cAtImaRetError;
    }

static uint32 MaxLinksGet(AtModuleIma self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 FreeLinkGet(AtModuleIma self)
    {
	AtUnused(self);
    return 0;
    }

static AtImaLink ImaLinkCreate(AtModuleIma self, uint32 linkId)
    {
	AtUnused(linkId);
	AtUnused(self);
    return NULL;
    }

static AtImaLink ImaLinkGet(AtModuleIma self, uint32 linkId)
    {
	AtUnused(linkId);
	AtUnused(self);
    return NULL;
    }

static eAtImaRet ImaLinkDelete(AtModuleIma self, uint32 linkId)
    {
	AtUnused(linkId);
	AtUnused(self);
    return cAtImaRetError;
    }

static void MethodsInit(AtModuleIma self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
    	mMethodOverride(m_methods, MaxGroupsGet);
		mMethodOverride(m_methods, FreeGroupGet);
        mMethodOverride(m_methods, ImaGroupCreate);
        mMethodOverride(m_methods, ImaGroupGet);
        mMethodOverride(m_methods, ImaGroupDelete);

        mMethodOverride(m_methods, MaxLinksGet);
		mMethodOverride(m_methods, FreeLinkGet);
        mMethodOverride(m_methods, ImaLinkCreate);
        mMethodOverride(m_methods, ImaLinkGet);
        mMethodOverride(m_methods, ImaLinkDelete);
        }
    mMethodsSet(self, &m_methods);
    }

/* Delete module */
static void Delete(AtObject self)
    {
    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static const char *ToString(AtObject self)
    {
    AtModuleIma moduleIma = (AtModuleIma)self;
    AtDevice dev = (AtDevice)AtModuleDeviceGet((AtModule)moduleIma);
    static char string[128];

    AtSprintf(string, "%sLink: %u, Group: %u", AtDeviceIdToString(dev), AtModuleImaMaxLinksGet(moduleIma), AtModuleImaMaxGroupsGet(moduleIma));
    return string;
    }

static void OverrideAtObject(AtModuleIma self)
    {
	AtOsal osal = AtSharedDriverOsalGet();
	AtObject object = (AtObject)self;

	if (!m_methodsInit)
		{
		/* Save reference to super methods */
		m_AtObjectMethods = mMethodsGet(object);

		/* Copy to reuse methods of super class. But override some Methods */
		mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
		m_AtObjectOverride.Delete = Delete;
		m_AtObjectOverride.ToString = ToString;
		}

	/* Change behavior of super class level 2 */
	mMethodsSet(object, &m_AtObjectOverride);
    }


static void Override(AtModuleIma self)
    {
    OverrideAtObject(self);
    }

AtModuleIma AtModuleImaObjectInit(AtModuleIma self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(AtModuleIma));

    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModuleIma, device) == NULL)
        return NULL;

    /* Override Delete method of AtObject class */
    Override(self);

    /* Init implemements */
    MethodsInit(self);

    /* Just init methods once */
    m_methodsInit = 1;

    return self;
    }

static eBool ModuleIsValid(AtModuleIma self)
    {
    return self ? cAtTrue : cAtFalse;
    }

/**
 * @addtogroup AtModuleIma
 * @{
 */

/**
 * Get maximum number of IMA groups that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of IMA groups that this module can support
 */
uint32 AtModuleImaMaxGroupsGet(AtModuleIma self)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->MaxGroupsGet(self);

    return 0;
    }

/**
 * Get a free IMA group
 *
 * @param self This module
 *
 * @return A free IMA group
 */
uint32 AtModuleImaFreeGroupGet(AtModuleIma self)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->FreeGroupGet(self);

    return 0;
    }

/**
 * Create a IMA group from Group identifier
 *
 * @param self This module
 * @param groupId Group identifier
 * @return Instance of IMA group
 */
AtImaGroup AtModuleImaGroupCreate(AtModuleIma self, uint32 groupId)
	{
    if (ModuleIsValid(self))
        return mMethodsGet(self)->ImaGroupCreate(self, groupId);

    return NULL;
	}

/**
 * Get IMA group object by ID
 *
 * @param self This module
 * @param groupId Group identifier
 *
 * @return A IMA group object on success or NULL on failure
 */
AtImaGroup AtModuleImaGroupGet(AtModuleIma self, uint32 groupId)
	{
    if (ModuleIsValid(self))
        return mMethodsGet(self)->ImaGroupGet(self, groupId);

    return NULL;
	}

/**
 * Delete IMA group
 *
 * @param self This module
 * @param groupId Group identifier
 *
 * @return AT return code
 */
eAtImaRet AtModuleImaGroupDelete(AtModuleIma self, uint32 groupId)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->ImaGroupDelete(self, groupId);

    return cAtImaRetError;
    }

/**
 * Get maximum number of IMA links that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of IMA links that this module can support
 */
uint32 AtModuleImaMaxLinksGet(AtModuleIma self)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->MaxLinksGet(self);

    return 0;
    }

/**
 * Get a free IMA link
 *
 * @param self This module
 *
 * @return A free IMA link
 */
uint32 AtModuleImaFreeLinkGet(AtModuleIma self)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->FreeLinkGet(self);

    return 0;
    }

/**
 * Create a IMA link from Group identifier
 *
 * @param self This module
 * @param linkId Link identifier
 * @return Instance of IMA link
 */
AtImaLink AtModuleImaLinkCreate(AtModuleIma self, uint32 linkId)
	{
    if (ModuleIsValid(self))
        return mMethodsGet(self)->ImaLinkCreate(self, linkId);

    return NULL;
	}

/**
 * Get IMA link object by ID
 *
 * @param self This module
 * @param linkId Link identifier
 *
 * @return A IMA link object on success or NULL on failure
 */
AtImaLink AtModuleImaLinkGet(AtModuleIma self, uint32 linkId)
	{
    if (ModuleIsValid(self))
        return mMethodsGet(self)->ImaLinkGet(self, linkId);

    return NULL;
	}

/**
 * Delete IMA link
 *
 * @param self This module
 * @param linkId Link identifier
 *
 * @return AT return code
 */
eAtImaRet AtModuleImaLinkDelete(AtModuleIma self, uint32 linkId)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->ImaLinkDelete(self, linkId);

    return cAtImaRetError;
    }

/**
 * @}
 */

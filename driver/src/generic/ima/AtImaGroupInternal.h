/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IMA
 * 
 * File        : AtImaGroupInternal.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : IMA group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIMAGROUPINTERNAL_H_
#define _ATIMAGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleIma.h"
#include "AtImaGroup.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Implementations */
typedef struct tAtImaGroupMethods
    {
    /* Set Group properties */
    eAtImaRet (*VersionSet)(AtImaGroup self, eAtImaVersion version);
    eAtImaVersion (*VersionGet)(AtImaGroup self);

    eAtImaRet (*ImaIdSet)(AtImaGroup self, uint32 imaId);
    uint32 (*ImaIdGet)(AtImaGroup self);

    eAtImaRet (*SymmetryModeSet)(AtImaGroup self, eAtImaSymmetryMode symmetryMode);
    eAtImaSymmetryMode (*SymmetryModeGet)(AtImaGroup self);

    eAtImaRet (*TxClockModeSet)(AtImaGroup self, eAtImaTxClockMode txClockMd);
    eAtImaTxClockMode (*TxClockModeGet)(AtImaGroup self);

    eAtImaRet (*TrlIdSet)(AtImaGroup self, uint8 trlId);
    uint8 (*TrlIdGet)(AtImaGroup self);

    eAtImaRet (*TxMinNumLinkSet)(AtImaGroup self, uint8 txMinNumLink);
    uint8 (*TxMinNumLinkGet)(AtImaGroup self);

    eAtImaRet (*RxMinNumLinkSet)(AtImaGroup self, uint8 rxMinNumLink);
    uint8 (*RxMinNumLinkGet)(AtImaGroup self);

    eAtImaRet (*FrameLenSet)(AtImaGroup self, eAtImaFrameLength frameLen);
    eAtImaFrameLength (*FrameLenGet)(AtImaGroup self);

    eAtImaRet (*MaxDifferentialLinkDelaySet)(AtImaGroup self, uint32 maxDifferentialLinkDelay);
    uint32 (*MaxDifferentialLinkDelayGet)(AtImaGroup self);

    /* Group management */
    eAtImaRet (*Inhibit)(AtImaGroup self, eBool enable);
    eBool (*IsInhibited)(AtImaGroup self);
    eAtImaRet (*Restart)(AtImaGroup self);

    /* Link management */
    eAtImaRet (*LinkAdd)(AtImaGroup self, AtImaLink txLink, AtImaLink rxLink);
    uint8 (*TxLinksGet)(AtImaGroup self, AtImaLink *links);
    uint8 (*RxLinksGet)(AtImaGroup self, AtImaLink *links);

    /* Status */
    eAtImaRet (*StatusGet)(AtImaGroup self, tAtImaGroupStatus *status);

    /* Test pattern for link in group */
    eAtImaRet (*TestEnable)(AtImaGroup self, AtImaLink link, eBool enable, uint8 pattern);
    eBool (*TestIsEnabled)(AtImaGroup self);
    eBool (*TestResultGet)(AtImaGroup self);
    }tAtImaGroupMethods;

/* IMA group class */
typedef struct tAtImaGroup
    {
    tAtChannel super;
    const tAtImaGroupMethods *methods;

    }tAtImaGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtImaGroup AtImaGroupObjectInit(AtImaGroup self, uint32 groupId, AtModuleIma module);

#ifdef __cplusplus
}
#endif
#endif /* _ATIMAGROUPINTERNAL_H_ */


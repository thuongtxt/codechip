/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : IMA
 *
 * File        : AtImaLink.c
 *
 * Created Date: Sep 27, 2012
 *
 * Description : IMA link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtImaLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtImaLinkMethods  m_methods;
static uint8 m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Physical bind */
static eAtImaRet TxBind(AtImaLink self, AtAtmTc atmTc)
    {
	AtUnused(atmTc);
	AtUnused(self);
    return cAtImaRetError;
    }

static eAtImaRet RxBind(AtImaLink self, AtAtmTc atmTc)
    {
	AtUnused(atmTc);
	AtUnused(self);
    return cAtImaRetError;
    }

/* Inhibit */
static eAtImaRet Inhibit(AtImaLink self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtImaRetError;
    }

static eBool IsInhibited(AtImaLink self)
    {
	AtUnused(self);
    return cAtFalse;
    }

/* Status */
static eAtImaRet StatusGet(AtImaLink self, tAtImaLinkStatus *status)
    {
	AtUnused(status);
	AtUnused(self);
    return cAtImaRetError;
    }

static void MethodsInit(AtImaLink self)
    {
    if (m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TxBind);
        mMethodOverride(m_methods, RxBind);
        mMethodOverride(m_methods, Inhibit);
        mMethodOverride(m_methods, IsInhibited);
        mMethodOverride(m_methods, StatusGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtImaLink AtImaLinkObjectInit(AtImaLink self, uint16 linkId, AtModuleIma module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(AtImaLink));

    /* Call super constructor */
    if (AtChannelObjectInit((AtChannel)self, linkId, (AtModule)module) == NULL)
        return NULL;

    /* Initialize implements */
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

static eBool LinkIsValid(AtImaLink self)
    {
    return self ? cAtTrue : cAtFalse;
    }

/**
 * @addtogroup AtImaLink
 * @{
 */

/**
 * Bind TX IMA link with physical channel
 *
 * @param self This module
 * @param atmTc ATM TC channel
 * @return IMA return code
 */
eAtImaRet AtImaLinkTxBind(AtImaLink self, AtAtmTc atmTc)
    {
    if (LinkIsValid(self))
        return mMethodsGet(self)->TxBind(self, atmTc);

    return cAtImaRetError;
    }
/**
 * Bind RX IMA link with physical channel
 *
 * @param self This module
 * @param atmTc ATM TC channel
 * @return IMA return code
 */
eAtImaRet AtImaLinkRxBind(AtImaLink self, AtAtmTc atmTc)
    {
    if (LinkIsValid(self))
        return mMethodsGet(self)->RxBind(self, atmTc);

    return cAtImaRetError;
    }

/**
 * Inhibit an IMA link
 *
 * @param self This module
 * @param enable Enable/disable inhibition
 * @return IMA return code
 */
eAtImaRet AtImaLinkInhibit(AtImaLink self, eBool enable)
    {
    if (LinkIsValid(self))
        return mMethodsGet(self)->Inhibit(self, enable);

    return cAtImaRetError;
    }
/**
 * Get Inhibition status of an IMA link
 *
 * @param self This module
 * @return Inhibition status (enable/disable)
 */
eBool AtImaLinkIsInhibited(AtImaLink self)
    {
    if (LinkIsValid(self))
        return mMethodsGet(self)->IsInhibited(self);

    return cAtFalse;
    }
/**
 * Get status of an IMA link
 *
 * @param self This module
 * @param status @ref tAtImaLinkStatus "IMA Link Status"
 *
 * @return IMA return code
 */
eAtImaRet AtImaLinkStatusGet(AtImaLink self, tAtImaLinkStatus *status)
    {
    if (LinkIsValid(self))
        return mMethodsGet(self)->StatusGet(self, status);

    return cAtImaRetError;
    }

/**
 * @}
 */

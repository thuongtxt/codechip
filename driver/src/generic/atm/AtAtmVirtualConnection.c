/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATM
 *
 * File        : AtmVirtualConnection.c
 *
 * Created Date: Oct 4, 2012
 *
 * Description : ATM virtual connection
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleAtmInternal.h"
#include "AtAtmVirtualConnectionInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* To store implementation of this class (require) */
static tAtAtmVirtualConnectionMethods m_methods;

/* To prevent initializing implementation structures more than one times */
static char m_methodsInit = 0;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Encapsulation type */
static eAtAtmRet AalTypeSet(AtAtmVirtualConnection self, eAtAtmAalType aalType)
	{
	AtUnused(aalType);
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmAalType AalTypeGet(AtAtmVirtualConnection self)
	{
	AtUnused(self);
	return cAtAtmAalTypeInvalid;
	}

/* Mapping flow */
static eAtAtmRet EthFlowSet(AtAtmVirtualConnection self, AtEthFlow flow)
	{
	AtUnused(flow);
	AtUnused(self);
	return cAtAtmRetError;
	}
static AtEthFlow EthFlowGet(AtAtmVirtualConnection self)
	{
	AtUnused(self);
	return NULL;
	}
static eAtAtmRet Bind(AtAtmVirtualConnection self, AtChannel atmChannel)
	{
	AtUnused(atmChannel);
	AtUnused(self);
	return cAtError;
	}

static void MethodsInit(AtAtmVirtualConnection self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, AalTypeSet);
        mMethodOverride(m_methods, AalTypeGet);
        mMethodOverride(m_methods, EthFlowSet);
        mMethodOverride(m_methods, EthFlowGet);
        mMethodOverride(m_methods, Bind);
        }

    mMethodsSet(self, &m_methods);
    }

static eBool VirtualConnectionIsValid(AtAtmVirtualConnection self)
    {
    return self ? cAtTrue : cAtFalse;
    }

AtAtmVirtualConnection AtAtmVirtualConnectionObjectInit(AtAtmVirtualConnection self, uint32 vcId, AtModuleAtm module)
	{
	/* Clear memory */
	AtOsal osal = AtSharedDriverOsalGet();
	mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtAtmVirtualConnection));

	/* Super constructor */
	if (AtChannelObjectInit((AtChannel)self, vcId, (AtModule)module) == NULL)
		return NULL;

	/* Initialize implementation */
	MethodsInit(self);

	/* Only initialize method structures one time */
	m_methodsInit = 1;

	return self;
	}

/**
 * @addtogroup AtAtmVirtualConnection
 * @{
 */

/**
 * Set ATM AAL type
 *
 * @param self ATM Virtual Connection
 * @param aalType AAL type
 * @return  ATM return code
 */
eAtAtmRet AtAtmVirtualConnectionAalTypeSet(AtAtmVirtualConnection self, eAtAtmAalType aalType)
	{
	if (VirtualConnectionIsValid(self))
		return mMethodsGet(self)->AalTypeSet(self, aalType);

	return cAtAtmRetError;
	}
/**
 * Get ATM AAL type
 *
 * @param self ATM Virtual Connection
 * @return  ATM AAL type. Refer
 * 						@ref eAtAtmAalType "ATM AAL type"
 */
eAtAtmAalType AtAtmVirtualConnectionAalTypeGet(AtAtmVirtualConnection self)
	{
	if (VirtualConnectionIsValid(self))
		return mMethodsGet(self)->AalTypeGet(self);

	return cAtAtmAalTypeInvalid;
	}

/**
 * Set Ethernet flow for virtual connection
 *
 * @param self ATM Virtual Connection
 * @param flow Ethernet flow
 * @return  ATM return code
 */
eAtAtmRet AtAtmVirtualConnectionEthFlowSet(AtAtmVirtualConnection self, AtEthFlow flow)
	{
	if (VirtualConnectionIsValid(self))
		return mMethodsGet(self)->EthFlowSet(self, flow);

	return cAtAtmRetError;
	}
/**
 * Get Ethernet flow for virtual connection
 *
 * @param self ATM Virtual Connection
 * @return  Ethernet flow. Refer
 * 						@ref AtEthFlow "Ethernet flow"
 */
AtEthFlow AtAtmVirtualConnectionEthFlowGet(AtAtmVirtualConnection self)
	{
	if (VirtualConnectionIsValid(self))
		return mMethodsGet(self)->EthFlowGet(self);

	return NULL;
	}
/**
 * Bind virtual connection to ATM channel. Currently, only AtAtmTc can be bound to
 * Virtual Connection
 *
 * @param self ATM Virtual Connection
 * @param atmChannel ATM channel
 * @return  AT return code
 */
eAtAtmRet AtAtmVirtualConnectionBind (AtAtmVirtualConnection self, AtChannel atmChannel)
	{
	if (VirtualConnectionIsValid(self))
		return mMethodsGet(self)->Bind (self, atmChannel);

	return cAtAtmRetError;
	}

/**
 * @}
 */

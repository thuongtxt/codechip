/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATM
 * 
 * File        : AtAtmCrossConnectInternal.h
 * 
 * Created Date: Oct 4, 2012
 *
 * Description : ATM cross-connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATMCROSSCONNECTINTERNAL_H_
#define _ATATMCROSSCONNECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAtm.h"
#include "AtAtmCrossConnect.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAtmCrossConnectMethods
	{
	/* Cross connect type */
	eAtAtmRet (*TypeSet)(AtAtmCrossConnect self, eAtAtmCrossConnectType type);
	eAtAtmCrossConnectType (*TypeGet)(AtAtmCrossConnect self);

	/* Source */
	eAtAtmRet (*SourcePortSet)(AtAtmCrossConnect self, AtChannel sourcePort);
	AtChannel (*SourcePortGet)(AtAtmCrossConnect self);
	eAtAtmRet (*SourceVpiSet)(AtAtmCrossConnect self, uint16 sourceVpi);
	uint16 (*SourceVpiGet)(AtAtmCrossConnect self);
	eAtAtmRet (*SourceVciSet)(AtAtmCrossConnect self, uint16 sourceVci);
	uint16 (*SourceVciGet)(AtAtmCrossConnect self);

	/* Destination */
	eAtAtmRet (*DestPortSet)(AtAtmCrossConnect self, AtChannel destPort);
	AtChannel (*DestPortGet)(AtAtmCrossConnect self);
	eAtAtmRet (*DestVpiSet)(AtAtmCrossConnect self, uint16 destVpi);
	uint16 (*DestVpiGet)(AtAtmCrossConnect self);
	eAtAtmRet (*DestVciSet)(AtAtmCrossConnect self, uint16 destVci);
	uint16 (*DestVciGet)(AtAtmCrossConnect self);

	/* Service Categories */
	eAtAtmRet (*ServiceTypeSet)(AtAtmCrossConnect self, eAtAtmServiceType serviceType);
	eAtAtmServiceType (*ServiceTypeGet)(AtAtmCrossConnect self);

	/* Policing parameters */
	eAtAtmRet (*PolicingModeSet)(AtAtmCrossConnect self, eAtAtmPolicingMode policingMode);
	eAtAtmPolicingMode (*PolicingModeGet)(AtAtmCrossConnect self);
	eAtAtmRet (*PolPcrSet)(AtAtmCrossConnect self, uint32 polPcr);
	uint32 (*PolPcrGet)(AtAtmCrossConnect self);
	eAtAtmRet (*PolScrSet)(AtAtmCrossConnect self, uint32 polScr);
	uint32 (*PolScrGet)(AtAtmCrossConnect self);
	eAtAtmRet (*PolCdvtSet)(AtAtmCrossConnect self, uint32 polCdvt);
	uint32 (*PolCdvtGet)(AtAtmCrossConnect self);
	eAtAtmRet (*PolMbsSet)(AtAtmCrossConnect self, uint32 polMbs);
	uint32 (*PolMbsGet)(AtAtmCrossConnect self);

	/* Shaping parameters */
	eAtAtmRet (*ShapingModeSet)(AtAtmCrossConnect self, eAtAtmShapingMode shapingMode);
	eAtAtmShapingMode (*ShapingModeGet)(AtAtmCrossConnect self);
	eAtAtmRet (*ShapPcrSet)(AtAtmCrossConnect self, uint32 shapPcr);
	uint32 (*ShapPcrGet)(AtAtmCrossConnect self);
	eAtAtmRet (*ShapScrSet)(AtAtmCrossConnect self, uint32 shapScr);
	uint32 (*ShapScrGet)(AtAtmCrossConnect self);
	eAtAtmRet (*ShapCdvtSet)(AtAtmCrossConnect self, uint32 shapCdvt);
	uint32 (*ShapCdvtGet)(AtAtmCrossConnect self);
	eAtAtmRet (*ShapMbsSet)(AtAtmCrossConnect self, uint32 shapMbs);
	uint32 (*ShapMbsGet)(AtAtmCrossConnect self);
	eAtAtmRet (*ShapMcrSet)(AtAtmCrossConnect self, uint32 shapMcr);
	uint32 (*ShapMcrGet)(AtAtmCrossConnect self);

	/* Unit of Traffic parameters */
	eAtAtmRet (*TrafficParamUnitSet)(AtAtmCrossConnect self, eAtAtmTrafficParamUnit trafficParamUnit);
	eAtAtmTrafficParamUnit (*TrafficParamUnitGet)(AtAtmCrossConnect self);

	eAtAtmRet (*PolicingEnable)(AtAtmCrossConnect self, eBool enable);
	eBool (*PolicingIsEnabled)(AtAtmCrossConnect self);
	eAtAtmRet (*ShapingEnable)(AtAtmCrossConnect self, eBool enable);
	eBool (*ShapingIsEnabled)(AtAtmCrossConnect self);
	}tAtAtmCrossConnectMethods;

typedef struct tAtAtmCrossConnect
	{
	/* Super class */
	tAtChannel	super;

	/* Implementation */
	const tAtAtmCrossConnectMethods *methods;
	}tAtAtmCrossConnect;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAtmCrossConnect AtAtmCrossConnectObjectInit(AtAtmCrossConnect self, uint32 xcId, AtModuleAtm module);

#ifdef __cplusplus
}
#endif
#endif /* _ATATMCROSSCONNECTINTERNAL_H_ */


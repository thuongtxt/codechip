/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATM
 * 
 * File        : AtModuleAtmInternal.h
 * 
 * Created Date: Oct 3, 2012
 *
 * Description : ATM module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEATMINTERNAL_H_
#define _ATMODULEATMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAtm.h"
#include "AtAtmCrossConnectInternal.h"
#include "AtAtmVirtualConnectionInternal.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleAtmMethods
	{
	/* ATM Cross Connect manage */
	uint32 (*MaxCrossConnectsGet)(AtModuleAtm self);
	uint32 (*FreeCrossConnectGet)(AtModuleAtm self);
	AtAtmCrossConnect (*CrossConnectCreate)(AtModuleAtm self, uint32 xcId);
	AtAtmCrossConnect (*CrossConnectGet)(AtModuleAtm self, uint32 xcId);
	eAtAtmRet (*CrossConnectDelete)(AtModuleAtm self, uint32 xcId);

	/* ATM Virtual Connection manage */
	uint32 (*MaxVirtualConnectionsGet)(AtModuleAtm self);
	uint32 (*FreeVirtualConnectionGet)(AtModuleAtm self);
	AtAtmVirtualConnection (*VirtualConnectionCreate)(AtModuleAtm self, uint32 vcId, uint16 vpi, uint16 vci);
	AtAtmVirtualConnection (*VirtualConnectionGet)(AtModuleAtm self, uint32 vcId);
	eAtAtmRet (*VirtualConnectionDelete)(AtModuleAtm self, uint32 vcId);

	/* ISIS MAC translation */
	eAtAtmRet (*LlcISISMacSet)(AtModuleAtm self, const uint8 *mac);
	eAtAtmRet (*LlcISISMacGet)(AtModuleAtm self, uint8 *mac);
	}tAtModuleAtmMethods;

typedef struct tAtModuleAtm
	{
	/* Super class */
	tAtModule 				super;

	/* Implementation */
	const tAtModuleAtmMethods *methods;
	}tAtModuleAtm;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAtm AtModuleAtmObjectInit(AtModuleAtm self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEATMINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATM
 * 
 * File        : AtmVirtualConnectionInternal.h
 * 
 * Created Date: Oct 4, 2012
 *
 * Description : ATM virtual connection
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMVIRTUALCONNECTIONINTERNAL_H_
#define _ATMVIRTUALCONNECTIONINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtAtmVirtualConnection.h"
#include "AtModuleAtm.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAtmVirtualConnectionMethods
	{
	/* Encapsulation type */
	eAtAtmRet (*AalTypeSet)(AtAtmVirtualConnection self, eAtAtmAalType aalType);
	eAtAtmAalType (*AalTypeGet)(AtAtmVirtualConnection self);

	/* Mapping flow */
	eAtAtmRet (*EthFlowSet)(AtAtmVirtualConnection self, AtEthFlow flow);
	AtEthFlow (*EthFlowGet)(AtAtmVirtualConnection self);
	eAtAtmRet (*Bind)(AtAtmVirtualConnection self, AtChannel atmChannel);
	}tAtAtmVirtualConnectionMethods;

typedef struct tAtAtmVirtualConnection
	{
	/* Super class */
	AtChannel	super;

	/* Implementation */
	const tAtAtmVirtualConnectionMethods *methods;

	}tAtAtmVirtualConnection;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAtmVirtualConnection AtAtmVirtualConnectionObjectInit(AtAtmVirtualConnection self, uint32 vcId, AtModuleAtm module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMVIRTUALCONNECTIONINTERNAL_H_ */

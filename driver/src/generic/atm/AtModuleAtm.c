/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATM
 *
 * File        : AtModuleAtm.c
 *
 * Created Date: Oct 3, 2012
 *
 * Description : ATM module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleAtmInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* To store implementation of this class (require) */
static tAtModuleAtmMethods m_methods;

/* To override super methods */
static tAtObjectMethods m_AtObjectOverride;

/* To cache method of AtObject */
static const tAtObjectMethods *m_AtObjectMethods;

/* To prevent initializing implementation structures more than one times */
static char m_methodsInit = 0;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Delete module */
static void Delete(AtObject self)
    {
    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static const char *ToString(AtObject self)
    {
    AtModuleAtm moduleAtm = (AtModuleAtm)self;
    static char string[32];
    AtDevice dev = AtModuleDeviceGet((AtModule)moduleAtm);

    AtSprintf(string, "%sXc: %u, Vc: %u", AtDeviceIdToString(dev), AtModuleAtmMaxCrossConnectsGet(moduleAtm), AtModuleAtmMaxVirtualConnectionsGet(moduleAtm));
    return string;
    }

static void OverrideAtObject(AtModuleAtm self)
    {
	AtOsal osal = AtSharedDriverOsalGet();
	AtObject object = (AtObject)self;

	if (!m_methodsInit)
		{
		/* Save reference to super methods */
		m_AtObjectMethods = mMethodsGet(object);

		/* Copy to reuse methods of super class. But override some Methods */
		mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

		m_AtObjectOverride.Delete   = Delete;
		m_AtObjectOverride.ToString = ToString;
		}

	/* Change behavior of super class level 2 */
	mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleAtm self)
    {
    OverrideAtObject(self);
    }

/* ATM Cross connect manage */
static uint32 MaxCrossConnectsGet(AtModuleAtm self)
	{
	AtUnused(self);
	return 0;
	}
static uint32 FreeCrossConnectGet(AtModuleAtm self)
	{
	AtUnused(self);
	return 0;
	}

static AtAtmCrossConnect CrossConnectCreate(AtModuleAtm self, uint32 xcId)
	{
	AtUnused(xcId);
	AtUnused(self);
	return NULL;
	}
static AtAtmCrossConnect CrossConnectGet(AtModuleAtm self, uint32 xcId)
	{
	AtUnused(xcId);
	AtUnused(self);
	return NULL;
	}
static eAtAtmRet CrossConnectDelete(AtModuleAtm self, uint32 xcId)
	{
	AtUnused(xcId);
	AtUnused(self);
	return cAtAtmRetError;
	}

/* ATM Virtual Connection manage */
static uint32 MaxVirtualConnectionsGet(AtModuleAtm self)
	{
	AtUnused(self);
	return 0;
	}
static uint32 FreeVirtualConnectionGet(AtModuleAtm self)
	{
	AtUnused(self);
	return 0;
	}
static AtAtmVirtualConnection VirtualConnectionCreate(AtModuleAtm self, uint32 vcId, uint16 vpi, uint16 vci)
	{
	AtUnused(vci);
	AtUnused(vpi);
	AtUnused(vcId);
	AtUnused(self);
	return NULL;
	}
static AtAtmVirtualConnection VirtualConnectionGet(AtModuleAtm self, uint32 vcId)
	{
	AtUnused(vcId);
	AtUnused(self);
	return NULL;
	}
static eAtAtmRet VirtualConnectionDelete(AtModuleAtm self, uint32 vcId)
	{
	AtUnused(vcId);
	AtUnused(self);
	return cAtAtmRetError;
	}

static eAtAtmRet LlcISISMacSet(AtModuleAtm self, const uint8 *mac)
    {
	AtUnused(mac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtAtmRetError;
    }

static eAtAtmRet LlcISISMacGet(AtModuleAtm self, uint8 *mac)
    {
	AtUnused(mac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtAtmRetError;
    }

static void MethodsInit(AtModuleAtm self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
    	mMethodOverride(m_methods, MaxCrossConnectsGet);
		mMethodOverride(m_methods, FreeCrossConnectGet);
    	mMethodOverride(m_methods, CrossConnectCreate);
    	mMethodOverride(m_methods, CrossConnectGet);
    	mMethodOverride(m_methods, CrossConnectDelete);

    	mMethodOverride(m_methods, MaxVirtualConnectionsGet);
    	mMethodOverride(m_methods, FreeVirtualConnectionGet);
    	mMethodOverride(m_methods, VirtualConnectionCreate);
    	mMethodOverride(m_methods, VirtualConnectionGet);
    	mMethodOverride(m_methods, VirtualConnectionDelete);
    	mMethodOverride(m_methods, LlcISISMacSet);
    	mMethodOverride(m_methods, LlcISISMacGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtModuleAtm AtModuleAtmObjectInit(AtModuleAtm self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModuleAtm));

    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModuleAtm, device) == NULL)
        return NULL;

    /* Override Delete method of AtObject class */
    Override(self);

    /* Initialize methods */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

static eBool ModuleIsValid(AtModuleAtm self)
    {
    return self ? cAtTrue : cAtFalse;
    }

/**
 * @addtogroup AtModuleAtm
 * @{
 */

/**
 * Get maximum number of Cross Connect
 *
 * @param self This module
 * @return Maximum Cross connect
 */
uint32 AtModuleAtmMaxCrossConnectsGet(AtModuleAtm self)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->MaxCrossConnectsGet(self);

	return 0;
	}
/**
 * Get the first free Cross Connect
 *
 * @param self This module
 * @return The first free Cross Connect
 */
uint32 AtModuleAtmFreeCrossConnectGet(AtModuleAtm self)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->FreeCrossConnectGet(self);

	return 0;
	}

/**
 * Create new ATM cross connect
 *
 * @param self This module
 * @param xcId Cross connect ID
 * @return Instance of ATM cross connect. Refer
 * 									@ref AtAtmCrossConnect "ATM Cross connect Object"
 */
AtAtmCrossConnect AtModuleAtmCrossConnectCreate(AtModuleAtm self, uint32 xcId)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->CrossConnectCreate(self, xcId);

    return NULL;
	}
/**
 * Get an ATM cross connect
 *
 * @param self This module
 * @param xcId Cross connect ID
 * @return Instance of ATM cross connect. Refer
 * 									@ref AtAtmCrossConnect "ATM Cross connect Object"
 */
AtAtmCrossConnect AtModuleAtmCrossConnectGet(AtModuleAtm self, uint32 xcId)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->CrossConnectGet(self, xcId);

    return NULL;
	}
/**
 * Delete an ATM cross connect
 *
 * @param self This module
 * @param xcId Cross connect ID
 * @return ATM return code
 */
eAtAtmRet AtModuleAtmCrossConnectDelete(AtModuleAtm self, uint32 xcId)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->CrossConnectDelete(self, xcId);
    return cAtAtmRetError;
	}

/**
 * Get maximum number of Virtual connection
 *
 * @param self This module
 * @return Maximum Virtual connection
 */
uint32 AtModuleAtmMaxVirtualConnectionsGet(AtModuleAtm self)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->MaxVirtualConnectionsGet(self);

	return 0;
	}
/**
 * Get the first free Virtual connection
 *
 * @param self This module
 * @return The first free Virtual connection
 */
uint32 AtModuleAtmFreeVirtualConnectionGet(AtModuleAtm self)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->FreeVirtualConnectionGet(self);

	return 0;
	}
/**
 * Create a new Virtual connection
 *
 * @param self This module
 * @param vcId Virtual Connection ID
 * @param vpi  VPI value
 * @param vci  VCI value
 * @return Instance of Virtual connection. Refer
 * 									@ref AtAtmVirtualConnection "ATM Virtual Connection Object"
 */
AtAtmVirtualConnection AtModuleAtmVirtualConnectionCreate(AtModuleAtm self, uint32 vcId, uint16 vpi, uint16 vci)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->VirtualConnectionCreate(self, vcId, vpi, vci);

	return NULL;
	}
/**
 * Get a Virtual connection
 *
 * @param self This module
 * @param vcId Virtual Connection ID
 * @return Instance of Virtual connection. Refer
 * 									@ref AtAtmVirtualConnection "ATM Virtual Connection Object"
 */
AtAtmVirtualConnection AtModuleAtmVirtualConnectionGet(AtModuleAtm self, uint32 vcId)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->VirtualConnectionGet(self, vcId);

	return NULL;
	}
/**
 * Delete a Virtual connection
 *
 * @param self This module
 * @param vcId Virtual Connection ID
 * @return ATM return code
 */
eAtAtmRet AtModuleAtmVirtualConnectionDelete(AtModuleAtm self, uint32 vcId)
	{
	if (ModuleIsValid(self))
		return mMethodsGet(self)->VirtualConnectionDelete(self, vcId);

	return cAtAtmRetError;
	}

/**
 * Configure MAC to be inserted to Dest MAC field of Ethernet frames sent to Ethernet
 * module for ISIS packets come from TDM side. Packets are considered as ISIS
 * packets, when hardware detects: LLC=0xfefe03, NPLID=0x83
 *
 * @param self This module
 * @param mac Inserted ISIS MAC
 *
 * @return ATM module return code
 */
eAtAtmRet AtModuleAtmLlcISISMacSet(AtModuleAtm self, const uint8 *mac)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->LlcISISMacSet(self, mac);

    return cAtAtmRetError;
    }

/**
 * Get configured MAC to be inserted to Dest MAC field of Ethernet frames sent to Ethernet
 * module for ISIS packets come from TDM side. Packets are considered as ISIS
 * packets, when hardware detects: LLC=0xfefe03, NPLID=0x83
 *
 * @param self This module
 * @param [out] mac Inserted ISIS MAC
 *
 * @return ATM module return code
 */
eAtAtmRet AtModuleAtmLlcISISMacGet(AtModuleAtm self, uint8 *mac)
    {
    if (ModuleIsValid(self))
        return mMethodsGet(self)->LlcISISMacGet(self, mac);

    return cAtAtmRetError;
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATM
 *
 * File        : AtAtmCrossConnect.c
 *
 * Created Date: Oct 4, 2012
 *
 * Description : ATM cross-connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleAtmInternal.h"
#include "AtAtmCrossConnectInternal.h"
#include "../common/AtChannelInternal.h"


/*--------------------------- Define -----------------------------------------*/
/* To store implementation of this class (require) */
static tAtAtmCrossConnectMethods m_methods;

/* To prevent initializing implementation structures more than one times */
static char m_methodsInit = 0;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/* Cross connect type */
static eAtAtmRet TypeSet(AtAtmCrossConnect self, eAtAtmCrossConnectType type)
	{
	AtUnused(type);
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmCrossConnectType TypeGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}

/* Source */
static eAtAtmRet SourcePortSet(AtAtmCrossConnect self, AtChannel sourcePort)
	{
	AtUnused(sourcePort);
	AtUnused(self);
	return cAtAtmRetError;
	}
static AtChannel SourcePortGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return NULL;
	}
static eAtAtmRet SourceVpiSet(AtAtmCrossConnect self, uint16 sourceVpi)
	{
	AtUnused(sourceVpi);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint16 SourceVpiGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet SourceVciSet(AtAtmCrossConnect self, uint16 sourceVci)
	{
	AtUnused(sourceVci);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint16 SourceVciGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}

/* Destination */
static eAtAtmRet DestPortSet(AtAtmCrossConnect self, AtChannel destPort)
	{
	AtUnused(destPort);
	AtUnused(self);
	return cAtAtmRetError;
	}
static AtChannel DestPortGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return NULL;
	}
static eAtAtmRet DestVpiSet(AtAtmCrossConnect self, uint16 destVpi)
	{
	AtUnused(destVpi);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint16 DestVpiGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet DestVciSet(AtAtmCrossConnect self, uint16 destVci)
	{
	AtUnused(destVci);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint16 DestVciGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}

/* Service Categories */
static eAtAtmRet ServiceTypeSet(AtAtmCrossConnect self, eAtAtmServiceType serviceType)
	{
	AtUnused(serviceType);
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmServiceType ServiceTypeGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}

/* Policing parameters */
static eAtAtmRet PolicingModeSet(AtAtmCrossConnect self, eAtAtmPolicingMode policingMode)
	{
	AtUnused(policingMode);
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmPolicingMode PolicingModeGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmRet PolPcrSet(AtAtmCrossConnect self, uint32 polPcr)
	{
	AtUnused(polPcr);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 PolPcrGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet PolScrSet(AtAtmCrossConnect self, uint32 polScr)
	{
	AtUnused(polScr);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 PolScrGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet PolCdvtSet(AtAtmCrossConnect self, uint32 polCdvt)
	{
	AtUnused(polCdvt);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 PolCdvtGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet PolMbsSet(AtAtmCrossConnect self, uint32 polMbs)
	{
	AtUnused(polMbs);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 PolMbsGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}

/* Shaping parameters */
static eAtAtmRet ShapingModeSet(AtAtmCrossConnect self, eAtAtmShapingMode shapingMode)
	{
	AtUnused(shapingMode);
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmShapingMode ShapingModeGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmRet ShapPcrSet(AtAtmCrossConnect self, uint32 shapPcr)
	{
	AtUnused(shapPcr);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 ShapPcrGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet ShapScrSet(AtAtmCrossConnect self, uint32 shapScr)
	{
	AtUnused(shapScr);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 ShapScrGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet ShapCdvtSet(AtAtmCrossConnect self, uint32 shapCdvt)
	{
	AtUnused(shapCdvt);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 ShapCdvtGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet ShapMbsSet(AtAtmCrossConnect self, uint32 shapMbs)
	{
	AtUnused(shapMbs);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 ShapMbsGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}
static eAtAtmRet ShapMcrSet(AtAtmCrossConnect self, uint32 shapMcr)
	{
	AtUnused(shapMcr);
	AtUnused(self);
	return cAtAtmRetError;
	}
static uint32 ShapMcrGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return 0;
	}

/* Unit of Traffic parameters */
static eAtAtmRet TrafficParamUnitSet(AtAtmCrossConnect self, eAtAtmTrafficParamUnit trafficParamUnit)
	{
	AtUnused(trafficParamUnit);
	AtUnused(self);
	return cAtAtmRetError;
	}
static eAtAtmTrafficParamUnit TrafficParamUnitGet(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return cAtAtmRetError;
	}

static eAtAtmRet PolicingEnable(AtAtmCrossConnect self, eBool enable)
	{
	AtUnused(enable);
	AtUnused(self);
	return cAtAtmRetError;
	}

static eBool PolicingIsEnabled(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return cAtFalse;
	}
static eAtAtmRet ShapingEnable(AtAtmCrossConnect self, eBool enable)
	{
	AtUnused(enable);
	AtUnused(self);
	return cAtAtmRetError;
	}
static eBool ShapingIsEnabled(AtAtmCrossConnect self)
	{
	AtUnused(self);
	return cAtFalse;
	}
static void MethodsInit(AtAtmCrossConnect self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
    	mMethodOverride(m_methods, TypeSet);
    	mMethodOverride(m_methods, TypeGet);
    	mMethodOverride(m_methods, SourcePortSet);
    	mMethodOverride(m_methods, SourcePortGet);
    	mMethodOverride(m_methods, SourceVpiSet);
    	mMethodOverride(m_methods, SourceVpiGet);
    	mMethodOverride(m_methods, SourceVciSet);
    	mMethodOverride(m_methods, SourceVciGet);
    	mMethodOverride(m_methods, DestPortSet);
    	mMethodOverride(m_methods, DestPortGet);
    	mMethodOverride(m_methods, DestVpiSet);
    	mMethodOverride(m_methods, DestVpiGet);
    	mMethodOverride(m_methods, DestVciSet);
    	mMethodOverride(m_methods, DestVciGet);
    	mMethodOverride(m_methods, ServiceTypeSet);
    	mMethodOverride(m_methods, ServiceTypeGet);
    	mMethodOverride(m_methods, PolicingModeSet);
    	mMethodOverride(m_methods, PolicingModeGet);
    	mMethodOverride(m_methods, PolPcrSet);
    	mMethodOverride(m_methods, PolPcrGet);
    	mMethodOverride(m_methods, PolScrSet);
    	mMethodOverride(m_methods, PolScrGet);
    	mMethodOverride(m_methods, PolCdvtSet);
    	mMethodOverride(m_methods, PolCdvtGet);
    	mMethodOverride(m_methods, PolMbsSet);
    	mMethodOverride(m_methods, PolMbsGet);
    	mMethodOverride(m_methods, ShapingModeSet);
    	mMethodOverride(m_methods, ShapingModeGet);
    	mMethodOverride(m_methods, ShapPcrSet);
    	mMethodOverride(m_methods, ShapPcrGet);
    	mMethodOverride(m_methods, ShapScrSet);
    	mMethodOverride(m_methods, ShapScrGet);
    	mMethodOverride(m_methods, ShapCdvtSet);
    	mMethodOverride(m_methods, ShapCdvtGet);
    	mMethodOverride(m_methods, ShapMbsSet);
    	mMethodOverride(m_methods, ShapMbsGet);
    	mMethodOverride(m_methods, ShapMcrSet);
    	mMethodOverride(m_methods, ShapMcrGet);
    	mMethodOverride(m_methods, TrafficParamUnitSet);
    	mMethodOverride(m_methods, TrafficParamUnitGet);
    	mMethodOverride(m_methods, PolicingEnable);
    	mMethodOverride(m_methods, PolicingIsEnabled);
    	mMethodOverride(m_methods, ShapingEnable);
    	mMethodOverride(m_methods, ShapingIsEnabled);
        }
    mMethodsSet(self, &m_methods);
    }

AtAtmCrossConnect AtAtmCrossConnectObjectInit(AtAtmCrossConnect self, uint32 channelId, AtModuleAtm module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtAtmCrossConnect));

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

static eBool CrossConnectIsValid(AtAtmCrossConnect self)
    {
    return self ? cAtTrue : cAtFalse;
    }

/**
 * @addtogroup AtAtmCrossConnect
 * @{
 */

/**
 * Set ATM cross connect type
 *
 * @param self ATM Cross connect
 * @param type Cross connect type
 * @return ATM return code
 */
eAtAtmRet AtAtmCrossConnectTypeSet(AtAtmCrossConnect self, eAtAtmCrossConnectType type)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->TypeSet(self, type);
	return cAtAtmRetError;
	}

/**
 * Get ATM cross connect type
 *
 * @param self ATM Cross connect
 * @return Cross connect type. Refer
 *                  - @ref eAtAtmCrossConnectType "ATM cross connect type"
 */
eAtAtmCrossConnectType AtAtmCrossConnectTypeGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->TypeGet(self);

	return cAtAtmRetError;
	}

/**
 * Set source port for ATM cross connect
 *
 * @param self ATM Cross connect
 * @param sourcePort Source port
 *
 * @return ATM return code
 */
eAtAtmRet AtAtmCrossConnectSourcePortSet(AtAtmCrossConnect self, AtChannel sourcePort)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->SourcePortSet(self, sourcePort);

	return cAtAtmRetError;
	}

/**
 * Get source port for ATM cross connect
 *
 * @param self ATM Cross connect
 * @return AT channel. Refer
 *                  - @ref AtChannel "AT channel"
 */
AtChannel AtAtmCrossConnectSourcePortGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->SourcePortGet(self);

	return NULL;
	}
/**
 * Set source VPI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @param sourceVpi Source VPI
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectSourceVpiSet(AtAtmCrossConnect self, uint16 sourceVpi)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->SourceVpiSet(self, sourceVpi);

	return cAtAtmRetError;
	}
/**
 * Get source VPI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @return  Source VPI
 */
uint16 AtAtmCrossConnectSourceVpiGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->SourceVpiGet(self);

	return 0;
	}
/**
 * Set source VCI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @param sourceVci Source VCI
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectSourceVciSet(AtAtmCrossConnect self, uint16 sourceVci)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->SourceVciSet(self, sourceVci);

	return cAtAtmRetError;
	}
/**
 * Get source VCI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @return  Source VCI
 */
uint16 AtAtmCrossConnectSourceVciGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->SourceVciGet(self);

	return 0;
	}
/**
 * Set destination port for ATM cross connect
 *
 * @param self ATM Cross connect
 * @param destPort Destination port
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectDestPortSet(AtAtmCrossConnect self, AtChannel destPort)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->DestPortSet(self, destPort);

	return cAtAtmRetError;
	}
/**
 * Get destination port for ATM cross connect
 *
 * @param self ATM Cross connect
 * @return  Destination port. Refer
 * 							- @ref AtChannel "AT channel"
 */
AtChannel AtAtmCrossConnectDestPortGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->DestPortGet(self);

	return NULL;
	}
/**
 * Set destination VPI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @param destVpi Destination VPI
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectDestVpiSet(AtAtmCrossConnect self, uint16 destVpi)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->DestVpiSet(self, destVpi);

	return cAtAtmRetError;
	}
/**
 * Get destination VPI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @return  Destination VPI
 */
uint16 AtAtmCrossConnectDestVpiGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->DestVpiGet(self);
	return 0;
	}
/**
 * Set destination VCI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @param destVci Destination VCI
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectDestVciSet(AtAtmCrossConnect self, uint16 destVci)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->DestVciSet(self, destVci);

	return cAtAtmRetError;
	}
/**
 * Get destination VCI for ATM cross connect
 *
 * @param self ATM Cross connect
 * @return  Destination VCI
 */
uint16 AtAtmCrossConnectDestVciGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->DestVciGet(self);
	return 0;
	}

/**
 * Set ATM service type
 *
 * @param self ATM Cross connect
 * @param serviceType ATM service type
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectServiceTypeSet(AtAtmCrossConnect self, eAtAtmServiceType serviceType)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ServiceTypeSet(self, serviceType);

	return cAtAtmRetError;
	}
/**
 * Get ATM service type
 *
 * @param self ATM Cross connect
 * @return  ATM service type. Refer
 * 							- @ref eAtAtmServiceType "ATM service type"
 */
eAtAtmServiceType AtAtmCrossConnectServiceTypeGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ServiceTypeGet(self);
	return 0;
	}

/**
 * Set ATM policing mode
 *
 * @param self ATM Cross connect
 * @param policingMode Policing mode
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectPolicingModeSet(AtAtmCrossConnect self, eAtAtmPolicingMode policingMode)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolicingModeSet(self, policingMode);

	return cAtAtmRetError;
	}
/**
 * Get ATM policing mode
 *
 * @param self ATM Cross connect
 * @return  Policing mode. Refer
 * 							- @ref eAtAtmPolicingMode "Policing mode"
 */
eAtAtmPolicingMode AtAtmCrossConnectPolicingModeGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolicingModeGet(self);
	return 0;
	}
/**
 * Set PCR value for Policing
 *
 * @param self ATM Cross connect
 * @param polPcr PCR value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectPolPcrSet(AtAtmCrossConnect self, uint32 polPcr)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolPcrSet(self, polPcr);

	return cAtAtmRetError;
	}
/**
 * Get PCR value for Policing
 *
 * @param self ATM Cross connect
 * @return  PCR value
 */
uint32 AtAtmCrossConnectPolPcrGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolPcrGet(self);
	return 0;
	}
/**
 * Set SCR value for Policing
 *
 * @param self ATM Cross connect
 * @param polScr SCR value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectPolScrSet(AtAtmCrossConnect self, uint32 polScr)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolScrSet(self, polScr);

	return cAtAtmRetError;
	}
/**
 * Get SCR value for Policing
 *
 * @param self ATM Cross connect
 * @return  SCR value
 */
uint32 AtAtmCrossConnectPolScrGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolScrGet(self);
	return 0;
	}
/**
 * Set CDVT value for Policing
 *
 * @param self ATM Cross connect
 * @param polCdvt CDVT value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectPolCdvtSet(AtAtmCrossConnect self, uint32 polCdvt)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolCdvtSet(self, polCdvt);

	return cAtAtmRetError;
	}
/**
 * Get CDVT value for Policing
 *
 * @param self ATM Cross connect
 * @return  CDVT value
 */
uint32 AtAtmCrossConnectPolCdvtGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolCdvtGet(self);

	return 0;
	}
/**
 * Set MBS value for Policing
 *
 * @param self ATM Cross connect
 * @param polMbs MBS value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectPolMbsSet(AtAtmCrossConnect self, uint32 polMbs)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolMbsSet(self, polMbs);

	return cAtAtmRetError;
	}
/**
 * Get MBS value for Policing
 *
 * @param self ATM Cross connect
 * @return  MBS value
 */
uint32 AtAtmCrossConnectPolMbsGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolMbsGet(self);

	return 0;
	}

/**
 * Set ATM shaping mode
 *
 * @param self ATM Cross connect
 * @param shapingMode Shaping mode
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectShapingModeSet(AtAtmCrossConnect self, eAtAtmShapingMode shapingMode)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapingModeSet(self, shapingMode);

	return cAtAtmRetError;
	}
/**
 * Get ATM shaping mode
 *
 * @param self ATM Cross connect
 * @return  Shaping mode. Refer
 * 						- @ref eAtAtmShapingMode "ATM Shaping Mode"
 */
eAtAtmShapingMode AtAtmCrossConnectShapingModeGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapingModeGet(self);
	return 0;
	}
/**
 * Set PCR value for Shaping
 *
 * @param self ATM Cross connect
 * @param shapPcr PCR value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectShapPcrSet(AtAtmCrossConnect self, uint32 shapPcr)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapPcrSet(self, shapPcr);

	return cAtAtmRetError;
	}
/**
 * Get PCR value for Shaping
 *
 * @param self ATM Cross connect
 * @return  PCR value
 */
uint32 AtAtmCrossConnectShapPcrGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapPcrGet(self);

	return 0;
	}
/**
 * Set SCR value for Shaping
 *
 * @param self ATM Cross connect
 * @param shapScr SCR value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectShapScrSet(AtAtmCrossConnect self, uint32 shapScr)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapScrSet(self, shapScr);

	return cAtAtmRetError;
	}
/**
 * Get SCR value for Shaping
 *
 * @param self ATM Cross connect
 * @return  SCR value
 */
uint32 AtAtmCrossConnectShapScrGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapScrGet(self);

	return 0;
	}
/**
 * Set CDVT value for Shaping
 *
 * @param self ATM Cross connect
 * @param shapCdvt CDVT value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectShapCdvtSet(AtAtmCrossConnect self, uint32 shapCdvt)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapCdvtSet(self, shapCdvt);

	return cAtAtmRetError;
	}
/**
 * Get CDVT value for Shaping
 *
 * @param self ATM Cross connect
 * @return  CDVT value
 */
uint32 AtAtmCrossConnectShapCdvtGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapCdvtGet(self);

	return 0;
	}
/**
 * Set MBS value for Shaping
 *
 * @param self ATM Cross connect
 * @param shapMbs MBS value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectShapMbsSet(AtAtmCrossConnect self, uint32 shapMbs)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapMbsSet(self, shapMbs);

	return cAtAtmRetError;
	}
/**
 * Get MBS value for Shaping
 *
 * @param self ATM Cross connect
 * @return MBS value
 */
uint32 AtAtmCrossConnectShapMbsGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapMbsGet(self);

	return 0;
	}
/**
 * Set MCR value for Shaping
 *
 * @param self ATM Cross connect
 * @param shapMcr MCR value
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectShapMcrSet(AtAtmCrossConnect self, uint32 shapMcr)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapMcrSet(self, shapMcr);

	return cAtAtmRetError;
	}
/**
 * Get MCR value for Shaping
 *
 * @param self ATM Cross connect
 * @return  MRC value
 */
uint32 AtAtmCrossConnectShapMcrGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapMcrGet(self);

	return 0;
	}
/**
 * Set unit for traffic parameters
 *
 * @param self ATM Cross connect
 * @param trafficParamUnit Traffic unit
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectTrafficParamUnitSet(AtAtmCrossConnect self, eAtAtmTrafficParamUnit trafficParamUnit)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->TrafficParamUnitSet(self, trafficParamUnit);

	return cAtAtmRetError;
	}
/**
 * Set unit for traffic parameters
 *
 * @param self ATM Cross connect
 * @return  Traffic unit. Refer
 * 						@ref eAtAtmTrafficParamUnit "Unit for traffic parameters"
 */
eAtAtmTrafficParamUnit AtAtmCrossConnectTrafficParamUnitGet(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->TrafficParamUnitGet(self);

	return cAtAtmRetError;
	}
/**
 * Set working mode for Policing
 *
 * @param self ATM Cross connect
 * @param enable Policing working mode(enable/disable)
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectPolicingEnable(AtAtmCrossConnect self, eBool enable)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolicingEnable(self, enable);

	return cAtAtmRetError;
	}
/**
 * Get working mode of Policing
 *
 * @param self ATM Cross connect
 * @return cAtTrue of policing is enabled. Otherwise, cAtFalse is returned
 */
eBool AtAtmCrossConnectPolicingIsEnabled(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->PolicingIsEnabled(self);

	return cAtFalse;
	}
/**
 * Set working mode for Shaping
 *
 * @param self ATM Cross connect
 * @param enable Shaping working mode(enable/disable)
 * @return  ATM return code
 */
eAtAtmRet AtAtmCrossConnectShapingEnable(AtAtmCrossConnect self, eBool enable)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapingEnable(self, enable);

	return cAtAtmRetError;
	}
/**
 * Get working mode of Shaping
 *
 * @param self ATM Cross connect
 * @return cAtTrue if shaping is enabled. Otherwise, cAtFalse is returned
 */
eBool AtAtmCrossConnectShapingIsEnabled(AtAtmCrossConnect self)
	{
	if (CrossConnectIsValid(self))
		return mMethodsGet(self)->ShapingIsEnabled(self);

	return cAtFalse;
	}

/**
 * @}
 */

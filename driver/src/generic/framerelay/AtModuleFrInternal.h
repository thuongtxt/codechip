/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FR
 * 
 * File        : AtModuleFrInternal.h
 * 
 * Created Date: Aug 29, 2012
 *
 * Description : FR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEFRINTERNAL_H_
#define _ATMODULEFRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#include "AtModuleFr.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of module framerelay */
typedef struct tAtModuleFrMethods
    {
    /* MFR bundle manage */
    AtMfrBundle (*MfrBundleCreate)(AtModuleFr self, uint32 bundleId);
    AtMfrBundle (*MfrBundleGet)(AtModuleFr self, uint32 bundleId);
    eAtModuleFrRet (*MfrBundleDelete)(AtModuleFr self, uint32 bundleId);
    uint32 (*MaxBundlesGet)(AtModuleFr self);
    uint32 (*MaxLinksPerBundleGet)(AtModuleFr self);
    }tAtModuleFrMethods;

/* Structure of class AtModuleFr */
typedef struct tAtModuleFr
    {
    /* Inherite AtModule */
    tAtModule super;

    /* This class 's methods */
    tAtModuleFrMethods *methods;

    /* Private attribute */
    } tAtModuleFr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleFr AtModuleFrObjectInit(AtModuleFr self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEFRINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Frame relay
 *
 * File        : AtModuleFr.c
 *
 * Created Date: Aug 14, 2012
 *
 * Description : Frame relay module
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleFrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* To store methods of this class (require) */
static tAtModuleFrMethods m_methods;

/* To override super Methods (require if need to override some Methods) */
static tAtObjectMethods m_AtObjectOverride;

/* To cache method of AtObject */
static const tAtObjectMethods *m_AtObjectMethods;

/* To prevent initializing implementation structures more than one times */
static char m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementations --------------------------------*/
static void Delete(AtObject self)
    {
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void OverrideAtObject(AtModuleFr self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        /* Save reference to super Methods */
        m_AtObjectMethods = mMethodsGet(object);

        /* Copy to reuse Methods of super class. But override some Methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        m_AtObjectOverride.Delete = Delete;
        }

    /* Change behavior of super class level 2 */
    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleFr self)
    {
    OverrideAtObject(self);
    }

static AtMfrBundle MfrBundleCreate(AtModuleFr self, uint32 bundleId)
    {
	AtUnused(bundleId);
	AtUnused(self);
    return NULL;
    }

static AtMfrBundle MfrBundleGet(AtModuleFr self, uint32 bundleId)
    {
	AtUnused(bundleId);
	AtUnused(self);
    return NULL;
    }

static eAtModuleFrRet MfrBundleDelete(AtModuleFr self, uint32 bundleId)
    {
	AtUnused(bundleId);
	AtUnused(self);
    return cAtOk;
    }

static uint32 MaxBundlesGet(AtModuleFr self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxLinksPerBundleGet(AtModuleFr self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(AtModuleFr self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MfrBundleCreate);
        mMethodOverride(m_methods, MfrBundleGet);
        mMethodOverride(m_methods, MfrBundleDelete);
        mMethodOverride(m_methods, MaxBundlesGet);
        mMethodOverride(m_methods, MaxLinksPerBundleGet);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize Module frame relay object */
AtModuleFr AtModuleFrObjectInit(AtModuleFr self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModuleFr));

    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModulePdh, device) == NULL)
        return NULL;

    /* Override Delete method of AtObject class */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModuleFr
 * @{
 */

/**
 * Create a MFR bundle from bundle ID
 *
 * @param self This module
 * @param bundleId Bundle ID
 * @return Instance of MFR bundle
 */
AtMfrBundle AtModuleFrMfrBundleCreate(AtModuleFr self, uint32 bundleId)
    {
    mOneParamObjectGet(MfrBundleCreate, AtMfrBundle, bundleId);
    }

/**
 * Get MFR bundle from bundle ID
 *
 * @param self This module
 * @param bundleId Bundle ID
 * @return Instance of MFR bundle
 */
AtMfrBundle AtModuleFrMfrBundleGet(AtModuleFr self, uint32 bundleId)
    {
    mOneParamObjectGet(MfrBundleGet, AtMfrBundle, bundleId);
    }

/**
 * Delete MFR bundle
 *
 * @param self This module
 * @param bundleId Bundle ID
 * @return
 */
eAtModuleFrRet AtModuleFrMfrBundleDelete(AtModuleFr self, uint32 bundleId)
    {
    mOneParamAttributeGet(MfrBundleDelete, bundleId, eAtModuleFrRet, cAtErrorNullPointer);
    }

/**
 * Create Iterator for MFR bundles managed by this module
 *
 * @param self This module
 *
 * @return AtIterator object
 */
AtIterator AtModuleFrMfrBundleIteratorCreate(AtModuleFr self)
    {
	AtUnused(self);
    return NULL;
    }

/**
 * Get maximum number of supported MFR bundles
 *
 * @param self This module
 *
 * @return Number of supported bundles
 */
uint32 AtModuleFrMaxBundlesGet(AtModuleFr self)
    {
    mAttributeGet(MaxBundlesGet, uint32, 0);
    }

 /**
  * Get maximum number of links per MFR bundle
  *
  * @param self This module
  *
  * @return Maximum number of links per bundle
  */
uint32 AtModuleFrMaxLinksPerBundleGet(AtModuleFr self)
    {
    mAttributeGet(MaxLinksPerBundleGet, uint32, 0);
    }

/**
 * @}
 */

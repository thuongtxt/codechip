/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : FR
 *
 * File        : AtFrVirtualCircuit.c
 *
 * Created Date: Apr 26, 2016
 *
 * Description : Frame Relay Virtual Circuit
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFrVirtualCircuitInternal.h"
#include "AtModuleEncap.h"
#include "../eth/AtEthFlowInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtFrVirtualCircuit)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtFrVirtualCircuitMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save Super Implementation */
static const tAtObjectMethods           *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtFrVirtualCircuitFlowBind((AtFrVirtualCircuit)self, NULL);
    m_AtObjectMethods->Delete(self);
    }

static eBool ShouldDeletePwOnCleanup(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static const char *TypeString(AtChannel self)
    {
    static char idString[16];
    AtChannel frChannel = (AtChannel)AtFrVirtualCircuitLinkGet((AtFrVirtualCircuit)self);
    if (frChannel != NULL)
        AtSprintf(idString, "frvc.%u", AtChannelIdGet(frChannel) + 1);
    else
        AtSprintf(idString, "mfrvc.%u", AtChannelIdGet((AtChannel)AtFrVirtualCircuitBundleGet((AtFrVirtualCircuit)self)) + 1);

    return idString;
    }

static eAtModuleFrRet FlowBinding(AtFrVirtualCircuit self, AtEthFlow flow)
    {
    if (AtEthFlowFrVirtualCircuitSet(flow, self) != cAtOk)
        return cAtErrorResourceBusy;

    self->boundflow = flow;
    return cAtOk;
    }

static eAtModuleFrRet FlowUnBind(AtFrVirtualCircuit self)
    {
    AtEthFlow boundFlow = AtFrVirtualCircuitBoundFlowGet(self);
    if (boundFlow == NULL)
        return cAtOk;

    AtEthFlowFrVirtualCircuitSet(boundFlow, NULL);
    self->boundflow = NULL;

    return cAtOk;
    }

static eAtModuleFrRet FlowBind(AtFrVirtualCircuit self, AtEthFlow flow)
    {
    AtEthFlow currentFlow = AtFrVirtualCircuitBoundFlowGet(self);
    if (currentFlow == flow)
        return cAtOk;

    if (flow == NULL)
        return FlowUnBind(self);

    if (currentFlow)
        return cAtErrorChannelBusy;

    return FlowBinding(self, flow);
    }

static AtEthFlow BoundFlowGet(AtFrVirtualCircuit self)
    {
    if (self)
        return self->boundflow;

    return NULL;
    }

static AtFrLink FrLinkGet(AtFrVirtualCircuit self)
    {
    AtUnused(self);
    return NULL;
    }

static AtMfrBundle FrBundleGet(AtFrVirtualCircuit self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    if ((pseudowire == NULL) || (AtPwTypeGet(pseudowire) == cAtPwTypeFr))
        return cAtOk;

    return cAtErrorInvlParm;
    }

static void OverrideAtObject(AtFrVirtualCircuit self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtFrVirtualCircuit self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, ShouldDeletePwOnCleanup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtFrVirtualCircuit self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(AtFrVirtualCircuit self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, FlowBind);
        mMethodOverride(m_methods, BoundFlowGet);
        mMethodOverride(m_methods, FrLinkGet);
        mMethodOverride(m_methods, FrBundleGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtFrVirtualCircuit AtFrVirtualCircuitObjectInit(AtFrVirtualCircuit self, AtChannel channel, uint32 dlci)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtFrVirtualCircuit));

    /* Call super constructor */
    if (AtChannelObjectInit((AtChannel)self, dlci, AtChannelModuleGet(channel)) == NULL)
        return NULL;

    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->channel = channel;

    return self;
    }

/**
 * @addtogroup AtFrVirtualCircuit
 * @{
 */

/**
 * Bind to Ethernet flow
 *
 * @param self This virtual circuit
 * @param flow Ethernet flow
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrVirtualCircuitFlowBind(AtFrVirtualCircuit self, AtEthFlow flow)
    {
    mObjectSet(FlowBind, flow);
    }

/**
 * Get Ethernet flow that is bound to this virtual circuit
 *
 * @param self This virtual circuit
 *
 * @return Ethernet flow
 */
AtEthFlow AtFrVirtualCircuitBoundFlowGet(AtFrVirtualCircuit self)
    {
    mNoParamObjectGet(BoundFlowGet, AtEthFlow);
    }

/**
 * Get MFR bundle that this virtual circuit belongs to
 *
 * @param self This virtual circuit
 *
 * @return MFR bundle that this virtual circuit belongs to.
 * NULL is returned if it belongs to FR link.
 */
AtMfrBundle AtFrVirtualCircuitBundleGet(AtFrVirtualCircuit self)
    {
    mAttributeGet(FrBundleGet, AtMfrBundle, NULL);
    }

/**
 * Get FR link that this virtual circuit belongs to
 *
 * @param self This virtual circuit
 *
 * @return FR link that this virtual circuit belongs to.
 * NULL is returned if it belongs to MFR bundle.
 */
AtFrLink AtFrVirtualCircuitLinkGet(AtFrVirtualCircuit self)
    {
    mAttributeGet(FrLinkGet, AtFrLink, NULL);
    }

/**
 * Get OAM flow of this virtual circuit
 *
 * @param self This virtual circuit
 * @return OAM flow
 */
AtEthFlow AtFrVirtualCircuitOamFlowGet(AtFrVirtualCircuit self)
    {
    /* TODO: implement me */
    AtUnused(self);
    return NULL;
    }

/**
 * Get DLCI
 *
 * @param self This virtual circuit
 *
 * @return DLCI
 */
uint32 AtFrVirtualCircuitDlciGet(AtFrVirtualCircuit self)
    {
    /* TODO: implement me */
    AtUnused(self);
    return cInvalidUint32;
    }

/**
 * @}
 */

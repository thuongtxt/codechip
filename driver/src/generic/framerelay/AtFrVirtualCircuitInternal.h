/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FR
 * 
 * File        : AtFrVirtualCircuitInternal.h
 * 
 * Created Date: Apr 26, 2016
 *
 * Description : Frame Relay Virtual Circuit Internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFRVIRTUALCIRCUITINTERNAL_H_
#define _ATFRVIRTUALCIRCUITINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelInternal.h"
#include "AtFrVirtualCircuit.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFrVirtualCircuitMethods
    {
    eAtModuleEncapRet (*FlowBind)(AtFrVirtualCircuit self, AtEthFlow flow);
    AtEthFlow (*BoundFlowGet)(AtFrVirtualCircuit self);
    AtFrLink (*FrLinkGet)(AtFrVirtualCircuit self);
    AtMfrBundle (*FrBundleGet)(AtFrVirtualCircuit self);
    }tAtFrVirtualCircuitMethods;

typedef struct tAtFrVirtualCircuit
    {
    tAtChannel super;
    const tAtFrVirtualCircuitMethods *methods;

    /* Private data */
    AtEthFlow boundflow;
    AtChannel channel; /* FrLink or MfrLink*/
    }tAtFrVirtualCircuit;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrVirtualCircuit AtFrVirtualCircuitObjectInit(AtFrVirtualCircuit self, AtChannel channel, uint32 dlci);

#ifdef __cplusplus
}
#endif
#endif /* _ATFRVIRTUALCIRCUITINTERNAL_H_ */


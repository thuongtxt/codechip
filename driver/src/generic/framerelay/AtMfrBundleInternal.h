/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtMfrBundleInternal.h
 * 
 * Created Date: Aug 31, 2012
 *
 * Description : Frame Relay bundle Internal Interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMFRBUNDLEINTERNAL_H_
#define _ATMFRBUNDLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMfrBundle.h"
#include "../encap/AtHdlcBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of MFR bundle */
typedef struct tAtMfrBundleMethods
    {
    AtFrVirtualCircuit (*VirtualCircuitCreate)(AtMfrBundle self, uint32 dlci);
    eAtModuleFrRet (*VirtualCircuitDelete)(AtMfrBundle self, uint32 dlci);
    AtFrVirtualCircuit (*VirtualCircuitGet)(AtMfrBundle self, uint32 dlci);
    AtFrVirtualCircuit (*VirtualCircuitObjectCreate)(AtMfrBundle self, uint32 dlci);
    eAtRet (*FragmentFormatSet)(AtMfrBundle self, eAtMfrBundleFragmentFormat format);
    eAtMfrBundleFragmentFormat (*FragmentFormatGet)(AtMfrBundle self);
    }tAtMfrBundleMethods;


/* Structure of class Mfr bundle */
typedef struct tAtMfrBundle
    {
    /* Inherit AtHdlcBundle */
    tAtHdlcBundle super;
    
    /* This class's methods */
    tAtMfrBundleMethods *methods;
    
    /* All added flows */
    AtList virtualCircuits; /* To cache all VCs that management by this bundle */
    }tAtMfrBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMfrBundle AtMfrBundleObjectInit(AtMfrBundle self, uint32 channelId, AtModuleEncap module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMFRBUNDLEINTERNAL_H_ */


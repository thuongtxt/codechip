/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FR
 * 
 * File        : AtFrLinkInternal.h
 * 
 * Created Date: Aug 31, 2012
 *
 * Description : Frame Relay Link Internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFRLINKINTERNAL_H_
#define _ATFRLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtFrLink.h"
#include "../encap/AtHdlcLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtFrLinkMethods
    {
    AtFrVirtualCircuit (*VirtualCircuitCreate)(AtFrLink self, uint32 dlci);
    eAtModuleFrRet (*VirtualCircuitDelete)(AtFrLink self, uint32 dlci);
    AtFrVirtualCircuit (*VirtualCircuitGet)(AtFrLink self, uint32 dlci);
    AtFrVirtualCircuit (*VirtualCircuitObjectCreate)(AtFrLink self, uint32 dlci);
    eAtModuleFrRet (*TxQ922AddressDCSet)(AtFrLink self, uint8 value);
    uint8 (*TxQ922AddressDCGet)(AtFrLink self);
    eAtModuleFrRet (*TxQ922AddressCRSet)(AtFrLink self, uint8 value);
    uint8 (*TxQ922AddressCRGet)(AtFrLink self);
    eAtModuleFrRet (*EncapTypeSet)(AtFrLink self, eAtFrLinkEncapType encapType);
    eAtFrLinkEncapType (*EncapTypeGet)(AtFrLink self);
    eAtModuleFrRet (*TxQ922AddressFECNSet)(AtFrLink self, uint8 fecn);
    uint8 (*TxQ922AddressFECNGet)(AtFrLink self);
    eAtModuleFrRet (*TxQ922AddressBECNSet)(AtFrLink self, uint8 becn);
    uint8 (*TxQ922AddressBECNGet)(AtFrLink self);
    eAtModuleFrRet (*TxQ922AddressDESet)(AtFrLink self, uint8 de);
    uint8 (*TxQ922AddressDEGet)(AtFrLink self);
    eAtModuleFrRet (*TxQ922AddressDlCoreSet)(AtFrLink self, uint8 de);
    uint8 (*TxQ922AddressDlCoreGet)(AtFrLink self);
    }tAtFrLinkMethods;

/* Structure of class FR link */
typedef struct tAtFrLink
    {
    tAtHdlcLink super;
    const tAtFrLinkMethods *methods;

    /* Private attribute */
    AtList virtualCircuits; /* To cache VCs management by this link*/
    }tAtFrLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrLink AtFrLinkObjectInit(AtFrLink self, AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _ATFRLINKINTERNAL_H_ */


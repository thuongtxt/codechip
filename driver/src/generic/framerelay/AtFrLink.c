/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : AtFrLink.c
 *
 * Created Date: Aug 11, 2012
 *
 * Description : Frame Relay link Implementation
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtFrLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mLinkIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Global variables -------------------------------*/

/*-------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtFrLinkMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods = NULL;
static const tAtChannelMethods  *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "frlink";
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtErrorModeNotSupport;
    }

static eBool VirtualCircuitIsExisting(AtFrLink self, uint32 dlci)
    {
    AtFrVirtualCircuit dlciCircuit;
    eBool ret = cAtFalse;
    AtIterator vcIterator = AtListIteratorCreate(self->virtualCircuits);

    while ((dlciCircuit = (AtFrVirtualCircuit)AtIteratorNext(vcIterator)) != NULL)
        {
        if (AtChannelIdGet((AtChannel)dlciCircuit) == dlci)
            {
            ret = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)vcIterator);
    return ret;
    }

static AtFrVirtualCircuit VirtualCircuitCreate(AtFrLink self, uint32 dlci)
    {
    AtFrVirtualCircuit virtualCircuit;

    if (VirtualCircuitIsExisting(self, dlci))
        return NULL;

    virtualCircuit = mMethodsGet(self)->VirtualCircuitObjectCreate(self, dlci);
    if (virtualCircuit == NULL)
        return NULL;

    AtListObjectAdd(self->virtualCircuits, (AtObject)virtualCircuit);
    return virtualCircuit;
    }

static eAtModuleFrRet VirtualCircuitDelete(AtFrLink self, uint32 dlci)
    {
    AtFrVirtualCircuit dlciCircuit;
    uint32 listIdx;
    eAtRet ret = cAtErrorObjectNotExist;
    uint32 numVcs = AtListLengthGet(self->virtualCircuits);

    for (listIdx = 0; listIdx < numVcs; listIdx++)
        {
        dlciCircuit = (AtFrVirtualCircuit)AtListObjectGet(self->virtualCircuits, listIdx);

        if (AtChannelIdGet((AtChannel)dlciCircuit) == dlci)
            {
            AtListObjectRemove(self->virtualCircuits, (AtObject)dlciCircuit);
            AtObjectDelete((AtObject)dlciCircuit);
            ret = cAtOk;
            break;
            }
        }

    return ret;
    }

static AtFrVirtualCircuit VirtualCircuitGet(AtFrLink self, uint32 dlci)
    {
    AtFrVirtualCircuit dlciCircuit = NULL;
    AtIterator vcIterator = AtListIteratorCreate(self->virtualCircuits);

    while ((dlciCircuit = (AtFrVirtualCircuit)AtIteratorNext(vcIterator)) != NULL)
        {
        if (AtChannelIdGet((AtChannel)dlciCircuit) == dlci)
            break;
        }

    AtObjectDelete((AtObject)vcIterator);
    return dlciCircuit;
    }

static void AllDlciVirtualCircuitsDelete(AtFrLink self)
    {
    AtObject vcObject;

    if (self->virtualCircuits == NULL)
        return;

    while ((vcObject = AtListObjectRemoveAtIndex(self->virtualCircuits, 0)) != NULL)
        AtObjectDelete((AtObject)vcObject);
    }

static eAtRet Init(AtChannel self)
    {
    AtFrLink link = (AtFrLink)self;
    eAtRet ret = m_AtChannelMethods->Init(self);

    if (ret != cAtOk)
        return ret;

    AllDlciVirtualCircuitsDelete(link);
    if (link->virtualCircuits == NULL)
        link->virtualCircuits = AtListCreate(0);

    return ret;
    }

static void Delete(AtObject self)
    {
    AtFrLink link = (AtFrLink)self;

    AllDlciVirtualCircuitsDelete(link);
    AtObjectDelete((AtObject)link->virtualCircuits);
    link->virtualCircuits = NULL;

    m_AtObjectMethods->Delete(self);
    }

static AtFrVirtualCircuit VirtualCircuitObjectCreate(AtFrLink self, uint32 dlci)
    {
    AtUnused(self);
    AtUnused(dlci);
    return NULL;
    }

static eAtModuleFrRet TxQ922AddressDCSet(AtFrLink self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static uint8 TxQ922AddressDCGet(AtFrLink self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static eAtModuleFrRet TxQ922AddressCRSet(AtFrLink self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static uint8 TxQ922AddressCRGet(AtFrLink self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static eAtModuleFrRet EncapTypeSet(AtFrLink self, eAtFrLinkEncapType encapType)
    {
    AtUnused(self);
    AtUnused(encapType);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eAtFrLinkEncapType EncapTypeGet(AtFrLink self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFrLinkEncapTypeUnknown;
    }

static eAtModuleFrRet TxQ922AddressFECNSet(AtFrLink self, uint8 fecn)
    {
    AtUnused(self);
    AtUnused(fecn);
    return cAtErrorNotImplemented;
    }

static uint8 TxQ922AddressFECNGet(AtFrLink self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleFrRet TxQ922AddressBECNSet(AtFrLink self, uint8 becn)
    {
    AtUnused(self);
    AtUnused(becn);
    return cAtErrorNotImplemented;
    }

static uint8 TxQ922AddressBECNGet(AtFrLink self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleFrRet TxQ922AddressDESet(AtFrLink self, uint8 de)
    {
    AtUnused(self);
    AtUnused(de);
    return cAtErrorNotImplemented;
    }

static uint8 TxQ922AddressDEGet(AtFrLink self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleFrRet TxQ922AddressDlCoreSet(AtFrLink self, uint8 de)
    {
    AtUnused(self);
    AtUnused(de);
    return cAtErrorNotImplemented;
    }

static uint8 TxQ922AddressDlCoreGet(AtFrLink self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtChannel(AtFrLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtFrLink self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtFrLink self)
    {
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtFrLink self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, VirtualCircuitCreate);
        mMethodOverride(m_methods, VirtualCircuitGet);
        mMethodOverride(m_methods, VirtualCircuitDelete);
        mMethodOverride(m_methods, VirtualCircuitObjectCreate);
        mMethodOverride(m_methods, TxQ922AddressDCSet);
        mMethodOverride(m_methods, TxQ922AddressDCGet);
        mMethodOverride(m_methods, TxQ922AddressCRSet);
        mMethodOverride(m_methods, TxQ922AddressCRGet);
        mMethodOverride(m_methods, EncapTypeSet);
        mMethodOverride(m_methods, EncapTypeGet);
        mMethodOverride(m_methods, TxQ922AddressFECNSet);
        mMethodOverride(m_methods, TxQ922AddressFECNGet);
        mMethodOverride(m_methods, TxQ922AddressBECNSet);
        mMethodOverride(m_methods, TxQ922AddressBECNGet);
        mMethodOverride(m_methods, TxQ922AddressDESet);
        mMethodOverride(m_methods, TxQ922AddressDEGet);
        mMethodOverride(m_methods, TxQ922AddressDlCoreSet);
        mMethodOverride(m_methods, TxQ922AddressDlCoreGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtFrLink);
    }

/* To initialize FR link object */
AtFrLink AtFrLinkObjectInit(AtFrLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtHdlcLinkObjectInit((AtHdlcLink) self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtFrLink
 * @{
 */
/**
 * Create Virtual Circuit Channel
 *
 * @param self This link
 * @param dlci DLCI value
 *
 * @return Instance of Virtual Circuit Channel
 */
AtFrVirtualCircuit AtFrLinkVirtualCircuitCreate(AtFrLink self, uint32 dlci)
    {
    mOneParamObjectGet(VirtualCircuitCreate, AtFrVirtualCircuit, dlci);
    }

/**
 * Delete Virtual Circuit Channel
 *
 * @param self This link
 * @param dlci DLCI value
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkVirtualCircuitDelete(AtFrLink self, uint32 dlci)
    {
    mNumericalAttributeSet(VirtualCircuitDelete, dlci);
    }

/**
 * Get Virtual Circuit Channel
 *
 * @param self This link
 * @param dlci DLCI value
 *
 * @return Virtual Circuit Channel
 */
AtFrVirtualCircuit AtFrLinkVirtualCircuitGet(AtFrLink self, uint32 dlci)
    {
    mOneParamObjectGet(VirtualCircuitGet, AtFrVirtualCircuit, dlci);
    }

/**
 * Get the number of virtual circuits created by AtFrLink
 *
 * @param self This link
 *
 * @return number of virtual circuits
 */
uint32 AtFrLinkNumberVirtualCircuitGet(AtFrLink self)
    {
    if (self)
        return AtListLengthGet(self->virtualCircuits);

    return 0;
    }

/**
 * Get the built-in virtual circuit iterator to iterate all of created virtual
 * circuits
 *
 * @param self This link
 *
 * @return Built-in virtual circuit iterator to iterate all of created virtual circuits
 * @note AtIteratorRestart() should be called before iterating
 */
AtIterator AtFrLinkVirtualCircuitIteratorGet(AtFrLink self)
    {
    if (self)
        {
        AtIterator iterator = AtListIteratorGet(self->virtualCircuits);
        AtIteratorRestart(iterator);
        return iterator;
        }

    return NULL;
    }

/**
 * Set DLCI/DL-CORE control indicator of Q922 Address
 *
 * @param self This link
 * @param value 0 will use for DLCI, 1 for DL-CORE.
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkTxQ922AddressDCSet(AtFrLink self, uint8 value)
    {
    mNumericalAttributeSet(TxQ922AddressDCSet, value);
    }

/**
 * Get DLCI/DL-CORE control indicator of Q922 Address
 *
 * @param self This link
 *
 * @return value 0 will use for DLCI, 1 for DL-CORE.
 */
uint8 AtFrLinkTxQ922AddressDCGet(AtFrLink self)
    {
    mAttributeGet(TxQ922AddressDCGet, uint8, cInvalidUint8);
    }

/**
 * Set encapsulation type for FR link
 * 
 * @param self This link
 * @param encapType Encapsulation type. @ref eAtFrLinkEncapType "Frame relay link encapsulation type"
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkEncapTypeSet(AtFrLink self, eAtFrLinkEncapType encapType)
    {
    mNumericalAttributeSet(EncapTypeSet, encapType);
    }

/**
 * Get encapsulation type of FR link
 * 
 * @param self This link
 * 
 * @return Encapsulation type. @ref eAtFrLinkEncapType "Frame relay link encapsulation type"
 */
eAtFrLinkEncapType AtFrLinkEncapTypeGet(AtFrLink self)
    {
    mAttributeGet(EncapTypeGet, eAtFrLinkEncapType, cAtFrLinkEncapTypeUnknown);
    }

/**
 * Set Command/Respond Field of Q922 Address
 *
 * @param self This link
 * @param value C/R Bit
 *              - 0: Command Frame
 *              - 1: Respond Frame
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkTxQ922AddressCRSet(AtFrLink self, uint8 value)
    {
    mNumericalAttributeSet(TxQ922AddressCRSet, value);
    }

/**
 * Get Command/Respond Field of Q922 Address
 *
 * @param self This link
 *
 * @retval 0 for Command Frame
 * @retval 1 for Respond Frame
 */
uint8 AtFrLinkTxQ922AddressCRGet(AtFrLink self)
    {
    mAttributeGet(TxQ922AddressCRGet, uint8, cInvalidUint8);
    }

/**
 * Set FECN bit of Q922 Address at transmit direction
 *
 * @param self This link
 * @param fecn FECN values.
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkTxQ922AddressFECNSet(AtFrLink self, uint8 fecn)
    {
    mNumericalAttributeSet(TxQ922AddressFECNSet, fecn);
    }

/**
 * Get value FECN bit of Q922 Address at transmit direction
 *
 * @param self This link
 *  
 * @return FECN values
 */
uint8 AtFrLinkTxQ922AddressFECNGet(AtFrLink self)
    {
    mAttributeGet(TxQ922AddressFECNGet, uint8, 0);
    }

/**
 * Set BECN bit of Q922 Address at transmit direction
 *
 * @param self This link
 * @param becn BECN values.
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkTxQ922AddressBECNSet(AtFrLink self, uint8 becn)
    {
    mNumericalAttributeSet(TxQ922AddressBECNSet, becn);
    }

/**
 * Get value BECN bit of Q922 Address at transmit direction
 *
 * @param self This link
 *  
 * @return BECN values
 */
uint8 AtFrLinkTxQ922AddressBECNGet(AtFrLink self)
    {
    mAttributeGet(TxQ922AddressBECNGet, uint8, 0);
    }

/**
 * Set DE bit of Q922 Address at transmit direction
 *
 * @param self This link
 * @param de DE values.
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkTxQ922AddressDESet(AtFrLink self, uint8 de)
    {
    mNumericalAttributeSet(TxQ922AddressDESet, de);
    }

/**
 * Get value DE bit of Q922 Address at transmit direction
 *
 * @param self This link
 *  
 * @return DE values
 */
uint8 AtFrLinkTxQ922AddressDEGet(AtFrLink self)
    {
    mAttributeGet(TxQ922AddressDEGet, uint8, 0);
    }

/**
 * Set DL-Core Field of Q922 Address at transmit direction
 *
 * @param self This link
 * @param dlCore DL-Core values.
 *
 * @return AT return code
 */
eAtModuleFrRet AtFrLinkTxQ922AddressDlCoreSet(AtFrLink self, uint8 dlCore)
    {
    mNumericalAttributeSet(TxQ922AddressDlCoreSet, dlCore);
    }

/**
 * Get value DL-Core Field of Q922 Address at transmit direction
 *
 * @param self This link
 *  
 * @return DL-Core values
 */
uint8 AtFrLinkTxQ922AddressDlCoreGet(AtFrLink self)
    {
    mAttributeGet(TxQ922AddressDlCoreGet, uint8, 0);
    }

/**
 * @}
 */

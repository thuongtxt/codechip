/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : FR
 *
 * File        : AtMfrBundle.c
 *
 * Created Date: Aug 10, 2012
 *
 * Description : Frame Relay bundle implementation
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMfrBundleInternal.h"
#include "../encap/AtEncapBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mBundleIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtMfrBundleMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;
static tAtEncapBundleMethods    m_AtEncapBundleOverride;

/* Save supper implementation */
static const tAtObjectMethods   *m_AtObjectMethods = NULL;
static const tAtChannelMethods  *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "mfr";
    }

static eBool CanCreateVirtualCircuit(AtMfrBundle self, uint32 dlci)
    {
    AtFrVirtualCircuit dlciCircuit;
    AtIterator vcIterator = AtListIteratorCreate(self->virtualCircuits);
    eBool ret = cAtTrue;

    while ((dlciCircuit = (AtFrVirtualCircuit)AtIteratorNext(vcIterator)) != NULL)
        {
        if (AtChannelIdGet((AtChannel)dlciCircuit) == dlci)
            {
            ret = cAtFalse;
            break;
            }
        }
    AtObjectDelete((AtObject)vcIterator);

    return ret;
    }

static AtFrVirtualCircuit VirtualCircuitCreate(AtMfrBundle self, uint32 dlci)
    {
    AtFrVirtualCircuit dlciCircuit;

    if (!CanCreateVirtualCircuit(self, dlci))
        return NULL;

    dlciCircuit = mMethodsGet(self)->VirtualCircuitObjectCreate(self, dlci);
    if (dlciCircuit == NULL)
        return NULL;

    AtListObjectAdd(self->virtualCircuits, (AtObject)dlciCircuit);
    return dlciCircuit;
    }

static eAtModuleFrRet VirtualCircuitDelete(AtMfrBundle self, uint32 dlci)
    {
    AtFrVirtualCircuit dlciCircuit;
    eAtRet ret = cAtErrorObjectNotExist;
    uint32 numVcs = AtListLengthGet(self->virtualCircuits);
    uint32 listIdx;

    for (listIdx = 0; listIdx < numVcs; listIdx++)
        {
        dlciCircuit = (AtFrVirtualCircuit)AtListObjectGet(self->virtualCircuits, listIdx);

        if (AtChannelIdGet((AtChannel)dlciCircuit) == dlci)
            {
            AtListObjectRemove(self->virtualCircuits, (AtObject)dlciCircuit);
            AtObjectDelete((AtObject)dlciCircuit);
            ret = cAtOk;
            break;
            }
        }

    return ret;
    }

static AtFrVirtualCircuit VirtualCircuitGet(AtMfrBundle self, uint32 dlci)
    {
    AtFrVirtualCircuit dlciCircuit;
    uint32 listIdx;
    uint32 numVcs = AtListLengthGet(self->virtualCircuits);

    for (listIdx = 0; listIdx < numVcs; listIdx++)
        {
        dlciCircuit = (AtFrVirtualCircuit)AtListObjectGet(self->virtualCircuits, listIdx);

        if (AtChannelIdGet((AtChannel)dlciCircuit) == dlci)
            return dlciCircuit;
        }

    return NULL;
    }

static uint32 TypeGet(AtEncapBundle self)
    {
    AtUnused(self);
    return cAtEncapBundleTypeMfr;
    }

static void AllDlciVirtualCircuitsDelete(AtMfrBundle self)
    {
    AtObject vcObject;

    if (self->virtualCircuits == NULL)
        return;

    while ((vcObject = AtListObjectRemoveAtIndex(self->virtualCircuits, 0)) != NULL)
        AtObjectDelete((AtObject)vcObject);
    }

static eAtRet Init(AtChannel self)
    {
    AtMfrBundle mfr = (AtMfrBundle)self;
    eAtRet ret = m_AtChannelMethods->Init(self);

    if (ret != cAtOk)
        return ret;

    AllDlciVirtualCircuitsDelete(mfr);
    if (mfr->virtualCircuits == NULL)
        mfr->virtualCircuits = AtListCreate(0);

    return ret;
    }

static void Delete(AtObject self)
    {
    AtMfrBundle mfr = (AtMfrBundle)self;

    AllDlciVirtualCircuitsDelete(mfr);
    AtObjectDelete((AtObject)mfr->virtualCircuits);
    mfr->virtualCircuits = NULL;

    m_AtObjectMethods->Delete(self);
    }

static AtFrVirtualCircuit VirtualCircuitObjectCreate(AtMfrBundle self, uint32 dlci)
    {
    AtUnused(self);
    AtUnused(dlci);
    return NULL;
    }

static eAtRet FragmentFormatSet(AtMfrBundle self, eAtMfrBundleFragmentFormat format)
    {
    AtUnused(self);
    AtUnused(format);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eAtMfrBundleFragmentFormat FragmentFormatGet(AtMfrBundle self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtMfrBundleFragmentFormatUnknown;
    }

static void OverrideAtObject(AtMfrBundle self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtMfrBundle self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapBundle(AtMfrBundle self)
    {
    AtEncapBundle bundle = (AtEncapBundle) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapBundleOverride, mMethodsGet(bundle), sizeof(m_AtEncapBundleOverride));

        mMethodOverride(m_AtEncapBundleOverride, TypeGet);
        }

    mMethodsSet(bundle, &m_AtEncapBundleOverride);
    }

static void Override(AtMfrBundle self)
    {
    OverrideAtChannel(self);
    OverrideAtEncapBundle(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtMfrBundle self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, VirtualCircuitCreate);
        mMethodOverride(m_methods, VirtualCircuitDelete);
        mMethodOverride(m_methods, VirtualCircuitGet);
        mMethodOverride(m_methods, VirtualCircuitObjectCreate);
        mMethodOverride(m_methods, FragmentFormatSet);
        mMethodOverride(m_methods, FragmentFormatGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtMfrBundle);
    }

AtMfrBundle AtMfrBundleObjectInit(AtMfrBundle self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtHdlcBundleObjectInit((AtHdlcBundle)self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtMfrBundle
 * @{
 */

/**
 * Create Virtual Circuit Channel
 *
 * @param self This bundle
 * @param dlci DLCI value
 *
 * @return Instance of Virtual Circuit Channel
 */
AtFrVirtualCircuit AtMfrBundleVirtualCircuitCreate(AtMfrBundle self, uint32 dlci)
    {
    mOneParamObjectGet(VirtualCircuitCreate, AtFrVirtualCircuit, dlci);
    }

/**
 * Delete Virtual Circuit Channel
 *
 * @param self This link
 * @param dlci DLCI value
 *
 * @return AT return code
 */
eAtModuleFrRet AtMfrBundleVirtualCircuitDelete(AtMfrBundle self, uint32 dlci)
    {
    mNumericalAttributeSet(VirtualCircuitDelete, dlci);
    }

/**
 * Get Virtual Circuit Channel
 *
 * @param self This bundle
 * @param dlci DLCI value
 *
 * @return Virtual Circuit Channel
 */
AtFrVirtualCircuit AtMfrBundleVirtualCircuitGet(AtMfrBundle self, uint32 dlci)
    {
    mOneParamObjectGet(VirtualCircuitGet, AtFrVirtualCircuit, dlci);
    }

/**
 * Get the number of virtual circuits created by bundle
 *
 * @param self This bundle
 *
 * @return number of virtual circuits
 */
uint32 AtMfrBundleNumberVirtualCircuitGet(AtMfrBundle self)
    {
    if (self)
        return AtListLengthGet(self->virtualCircuits);

    return 0;
    }

/**
 * Get the built-in virtual circuit iterator to iterate all of created virtual
 * circuits
 *
 * @param self This bundle
 *
 * @return Built-in virtual circuit iterator to iterate all of created virtual circuits
 * @note AtIteratorRestart() should be called before iterating
 */
AtIterator AtMfrBundleVirtualCircuitIteratorGet(AtMfrBundle self)
    {
    if(self)
        {
        AtIterator iterator = AtListIteratorGet(self->virtualCircuits);
        AtIteratorRestart(iterator);
        return iterator;
        }

    return NULL;
    }

/**
 * Set MFR fragmentation format
 * 
 * @param self This bundle
 * @param format Fragmentation format. @ref eAtMfrBundleFragmentFormat "MFR Fragmentation format"
 * 
 * @@return AT return code
 */
eAtRet AtMfrBundleFragmentFormatSet(AtMfrBundle self, eAtMfrBundleFragmentFormat format)
    {
    mNumericalAttributeSet(FragmentFormatSet, format);
    }

/**
 * Get MFR fragmentation format
 * 
 * @param self This bundle
 * 
 * @return Fragmentation format. @ref eAtMfrBundleFragmentFormat "MFR Fragmentation format"
 */
eAtMfrBundleFragmentFormat AtMfrBundleFragmentFormatGet(AtMfrBundle self)
    {
    mAttributeGet(FragmentFormatGet, eAtMfrBundleFragmentFormat, cAtMfrBundleFragmentFormatUnknown);
    }

/**
 * @}
 */

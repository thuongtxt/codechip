/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtBerControllerInternal.h
 * 
 * Created Date: Feb 6, 2013
 *
 * Description : BER abstract controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBERCONTROLLERINTERNAL_H_
#define _ATBERCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleBer.h"
#include "AtBerController.h"

#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtBerControllerMethods
    {
    eAtRet (*Init)(AtBerController self);
    eAtRet (*Enable)(AtBerController self, eBool enable);
    eBool (*IsEnabled)(AtBerController self);
    eAtRet (*SdThresholdSet)(AtBerController self, eAtBerRate threshold);
    eAtBerRate (*SdThresholdGet)(AtBerController self);
    eAtRet (*SfThresholdSet)(AtBerController self, eAtBerRate threshold);
    eAtBerRate (*SfThresholdGet)(AtBerController self);
    eAtRet (*TcaThresholdSet)(AtBerController self, eAtBerRate threshold);
    eAtBerRate (*TcaThresholdGet)(AtBerController self);
    eAtBerRate (*CurBerGet)(AtBerController self);

    eBool (*IsSd)(AtBerController self);
    eBool (*IsSf)(AtBerController self);
    eBool (*IsTca)(AtBerController self);

    eBool (*SdHistoryGet)(AtBerController self, eBool clear);
    eBool (*SfHistoryGet)(AtBerController self, eBool clear);
    eBool (*TcaHistoryGet)(AtBerController self, eBool clear);

    /* Interrupt handling */
    eAtRet (*InterruptMaskSet)(AtBerController self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtBerController self);
    uint32 (*InterruptProcess)(AtBerController self, uint32 isrContext);
    uint32 (*DefectGet)(AtBerController self);
    uint32 (*DefectHistoryGet)(AtBerController self);
    uint32 (*DefectHistoryClear)(AtBerController self);

    void (*DebugOn)(AtBerController self, eBool debugOn);
    eBool (*DebugIsOn)(AtBerController self);

    /* To update hw when changing rate */
    eAtRet (*HwChannelTypeUpdate)(AtBerController self);

    /* Util to check Detection/Clearing time is pass or failed */
    uint32 (*ExpectedDetectionTimeInMs)(AtBerController self, eAtBerRate rate);
    uint32 (*ExpectedClearingTimeInMs)(AtBerController self, eAtBerRate rate);
    uint32 (*MeasureEngineId)(AtBerController self);
    uint8  (*PartId)(AtBerController self);

    }tAtBerControllerMethods;

/* BER controller abstract class representation */
typedef struct tAtBerController
    {
    tAtObject super;
    const tAtBerControllerMethods *methods;

    uint32 controllerId;
    AtModuleBer berModule;

    /* Channel needs to be monitored BER */
    AtChannel monitoredChannel;

    uint8 debugOn;
    }tAtBerController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController AtBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

uint32 AtBerControllerRead(AtBerController self, uint32 address);
void AtBerControllerWrite(AtBerController self, uint32 address, uint32 value);
uint16 AtBerControllerLongRead(AtBerController self, uint32 address, uint32* regVal);
uint16 AtBerControllerLongWrite(AtBerController self, uint32 address, const uint32* regVal);
eAtRet AtBerControllerHwChannelTypeUpdate(AtBerController self);

eAtRet AtBerControllerInterruptMaskSet(AtBerController self, uint32 defectMask, uint32 enableMask);
uint32 AtBerControllerInterruptMaskGet(AtBerController self);
uint32 AtBerControllerInterruptProcess(AtBerController self, uint32 isrContext);

uint32 AtBerControllerDefectGet(AtBerController self);
uint32 AtBerControllerDefectHistoryGet(AtBerController self);
uint32 AtBerControllerDefectHistoryClear(AtBerController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATBERCONTROLLERINTERNAL_H_ */


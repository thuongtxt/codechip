/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtSdhPathBerSoftController.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : Concrete BER controller for SDH Path
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "AtSdhPath.h"
#include "AtSdhPathBerSoftControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtBerSoftControllerMethods m_AtBerSoftControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhPathBerSoftController);
    }

static uint32 SdDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    return cAtSdhPathAlarmBerSd;
    }

static uint32 SfDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    return cAtSdhPathAlarmBerSf;
    }

static eAtBerChnType ChannelType(AtBerSoftController self)
    {
    eAtSdhChannelType channelType;

    channelType = AtSdhChannelTypeGet((AtSdhChannel)AtBerControllerMonitoredChannel((AtBerController)self));
    if (channelType == cAtSdhChannelTypeVc4_4c) return cAtBerPathVc44c;
    if (channelType == cAtSdhChannelTypeVc4)    return cAtBerPathVc4;
    if (channelType == cAtSdhChannelTypeVc3)    return cAtBerPathVc3;
    if (channelType == cAtSdhChannelTypeVc12)   return cAtBerPathVc12;
    if (channelType == cAtSdhChannelTypeVc11)   return cAtBerPathVc11;

    return cAtBerMaxChnType;
    }

static uint32 ChannelBitErrorGet(AtBerSoftController self)
    {
    AtChannel channel = AtBerControllerMonitoredChannel((AtBerController)self);
    return AtChannelCounterClear(channel, cAtSdhPathCounterTypeBip);
    }

static void OverrideAtBerSoftController(AtBerController self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerSoftControllerOverride, mMethodsGet(softController), sizeof(m_AtBerSoftControllerOverride));

        mMethodOverride(m_AtBerSoftControllerOverride, ChannelBitErrorGet);
        mMethodOverride(m_AtBerSoftControllerOverride, ChannelType);
        mMethodOverride(m_AtBerSoftControllerOverride, SdDefectMask);
        mMethodOverride(m_AtBerSoftControllerOverride, SfDefectMask);
        }

    mMethodsSet(softController, &m_AtBerSoftControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerSoftController(self);
    }

AtBerController AtSdhPathBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerSoftControllerObjectInit((AtBerSoftController)self, controllerId, channel, berModule) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController AtSdhPathBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return AtSdhPathBerSoftControllerObjectInit(newController, controllerId, channel, berModule);
    }

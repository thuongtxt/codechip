/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtModuleBerOsIntf.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : BER OS interface to work with common software BER engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "atber/atber.h"
#include "../common/AtObjectInternal.h"
#include "AtModuleSoftBerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtBerOsIntf m_OsIntf;
static uint8 m_osIntfInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void* SemCreate(void)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtOsalSem sem = mMethodsGet(osal)->SemCreate(osal, cAtFalse, 1);
    return sem;
    }

/* Function to delete semaphore */
static int SemDelete(void *locker)
    {
    AtOsalSem sem = (AtOsalSem)locker;
    if (mMethodsGet(sem)->Delete(sem) == cAtOsalOk)
        return 1;

    return 0;
    }

/* Function to lock semaphore */
static int SemLock(void *locker)
    {
    AtOsalSem sem = (AtOsalSem)locker;
    if(mMethodsGet(sem)->Take(sem) == cAtOsalOk)
        return 1;

    return 0;
    }

/* Function to unlock semaphore */
static int SemUnlock(void *locker)
    {
    AtOsalSem sem = (AtOsalSem)locker;
    if(mMethodsGet(sem)->Give(sem) == cAtOsalOk)
        return 1;

    return 0;
    }

/* Function to get system time (ms unit) */
static uint32 GetTime(void)
    {
    tAtOsalCurTime currentTime;
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->CurTimeGet(osal, &currentTime);
    return (currentTime.sec * 1000) + (currentTime.usec / 1000);
    }

static void *_malloc(uint32 msize)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    return mMethodsGet(osal)->MemAlloc(osal, msize);
    }

static void *_memset(void *_mem, int val, uint32 msize)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    return mMethodsGet(osal)->MemInit(osal, _mem, (uint32)val, msize);
    }

static void *_memcpy(void *_mdest, const void *_msrc, uint32 msize)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    return mMethodsGet(osal)->MemCpy(osal, _mdest, _msrc, msize);
    }

static void _free(void *_mem)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, _mem);
    }

const tAtBerOsIntf *AtModuleBerDefaultOsIntf(void)
    {
    if (!m_osIntfInit)
        {
        m_OsIntf._create  = SemCreate;
        m_OsIntf._delete  = SemDelete;
        m_OsIntf.lock     = SemLock;
        m_OsIntf.unlock   = SemUnlock;
        m_OsIntf.gettime  = GetTime;
        m_OsIntf._malloc  = _malloc;
        m_OsIntf._memset  = _memset;
        m_OsIntf._memcpy  = _memcpy;
        m_OsIntf._free    = _free;

        /* Just setup one time */
        m_osIntfInit     = 1;
        }

    return &m_OsIntf;

    }

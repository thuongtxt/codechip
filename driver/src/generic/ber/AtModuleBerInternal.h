/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtModuleBerInternal.h
 *
 * Created Date: Aug 31, 2012
 *
 * Author      : huandh
 *
 * Description : BER monitoring abstract module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEBERINTERNAL_H_
#define _ATMODULEBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#include "AtModuleBer.h"
#include "AtBerMeasureTimeEngine.h"
#include "AtBerMeasureTimeEngineInternal.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of module BER */
typedef struct tAtModuleBerMethods
    {
    uint32 (*PeriodProcess)(AtModuleBer self, uint32 periodInMs);
    void (*ControllerDelete)(AtModuleBer self, AtBerController controller);

    /* BER controller factory */
    AtBerController (*SdhLineRsBerControlerCreate)(AtModuleBer self, AtChannel line);
    AtBerController (*SdhLineMsBerControlerCreate)(AtModuleBer self, AtChannel line);
    AtBerController (*SdhPathBerControlerCreate)(AtModuleBer self, AtChannel path);
    AtBerController (*PdhChannelPathBerControlerCreate)(AtModuleBer self, AtChannel pdhChannel);
    AtBerController (*PdhChannelLineBerControlerCreate)(AtModuleBer self, AtChannel pdhChannel);

    /* Internal methods */
    AtBerController (*SdhLineRsBerControlerObjectCreate)(AtModuleBer self, uint32 controllerId, AtChannel line);
    AtBerController (*SdhLineMsBerControlerObjectCreate)(AtModuleBer self, uint32 controllerId, AtChannel line);
    AtBerController (*SdhPathBerControlerObjectCreate)(AtModuleBer self, uint32 controllerId, AtChannel path);
    uint32 (*BaseAddress)(AtModuleBer self);
    eBool (*ChannelBerShouldBeEnabledByDefault)(AtModuleBer self);

    AtBerMeasureTimeEngine (*MeasureTimeSoftEngineCreate)(AtModuleBer self, AtBerController controller);
    AtBerMeasureTimeEngine (*MeasureTimeHardEngineCreate)(AtModuleBer self, AtBerController controller);
    AtBerMeasureTimeEngine (*MeasureTimeHardEngineObjectCreate)(AtModuleBer self, AtBerController controller);
    AtBerMeasureTimeEngine (*MeasureTimeSoftEngineObjectCreate)(AtModuleBer self, AtBerController controller);
    eBool (*MeasureTimeEngineIsSupported)(AtModuleBer self);

    }tAtModuleBerMethods;

/* Structure of class AtModuleBer */
typedef struct tAtModuleBer
    {
    tAtModule super;
    const tAtModuleBerMethods *methods;

    /* Cached Data */
    AtBerMeasureTimeEngine engine;

    } tAtModuleBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ---------	-------------------------------*/
AtModuleBer AtModuleBerObjectInit(AtModuleBer self, AtDevice device);
uint32 AtModuleBerBaseAddress(AtModuleBer self);
eBool AtModuleBerChannelBerShouldBeEnabledByDefault(AtModuleBer self);

/* To cache BER-Measure-Engine */
eAtRet AtModuleBerMeasureTimeEngineSet(AtModuleBer self, AtBerMeasureTimeEngine engine);
eAtRet AtModuleBerMeasureTimeEngineDeleteNoLock(AtModuleBer self, AtBerMeasureTimeEngine engine);
eBool AtModuleBerMeasureTimeEngineIsSupported(AtModuleBer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEBERINTERNAL_H_ */

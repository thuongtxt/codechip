/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtSdhLineMsBerSoftController.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : BER controller for SDH Line MS layer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLineBerSoftControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerSoftControllerMethods m_AtBerSoftControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelBitErrorGet(AtBerSoftController self)
    {
    AtChannel line = AtBerControllerMonitoredChannel((AtBerController)self);
    return AtChannelCounterClear(line, cAtSdhLineCounterTypeB2);
    }

static eAtBerChnType ChannelType(AtBerSoftController self)
    {
    eAtSdhLineRate lineRate = AtSdhLineRateGet((AtSdhLine)AtBerControllerMonitoredChannel((AtBerController)self));

    if (lineRate == cAtSdhLineRateStm1)  return cAtBerLineStm1;
    if (lineRate == cAtSdhLineRateStm4)  return cAtBerLineStm4;
    if (lineRate == cAtSdhLineRateStm16) return cAtBerLineStm16;

    return cAtBerMaxChnType;
    }

static void OverrideAtBerSoftController(AtBerController self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerSoftControllerOverride, mMethodsGet(softController), sizeof(m_AtBerSoftControllerOverride));

        mMethodOverride(m_AtBerSoftControllerOverride, ChannelType);
        mMethodOverride(m_AtBerSoftControllerOverride, ChannelBitErrorGet);
        }

    mMethodsSet(softController, &m_AtBerSoftControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerSoftController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhLineMsBerSoftController);
    }

AtBerController AtSdhLineMsBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhLineBerSoftControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController AtSdhLineMsBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return AtSdhLineMsBerSoftControllerObjectInit(newController, controllerId, channel, berModule);
    }

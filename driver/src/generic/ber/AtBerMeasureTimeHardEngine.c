/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtBerMeasureTimeEngineHard.c
 *
 * Created Date: Nov 13, 2017
 *
 * Description :  BER-Detection and clearing time Measurement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtBerMeasureTimeEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerMeasureTimeEngineMethods m_AtBerMeasureOverride;

/* Save super implementation */
static const tAtBerMeasureTimeEngineMethods *m_AtBerMeasureMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet AllEstimatedTimeClear(AtBerMeasureTimeEngine self)
    {
    AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeSfDetection);
    AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeSfClearing);
    AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeSdClearing);
    AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeTcaClearing);
    return cAtOk;
    }

static eAtRet Init(AtBerMeasureTimeEngine self)
    {
    AtBerMeasureTimeEngineInputErrorDisConnect(self);
    AllEstimatedTimeClear(self);
    return cAtOk;
    }

static uint32 WaitErrorStartingProcess(AtBerMeasureTimeEngine self)
    {
    AllEstimatedTimeClear(self);
    AtBerMeasureTimeEngineInputErrorConnect(self);
    AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitSfDetectionTime);
    return 0;
    }

static uint32 WaitSfDetectionTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (AtBerControllerIsSf(controller))
        {
        uint32 timeInMs = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeSfDetection);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeSfDetection, timeInMs);
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitErrorStop);
        }

    return 0;
    }

static uint32 WaitErrorStopProcess(AtBerMeasureTimeEngine self)
    {
  	AtBerMeasureTimeEngineInputErrorDisConnect(self);
    AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitSfClearingTime);
    return 0;
    }

static uint32 WaitSfClearingTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (!AtBerControllerIsSf(controller))
        {
        uint32 timeInMs = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeSfClearing);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeSfClearing, timeInMs);
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitSdClearingTime);
        }
    return 0;
    }

static uint32 WaitSdClearingTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (!AtBerControllerIsSd(controller))
        {
        uint32 timeInMs = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeSdClearing);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeSdClearing, timeInMs);
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitTcaClearingTime);
        }
    return 0;
    }

static uint32 WaitTcaClearingTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (!AtBerControllerIsTca(controller))
        {
        uint32 timeInMs = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeTcaClearing);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeTcaClearing, timeInMs);

        if (AtBerMeasureTimeEngineIsContinousMode(self))
            {
            if (AtBerMeasureTimeEngineIsAutoStopMode(self) && !AtBerMeasureTimeEngineEstimatedTimeIsPassed(self))
                AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateStopInContinous);
            else
                AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitErrorStart);
            }
        else
            AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateOneShotDone);
        }
    return 0;
    }

static uint32 PeriodProcess(AtBerMeasureTimeEngine self)
    {
    eAtBerMeasureState state = AtBerMeasureTimeEngineStateGet(self);
    switch ((uint32) state)
        {
        case cAtBerMeasureStateWaitErrorStart:
            return WaitErrorStartingProcess(self);
        case cAtBerMeasureStateWaitSfDetectionTime:
            return WaitSfDetectionTimeProcess(self);
        case cAtBerMeasureStateWaitErrorStop:
            return WaitErrorStopProcess(self);
        case cAtBerMeasureStateWaitSfClearingTime:
            return WaitSfClearingTimeProcess(self);
        case cAtBerMeasureStateWaitSdClearingTime:
            return WaitSdClearingTimeProcess(self);
        case cAtBerMeasureStateWaitTcaClearingTime:
            return WaitTcaClearingTimeProcess(self);
        default:
            return 0;
        }
    }

static eBool ModeIsSupported(eAtBerMeasureMode mode)
    {
    if (mode == cAtBerMeasureModeContinousNoneStop)
        return cAtTrue;

    if (mode == cAtBerMeasureModeContinousAutoStop)
        return cAtTrue;

    if (mode == cAtBerMeasureModeOneShot)
        return cAtTrue;

    if (mode == cAtBerMeasureModeManaual)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ModeSet(AtBerMeasureTimeEngine self, eAtBerMeasureMode mode)
    {
    if (!ModeIsSupported(mode))
        return cAtErrorModeNotSupport;

    /* Disable Polling when manual mode */
    if (mode == cAtBerMeasureModeManaual)
        {
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitToLachInManual);
        AtBerMeasureTimeEnginePollingStop(self);
        }

    return m_AtBerMeasureMethods->ModeSet(self, mode);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtBerMeasureTimeHardEngine);
    }

static void OverrideAtBerMeasureTimeEngine(AtBerMeasureTimeEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerMeasureMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerMeasureOverride, m_AtBerMeasureMethods, sizeof(m_AtBerMeasureOverride));

        mMethodOverride(m_AtBerMeasureOverride, Init);
        mMethodOverride(m_AtBerMeasureOverride, PeriodProcess);
        mMethodOverride(m_AtBerMeasureOverride, ModeSet);
        }

    mMethodsSet(self, &m_AtBerMeasureOverride);
    }

AtBerMeasureTimeEngine AtBerMeasureTimeHardEngineObjectInit(AtBerMeasureTimeEngine self, AtBerController controller)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerMeasureTimeEngineObjectInit(self, controller) == NULL)
        return NULL;

    /* Setup methods */
    OverrideAtBerMeasureTimeEngine(self);
    m_methodsInit = 1;

    return self;
    }

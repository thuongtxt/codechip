/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtBerSoftController.c
 *
 * Created Date: Feb 7, 2013
 *
 * Description : BER controller - Software solution
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atber/atber.h"
#include "AtModuleSoftBerInternal.h"
#include "AtBerSoftControllerInternal.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtBerSoftControllerMethods m_methods;

/* Override */
static tAtBerControllerMethods m_AtBerControllerOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtBerControllerMethods *m_AtBerControllerMethods = NULL;
static const tAtObjectMethods        *m_AtObjectMethods        = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtBerSoftController);
    }

static uint32 ChannelBitErrorGet(AtBerSoftController self)
    {
	AtUnused(self);
    return 0x0;
    }

/* Get error counter. This function break encapsulation for the sake of reducing
 * context switching for BER monitoring task just to save CPU usage */
static eAtBerRet EngineBitErrorGet(AtBerEngine engine, uint16 chid, uint32 *bitErrCount, void *pUsrData)
    {
    AtBerSoftController controller = (AtBerSoftController)pUsrData;
    eBool isDebugOn = mMethodsGet((AtBerController)controller)->DebugIsOn((AtBerController)controller);
    tAtOsalCurTime startTime;
    AtUnused(engine);
    AtUnused(chid);

    mMethodsGet(controller->channelModule)->Lock(controller->channelModule);

    if (isDebugOn)
        AtOsalCurTimeGet(&startTime);

    *bitErrCount = mMethodsGet(controller)->ChannelBitErrorGet(controller);
    if (isDebugOn)
        {
        tAtOsalCurTime currentTime;
        AtOsalCurTimeGet(&currentTime);
        AtPrintc(cSevNormal,
                 "Counter value: %u, elapse time: %u (us), period %u (ms)\r\n",
                 *bitErrCount,
                 mTimeIntervalInUsGet(startTime, currentTime),
                 mTimeIntervalInMsGet(controller->previousPollTime, currentTime));
        AtOsalMemCpy(&controller->previousPollTime, &currentTime, sizeof(tAtOsalCurTime));
        }

    mMethodsGet(controller->channelModule)->UnLock(controller->channelModule);

    return cAtBerOk;
    }

static AtBerBitErrGet BitErrorGetFunc(AtBerSoftController self)
    {
	AtUnused(self);
    return EngineBitErrorGet;
    }

static eAtBerChnType ChannelType(AtBerSoftController self)
    {
	AtUnused(self);
    /* Let concrete class does */
    return cAtBerMaxChnType;
    }

static void DefectChanged(AtBerSoftController self, uint32 changedAlarms, uint32 currentStatus)
    {
    AtChannel channel = AtBerControllerMonitoredChannel((AtBerController)self);
    AtChannelAllAlarmListenersCall(channel, changedAlarms, currentStatus);
    }

static uint32 BerCurrentDefectGet(AtBerSoftController self)
    {
    uint32 alarm;

    AtModuleLock(self->channelModule);
    alarm = AtChannelDefectGet(AtBerControllerMonitoredChannel((AtBerController)self));
    AtModuleUnLock(self->channelModule);

    return alarm;
    }

static eAtBerRet BerStatusChanged(AtBerEngine engine,
                                  uint16 chid,
                                  atbool excessive,
                                  atbool degrade,
                                  void *pUsrData)
    {
    uint32 changedAlarms = 0;
    uint32 currentStatus = 0;
    atbool oldStatus;
    AtBerSoftController controller = (AtBerSoftController)pUsrData;
    uint32 sdMask = mMethodsGet(controller)->SdDefectMask(controller);
    uint32 sfMask = mMethodsGet(controller)->SfDefectMask(controller);
    AtUnused(engine);
    AtUnused(chid);

    /* Get history of alarm */
    currentStatus = mMethodsGet(controller)->BerCurrentDefectGet(controller);

    /* SF changed */
    oldStatus = (currentStatus & sfMask) ? cAtTrue : cAtFalse;
    if (excessive != oldStatus)
        {
        changedAlarms |= sfMask;
        if (excessive)
            currentStatus |= sfMask;
        else
            currentStatus &= ~sfMask;

        controller->excChanged = cAtTrue;
        }

    /* SD changed */
    oldStatus = (currentStatus & sdMask) ? cAtTrue : cAtFalse;
    if (degrade != oldStatus)
        {
        changedAlarms |= sdMask;
        if (degrade)
            currentStatus |= sdMask;
        else
            currentStatus &= ~sdMask;

        controller->degChanged = cAtTrue;
        }

    /* Notify */
    mMethodsGet(controller)->DefectChanged(controller, changedAlarms, currentStatus);

    return cAtBerOk;
    }

static AtBerStatusChange BerStatusChangedFunc(AtBerSoftController self)
    {
	AtUnused(self);
    return BerStatusChanged;
    }

static uint32 SdDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint32 SfDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet BerEngineCreate(AtBerController self)
    {
    AtBerModule berHandle;
    AtBerEngine berEngine;
    AtBerSoftController softController = (AtBerSoftController)self;

    /* And create a new one */
    berHandle = AtModuleBerSwBerHandleGet((self)->berModule);
    if (berHandle == NULL)
        return cAtErrorRsrcNoAvail;
    berEngine = AtBerEngineCreate(berHandle,
                                  (uint16)AtBerControllerIdGet(self),
                                  mMethodsGet(softController)->ChannelType(softController),
                                  cAtBer10e3,
                                  cAtBer10e4,
                                  mMethodsGet(softController)->BerStatusChangedFunc(softController),
                                  mMethodsGet(softController)->BitErrorGetFunc(softController),
                                  (void*)self);
    if (berEngine == NULL)
        return cAtErrorRsrcNoAvail;

    /* Disable this engine as default to save CPU usage, let application enable */
    if (AtBerEngineEnable(berEngine, cAtFalse) != cAtBerOk)
        return cAtErrorBerEngineFail;

    /* Cache this engine */
    softController->berEngine = berEngine;
    return cAtOk;
    }

static eAtRet Init(AtBerController self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    if (softController->berEngine == NULL)
        return cAtOk;

    /* If BER engine exist, destroy it and re-create */
    AtBerEngineDelete(softController->berEngine);

    return BerEngineCreate(self);
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    /* Create engine if it has not been */
    if (softController->berEngine == NULL)
        {
        if (BerEngineCreate(self) != cAtOk)
            return cAtError;

        /* Apply thresholds that application configure before */
        if (softController->sdThreshold != cAtBerRateUnknown)
            AtBerControllerSdThresholdSet(self, softController->sdThreshold);
        if (softController->sfThreshold != cAtBerRateUnknown)
            AtBerControllerSfThresholdSet(self, softController->sfThreshold);

        /* Already apply, just mark database fields to invalid */
        softController->sdThreshold = cAtBerRateUnknown;
        softController->sfThreshold = cAtBerRateUnknown;
        }

    /* Enable/disable engine */
    if (AtBerEngineEnable(((AtBerSoftController)self)->berEngine, enable) != cAtBerOk)
        return cAtError;

    return cAtOk;
    }

static eBool IsEnabled(AtBerController self)
    {
    return AtBerEngineEnableGet(((AtBerSoftController)self)->berEngine);
    }

static eAtBerRate BerRateUtil2At(eAtUtilBerRate utilRate)
    {
    if (utilRate == cAtBer10e3)  return cAtBerRate1E3;
    if (utilRate == cAtBer10e4)  return cAtBerRate1E4;
    if (utilRate == cAtBer10e5)  return cAtBerRate1E5;
    if (utilRate == cAtBer10e6)  return cAtBerRate1E6;
    if (utilRate == cAtBer10e7)  return cAtBerRate1E7;
    if (utilRate == cAtBer10e8)  return cAtBerRate1E8;
    if (utilRate == cAtBer10e9)  return cAtBerRate1E9;
    if (utilRate == cAtBer10e10) return cAtBerRate1E10;

    return cAtBerRateUnknown;
    }

static eAtUtilBerRate BerRateAt2Util(eAtBerRate rate)
    {
    if (rate == cAtBerRate1E3) return cAtBer10e3;
    if (rate == cAtBerRate1E4) return cAtBer10e4;
    if (rate == cAtBerRate1E5) return cAtBer10e5;
    if (rate == cAtBerRate1E6) return cAtBer10e6;
    if (rate == cAtBerRate1E7) return cAtBer10e7;
    if (rate == cAtBerRate1E8) return cAtBer10e8;
    if (rate == cAtBerRate1E9) return cAtBer10e9;

    return cAtBerMaxRates;
    }

static eAtRet SdThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    eAtBerRet berRet;
    AtBerSoftController softController = (AtBerSoftController)self;
    eAtUtilBerRate degThres;

    /* If engine has not been created, just cache information */
    if (softController->berEngine == NULL)
        {
        softController->sdThreshold = (uint8)threshold;
        return cAtOk;
        }

    /* Configure BER engine */
    degThres = BerRateAt2Util(threshold);
    berRet = AtBerEngineThresSet(softController->berEngine, NULL, &degThres);
    if (berRet != cAtBerOk)
        return cAtError;

    return cAtOk;
    }

static eAtBerRate SdThresholdGet(AtBerController self)
    {
    eAtUtilBerRate excThres, degThres;
    eAtBerRet berRet;
    AtBerSoftController softController = (AtBerSoftController)self;

    if (softController->berEngine == NULL)
        return softController->sdThreshold;

    berRet = AtBerEngineThresGet(softController->berEngine, &excThres, &degThres);
    if (berRet != cAtBerOk)
        return cAtBerRateUnknown;

    return BerRateUtil2At(degThres);
    }

static eAtRet SfThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    eAtBerRet berRet;
    AtBerSoftController softController = (AtBerSoftController)self;
    eAtUtilBerRate excThres;

    if (softController->berEngine == NULL)
        {
        softController->sfThreshold = (uint8)threshold;
        return cAtOk;
        }

    excThres = BerRateAt2Util(threshold);
    berRet = AtBerEngineThresSet(softController->berEngine, &excThres, NULL);
    if (berRet != cAtBerOk)
        return cAtError;

    return cAtOk;
    }

static eAtBerRate SfThresholdGet(AtBerController self)
    {
    eAtUtilBerRate excThres, degThres;
    eAtBerRet berRet;
    AtBerSoftController softController = (AtBerSoftController)self;

    if (softController->berEngine == NULL)
        return softController->sfThreshold;

    berRet  = AtBerEngineThresGet(softController->berEngine, &excThres, &degThres);
    if (berRet != cAtBerOk)
        return cAtBerRateUnknown;

    return BerRateUtil2At(excThres);
    }

static eAtBerRate CurBerGet(AtBerController self)
    {
    eAtUtilBerRate currentBer;
    AtBerSoftController softController = (AtBerSoftController)self;
    eAtBerRet berRet = AtBerEngineStatusGet(softController->berEngine, NULL, NULL, &currentBer);

    return (berRet == cAtBerOk) ? BerRateUtil2At(currentBer) : cAtBerRateUnknown;
    }

static eBool IsSd(AtBerController self)
    {
    atbool isSd;
    AtBerSoftController softController = (AtBerSoftController)self;
    eAtBerRet berRet = AtBerEngineStatusGet(softController->berEngine, NULL, &isSd, NULL);

    return (eBool)((berRet == cAtBerOk) ? isSd : cAtFalse);
    }

static eBool IsSf(AtBerController self)
    {
    atbool isSf;
    AtBerSoftController softController = (AtBerSoftController)self;
    eAtBerRet berRet = AtBerEngineStatusGet(softController->berEngine, &isSf, NULL, NULL);

    return (eBool)((berRet == cAtBerOk) ? isSf : cAtFalse);
    }

static eBool SdHistoryGet(AtBerController self, eBool clear)
    {
    AtBerSoftController softController = (AtBerSoftController)self;
    eBool sdHistory = softController->degChanged;

    if (clear)
        softController->degChanged = cAtFalse;

    return sdHistory;
    }

static eBool SfHistoryGet(AtBerController self, eBool clear)
    {
    AtBerSoftController softController = (AtBerSoftController)self;
    eBool sfHistory = softController->excChanged;

    if (clear)
        softController->excChanged = cAtFalse;

    return sfHistory;
    }

static void DebugOn(AtBerController self, eBool debugOn)
    {
    AtBerSoftController controller = (AtBerSoftController)self;
    AtBerEngineDebugEnable(controller->berEngine, debugOn);
    m_AtBerControllerMethods->DebugOn(self, debugOn);
    }

static void Delete(AtObject self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    /* Delete internal SW engine */
    AtBerEngineDelete(softController->berEngine);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtBerController(AtBerSoftController self)
    {
    AtBerController controller = (AtBerController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerControllerOverride, m_AtBerControllerMethods, sizeof(m_AtBerControllerOverride));

        mMethodOverride(m_AtBerControllerOverride, Init);
        mMethodOverride(m_AtBerControllerOverride, Enable);
        mMethodOverride(m_AtBerControllerOverride, IsEnabled);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SdThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdSet);
        mMethodOverride(m_AtBerControllerOverride, SfThresholdGet);
        mMethodOverride(m_AtBerControllerOverride, CurBerGet);
        mMethodOverride(m_AtBerControllerOverride, IsSd);
        mMethodOverride(m_AtBerControllerOverride, IsSf);
        mMethodOverride(m_AtBerControllerOverride, SdHistoryGet);
        mMethodOverride(m_AtBerControllerOverride, SfHistoryGet);
        mMethodOverride(m_AtBerControllerOverride, DebugOn);
        }

    mMethodsSet(controller, &m_AtBerControllerOverride);
    }

static void OverrideAtObject(AtBerSoftController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtBerSoftController self)
    {
    OverrideAtBerController(self);
    OverrideAtObject(self);
    }

static void MethodsInit(AtBerSoftController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ChannelType);
        mMethodOverride(m_methods, BitErrorGetFunc);
        mMethodOverride(m_methods, BerStatusChangedFunc);
        mMethodOverride(m_methods, BerCurrentDefectGet);
        mMethodOverride(m_methods, SdDefectMask);
        mMethodOverride(m_methods, SfDefectMask);
        mMethodOverride(m_methods, DefectChanged);
        mMethodOverride(m_methods, ChannelBitErrorGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtBerSoftController AtBerSoftControllerObjectInit(AtBerSoftController self,
                                                  uint32 controllerId,
                                                  AtChannel channel,
                                                  AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerControllerObjectInit((AtBerController)self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Save channel module */
    self->channelModule = AtChannelModuleGet(channel);

    return self;
    }

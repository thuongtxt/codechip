/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtBerMeasureInternal.h
 * 
 * Created Date: Nov 13, 2017
 *
 * Description : BER-Detection and clearing time Measurement
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBERMEASUREINTERNAL_H_
#define _ATBERMEASUREINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../include/ber/AtBerMeasureTimeEngine.h"
#include "AtModuleBer.h"
#include "AtBerController.h"
#include "../common/AtObjectInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtBerMeasureTimeEngineMethods
    {
    eAtRet (*Init)(AtBerMeasureTimeEngine self);
    uint32 (*EstimatedTimeGet)(AtBerMeasureTimeEngine self, eAtBerTimerType type);
    eAtRet (*EstimatedTimeClear)(AtBerMeasureTimeEngine self, eAtBerTimerType type);
    eAtRet (*InputErrorConnect)(AtBerMeasureTimeEngine self);
    eAtRet (*InputErrorDisConnect)(AtBerMeasureTimeEngine self);
    uint32 (*PeriodProcess)(AtBerMeasureTimeEngine self);
    eAtRet (*PollingStart)(AtBerMeasureTimeEngine self);
    eAtRet (*PollingStop)(AtBerMeasureTimeEngine self);
    eAtRet (*ModeSet)(AtBerMeasureTimeEngine self, eAtBerMeasureMode mode);
    }tAtBerMeasureTimeEngineMethods;

/* BER Measure abstract class representation */
typedef struct tAtBerMeasureTimeEngine
    {
    tAtObject super;
    const tAtBerMeasureTimeEngineMethods *methods;

    /* Private data */
    AtBerController controller;
    tAtBerMeasureEstimatedTime estimateTimes;
    eAtBerMeasureState state;
    eAtBerMeasureMode mode;
    eBool pollingEnable;
    }tAtBerMeasureTimeEngine;

/* BER Measure Software abstract class representation */
typedef struct tAtBerMeasureTimeSoftEngine
    {
    tAtBerMeasureTimeEngine super;

    /* Private data */
    tAtOsalCurTime curTime;
    tAtOsalCurTime startTime;
    }tAtBerMeasureTimeSoftEngine;

typedef struct tAtBerMeasureTimeHardEngine
    {
    tAtBerMeasureTimeEngine super;
    }tAtBerMeasureTimeHardEngine;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerMeasureTimeEngine AtBerMeasureTimeEngineObjectInit(AtBerMeasureTimeEngine self, AtBerController controller);
AtBerMeasureTimeEngine AtBerMeasureTimeSoftEngineObjectInit(AtBerMeasureTimeEngine self, AtBerController controller);
AtBerMeasureTimeEngine AtBerMeasureTimeHardEngineObjectInit(AtBerMeasureTimeEngine self, AtBerController controller);

eBool AtBerMeasureTimeEngineIsContinousMode(AtBerMeasureTimeEngine self);
eBool AtBerMeasureTimeEngineIsAutoStopMode(AtBerMeasureTimeEngine self);
eAtRet AtBerMeasureTimeEngineInit(AtBerMeasureTimeEngine self);

/* For Reset all data when stop --> start polling */
eAtRet AtBerMeasureTimeEnginePollingEnableSet(AtBerMeasureTimeEngine self, eBool enable);
eBool AtBerMeasureTimeEnginePollingIsEnabled(AtBerMeasureTimeEngine self);
eAtRet AtBerMeasureTimeEngineEstimatedTimeSet(AtBerMeasureTimeEngine self, eAtBerTimerType type, uint32 timeInMs);
eBool AtBerMeasureTimeEngineEstimatedTimeIsPassed(AtBerMeasureTimeEngine self);
eAtRet AtBerMeasureTimeEngineEstimatedTimeClear(AtBerMeasureTimeEngine self, eAtBerTimerType type);
uint32 AtBerMeasureTimeEngineEstimatedTimeGet(AtBerMeasureTimeEngine self, eAtBerTimerType type);

void AtBerMeasureTimeEngineStateSet(AtBerMeasureTimeEngine self, eAtBerMeasureState state);

#ifdef __cplusplus
}
#endif
#endif /* _ATBERMEASUREINTERNAL_H_ */


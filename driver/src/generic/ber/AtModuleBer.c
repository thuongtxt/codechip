/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtModuleBer.c
 *
 * Created Date: Feb 07, 2013
 *
 * Description : Generic BER module
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModuleBerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModuleIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtModuleBerMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods        *m_AtObjectMethods        = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtBerController SdhLineMsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
	AtUnused(line);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtBerController SdhLineRsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
	AtUnused(line);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtBerController SdhPathBerControlerCreate(AtModuleBer self, AtChannel path)
    {
	AtUnused(path);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static void ControllerDelete(AtModuleBer self, AtBerController controller)
    {
	AtUnused(controller);
	AtUnused(self);
    /* Let concrete class do */
    }

static uint32 PeriodProcess(AtModuleBer self, uint32 periodInMs)
    {
	AtUnused(periodInMs);
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "ber";
    }

static uint32 BaseAddress(AtModuleBer self)
    {
    AtUnused(self);
    return 0x0;
    }

static AtBerController PdhChannelPathBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    AtUnused(self);
    AtUnused(pdhChannel);
    /* Let concrete class do */
    return NULL;
    }

static AtBerController PdhChannelLineBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    AtUnused(self);
    AtUnused(pdhChannel);
    /* Let concrete class do */
    return NULL;
    }

static AtBerController SdhLineRsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    AtUnused(self);
    AtUnused(controllerId);
    AtUnused(line);
    return NULL;
    }

static AtBerController SdhLineMsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    AtUnused(self);
    AtUnused(controllerId);
    AtUnused(line);
    /* Let concrete class do */
    return NULL;
    }

static AtBerController SdhPathBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel path)
    {
    AtUnused(self);
    AtUnused(controllerId);
    AtUnused(path);
    /* Let concrete class do */
    return NULL;
    }

static eBool ChannelBerShouldBeEnabledByDefault(AtModuleBer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtBerMeasureTimeEngine MeasureTimeSoftEngineCreate(AtModuleBer self, AtBerController controller)
    {
    if (!AtModuleBerMeasureTimeEngineIsSupported(self))
        return NULL;

    if (self->engine)
        return NULL;

    self->engine = mMethodsGet(self)->MeasureTimeSoftEngineObjectCreate(self, controller);
    if (self->engine)
        AtBerMeasureTimeEngineInit(self->engine);

    return self->engine;
    }

static AtBerMeasureTimeEngine MeasureTimeHardEngineCreate(AtModuleBer self, AtBerController controller)
    {
    if (!AtModuleBerMeasureTimeEngineIsSupported(self))
        return NULL;

    if (self->engine)
        return NULL;

    self->engine = mMethodsGet(self)->MeasureTimeHardEngineObjectCreate(self, controller);
    if (self->engine)
        AtBerMeasureTimeEngineInit(self->engine);

    return self->engine;
    }

static AtBerMeasureTimeEngine MeasureTimeHardEngineObjectCreate(AtModuleBer self, AtBerController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return NULL;
    }

static AtBerMeasureTimeEngine MeasureTimeSoftEngineObjectCreate(AtModuleBer self, AtBerController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return NULL;
    }

static eBool MeasureTimeEngineIsSupported(AtModuleBer self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    AtModuleBer module = (AtModuleBer)self;

    /* Delete internal SW engine */
    if (module->engine)
        {
        AtObjectDelete((AtObject) module->engine);
        module->engine = NULL;
        }

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleBer object = (AtModuleBer)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(engine);
    }

static void OverrideAtObject(AtModuleBer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleBer self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, TypeString);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

static void MethodsInit(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, PeriodProcess);
        mMethodOverride(m_methods, SdhLineRsBerControlerCreate);
        mMethodOverride(m_methods, SdhLineMsBerControlerCreate);
        mMethodOverride(m_methods, SdhPathBerControlerCreate);
        mMethodOverride(m_methods, ControllerDelete);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, PdhChannelPathBerControlerCreate);
        mMethodOverride(m_methods, PdhChannelLineBerControlerCreate);
        mMethodOverride(m_methods, SdhLineRsBerControlerObjectCreate);
        mMethodOverride(m_methods, SdhLineMsBerControlerObjectCreate);
        mMethodOverride(m_methods, SdhPathBerControlerObjectCreate);
        mMethodOverride(m_methods, ChannelBerShouldBeEnabledByDefault);

        mMethodOverride(m_methods, MeasureTimeHardEngineObjectCreate);
        mMethodOverride(m_methods, MeasureTimeSoftEngineObjectCreate);
        mMethodOverride(m_methods, MeasureTimeEngineIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

AtModuleBer AtModuleBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModuleBer));
    
    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModuleBer, device) == NULL)
        return NULL;
    
    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;
    
    return self;
    }

AtBerMeasureTimeEngine AtModuleBerMeasureTimeEngineGet(AtModuleBer self)
    {
    if (self)
        return self->engine;
    return NULL;
    }

eBool AtModuleBerMeasureTimeEngineIsSupported(AtModuleBer self)
    {
    if (self)
        return mMethodsGet(self)->MeasureTimeEngineIsSupported(self);
    return cAtFalse;
    }

eAtRet AtModuleBerMeasureTimeEngineSet(AtModuleBer self, AtBerMeasureTimeEngine engine)
    {
    if (self)
        {
        self->engine = engine;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

uint32 AtModuleBerBaseAddress(AtModuleBer self)
    {
    return (self) ? mMethodsGet(self)->BaseAddress(self) : 0x0;
    }

eBool AtModuleBerChannelBerShouldBeEnabledByDefault(AtModuleBer self)
    {
    if (self)
        return mMethodsGet(self)->ChannelBerShouldBeEnabledByDefault(self);
    return cAtFalse;
    }

AtBerMeasureTimeEngine AtModuleBerMeasureTimeSoftEngineCreate(AtModuleBer self, AtBerController controller)
    {
    if (self && controller)
        return MeasureTimeSoftEngineCreate(self, controller);
    return NULL;
    }

AtBerMeasureTimeEngine AtModuleBerMeasureTimeHardEngineCreate(AtModuleBer self, AtBerController controller)
    {
    if (self && controller)
        return MeasureTimeHardEngineCreate(self, controller);
    return NULL;
    }

eAtRet AtModuleBerMeasureTimeEngineDeleteNoLock(AtModuleBer self, AtBerMeasureTimeEngine engine)
    {
    if (self && engine)
        {
        if (self->engine != engine)
            return cAtErrorRsrcNoAvail;

        /* Keep Error normal to BER controller */
        AtBerMeasureTimeEngineInputErrorConnect(engine);
        AtObjectDelete((AtObject)engine);

        self->engine = NULL;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eAtRet AtModuleBerMeasureTimeEngineDelete(AtModuleBer self, AtBerMeasureTimeEngine engine)
    {
    if (self && engine)
        {
    	eAtRet ret;
        AtModuleLock((AtModule)self);
        ret = AtModuleBerMeasureTimeEngineDeleteNoLock(self, engine);
        AtModuleUnLock((AtModule)self);
        return ret;
        }

    return cAtErrorNullPointer;
    }

/**
 * @addtogroup AtModuleBer
 * @{
 */

/**
 * This API must be called to help software BER controller run and monitor BER
 *
 * @param self This module
 * @param periodInMs Period in milliseconds
 *
 * @return Time in millisecond that this API takes to run one cycle.
 */
uint32 AtModuleBerPeriodicProcess(AtModuleBer self, uint32 periodInMs)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->PeriodProcess(self, periodInMs);

    return 0;
    }

/**
 * Delete BER controller
 *
 * @param self This module
 * @param controller Controller to be deleted
 */
void AtModuleBerControllerDelete(AtModuleBer self, AtBerController controller)
    {
    if (!mModuleIsValid(self))
        return;
    mMethodsGet(self)->ControllerDelete(self, controller);
    }

/**
 * Create BER controller for SDH Line RS layer
 *
 * @param self This module
 * @param line SDH Line needs to monitor BER
 *
 * @return BER controller on success or NULL on failure
 */
AtBerController AtModuleBerSdhLineRsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
    if (!mModuleIsValid(self))
        return NULL;

    return mMethodsGet(self)->SdhLineRsBerControlerCreate(self, line);
    }

/**
 * Create BER controller for SDH Line MS layer
 *
 * @param self This module
 * @param line SDH Line needs to monitor BER
 *
 * @return BER controller on success or NULL on failure
 */
AtBerController AtModuleBerSdhLineMsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
    if (!mModuleIsValid(self))
        return NULL;

    return mMethodsGet(self)->SdhLineMsBerControlerCreate(self, line);
    }

/**
 * Create BER controller for SDH Path
 *
 * @param self This module
 * @param path SDH path needs to monitor BER
 *
 * @return BER controller on success or NULL on failure
 */
AtBerController AtModuleBerSdhPathBerControlerCreate(AtModuleBer self, AtChannel path)
    {
    if (!mModuleIsValid(self))
        return NULL;

    return mMethodsGet(self)->SdhPathBerControlerCreate(self, path);
    }

/**
 * Create BER controller for PDH path
 *
 * @param self This module
 * @param pdhChannel PDH channel needs to monitor BER
 *
 * @return BER controller on success or NULL on failure
 */
AtBerController AtModuleBerPdhChannelPathBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    if (!mModuleIsValid(self))
        return NULL;

    return mMethodsGet(self)->PdhChannelPathBerControlerCreate(self, pdhChannel);
    }

/**
 * Create BER controller for PDH line
 *
 * @param self This module
 * @param pdhChannel PDH channel needs to monitor BER
 *
 * @return BER controller on success or NULL on failure
 */
AtBerController AtModuleBerPdhChannelLineBerControlerCreate(AtModuleBer self, AtChannel pdhChannel)
    {
    if (!mModuleIsValid(self))
        return NULL;

    return mMethodsGet(self)->PdhChannelLineBerControlerCreate(self, pdhChannel);
    }
/**
 * @}
 */

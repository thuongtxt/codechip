/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtBerController.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : BER controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../common/AtChannelInternal.h"
#include "AtBerControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mControllerIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtBerControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtBerController self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtOk;
    }

static eAtRet Enable(AtBerController self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eBool IsEnabled(AtBerController self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
    }

static eAtRet SdThresholdSet(AtBerController self, eAtBerRate threshold)
    {
	AtUnused(threshold);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eAtBerRate SdThresholdGet(AtBerController self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtBerRateUnknown;
    }

static eAtRet SfThresholdSet(AtBerController self, eAtBerRate threshold)
    {
	AtUnused(threshold);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eAtBerRate SfThresholdGet(AtBerController self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtBerRateUnknown;
    }

static eAtRet TcaThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static eAtBerRate TcaThresholdGet(AtBerController self)
    {
    AtUnused(self);
    return cAtBerRateUnknown;
    }

static eAtBerRate CurBerGet(AtBerController self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtBerRateUnknown;
    }

static eBool IsSd(AtBerController self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
    }

static eBool IsSf(AtBerController self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
    }

static eBool IsTca(AtBerController self)
    {
    AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
    }

static eBool SdHistoryGet(AtBerController self, eBool clear)
    {
	AtUnused(clear);
	AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool SfHistoryGet(AtBerController self, eBool clear)
    {
	AtUnused(clear);
	AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static eBool TcaHistoryGet(AtBerController self, eBool clear)
    {
    AtUnused(clear);
    AtUnused(self);
    /* Let sub-class do */
    return cAtFalse;
    }

static void DebugOn(AtBerController self, eBool debugOn)
    {
    self->debugOn = debugOn;
    }

static eBool DebugIsOn(AtBerController self)
    {
    return self->debugOn;
    }

static eAtRet HwChannelTypeUpdate(AtBerController self)
	{
    AtUnused(self);
    /* Let sub-class do */
	return cAtOk;
	}

static uint8 PartId(AtBerController self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static eAtRet InterruptMaskSet(AtBerController self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    AtUnused(enableMask);
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtBerController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 InterruptProcess(AtBerController self, uint32 isrContext)
    {
    AtUnused(self);
    AtUnused(isrContext);
    return 0;
    }

static uint32 DefectGet(AtBerController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtBerController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtBerController self)
    {
    AtUnused(self);
    return 0;
    }

static eBool IsValidRate(eAtBerRate threshold)
    {
    if ((threshold >= cAtBerRate1E3) &&
        (threshold <= cAtBerRate1E10))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 ExpectedDetectionTimeInMs(AtBerController self, eAtBerRate rate)
    {
    AtUnused(self);
    AtUnused(rate);
    /* Let sub-class do */
    return 0;
    }

static uint32 ExpectedClearingTimeInMs(AtBerController self, eAtBerRate rate)
    {
    AtUnused(self);
    AtUnused(rate);
    /* Let sub-class do */
    return 0;
    }

static uint32 MeasureEngineId(AtBerController self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtBerController);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtBerController object = (AtBerController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(controllerId);
    mEncodeChannelIdString(monitoredChannel);
    mEncodeUInt(debugOn);
    mEncodeObjectDescription(berModule);
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtChannel channel = AtBerControllerMonitoredChannel((AtBerController)self);
    AtSnprintf(description, sizeof(description), "ber.%s", AtObjectToString((AtObject)channel));
    return description;
    }

static void OverrideAtObject(AtBerController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtBerController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, SdThresholdSet);
        mMethodOverride(m_methods, SdThresholdGet);
        mMethodOverride(m_methods, SfThresholdSet);
        mMethodOverride(m_methods, SfThresholdGet);
        mMethodOverride(m_methods, TcaThresholdSet);
        mMethodOverride(m_methods, TcaThresholdGet);
        mMethodOverride(m_methods, CurBerGet);
        mMethodOverride(m_methods, IsSd);
        mMethodOverride(m_methods, IsSf);
        mMethodOverride(m_methods, IsTca);
        mMethodOverride(m_methods, SdHistoryGet);
        mMethodOverride(m_methods, SfHistoryGet);
        mMethodOverride(m_methods, TcaHistoryGet);
        mMethodOverride(m_methods, DebugOn);
        mMethodOverride(m_methods, DebugIsOn);
        mMethodOverride(m_methods, HwChannelTypeUpdate);
        mMethodOverride(m_methods, PartId);
        mMethodOverride(m_methods, ExpectedDetectionTimeInMs);
        mMethodOverride(m_methods, ExpectedClearingTimeInMs);
        mMethodOverride(m_methods, MeasureEngineId);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, DefectGet);
        mMethodOverride(m_methods, DefectHistoryGet);
        mMethodOverride(m_methods, DefectHistoryClear);
        }

    mMethodsSet(self, &m_methods);
    }

AtBerController AtBerControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* And private data */
    self->controllerId = controllerId;
    self->monitoredChannel = channel;
    self->berModule = berModule;

    return self;
    }

void AtBerControllerDebugOn(AtBerController self, eBool debugOn)
    {
    if (self)
        mMethodsGet(self)->DebugOn(self, debugOn);
    }

eBool AtBerControllerDebugIsOn(AtBerController self)
    {
    return (eBool)(self ? mMethodsGet(self)->DebugIsOn(self) : cAtFalse);
    }

uint32 AtBerControllerExpectedDetectionTimeInMs(AtBerController self, eAtBerRate rate)
    {
    if (self)
        return mMethodsGet(self)->ExpectedDetectionTimeInMs(self, rate);
    return 0;
    }

uint32 AtBerControllerExpectedClearingTimeInMs(AtBerController self, eAtBerRate rate)
    {
    if (self)
        return mMethodsGet(self)->ExpectedClearingTimeInMs(self, rate);
    return 0;
    }

uint32 AtBerControllerMeasureEngineId(AtBerController self)
    {
    if (self)
        return mMethodsGet(self)->MeasureEngineId(self);
    return 0;
    }

uint8 AtBerControllerPartId(AtBerController self)
    {
    if (self)
        return mMethodsGet(self)->PartId(self);
    return 0;
    }

uint32 AtBerControllerRead(AtBerController self, uint32 address)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    return mChannelHwRead(channel, address, cAtModuleBer);
    }

void AtBerControllerWrite(AtBerController self, uint32 address, uint32 value)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    mChannelHwWrite(channel, address, value, cAtModuleBer);
    }

uint16 AtBerControllerLongRead(AtBerController self, uint32 address, uint32* regVal)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    return mChannelHwLongRead(channel, address, regVal, 8, cAtModuleBer);
    }

uint16 AtBerControllerLongWrite(AtBerController self, uint32 address, const uint32* regVal)
    {
    AtChannel channel = AtBerControllerMonitoredChannel(self);
    return mChannelHwLongWrite(channel, address, regVal, 8, cAtModuleBer);
    }

eAtRet AtBerControllerHwChannelTypeUpdate(AtBerController self)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->HwChannelTypeUpdate(self);

    return cAtError;
    }

eAtRet AtBerControllerInterruptMaskSet(AtBerController self, uint32 defectMask, uint32 enableMask)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaskSet(self, defectMask, enableMask);
    return cAtErrorNullPointer;
    }

uint32 AtBerControllerInterruptMaskGet(AtBerController self)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaskGet(self);
    return 0;
    }

uint32 AtBerControllerInterruptProcess(AtBerController self, uint32 isrContext)
    {
    if (self)
        return mMethodsGet(self)->InterruptProcess(self, isrContext);
    return 0;
    }

uint32 AtBerControllerDefectGet(AtBerController self)
    {
    if (self)
        return mMethodsGet(self)->DefectGet(self);
    return 0;
    }

uint32 AtBerControllerDefectHistoryGet(AtBerController self)
    {
    if (self)
        return mMethodsGet(self)->DefectHistoryGet(self);
    return 0;
    }

uint32 AtBerControllerDefectHistoryClear(AtBerController self)
    {
    if (self)
        return mMethodsGet(self)->DefectHistoryClear(self);
    return 0;
    }

/**
 * @addtogroup AtBerController
 * @{
 */

/**
 * Initialize this BER controller
 *
 * @param self This controller
 *
 * @return AT return code
 */
eAtModuleBerRet AtBerControllerInit(AtBerController self)
    {
    mNoParamCall(Init, eAtModuleBerRet, cAtErrorObjectNotExist);
    }

/**
 * Get ID of this controller
 *
 * @param self This controller
 *
 * @return Controller ID
 */
uint32 AtBerControllerIdGet(AtBerController self)
    {
    if (mControllerIsValid(self))
        return self->controllerId;

    return 0;
    }

/**
 * Get the channel that this controller is monitoring
 *
 * @param self This controller
 *
 * @return The channel that this controller is monitoring
 */
AtChannel AtBerControllerMonitoredChannel(AtBerController self)
    {
    if (mControllerIsValid(self))
        return self->monitoredChannel;

    return NULL;
    }

/**
 * Get BER module of this controller
 *
 * @param self This controller
 *
 * @return Its BER module
 */
AtModule AtBerControllerModuleGet(AtBerController self)
    {
    return mControllerIsValid(self) ? (AtModule)(self->berModule) : NULL;
    }

/**
 * Enable this controller to monitor BER
 *
 * @param self This controller
 * @param enable Enable/disable
 *
 * @return AT return code
 */
eAtModuleBerRet AtBerControllerEnable(AtBerController self, eBool enable)
    {
    mNumericalAttributeSet(Enable, enable);
    }

/**
 * Check if this controller is enabled
 *
 * @param self This controller
 * @retval cAtTrue It is enabled
 * @retval cAtFalse It is disabled
 */
eBool AtBerControllerIsEnabled(AtBerController self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * Set threshold to report BER-SD defect
 *
 * @param self This controller
 * @param threshold @ref eAtBerRate "BER rates"
 *
 * @return AT return code
 */
eAtModuleBerRet AtBerControllerSdThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    if (!IsValidRate(threshold))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with invalid threshold: %d\r\n", AtFunction, threshold);
        return cAtErrorInvlParm;
        }

    mNumericalAttributeSet(SdThresholdSet, threshold);
    }

/**
 * Get threshold used to report BER-SD defect
 *
 * @param self This controller
 *
 * @return @ref eAtBerRate "BER rates"
 */
eAtBerRate AtBerControllerSdThresholdGet(AtBerController self)
    {
    mAttributeGet(SdThresholdGet, eAtBerRate, cAtBerRateUnknown);
    }

/**
 * Set threshold to report BER-SF defect
 *
 * @param self This controller
 * @param threshold @ref eAtBerRate "BER rates"
 *
 * @return AT return code
 */
eAtModuleBerRet AtBerControllerSfThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    if (!IsValidRate(threshold))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with invalid threshold: %d\r\n", AtFunction, threshold);
        return cAtErrorInvlParm;
        }

    mNumericalAttributeSet(SfThresholdSet, threshold);
    }

/**
 * Get threshold used to report BER-SF defect
 *
 * @param self This controller
 *
 * @return @ref eAtBerRate "BER rates"
 */
eAtBerRate AtBerControllerSfThresholdGet(AtBerController self)
    {
    mAttributeGet(SfThresholdGet, eAtBerRate, cAtBerRateUnknown);
    }

/**
 * Set threshold used to report BER-TCA alarm
 *
 * @param self This controller
 * @param threshold threshold @ref eAtBerRate "BER rates"
 *
 * @return AT return code
 */
eAtModuleBerRet AtBerControllerTcaThresholdSet(AtBerController self, eAtBerRate threshold)
    {
    if (!IsValidRate(threshold))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with invalid threshold: %d\r\n", AtFunction, threshold);
        return cAtErrorInvlParm;
        }

    mNumericalAttributeSet(TcaThresholdSet, threshold);
    }

/**
 * Get threshold used to report BER-TCA alarm
 *
 * @param self This controller
 *
 * @return @ref eAtBerRate "BER rates"
 */
eAtBerRate AtBerControllerTcaThresholdGet(AtBerController self)
    {
    mAttributeGet(TcaThresholdGet, eAtBerRate, cAtBerRateUnknown);
    }

/**
 * Get current monitored BER rate
 *
 * @param self This controller
 *
 * @return @ref eAtBerRate "Current BER rate"
 */
eAtBerRate AtBerControllerCurBerGet(AtBerController self)
    {
    mAttributeGet(CurBerGet, eAtBerRate, cAtBerRateUnknown);
    }

/**
 * Check if the controller enter BER-SD condition
 *
 * @param self This controller
 *
 * @retval cAtTrue BER-SD is happening now
 * @retval cAtFalse BER-SD is clear
 */
eBool AtBerControllerIsSd(AtBerController self)
    {
    if (!mControllerIsValid(self))
        return cAtFalse;

    if (!AtBerControllerIsEnabled(self))
        return cAtFalse;

    return mMethodsGet(self)->IsSd(self);
    }

/**
 * Check if the controller enter BER-SF condition
 *
 * @param self This controller
 *
 * @retval cAtTrue BER-SF is happening now
 * @retval cAtFalse BER-SF is clear
 */
eBool AtBerControllerIsSf(AtBerController self)
    {
    if (!mControllerIsValid(self))
        return cAtFalse;

    if (!AtBerControllerIsEnabled(self))
        return cAtFalse;

    return mMethodsGet(self)->IsSf(self);
    }

/**
 * Check if TCA has been raised.
 *
 * @param self This controller
 *
 * @retval cAtTrue if TCA
 * @retval cAtFalse if no TCA
 */
eBool AtBerControllerIsTca(AtBerController self)
    {
    mAttributeGet(IsTca, eBool, cAtFalse);
    }


/**
 * Get history of BER-SD
 *
 * @param self This controller
 * @param clear Get and clear history
 *
 * @retval cAtTrue BER-SD occur in history
 * @retval cAtFalse BER-SD do not occur in history
 */
eBool AtBerControllerSdHistoryGet(AtBerController self, eBool clear)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->SdHistoryGet(self, clear);

    return cAtFalse;
    }

/**
 * Get history of BER-SF
 *
 * @param self This controller
 * @param clear Get and clear history
 *
 * @retval cAtTrue BER-SF occur in history
 * @retval cAtFalse BER-SF do not occur in history
 */
eBool AtBerControllerSfHistoryGet(AtBerController self, eBool clear)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->SfHistoryGet(self, clear);

    return cAtFalse;
    }

/**
 * Get history of BER-TCA
 *
 * @param self This controller
 * @param clear Get and clear history
 *
 * @retval cAtTrue BER-TCA occur in history
 * @retval cAtFalse BER-TCA do not occur in history
 */
eBool AtBerControllerTcaHistoryGet(AtBerController self, eBool clear)
    {
    if (mControllerIsValid(self))
        return mMethodsGet(self)->TcaHistoryGet(self, clear);
    return cAtFalse;
    }

/**
 * @}
 */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtModuleSoftBer.h
 * 
 * Created Date: Feb 7, 2013
 *
 * Description : BER module - software solution
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESOFTBER_H_
#define _ATMODULESOFTBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleBer.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSoftBer * AtModuleSoftBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULESOFTBER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtSdhPathBerSoftControllerInternal.h
 * 
 * Created Date: Feb 20, 2013
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHPATHBERSOFTCONTROLLERINTERNAL_H_
#define _ATSDHPATHBERSOFTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtBerSoftControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhPathBerSoftController
    {
    tAtBerSoftController super;

    /* Private data may be added in the future */
    }tAtSdhPathBerSoftController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController AtSdhPathBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHPATHBERSOFTCONTROLLERINTERNAL_H_ */


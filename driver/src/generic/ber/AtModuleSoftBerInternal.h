/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtModuleSoftBerInternal.h
 * 
 * Created Date: Feb 7, 2013
 *
 * Description : Software BER module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESOFTBERINTERNAL_H_
#define _ATMODULESOFTBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atber/atber.h"
#include "AtModuleSoftBer.h"
#include "AtModuleBerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of module BER */
typedef struct tAtModuleSoftBerMethods
    {
    uint32 (*TaskPeriod)(AtModuleSoftBer self);
    uint32 (*MaxNumControllersGet)(AtModuleSoftBer self);
    eBool (*BerProcessCanRunWithTaskPeriod)(AtModuleSoftBer self, uint32 periodInMs);
    }tAtModuleSoftBerMethods;

/* Representation */
typedef struct tAtModuleSoftBer
    {
    /* Inherit from tAtModule */
    tAtModuleBer super;

    /* This class 's methods */
    const tAtModuleSoftBerMethods *methods;

    /* To work with BER SW common engine */
    AtBerModule berHandle;
    const tAtBerOsIntf *osIntf;
    AtBerController *controllers;
    uint32 maxNumControllers;
    } tAtModuleSoftBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSoftBer AtModuleSoftBerObjectInit(AtModuleSoftBer self, AtDevice device, uint32 maxNumControllers);
AtBerModule AtModuleBerSwBerHandleGet(AtModuleBer self);
const tAtBerOsIntf *AtModuleBerDefaultOsIntf(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULESOFTBERINTERNAL_H_ */


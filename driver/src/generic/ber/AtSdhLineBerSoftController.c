/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtSdhLineBerSoftController.c
 *
 * Created Date: Feb 6, 2013
 *
 * Description : Concrete BER controller for SDH Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLineBerSoftControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtBerSoftControllerMethods m_AtBerSoftControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhLineBerSoftController);
    }

static uint32 SdDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    return cAtSdhLineAlarmBerSd;
    }

static uint32 SfDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    return cAtSdhLineAlarmBerSf;
    }

static void OverrideAtBerSoftController(AtBerController self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerSoftControllerOverride, mMethodsGet(softController), sizeof(m_AtBerSoftControllerOverride));

        mMethodOverride(m_AtBerSoftControllerOverride, SdDefectMask);
        mMethodOverride(m_AtBerSoftControllerOverride, SfDefectMask);
        }

    mMethodsSet(softController, &m_AtBerSoftControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerSoftController(self);
    }

AtBerController AtSdhLineBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerSoftControllerObjectInit((AtBerSoftController)self, controllerId, channel, berModule) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtBerMeasureTimeEngineSoft.c
 *
 * Created Date: Nov 13, 2017
 *
 * Description : BER-Detection and clearing time Measurement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtBerMeasureTimeEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self)                            ((tAtBerMeasureTimeSoftEngine*)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerMeasureTimeEngineMethods m_AtBerMeasureOverride;

/* Save super implementation */
static const tAtBerMeasureTimeEngineMethods *m_AtBerMeasureMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 WaitErrorStartingProcess(AtBerMeasureTimeEngine self)
    {
 	AtBerMeasureTimeEngineInputErrorConnect(self);
	AtOsalCurTimeGet(&mThis(self)->startTime);
	AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitSfDetectionTime);
    return 0;
    }

static uint32 WaitSfDetectionTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (AtBerControllerIsSf(controller))
        {
        uint32 estimateTimeInMs;
        AtOsalCurTimeGet(&mThis(self)->curTime);
        estimateTimeInMs = mTimeIntervalInMsGet(mThis(self)->startTime, mThis(self)->curTime);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeSfDetection, estimateTimeInMs);
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitErrorStop);
        }

    return 0;
    }

static uint32 WaitErrorStopProcess(AtBerMeasureTimeEngine self)
    {
	AtBerMeasureTimeEngineInputErrorDisConnect(self);
	AtOsalCurTimeGet(&mThis(self)->startTime);
	AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitSfClearingTime);
    return 0;
    }

static uint32 WaitSfClearingTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (!AtBerControllerIsSf(controller))
        {
        uint32 estimateTimeInMs;
        AtOsalCurTimeGet(&mThis(self)->curTime);
        estimateTimeInMs = mTimeIntervalInMsGet(mThis(self)->startTime, mThis(self)->curTime);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeSfClearing, estimateTimeInMs);
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitSdClearingTime);
        }
    return 0;
    }

static uint32 WaitSdClearingTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (!AtBerControllerIsSd(controller))
        {
        uint32 estimateTimeInMs;
        AtOsalCurTimeGet(&mThis(self)->curTime);
        estimateTimeInMs = mTimeIntervalInMsGet(mThis(self)->startTime, mThis(self)->curTime);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeSdClearing, estimateTimeInMs);
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitTcaClearingTime);
        }
    return 0;
    }

static uint32 WaitTcaClearingTimeProcess(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);
    if (!AtBerControllerIsTca(controller))
        {
        uint32 estimateTimeInMs;
        AtOsalCurTimeGet(&mThis(self)->curTime);
        estimateTimeInMs = mTimeIntervalInMsGet(mThis(self)->startTime, mThis(self)->curTime);
        (void) AtBerMeasureTimeEngineEstimatedTimeSet(self, cAtBerTimerTypeTcaClearing, estimateTimeInMs);

        if (AtBerMeasureTimeEngineIsContinousMode(self))
            {
            if (AtBerMeasureTimeEngineIsAutoStopMode(self) && !AtBerMeasureTimeEngineEstimatedTimeIsPassed(self))
                AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateStopInContinous);
            else
                AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitErrorStart);
            }
        else
            AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateOneShotDone);
        }
    return 0;
    }

static uint32 PeriodProcess(AtBerMeasureTimeEngine self)
    {
    eAtBerMeasureState state = AtBerMeasureTimeEngineStateGet(self);
    switch ((uint32) state)
        {
        case cAtBerMeasureStateWaitErrorStart:
            return WaitErrorStartingProcess(self);
        case cAtBerMeasureStateWaitSfDetectionTime:
            return WaitSfDetectionTimeProcess(self);
        case cAtBerMeasureStateWaitErrorStop:
            return WaitErrorStopProcess(self);
        case cAtBerMeasureStateWaitSfClearingTime:
            return WaitSfClearingTimeProcess(self);
        case cAtBerMeasureStateWaitSdClearingTime:
            return WaitSdClearingTimeProcess(self);
        case cAtBerMeasureStateWaitTcaClearingTime:
            return WaitTcaClearingTimeProcess(self);
        default:
            return 0;
        }
    }

static eBool ModeIsSupported(eAtBerMeasureMode mode)
    {
    if (mode == cAtBerMeasureModeContinousNoneStop)
        return cAtTrue;
    if (mode == cAtBerMeasureModeContinousAutoStop)
        return cAtTrue;
    if (mode == cAtBerMeasureModeOneShot)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet ModeSet(AtBerMeasureTimeEngine self, eAtBerMeasureMode mode)
    {
    if (!ModeIsSupported(mode))
        return cAtErrorModeNotSupport;

    return m_AtBerMeasureMethods->ModeSet(self, mode);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtBerMeasureTimeSoftEngine);
    }

static void OverrideAtBerMeasureTimeEngine(AtBerMeasureTimeEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtBerMeasureMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerMeasureOverride, m_AtBerMeasureMethods, sizeof(m_AtBerMeasureOverride));

        mMethodOverride(m_AtBerMeasureOverride, PeriodProcess);
        mMethodOverride(m_AtBerMeasureOverride, ModeSet);
        }

    mMethodsSet(self, &m_AtBerMeasureOverride);
    }

AtBerMeasureTimeEngine AtBerMeasureTimeSoftEngineObjectInit(AtBerMeasureTimeEngine self, AtBerController controller)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtBerMeasureTimeEngineObjectInit(self, controller) == NULL)
        return NULL;

    /* Setup methods */
    OverrideAtBerMeasureTimeEngine(self);
    m_methodsInit = 1;

    return self;
    }

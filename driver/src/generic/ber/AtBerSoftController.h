/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtBerSoftController.h
 * 
 * Created Date: Feb 7, 2013
 *
 * Description : BER software controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBERSOFTCONTROLLER_H_
#define _ATBERSOFTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtBerController.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief Software BER controller class
 */
typedef struct tAtBerSoftController * AtBerSoftController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtBerSoftControllerDebugEnable(AtBerSoftController self, eBool enable);
eBool AtBerSoftControllerDebugIsEnabled(AtBerSoftController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATBERSOFTCONTROLLER_H_ */


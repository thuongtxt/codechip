/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtBerMeasure.c
 *
 * Created Date: Nov 13, 2017
 *
 * Description : BER-Detection and clearing time Measurement
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtBerMeasureTimeEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtBerMeasureTimeEngineMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtBerMeasureTimeEngine self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return cAtErrorModeNotSupport;
    }

static uint32 EstimatedTimeGet(AtBerMeasureTimeEngine self, eAtBerTimerType type)
    {
    AtUnused(self);
    AtUnused(type);
    /* Let sub-class do */
    return 0;
    }

static eAtRet EstimatedTimeClear(AtBerMeasureTimeEngine self, eAtBerTimerType type)
    {
    AtUnused(self);
    AtUnused(type);
    /* Let sub-class do */
    return cAtErrorModeNotSupport;
    }

static eAtRet InputErrorConnect(AtBerMeasureTimeEngine self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return cAtErrorModeNotSupport;
    }

static eAtRet InputErrorDisConnect(AtBerMeasureTimeEngine self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return cAtErrorModeNotSupport;
    }

static uint32 PeriodProcess(AtBerMeasureTimeEngine self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static eBool CanPolling(AtBerMeasureTimeEngine self)
    {
    eAtBerMeasureMode mode = AtBerMeasureTimeEngineModeGet(self);
    if (mode == cAtBerMeasureModeContinousNoneStop)
        return cAtTrue;
    if (mode == cAtBerMeasureModeContinousAutoStop)
        return cAtTrue;
    if (mode == cAtBerMeasureModeOneShot)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet PollingStart(AtBerMeasureTimeEngine self)
    {
    if (!CanPolling(self))
        return cAtErrorNotApplicable;

    if (!AtBerMeasureTimeEnginePollingIsEnabled(self))
        {
        AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitErrorStart);
        AtBerMeasureTimeEngineEstimatedTimesClear(self);
        AtBerMeasureTimeEnginePollingEnableSet(self, cAtTrue);
        }
    return cAtOk;
    }

static eAtRet PollingStop(AtBerMeasureTimeEngine self)
    {
    if (AtBerMeasureTimeEnginePollingIsEnabled(self))
        AtBerMeasureTimeEnginePollingEnableSet(self, cAtFalse);
    return cAtOk;
    }

static eAtRet ModeSet(AtBerMeasureTimeEngine self, eAtBerMeasureMode mode)
    {
    self->mode = mode;
    return cAtOk;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtBerMeasureTimeEngine);
    }

static const char *ToString(AtObject self)
    {
    static char buf[64];
    AtSnprintf(buf,
               sizeof(buf), "Engine#%s",
               AtObjectToString((AtObject)((AtBerMeasureTimeEngine)self)->controller));
    return buf;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtBerMeasureTimeEngine object = (AtBerMeasureTimeEngine)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(controller);
    mEncodeUInt(state);
    mEncodeUInt(mode);
    mEncodeUInt(pollingEnable);
    }

static void OverrideAtObject(AtBerMeasureTimeEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtBerMeasureTimeEngine self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtBerMeasureTimeEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, EstimatedTimeGet);
        mMethodOverride(m_methods, EstimatedTimeClear);
        mMethodOverride(m_methods, InputErrorConnect);
        mMethodOverride(m_methods, InputErrorDisConnect);
        mMethodOverride(m_methods, PeriodProcess);
        mMethodOverride(m_methods, ModeSet);
        }

    mMethodsSet(self, &m_methods);
    }

AtBerMeasureTimeEngine AtBerMeasureTimeEngineObjectInit(AtBerMeasureTimeEngine self, AtBerController controller)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* And private data */
    self->controller = controller;

    return self;
    }

AtBerController AtBerMeasureTimeEngineControllerGet(AtBerMeasureTimeEngine self)
    {
    if (self)
        return self->controller;
    return NULL;
    }

void AtBerMeasureTimeEngineStateSet(AtBerMeasureTimeEngine self, eAtBerMeasureState state)
    {
    if (self)
        self->state = state;
    }

eAtBerMeasureState AtBerMeasureTimeEngineStateGet(AtBerMeasureTimeEngine self)
    {
    if (self)
        return self->state;
    return cAtBerMeasureStateUnKnown;
    }

eAtRet AtBerMeasureTimeEngineModeSet(AtBerMeasureTimeEngine self, eAtBerMeasureMode mode)
    {
    if (self)
        {
        if (AtBerMeasureTimeEngineModeGet(self) == mode)
            return cAtOk;

        return mMethodsGet(self)->ModeSet(self, mode);
        }

    return cAtErrorNullPointer;
    }

eAtBerMeasureMode AtBerMeasureTimeEngineModeGet(AtBerMeasureTimeEngine self)
    {
    if (self)
        return self->mode;
    return cAtBerMeasureModeUnKnown;
    }

eBool AtBerMeasureTimeEngineIsContinousMode(AtBerMeasureTimeEngine self)
    {
    if (self)
        {
        if (self->mode == cAtBerMeasureModeContinousNoneStop)
            return cAtTrue;

        if (self->mode == cAtBerMeasureModeContinousAutoStop)
            return cAtTrue;
        }

    return cAtFalse;
    }

eBool AtBerMeasureTimeEngineIsAutoStopMode(AtBerMeasureTimeEngine self)
    {
    if (self)
        return (self->mode == cAtBerMeasureModeContinousAutoStop) ? cAtTrue : cAtFalse;
    return cAtFalse;
    }

eAtRet AtBerMeasureTimeEnginePollingEnableSet(AtBerMeasureTimeEngine self, eBool enable)
    {
    if (self)
        {
        self->pollingEnable = enable;
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eBool AtBerMeasureTimeEnginePollingIsEnabled(AtBerMeasureTimeEngine self)
    {
    if (self)
        return self->pollingEnable;
    return cAtFalse;
    }

static void EstimatedTimeUpdate(tAtEstimatedTime *pTime, uint32 timeInMs, eBool firstData)
    {
    pTime->estimateTime = timeInMs;
    if ((pTime->estimateTimeMax < timeInMs)||firstData)
        pTime->estimateTimeMax = timeInMs;

    if ((pTime->estimateTimeMin > timeInMs)||firstData)
        pTime->estimateTimeMin = timeInMs;
    }

static eAtRet EstimatedTimeSet(AtBerMeasureTimeEngine self, uint32 type, uint32 timeInMs)
    {
    tAtBerMeasureEstimatedTime *pEstimateTime = &self->estimateTimes;
    eBool firstData = (pEstimateTime->numberOfEstimation <= 1) ? cAtTrue: cAtFalse;
    switch (type)
        {
        case cAtBerTimerTypeSfDetection:
            {
            pEstimateTime->numberOfEstimation = pEstimateTime->numberOfEstimation + 1;
            EstimatedTimeUpdate(&pEstimateTime->sfDetection, timeInMs, firstData);
            }break;
        case cAtBerTimerTypeSfClearing:
            {
            EstimatedTimeUpdate(&pEstimateTime->sfClearing, timeInMs, firstData);
            }break;
        case cAtBerTimerTypeSdClearing:
            {
            EstimatedTimeUpdate(&pEstimateTime->sdClearing, timeInMs, firstData);
            }break;
        case cAtBerTimerTypeTcaClearing:
            {
            EstimatedTimeUpdate(&pEstimateTime->tcaClearing, timeInMs, firstData);
            }break;
        default:
            return cAtErrorInvlParm;
        }

    return cAtOk;
    }

eAtRet AtBerMeasureTimeEngineEstimatedTimeSet(AtBerMeasureTimeEngine self, eAtBerTimerType type, uint32 timeInMs)
    {
    if (self)
        return EstimatedTimeSet(self, type, timeInMs);
    return cAtErrorNullPointer;
    }

eAtRet AtBerMeasureTimeEngineEstimatedTimesClear(AtBerMeasureTimeEngine self)
    {
    if (self)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &self->estimateTimes, 0, sizeof(tAtBerMeasureEstimatedTime));
        if (AtBerMeasureTimeEngineModeGet(self) == cAtBerMeasureModeManaual)
            {
            eAtRet ret = cAtOk;
            if (AtBerMeasureTimeEngineStateGet(self) == cAtBerMeasureStateWaitToLachInManual)
                return ret;

            ret |= AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeSfDetection);
            ret |= AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeSfClearing);
            ret |= AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeSdClearing);
            ret |= AtBerMeasureTimeEngineEstimatedTimeClear(self, cAtBerTimerTypeTcaClearing);

            AtBerMeasureTimeEngineStateSet(self, cAtBerMeasureStateWaitToLachInManual);
            return ret;
            }
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

static eBool SfDetectionTimeUnderExpected(AtBerController controller, uint32 timeInMs)
    {
    eAtBerRate rate = AtBerControllerSfThresholdGet(controller);
    uint32 expectedTimeInMs = AtBerControllerExpectedDetectionTimeInMs(controller, rate);
    return (timeInMs <= expectedTimeInMs) ? cAtTrue : cAtFalse;
    }

static eBool SfClearingTimeUnderExpected(AtBerController controller, uint32 timeInMs)
    {
    eAtBerRate rate = AtBerControllerSfThresholdGet(controller);
    uint32 expectedTimeInMs = AtBerControllerExpectedClearingTimeInMs(controller, rate);
    return (timeInMs <= expectedTimeInMs) ? cAtTrue : cAtFalse;
    }

static eBool SdClearingTimeUnderExpected(AtBerController controller, uint32 timeInMs)
    {
    eAtBerRate rate = AtBerControllerSdThresholdGet(controller);
    uint32 expectedTimeInMs = AtBerControllerExpectedClearingTimeInMs(controller, rate);
    return (timeInMs <= expectedTimeInMs) ? cAtTrue : cAtFalse;
    }

static eBool TcaClearingTimeUnderExpected(AtBerController controller, uint32 timeInMs)
    {
    eAtBerRate rate = AtBerControllerTcaThresholdGet(controller);
    uint32 expectedTimeInMs = AtBerControllerExpectedClearingTimeInMs(controller, rate);
    return (timeInMs <= expectedTimeInMs) ? cAtTrue : cAtFalse;
    }

static eBool EstimatedTimeIsPassed(AtBerMeasureTimeEngine self)
    {
    AtBerController controller = AtBerMeasureTimeEngineControllerGet(self);

    if (!SfDetectionTimeUnderExpected(controller, self->estimateTimes.sfDetection.estimateTime))
        return cAtFalse;

    if (!SfClearingTimeUnderExpected(controller, self->estimateTimes.sfClearing.estimateTime))
        return cAtFalse;

    if (!SdClearingTimeUnderExpected(controller, self->estimateTimes.sdClearing.estimateTime))
        return cAtFalse;

    if (!TcaClearingTimeUnderExpected(controller, self->estimateTimes.tcaClearing.estimateTime))
        return cAtFalse;

    return cAtTrue;
    }

eBool AtBerMeasureTimeEngineEstimatedTimeIsPassed(AtBerMeasureTimeEngine self)
    {
    if (self)
        return EstimatedTimeIsPassed(self);
    return cAtTrue;
    }

uint32 AtBerMeasureTimeEngineEstimatedTimeGet(AtBerMeasureTimeEngine self, eAtBerTimerType type)
    {
    if (self)
        return mMethodsGet(self)->EstimatedTimeGet(self, type);
    return 0;
    }

eAtRet AtBerMeasureTimeEngineEstimatedTimeClear(AtBerMeasureTimeEngine self, eAtBerTimerType type)
    {
    if (self)
        return mMethodsGet(self)->EstimatedTimeClear(self, type);
    return cAtErrorNullPointer;
    }

eAtRet AtBerMeasureTimeEngineInputErrorConnect(AtBerMeasureTimeEngine self)
    {
    if (self)
        return mMethodsGet(self)->InputErrorConnect(self);
    return cAtErrorNullPointer;
    }

eAtRet AtBerMeasureTimeEngineInputErrorDisConnect(AtBerMeasureTimeEngine self)
    {
    if (self)
        return mMethodsGet(self)->InputErrorDisConnect(self);
    return cAtErrorNullPointer;
    }

uint32 AtBerMeasureTimeEnginePeriodProcess(AtBerMeasureTimeEngine self)
    {
    if (self && AtBerMeasureTimeEnginePollingIsEnabled(self))
        return mMethodsGet(self)->PeriodProcess(self);
    return 0;
    }

eAtRet AtBerMeasureTimeEnginePollingStart(AtBerMeasureTimeEngine self)
    {
    if (self)
        return PollingStart(self);
    return cAtErrorNullPointer;
    }

eAtRet AtBerMeasureTimeEnginePollingStop(AtBerMeasureTimeEngine self)
    {
    if (self)
        return PollingStop(self);
    return cAtErrorNullPointer;
    }

eAtRet AtBerMeasureTimeEngineEstimatedTimesGet(AtBerMeasureTimeEngine self, void *estimateTimes)
    {
    if (self && estimateTimes)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        if ((self->state == cAtBerMeasureStateWaitToLachInManual)  && (self->mode == cAtBerMeasureModeManaual))
            {
            self->estimateTimes.numberOfEstimation = self->estimateTimes.numberOfEstimation + 1;
            self->estimateTimes.sfDetection.estimateTime = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeSfDetection);
            self->estimateTimes.sfClearing.estimateTime = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeSfClearing);
            self->estimateTimes.sdClearing.estimateTime = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeSdClearing);
            self->estimateTimes.tcaClearing.estimateTime = AtBerMeasureTimeEngineEstimatedTimeGet(self, cAtBerTimerTypeTcaClearing);
            self->state = cAtBerMeasureStateLatchedInManual;
            }

        mMethodsGet(osal)->MemCpy(osal,
                                  estimateTimes,
                                  &self->estimateTimes,
                                  sizeof(tAtBerMeasureEstimatedTime));
        return cAtOk;
        }

    return cAtErrorNullPointer;
    }

eAtRet AtBerMeasureTimeEngineInit(AtBerMeasureTimeEngine self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }

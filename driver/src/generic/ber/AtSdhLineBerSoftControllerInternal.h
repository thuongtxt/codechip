/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtSdhLineBerSoftControllerInternal.h
 * 
 * Created Date: Feb 20, 2013
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHLINEBERSOFTCONTROLLERINTERNAL_H_
#define _ATSDHLINEBERSOFTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atber/atber.h"
#include "AtSdhLine.h"
#include "AtBerSoftControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhLineBerSoftController
    {
    tAtBerSoftController super;

    /* Private data may be added in the future */
    }tAtSdhLineBerSoftController;

typedef struct tAtSdhLineMsBerSoftController
    {
    tAtSdhLineBerSoftController super;
    }tAtSdhLineMsBerSoftController;

typedef struct tAtSdhLineRsBerSoftController
    {
    tAtSdhLineBerSoftController super;
    }tAtSdhLineRsBerSoftController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController AtSdhLineBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController AtSdhLineMsBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController AtSdhLineRsBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHLINEBERSOFTCONTROLLERINTERNAL_H_ */


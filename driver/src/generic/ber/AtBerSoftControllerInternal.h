/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtBerSoftControllerInternal.h
 * 
 * Created Date: Feb 7, 2013
 *
 * Description : BER controller - Software solution
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBERSOFTCONTROLLERINTERNAL_H_
#define _ATBERSOFTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atber/atber.h"
#include "AtBerSoftController.h"
#include "AtBerControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mTimeIntervalInUsGet(previousTime, currentTime) ((((currentTime.sec - previousTime.sec) * 1000000) + (currentTime.usec)) - (previousTime.usec))

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtBerSoftControllerMethods
    {
    /* To work with BER monitoring engine */
    eAtBerChnType (*ChannelType)(AtBerSoftController self);
    AtBerBitErrGet (*BitErrorGetFunc)(AtBerSoftController self);
    AtBerStatusChange (*BerStatusChangedFunc)(AtBerSoftController self);

    /* To work with this SDK */
    uint32 (*ChannelBitErrorGet)(AtBerSoftController self);
    uint32 (*BerCurrentDefectGet)(AtBerSoftController self);
    uint32 (*SdDefectMask)(AtBerSoftController self);
    uint32 (*SfDefectMask)(AtBerSoftController self);

    /* Sub class may override this function for other purpose (such as
     * internally control hardware) when SD or SF is detected */
    void (*DefectChanged)(AtBerSoftController self, uint32 changedAlarms, uint32 currentStatus);
    }tAtBerSoftControllerMethods;

/* BER controller abstract class representation */
typedef struct tAtBerSoftController
    {
    tAtBerController super;
    const tAtBerSoftControllerMethods *methods;

    /* BER engine */
    AtBerEngine berEngine;
    AtModule channelModule; /* For fast reference to reduce context switching */

    /* To store history */
    eBool excChanged;
    eBool degChanged;

    /* To cache information when BER engine has not been created but application
     * still configuration */
    uint8 sdThreshold;
    uint8 sfThreshold;

    tAtOsalCurTime previousPollTime;
    }tAtBerSoftController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerSoftController AtBerSoftControllerObjectInit(AtBerSoftController self,
                                                  uint32 controllerId,
                                                  AtChannel channel,
                                                  AtModuleBer berModule);

void AtBerSoftControllerLockChannelModule(AtBerSoftController self);
void AtBerSoftControllerUnLockChannelModule(AtBerSoftController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATBERSOFTCONTROLLERINTERNAL_H_ */


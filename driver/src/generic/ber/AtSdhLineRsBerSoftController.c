/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtSdhLineRsBerSoftController.c
 *
 * Created Date: Sep 30, 2013
 *
 * Description : BER controller for SDH Line MS layer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLineBerSoftControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtBerSoftControllerMethods m_AtBerSoftControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelBitErrorGet(AtBerSoftController self)
    {
    AtChannel line = AtBerControllerMonitoredChannel((AtBerController)self);
    return AtChannelCounterClear(line, cAtSdhLineCounterTypeB1);
    }

static eAtBerChnType ChannelType(AtBerSoftController self)
    {
    eAtSdhLineRate lineRate = AtSdhLineRateGet((AtSdhLine)AtBerControllerMonitoredChannel((AtBerController)self));

    if (lineRate == cAtSdhLineRateStm1)  return cAtBerLineStm1RS;
    if (lineRate == cAtSdhLineRateStm4)  return cAtBerLineStm4RS;
    if (lineRate == cAtSdhLineRateStm16) return cAtBerLineStm16RS;

    return cAtBerMaxChnType;
    }

static uint32 SdDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    return cAtSdhLineAlarmRsBerSd;
    }

static uint32 SfDefectMask(AtBerSoftController self)
    {
	AtUnused(self);
    return cAtSdhLineAlarmRsBerSf;
    }

static void OverrideAtBerSoftController(AtBerController self)
    {
    AtBerSoftController softController = (AtBerSoftController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtBerSoftControllerOverride, mMethodsGet(softController), sizeof(m_AtBerSoftControllerOverride));

        mMethodOverride(m_AtBerSoftControllerOverride, ChannelType);
        mMethodOverride(m_AtBerSoftControllerOverride, ChannelBitErrorGet);
        mMethodOverride(m_AtBerSoftControllerOverride, SdDefectMask);
        mMethodOverride(m_AtBerSoftControllerOverride, SfDefectMask);
        }

    mMethodsSet(softController, &m_AtBerSoftControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideAtBerSoftController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSdhLineRsBerSoftController);
    }

AtBerController AtSdhLineRsBerSoftControllerObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSdhLineBerSoftControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController AtSdhLineRsBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return AtSdhLineRsBerSoftControllerObjectInit(newController, controllerId, channel, berModule);
    }

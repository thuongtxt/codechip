/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtModuleSoftBer.c
 *
 * Created Date: Feb 7, 2013
 *
 * Description : BER module - software solution
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atber/atber.h"
#include "AtModuleSoftBerInternal.h"
#include "AtBerController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleSoftBer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleSoftBerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleBerMethods m_AtModuleBerOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Cache super */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleSoftBer);
    }

/* Manager BER controllers */
static uint32 MaxNumControllersGet(AtModuleSoftBer self)
    {
    return self->maxNumControllers;
    }

static eBool ControllerIsFree(AtModuleSoftBer self, uint32 controllerId)
    {
    if (self->controllers == NULL)
        return cAtFalse;
    return (self->controllers[controllerId] == NULL) ? cAtTrue : cAtFalse;
    }

static uint32 SoftBerMaxNumControllersGet(AtModuleSoftBer self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumControllersGet(self);

    return 0;
    }

static uint32 FreeControllerGet(AtModuleSoftBer self)
    {
    uint32 numControllers, i;

    numControllers = SoftBerMaxNumControllersGet(self);
    for (i = 0; i < numControllers; i++)
        {
        if (ControllerIsFree(self, i))
            return i;
        }

    /* Return invalid controller ID */
    return numControllers;
    }

static void ControllerDelete(AtModuleBer self, AtBerController controller)
    {
    uint32 numControllers, i;
    AtModuleSoftBer softBerModule = (AtModuleSoftBer)self;

    numControllers = SoftBerMaxNumControllersGet(softBerModule);
    for (i = 0; i < numControllers; i++)
        {
        if (softBerModule->controllers[i] != controller)
            continue;

        softBerModule->controllers[i] = NULL;
        AtObjectDelete((AtObject)controller);
        break;
        }
    }

static uint32 TaskPeriod(AtModuleSoftBer self)
    {
	AtUnused(self);
    return cAtBerTimer100ms;
    }

static AtBerModule BerHandleCreate(AtModuleSoftBer self)
    {
    uint32 taskPeriod = mMethodsGet(self)->TaskPeriod(self);
    return AtBerModuleCreate((uint16)SoftBerMaxNumControllersGet(self), taskPeriod, AtModuleBerDefaultOsIntf());
    }

static eBool BerProcessCanRunWithTaskPeriod(AtModuleSoftBer self, uint32 periodInMs)
    {
	AtUnused(self);
    if ((periodInMs == cAtBerTimer100ms) || (periodInMs == cAtBerTimer50ms))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 PeriodProcess(AtModuleBer self, uint32 periodInMs)
    {
    AtModuleSoftBer softBerModule = (AtModuleSoftBer)self;
    tAtOsalCurTime startTime, endTime;
    AtBerModule berHandle = softBerModule->berHandle;
    eBool canRun = mMethodsGet(softBerModule)->BerProcessCanRunWithTaskPeriod(softBerModule, periodInMs);
    uint32 elapseTime;

    AtModuleLock((AtModule)self);

    AtOsalCurTimeGet(&startTime);
    if ((berHandle != NULL) && canRun)
        AtBerModulePeriodProcess(berHandle);

    AtOsalCurTimeGet(&endTime);
    elapseTime = mTimeIntervalInMsGet(startTime, endTime);

    AtModuleUnLock((AtModule)self);
    return elapseTime;
    }

static void AllControllersDelete(AtModuleSoftBer self)
    {
    uint32 i;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->controllers == NULL)
        return;

    /* Delete all controllers */
    for (i = 0; i < SoftBerMaxNumControllersGet(self); i++)
        AtModuleBerControllerDelete((AtModuleBer)self, (AtBerController)(self->controllers[i]));

    /* And the memory that holds them */
    mMethodsGet(osal)->MemFree(osal, self->controllers);
    }

static void AllResourcesDelete(AtModuleSoftBer self)
    {
    AllControllersDelete(self);
    AtBerModuleDelete(self->berHandle);
    }

static eAtRet Setup(AtModule self)
    {
    uint32 memSize;
    AtBerController *controllers;
    AtModuleSoftBer berModule = (AtModuleSoftBer)self;
    AtBerModule berHandle;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Clean first */
    AllResourcesDelete(berModule);

    /* Create BER handle */
    berHandle = BerHandleCreate(berModule);
    if (berHandle == NULL)
        return cAtErrorRsrcNoAvail;
    berModule->berHandle = berHandle;

    /* Create memory for all of controllers */
    memSize = (uint32)(SoftBerMaxNumControllersGet(berModule) * sizeof(AtBerController));
    controllers = mMethodsGet(osal)->MemAlloc(osal, memSize);
    if (controllers == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, controllers, 0, memSize);
    berModule->controllers = controllers;

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    /* Delete private data */
    AllResourcesDelete((AtModuleSoftBer)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static eBool ControllerIdIsValid(AtModuleBer self, uint32 controllerId)
    {
    return (controllerId < SoftBerMaxNumControllersGet((AtModuleSoftBer)self)) ? cAtTrue : cAtFalse;
    }

static AtBerController SdhLineRsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    return AtSdhLineRsBerSoftControllerNew(controllerId, line, self);
    }

static AtBerController SdhLineMsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    return AtSdhLineMsBerSoftControllerNew(controllerId, line, self);
    }

static AtBerController SdhPathBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel path)
    {
    return AtSdhPathBerSoftControllerNew(controllerId, path, self);
    }

static AtBerController SdhLineRsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
    AtModuleSoftBer softBerModule = (AtModuleSoftBer)self;
    uint32 controllerId = FreeControllerGet(softBerModule);
    AtBerController newController;

    if (!ControllerIdIsValid(self, controllerId))
        return NULL;

    newController = mMethodsGet(self)->SdhLineRsBerControlerObjectCreate(self, controllerId, line);
    softBerModule->controllers[controllerId] = newController;

    return newController;
    }

static AtBerController SdhLineMsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
    AtModuleSoftBer softBerModule = (AtModuleSoftBer)self;
    uint32 controllerId = FreeControllerGet(softBerModule);
    AtBerController newController;

    if (!ControllerIdIsValid(self, controllerId))
        return NULL;

    newController = mMethodsGet(self)->SdhLineMsBerControlerObjectCreate(self, controllerId, line);
    softBerModule->controllers[controllerId] = newController;

    return newController;
    }

static AtBerController SdhPathBerControlerCreate(AtModuleBer self, AtChannel path)
    {
    AtModuleSoftBer softBerModule = (AtModuleSoftBer)self;
    uint32 controllerId = FreeControllerGet(softBerModule);
    AtBerController newController;

    if (!ControllerIdIsValid(self, controllerId))
        return NULL;

    newController = mMethodsGet(self)->SdhPathBerControlerObjectCreate(self, controllerId, path);
    softBerModule->controllers[controllerId] = newController;

    return newController;
    }

static const char *CapacityDescription(AtModule self)
    {
    static char string[32];
    AtModuleSoftBer berModule = (AtModuleSoftBer)self;

    AtSprintf(string, "engines: %u", SoftBerMaxNumControllersGet(berModule));
    return string;
    }

static eAtRet DebugEnable(AtModule self, eBool enable)
    {
    AtBerModuleDebugEnable(mThis(self)->berHandle, enable);
    return cAtOk;
    }

static eBool DebugIsEnabled(AtModule self)
    {
    return AtBerModuleDebugIsEnabled(mThis(self)->berHandle);
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);
    AtPrintc(cSevNormal, "- BER component version: %s\r\n", AtBerModuleVersion(mThis(self)->berHandle));
    return ret;
    }

static void OverrideAtModule(AtModuleSoftBer self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, DebugEnable);
        mMethodOverride(m_AtModuleOverride, DebugIsEnabled);
        mMethodOverride(m_AtModuleOverride, Debug);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleBer(AtModuleSoftBer self)
    {
    AtModuleBer berModule = (AtModuleBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(berModule), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, SdhLineRsBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhPathBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, PeriodProcess);
        mMethodOverride(m_AtModuleBerOverride, ControllerDelete);

        mMethodOverride(m_AtModuleBerOverride, SdhLineRsBerControlerObjectCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerObjectCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhPathBerControlerObjectCreate);
        }

    mMethodsSet(berModule, &m_AtModuleBerOverride);
    }

static void OverrideAtObject(AtModuleSoftBer self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleSoftBer self)
    {
    OverrideAtObject(self);
    OverrideAtModuleBer(self);
    OverrideAtModule(self);
    }

static void MethodsInit(AtModuleSoftBer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxNumControllersGet);
        mMethodOverride(m_methods, BerProcessCanRunWithTaskPeriod);
        mMethodOverride(m_methods, TaskPeriod);
        }

    mMethodsSet(self, &m_methods);
    }

AtModuleSoftBer AtModuleSoftBerObjectInit(AtModuleSoftBer self, AtDevice device, uint32 maxNumControllers)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleBerObjectInit((AtModuleBer)self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->maxNumControllers = maxNumControllers;

    return self;
    }

AtBerModule AtModuleBerSwBerHandleGet(AtModuleBer self)
    {
    return ((AtModuleSoftBer)self)->berHandle;
    }

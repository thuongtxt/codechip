/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap Module
 * 
 * File        : AtLapsLinkInternal.h
 * 
 * Created Date: May 24, 2016
 *
 * Description : LAPS Link for HDLC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLAPSLINKINTERNAL_H_
#define _ATLAPSLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtLapsLinkMethods
    {
    eBool  (*TxSapiCanModify)(AtLapsLink self);
    eAtRet (*TxSapiSet)(AtLapsLink self, uint16 value);
    uint16 (*TxSapiGet)(AtLapsLink self);

    eBool  (*SapiMonitorCanEnable)(AtLapsLink self, eBool enable);
    eAtRet (*SapiMonitorEnable)(AtLapsLink self, eBool enable);
    eBool  (*SapiMonitorIsEnabled)(AtLapsLink self);

    eBool  (*ExpectedSapiCanModify)(AtLapsLink self);
    eAtRet (*ExpectedSapiSet)(AtLapsLink self, uint16 value);
    uint16  (*ExpectedSapiGet)(AtLapsLink self);
    }tAtLapsLinkMethods;

typedef struct tAtLapsLink
    {
    tAtHdlcLink super;
    const tAtLapsLinkMethods *methods;
    }tAtLapsLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtLapsLink AtLapsLinkObjectInit(AtLapsLink self, AtHdlcChannel hdlcChannel);

eBool AtLapsLinkTxSapiCanModify(AtLapsLink self);
eBool AtLapsLinkSapiMonitorCanEnable(AtLapsLink self, eBool enable);
eBool AtLapsLinkExpectedSapiCanModify(AtLapsLink self);

#ifdef __cplusplus
}
#endif
#endif /* _ATLAPSLINKINTERNAL_H_ */


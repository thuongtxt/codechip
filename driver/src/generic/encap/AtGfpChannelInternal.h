/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtGfpChannelInternal.h
 * 
 * Created Date: Apr 28, 2014
 *
 * Description : GFP channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATGFPCHANNELINTERNAL_H_
#define _ATGFPCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEncapChannelInternal.h"
#include "AtGfpChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtGfpChannelMethods
    {
    /* PTI */
    eAtModuleEncapRet (*ExpectedPtiSet)(AtGfpChannel self, uint8 expectedPti);
    uint8 (*ExpectedPtiGet)(AtGfpChannel self);
    eAtModuleEncapRet (*PtiMonitorEnable)(AtGfpChannel self, eBool enable);
    eBool (*PtiMonitorIsEnabled)(AtGfpChannel self);
    eAtModuleEncapRet (*TxPtiSet)(AtGfpChannel self, uint8 txPti);
    uint8 (*TxPtiGet)(AtGfpChannel self);

    /* EXI */
    eAtModuleEncapRet (*ExpectedExiSet)(AtGfpChannel self, uint8 expectedExi);
    uint8 (*ExpectedExiGet)(AtGfpChannel self);
    eAtModuleEncapRet (*ExiMonitorEnable)(AtGfpChannel self, eBool enable);
    eBool (*ExiMonitorIsEnabled)(AtGfpChannel self);
    eAtModuleEncapRet (*TxExiSet)(AtGfpChannel self, uint8 txExi);
    uint8 (*TxExiGet)(AtGfpChannel self);

    /* UPI */
    eAtModuleEncapRet (*ExpectedUpiSet)(AtGfpChannel self, uint8 expectedUpi);
    uint8 (*ExpectedUpiGet)(AtGfpChannel self);
    eAtModuleEncapRet (*UpiMonitorEnable)(AtGfpChannel self, eBool enable);
    eBool (*UpiMonitorIsEnabled)(AtGfpChannel self);
    eAtModuleEncapRet (*TxUpiSet)(AtGfpChannel self, uint8 txUpi);
    uint8 (*TxUpiGet)(AtGfpChannel self);

    /* FCS */
    eAtModuleEncapRet (*FcsMonitorEnable)(AtGfpChannel self, eBool enable);
    eBool (*FcsMonitorIsEnabled)(AtGfpChannel self);
    eAtModuleEncapRet (*TxFcsEnable)(AtGfpChannel self, eBool enable);
    eBool (*TxFcsIsEnabled)(AtGfpChannel self);

    /* PFI */
    eAtModuleEncapRet (*TxPfiSet)(AtGfpChannel self, uint8 txPfi);
    uint8 (*TxPfiGet)(AtGfpChannel self);
    uint8 (*RxPfiGet)(AtGfpChannel self);

    /* Frame mode */
    eBool (*FrameModeIsSupported)(AtGfpChannel self, eAtGfpFrameMode frameMappingMode);
    eAtModuleEncapRet (*FrameModeSet)(AtGfpChannel self, eAtGfpFrameMode frameMappingMode);
    eAtGfpFrameMode (*FrameModeGet)(AtGfpChannel self);

    /* CSF */
    eAtModuleEncapRet (*TxCsfEnable)(AtGfpChannel self, eBool enable);
    eBool (*TxCsfIsEnabled)(AtGfpChannel self);
    eAtModuleEncapRet (*TxCsfUpiSet)(AtGfpChannel self, uint8 txUpi);
    uint8 (*TxCsfUpiGet)(AtGfpChannel self);
    uint8 (*RxCsfUpiGet)(AtGfpChannel self);

    /* Scramble */
    eAtModuleEncapRet (*PayloadScrambleEnable)(AtGfpChannel self, eBool enable);
    eBool (*PayloadScrambleIsEnabled)(AtGfpChannel self);
    eAtModuleEncapRet (*CoreHeaderScrambleEnable)(AtGfpChannel self, eBool enable);
    eBool (*CoreHeaderScrambleIsEnabled)(AtGfpChannel self);
    }tAtGfpChannelMethods;

typedef struct tAtGfpChannel
    {
    tAtEncapChannel super;
    const tAtGfpChannelMethods *methods;
    }tAtGfpChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtGfpChannel AtGfpChannelObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module);

#endif /* _ATGFPCHANNELINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtGfpChannel.c
 *
 * Created Date: Apr 28, 2014
 *
 * Description : GFP channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtGfpChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterField, counterType) \
    value = CounterGetFunc(self, counterType); \
    if (gfpCounter)                            \
        gfpCounter->counterField = value;

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtGfpChannelMethods m_methods;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEncapRet ExpectedPtiSet(AtGfpChannel self, uint8 expectedPti)
    {
    AtUnused(self);
    AtUnused(expectedPti);
    return cAtErrorNotImplemented;
    }

static eAtModuleEncapRet ExpectedExiSet(AtGfpChannel self, uint8 expectedExi)
    {
    AtUnused(self);
    AtUnused(expectedExi);
    return cAtErrorNotImplemented;
    }

static eAtModuleEncapRet ExpectedUpiSet(AtGfpChannel self, uint8 expectedUpi)
    {
    AtUnused(self);
    AtUnused(expectedUpi);
    return cAtErrorNotImplemented;
    }

static uint8 ExpectedPtiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 ExpectedExiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint8 ExpectedUpiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModuleEncapRet FcsMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool FcsMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet PtiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PtiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet UpiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool UpiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet ExiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool ExiMonitorIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet TxPtiSet(AtGfpChannel self, uint8 txPti)
    {
    AtUnused(self);
    AtUnused(txPti);
    return cAtErrorNotImplemented;
    }

static uint8 TxPtiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModuleEncapRet TxExiSet(AtGfpChannel self, uint8 txExi)
    {
    AtUnused(self);
    AtUnused(txExi);
    return cAtErrorNotImplemented;
    }

static uint8 TxExiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModuleEncapRet TxUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    AtUnused(self);
    AtUnused(txUpi);
    return cAtErrorNotImplemented;
    }

static uint8 TxUpiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModuleEncapRet TxPfiSet(AtGfpChannel self, uint8 txPfi)
    {
    AtUnused(self);
    AtUnused(txPfi);
    return cAtErrorNotImplemented;
    }

static uint8 TxPfiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eBool FrameModeIsSupported(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    AtUnused(self);
    if ((frameMappingMode == cAtGfpFrameModeGfpF) || (frameMappingMode == cAtGfpFrameModeGfpT))
        return cAtTrue;
    return cAtFalse;
    }

static eBool FrameModeIsValid(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    AtUnused(self);
    if ((frameMappingMode == cAtGfpFrameModeGfpF) ||
        (frameMappingMode == cAtGfpFrameModeGfpT))
        return cAtTrue;
    return cAtFalse;
    }

static eAtModuleEncapRet FrameModeSet(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    AtUnused(self);
    AtUnused(frameMappingMode);
    return cAtErrorNotImplemented;
    }

static eAtGfpFrameMode FrameModeGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModuleEncapRet TxFcsEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool TxFcsIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet TxCsfEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool TxCsfIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet TxCsfUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    AtUnused(self);
    AtUnused(txUpi);
    return cAtErrorNotImplemented;
    }

static uint8 TxCsfUpiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 RxCsfUpiGet(AtGfpChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEncapRet PayloadScrambleEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PayloadScrambleIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEncapRet CoreHeaderScrambleEnable(AtGfpChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool CoreHeaderScrambleIsEnabled(AtGfpChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "gfp";
    }

static eAtRet HeaderDefaultSet(AtGfpChannel self)
    {
    /* User data */
    AtGfpChannelTxPtiSet(self, 0);
    AtGfpChannelExpectedPtiSet(self, 0);

    /* Null extension header */
    AtGfpChannelTxExiSet(self, 0);
    AtGfpChannelExpectedExiSet(self, 0);

    /* Frame-mapped Ethernet */
    AtGfpChannelTxUpiSet(self, 1);
    AtGfpChannelExpectedUpiSet(self, 1);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return HeaderDefaultSet((AtGfpChannel)self);
    }

static eBool CanBindEthFlow(AtEncapChannel self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtTrue;
    }

static eBool PtiIsInRange(AtGfpChannel self, uint8 pti)
    {
    AtUnused(self);
    return (pti <= 0x7);
    }

static eBool ExiIsInRange(AtGfpChannel self, uint8 exi)
    {
    AtUnused(self);
    return (exi <= 0xF);
    }

static eBool PfiIsInRange(AtGfpChannel self, uint8 pfi)
    {
    AtUnused(self);
    return (pfi <= 0x1);
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool clear)
    {
    uint32 value;
    tAtGfpChannelCounters *gfpCounter;
    uint32 (*CounterGetFunc)(AtChannel, uint16) = AtChannelCounterGet;

    /* Initialize counters */
    gfpCounter = (tAtGfpChannelCounters *)pAllCounters;
    if (gfpCounter)
        AtOsalMemInit(gfpCounter, 0, sizeof(tAtGfpChannelCounters));

    if (clear)
        CounterGetFunc = AtChannelCounterClear;

    mPutCounter(txbyte,         cAtGfpChannelCounterTypeTxBytes);
    mPutCounter(txidle,         cAtGfpChannelCounterTypeTxIdleFrames);
    mPutCounter(txgdFrm,        cAtGfpChannelCounterTypeTxGoodFrames);
    mPutCounter(txFrm,          cAtGfpChannelCounterTypeTxFrames);
    mPutCounter(txcsf,          cAtGfpChannelCounterTypeTxCsf);
    mPutCounter(txcmf,          cAtGfpChannelCounterTypeTxCmf);
    mPutCounter(rxbyte,         cAtGfpChannelCounterTypeRxBytes);
    mPutCounter(rxidleFrm,      cAtGfpChannelCounterTypeRxIdleFrames);
    mPutCounter(rxchecUCorrErr, cAtGfpChannelCounterTypeRxCHecError);
    mPutCounter(rxchecCorrErr,  cAtGfpChannelCounterTypeRxCHecCorrected);
    mPutCounter(rxgdFrm,        cAtGfpChannelCounterTypeRxGoodFrames);
    mPutCounter(rxFrm,          cAtGfpChannelCounterTypeRxFrames);
    mPutCounter(rxcsf,          cAtGfpChannelCounterTypeRxCsf);
    mPutCounter(rxcmf,          cAtGfpChannelCounterTypeRxCmf);
    mPutCounter(rxthecCorrErr,  cAtGfpChannelCounterTypeRxTHecCorrected);
    mPutCounter(rxthecUCorrErr, cAtGfpChannelCounterTypeRxTHecError);
    mPutCounter(rxehecErr,      cAtGfpChannelCounterTypeRxEHecError);
    mPutCounter(rxfcsErr,       cAtGfpChannelCounterTypeRxFcsError);
    mPutCounter(rxupm,          cAtGfpChannelCounterTypeRxUpm);
    mPutCounter(rxexm,          cAtGfpChannelCounterTypeRxExm);
    mPutCounter(rxdropFrm,      cAtGfpChannelCounterTypeRxDrops);

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static void OverrideAtChannel(AtGfpChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtGfpChannel self)
    {
    AtEncapChannel encapChannel = (AtEncapChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, mMethodsGet(encapChannel), sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, CanBindEthFlow);
        }

    mMethodsSet(encapChannel, &m_AtEncapChannelOverride);
    }

static void Override(AtGfpChannel self)
    {
    OverrideAtChannel(self);
    OverrideAtEncapChannel(self);
    }

static void MethodsInit(AtGfpChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ExpectedPtiSet);
        mMethodOverride(m_methods, ExpectedExiSet);
        mMethodOverride(m_methods, ExpectedUpiSet);
        mMethodOverride(m_methods, ExpectedPtiGet);
        mMethodOverride(m_methods, ExpectedExiGet);
        mMethodOverride(m_methods, ExpectedUpiGet);
        mMethodOverride(m_methods, FcsMonitorEnable);
        mMethodOverride(m_methods, FcsMonitorIsEnabled);
        mMethodOverride(m_methods, PtiMonitorEnable);
        mMethodOverride(m_methods, PtiMonitorIsEnabled);
        mMethodOverride(m_methods, UpiMonitorEnable);
        mMethodOverride(m_methods, UpiMonitorIsEnabled);
        mMethodOverride(m_methods, ExiMonitorEnable);
        mMethodOverride(m_methods, ExiMonitorIsEnabled);
        mMethodOverride(m_methods, TxPtiSet);
        mMethodOverride(m_methods, TxPtiGet);
        mMethodOverride(m_methods, TxExiSet);
        mMethodOverride(m_methods, TxExiGet);
        mMethodOverride(m_methods, TxUpiSet);
        mMethodOverride(m_methods, TxUpiGet);
        mMethodOverride(m_methods, TxPfiSet);
        mMethodOverride(m_methods, TxPfiGet);
        mMethodOverride(m_methods, FrameModeIsSupported);
        mMethodOverride(m_methods, FrameModeSet);
        mMethodOverride(m_methods, FrameModeGet);
        mMethodOverride(m_methods, TxFcsEnable);
        mMethodOverride(m_methods, TxFcsIsEnabled);
        mMethodOverride(m_methods, TxCsfUpiSet);
        mMethodOverride(m_methods, TxCsfUpiGet);
        mMethodOverride(m_methods, RxCsfUpiGet);
        mMethodOverride(m_methods, TxCsfEnable);
        mMethodOverride(m_methods, TxCsfIsEnabled);
        mMethodOverride(m_methods, PayloadScrambleEnable);
        mMethodOverride(m_methods, PayloadScrambleIsEnabled);
        mMethodOverride(m_methods, CoreHeaderScrambleEnable);
        mMethodOverride(m_methods, CoreHeaderScrambleIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtGfpChannel);
    }

AtGfpChannel AtGfpChannelObjectInit(AtGfpChannel self, uint32 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEncapChannelObjectInit((AtEncapChannel)self, channelId, cAtEncapGfp, (AtModule) module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

eAtModuleEncapRet AtGfpChannelFcsCheck(AtGfpChannel self, eBool check)
    {
    return AtGfpChannelFcsMonitorEnable(self, check);
    }

eBool AtGfpChannelFcsIsChecked(AtGfpChannel self)
    {
    return AtGfpChannelFcsMonitorIsEnabled(self);
    }

eAtModuleEncapRet AtGfpChannelPtiCheck(AtGfpChannel self, eBool check)
    {
    return AtGfpChannelPtiMonitorEnable(self, check);
    }

eBool AtGfpChannelPtiIsChecked(AtGfpChannel self)
    {
    return AtGfpChannelPtiMonitorIsEnabled(self);
    }

eAtModuleEncapRet AtGfpChannelUpiCheck(AtGfpChannel self, eBool check)
    {
    return AtGfpChannelUpiMonitorEnable(self, check);
    }

eBool AtGfpChannelUpiIsChecked(AtGfpChannel self)
    {
    return AtGfpChannelUpiMonitorIsEnabled(self);
    }

eAtGfpFcsMode AtGfpChannelFcsModeGet(AtGfpChannel self)
    {
    return (AtGfpChannelTxFcsIsEnabled(self)) ? cAtGfpFcsModeFcs32 : cAtGfpFcsModeNoFcs;
    }

eAtModuleEncapRet AtGfpChannelFcsModeSet(AtGfpChannel self,eAtGfpFcsMode fcsMode)
    {
    if (fcsMode == cAtGfpFcsModeFcs32)
        return AtGfpChannelTxFcsEnable(self, cAtTrue);
    if (fcsMode == cAtGfpFcsModeNoFcs)
        return AtGfpChannelTxFcsEnable(self, cAtFalse);
    return cAtErrorInvlParm;
    }

eAtModuleEncapRet AtGfpChannelTxCsfForce(AtGfpChannel self, eBool enable, eAtGfpCsfType csfType)
    {
    eAtModuleEncapRet ret = cAtOk;
    uint8 txUpi = 0;

    if (csfType == cAtGfpLossOfSync)
        txUpi = 1;
    else if (csfType == cAtGfpLossOfClienSignal)
        txUpi = 2;

    ret = AtGfpChannelTxCsfUpiSet(self, txUpi);
    ret |= AtGfpChannelTxCsfEnable(self, enable);
    return ret;
    }

eBool AtGfpChannelTxCsfForceGet(AtGfpChannel self, eAtGfpCsfType *csfType)
    {
    *csfType = (AtGfpChannelTxCsfUpiGet(self) == 1) ? cAtGfpLossOfSync : cAtGfpLossOfClienSignal;
    return AtGfpChannelTxCsfIsEnabled(self);
    }

/**
 * @addtogroup AtGfpChannel
 * @{
 */

/**
 * Set expected PTI
 *
 * @param self This channel
 * @param expectedPti Expected PTI
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelExpectedPtiSet(AtGfpChannel self, uint8 expectedPti)
    {
    if (!PtiIsInRange(self, expectedPti))
        return cAtErrorOutOfRangParm;

    mNumericalAttributeSet(ExpectedPtiSet, expectedPti);
    }

/**
 * Set expected EXI
 *
 * @param self This channel
 * @param expectedExi Expected EXI
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelExpectedExiSet(AtGfpChannel self, uint8 expectedExi)
    {
    if (!ExiIsInRange(self, expectedExi))
        return cAtErrorOutOfRangParm;

    mNumericalAttributeSet(ExpectedExiSet, expectedExi);
    }

/**
 * Set expected UPI
 *
 * @param self This channel
 * @param expectedUpi Expected UPI
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelExpectedUpiSet(AtGfpChannel self, uint8 expectedUpi)
    {
    mNumericalAttributeSet(ExpectedUpiSet, expectedUpi);
    }

/**
 * Get expected PTI
 *
 * @param self This channel
 *
 * @return Expected PTI
 */
uint8 AtGfpChannelExpectedPtiGet(AtGfpChannel self)
    {
    mAttributeGet(ExpectedPtiGet, uint8, 0);
    }

/**
 * Get expected EXI
 *
 * @param self This channel
 *
 * @return Expected PTI
 */
uint8 AtGfpChannelExpectedExiGet(AtGfpChannel self)
    {
    mAttributeGet(ExpectedExiGet, uint8, 0);
    }

/**
 * Get expected UPI
 *
 * @param self This channel
 *
 * @return Expected UPI
 */
uint8 AtGfpChannelExpectedUpiGet(AtGfpChannel self)
    {
    mAttributeGet(ExpectedUpiGet, uint8, 0);
    }

/**
 * Enable/Disable FCS monitoring.
 *
 * @param self This channel
 * @param enable FCS monitoring enable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelFcsMonitorEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(FcsMonitorEnable, enable);
    }

/**
 * Check if FCS monitoring is enabled or disabled
 *
 * @param self This channel
 *
 * @retval cAtTrue if FCS monitoring is enabled
 * @retval cAtFalse if FCS monitoring is disabled
 */
eBool AtGfpChannelFcsMonitorIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(FcsMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/Disable PTI monitoring.
 *
 * @param self This channel
 * @param enable PTI monitoring enable.
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelPtiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(PtiMonitorEnable, enable);
    }

/**
 * Check if PTI monitoring is enabled or disabled
 *
 * @param self This channel
 *
 * @retval cAtTrue if PTI monitoring is enabled
 * @retval cAtFalse if PTI monitoring is disabled
 */
eBool AtGfpChannelPtiMonitorIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(PtiMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/Disable UPI monitoring
 *
 * @param self This channel
 * @param enable UPI monitoring enable.
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelUpiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(UpiMonitorEnable, enable);
    }

/**
 * Check if UPI monitoring is enabled or not
 *
 * @param self This channel
 *
 * @retval cAtTrue if UPI monitoring is enabled
 * @retval cAtFalse if UPI monitoring is disabled
 */
eBool AtGfpChannelUpiMonitorIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(UpiMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/Disable EXI monitoring
 *
 * @param self This channel
 * @param enable EXI monitoring enable.
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelExiMonitorEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(ExiMonitorEnable, enable);
    }

/**
 * Check if EXI monitoring is enabled or not
 *
 * @param self This channel
 *
 * @retval cAtTrue if EXI monitoring is enabled
 * @retval cAtFalse if EXI monitoring is disabled
 */
eBool AtGfpChannelExiMonitorIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(ExiMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Set transmitted PTI
 *
 * @param self This channel
 * @param txPti TX PTI
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelTxPtiSet(AtGfpChannel self, uint8 txPti)
    {
    if (!PtiIsInRange(self, txPti))
        return cAtErrorOutOfRangParm;

    mNumericalAttributeSet(TxPtiSet, txPti);
    }

/**
 * Get transmitted PTI
 *
 * @param self This channel
 *
 * @return Transmitted PTI
 */
uint8 AtGfpChannelTxPtiGet(AtGfpChannel self)
    {
    mAttributeGet(TxPtiGet, uint8, 0);
    }

/**
 * Set transmitted EXI value
 *
 * @param self This channel
 * @param txExi EXI value
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelTxExiSet(AtGfpChannel self, uint8 txExi)
    {
    if (!ExiIsInRange(self, txExi))
        return cAtErrorOutOfRangParm;

    mNumericalAttributeSet(TxExiSet, txExi);
    }

/**
 * Get transmitted EXI value
 *
 * @param self This channel
 *
 * @return Transmitted EXI
 */
uint8 AtGfpChannelTxExiGet(AtGfpChannel self)
    {
    mAttributeGet(TxExiGet, uint8, 0);
    }

/**
 * Set transmitted UPI value
 *
 * @param self This channel
 * @param txUpi UPI value
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelTxUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    mNumericalAttributeSet(TxUpiSet, txUpi);
    }

/**
 * Get transmitted UPI value
 *
 * @param self This channel
 *
 * @return Transmitted UPI
 */
uint8 AtGfpChannelTxUpiGet(AtGfpChannel self)
    {
    mAttributeGet(TxUpiGet, uint8, 0);
    }

/**
 * Set transmitted PFI value
 *
 * @param self This channel
 * @param txPfi PFI value
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelTxPfiSet(AtGfpChannel self, uint8 txPfi)
    {
    if (!PfiIsInRange(self, txPfi))
        return cAtErrorOutOfRangParm;

    mNumericalAttributeSet(TxPfiSet, txPfi);
    }

/**
 * Get transmitted PFI value
 *
 * @param self This channel
 *
 * @return Transmitted PFI
 */
uint8 AtGfpChannelTxPfiGet(AtGfpChannel self)
    {
    mAttributeGet(TxPfiGet, uint8, 0);
    }

/**
 * Check if FCS is transmitted or not.
 *
 * @param self This channel
 *
 * @retval cAtTrue if FCS is transmitted
 * @retval cAtFalse if FCS is not transmitted
 */
eBool AtGfpChannelTxFcsIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(TxFcsIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable transmit FCS
 *
 * @param self This channel
 * @param enable FCS transmission enable.
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelTxFcsEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(TxFcsEnable, enable);
    }

/**
 * Get frame mode
 *
 * @param self This channel
 *
 * @return @ref eAtGfpFrameMode "frame mode"
 */
eAtGfpFrameMode AtGfpChannelFrameModeGet(AtGfpChannel self)
    {
    mAttributeGet(FrameModeGet, eAtGfpFrameMode, cAtGfpFrameModeUnknown);
    }

/**
 * Set frame mode
 *
 * @param self This channel
 * @param frameMappingMode @ref eAtGfpFrameMode "frame mode"
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelFrameModeSet(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    mNullObjectCheck(cAtErrorObjectNotExist);

    if (!FrameModeIsValid(self, frameMappingMode))
        return cAtErrorInvlParm;

    if (!AtGfpChannelFrameModeIsSupported(self, frameMappingMode))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->FrameModeSet(self, frameMappingMode);
    }

/**
 * Check if a specified frame mode is supported or not
 *
 * @param self This channel
 * @param frameMappingMode Frame mode to check. See @ref eAtGfpFrameMode "Frame modes"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtGfpChannelFrameModeIsSupported(AtGfpChannel self, eAtGfpFrameMode frameMappingMode)
    {
    mOneParamAttributeGet(FrameModeIsSupported, frameMappingMode, eBool, cAtFalse);
    }

/**
 * Enable/disable Payload scambling
 *
 * @param self This channel
 * @param enable Scramble enabling. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelPayloadScrambleEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(PayloadScrambleEnable, enable);
    }

/**
 * Check if payload scrambling is enabled or disabled
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtGfpChannelPayloadScrambleIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(PayloadScrambleIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable Core Header scambling
 *
 * @param self This channel
 * @param enable Scramble enabling. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelCoreHeaderScrambleEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(CoreHeaderScrambleEnable, enable);
    }

/**
 * Check if Core Header scrambling is enabled or disabled
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtGfpChannelCoreHeaderScrambleIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(CoreHeaderScrambleIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable transmit GFP CSF. Once GFP CSF transmission is enabled, one
 * GFP CSF frame will be repeatedly transmitted for each 500ms.
 *
 * @param self This channel
 * @param enable GFP CSF transmit enable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelTxCsfEnable(AtGfpChannel self, eBool enable)
    {
    mNumericalAttributeSet(TxCsfEnable, enable);
    }

/**
 * Check if GFP CSF is enabled to be transmitted or not.
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled to be transmitted
 * @retval cAtFalse if disabled to be transmitted
 */
eBool AtGfpChannelTxCsfIsEnabled(AtGfpChannel self)
    {
    mAttributeGet(TxCsfIsEnabled, eBool, cAtFalse);
    }

/**
 * Set the transmitted UPI value in GFP CSF frame.
 *
 * @param self This channel
 * @param txUpi UPI value in transmit CSF frame.
 *
 * @return AT return code
 */
eAtModuleEncapRet AtGfpChannelTxCsfUpiSet(AtGfpChannel self, uint8 txUpi)
    {
    mNumericalAttributeSet(TxCsfUpiSet, txUpi);
    }

/**
 * Get the transmitted UPI value in GFP CSF frame.
 *
 * @param self This channel
 *
 * @return Transmitted UPI value in CSF frame.
 */
uint8 AtGfpChannelTxCsfUpiGet(AtGfpChannel self)
    {
    mAttributeGet(TxCsfUpiGet, uint8, 0);
    }

/**
 * Get the received UPI value in incoming GFP CSF frames.
 *
 * @param self This channel
 *
 * @return Received UPI value in CSF frame.
 */
uint8 AtGfpChannelRxCsfUpiGet(AtGfpChannel self)
    {
    mAttributeGet(RxCsfUpiGet, uint8, 0);
    }

/**
 * @}
 */

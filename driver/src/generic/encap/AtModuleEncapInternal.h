/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtModuleEncapInternal.h
 * 
 * Created Date: Aug 2, 2012
 *
 * Description : ENCAP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AtMODULEENCAPINTERNAL_H_
#define _AtMODULEENCAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"
#include "AtPppLink.h"
#include "AtFrLink.h"
#include "AtLapsLink.h"
#include "../man/AtModuleInternal.h"
#include "AtHdlcBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Class methods */
typedef struct tAtModuleEncapMethods
    {
    /* Channel factory */
    uint16 (*MaxChannelsGet)(AtModuleEncap self);
    AtHdlcChannel (*HdlcPppChannelCreate)(AtModuleEncap self, uint32 channelId);
    AtHdlcChannel (*FrChannelCreate)(AtModuleEncap self, uint32 channelId);
    AtHdlcChannel (*CiscoHdlcChannelCreate)(AtModuleEncap self, uint32 channelId);
    AtAtmTc (*AtmTcCreate)(AtModuleEncap self, uint32 channelId);
    AtGfpChannel (*GfpChannelCreate)(AtModuleEncap self, uint32 channelId);

    /* Channel management */
    AtEncapChannel (*ChannelGet)(AtModuleEncap self, uint32 channelId);
    eAtRet (*ChannelDelete)(AtModuleEncap self, uint32 channelId);
    uint16 (*FreeChannelGet)(AtModuleEncap self);

    /* Capacity */
    eBool (*GfpIsSupported)(AtModuleEncap self);

    /* ISIS MAC translation */
    eAtRet (*HdlcISISMacSet)(AtModuleEncap self, const uint8 *mac);
    eAtRet (*HdlcISISMacGet)(AtModuleEncap self, uint8 *mac);

    /* Idle pattern */
    eAtRet (*IdlePatternSet)(AtModuleEncap self, uint8 pattern);
    uint8 (*IdlePatternGet)(AtModuleEncap self);

    /* Internal methods */
    AtHdlcChannel (*HdlcPppChannelObjectCreate)(AtModuleEncap self, uint32 channelId);
    AtHdlcChannel (*FrChannelObjectCreate)(AtModuleEncap self, uint32 channelId);
    AtHdlcChannel (*CiscoHdlcChannelObjectCreate)(AtModuleEncap self, uint32 channelId);

    AtPppLink (*PppLinkObjectCreate)(AtModuleEncap self, AtHdlcChannel hdlcChannel);
    AtAtmTc (*AtmTcObjectCreate)(AtModuleEncap self, uint32 channelId);
    AtHdlcLink (*CiscoHdlcLinkObjectCreate)(AtModuleEncap self, AtHdlcChannel hdlcChannel);
    AtHdlcLink (*FrLinkObjectCreate)(AtModuleEncap self, AtHdlcChannel hdlcChannel);
    AtGfpChannel (*GfpChannelObjectCreate)(AtModuleEncap self, uint32 channelId);

    /* For LAPS */
    AtHdlcChannel (*HdlcLapsChannelObjectCreate)(AtModuleEncap self, uint32 channelId);
    AtLapsLink (*LapsLinkObjectCreate)(AtModuleEncap self, AtHdlcChannel hdlcChannel);

    /* Bundle management */
    uint32 (*MaxBundlesGet)(AtModuleEncap self);
    AtMfrBundle (*MfrBundleCreate)(AtModuleEncap self, uint32 bundleId);
    AtMfrBundle (*MfrBundleObjectCreate)(AtModuleEncap self, uint32 bundleId);
    AtMpBundle (*MpBundleCreate)(AtModuleEncap self, uint32 bundleId);
    AtMpBundle (*MpBundleObjectCreate)(AtModuleEncap self, uint32 bundleId);
    eAtRet (*BundleDelete)(AtModuleEncap self, uint32 bundleId);
    AtHdlcBundle (*BundleGet)(AtModuleEncap self, uint32 bundleId);

    /* Debug */
    void (*EncapChannelRegsShow)(AtModuleEncap self, AtEncapChannel channel);
    void (*HdlcBundleRegsShow)(AtModuleEncap self, AtHdlcBundle bundle);
    }tAtModuleEncapMethods;

/* Encapsulation module */
typedef struct tAtModuleEncap
    {
    tAtModule super;
    const tAtModuleEncapMethods *methods;

    /* Private variable */
    AtEncapChannel *channels; /* To cache created channel so do not need to create Line object every time application refer one of them */
    AtHdlcBundle *bundles; /* Bundles supported by this module */
    }tAtModuleEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap AtModuleEncapObjectInit(AtModuleEncap self, AtDevice device);

AtHdlcLink AtModuleEncapLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChanel, uint8 frameType);
AtGfpChannel AtModuleEncapGfpChannelObjectCreate(AtModuleEncap self, uint32 channelId);
AtHdlcChannel AtModuleEncapHdlcLapsChannelObjectCreate(AtModuleEncap self, uint32 channelId);
AtLapsLink AtModuleEncapLapsLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _AtMODULEENCAPINTERNAL_H_ */

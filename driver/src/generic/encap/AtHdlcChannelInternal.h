/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : AtHdlcChannelInternal.h
 * 
 * Created Date: Sep 1, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCCHANNELINTERNAL_H_
#define _ATHDLCCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcChannel.h"
#include "AtEncapChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtHdlcChannelMethods
    {
    /* Address field */
    eAtRet (*AddressSizeSet)(AtHdlcChannel self, uint8 numberOfAddressByte);
    uint8 (*AddressSizeGet)(AtHdlcChannel self);

    eBool (*AddressCanCompare)(AtHdlcChannel self, eBool compareEnable);
    eAtRet (*AddressMonitorEnable)(AtHdlcChannel self, eBool enable);
    eBool (*AddressMonitorIsEnabled)(AtHdlcChannel self);
    eAtRet (*ExpectedAddressSet)(AtHdlcChannel self, uint8 *address);
    eAtRet (*ExpectedAddressGet)(AtHdlcChannel self, uint8 *address);

    eBool (*AddressCanInsert)(AtHdlcChannel self, eBool insertEnable);
    eAtRet (*AddressInsertionEnable)(AtHdlcChannel self, eBool enable);
    eBool (*AddressInsertionIsEnabled)(AtHdlcChannel self);
    eAtRet (*TxAddressSet)(AtHdlcChannel self, uint8 *address);
    eAtRet (*TxAddressGet)(AtHdlcChannel self, uint8 *address);

    /* Control field */
    eAtRet (*ControlSizeSet)(AtHdlcChannel self, uint8 numberOfByte);
    uint8 (*ControlSizeGet)(AtHdlcChannel self);

    eBool (*ControlCanCompare)(AtHdlcChannel self, eBool compareEnable);
    eAtRet (*ControlMonitorEnable)(AtHdlcChannel self, eBool enable);
    eBool (*ControlMonitorIsEnabled)(AtHdlcChannel self);
    eAtRet (*ExpectedControlSet)(AtHdlcChannel self, uint8 *control);
    eAtRet (*ExpectedControlGet)(AtHdlcChannel self, uint8 *control);

    eBool (*ControlCanInsert)(AtHdlcChannel self, eBool insertEnable);
    eAtRet (*ControlInsertionEnable)(AtHdlcChannel self, eBool enable);
    eBool (*ControlInsertionIsEnabled)(AtHdlcChannel self);
    eAtRet (*TxControlSet)(AtHdlcChannel self, uint8 *control);
    eAtRet (*TxControlGet)(AtHdlcChannel self, uint8 *control);

    /* Address/control bypassing to Ethernet side */
    eBool (*AddressControlCanBypass)(AtHdlcChannel self, eBool bypass);
    eAtRet (*AddressControlBypass)(AtHdlcChannel self, eBool bypass);
    eBool (*AddressControlIsBypassed)(AtHdlcChannel self);

    /* Address/control compressing */
    eBool (*AddressControlCanCompress)(AtHdlcChannel self, eBool compress);
    eAtRet (*TxAddressControlCompress)(AtHdlcChannel self, eBool compress);
    eBool (*TxAddressControlIsCompressed)(AtHdlcChannel self);
    eAtRet (*RxAddressControlCompress)(AtHdlcChannel self, eBool compress);
    eBool (*RxAddressControlIsCompressed)(AtHdlcChannel self);

    /* FCS controlling */
    eAtRet (*FcsModeSet)(AtHdlcChannel self, eAtHdlcFcsMode fcsMode);
    eAtHdlcFcsMode (*FcsModeGet)(AtHdlcChannel self);
    eAtHdlcFcsMode (*FcsModeDefault)(AtHdlcChannel self);
    eBool (*FcsModeIsSupported)(AtHdlcChannel self, eAtHdlcFcsMode fcsMode);
    eAtRet (*FcsCalculationModeSet)(AtHdlcChannel self, eAtHdlcFcsCalculationMode mode);
    eAtHdlcFcsCalculationMode (*FcsCalculationModeGet)(AtHdlcChannel self);

    /* Other attributes */
    eAtRet (*FlagSet)(AtHdlcChannel self, uint8 flag);
    uint8 (*FlagGet)(AtHdlcChannel self);
    eBool (*FlagIsSupported)(AtHdlcChannel self, uint8 flag);
    eAtRet (*NumFlagsSet)(AtHdlcChannel self, uint8 numFlag);
    uint8 (*NumFlagsGet)(AtHdlcChannel self);

    eAtRet (*StuffModeSet)(AtHdlcChannel self, eAtHdlcStuffMode stuffMode);
    eAtHdlcStuffMode (*StuffModeGet)(AtHdlcChannel self);
    eAtHdlcStuffMode (*StuffModeDefault)(AtHdlcChannel self);
    eBool (*StuffModeIsSupported)(AtHdlcChannel self, eAtHdlcStuffMode stuffMode);

    eAtRet (*MtuSet)(AtHdlcChannel self, uint32 mtu);
    uint32 (*MtuGet)(AtHdlcChannel self);
    eBool (*MtuIsSupported)(AtHdlcChannel self);

    /* Methods */
    AtHdlcLink (*HdlcLinkGet)(AtHdlcChannel self);
    eAtRet (*HdlcLinkSet)(AtHdlcChannel self, AtHdlcLink link);
    eAtHdlcFrameType (*FrameTypeGet)(AtHdlcChannel self);
    eAtRet (*FrameTypeSet)(AtHdlcChannel self, uint8 frameType);

    eAtModuleEncapRet (*IdlePatternSet)(AtHdlcChannel self, uint8 idle);
    uint8 (*IdlePatternGet)(AtHdlcChannel self);
    }tAtHdlcChannelMethods;

/* HDLC channel object */
typedef struct tAtHdlcChannel
    {
    tAtEncapChannel super;
    const tAtHdlcChannelMethods *methods;

    /* Private data */
    AtHdlcLink link;
    uint8 frameType; /* eAtHdlcFrameType - Frame type */
    }tAtHdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel AtHdlcChannelObjectInit(AtHdlcChannel self,
                                      uint32 channelId,
                                      eAtHdlcFrameType frameType,
                                      AtModule module);

/* Access private data */
void AtHdlcChannelHdlcLinkSet(AtHdlcChannel self, AtHdlcLink link);

/* Capability */
eBool AtHdlcChannelAddressControlCanCompress(AtHdlcChannel self, eBool compress);
eBool AtHdlcChannelAddressCanInsert(AtHdlcChannel self, eBool insertEnable);
eBool AtHdlcChannelControlCanInsert(AtHdlcChannel self, eBool insertEnable);
eBool AtHdlcChannelAddressCanCompare(AtHdlcChannel self, eBool compareEnable);
eBool AtHdlcChannelControlCanCompare(AtHdlcChannel self, eBool compareEnable);
eBool AtHdlcChannelAddressControlCanBypass(AtHdlcChannel self, eBool bypass);
eBool AtHdlcChannelFlagIsSupported(AtHdlcChannel self, uint8 flag);
eBool AtHdlcChannelFcsModeIsSupported(AtHdlcChannel self, eAtHdlcFcsMode fcsMode);
eBool AtHdlcChannelStuffModeIsSupported(AtHdlcChannel self, eAtHdlcStuffMode stuffMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCCHANNELINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtAtmTc.c
 *
 * Created Date: Sep 27, 2012
 *
 * Description : ATM TC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEncapInternal.h"
#include "AtAtmTcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtAtmTcMethods m_methods;
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "atmtc";
    }

static eAtRet NetworkInterfaceTypeSet(AtAtmTc self, eAtAtmNetworkInterfaceType interfaceType)
    {
	AtUnused(interfaceType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
static eAtAtmNetworkInterfaceType NetworkInterfaceTypeGet(AtAtmTc self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet CellMappingModeSet(AtAtmTc self, eAtAtmCellMappingMode mapMode)
    {
	AtUnused(mapMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtAtmCellMappingMode CellMappingModeGet(AtAtmTc self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static void MethodsInit(AtAtmTc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NetworkInterfaceTypeSet);
        mMethodOverride(m_methods, NetworkInterfaceTypeGet);
        mMethodOverride(m_methods, CellMappingModeSet);
        mMethodOverride(m_methods, CellMappingModeGet);
        }
    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtAtmTc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtAtmTc self)
    {
    OverrideAtChannel(self);
    }

AtAtmTc AtAtmTcObjectInit(AtAtmTc self, uint16 channelId, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtAtmTc));

    /* Super constructor */
    if (AtEncapChannelObjectInit((AtEncapChannel)self, (uint16)channelId, cAtEncapAtm, (AtModule) module) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit(self);
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * Set ATM network interface type(NNI/UNI)
 *
 * @param self ATM TC
 * @param interfaceType Interface type
 * @return AT return code
 */
/* Network interface type */
eAtModuleEncapRet AtAtmTcNetworkInterfaceTypeSet(AtAtmTc self, eAtAtmNetworkInterfaceType interfaceType)
    {
    mNumericalAttributeSet(NetworkInterfaceTypeSet, interfaceType);
    }

/**
 * Get ATM network interface type(NNI/UNI)
 *
 * @param self ATM TC
 * @return @ref eAtAtmNetworkInterfaceType "ATM network interface type"
 */
eAtAtmNetworkInterfaceType AtAtmTcNetworkInterfaceTypeGet(AtAtmTc self)
    {
    mAttributeGet(NetworkInterfaceTypeGet, eAtAtmNetworkInterfaceType, cAtAtmNetworkInterfaceTypeInvalid);
    }

/**
 * Set ATM cell mapping mode
 *
 * @param self ATM TC
 * @param mapMode Cell mapping mode
 * @return AT return code
 */
eAtModuleEncapRet AtAtmTcCellMappingModeSet(AtAtmTc self, eAtAtmCellMappingMode mapMode)
    {
    mNumericalAttributeSet(CellMappingModeSet, mapMode);
    }
/**
 * Get ATM cell mapping mode
 *
 * @param self ATM TC
 * @return @ref eAtAtmCellMappingMode "ATM cell mapping mode"
 */
eAtAtmCellMappingMode AtAtmTcCellMappingModeGet(AtAtmTc self)
    {
    mAttributeGet(CellMappingModeGet, eAtAtmCellMappingMode, cAtAtmCellMappingModeInvalid);
    }

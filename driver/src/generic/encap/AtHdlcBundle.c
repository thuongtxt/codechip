/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : AtHdlcBundle.c
 *
 * Created Date: Aug 3, 2012
 *
 * Description : 
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModuleEncapInternal.h"
#include "AtHdlcLinkInternal.h"
#include "AtHdlcBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtHdlcBundle)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtHdlcBundleMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtObjectMethods m_AtObjectOverride;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet HdlcBundleAllLinksDelete(AtHdlcBundle self)
    {
    if (self->links == NULL)
        return cAtOk;

    /* Delete list stores all links */
    AtObjectDelete((AtObject)(self->links));
    self->links = NULL;

    return cAtOk;
    }

static eBool ShouldDeletePwOnCleanup(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "hdlc_bundle";
    }

static eAtRet HdlcBundleAllLinksAllocate(AtHdlcBundle self)
    {
    uint8 maxNumLinkInbundle;

    /* Get maximum number of link in bundle */
    maxNumLinkInbundle = mMethodsGet(self)->MaxNumLinkInBundleGet(self);

    /* Create list to cache HDLC links */
    self->links = AtListCreate(maxNumLinkInbundle);
    if (self->links == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    ret  = HdlcBundleAllLinksDelete((AtHdlcBundle)self);
    ret |= HdlcBundleAllLinksAllocate((AtHdlcBundle)self);

    return ret;
    }

static void Delete(AtObject self)
    {
    AtHdlcBundle bundle = (AtHdlcBundle)self;

    /* Delete all of links */
    HdlcBundleAllLinksDelete(bundle);

    if (bundle->oamFlow != NULL)
        {
        mMethodsGet(bundle)->OamFlowDelete(bundle);
        bundle->oamFlow = NULL;
        }

    /* Call super to fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtHdlcBundle object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescriptionList(links);
    mEncodeUInt(isValid);
    mEncodeObject(oamFlow);
    }

static eAtRet MaxDelaySet(AtHdlcBundle self, uint8 maxDelay)
    {
	AtUnused(maxDelay);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 MaxDelayGet(AtHdlcBundle self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet FragmentSizeSet(AtHdlcBundle self, eAtFragmentSize fragmentSize)
    {
	AtUnused(fragmentSize);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtFragmentSize FragmentSizeGet(AtHdlcBundle self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtNoFragment;
    }

static eAtRet LinkRemove(AtHdlcBundle self, AtHdlcLink link)
    {
    AtHdlcLinkBundleSet(link, NULL);
    return AtListObjectRemove(self->links, (AtObject)link);
    }

static eAtRet LinkAdd(AtHdlcBundle self, AtHdlcLink link)
    {
    /* Check if this link belongs to another bundle */
    if (AtHdlcLinkBundleGet(link) != NULL)
        return cAtErrorChannelBusy;

    /* It is available, add it */
    AtHdlcLinkBundleSet(link, self);
    return AtListObjectAdd(self->links, (AtObject)link);
    }

static uint16 LinksGet(AtHdlcBundle self, AtHdlcLink *links)
    {
    uint16 wIndex;
    uint32 numLinkInBundle;

    /* check if link not existing */
    if (links == NULL)
        return cAtErrorInvlParm;

    numLinkInBundle = AtListLengthGet(self->links);
    for (wIndex = 0; wIndex < numLinkInBundle; wIndex++)
        links[wIndex] = (AtHdlcLink)AtListObjectGet(self->links, wIndex);

    return (uint16)numLinkInBundle;
    }

static uint8 MaxNumLinkInBundleGet(AtHdlcBundle self)
    {
	AtUnused(self);
    /* Default implement will return 32 */
    return 32;
    }

static AtEthFlow OamFlowGet(AtHdlcBundle self)
    {
    if (self->oamFlow == NULL)
        self->oamFlow = mMethodsGet(self)->OamFlowCreate(self);
    return self->oamFlow;
    }

static void OamFlowDelete(AtHdlcBundle self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return;
    }

static AtEthFlow OamFlowCreate(AtHdlcBundle self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtRet SchedulingModeSet(AtHdlcBundle self, eAtHdlcBundleSchedulingMode schedulingMode)
    {
    AtUnused(schedulingMode);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtHdlcBundleSchedulingMode SchedulingModeGet(AtHdlcBundle self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtHdlcBundleSchedulingModeRandom;
    }

static eAtModulePwRet PduTypeSet(AtHdlcBundle self, eAtHdlcPduType pduType)
    {
    AtUnused(self);
    AtUnused(pduType);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtHdlcPduType PduTypeGet(AtHdlcBundle self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtHdlcPduTypeAny;
    }

static uint8 DefaultMaxLinkDelay(AtHdlcBundle self)
    {
    AtUnused(self);
    return 128;
    }

static void MethodsInit(AtHdlcBundle self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxDelaySet);
        mMethodOverride(m_methods, MaxDelayGet);
        mMethodOverride(m_methods, FragmentSizeSet);
        mMethodOverride(m_methods, FragmentSizeGet);
        mMethodOverride(m_methods, LinkRemove);
        mMethodOverride(m_methods, LinkAdd);
        mMethodOverride(m_methods, LinksGet);
        mMethodOverride(m_methods, MaxNumLinkInBundleGet);
        mMethodOverride(m_methods, OamFlowDelete);
        mMethodOverride(m_methods, OamFlowCreate);
        mMethodOverride(m_methods, SchedulingModeSet);
        mMethodOverride(m_methods, SchedulingModeGet);
        mMethodOverride(m_methods, PduTypeSet);
        mMethodOverride(m_methods, PduTypeGet);
        mMethodOverride(m_methods, DefaultMaxLinkDelay);
        }
    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtHdlcBundle self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtHdlcBundle self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, ShouldDeletePwOnCleanup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtHdlcBundle self)
    {
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

AtHdlcBundle AtHdlcBundleObjectInit(AtHdlcBundle self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtHdlcBundle));

    /* Super constructor */
    if (AtEncapBundleObjectInit((AtEncapBundle)self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

void AtHdlcBundleValidate(AtHdlcBundle self, eBool isValid)
    {
    self->isValid = isValid;
    }

eBool AtHdlcBundleIsValid(AtHdlcBundle self)
    {
    if (self == NULL)
        return cAtFalse;
        
    return (self->isValid) ? cAtTrue : cAtFalse;
    }

void AtHdlcBundleOamFlowSet(AtHdlcBundle self, AtEthFlow oamFlow)
    {
    if (self)
        self->oamFlow = oamFlow;
    }

uint8 AtHdlcBundleDefaultMaxLinkDelay(AtHdlcBundle self)
    {
    if (self)
        return mMethodsGet(self)->DefaultMaxLinkDelay(self);
    return 0;
    }

/**
 * @addtogroup AtHdlcBundle
 * @{
 */

/**
 * Remove link from bundle
 *
 * @param self Bundle
 * @param linkId Link Id
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcBundleLinkRemove(AtHdlcBundle self, AtHdlcLink linkId)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->LinkRemove(self, linkId);
        
    return cAtError;
    }

/**
 * Add link to bundle
 *
 * @param self Bundle
 * @param link Link
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcBundleLinkAdd(AtHdlcBundle self, AtHdlcLink link)
    {
    if ((!AtHdlcBundleIsValid(self)) || (link == NULL))
        return cAtErrorInvlParm;

    return mMethodsGet(self)->LinkAdd(self, link);
    }

/**
 * Get all links joined this bundle
 *
 * @param self This bundle
 * @param links All links joined this bundle
 * @return Number of links
 */
uint16 AtHdlcBundleLinksGet(AtHdlcBundle self, AtHdlcLink * links)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->LinksGet(self, links);
        
    return 0;
    }

/**
 * Set fragment size for MLPPP bundle
 *
 * @param self MLPPP bundle
 * @param fragmentSize Fragment size. Refer
 *              - @ref eAtFragmentSize "Fragment size"
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcBundleFragmentSizeSet(AtHdlcBundle self, eAtFragmentSize fragmentSize)
	{
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->FragmentSizeSet(self, fragmentSize);
    
    return cAtError;
	}

/**
 * Get maximum number of links can be added to bundle
 *
 * @param self This bundle
 *
 * @return Maximum number of links can be added to bundle
 */
uint16 AtHdlcBundleMaxLinksGet(AtHdlcBundle self)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->MaxNumLinkInBundleGet(self);

    return 0;
    }

/**
 * Get fragment size of MLPPP bundle
 *
 * @param self MLPPP bundle
 * @return Fragment size. Refer
 *              - @ref eAtFragmentSize "Fragment size"
 */
eAtFragmentSize AtHdlcBundleFragmentSizeGet(AtHdlcBundle self)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->FragmentSizeGet(self);
    
    return cAtNoFragment;
    }

/**
 * Set bundle max delay
 *
 * @param self This bundle
 * @param maxDelayInMs Delay in milliseconds
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcBundleMaxDelaySet(AtHdlcBundle self, uint8 maxDelayInMs)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->MaxDelaySet(self, maxDelayInMs);

    return cAtError;
    }

/**
 * Get bundle max delay
 *
 * @param self This bundle
 *
 * @return Bundle max delay in milliseconds
 */
uint8 AtHdlcBundleMaxDelayGet(AtHdlcBundle self)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->MaxDelayGet(self);
    
    return 0;
    }

/**
 * Get number of links that are added to bundle
 *
 * @param self This bundle
 *
 * @return Number of links that are added to bundle
 */
uint16 AtHdlcBundleNumberOfLinksGet(AtHdlcBundle self)
    {
    if (AtHdlcBundleIsValid(self))
        return (uint16)AtListLengthGet(self->links);

    return 0;
    }

/**
 * Get link by index
 *
 * @param self This bundle
 * @param linkIndex index
 *
 * @return Valid AtHdlcLink object or NULL if index is out of range
 */
AtHdlcLink AtHdlcBundleLinkGet(AtHdlcBundle self, uint16 linkIndex)
    {
    if (AtHdlcBundleIsValid(self))
        return (AtHdlcLink)AtListObjectGet(self->links, linkIndex);
    return NULL;
    }

/**
 * Create iterator for links in this bundle

 * @param self This bundle
 * @return Iterator object
 */
AtIterator AtHdlcBundleLinkIteratorCreate(AtHdlcBundle self)
    {
    if (AtHdlcBundleIsValid(self))
        return AtListIteratorCreate(self->links);

    return NULL;
    }

/**
 * Get OAM flow of this bundle
 *
 * @param self This bundle
 *
 * @return OAM flow
 */
AtEthFlow AtHdlcBundleOamFlowGet(AtHdlcBundle self)
    {
    mNullObjectCheck(NULL);
    return OamFlowGet(self);
    }

/**
 * Set scheduling mode
 *
 * @param self This bundle
 * @param schedulingMode Scheduling mode. Refer @ref eAtHdlcBundleSchedulingMode "Scheduling modes"
 * @return AT return code
 */
eAtRet AtHdlcBundleSchedulingModeSet(AtHdlcBundle self, eAtHdlcBundleSchedulingMode schedulingMode)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->SchedulingModeSet(self, schedulingMode);

    return cAtError;
    }

/**
 * Get scheduling mode
 *
 * @param self This bundle
 * @return @ref eAtHdlcBundleSchedulingMode "Scheduling mode"
 */
eAtHdlcBundleSchedulingMode AtHdlcBundleSchedulingModeGet(AtHdlcBundle self)
    {
    if (AtHdlcBundleIsValid(self))
        return mMethodsGet(self)->SchedulingModeGet(self);

    return cAtHdlcBundleSchedulingModeRandom;
    }

/**
 * Set type of Protocol Data Unit
 *
 * @param self This bundle
 * @param pduType @ref eAtHdlcPduType "PDU Type"
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcBundlePduTypeSet(AtHdlcBundle self, eAtHdlcPduType pduType)
    {
    mNumericalAttributeSet(PduTypeSet, pduType);
    }

/**
 * Get Protocol Data Unit type
 *
 * @param self This bundle
 *
 * @return PDU type @ref eAtHdlcPduType "PDU Type"
 */
eAtHdlcPduType AtHdlcBundlePduTypeGet(AtHdlcBundle self)
    {
    mAttributeGet(PduTypeGet, uint16, 0);
    }

/**
 * @}
 */

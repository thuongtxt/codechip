/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtHdlcChannelDeprecated.c
 *
 * Created Date: Jun 23, 2017
 *
 * Description : Deprecated generic HDLC implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleEncapRet DropConditionSet(AtHdlcChannel self, uint32 aCondition, eBool bypass)
    {
    return AtEncapChannelDropPacketConditionMaskSet((AtEncapChannel)self, aCondition, (bypass) ? 0 : aCondition);
    }

/*
 * Enable/disable Address comparison
 *
 * @param self HDLC channel
 * @param compareEnable Enable/disable comparison
 * @param expectedAddress Expected Address value
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelAddressCompare(AtHdlcChannel self, eBool compareEnable, uint8 * expectedAddress)
    {
    eAtModuleEncapRet ret;
    ret  = AtHdlcChannelAddressMonitorEnable(self, compareEnable);
    if (expectedAddress)
        ret |= AtHdlcChannelExpectedAddressSet(self, expectedAddress);
    return ret;
    }

/*
 * Get enable/disable Address comparison
 *
 * @param self HDLC channel
 * @param address Expected address value
 * @return Enable/disable
 */
eBool AtHdlcChannelAddressIsCompared(AtHdlcChannel self, uint8 *address)
    {
    if (address)
        AtHdlcChannelExpectedAddressGet(self, address);
    return AtHdlcChannelAddressMonitorIsEnabled(self);
    }

/*
 * Enable/disable Address insertion
 *
 * @param self HDLC channel
 * @param insertEnable Enable/disable Address insertion
 * @param address Inserted address value
 * @return At return code
 */
eAtModuleEncapRet AtHdlcChannelAddressInsert(AtHdlcChannel self, eBool insertEnable, uint8 *address)
    {
    eAtModuleEncapRet ret;
    ret  = AtHdlcChannelAddressInsertionEnable(self, insertEnable);
    if (address)
        ret |= AtHdlcChannelTxAddressSet(self, address);
    return ret;
    }

/*
 * Get enable/disable Address insertion
 *
 * @param self HDLC channel
 * @param insertedAddress Inserted address value
 * @return Enable/disable Address insertion
 */
eBool AtHdlcChannelAddressIsInserted(AtHdlcChannel self, uint8 *insertedAddress)
    {
    if (insertedAddress)
        AtHdlcChannelTxAddressGet(self, insertedAddress);
    return AtHdlcChannelAddressInsertionIsEnabled(self);
    }

/*
 * Enable/disable Control comparison
 *
 * @param self HDLC channel
 * @param compareEnable Enable/disable
 * @param expectedControl Expected Control value
 * @return At return code
 */
eAtModuleEncapRet AtHdlcChannelControlCompare(AtHdlcChannel self, eBool compareEnable, uint8 *expectedControl)
    {
    eAtModuleEncapRet ret;
    ret  = AtHdlcChannelControlMonitorEnable(self, compareEnable);
    if (expectedControl)
        ret |= AtHdlcChannelExpectedControlSet(self, expectedControl);
    return ret;
    }

/*
 * Get enable/disable Control comparison
 *
 * @param self HDLC channel
 * @param control Expected control value
 * @return Enable/disable Control comparison
 */
eBool AtHdlcChannelControlIsCompared(AtHdlcChannel self, uint8 *control)
    {
    if (control)
        AtHdlcChannelExpectedControlGet(self, control);
    return AtHdlcChannelAddressMonitorIsEnabled(self);
    }

/*
 * Enable/disable Control insertion
 *
 * @param self HDLC channel
 * @param insertEnable Enable/disable Control insertion
 * @param control Inserted Control value
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelControlInsert(AtHdlcChannel self, eBool insertEnable, uint8 *control)
    {
    eAtModuleEncapRet ret;
    ret  = AtHdlcChannelControlInsertionEnable(self, insertEnable);
    if (control)
        ret |= AtHdlcChannelTxControlSet(self, control);
    return ret;
    }

/*
 * Get enable/disable Control insertion
 *
 * @param self HDLC channel
 * @param insertedControl Inserted Control value
 * @return Enable/disable Control insertion
 */
eBool AtHdlcChannelControlIsInserted(AtHdlcChannel self, uint8 *insertedControl)
    {
    if (insertedControl)
        AtHdlcChannelTxControlGet(self, insertedControl);
    return AtHdlcChannelAddressInsertionIsEnabled(self);
    }

/*
 * Enable/disable frames that address/control are mismatch be bypassed to Ethernet side
 *
 * @param self This channel
 * @param bypass cAtTrue  - enable
 *               cAtFalse - disable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelAddressControlErrorBypass(AtHdlcChannel self, eBool bypass)
    {
    return DropConditionSet(self, cAtHdlcChannelDropConditionPktAddrCtrlError, bypass);
    }

/*
 * Check if frames that address/control are mismatch are enabled to be bypassed
 * to Ethernet side
 *
 * @param self This channel
 *
 * @retval cAtTrue - frames that address/control are mismatch are bypassed
 * @retval cAtFalse - frames that address/control are mismatch are dropped
 */
eBool AtHdlcChannelAddressControlErrorIsBypassed(AtHdlcChannel self)
    {
    uint32 dropConditions = AtEncapChannelDropPacketConditionMaskGet((AtEncapChannel)self);
    return (dropConditions & cAtHdlcChannelDropConditionPktAddrCtrlError) ? cAtFalse : cAtTrue;
    }

/*
 * Enable FCS error frames bypassed to Ethernet side
 *
 * @param self This channel
 * @param bypass cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelFcsErrorBypass(AtHdlcChannel self, eBool bypass)
    {
    return DropConditionSet(self, cAtHdlcChannelDropConditionPktFcsError, bypass);
    }

/*
 * Check if FCS error frames are bypassed to Ethernet side
 *
 * @param self This channel
 *
 * @retval cAtTrue FCS error frames are bypassed to Ethernet side
 * @retval cAtFalse FCS error frames dropped
 */
eBool AtHdlcChannelFcsErrorIsBypassed(AtHdlcChannel self)
    {
    uint32 dropConditions = AtEncapChannelDropPacketConditionMaskGet((AtEncapChannel)self);
    return (dropConditions & cAtHdlcChannelDropConditionPktFcsError) ? cAtFalse : cAtTrue;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : AtEncapBundleInternal.h
 * 
 * Created Date: Sep 1, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATENCAPBUNDLEINTERNAL_H_
#define _ATENCAPBUNDLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEncapBundle.h"
#include "AtModuleEncap.h"

#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEncapBundleMethods
    {
    uint32 (*TypeGet)(AtEncapBundle self);
    } tAtEncapBundleMethods;

typedef struct tAtEncapBundle
    {
    tAtChannel super;
    const tAtEncapBundleMethods *methods;    
    }tAtEncapBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEncapBundle AtEncapBundleObjectInit(AtEncapBundle self, uint32 channelId, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATENCAPBUNDLEINTERNAL_H_ */


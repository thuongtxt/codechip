/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtAtmTcInternal.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : ATM TC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATMTCINTERNAL_H_
#define _ATATMTCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtAtmTc.h"
#include "AtEncapChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtAtmTcMethods
    {
    /* Network interface type */
    eAtRet (*NetworkInterfaceTypeSet)(AtAtmTc self, eAtAtmNetworkInterfaceType interfaceType);
    eAtAtmNetworkInterfaceType (*NetworkInterfaceTypeGet)(AtAtmTc self);

    /* Mapping mode */
    eAtRet (*CellMappingModeSet)(AtAtmTc self, eAtAtmCellMappingMode mapMode);
    eAtAtmCellMappingMode (*CellMappingModeGet)(AtAtmTc self);
    }tAtAtmTcMethods;

/* ATM TC object */
typedef struct tAtAtmTc
    {
    tAtEncapChannel super;
    const tAtAtmTcMethods *methods;
    }tAtAtmTc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAtmTc AtAtmTcObjectInit(AtAtmTc self, uint16 channelId, AtModuleEncap module);

#ifdef __cplusplus
}
#endif
#endif /* _ATATMTCINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : AtHdlcLink.c
 *
 * Created Date: Aug 3, 2012
 *
 * Description :
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../util/AtUtil.h"
#include "AtHdlcChannel.h"
#include "AtHdlcLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mLinkIsValid(self) (self ? cAtTrue : cAtFalse)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtHdlcLinkMethods m_methods;
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "link";
    }

static AtHdlcChannel HdlcChannelGet(AtHdlcLink self)
    {
    return self->hdlcChannel;
    }

/* OAM packet mode */
static eAtRet OamPacketModeSet(AtHdlcLink self, eAtHdlcLinkOamMode oamPacketMode)
    {
	AtUnused(oamPacketMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtHdlcLinkOamMode OamPacketModeGet(AtHdlcLink self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtHdlcLinkOamModeInvalid;
    }

/* Traffic binding */
static eAtRet FlowBind(AtHdlcLink self, AtEthFlow flow)
    {
    self->boundFlow = flow;
    return cAtOk;
    }

static AtEthFlow BoundFlowGet(AtHdlcLink self)
    {
    return self->boundFlow;
    }

/* OAM send/receive */
static eAtRet OamSend(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
	AtUnused(bufferLength);
	AtUnused(buffer);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 OamReceive(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
	AtUnused(bufferLength);
	AtUnused(buffer);
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static AtEthFlow OamFlowCreate(AtHdlcLink self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eBool AllOamsSent(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OamFlowDelete(AtHdlcLink self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return;
    }

static eAtRet ProtocolCompress(AtHdlcLink self, eBool compress)
    {
	AtUnused(compress);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool ProtocolIsCompressed(AtHdlcLink self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static AtEthFlow OamFlowGet(AtHdlcLink self)
    {
    if (self->oamFlow == NULL)
        self->oamFlow = mMethodsGet(self)->OamFlowCreate(self);
    return self->oamFlow;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    if ((pseudowire == NULL) || (AtPwTypeGet(pseudowire) == cAtPwTypePpp))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eAtModulePwRet PduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType)
    {
    AtUnused(self);
    AtUnused(pduType);
    return cAtErrorNotImplemented;
    }

static eAtHdlcPduType PduTypeGet(AtHdlcLink self)
    {
    AtUnused(self);
    return cAtHdlcPduTypeAny;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    if (AtHdlcLinkBoundFlowGet((AtHdlcLink)self))
        return cAtTrue;

    return m_AtChannelMethods->ServiceIsRunning(self);
    }

static void MethodsInit(AtHdlcLink self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HdlcChannelGet);
        mMethodOverride(m_methods, OamPacketModeSet);
        mMethodOverride(m_methods, OamPacketModeGet);
        mMethodOverride(m_methods, FlowBind);
        mMethodOverride(m_methods, BoundFlowGet);
        mMethodOverride(m_methods, OamSend);
        mMethodOverride(m_methods, OamReceive);
        mMethodOverride(m_methods, OamFlowCreate);
        mMethodOverride(m_methods, AllOamsSent);
        mMethodOverride(m_methods, OamFlowDelete);
        mMethodOverride(m_methods, ProtocolCompress);
        mMethodOverride(m_methods, ProtocolIsCompressed);
        mMethodOverride(m_methods, PduTypeSet);
        mMethodOverride(m_methods, PduTypeGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtHdlcLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Delete(AtObject self)
    {
    AtHdlcLink link = (AtHdlcLink)self;

    /* Free private data */
    if (link->oamFlow != NULL)
        {
        mMethodsGet(link)->OamFlowDelete(link);
        link->oamFlow = NULL;
        }

    /* Fully delete it */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtHdlcLink object = (AtHdlcLink)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(hdlcChannel);
    mEncodeObjectDescription(hdlcBundle);
    mEncodeObject(oamFlow);
    mEncodeObjectDescription(boundFlow);
    }

static void OverrideAtObject(AtHdlcLink self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtHdlcLink self)
    {
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

AtHdlcLink AtHdlcLinkObjectInit(AtHdlcLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtHdlcLink));

    /* Call super constructor */
    if (AtChannelObjectInit((AtChannel)self,
                            AtChannelIdGet((AtChannel)hdlcChannel),
                            AtChannelModuleGet((AtChannel)hdlcChannel)) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    /* Private data */
    self->hdlcChannel = hdlcChannel;

    return self;
    }

void AtHdlcLinkBundleSet(AtHdlcLink self, AtHdlcBundle bundle)
    {
    if (self)
        self->hdlcBundle = bundle;
    }

void AtHdlcLinkOamFlowSet(AtHdlcLink self, AtEthFlow oamFlow)
    {
    if (self)
        self->oamFlow = oamFlow;
    }

void AtHdlcLinkFlowSet(AtHdlcLink self, AtEthFlow flow)
    {
    if (self)
        self->boundFlow = flow;
    }

/*
 * Enable/disable transmit direction of traffic
 *
 * @param self This link
 * @param enable Enable/disable
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcLinkTxTrafficEnable(AtHdlcLink self, eBool enable)
    {
    return AtChannelTxTrafficEnable((AtChannel)self, enable);
    }

/*
 * Get transmit direction of traffic is enabled or disabled
 *
 * @param self This link
 * @return Enabled/disabled
 */
eBool AtHdlcLinkTxTrafficIsEnabled(AtHdlcLink self)
    {
    return AtChannelTxTrafficIsEnabled((AtChannel)self);
    }

/*
 * Enable/disable receive direction of traffic
 *
 * @param self This link
 * @param enable Enable/disable
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcLinkRxTrafficEnable(AtHdlcLink self, eBool enable)
    {
    return AtChannelRxTrafficEnable((AtChannel)self, enable);
    }

/*
 * Get receive direction of traffic is enabled or disabled
 *
 * @param self This link
 * @return Enabled/disabled
 */
eBool AtHdlcLinkRxTrafficIsEnabled(AtHdlcLink self)
    {
    return AtChannelRxTrafficIsEnabled((AtChannel)self);
    }

/**
 * @addtogroup AtHdlcLink
 * @{
 */

/**
 * Enable/disable protocol field compression
 *
 * @param self This link
 * @param compress cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcLinkProtocolCompress(AtHdlcLink self, eBool compress)
    {
    mNumericalAttributeSet(ProtocolCompress, compress);
    }

/**
 * Check if protocol field compression is enabled
 *
 * @param self This link
 *
 * @retval cAtTrue Protocol field compression is enabled
 * @retval cAtFalse Protocol field compression is disabled
 */
eBool AtHdlcLinkProtocolIsCompressed(AtHdlcLink self)
    {
    mAttributeGet(ProtocolIsCompressed, eBool, cAtFalse);
    }

/**
 * Bind Ethernet flow
 *
 * @param self This link
 * @param flow Ethernet flow
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcLinkFlowBind(AtHdlcLink self, AtEthFlow flow)
    {
    mObjectSet(FlowBind, flow);
    }

/**
 * Get Ethernet flow that is bound to this link
 *
 * @param self This link
 *
 * @return Ethernet flow
 */
AtEthFlow AtHdlcLinkBoundFlowGet(AtHdlcLink self)
    {
    mAttributeGet(BoundFlowGet, AtEthFlow, NULL);
    }

/**
 * Set OAM packet mode at receive side
 *
 * @param self This link
 * @param oamMode @ref eAtHdlcLinkOamMode "OAM modes"
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcLinkOamPacketModeSet(AtHdlcLink self, eAtHdlcLinkOamMode oamMode)
    {
    eAtRet retCode;

    mNullObjectCheck(cAtErrorNullPointer);

    retCode = mMethodsGet(self)->OamPacketModeSet(self, oamMode);
    if(retCode != cAtOk)
        return (eAtModuleEncapRet)retCode;

    /* If OAM mode is to CPU, return.
       If OAM mode is to PSN and OAM flow has been created, return also */
    if ((oamMode == cAtHdlcLinkOamModeToCpu) || (self->oamFlow))
        return cAtOk;

    /* Create OAM flow */
    self->oamFlow = mMethodsGet(self)->OamFlowCreate(self);
    if (self->oamFlow == NULL)
        return cAtErrorNullPointer;

    return cAtOk;
    }

/**
 * Get OAM packet mode at receive side
 *
 * @param self This link
 *
 * @return @ref eAtHdlcLinkOamMode "OAM modes"
 */
eAtHdlcLinkOamMode AtHdlcLinkOamPacketModeGet(AtHdlcLink self)
    {
    mAttributeGet(OamPacketModeGet, eAtHdlcLinkOamMode, cAtHdlcLinkOamModeInvalid);
    }

/**
 * Get OAM flow of this link
 *
 * @param self This link
 *
 * @return @ref eAtHdlcLinkOamMode "OAM modes"
 */
AtEthFlow AtHdlcLinkOamFlowGet(AtHdlcLink self)
    {
    mNullObjectCheck(NULL);
    return OamFlowGet(self);
    }

/**
 * Send OAM packet on link
 *
 * @param self This link
 * @param buffer Buffer that holds OAM packet
 * @param bufferLength Buffer length
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcLinkOamSend(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
    mBufferAttributeSet(OamSend, buffer, bufferLength);
    }

/**
 * Get received OAM packets on link
 *
 * @param self This link
 * @param [out] buffer Buffer to hold received OAM packet
 * @param bufferLength Length of this buffer
 *
 * @return OAM packet length if there is OAM packet. Otherwise, 0 is returned.
 */
uint32 AtHdlcLinkOamReceive(AtHdlcLink self, uint8 *buffer, uint32 bufferLength)
    {
    mTwoParamAttributeGet(OamReceive, buffer, bufferLength, uint32, 0x0);
    }

/**
 * Get HDLC channel that this link belong to
 *
 * @param self This link
 *
 * @return HDLC channel
 */
AtHdlcChannel AtHdlcLinkHdlcChannelGet(AtHdlcLink self)
    {
    mAttributeGet(HdlcChannelGet, AtHdlcChannel, NULL);
    }

/**
 * To get HDLC bundle that this link belong to
 * @param self
 * @return
 */
AtHdlcBundle AtHdlcLinkBundleGet(AtHdlcLink self)
    {
    mNullObjectCheck(NULL);
    return self->hdlcBundle;
    }

/**
 * Check if all of OAMs requested by AtHdlcLinkOamSend() are sent.
 *
 * @param self This link
 *
 * @retval cAtTrue If all OAMs are sent
 * @retval cAtFalse If there are some OAMs have not been sent.
 */
eBool AtHdlcLinkAllOamsSent(AtHdlcLink self)
    {
    mAttributeGet(AllOamsSent, eBool, cAtFalse);
    }

/**
 * Set type of Protocol Data Unit
 *
 * @param self This link
 * @param pduType @ref eAtHdlcPduType "PDU Type"
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcLinkPduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType)
    {
    mNumericalAttributeSet(PduTypeSet, pduType);
    }

/**
 * Get Protocol Data Unit type
 *
 * @param self This link
 *
 * @return PDU type @ref eAtHdlcPduType "PDU Type"
 */
eAtHdlcPduType AtHdlcLinkPduTypeGet(AtHdlcLink self)
    {
    mAttributeGet(PduTypeGet, uint16, 0);
    }

/**
 * @}
 */

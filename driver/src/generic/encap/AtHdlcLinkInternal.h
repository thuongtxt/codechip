/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtHdlcLinkInternal.h
 * 
 * Created Date: Aug 31, 2012
 * 
 * Description : HDLC Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCLINKINTERNAL_H_
#define _ATHDLCLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcLink.h"
#include "AtHdlcChannel.h"
#include "AtHdlcBundle.h"

#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtHdlcLinkMethods
    {
    AtHdlcChannel (*HdlcChannelGet)(AtHdlcLink self);

    /* OAM packet mode */
    eAtRet (*OamPacketModeSet)(AtHdlcLink self, eAtHdlcLinkOamMode oamPacketMode);
    eAtHdlcLinkOamMode (*OamPacketModeGet)(AtHdlcLink self);

    eAtRet (*ProtocolCompress)(AtHdlcLink self, eBool compress);
    eBool (*ProtocolIsCompressed)(AtHdlcLink self);

    /* Traffic binding */
    eAtRet (*FlowBind)(AtHdlcLink self, AtEthFlow flow);
    AtEthFlow (*BoundFlowGet)(AtHdlcLink self);

    /* OAM send/receive */
    eAtRet (*OamSend)(AtHdlcLink self, uint8 *buffer, uint32 bufferLength);
    uint32 (*OamReceive)(AtHdlcLink self, uint8 *buffer, uint32 bufferLength);
    eBool  (*AllOamsSent)(AtHdlcLink self);

    /* OAM flow create */
    AtEthFlow (*OamFlowCreate)(AtHdlcLink self);
    void (*OamFlowDelete)(AtHdlcLink self);

    /* Bypass PID */
    eAtRet (*PidByPass)(AtHdlcLink self, eBool pidByPass);

    /* PDU type */
    eAtModuleEncapRet (*PduTypeSet)(AtHdlcLink self, eAtHdlcPduType pduType);
    eAtHdlcPduType (*PduTypeGet)(AtHdlcLink self);
    }tAtHdlcLinkMethods;

/* HDLC link class */
typedef struct tAtHdlcLink
    {
    tAtChannel super;
    const tAtHdlcLinkMethods *methods;

    /* Private data */
    AtHdlcChannel hdlcChannel; /* The HDLC channel that contains this link */

    /* Bundle this link belong to, if null, this link does not belong to any bundle */
    AtHdlcBundle hdlcBundle;

    /* OAM flow */
    AtEthFlow oamFlow;
    AtEthFlow boundFlow;
    }tAtHdlcLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcLink AtHdlcLinkObjectInit(AtHdlcLink self, AtHdlcChannel hdlcChannel);

/* Access private data */
void AtHdlcLinkBundleSet(AtHdlcLink self, AtHdlcBundle bundle);
void AtHdlcLinkOamFlowSet(AtHdlcLink self, AtEthFlow oamFlow);
void AtHdlcLinkFlowSet(AtHdlcLink self, AtEthFlow flow);

#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCLINKINTERNAL_H_ */


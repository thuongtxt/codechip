/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : AtEncapChannel.c
 *
 * Created Date: Aug 2, 2012
 *
 * Description :
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../eth/AtEthFlowInternal.h"
#include "AtModuleEncapInternal.h"
#include "AtEncapChannelInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtEncapChannelMethods m_methods;
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "encap";
    }

static eAtRet PhyBind(AtEncapChannel self, AtChannel phyChannel)
    {
    AtChannel currentChannel;

    if (!mMethodsGet(self)->CanBindPhyChannel(self, phyChannel))
        return cAtErrorModeNotSupport;

    /* Get current bound physical */
    currentChannel = AtEncapChannelBoundPhyGet(self);
    if (currentChannel == phyChannel)
        return cAtOk;

    AtEncapChannelPhysicalChannelSet(self, phyChannel);
    return cAtOk;
    }

static AtChannel BoundPhyGet(AtEncapChannel self)
    {
    return self->physicalChannel;
    }

static eAtEncapType EncapTypeGet(AtEncapChannel self)
    {
    return self->encapType;
    }

static eAtRet ScrambleEnable(AtEncapChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool ScrambleIsEnabled(AtEncapChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModuleEncapRet FlowBinding(AtEncapChannel self, AtEthFlow flow)
    {
    AtEthFlow currentFlow;

    if (!mMethodsGet(self)->CanBindEthFlow(self, flow))
        return cAtErrorModeNotSupport;

    /* Get current bound flow */
    currentFlow = AtEncapChannelBoundFlowGet(self);
    if (currentFlow == flow)
        return cAtOk;

    /* Do not allow to bind to another flow if this channel is busy */
    if ((currentFlow != NULL) || AtEthFlowIsBusy(flow))
        return cAtErrorChannelBusy;

    self->ethFlow = flow;
    AtEthFlowBoundEncapChannelSet(flow, self);

    return cAtOk;
    }

static eAtModuleEncapRet FlowUnBind(AtEncapChannel self)
    {
    AtEthFlow currentFlow = AtEncapChannelBoundFlowGet(self);
    if (currentFlow)
        AtEthFlowBoundEncapChannelSet(currentFlow, NULL);

    self->ethFlow = NULL;
    return cAtOk;
    }

static eAtModuleEncapRet FlowBind(AtEncapChannel self, AtEthFlow flow)
    {
    if (flow)
        return FlowBinding(self, flow);

    return FlowUnBind(self);
    }

static AtEthFlow BoundFlowGet(AtEncapChannel self)
    {
    return self->ethFlow;
    }

static eBool CanBindEthFlow(AtEncapChannel self, AtEthFlow flow)
    {
    AtUnused(self);
    AtUnused(flow);
    return cAtFalse;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    if (AtEncapChannelBoundFlowGet((AtEncapChannel)self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanChangePacketDroppingCondition(AtEncapChannel self, uint32 conditionMask)
    {
    AtUnused(self);
    AtUnused(conditionMask);
    /* Let concrete class determine */
    return cAtFalse;
    }

static eAtRet DropPacketConditionMaskSet(AtEncapChannel self, uint32 conditionMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(conditionMask);
    AtUnused(enableMask);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint32 DropPacketConditionMaskGet(AtEncapChannel self)
    {
    AtUnused(self);
    /* Let concrete class determine */
    return 0;
    }

static eBool CanBindPhyChannel(AtEncapChannel self, AtChannel phyChannel)
    {
    AtUnused(self);
    AtUnused(phyChannel);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtEncapChannel object = (AtEncapChannel)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(physicalChannel);
    mEncodeUInt(encapType);
    mEncodeObjectDescription(ethFlow);
    }

static void OverrideAtObject(AtEncapChannel self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEncapChannel self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        /* Save super's implementation */
        m_AtChannelMethods = mMethodsGet(channel);

        /* And override */
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEncapChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(AtEncapChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BoundPhyGet);
        mMethodOverride(m_methods, PhyBind);
        mMethodOverride(m_methods, EncapTypeGet);
        mMethodOverride(m_methods, ScrambleEnable);
        mMethodOverride(m_methods, ScrambleIsEnabled);
        mMethodOverride(m_methods, FlowBind);
        mMethodOverride(m_methods, BoundFlowGet);
        mMethodOverride(m_methods, CanBindEthFlow);
        mMethodOverride(m_methods, CanBindPhyChannel);
        mMethodOverride(m_methods, CanChangePacketDroppingCondition);
        mMethodOverride(m_methods, DropPacketConditionMaskSet);
        mMethodOverride(m_methods, DropPacketConditionMaskGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtEncapChannel AtEncapChannelObjectInit(AtEncapChannel self, uint32 channelId, eAtEncapType encapType, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtEncapChannel));

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, channelId, module) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit(self);

    /* Override */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    /* Initialize private attribute */
    self->encapType = encapType;

    return self;
    }

void AtEncapChannelPhysicalChannelSet(AtEncapChannel self, AtChannel physicalChannel)
    {
    if (self)
        self->physicalChannel = physicalChannel;
    }

eBool AtEncapChannelCanChangePacketDroppingCondition(AtEncapChannel self, uint32 conditionMask)
    {
    if (self)
        return mMethodsGet(self)->CanChangePacketDroppingCondition(self, conditionMask);
    return cAtFalse;
    }

/**
 * @addtogroup AtEncapChannel
 * @{
 */

/**
 * Bind Encapsulation channel with physical channel
 *
 * @param self Encapsulation channel
 * @param phyChannel Physical channel
 * @return AT return code
 */
eAtModuleEncapRet AtEncapChannelPhyBind(AtEncapChannel self, AtChannel phyChannel)
    {
    mObjectSet(PhyBind, phyChannel);
    }

/**
 * Get physical channel that is bound to this encapsulation channel
 *
 * @param self This channel
 *
 * @return Physical channel that is bound to this encapsulation channel
 */
AtChannel AtEncapChannelBoundPhyGet(AtEncapChannel self)
    {
    mAttributeGet(BoundPhyGet, AtChannel, NULL);
    }

/**
 * Get encapsulation type
 *
 * @param self This channel
 * @return Encapsulation type
 */
eAtEncapType AtEncapChannelEncapTypeGet(AtEncapChannel self)
    {
    mAttributeGet(EncapTypeGet, eAtEncapType, cAtEncapUnknown);
    }

/**
 * Enable/disable scrambling
 *
 * @param self This channel
 * @param scrambleEnable Enable/disable
 * @return AT return code
 */
eAtModuleEncapRet AtEncapChannelScrambleEnable(AtEncapChannel self, eBool scrambleEnable)
    {
    mNumericalAttributeSet(ScrambleEnable, scrambleEnable);
    }

/**
 * Get enable/disable scrambling
 *
 * @param self This channel
 * @return Enable/disable
 */
eBool AtEncapChannelScrambleIsEnabled(AtEncapChannel self)
    {
    mAttributeGet(ScrambleIsEnabled, eBool, cAtFalse);
    }

/**
 * Bind Encap channel with an Ethernet flow
 *
 * @param self This channel
 * @param flow Ethernet flow
 *
 * @return AT return code
 */
eAtModuleEncapRet AtEncapChannelFlowBind(AtEncapChannel self, AtEthFlow flow)
    {
    mObjectSet(FlowBind, flow);
    }

/**
 * Get bound Ethernet traffic flow
 *
 * @param self This channel
 *
 * @return Ethernet flow
 */
AtEthFlow AtEncapChannelBoundFlowGet(AtEncapChannel self)
    {
    mAttributeGet(BoundFlowGet, AtEthFlow, NULL);
    }

/**
 * Set conditions to drop packets
 *
 * @param self This channel
 * @param conditionMask Conditions mask. @ref eAtHdlcChannelDropCondition "HDLC packet-dropping conditions"
 * @param enableMask Conditions enable mask
 *
 * @return AT return code
 */
eAtRet AtEncapChannelDropPacketConditionMaskSet(AtEncapChannel self, uint32 conditionMask, uint32 enableMask)
    {
    if (self)
        return mMethodsGet(self)->DropPacketConditionMaskSet(self, conditionMask, enableMask);
    return cAtErrorNullPointer;
    }

/**
 * Get conditions that cause packets to be dropped
 *
 * @param self This channel
 * @return @ref eAtHdlcChannelDropCondition "Drop Packet Conditions"
 */
uint32 AtEncapChannelDropPacketConditionMaskGet(AtEncapChannel self)
    {
    if (self)
        return mMethodsGet(self)->DropPacketConditionMaskGet(self);
    return 0;
    }

/**
 * @}
 */


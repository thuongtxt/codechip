/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtHdlcChannel.c
 *
 * Created Date: Aug 3, 2012
 *
 * Description : HDLC generic channel
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../util/AtUtil.h"
#include "../../util/coder/AtCoderUtil.h"
#include "atclib.h"
#include "AtHdlcChannelInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtHdlcChannelMethods m_methods;
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)self;

    /* Super initialization */
    ret = m_AtChannelMethods->Init(self);

    /* Additional initialization. Not sure that all of concrete classes can
     * support all of these default settings, so just ignore return codes */
    AtHdlcChannelAddressSizeSet(hdlcChannel, 1);
    AtHdlcChannelControlSizeSet(hdlcChannel, 1);
    AtHdlcChannelFlagSet(hdlcChannel, 0x7E);
    AtHdlcChannelIdlePatternSet(hdlcChannel, 0x7E);
    AtHdlcChannelFcsModeSet(hdlcChannel, mMethodsGet(hdlcChannel)->FcsModeDefault(hdlcChannel));
    AtHdlcChannelStuffModeSet(hdlcChannel, mMethodsGet(hdlcChannel)->StuffModeDefault(hdlcChannel));

    /* To Advoid some logger */
    if (mMethodsGet(hdlcChannel)->MtuIsSupported(hdlcChannel))
        AtHdlcChannelMtuSet(hdlcChannel, 9600);

    /* Initialize its link */
    ret |= AtChannelInit((AtChannel)AtHdlcChannelHdlcLinkGet(hdlcChannel));

    return ret;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "hdlc";
    }

static eAtRet AddressSizeSet(AtHdlcChannel self, uint8 addressSize)
    {
	AtUnused(addressSize);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 AddressSizeGet(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 1;
    }

static eBool AddressCanCompare(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(compareEnable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet AddressMonitorEnable(AtHdlcChannel self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool AddressMonitorIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet ExpectedAddressSet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet ExpectedAddressGet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool AddressCanInsert(AtHdlcChannel self, eBool insertEnable)
    {
    AtUnused(insertEnable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet AddressInsertionEnable(AtHdlcChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    /* Let concrete class do */
    return cAtError;
    }

static eBool AddressInsertionIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet TxAddressSet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TxAddressGet(AtHdlcChannel self, uint8 *address)
    {
    AtUnused(address);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet ControlSizeSet(AtHdlcChannel self, uint8 controlSize)
    {
	AtUnused(controlSize);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 ControlSizeGet(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 1;
    }

static eBool ControlCanCompare(AtHdlcChannel self, eBool compareEnable)
    {
    AtUnused(compareEnable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet ControlMonitorEnable(AtHdlcChannel self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool ControlMonitorIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet ExpectedControlSet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(control);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet ExpectedControlGet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(control);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool ControlCanInsert(AtHdlcChannel self, eBool insertEnable)
    {
    AtUnused(insertEnable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet ControlInsertionEnable(AtHdlcChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    /* Let concrete class do */
    return cAtError;
    }

static eBool ControlInsertionIsEnabled(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet TxControlSet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(control);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TxControlGet(AtHdlcChannel self, uint8 *control)
    {
    AtUnused(control);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet FlagSet(AtHdlcChannel self, uint8 flag)
    {
	AtUnused(flag);
	AtUnused(self);
    /* Let concrete class do */
    return (flag == 0x7E) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 FlagGet(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0x7E;
    }

static eBool FlagIsSupported(AtHdlcChannel self, uint8 flag)
    {
    AtUnused(self);
    return (flag == 0x7E) ? cAtTrue : cAtFalse;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
	AtUnused(self);
    /* Let concrete class do */
    return (fcsMode == cAtHdlcFcsModeFcs16) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtHdlcFcsModeFcs16;
    }

static eAtHdlcFcsMode FcsModeDefault(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcFcsModeFcs16;
    }

static eBool FcsModeIsSupported(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    AtUnused(fcsMode);
    AtUnused(self);
    /* Let concrete class do */
    return (fcsMode == cAtHdlcFcsModeFcs16) ? cAtTrue : cAtFalse;
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
	AtUnused(stuffMode);
	AtUnused(self);
    /* Let concrete class do */
	return (stuffMode == cAtHdlcStuffBit) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtHdlcStuffBit;
    }

static eAtHdlcStuffMode StuffModeDefault(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtHdlcStuffBit;
    }

static eBool StuffModeIsSupported(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    AtUnused(self);
    return (stuffMode == cAtHdlcStuffBit) ? cAtTrue : cAtFalse;
    }

static eBool MtuIsSupported(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtTrue;
    }

static eAtRet MtuSet(AtHdlcChannel self, uint32 mtu)
    {
	AtUnused(mtu);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 MtuGet(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static AtHdlcLink HdlcLinkGet(AtHdlcChannel self)
    {
    return self->link;
    }

static eAtHdlcFrameType FrameTypeGet(AtHdlcChannel self)
    {
    return self->frameType;
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
	AtUnused(frameType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eBool AddressControlCanBypass(AtHdlcChannel self, eBool bypass)
    {
    AtUnused(bypass);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet AddressControlBypass(AtHdlcChannel self, eBool bypass)
    {
    AtUnused(bypass);
    AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eBool AddressControlIsBypassed(AtHdlcChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool AddressControlCanCompress(AtHdlcChannel self, eBool compress)
    {
    AtUnused(compress);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet TxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
	AtUnused(compress);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eBool TxAddressControlIsCompressed(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet RxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
	AtUnused(compress);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eBool RxAddressControlIsCompressed(AtHdlcChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    AtHdlcChannel channel = (AtHdlcChannel)self;

    /* Delete link object */
    AtObjectDelete((AtObject)(channel->link));
    channel->link = NULL;

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtHdlcChannel object = (AtHdlcChannel)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(link);
    mEncodeUInt(frameType);
    }

static void StatusClear(AtChannel self)
    {
    AtChannelStatusClear((AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self));
    AtChannelStatusClear((AtChannel)AtChannelBoundPwGet(self));
    m_AtChannelMethods->StatusClear(self);
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    if ((pseudowire == NULL) || (AtPwTypeGet(pseudowire) == cAtPwTypeHdlc))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static eBool ShouldDeletePwOnCleanup(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet HdlcLinkSet(AtHdlcChannel self, AtHdlcLink link)
    {
    self->link = link;
    return cAtOk;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    if (m_AtChannelMethods->ServiceIsRunning(self))
        return cAtTrue;

    if (AtChannelBoundPwGet(self))
        return cAtTrue;

    return AtChannelServiceIsRunning((AtChannel)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self));
    }

static eAtModuleEncapRet IdlePatternSet(AtHdlcChannel self, uint8 idle)
    {
    AtUnused(self);
    AtUnused(idle);
    return cAtErrorNotImplemented;
    }

static uint8 IdlePatternGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet NumFlagsSet(AtHdlcChannel self, uint8 numFlag)
    {
    AtUnused(self);
    return (numFlag == 1) ? cAtOk : cAtErrorNotImplemented;
    }

static uint8 NumFlagsGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet FcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtErrorModeNotSupport;
    }

static void AddressControlApiLogStart(AtHdlcChannel self, uint8 *buffers, uint8 numBytes,
                                      const char *file, uint32 line, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize;
        char *buffer, *controlString;

        AtDriverLogLock();
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);
        controlString = AtUtilBytes2String(buffers, numBytes, 10, buffer, bufferSize);
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                file, line, "API: %s([%s], [%s])\r\n",
                                function, AtObjectToString((AtObject)self), controlString);
        AtDriverLogUnLock();
        }
    }

static void OverrideAtObject(AtHdlcChannel self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);

        /* Copy to reuse implementation of super class. But override some methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        /* Save super's implementation */
        m_AtChannelMethods = mMethodsGet(channel);

        /* And override */
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, ShouldDeletePwOnCleanup);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, AddressSizeSet);
        mMethodOverride(m_methods, AddressSizeGet);
        mMethodOverride(m_methods, AddressCanCompare);
        mMethodOverride(m_methods, AddressMonitorEnable);
        mMethodOverride(m_methods, AddressMonitorIsEnabled);
        mMethodOverride(m_methods, ExpectedAddressSet);
        mMethodOverride(m_methods, ExpectedAddressGet);
        mMethodOverride(m_methods, AddressCanInsert);
        mMethodOverride(m_methods, AddressInsertionEnable);
        mMethodOverride(m_methods, AddressInsertionIsEnabled);
        mMethodOverride(m_methods, TxAddressSet);
        mMethodOverride(m_methods, TxAddressGet);
        mMethodOverride(m_methods, ControlSizeSet);
        mMethodOverride(m_methods, ControlSizeGet);
        mMethodOverride(m_methods, ControlCanCompare);
        mMethodOverride(m_methods, ControlMonitorEnable);
        mMethodOverride(m_methods, ControlMonitorIsEnabled);
        mMethodOverride(m_methods, ExpectedControlSet);
        mMethodOverride(m_methods, ExpectedControlGet);
        mMethodOverride(m_methods, ControlCanInsert);
        mMethodOverride(m_methods, ControlInsertionEnable);
        mMethodOverride(m_methods, ControlInsertionIsEnabled);
        mMethodOverride(m_methods, TxControlSet);
        mMethodOverride(m_methods, TxControlGet);
        mMethodOverride(m_methods, FlagSet);
        mMethodOverride(m_methods, FlagGet);
        mMethodOverride(m_methods, FlagIsSupported);
        mMethodOverride(m_methods, FcsModeSet);
        mMethodOverride(m_methods, FcsModeGet);
        mMethodOverride(m_methods, FcsModeDefault);
        mMethodOverride(m_methods, FcsModeIsSupported);
        mMethodOverride(m_methods, StuffModeSet);
        mMethodOverride(m_methods, StuffModeGet);
        mMethodOverride(m_methods, StuffModeDefault);
        mMethodOverride(m_methods, StuffModeIsSupported);
        mMethodOverride(m_methods, MtuSet);
        mMethodOverride(m_methods, MtuGet);
        mMethodOverride(m_methods, MtuIsSupported);
        mMethodOverride(m_methods, HdlcLinkSet);
        mMethodOverride(m_methods, HdlcLinkGet);
        mMethodOverride(m_methods, FrameTypeGet);
        mMethodOverride(m_methods, AddressControlCanBypass);
        mMethodOverride(m_methods, AddressControlBypass);
        mMethodOverride(m_methods, AddressControlIsBypassed);
        mMethodOverride(m_methods, FrameTypeSet);
        mMethodOverride(m_methods, AddressControlCanCompress);
        mMethodOverride(m_methods, TxAddressControlCompress);
        mMethodOverride(m_methods, TxAddressControlIsCompressed);
        mMethodOverride(m_methods, RxAddressControlCompress);
        mMethodOverride(m_methods, RxAddressControlIsCompressed);
        mMethodOverride(m_methods, IdlePatternSet);
        mMethodOverride(m_methods, IdlePatternGet);
        mMethodOverride(m_methods, NumFlagsSet);
        mMethodOverride(m_methods, NumFlagsGet);
        mMethodOverride(m_methods, FcsCalculationModeSet);
        }

    mMethodsSet(self, &m_methods);
    }

AtHdlcChannel AtHdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, eAtHdlcFrameType frameType, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtHdlcChannel));

    /* Super constructor */
    if (AtEncapChannelObjectInit((AtEncapChannel)self, channelId, cAtEncapHdlc, module) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    /* Initialize private data */
    self->frameType = (uint8)frameType;

    return self;
    }

void AtHdlcChannelHdlcLinkSet(AtHdlcChannel self, AtHdlcLink link)
    {
    if (self)
        mMethodsGet(self)->HdlcLinkSet(self, link);
    }

/*
 * Enable/disable Address/Control fields compression at both RX and TX direction
 *
 * @param self This channel
 * @param compress cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 * @see AtHdlcChannelRxAddressControlCompress/AtHdlcChannelTxAddressControlCompress
 * @note Because one side may not support compression, so this API will return
 *       cAtOk if configuring one side is success. If two sides are fail, first
 *       error code will be returned;
 */
eBool AtHdlcChannelAddressControlCanCompress(AtHdlcChannel self, eBool compress)
    {
    mOneParamAttributeGet(AddressControlCanCompress, compress, eBool, cAtFalse);
    }

/*
 * Check if  Address insertion cam be
 *
 * @param self HDLC channel
 * @param compareEnable Enable/disable comparison
 * @param expectedAddress Expected Address value
 * @return AT return code
 */
eBool AtHdlcChannelAddressCanInsert(AtHdlcChannel self, eBool insertEnable)
    {
    mOneParamAttributeGet(AddressCanInsert, insertEnable, eBool, cAtFalse);
    }

/*
 * Check if  Address comparison cam be
 *
 * @param self HDLC channel
 * @param compareEnable Enable/disable comparison
 * @param expectedAddress Expected Address value
 * @return AT return code
 */
eBool AtHdlcChannelAddressCanCompare(AtHdlcChannel self, eBool compareEnable)
    {
    mOneParamAttributeGet(AddressCanCompare, compareEnable, eBool, cAtFalse);
    }

/*
 * Check if  Control insertion cam be
 *
 * @param self HDLC channel
 * @param compareEnable Enable/disable comparison
 * @param expectedAddress Expected Address value
 * @return AT return code
 */
eBool AtHdlcChannelControlCanCompare(AtHdlcChannel self, eBool compareEnable)
    {
    mOneParamAttributeGet(ControlCanCompare, compareEnable, eBool, cAtFalse);
    }

/*
 * Check if  Control insertion cam be
 *
 * @param self HDLC channel
 * @param compareEnable Enable/disable comparison
 * @param expectedAddress Expected Address value
 * @return AT return code
 */
eBool AtHdlcChannelControlCanInsert(AtHdlcChannel self, eBool insertEnable)
    {
    mOneParamAttributeGet(ControlCanInsert, insertEnable, eBool, cAtFalse);
    }

/*
 * Check FCS mode if supported
 *
 * @param self HDLC channel
 * @param fcsMode FCS mode. Refer
 *            - @ref eAtHdlcFcsMode "FCS mode"
 * @return cAtTrue if suppported
 *
 *
 */
eBool AtHdlcChannelFcsModeIsSupported(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    mOneParamAttributeGet(FcsModeIsSupported, fcsMode, eBool, cAtFalse);
    }

/*
 * check if can Bypass address/control fields to Ethernet side
 *
 * @param self This channel
 * @param bypass cAtTrue to bypass, cAtFalse to drop
 *
 * @return cAtTrue if can bypass
 */
eBool AtHdlcChannelAddressControlCanBypass(AtHdlcChannel self, eBool bypass)
    {
    mOneParamAttributeGet(AddressControlCanBypass, bypass, eBool, cAtFalse);
    }

/**
 * @addtogroup AtHdlcChannel
 * @{
 */

/**
 * Get HDLC link from HDLC channel, if framing mode of HDCL channel is PPP or Cisco HDLC
 *
 * @param self HDCL channel
 * @return HDLC link
 */
AtHdlcLink AtHdlcChannelHdlcLinkGet(AtHdlcChannel self)
    {
    mAttributeGet(HdlcLinkGet, AtHdlcLink, NULL);
    }

/**
 * Get HDLC channel's frame type
 *
 * @param self HDLC channel
 * @return @ref eAtHdlcFrameType "HDLC frame type"
 */
eAtHdlcFrameType AtHdlcChannelFrameTypeGet(AtHdlcChannel self)
	{
    mAttributeGet(FrameTypeGet, eAtHdlcFrameType, cAtHdlcFrmUnknown);
	}

/**
 * Set HDLC channel's frame type
 *
 * @param self HDLC channel
 * @param frameType @ref eAtHdlcFrameType "HDLC frame type"
 *
 * @return At return code
 */
eAtModuleEncapRet AtHdlcChannelFrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    mNumericalAttributeSet(FrameTypeSet, frameType);
    }

/**
 * Set number byte of address field
 *
 * @param self This channel
 * @param numberOfAddressByte Number byte of address field
 * @return
 */
eAtModuleEncapRet AtHdlcChannelAddressSizeSet(AtHdlcChannel self, uint8 numberOfAddressByte)
    {
    mNumericalAttributeSet(AddressSizeSet, numberOfAddressByte);
    }

/**
 * Get number byte of address field
 *
 * @param self This channel
 * @return Number byte of address field
 */
uint8 AtHdlcChannelAddressSizeGet(AtHdlcChannel self)
    {
    mAttributeGet(AddressSizeGet, uint8, 1);
    }

/**
 * Enable/disable Address monitoring
 *
 * @param self HDLC channel
 * @param enable Enable/disable Address field monitoring
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelAddressMonitorEnable(AtHdlcChannel self, eBool enable)
    {
    mNumericalAttributeSet(AddressMonitorEnable, enable);
    }

/**
 * Get the enable state of Address monitoring
 *
 * @param self HDLC channel
 *
 * @retval cAtTrue if Address monitoring is enabled
 * @retval cAtFalse if Address monitoring is disabled
 */
eBool AtHdlcChannelAddressMonitorIsEnabled(AtHdlcChannel self)
    {
    mAttributeGet(AddressMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Set the expected Address field
 *
 * @param self HDLC channel
 * @param address Expected address octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelExpectedAddressSet(AtHdlcChannel self, uint8 *address)
    {
    eAtModuleEncapRet ret = cAtErrorObjectNotExist;

    AddressControlApiLogStart(self, address, AtHdlcChannelAddressSizeGet(self), AtSourceLocationNone, AtFunction);
    if (self)
        ret = mMethodsGet(self)->ExpectedAddressSet(self, address);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get the expected Address field
 *
 * @param self HDLC channel
 * @param [out] address Expected Address octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelExpectedAddressGet(AtHdlcChannel self, uint8 *address)
    {
    mOneParamAttributeGet(ExpectedAddressGet, address, eAtModuleEncapRet, cAtErrorNullPointer);
    }

/**
 * Enable/disable address field insertion before transmitting to TDM side.
 *
 * @param self HDLC channel
 * @param enable Enable/disable address insertion
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelAddressInsertionEnable(AtHdlcChannel self, eBool enable)
    {
    mNumericalAttributeSet(AddressInsertionEnable, enable);
    }

/**
 * Get the enable state of address field insertion
 *
 * @param self HDLC channel
 *
 * @retval cAtTrue if Address insertion is enabled
 * @retval cAtFalse if Address insertion is disabled
 */
eBool AtHdlcChannelAddressInsertionIsEnabled(AtHdlcChannel self)
    {
    mAttributeGet(AddressInsertionIsEnabled, eBool, cAtFalse);
    }

/**
 * Set the transmitted Address field
 *
 * @param self HDLC channel
 * @param address Transmitted address octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelTxAddressSet(AtHdlcChannel self, uint8 *address)
    {
    eAtModuleEncapRet ret = cAtErrorObjectNotExist;

    AddressControlApiLogStart(self, address, AtHdlcChannelAddressSizeGet(self), AtSourceLocationNone, AtFunction);
    if (self)
        ret = mMethodsGet(self)->TxAddressSet(self, address);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get the transmitted Address field
 *
 * @param self HDLC channel
 * @param [out] address Transmitted Address octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelTxAddressGet(AtHdlcChannel self, uint8 *address)
    {
    mOneParamAttributeGet(TxAddressGet, address, eAtModuleEncapRet, cAtErrorNullPointer);
    }

/**
 * Set number byte of Control field
 *
 * @param self This channel
 * @param numberOfByte Number byte of Control field
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelControlSizeSet(AtHdlcChannel self, uint8 numberOfByte)
    {
    mNumericalAttributeSet(ControlSizeSet, numberOfByte);
    }

/**
 * Get number byte of Control field
 *
 * @param self This channel
 * @return Number byte of Control field
 */
uint8 AtHdlcChannelControlSizeGet(AtHdlcChannel self)
    {
    mAttributeGet(ControlSizeGet, uint8, 1);
    }

/**
 * Enable/disable Control monitoring
 *
 * @param self HDLC channel
 * @param enable Enable/disable Control field monitoring
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelControlMonitorEnable(AtHdlcChannel self, eBool enable)
    {
    mNumericalAttributeSet(ControlMonitorEnable, enable);
    }

/**
 * Get the enable state of Control monitoring
 *
 * @param self HDLC channel
 *
 * @retval cAtTrue if Control monitoring is enabled
 * @retval cAtFalse if Control monitoring is disabled
 */
eBool AtHdlcChannelControlMonitorIsEnabled(AtHdlcChannel self)
    {
    mAttributeGet(ControlMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Set the expected Control field
 *
 * @param self HDLC channel
 * @param control Expected control octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelExpectedControlSet(AtHdlcChannel self, uint8 *control)
    {
    eAtModuleEncapRet ret = cAtErrorObjectNotExist;

    AddressControlApiLogStart(self, control, AtHdlcChannelControlSizeGet(self), AtSourceLocationNone, AtFunction);
    if (self)
        ret = mMethodsGet(self)->ExpectedControlSet(self, control);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get the expected Control field
 *
 * @param self HDLC channel
 * @param [out] control Expected Control octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelExpectedControlGet(AtHdlcChannel self, uint8 *control)
    {
    mOneParamAttributeGet(ExpectedControlGet, control, eAtModuleEncapRet, cAtErrorNullPointer);
    }

/**
 * Enable/disable control field insertion before transmitting to TDM side.
 *
 * @param self HDLC channel
 * @param enable Enable/disable address insertion
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelControlInsertionEnable(AtHdlcChannel self, eBool enable)
    {
    mNumericalAttributeSet(ControlInsertionEnable, enable);
    }

/**
 * Get the enable state of control field insertion
 *
 * @param self HDLC channel
 *
 * @retval cAtTrue if Control insertion is enabled
 * @retval cAtFalse if Control insertion is disabled
 */
eBool AtHdlcChannelControlInsertionIsEnabled(AtHdlcChannel self)
    {
    mAttributeGet(ControlInsertionIsEnabled, eBool, cAtFalse);
    }

/**
 * Set the transmitted Control field
 *
 * @param self HDLC channel
 * @param control Transmitted control octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelTxControlSet(AtHdlcChannel self, uint8 *control)
    {
    eAtModuleEncapRet ret = cAtErrorObjectNotExist;

    AddressControlApiLogStart(self, control, AtHdlcChannelControlSizeGet(self), AtSourceLocationNone, AtFunction);
    if (self)
        ret = mMethodsGet(self)->TxControlSet(self, control);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get the transmitted Control field
 *
 * @param self HDLC channel
 * @param [out] control Transmitted Control octets
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelTxControlGet(AtHdlcChannel self, uint8 *control)
    {
    mOneParamAttributeGet(TxControlGet, control, eAtModuleEncapRet, cAtErrorNullPointer);
    }

/**
 * Enable/disable Address/Control fields compression at both RX and TX direction
 *
 * @param self This channel
 * @param compress cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 * @see AtHdlcChannelRxAddressControlCompress/AtHdlcChannelTxAddressControlCompress
 * @note Because one side may not support compression, so this API will return
 *       cAtOk if configuring one side is success. If two sides are fail, first
 *       error code will be returned;
 */
eAtModuleEncapRet AtHdlcChannelAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
    eAtRet rxRet, txRet, ret;

    mOneParamApiLogStart(compress);

    rxRet = AtHdlcChannelRxAddressControlCompress(self, compress);
    txRet = AtHdlcChannelTxAddressControlCompress(self, compress);

    if ((rxRet == cAtOk) || (txRet == cAtOk))
        ret = cAtOk;
    else
        ret = rxRet | txRet;

    AtDriverApiLogStop();

    return ret;
    }

/**
 * Check if Address/Control fields compression is enabled
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtHdlcChannelAddressControlIsCompressed(AtHdlcChannel self)
    {
    return AtHdlcChannelRxAddressControlIsCompressed(self);
    }

/**
 * Set FCS mode
 *
 * @param self HDLC channel
 * @param fcsMode FCS mode. Refer
 *            - @ref eAtHdlcFcsMode "FCS mode"
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelFcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    mNumericalAttributeSet(FcsModeSet, fcsMode);
    }

/**
 * Get FCS mode
 *
 * @param self HDLC channel
 * @return FCS mode. Refer
 *            - @ref eAtHdlcFcsMode "FCS mode"
 */
eAtHdlcFcsMode AtHdlcChannelFcsModeGet(AtHdlcChannel self)
    {
    mAttributeGet(FcsModeGet, eAtHdlcFcsMode, cAtHdlcFcsModeNoFcs);
    }

/**
 * Enable/disable scrambling
 *
 * @param self HDLC channel
 * @param scrambleEnable Enable/disable
 * @return AT return code
 *
 * @note This API is deprecated by AtEncapChannelScrambleEnable
 */
eAtModuleEncapRet AtHdlcChannelScrambleEnable(AtHdlcChannel self, eBool scrambleEnable)
	{
    mOneParamWrapCall(scrambleEnable, eAtModuleEncapRet, AtEncapChannelScrambleEnable((AtEncapChannel)self, scrambleEnable));
	}

/**
 * Get enable/disable scrambling
 *
 * @param self HDLC channel
 * @return Enable/disable
 *
 * @note This API is deprecated by AtEncapChannelScrambleIsEnabled
 */
eBool AtHdlcChannelScrambleIsEnabled(AtHdlcChannel self)
    {
    return AtEncapChannelScrambleIsEnabled((AtEncapChannel)self);
    }

/**
 * Check flag is supported
 *
 * @param self HDLC channel
 * @param flag Flag value
 * @return AT return code
 */
eBool AtHdlcChannelFlagIsSupported(AtHdlcChannel self, uint8 flag)
    {
    mOneParamAttributeGet(FlagIsSupported, flag, eBool, cAtFalse);
    }

/**
 * Set flag value
 *
 * @param self HDLC channel
 * @param flag Flag value
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelFlagSet(AtHdlcChannel self, uint8 flag)
    {
    mNumericalAttributeSet(FlagSet, flag);
    }

/**
 * Get Flag value
 *
 * @param self HDLC channel
 * @return Flag value
 */
uint8 AtHdlcChannelFlagGet(AtHdlcChannel self)
    {
    mAttributeGet(FlagGet, uint8, 0x0);
    }

/**
 * Check stuffing mode is supported
 *
 * @param self HDLC channel
 * @param stuffMode Stuffing mode. Refer
 *               - @ref eAtHdlcStuffMode "Stuffing mode"
 * @return cAtTrue if supports
 */
eBool AtHdlcChannelStuffModeIsSupported(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    mOneParamAttributeGet(StuffModeIsSupported, stuffMode, eBool, cAtFalse);
    }

/**
 * Set stuffing mode
 *
 * @param self HDLC channel
 * @param stuffMode Stuffing mode. Refer
 *               - @ref eAtHdlcStuffMode "Stuffing mode"
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelStuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    mNumericalAttributeSet(StuffModeSet, stuffMode);
    }

/**
 * Get stuffing mode
 *
 * @param self HDLC channel
 * @return Stuffing mode. Refer
 *               - @ref eAtHdlcStuffMode "Stuffing mode"
 */
eAtHdlcStuffMode AtHdlcChannelStuffModeGet(AtHdlcChannel self)
    {
    mAttributeGet(StuffModeGet, eAtHdlcStuffMode, cAtHdlcStuffBit);
    }

/**
 * Set MTU
 *
 * @param self HDLC channel
 * @param mtu MTU
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelMtuSet(AtHdlcChannel self, uint32 mtu)
    {
    mNumericalAttributeSet(MtuSet, mtu);
    }

/**
 * Get MTU
 *
 * @param self HDLC channel
 * @return MTU
 */
uint32 AtHdlcChannelMtuGet(AtHdlcChannel self)
    {
    mAttributeGet(MtuGet, uint32, 0x0);
    }

/**
 * Bypass address/control fields to Ethernet side
 *
 * @param self This channel
 * @param bypass cAtTrue to bypass, cAtFalse to drop
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelAddressControlBypass(AtHdlcChannel self, eBool bypass)
    {
    mNumericalAttributeSet(AddressControlBypass, bypass);
    }

/**
 * Check if address/fields are bypassed to Ethernet side
 *
 * @param self This channel
 *
 * @retval cAtTrue if bypass
 * @retval cAtFalse if drop
 */
eBool AtHdlcChannelAddressControlIsBypassed(AtHdlcChannel self)
    {
    mAttributeGet(AddressControlIsBypassed, eBool, cAtFalse);
    }

/**
 * Enable/disable Address/Control fields compression (ACFC) at TX direction
 *
 * @param self This channel
 * @param compress cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 *
 * @see AtHdlcChannelAddressControlCompress()
 */
eAtModuleEncapRet AtHdlcChannelTxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
    mNumericalAttributeSet(TxAddressControlCompress, compress);
    }

/**
 * Check if Address/Control fields compression is enabled at TX direction
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtHdlcChannelTxAddressControlIsCompressed(AtHdlcChannel self)
    {
    mAttributeGet(TxAddressControlIsCompressed, eBool, cAtFalse);
    }

/**
 * Enable/disable Address/Control fields compression (ACFC) at RX direction
 *
 * @param self This channel
 * @param compress cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelRxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
    mNumericalAttributeSet(RxAddressControlCompress, compress);
    }

/**
 * Check if Address/Control fields compression is enabled at RX direction
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtHdlcChannelRxAddressControlIsCompressed(AtHdlcChannel self)
    {
    mAttributeGet(RxAddressControlIsCompressed, eBool, cAtFalse);
    }

/**
 * Set idle pattern
 *
 * @param self HDLC channel
 * @param idle Idle pattern
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelIdlePatternSet(AtHdlcChannel self, uint8 idle)
    {
    mNumericalAttributeSet(IdlePatternSet, idle);
    }

/**
 * Get idle pattern
 *
 * @param self HDLC channel
 *
 * @return IDLE pattern
 */
uint8 AtHdlcChannelIdlePatternGet(AtHdlcChannel self)
    {
    mAttributeGet(IdlePatternGet, uint8, 0);
    }

/**
 * Configure FCS calculation mode from MSB or LSB
 *
 * @param self HDLC channel
 * @param mode FCS calculation mode
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelFcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode mode)
    {
    mNumericalAttributeSet(FcsCalculationModeSet, mode);
    }

/**
 * Get FCS calculation mode from MSB or LSB
 *
 * @param self HDLC channel
 * @return FCS calculation mode
 */
eAtHdlcFcsCalculationMode AtHdlcChannelFcsCalculationModeGet(AtHdlcChannel self)
    {
    mAttributeGet(FcsCalculationModeGet, eAtHdlcFcsCalculationMode, cAtHdlcFcsCalculationModeUnknown);
    }
    
/**
 * Set number of flags between two HDLC packets
 *
 * @param self HDLC channel
 * @param numFlag number of flags
 *
 * @return AT return code
 */
eAtModuleEncapRet AtHdlcChannelNumFlagsSet(AtHdlcChannel self, uint8 numFlag)
    {
    mNumericalAttributeSet(NumFlagsSet, numFlag);
    }

/**
 * Get number of flags between two HDLC packets
 *
 * @param self HDLC channel
 *
 * @return Number of flags
 */
uint8 AtHdlcChannelNumFlagsGet(AtHdlcChannel self)
    {
    mAttributeGet(NumFlagsGet, uint8, 0x0);
    }

/**
 * @}
 */

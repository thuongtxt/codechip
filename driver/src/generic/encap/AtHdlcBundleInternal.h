/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtHdlcBundleInternal.h
 * 
 * Created Date: Sep 1, 2012
 *
 * Description : HDLC bundle
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCBUNDLEINTERNAL_H_
#define _ATHDLCBUNDLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"
#include "AtHdlcBundle.h"
#include "AtEncapBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtHdlcBundleMethods{
    /* Accessors */
    eAtRet (*MaxDelaySet)(AtHdlcBundle self, uint8 maxDelay);
    uint8 (*MaxDelayGet)(AtHdlcBundle self);
    eAtRet (*FragmentSizeSet)(AtHdlcBundle self, eAtFragmentSize fragmentSize);
    eAtFragmentSize (*FragmentSizeGet)(AtHdlcBundle self);
    
    /* Methods */
    eAtRet (*LinkRemove)(AtHdlcBundle self, AtHdlcLink link);
    eAtRet (*LinkAdd)(AtHdlcBundle self, AtHdlcLink link);
    uint16 (*LinksGet)(AtHdlcBundle self, AtHdlcLink *links);
    uint8 (*MaxNumLinkInBundleGet)(AtHdlcBundle self);
    eAtRet (*SchedulingModeSet)(AtHdlcBundle self, eAtHdlcBundleSchedulingMode schedulingMode);
    eAtHdlcBundleSchedulingMode (*SchedulingModeGet)(AtHdlcBundle self);

    /* OAM flow */
    AtEthFlow (*OamFlowCreate)(AtHdlcBundle self);
    void (*OamFlowDelete)(AtHdlcBundle self);

    /* PDU type */
    eAtRet (*PduTypeSet)(AtHdlcBundle self, eAtHdlcPduType pduType);
    eAtHdlcPduType (*PduTypeGet)(AtHdlcBundle self);
    uint8 (*DefaultMaxLinkDelay)(AtHdlcBundle self);
    }tAtHdlcBundleMethods;

/* HDLC bundle */
typedef struct tAtHdlcBundle
    {
    tAtEncapBundle super;
    const tAtHdlcBundleMethods *methods;

    AtList links;  /* Cache all links in this bundle */
    uint8 isValid; /* Is true when bundle has been created explicitly */
    AtEthFlow oamFlow;
    }tAtHdlcBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcBundle AtHdlcBundleObjectInit(AtHdlcBundle self, uint32 channelId, AtModule module);

/* Internal functions to access private data */
void HdlcBundleLinkAdd(AtHdlcBundle self, AtHdlcLink link);
void HdlcBundleLinkRemove(AtHdlcBundle self, AtHdlcLink link);
void AtHdlcBundleValidate(AtHdlcBundle self, eBool isValid);
eBool AtHdlcBundleIsValid(AtHdlcBundle self);
void AtHdlcBundleOamFlowSet(AtHdlcBundle self, AtEthFlow oamFlow);
uint8 AtHdlcBundleDefaultMaxLinkDelay(AtHdlcBundle self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCBUNDLEINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : AtModuleEncap.c
 *
 * Created Date: Aug 3, 2012
 *
 * Description :
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEncapInternal.h"

#include "../encap/AtHdlcChannelInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../../util/AtIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtHdlcBundle (*BundleObjectCreate)(AtModuleEncap self, uint32 bundleId);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleEncapMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/* Channel Array Iterator initialize */
static char m_arrayIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_AtArrayIteratorOverride;
static tAtIteratorMethods m_AtIteratorOverride;

/* Bundle Array Iterator initialize */
static char m_bundleIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_bundleAtArrayIteratorOverride;
static tAtIteratorMethods m_bundleAtIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool DataAtIndexIsValid(AtArrayIterator self, uint32 channelIndex)
    {
    AtEncapChannel *channels = self->array;
    return ((channels[channelIndex]) != NULL);
    }

static AtObject DataAtIndex(AtArrayIterator self, uint32 channelIndex)
    {
    AtEncapChannel *channels = self->array;
    return (AtObject)channels[channelIndex];
    }

static uint16 AtModuleEncapChannelCountGet(AtArrayIterator iterator)
    {
    uint16 count = 0;
    uint16 i;
    AtEncapChannel *channels = iterator->array;
    if (NULL == channels)
        return 0;

    for (i = 0; i < iterator->capacity; i++)
        {
        if (channels[i])
            count++;
        }

    return count;
    }

static uint32 Count(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count >= 0)
        return (uint32)arrayIterator->count;

    arrayIterator->count = AtModuleEncapChannelCountGet(arrayIterator);
    return (uint32)arrayIterator->count;
    }

static void OverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_arrayIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtArrayIteratorOverride, mMethodsGet(self), sizeof(m_AtArrayIteratorOverride));
        mMethodOverride(m_AtArrayIteratorOverride, DataAtIndexIsValid);
        mMethodOverride(m_AtArrayIteratorOverride, DataAtIndex);
        }

    mMethodsSet(self, &m_AtArrayIteratorOverride);
    }

static void OverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_arrayIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(iterator), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, Count);
        }
    mMethodsSet(iterator, &m_AtIteratorOverride);
    }

static void IteratorOverride(AtArrayIterator self)
    {
    OverrideAtArrayIterator(self);
    OverrideAtIterator(self);
    }

static eBool BundleAtIndexIsValid(AtArrayIterator self, uint32 bundleIndex)
    {
    AtHdlcBundle *bundles = self->array;
    return AtHdlcBundleIsValid(bundles[bundleIndex]);
    }

static AtObject BundleAtIndex(AtArrayIterator self, uint32 bundleIndex)
    {
    AtHdlcBundle *bundles = self->array;
    return (AtObject)bundles[bundleIndex];
    }

static void BundleIteratorOverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_bundleIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_bundleAtArrayIteratorOverride, mMethodsGet(self), sizeof(m_bundleAtArrayIteratorOverride));

        m_bundleAtArrayIteratorOverride.DataAtIndexIsValid = BundleAtIndexIsValid;
        m_bundleAtArrayIteratorOverride.DataAtIndex = BundleAtIndex;
        }

    mMethodsSet(self, &m_bundleAtArrayIteratorOverride);
    }

static uint16 BundleCountGet(AtArrayIterator self)
    {
    uint16 count = 0;
    uint16 i;
    AtHdlcBundle *bundles = self->array;
    if (bundles == NULL)
        return 0;

    for (i = 0; i < self->capacity; i++)
        {
        if (AtHdlcBundleIsValid(bundles[i]))
            count++;
        }

    return count;
    }

static uint32 BundleCount(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count >= 0)
        return (uint32)arrayIterator->count;

    arrayIterator->count = (int32)BundleCountGet(arrayIterator);
    return (uint32)arrayIterator->count;
    }

static void BundleIteratorOverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_bundleIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_bundleAtIteratorOverride, mMethodsGet(iterator), sizeof(m_bundleAtIteratorOverride));

        m_bundleAtIteratorOverride.Count = BundleCount;
        }

    mMethodsSet(iterator, &m_bundleAtIteratorOverride);
    }

static void BundleIteratorOverride(AtArrayIterator self)
    {
    BundleIteratorOverrideAtArrayIterator(self);
    BundleIteratorOverrideAtIterator(self);
    }

static void AllChannelsDelete(AtModuleEncap self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint16 i;
    uint16 maxChannel = mMethodsGet(self)->MaxChannelsGet(self);

    /* Delete all of channels */
    if (self->channels)
        {
        for (i = 0; i < maxChannel; i++)
            {
            if (self->channels[i])
                AtObjectDelete((AtObject)(self->channels[i]));
            }

        /* And memory that hold all of channels */
        mMethodsGet(osal)->MemFree(osal, self->channels);
        self->channels = NULL;
        }
    }

static void AllBundlesDelete(AtModuleEncap self)
    {
    AtOsal osal;
    uint32 maxBundles, bundle_i;

    if (self->bundles == NULL)
        return;

    /* Delete all of bunldes */
    osal = AtSharedDriverOsalGet();
    maxBundles = mMethodsGet(self)->MaxBundlesGet(self);
    for (bundle_i = 0; bundle_i < maxBundles; bundle_i++)
        AtObjectDelete((AtObject)(self->bundles[bundle_i]));

    /* And memory that hold all of bundles */
    mMethodsGet(osal)->MemFree(osal, self->bundles);
    self->bundles = NULL;
    }

static AtHdlcBundle *BundlesListAllocate(uint32 maxBundles)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 size = sizeof(AtHdlcBundle) * maxBundles;
    AtHdlcBundle *bundlesList = mMethodsGet(osal)->MemAlloc(osal, size);

    if (bundlesList)
        mMethodsGet(osal)->MemInit(osal, bundlesList, 0, size);

    return bundlesList;
    }

static AtEncapChannel *ChannelsListAllocate(AtModuleEncap encapModule)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint16 maxChannel = mMethodsGet(encapModule)->MaxChannelsGet(encapModule);
    uint32 size = sizeof(AtEncapChannel) * maxChannel;
    AtEncapChannel * channelsList = mMethodsGet(osal)->MemAlloc(osal, size);

    if (channelsList)
        mMethodsGet(osal)->MemInit(osal, channelsList, 0, size);

    return channelsList;
    }

static eAtRet Setup(AtModule self)
    {
    AtModuleEncap encapModule = (AtModuleEncap)self;
    eAtRet ret;
    uint32 maxBundles;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Delete all channels if they were created */
    if (encapModule->channels)
        AllChannelsDelete(encapModule);
    if (encapModule->bundles)
        AllBundlesDelete(encapModule);

    /* Create memory to hold all of encapsulation channels */
    encapModule->channels = ChannelsListAllocate(encapModule);
    if (encapModule->channels == NULL)
        return cAtErrorRsrcNoAvail;

    maxBundles = mMethodsGet(encapModule)->MaxBundlesGet(encapModule);
    if (maxBundles == 0)
        return cAtOk;

    /* Create memory to hold all of hdlc bundle */
    encapModule->bundles = BundlesListAllocate(maxBundles);
    if (encapModule->bundles == NULL)
        {
        /* Need free allocated channels mem before return */
        AllChannelsDelete(encapModule);
        return cAtErrorRsrcNoAvail;
        }

    return cAtOk;
    }

static eAtRet AllChannelsInit(AtModuleEncap self)
    {
    eAtRet ret = cAtOk;
    uint16 numChannels = AtModuleEncapMaxChannelsGet(self);
    uint16 i;

    for (i = 0; i < numChannels; i++)
        {
        AtEncapChannel encapChannel = AtModuleEncapChannelGet(self, i);
        if (encapChannel)
            ret |= AtChannelInit((AtChannel)encapChannel);
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret;

    /* Super initialization */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Initialize all channel */
    return AllChannelsInit((AtModuleEncap)self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete all of channels */
    AllChannelsDelete((AtModuleEncap)self);
    AllBundlesDelete((AtModuleEncap)self);

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleEncap object = (AtModuleEncap)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(channels, AtModuleEncapMaxChannelsGet(object));
    mEncodeObjects(bundles, AtModuleEncapMaxBundlesGet(object));
    }

static uint16 MaxChannelsGet(AtModuleEncap self)
    {
	AtUnused(self);
    /* Let's concrete class do */
    return 0;
    }

static eBool ChannelIdIsValid(AtModuleEncap self, uint32 channelId)
    {
    return (channelId < mMethodsGet(self)->MaxChannelsGet(self));
    }

static eBool ChannelIsBusy(AtModuleEncap self, uint32 channelId)
    {
    return self->channels[channelId] ? cAtTrue : cAtFalse;
    }

static AtHdlcChannel HdlcPppChannelCreate(AtModuleEncap self, uint32 channelId)
    {
    AtHdlcChannel channel;
    AtPppLink link;

    /* Check if ID is invalid or channel is busy */
    if (!ChannelIdIsValid(self, channelId) || ChannelIsBusy(self, channelId))
        return NULL;

    /* Create object */
    channel = mMethodsGet(self)->HdlcPppChannelObjectCreate(self, channelId);
    if (channel == NULL)
        return NULL;

    /* Create PPP link for this channel */
    link = mMethodsGet(self)->PppLinkObjectCreate(self, channel);
    if (link == NULL)
        {
        AtObjectDelete((AtObject)channel);
        return NULL;
        }

    AtHdlcChannelHdlcLinkSet(channel, (AtHdlcLink)link);

    /* Initialize and cache it */
    AtChannelInit((AtChannel)channel);
    AtChannelInit((AtChannel)link);
    self->channels[channelId] = (AtEncapChannel)channel;

    return channel;
    }

static AtHdlcChannel FrChannelCreate(AtModuleEncap self, uint32 channelId)
    {
    AtHdlcChannel channel;
    AtHdlcLink link;

    /* Check if ID is invalid or channel is busy */
    if (!ChannelIdIsValid(self, channelId) || ChannelIsBusy(self, channelId))
        return NULL;

    /* Create object */
    channel = mMethodsGet(self)->FrChannelObjectCreate(self, channelId);
    if (channel == NULL)
        return NULL;

    /* Create Fr link for this channel */
    link = mMethodsGet(self)->FrLinkObjectCreate(self, channel);
    if (link == NULL)
        {
        AtObjectDelete((AtObject)channel);
        return NULL;
        }
    AtHdlcChannelHdlcLinkSet(channel, (AtHdlcLink)link);

    /* Initialize and cache it */
    AtChannelInit((AtChannel)channel);
    AtChannelInit((AtChannel)link);
    self->channels[channelId] = (AtEncapChannel)channel;

    return channel;
    }

static AtHdlcLink FrLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    AtUnused(hdlcChannel);
    AtUnused(self);

    /* Concrete class will know how to create this link object */
    return NULL;
    }

static AtHdlcLink CiscoHdlcLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(hdlcChannel);
	AtUnused(self);
    /* Concrete class will know how to create this link object */
    return NULL;
    }

static AtHdlcChannel CiscoHdlcChannelCreate(AtModuleEncap self, uint32 channelId)
    {
    AtHdlcChannel channel;
    AtHdlcLink link;

    /* Check if ID is invalid or channel is busy */
    if (!ChannelIdIsValid(self, channelId) || ChannelIsBusy(self, channelId))
        return NULL;

    /* Create object */
    channel = mMethodsGet(self)->CiscoHdlcChannelObjectCreate(self, channelId);
    if (channel == NULL)
        return NULL;

    /* Create Cisco HDLC link for this channel */
    link = mMethodsGet(self)->CiscoHdlcLinkObjectCreate(self, channel);
    if (link == NULL)
        {
        AtObjectDelete((AtObject)channel);
        return NULL;
        }

    AtHdlcChannelHdlcLinkSet(channel, link);

    /* Initialize and cache it */
    AtChannelInit((AtChannel)channel);
    AtChannelInit((AtChannel)link);
    self->channels[channelId] = (AtEncapChannel)channel;

    return channel;
    }

static AtAtmTc AtmTcCreate(AtModuleEncap self, uint32 channelId)
    {
    AtAtmTc channel;

    /* Check if ID is invalid or channel is busy */
    if (!ChannelIdIsValid(self, channelId) || ChannelIsBusy(self, channelId))
        return NULL;

    /* Create object */
    channel = mMethodsGet(self)->AtmTcObjectCreate(self, channelId);
    if (channel == NULL)
        return NULL;

    /* Initialize and cache it */
    AtChannelInit((AtChannel)self);
    self->channels[channelId] = (AtEncapChannel)channel;

    return channel;
    }

static AtGfpChannel GfpChannelCreate(AtModuleEncap self, uint32 channelId)
    {
    AtGfpChannel channel;

    /* Check if ID is invalid or channel is busy */
    if (!ChannelIdIsValid(self, channelId) || ChannelIsBusy(self, channelId))
        return NULL;

    /* Create object */
    channel = mMethodsGet(self)->GfpChannelObjectCreate(self, channelId);
    if (channel == NULL)
        return NULL;

    /* Initialize and cache it */
    AtChannelInit((AtChannel)channel);
    self->channels[channelId] = (AtEncapChannel)channel;

    return channel;
    }

static AtPppLink PppLink(AtEncapChannel channel)
    {
    if ((AtEncapChannelEncapTypeGet(channel) == cAtEncapHdlc) &&
        (AtHdlcChannelFrameTypeGet((AtHdlcChannel)channel) == cAtHdlcFrmPpp))
        return (AtPppLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)channel);
    return NULL;
    }

static eBool LinkIsBusy(AtEncapChannel channel)
    {
    AtPppLink link = PppLink(channel);

    if (link == NULL)
        return cAtFalse;

    if (AtChannelBoundPwGet((AtChannel)link))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanDeleteChannel(AtEncapChannel channel)
    {
    if (AtEncapChannelBoundPhyGet(channel) != NULL)
        return cAtFalse;

    if (AtChannelBoundPwGet((AtChannel)channel) != NULL)
        return cAtFalse;

    if (LinkIsBusy(channel))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet ChannelDelete(AtModuleEncap self, uint32 channelId)
    {
    AtEncapChannel channel;

    /* Check if this channel is valid */
    if (!ChannelIdIsValid(self, channelId))
        return cAtErrorInvlParm;

    /* Do nothing if it is not created */
    channel = self->channels[channelId];
    if (channel == NULL)
        return cAtOk;

    /* Cannot delete if it is busy on working with other channel */
    if (!CanDeleteChannel(channel))
        return cAtErrorChannelBusy;

    /* Delete and remove it out of database */
    AtObjectDelete((AtObject)channel);
    self->channels[channelId] = NULL;

    return cAtOk;
    }

static uint16 FreeChannelGet(AtModuleEncap self)
    {
    uint16 i, maxChannels;

    /* Scan to find an empty slot */
    maxChannels = mMethodsGet(self)->MaxChannelsGet(self);
    for (i = 0; i < maxChannels; i++)
        {
        if (self->channels[i] == NULL)
            return i;
        }

    /* Return invalid ID */
    return maxChannels;
    }

static eAtRet HdlcISISMacSet(AtModuleEncap self, const uint8 *mac)
    {
	AtUnused(mac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet HdlcISISMacGet(AtModuleEncap self, uint8 *mac)
    {
	AtUnused(mac);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static AtHdlcChannel HdlcPppChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
	AtUnused(channelId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtHdlcChannel FrChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
	AtUnused(channelId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtHdlcChannel CiscoHdlcChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
	AtUnused(channelId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtAtmTc AtmTcObjectCreate(AtModuleEncap self, uint32 channelId)
    {
	AtUnused(channelId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtGfpChannel GfpChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    AtUnused(self);
    AtUnused(channelId);
    return NULL;
    }

static AtEncapChannel ChannelGet(AtModuleEncap self, uint32 channelId)
    {
    if (!ChannelIdIsValid(self, channelId))
        return NULL;

    /* Database has not been allocated */
    if (self->channels == NULL)
        return NULL;

    return self->channels[channelId];
    }

static AtPppLink PppLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(hdlcChannel);
	AtUnused(self);
    /* Concrete class will know how to create this link object */
    return NULL;
    }

/* Idle pattern */
static eAtRet IdlePatternSet(AtModuleEncap self, uint8 pattern)
    {
	AtUnused(pattern);
	AtUnused(self);
    /* Let concrete class implement */
    return cAtError;
    }

static uint8 IdlePatternGet(AtModuleEncap self)
    {
	AtUnused(self);
    /* Let concrete class implement */
    return 0;
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "encap";
    }

static const char *CapacityDescription(AtModule self)
    {
    AtModuleEncap moduleEncap = (AtModuleEncap)self;
    static char string[32];

    AtSprintf(string, "channel: %hu", AtModuleEncapMaxChannelsGet(moduleEncap));
    return string;
    }

static void StatusClear(AtModule self)
    {
    AtIterator channelIterator = AtModuleEncapChannelIteratorCreate((AtModuleEncap)self);
    AtChannel channel;

    while ((channel = (AtChannel)AtIteratorNext(channelIterator)) != NULL)
        AtChannelStatusClear(channel);

    AtObjectDelete((AtObject)channelIterator);
    }

static eAtRet DestroyResourceOfHdlcChannel(AtHdlcChannel hdlcChannel)
    {
    eAtHdlcFrameType frameType;
    AtPppLink link;

    frameType = AtHdlcChannelFrameTypeGet(hdlcChannel);
    if ((frameType != cAtHdlcFrmCiscoHdlc) &&
        (frameType != cAtHdlcFrmPpp))
        return cAtOk;

    link = (AtPppLink)AtHdlcChannelHdlcLinkGet(hdlcChannel);
    if (link == NULL)
        return cAtOk;

    return AtHdlcLinkFlowBind((AtHdlcLink)link, NULL);
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    AtModuleEncap encapModule = (AtModuleEncap)self;
    AtIterator channelIterator = AtModuleEncapChannelIteratorCreate(encapModule);
    AtEncapChannel encapChannel;
    eAtRet ret = cAtOk;

    /* Delete all created channels */
    while ((encapChannel = (AtEncapChannel)AtIteratorNext(channelIterator)) != NULL)
        {
        if (AtEncapChannelEncapTypeGet(encapChannel) == cAtEncapHdlc)
            ret |= DestroyResourceOfHdlcChannel((AtHdlcChannel)encapChannel);

        ret |= AtModuleEncapChannelDelete(encapModule, (uint16)AtChannelIdGet((AtChannel)encapChannel));
        }

    AtObjectDelete((AtObject)channelIterator);

    return ret;
    }

static eBool GfpIsSupported(AtModuleEncap self)
    {
    /* Concrete may know */
    AtUnused(self);
    return cAtFalse;
    }

static AtHdlcChannel HdlcLapsChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    /* Concrete may know */
    AtUnused(self);
    AtUnused(channelId);
    return NULL;
    }

static AtLapsLink LapsLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    /* Concrete may know */
    AtUnused(self);
    AtUnused(hdlcChannel);
    return NULL;
    }

static AtMfrBundle MfrBundleObjectCreate(AtModuleEncap self, uint32 bundleId)
    {
    /* Concrete may know */
    AtUnused(self);
    AtUnused(bundleId);
    return NULL;
    }

static AtMpBundle MpBundleObjectCreate(AtModuleEncap self, uint32 bundleId)
    {
    /* Concrete may know */
    AtUnused(self);
    AtUnused(bundleId);
    return NULL;
    }

static eBool BundleIdIsValid(AtModuleEncap self, uint32 bundleId)
    {
    return (bundleId < AtModuleEncapMaxBundlesGet(self)) ? cAtTrue : cAtFalse;
    }

static eBool BundleIsBusy(AtModuleEncap self, uint32 bundleId)
    {
    return self->bundles[bundleId] ? cAtTrue : cAtFalse;
    }

static AtHdlcBundle HdlcBundleCreate(AtModuleEncap self, uint32 bundleId,
                                     BundleObjectCreate funcCreate)
    {
    AtHdlcBundle bundle;

    /* Cannot create an invalid bundle or bundle is busy */
    if (!BundleIdIsValid(self, bundleId) || BundleIsBusy(self, bundleId))
        return NULL;

    /* Database has not been allocated */
    if (self->bundles == NULL)
        return NULL;

    /* Create object */
    bundle = funcCreate(self, bundleId);
    if (bundle == NULL)
        return NULL;

    /* Cannot create a bundle for more than one times */
    if (AtHdlcBundleIsValid(bundle))
        return NULL;

    /* Initialize and cache it */
    AtHdlcBundleValidate(bundle, cAtTrue);
    AtChannelInit((AtChannel) bundle);
    self->bundles[bundleId] = bundle;

    return bundle;
    }

static AtMfrBundle MfrBundleCreate(AtModuleEncap self, uint32 bundleId)
    {
    return (AtMfrBundle)HdlcBundleCreate(self, bundleId, (BundleObjectCreate)mMethodsGet(self)->MfrBundleObjectCreate);
    }

static AtMpBundle MpBundleCreate(AtModuleEncap self, uint32 bundleId)
    {
    return (AtMpBundle)HdlcBundleCreate(self, bundleId, (BundleObjectCreate)(mMethodsGet(self)->MpBundleObjectCreate));
    }

static eAtRet BundleDelete(AtModuleEncap self, uint32 bundleId)
    {
    AtHdlcBundle bundle;
    if (!BundleIdIsValid(self, bundleId))
        return cAtErrorOutOfRangParm;

    bundle = AtModuleEncapBundleGet(self, bundleId);
    if (bundle == NULL)
        return cAtOk;

    /* Delete and remove it out of database */
    AtObjectDelete((AtObject)bundle);
    self->bundles[bundleId] = NULL;

    return cAtOk;
    }

static AtHdlcBundle BundleGet(AtModuleEncap self, uint32 bundleId)
    {
    if (!BundleIdIsValid(self, bundleId))
        return NULL;

    /* Database has not been allocated */
    if (self->bundles == NULL)
        return NULL;

    return self->bundles[bundleId];
    }

static uint32 MaxBundlesGet(AtModuleEncap self)
    {
    AtUnused(self);
    return 0;
    }

static void EncapChannelRegsShow(AtModuleEncap self, AtEncapChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    }

static void HdlcBundleRegsShow(AtModuleEncap self, AtHdlcBundle bundle)
    {
    AtUnused(self);
    AtUnused(bundle);
    }

static void MethodInit(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_methods, MaxChannelsGet);
        mMethodOverride(m_methods, HdlcPppChannelCreate);
        mMethodOverride(m_methods, FrChannelCreate);
        mMethodOverride(m_methods, CiscoHdlcChannelCreate);
        mMethodOverride(m_methods, AtmTcCreate);
        mMethodOverride(m_methods, GfpChannelCreate);
        mMethodOverride(m_methods, ChannelGet);
        mMethodOverride(m_methods, ChannelDelete);
        mMethodOverride(m_methods, FreeChannelGet);
        mMethodOverride(m_methods, HdlcISISMacSet);
        mMethodOverride(m_methods, HdlcISISMacGet);
        mMethodOverride(m_methods, HdlcPppChannelObjectCreate);
        mMethodOverride(m_methods, FrChannelObjectCreate);
        mMethodOverride(m_methods, CiscoHdlcChannelObjectCreate);
        mMethodOverride(m_methods, PppLinkObjectCreate);
        mMethodOverride(m_methods, AtmTcObjectCreate);
        mMethodOverride(m_methods, GfpChannelObjectCreate);
        mMethodOverride(m_methods, CiscoHdlcLinkObjectCreate);
        mMethodOverride(m_methods, IdlePatternSet);
        mMethodOverride(m_methods, IdlePatternGet);
        mMethodOverride(m_methods, FrLinkObjectCreate);
        mMethodOverride(m_methods, GfpIsSupported);
        mMethodOverride(m_methods, HdlcLapsChannelObjectCreate);
        mMethodOverride(m_methods, LapsLinkObjectCreate);
        mMethodOverride(m_methods, MfrBundleCreate);
        mMethodOverride(m_methods, MfrBundleObjectCreate);
        mMethodOverride(m_methods, MpBundleCreate);
        mMethodOverride(m_methods, MpBundleObjectCreate);
        mMethodOverride(m_methods, BundleDelete);
        mMethodOverride(m_methods, BundleGet);
        mMethodOverride(m_methods, MaxBundlesGet);
        mMethodOverride(m_methods, EncapChannelRegsShow);
        mMethodOverride(m_methods, HdlcBundleRegsShow);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModuleEncap self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        mMethodOverride(m_AtModuleOverride, AllServicesDestroy);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleEncap self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

/* Constructor of Encap module object */
AtModuleEncap AtModuleEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModuleEncap));

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleEncap, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodInit(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcLink AtModuleEncapLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChanel, uint8 frameType)
    {
    if (self == NULL)
        return NULL;

    if (frameType == cAtHdlcFrmFr)
        return (AtHdlcLink)mMethodsGet(self)->FrLinkObjectCreate(self, hdlcChanel);
    if (frameType == cAtHdlcFrmPpp)
        return (AtHdlcLink)mMethodsGet(self)->PppLinkObjectCreate(self, hdlcChanel);
    if (frameType == cAtHdlcFrmCiscoHdlc)
        return mMethodsGet(self)->CiscoHdlcLinkObjectCreate(self, hdlcChanel);

    return NULL;
    }

AtGfpChannel AtModuleEncapGfpChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    if (self)
        return mMethodsGet(self)->GfpChannelObjectCreate(self, channelId);
    return NULL;
    }

AtHdlcChannel AtModuleEncapHdlcLapsChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    if (self)
        return mMethodsGet(self)->HdlcLapsChannelObjectCreate(self, channelId);
    return NULL;
    }

AtLapsLink AtModuleEncapLapsLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
    if (self)
        return mMethodsGet(self)->LapsLinkObjectCreate(self, hdlcChannel);
    return NULL;
    }

/**
 * @addtogroup AtModuleEncap
 * @{
 */

/**
 * Get maximum number of encapsulation channels that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of encapsulation channels that this module can support
 */
uint16 AtModuleEncapMaxChannelsGet(AtModuleEncap self)
    {
    mAttributeGet(MaxChannelsGet, uint16, 0);
    }

/**
 * Create HDLC Encapsulation channel contains PPP frame
 *
 * @param self This module
 * @param channelId Encapsulation channel id
 * @return Instance of HDCL Encapsulation channel
 */
AtHdlcChannel AtModuleEncapHdlcPppChannelCreate(AtModuleEncap self, uint16 channelId)
    {
    mOneParamAttributeGet(HdlcPppChannelCreate, channelId, AtHdlcChannel, NULL);
    }

/**
 * Create Frame relay Encapsulation channel
 *
 * @param self This module
 * @param channelId Encapsulation channel ID
 * @return Instance of Frame relay Encapsulation channel
 */
AtHdlcChannel AtModuleEncapFrChannelCreate(AtModuleEncap self, uint16 channelId)
    {
    mOneParamAttributeGet(FrChannelCreate, channelId, AtHdlcChannel, NULL);
    }

/**
 * Create Ciso HDLC encapsulation channel
 *
 * @param self This module
 * @param channelId Encapsulation channel ID
 * @return Instance of HDLC Encapsulation channel
 */
AtHdlcChannel AtModuleEncapCiscoHdlcChannelCreate(AtModuleEncap self, uint16 channelId)
    {
    mOneParamAttributeGet(CiscoHdlcChannelCreate, channelId, AtHdlcChannel, NULL);
    }

/**
 * Create ATM TC
 *
 * @param self This module
 * @param channelId Encapsulation channel ID
 *
 * @return Instance of ATM TC
 */
AtAtmTc AtModuleEncapAtmTcCreate(AtModuleEncap self, uint16 channelId)
    {
    mOneParamAttributeGet(AtmTcCreate, channelId, AtAtmTc, NULL);
    }

/**
 * Get Encapsulation channel by its ID
 *
 * @param self This module
 * @param channelId Encapsulation channel id
 * @return Instance of Encapsulation channel
 */
AtEncapChannel AtModuleEncapChannelGet(AtModuleEncap self, uint16 channelId)
    {
    mOneParamAttributeGet(ChannelGet, channelId, AtEncapChannel, NULL);
    }

/**
 * Delete Encapsulation channel by its ID
 *
 * @param self This module
 * @param channelId Encapsulation channel id
 * @return AT return code
 */
eAtModuleEncapRet AtModuleEncapChannelDelete(AtModuleEncap self, uint16 channelId)
    {
    eAtRet ret;

    mNullObjectCheck(cAtErrorNullPointer);
    AtModuleLock((AtModule)self);
    ret = mMethodsGet(self)->ChannelDelete(self, channelId);
    AtModuleUnLock((AtModule)self);

    return ret;
    }

/**
 * Get a channel that has not been used
 *
 * @param self This module
 *
 * @return ID of encapsulation channel that has not been used
 */
uint32 AtModuleEncapFreeChannelGet(AtModuleEncap self)
    {
    mAttributeGet(FreeChannelGet, uint32, 0x0);
    }

/**
 * Create GFP encapsulation channel
 *
 * @param self This module
 * @param channelId Encapsulation channel ID
 *
 * @return GFP channel if success, NULL if fail
 */
AtGfpChannel AtModuleEncapGfpChannelCreate(AtModuleEncap self, uint16 channelId)
    {
    mOneParamAttributeGet(GfpChannelCreate, channelId, AtGfpChannel, NULL);
    }

/**
 * Create Iterator for channels managed by this module
 *
 * @param self This module
 * @return Iterator object
 */
AtIterator AtModuleEncapChannelIteratorCreate(AtModuleEncap self)
    {
    uint16 maxChannel;
    AtArrayIterator iterator;

    mNullObjectCheck(NULL);

    maxChannel = mMethodsGet(self)->MaxChannelsGet(self);
    iterator = AtArrayIteratorNew(self->channels, maxChannel);

    if (iterator == NULL)
        return NULL;

    /* Override */
    IteratorOverride(iterator);

    m_arrayIteratorMethodsInit = 1;

    return (AtIterator)iterator;
    }

/**
 * Set Idle pattern when physical interface is unused
 *
 * @param self This Module
 * @param pattern Idle pattern
 * @return AT return code
 */
eAtRet AtModuleEncapIdlePatternSet(AtModuleEncap self, uint8 pattern)
    {
    mNumericalAttributeSet(IdlePatternSet, pattern);
    }

/**
 * Get configured Idle pattern when physical interface is unused
 *
 * @param self This module
 * @return Idle pattern
 */
uint8 AtModuleEncapIdlePatternGet(AtModuleEncap self)
    {
    mAttributeGet(IdlePatternGet, uint8, 0x0);
    }

/**
 * To check if GFP encapsulation is supported
 *
 * @param self This module
 * @retval cAtTrue if GFP is supported
 * @retval cAtFalse if GFP is not supported
 */
eBool AtModuleEncapGfpIsSupported(AtModuleEncap self)
    {
    mAttributeGet(GfpIsSupported, eBool, cAtFalse);
    }

/**
 * Get maximum number of bundles that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of bundles that this module can support
 */
uint32 AtModuleEncapMaxBundlesGet(AtModuleEncap self)
    {
    mAttributeGet(MaxBundlesGet, uint32, 0);
    }

/**
 * Create MLPPP bundle
 *
 * @param self This module
 * @param bundleId ID of bundle to be created
 *
 * @return Bundle instance if success. NULL if it has been created.
 */
AtMpBundle AtModuleEncapMpBundleCreate(AtModuleEncap self, uint32 bundleId)
    {
    mOneParamAttributeGet(MpBundleCreate, bundleId, AtMpBundle, NULL);
    }

/**
 * Create MFR bundle
 *
 * @param self This module
 * @param bundleId ID of bundle to be created
 *
 * @return Bundle instance if success. NULL if it has been created.
 */
AtMfrBundle AtModuleEncapMfrBundleCreate(AtModuleEncap self, uint32 bundleId)
    {
    mOneParamAttributeGet(MfrBundleCreate, bundleId, AtMfrBundle, NULL);
    }

/**
 * Delete a bundle which has been created.
 *
 * @param self This module
 * @param bundleId Bundle ID to delete
 *
 * @return AT return code.
 */
eAtRet AtModuleEncapBundleDelete(AtModuleEncap self, uint32 bundleId)
    {
    mOneParamAttributeGet(BundleDelete, bundleId, eAtRet, cAtErrorNullPointer);
    }

/**
 * Get bundle instance by its ID
 *
 * @param self This module
 * @param bundleId Bundle ID
 *
 * @return Bundle instance. NULL is returned if it has not been created.
 */
AtHdlcBundle AtModuleEncapBundleGet(AtModuleEncap self, uint32 bundleId)
    {
    mOneParamAttributeGet(BundleGet, bundleId, AtHdlcBundle, NULL);
    }

/**
 * Create Iterator for bundles managed by this module
 *
 * @param self This module
 * @return Iterator object
 */
AtIterator AtModuleEncapBundleIteratorCreate(AtModuleEncap self)
    {
    uint32 maxBundles;
    AtArrayIterator iterator;

    mNullObjectCheck(NULL);

    maxBundles = mMethodsGet(self)->MaxBundlesGet(self);
    iterator = AtArrayIteratorNew(self->bundles, maxBundles);

    if (iterator == NULL)
        return NULL;

    BundleIteratorOverride(iterator);
    m_bundleIteratorMethodsInit = 1;

    return (AtIterator)iterator;
    }

/**
 * @}
 */

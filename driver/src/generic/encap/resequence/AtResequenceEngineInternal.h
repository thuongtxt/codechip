/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtResequenceEngineInternal.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRESEQUENCEENGINEINTERNAL_H_
#define _ATRESEQUENCEENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../common/AtObjectInternal.h"
#include "AtResequenceEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtResequenceEngineMethods
    {
    eAtRet (*Init)(AtResequenceEngine self);

    /* Enabling */
    eAtRet (*Enable)(AtResequenceEngine self, eBool enabled);
    eBool (*IsEnabled)(AtResequenceEngine self);

    /* Output queue for QoS */
    eAtRet (*OutputQueueSet)(AtResequenceEngine self, uint32 queueId);
    uint32 (*OutputQueueGet)(AtResequenceEngine self);
    eAtRet (*OutputQueueEnable)(AtResequenceEngine self, eBool enabled);
    eBool (*OutputQueueIsEnabled)(AtResequenceEngine self);

    /* To flush all of data */
    eAtRet (*Flush)(AtResequenceEngine self);

    /* Bundle association */
    eAtRet (*HdlcBundleSet)(AtResequenceEngine self, AtHdlcBundle bundle);
    AtHdlcBundle (*HdlcBundleGet)(AtResequenceEngine self);

    /* Flow association */
    eAtRet (*EthFlowSet)(AtResequenceEngine self, AtEthFlow flow);
    AtEthFlow (*EthFlowGet)(AtResequenceEngine self);

    /* Re-sequence timeout */
    eAtRet (*TimeoutSet)(AtResequenceEngine self, uint32 timeoutMs);
    uint32 (*TimeoutGet)(AtResequenceEngine self);

    /* MSRU */
    eAtRet (*MsruSet)(AtResequenceEngine self, uint32 msru);
    uint32 (*MsruGet)(AtResequenceEngine self);

    /* MRRU */
    eAtRet (*MrruSet)(AtResequenceEngine self, uint32 mrru);
    uint32 (*MrruGet)(AtResequenceEngine self);

    /* Internal methods */
    eAtRet (*Activate)(AtResequenceEngine self);
    eAtRet (*DeActivate)(AtResequenceEngine self);
    eBool (*HardwareEngineIsReady)(AtResequenceEngine self);
    }tAtResequenceEngineMethods;

typedef struct tAtResequenceEngine
    {
    tAtObject super;
    const tAtResequenceEngineMethods *methods;

    /* Private data */
    uint32 engineId;
    AtResequenceManager manager;
    AtHdlcBundle bundle;
    AtEthFlow flow;
    }tAtResequenceEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtResequenceEngine AtResequenceEngineObjectInit(AtResequenceEngine self, AtResequenceManager manager, uint32 engineId);

AtResequenceManager AtResequenceEngineManagerGet(AtResequenceEngine self);
uint32 AtResequenceEngineRead(AtResequenceEngine self, uint32 regAddress);
void AtResequenceEngineWrite(AtResequenceEngine self, uint32 regAddress, uint32 regValue);


#ifdef __cplusplus
}
#endif
#endif /* _ATRESEQUENCEENGINEINTERNAL_H_ */


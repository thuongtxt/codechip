/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtResequenceManager.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence engine management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRESEQUENCEMANAGER_H_
#define _ATRESEQUENCEMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtResequenceEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtResequenceManager *AtResequenceManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtResequenceManagerInit(AtResequenceManager self);
uint32 AtResequenceManagerNumEngines(AtResequenceManager self);
AtResequenceEngine AtResequenceManagerEngineGet(AtResequenceManager self, uint32 engineId);
AtResequenceEngine AtResequenceManagerFreeEngineGet(AtResequenceManager self);
eAtRet AtResequenceManagerEngineUnuse(AtResequenceManager self, AtResequenceEngine engine);
eAtRet AtResequenceManagerEngineUse(AtResequenceManager self, AtResequenceEngine engine);
AtDevice AtResequenceManagerDevice(AtResequenceManager self);
uint32 AtResequenceManagerBaseAddress(AtResequenceManager self);
void AtResequenceManagerDebug(AtResequenceManager self);

#ifdef __cplusplus
}
#endif
#endif /* _ATRESEQUENCEMANAGER_H_ */


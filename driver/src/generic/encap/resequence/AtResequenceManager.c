/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtResequenceManager.c
 *
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence engine management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtResequenceEngine.h"
#include "AtResequenceManagerInternal.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtResequenceManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtResequenceManagerMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void AllEnginesDelete(AtResequenceManager self)
    {
    AtResequenceEngine engine;

    if (self->engines == NULL)
        return;

    while ((engine = (AtResequenceEngine)AtListObjectRemoveAtIndex(self->engines, 0)) != NULL)
        AtObjectDelete((AtObject)engine);

    AtObjectDelete((AtObject)self->engines);
    self->engines = NULL;
    }

static eAtRet AllEnginesCreate(AtResequenceManager self)
    {
    uint32 engineId;
    uint32 numEngines = mMethodsGet(self)->NumEngines(self);

    /* Create list to cache Engines */
    self->engines = AtListCreate(numEngines);
    if (self->engines == NULL)
        return cAtError;

    /* Create each engine */
    for (engineId = 0; engineId < numEngines; engineId++)
        {
        AtResequenceEngine engine = mMethodsGet(self)->EngineObjectCreate(self, engineId);
        if (AtResequenceEngineInit(engine) != cAtOk)
            {
            AtObjectDelete((AtObject)engine);
            AllEnginesDelete(self);
            return cAtError;
            }

        AtListObjectAdd(self->engines, (AtObject)engine);
        }

    return cAtOk;
    }

static eAtModuleEncapRet Init(AtResequenceManager self)
    {
    AllEnginesDelete(self);
    return AllEnginesCreate(self);
    }

static uint32 NumEngines(AtResequenceManager self)
    {
    AtUnused(self);
    return 0;
    }

static AtResequenceEngine EngineGet(AtResequenceManager self, uint32 engineId)
    {
    return (AtResequenceEngine)AtListObjectGet(self->engines, engineId);
    }

static AtResequenceEngine FreeEngineGet(AtResequenceManager self)
    {
    AtUnused(self);
    return NULL;
    }

static AtResequenceEngine EngineObjectCreate(AtResequenceManager self, uint32 engineId)
    {
    AtUnused(self);
    AtUnused(engineId);
    return NULL;
    }

static uint32 BaseAddress(AtResequenceManager self)
    {
    AtUnused(self);
    return 0;
    }

static void Debug(AtResequenceManager self)
    {
    AtUnused(self);
    }

static eAtRet EngineUnuse(AtResequenceManager self, AtResequenceEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    return cAtErrorNotImplemented;
    }

static eAtRet EngineUse(AtResequenceManager self, AtResequenceEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    return cAtErrorNotImplemented;
    }

static void Delete(AtObject self)
    {
    AllEnginesDelete((AtResequenceManager)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtResequenceManager object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(device);
    mEncodeList(engines);
    }

static const char *ToString(AtObject self)
    {
    static char str[64];
    AtDevice dev = AtResequenceManagerDevice((AtResequenceManager)self);

    AtSprintf(str, "%sresequence_manager", AtDeviceIdToString(dev));
    return str;
    }

static void MethodInit(AtResequenceManager self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, BaseAddress);
        mMethodOverride(m_methods, NumEngines);
        mMethodOverride(m_methods, EngineGet);
        mMethodOverride(m_methods, EngineUse);
        mMethodOverride(m_methods, FreeEngineGet);
        mMethodOverride(m_methods, EngineUnuse);
        mMethodOverride(m_methods, EngineObjectCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtResequenceManager self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtResequenceManager self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtResequenceManager);
    }

AtResequenceManager AtResequenceManagerObjectInit(AtResequenceManager self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodInit(self);
    m_methodsInit = 1;

    mThis(self)->device = device;
    return self;
    }

AtDevice AtResequenceManagerDevice(AtResequenceManager self)
    {
    if (self)
        return self->device;
    return NULL;
    }

eAtRet AtResequenceManagerEngineUnuse(AtResequenceManager self, AtResequenceEngine engine)
    {
    if (self)
        return mMethodsGet(self)->EngineUnuse(self, engine);
    return cAtErrorNullPointer;
    }

eAtRet AtResequenceManagerEngineUse(AtResequenceManager self, AtResequenceEngine engine)
    {
    if (self)
        return mMethodsGet(self)->EngineUse(self, engine);
    return cAtErrorNullPointer;
    }

uint32 AtResequenceManagerBaseAddress(AtResequenceManager self)
    {
    if (self)
        return mMethodsGet(self)->BaseAddress(self);
    return 0;
    }

eAtRet AtResequenceManagerInit(AtResequenceManager self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }

uint32 AtResequenceManagerNumEngines(AtResequenceManager self)
    {
    if (self)
        return mMethodsGet(self)->NumEngines(self);
    return 0;
    }

AtResequenceEngine AtResequenceManagerEngineGet(AtResequenceManager self, uint32 engineId)
    {
    if (self)
        return mMethodsGet(self)->EngineGet(self, engineId);
    return NULL;
    }

AtResequenceEngine AtResequenceManagerFreeEngineGet(AtResequenceManager self)
    {
    if (self)
        return mMethodsGet(self)->FreeEngineGet(self);
    return NULL;
    }

void AtResequenceManagerDebug(AtResequenceManager self)
    {
    if (self == NULL)
        return;

    AtPrintc(cSevInfo, "Resequence manager information\r\n");
    AtPrintc(cSevInfo, "===============================\r\n");
    mMethodsGet(self)->Debug(self);
    }


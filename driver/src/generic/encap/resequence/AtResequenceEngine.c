/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtResequenceEngine.c
 *
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "AtResequenceManagerInternal.h"
#include "AtResequenceEngineInternal.h"
#include "../../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtResequenceEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtResequenceEngineMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtResequenceEngine self)
    {
    eAtRet ret;

    ret = AtResequenceEngineEnable(self, cAtFalse);
    ret |= AtResequenceEngineOutputQueueSet(self, 0);
    ret |= AtResequenceEngineOutputQueueEnable(self, cAtFalse);
    ret |= AtResequenceEngineMsruSet(self, 128);
    ret |= AtResequenceEngineMrruSet(self, 9600);

    return cAtOk;
    }

static uint32 IdGet(AtResequenceEngine self)
    {
    return self->engineId;
    }

static eAtRet Enable(AtResequenceEngine self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtErrorNotImplemented;
    }

static eBool IsEnabled(AtResequenceEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet OutputQueueSet(AtResequenceEngine self, uint32 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);
    return cAtErrorNotImplemented;
    }

static uint32 OutputQueueGet(AtResequenceEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet OutputQueueEnable(AtResequenceEngine self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtErrorNotImplemented;
    }

static eBool OutputQueueIsEnabled(AtResequenceEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Flush(AtResequenceEngine self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet HdlcBundleSet(AtResequenceEngine self, AtHdlcBundle bundle)
    {
    self->bundle = bundle;
    return cAtOk;
    }

static AtHdlcBundle HdlcBundleGet(AtResequenceEngine self)
    {
    return self->bundle;
    }

static eAtRet EthFlowSet(AtResequenceEngine self, AtEthFlow flow)
    {
    self->flow = flow;
    return cAtOk;
    }

static AtEthFlow EthFlowGet(AtResequenceEngine self)
    {
    return self->flow;
    }

static eAtRet TimeoutSet(AtResequenceEngine self, uint32 timeoutMs)
    {
    AtUnused(self);
    AtUnused(timeoutMs);
    return cAtErrorNotImplemented;
    }

static uint32 TimeoutGet(AtResequenceEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet MsruSet(AtResequenceEngine self, uint32 msru)
    {
    AtUnused(self);
    AtUnused(msru);
    return cAtErrorNotImplemented;
    }

static uint32 MsruGet(AtResequenceEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet MrruSet(AtResequenceEngine self, uint32 mrru)
    {
    AtUnused(self);
    AtUnused(mrru);
    return cAtErrorNotImplemented;
    }

static uint32 MrruGet(AtResequenceEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eBool HardwareEngineIsReady(AtResequenceEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Activate(AtResequenceEngine self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet DeActivate(AtResequenceEngine self)
    {
    AtResequenceEngineHdlcBundleSet(self, NULL);
    AtResequenceEngineEthFlowSet(self, NULL);
    return cAtOk;
    }

static AtHal Hal(AtResequenceEngine self)
    {
    AtDevice device = AtResequenceManagerDevice(AtResequenceEngineManagerGet(self));
    AtIpCore core = AtDeviceIpCoreGet(device, 0);
    return AtIpCoreHalGet(core);
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtResequenceManager manager = AtResequenceEngineManagerGet(mThis(self));
    AtDevice dev = AtResequenceManagerDevice(manager);
    AtSnprintf(description, sizeof(description), "%sresequence_engine.%d", AtDeviceIdToString(dev), AtResequenceEngineIdGet(mThis(self)) + 1);
    return description;
    }

static void MethodInit(AtResequenceEngine self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Flush);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, OutputQueueSet);
        mMethodOverride(m_methods, OutputQueueGet);
        mMethodOverride(m_methods, OutputQueueEnable);
        mMethodOverride(m_methods, OutputQueueIsEnabled);
        mMethodOverride(m_methods, HdlcBundleSet);
        mMethodOverride(m_methods, HdlcBundleGet);
        mMethodOverride(m_methods, EthFlowSet);
        mMethodOverride(m_methods, EthFlowGet);
        mMethodOverride(m_methods, TimeoutSet);
        mMethodOverride(m_methods, TimeoutGet);
        mMethodOverride(m_methods, MsruSet);
        mMethodOverride(m_methods, MsruGet);
        mMethodOverride(m_methods, MrruSet);
        mMethodOverride(m_methods, MrruGet);
        mMethodOverride(m_methods, Activate);
        mMethodOverride(m_methods, DeActivate);
        mMethodOverride(m_methods, HardwareEngineIsReady);
        }

    mMethodsSet(self, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtResequenceEngine object = (AtResequenceEngine)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(engineId);
    mEncodeObjectDescription(manager);
    mEncodeObjectDescription(bundle);
    mEncodeObjectDescription(flow);
    }

static void OverrideAtObject(AtResequenceEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtResequenceEngine self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtResequenceEngine);
    }

AtResequenceEngine AtResequenceEngineObjectInit(AtResequenceEngine self, AtResequenceManager manager, uint32 engineId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodInit(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->manager  = manager;
    mThis(self)->engineId = engineId;

    return self;
    }

uint32 AtResequenceEngineRead(AtResequenceEngine self, uint32 regAddress)
    {
    if (self)
        return AtHalRead(Hal(self), regAddress);
    return 0xDEADCAFE;
    }

void AtResequenceEngineWrite(AtResequenceEngine self, uint32 regAddress, uint32 regValue)
    {
    if (self)
        AtHalWrite(Hal(self), regAddress, regValue);
    }

AtResequenceManager AtResequenceEngineManagerGet(AtResequenceEngine self)
    {
    if (self)
        return self->manager;
    return NULL;
    }

eAtRet AtResequenceEngineInit(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }

uint32 AtResequenceEngineIdGet(AtResequenceEngine self)
    {
    if (self)
        return IdGet(self);
    return cInvalidUint32;
    }

eAtRet AtResequenceEngineEnable(AtResequenceEngine self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->Enable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool AtResequenceEngineIsEnabled(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->IsEnabled(self);
    return cAtFalse;
    }

eAtRet AtResequenceEngineOutputQueueSet(AtResequenceEngine self, uint32 queueId)
    {
    if (self)
        return mMethodsGet(self)->OutputQueueSet(self, queueId);
    return cAtErrorNullPointer;
    }

uint32 AtResequenceEngineOutputQueueGet(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->OutputQueueGet(self);
    return cInvalidUint32;
    }

eAtRet AtResequenceEngineOutputQueueEnable(AtResequenceEngine self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->OutputQueueEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool AtResequenceEngineOutputQueueIsEnabled(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->OutputQueueIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtResequenceEngineFlush(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->Flush(self);
    return cAtErrorNullPointer;
    }

eAtRet AtResequenceEngineHdlcBundleSet(AtResequenceEngine self, AtHdlcBundle bundle)
    {
    if (self)
        return mMethodsGet(self)->HdlcBundleSet(self, bundle);
    return cAtErrorNullPointer;
    }

AtHdlcBundle AtResequenceEngineHdlcBundleGet(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->HdlcBundleGet(self);
    return NULL;
    }

eAtRet AtResequenceEngineEthFlowSet(AtResequenceEngine self, AtEthFlow flow)
    {
    if (self)
        return mMethodsGet(self)->EthFlowSet(self, flow);
    return cAtErrorNullPointer;
    }

AtEthFlow AtResequenceEngineEthFlowGet(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->EthFlowGet(self);
    return NULL;
    }

eAtRet AtResequenceEngineTimeoutSet(AtResequenceEngine self, uint32 timeoutMs)
    {
    if (self)
        return mMethodsGet(self)->TimeoutSet(self, timeoutMs);
    return cAtErrorNullPointer;
    }

uint32 AtResequenceEngineTimeoutGet(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->TimeoutGet(self);
    return 0;
    }

eAtRet AtResequenceEngineMsruSet(AtResequenceEngine self, uint32 msru)
    {
    if (self)
        return mMethodsGet(self)->MsruSet(self, msru);
    return cAtErrorNullPointer;
    }

uint32 AtResequenceEngineMsruGet(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->MsruGet(self);
    return 0;
    }

eAtRet AtResequenceEngineMrruSet(AtResequenceEngine self, uint32 mrru)
    {
    if (self)
        return mMethodsGet(self)->MrruSet(self, mrru);
    return cAtErrorNullPointer;
    }

uint32 AtResequenceEngineMrruGet(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->MrruGet(self);
    return 0;
    }

eAtRet AtResequenceEngineActivate(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->Activate(self);
    return cAtErrorNullPointer;
    }

eAtRet AtResequenceEngineDeActivate(AtResequenceEngine self)
    {
    if (self)
        return mMethodsGet(self)->DeActivate(self);
    return cAtErrorNullPointer;
    }

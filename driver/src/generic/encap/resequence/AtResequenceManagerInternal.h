/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtResequenceManagerInternal.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence engine management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRESEQUENCEMANAGERINTERNAL_H_
#define _ATRESEQUENCEMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../common/AtObjectInternal.h"
#include "AtResequenceManager.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtResequenceManagerMethods
    {
    eAtModuleEncapRet (*Init)(AtResequenceManager self);
    uint32 (*NumEngines)(AtResequenceManager self);
    AtResequenceEngine (*EngineGet)(AtResequenceManager self, uint32 engineId);
    AtResequenceEngine (*FreeEngineGet)(AtResequenceManager self);
    void (*Debug)(AtResequenceManager self);

    /* Internal methods */
    uint32 (*BaseAddress)(AtResequenceManager self);
    AtResequenceEngine (*EngineObjectCreate)(AtResequenceManager self, uint32 engineId);
    eAtRet (*EngineUse)(AtResequenceManager self, AtResequenceEngine engine);
    eAtRet (*EngineUnuse)(AtResequenceManager self, AtResequenceEngine engine);
    }tAtResequenceManagerMethods;

typedef struct tAtResequenceManager
    {
    tAtObject super;
    const tAtResequenceManagerMethods *methods;

    /* Private data */
    AtDevice device;
    AtList engines;
    }tAtResequenceManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtResequenceManager AtResequenceManagerObjectInit(AtResequenceManager self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATRESEQUENCEMANAGERINTERNAL_H_ */


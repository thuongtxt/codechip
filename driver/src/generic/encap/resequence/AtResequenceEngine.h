/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtResequenceEngine.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : Re-sequence engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRESEQUENCEENGINE_H_
#define _ATRESEQUENCEENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtHdlcBundle.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtResequenceEngine  *AtResequenceEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtResequenceEngineInit(AtResequenceEngine self);
uint32 AtResequenceEngineIdGet(AtResequenceEngine self);

/* Enabling */
eAtRet AtResequenceEngineEnable(AtResequenceEngine self, eBool enabled);
eBool AtResequenceEngineIsEnabled(AtResequenceEngine self);

/* Output queue for QoS */
eAtRet AtResequenceEngineOutputQueueSet(AtResequenceEngine self, uint32 queueId);
uint32 AtResequenceEngineOutputQueueGet(AtResequenceEngine self);
eAtRet AtResequenceEngineOutputQueueEnable(AtResequenceEngine self, eBool enabled);
eBool AtResequenceEngineOutputQueueIsEnabled(AtResequenceEngine self);

/* To flush all of data */
eAtRet AtResequenceEngineFlush(AtResequenceEngine self);

/* Bundle association */
eAtRet AtResequenceEngineHdlcBundleSet(AtResequenceEngine self, AtHdlcBundle bundle);
AtHdlcBundle AtResequenceEngineHdlcBundleGet(AtResequenceEngine self);

/* Flow association */
eAtRet AtResequenceEngineEthFlowSet(AtResequenceEngine self, AtEthFlow flow);
AtEthFlow AtResequenceEngineEthFlowGet(AtResequenceEngine self);

/* Resequence timeout */
eAtRet AtResequenceEngineTimeoutSet(AtResequenceEngine self, uint32 timeoutMs);
uint32 AtResequenceEngineTimeoutGet(AtResequenceEngine self);

/* MSRU */
eAtRet AtResequenceEngineMsruSet(AtResequenceEngine self, uint32 msru);
uint32 AtResequenceEngineMsruGet(AtResequenceEngine self);

/* MRRU */
eAtRet AtResequenceEngineMrruSet(AtResequenceEngine self, uint32 mrru);
uint32 AtResequenceEngineMrruGet(AtResequenceEngine self);

/* Engine activation */
eAtRet AtResequenceEngineActivate(AtResequenceEngine self);
eAtRet AtResequenceEngineDeActivate(AtResequenceEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATRESEQUENCEENGINE_H_ */


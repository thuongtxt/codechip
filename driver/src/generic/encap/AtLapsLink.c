/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : AtLapsLink.c
 *
 * Created Date: May 24, 2016
 *
 * Description : LAPS link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtLapsLink.h"
#include "AtLapsLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtLapsLinkMethods m_methods;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "laps";
    }

static eBool TxSapiCanModify(AtLapsLink self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TxSapiSet(AtLapsLink self, uint16 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint16 TxSapiGet(AtLapsLink self)
    {
    AtUnused(self);
    return 0x0;
    }

static eBool SapiMonitorCanEnable(AtLapsLink self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eAtRet SapiMonitorEnable(AtLapsLink self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool SapiMonitorIsEnabled(AtLapsLink self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool ExpectedSapiCanModify(AtLapsLink self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ExpectedSapiSet(AtLapsLink self, uint16 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint16 ExpectedSapiGet(AtLapsLink self)
    {
    AtUnused(self);
    return 0x0;
    }

static void MethodsInit(AtLapsLink self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TxSapiCanModify);
        mMethodOverride(m_methods, TxSapiSet);
        mMethodOverride(m_methods, TxSapiGet);
        mMethodOverride(m_methods, SapiMonitorCanEnable);
        mMethodOverride(m_methods, SapiMonitorEnable);
        mMethodOverride(m_methods, SapiMonitorIsEnabled);
        mMethodOverride(m_methods, ExpectedSapiCanModify);
        mMethodOverride(m_methods, ExpectedSapiSet);
        mMethodOverride(m_methods, ExpectedSapiGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtLapsLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtLapsLink self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtLapsLink);
    }

AtLapsLink AtLapsLinkObjectInit(AtLapsLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Call super constructor */
    if (AtHdlcLinkObjectInit((AtHdlcLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/*
 * Check if Tx SAPI value can modify
 *
 * @param self This link
 *
 * @return cAtTrue if support modify Tx SAPI
 */
eBool AtLapsLinkTxSapiCanModify(AtLapsLink self)
    {
    mAttributeGet(TxSapiCanModify, eBool, cAtFalse);
    }

/*
 * Check if can enable/disable SAPI monitoring
 *
 * @param self This link
 *
 * @return cAtTrue mean can enable/disable SAPI monitoring
 */
eBool AtLapsLinkSapiMonitorCanEnable(AtLapsLink self, eBool enable)
    {
    mOneParamAttributeGet(SapiMonitorCanEnable, enable, eBool, cAtFalse);
    }

/*
 * Check if Expected SAPI value can modify
 *
 * @param self This link
 *
 * @return cAtTrue if support modify Expected SAPI
 */
eBool AtLapsLinkExpectedSapiCanModify(AtLapsLink self)
    {
    mAttributeGet(ExpectedSapiCanModify, eBool, cAtFalse);
    }

/**
 * @addtogroup AtLapsLink
 * @{
 */

/**
 * Set transmitted SAPI value
 *
 * @param self This link
 * @param value Transmitted SAPI value
 *
 * @return AT return code
 */
eAtRet AtLapsLinkTxSapiSet(AtLapsLink self, uint16 value)
    {
    mNumericalAttributeSet(TxSapiSet, value);
    }

/**
 * Get transmitted SAPI value
 *
 * @param self This link
 *
 * @return Transmitted SAPI value
 */
uint16 AtLapsLinkTxSapiGet(AtLapsLink self)
    {
    mAttributeGet(TxSapiGet, uint16, 0x0);
    }

/**
 * Enable/disable SAPI monitoring
 *
 * @param self This link
 * @param enable SAPI monitor enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtLapsLinkSapiMonitorEnable(AtLapsLink self, eBool enable)
    {
    mNumericalAttributeSet(SapiMonitorEnable, enable);
    }

/**
 * Check if SAPI monitoring is enabled.
 *
 * @param self This link
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse  if disabled
 */
eBool AtLapsLinkSapiMonitorIsEnabled(AtLapsLink self)
    {
    mAttributeGet(SapiMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Set Expected SAPI
 *
 * @param self This link
 * @param value Expected SAPI
 *
 * @return AT return code
 */
eAtRet AtLapsLinkExpectedSapiSet(AtLapsLink self, uint16 value)
    {
    mNumericalAttributeSet(ExpectedSapiSet, value);
    }

/**
 * Get Expected SAPI
 *
 * @param self This link
 *
 * @return Expected SAPI
 */
uint16 AtLapsLinkExpectedSapiGet(AtLapsLink self)
    {
    mAttributeGet(ExpectedSapiGet, uint16, 0x0);
    }
/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : AtEncapBundle.c
 *
 * Created Date: Aug 3, 2012
 *
 * Description : Abstract bundle
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEncapBundleInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEncapBundleMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "bundle";
    }

static uint32 TypeGet(AtEncapBundle self)
    {
    AtUnused(self);
    return cAtEncapBundleTypeUnknown;
    }

static void OverrideAtChannel(AtEncapBundle self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void MethodsInit(AtEncapBundle self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TypeGet);
        }
        
    mMethodsSet(self, &m_methods);
    }

static void Override(AtEncapBundle self)
    {
    OverrideAtChannel(self);
    }

AtEncapBundle AtEncapBundleObjectInit(AtEncapBundle self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtEncapBundle));

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtEncapBundle) self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtEncapBundle
 * @{
 */

/**
 * Get Bundle type
 *
 * @param self This channel
 *
 * @return @ref eAtEncapBundleType "Bundle type"
 */
uint32 AtEncapBundleTypeGet(AtEncapBundle self)
    {
    mAttributeGet(TypeGet, uint32, cAtEncapBundleTypeUnknown);
    }

/**
 * @}
 */

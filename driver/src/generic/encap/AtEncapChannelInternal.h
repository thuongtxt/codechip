/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : AtEncapChannelInternal.h
 * 
 * Created Date: Sep 1, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATENCAPCHANNELINTERNAL_H_
#define _ATENCAPCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"
#include "AtEncapChannel.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtEncapChannelMethods
    {
    /* Binding */
    eBool (*CanBindEthFlow)(AtEncapChannel self, AtEthFlow flow);
    eAtModuleEncapRet (*FlowBind)(AtEncapChannel self, AtEthFlow flow);
    AtEthFlow (*BoundFlowGet)(AtEncapChannel self);
    AtChannel (*BoundPhyGet)(AtEncapChannel self);
    eAtRet (*PhyBind)(AtEncapChannel self, AtChannel phyChannel);
    eBool (*CanBindPhyChannel)(AtEncapChannel self, AtChannel phyChannel);

    eAtEncapType (*EncapTypeGet)(AtEncapChannel self);
    eAtRet (*ScrambleEnable)(AtEncapChannel self, eBool enable);
    eBool (*ScrambleIsEnabled)(AtEncapChannel self);

    /* Drop packet conditions. */
    eBool (*CanChangePacketDroppingCondition)(AtEncapChannel self, uint32 conditionMask);
    eAtRet (*DropPacketConditionMaskSet)(AtEncapChannel self, uint32 conditionMask, uint32 enableMask);
    uint32 (*DropPacketConditionMaskGet)(AtEncapChannel self);
    }tAtEncapChannelMethods;

/* Encapsulation class */
typedef struct tAtEncapChannel
    {
    tAtChannel super;
    const tAtEncapChannelMethods *methods;

    /* Private data */
    AtChannel physicalChannel;
    eAtEncapType encapType; /* Encapsulation type of this channel */
    AtEthFlow ethFlow;
    }tAtEncapChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEncapChannel AtEncapChannelObjectInit(AtEncapChannel self,
                                        uint32 channelId,
                                        eAtEncapType encapType,
                                        AtModule module);

/* Access private data */
void AtEncapChannelPhysicalChannelSet(AtEncapChannel self, AtChannel physicalChannel);
eBool AtEncapChannelCanChangePacketDroppingCondition(AtEncapChannel self, uint32 conditionMask);

#ifdef __cplusplus
}
#endif
#endif /* _ATENCAPCHANNELINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtUdpPacket.c
 *
 * Created Date: Apr 27, 2013
 *
 * Description : IPv6 packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacketInternal.h"
#include "AtUdpPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cUnusedPort 0x85e

#define cPtpEventPort       319
#define cPtpGeneralPort     320

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HeaderLength(AtPacket self)
    {
	AtUnused(self);
    return 8;
    }

static uint16 SourcePort(AtPacket self)
    {
    return AtPktUtilWordGet(AtPacketDataBuffer(self, NULL));
    }

static uint16 DestPort(AtPacket self)
    {
    return AtPktUtilWordGet(&(AtPacketDataBuffer(self, NULL)[2]));
    }

static uint16 Length(AtPacket self)
    {
    return AtPktUtilWordGet(&(AtPacketDataBuffer(self, NULL)[4]));
    }

static uint16 CheckSum(AtPacket self)
    {
    return AtPktUtilWordGet(&(AtPacketDataBuffer(self, NULL)[6]));
    }

static eBool IsUnusedPort(uint16 port)
    {
    return (port == cUnusedPort) ? cAtTrue : cAtFalse;
    }

static void DisplayPort(AtPacket self, const char *portName, uint16 portId, uint8 level)
    {
    if (IsUnusedPort(portId))
        {
        AtPacketPrintSpaces(level);
        AtPrintc(cSevNormal, "* %s: unused(%d)\r\n", portName, portId);
        }
    else
        AtPacketPrintAttribute(self, portName, portId, level);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    DisplayPort(self, "Source Port", SourcePort(self), level);
    DisplayPort(self, "Dest Port",  DestPort(self),    level);

    AtPacketPrintAttribute   (self, "Length",   Length(self),   level);
    AtPacketPrintHexAttribute(self, "Checksum", CheckSum(self), level);
    }

static uint32 LookupPort(AtPacket self)
    {
    uint32 udpPort = SourcePort(self);
    if (udpPort != cUnusedPort)
        return udpPort;

    return DestPort(self);
    }

static eBool IsPtpAssignedPort(uint16 udpPort)
    {
    return ((udpPort == cPtpEventPort) || (udpPort == cPtpGeneralPort)) ? cAtTrue : cAtFalse;
    }

static AtPacket PayloadPacketCreateByPort(AtPacket self, uint16 udpPort, uint8 *data, uint32 length)
    {
    AtPacketFactory factory = AtPacketFactoryGet(self);

    if (IsPtpAssignedPort(udpPort))
        return AtPacketFactoryPtpPacketCreate(factory, data, length, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);

    return NULL;
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    AtPacket payload = NULL;
    uint32 length, payloadLength;
    uint8 *buffer = AtPacketDataBuffer(self, &length);
    uint32 payloadOffset;

    /* Get payload length */
    payloadLength = mMethodsGet(self)->PayloadLength(self);
    if (payloadLength == 0)
        return;

    /* For unknown payload, just let its super do */
    payloadOffset = (uint16)(mMethodsGet(self)->HeaderLength(self));
    payload = PayloadPacketCreateByPort(self, (uint16)LookupPort(self), &(buffer[payloadOffset]), payloadLength);
    AtPacketFactorySet(payload, AtPacketFactoryGet(self));
    if (payload == NULL)
        {
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
        }

    /* Display this payload */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDisplayAtLevel(payload, (uint8)(level + 1));
    AtObjectDelete((AtObject)payload);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, HeaderLength);
        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtUdpPacket);
    }

AtPacket AtUdpPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint32 AtUdpPacketUdpLookupPort(AtPacket self)
    {
    if (self)
        return LookupPort(self);
    return cInvalidUint32;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New UDP packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtUdpPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return AtUdpPacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }
/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtLcpPacket.c
 *
 * Created Date: Apr 27, 2013
 *
 * Description : IPv6 packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacketInternal.h"
#include "AtLcpPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtLcpPacket
    {
    tAtPacket super;

    /* Private */
    }tAtLcpPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HeaderLength(AtPacket self)
    {
    AtUnused(self);
    return 4;
    }

static const char *CodeString(AtPacket self, uint8 code)
    {
	AtUnused(self);
    if (code == 1)  return "Configure-Request";
    if (code == 2)  return "Configure-Ack";
    if (code == 3)  return "Configure-Nak";
    if (code == 4)  return "Configure-Reject";
    if (code == 5)  return "Terminate-Request";
    if (code == 6)  return "Terminate-Ack";
    if (code == 7)  return "Code-Reject";
    if (code == 8)  return "Protocol-Reject";
    if (code == 9)  return "Echo-Request";
    if (code == 10) return "Echo-Reply";
    if (code == 11) return "Discard-Request";

    return NULL;
    }

static uint8 Code(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[0];
    }

static uint8 Identifer(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[1];
    }

static uint16 Length(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);
    return (uint16)((buffer[2] << 8) | buffer[3]);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    const char *codeString;

    if (AtPacketLengthGet(self) < mMethodsGet(self)->HeaderLength(self))
        return;

    /* Code */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "Code: ");
    codeString = CodeString(self, Code(self));
    if (codeString)
        AtPrintc(cSevNormal, "%s\r\n", codeString);
    else
        AtPrintc(cSevNormal, "Unknown [0x%x]\r\n", Code(self));

    AtPacketPrintAttribute(self, "Identifier", Identifer(self), level);
    AtPacketPrintAttribute(self, "Length",     Length(self),    level);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, mMethodsGet(self), sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, HeaderLength);
        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
	{
	return sizeof(tAtLcpPacket);
	}

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New LCP packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtLcpPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode);
    }
/**
 * @}
 */

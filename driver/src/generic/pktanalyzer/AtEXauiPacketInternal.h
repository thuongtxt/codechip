/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : AtXauiPacketInternal.h
 * 
 * Created Date: May 10, 2016
 *
 * Description : XAUI Packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEXAUIPACKETINTERNAL_H_
#define _ATEXAUIPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../include/pktanalyzer/AtEXauiPacket.h"
#include "AtPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtEXauiPacket
    {
    tAtPacket super;

    /* Private data */
    uint32 portId;
    uint8 direction;
    }tAtEXauiPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtEXauiPacketObjectInit(AtPacket self,
                                uint8 *dataBuffer,
                                uint32 length,
                                eAtPacketCacheMode cacheMode,
                                eAtPacketFactoryDirection direction,
                                uint32 portId);

uint8 AtXauiPacketDirectionGet(AtEXauiPacket self);
uint32 AtXauiPacketPortIdGet(AtEXauiPacket self);

#ifdef __cplusplus
}
#endif
#endif /* _ATEXAUIPACKETINTERNAL_H_ */


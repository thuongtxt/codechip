/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PACKET ANALYZER
 *
 * File        : AtMefMplsPacket.c
 *
 * Created Date: Nov 7, 2014
 *
 * Description : MEF packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthPacket.h"
#include "AtEthPacketInternal.h"
#include "AtMefPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMacLength          6
#define cVlanLength         4
#define cEthTypeLength      2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtMefMplsPacket *)((void*)self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtMefMplsPacket
    {
    tAtMefPwPacket super;
    }tAtMefMplsPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;
static tAtEthPacketMethods m_AtEthPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint16 FirstVlanOffset(AtEthPacket self)
    {
    AtUnused(self);
    return (cMacLength * 2);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    AtEthPacket ethPacket = (AtEthPacket)self;

    if (AtPacketLengthGet(self) < mMethodsGet(self)->HeaderLength(self))
        return;

    AtEthPacketDisplayMac(ethPacket, "DMAC", &(AtPacketDataBuffer(self, NULL)[0]), level);
    AtEthPacketDisplayMac(ethPacket, "SMAC", &(AtPacketDataBuffer(self, NULL)[cMacLength]), level);
    AtEthPacketDisplayVlans(ethPacket, level);
    AtEthPacketDisplayEthTypeLength(ethPacket, level);
    AtPacketPrintHexAttribute(self, "ECID",  AtMefPacketEcid(self), level);
    }

static uint32 HeaderLength(AtPacket self)
    {
    return AtEthPacketNumVlans((AtEthPacket)self) * (uint32)cVlanLength + 18;
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket ethPacket = (AtEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, mMethodsGet(ethPacket), sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, FirstVlanOffset);
        }

    mMethodsSet(ethPacket, &m_AtEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    OverrideAtEthPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtMefMplsPacket);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtMefPwPacketObjectInit(self, dataBuffer, length, cacheMode, type, rtp) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket AtMefMplsPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode, type, rtp);
    }

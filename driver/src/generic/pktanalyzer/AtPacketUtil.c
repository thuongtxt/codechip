/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPacketUtil.c
 *
 * Created Date: May 2, 2013
 *
 * Description : Packet util
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPacket.h"
#include "AtPacketUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
void AtPacketPrintAttribute(AtPacket self, const char *name, uint32 value, uint8 level)
    {
	AtUnused(self);
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: %u\r\n", name, value);
    }

void AtPacketPrintHexAttribute(AtPacket self,const char *name, uint32 value, uint8 level)
    {
	AtUnused(self);
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: 0x%x\r\n", name, value);
    }

void AtPacketPrintStringAttribute(AtPacket self,const char *name, const char *value, uint8 level)
    {
	AtUnused(self);
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: %s\r\n", name, value);
    }

static void DisplayBufferInDecimal(AtPacket self, uint8 *buffer, uint32 length, const char *separate)
    {
    uint32 byte_i;

    AtUnused(self);

    if (length == 0)
        return;

    for (byte_i = 0; byte_i < length; byte_i++)
        {
        if (byte_i == 0)
            AtPrintc(cSevNormal, "%02u", buffer[byte_i]);
        else
            AtPrintc(cSevNormal, "%s%02u", separate, buffer[byte_i]);
        }
    }

void AtPacketPrintIp(AtPacket self, const char *name, uint8 *ip, uint8 length, uint8 level)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: ", name);
    DisplayBufferInDecimal(self, ip, length, ".");
    AtPrintc(cSevNormal, "\r\n");
    }

void AtPacketPrintSpaces(uint8 level)
    {
    uint8 i;

    for (i = 0; i < (level * 4); i++)
        AtPrintc(cSevNormal, " ");
    }

void AtPacketDisplayBufferInHex(AtPacket self, uint8 *buffer, uint32 length, const char *separate)
    {
    uint32 byte_i;
	AtUnused(self);

    if (length == 0)
        return;

    for (byte_i = 0; byte_i < length; byte_i++)
        {
        if (byte_i == 0)
            AtPrintc(cSevNormal, "%02X", buffer[byte_i]);
        else
            AtPrintc(cSevNormal, "%s%02X", separate, buffer[byte_i]);
        }
    }

void AtPacketDumpBuffer(AtPacket self, uint8 *buffer, uint32 length, uint8 level)
    {
    uint32 byte_i;
	AtUnused(self);

    AtPacketPrintSpaces(level);
    for (byte_i = 0; byte_i < length; byte_i++)
        {
        AtPrintc(cSevNormal,"%02X ", buffer[byte_i]);

        /* Enough 16-bytes, next row */
        if ((byte_i % 16) == 15)
            {
            AtPrintc(cSevNormal, "\r\n");
            AtPacketPrintSpaces(level);
            }

        /* 4-bytes */
        else if (byte_i % 4 == 3)
            AtPrintc(cSevNormal, " ");
        }

    AtPrintc(cSevNormal, "\r\n");
    }

uint16 AtPktUtilWordGet(uint8 *buffer)
    {
    uint32 wordValue;

    wordValue  = buffer[0]; wordValue <<= 8;
    wordValue |= buffer[1];

    return (uint16)wordValue;
    }

uint32 AtPktUtilDwordGet(uint8 *buffer)
    {
    uint32 dwordValue;

    dwordValue  = buffer[0]; dwordValue <<= 8;
    dwordValue |= buffer[1]; dwordValue <<= 8;
    dwordValue |= buffer[2]; dwordValue <<= 8;
    dwordValue |= buffer[3];

    return dwordValue;
    }

uint8 *AtPktUtilEthPreambleDetect(uint8 *dataBuffer, uint32 bufferSize, uint8 *length)
    {
    static const uint8 cDelimiter = 0xD5;
    static const uint8 cPreambleLength = 8;
    uint8 *preamble = dataBuffer;
    uint8 i, preambleLen = 0;
    eBool hasDelimiter = cAtFalse;

    /* Until delimiter is found */
    for (i = 0; i < cPreambleLength; i++)
        {
        if (i >= bufferSize)
            break;

        if (preamble[i] == cDelimiter)
            hasDelimiter = cAtTrue;
        preambleLen = (uint8)(preambleLen + 1);

        if (hasDelimiter)
            break;
        }

    if (length)
        *length = (uint8)(hasDelimiter ? preambleLen : 0);

    return preamble;
    }

uint16 AtPktUtilEthFirstVlanOffset(uint8 *dataBuffer, uint32 bufferSize)
    {
    static const uint32 cMacLength = 6;
    uint16 offset;
    uint8 length;

    AtPktUtilEthPreambleDetect(dataBuffer, bufferSize, &length);
    offset = (uint16)(length + (uint32)(cMacLength * 2));
    if (offset > bufferSize)
        return cInvalidUint16;

    return offset;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PACKET ANALYZER
 *
 * File        : AtMefPacket.c
 *
 * Created Date: Sep 16, 2013
 *
 * Description : MEF packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMefPacket.h"
#include "AtPacketInternal.h"
#include "AtMefPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMefLabelLength 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtMefPacket);
    }

static uint32 Ecid(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);
    uint8 bufferIndex = (uint8)(mMethodsGet(self)->HeaderLength(self) - cMefLabelLength);

    return ((uint32)(buffer[bufferIndex] << 12) | (uint32)(buffer[bufferIndex + 1] << 4) | (uint32)(buffer[bufferIndex + 2] >> 4)) ;
    }

static uint32 HeaderLength(AtPacket self)
    {
	AtUnused(self);
    return 4;
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    if (AtPacketLengthGet(self) < mMethodsGet(self)->HeaderLength(self))
        return;

    AtPacketPrintHexAttribute(self, "ECID",  Ecid(self)  , level);
    }

static uint32 PayloadLength(AtPacket self)
    {
    if (AtPacketLengthGet(self) > mMethodsGet(self)->HeaderLength(self))
        return AtPacketLengthGet(self) - mMethodsGet(self)->HeaderLength(self);
    return 0;
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    uint32 length;
    uint8 *buffer = AtPacketDataBuffer(self, &length);
    uint32 payloadOffset = mMethodsGet(self)->HeaderLength(self);
    uint32 payloadLength;

    payloadLength = mMethodsGet(self)->PayloadLength(self);
    if (payloadLength == 0)
        return;

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDumpBuffer(self, &(buffer[payloadOffset]), payloadLength, (uint8)(level + 1));
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        mMethodOverride(m_AtPacketOverride, PayloadLength);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

AtPacket AtMefPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket AtMefPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return AtMefPacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

uint32 AtMefPacketEcid(AtPacket self)
    {
    uint32 hdrLen = mMethodsGet(self)->HeaderLength(self);

    if (AtPacketLengthGet(self) < hdrLen)
        return 0xFFFFFFFF;

    return Ecid(self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtMplsPacketInternal.h
 * 
 * Created Date: Mar 28, 2014
 *
 * Description : MPLS packet class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMPLSPACKETINTERNAL_H_
#define _ATMPLSPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMefPacket.h"
#include "AtPacketInternal.h"
#include "AtEthPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtMefPacket
    {
    tAtEthPacket super;
    }tAtMefPacket;

typedef struct tAtMefPwPacket
    {
    tAtMefPacket super;

    /* Private data */
    eAtPwPacketType type;
    eAtPwPacketRtpMode rtp;
    }tAtMefPwPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtMefPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket AtMefPwPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp);

#ifdef __cplusplus
}
#endif
#endif /* _ATMPLSPACKETINTERNAL_H_ */


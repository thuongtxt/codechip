/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPktAnalyzer.c
 *
 * Created Date: Dec 18, 2012
 *
 * Description : Packet analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPacket.h"
#include "../../generic/man/AtDeviceInternal.h"
#include "AtPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPktAnalyzerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Init(AtPktAnalyzer self)
    {
    AtPktAnalyzerNumPacketsSet(self, 16);
    AtPktAnalyzerPacketLengthSet(self, 0);
    AtPktAnalyzerPatternCompareModeSet(self, cAtPktAnalyzerPatternCompareModeAny);
    AtPktAnalyzerPacketDumpTypeSet(self, cAtPktAnalyzerDumpAllPkt);
    AtPktAnalyzerPppPacketTypeSet(self, cAtPktAnalyzerPppPktTypeMlpppProtocol);
    }

static void Delete(AtObject self)
    {
    AtPktAnalyzer analyzer = (AtPktAnalyzer)self;
    AtPktAnalyzerPacketFlush(analyzer);
    AtObjectDelete((AtObject)(analyzer->packets));
    analyzer->packets = NULL;
    AtObjectDelete((AtObject)(analyzer->factory));
    analyzer->factory = NULL;

    m_AtObjectMethods->Delete(self);
    }

static eBool PktAnalyzerIsValid(AtPktAnalyzer self)
    {
    return self ? cAtTrue : cAtFalse;
    }

static void Recapture(AtPktAnalyzer self)
    {
	AtUnused(self);
	/* Just let concrete class does */
    }

static void PatternSet(AtPktAnalyzer self, uint8 *pattern)
    {
	AtUnused(pattern);
	AtUnused(self);
	/* Just let concrete class does */
    }

static void PatternMaskSet(AtPktAnalyzer self, uint32 patternMask)
    {
	AtUnused(patternMask);
	AtUnused(self);
	/* Just let concrete class does */
    }

static void PacketNumSet(AtPktAnalyzer self, uint32 packetNums)
    {
    self->displayedPacketNum = packetNums;
    }

static void PacketLengthSet(AtPktAnalyzer self, uint32 packetLength)
    {
	AtUnused(packetLength);
	AtUnused(self);
	/* Just let concrete class does */
    }

static void PppPacketModeSet(AtPktAnalyzer self, eAtPktAnalyzerPppPktType packetMode)
    {
	AtUnused(packetMode);
	AtUnused(self);
	/* Just let concrete class does */
    }

static void PatternCompareModeSet(AtPktAnalyzer self, eAtPktAnalyzerPatternCompareMode compareMode)
    {
	AtUnused(compareMode);
	AtUnused(self);
	/* Just let concrete class does */
    }

static void PacketDumpTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPktDumpType dumpType)
    {
	AtUnused(dumpType);
	AtUnused(self);
    /* Just let concrete class does */
    }

static void AnalyzeTxPackets(AtPktAnalyzer self)
    {
	AtUnused(self);
	/* Just let concrete class does */
    }

static void AnalyzeRxPackets(AtPktAnalyzer self)
    {
	AtUnused(self);
	/* Just let concrete class does */
    }

static AtList CaptureTxPackets(AtPktAnalyzer self)
    {
	AtUnused(self);
    /* Just let concrete class does */
    return NULL;
    }

static AtList CaptureRxPackets(AtPktAnalyzer self)
    {
	AtUnused(self);
    /* Just let concrete class does */
    return NULL;
    }

static eAtRet PacketTypeSet(AtPktAnalyzer self, uint8 pktType)
    {
    AtUnused(self);
    AtUnused(pktType);
    /* Just let concrete class does */
    return cAtErrorModeNotSupport;
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    return AtPacketFactoryRawPacketCreate(AtPktAnalyzerPacketFactory(self), data, length, cAtPacketCacheModeCacheData, direction);
    }

static AtPacketFactory PacketFactoryCreate(AtPktAnalyzer self)
    {
	AtUnused(self);
    return AtPacketFactoryNew();
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPktAnalyzer object = (AtPktAnalyzer)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(channel);
    mEncodeUInt(displayedPacketNum);
    mEncodeObjectDescription(packets);
    mEncodeObjectDescription(factory);
    mEncodeNone(buffer);
    }

static eAtRet Start(AtPktAnalyzer self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet Stop(AtPktAnalyzer self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool IsStarted(AtPktAnalyzer self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet ChannelSet(AtPktAnalyzer self, AtChannel channel)
    {
    self->channel = channel;
    return cAtOk;
    }

static void OverrideAtObject(AtPktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Recapture);
        mMethodOverride(m_methods, PatternSet);
        mMethodOverride(m_methods, PatternMaskSet);
        mMethodOverride(m_methods, PacketNumSet);
        mMethodOverride(m_methods, PacketLengthSet);
        mMethodOverride(m_methods, PppPacketModeSet);
        mMethodOverride(m_methods, PatternCompareModeSet);
        mMethodOverride(m_methods, PacketDumpTypeSet);
        mMethodOverride(m_methods, AnalyzeTxPackets);
        mMethodOverride(m_methods, AnalyzeRxPackets);
        mMethodOverride(m_methods, CaptureTxPackets);
        mMethodOverride(m_methods, CaptureRxPackets);
        mMethodOverride(m_methods, PacketCreate);
        mMethodOverride(m_methods, PacketFactoryCreate);
        mMethodOverride(m_methods, Start);
        mMethodOverride(m_methods, Stop);
        mMethodOverride(m_methods, IsStarted);
        mMethodOverride(m_methods, PacketTypeSet);
        mMethodOverride(m_methods, ChannelSet);
        }

    mMethodsSet(self, &m_methods);
    }

/* Constructor */
AtPktAnalyzer AtPktAnalyzerObjectInit(AtPktAnalyzer self, AtChannel channel)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPktAnalyzer));

    /* Call super constructor to reuse all of its implementation */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->channel = channel;

    Init(self);

    return self;
    }

uint8 *AtPktAnalyzerBufferGet(AtPktAnalyzer self, uint32 *bufferSize)
    {
    if (bufferSize)
        *bufferSize = cAtPktAnalyzerBufferLength;

    return self->buffer;
    }

AtList AtPktAnalyzerPacketListGet(AtPktAnalyzer self)
    {
    if (self->packets == NULL)
        self->packets = AtListCreate(0);
    return self->packets;
    }

void AtPktAnalyzerRecapture(AtPktAnalyzer self)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->Recapture(self);
    }

void AtPktAnalyzerPatternSet(AtPktAnalyzer self, uint8 *pattern)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->PatternSet(self, pattern);
    }

void AtPktAnalyzerPatternMaskSet(AtPktAnalyzer self, uint32 patternMask)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->PatternMaskSet(self, patternMask);
    }

void AtPktAnalyzerNumPacketsSet(AtPktAnalyzer self, uint32 packetNums)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->PacketNumSet(self, packetNums);
    }

void AtPktAnalyzerPacketLengthSet(AtPktAnalyzer self, uint32 packetLength)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->PacketLengthSet(self, packetLength);
    }

void AtPktAnalyzerPppPacketTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPppPktType packetMode)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->PppPacketModeSet(self, packetMode);
    }

void AtPktAnalyzerPatternCompareModeSet(AtPktAnalyzer self, eAtPktAnalyzerPatternCompareMode compareMode)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->PatternCompareModeSet(self, compareMode);
    }

void AtPktAnalyzerPacketFlush(AtPktAnalyzer self)
    {
    while (AtListLengthGet(self->packets) > 0)
        AtObjectDelete(AtListObjectRemoveAtIndex(self->packets, 0));
    }

void AtPktAnalyzerPacketDumpTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPktDumpType dumpType)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->PacketDumpTypeSet(self, dumpType);
    }

void AtPktAnalyzerInit(AtPktAnalyzer self)
    {
    if (PktAnalyzerIsValid(self))
        mMethodsGet(self)->Init(self);
    }

eAtRet AtPktAnalyzerPacketTypeSet(AtPktAnalyzer self, uint8 pktType)
    {
    if (!PktAnalyzerIsValid(self))
        return cAtErrorNullPointer;

    return mMethodsGet(self)->PacketTypeSet(self, pktType);
    }

/**
 * @addtogroup AtPktAnalyzer
 * @{
 */

/**
 * Analyze TX packets. All of packets are displayed in meaning format directly
 * on console
 *
 * @param self This analyzer
 */
void AtPktAnalyzerAnalyzeTxPackets(AtPktAnalyzer self)
    {
    if (!PktAnalyzerIsValid(self))
        return;

    mMethodsGet(self)->AnalyzeTxPackets(self);
    mMethodsGet(self)->Recapture(self);
    }

/**
 * Get the channel that this analyzer is analyzing on
 *
 * @param self This analyzer
 *
 * @return Channel that this analyzer is analyzing on
 */
AtChannel AtPktAnalyzerChannelGet(AtPktAnalyzer self)
    {
    if (!PktAnalyzerIsValid(self))
        return NULL;

    return self->channel;
    }

/**
 * Specify channel to analyze packets
 *
 * @param self This analyzer
 * @param channel Channel to analyze packets
 *
 * @return AT return code
 */
eAtRet AtPktAnalyzerChannelSet(AtPktAnalyzer self, AtChannel channel)
    {
    if (PktAnalyzerIsValid(self))
        return mMethodsGet(self)->ChannelSet(self, channel);
    return cAtErrorObjectNotExist;
    }

/**
 * Analyze RX packets. All of packets are displayed in meaning format directly
 * on console
 *
 * @param self This analyzer
 */
void AtPktAnalyzerAnalyzeRxPackets(AtPktAnalyzer self)
    {
    if (!PktAnalyzerIsValid(self))
        return;

    mMethodsGet(self)->AnalyzeRxPackets(self);
    mMethodsGet(self)->Recapture(self);
    }

/**
 * Capture TX packets. All of packets list are dumped directly
 *
 * @param self This analyzer
 * @return List of captured packet
 */
AtList AtPktAnalyzerCaptureTxPackets(AtPktAnalyzer self)
    {
    if (!PktAnalyzerIsValid(self))
        return 0;

    return mMethodsGet(self)->CaptureTxPackets(self);
    }

/**
 * Capture RX packets. All of packets list are dumped directly
 *
 * @param self This analyzer
 */
AtList AtPktAnalyzerCaptureRxPackets(AtPktAnalyzer self)
    {
    if (!PktAnalyzerIsValid(self))
        return 0;

    return mMethodsGet(self)->CaptureRxPackets(self);
    }

/**
 * Get packet factory
 *
 * @param self This analyzer
 * @return Packet factory
 */
AtPacketFactory AtPktAnalyzerPacketFactory(AtPktAnalyzer self)
    {
    if (self == NULL)
        return NULL;

    if (self->factory == NULL)
        self->factory = mMethodsGet(self)->PacketFactoryCreate(self);
    return self->factory;
    }

/**
 * Display all of captured packets
 *
 * @param packets List of packets
 * @param displayMode @ref eAtPktAnalyzerDisplayMode "Display mode"
 */
void AtPktAnalyzerAllPacketsDisplay(AtList packets, eAtPktAnalyzerDisplayMode displayMode)
    {
    uint32 packetId = 1;
    AtIterator packetIterator;
    AtPacket packet;

    if (AtListLengthGet(packets) == 0)
        {
        AtPrintc(cSevInfo, "No packets to display\r\n");
        return;
        }

    packetIterator = AtListIteratorCreate(packets);
    while ((packet = (AtPacket)AtIteratorNext(packetIterator)) != NULL)
        {
        eAtSevLevel color = AtPacketIsErrored(packet) ? cSevCritical : cSevInfo;

        AtPrintc(color, "==============================================\r\n");
        AtPrintc(color, "= Packet %u, length %u: ", packetId, AtPacketLengthGet(packet));
        if (AtPacketIsCompleted(packet))
            AtPrintc(color, "\r\n");
        else
            AtPrintc(color, "(in-completed packet)\r\n");
        AtPrintc(color, "==============================================\r\n");

        if (AtPacketIsErrored(packet))
            AtPrintc(color, "This is error packet\r\n");

        if (displayMode == cAtPktAnalyzerDisplayModeHumanReadable)
            AtPacketDisplay(packet);

        AtPrintc(cSevNormal, "*** Raw:\r\n");
        AtPacketDisplayRaw(packet, 0);
        packetId = packetId + 1;
        }

    AtObjectDelete((AtObject)packetIterator);
    }

/**
 * Start analyzing.
 *
 * @param self This analyzer
 *
 * @return AT return code
 */
eAtRet AtPktAnalyzerStart(AtPktAnalyzer self)
    {
    if (self)
        return mMethodsGet(self)->Start(self);
    return cAtErrorObjectNotExist;
    }

/**
 * Stop analyzing
 *
 * @param self This analyzer
 *
 * @return AT return code
 */
eAtRet AtPktAnalyzerStop(AtPktAnalyzer self)
    {
    if (self)
        return mMethodsGet(self)->Stop(self);
    return cAtErrorObjectNotExist;
    }

/**
 * Check if analyzer is started or not
 *
 * @param self This analyzer
 *
 * @retval cAtTrue if started
 * @retval cAtFalse if stopped
 */
eBool AtPktAnalyzerIsStarted(AtPktAnalyzer self)
    {
    if (self)
        return mMethodsGet(self)->IsStarted(self);
    return cAtFalse;
    }

/**
 * @}
 */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtIpV6Packet.c
 *
 * Created Date: Apr 27, 2013
 *
 * Description : IPv6 packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacketInternal.h"
#include "AtIpV6Packet.h"
#include "AtIpV4PacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cIpLength 16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtIpV6Packet
    {
    tAtIpV4Packet super;

    /* Private */
    }tAtIpV6Packet;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods     m_AtPacketOverride;
static tAtIpV4PacketMethods m_AtIpV4PacketOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HeaderLength(AtPacket self)
    {
	AtUnused(self);
    return 40;
    }

static uint8 Version(AtPacket self)
    {
    return (AtPacketDataBuffer(self, NULL)[0] & cBit7_4) >> 4;
    }

static uint8 TraffiClass(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);

    return (uint8)((((buffer[0] & cBit3_0) >> 0) << 4) | ((buffer[1] & cBit7_4) >> 4));
    }

static uint32 FlowLabel(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);

    return (uint32)((buffer[1] & cBit3_0) << 16) | (uint32)(buffer[2] << 8) | buffer[3];
    }

static uint16 PayloadLength(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);
    return (uint16)((buffer[4] << 8) | buffer[5]);
    }

static uint8 NextHeader(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[6];
    }

static uint8 HopLimit(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[7];
    }

static uint8 *SourceAddress(AtPacket self)
    {
    return &(AtPacketDataBuffer(self, NULL)[8]);
    }

static uint8 *DestAddress(AtPacket self)
    {
    return &(AtPacketDataBuffer(self, NULL)[8 + cIpLength]);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    if (AtPacketLengthGet(self) < mMethodsGet(self)->HeaderLength(self))
        return;

    /* Scalar attributes */
    AtPacketPrintAttribute   (self, "Version",        Version(self)      , level);
    AtPacketPrintHexAttribute(self, "Traffic Class",  TraffiClass(self)  , level);
    AtPacketPrintHexAttribute(self, "Flow Label",     FlowLabel(self)    , level);
    AtPacketPrintAttribute   (self, "Payload Length", PayloadLength(self), level);
    AtIpPacketDisplayProtocol(self, level, "Next header");
    AtPacketPrintAttribute   (self, "Hop Limit",      HopLimit(self)     , level);

    /* IPs */
    AtPacketPrintIp(self, "Source address",      SourceAddress(self), cIpLength, level);
    AtPacketPrintIp(self, "Destination address", DestAddress(self)  , cIpLength, level);
    }

static uint8 Protocol(AtIpV4Packet self)
    {
    return NextHeader((AtPacket)self);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, mMethodsGet(self), sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, HeaderLength);
        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void OverrideAtIpV4Packet(AtPacket self)
    {
	AtIpV4Packet packet = (AtIpV4Packet)self;
	
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIpV4PacketOverride, mMethodsGet(packet), sizeof(m_AtIpV4PacketOverride));

        mMethodOverride(m_AtIpV4PacketOverride, Protocol);
        }

    mMethodsSet(packet, &m_AtIpV4PacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    OverrideAtIpV4Packet(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtIpV6Packet);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtIpV4PacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New IPv6 packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtIpV6PacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode);
    }
/**
 * @}
 */

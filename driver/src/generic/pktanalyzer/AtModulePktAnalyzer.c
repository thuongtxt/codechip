/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtModulePktAnalyzer.c
 *
 * Created Date: Apr 26, 2013
 *
 * Description : Packet analyzer module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModulePktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModuleIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/
typedef AtPktAnalyzer (*PktAnalyzerCreateFunction)(AtModulePktAnalyzer self, AtChannel channel);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModulePktAnalyzerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Super's implementation */
static const tAtObjectMethods *m_AtObjectMethods;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)((AtModulePktAnalyzer)self)->packetAnalyzer);
    ((AtModulePktAnalyzer)self)->packetAnalyzer = NULL;
    m_AtObjectMethods->Delete((AtObject)self);
    }

static eAtRet Init(AtModule self)
    {
    AtModulePktAnalyzer module = (AtModulePktAnalyzer)self;
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Destroy current analyzer */
    AtObjectDelete((AtObject)(module->packetAnalyzer));
    module->packetAnalyzer = NULL;

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModulePktAnalyzer);
    }

static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
	AtUnused(port);
	AtUnused(self);
    return NULL;
    }

static AtPktAnalyzer EthFlowPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    return NULL;
    }

static AtPktAnalyzer PppLinkPktAnalyzerCreate(AtModulePktAnalyzer self, AtPppLink link)
    {
	AtUnused(link);
	AtUnused(self);
    return NULL;
    }

static AtPktAnalyzer HdlcChannelPktAnalyzerCreate(AtModulePktAnalyzer self, AtHdlcChannel channel)
    {
	AtUnused(channel);
	AtUnused(self);
    return NULL;
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "pktanalyzer";
    }

static const char *CapacityDescription(AtModule self)
    {
	AtUnused(self);
    return "analyzers: 1";
    }

static AtPktAnalyzer DefaultAnalyzerCreate(AtModulePktAnalyzer self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthPort ethPort = AtModuleEthPortGet(ethModule, 0);
    return mMethodsGet(self)->EthPortPktAnalyzerCreate(self, ethPort);
    }

static AtPktAnalyzer PktAnalyzerCreateByFunction(AtModulePktAnalyzer self, AtChannel channel, PktAnalyzerCreateFunction createFunc)
    {
    AtPktAnalyzer packetAnalyzer;

    if (!mModuleIsValid(self))
        return NULL;

    /* Delete packet analyzer first */
    if (self->packetAnalyzer != NULL)
        AtObjectDelete((AtObject)(self->packetAnalyzer));

    packetAnalyzer = createFunc(self, channel);
    self->packetAnalyzer = packetAnalyzer;
    AtPktAnalyzerInit(packetAnalyzer);

    return packetAnalyzer;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModulePktAnalyzer object = (AtModulePktAnalyzer)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(packetAnalyzer);
    }

static void OverrideAtModule(AtModulePktAnalyzer self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void MethodsInit(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, EthPortPktAnalyzerCreate);
        mMethodOverride(m_methods, EthFlowPktAnalyzerCreate);
        mMethodOverride(m_methods, PppLinkPktAnalyzerCreate);
        mMethodOverride(m_methods, HdlcChannelPktAnalyzerCreate);
        mMethodOverride(m_methods, DefaultAnalyzerCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModulePktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

AtModulePktAnalyzer AtModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModulePktAnalyzer, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPktAnalyzer
 * @{
 */

/**
 * Get current packet analyzer.
 *
 * @param self This module
 *
 * @return Current packet analyzer or NULL if no analyzer is supported
 */
AtPktAnalyzer AtModulePacketAnalyzerCurrentAnalyzerGet(AtModulePktAnalyzer self)
    {
    if (!mModuleIsValid(self))
        return NULL;

    if (self->packetAnalyzer)
        return self->packetAnalyzer;

    /* Try using default analyzer */
    self->packetAnalyzer = mMethodsGet(self)->DefaultAnalyzerCreate(self);
    AtPktAnalyzerInit(self->packetAnalyzer);

    return self->packetAnalyzer;
    }

/**
 * Create packet analyzer to analyze packets on Ethernet Port
 *
 * @param self This module
 * @param port Ethernet port needs to be analyzed
 *
 * @return Ethernet Port packet analyzer or NULL if error happens or it is not
 *         supported
 */
AtPktAnalyzer AtModulePacketAnalyzerEthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
    return PktAnalyzerCreateByFunction(self, (AtChannel)port, (PktAnalyzerCreateFunction)mMethodsGet(self)->EthPortPktAnalyzerCreate);
    }

/**
 * Create packet analyzer to analyze packets on Ethernet Flow.
 *
 * @param self This module
 * @param flow Ethernet Flow needs to be analyzed
 *
 * @return Ethernet Flow packet analyzer or NULL if error happens or it is not
 *         supported
 */
AtPktAnalyzer AtModulePacketAnalyzerEthFlowPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthFlow flow)
    {
    return PktAnalyzerCreateByFunction(self, (AtChannel)flow, (PktAnalyzerCreateFunction)mMethodsGet(self)->EthFlowPktAnalyzerCreate);
    }

/**
 * Create packet analyzer to analyze packets on PPP Link
 *
 * @param self This module
 * @param link PPP Link needs to be analyzed
 *
 * @return PPP Link packet analyzer or NULL if error happens or it is not
 *         supported
 */
AtPktAnalyzer AtModulePacketAnalyzerPppLinkPktAnalyzerCreate(AtModulePktAnalyzer self, AtPppLink link)
    {
    return PktAnalyzerCreateByFunction(self, (AtChannel)link, (PktAnalyzerCreateFunction)mMethodsGet(self)->PppLinkPktAnalyzerCreate);
    }

/**
 * Create packet analyzer to analyze packets on HDLC channel
 *
 * @param self This module
 * @param channel HDLC channel needs to be analyzed
 *
 * @return HDLC packet analyzer or NULL if error happens or it is not
 *         supported
 */
AtPktAnalyzer AtModulePacketAnalyzerHdlcChannelPktAnalyzerCreate(AtModulePktAnalyzer self, AtHdlcChannel channel)
    {
    return PktAnalyzerCreateByFunction(self, (AtChannel)channel, (PktAnalyzerCreateFunction)mMethodsGet(self)->HdlcChannelPktAnalyzerCreate);
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PACKET ANALYZER
 *
 * File        : AtMplsPacket.c
 *
 * Created Date: Jun 6, 2013
 *
 * Description : MPLS packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMplsPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
	{
	return sizeof(tAtMplsPacket);
	}

static uint32 Label(AtPacket self, uint8 labelIndex)
    {
	return ((uint32)(AtPacketDataBuffer(self, NULL)[(labelIndex * 4)] << 12)    |
			(uint32)(AtPacketDataBuffer(self, NULL)[(labelIndex * 4) + 1] << 4) |
			(uint32)(AtPacketDataBuffer(self, NULL)[(labelIndex * 4) + 2] >> 4));
    }

static uint8 Exp(AtPacket self, uint8 labelIndex)
    {
	return (uint8)((AtPacketDataBuffer(self, NULL)[labelIndex * 4 + 2] & cBit3_1) >> 1);
    }

static uint8 SBit(AtPacket self, uint8 labelIndex)
    {
	return (AtPacketDataBuffer(self, NULL)[labelIndex * 4 + 2] & cBit0);
    }

static uint8 Ttl(AtPacket self, uint8 labelIndex)
    {
	return (AtPacketDataBuffer(self, NULL)[labelIndex * 4 + 3]);
    }

static uint32 HeaderLength(AtPacket self)
    {
	uint8 labelIndex = 0;
	static const uint8 cMaxMplsLabelNum = 10;

	while (!SBit(self, labelIndex))
		{
		labelIndex = (uint8)(labelIndex + 1);
		if (labelIndex == cMaxMplsLabelNum)
			break;
		}

	return ((labelIndex + 1UL) * 4UL);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
	uint32 hdrLen = mMethodsGet(self)->HeaderLength(self);
	uint8 labelIndex;

	if (AtPacketLengthGet(self) < hdrLen)
		return;

	/* Scalar attributes */
	for (labelIndex = 0; labelIndex < (hdrLen / 4); labelIndex++)
		{
	    AtPacketPrintSpaces(level);
		if (SBit(self, labelIndex))
			AtPrintc(cSevNormal, "* Inner label   = 0x%x, EXP = %d, TTL = %d\n",
					 Label(self, labelIndex), Exp(self, labelIndex), Ttl(self, labelIndex));
		else
			AtPrintc(cSevNormal, "* Outer label %d = 0x%x, EXP = %d, TTL = %d\n",
					 labelIndex + 1, Label(self, labelIndex), Exp(self, labelIndex), Ttl(self, labelIndex));
		}
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

AtPacket AtMplsPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket AtMplsPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return AtMplsPacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

uint32 AtMplsPacketInnerLabel(AtPacket self)
    {
    uint32 hdrLen = mMethodsGet(self)->HeaderLength(self);
    uint8 labelIndex;

    if (AtPacketLengthGet(self) < hdrLen)
        return 0xFFFFFFFF;

    for (labelIndex = 0; labelIndex < (hdrLen / 4); labelIndex++)
        {
        if (SBit(self, labelIndex))
            return Label(self, labelIndex);
        }

    return 0xFFFFFFFF;
    }

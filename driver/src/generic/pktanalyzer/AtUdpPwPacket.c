/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtUdpPwPacket.c
 *
 * Created Date: Sep 14, 2016
 *
 * Description : UDP PW packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtUdpPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtUdpPwPacket)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtUdpPwPacket * AtUdpPwPacket;

typedef struct tAtUdpPwPacket
	{
	tAtUdpPacket super;

	/* Private data */
	eAtPwPacketType type;
	eAtPwPacketRtpMode rtp;
	}tAtUdpPwPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DisplayPayload(AtPacket self, uint8 level)
    {
	if (AtPacketPayloadBuffer(self, NULL))
		{
		uint32 payloadSize;
		uint8 *payload = AtPacketPayloadBuffer(self, &payloadSize);
		AtPacket pwPacket = AtPacketFactoryPwPacketCreate(AtPacketFactoryGet(self), payload, payloadSize, cAtPacketCacheModeNoCache, mThis(self)->type, mThis(self)->rtp, cAtPacketFactoryDirectionNone);
		AtPacketUdpPwPacketMark(pwPacket, cAtTrue);
		AtPacketDisplayAtLevel(pwPacket, level);
		AtObjectDelete((AtObject)pwPacket);
		}

	m_AtPacketMethods->DisplayPayload(self, level);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtUdpPwPacket);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtUdpPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    mThis(self)->type = type;
    mThis(self)->rtp  = rtp;

    return self;
    }

AtPacket AtUdpPwPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode, type, rtp);
    }

void AtUdpPwPacketTypeSet(AtPacket self, eAtPwPacketType type)
    {
    if (self)
        mThis(self)->type = type;
    }

void AtUdpPwPacketRtpModeSet(AtPacket self, eAtPwPacketRtpMode rtp)
    {
    if (self)
        mThis(self)->rtp = rtp;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPtpPacket.c
 *
 * Created Date: Jul 13, 2018
 *
 * Description : PTP packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPtpPacket.h"
#include "AtPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cCorrectionFieldLength        8
#define cClockPortIdentityLength     10


/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPtpPacket)self)

#define DisplayAttribute(level, format, value)                                 \
    AtPacketPrintSpaces(level);                                                \
    AtPrintc(cSevNormal, format, value);                                       \

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPtpPacket
    {
    tAtPacket super;
    }tAtPtpPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPtpPacket);
    }

static uint8 UpperNibble(AtPacket self, uint16 bufPtr)
    {
    uint8 value = AtPacketDataBuffer(self, NULL)[bufPtr];
    return (uint8)((value >> 4) & cBit3_0);
    }

static uint8 LowerNibble(AtPacket self, uint16 bufPtr)
    {
    uint8 value = AtPacketDataBuffer(self, NULL)[bufPtr];
    return (value & cBit3_0);
    }

static uint8 OctetValue(AtPacket self, uint16 *saveBufPtr)
    {
    uint8 value = AtPacketDataBuffer(self, NULL)[*saveBufPtr];
    *saveBufPtr = (uint16)(*saveBufPtr + 1);
    return value;
    }

static uint16 DoubleOctetValue(AtPacket self, uint16 *saveBufPtr)
    {
    uint8 *buff = &AtPacketDataBuffer(self, NULL)[*saveBufPtr];
    *saveBufPtr = (uint16)(*saveBufPtr + 2);
    return (uint16)((buff[0] << 8) | buff[1]);
    }

static uint32 QuartOctetValue(AtPacket self, uint16 *saveBufPtr)
    {
    uint8 *buff = &AtPacketDataBuffer(self, NULL)[*saveBufPtr];
    *saveBufPtr = (uint16)(*saveBufPtr + 4);
    return (uint32)((buff[0] << 24) | (buff[1] << 16) | (buff[2] << 8) | buff[3]);
    }

static uint64 HexaOctetValue(AtPacket self, uint16 *saveBufPtr)
    {
    uint8 *octetBuf = &AtPacketDataBuffer(self, NULL)[*saveBufPtr];
    uint32 ls32 = (uint32)((octetBuf[2] << 24) | (octetBuf[3] << 16) | (octetBuf[4] << 8)  | octetBuf[5]);
    uint32 ms16 = (uint32)((octetBuf[0] << 8)  | octetBuf[1]);
    uint64 result = ms16;
    result = (uint64)((result << 32) | ls32);

    *saveBufPtr = (uint16)(*saveBufPtr + 6);
    return result;
    }

static uint8 TransportSpecific(AtPtpPacket self)
    {
    return UpperNibble((AtPacket)self, 0);
    }

static uint8 MessageType(AtPtpPacket self)
    {
    return LowerNibble((AtPacket)self, 0);
    }

static const char *MessageTypeString(uint8 mesgType)
    {
    switch (mesgType)
        {
        case 0x0:     return "Sync";
        case 0x1:     return "Delay_Req";
        case 0x2:     return "Pdelay_Req";
        case 0x3:     return "Pdelay_Resp";
        case 0x8:     return "Follow_Up";
        case 0x9:     return "Delay_Resp";
        case 0xA:     return "Pdelay_Resp_Follow_Up";
        case 0xB:     return "Announce";
        case 0xC:     return "Signaling";
        case 0xD:     return "Management";
        default:      return "Reserved";
        }
    }

static void DisplayMessageType(AtPtpPacket self, uint8 level)
    {
    uint8 mesgType = MessageType(self);
    const char *typeString = MessageTypeString(mesgType);

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Message Type: 0x%x", mesgType);
    if (typeString)
        AtPrintc(cSevNormal, " [%s]", typeString);
    AtPrintc(cSevNormal, "\r\n");
    }

static uint8 ReservedNibble(AtPtpPacket self)
    {
    return UpperNibble((AtPacket)self, 1);
    }

static uint8 Version(AtPtpPacket self)
    {
    return LowerNibble((AtPacket)self, 1);
    }

static uint16 MessageLength(AtPtpPacket self)
    {
    uint16 msgLenPtr = 2;
    return DoubleOctetValue((AtPacket)self, &msgLenPtr);
    }

static void DisplayOneFlag(uint8 level, uint16 flags, uint16 flagMask, const char *flagName)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: %s\r\n", flagName, (flags & flagMask) ? "True (1)" : "False (0)");
    }

static void DisplayFlags(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    uint16 flags = DoubleOctetValue(self, saveBufPtr);
    DisplayAttribute(level, "* Flags: 0x%04x\r\n", flags);

    level++;
    /* Octet 0 */
    DisplayOneFlag(level, flags, cBit15, "PTP Security");
    DisplayOneFlag(level, flags, cBit14, "PTP profile Specific 2");
    DisplayOneFlag(level, flags, cBit13, "PTP profile Specific 1");
    DisplayOneFlag(level, flags, cBit10, "PTP Unicast");
    DisplayOneFlag(level, flags, cBit9,  "PTP Two-step");
    DisplayOneFlag(level, flags, cBit8,  "PTP Alternate Master");
    /* Octet 1 */
    DisplayOneFlag(level, flags, cBit5,  "Frequency Traceable");
    DisplayOneFlag(level, flags, cBit4,  "Time Traceable");
    DisplayOneFlag(level, flags, cBit3,  "PTP Timescale");
    DisplayOneFlag(level, flags, cBit2,  "PTP UTC Offset Valid");
    DisplayOneFlag(level, flags, cBit1,  "PTP Leap 59");
    DisplayOneFlag(level, flags, cBit0,  "PTP Leap 61");
    }

static void DisplayCorrectionField(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    uint64 nanoseconds = HexaOctetValue(self, saveBufPtr);
    uint16 subNanoseconds = DoubleOctetValue(self, saveBufPtr);

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* CorrectionField: %llu.%06u nanoseconds\r\n", nanoseconds, subNanoseconds);
    }

static void DisplayClockIdentity(AtPacket self, uint8 level, const char *clockIdentity, uint16 *saveBufPtr)
    {
    uint8 *clockIdBuf = &AtPacketDataBuffer(self, NULL)[*saveBufPtr];
    uint8 octet_i;

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: 0x", clockIdentity);
    for (octet_i = 0; octet_i < 8; octet_i++)
        AtPrintc(cSevNormal, "%02x", clockIdBuf[octet_i]);
    AtPrintc(cSevNormal, "\r\n");

    *saveBufPtr = (uint16)(*saveBufPtr + 8);
    }

static const char *ControlString(uint8 controlField)
    {
    switch (controlField)
        {
        case 0x0:     return "Sync";
        case 0x1:     return "Delay_Req";
        case 0x2:     return "Follow_Up";
        case 0x3:     return "Delay_Resp";
        case 0x4:     return "Management";
        default:      return NULL;
        }
    }

static void DisplayControlField(AtPtpPacket self, uint8 level, uint16 *saveBufPtr)
    {
    uint8 controlField = OctetValue((AtPacket)self, saveBufPtr);
    const char *controlString = ControlString(controlField);

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* ControlField: 0x%02x ", controlField);
    if (controlString)
        AtPrintc(cSevNormal, "[%s]", controlString);
    AtPrintc(cSevNormal, "\r\n");
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    AtPtpPacket packet = mThis(self);
    uint16 buffPtr = 4;

    DisplayAttribute(level, "* Transport Specific: 0x%x\r\n", TransportSpecific(packet));
    DisplayMessageType(packet, level);
    DisplayAttribute(level, "* Reserved: 0x%x\r\n", ReservedNibble(packet));
    DisplayAttribute(level, "* Version: %d\r\n", Version(packet));
    DisplayAttribute(level, "* Message Length: %d\r\n", MessageLength(packet));
    DisplayAttribute(level, "* Domain Number: %d\r\n", OctetValue(self, &buffPtr));
    DisplayAttribute(level, "* Reserved: 0x%02x\r\n", OctetValue(self, &buffPtr));
    DisplayFlags(self, level, &buffPtr);
    DisplayCorrectionField(self, level, &buffPtr);
    DisplayAttribute(level, "* Reserved: 0x%08x\r\n", QuartOctetValue(self, &buffPtr));
    DisplayClockIdentity(self, level, "ClockIdentity", &buffPtr);
    DisplayAttribute(level, "* SourcePortID: %u\r\n", DoubleOctetValue(self, &buffPtr));
    DisplayAttribute(level, "* Sequence ID: %u\r\n", DoubleOctetValue(self, &buffPtr));
    DisplayControlField(packet, level, &buffPtr);
    DisplayAttribute(level, "* Log Message Interval: %u\r\n", OctetValue(self, &buffPtr));
    }

static uint32 HeaderLength(AtPacket self)
    {
    AtUnused(self);
    return 34;
    }

static void DisplayTimestamp(AtPacket self, uint8 level, const char *timestamp, uint16 *saveBufPtr)
    {
    uint64 seconds = HexaOctetValue(self, saveBufPtr);
    uint32 nanoseconds = QuartOctetValue(self, saveBufPtr);

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s in seconds: %llu \r\n", timestamp, seconds);
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s in nanoseconds: %u \r\n", timestamp, nanoseconds);
    }

static void DisplaySyncPayload(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "originTimestamp", saveBufPtr);
    }

static void DisplayDelayReqPayload(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "originTimestamp", saveBufPtr);
    }

static void DisplayPdelayReqPayload(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "originTimestamp", saveBufPtr);
    DisplayTimestamp(self, level, "Reserved", saveBufPtr); /* TODO: how to represent reserved field here. */
    }

static void DisplayRequestPortIdentity(AtPacket self, uint8 level, const char *prefixPortIdentity, uint16 *saveBufPtr)
    {
    DisplayClockIdentity(self, level, "requestingSourcePortIdentity", saveBufPtr);
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %sSourcePortID: %u\r\n", prefixPortIdentity, DoubleOctetValue(self, saveBufPtr));
    }

static void DisplayPdelayRespPayload(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "requestReceiptTimestamp", saveBufPtr);
    DisplayRequestPortIdentity(self, level, "requesting", saveBufPtr);
    }

static void DisplayNothing(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    AtUnused(self);
    AtUnused(level);
    AtUnused(saveBufPtr);
    }

static void DisplayFollowUpPayload(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "preciseOriginTimestamp", saveBufPtr);
    }

static void DisplayDelayRespPayload(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "receivedTimestamp", saveBufPtr);
    DisplayRequestPortIdentity(self, level, "requesting", saveBufPtr);
    }

static void DisplayPdelayRespFollowUpPayload(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "responseOriginTimestamp", saveBufPtr);
    DisplayRequestPortIdentity(self, level, "requesting", saveBufPtr);
    }

static void DisplayClockQuality(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Grandmaster Clock Quality: \r\n");

    level++;
    DisplayAttribute(level, "* ClockClass: %u \r\n", OctetValue(self, saveBufPtr));
    DisplayAttribute(level, "* ClockAccuracy: %u \r\n", OctetValue(self, saveBufPtr));
    DisplayAttribute(level, "* OffsetScaledLogVariance: %u \r\n", DoubleOctetValue(self, saveBufPtr));
    }

static void DisplayAnnounce(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "originTimestamp", saveBufPtr);
    DisplayAttribute(level, "* Current UTC Offset: %u \r\n", DoubleOctetValue(self, saveBufPtr));
    DisplayAttribute(level, "* Reserved: 0x%02x \r\n", OctetValue(self, saveBufPtr));
    DisplayAttribute(level, "* Grandmaster Priority 1: %u \r\n", OctetValue(self, saveBufPtr));
    DisplayClockQuality(self, level, saveBufPtr);
    DisplayAttribute(level, "* Grandmaster Priority 2: %u \r\n", OctetValue(self, saveBufPtr));
    DisplayClockIdentity(self, level, "Grandmaster Identity", saveBufPtr);
    DisplayAttribute(level, "* Steps Removed: %u \r\n", DoubleOctetValue(self, saveBufPtr));
    DisplayAttribute(level, "* Time Source: %u \r\n", OctetValue(self, saveBufPtr));
    }

static void DisaplayTLV(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    AtUnused(self);
    AtUnused(level);
    *saveBufPtr = (uint16)(*saveBufPtr + 10); /* TODO: Just to break the loop. */
    }

static void DisplaySignaling(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    uint16 length = (uint16)mMethodsGet(self)->PayloadLength(self);
    uint16 *payloadOffset = saveBufPtr;

    DisplayTimestamp(self, level, "requestReceiptTimestamp", saveBufPtr);
    level++;
    while ((saveBufPtr - payloadOffset) < length)
        DisaplayTLV(self, level, saveBufPtr);
    }

static void DisplayManagement(AtPacket self, uint8 level, uint16 *saveBufPtr)
    {
    DisplayTimestamp(self, level, "requestReceiptTimestamp", saveBufPtr);
    }

static void DisplayPayloadByMessageType(AtPacket self, uint8 level, uint8 mesgType)
    {
    static void (*payloadDisplayer[])(AtPacket self, uint8 level, uint16 *saveBufPtr) =
        {
        DisplaySyncPayload, /* 0x0 */
        DisplayDelayReqPayload, /* 0x1 */
        DisplayPdelayReqPayload, /* 0x2 */
        DisplayPdelayRespPayload, /* 0x3 */
        DisplayNothing, /* 0x4 */
        DisplayNothing,
        DisplayNothing,
        DisplayNothing, /* 0x7 */
        DisplayFollowUpPayload, /* 0x8 */
        DisplayDelayRespPayload, /* 0x9 */
        DisplayPdelayRespFollowUpPayload, /* 0xA */
        DisplayAnnounce, /* 0xB */
        DisplaySignaling, /* 0xC */
        DisplayManagement /* 0xD */
        };
    uint16 buffPtr = (uint16)mMethodsGet(self)->HeaderLength(self);

    if (mesgType < mCount(payloadDisplayer))
        payloadDisplayer[mesgType](self, level, &buffPtr);
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    uint32 payloadLength;
    uint8 mesgType = MessageType((AtPtpPacket)self);

    /* Get payload length */
    payloadLength = mMethodsGet(self)->PayloadLength(self);
    if (payloadLength == 0)
        return;

    /* Display this payload */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    DisplayPayloadByMessageType(self, (uint8)(level + 1), mesgType);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static AtPacket AtPtpPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New PTP packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtPtpPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return AtPtpPacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

/**
 * @}
 */

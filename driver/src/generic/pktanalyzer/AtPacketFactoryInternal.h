/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPacketFactoryInternal.h
 * 
 * Created Date: Mar 29, 2014
 *
 * Description : Packet factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPACKETFACTORYINTERNAL_H_
#define _ATPACKETFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacketFactory.h"
#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPacketFactoryMethods
    {
    AtPacket (*RawPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*EthPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*HdlcPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*IpV4PacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*IpV6PacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*LcpPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*MefPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*MefMplsPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*MplsPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*PppPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*UdpPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    AtPacket (*PwPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp, eAtPacketFactoryDirection direction);
    AtPacket (*XauiPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction, uint32 portId);
    AtPacket (*PtpPacketCreate)(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
    }tAtPacketFactoryMethods;

typedef struct tAtPacketFactory
    {
    tAtObject super;
    const tAtPacketFactoryMethods *methods;
    }tAtPacketFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacketFactory AtPacketFactoryObjectInit(AtPacketFactory self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPACKETFACTORYINTERNAL_H_ */


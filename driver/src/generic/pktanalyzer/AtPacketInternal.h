/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPacketInternal.h
 * 
 * Created Date: Apr 26, 2013
 *
 * Description : Packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPACKETINTERNAL_H_
#define _ATPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h"
#include "AtPacketUtil.h"
#include "AtPacketFactory.h"
#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPacketMethods
    {
    void (*DisplayAtLevel)(AtPacket self, uint8 level);
    void (*DisplayRaw)(AtPacket self, uint8 level);

    /* Internal */
    uint32 (*HeaderLength)(AtPacket self);
    uint32 (*PayloadLength)(AtPacket self);
    void (*DisplayHeader)(AtPacket self, uint8 level);
    void (*DisplayPayload)(AtPacket self, uint8 level);
    }tAtPacketMethods;

typedef struct tAtPacket
    {
    tAtObject super;
    const tAtPacketMethods *methods;

    /* Private */
    uint8 *dataBuffer;
    uint32 length;
    uint8 cacheMode;
    AtPacketFactory packetFactory;
    eBool packetIsCompleted; /* Mark packet as completed or not, last in-completed packet may also be analyzed */
    eBool isErrored; /* Mark error packet to be analyzed */
    }tAtPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

/* Internal methods */
void AtPacketDisplayAtLevel(AtPacket self, uint8 level);
void AtPacketUdpPwPacketMark(AtPacket self, eBool enable);

#ifdef __cplusplus
}
#endif
#endif /* _ATPACKETINTERNAL_H_ */


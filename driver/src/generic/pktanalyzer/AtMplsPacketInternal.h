/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtMplsPacketInternal.h
 * 
 * Created Date: Mar 28, 2014
 *
 * Description : MPLS packet class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMPLSPACKETINTERNAL_H_
#define _ATMPLSPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMplsPacket.h"
#include "AtPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtMplsPacket
    {
    tAtPacket super;

    /* Private */
    }tAtMplsPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtMplsPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATMPLSPACKETINTERNAL_H_ */


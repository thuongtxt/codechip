/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtIpV4PacketInternal.h
 * 
 * Created Date: Sep 16, 2016
 *
 * Description : IPV4 packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIPV4PACKETINTERNAL_H_
#define _ATIPV4PACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIpV4Packet.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtIpV4PacketMethods
	{
	uint8 (*Protocol)(AtIpV4Packet self);
	}tAtIpV4PacketMethods;

typedef struct tAtIpV4Packet
    {
    tAtPacket super;
    const tAtIpV4PacketMethods *methods;
    }tAtIpV4Packet;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtIpV4PacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
void AtIpPacketDisplayProtocol(AtPacket self, uint8 level, const char *description);

#ifdef __cplusplus
}
#endif
#endif /* _ATIPV4PACKETINTERNAL_H_ */


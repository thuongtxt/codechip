/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPacketFactory.c
 *
 * Created Date: Mar 29, 2014
 *
 * Description : Packet factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include <AtEXauiPacket.h>
#include "AtPacket.h"
#include "AtEthPacket.h"
#include "AtHdlcPacket.h"
#include "AtIpV4Packet.h"
#include "AtIpV6Packet.h"
#include "AtLcpPacket.h"
#include "AtMefPacket.h"
#include "AtMplsPacket.h"
#include "AtPppPacket.h"
#include "AtUdpPacket.h"
#include "AtPtpPacket.h"
#include "AtPacketFactoryInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPacketFactoryMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPacket RawPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket EthPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtEthPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket HdlcPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtHdlcPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket IpV4PacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtIpV4PacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket IpV6PacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtIpV6PacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket LcpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtLcpPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket MefPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtMefPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket MefMplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    AtUnused(self);
    AtUnused(direction);
    return AtMefPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket MplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtMplsPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket PppPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtPppPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket UdpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtUdpPacketNew(dataBuffer, length, cacheMode);
    }

static AtPacket PwPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp, eAtPacketFactoryDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    return AtPwPacketNew(dataBuffer, length, cacheMode, type, rtp);
    }

static AtPacket XauiPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction, uint32 portId)
    {
    AtUnused(self);
    return AtEXauiPacketNew(dataBuffer, length, cacheMode, direction, portId);
    }

static AtPacket PtpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    AtUnused(direction);
    AtUnused(self);
    return AtPtpPacketNew(dataBuffer, length, cacheMode);
    }

static void Override(AtPacketFactory self)
    {
	AtUnused(self);
    }

static void MethodsInit(AtPacketFactory self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, RawPacketCreate);
        mMethodOverride(m_methods, EthPacketCreate);
        mMethodOverride(m_methods, HdlcPacketCreate);
        mMethodOverride(m_methods, IpV4PacketCreate);
        mMethodOverride(m_methods, IpV6PacketCreate);
        mMethodOverride(m_methods, LcpPacketCreate);
        mMethodOverride(m_methods, MefPacketCreate);
        mMethodOverride(m_methods, MefMplsPacketCreate);
        mMethodOverride(m_methods, MplsPacketCreate);
        mMethodOverride(m_methods, PppPacketCreate);
        mMethodOverride(m_methods, UdpPacketCreate);
        mMethodOverride(m_methods, PwPacketCreate);
        mMethodOverride(m_methods, XauiPacketCreate);
        mMethodOverride(m_methods, PtpPacketCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPacketFactory);
    }

AtPacketFactory AtPacketFactoryObjectInit(AtPacketFactory self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPacketFactory AtPacketFactoryNew(void)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacketFactory newFactory = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFactory == NULL)
        return NULL;

    return AtPacketFactoryObjectInit(newFactory);
    }

AtPacketFactory AtPacketFactoryDefaultFactory(void)
    {
    static AtPacketFactory factory = NULL;

    if (factory == NULL)
        {
        static tAtPacketFactory _factory;
        factory = &_factory;
        factory = AtPacketFactoryObjectInit(factory);
        }

    return factory;
    }

AtPacket AtPacketFactoryRawPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->RawPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryEthPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->EthPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryHdlcPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->HdlcPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryIpV4PacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->IpV4PacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryIpV6PacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->IpV6PacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryLcpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->LcpPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryMefPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->MefPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryMefMplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->MefMplsPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryMplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->MplsPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryPppPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->PppPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryUdpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->UdpPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

AtPacket AtPacketFactoryPwPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->PwPacketCreate(self, dataBuffer, length, cacheMode, type, rtp, direction);
    return NULL;
    }

AtPacket AtPacketFactoryXauiPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction, uint32 portId)
    {
    if (self)
        return mMethodsGet(self)->XauiPacketCreate(self, dataBuffer, length, cacheMode, direction, portId);
    return NULL;
    }

AtPacket AtPacketFactoryPtpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction)
    {
    if (self)
        return mMethodsGet(self)->PtpPacketCreate(self, dataBuffer, length, cacheMode, direction);
    return NULL;
    }

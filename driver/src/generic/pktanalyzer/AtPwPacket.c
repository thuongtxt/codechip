/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPwPacket.c
 *
 * Created Date: Mar 29, 2014
 *
 * Description : PW packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwPacket.h"
#include "AtPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cTdmControlWordLength 4
#define cRtpHeaderLength 12

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtPwPacket *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPwPacket
    {
    tAtPacket super;

    /* Private data */
    eAtPwPacketType    type;
    eAtPwPacketRtpMode rtp;
    eBool isUdpPwPacket;
    }tAtPwPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * * CEP control word
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |0|0|0|0|L|R|N|P|FRG|Length[0:5]|    Sequence Number[0:15]      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |               Reserved                |Structure Pointer[0:11]|
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 * * SAToP control word
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |0 0 0 0|L|R|RSV|FRG|   LEN     |       Sequence number         |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 * * CESoP control word
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |0|0|0|0|L|R| M |FRG|   LEN     |       Sequence number         |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 * * RTP
 *  0                   1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |V=2|P|X|  CC   |M|     PT      |       Sequence Number         |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                           Timestamp                           |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |           Synchronization Source (SSRC) Identifier            |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
static uint8 RtpV(uint8 *buffer, uint32 length)
    {
	AtUnused(length);
    return (buffer[0] & cBit7_6) >> 6;
    }

static uint8 RtpP(uint8 *buffer, uint32 length)
    {
	AtUnused(length);
    return (buffer[0] & cBit5) ? 1 : 0;
    }

static uint8 RtpX(uint8 *buffer, uint32 length)
    {
	AtUnused(length);
    return (buffer[0] & cBit4) ? 1 : 0;
    }

static uint8 RtpCC(uint8 *buffer, uint32 length)
    {
	AtUnused(length);
    return (buffer[0] & cBit3_0);
    }

static uint8 RtpM(uint8 *buffer, uint32 length)
    {
    if (length < 2)
        return 0x0;
    return (buffer[1] & cBit7) ? 1 : 0;
    }

static uint8 RtpPT(uint8 *buffer, uint32 length)
    {
    if (length < 2)
        return 0x0;
    return (buffer[1] & cBit6_0);
    }

static uint16 RtpSequenceNumber(uint8 *buffer, uint32 length)
    {
    if (length < 4)
        return 0x0;
    return (uint16)((buffer[2] << 8) | buffer[3]);
    }

static uint32 RtpTimestamp(uint8 *buffer, uint32 length)
    {
    if (length < 8)
        return 0x0;
    return AtPktUtilDwordGet(&(buffer[4]));
    }

static uint32 RtpSSRC(uint8 *buffer, uint32 length)
    {
    if (length < 12)
        return 0x0;
    return AtPktUtilDwordGet(&(buffer[8]));
    }

static uint8 ControlWordL(uint8 *payload, uint32 size)
    {
	AtUnused(size);
    return (payload[0] & cBit3) ? 1 : 0;
    }

static uint8 ControlWordR(uint8 *payload, uint32 size)
    {
	AtUnused(size);
    return (payload[0] & cBit2) ? 1 : 0;
    }

static uint8 ControlWordFRG(uint8 *payload, uint32 size)
    {
    if (size < 2)
        return 0x0;
    return ((payload[1] & cBit7_6) >> 6);
    }

static uint8 ControlWordLength(uint8 *payload, uint32 size)
    {
    if (size < 2)
        return 0x0;
    return (payload[1] & cBit5_0);
    }

static uint16 ControlWordSequenceNumber(uint8 *payload, uint32 size)
    {
    if (size < 4)
        return 0x0;
    return (uint16)((payload[2] << 8) | payload[3]);
    }

static uint8 CepControlWordN(uint8 *payload, uint32 size)
    {
	AtUnused(size);
    return (payload[0] & cBit1) ? 1 : 0;
    }

static uint8 CepControlWordP(uint8 *payload, uint32 size)
    {
	AtUnused(size);
    return (payload[0] & cBit0) ? 1 : 0;
    }

static uint16 CepControlWordReserved(uint8 *payload, uint32 size)
    {
    uint32 controlWordReserved;

    if (size < 7)
        return 0x0;

    controlWordReserved  = payload[4]; controlWordReserved <<= 8;
    controlWordReserved |= payload[5]; controlWordReserved <<= 8;
    controlWordReserved |= ((payload[6] & cBit7_4) >> 4);

    return (uint16)controlWordReserved;
    }

static uint16 CepControlWordStructurePointer(uint8 *payload, uint32 size)
    {
    if (size < 8)
        return 0x0;
    return (uint16)(((payload[6] & cBit3_0) << 8) | payload[7]);
    }

static uint8 CepControlLength(AtPacket self, eAtPwPacketType type)
    {
	AtUnused(self);
    switch (type)
        {
        case cAtPwPacketTypeCEPBasic            : return 8;
        case cAtPwPacketTypeCEPVc3FactionalNoEbm: return 8;
        case cAtPwPacketTypeCEPVc3FactionalEbm  : return 12;
        case cAtPwPacketTypeCEPVc4FactionalNoEbm: return 8;
        case cAtPwPacketTypeCEPVc4FactionalEbm  : return 20;

        case cAtPwPacketTypeToh:
        case cAtPwPacketTypeSAToP:
        case cAtPwPacketTypeCESoP:
        case cAtPwPacketTypeUnknown:
        default:
            return 8;
        }
    }

static eBool RtpIsEnabled(AtPacket self)
	{
	return (mThis(self)->rtp == cAtPwPacketRtpModeRtp) ? cAtTrue : cAtFalse;
	}

static eBool IsUdpPwPacket(AtPacket self)
	{
	return mThis(self)->isUdpPwPacket;
	}

static uint8 *CepControlWord(AtPacket self, uint32 *size, eAtPwPacketType type)
    {
    uint32 payloadSize;
    uint8 *payload = AtPacketPayloadBuffer(self, &payloadSize);
    uint32 cepCwLength = (uint32)CepControlLength(self, type);
    uint32 cepHeaderSize = mMin(cepCwLength, payloadSize);
    if (size)
        *size = cepHeaderSize;

    if (IsUdpPwPacket(self) && RtpIsEnabled(self))
    	return cepHeaderSize ? &(payload[cRtpHeaderLength]) : NULL;

    return cepHeaderSize ? payload : NULL;
    }

static uint8 *CepRtp(AtPacket self, uint32 *size, eAtPwPacketType type, eAtPwPacketRtpMode rtpMode)
    {
    uint32 payloadSize, controlWordSize;
    uint8 *payload;

    if (rtpMode == cAtPwPacketRtpModeNoRtp)
        return NULL;

    payload = AtPacketPayloadBuffer(self, &payloadSize);
    if (payload == NULL)
        return NULL;

    controlWordSize = CepControlLength(self, type);
    if (payloadSize <= controlWordSize)
        return NULL;

    if (size)
        *size = payloadSize - controlWordSize;

    if (IsUdpPwPacket(self))
    	return payload;

    return &(payload[controlWordSize]);
    }

static const char *PacketTypeString(AtPacket self, eAtPwPacketType type)
    {
	AtUnused(self);
    switch (type)
        {
        case cAtPwPacketTypeCEPBasic            : return "Basic";
        case cAtPwPacketTypeCEPVc3FactionalNoEbm: return "VC3 fractional (No EBM)";
        case cAtPwPacketTypeCEPVc3FactionalEbm  : return "VC3 fractional (EBM)";
        case cAtPwPacketTypeCEPVc4FactionalNoEbm: return "VC4 fractional (No EBM)";
        case cAtPwPacketTypeCEPVc4FactionalEbm  : return "VC4 fractional (EBM)";
        case cAtPwPacketTypeToh                 : return "TOH";
        case cAtPwPacketTypeSAToP               : return "SAToP";
        case cAtPwPacketTypeCESoP               : return "CESoP";

        case cAtPwPacketTypeUnknown:
        default:
            return "Unknown";
        }
    }

static void RtpDisplay(uint8 *rtp, uint32 rtpSize, uint8 level)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* RTP: V = %u, P = %u, X = %u, CC = %u, M = %u, PT = %u, SequenceNumber = %u, Timestamp = %u, SSRC = %u\r\n",
             RtpV(rtp, rtpSize),
             RtpP(rtp, rtpSize),
             RtpX(rtp, rtpSize),
             RtpCC(rtp, rtpSize),
             RtpM(rtp, rtpSize),
             RtpPT(rtp, rtpSize),
             RtpSequenceNumber(rtp, rtpSize),
             RtpTimestamp(rtp, rtpSize),
             RtpSSRC(rtp, rtpSize));
    }

static eBool HasEbm(eAtPwPacketType type)
    {
    if ((type == cAtPwPacketTypeCEPVc3FactionalEbm) || (type == cAtPwPacketTypeCEPVc4FactionalEbm))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 CepControlWordNumEbm(eAtPwPacketType type)
    {
    return (type == cAtPwPacketTypeCEPVc3FactionalEbm) ? 1 : 3;
    }

static uint32 CepControlWordEbmAtIndex(eAtPwPacketType type, uint8 *controlWord, uint32 controlWordSize, uint8 ebmIndex)
    {
    uint32 startPosition = 8 + (ebmIndex * 4UL);
    AtUnused(type);
    if (startPosition >= controlWordSize)
        return 0x0;
    return AtPktUtilDwordGet(&(controlWord[startPosition]));
    }

static void CepControlWordEbmDisplay(eAtPwPacketType type, uint8 *controlWord, uint32 controlWordSize)
    {
    uint8 ebm_i;
    for (ebm_i = 0; ebm_i < CepControlWordNumEbm(type); ebm_i++)
        {
        uint32 ebm = CepControlWordEbmAtIndex(type, controlWord, controlWordSize, ebm_i);
        AtPrintc(cSevNormal, ", EBM#%d = 0x%08x", ebm_i + 1, ebm);
        }
    }

static void CepHeaderDisplayWithType(AtPacket self, uint8 level, eAtPwPacketType type, eAtPwPacketRtpMode rtpMode)
    {
    uint8 nextLevel = (uint8)(level + 1);
    uint32 controlWordSize, rtpSize = 0;
    uint8 *controlWord = CepControlWord(self, &controlWordSize, type);
    uint8 *rtp = CepRtp(self, &rtpSize, type, rtpMode);

    if (controlWord == NULL)
        return;

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* CEP %s header: \r\n", PacketTypeString(self, type));

    /* Display control word */
    AtPacketPrintSpaces(nextLevel);
    AtPrintc(cSevNormal,
             "* Control Word: L = %u, R = %u, N = %u, P = %u, FRG = %u, Length = %u, SequenceNumber = %u, Reserved = %u, StructurePointer = %u",
             ControlWordL(controlWord, controlWordSize),
             ControlWordR(controlWord, controlWordSize),
             CepControlWordN(controlWord, controlWordSize),
             CepControlWordP(controlWord, controlWordSize),
             ControlWordFRG(controlWord, controlWordSize),
             ControlWordLength(controlWord, controlWordSize),
             ControlWordSequenceNumber(controlWord, controlWordSize),
             CepControlWordReserved(controlWord, controlWordSize),
             CepControlWordStructurePointer(controlWord, controlWordSize));
    if (HasEbm(type))
        CepControlWordEbmDisplay(type, controlWord, controlWordSize);
    AtPrintc(cSevNormal, "\r\n");

    /* Display RTP header */
    if (rtp)
        RtpDisplay(rtp, rtpSize, nextLevel);
    }

static void CepHeaderDisplay(AtPacket self, uint8 level)
    {
    if (mThis(self)->type != cAtPwPacketTypeUnknown)
        {
        CepHeaderDisplayWithType(self, level, mThis(self)->type, mThis(self)->rtp);
        return;
        }

    /* Unknown type, just display all of possible cases */
    CepHeaderDisplayWithType(self, level, cAtPwPacketTypeCEPBasic,             cAtPwPacketRtpModeRtp);
    CepHeaderDisplayWithType(self, level, cAtPwPacketTypeCEPVc3FactionalNoEbm, cAtPwPacketRtpModeRtp);
    CepHeaderDisplayWithType(self, level, cAtPwPacketTypeCEPVc3FactionalEbm,   cAtPwPacketRtpModeRtp);
    CepHeaderDisplayWithType(self, level, cAtPwPacketTypeCEPVc4FactionalNoEbm, cAtPwPacketRtpModeRtp);
    CepHeaderDisplayWithType(self, level, cAtPwPacketTypeCEPVc4FactionalEbm,   cAtPwPacketRtpModeRtp);
    }

static uint8 SAToPControlWordRSV(uint8 *payload, uint32 size)
    {
	AtUnused(size);
    return (payload[0] & cBit1_0);
    }

static uint8 *TdmControlWord(AtPacket self, uint32 *size)
    {
    uint32 payloadSize;
    uint8 *payload = AtPacketPayloadBuffer(self, &payloadSize);
    uint32 controlWordSize = mMin(cTdmControlWordLength, payloadSize);
    if (size)
        *size = controlWordSize;

    if (IsUdpPwPacket(self) && RtpIsEnabled(self))
    	return controlWordSize ? &(payload[cRtpHeaderLength]) : NULL;

    return controlWordSize ? payload : NULL;
    }

static uint8 *TdmRtp(AtPacket self, uint32 *size)
    {
    uint32 payloadSize;
    uint8 *payload;

    if (mThis(self)->rtp == cAtPwPacketRtpModeNoRtp)
        return NULL;

    payload = AtPacketPayloadBuffer(self, &payloadSize);
    if (payload == NULL)
        return NULL;

    if (payloadSize <= cTdmControlWordLength)
        return NULL;

    if (size)
        *size = payloadSize - cTdmControlWordLength;

    if (IsUdpPwPacket(self))
    	return payload;

    return &(payload[cTdmControlWordLength]);
    }

static void TdmHeaderDisplayWithType(AtPacket self, uint8 level, eAtPwPacketType type)
    {
    uint8 nextLevel = (uint8)(level + 1);
    uint32 controlWordSize, rtpSize = 0;
    uint8 *controlWord = TdmControlWord(self, &controlWordSize);
    uint8 *rtp = TdmRtp(self, &rtpSize);

    if (controlWord == NULL)
        return;

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s header: \r\n", PacketTypeString(self, type));

    /* Display control word */
    AtPacketPrintSpaces(nextLevel);
    AtPrintc(cSevNormal,
             "* Control Word: L = %u, R = %u, RSV = %u, FRG = %u, Length = %u,  SequenceNumber = %u\r\n",
             ControlWordL(controlWord, controlWordSize),
             ControlWordR(controlWord, controlWordSize),
             SAToPControlWordRSV(controlWord, controlWordSize),
             ControlWordFRG(controlWord, controlWordSize),
             ControlWordLength(controlWord, controlWordSize),
             ControlWordSequenceNumber(controlWord, controlWordSize));

    /* Display RTP header */
    if (rtp)
        RtpDisplay(rtp, rtpSize, nextLevel);
    }

static uint8 CESoPControlWordM(uint8 *payload, uint32 size)
    {
	AtUnused(size);
    return (payload[0] & cBit1_0);
    }

static void CESopHeaderDisplay(AtPacket self, uint8 level)
    {
    uint8 nextLevel = (uint8)(level + 1);
    uint32 controlWordSize, rtpSize = 0;
    uint8 *controlWord = TdmControlWord(self, &controlWordSize);
    uint8 *rtp = TdmRtp(self, &rtpSize);

    if (controlWord == NULL)
        return;

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* CESoP header: \r\n");

    /* Display control word */
    AtPacketPrintSpaces(nextLevel);
    AtPrintc(cSevNormal,
             "* Control Word: L = %u, R = %u, M = %u, FRG = %u, Length = %u,  SequenceNumber = %u\r\n",
             ControlWordL(controlWord, controlWordSize),
             ControlWordR(controlWord, controlWordSize),
             CESoPControlWordM(controlWord, controlWordSize),
             ControlWordFRG(controlWord, controlWordSize),
             ControlWordLength(controlWord, controlWordSize),
             ControlWordSequenceNumber(controlWord, controlWordSize));

    /* Display RTP header */
    if (rtp)
        RtpDisplay(rtp, rtpSize, nextLevel);
    }

static eBool NeedDisplayCepHeader(AtPacket self)
    {
    if ((mThis(self)->type == cAtPwPacketTypeCEPBasic)             ||
        (mThis(self)->type == cAtPwPacketTypeCEPVc3FactionalEbm)   ||
        (mThis(self)->type == cAtPwPacketTypeCEPVc3FactionalNoEbm) ||
        (mThis(self)->type == cAtPwPacketTypeCEPVc4FactionalEbm)   ||
        (mThis(self)->type == cAtPwPacketTypeCEPVc4FactionalNoEbm) ||
        (mThis(self)->type == cAtPwPacketTypeUnknown))
        return cAtTrue;
    return cAtFalse;
    }

static eBool NeedDisplaySAToPHeader(AtPacket self)
    {
    if ((mThis(self)->type == cAtPwPacketTypeSAToP) ||
        (mThis(self)->type == cAtPwPacketTypeUnknown))
        return cAtTrue;
    return cAtFalse;
    }

static eBool NeedDisplayCESoPPHeader(AtPacket self)
    {
    if ((mThis(self)->type == cAtPwPacketTypeCESoP) ||
        (mThis(self)->type == cAtPwPacketTypeUnknown))
        return cAtTrue;
    return cAtFalse;
    }

static eBool NeedDisplayTohHeader(AtPacket self)
    {
    if ((mThis(self)->type == cAtPwPacketTypeToh) ||
        (mThis(self)->type == cAtPwPacketTypeUnknown))
        return cAtTrue;
    return cAtFalse;
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    if (NeedDisplayCepHeader(self))
        CepHeaderDisplay(self, level);
    if (NeedDisplayTohHeader(self))
        TdmHeaderDisplayWithType(self, level, cAtPwPacketTypeToh);
    if (NeedDisplaySAToPHeader(self))
        TdmHeaderDisplayWithType(self, level, cAtPwPacketTypeSAToP);
    if (NeedDisplayCESoPPHeader(self))
        CESopHeaderDisplay(self, level);
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
	AtUnused(level);
	AtUnused(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPwPacket);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *data, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, data, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->type = type;
    mThis(self)->rtp  = rtp;
    mThis(self)->isUdpPwPacket = cAtFalse;

    return self;
    }

AtPacket AtPwPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    return ObjectInit(newPacket, data, length, cacheMode, type, rtp);
    }

void AtPacketUdpPwPacketMark(AtPacket self, eBool marked)
	{
    if (self)
        mThis(self)->isUdpPwPacket = marked;
	}

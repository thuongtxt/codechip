/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPrmMessage.c
 *
 * Created Date: Nov 7, 2017
 *
 * Description : PRM message
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacketInternal.h"
#include "AtPrmMessage.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtPrmMessage *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPrmMessage
    {
    tAtPacket super;

    /* Private data */
    uint32 rowIndex;
    uint8 messageType;
    }tAtPrmMessage;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsATandTPacket(AtPacket self)
    {
    uint32 packetLength = AtPacketLengthGet(self);
    if (packetLength == cAtandTPrmSize)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 HeaderLength(AtPacket self)
    {
    if (IsATandTPacket(self))
        return 2;
    return 3;
    }

static uint32 NumPayloadRows(AtPacket self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 FcsLength(AtPacket self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 PayloadLength(AtPacket self)
    {
    AtUnused(self);
    return NumPayloadRows(self) + FcsLength(self);
    }

static uint32 SAPI(AtPacket self)
    {
    return (AtPacketDataBuffer(self, NULL)[0] & cBit7_2) >> 2;
    }

static uint32 CR(AtPacket self)
    {
    return (AtPacketDataBuffer(self, NULL)[0] & cBit1) >> 1;
    }

static uint32 EA0(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[0] & cBit0;
    }

static uint32 TEI(AtPacket self)
    {
    return (AtPacketDataBuffer(self, NULL)[1] & cBit7_1) >> 1;
    }

static uint32 CONTROL(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[2];
    }

static uint32 EA1(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[1] & cBit0;
    }

static uint32 NextRow(AtPacket self)
    {
    uint32 currentRow = mThis(self)->rowIndex;
    mThis(self)->rowIndex = mThis(self)->rowIndex + 1;
    return currentRow;
    }

static uint32 ATandT_ADDRESS(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[0];
    }

static uint32 ATandT_CONTROL(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[1];
    }

static void DisplayAnsiHeader(AtPacket self)
    {
    AtPrintf("[%2d] SAPI: 0x%x, C/R: %d, EA: %d\r\n", NextRow(self), SAPI(self), CR(self), EA0(self));
    AtPrintf("[%2d] TEI: 0x%x, EA: %d\r\n", NextRow(self), TEI(self), EA1(self));
    AtPrintf("[%2d] CONTROL: 0x%x\r\n", NextRow(self), CONTROL(self));
    }

static void DisplayATandTHeader(AtPacket self)
    {
    AtPrintf("[%2d] ADDRESS: 0x%x\r\n", NextRow(self), ATandT_ADDRESS(self));
    AtPrintf("[%2d] CONTROL: 0x%x\r\n", NextRow(self), ATandT_CONTROL(self));
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    uint32 headerLength = mMethodsGet(self)->HeaderLength(self);
    if (AtPacketLengthGet(self) < headerLength)
        return;

    /* Scalar attributes */
    AtPacketPrintSpaces(level);
    if (IsATandTPacket(self))
        DisplayATandTHeader(self);
    else
        DisplayAnsiHeader(self);
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    uint32 payloadSize;
    uint8 *buffer;
    const char *nprmBitNames0[] = {"N1", "N2", "NSE", " R", "N3", "N4", "PA", "FC"};
    const char *nprmBitNames1[] = {"F1", "F2", "FSE", " R", "F3", "F4", "Nm", "Nl"};
    const char *prmBitNames0[]  = {"G3", "LV", "G4" , "U1", "U2", "G5", "SL", "G6"};
    const char *prmBitNames1[]  = {"FE", "SE", "LB" , "G1", " R", "G2", "Nm", "Nl"};
    const char **bitNames0 = prmBitNames0;
    const char **bitNames1 = prmBitNames1;
    uint32 byte_i;

    buffer = AtPacketPayloadBuffer(self, &payloadSize);
    if (buffer == NULL)
        return;

    if (mThis(self)->messageType == cAtPrmMessageTypeNPRM)
        {
        bitNames0 = nprmBitNames0;
        bitNames1 = nprmBitNames1;
        }

    for (byte_i = 0; byte_i < NumPayloadRows(self); byte_i++)
        {
        const char **rowBitNames = ((byte_i % 2) == 0) ? bitNames0 : bitNames1;
        uint32 bit_i;

        AtPacketPrintSpaces(level);
        AtPrintf("[%2d] ", NextRow(self));
        for (bit_i = 0; bit_i < 8; bit_i++)
            {
            uint32 mask = cBit7 >> bit_i; /* MSB will be displayed first */
            uint32 bitValue = (buffer[byte_i] & mask) ? 1 : 0;
            if (bit_i > 0)
                AtPrintf(", ");
            AtPrintf("%s: ", rowBitNames[bit_i]);
            AtPrintc(bitValue ? cSevInfo : cSevNormal, "%d", bitValue);
            }
        AtPrintf("\r\n");
        }

    /* Display FCS */
    AtPacketPrintSpaces(level); AtPrintf("[%2d] FCS-lsb: 0x%02x\r\n", NextRow(self), buffer[byte_i++]);
    AtPacketPrintSpaces(level); AtPrintf("[%2d] FCS-msb: 0x%02x\r\n", NextRow(self), buffer[byte_i++]);
    }

static void DisplayAtLevel(AtPacket self, uint8 level)
    {
    mThis(self)->rowIndex = 0;
    m_AtPacketMethods->DisplayAtLevel(self, level);
    }

static void OverrideAtPacket(AtPacket self)
    {
    AtPacket packet = (AtPacket)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(packet);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayAtLevel);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        mMethodOverride(m_AtPacketOverride, PayloadLength);
        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(packet, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPrmMessage);
    }

static AtPacket ObjectInit(AtPacket self, eAtPrmMessageType messageType, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->messageType = messageType;

    return self;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New PRM message
 *
 * @param messageType @ref eAtPrmMessageType "PRM message type"
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtPrmMessageNew(eAtPrmMessageType messageType, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, messageType, dataBuffer, length, cacheMode);
    }
/**
 * @}
 */

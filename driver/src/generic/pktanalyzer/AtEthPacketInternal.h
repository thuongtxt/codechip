/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtEthPacketInternal.h
 * 
 * Created Date: Jun 27, 2013
 *
 * Description : Ethernet packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHPACKETINTERNAL_H_
#define _ATETHPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacketInternal.h"
#include "AtEthPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cPreambleLength     8
#define cMacLength          6
#define cVlanLength         4
#define cEthTypeLength      2
#define cCrcLength          4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtVlanTag
    {
    uint16 tpid;   /* Tag Protocol Identifier */
    uint8 pcp;     /* Priority Code Point */
    uint8 cfi;     /* Canonical Format Indicator */
    uint16 vlanId; /* VLAN ID */
    }tAtVlanTag;

typedef struct tAtEthPacketMethods
    {
    void (*DisplayVlans)(AtEthPacket self, uint8 level);
    const char* (*VlanTypeString)(AtEthPacket self, uint16 tpid);
    eBool (*VlanTpidIsValid)(AtEthPacket self, uint16 tpid);
    eBool (*HasEthTypeLength)(AtEthPacket self);
    eBool (*HasCrc)(AtEthPacket self);
    uint8 (*DisplayPreamble)(AtEthPacket self, uint8 level);
    uint8 (*PreambleLen)(AtEthPacket self);
    uint16 (*FirstVlanOffset)(AtEthPacket self);
    tAtVlanTag *(*VlanAtOffset)(AtEthPacket self, uint32 offset, tAtVlanTag *vlanTag);
    tAtVlanTag *(*VlagTagFromBuffer)(AtEthPacket self, tAtVlanTag *vlanTag, uint8 *buffer, uint32 offset);
    eBool (*ShouldCheckVlanTpid)(AtEthPacket self, uint32 offset);
    void (*DisplayMacContent)(AtEthPacket self, const char *name, uint8 *mac);
    }tAtEthPacketMethods;

typedef struct tAtEthPacket
    {
    tAtPacket super;
    const tAtEthPacketMethods *methods;

    /* Private */
    }tAtEthPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtEthPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

uint8 AtEthPacketPreambleLen(AtEthPacket self);
void AtEthPacketDisplayMac(AtEthPacket self, const char *name, uint8 *mac, uint8 level);
uint8 AtEthPacketNumVlans(AtEthPacket self);
void AtEthPacketDisplayVlans(AtEthPacket self, uint8 level);
void AtEthPacketDisplayEthTypeLength(AtEthPacket self, uint8 level);
void AtEthPacketDisplayCrc(AtEthPacket self, uint8 level);
uint16 AtEthPacketEthTypeLength(AtEthPacket self);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHPACKETINTERNAL_H_ */


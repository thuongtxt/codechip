/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPacketUtil.h
 * 
 * Created Date: May 2, 2013
 *
 * Description : Packet util
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPACKETUTIL_H_
#define _ATPACKETUTIL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtPacketPrintSpaces(uint8 numSpaces);
void AtPacketDumpBuffer(AtPacket self, uint8 *buffer, uint32 length, uint8 level);
void AtPacketDisplayBufferInHex(AtPacket self, uint8 *buffer, uint32 length, const char *separate);
void AtPacketPrintAttribute(AtPacket self, const char *name, uint32 value, uint8 level);
void AtPacketPrintStringAttribute(AtPacket self, const char *name, const char *value, uint8 level);
void AtPacketPrintHexAttribute(AtPacket self,const char *name, uint32 value, uint8 level);
void AtPacketPrintIp(AtPacket self, const char *name, uint8 *ip, uint8 length, uint8 level);

uint8 *AtPktUtilEthPreambleDetect(uint8 *dataBuffer, uint32 bufferSize, uint8 *length);
uint16 AtPktUtilEthFirstVlanOffset(uint8 *dataBuffer, uint32 bufferSize);

uint16 AtPktUtilWordGet(uint8 *buffer);
uint32 AtPktUtilDwordGet(uint8 *buffer);

#ifdef __cplusplus
}
#endif
#endif /* _ATPACKETUTIL_H_ */


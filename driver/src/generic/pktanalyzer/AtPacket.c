/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPacket.c
 *
 * Created Date: Apr 26, 2013
 *
 * Description : Packet implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacketInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtPacket)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPacketMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPacket);
    }

static void Delete(AtObject self)
    {
    /* Delete message buffer first */
    AtPacket packet = (AtPacket)self;
    AtOsal osal = AtSharedDriverOsalGet();
    if (packet->cacheMode == cAtPacketCacheModeCacheData)
        mMethodsGet(osal)->MemFree(osal, packet->dataBuffer);
    packet->dataBuffer = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPacket object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(dataBuffer);
    mEncodeNone(length);
    mEncodeUInt(cacheMode);
    mEncodeObjectDescription(packetFactory);
    mEncodeNone(packetIsCompleted);
    mEncodeNone(isErrored);
    }

static uint32 HeaderLength(AtPacket self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 PayloadLength(AtPacket self)
    {
    if (AtPacketLengthGet(self) > mMethodsGet(self)->HeaderLength(self))
        return AtPacketLengthGet(self) - mMethodsGet(self)->HeaderLength(self);
    return 0;
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
	AtUnused(level);
	AtUnused(self);
    /* Sub class must know */
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    uint32 length;
    uint8 *buffer = AtPacketDataBuffer(self, &length);
    uint32 payloadOffset = mMethodsGet(self)->HeaderLength(self);
    uint32 payloadLength;

    payloadLength = mMethodsGet(self)->PayloadLength(self);
    if (payloadLength == 0)
        return;

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDumpBuffer(self, &(buffer[payloadOffset]), payloadLength, (uint8)(level + 1));
    }

static void DisplayAtLevel(AtPacket self, uint8 level)
    {
    mMethodsGet(self)->DisplayHeader(self, level);
    mMethodsGet(self)->DisplayPayload(self, level);
    }

static void DisplayRaw(AtPacket self, uint8 level)
    {
    uint32 length;
    uint8 *buffer = AtPacketDataBuffer(self, &length);

    AtPacketDumpBuffer(self, buffer, length, level);
    }

static void OverrideAtObject(AtPacket self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPacket self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, DisplayAtLevel);
        mMethodOverride(m_methods, HeaderLength);
        mMethodOverride(m_methods, DisplayHeader);
        mMethodOverride(m_methods, DisplayPayload);
        mMethodOverride(m_methods, PayloadLength);
        mMethodOverride(m_methods, DisplayRaw);
        }

    mMethodsSet(self, &m_methods);
    }

AtPacket AtPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
	uint8 *buffer;
	
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save data buffer */
    if (length == 0)
        return self;

    /* Need memory to copy input data buffer */
    if (cacheMode == cAtPacketCacheModeCacheData)
        {
        buffer = mMethodsGet(osal)->MemAlloc(osal, length);
        if (buffer == NULL)
            return NULL;
        mMethodsGet(osal)->MemCpy(osal, buffer, dataBuffer, length);
        }
    else
        buffer = dataBuffer;

    self->dataBuffer = buffer;
    self->length     = length;
    self->cacheMode  = (uint8)cacheMode;
    self->packetIsCompleted = cAtTrue;

    return self;
    }

void AtPacketDisplayAtLevel(AtPacket self, uint8 level)
    {
    if (self)
        mMethodsGet(self)->DisplayAtLevel(self, level);
    }

void AtPacketIsCompletedMark(AtPacket self, eBool isCompleted)
    {
    if (self)
        self->packetIsCompleted = isCompleted;
    }

eBool AtPacketIsCompleted(AtPacket self)
    {
    if (self)
        return self->packetIsCompleted;
    return cAtFalse;
    }

void AtPacketMarkError(AtPacket self, eBool error)
    {
    if (self)
        self->isErrored = error;
    }

/**
 * @addtogroup AtPacket
 * @{
 */

/**
 * New packet
 *
 * @param data Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return AtPacketObjectInit(newPacket, data, length, cacheMode);
    }

/**
 * Get data buffer
 *
 * @param self This packet
 * @param [out] numBytesInBuffer Number of bytes in buffer.
 *              Note, it is not buffer size
 *
 * @return Packet buffer
 */
uint8 *AtPacketDataBuffer(AtPacket self, uint32 *numBytesInBuffer)
    {
    if (numBytesInBuffer)
        *numBytesInBuffer = self->length;

    return self->dataBuffer;
    }

/**
 * Packet length
 *
 * @param self This packet
 *
 * @return Packet length
 */
uint32 AtPacketLengthGet(AtPacket self)
    {
    if (self)
        return self->length;

    return 0;
    }

/**
 * Get payload buffer
 *
 * @param self This packet
 * @param [out] payloadSize Payload size
 *
 * @return Payload buffer
 */
uint8 *AtPacketPayloadBuffer(AtPacket self, uint32 *payloadSize)
    {
    uint32 bufferSize;
    uint8 *buffer = AtPacketDataBuffer(self, &bufferSize);
    uint32 headerLength = mMethodsGet(self)->HeaderLength(self);
    if (headerLength >= bufferSize)
        return NULL;

    if (payloadSize)
        *payloadSize = bufferSize - headerLength;

    return &(buffer[headerLength]);
    }

/**
 * Get data caching mode
 *
 * @param self This packet
 *
 * @return @ref eAtPacketCacheMode "Data caching mode"
 */
eAtPacketCacheMode AtPacketDataCacheModeGet(AtPacket self)
    {
    if (self)
        return self->cacheMode;

    return cAtPacketCacheModeNoCache;
    }

/**
 * Display packet in meaningful format
 *
 * @param self This packet
 */
void AtPacketDisplay(AtPacket self)
    {
    AtPacketDisplayAtLevel(self, 0);
    }

/**
 * Display packet in raw format
 *
 * @param self This packet
 * @param level Level to display
 */
void AtPacketDisplayRaw(AtPacket self, uint8 level)
    {
    if (self)
        mMethodsGet(self)->DisplayRaw(self, level);
    }

/**
 * Change the packet factory used to create packets in payload
 *
 * @param self This packet
 * @param factory Packet factory
 */
void AtPacketFactorySet(AtPacket self, AtPacketFactory factory)
    {
    if (self)
        self->packetFactory = factory;
    }

/**
 * Get packet factory used to create packets in payload
 *
 * @param self This packet
 * @return Packet factory
 */
AtPacketFactory AtPacketFactoryGet(AtPacket self)
    {
    if (self == NULL)
        return NULL;

    if (self->packetFactory)
        return self->packetFactory;

    return AtPacketFactoryDefaultFactory();
    }

/**
 * Check if packet is error packet
 *
 * @param self This packet
 *
 * @retval cAtTrue if packet is error
 * @retval cAtFalse if packet is good
 */
eBool AtPacketIsErrored(AtPacket self)
    {
    if (self)
        return self->isErrored;
    return cAtFalse;
    }

/**
 * @}
 */

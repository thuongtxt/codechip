/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPktAnalyzerInternal.h
 * 
 * Created Date: Dec 18, 2012
 *
 * Description : Packet analyzer abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPKTANALYZERINTERNAL_H_
#define _ATPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtList.h"
#include "AtPktAnalyzer.h"
#include "AtPacket.h"
#include "AtPacketFactory.h"

#include "../common/AtObjectInternal.h"
#include "../../generic/man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtPktAnalyzerBufferLength 2048

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtPktAnalyzerMethods
    {
    /* Public methods */
    void (*Init)(AtPktAnalyzer self);
    void (*Recapture)(AtPktAnalyzer self);
    void (*PatternSet)(AtPktAnalyzer self, uint8 *pattern);
    void (*PatternMaskSet)(AtPktAnalyzer self, uint32 patternMask);
    void (*PacketNumSet)(AtPktAnalyzer self, uint32 packetNums);
    void (*PacketLengthSet)(AtPktAnalyzer self, uint32 packetLength);
    void (*PacketDumpTypeSet)(AtPktAnalyzer self, eAtPktAnalyzerPktDumpType dumpType);
    void (*PppPacketModeSet)(AtPktAnalyzer self, eAtPktAnalyzerPppPktType packetMode);
    void (*PatternCompareModeSet)(AtPktAnalyzer self, eAtPktAnalyzerPatternCompareMode compareMode);
    void (*AnalyzeTxPackets)(AtPktAnalyzer self);
    void (*AnalyzeRxPackets)(AtPktAnalyzer self);
    AtList (*CaptureTxPackets)(AtPktAnalyzer self);
    AtList (*CaptureRxPackets)(AtPktAnalyzer self);
    eAtRet (*Start)(AtPktAnalyzer self);
    eAtRet (*Stop)(AtPktAnalyzer self);
    eBool (*IsStarted)(AtPktAnalyzer self);
    eAtRet (*ChannelSet)(AtPktAnalyzer self, AtChannel channel);

    AtPacketFactory (*PacketFactoryCreate)(AtPktAnalyzer self);

    /* Internal methods */
    AtPacket (*PacketCreate)(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction);

    /* For class packetType */
    eAtRet  (*PacketTypeSet)(AtPktAnalyzer self, uint8 pktType);
    }tAtPktAnalyzerMethods;

/*
 * AT device structure. All of concrete AT must implement all of interface
 * defined in implementation structure
 */
typedef struct tAtPktAnalyzer
    {
    tAtObject super;
    tAtPktAnalyzerMethods *methods;

    AtChannel channel; /* Channel to analyze packet */
    uint32 displayedPacketNum;
    uint8  buffer[cAtPktAnalyzerBufferLength];
    AtList packets;
    AtPacketFactory factory;
    }tAtPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer AtPktAnalyzerObjectInit(AtPktAnalyzer self, AtChannel channel);

uint8 *AtPktAnalyzerBufferGet(AtPktAnalyzer self, uint32 *bufferSize);

AtList AtPktAnalyzerPacketListGet(AtPktAnalyzer self);
void AtPktAnalyzerPacketFlush(AtPktAnalyzer self);
AtPacketFactory AtPktAnalyzerPacketFactory(AtPktAnalyzer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPKTANALYZERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtUdpPacketInternal.h
 * 
 * Created Date: Sep 14, 2016
 *
 * Description : UDP packet class description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUDPPACKETINTERNAL_H_
#define _ATUDPPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtUdpPacket.h"
#include "AtPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtUdpPacket
    {
    tAtPacket super;

    /* Private */
    }tAtUdpPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtUdpPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATUDPPACKETINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtModulePktAnalyzerInternal.h
 * 
 * Created Date: Apr 26, 2013
 *
 * Description : Packet analyzer module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPKTANALYZERINTERNAL_H_
#define _ATMODULEPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtModuleInternal.h"
#include "AtModulePktAnalyzer.h"
#include "AtPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePktAnalyzerMethods
    {
    AtPktAnalyzer (*EthPortPktAnalyzerCreate)(AtModulePktAnalyzer self, AtEthPort port);
    AtPktAnalyzer (*EthFlowPktAnalyzerCreate)(AtModulePktAnalyzer self, AtEthFlow flow);
    AtPktAnalyzer (*PppLinkPktAnalyzerCreate)(AtModulePktAnalyzer self, AtPppLink ppplink);
    AtPktAnalyzer (*HdlcChannelPktAnalyzerCreate)(AtModulePktAnalyzer self, AtHdlcChannel channel);

    /* Internal methods */
    AtPktAnalyzer (*DefaultAnalyzerCreate)(AtModulePktAnalyzer self);
    }tAtModulePktAnalyzerMethods;

typedef struct tAtModulePktAnalyzer
    {
    tAtModule super;
    const tAtModulePktAnalyzerMethods *methods;

    /* Private data */
    AtPktAnalyzer packetAnalyzer;
    }tAtModulePktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer AtModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPKTANALYZERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtIpV4Packet.c
 *
 * Created Date: Apr 27, 2013
 *
 * Description : IPv4 packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtUdpPacket.h"
#include "AtPacketInternal.h"
#include "AtIpV4PacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cProtocolUdp 0x11
#define cIpLength    4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtIpV4Packet)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIpV4PacketMethods m_methods;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
	{
	return sizeof(tAtIpV4Packet);
	}

static uint8 Ihl(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[0] & cBit3_0;
    }

static uint32 HeaderLength(AtPacket self)
    {
    return (Ihl(self) > 5) ? 24 : 20;
    }

static uint8 Version(AtPacket self)
    {
    return (AtPacketDataBuffer(self, NULL)[0] & cBit7_4) >> 4;
    }

static uint8 Dscp(AtPacket self)
    {
    return (AtPacketDataBuffer(self, NULL)[1] & cBit7_2) >> 2;
    }

static uint8 Ecn(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[1] & cBit1_0;
    }

static uint16 TotalLength(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);
    return (uint16)((buffer[2] << 8) | buffer[3]);
    }

static uint16 Identification(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);
    return (uint16)((buffer[4] << 8) | buffer[5]);
    }

static uint8 Flags(AtPacket self)
    {
    return (AtPacketDataBuffer(self, NULL)[6] & cBit7_5) >> 5;
    }

static uint16 FragmentOffset(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);
    return (uint16)(((buffer[6] & cBit4_0) << 8) | buffer[7]);
    }

static uint8 TimeToLive(AtPacket self)
    {
    return AtPacketDataBuffer(self, NULL)[8];
    }

static uint8 Protocol(AtIpV4Packet self)
    {
    return AtPacketDataBuffer((AtPacket)self, NULL)[9];
    }

static uint16 HeaderChecksum(AtPacket self)
    {
    uint8 *buffer = AtPacketDataBuffer(self, NULL);
    return (uint16)((buffer[10] << 8) | buffer[11]);
    }

static uint8 *SourceIp(AtPacket self)
    {
    return &(AtPacketDataBuffer(self, NULL)[12]);
    }

static uint8 *DestIp(AtPacket self)
    {
    return &(AtPacketDataBuffer(self, NULL)[12 + cIpLength]);
    }

static uint32 Options(AtPacket self)
    {
    uint8 *buffer = &(AtPacketDataBuffer(self, NULL)[12 + cIpLength + cIpLength]);
    return AtPktUtilDwordGet(buffer);
    }

static const char *ProtocolName(uint8 protocol)
    {
    if (protocol == cProtocolUdp) return "UDP";

    /* Add more here */

    return NULL;
    }

static void DisplayProtocol(AtPacket self, uint8 level, const char *description)
    {
    uint8 protocolId = mMethodsGet(mThis(self))->Protocol(mThis(self));
    const char *protocolName = ProtocolName(protocolId);
    if (protocolName)
        AtPacketPrintStringAttribute(self, description, protocolName, level);
    else
        AtPacketPrintHexAttribute(self, description, protocolId, level);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    if (AtPacketLengthGet(self) < mMethodsGet(self)->HeaderLength(self))
        return;

    /* Scalar attributes */
    AtPacketPrintAttribute   (self, "Version",         Version(self),        level);
    AtPacketPrintAttribute   (self, "IHL",             Ihl(self),            level);
    AtPacketPrintHexAttribute(self, "DSCP",            Dscp(self),           level);
    AtPacketPrintAttribute   (self, "ECN",             Ecn(self),            level);
    AtPacketPrintAttribute   (self, "Total Length",    TotalLength(self),    level);
    AtPacketPrintHexAttribute(self, "Identification",  Identification(self), level);
    AtPacketPrintHexAttribute(self, "Flags",           Flags(self),          level);
    AtPacketPrintAttribute   (self, "Fragment Offset", FragmentOffset(self), level);
    AtPacketPrintAttribute   (self, "Time To Live",    TimeToLive(self),     level);
    AtIpPacketDisplayProtocol(self, level, "Protocol");
    AtPacketPrintHexAttribute(self, "Header Checksum", HeaderChecksum(self), level);
    if (Ihl(self) > 5)
        AtPacketPrintHexAttribute(self, "Options",         Options(self),        level);

    /* IP */
    AtPacketPrintIp(self, "Source IP",      SourceIp(self), cIpLength, level);
    AtPacketPrintIp(self, "Destination IP", DestIp(self),   cIpLength, level);
    }

static AtPacket CreatePacketByProtocolId(AtPacket self, uint8 protocolId)
    {
    uint8 *buffer = &(AtPacketDataBuffer(self, NULL)[mMethodsGet(self)->HeaderLength(self)]);
    uint32 pageloadLength = mMethodsGet(self)->PayloadLength(self);
    AtPacketFactory factory = AtPacketFactoryGet(self);

    if (protocolId == cProtocolUdp)
        return AtPacketFactoryUdpPacketCreate(factory, buffer, pageloadLength, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);

    return NULL;
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    AtPacket payload = CreatePacketByProtocolId(self, mMethodsGet(mThis(self))->Protocol(mThis(self)));
    AtPacketFactorySet(payload, AtPacketFactoryGet(self));
    if (payload == NULL)
    	{
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
    	}

    /* Display this payload */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDisplayAtLevel(payload, (uint8)(level + 1));
    AtObjectDelete((AtObject)payload);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static void MethodsInit(AtIpV4Packet self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Protocol);
        }

    mMethodsSet(self, &m_methods);
    }

AtPacket AtIpV4PacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtIpV4Packet)self);
    m_methodsInit = 1;

    return self;
    }

void AtIpPacketDisplayProtocol(AtPacket self, uint8 level, const char *description)
    {
    if (self)
        DisplayProtocol(self, level, description);
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New IPv4 packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtIpV4PacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return AtIpV4PacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }
/**
 * @}
 */

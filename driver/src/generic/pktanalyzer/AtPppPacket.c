/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtPppPacket.c
 *
 * Created Date: May 10, 2013
 *
 * Description : PPP Packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPacketInternal.h"
#include "AtIpV4Packet.h"
#include "AtIpV6Packet.h"
#include "AtLcpPacket.h"
#include "AtPppPacket.h"

/*--------------------------- Define -----------------------------------------*/
#define cProtocolIp  0x0021
#define cProtocolLcp 0xc021

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtPppPacket
    {
    tAtPacket super;

    /* Private */
    }tAtPppPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HeaderLength(AtPacket self)
    {
	AtUnused(self);
    return 2;
    }

static const char *ProtocolString(AtPacket self, uint16 protocolId)
    {
	AtUnused(self);
    if (protocolId == cProtocolIp)  return "IP";
    if (protocolId == cProtocolLcp) return "LCP";

    return NULL;
    }

static uint16 ProtocolId(AtPacket self)
    {
    uint32 length;
    uint8 *buffer = AtPacketDataBuffer(self, &length);

    if (length < mMethodsGet(self)->HeaderLength(self))
        return 0;

    return (uint16)((buffer[0] << 8) | buffer[1]);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    uint32 length = AtPacketLengthGet(self);
    const char *protocolString;

    if (length < mMethodsGet(self)->HeaderLength(self))
        return;

    protocolString = ProtocolString(self, ProtocolId(self));
    if (protocolString == NULL)
        AtPacketPrintHexAttribute(self, "Protocol", ProtocolId(self), level);
    else
        AtPacketPrintStringAttribute(self, "Protocol", protocolString, level);
    }

static AtPacket CreatePacketByProtocolId(AtPacket self, uint16 protocolId)
    {
    uint8 *buffer = &(AtPacketDataBuffer(self, NULL)[mMethodsGet(self)->HeaderLength(self)]);
    uint32 payloadLength = mMethodsGet(self)->PayloadLength(self);
    AtPacketFactory factory = AtPacketFactoryGet(self);

    if (protocolId == cProtocolIp)
        {
        uint8 version = (buffer[0] & cBit7_4) >> 4;
        if (version == 4) return AtPacketFactoryIpV4PacketCreate(factory, buffer, payloadLength, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);
        if (version == 6) return AtPacketFactoryIpV6PacketCreate(factory, buffer, payloadLength, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);
        }

    if (protocolId == cProtocolLcp)
        return AtPacketFactoryLcpPacketCreate(factory, buffer, payloadLength, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);

    return NULL;
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    AtPacket payload = CreatePacketByProtocolId(self, ProtocolId(self));
    AtPacketFactorySet(payload, AtPacketFactoryGet(self));

    if (payload == NULL)
    	{
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
    	}
    
    /* Display this payload */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDisplayAtLevel(payload, (uint8)(level + 1));
    AtObjectDelete((AtObject)payload);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, HeaderLength);
        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPppPacket);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New PPP packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtPppPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode);
    }
/**
 * @}
 */

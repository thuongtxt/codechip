/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtMplsPwPacket.c
 *
 * Created Date: Mar 28, 2014
 *
 * Description : MPLS PW packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtMplsPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cProtocolMef 0x88D8

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtMplsPwPacket *)((void*)self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtMplsPwPacket
    {
    tAtMplsPacket super;

    /* Private data */
    eAtPwPacketType type;
    eAtPwPacketRtpMode rtp;
    }tAtMplsPwPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DisplayPwHeader(AtPacket self, uint8 level)
    {
    uint32 payloadSize;
    uint8 *payload = AtPacketPayloadBuffer(self, &payloadSize);
    AtPacket pwPacket = AtPacketFactoryPwPacketCreate(AtPacketFactoryGet(self), payload, payloadSize, cAtPacketCacheModeNoCache, mThis(self)->type, mThis(self)->rtp, cAtPacketFactoryDirectionNone);
    AtPacketDisplayAtLevel(pwPacket, level);
    AtObjectDelete((AtObject)pwPacket);
    }

static eBool IsMefOverMpls(AtPacket self)
    {
    /* Check MEF Ethernet type at possible index */
    if ((AtPacketDataBuffer(self, NULL)[16] == 0x88) && (AtPacketDataBuffer(self, NULL)[17] == 0xD8))
        return cAtTrue;

    if ((AtPacketDataBuffer(self, NULL)[20] == 0x88) && (AtPacketDataBuffer(self, NULL)[21] == 0xD8))
        return cAtTrue;

    if ((AtPacketDataBuffer(self, NULL)[24] == 0x88) && (AtPacketDataBuffer(self, NULL)[25] == 0xD8))
        return cAtTrue;

    if ((AtPacketDataBuffer(self, NULL)[28] == 0x88) && (AtPacketDataBuffer(self, NULL)[29] == 0xD8))
        return cAtTrue;

    return cAtFalse;
    }

static AtPacket CreateMefPacket(AtPacket self)
    {
    uint8 *buffer = &(AtPacketDataBuffer(self, NULL)[mMethodsGet(self)->HeaderLength(self)]);
    uint32 payloadLength = mMethodsGet(self)->PayloadLength(self);
    AtPacketFactory factory = AtPacketFactoryGet(self);

    return AtPacketFactoryMefMplsPacketCreate(factory, buffer, payloadLength, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    AtPacket payload;

    if (!IsMefOverMpls(self))
        {
        if (AtPacketPayloadBuffer(self, NULL))
            DisplayPwHeader(self, level);
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
        }

    payload = CreateMefPacket(self);
    AtPacketFactorySet(payload, AtPacketFactoryGet(self));
    if (payload == NULL)
        {
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
        }

    /* Display this payload */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDisplayAtLevel(payload, (uint8)(level + 1));
    AtObjectDelete((AtObject)payload);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtMplsPwPacket);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtMplsPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    mThis(self)->type = type;
    mThis(self)->rtp  = rtp;

    return self;
    }

AtPacket AtMplsPwPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPacket, dataBuffer, length, cacheMode, type, rtp);
    }

void AtMplsPwPacketTypeSet(AtPacket self, eAtPwPacketType type)
    {
    if (self)
        mThis(self)->type = type;
    }

void AtMplsPwPacketRtpModeSet(AtPacket self, eAtPwPacketRtpMode rtp)
    {
    if (self)
        mThis(self)->rtp = rtp;
    }

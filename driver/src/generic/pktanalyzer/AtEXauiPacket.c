/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtEXauiPacket.c
 *
 * Created Date: May 10, 2016
 *
 * Description : XAUI packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEXauiPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtEXauiPacket)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BIP8(AtPacket self, uint8 *buffer)
    {
    uint32 bip8Offset;
    uint32 length;
    static const uint8 cBip8Length = 1;

    bip8Offset = mMethodsGet(self)->HeaderLength(self) +
                mMethodsGet(self)->PayloadLength(self);
    buffer = AtPacketDataBuffer(self, &length);
    if ((bip8Offset + cBip8Length) > length)
        return 0xcafecafe;

    return buffer[bip8Offset];
    }

static void TransmitDirectionDisplayHeader(AtPacket self, uint8 level, uint8 *buffer)
    {
    AtPacketPrintHexAttribute(self, "Preamble", buffer[0],  level);
    AtPacketPrintHexAttribute(self, "Channel", buffer[1],  level);
    AtPacketPrintHexAttribute(self, "Type", buffer[2],  level);
    AtPacketPrintHexAttribute(self, "BIP8", BIP8(self, buffer),  level);
    }

static void ReceivedDirectionDisplayHeader(AtPacket self, uint8 level, uint8 *buffer)
    {
    AtPacketPrintHexAttribute(self, "Preamble", buffer[0],  level);
    AtPacketPrintHexAttribute(self, "Channel", buffer[1],  level);
    AtPacketPrintHexAttribute(self, "BIP8", BIP8(self, buffer),  level);
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    uint32 length;
    uint8 *buffer = AtPacketDataBuffer(self, &length);
    uint8 direction = AtXauiPacketDirectionGet((AtEXauiPacket)self);

    if (length < mMethodsGet(self)->HeaderLength(self))
        return;

    if (direction == cAtPacketFactoryDirectionTx)
        TransmitDirectionDisplayHeader(self, level, buffer);
    if (direction == cAtPacketFactoryDirectionRx)
        ReceivedDirectionDisplayHeader(self, level, buffer);
    }

static void DisplayPayloadWithNoneEthFrame(AtPacket self, uint8 level)
    {
    uint32 length;
    uint8 *buffer = AtPacketDataBuffer(self, &length);
    uint32 payloadOffset = mMethodsGet(self)->HeaderLength(self);
    uint32 payloadLength;

    payloadLength = mMethodsGet(self)->PayloadLength(self);
    if (payloadLength == 0)
        return;

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDumpBuffer(self, &(buffer[payloadOffset]), payloadLength, (uint8)(level + 1));
    }

static AtPacket CreatePacketWithEthFrame(AtPacket self)
    {
    uint8 *buffer = &(AtPacketDataBuffer(self, NULL)[mMethodsGet(self)->HeaderLength(self)]);
    uint32 payloadLength = mMethodsGet(self)->PayloadLength(self);
    AtPacketFactory factory = AtPacketFactoryGet(self);
    return AtPacketFactoryEthPacketCreate(factory, buffer, payloadLength, cAtPacketCacheModeCacheData, cAtPacketFactoryDirectionNone);
    }

static void DisplayPayloadWithEthFrame(AtPacket self, uint8 level)
    {
    AtPacket payload = CreatePacketWithEthFrame(self);
    AtPacketFactorySet(payload, AtPacketFactoryGet(self));

    if (payload == NULL)
        {
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
        }

    /* Display this payload */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDisplayAtLevel(payload, (uint8)(level + 1));
    AtObjectDelete((AtObject)payload);
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    uint32 portId = AtXauiPacketPortIdGet((AtEXauiPacket)self);

    if (portId == 1)
        DisplayPayloadWithNoneEthFrame(self, level);
    if (portId == 2)
        DisplayPayloadWithEthFrame(self, level);
    }

static uint32 HeaderLength(AtPacket self)
    {
    uint8 direction = AtXauiPacketDirectionGet((AtEXauiPacket)self);

    if (direction == cAtPacketFactoryDirectionTx) return 3;
    if (direction == cAtPacketFactoryDirectionRx) return 2;
    return 0;
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtEXauiPacket);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

AtPacket AtEXauiPacketObjectInit(AtPacket self,
                                uint8 *dataBuffer,
                                uint32 length,
                                eAtPacketCacheMode cacheMode,
                                eAtPacketFactoryDirection direction,
                                uint32 portId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Save private data */
    mThis(self)->direction = direction;
    mThis(self)->portId = portId;
    return self;
    }

uint8 AtXauiPacketDirectionGet(AtEXauiPacket self)
    {
    if (self)
        return self->direction;
    return cAtPacketFactoryDirectionNone;
    }

uint32 AtXauiPacketPortIdGet(AtEXauiPacket self)
    {
    if (self)
        return self->portId;
    return cInvalidUint32;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New XAUI packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 * @param direction @ref eAtPacketFactoryDirection "Direction"
 * @param portId Port ID
 *
 * @return New packet
 */
AtPacket AtEXauiPacketNew(uint8 *dataBuffer,
                          uint32 length,
                          eAtPacketCacheMode cacheMode,
                          eAtPacketFactoryDirection direction,
                          uint32 portId)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    return AtEXauiPacketObjectInit(newPacket, dataBuffer, length, cacheMode, direction, portId);
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : AtEthPacket.c
 *
 * Created Date: Apr 27, 2013
 *
 * Description : Ethernet packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthPacketInternal.h"

/* For payload */
#include "AtIpV4Packet.h"
#include "AtIpV6Packet.h"
#include "AtMplsPacket.h"
#include "AtMefPacket.h"
#include "AtPtpPacket.h"

/*--------------------------- Define -----------------------------------------*/
#define cSVlanTpid          0x88A8
#define cCVlanTpid          0x8100
#define cSVlanTpidNonStd    0x9100 /* An old non-standard 802.1ad QinQ */
#define cSVlanTpidNonStd2   0x9200 /* An old non-standard 802.1ad QinQ */

/* Ethernet type that can be parsed */
#define cEthTypeIpV4        0x0800
#define cEthTypeIpV6        0x86DD
#define cEthTypeMplsUnicast 0x8847
#define cEthTypeMef         0x88D8
#define cEthTypePtp         0x88F7

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtEthPacket)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtEthPacketMethods m_methods;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtEthPacket);
    }

static uint32 PayloadLength(AtPacket self)
    {
    uint32 payloadLength = m_AtPacketMethods->PayloadLength(self);

    if (!mMethodsGet(mThis(self))->HasCrc(mThis(self)))
        return payloadLength;

    if (payloadLength > cCrcLength)
        return payloadLength - cCrcLength;

    return 0;
    }

static uint8 *Preamble(AtEthPacket self, uint8 *length)
    {
    uint32 bufferSize;
    uint8 *dataBuffer = AtPacketDataBuffer((AtPacket)self, &bufferSize);
    return AtPktUtilEthPreambleDetect(dataBuffer, bufferSize, length);
    }

static uint8 DisplayPreamble(AtEthPacket self, uint8 level)
    {
    uint8 length;
    uint8 *preamble = Preamble(self, &length);

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Preamble: ");
    AtPacketDisplayBufferInHex((AtPacket)self, preamble, length, ".");
    AtPrintc(cSevNormal, "\r\n");

    return length;
    }

static void DisplayMacContent(AtEthPacket self, const char *name, uint8 *mac)
    {
    AtUnused(name);
    AtPacketDisplayBufferInHex((AtPacket)self, mac, cMacLength, ".");
    }

static void DisplayMac(AtEthPacket self, const char *name, uint8 *mac, uint8 level)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: ", name);
    mMethodsGet(self)->DisplayMacContent(self, name, mac);
    AtPrintc(cSevNormal, "\r\n");
    }

static const char *VlanTypeString(AtEthPacket self, uint16 tpid)
    {
    AtUnused(self);
    switch (tpid)
        {
        case cSVlanTpid:        return "S-VLAN";
        case cCVlanTpid:        return "C-VLAN";
        case cSVlanTpidNonStd:  return "S-VLAN (old 802.1ad)";
        case cSVlanTpidNonStd2: return "S-VLAN (old 802.1ad)";
        default:                return "Unknown";
        }
    }

static void DisplayVlanTag(AtEthPacket self, tAtVlanTag *vlanTag, uint8 level)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* %s: TPID = 0x%04x, PCP = %d, CFI = %d, VLAN-ID = %x\r\n",
             mMethodsGet(self)->VlanTypeString(self, vlanTag->tpid),
             vlanTag->tpid, vlanTag->pcp, vlanTag->cfi, vlanTag->vlanId);
    }

static eBool VlanTpidIsValid(AtEthPacket self, uint16 tpid)
    {
    AtUnused(self);
    switch (tpid)
        {
        case cSVlanTpid:        return cAtTrue;
        case cCVlanTpid:        return cAtTrue;
        case cSVlanTpidNonStd:  return cAtTrue;
        case cSVlanTpidNonStd2: return cAtTrue;
        default:                return cAtFalse;
        }
    }

static eBool ShouldCheckVlanTpid(AtEthPacket self, uint32 offset)
    {
    AtUnused(self);
    AtUnused(offset);
    return cAtTrue;
    }

static tAtVlanTag *VlagTagFromBuffer(AtEthPacket self, tAtVlanTag *vlanTag, uint8 *buffer, uint32 offset)
    {
    uint16 tpid;

    AtUnused(self);
    AtUnused(offset);

    /* TPID */
    tpid = (uint16)(((buffer[0]) << 8) | buffer[1]);
    if (mMethodsGet(self)->ShouldCheckVlanTpid(self, offset) && !mMethodsGet(self)->VlanTpidIsValid(self, tpid))
        return NULL;
    vlanTag->tpid = tpid;

    /* PCP, CFI */
    mFieldGet(buffer[2], cBit7_5, 5, uint8, &(vlanTag->pcp));
    vlanTag->cfi = (buffer[2] & cBit4) ? 1 : 0;

    /* ID */
    vlanTag->vlanId = (uint16)(((buffer[2] & cBit3_0) << 8) | buffer[3]);

    return vlanTag;
    }

static tAtVlanTag *VlanAtOffset(AtEthPacket self, uint32 offset, tAtVlanTag *vlanTag)
    {
    uint32 length, remainingBytes;

    AtPacketDataBuffer((AtPacket)self, &length);
    remainingBytes = length - offset;
    if (remainingBytes < cVlanLength)
        return NULL;

    return mMethodsGet(self)->VlagTagFromBuffer(self, vlanTag, &(AtPacketDataBuffer((AtPacket)self, &length)[offset]), offset);
    }

static uint16 FirstVlanOffset(AtEthPacket self)
    {
    return (uint16)(AtEthPacketPreambleLen(self) + (uint32)(cMacLength * 2));
    }

static void DisplayVlans(AtEthPacket self, uint8 level)
    {
    tAtVlanTag vlanTag, *pVlanTag;
    uint32 offset;

    offset = mMethodsGet(self)->FirstVlanOffset(self);
    while ((pVlanTag = VlanAtOffset(self, offset, &vlanTag)) != NULL)
        {
        DisplayVlanTag(self, pVlanTag, level);
        offset = (uint32)(offset + cVlanLength);
        }
    }

static const char *EthTypeString(uint16 ethType)
    {
    switch (ethType)
        {
        case cEthTypeIpV4:  return "IPv4";
        case cEthTypeIpV6:  return "IPv6";
        case 0x8847:        return "MPLS unicast";
        case 0x8848:        return "MPLS multicast";
        case 0x8863:        return "PPPoE Discovery Stage";
        case 0x8864:        return "PPPoE Session Stage";
        case 0x88CC:        return "LLDP";
        case cEthTypeMef:   return "CESoE (MEF8)";
        case cEthTypePtp:   return "PTP";

        /* Can be defined more... */

        default:            return NULL;
        }
    }

static uint16 EthTypeOffset(AtEthPacket self)
    {
    uint16 numVlans = AtEthPacketNumVlans(self);
    uint16 firstVlanOffset = mMethodsGet(self)->FirstVlanOffset(self);
    return (uint16)(firstVlanOffset + numVlans * cVlanLength);
    }

static uint16 EthTypeLength(AtEthPacket self)
    {
	uint8 *ethTypeBuf;

	if (!mMethodsGet(self)->HasEthTypeLength(self))
	    return 0x0;

	ethTypeBuf = &AtPacketDataBuffer((AtPacket)self, NULL)[EthTypeOffset(self)];
    return (uint16)((ethTypeBuf[0] << 8) | ethTypeBuf[1]);
    }

static eBool HasEthTypeLength(AtEthPacket self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool HasCrc(AtEthPacket self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void DisplayEthTypeLength(AtEthPacket self, uint8 level)
    {
    const uint16 cStartEthType = 1536;
    uint16 ethTypeLength;
    const char *typeString;

    if (!mMethodsGet(mThis(self))->HasEthTypeLength(mThis(self)))
        return;

    ethTypeLength = EthTypeLength(self);
    AtPacketPrintSpaces(level);
    if (ethTypeLength >= cStartEthType)
        {
        typeString = EthTypeString(ethTypeLength);
        AtPrintc(cSevNormal, "* Ethernet Type: 0x%04x", ethTypeLength);
        if (typeString)
            AtPrintc(cSevNormal, "[%s]", typeString);
        }
    else
        AtPrintc(cSevNormal, "* Ethernet Length: %d (bytes)", ethTypeLength);
    AtPrintc(cSevNormal, "\r\n");
    }

static uint32 Crc(AtEthPacket self)
    {
    uint32 crcOffset;
    uint32 length;
    uint8 *buffer;
    uint8 *crcBuf;
    AtPacket packet = (AtPacket)self;

    crcOffset = mMethodsGet(packet)->HeaderLength(packet) +
                mMethodsGet(packet)->PayloadLength(packet);
    buffer = AtPacketDataBuffer(packet, &length);
    if ((crcOffset + cCrcLength) > length)
        return 0xcafecafe;

    crcBuf = &(buffer[crcOffset]);
    return ((uint32)crcBuf[0] << 24) | ((uint32)crcBuf[1] << 16) | ((uint32)crcBuf[2] << 8) | crcBuf[3];
    }

static void DisplayCrc(AtEthPacket self, uint8 level)
    {
    if (mMethodsGet(self)->HasCrc(self))
        AtPacketPrintHexAttribute((AtPacket)self, "CRC", Crc(self), level);
    }

static AtPacket PayloadPacketCreate(AtPacket self, uint16 ethType, uint8 *data, uint32 length)
    {
    AtPacketFactory factory = AtPacketFactoryGet(self);

    if (ethType == cEthTypeIpV4)        return AtPacketFactoryIpV4PacketCreate(factory, data, length, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);
    if (ethType == cEthTypeIpV6)        return AtPacketFactoryIpV6PacketCreate(factory, data, length, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);
    if (ethType == cEthTypeMplsUnicast) return AtPacketFactoryMplsPacketCreate(factory, data, length, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);
    if (ethType == cEthTypeMef)         return AtPacketFactoryMefPacketCreate(factory, data, length, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);
    if (ethType == cEthTypePtp)         return AtPacketFactoryPtpPacketCreate(factory, data, length, cAtPacketCacheModeNoCache, cAtPacketFactoryDirectionNone);

    return NULL;
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    AtEthPacket packet = mThis(self);
    uint8 preambleLen;

    mMethodsGet(packet)->DisplayPreamble(packet, level);

    preambleLen = AtEthPacketPreambleLen(packet);
    DisplayMac(packet, "DMAC", &(AtPacketDataBuffer(self, NULL)[preambleLen]), level);
    DisplayMac(packet, "SMAC", &(AtPacketDataBuffer(self, NULL)[preambleLen + cMacLength]), level);
    mMethodsGet(packet)->DisplayVlans(packet, level);
    DisplayEthTypeLength(packet, level);
    DisplayCrc(packet, level);
    }

static uint32 HeaderLength(AtPacket self)
    {
    AtEthPacket packet = (AtEthPacket)self;

    if (mMethodsGet(packet)->HasEthTypeLength(packet))
        return EthTypeOffset(packet) + (uint32)cEthTypeLength;

    return EthTypeOffset(packet);
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    AtPacket payload = NULL;
    uint32 length, payloadLength;
    uint8 *buffer = AtPacketDataBuffer(self, &length);
    uint32 payloadOffset;

    /* Get payload length */
    payloadLength = mMethodsGet(self)->PayloadLength(self);
    if (payloadLength == 0)
        return;

    /* For unknown payload, just let its super do */
    payloadOffset = (uint16)(mMethodsGet(self)->HeaderLength(self));
    payload = PayloadPacketCreate(self, EthTypeLength(mThis(self)), &(buffer[payloadOffset]), payloadLength);
    AtPacketFactorySet(payload, AtPacketFactoryGet(self));
    if (payload == NULL)
        {
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
        }

    /* Display this payload */
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDisplayAtLevel(payload, (uint8)(level + 1));
    AtObjectDelete((AtObject)payload);
    }

static uint8 PreambleLen(AtEthPacket self)
    {
    uint8 length;
    Preamble(self, &length);
    return length;
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, PayloadLength);
        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        mMethodOverride(m_AtPacketOverride, HeaderLength);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static void MethodsInit(AtEthPacket self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, HasEthTypeLength);
        mMethodOverride(m_methods, HasCrc);
        mMethodOverride(m_methods, DisplayVlans);
        mMethodOverride(m_methods, DisplayPreamble);
        mMethodOverride(m_methods, PreambleLen);
        mMethodOverride(m_methods, FirstVlanOffset);
        mMethodOverride(m_methods, VlanAtOffset);
        mMethodOverride(m_methods, VlagTagFromBuffer);
        mMethodOverride(m_methods, ShouldCheckVlanTpid);
        mMethodOverride(m_methods, DisplayMacContent);
        mMethodOverride(m_methods, VlanTypeString);
        mMethodOverride(m_methods, VlanTpidIsValid);
        }

    mMethodsSet(self, &m_methods);
    }

AtPacket AtEthPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((AtEthPacket)self);
    m_methodsInit = 1;

    return self;
    }

uint8 AtEthPacketPreambleLen(AtEthPacket self)
    {
    if (self)
        return mMethodsGet(self)->PreambleLen(self);
    return 0;
    }

void AtEthPacketDisplayMac(AtEthPacket self, const char *name, uint8 *mac, uint8 level)
    {
    if (self)
        DisplayMac(self, name, mac, level);
    }

uint8 AtEthPacketNumVlans(AtEthPacket self)
    {
    tAtVlanTag vlanTag, *pVlanTag;
    uint32 offset;
    uint8 numVlans = 0;

    if (self == NULL)
        return 0;

    offset = mMethodsGet(self)->FirstVlanOffset(self);
    while ((pVlanTag = VlanAtOffset(self, offset, &vlanTag)) != NULL)
        {
        offset = (uint32)(offset + cVlanLength);
        numVlans++;
        }

    return numVlans;
    }

void AtEthPacketDisplayVlans(AtEthPacket self, uint8 level)
    {
    if (self)
        mMethodsGet(self)->DisplayVlans(self, level);
    }

void AtEthPacketDisplayEthTypeLength(AtEthPacket self, uint8 level)
    {
    if (self)
        DisplayEthTypeLength(self, level);
    }

void AtEthPacketDisplayCrc(AtEthPacket self, uint8 level)
    {
    if (self)
        DisplayCrc(self, level);
    }

uint16 AtEthPacketEthTypeLength(AtEthPacket self)
    {
    if (self)
        return EthTypeLength(self);
    return cInvalidUint16;
    }

/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/**
 * New Ethernet packet
 *
 * @param dataBuffer Buffer that hold packet content
 * @param length Packet length
 * @param cacheMode @ref eAtPacketCacheMode "Packet content caching modes"
 *
 * @return New packet
 */
AtPacket AtEthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return AtEthPacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtEthVlanTagInternal.h
 * 
 * Created Date: Oct 10, 2016
 *
 * Description : VLAN tag
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHVLANTAGINTERNAL_H_
#define _ATETHVLANTAGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthVlanTag.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtEthVlanTagToUint32(const tAtEthVlanTag *vlanTag);
tAtEthVlanTag *AtEthVlanTagFromUint32(uint32 vlanValue, tAtEthVlanTag *vlanTag);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHVLANTAGINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtChannelInternal.h
 *
 * Created Date: Aug 4, 2012
 *
 * Author      : namnn
 *
 * Description : AtChannel class representation
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELINTERNAL_H_
#define _ATCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "AtVcg.h"
#include "AtModuleXc.h"
#include "AtChannel.h"
#include "AtObjectInternal.h"
#include "AtList.h"
#include "AtPw.h"
#include "AtDebugger.h"
#include "AtFailureProfiler.h"

#include "../concate/binder/AtVcgBinder.h"
#include "../man/AtDeviceInternal.h"
#include "../diag/querier/common/AtQueriers.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
/* To fast accessing read/write function */
#define mChannelHwRead(self, address, moduleId)                                \
    AtChannelHwReadOnModule((AtChannel)self, (uint32)address, moduleId)
#define mChannelHwWrite(self, address, value, moduleId)                        \
    AtChannelHwWriteOnModule((AtChannel)self, (uint32)address, (uint32)value, moduleId)
#define mChannelHwLongRead(self, address, dataBuffer, bufferLen, moduleId)     \
    AtChannelHwLongReadOnModule((AtChannel)self, (uint32)address, dataBuffer, bufferLen, moduleId)
#define mChannelHwLongWrite(self, address, dataBuffer, bufferLen, moduleId)    \
    AtChannelHwLongWriteOnModule((AtChannel)self, address, dataBuffer, bufferLen, moduleId)

#define mChannelError(self_, ret_)                                             \
    do                                                                         \
        {                                                                      \
        uint32 saveRet__ = ret_;                                               \
        AtChannelLog((AtChannel)self_, cAtLogLevelCritical, AtSourceLocation, "Error code = %s\r\n", AtRet2String(saveRet__)); \
        return saveRet__;                                                      \
        }while(0)

#define mChannelLog(self_, level, messageNoNewLine)                            \
    do                                                                         \
        {                                                                      \
        AtChannelLog((AtChannel)self_, level, AtSourceLocation, "%s: %s\r\n", AtFunction, messageNoNewLine); \
        }while(0)

#define mChannelReturn(self_, ret_)                                            \
    do                                                                         \
        {                                                                      \
        uint32 saveRet__ = ret_;                                               \
        if (AtDriverShouldLog(mLogLevel(saveRet__)))                           \
            {                                                                  \
            AtChannelLog((AtChannel)self_, mLogLevel(saveRet__),               \
                         AtSourceLocation, "%s = %s\r\n", AtFunction, AtRet2String(saveRet__)); \
            }                                                                  \
                                                                               \
        return saveRet__;                                                      \
        }while(0)
        
#define mChannelSuccessAssert(self_, ret_)                                     \
    do                                                                         \
        {                                                                      \
        eAtRet saveRet___ = ret_;                                              \
        if (saveRet___ != cAtOk)                                               \
            mChannelError(self_, saveRet___);                                  \
        }while (0)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtChannelCache
    {
    uint8 txEnabled, rxEnabled;
    uint8 loopback;
    uint32 txForcedAlarms, rxForcedAlarms;
    uint32 interruptMask;
    uint8 timingMode;
    AtChannel timingSource;
    }tAtChannelCache;

typedef struct tAtChannelSimulationDb
    {
    uint32 interruptMask;
    uint8 loopbackMode;
    uint32 txForcedAlarms, rxForcedAlarms;
    uint8 enabled;
    }tAtChannelSimulationDb;

typedef struct tAtChannelMethods
    {
    AtModule (*ModuleGet)(AtChannel self);
    uint32 (*IdGet)(AtChannel self);
    const char *(*TypeString)(AtChannel self);
    const char *(*IdString)(AtChannel self);
    eAtRet (*HardwareCleanup)(AtChannel self);
    void (*HwResourceSerialize)(AtChannel self, void *hwResource);

    /* Enable/disable (all directions) */
    eAtRet (*Enable)(AtChannel self, eBool enable);
    eBool (*IsEnabled)(AtChannel self);
    eBool (*CanEnable)(AtChannel self, eBool enable);

    /* Enable/disable for each direction */
    eAtRet (*TxEnable)(AtChannel self, eBool enable);
    eBool (*TxIsEnabled)(AtChannel self);
    eAtRet (*RxEnable)(AtChannel self, eBool enable);
    eBool (*RxIsEnabled)(AtChannel self);
    eBool (*RxCanEnable)(AtChannel self, eBool enabled);
    eBool (*TxCanEnable)(AtChannel self, eBool enabled);

    /* Timing */
    eBool (*TimingModeIsSupported)(AtChannel self, eAtTimingMode timingMode);
    eAtRet (*TimingSet)(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource);
    eAtTimingMode (*TimingModeGet)(AtChannel self);
    AtChannel (*TimingSourceGet)(AtChannel self);
    eAtClockState (*ClockStateGet)(AtChannel self);
    uint8 (*HwClockStateGet)(AtChannel self);
    uint32 (*PwTimingRestore)(AtChannel self, AtPw pw);

    /* Defect */
    uint32 (*DefectGet)(AtChannel self);
    uint32 (*DefectHistoryGet)(AtChannel self);
    uint32 (*DefectHistoryClear)(AtChannel self);
    uint32 (*SpecificDefectInterruptClear)(AtChannel self, uint32 defectTypes);

    /* Alarms */
    uint32 (*AlarmGet)(AtChannel self);
    uint32 (*AlarmHistoryGet)(AtChannel self);
    uint32 (*AlarmHistoryClear)(AtChannel self);
    eAtRet (*EventListenerAdd)(AtChannel self, tAtChannelEventListener *listener);
    eBool  (*AlarmIsSupported)(AtChannel self, uint32 alarmType);
    void (*ListenerDidAdd)(AtChannel self, tAtChannelEventListener *listener, void *userData);

    /* Interrupt mask */
    uint32 (*SupportedInterruptMasks)(AtChannel self);
    eAtRet (*InterruptMaskSet)(AtChannel self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtChannel self);

    eAtRet (*InterruptEnable)(AtChannel self);
    eAtRet (*InterruptDisable)(AtChannel self);
    eBool (*InterruptIsEnabled)(AtChannel self);
    uint32 (*InterruptProcess)(AtChannel self, uint32 offset);

    /* TX alarm forcing */
    eAtRet (*TxAlarmForce)(AtChannel self, uint32 alarmType);
    eAtRet (*TxAlarmUnForce)(AtChannel self, uint32 alarmType);
    uint32 (*TxForcedAlarmGet)(AtChannel self);
    uint32 (*TxForcibleAlarmsGet)(AtChannel self);
    eAtRet (*TxSquelch)(AtChannel self, eBool squelched);

    /* RX alarm forcing */
    eAtRet (*RxAlarmForce)(AtChannel self, uint32 alarmType);
    eAtRet (*RxAlarmUnForce)(AtChannel self, uint32 alarmType);
    uint32 (*RxForcedAlarmGet)(AtChannel self);
    uint32 (*RxForcibleAlarmsGet)(AtChannel self);

    /* Tx error counter forcing */
    eAtRet (*TxErrorForce)(AtChannel self, uint32 errorType);
    eAtRet (*TxErrorUnForce)(AtChannel self, uint32 errorType);
    uint32 (*TxForcedErrorGet)(AtChannel self);
    uint32 (*TxForcableErrorsGet)(AtChannel self);

    /* Loopback */
    eAtRet (*LoopbackSet)(AtChannel self, uint8 loopbackMode);
    uint8 (*LoopbackGet)(AtChannel self);
    eBool (*LoopbackIsSupported)(AtChannel self, uint8 loopbackMode);
    const char *(*LoopbackMode2String)(AtChannel self, uint8 loopbackMode);

    /* Performance counters */
    uint32 (*CounterGet)(AtChannel self, uint16 counterType);
    uint32 (*CounterClear)(AtChannel self, uint16 counterType);
    eAtRet (*AllCountersGet)(AtChannel self, void *pAllCounters);
    eAtRet (*AllCountersClear)(AtChannel self, void *pAllCounters);
    eBool (*CounterIsSupported)(AtChannel self, uint16 counterType);
    eAtRet (*AllCountersLatchAndClear)(AtChannel self, eBool clear);

    /* Full configuration */
    eAtRet (*AllConfigSet)(AtChannel self, void *pAllConfig);
    eAtRet (*AllConfigGet)(AtChannel self, void *pAllConfig);

    /*
     * Internal methods
     */
    eAtRet (*Init)(AtChannel self);        /* Initialize to set configure default */
    uint32 (*HwIdGet)(AtChannel self);     /* Convert software ID to a flat Hardware ID */
    AtIpCore (*IpCoreGet)(AtChannel self, eAtModule moduleId); /* Get IP core that this channel belongs to */

    /* To access registers. For one channel, its module is just a logical one.
     * A logical module can contain more than one physical modules. Not all physical
     * modules belong to one IP core. Concrete channel of concrete product may
     * know what it IP core of the physical module it belong to.
     * This is the reason why the module parameter is input.
     *
     * For long register, its size is different among concrete product, so the
     * bufferLen parameter is to specify register length
     */
    uint32 (*HwReadOnModule)(AtChannel self, uint32 localAddress, eAtModule module);
    void (*HwWriteOnModule)(AtChannel self, uint32 localAddress, uint32 value, eAtModule module);
    uint16 (*HwLongReadOnModule)(AtChannel self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, eAtModule module);
    uint16 (*HwLongWriteOnModule)(AtChannel self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, eAtModule module);

    /* Binding to Encapsulation */
    eAtRet (*BindToEncapChannel)(AtChannel self, AtEncapChannel encapChannel);
    AtEncapChannel (*BoundEncapChannelGet)(AtChannel self);
    uint8 (*NumBlocksNeedToBindToEncapChannel)(AtChannel self, AtEncapChannel encapChannel);
    uint32 (*BoundEncapHwIdGet)(AtChannel self);

    /* Some products need to allocate encap channel ID for circuit */
    uint32 (*EncapHwIdAllocate)(AtChannel self);

    /* Even when the physical is bound to encap channel. This connection can be
     * enabled/disabled by application. This internal function is to help concrete
     * physical interface disable its connection to encap channel, but binding
     * information is still remained */
    eAtRet (*EncapConnectionEnable)(AtChannel self, eBool enable);
    eAtRet (*TxEncapConnectionEnable)(AtChannel self, eBool enable);
    eBool (*TxEncapConnectionIsEnabled)(AtChannel self);
    eAtRet (*RxEncapConnectionEnable)(AtChannel self, eBool enable);
    eBool (*RxEncapConnectionIsEnabled)(AtChannel self);

    /* For debugging purpose, to show all of information of a channel */
    eAtRet (*Debug)(AtChannel self);
    void (*StatusClear)(AtChannel self);
    AtDebugger (*DebuggerCreate)(AtChannel self);
    AtDebugger (*DebuggerObjectCreate)(AtChannel self);
    void (*DebuggerEntriesFill)(AtChannel self, AtDebugger debugger);
    AtQuerier (*QuerierGet)(AtChannel self);

    /* PRBS engine */
    AtPrbsEngine (*PrbsEngineGet)(AtChannel self);
    void (*PrbsEngineSet)(AtChannel self, AtPrbsEngine engine);
    eBool (*PrbsEngineIsCreated)(AtChannel self);

    /* Some counters need to be read only, this methods is used to read value of these counters */
    uint32 (*ReadOnlyCntGet)(AtChannel self, uint32 counterType);

    /* Transfer all of reference */
    eAtRet (*ReferenceTransfer)(AtChannel self, AtChannel dest);
    uint8 (*PppBlockRange)(AtChannel self);
    uint8 (*MlpppBlockRange)(AtChannel self);

    /* Enable/disable traffic on each direction */
    eAtRet (*TxTrafficEnable)(AtChannel self, eBool enable);
    eBool (*TxTrafficIsEnabled)(AtChannel self);
    eAtRet (*RxTrafficEnable)(AtChannel self, eBool enable);
    eBool (*RxTrafficIsEnabled)(AtChannel self);

    /* Binding to Pseudowire */
    eAtRet (*CanBindToPseudowire)(AtChannel self, AtPw pseudowire);
    eAtRet (*DidBindToPseudowire)(AtChannel self, AtPw pseudowire);
    eAtRet (*WillBindToPseudowire)(AtChannel self, AtPw pseudowire);
    eAtRet (*BindToPseudowire)(AtChannel self, AtPw pseudowire);
    AtPw (*BoundPseudowireGet)(AtChannel self);
    uint32 (*BoundPwHwIdGet)(AtChannel self);
    uint32 (*DataRateInBytesPer125Us)(AtChannel self);
    uint32 (*DataRateInBytesPerMs)(AtChannel self);
    eBool (*ShouldDeletePwOnCleanup)(AtChannel self);

    eAtRet (*WarmRestore)(AtChannel self);

    /* XC method */
    eAtRet (*SourceSet)(AtChannel self, AtChannel source);
    AtChannel (*SourceGet)(AtChannel self);
    AtList (*DestinationChannels)(AtChannel self);
    eAtRet(*DestinationAdd)(AtChannel self, AtChannel destination);
    eAtRet(*DestinationRemove)(AtChannel self, AtChannel destination);

    /* To work with XC loopback */
    eBool (*OnlySupportXcLoopback)(AtChannel self);
    eBool (*XcLoopbackModeIsSupported)(AtChannel self, uint8 loopbackMode);
    eAtRet (*XcLoopbackSet)(AtChannel self, uint8 loopbackMode);
    uint8 (*XcLoopbackGet)(AtChannel self);
    uint8 *(*CachedLoopbackMode)(AtChannel self);
    AtCrossConnect (*XcEngine)(AtChannel self);

    /* For standby driver */
    void *(*CacheGet)(AtChannel self);
    uint32 (*CacheSize)(AtChannel self);
    void **(*CacheMemoryAddress)(AtChannel self);
    eBool (*CacheIsEnabled)(AtChannel self);
    eBool (*ShouldPreventReconfigure)(AtChannel self);

    /* For debugging purpose and should not merge to main branch for performance */
    eBool (*CanAccessRegister)(AtChannel self, eAtModule moduleId, uint32 address);
    void (*WillAccessRegister)(AtChannel self, eAtModule moduleId, uint32 address);

    /* Work with Surveillance */
    AtSurEngine (*SurEngineGet)(AtChannel self);
    AtSurEngine (*SurEngineObjectCreate)(AtChannel self);
    void (*SurEngineObjectDelete)(AtChannel self, AtSurEngine engine);
    AtSurEngine *(*SurEngineAddress)(AtChannel self);

    /* Note, the following should be AtAttController, but this class is only for
     * internally used. Not to depend directly on this symbol */
    AtObjectAny (*AttController)(AtChannel self);

    /* For VCG */
    AtVcgBinder (*VcgBinderCreate)(AtChannel self);
    eBool (*CanJoinVcg)(AtChannel self, AtConcateGroup vcg);
    eAtRet (*BindToSourceGroup)(AtChannel self, AtConcateGroup group);
    eAtRet (*BindToSinkGroup)(AtChannel self, AtConcateGroup group);
    uint32 (*BoundConcateHwIdGet)(AtChannel self);

    /* To handle hardware resource */
    eAtRet (*HwResourceAllocate)(AtChannel self);
    eAtRet (*HwResourceDeallocate)(AtChannel self);
    eAtRet (*AllServicesDestroy)(AtChannel self);

    /* To protect running services */
    eBool (*ServiceIsRunning)(AtChannel self);

    /* Queue manage */
    eAtRet (*QueueEnable)(AtChannel self, eBool enable);
    eBool (*QueueIsEnabled)(AtChannel self);

    /* Simulation */
    uint32 (*SimulationDatabaseSize)(AtChannel self);
    void **(*SimulationDatabaseAddress)(AtChannel self);
    eBool (*HwIsReady)(AtChannel self);

    /* Clock state translate */
    eAtClockState (*HwClockStateTranslate)(AtChannel self, uint8 hwClockState);

    /* Internal Methods */
    AtFailureProfiler (*FailureProfilerObjectCreate)(AtChannel self);
    }tAtChannelMethods;

/* AT channel class */
typedef struct tAtChannel
    {
    tAtObject super;
    const tAtChannelMethods *methods;

    /* Private data. Note, it is tempted to remove the device reference to save
     * memory because it can be retrieved by using module reference. But do not
     * do that, there are a lot of places in channel context need device, if
     * lookup always takes place, context switching will take so much CPU
     * especially for polling tasks such as PM and BER monitoring */
    AtModule module;
    AtDevice device;
    uint32 channelId; /* Logical SW ID */
    AtHal hal;        /* Cache HAL object for fast accessing register */

    /* Listeners */
    AtList allEventListeners;
    AtOsalMutex listenerListMutex; /* Locker to protect list of event listener */
    uint32 listenedDefects; /* Defect database reported by listeners. */
    uint32 listenedCounts; /* Number of listener report time.*/
    AtList propertyListeners;

    /* Binding information */
    AtEncapChannel boundEncapChannel;
    AtPw boundPw;
    AtVcgBinder vcgBinder;

    /* HW resource, channel sometimes need to save some HW resource */
    void* hwResource;

    /* Log is enabled */
    eBool logIsEnabled;
    AtFailureProfiler failureProfiler;

    /* is tx squelched */
    eBool isTxSquelched;
    }tAtChannel;

typedef struct tAtChannelEventListenerWrapper * AtChannelEventListener;

typedef struct tAtChannelEventListenerWrapper
    {
    tAtObject super;
    tAtChannelEventListener listener;
    void *userData;
    }tAtChannelEventListenerWrapper;

typedef enum eAtChannelPropertyId
    {
    cAtChannelPropertyIdStart,
    cAtChannelPropertyIdLoopback = cAtChannelPropertyIdStart
    }eAtChannelPropertyId;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtChannel AtChannelObjectInit(AtChannel self, uint32 channelId, AtModule module);
void AtChannelAllAlarmListenersCall(AtChannel self, uint32 changedAlarms, uint32 currentStatus);
void AtChannelAllFailureListenersCall(AtChannel self, uint32 changedFailures, uint32 currentStatus);
void AtChannelOamReceivedNotify(AtChannel self, uint8 *data, uint32 length);
eBool AtChannelHwIsReady(AtChannel self);

eAtRet AtChannelCanBindToPseudowire(AtChannel self, AtPw pseudowire);
eAtRet AtChannelBindToPseudowire(AtChannel self, AtPw pseudowire);
uint32 AtChannelBoundPwHwIdGet(AtChannel self);

/* Access private data */
eAtRet AtChannelBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel);
AtEncapChannel AtChannelBoundEncapChannel(AtChannel self);
void AtChannelBoundEncapChannelSet(AtChannel self, AtEncapChannel encapChannel);
eAtRet AtChannelReferenceTransfer(AtChannel self, AtChannel dest);
uint8 AtChannelPppBlockRange(AtChannel self);
eAtRet AtChannelBoundPwSet(AtChannel self, AtPw pw);
uint32 AtChannelHwIdGet(AtChannel self);
uint8 AtChannelMlpppBlockRange(AtChannel self);
void AtChannelIdSet(AtChannel self, uint32 channelId);
eBool AtChannelPrbsEngineIsCreated(AtChannel self);

/* Internal methods */
eAtRet AtChannelTxTrafficEnable(AtChannel self, eBool enable);
eBool AtChannelTxTrafficIsEnabled(AtChannel self);
eAtRet AtChannelRxTrafficEnable(AtChannel self, eBool enable);
eBool AtChannelRxTrafficIsEnabled(AtChannel self);
uint32 AtChannelDataRateInBytesPer125Us(AtChannel self);
uint32 AtChannelDataRateInBytesPerMs(AtChannel self);
eBool AtChannelRxCanEnable(AtChannel self, eBool enabled);
eBool AtChannelTxCanEnable(AtChannel self, eBool enabled);
eBool AtChannelServiceIsRunning(AtChannel self);
void *AtChannelSimulationDatabaseGet(AtChannel self);
eBool AtChannelCanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address);
uint32 AtChannelDefectHistoryGet(AtChannel self);
uint32 AtChannelDefectHistoryClear(AtChannel self);
eAtClockState AtChannelHwClockStateTranslate(AtChannel self, uint8 hwState);
eAtRet AtChannelHardwareCleanup(AtChannel self);
eAtRet AtChannelAllServicesDestroy(AtChannel self);
eAtRet AtChannelSurEngineInterruptDisable(AtChannel self);
eAtRet AtChannelTxSquelch(AtChannel self, eBool squelched);
eAtRet AtChannelTxIsSquelched(AtChannel self);
uint8 AtChannelHwClockStateGet(AtChannel self);

/* Interrupt. */
uint32 AtChannelInterruptProcess(AtChannel self, uint32 offset); /* Return current software bitmap defects. */
uint32 AtChannelSupportedInterruptMasks(AtChannel self);
eBool AtChannelInterruptMaskCanSet(AtChannel self, uint32 defectMask, uint32 enableMask);

/* Channel has alarm forwarding from upper layer. */
eBool AtChannelHasAlarmForwarding(AtChannel self);

uint32 AtChannelReadOnlyCntGet(AtChannel self, uint32 counterType);
void AtChannelLog(AtChannel self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...) AtAttributePrintf(5, 6);

/* Access hardware */
uint32 AtChannelHwReadOnModule(AtChannel self, uint32 address, eAtModule moduleId);
void AtChannelHwWriteOnModule(AtChannel self, uint32 localAddress, uint32 value, eAtModule module);
uint16 AtChannelHwLongReadOnModule(AtChannel self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, eAtModule module);
uint16 AtChannelHwLongWriteOnModule(AtChannel self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, eAtModule module);
eBool AtChannelDeviceInWarmRestore(AtChannel self);
eAtRet AtChannelWarmRestore(AtChannel self);
uint32 AtChannelPwTimingRestore(AtChannel self, AtPw pw);

eBool AtChannelAccessible(AtChannel self);
eBool AtChannelInAccessible(AtChannel self);
eBool AtChannelShouldPreventReconfigure(AtChannel self);

void AtChannelPrbsEngineSet(AtChannel self, AtPrbsEngine engine);

/* To manage XC database */
eAtRet AtChannelSourceSet(AtChannel self, AtChannel source);
AtChannel AtChannelSourceGet(AtChannel self);
AtList AtChannelDestinationChannels(AtChannel self);
eAtRet AtChannelDestinationAdd(AtChannel self, AtChannel destination);
eAtRet AtChannelDestinationRemove(AtChannel self, AtChannel destination);
char *AtChannelIdDescriptionBuild(AtChannel self);
eBool AtChannelHasCrossConnect(AtChannel self);

/* For standby driver */
void *AtChannelCacheGet(AtChannel self);

/* For encapsulation */
uint32 AtChannelEncapHwIdAllocate(AtChannel self);
uint32 AtChannelBoundEncapHwIdGet(AtChannel self);

/* To manage BLock resource */
eAtRet AtChannelHwResourceAllocate(AtChannel self);
eAtRet AtChannelHwResourceDeallocate(AtChannel self);
void *AtChannelHwResourceGet(AtChannel self);
void AtChannelHwResourceCache(AtChannel self, void* hwResource);

void AtChannelBoundPwObjectDelete(AtChannel self);

/* For Queue manages */
eAtRet AtChannelQueueEnable(AtChannel self, eBool enable);
eBool AtChannelQueueIsEnabled(AtChannel self);

/* For VCG */
eBool AtChannelCanJoinVcg(AtChannel self, AtConcateGroup vcg);
eAtRet AtChannelBindToSourceGroup(AtChannel self, AtConcateGroup group);
eAtRet AtChannelBindToSinkGroup(AtChannel self, AtConcateGroup group);
uint32 AtChannelBoundConcateHwIdGet(AtChannel self);

/* Work with testbench */
eBool AtChannelShouldFullyInit(AtChannel self);

/* Debug */
void AtChannelDebuggerEntriesFill(AtChannel self, AtDebugger debugger);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtCommon.c
 *
 * Created Date: Jan 23, 2013
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include  "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool AtLedStateIsValid(eAtLedState ledState)
    {
    if ((ledState == cAtLedStateOn)  ||
        (ledState == cAtLedStateOff) ||
        (ledState == cAtLedStateBlink))
        return cAtTrue;

    return cAtFalse;
    }

eBool AtLoopbackModeIsValid(eAtLoopbackMode loopbackMode)
    {
    switch (loopbackMode)
        {
        case cAtLoopbackModeRelease: return cAtTrue;
        case cAtLoopbackModeLocal  : return cAtTrue;
        case cAtLoopbackModeRemote : return cAtTrue;
        case cAtLoopbackModeDual   : return cAtTrue;
        default: return cAtFalse;
        }
    }

const char *AtLoopbackMode2Str(eAtLoopbackMode loopbackMode)
    {
    switch (loopbackMode)
        {
        case cAtLoopbackModeRelease: return "release";
        case cAtLoopbackModeLocal  : return "local";
        case cAtLoopbackModeRemote : return "remote";
        case cAtLoopbackModeDual   : return "dual";
        default: return NULL;
        }
    }

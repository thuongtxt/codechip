/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtObject.c
 *
 * Created Date: Aug 24, 2012
 *
 * Author      : namnn
 *
 * Description : AtObject implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtObjectInternal.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tPropertyListenerWrapper
    {
    tAtObjectPropertyListener listener;
    void *userData;
    }tPropertyListenerWrapper;

typedef void (*PropertyCallback)(AtObject self, uint32 propertyId, AtSize value, void *userData);

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class*/
static tAtObjectMethods m_methods;
static char m_methodsInit = 0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void WillDelete(AtObject self)
    {
    /* Let's sub class do */
    AtUnused(self);
    }

static AtList PropertyListenerList(AtObject self)
    {
    AtList *address = mMethodsGet(self)->PropertyListenerListAddress(self);
    if (address)
        return *address;
    return NULL;
    }

static AtList PropertyListenerListCreateIfNeed(AtObject self)
    {
    AtList *address = mMethodsGet(self)->PropertyListenerListAddress(self);

    /* Concrete does not have memory to hold this list */
    if (address == NULL)
        return NULL;

    /* Concrete has memory but the list has not been created yet, create it */
    if (*address == NULL)
        *address = AtListCreate(0);

    return *address;
    }

static void AllPropertyListenersDelete(AtObject self)
    {
    AtList *listenersAddress;
    AtList listeners;

    listenersAddress = mMethodsGet(self)->PropertyListenerListAddress(self);
    if (listenersAddress == NULL)
        return;

    listeners = *listenersAddress;
    if (listeners)
        {
        while (AtListLengthGet(listeners) > 0)
            {
            tPropertyListenerWrapper *wrapper = (tPropertyListenerWrapper *)AtListObjectRemoveAtIndex(listeners, 0);
            if (wrapper->listener.ListenerWillRemove)
                wrapper->listener.ListenerWillRemove(self, wrapper->userData);
            AtOsalMemFree(wrapper);
            }
        }

    AtObjectDelete((AtObject)listeners);
    }

static void Delete(AtObject self)
    {
    AllPropertyListenersDelete(self);
    AtOsalMemFree(self);
    }

static AtObject Clone(AtObject self)
    {
	AtUnused(self);
    /* Let's sub class do */
    return NULL;
    }

static const char *DescriptionBuild(AtObject self, char *buffer, uint32 bufferSize)
    {
    AtSnprintf(buffer, bufferSize, "AtObject: %p", (void *)self);
    return buffer;
    }

static const char *ToString(AtObject self)
    {
    static char string[32];
    return mMethodsGet(self)->DescriptionBuild(self, string, sizeof(string));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtUnused(self);
    AtUnused(encoder);
    }

static void Deserialize(AtObject self, AtCoder decoder)
    {
    AtUnused(self);
    AtUnused(decoder);
    }

static tPropertyListenerWrapper *ListenerFind(AtObject self, const tAtObjectPropertyListener *listener, void *userData)
    {
    AtList listeners;
    AtIterator iterator;
    tPropertyListenerWrapper *wrapper;

    /* Concrete may not support */
    listeners = PropertyListenerList(self);
    if (listeners == NULL)
        return NULL;

    /* Find */
    iterator = AtListIteratorCreate(listeners);
    while ((wrapper = (tPropertyListenerWrapper *)AtIteratorNext(iterator)))
        {
        /* Found it */
        if ((AtOsalMemCmp(&(wrapper->listener), listener, sizeof(tAtObjectPropertyListener)) == 0) &&
            (wrapper->userData == userData))
            break;
        }
    AtObjectDelete((AtObject)iterator);

    return wrapper;
    }

static tPropertyListenerWrapper *PropertyListenerCreate(const tAtObjectPropertyListener *listener, void *userData)
    {
    uint32 memorySize = sizeof(tPropertyListenerWrapper);
    tPropertyListenerWrapper *wrapper;

    /* Allocate memory */
    wrapper = AtOsalMemAlloc(memorySize);
    AtOsalMemInit(wrapper, 0, memorySize);
    if (wrapper == NULL)
        return NULL;

    /* Copy data */
    AtOsalMemCpy(&(wrapper->listener), listener, sizeof(tAtObjectPropertyListener));
    wrapper->userData = userData;
    return wrapper;
    }

static eAtRet PropertyListenerAdd(AtObject self, const tAtObjectPropertyListener *listener, void *userData)
    {
    AtList listeners;
    tPropertyListenerWrapper *wrapper;

    if (listener == NULL)
        return cAtErrorNullPointer;

    /* Already exists */
    if (ListenerFind(self, listener, userData))
        return cAtOk;

    /* Concrete does not support */
    listeners = PropertyListenerListCreateIfNeed(self);
    if (listeners == NULL)
        return cAtErrorNotImplemented;

    /* Add */
    wrapper = PropertyListenerCreate(listener, userData);
    if (wrapper == NULL)
        return cAtErrorRsrcNoAvail;

    return AtListObjectAdd(listeners, (AtObject)wrapper);
    }

static eAtRet PropertyListenerRemove(AtObject self, const tAtObjectPropertyListener *listener, void *userData)
    {
    tPropertyListenerWrapper *wrapper;
    AtList listeners;

    /* Nothing to do when it has not been added yet */
    wrapper = ListenerFind(self, listener, userData);
    if (wrapper == NULL)
        return cAtOk;

    /* Concrete may not support */
    listeners = PropertyListenerList(self);
    if (listeners == NULL)
        return cAtOk;

    if (wrapper->listener.ListenerWillRemove)
        wrapper->listener.ListenerWillRemove(self, wrapper->userData);
    AtListObjectRemove(listeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);

    return cAtOk;
    }

static uint32 NumPropertyListenersGet(AtObject self)
    {
    AtList listeners = PropertyListenerList(self);
    return AtListLengthGet(listeners);
    }

static const tAtObjectPropertyListener *PropertyListenerAtIndex(AtObject self, uint32 listenerIndex, void **pUserData)
    {
    AtList listeners;
    tPropertyListenerWrapper *wrapper;

    /* Concrete may not support */
    listeners = PropertyListenerList(self);
    if (listeners == NULL)
        return NULL;

    /* It may not exist */
    wrapper = (tPropertyListenerWrapper *)AtListObjectGet(listeners, listenerIndex);
    if (wrapper == NULL)
        return NULL;

    /* It does exist */
    if (pUserData)
        *pUserData = &(wrapper->userData);

    return &(wrapper->listener);
    }

static PropertyCallback PropertyWillChangeCallback(tPropertyListenerWrapper *wrapper)
    {
    return wrapper->listener.PropertyWillChange;
    }

static PropertyCallback PropertyDidChangeCallback(tPropertyListenerWrapper *wrapper)
    {
    return wrapper->listener.PropertyDidChange;
    }

static void NotifyPropertyChangeChange(AtObject self, uint32 propertyId, AtSize value, PropertyCallback (*CallbackLookup)(tPropertyListenerWrapper *wrapper))
    {
    AtList listeners;
    AtIterator iterator;
    tPropertyListenerWrapper *wrapper;

    /* Concrete may not support */
    listeners = PropertyListenerList(self);
    if (listeners == NULL)
        return;

    iterator = AtListIteratorCreate(listeners);
    while ((wrapper = (tPropertyListenerWrapper *)AtIteratorNext(iterator)))
        {
        PropertyCallback callback = CallbackLookup(wrapper);
        if (callback)
            callback(self, propertyId, value, wrapper->userData);
        }
    AtObjectDelete((AtObject)iterator);
    }

static void NotifyPropertyWillChange(AtObject self, uint32 propertyId, AtSize value)
    {
    NotifyPropertyChangeChange(self, propertyId, value, PropertyWillChangeCallback);
    }

static void NotifyPropertyDidChange(AtObject self, uint32 propertyId, AtSize value)
    {
    NotifyPropertyChangeChange(self, propertyId, value, PropertyDidChangeCallback);
    }

static AtList *PropertyListenerListAddress(AtObject self)
    {
    AtUnused(self);
    return NULL;
    }

static void MethodsInit(AtObject self)
    {
    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsalMemInit(&m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, WillDelete);
        mMethodOverride(m_methods, Delete);
        mMethodOverride(m_methods, ToString);
        mMethodOverride(m_methods, Clone);
        mMethodOverride(m_methods, DescriptionBuild);
        mMethodOverride(m_methods, Serialize);
        mMethodOverride(m_methods, Deserialize);
        mMethodOverride(m_methods, PropertyListenerListAddress);
        }

    mMethodsSet(self, &m_methods);
    }

/*
 * Constructor
 */
AtObject AtObjectInit(AtObject self)
    {
    if (self == NULL)
        return NULL;

    /* Setup class */
    AtOsalMemInit(self, 0, sizeof(tAtObject));
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtObjectNotifyPropertyWillChange(AtObject self, uint32 propertyId, AtSize value)
    {
    if (self)
        NotifyPropertyWillChange(self, propertyId, value);
    }

void AtObjectNotifyPropertyDidChange(AtObject self, uint32 propertyId, AtSize value)
    {
    if (self)
        NotifyPropertyDidChange(self, propertyId, value);
    }

/**
 * Delete an object
 *
 * @param self This object
 */
void AtObjectDelete(AtObject self)
    {
    if (self)
        {
        mMethodsGet(self)->WillDelete(self);
        mMethodsGet(self)->Delete(self);
        }
    }

/**
 * To get description of an object
 *
 * @param self This object
 *
 * @return String that describes this object
 * @note This API may return static data, so it is not thread-safe.
 */
const char *AtObjectToString(AtObject self)
    {
    if (self)
        return mMethodsGet(self)->ToString(self);
    return NULL;
    }

/**
 * To clone an object
 *
 * @param self
 *
 * @return New object that has the same state as self
 */
AtObject AtObjectClone(AtObject self)
    {
    if (self)
        return mMethodsGet(self)->Clone(self);

    return NULL;
    }

/**
 * Serialize an object by specified encoder
 *
 * @param self This object
 * @param encoder Encoder
 */
void AtObjectSerialize(AtObject self, AtCoder encoder)
    {
    if (self)
        mMethodsGet(self)->Serialize(self, encoder);
    }

/**
 * De-serialize an object by a specified decoder
 *
 * @param self This object
 * @param decoder Decoder
 */
void AtObjectDeserialize(AtObject self, AtCoder decoder)
    {
    if (self)
        mMethodsGet(self)->Deserialize(self, decoder);
    }

/**
 * Add property listener
 *
 * @param self This object
 * @param listener Listener. Must not be NULL.
 * @param userData User data which will be input to callback when it is called
 *
 * @return AT return code
 *
 * @note input listener will be copied to object internal data structure.
 */
eAtRet AtObjectPropertyListenerAdd(AtObject self, const tAtObjectPropertyListener *listener, void *userData)
    {
    if (self)
        return PropertyListenerAdd(self, listener, userData);
    return cAtErrorObjectNotExist;
    }

/**
 * Remove property listener
 *
 * @param self This object
 * @param listener Listener to remove
 * @param userData User data.
 *
 * @return AT return code
 */
eAtRet AtObjectPropertyListenerRemove(AtObject self, const tAtObjectPropertyListener *listener, void *userData)
    {
    if (self)
        return PropertyListenerRemove(self, listener, userData);
    return cAtErrorObjectNotExist;
    }

/**
 * Get number of registered listeners
 *
 * @param self This object
 *
 * @return Number of registered listeners
 */
uint32 AtObjectNumPropertyListenersGet(AtObject self)
    {
    if (self)
        return NumPropertyListenersGet(self);
    return 0;
    }

/**
 * Get listener along with its user data at specified index
 *
 * @param self This object
 * @param listenerIndex Listener index
 * @param [out] pUserData User data associated to listener
 *
 * @return Listener at specified index or NULL if index is invalid
 */
const tAtObjectPropertyListener *AtObjectPropertyListenerAtIndex(AtObject self, uint32 listenerIndex, void **pUserData)
    {
    if (self)
        return PropertyListenerAtIndex(self, listenerIndex, pUserData);
    return NULL;
    }


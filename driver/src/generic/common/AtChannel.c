/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtChannel.c
 *
 * Created Date: Aug 4, 2012
 *
 * Author      : namnn
 *
 * Description : Default implementation of AT channel class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtChannelInternal.h"
#include "../../util/AtIteratorInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../man/AtModuleInternal.h"
#include "../xc/AtCrossConnectInternal.h"
#include "../concate/AtConcateMemberInternal.h"
#include "../sur/AtSurEngineInternal.h"
#include "../prbs/AtPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtMaxNumEventListener 32

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtChannel)self)
#define mChannelIsValid(self) (self ? cAtTrue : cAtFalse)
#define mInAccessible(self) AtChannelInAccessible(self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class*/
static tAtChannelMethods m_methods;
static char m_methodsInit = 0;

static const tAtObjectMethods *m_AtObjectMethods = NULL;
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtOk;
    }

static void CacheDelete(AtObject self)
    {
    void **cache = mMethodsGet(mThis(self))->CacheMemoryAddress(mThis(self));

    if (cache)
        {
        AtOsalMemFree(*cache);
        *cache = NULL;
        }
    }

static void SurEngineDelete(AtChannel self)
    {
    AtSurEngine *engineAdderss = mMethodsGet(self)->SurEngineAddress(self);

    if (engineAdderss == NULL)
        return;

    if (*engineAdderss)
        {
        mMethodsGet(mThis(self))->SurEngineObjectDelete(self, *engineAdderss);
        *engineAdderss = NULL;
        }
    }

static void BoundPwObjectDelete(AtChannel self)
    {
    AtPw pw = AtChannelBoundPwGet(self);
    if (pw == NULL)
        return;

    AtObjectDelete((AtObject)pw);
    self->boundPw = NULL;
    }

static void SimulationDatabaseDelete(AtChannel self)
    {
    void **database = mMethodsGet(mThis(self))->SimulationDatabaseAddress(mThis(self));

    if (database)
        {
        AtOsalMemFree(*database);
        *database = NULL;
        }
    }

static void HwResouceCacheDelete(AtObject self)
    {
    AtOsalMemFree(mThis(self)->hwResource);
    mThis(self)->hwResource = NULL;
    }

static void Delete(AtObject self)
    {
    AtChannel channel = (AtChannel)self;

    if (channel->allEventListeners)
        {
        AtOsalMutexLock(channel->listenerListMutex);
        AtListDeleteWithObjectHandler(channel->allEventListeners, AtObjectDelete);
        AtOsalMutexUnLock(channel->listenerListMutex);

        /* Delete private attribute */
        AtOsalMutexDestroy(channel->listenerListMutex);
        }

    AtObjectDelete((AtObject)channel->vcgBinder);
    channel->vcgBinder = NULL;
    CacheDelete(self);
    SurEngineDelete(channel);
    SimulationDatabaseDelete(channel);
    HwResouceCacheDelete(self);
    AtObjectDelete((AtObject)channel->failureProfiler);

    /* Call object delete */
    m_AtObjectMethods->Delete(self);
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtChannel channel = (AtChannel)self;
    AtDevice dev = AtChannelDeviceGet(channel);

    AtSprintf(description, "%s%s.%s", AtDeviceIdToString(dev), AtChannelTypeString(channel), AtChannelIdString(channel));

    return description;
    }

static AtModule ModuleGet(AtChannel self)
    {
    if (self)
        return self->module;
    return NULL;
    }

static uint32 IdGet(AtChannel self)
    {
    if (self)
        return self->channelId;
    return 0;
    }

static const char *IdString(AtChannel self)
    {
    static char idString[16];

    AtSprintf(idString, "%u", AtChannelIdGet(self) + 1);

    return idString;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "channel";
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtFalse;
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtFalse;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtChannelTxTrafficEnable(self, enable);
    ret |= AtChannelRxTrafficEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Default implementation is disable */
    return cAtFalse;
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
	AtUnused(timingMode);
	AtUnused(self);
    /* Let concrete class determine */
    return cAtFalse;
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
	AtUnused(timingSource);
	AtUnused(timingMode);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return unknown */
    return cAtTimingModeUnknown;
    }

static AtChannel TimingSourceGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return null */
    return NULL;
    }

static eAtClockState ClockStateGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return null */
    return cAtClockStateUnknown;
    }

static uint8 HwClockStateGet(AtChannel self)
    {
    AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static eAtClockState HwClockStateTranslate(AtChannel self, uint8 hwState)
    {
    AtUnused(self);
    AtUnused(hwState);
    return cAtClockStateUnknown;
    }

static uint32 DefectGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return no alarm */
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return no alarm */
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return no alarm */
    return 0;
    }

static uint32 SpecificDefectInterruptClear(AtChannel self, uint32 defectTypes)
    {
    /* Let sub-class do. Just return no alarm */
    AtUnused(self);
    AtUnused(defectTypes);
    return 0;
    }

static uint32 AlarmGet(AtChannel self)
    {
    /* All of products support defect. For alarms, just some of them support. So,
     * let concrete product handle alarm retrieving logic. */
    return mMethodsGet(self)->DefectGet(self);
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    /* All of products support defect. For alarms, just some of them support. So,
     * let concrete product handle alarm retrieving logic. */
    return mMethodsGet(self)->DefectHistoryGet(self);
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    /* All of products support defect. For alarms, just some of them support. So,
     * let concrete product handle alarm retrieving logic. */
    return mMethodsGet(self)->DefectHistoryClear(self);
    }

static AtChannelEventListener EventListenerObjectInit(AtChannelEventListener self, tAtChannelEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initialize memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtChannelEventListenerWrapper));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize private data */
    mMethodsGet(osal)->MemCpy(osal, &(self->listener), callbacks, sizeof(tAtChannelEventListener));
    self->userData = userData;

    return self;
    }

static AtChannelEventListener EventListenerNew(tAtChannelEventListener *callbacks, void *userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtChannelEventListener newListener = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtChannelEventListenerWrapper));

    if (newListener)
        return EventListenerObjectInit(newListener, callbacks, userData);

    return NULL;
    }

static eBool TwoListenersAreIdentical(AtChannelEventListener self, tAtChannelEventListener * listener)
    {
    if ((self->listener.AlarmChangeState != listener->AlarmChangeState) ||
        (self->listener.OamReceived != listener->OamReceived) ||
        (self->listener.AlarmChangeStateWithUserData != listener->AlarmChangeStateWithUserData))
        return cAtFalse;

    return cAtTrue;
    }

static eBool ListenerIsExistingInChannel(AtChannel self, tAtChannelEventListener *listener)
    {
    AtIterator iterator = AtListIteratorCreate(self->allEventListeners);
    AtChannelEventListener aListener;
    eBool isExisting = cAtFalse;

    /* For all event listeners */
    while ((aListener = (AtChannelEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (TwoListenersAreIdentical(aListener, listener))
            {
            isExisting = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)iterator);
    return isExisting;
    }

static eAtRet EventListenerAdd(AtChannel self, tAtChannelEventListener *listener)
    {
    return AtChannelEventListenerAddWithUserData(self, listener, NULL);
    }

static eAtRet EventListenerAddWithUserData(AtChannel self, tAtChannelEventListener *listener, void *userData)
    {
    eAtRet ret;
    AtChannelEventListener newListener;

    if (self->allEventListeners == NULL)
        {
        self->listenerListMutex = AtOsalMutexCreate();
        self->allEventListeners = AtListCreate(cAtMaxNumEventListener);
        }

    if (ListenerIsExistingInChannel(self, listener))
        return cAtOk;

    newListener = EventListenerNew(listener, userData);
    if (newListener == NULL)
        return cAtErrorRsrcNoAvail;

    /* Only add a new listener */
    AtOsalMutexLock(self->listenerListMutex);
    ret = AtListObjectAdd(self->allEventListeners, (AtObject)newListener);
    AtOsalMutexUnLock(self->listenerListMutex);

    /* To give concrete class chance to have more further handling on this operation */
    mMethodsGet(self)->ListenerDidAdd(self, listener, userData);

    return ret;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return none */
    return 0x0;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
	AtUnused(enableMask);
	AtUnused(defectMask);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return no mask */
    return 0;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    AtUnused(self);
    /* Let sub-class do. Just return no mask */
    return cAtFalse;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    AtUnused(self);
    AtUnused(offset);
    /* Let sub-class do. Just return no mask */
    return 0;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    /* Let sub-class do. Just return error */
    AtUnused(self);
    return (alarmType == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return no alarm */
    return 0;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
	AtUnused(alarmType);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    /* Let sub-class do. Just return error */
    AtUnused(self);
    return (alarmType == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return no alarm */
    return 0;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
	AtUnused(errorType);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
	AtUnused(errorType);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet XcLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtCrossConnect xc = mMethodsGet(self)->XcEngine(self);
    uint8 *cache = mMethodsGet(self)->CachedLoopbackMode(self);

    if (cache)
        *cache = loopbackMode;

    if ((loopbackMode == cAtLoopbackModeLocal) || (loopbackMode == cAtLoopbackModeRemote))
        return AtCrossConnectHwConnect(xc, self, self);

    if (loopbackMode == cAtLoopbackModeRelease)
        {
        AtChannel source = AtChannelSourceGet(self);
        if (source)
            return AtCrossConnectHwConnect(xc, source, self);
        return AtCrossConnectHwDisconnect(xc, self);
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if (mMethodsGet(self)->OnlySupportXcLoopback(self))
        {
        if (loopbackMode == AtChannelLoopbackGet(self))
            return cAtOk;

        return mMethodsGet(self)->XcLoopbackSet(self, loopbackMode);
        }

    return (loopbackMode == cAtLoopbackModeRelease) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 XcLoopbackGet(AtChannel self)
    {
    return *(mMethodsGet(self)->CachedLoopbackMode(self));
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (mMethodsGet(self)->OnlySupportXcLoopback(self))
        return mMethodsGet(self)->XcLoopbackGet(self);

    return cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (mMethodsGet(self)->OnlySupportXcLoopback(self))
        return mMethodsGet(self)->XcLoopbackModeIsSupported(self, loopbackMode);

    return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue: cAtFalse;
    }

static eAtRet AllCountersLatchAndClear(AtChannel self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return cAtOk;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
	AtUnused(counterType);
	AtUnused(self);
    /* Let sub-class do. Just return no error */
    return 0;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
	AtUnused(pAllCounters);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
	AtUnused(pAllCounters);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
	AtUnused(counterType);
	AtUnused(self);
    /* Let sub-class do. Just return no error */
    return 0;
    }

static eAtRet AllConfigSet(AtChannel self, void *pAllConfig)
    {
	AtUnused(pAllConfig);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet AllConfigGet(AtChannel self, void *pAllConfig)
    {
	AtUnused(pAllConfig);
	AtUnused(self);
    /* Let sub-class do. Just return error */
    return cAtErrorModeNotSupport;
    }

static eAtRet Debug(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void StatusClear(AtChannel self)
    {
	AtPrbsEngine engine = AtChannelPrbsEngineGet(self);
	if (engine)
		AtPrbsEngineStatusClear(engine);

    AtChannelAllCountersClear(self, NULL);
    /* AtChannelAlarmInterruptClear(self); */ /* Alias to defect interrupt. */
    AtChannelDefectInterruptClear(self);

    /* Should clear sticky of channel */
    AtPrintEnable(cAtFalse);
    AtChannelDebug(self);
    AtPrintEnable(cAtTrue);
    }

static uint32 HwIdGet(AtChannel self)
    {
    return self->channelId;
    }

static uint32 ReadOnlyCntGet(AtChannel self, uint32 counterType)
    {
    AtUnused(counterType);
    AtUnused(self);
    /* Let sub-class do */
    return 0;
    }

static AtIpCore IpCoreGet(AtChannel self, eAtModule moduleId)
    {
    /* Default implementation just ask channel's module for default core */
    AtDevice device = AtChannelDeviceGet(self);
    uint8 coreId = AtModuleDefaultCoreGet(AtDeviceModuleGet(device, moduleId));

    return AtDeviceIpCoreGet(device, coreId);
    }

static AtHal LookupAndCacheHal(AtChannel self, eAtModule module, eBool cached)
    {
    AtIpCore core = mMethodsGet(self)->IpCoreGet(self, module);
    AtHal hal = AtIpCoreHalGet(core);

    /* Only need to cache when device has only one core. */
    if (cached && AtDeviceNumIpCoresGet(AtChannelDeviceGet(self)) == 1)
        self->hal = hal;

    return hal;
    }

static eBool ShouldCacheHal(AtChannel self)
    {
    return AtDeviceShouldCacheHalOnChannel(AtChannelDeviceGet(self));
    }

static AtHal HalGetOnModule(AtChannel self, eAtModule moduleId)
    {
    if (!ShouldCacheHal(self))
        return LookupAndCacheHal(self, moduleId, cAtFalse);

    return self->hal ? self->hal : LookupAndCacheHal(self, moduleId, cAtTrue);
    }

/* Common strategy to read local register of a channel */
static uint32 HwReadOnModule(AtChannel self, uint32 localAddress, eAtModule moduleId)
    {
    AtHal hal = HalGetOnModule(self, moduleId);
    if (hal)
        {
        mMethodsGet(self)->WillAccessRegister(self, moduleId, localAddress);
        return AtHalRead(hal, localAddress);
        }

    return 0;
    }

/* Common strategy to write local register of a channel */
static void HwWriteOnModule(AtChannel self, uint32 localAddress, uint32 value, eAtModule moduleId)
    {
    AtHal hal = HalGetOnModule(self, moduleId);
    if (hal)
        {
        mMethodsGet(self)->WillAccessRegister(self, moduleId, localAddress);
        AtHalWrite(hal, localAddress, value);
        }
    }

/*
 * Read a long registers and return number of dwords are read
 *
 * @param self This channel
 * @param localAddress Local address associated with this channel
 * @param [out] dataBuffer Buffer will be used to store data
 * @param bufferLen Capacity of data buffer
 *
 * @return Number of data dwords
 */
static uint16 HwLongReadOnModule(AtChannel self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, eAtModule moduleId)
    {
    AtIpCore core = mMethodsGet(self)->IpCoreGet(self, moduleId);
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet(self), moduleId);
    if (module && core)
        return mMethodsGet(module)->HwLongReadOnCore(module, localAddress, dataBuffer, bufferLen, core);
    return 0;
    }

/*
 * Write a long registers and return number of dwords are written
 *
 * @param self This channel
 * @param localAddress Local address associated with this channel
 * @param dataBuffer Data buffer
 * @param bufferLen Number of dwords in data buffer
 *
 * @return Number of data dwords are written
 */
static uint16 HwLongWriteOnModule(AtChannel self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, eAtModule moduleId)
    {
    AtIpCore core = mMethodsGet(self)->IpCoreGet(self, moduleId);
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet(self), moduleId);
    if (module && core)
        return mMethodsGet(module)->HwLongWriteOnCore(module, localAddress, dataBuffer, bufferLen, core);
    return 0x0;
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    self->boundEncapChannel = encapChannel;
    return cAtOk;
    }

static AtEncapChannel BoundEncapChannelGet(AtChannel self)
    {
    return self->boundEncapChannel;
    }

static uint8 NumBlocksNeedToBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
	AtUnused(encapChannel);
	AtUnused(self);
    /* Let concrete class determine */
    return 0;
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    AtUnused(self);

    /* Let concrete class determine */
    return cInvalidUint32;
    }
    
static eAtRet ReferenceTransfer(AtChannel self, AtChannel dest)
    {
    if (dest == NULL)
        return cAtError;

    dest->boundEncapChannel = self->boundEncapChannel;
    if (dest->boundEncapChannel)
        {
        AtDevice device = AtChannelDeviceGet(self);
        AtEncapBinder encapBinder = AtDeviceEncapBinder(device);
        AtEncapBinderEncapChannelPhysicalChannelSet(encapBinder, dest->boundEncapChannel, dest);
        }
    self->boundEncapChannel = NULL;

    return cAtOk;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    self->boundPw = pseudowire;
    return cAtOk;
    }

static AtPw BoundPseudowireGet(AtChannel self)
    {
    return self->boundPw;
    }

static uint32 DataRateInBytesPer125Us(AtChannel self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 DataRateInBytesPerMs(AtChannel self)
    {
    return mMethodsGet(self)->DataRateInBytesPer125Us(self) * 8;
    }

static eBool ShouldDeletePwOnCleanup(AtChannel self)
    {
    AtUnused(self);
    /* Some kind of PWs can be deleted implicitly in the associated channel. */
    /* Default implement requires explicitly un-binding and deleting PW. */
    return cAtFalse;
    }

static uint32 BoundEncapHwIdGet(AtChannel self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

eAtRet AtChannelReferenceTransfer(AtChannel self, AtChannel dest)
    {
    if (self != NULL)
        return mMethodsGet(self)->ReferenceTransfer(self, dest);

    return cAtError;
    }

eAtRet AtChannelBoundPwSet(AtChannel self, AtPw pw)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    self->boundPw = pw;
    return cAtOk;
    }

eAtRet AtChannelBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret;

    if (self == NULL)
    return cAtErrorNullPointer;

    mMethodsGet(self)->WillBindToPseudowire(self, pseudowire);

    ret = mMethodsGet(self)->BindToPseudowire(self, pseudowire);
    if (ret == cAtOk)
        mMethodsGet(self)->DidBindToPseudowire(self, pseudowire);

    return ret;
    }

/* Return cAtOk if can bind, otherwise, return code will explain why */
eAtRet AtChannelCanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    if (self)
        return mMethodsGet(self)->CanBindToPseudowire(self, pseudowire);

    return cAtErrorNullPointer;
    }

static eAtRet EncapConnectionEnable(AtChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class implement it */
    return cAtError;
    }

static uint8 PppBlockRange(AtChannel self)
    {
	AtUnused(self);
    return 7; /* Just be safe for all products */
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
	AtUnused(self);

    /* For unbind case */
    if (pseudowire == NULL)
        return cAtOk;

    /* Sub class will do for binding case */
    return cAtError;
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    AtUnused(self);
    /* Sub class will do */
    return NULL;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 MlpppBlockRange(AtChannel self)
    {
    AtUnused(self);
    return 7; /* Just be safe for all products */
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    return mMethodsGet(self)->TxTrafficEnable(self, enable);
    }

static eBool TxIsEnabled(AtChannel self)
    {
    return mMethodsGet(self)->TxTrafficIsEnabled(self);
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    return mMethodsGet(self)->RxTrafficEnable(self, enable);
    }

static eBool RxIsEnabled(AtChannel self)
    {
    return mMethodsGet(self)->RxTrafficIsEnabled(self);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(counterType);
    AtUnused(self);

    /* Basically, most of counters defined by one class are supported. So,
     * let's assume all of them are supported. Sub class may override this. */
    return cAtTrue;
    }

static AtSurEngine *SurEngineAddress(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static AtSurEngine SurEngineObjectCreate(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static void SurEngineObjectDelete(AtChannel self, AtSurEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
    }

static AtSurEngine SurEngineGet(AtChannel self)
    {
    AtSurEngine *engineAddress = mMethodsGet(self)->SurEngineAddress(self);

    if (engineAddress == NULL)
        return NULL;

    if (*engineAddress == NULL)
        *engineAddress = mMethodsGet(self)->SurEngineObjectCreate(self);
    return *engineAddress;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtOk;
    }

static AtVcgBinder VcgBinderCreate(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engine);
    }

static eAtRet SourceSet(AtChannel self, AtChannel source)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(source);
    return cAtOk;
    }

static AtChannel SourceGet(AtChannel self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return NULL;
    }

static AtList DestinationChannels(AtChannel self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return NULL;
    }

static eAtRet DestinationAdd(AtChannel self, AtChannel destination)
    {
    return AtListObjectAdd(AtChannelDestinationChannels(self), (AtObject)destination);
    }

static eAtRet DestinationRemove(AtChannel self, AtChannel destination)
    {
    return AtListObjectRemove(AtChannelDestinationChannels(self), (AtObject)destination);
    }

static eBool ShouldCheckRegisterAccess(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    if (AtDeviceShouldCheckRegisterAccess(device))
        return cAtTrue;
    return cAtFalse;
    }

static void WillAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    AtModule module = AtDeviceModuleGet(AtChannelDeviceGet(self), moduleId);

    if (!ShouldCheckRegisterAccess(self))
        return;

    if (!AtModuleRegisterIsInRange(module, address))
        {
        AtChannelLog(self, cAtLogLevelCritical,
                         __FILE__, __LINE__,
                         "Access wrong address 0x%08x in module (%d)\r\n",
                         address, moduleId);
        }

    if (mMethodsGet(self)->CanAccessRegister(self, moduleId, address))
        return;

    AtChannelLog(self, cAtLogLevelCritical,
                 __FILE__, __LINE__,
                 "Access wrong module (%d) with address 0x%08x\r\n",
                 moduleId,
                 address);
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    AtUnused(self);
    AtUnused(moduleId);
    AtUnused(address);
    return cAtTrue;
    }

static eBool OnlySupportXcLoopback(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool XcLoopbackModeIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;
    }

static uint8 *CachedLoopbackMode(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static AtCrossConnect XcEngine(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtChannel object = (AtChannel)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(channelId);
    mEncodeChannelIdString(boundEncapChannel);
    mEncodeChannelIdString(boundPw);
    mEncodeObjectDescription(module);
    mEncodeObjectDescription(device);
    mEncodeUInt(logIsEnabled);
    mEncodeUInt(isTxSquelched);
    mEncodeObject(vcgBinder);
    mEncodeObject(failureProfiler);
    AtCoderEncodeUInt(encoder, AtListLengthGet(object->propertyListeners), "propertyListeners");
    AtCoderEncodeUInt(encoder, AtListLengthGet(object->allEventListeners), "allEventListeners");
    mEncodeNone(listenerListMutex);
    mEncodeNone(listenedDefects);
    mEncodeNone(listenedCounts);
    mEncodeNone(hal);
    mEncodeNone(hwResource); /* Concrete must handle this, here is just to make tool happy */

    AtCoderEncodeString(encoder, AtChannelIdDescriptionBuild(object), "self");
    }

static void WillDeleteNotify(AtChannel self)
	{
	AtChannelEventListener aListener;
	AtIterator iterator;

	if (self->allEventListeners == NULL)
		return;

	/* For all event listeners */
	iterator = AtListIteratorCreate(self->allEventListeners);
	while((aListener = (AtChannelEventListener)AtIteratorNext(iterator)) != NULL)
		{
		if (aListener->listener.WillDelete)
		    aListener->listener.WillDelete(self, aListener->userData);
		}

	AtObjectDelete((AtObject)iterator);
	}

static void WillDelete(AtObject self)
	{
	WillDeleteNotify((AtChannel)self);
	}

static void CacheSetup(AtChannel self, void *cache)
    {
    tAtChannelCache *channelCache = cache;
    AtUnused(self);
    channelCache->timingMode = cAtTimingModeSys;
    }

static void *CacheCreate(AtChannel self)
    {
    void *newCache;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 cacheSize = mMethodsGet(self)->CacheSize(self);

    if (cacheSize == 0)
        return NULL;

    newCache = mMethodsGet(osal)->MemAlloc(osal, cacheSize);
    if (newCache == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, newCache, 0, cacheSize);
    CacheSetup(self, newCache);

    return newCache;
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static void *CacheGet(AtChannel self)
    {
    void **memory;

    if (AtChannelAccessible(self))
        return NULL;

    memory = mMethodsGet(self)->CacheMemoryAddress(self);
    if (memory == NULL)
        return NULL;

    if ((*memory) == NULL)
        *memory = CacheCreate(self);

    return *memory;
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtChannelCache);
    }

static void CacheTxEnable(AtChannel self, eBool enable)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->txEnabled = enable;
    }

static eBool CacheTxIsEnabled(AtChannel self)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    return (eBool)(cache ? cache->txEnabled : cAtFalse);
    }

static void CacheRxEnable(AtChannel self, eBool enable)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->rxEnabled = enable;
    }

static eBool CacheRxIsEnabled(AtChannel self)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    return (eBool)(cache ? cache->rxEnabled : cAtFalse);
    }

static void CacheTimingModeSet(AtChannel self, eAtTimingMode timingMode)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->timingMode   = timingMode;
    }

static eAtTimingMode CacheTimingModeGet(AtChannel self)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    return cache ? cache->timingMode : cAtTimingModeUnknown;
    }

static void CacheInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);

    if (cache)
        {
        uint32 enabledBits  = defectMask &  enableMask;
        uint32 disabledBits = defectMask & (~enableMask);

        cache->interruptMask |=   enabledBits;
        cache->interruptMask &= (~disabledBits);
        }
    }

static uint32 CacheInterruptMaskGet(AtChannel self)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    return cache ? cache->interruptMask : 0;
    }

static void CacheTxAlarmForce(AtChannel self, uint32 alarmType)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->txForcedAlarms |= alarmType;
    }

static void CacheTxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->txForcedAlarms &= (~alarmType);
    }

static uint32 CacheTxForcedAlarmGet(AtChannel self)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    return cache ? cache->txForcedAlarms : 0;
    }

static void CacheRxAlarmForce(AtChannel self, uint32 alarmType)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->rxForcedAlarms |= alarmType;
    }

static void CacheRxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->rxForcedAlarms &= (~alarmType);
    }

static uint32 CacheRxForcedAlarmGet(AtChannel self)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    return cache ? cache->rxForcedAlarms : 0;
    }

static void CacheLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    if (cache)
        cache->loopback = loopbackMode;
    }

static uint8 CacheLoopbackGet(AtChannel self)
    {
    tAtChannelCache *cache = AtChannelCacheGet(self);
    return (uint8)(cache ? cache->loopback : 0);
    }

static void CacheEnable(AtChannel self, eBool enable)
    {
    CacheTxEnable(self, enable);
    CacheRxEnable(self, enable);
    }

static eBool CacheIsEnabled(AtChannel self)
    {
    return (CacheTxIsEnabled(self) || CacheRxIsEnabled(self)) ? cAtTrue : cAtFalse;
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(alarmType);
    AtUnused(self);

    /* Let concrete class determine */
    return cAtTrue;
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    /* Concrete must know. But not to affect running logic, let's assume that
     * all kinds of channel can be enabled/disabled. There current implementation
     * must handle not supported case */
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    /* Concrete must know. But not to affect running logic, let's assume that
     * all kinds of channel can be enabled/disabled. There current implementation
     * must handle not supported case */
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static void AlarmListenerCall(AtChannelEventListener self, AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    if (self->listener.AlarmChangeState)
        self->listener.AlarmChangeState(channel, changedAlarms, currentStatus);

    if (self->listener.AlarmChangeStateWithUserData)
        self->listener.AlarmChangeStateWithUserData(channel, changedAlarms, currentStatus, self->userData);
    }

static uint32 DefectAlarmGet(AtChannel self, uint32 (*AlarmGetFunction)(AtChannel self))
    {
    if (AtDeviceIsEjected(AtChannelDeviceGet(self)))
        return 0;

    return AlarmGetFunction(self);
    }

static char* FormatWithChannelDescription(AtChannel self, const char *format)
    {
    static char m_FormatWithChannelDesc[256];
    AtSnprintf(m_FormatWithChannelDesc, 256, "%s: %s", AtObjectToString((AtObject)self), format);

    m_FormatWithChannelDesc[253] = '\r';
    m_FormatWithChannelDesc[254] = '\n';
    m_FormatWithChannelDesc[255] = '\0';

    return m_FormatWithChannelDesc;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    return AtChannelRxCanEnable(self, enable) || (AtChannelTxCanEnable(self, enable));
    }

static void ListenerDidAdd(AtChannel self, tAtChannelEventListener *listener, void *userData)
    {
    AtUnused(self);
    AtUnused(listener);
    AtUnused(userData);
    }

static AtObjectAny AttController(AtChannel self)
    {
    AtUnused(self);

    /* Let concrete class do */
    return NULL;
    }

static eAtRet HwResourceAllocate(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet HwResourceDeallocate(AtChannel self)
    {
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 BoundPwHwIdGet(AtChannel self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

/* Initialize implementation */
static eBool ServiceIsRunning(AtChannel self)
    {
	AtVcgBinder vcgBinder;
    if (AtChannelBoundEncapChannel(self))
        return cAtTrue;

    if (AtChannelBoundPwGet(self))
        return cAtTrue;

    vcgBinder = AtChannelVcgBinder(self);
    if (vcgBinder == NULL)
        return cAtFalse;

    if (AtVcgBinderSinkMemberGet(vcgBinder))
        return cAtTrue;

    if (AtVcgBinderSourceMemberGet(vcgBinder))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet QueueEnable(AtChannel self, eBool enable)
    {
    /* Let concrete classes do */
    AtUnused(self);
    AtUnused(enable);
    return cAtError;
    }

static eBool QueueIsEnabled(AtChannel self)
    {
    /* Let concrete classes do */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool TxEncapConnectionIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RxEncapConnectionEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool RxEncapConnectionIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void **SimulationDatabaseAddress(AtChannel self)
    {
    /* Concrete should know */
    AtUnused(self);
    return NULL;
    }

static uint32 SimulationDatabaseSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtChannelSimulationDb);
    }

static void *SimulationDatabaseCreate(AtChannel self)
    {
    void *newCache;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 databaseSize = mMethodsGet(self)->SimulationDatabaseSize(self);

    if (databaseSize == 0)
        return NULL;

    newCache = mMethodsGet(osal)->MemAlloc(osal, databaseSize);
    if (newCache == NULL)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, newCache, 0, databaseSize);

    return newCache;
    }

static void *SimulationDatabaseGet(AtChannel self)
    {
    void **memory = mMethodsGet(self)->SimulationDatabaseAddress(self);
    if (memory == NULL)
        return NULL;

    if ((*memory) == NULL)
        *memory = SimulationDatabaseCreate(self);

    return *memory;
    }

static eBool CanJoinVcg(AtChannel self, AtConcateGroup vcg)
    {
    AtUnused(self);
    AtUnused(vcg);

    return cAtFalse;
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    AtUnused(self);
    AtUnused(group);
    return cAtErrorNotImplemented;
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    AtUnused(self);
    AtUnused(group);
    return cAtErrorNotImplemented;
    }

static uint32 BoundConcateHwIdGet(AtChannel self)
    {
    AtUnused(self);
    /* Let subclass implement it */
    return cInvalidUint32;
    }

static eAtRet AllAlarmsUnforce(AtChannel self)
    {
    uint32 forcedAlarms;
    eAtRet ret = cAtOk;

    forcedAlarms = AtChannelTxForcedAlarmGet(self);
    if (forcedAlarms)
        ret |= AtChannelTxAlarmUnForce(self, forcedAlarms);

    forcedAlarms = AtChannelRxForcedAlarmGet(self);
    if (forcedAlarms)
        ret |= AtChannelRxAlarmUnForce(self, forcedAlarms);

    return ret;
    }

static eAtRet AllInterruptMaskClear(AtChannel self)
    {
    uint32 mask = AtChannelInterruptMaskGet(self);
    return AtChannelInterruptMaskSet(self, mask, 0);
    }

static eAtRet HardwareCleanup(AtChannel self)
    {
    eAtRet ret = cAtOk;

    ret |= AllAlarmsUnforce(self);
    ret |= AllInterruptMaskClear(self);

    /* Listeners are software things so let listener list be cleanup by the
     * AtObjectDelete() */

    return ret;
    }

static eBool HasCrossConnect(AtChannel self)
    {
    if (AtListLengthGet(AtChannelDestinationChannels(self)) > 0)
        return cAtTrue;
    return AtChannelSourceGet((AtChannel) self) ? cAtTrue : cAtFalse;
    }

static eBool HwIsReady(AtChannel self)
    {
    return AtModuleHwIsReady(AtChannelModuleGet(self));
    }

static eBool ShouldFullyInit(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);

    if (AtDeviceTestbenchIsEnabled(device))
        return cAtFalse;

    if (AtDeviceDiagnosticModeIsEnabled(device))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet AllServicesDestroy(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static uint32 PwTimingRestore(AtChannel self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static AtDebugger DebuggerObjectCreate(AtChannel self)
    {
    AtUnused(self);
    return AtDebuggerNew();
    }

static AtDebugger DebuggerCreate(AtChannel self)
    {
    AtDebugger debugger = mMethodsGet(self)->DebuggerObjectCreate(self);
    mMethodsGet(self)->DebuggerEntriesFill(self, debugger);
    return debugger;
    }

static void DebuggerEntriesFill(AtChannel self, AtDebugger debugger)
    {
    uint32 bufferSize;
    char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
    AtSnprintf(buffer, bufferSize, "%s.%s", AtChannelTypeString(self), AtChannelIdString(self));
    AtDebuggerEntryAdd(debugger, AtDebugEntryNew(cSevNormal, "Channel", buffer));
    }

static eAtRet LoopbackSetWithLogger(AtChannel self, uint8 loopbackMode)
    {
    mNumericalAttributeSetThenCache(LoopbackSet, loopbackMode);
    }

static AtList *PropertyListenerListAddress(AtObject self)
    {
    return &(mThis(self)->propertyListeners);
    }

static const char *LoopbackMode2String(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);

    switch (loopbackMode)
        {
        case cAtLoopbackModeRelease: return "release";
        case cAtLoopbackModeLocal  : return "local";
        case cAtLoopbackModeRemote : return "remote";
        case cAtLoopbackModeDual   : return "dual";
        default:
            return "unknown";
        }
    }

static AtFailureProfiler FailureProfilerObjectCreate(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static AtFailureProfiler FailureProfilerGet(AtChannel self)
    {
    if (self->failureProfiler)
        return self->failureProfiler;

    self->failureProfiler = mMethodsGet(self)->FailureProfilerObjectCreate(self);
    return self->failureProfiler;
    }

static eBool ShouldPreventReconfigure(AtChannel self)
    {
    return AtDeviceShouldPreventReconfigure(AtChannelDeviceGet(self));
    }

static eAtRet TxSquelch(AtChannel self, eBool squelched)
    {
    /* Do nothing so that it will not affect current products */
    AtUnused(self);
    AtUnused(squelched);
    return cAtOk;
    }

static eAtRet DidBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtOk;
    }

static eAtRet WillBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtOk;
    }

static eBool ShouldIgnoreTimingSourceForLogging(eAtTimingMode timingMode)
    {
    switch ((uint32)timingMode)
        {
        case cAtTimingModeUnknown      : return cAtTrue;
        case cAtTimingModeSys          : return cAtTrue;
        case cAtTimingModeLoop         : return cAtTrue;
        case cAtTimingModeSdhSys       : return cAtTrue;
        case cAtTimingModeSdhLineRef   : return cAtFalse;
        case cAtTimingModePrc          : return cAtTrue;
        case cAtTimingModeExt1Ref      : return cAtTrue;
        case cAtTimingModeExt2Ref      : return cAtTrue;
        case cAtTimingModeAcr          : return cAtFalse;
        case cAtTimingModeDcr          : return cAtFalse;
        case cAtTimingModeSlave        : return cAtFalse;
        case cAtTimingModeFreeRunning  : return cAtTrue;
        case cAtTimingModeNotApplicable: return cAtTrue;
        default:
            return cAtTrue;
        }
    }

static void TimingSetApiLogStart(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        const char* objectDesc = NULL;
        AtDriverLogLock();

        if (!ShouldIgnoreTimingSourceForLogging(timingMode))
            objectDesc = AtDriverObjectStringToSharedBuffer((AtObject)timingSource);

        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                AtSourceLocationNone, "API: %s([%s], %u, %s)\r\n",
                                function, AtObjectToString((AtObject)self), timingMode, objectDesc);
        AtDriverLogUnLock();
        }
    }

static void MethodsInit(AtChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, ModuleGet);
        mMethodOverride(m_methods, IdGet);
        mMethodOverride(m_methods, IdString);
        mMethodOverride(m_methods, TypeString);
        mMethodOverride(m_methods, TxTrafficEnable);
        mMethodOverride(m_methods, RxTrafficEnable);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, CanEnable);
        mMethodOverride(m_methods, RxCanEnable);
        mMethodOverride(m_methods, TxCanEnable);
        mMethodOverride(m_methods, TimingModeIsSupported);
        mMethodOverride(m_methods, TimingSet);
        mMethodOverride(m_methods, TimingModeGet);
        mMethodOverride(m_methods, TimingSourceGet);
        mMethodOverride(m_methods, ClockStateGet);
        mMethodOverride(m_methods, HwClockStateGet);
        mMethodOverride(m_methods, HwClockStateTranslate);
        mMethodOverride(m_methods, DefectGet);
        mMethodOverride(m_methods, DefectHistoryGet);
        mMethodOverride(m_methods, DefectHistoryClear);
        mMethodOverride(m_methods, SpecificDefectInterruptClear);
        mMethodOverride(m_methods, AlarmGet);
        mMethodOverride(m_methods, AlarmHistoryGet);
        mMethodOverride(m_methods, AlarmHistoryClear);
        mMethodOverride(m_methods, EventListenerAdd);
        mMethodOverride(m_methods, SupportedInterruptMasks);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, InterruptEnable);
        mMethodOverride(m_methods, InterruptDisable);
        mMethodOverride(m_methods, InterruptIsEnabled);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, TxAlarmForce);
        mMethodOverride(m_methods, TxAlarmUnForce);
        mMethodOverride(m_methods, TxForcedAlarmGet);
        mMethodOverride(m_methods, TxForcibleAlarmsGet);
        mMethodOverride(m_methods, RxAlarmForce);
        mMethodOverride(m_methods, RxAlarmUnForce);
        mMethodOverride(m_methods, RxForcedAlarmGet);
        mMethodOverride(m_methods, RxForcibleAlarmsGet);
        mMethodOverride(m_methods, TxErrorForce);
        mMethodOverride(m_methods, TxErrorUnForce);
        mMethodOverride(m_methods, TxForcedErrorGet);
        mMethodOverride(m_methods, TxForcableErrorsGet);
        mMethodOverride(m_methods, LoopbackSet);
        mMethodOverride(m_methods, LoopbackGet);
        mMethodOverride(m_methods, LoopbackIsSupported);
        mMethodOverride(m_methods, LoopbackMode2String);
        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, AllCountersLatchAndClear);
        mMethodOverride(m_methods, AllCountersGet);
        mMethodOverride(m_methods, AllCountersClear);
        mMethodOverride(m_methods, AllConfigSet);
        mMethodOverride(m_methods, AllConfigGet);
        mMethodOverride(m_methods, HwIdGet);
        mMethodOverride(m_methods, IpCoreGet);
        mMethodOverride(m_methods, HwReadOnModule);
        mMethodOverride(m_methods, HwWriteOnModule);
        mMethodOverride(m_methods, HwLongReadOnModule);
        mMethodOverride(m_methods, HwLongWriteOnModule);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, StatusClear);
        mMethodOverride(m_methods, BindToEncapChannel);
        mMethodOverride(m_methods, BoundEncapChannelGet);
        mMethodOverride(m_methods, BoundEncapHwIdGet);
        mMethodOverride(m_methods, NumBlocksNeedToBindToEncapChannel);
        mMethodOverride(m_methods, EncapHwIdAllocate);
        mMethodOverride(m_methods, ReadOnlyCntGet);
        mMethodOverride(m_methods, ReferenceTransfer);
        mMethodOverride(m_methods, EncapConnectionEnable);
        mMethodOverride(m_methods, PppBlockRange);
        mMethodOverride(m_methods, BindToPseudowire);
        mMethodOverride(m_methods, BoundPseudowireGet);
        mMethodOverride(m_methods, BoundPwHwIdGet);
        mMethodOverride(m_methods, DataRateInBytesPer125Us);
        mMethodOverride(m_methods, DataRateInBytesPerMs);
        mMethodOverride(m_methods, CanBindToPseudowire);
        mMethodOverride(m_methods, ShouldDeletePwOnCleanup);
        mMethodOverride(m_methods, PrbsEngineGet);
        mMethodOverride(m_methods, PrbsEngineIsCreated);
        mMethodOverride(m_methods, MlpppBlockRange);
        mMethodOverride(m_methods, TxEnable);
        mMethodOverride(m_methods, TxIsEnabled);
        mMethodOverride(m_methods, RxEnable);
        mMethodOverride(m_methods, RxIsEnabled);
        mMethodOverride(m_methods, CounterIsSupported);
        mMethodOverride(m_methods, WarmRestore);
        mMethodOverride(m_methods, VcgBinderCreate);
        mMethodOverride(m_methods, CanJoinVcg);
        mMethodOverride(m_methods, BindToSourceGroup);
        mMethodOverride(m_methods, BindToSinkGroup);
        mMethodOverride(m_methods, BoundConcateHwIdGet);
        mMethodOverride(m_methods, TxTrafficIsEnabled);
        mMethodOverride(m_methods, RxTrafficIsEnabled);
        mMethodOverride(m_methods, ListenerDidAdd);
        mMethodOverride(m_methods, ServiceIsRunning);
        mMethodOverride(m_methods, HardwareCleanup);
        mMethodOverride(m_methods, HwIsReady);
        mMethodOverride(m_methods, AllServicesDestroy);
        mMethodOverride(m_methods, PwTimingRestore);
        mMethodOverride(m_methods, DebuggerObjectCreate);
        mMethodOverride(m_methods, DebuggerCreate);
        mMethodOverride(m_methods, DebuggerEntriesFill);
        mMethodOverride(m_methods, QuerierGet);
        mMethodOverride(m_methods, TxSquelch);
        mMethodOverride(m_methods, DidBindToPseudowire);
        mMethodOverride(m_methods, WillBindToPseudowire);

        /* XC methods */
        mMethodOverride(m_methods, SourceSet);
        mMethodOverride(m_methods, SourceGet);
        mMethodOverride(m_methods, DestinationChannels);
        mMethodOverride(m_methods, DestinationAdd);
        mMethodOverride(m_methods, DestinationRemove);
        mMethodOverride(m_methods, OnlySupportXcLoopback);
        mMethodOverride(m_methods, XcLoopbackModeIsSupported);
        mMethodOverride(m_methods, XcLoopbackSet);
        mMethodOverride(m_methods, XcLoopbackGet);
        mMethodOverride(m_methods, CachedLoopbackMode);
        mMethodOverride(m_methods, XcEngine);

        mMethodOverride(m_methods, WillAccessRegister);
        mMethodOverride(m_methods, CanAccessRegister);
        mMethodOverride(m_methods, PrbsEngineSet);

        /* For standby driver */
        mMethodOverride(m_methods, CacheGet);
        mMethodOverride(m_methods, CacheMemoryAddress);
        mMethodOverride(m_methods, CacheSize);
        mMethodOverride(m_methods, CacheIsEnabled);

        /* Alarm supporting */
        mMethodOverride(m_methods, AlarmIsSupported);

        /* Surveillance */
        mMethodOverride(m_methods, SurEngineGet);
        mMethodOverride(m_methods, SurEngineAddress);
        mMethodOverride(m_methods, SurEngineObjectCreate);
        mMethodOverride(m_methods, SurEngineObjectDelete);

        /* ATT */
        mMethodOverride(m_methods, AttController);

        /* Hardware resource caching */
        mMethodOverride(m_methods, HwResourceAllocate);
        mMethodOverride(m_methods, HwResourceDeallocate);

        /* Queue Manage */
        mMethodOverride(m_methods, QueueEnable);
        mMethodOverride(m_methods, QueueIsEnabled);
        
        /* To handle encapsulation connection */
        mMethodOverride(m_methods, RxEncapConnectionIsEnabled);
        mMethodOverride(m_methods, RxEncapConnectionEnable);
        mMethodOverride(m_methods, TxEncapConnectionEnable);
        mMethodOverride(m_methods, TxEncapConnectionIsEnabled);

        /* Simulation */
        mMethodOverride(m_methods, SimulationDatabaseSize);
        mMethodOverride(m_methods, SimulationDatabaseAddress);

        mMethodOverride(m_methods, FailureProfilerObjectCreate);
        mMethodOverride(m_methods, ShouldPreventReconfigure);
        }

    mMethodsGet(self) = &m_methods;
    }

static void OverrideAtObject(AtChannel self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        /* Save reference to super implementation */
        m_AtObjectMethods = mMethodsGet((AtObject)self);

        /* Copy to reuse implementation of super class. But override some methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, WillDelete);
        mMethodOverride(m_AtObjectOverride, PropertyListenerListAddress);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

/* Constructor */
AtChannel AtChannelObjectInit(AtChannel self, uint32 channelId, AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Initialize memory */
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtChannel));

    /* Call super constructor to reuse all of its implementation */
    AtObjectInit((AtObject)self);

    /* Initialize implementation */
    MethodsInit(self);
    OverrideAtObject(self);

    /* Initialize private data */
    self->module    = module;
    self->channelId = channelId;
    self->device    = AtModuleDeviceGet(module);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

void *AtChannelCacheGet(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->CacheGet(self);
    return NULL;
    }

uint32 AtChannelDataRateInBytesPer125Us(AtChannel self)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->DataRateInBytesPer125Us(self);

    return 0;
    }

uint32 AtChannelDataRateInBytesPerMs(AtChannel self)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->DataRateInBytesPerMs(self);

    return 0;
    }

/* Call all alarm listeners of this channel */
void AtChannelAllAlarmListenersCall(AtChannel self, uint32 changedAlarms, uint32 currentStatus)
    {
    AtChannelEventListener aListener;
    AtIterator iterator;
    uint32 alarm;

    if ((self == NULL) || (self->allEventListeners == NULL))
        return;

    /* Opps, nothing to be report. */
    if (changedAlarms == 0)
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(self->allEventListeners);
    while((aListener = (AtChannelEventListener)AtIteratorNext(iterator)) != NULL)
        AlarmListenerCall(aListener, self, changedAlarms, currentStatus);

    /* Notify itself */
    for (alarm = cBit0; alarm < cBit31; alarm <<= 1)
        {
        if (changedAlarms & alarm)
            {
            if (currentStatus & alarm)
                self->listenedDefects |= alarm;
            else
                self->listenedDefects &= ~alarm;
            }
        }
    self->listenedCounts++;

    AtObjectDelete((AtObject)iterator);
    }

void AtChannelAllFailureListenersCall(AtChannel self, uint32 changedFailures, uint32 currentStatus)
    {
    AtSurEngineFailureNotify(AtChannelSurEngineGet(self), changedFailures, currentStatus);
    }

/* Call OAM listeners. */
void AtChannelOamReceivedNotify(AtChannel self, uint8 *data, uint32 length)
    {
    AtChannelEventListener aListener;
    AtIterator iterator;

    /* return if it NULL */
    if ((self == NULL) || (self->allEventListeners == NULL))
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(self->allEventListeners);
    while((aListener = (AtChannelEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (aListener->listener.OamReceived == NULL)
            continue;

        aListener->listener.OamReceived(self, data, length);
        }

    AtObjectDelete((AtObject)iterator);
    }

eAtRet AtChannelBindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    if (self)
        return mMethodsGet(self)->BindToEncapChannel(self, encapChannel);
    return cAtErrorNullPointer;
    }

AtEncapChannel AtChannelBoundEncapChannel(AtChannel self)
    {
    return self ? self->boundEncapChannel : NULL;
    }

void AtChannelBoundEncapChannelSet(AtChannel self, AtEncapChannel encapChannel)
    {
    if (self)
        self->boundEncapChannel = encapChannel;
    }

eAtRet AtChannelTxTrafficEnable(AtChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TxTrafficEnable(self, enable);

    return cAtError;
    }

eBool AtChannelTxTrafficIsEnabled(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->TxTrafficIsEnabled(self);

    return cAtFalse;
    }

eAtRet AtChannelRxTrafficEnable(AtChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->RxTrafficEnable(self, enable);

    return cAtError;
    }

eBool AtChannelRxTrafficIsEnabled(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->RxTrafficIsEnabled(self);

    return cAtFalse;
    }

uint32 AtChannelHwIdGet(AtChannel self)
    {
    mAttributeGet(HwIdGet, uint32, cBit31_0);
    }

uint8 AtChannelPppBlockRange(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->PppBlockRange(self);

    return 0;
    }

uint32 AtChannelReadOnlyCntGet(AtChannel self, uint32 counterType)
    {
    if (self)
        return mMethodsGet(self)->ReadOnlyCntGet(self, counterType);

    return 0;
    }

uint32 AtChannelHwReadOnModule(AtChannel self, uint32 address, eAtModule moduleId)
    {
    uint32 value;

    if (self == NULL)
    return 0;

    value = mMethodsGet(self)->HwReadOnModule(self, address, moduleId);
    if (AtDriverDebugIsEnabled())
        AtDeviceReadNotify(AtChannelDeviceGet(self), address, value, 0);

    return value;
    }

void AtChannelHwWriteOnModule(AtChannel self, uint32 localAddress, uint32 value, eAtModule module)
    {
    if (self == NULL)
        return;

        mMethodsGet(self)->HwWriteOnModule(self, localAddress, value, module);
    if (AtDriverDebugIsEnabled())
        AtDeviceWriteNotify(AtChannelDeviceGet(self), localAddress, value, 0);
    }

uint16 AtChannelHwLongReadOnModule(AtChannel self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, eAtModule module)
    {
    uint16 numDwords;

    if (self == NULL)
    return 0;

    numDwords = mMethodsGet(self)->HwLongReadOnModule(self, localAddress, dataBuffer, bufferLen, module);
    if (AtDriverDebugIsEnabled())
        AtDeviceLongReadNotify(AtChannelDeviceGet(self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

uint16 AtChannelHwLongWriteOnModule(AtChannel self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, eAtModule module)
    {
    uint16 numDwords;

    if (self == NULL)
    return 0;

    numDwords = mMethodsGet(self)->HwLongWriteOnModule(self, localAddress, dataBuffer, bufferLen, module);
    if (AtDriverDebugIsEnabled())
        AtDeviceLongWriteNotify(AtChannelDeviceGet(self), localAddress, dataBuffer, numDwords, 0);

    return numDwords;
    }

AtVcgBinder AtChannelVcgBinder(AtChannel self)
    {
    if (self->vcgBinder)
        return self->vcgBinder;

    self->vcgBinder = mMethodsGet(self)->VcgBinderCreate(self);
    return self->vcgBinder;
    }

AtConcateGroup AtChannelSourceGroupGet(AtChannel self)
    {
    AtConcateMember member = AtVcgBinderSourceMemberGet(AtChannelVcgBinder(self));
    if (member)
        return AtConcateMemberConcateGroupGet(member);
    return NULL;
    }

AtConcateGroup AtChannelSinkGroupGet(AtChannel self)
    {
    AtConcateMember member = AtVcgBinderSinkMemberGet(AtChannelVcgBinder(self));
    if (member)
        return AtConcateMemberConcateGroupGet(member);
    return NULL;
    }

void AtChannelLog(AtChannel self, eAtLogLevel level, const char *file, uint32 line, const char *format, ...)
    {
    va_list args;
    if (self == NULL)
        return;

    AtDriverLogLock();

    va_start(args, format);
    AtDriverVaListLog(AtDriverSharedDriverGet(), level, file, line, FormatWithChannelDescription(self, format), args);
    va_end(args);

    AtDriverLogUnLock();
    }

uint8 AtChannelMlpppBlockRange(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->MlpppBlockRange(self);
    return 0;
    }

eBool AtChannelDeviceInWarmRestore(AtChannel self)
    {
    return AtDeviceInWarmRestore(AtChannelDeviceGet(self));
    }

eAtRet AtChannelWarmRestore(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->WarmRestore(self);

    return cAtErrorNullPointer;
    }

void AtChannelIdSet(AtChannel self, uint32 channelId)
    {
    if (self)
        self->channelId = channelId;
    }

void AtChannelPrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    if (self)
        mMethodsGet(self)->PrbsEngineSet(self, engine);
    }

eAtRet AtChannelHwResourceAllocate(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->HwResourceAllocate(self);

    return cAtErrorModeNotSupport;
    }

eAtRet AtChannelHwResourceDeallocate(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->HwResourceDeallocate(self);

    return cAtErrorModeNotSupport;
    }

void *AtChannelHwResourceGet(AtChannel self)
    {
    if (self)
        return self->hwResource;
    return NULL;
    }

void AtChannelHwResourceCache(AtChannel self, void* hwResource)
    {
    if (self)
        self->hwResource = hwResource;
    }

void AtChannelBoundPwObjectDelete(AtChannel self)
    {
    if (self && mMethodsGet(self)->ShouldDeletePwOnCleanup(self))
        BoundPwObjectDelete(self);
    }

eAtRet AtChannelQueueEnable(AtChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->QueueEnable(self, enable);

    return cAtErrorNotExist;
    }

eBool AtChannelQueueIsEnabled(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->QueueIsEnabled(self);

    return cAtFalse;
    }

/*
 * Set Source for a Channel, usage in XC function
 *
 * @param self This channel
 * @param source The source channel
 *
 * @return Counter value
 */
eAtRet AtChannelSourceSet(AtChannel self, AtChannel source)
    {
    if (self)
        return mMethodsGet(self)->SourceSet(self, source);
    return cAtError;
    }

/*
 * Get Source of a Channel, usage in XC function
 *
 * @param self This channel
 *
 * @return NULL if no source, otherwise will a valid channel
 */
AtChannel AtChannelSourceGet(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->SourceGet(self);
    return NULL;
    }

/*
 * Get an AtList of Destinations channel
 *
 * @param self This channel
 *
 * @return Iterator
 */
AtList AtChannelDestinationChannels(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->DestinationChannels(self);
    return NULL;
    }

/*
 * Add destination for a channel, usage in XC function
 *
 * @param self This channel
 * @param destination The destination channel
 *
 * @return Counter value
 */
eAtRet AtChannelDestinationAdd(AtChannel self, AtChannel destination)
    {
    if (self)
        return mMethodsGet(self)->DestinationAdd(self, destination);
    return cAtError;
    }

/*
 * Remove destination for a channel, usage in XC function
 *
 * @param self This channel
 * @param destination The destination channel
 *
 * @return Counter value
 */
eAtRet AtChannelDestinationRemove(AtChannel self, AtChannel destination)
    {
    if (self)
        return mMethodsGet(self)->DestinationRemove(self, destination);
    return cAtError;
    }

char *AtChannelIdDescriptionBuild(AtChannel self)
    {
    uint32 bufferSize;
    char *buffer = AtSharedDriverSharedBuffer(&bufferSize);
    if (self == NULL)
        AtSnprintf(buffer, bufferSize, "%s", "none");
    else
        AtSnprintf(buffer, bufferSize, "%s.%s", AtChannelTypeString(self), AtChannelIdString(self));
    return buffer;
    }

eBool AtChannelRxCanEnable(AtChannel self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->RxCanEnable(self, enabled);
    return cAtFalse;
    }

eBool AtChannelTxCanEnable(AtChannel self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->TxCanEnable(self, enabled);
    return cAtFalse;
    }

/*
 * Process interrupt
 *
 * @param self This channel
 * @param offset An optional address offset that can be easily calculated when
 *               traversing interrupt tree.
 */
uint32 AtChannelInterruptProcess(AtChannel self, uint32 offset)
    {
    if (self)
        return mMethodsGet(self)->InterruptProcess(self, offset);
    return 0;
    }

uint32 AtChannelListenedDefectGet(AtChannel self, uint32 * listenedCount)
    {
    if (self)
        {
        if (listenedCount)
            *listenedCount = self->listenedCounts;
        return self->listenedDefects;
        }

    if (listenedCount)
        *listenedCount = 0;
    return 0;
    }

uint32 AtChannelListenedDefectClear(AtChannel self, uint32 * listenedCount)
    {
    if (self)
        {
        uint32 defect = self->listenedDefects;
        uint32 counts = self->listenedCounts;

        self->listenedCounts = 0;
        self->listenedDefects = 0;

        if (listenedCount)
            *listenedCount = counts;
        return defect;
        }

    if (listenedCount)
        *listenedCount = 0;
    return 0;
    }

uint32 AtChannelSupportedInterruptMasks(AtChannel self)
    {
    if (self)
    	return mMethodsGet(self)->SupportedInterruptMasks(self);
    return 0x0;
    }

eBool AtChannelInterruptMaskCanSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 noneSupportedMasks = ~AtChannelSupportedInterruptMasks(self);
    if (noneSupportedMasks & defectMask & enableMask)
        return cAtFalse;
    return cAtTrue;
    }

AtObjectAny AtChannelAttController(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->AttController(self);
    return NULL;
    }

uint32 AtChannelEncapHwIdAllocate(AtChannel self)
    {
    uint32 encapHwId = cInvalidUint32;
    if (self)
        encapHwId = mMethodsGet(self)->EncapHwIdAllocate(self);

    if (encapHwId == cInvalidUint32)
        AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation, "Cannot allocate TDM local HW Id\r\n");

    return encapHwId;
    }

uint32 AtChannelBoundPwHwIdGet(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->BoundPwHwIdGet(self);
    return cInvalidUint32;
    }

uint32 AtChannelBoundEncapHwIdGet(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->BoundEncapHwIdGet(self);

    return cInvalidUint32;
    }

eBool AtChannelServiceIsRunning(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->ServiceIsRunning(self);
    return cAtFalse;
    }

void *AtChannelSimulationDatabaseGet(AtChannel self)
    {
    if (self == NULL)
        return NULL;

    if (AtChannelHwIsReady(self))
        return NULL;

    return SimulationDatabaseGet(self);
    }

eBool AtChannelHwIsReady(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->HwIsReady(self);
    return cAtFalse;
    }

eBool AtChannelHasCrossConnect(AtChannel self)
    {
    if (self)
        return HasCrossConnect(self);
    return cAtFalse;
    }

eBool AtChannelCanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    if (self)
        return mMethodsGet(self)->CanAccessRegister(self, moduleId, address);
    return cAtFalse;
    }

eBool AtChannelShouldFullyInit(AtChannel self)
    {
    if (self)
        return ShouldFullyInit(self);
    return cAtFalse;
    }

uint32 AtChannelDefectHistoryGet(AtChannel self)
    {
    return AtChannelDefectInterruptGet(self);
    }

uint32 AtChannelDefectHistoryClear(AtChannel self)
    {
    return AtChannelDefectInterruptClear(self);
    }

eAtClockState AtChannelHwClockStateTranslate(AtChannel self, uint8 hwState)
    {
    mOneParamAttributeGet(HwClockStateTranslate, hwState, eAtClockState, cAtClockStateUnknown);
    }

uint8 AtChannelHwClockStateGet(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->HwClockStateGet(self);
    return cInvalidUint8;
    }

eBool AtChannelCanJoinVcg(AtChannel self, AtConcateGroup vcg)
    {
    mOneParamAttributeGet(CanJoinVcg, vcg, eBool, cAtFalse);
    }

eAtRet AtChannelBindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindToSourceGroup(self, group);
    return cAtErrorNullPointer;
    }

eAtRet AtChannelBindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->BindToSinkGroup(self, group);
    return cAtErrorNullPointer;
    }

uint32 AtChannelBoundConcateHwIdGet(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->BoundConcateHwIdGet(self);
    return cInvalidUint32;
    }

eAtRet AtChannelHardwareCleanup(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->HardwareCleanup(self);
    return cAtErrorNullPointer;
    }

eAtRet AtChannelAllServicesDestroy(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->AllServicesDestroy(self);
    return cAtErrorNullPointer;
    }

eAtRet AtChannelSurEngineInterruptDisable(AtChannel self)
    {
    AtModule sur = AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleSur);
    AtSurEngine engine = AtChannelSurEngineGet(self);
    eAtRet ret = cAtOk;

    if (engine == NULL)
        return cAtOk;

    if (AtModuleSurFailureIsSupported((AtModuleSur)sur))
        ret |= AtSurEngineFailureNotificationMaskSet(engine, AtSurEngineFailureNotificationMaskGet(engine), 0);
    if (AtSurEngineTcaIsSupported(engine))
        ret |= AtSurEngineTcaNotificationMaskSet(engine, AtSurEngineTcaNotificationMaskGet(engine), 0);

    return ret;
    }

AtDebugger AtChannelDebuggerCreate(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->DebuggerCreate(self);
    return NULL;
    }

void AtChannelDebuggerEntriesFill(AtChannel self, AtDebugger debugger)
    {
    if (self)
        mMethodsGet(self)->DebuggerEntriesFill(self, debugger);
    }

eBool AtChannelAccessible(AtChannel self)
    {
    return AtDeviceAccessible(AtChannelDeviceGet(self));
    }

eBool AtChannelInAccessible(AtChannel self)
    {
    return AtDeviceAccessible(AtChannelDeviceGet(self)) ? cAtFalse : cAtTrue;
    }

eBool AtChannelShouldPreventReconfigure(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->ShouldPreventReconfigure(self);
    return cAtFalse;
    }

AtFailureProfiler AtChannelFailureProfilerGet(AtChannel self)
    {
    if (self)
        return FailureProfilerGet(self);
    return NULL;
    }

uint32 AtChannelPwTimingRestore(AtChannel self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwTimingRestore(self, pw);
    return 0;
    }

AtQuerier AtChannelQuerierGet(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->QuerierGet(self);
    return NULL;
    }

eAtRet AtChannelTxSquelch(AtChannel self, eBool squelched)
    {
    if (self)
        {
        eAtRet ret = mMethodsGet(self)->TxSquelch(self, squelched);
        if (ret == cAtOk)
            self->isTxSquelched = squelched;
        return ret;
        }

    return cAtErrorObjectNotExist;
    }

eAtRet AtChannelTxIsSquelched(AtChannel self)
    {
    if (self)
        return self->isTxSquelched;

    return cAtFalse;
    }

eBool AtChannelPrbsEngineIsCreated(AtChannel self)
    {
    if (self)
        return mMethodsGet(self)->PrbsEngineIsCreated(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtChannel
 * @{
 */

/**
 * Initialize a channel
 *
 * @param self This channel
 *
 * @return AT return code
 */
eAtRet AtChannelInit(AtChannel self)
    {
    mNoParamCall(Init, eAtRet, cAtErrorObjectNotExist);
    }

/**
 * Get module that this channel belongs to
 *
 * @param self This channel
 *
 * @return Module of this channel
 */
AtModule AtChannelModuleGet(AtChannel self)
    {
    mNoParamObjectGet(ModuleGet, AtModule);
    }

/**
 * Get device that this channel belongs to
 *
 * @param self This channel
 * @return Device
 */
AtDevice AtChannelDeviceGet(AtChannel self)
    {
    return self ? self->device : NULL;
    }

/**
 * Get ID of this channel
 *
 * @param self This channel
 *
 * @return ID of this channel
 */
uint32 AtChannelIdGet(AtChannel self)
    {
    mAttributeGet(IdGet, uint32, 0);
    }

/**
 * ID string a a channel
 *
 * @param self This channel
 *
 * @return String that describes ID of this channel
 * @note This API may return static data and not thread-safe
 */
const char *AtChannelIdString(AtChannel self)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->IdString(self);

    return NULL;
    }

/**
 * Get string of channel type
 *
 * @param self This channel
 *
 * @return Channel type string
 */
const char *AtChannelTypeString(AtChannel self)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->TypeString(self);

    return NULL;
    }

/**
 * Enable this channel
 *
 * @param self This channel
 * @param enable Channel enabling.
 *               - cAtTrue: Enable
 *               - cAtFalse: Disable
 *
 * @return AT return codes
 */
eAtRet AtChannelEnable(AtChannel self, eBool enable)
    {
    mNumericalAttributeSetWithCache(Enable, enable);
    }

/**
 * Check if this channel is enabled
 *
 * @param self This channel
 *
 * @return cAtTrue if it is enabled or cAtFalse if it is disabled
 */
eBool AtChannelIsEnabled(AtChannel self)
    {
    if (self == NULL)
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtFalse;
        }

    if (AtChannelInAccessible(self))
        return mMethodsGet(self)->CacheIsEnabled(self);

    return mMethodsGet(self)->IsEnabled(self);
    }

/**
 * Check if channel can be enabled/disabled
 *
 * @param self This channel
 * @param enable Channel enabling to check.
 *               - cAtTrue: to check if channel can be enabled
 *               - cAtFalse: to check if channel can be disabled
 *
 * @retval cAtTrue if channel can be enabled/disabled
 * @retval cAtFalse if channel cannot be enabled/disabled
 */
eBool AtChannelCanEnable(AtChannel self, eBool enable)
    {
    mOneParamAttributeGet(CanEnable, enable, eBool, cAtFalse);
    }

/**
 * To check if specified timing mode is supported
 *
 * @param self This channel
 * @param timingMode Timing mode to check
 *
 * @retval cAtTrue if timing mode is supported
 * @retval cAtFalse if timing mode is not supported
 */
eBool AtChannelTimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    if (self)
        return mMethodsGet(self)->TimingModeIsSupported(self, timingMode);
    return cAtFalse;
    }

/**
 * Set timing mode of this channel
 *
 * @param self This channel
 * @param timingMode Timing mode. @ref eAtTimingMode
 * @param timingSource Timing source. It is only used when timing needs to refer
 *        from a source. For example, it timing mode is
 *        - SDH Line: SDH Line object is expected
 *
 *        Note, ACR/DCR are only applicable for channels that are bound to
 *        Pseudowire and no timing source is required
 *
 * @return AT return codes
 */
eAtRet AtChannelTimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtErrorObjectNotExist;

    TimingSetApiLogStart(self, timingMode, timingSource, AtFunction);
    if (mChannelIsValid(self))
        {
        CacheTimingModeSet(self, timingMode);
        ret = mMethodsGet(self)->TimingSet(self, timingMode, timingSource);
        }
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get timing mode of this channel
 *
 * @param self This channel
 *
 * @return @ref eAtTimingMode "Valid timing mode" on success or
 *              cAtTimingModeUnknown on failure
 */
eAtTimingMode AtChannelTimingModeGet(AtChannel self)
    {
    mAttributeGetWithCache(TimingModeGet, eAtTimingMode, cAtTimingModeUnknown);
    }

/**
 * Get timing source
 * @param self
 * @return
 */
AtChannel AtChannelTimingSourceGet(AtChannel self)
    {
    mNoParamObjectGet(TimingSourceGet, AtChannel);
    }

/**
 * Get clock state
 *
 * @param self This channel
 *
 * @return @ref eAtClockState "Clock states"
 */
eAtClockState AtChannelClockStateGet(AtChannel self)
    {
    mAttributeGet(ClockStateGet, eAtClockState, cAtClockStateUnknown);
    }

/**
 * Get alarm of this channel
 *
 * @param self This channel
 *
 * @return Current alarms. Note, channels of each module have predefined alarm type
 *         masks. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *         This API ORed all of happening alarms and return
 */
uint32 AtChannelAlarmGet(AtChannel self)
    {
    if (!mChannelIsValid(self))
        return 0;

    return DefectAlarmGet(self, mMethodsGet(self)->AlarmGet);
    }

/**
 * Get defect of this channel
 *
 * @param self This channel
 *
 * @return Current defects. Note, channels of each module have predefined defect/alarm type
 *         masks. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *         This API ORed all of happening defects and return
 */
uint32 AtChannelDefectGet(AtChannel self)
    {
    if (!mChannelIsValid(self))
        return 0;

    return DefectAlarmGet(self, mMethodsGet(self)->DefectGet);
    }

/**
 * Get defect interrupt change status. When there is any change in defect from clear to
 * raise or vice versa from raise to clear, corresponding alarm bit is set to 1
 * and kept that state until AtChannelDefectInterruptClear() is called. If
 * interrupt is enabled by AtChannelInterruptMaskSet(), the global interrupt pin
 * will be triggered to let CPU know
 *
 * @param self This channel
 *
 * @return Defect interrupt change status. Note, channels of each module have predefined alarm
 *         masks. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *         This API ORed all of alarms that changed their state in the past
 */
uint32 AtChannelDefectInterruptGet(AtChannel self)
    {
    if (!mChannelIsValid(self))
        return 0;

    return DefectAlarmGet(self, mMethodsGet(self)->DefectHistoryGet);
    }

/**
 * Clear defect history
 *
 * @param self This channel
 *
 * @return Defect history before clearing
 */
uint32 AtChannelDefectInterruptClear(AtChannel self)
    {
    if (!mChannelIsValid(self))
        return 0;

    return DefectAlarmGet(self, mMethodsGet(self)->DefectHistoryClear);
    }

/**
 * To clear interrupt of specific defects
 *
 * @param self This channel
 * @param defectTypes  Defects to clear interrupt. Note, channels of each module
 *        have predefined defect masks. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *
 * @return Interrupt status before clearing
 */
uint32 AtChannelSpecificDefectInterruptClear(AtChannel self, uint32 defectTypes)
    {
    if (self)
        return mMethodsGet(self)->SpecificDefectInterruptClear(self, defectTypes);
    return 0;
    }

/**
 * Get alarm interrupt change status. When there is any change in alarm from clear to
 * raise or vice versa from raise to clear, corresponding alarm bit is set to 1
 * and kept that state until AtChannelAlarmInterruptClear() is called. If
 * interrupt is enabled by AtChannelInterruptMaskSet(), the global interrupt pin
 * will be triggered to let CPU know
 *
 * @param self This channel
 *
 * @return Alarm interrupt change status. Note, channels of each module have predefined alarm
 *         masks. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *
 *         This API ORed all of alarms that changed their state in the past
 */
uint32 AtChannelAlarmInterruptGet(AtChannel self)
    {
    if (!mChannelIsValid(self))
        return 0;

    return DefectAlarmGet(self, mMethodsGet(self)->AlarmHistoryGet);
    }

/**
 * Clear alarm history
 *
 * @param self This channel
 *
 * @return Alarm history before clearing
 */
uint32 AtChannelAlarmInterruptClear(AtChannel self)
    {
    if (!mChannelIsValid(self))
        return 0;

    return DefectAlarmGet(self, mMethodsGet(self)->AlarmHistoryClear);
    }

/**
 * Set interrupt mask.
 *
 * @param self This channel
 * @param defectMask Mask of defects that will be configured
 * @param enableMask Interrupt mask of each defect
 *
 * @return AT return code.
 *
 * @note Channels of each module have predefined alarm masks. Refer:
 *       - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *       - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *       - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *       - @ref eAtPwAlarmType "Pseudowire alarm types"
 *       - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 */
eAtRet AtChannelInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 unsupportedMasks;

    if (defectMask == 0)
        return cAtOk;

    if (!mChannelIsValid(self))
        {
        AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    unsupportedMasks = ~AtChannelSupportedInterruptMasks(self);
    unsupportedMasks &= (defectMask & enableMask);
    if (unsupportedMasks)
        {
        AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation,
                     "does not support interrupt mask 0x%x\r\n", unsupportedMasks);
        return cAtErrorModeNotSupport;
        }

    mTwoParamsAttributeSetWithCache(InterruptMaskSet, defectMask, enableMask);
    }

/**
 * Get interrupt mask
 *
 * @param self This channel
 *
 * @return Interrupt mask
 */
uint32 AtChannelInterruptMaskGet(AtChannel self)
    {
    mAttributeGetWithCache(InterruptMaskGet, uint32, 0);
    }

/**
 * Check if an interrupt mask is supported or not.
 *
 * @param self This channel
 * @param defectMask Mask of defects that will be configured
 *
 * @retval cAtTrue if the interrupt mask is supported.
 *         cAtFalse if the interrupt mask is not supported.
 *
 * @note Channels of each module have predefined alarm masks. Refer:
 *       - @ref eAtPdhDe1AlarmType "PDH DS1/E1 alarm types"
 *       - @ref eAtPdhDe3AlarmType "PDH DS3/E3 alarm types"
 *       - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *       - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *       - @ref eAtPwAlarmType "Pseudowire alarm types"
 *       - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 */
eBool AtChannelInterruptMaskIsSupported(AtChannel self, uint32 defectMask)
    {
    if (!mChannelIsValid(self))
        {
        AtChannelLog(self, cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    if (defectMask == 0)
        return cAtTrue;

    if (defectMask & AtChannelSupportedInterruptMasks(self))
        return cAtTrue;

    return cAtFalse;
    }

/**
 * Enable interrupt.
 *
 * @param self This channel
 *
 * @return AT return code.
 */
eAtRet AtChannelInterruptEnable(AtChannel self)
    {
    mNoParamCall(InterruptEnable, eAtRet, cAtErrorObjectNotExist);
    }

/**
 * Disable interrupt.
 *
 * @param self This channel
 *
 * @return AT return code.
 */
eAtRet AtChannelInterruptDisable(AtChannel self)
    {
    mNoParamCall(InterruptDisable, eAtRet, cAtErrorObjectNotExist);
    }

/**
 * Get interrupt enabled state
 *
 * @param self This channel
 *
 * @return Interrupt enabled state
 */
eBool AtChannelInterruptIsEnabled(AtChannel self)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->InterruptIsEnabled(self);

    return cAtFalse;
    }

/**
 * Force TX alarm
 *
 * @param self This channel
 * @param alarmType Alarm to force. Note, channels of each module have predefined
 *        alarm types. Refer:
 *        - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *        - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *        - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *        - @ref eAtPwAlarmType "Pseudowire alarm types"
 *        - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *
 * @return AT return codes
 */
eAtRet AtChannelTxAlarmForce(AtChannel self, uint32 alarmType)
    {
    mNumericalAttributeSetWithCache(TxAlarmForce, alarmType);
    }

/**
 * Un-Force TX alarm
 *
 * @param self This channel
 * @param alarmType Alarm to un-force. Note, channels of each module have predefined
 *        alarm types. Refer:
 *        - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *        - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *        - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *        - @ref eAtPwAlarmType "Pseudowire alarm types"
 *        - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *
 * @return AT return codes
 */
eAtRet AtChannelTxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    mNumericalAttributeSetWithCache(TxAlarmUnForce, alarmType);
    }

/**
 * Get alarms that are being forced at TX side
 *
 * @param self This channel
 *
 * @return Alarms that are being forced. Note, channels of each module have predefined
 *         alarm types. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 */
uint32 AtChannelTxForcedAlarmGet(AtChannel self)
    {
    mAttributeGetWithCache(TxForcedAlarmGet, uint32, 0);
    }

/**
 * Get forcible alarms at TX side
 *
 * @param self This channel
 *
 * @return Alarms that can be forced. Note, channels of each module have predefined
 *         alarm types. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 */
uint32 AtChannelTxForcableAlarmsGet(AtChannel self)
    {
    mAttributeGet(TxForcibleAlarmsGet, uint32, 0);
    }

/**
 * Force RX alarm
 *
 * @param self This channel
 * @param alarmType Alarm to force. Note, channels of each module have predefined
 *        alarm types. Refer:
 *        - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *        - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *        - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *        - @ref eAtPwAlarmType "Pseudowire alarm types"
 *        - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *
 * @return AT return codes
 */
eAtRet AtChannelRxAlarmForce(AtChannel self, uint32 alarmType)
    {
    mNumericalAttributeSetWithCache(RxAlarmForce, alarmType);
    }

/**
 * Un-Force RX alarm
 *
 * @param self This channel
 * @param alarmType Alarm to un-force. Note, channels of each module have predefined
 *        alarm types. Refer:
 *        - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *        - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *        - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *        - @ref eAtPwAlarmType "Pseudowire alarm types"
 *        - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 *
 * @return AT return codes
 */
eAtRet AtChannelRxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    mNumericalAttributeSetWithCache(RxAlarmUnForce, alarmType);
    }

/**
 * Get alarms that are being forced at RX side
 *
 * @param self This channel
 *
 * @return Alarms that are being forced. Note, channels of each module have predefined
 *         alarm types. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 */
uint32 AtChannelRxForcedAlarmGet(AtChannel self)
    {
    mAttributeGetWithCache(RxForcedAlarmGet, uint32, 0);
    }

/**
 * Get forcible alarms at RX side
 *
 * @param self This channel
 *
 * @return Alarms that can be forced. Note, channels of each module have predefined
 *         alarm types. Refer:
 *         - @ref eAtPdhDe1AlarmType "PDH alarm types"
 *         - @ref eAtSdhLineAlarmType "SDH Line alarm types"
 *         - @ref eAtSdhPathAlarmType "SDH Path alarm types"
 *         - @ref eAtPwAlarmType "Pseudowire alarm types"
 *         - @ref eAtEthPortAlarmType "Ethernet port alarm types"
 */
uint32 AtChannelRxForcableAlarmsGet(AtChannel self)
    {
    mAttributeGet(RxForcibleAlarmsGet, uint32, 0);
    }

/**
 * Force error on a channel at TX direction
 *
 * @param self This channel
 * @param errorType Error type to force. Note, channels of each module have
 *        predefined error types. Refer the following for example:
 *        - @ref eAtSdhLineErrorType "SDH Line error types"
 *        - @ref eAtSdhPathErrorType "SDH Path error types"
 *        - @ref eAtPdhDe3ErrorType  "PDH DS3/E3 error types"
 *        - @ref eAtPdhDe1ErrorType  "PDH DS1/E1 error types"
 *
 * @return AT return codes
 */
eAtRet AtChannelTxErrorForce(AtChannel self, uint32 errorType)
    {
    mNumericalAttributeSet(TxErrorForce, errorType);
    }

/**
 * Unforce error on a channel at TX direction
 *
 * @param self This channel
 * @param errorType Error type to unforce. Note, channels of each module have
 *        predefined error types. Refer the following for example:
 *        - @ref eAtSdhLineErrorType "SDH Line error types"
 *        - @ref eAtSdhPathErrorType "SDH Path error types"
 *        - @ref eAtPdhDe3ErrorType  "PDH DS3/E3 error types"
 *        - @ref eAtPdhDe1ErrorType  "PDH DS1/E1 error types"
 *
 * @return AT return codes
 */
eAtRet AtChannelTxErrorUnForce(AtChannel self, uint32 errorType)
    {
    mNumericalAttributeSet(TxErrorUnForce, errorType);
    }

/**
 * Get error that being forced at TX direction
 *
 * @param self This channel
 *
 * @return Errors that are being forced. Note, channels of each module have
 *         predefined error types. Refer the following for example:
 *        - @ref eAtSdhLineErrorType "SDH Line error types"
 *        - @ref eAtSdhPathErrorType "SDH Path error types"
 *        - @ref eAtPdhDe3ErrorType  "PDH DS3/E3 error types"
 *        - @ref eAtPdhDe1ErrorType  "PDH DS1/E1 error types"
 */
uint32 AtChannelTxForcedErrorGet(AtChannel self)
    {
    mAttributeGet(TxForcedErrorGet, uint32, 0);
    }

/**
 * Get force-able error at TX direction
 *
 * @param self This channel
 *
 * @return Errors that can be forced. Note, channels of each module have
 *         predefined error types. Refer the following for example:
 *        - @ref eAtSdhLineErrorType "SDH Line error types"
 *        - @ref eAtSdhPathErrorType "SDH Path error types"
 *        - @ref eAtPdhDe3ErrorType  "PDH DS3/E3 error types"
 *        - @ref eAtPdhDe1ErrorType  "PDH DS1/E1 error types"
 */
uint32 AtChannelTxForcableErrorsGet(AtChannel self)
    {
    mAttributeGet(TxForcableErrorsGet, uint32, 0);
    }

/**
 * Set loopback
 *
 * @param self This channel
 * @param loopbackMode Loopback mode. Refer @ref eAtLoopbackMode
 *        "Common loopback mode". Note, channels of each module may have
 *        predefined loopback modes, for example @ref eAtPdhLoopbackMode
 *        "PDH channel loopback mode"
 *
 * @return AT return codes
 */
eAtRet AtChannelLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtRet ret;

    if (!AtChannelLoopbackIsSupported(self, loopbackMode))
        return cAtErrorModeNotSupport;

    mOneParamApiLogStart(loopbackMode);
    AtObjectNotifyPropertyWillChange((AtObject)self, cAtChannelPropertyIdLoopback, loopbackMode);
    ret = LoopbackSetWithLogger(self, loopbackMode);
    if (ret == cAtOk)
        AtObjectNotifyPropertyDidChange((AtObject)self, cAtChannelPropertyIdLoopback, loopbackMode);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get loopback mode
 *
 * @param self This channel
 *
 * @return Loopback mode. Refer @ref eAtLoopbackMode "Common loopback mode".
 *         Note, channels of each module may have predefined loopback modes,
 *         for example @ref eAtPdhLoopbackMode "PDH channel loopback mode"
 */
uint8 AtChannelLoopbackGet(AtChannel self)
    {
    mAttributeGetWithCache(LoopbackGet, uint8, 0);
    }

/**
 * Check if a loopback mode is supported
 *
 * @param self This channel
 * @param loopbackMode Loopback mode.
 *
 * @return cAtTrue if loopback mode is supported. Otherwise, cAtFalse is returned
 */
eBool AtChannelLoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    mOneParamAttributeGet(LoopbackIsSupported, loopbackMode, eBool, cAtFalse);
    }

/**
 * Translate input loopback mode to string
 *
 * @param self This channel
 * @param loopbackMode Loopback mode
 *
 * @return Loopback mode string
 */
const char *AtChannelLoopbackMode2String(AtChannel self, uint8 loopbackMode)
    {
    if (self)
        return mMethodsGet(self)->LoopbackMode2String(self, loopbackMode);
    return NULL;
    }

/**
 * Get performance counter
 *
 * @param self This channel
 * @param counterType Counter to get. Channels of each module have predefined
 *        counter types. For example:
 *        - @ref eAtPdhDe1CounterType "PDH DS1/E1 counter types"
 *        - TBD
 *
 * @return Counter value
 */
uint32 AtChannelCounterGet(AtChannel self, uint16 counterType)
    {
    if (!mChannelIsValid(self))
        return 0;

    if (AtDeviceIsEjected(AtChannelDeviceGet(self)))
        return 0;

    return mMethodsGet(self)->CounterGet(self, counterType);
    }

/**
 * Clear performance counter
 *
 * @param self This channel
 * @param counterType Counter to clear. Channels of each module have predefined
 *        counter types. For example:
 *        - @ref eAtPdhDe1CounterType "PDH DS1/E1 counter types"
 *        - TBD
 *
 * @return Value of counter before clearing
 */
uint32 AtChannelCounterClear(AtChannel self, uint16 counterType)
    {
    if (!mChannelIsValid(self))
        return 0;

    if (AtDeviceIsEjected(AtChannelDeviceGet(self)))
        return 0;

    return mMethodsGet(self)->CounterClear(self, counterType);
    }

/**
 * Latch all of hardware counters for reading later. Products that sync counters
 * every predefined of time (1s, for example) need to latch all counters before
 * accessing individual counters. Most of products do not require counter latching
 * and calling latching API just does nothing.
 *
 * There is no issue when calling this API on all products.
 *
 * @param self This channel
 * @param clear Clear hardware counters after latching
 *              - cAtTrue: after latching, hardware counters will be clear
 *              - cAtFalse: just latch, hardware continue counts from current values
 *
 * @return AT return code
 */
eAtRet AtChannelAllCountersLatchAndClear(AtChannel self, eBool clear)
    {
    if (self)
        return mMethodsGet(self)->AllCountersLatchAndClear(self, clear);
    return cAtErrorNullPointer;
    }

/**
 * Get all of performance counters.
 *
 * @param self This channel
 * @param counters Data structure to hold all of counters. Each kind of channel
 *                 has corresponding structure to hold all of counters.
 *
 * @return AT return code
 */
eAtRet AtChannelAllCountersGet(AtChannel self, void *counters)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->AllCountersGet(self, counters);

    return cAtError;
    }

/**
 * Clear all of performance counters
 *
 * @param self This channel
 * @param counters Data structure to hold all of counters. Each kind of channel
 *                 has corresponding structure to hold all of counters.
 *
 * @return AT return code
 */
eAtRet AtChannelAllCountersClear(AtChannel self, void *counters)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->AllCountersClear(self, counters);

    return cAtError;
    }

/**
 * Set all configuration parameters
 *
 * @param self This channel
 * @param pAllConfig All configuration parameters. Each kind of channel will have
 *                corresponding data structure to hold all of configuration
 * @return AT return codes
 */
eAtRet AtChannelAllConfigSet(AtChannel self, void* pAllConfig)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->AllConfigSet(self, pAllConfig);
    return cAtError;
    }

/**
 * Get all configuration parameters
 *
 * @param self This channel
 * @param pAllConfig All configuration parameters. Each kind of channel will have
 *                corresponding data structure to hold all of configuration
 * @return
 */
eAtRet AtChannelAllConfigGet(AtChannel self, void* pAllConfig)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->AllConfigGet(self, pAllConfig);
    return cAtError;
    }

/**
 * Add event listener. When there is any alarm or OAM message come, corresponding
 * interface will be called. Default listener interface is @ref tAtChannelEventListener
 * Note, each channel type can have its own event listener interface, so this
 * default listener interface must not be used.
 *
 * Application just implemented functions defined in event listener interface in
 * order to receiving event.
 *
 * @param self This channel
 * @param listener Event listener
 *
 * @return AT return code
 */
eAtRet AtChannelEventListenerAdd(AtChannel self, tAtChannelEventListener *listener)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->EventListenerAdd(self, listener);

    return cAtError;
    }

/**
 * Add event listener with associated user data. When there is any alarm or OAM
 * message come, corresponding interface will be called and user data will be
 * input when callback get called.
 *
 * Application just implemented functions defined in event listener interface in
 * order to receiving event.
 *
 * @param self This channel
 * @param listener Event listener
 * @param userData User data that will be input when callback is called.
 *
 * @return AT return code
 */
eAtRet AtChannelEventListenerAddWithUserData(AtChannel self, tAtChannelEventListener *listener, void *userData)
    {
    if (self)
        return EventListenerAddWithUserData(self, listener, userData);
    return cAtErrorNullPointer;
    }

/**
 * Remove event listener
 *
 * @param self This channel
 * @param listener Event listener
 *
 * @return AT return code
 */
eAtRet AtChannelEventListenerRemove(AtChannel self, tAtChannelEventListener *listener)
    {
    /* Search to find listener match with input information */
    AtChannelEventListener aListener;
    AtIterator iterator;

    if (!mChannelIsValid(self))
        return cAtErrorNullPointer;

    if (self->allEventListeners == NULL)
        return cAtOk;

    iterator = AtListIteratorCreate(self->allEventListeners);

    AtOsalMutexLock(self->listenerListMutex);

    /* For all event listeners */
    while((aListener = (AtChannelEventListener)AtIteratorNext(iterator)) != NULL)
        {
        if (!TwoListenersAreIdentical(aListener, listener))
            continue;

        /* Remove current object out of list */
        AtIteratorRemove(iterator);

        /* Delete listener object */
        AtObjectDelete((AtObject)aListener);
        }

    AtOsalMutexUnLock(self->listenerListMutex);

    AtObjectDelete((AtObject)iterator);
    return cAtOk;
    }

/**
 * Get encapsulation channel which is bound to this channel
 *
 * @param self This channel
 * @return Bound encapsulation channel
 */
AtEncapChannel AtChannelBoundEncapChannelGet(AtChannel self)
    {
    mNoParamObjectGet(BoundEncapChannelGet, AtEncapChannel);
    }

/**
 * Get pseudowire which is bound to this channel
 *
 * @param self This channel
 * @return Bound pseudowire
 */
AtPw AtChannelBoundPwGet(AtChannel self)
    {
    mNoParamObjectGet(BoundPseudowireGet, AtPw);
    }

/**
 * Show all of information of channel including:
 * - Counters
 * - Configuration
 * - Alarm status
 * - Alarm history
 *
 * @param self This channel
 * @note All of status will be clear by this API after displaying
 */
void AtChannelDebug(AtChannel self)
    {
    if (mChannelIsValid(self))
        mMethodsGet(self)->Debug(self);
    }

/**
 * Clear status channel. All of counters and interrupt history will be cleared.
 *
 * @param self This channel
 */
void AtChannelStatusClear(AtChannel self)
    {
    if (mChannelIsValid(self))
        mMethodsGet(self)->StatusClear(self);
    }

/**
 * Get PRBS engine associated with this channel
 *
 * @param self This channel
 * @return AtPrbsEngine object if this channel support PRBS, otherwise, NULL is
 *         returned
 */
AtPrbsEngine AtChannelPrbsEngineGet(AtChannel self)
    {
    mNoParamObjectGet(PrbsEngineGet, AtPrbsEngine);
    }

/**
 * Enable/disable TX
 *
 * @param self This channel
 * @param enable Channel enabling.
 *               - cAtTrue: Enable
 *               - cAtFalse: Disable
 *
 * @return AT return code
 */
eAtRet AtChannelTxEnable(AtChannel self, eBool enable)
    {
    if (self == NULL)
        mLogNullPointer();

    if (!mMethodsGet(self)->TxCanEnable(self, enable))
        mAttributeErrorLog(enable, cAtErrorModeNotSupport);

    mNumericalAttributeSetWithCache(TxEnable, enable);
    }

/**
 * Check if TX is enabled or not
 *
 * @param self This channel
 *
 * @return cAtTrue if it is enabled or cAtFalse if it is disabled
 */
eBool AtChannelTxIsEnabled(AtChannel self)
    {
    mAttributeGetWithCache(TxIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable RX
 *
 * @param self This channel
 * @param enable Channel enabling.
 *               - cAtTrue: Enable
 *               - cAtFalse: Disable
 *
 * @return AT return codes
 */
eAtRet AtChannelRxEnable(AtChannel self, eBool enable)
    {
    if (self == NULL)
        mLogNullPointer();

    if (!mMethodsGet(self)->RxCanEnable(self, enable))
        mAttributeErrorLog(enable, cAtErrorModeNotSupport);

    mNumericalAttributeSetWithCache(RxEnable, enable);
    }

/**
 * Check if RX is enabled or not.
 *
 * @param self This channel
 *
 * @return cAtTrue if it is enabled or cAtFalse if it is disabled
 */
eBool AtChannelRxIsEnabled(AtChannel self)
    {
    mAttributeGetWithCache(RxIsEnabled, eBool, cAtFalse);
    }

/**
 * Check if a counter type is supported
 *
 * @param self This channel
 * @param counterType Counter to get. Channels of each module have predefined
 *        counter types. For example:
 *        - @ref eAtPdhDe1CounterType "PDH DS1/E1 counter types"
 *        - TBD
 *
 * @return Counter value
 */
eBool AtChannelCounterIsSupported(AtChannel self, uint16 counterType)
    {
    mOneParamAttributeGet(CounterIsSupported, counterType, eBool, cAtFalse);
    }

/**
 * Get Surveillance engine from channel
 *
 * @param self This channel
 *
 * @return Surveillance engine
 */
AtSurEngine AtChannelSurEngineGet(AtChannel self)
    {
    mNoParamObjectGet(SurEngineGet, AtSurEngine);
    }

/**
 * Enable message log for channel
 *
 * @param self This channel
 * @param enable Enable/disable message logging
 * @return At return code
 */
eAtRet AtChannelLogEnable(AtChannel self, eBool enable)
    {
    if (!self)
        return cAtErrorNullPointer;

    self->logIsEnabled = enable;
    return cAtOk;
    }

/**
 * Check if message logging of this channel is enabled
 *
 * @param self This channel
 *
 * @return cAtTrue if it is enabled or cAtFalse if it is disabled
 */
eBool AtChannelLogIsEnabled(AtChannel self)
    {
    if (self)
        return self->logIsEnabled;

    return cAtFalse;
    }

/**
 * Show logged message of channel
 * @param self This channel
 */
void AtChannelLogShow(AtChannel self)
    {
    AtDriverLogLock();
    AtLoggerShowMessageContainSubString(AtSharedDriverLoggerGet(), AtDriverObjectStringToSharedBuffer((AtObject)self));
    AtDriverLogUnLock();
    }

/**
 * Check if an alarm type is supported
 *
 * @param self This channel
 * @param alarmType Alarm to check. Channels of each module have predefined
 *        alarm types
 * @return cAtTrue if alarm is supported or cAtFalse if it is not supported
 */
eBool AtChannelAlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    mOneParamAttributeGet(AlarmIsSupported, alarmType, eBool, cAtFalse);
    }

/**
 * @}
 */

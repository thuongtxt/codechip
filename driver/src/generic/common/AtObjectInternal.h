/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtObjectInternal.h
 * 
 * Created Date: Aug 24, 2012
 *
 * Author      : namnn
 * 
 * Description : AtObject class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOBJECTINTERNAL_H_
#define _ATOBJECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"

#include "AtCommon.h"
#include "AtObject.h"

#include "../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtObjectNotifyPropertyWillChange(AtObject self, uint32 propertyId, AtSize value);
void AtObjectNotifyPropertyDidChange(AtObject self, uint32 propertyId, AtSize value);

#ifdef __cplusplus
}
#endif
#endif /* _ATOBJECTINTERNAL_H_ */


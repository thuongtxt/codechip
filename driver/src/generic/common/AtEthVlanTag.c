/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtEthVlanTag.c
 *
 * Created Date: Apr 3, 2015
 *
 * Description : VLAN tag
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtEthVlanTag.h"
#include "AtEthVlanTagInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
uint32 AtEthVlanTagToUint32(const tAtEthVlanTag *vlanTag)
    {
    if (vlanTag != NULL)
        return (uint32) ((vlanTag->priority << 13) + (vlanTag->cfi << 12) + vlanTag->vlanId);
    else
        return cInvalidUint32;
    }

tAtEthVlanTag *AtEthVlanTagFromUint32(uint32 vlanValue, tAtEthVlanTag *vlanTag)
    {
    if (vlanTag != NULL)
        {
        vlanTag->priority = (vlanValue >> 13) & 0x7;
        vlanTag->cfi = (vlanValue >> 12) & 0x1;
        vlanTag->vlanId = vlanValue & cBit11_0;
        return vlanTag;
        }
    else
        return NULL;
    }

/**
 * Construct a VLAN tag
 *
 * @param priority Priority field
 * @param cfi CFI field
 * @param vlanId VLAN ID field
 *
 * @param [out] vlanTag VLAN tag data structure
 *
 * @return VLAN tag structure on success or null on failure
 */
tAtEthVlanTag *AtEthVlanTagConstruct(uint8 priority, uint8 cfi, uint16 vlanId, tAtEthVlanTag *vlanTag)
    {
    if (vlanTag != NULL)
        {
        vlanTag->cfi = cfi;
        vlanTag->priority = priority;
        vlanTag->vlanId = vlanId;
        return vlanTag;
        }
    else
        return NULL;
    }

/**
 * Get C-VLAN of VLAN descriptor
 *
 * @param descriptor VLAN descriptor
 *
 * @return C-VLAN on success or NULL on failure
 */
tAtEthVlanTag *AtEthVlanDescCVlan(tAtEthVlanDesc *descriptor)
    {
    if (descriptor == NULL)
        return NULL;

    if (descriptor->numberOfVlans == 0)
        return NULL;

    return &(descriptor->vlans[0]);
    }

/**
 * Get S-VLAN of VLAN descriptor
 *
 * @param descriptor VLAN descriptor
 *
 * @return S-VLAN on success or NULL on failure
 */
tAtEthVlanTag *AtEthVlanDescSVlan(tAtEthVlanDesc *descriptor)
    {
    if (descriptor == NULL)
        return NULL;

    if (descriptor->numberOfVlans <= 1)
        return NULL;

    return &(descriptor->vlans[1]);
    }

/**
 * Construct a VLAN traffic descriptor
 *
 * @param port Ethernet port ID
 * @param sVlan S-VLAN
 * @param cVlan C-VLAN
 * @param [out] desc Traffic descriptor
 *
 * @return Traffic descriptor on success or null on failure
 */
tAtEthVlanDesc *AtEthFlowVlanDescConstruct(uint8 port, tAtEthVlanTag *sVlan, tAtEthVlanTag *cVlan, tAtEthVlanDesc *desc)
    {
    desc->numberOfVlans = 0;
    desc->ethPortId = port;
    if (cVlan != NULL)
        {
        AtEthVlanTagConstruct(cVlan->priority, cVlan->cfi, cVlan->vlanId, &desc->vlans[0]);
        desc->numberOfVlans++;
        }
    else
        return NULL;

    if (sVlan != NULL)
        {
        AtEthVlanTagConstruct(sVlan->priority, sVlan->cfi, sVlan->vlanId, &desc->vlans[1]);
        desc->numberOfVlans++;
        }

    return desc;
    }


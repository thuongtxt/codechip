/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmSesThresholdProfileInternal.h
 * 
 * Created Date: Aug 25, 2017
 *
 * Description : SES threshold profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMSESTHRESHOLDPROFILEINTERNAL_H_
#define _ATPMSESTHRESHOLDPROFILEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Superclass */
#include "AtModuleSur.h"
#include "AtPmSesThresholdProfile.h"
#include "../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPmSesThresholdProfileMethods
    {
    /* Line thresholds */
    eAtRet (*SdhLineThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*SdhLineThresholdGet)(AtPmSesThresholdProfile self);

    /* Section thresholds */
    eAtRet (*SdhSectionThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*SdhSectionThresholdGet)(AtPmSesThresholdProfile self);

    /* HO path thresholds */
    eAtRet (*SdhHoPathThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*SdhHoPathThresholdGet)(AtPmSesThresholdProfile self);

    /* LO path thresholds */
    eAtRet (*SdhLoPathThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*SdhLoPathThresholdGet)(AtPmSesThresholdProfile self);

    /* DS3/E3 Line thresholds */
    eAtRet (*PdhDe3LineThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*PdhDe3LineThresholdGet)(AtPmSesThresholdProfile self);

    /* DS3/E3 Path thresholds */
    eAtRet (*PdhDe3PathThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*PdhDe3PathThresholdGet)(AtPmSesThresholdProfile self);

    /* DS1/E1 Line thresholds */
    eAtRet (*PdhDe1LineThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*PdhDe1LineThresholdGet)(AtPmSesThresholdProfile self);

    /* DS1/E1 Path thresholds */
    eAtRet (*PdhDe1PathThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*PdhDe1PathThresholdGet)(AtPmSesThresholdProfile self);

    /* PW thresholds */
    eAtRet (*PwThresholdSet)(AtPmSesThresholdProfile self, uint32 threshold);
    uint32 (*PwThresholdGet)(AtPmSesThresholdProfile self);

    /* Internal methods */
    eAtRet (*Init)(AtPmSesThresholdProfile self);
    }tAtPmSesThresholdProfileMethods;

typedef struct tAtPmSesThresholdProfile
    {
    tAtObject super;
    const tAtPmSesThresholdProfileMethods *methods;

    /* Private data */
    AtModuleSur surModule;
    uint32 profileId;
    }tAtPmSesThresholdProfile;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmSesThresholdProfile AtPmSesThresholdProfileObjectInit(AtPmSesThresholdProfile self, AtModuleSur module, uint32 profileId);
eAtRet AtPmSesThresholdProfileInit(AtPmSesThresholdProfile self);
AtModuleSur AtPmSesThresholdProfileModuleGet(AtPmSesThresholdProfile self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPMSESTHRESHOLDPROFILEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmThresholdProfileInternal.h
 * 
 * Created Date: Sep 9, 2016
 *
 * Description : PM Threshold Profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMTHRESHOLDPROFILEINTERNAL_H_
#define _ATPMTHRESHOLDPROFILEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPmThresholdProfile.h"
#include "../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPmThresholdProfileMethods
    {
    /* Line thresholds */
    eAtRet (*SdhLinePeriodTcaThresholdSet)(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param, uint32 threshold);
    uint32 (*SdhLinePeriodTcaThresholdGet)(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param);

    /* HO path thresholds */
    eAtRet (*SdhHoPathPeriodTcaThresholdSet)(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold);
    uint32 (*SdhHoPathPeriodTcaThresholdGet)(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param);

    /* LO path thresholds */
    eAtRet (*SdhLoPathPeriodTcaThresholdSet)(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold);
    uint32 (*SdhLoPathPeriodTcaThresholdGet)(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param);

    /* DS3/E3 thresholds */
    eAtRet (*PdhDe3PeriodTcaThresholdSet)(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param, uint32 threshold);
    uint32 (*PdhDe3PeriodTcaThresholdGet)(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param);

    /* DS1/E1 thresholds */
    eAtRet (*PdhDe1PeriodTcaThresholdSet)(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param, uint32 threshold);
    uint32 (*PdhDe1PeriodTcaThresholdGet)(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param);

    /* PW thresholds */
    eAtRet (*PwPeriodTcaThresholdSet)(AtPmThresholdProfile self, eAtSurEnginePwPmParam param, uint32 threshold);
    uint32 (*PwPeriodTcaThresholdGet)(AtPmThresholdProfile self, eAtSurEnginePwPmParam param);

    /* To Set Default All Threshold */
    eAtRet (*Init)(AtPmThresholdProfile self);
    }tAtPmThresholdProfileMethods;

typedef struct tAtPmThresholdProfile
    {
    tAtObject super;
    const tAtPmThresholdProfileMethods *methods;

    /* Private data */
    AtModuleSur surModule;
    uint32 profileId;
    }tAtPmThresholdProfile;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmThresholdProfile AtPmThresholdProfileObjectInit(AtPmThresholdProfile self, AtModuleSur module, uint32 profileId);
eAtRet AtPmThresholdProfileInit(AtPmThresholdProfile self);
AtModuleSur AtPmThresholdProfileModuleGet(AtPmThresholdProfile self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPMTHRESHOLDPROFILEINTERNAL_H_ */


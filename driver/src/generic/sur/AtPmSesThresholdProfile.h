/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmSesThresholdProfile.h
 * 
 * Created Date: Aug 25, 2017
 *
 * Description : SES Threshold profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMSESTHRESHOLDPROFILE_H_
#define _ATPMSESTHRESHOLDPROFILE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPmSesThresholdProfile
 * @{
 */

/**
 * @brief SES threshold profile
 */
typedef struct tAtPmSesThresholdProfile *AtPmSesThresholdProfile;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtPmSesThresholdProfileIdGet(AtPmSesThresholdProfile self);

/* Line thresholds */
eAtRet AtPmSesThresholdProfileSdhLineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfileSdhLineThresholdGet(AtPmSesThresholdProfile self);

/* Section thresholds */
eAtRet AtPmSesThresholdProfileSdhSectionThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfileSdhSectionThresholdGet(AtPmSesThresholdProfile self);

/* HO path thresholds */
eAtRet AtPmSesThresholdProfileSdhHoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfileSdhHoPathThresholdGet(AtPmSesThresholdProfile self);

/* LO path thresholds */
eAtRet AtPmSesThresholdProfileSdhLoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfileSdhLoPathThresholdGet(AtPmSesThresholdProfile self);

/* DS3/E3 Line thresholds */
eAtRet AtPmSesThresholdProfilePdhDe3LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfilePdhDe3LineThresholdGet(AtPmSesThresholdProfile self);

/* DS3/E3 Path thresholds */
eAtRet AtPmSesThresholdProfilePdhDe3PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfilePdhDe3PathThresholdGet(AtPmSesThresholdProfile self);

/* DS1/E1 Line thresholds */
eAtRet AtPmSesThresholdProfilePdhDe1LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfilePdhDe1LineThresholdGet(AtPmSesThresholdProfile self);

/* DS1/E1 Path thresholds */
eAtRet AtPmSesThresholdProfilePdhDe1PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfilePdhDe1PathThresholdGet(AtPmSesThresholdProfile self);

/* PW thresholds */
eAtRet AtPmSesThresholdProfilePwThresholdSet(AtPmSesThresholdProfile self, uint32 threshold);
uint32 AtPmSesThresholdProfilePwThresholdGet(AtPmSesThresholdProfile self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPMSESTHRESHOLDPROFILE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtModuleSurInternal.h
 * 
 * Created Date: Mar 17, 2015
 *
 * Description : Surveillance module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESURINTERNAL_H_
#define _ATMODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtModulePdh.h"
#include "AtModulePw.h"
#include "AtModuleSur.h"
#include "AtSurEngine.h"
#include "AtPmSesThresholdProfile.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSurMethods
    {
    eAtRet (*PeriodExpire)(AtModuleSur self);
    eAtRet (*PeriodAsyncExpire)(AtModuleSur self);
    uint32 (*PeriodElapsedSecondsGet)(AtModuleSur self);
    eAtRet (*ExpireMethodSet)(AtModuleSur self, eAtSurPeriodExpireMethod method);
    eAtSurPeriodExpireMethod (*ExpireMethodGet)(AtModuleSur self);
    eAtRet (*TickSourceSet)(AtModuleSur self, eAtSurTickSource source);
    eAtSurTickSource (*TickSourceGet)(AtModuleSur self);
    eAtRet (*ExpireNotificationEnable)(AtModuleSur self, eBool enable);
    eBool (*ExpireNotificationIsEnabled)(AtModuleSur self);

    /* Engine factory */
    AtSurEngine (*SdhLineEngineCreate)(AtModuleSur self, AtSdhLine line);
    AtSurEngine (*SdhHoPathEngineCreate)(AtModuleSur self, AtSdhPath path);
    AtSurEngine (*SdhLoPathEngineCreate)(AtModuleSur self, AtSdhPath path);
    AtSurEngine (*PdhDe1EngineCreate)(AtModuleSur self, AtPdhDe1 de1);
    AtSurEngine (*PdhDe3EngineCreate)(AtModuleSur self, AtPdhDe3 de3);
    AtSurEngine (*PwEngineCreate)(AtModuleSur self, AtPw pw);

    /* Engine deleting */
    eAtRet (*SdhLineEngineDelete)(AtModuleSur self, AtSurEngine line);
    eAtRet (*SdhHoPathEngineDelete)(AtModuleSur self, AtSurEngine path);
    eAtRet (*SdhLoPathEngineDelete)(AtModuleSur self, AtSurEngine path);
    eAtRet (*PdhDe1EngineDelete)(AtModuleSur self, AtSurEngine de1);
    eAtRet (*PdhDe3EngineDelete)(AtModuleSur self, AtSurEngine de3);
    eAtRet (*PwEngineDelete)(AtModuleSur self, AtSurEngine pw);

    /* Support */
    eBool (*LineIsSupported)(AtModuleSur self);
    eBool (*ShouldMaskFailuresWhenNonAutonomousFailures)(AtModuleSur self);
    eBool (*TcaIsSupported)(AtModuleSur self);

    /* Register factory */
    AtPmRegister (*CurrentSecondRegisterCreate)(AtModuleSur self, AtPmParam param);
    AtPmRegister (*CurrentPeriodRegisterCreate)(AtModuleSur self, AtPmParam param);
    AtPmRegister (*PreviousPeriodRegisterCreate)(AtModuleSur self, AtPmParam param);
    AtPmRegister (*CurrentDayRegisterCreate)(AtModuleSur self, AtPmParam param);
    AtPmRegister (*PreviousDayRegisterCreate)(AtModuleSur self, AtPmParam param);
    AtPmRegister (*RecentRegisterCreate)(AtModuleSur self, AtPmParam param, uint8 recentPeriod);

    /* Param factory */
    AtPmParam (*SdhLineParamCreate)(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
    AtPmParam (*SdhPathParamCreate)(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
    AtPmParam (*PdhDe3ParamCreate)(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
    AtPmParam (*PdhDe1ParamCreate)(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
    AtPmParam (*PwParamCreate)(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);

    /* PM Threshold Profile */
    AtPmThresholdProfile (*ThresholdProfileGet)(AtModuleSur self, uint32 profileId);
    AtPmThresholdProfile (*ThresholdProfileCreate)(AtModuleSur self, uint32 profileId);
    uint32 (*NumThresholdProfiles)(AtModuleSur self);
    eBool (*ShouldInitThresholdProfiles)(AtModuleSur self);

    /* SES Threshold Profile */
    AtPmSesThresholdProfile (*SesThresholdProfileGet)(AtModuleSur self, uint32 profileId);
    AtPmSesThresholdProfile (*SesThresholdProfileCreate)(AtModuleSur self, uint32 profileId);
    uint32 (*NumSesThresholdProfiles)(AtModuleSur self);

    /* Timers */
    eAtRet (*FailureHoldOffTimerSet)(AtModuleSur self, uint32 timerInMs);
    uint32 (*FailureHoldOffTimerGet)(AtModuleSur self);
    eBool (*FailureHoldOffTimerConfigurable)(AtModuleSur self);
    eAtRet (*FailureHoldOnTimerSet)(AtModuleSur self, uint32 timerInMs);
    uint32 (*FailureHoldOnTimerGet)(AtModuleSur self);
    eBool (*FailureHoldOnTimerConfigurable)(AtModuleSur self);
    eBool (*FailureIsSupported)(AtModuleSur self);
    }tAtModuleSurMethods;

typedef struct tAtModuleSur
    {
    tAtModule super;
    const tAtModuleSurMethods *methods;

    /* Private data */
    AtPmThresholdProfile *allProfiles;
    AtPmSesThresholdProfile *allSesProfiles;
    }tAtModuleSur;

typedef struct tAtModuleSurEventListenerWrapper * AtModuleSurEventListenerWrapper;
typedef struct tAtModuleSurEventListenerWrapper
    {
    tAtModuleEventListenerWrapper super;
    tAtModuleSurEventListener listener;
    }tAtModuleSurEventListenerWrapper;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur AtModuleSurObjectInit(AtModuleSur self, AtDevice device);

/* Engine creating */
AtSurEngine AtModuleSurSdhLineEngineCreate(AtModuleSur self, AtSdhLine line);
AtSurEngine AtModuleSurSdhHoPathEngineCreate(AtModuleSur self, AtSdhPath path);
AtSurEngine AtModuleSurSdhLoPathEngineCreate(AtModuleSur self, AtSdhPath path);
AtSurEngine AtModuleSurPdhDe3EngineCreate(AtModuleSur self, AtPdhDe3 de3);
AtSurEngine AtModuleSurPdhDe1EngineCreate(AtModuleSur self, AtPdhDe1 de1);
AtSurEngine AtModuleSurPwEngineCreate(AtModuleSur self, AtPw pw);

/* Engine deleting */
eAtRet AtModuleSurEngineSdhLineDelete(AtModuleSur self, AtSurEngine engine);
eAtRet AtModuleSurEngineSdhHoPathDelete(AtModuleSur self, AtSurEngine engine);
eAtRet AtModuleSurEngineSdhLoPathDelete(AtModuleSur self, AtSurEngine engine);
eAtRet AtModuleSurEnginePdhDe3Delete(AtModuleSur self, AtSurEngine engine);
eAtRet AtModuleSurEnginePdhDe1Delete(AtModuleSur self, AtSurEngine engine);
eAtRet AtModuleSurEnginePwDelete(AtModuleSur self, AtSurEngine engine);

/* Register factory */
AtPmRegister AtModuleSurCurrentSecondRegisterCreate(AtModuleSur self, AtPmParam param);
AtPmRegister AtModuleSurCurrentPeriodRegisterCreate(AtModuleSur self, AtPmParam param);
AtPmRegister AtModuleSurPreviousPeriodRegisterCreate(AtModuleSur self, AtPmParam param);
AtPmRegister AtModuleSurCurrentDayRegisterCreate(AtModuleSur self, AtPmParam param);
AtPmRegister AtModuleSurPreviousDayRegisterCreate(AtModuleSur self, AtPmParam param);
AtPmRegister AtModuleSurRecentRegisterCreate(AtModuleSur self, AtPmParam param, uint8 recentPeriod);

/* PM Parameter factory */
AtPmParam AtModuleSurSdhLineParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
AtPmParam AtModuleSurSdhPathParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
AtPmParam AtModuleSurPdhDe3ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
AtPmParam AtModuleSurPdhDe1ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);
AtPmParam AtModuleSurPwParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType);

/* SES threshold profile */
uint32 AtModuleSurNumSesThresholdProfiles(AtModuleSur self);
AtPmSesThresholdProfile AtModuleSurSesThresholdProfileGet(AtModuleSur self, uint32 profileId);

void AtModuleSurAllEventListenersCall(AtModuleSur self);

/* Supported features */
eBool AtModuleSurLineIsSupported(AtModuleSur self);
eBool AtModuleSurShouldMaskFailuresWhenNonAutonomousFailures(AtModuleSur self);
eBool AtModuleSurTcaIsSupported(AtModuleSur self);

#endif /* _ATMODULESURINTERNAL_H_ */


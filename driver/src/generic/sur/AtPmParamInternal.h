/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmParamInternal.h
 * 
 * Created Date: Mar 18, 2015
 *
 * Description : PM parameter
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMPARAMINTERNAL_H_
#define _ATPMPARAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtSurEngine.h"
#include "AtPmParam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPmParamMethods
    {
    const char *(*Name)(AtPmParam self);

    /* Registers */
    AtPmRegister (*CurrentSecondRegister)(AtPmParam self);
    AtPmRegister (*CurrentPeriodRegister)(AtPmParam self);
    AtPmRegister (*PreviousPeriodRegister)(AtPmParam self);
    AtPmRegister (*CurrentDayRegister)(AtPmParam self);
    AtPmRegister (*PreviousDayRegister)(AtPmParam self);
    uint8 (*NumRecentPeriodRegisters)(AtPmParam self);
    AtPmRegister (*RecentRegister)(AtPmParam self, uint8 recentPeriod);
    void (*RecentPeriodShiftLeft)(AtPmParam self);

    /* Thresholds */
    eAtRet (*PeriodThresholdSet)(AtPmParam self, uint32 threshold);
    uint32 (*PeriodThresholdGet)(AtPmParam self);
    eAtRet (*DayThresholdSet)(AtPmParam self, uint32 threshold);
    uint32 (*DayThresholdGet)(AtPmParam self);
    }tAtPmParamMethods;

typedef struct tAtPmParam
    {
    tAtObject super;
    const tAtPmParamMethods *methods;

    /* Private data */
    AtSurEngine engine;
    const char *name;
    uint32 pmType;

    AtPmRegister currentSecondRegister;
    AtPmRegister currentPeriodRegister;
    AtPmRegister previousPeriodRegister;
    AtPmRegister currentDayRegister;
    AtPmRegister previousDayRegister;
    AtPmRegister *recentRegisters;
    }tAtPmParam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmParam AtPmParamObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType);

void AtPmParamRecentPeriodShiftLeft(AtPmParam self);
AtPmRegister AtPmParamRegisterGet(AtPmParam self, eAtPmRegisterType type);

#endif /* _ATPMPARAMINTERNAL_H_ */


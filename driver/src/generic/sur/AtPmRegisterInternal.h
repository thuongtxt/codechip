/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmRegisterInternal.h
 * 
 * Created Date: Mar 18, 2015
 *
 * Description : PM registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMREGISTERINTERNAL_H_
#define _ATPMREGISTERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtPmParam.h"
#include "AtPmRegister.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPmRegisterMethods
    {
    int32 (*Reset)(AtPmRegister self);
    int32 (*Value)(AtPmRegister self);
    eBool (*IsValid)(AtPmRegister self);
    }tAtPmRegisterMethods;

typedef struct tAtPmRegister
    {
    tAtObject super;
    const tAtPmRegisterMethods *methods;

    /* Private data */
    AtPmParam param;
    uint8 type;
    int32 value;
    }tAtPmRegister;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPmRegister AtPmRegisterObjectInit(AtPmRegister self, AtPmParam param);
AtPmParam AtPmRegisterParamGet(AtPmRegister self);
void AtPmRegisterValueSet(AtPmRegister self, int32 value);

#endif /* _ATPMREGISTERINTERNAL_H_ */


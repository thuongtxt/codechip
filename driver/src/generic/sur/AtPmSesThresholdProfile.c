/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtPmSesThresholdProfile.c
 *
 * Created Date: Aug 25, 2017
 *
 * Description : SES threshold profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtPmSesThresholdProfileInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static char m_methodsInit = 0;
static tAtPmSesThresholdProfileMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtObject self)
    {
    static char description[64];
    AtModule module = (AtModule)AtPmSesThresholdProfileModuleGet((AtPmSesThresholdProfile)self);
    AtDevice dev = AtModuleDeviceGet(module);
    AtSprintf(description, "%sses_profile.%d", AtDeviceIdToString(dev), AtPmSesThresholdProfileIdGet((AtPmSesThresholdProfile)self));

    return description;
    }

static eAtRet SdhLineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 SdhLineThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet SdhSectionThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 SdhSectionThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet SdhHoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 SdhHoPathThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet SdhLoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 SdhLoPathThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PdhDe3LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PdhDe3LineThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PdhDe3PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PdhDe3PathThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PdhDe1LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PdhDe1LineThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PdhDe1PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PdhDe1PathThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PwThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PwThresholdGet(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet Init(AtPmSesThresholdProfile self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtObject(AtPmSesThresholdProfile self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPmSesThresholdProfile self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPmSesThresholdProfile self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SdhLineThresholdSet);
        mMethodOverride(m_methods, SdhLineThresholdGet);
        mMethodOverride(m_methods, SdhSectionThresholdSet);
        mMethodOverride(m_methods, SdhSectionThresholdGet);
        mMethodOverride(m_methods, SdhHoPathThresholdSet);
        mMethodOverride(m_methods, SdhHoPathThresholdGet);
        mMethodOverride(m_methods, SdhLoPathThresholdSet);
        mMethodOverride(m_methods, SdhLoPathThresholdGet);
        mMethodOverride(m_methods, PdhDe3LineThresholdSet);
        mMethodOverride(m_methods, PdhDe3LineThresholdGet);
        mMethodOverride(m_methods, PdhDe3PathThresholdSet);
        mMethodOverride(m_methods, PdhDe3PathThresholdGet);
        mMethodOverride(m_methods, PdhDe1LineThresholdSet);
        mMethodOverride(m_methods, PdhDe1LineThresholdGet);
        mMethodOverride(m_methods, PdhDe1PathThresholdSet);
        mMethodOverride(m_methods, PdhDe1PathThresholdGet);
        mMethodOverride(m_methods, PwThresholdSet);
        mMethodOverride(m_methods, PwThresholdGet);
        mMethodOverride(m_methods, Init);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPmSesThresholdProfile);
    }

AtPmSesThresholdProfile AtPmSesThresholdProfileObjectInit(AtPmSesThresholdProfile self, AtModuleSur module, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->surModule  = module;
    self->profileId  = profileId;

    return self;
    }

eAtRet AtPmSesThresholdProfileInit(AtPmSesThresholdProfile self)
    {
    mNoParamAttributeSet(Init);
    }

AtModuleSur AtPmSesThresholdProfileModuleGet(AtPmSesThresholdProfile self)
    {
    if (self)
        return self->surModule;
    return NULL;
    }

/**
 * @addtogroup AtPmSesThresholdProfile
 * @{
 */

/**
 * Set PM SES Threshold of SONET/SDH Section
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-S
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfileSdhSectionThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(SdhSectionThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of SONET/SDH Section
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfileSdhSectionThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(SdhSectionThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of SONET/SDH Line
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-L
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfileSdhLineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(SdhLineThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of SONET/SDH Line
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfileSdhLineThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(SdhLineThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of SONET/SDH High-order Path
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-P
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfileSdhHoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(SdhHoPathThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of SONET/SDH High-order Path
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfileSdhHoPathThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(SdhHoPathThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of SONET/SDH Low-order Path
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-V
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfileSdhLoPathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(SdhLoPathThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of SONET/SDH Low-order Path
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfileSdhLoPathThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(SdhLoPathThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of PDH DS3/E3 Line
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-L
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfilePdhDe3LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(PdhDe3LineThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of PDH DS3/E3 Line
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfilePdhDe3LineThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(PdhDe3LineThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of PDH DS3/E3 Path
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-P
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfilePdhDe3PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(PdhDe3PathThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of PDH DS3/E3 Path
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfilePdhDe3PathThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(PdhDe3PathThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of PDH DS1/E1 Line
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-L
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfilePdhDe1LineThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(PdhDe1LineThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of PDH DS1/E1 Line
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfilePdhDe1LineThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(PdhDe1LineThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of PDH DS1/E1 Path
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES-P
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfilePdhDe1PathThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(PdhDe1PathThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of PDH DS1/E1 Path
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfilePdhDe1PathThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(PdhDe1PathThresholdGet, uint32, 0);
    }

/**
 * Set PM SES Threshold of PW
 *
 * @param self This threshold profile
 * @param threshold Threshold value to declare SES
 *
 * @return AT return code
 */
eAtRet AtPmSesThresholdProfilePwThresholdSet(AtPmSesThresholdProfile self, uint32 threshold)
    {
    mNumericalAttributeSet(PwThresholdSet, threshold);
    }

/**
 * Get PM SES Threshold of PW
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfilePwThresholdGet(AtPmSesThresholdProfile self)
    {
    mAttributeGet(PwThresholdGet, uint32, 0);
    }

/**
 * Get Profile ID
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmSesThresholdProfileIdGet(AtPmSesThresholdProfile self)
    {
    if (self)
        return self->profileId;
    return cInvalidUint32;
    }

/**
 * @}
 */

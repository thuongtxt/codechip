/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtPmThresholdProfile.c
 *
 * Created Date: Sep 9, 2016
 *
 * Description : PM Threshold Profile
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPmThresholdProfileInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static char m_methodsInit = 0;
static tAtPmThresholdProfileMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *ToString(AtObject self)
    {
    static char description[64];
    AtModule module = (AtModule)AtPmThresholdProfileModuleGet((AtPmThresholdProfile)self);
    AtDevice dev = AtModuleDeviceGet(module);

    AtSprintf(description, "%sPmThresholdProfile.%d", AtDeviceIdToString(dev), AtPmThresholdProfileIdGet((AtPmThresholdProfile)self));

    return description;
    }

static eAtRet SdhLinePeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(param);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 SdhLinePeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return 0;
    }

static eAtRet SdhHoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(param);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 SdhHoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return 0;
    }

static eAtRet SdhLoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(param);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 SdhLoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return 0;
    }

static eAtRet PdhDe3PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(param);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PdhDe3PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return 0;
    }

static eAtRet PdhDe1PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(param);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PdhDe1PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return 0;
    }

static eAtRet PwPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param, uint32 threshold)
    {
    AtUnused(self);
    AtUnused(param);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PwPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return 0;
    }

static eAtRet Init(AtPmThresholdProfile self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtObject(AtPmThresholdProfile self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPmThresholdProfile self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPmThresholdProfile self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SdhLinePeriodTcaThresholdSet);
        mMethodOverride(m_methods, SdhLinePeriodTcaThresholdGet);
        mMethodOverride(m_methods, SdhHoPathPeriodTcaThresholdSet);
        mMethodOverride(m_methods, SdhHoPathPeriodTcaThresholdGet);
        mMethodOverride(m_methods, SdhLoPathPeriodTcaThresholdSet);
        mMethodOverride(m_methods, SdhLoPathPeriodTcaThresholdGet);
        mMethodOverride(m_methods, PdhDe3PeriodTcaThresholdSet);
        mMethodOverride(m_methods, PdhDe3PeriodTcaThresholdGet);
        mMethodOverride(m_methods, PdhDe1PeriodTcaThresholdSet);
        mMethodOverride(m_methods, PdhDe1PeriodTcaThresholdGet);
        mMethodOverride(m_methods, PwPeriodTcaThresholdSet);
        mMethodOverride(m_methods, PwPeriodTcaThresholdGet);
        mMethodOverride(m_methods, Init);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPmThresholdProfile);
    }

AtPmThresholdProfile AtPmThresholdProfileObjectInit(AtPmThresholdProfile self, AtModuleSur module, uint32 profileId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->surModule  = module;
    self->profileId  = profileId;

    return self;
    }

eAtRet AtPmThresholdProfileInit(AtPmThresholdProfile self)
	{
	mNoParamAttributeSet(Init);
	}

AtModuleSur AtPmThresholdProfileModuleGet(AtPmThresholdProfile self)
    {
    if (self)
        return self->surModule;
    return NULL;
    }

/**
 * @addtogroup AtPmThresholdProfile
 * @{
 */

/**
 * Set PM TCA Threshold of SONET/SDH Line
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEngineSdhLinePmParam "PM Param type".
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param, uint32 threshold)
    {
    mTwoParamsAttributeSetWithMoreLogOption(SdhLinePeriodTcaThresholdSet, param, threshold);
    }

/**
 * Get PM TCA Threshold of SONET/SDH Line
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEngineSdhLinePmParam "PM Param type".
 *
 * @return Threshold value
 */
uint32 AtPmThresholdProfileSdhLinePeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param)
    {
    mOneParamAttributeGet(SdhLinePeriodTcaThresholdGet, param, uint32, 0);
    }

/**
 * Set PM TCA Threshold of SONET/SDH HoPath
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEngineSdhPathPmParam "PM Param type".
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold)
    {
    mTwoParamsAttributeSetWithMoreLogOption(SdhHoPathPeriodTcaThresholdSet, param, threshold);
    }

/**
 * Get PM TCA Threshold of SONET/SDH HoPath
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEngineSdhPathPmParam "PM Param type".
 *
 * @return Threshold value
 */
uint32 AtPmThresholdProfileSdhHoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param)
    {
    mOneParamAttributeGet(SdhHoPathPeriodTcaThresholdGet, param, uint32, 0);
    }

/**
 * Set PM TCA Threshold of SONET/SDH LoPath
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEngineSdhPathPmParam "PM Param type".
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold)
    {
    mTwoParamsAttributeSetWithMoreLogOption(SdhLoPathPeriodTcaThresholdSet, param, threshold);
    }

/**
 * Get PM TCA Threshold of SONET/SDH LoPath
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEngineSdhPathPmParam "PM Param type".
 *
 * @return Threshold value
 */
uint32 AtPmThresholdProfileSdhLoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param)
    {
    mOneParamAttributeGet(SdhLoPathPeriodTcaThresholdGet, param, uint32, 0);
    }

/**
 * Set PM TCA Threshold of PDH De3
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEnginePdhDe3PmParam "PM Param type".
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param, uint32 threshold)
    {
    mTwoParamsAttributeSetWithMoreLogOption(PdhDe3PeriodTcaThresholdSet, param, threshold);
    }

/**
 * Get PM TCA Threshold of PDH De3
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEnginePdhDe3PmParam "PM Param type".
 *
 * @return Threshold value
 */
uint32 AtPmThresholdProfilePdhDe3PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param)
    {
    mOneParamAttributeGet(PdhDe3PeriodTcaThresholdGet, param, uint32, 0);
    }

/**
 * Set PM TCA Threshold of PDH De1
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEnginePdhDe1PmParam "PM Param type".
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param, uint32 threshold)
    {
    mTwoParamsAttributeSetWithMoreLogOption(PdhDe1PeriodTcaThresholdSet, param, threshold);
    }

/**
 * Get PM TCA Threshold of PDH De1
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEnginePdhDe1PmParam "PM Param type".
 *
 * @return Threshold value
 */
uint32 AtPmThresholdProfilePdhDe1PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param)
    {
    mOneParamAttributeGet(PdhDe1PeriodTcaThresholdGet, param, uint32, 0);
    }

/**
 * Set PM TCA Threshold of PW
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEnginePwPmParam "PM Param type".
 * @param threshold Threshold value
 *
 * @return AT return code
 */
eAtRet AtPmThresholdProfilePwPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param, uint32 threshold)
    {
    mTwoParamsAttributeSetWithMoreLogOption(PwPeriodTcaThresholdSet, param, threshold);
    }

/**
 * Get PM TCA Threshold of PW
 *
 * @param self This threshold profile
 * @param param @ref eAtSurEnginePwPmParam "PM Param type".
 *
 * @return Threshold value
 */
uint32 AtPmThresholdProfilePwPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param)
    {
    mOneParamAttributeGet(PwPeriodTcaThresholdGet, param, uint32, 0);
    }

/**
 * Get Profile ID
 *
 * @param self This threshold profile
 *
 * @return Threshold value
 */
uint32 AtPmThresholdProfileIdGet(AtPmThresholdProfile self)
    {
    if (self)
        return self->profileId;
    return cInvalidUint32;
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtSurEnginePdhDe1.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : DS1/E1 Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../pdh/AtPdhDe1Internal.h"
#include "AtSurEngineInternal.h"
#include "AtModuleSurInternal.h"
#include "AtPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtSurEnginePdhDe1 *)self)

/*--------------------------- Macros -----------------------------------------*/
#define mInAccessible(self) InAccessible(self)

#define mParamType(field)                                                      \
    {                                                                          \
    if (engine->field == NULL)                                                 \
        engine->field = AtModuleSurPdhDe1ParamCreate(AtSurEngineModuleGet(self), self, #field, paramType); \
    return engine->field;                                                      \
    }

#define mPmCounterGet(regtype, field)                                          \
    {                                                                          \
    counters->field = AtPmRegisterValue(AtPmParamRegisterGet(engine->field, regtype));\
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtSurEngineMethods m_AtSurEngineOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPmParam PmParamGet(AtSurEngine self, uint32 paramType)
    {
    AtSurEnginePdhDe1 engine = (AtSurEnginePdhDe1)self;

    switch (paramType)
        {
        case cAtSurEnginePdhDe1PmParamCvL:
            mParamType(cvL)
        case cAtSurEnginePdhDe1PmParamEsL:
            mParamType(esL)
        case cAtSurEnginePdhDe1PmParamSesL:
            mParamType(sesL)
        case cAtSurEnginePdhDe1PmParamLossL:
            mParamType(lossL)
        case cAtSurEnginePdhDe1PmParamEsLfe:
            mParamType(esLfe)
        case cAtSurEnginePdhDe1PmParamCvP:
            mParamType(cvP)
        case cAtSurEnginePdhDe1PmParamEsP:
            mParamType(esP)
        case cAtSurEnginePdhDe1PmParamSesP:
            mParamType(sesP)
        case cAtSurEnginePdhDe1PmParamAissP:
            mParamType(aissP)
        case cAtSurEnginePdhDe1PmParamSasP:
            mParamType(sasP)
        case cAtSurEnginePdhDe1PmParamCssP:
            mParamType(cssP)
        case cAtSurEnginePdhDe1PmParamUasP:
            mParamType(uasP)
        case cAtSurEnginePdhDe1PmParamSefsPfe:
            mParamType(sefsPfe)
        case cAtSurEnginePdhDe1PmParamEsPfe:
            mParamType(esPfe)
        case cAtSurEnginePdhDe1PmParamSesPfe:
            mParamType(sesPfe)
        case cAtSurEnginePdhDe1PmParamCssPfe:
            mParamType(cssPfe)
        case cAtSurEnginePdhDe1PmParamFcPfe:
            mParamType(fcPfe)
        case cAtSurEnginePdhDe1PmParamUasPfe:
            mParamType(uasPfe)
        case cAtSurEnginePdhDe1PmParamFcP:
            mParamType(fcP)
        default:
            return NULL;
        }

    return NULL;
    }

static eAtRet CountersReadOnClear(AtSurEngine self, eAtPmRegisterType type, tAtSurEnginePdhDe1PmCounters *counters, eBool readOnClear)
    {
    AtSurEnginePdhDe1 engine = (AtSurEnginePdhDe1)self;
    eAtRet ret = AtSurEngineRegistersLatchAndClear(self, type, readOnClear);
    if (ret != cAtOk)
        return ret;

    /* Let the active driver fill each field, only clear memory on standby as it
     * is not able to access hardware */
    if (AtSurEngineInAccessible(self))
        {
        AtOsalMemInit(counters, 0, sizeof(tAtSurEnginePdhDe1PmCounters));
        return cAtOk;
        }

    mPmCounterGet(type, cvL);
    mPmCounterGet(type, esL);
    mPmCounterGet(type, sesL);
    mPmCounterGet(type, lossL);
    mPmCounterGet(type, esLfe);
    mPmCounterGet(type, cvP);
    mPmCounterGet(type, esP);
    mPmCounterGet(type, sesP);
    mPmCounterGet(type, aissP);
    mPmCounterGet(type, sasP);
    mPmCounterGet(type, cssP);
    mPmCounterGet(type, uasP);
    mPmCounterGet(type, sefsPfe);
    mPmCounterGet(type, esPfe);
    mPmCounterGet(type, sesPfe);
    mPmCounterGet(type, cssPfe);
    mPmCounterGet(type, fcPfe);
    mPmCounterGet(type, uasPfe);
    mPmCounterGet(type, fcP);

    return cAtOk;
    }

static eAtRet RegisterAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEnginePdhDe1PmCounters*)counters, cAtFalse);
    }

static eAtRet RegisterAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEnginePdhDe1PmCounters*)counters, cAtTrue);
    }

static eBool PmParamIsDefined(AtSurEngine self, uint32 paramType)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtSurEngineChannelGet(self);

    if (AtPdhDe1HasFarEndPerformance(de1, AtPdhChannelFrameTypeGet((AtPdhChannel)de1)) == cAtFalse)
        {
        switch (paramType)
            {
            case cAtSurEnginePdhDe1PmParamEsPfe:    return cAtFalse;
            case cAtSurEnginePdhDe1PmParamSesPfe:   return cAtFalse;
            case cAtSurEnginePdhDe1PmParamUasPfe:   return cAtFalse;
            case cAtSurEnginePdhDe1PmParamEsLfe:    return cAtFalse;
            case cAtSurEnginePdhDe1PmParamSefsPfe:  return cAtFalse;
            case cAtSurEnginePdhDe1PmParamCssPfe:   return cAtFalse;
            default: return cAtTrue;
            }
        }
    return cAtTrue;
    }

static uint32 CurrentFailuresGet(AtSurEngine self)
    {
    static const uint32 cLineFailures = cAtPdhDe1AlarmLos;
    uint32 failures = AtSurEngineNonAutonomousFailuresGet(self);

    if (failures == 0)
        return AtSurEngineAutonomousFailuresGet(self);

    /* If AIS failure caused by Line maintenance signal, it should mask all of Path failures. */
    if (AtModuleSurShouldMaskFailuresWhenNonAutonomousFailures(AtSurEngineModuleGet(self)))
        return (AtSurEngineAutonomousFailuresGet(self) & cLineFailures) | failures;

    failures |= AtSurEngineAutonomousFailuresGet(self);

    /* DS1 SF/ESF should filter LOF failure if high-level AIS failure downstream. */
    if (AtPdhDe1IsE1((AtPdhDe1)AtSurEngineChannelGet(self)) == cAtFalse)
        failures &= (uint32)(~cAtPdhDe1AlarmLof);

    return failures;
    }

static void DeletePmParam(AtPmParam *param)
    {
    AtObjectDelete((AtObject)*param);
    *param = NULL;
    }

static void Delete(AtObject self)
    {
    DeletePmParam(&mThis(self)->cvL);
    DeletePmParam(&mThis(self)->esL);
    DeletePmParam(&mThis(self)->sesL);
    DeletePmParam(&mThis(self)->lossL);
    DeletePmParam(&mThis(self)->esLfe);
    DeletePmParam(&mThis(self)->cvP);
    DeletePmParam(&mThis(self)->esP);
    DeletePmParam(&mThis(self)->sesP);
    DeletePmParam(&mThis(self)->aissP);
    DeletePmParam(&mThis(self)->sasP);
    DeletePmParam(&mThis(self)->cssP);
    DeletePmParam(&mThis(self)->uasP);
    DeletePmParam(&mThis(self)->sefsPfe);
    DeletePmParam(&mThis(self)->esPfe);
    DeletePmParam(&mThis(self)->sesPfe);
    DeletePmParam(&mThis(self)->cssPfe);
    DeletePmParam(&mThis(self)->fcPfe);
    DeletePmParam(&mThis(self)->uasPfe);
    DeletePmParam(&mThis(self)->fcP);

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSurEnginePdhDe1 object = (AtSurEnginePdhDe1)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(cvL);
    mEncodeObject(esL);
    mEncodeObject(sesL);
    mEncodeObject(lossL);
    mEncodeObject(esLfe);
    mEncodeObject(cvP);
    mEncodeObject(esP);
    mEncodeObject(sesP);
    mEncodeObject(aissP);
    mEncodeObject(sasP);
    mEncodeObject(cssP);
    mEncodeObject(uasP);
    mEncodeObject(sefsPfe);
    mEncodeObject(esPfe);
    mEncodeObject(sesPfe);
    mEncodeObject(cssPfe);
    mEncodeObject(fcPfe);
    mEncodeObject(uasPfe);
    mEncodeObject(fcP);
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmParamGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersClear);
        mMethodOverride(m_AtSurEngineOverride, PmParamIsDefined);
        mMethodOverride(m_AtSurEngineOverride, CurrentFailuresGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtSurEngine(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEnginePdhDe1);
    }

AtSurEngine AtSurEnginePdhDe1ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint32 AtSurEngineDe1FarEndParams(void)
    {
    return (cAtSurEnginePdhDe1PmParamEsLfe   |
            cAtSurEnginePdhDe1PmParamSefsPfe |
            cAtSurEnginePdhDe1PmParamEsPfe   |
            cAtSurEnginePdhDe1PmParamSesPfe  |
            cAtSurEnginePdhDe1PmParamCssPfe  |
            cAtSurEnginePdhDe1PmParamFcPfe   |
            cAtSurEnginePdhDe1PmParamUasPfe);
    }


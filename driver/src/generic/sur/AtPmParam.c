/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Surveillance
 *
 * File        : AtPmParam.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : PM param abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPmParamInternal.h"
#include "AtModuleSurInternal.h"
#include "AtSurEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtPmParam)(self))

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPmParamMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *Name(AtPmParam self)
    {
    /* Let concrete class do */
    return self->name;
    }

static AtModuleSur Module(AtPmParam self)
    {
    AtSurEngine engine = AtPmParamEngineGet(self);
    return AtSurEngineModuleGet(engine);
    }

static AtPmRegister CurrentSecondRegister(AtPmParam self)
    {
    if (self->currentSecondRegister == NULL)
        self->currentSecondRegister = AtModuleSurCurrentSecondRegisterCreate(Module(self), self);
    return self->currentSecondRegister;
    }

static AtPmRegister CurrentPeriodRegister(AtPmParam self)
    {
    if (self->currentPeriodRegister == NULL)
        self->currentPeriodRegister = AtModuleSurCurrentPeriodRegisterCreate(Module(self), self);
    return self->currentPeriodRegister;
    }

static AtPmRegister PreviousPeriodRegister(AtPmParam self)
    {
    if (self->previousPeriodRegister == NULL)
        self->previousPeriodRegister = AtModuleSurPreviousPeriodRegisterCreate(Module(self), self);
    return self->previousPeriodRegister;
    }

static AtPmRegister CurrentDayRegister(AtPmParam self)
    {
    if (self->currentDayRegister == NULL)
        self->currentDayRegister = AtModuleSurCurrentDayRegisterCreate(Module(self), self);
    return self->currentDayRegister;
    }

static AtPmRegister PreviousDayRegister(AtPmParam self)
    {
    if (self->previousDayRegister == NULL)
        self->previousDayRegister = AtModuleSurPreviousDayRegisterCreate(Module(self) , self);
    return self->previousDayRegister;
    }

static AtPmRegister RecentRegister(AtPmParam self, uint8 recentPeriod)
    {
    uint8 numRegisters = AtPmParamNumRecentPeriodRegisters(self);
    if (numRegisters == 0)
        return NULL;

    if (recentPeriod >= numRegisters)
        return NULL;

    if (self->recentRegisters == NULL)
        {
        uint32 memorySize = numRegisters * sizeof(AtPmParam);
        self->recentRegisters = AtOsalMemAlloc(memorySize);
        AtOsalMemInit(self->recentRegisters, 0, memorySize);
        }

    if (self->recentRegisters[recentPeriod] == NULL)
        self->recentRegisters[recentPeriod] = AtModuleSurRecentRegisterCreate(Module(self), self, recentPeriod);

    return self->recentRegisters[recentPeriod];
    }

static uint8 NumRecentPeriodRegisters(AtPmParam self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static eAtRet PeriodThresholdSet(AtPmParam self, uint32 threshold)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 PeriodThresholdGet(AtPmParam self)
    {
    AtSurEngine engine = AtPmParamEngineGet(self);
    return AtSurEngineTcaPeriodThresholdValueGet(engine, AtPmParamTypeGet(self));
    }

static eAtRet DayThresholdSet(AtPmParam self, uint32 threshold)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(threshold);
    return cAtErrorNotImplemented;
    }

static uint32 DayThresholdGet(AtPmParam self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static void DeleteRecentRegisters(AtObject self)
    {
    uint8 register_i;
    uint8 numRegisters;

    if (mThis(self)->recentRegisters == NULL)
        return;

    numRegisters = AtPmParamNumRecentPeriodRegisters(mThis(self));
    for (register_i = 0; register_i < numRegisters; register_i++)
        AtObjectDelete((AtObject)mThis(self)->recentRegisters[register_i]);

    AtOsalMemFree(mThis(self)->recentRegisters);
    mThis(self)->recentRegisters = NULL;
    }

static void DeleteRegister(AtPmRegister *pmRegister)
    {
    AtObjectDelete((AtObject)*pmRegister);
    *pmRegister = NULL;
    }

static void DeleteAllRegisters(AtObject self)
    {
    DeleteRegister(&mThis(self)->currentDayRegister);
    DeleteRegister(&mThis(self)->previousDayRegister);
    DeleteRegister(&mThis(self)->currentPeriodRegister);
    DeleteRegister(&mThis(self)->previousPeriodRegister);
    DeleteRegister(&mThis(self)->currentSecondRegister);
    DeleteRecentRegisters(self);
    }

static void Delete(AtObject self)
    {
    DeleteAllRegisters(self);
    m_AtObjectMethods->Delete(self);
    }

static void RecentPeriodShiftLeft(AtPmParam self)
    {
    AtUnused(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPmParam object = (AtPmParam)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(engine);
    mEncodeString(name);
    mEncodeUInt(pmType);
    }

static void OverrideAtObject(AtPmParam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPmParam self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPmParam self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Name);
        mMethodOverride(m_methods, CurrentSecondRegister);
        mMethodOverride(m_methods, CurrentPeriodRegister);
        mMethodOverride(m_methods, PreviousPeriodRegister);
        mMethodOverride(m_methods, CurrentDayRegister);
        mMethodOverride(m_methods, PreviousDayRegister);
        mMethodOverride(m_methods, NumRecentPeriodRegisters);
        mMethodOverride(m_methods, RecentRegister);
        mMethodOverride(m_methods, RecentPeriodShiftLeft);
        mMethodOverride(m_methods, PeriodThresholdSet);
        mMethodOverride(m_methods, PeriodThresholdGet);
        mMethodOverride(m_methods, DayThresholdSet);
        mMethodOverride(m_methods, DayThresholdGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPmParam);
    }

AtPmParam AtPmParamObjectInit(AtPmParam self, AtSurEngine engine, const char *name, uint32 pmType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    self->engine = engine;
    self->name = name;
    self->pmType = pmType;

    return self;
    }

void AtPmParamRecentPeriodShiftLeft(AtPmParam self)
    {
    if (self)
        mMethodsGet(self)->RecentPeriodShiftLeft(self);
    }

AtPmRegister AtPmParamRegisterGet(AtPmParam self, eAtPmRegisterType type)
    {
    switch (type)
        {
        case cAtPmCurrentSecondRegister:
            return AtPmParamCurrentSecondRegister(self);
        case cAtPmCurrentPeriodRegister:
            return AtPmParamCurrentPeriodRegister(self);
        case cAtPmPreviousPeriodRegister:
            return AtPmParamPreviousPeriodRegister(self);
        case cAtPmCurrentDayRegister:
            return AtPmParamCurrentDayRegister(self);
        case cAtPmPreviousDayRegister:
            return AtPmParamPreviousDayRegister(self);
        case cAtPmRecentPeriodRegister:
            return AtPmParamRecentRegister(self, 0);
        case cAtPmUnknownRegister:
            return NULL;
        default:
            if ((type > cAtPmRecentPeriodRegister) && (type < cAtPmCurrentDayRegister))
                return AtPmParamRecentRegister(self, (uint8)(type - cAtPmRecentPeriodRegister));
        }
    return NULL;
    }

/**
 * @addtogroup AtPmParam
 * @{
 */

/**
 * Get name of PM param
 *
 * @param self This param
 *
 * @return PM param name
 */
const char *AtPmParamName(AtPmParam self)
    {
    if (self)
        return mMethodsGet(self)->Name(self);
    return NULL;
    }

/**
 * Get current second PM register
 *
 * @param self This param
 *
 * @return PM register
 */
AtPmRegister AtPmParamCurrentSecondRegister(AtPmParam self)
    {
    mNoParamObjectGet(CurrentSecondRegister, AtPmRegister);
    }

/**
 * Get current period PM register
 *
 * @param self This param
 *
 * @return PM register
 */
AtPmRegister AtPmParamCurrentPeriodRegister(AtPmParam self)
    {
    mNoParamObjectGet(CurrentPeriodRegister, AtPmRegister);
    }

/**
 * Get previous period PM register
 *
 * @param self This param
 *
 * @return PM register
 */
AtPmRegister AtPmParamPreviousPeriodRegister(AtPmParam self)
    {
    mNoParamObjectGet(PreviousPeriodRegister, AtPmRegister);
    }

/**
 * Get current day PM register
 *
 * @param self This param
 *
 * @return PM register
 */
AtPmRegister AtPmParamCurrentDayRegister(AtPmParam self)
    {
    mNoParamObjectGet(CurrentDayRegister, AtPmRegister);
    }

/**
 * Get previous day PM register
 *
 * @param self This param
 *
 * @return PM register
 */
AtPmRegister AtPmParamPreviousDayRegister(AtPmParam self)
    {
    mNoParamObjectGet(PreviousDayRegister, AtPmRegister);
    }

/**
 * Get recent PM register
 *
 * @param self This param
 * @param recentPeriod Recent Period
 *
 * @return PM register
 */
AtPmRegister AtPmParamRecentRegister(AtPmParam self, uint8 recentPeriod)
    {
    mOneParamObjectGet(RecentRegister, AtPmRegister, recentPeriod);
    }

/**
 * Get number of PM recent period registers
 *
 * @param self This param
 *
 * @return number of recent period registers
 */
uint8 AtPmParamNumRecentPeriodRegisters(AtPmParam self)
    {
    mAttributeGet(NumRecentPeriodRegisters, uint8, 0);
    }

/**
 * Set threshold for PM period
 *
 * @param self This param
 * @param threshold PM Threshold
 *
 * @return AT return code
 */
eAtRet AtPmParamPeriodThresholdSet(AtPmParam self, uint32 threshold)
    {
    mNumericalAttributeSet(PeriodThresholdSet, threshold);
    }

/**
 * Get PM period threshold
 *
 * @param self This param
 *
 * @return threshold
 */
uint32 AtPmParamPeriodThresholdGet(AtPmParam self)
    {
    mAttributeGet(PeriodThresholdGet, uint32, 0);
    }

/**
 * Set PM threshold for day
 *
 * @param self This param
 * @param threshold Day Threshold
 *
 * @return AT return code
 */
eAtRet AtPmParamDayThresholdSet(AtPmParam self, uint32 threshold)
    {
    mNumericalAttributeSet(DayThresholdSet, threshold);
    }

/**
 * Get PM day threshold
 *
 * @param self This param
 *
 * @return threshold
 */
uint32 AtPmParamDayThresholdGet(AtPmParam self)
    {
    mAttributeGet(DayThresholdGet, uint32, 0);
    }

/**
 * Get PM param type
 *
 * @param self This param
 *
 * @return Param type
 */
uint32 AtPmParamTypeGet(AtPmParam self)
    {
    return self ? self->pmType : 0;
    }

/**
 * Get associated Surveillance engine
 *
 * @param self This param
 *
 * @return Surveillance engine
 */
AtSurEngine AtPmParamEngineGet(AtPmParam self)
    {
    return self ? self->engine : NULL;
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtSurEngineInternal.h
 * 
 * Created Date: Mar 18, 2015
 *
 * Description : Surveillance engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSURENGINEINTERNAL_H_
#define _ATSURENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtObject.h"
#include "AtModuleSur.h"
#include "AtSurEngine.h"
#include "AtModuleSdh.h"
#include "AtModulePdh.h"
#include "AtModulePw.h"
#include "AtPmSesThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSurEngineSdhLine *AtSurEngineSdhLine;
typedef struct tAtSurEngineSdhPath *AtSurEngineSdhPath;
typedef struct tAtSurEnginePdhDe1 *AtSurEnginePdhDe1;
typedef struct tAtSurEnginePdhDe3 *AtSurEnginePdhDe3;
typedef struct tAtSurEnginePw *AtSurEnginePw;

typedef struct tAtSurEngineMethods
    {
    eAtRet (*Init)(AtSurEngine self);
    eAtRet (*Enable)(AtSurEngine self, eBool enable);
    eBool (*IsEnabled)(AtSurEngine self);
    eAtRet (*PmEnable)(AtSurEngine self, eBool enable);
    eBool (*PmIsEnabled)(AtSurEngine self);
    eBool (*PmCanEnable)(AtSurEngine self, eBool enable);
    AtChannel (*ChannelGet)(AtSurEngine self);

    /* Get PM counter. */
    eAtRet (*RegisterAllCountersGet)(AtSurEngine self, eAtPmRegisterType type, void *counters);
    eAtRet (*RegisterAllCountersClear)(AtSurEngine self, eAtPmRegisterType type, void *counters);
    uint32 (*PmInvalidFlagsGet)(AtSurEngine self, eAtPmRegisterType type);

    /* PM TCA. */
    uint32 (*TcaChangedHistoryGet)(AtSurEngine self);
    uint32 (*TcaChangedHistoryClear)(AtSurEngine self);
    
    /* Failures. */
    uint32 (*CurrentFailuresGet)(AtSurEngine self);
    uint32 (*FailureChangedHistoryGet)(AtSurEngine self);
    uint32 (*FailureChangedHistoryClear)(AtSurEngine self);
    
    /* Listeners */
    eAtRet (*ListenerAdd)(AtSurEngine self, void *userData, const tAtSurEngineListener *callback);
    eAtRet (*ListenerRemove)(AtSurEngine self, void *userData, const tAtSurEngineListener *callback);

    /* Thresholds */
    uint32 (*SesThresholdPoolEntryGet)(AtSurEngine self);
    eAtRet (*SesThresholdPoolEntrySet)(AtSurEngine self, uint32 entryIndex);
    uint32 (*TcaThresholdPoolEntryGet)(AtSurEngine self);
    eAtRet (*TcaThresholdPoolEntrySet)(AtSurEngine self, uint32 entryIndex);
    uint32 (*TcaPeriodThresholdValueGet)(AtSurEngine self, uint32 paramType);

    /* Notifications */
    eAtRet (*FailureNotificationMaskSet)(AtSurEngine self, uint32 failureMask, uint32 enableMask);
    uint32 (*FailureNotificationMaskGet)(AtSurEngine self);
    eAtRet (*TcaNotificationMaskSet)(AtSurEngine self, uint32 paramMask, uint32 enableMask);
    uint32 (*TcaNotificationMaskGet)(AtSurEngine self);
    void (*FailureNotificationProcess)(AtSurEngine self, uint32 offset);
    void (*TcaNotificationProcess)(AtSurEngine self, uint32 offset);

    /* Parameters */
    AtPmParam (*PmParamGet)(AtSurEngine self, uint32 paramType);
    eBool (*PmParamIsSupported)(AtSurEngine self, uint32 paramType);
    eBool (*PmParamIsDefined)(AtSurEngine self, uint32 paramType); /* Some parameters are not defined for all framings */

    /* Internal methods */
    eAtRet (*Provision)(AtSurEngine self, eBool provision);
    eAtRet (*PeriodExpire)(AtSurEngine self, void *data);
    eAtRet (*FailureNotificationEnable)(AtSurEngine self, eBool enable);
    eBool (*FailureNotificationIsEnabled)(AtSurEngine self);
    eAtRet (*TcaNotificationEnable)(AtSurEngine self, eBool enable);
    eBool (*TcaNotificationIsEnabled)(AtSurEngine self);
    uint32 (*AutonomousFailuresGet)(AtSurEngine self); /* Failures self-detected and autonomous notification */
    uint32 (*NonAutonomousFailuresGet)(AtSurEngine self); /* Failures caused by alarm forwarding, non-autonomous notification */

    /* Work with registers */
    uint32 (*DefaultOffset)(AtSurEngine self);

    /* Deprecated. */
    eAtRet (*AllCountersLatchAndClear)(AtSurEngine self, eBool clear);
    eAtRet (*RegistersLatchAndClear)(AtSurEngine self, eAtPmRegisterType type, eBool clear);

    /* Threshold Profile */
    eAtRet (*ThresholdProfileSet)(AtSurEngine self, AtPmThresholdProfile profile);
    AtPmThresholdProfile(*ThresholdProfileGet)(AtSurEngine self);
    eAtRet (*SesThresholdProfileSet)(AtSurEngine self, AtPmSesThresholdProfile profile);
    AtPmSesThresholdProfile(*SesThresholdProfileGet)(AtSurEngine self);
    }tAtSurEngineMethods;

typedef struct tAtSurEngine
    {
    tAtObject super;
    const tAtSurEngineMethods *methods;

    /* Private data */
    AtModuleSur module;
    AtChannel channel;
    eBool isProvisioned;
    eBool enable;

    /* Listeners */
    void *listeners;
    tAtOsalCurTime *periodStartTime;
    tAtOsalCurTime *currentTime;
    int32         differenceTime;
    }tAtSurEngine;

typedef struct tAtSurEngineSdhLine
    {
    tAtSurEngine super;

    /* Private data */
    AtPmParam sefsS;
    AtPmParam cvS;
    AtPmParam esS;
    AtPmParam sesS;

    AtPmParam cvL;
    AtPmParam esL;
    AtPmParam sesL;
    AtPmParam uasL;
    AtPmParam fcL;

    AtPmParam cvLfe;
    AtPmParam esLfe;
    AtPmParam sesLfe;
    AtPmParam uasLfe;
    AtPmParam fcLfe;
    }tAtSurEngineSdhLine;

typedef struct tAtSurEngineSdhPath
    {
    tAtSurEngine super;

    /* Private data */
    AtPmParam ppjcPdet;
    AtPmParam npjcPdet;
    AtPmParam ppjcPgen;
    AtPmParam npjcPgen;
    AtPmParam pjcDiff;
    AtPmParam pjcsPdet;
    AtPmParam pjcsPgen;

    AtPmParam cvP;
    AtPmParam esP;
    AtPmParam sesP;
    AtPmParam uasP;
    AtPmParam fcP;

    AtPmParam cvPfe;
    AtPmParam esPfe;
    AtPmParam sesPfe;
    AtPmParam uasPfe;
    AtPmParam fcPfe;

    AtPmParam efsP;
    AtPmParam efsPfe;
    AtPmParam asP;
    AtPmParam asPfe;
    AtPmParam ebP;
    AtPmParam bbeP;
    AtPmParam esrP;
    AtPmParam sesrP;
    AtPmParam bberP;
    }tAtSurEngineSdhPath;

typedef struct tAtSurEnginePdhDe3
    {
    tAtSurEngine super;

    /* Private data */
    AtPmParam cvL;
    AtPmParam esL;
    AtPmParam sesL;
    AtPmParam lossL;

    AtPmParam cvpP;
    AtPmParam cvcpP;
    AtPmParam espP;
    AtPmParam escpP;
    AtPmParam sespP;
    AtPmParam sescpP;
    AtPmParam sasP;
    AtPmParam aissP;
    AtPmParam uaspP;
    AtPmParam uascpP;

    AtPmParam cvcpPfe;
    AtPmParam escpPfe;
    AtPmParam esbcpPfe;
    AtPmParam sescpPfe;
    AtPmParam sascpPfe;
    AtPmParam uascpPfe;

    AtPmParam esaL;
    AtPmParam esbL;
    AtPmParam esapP;
    AtPmParam esacpP;
    AtPmParam esbpP;
    AtPmParam esbcpP;
    AtPmParam fcP;
    AtPmParam esacpPfe;
    AtPmParam fccpPfe;
    }tAtSurEnginePdhDe3;

typedef struct tAtSurEnginePdhDe1
    {
    tAtSurEngine super;

    /* Private data */
    AtPmParam cvL;
    AtPmParam esL;
    AtPmParam sesL;
    AtPmParam lossL;

    AtPmParam esLfe;

    AtPmParam cvP;
    AtPmParam esP;
    AtPmParam sesP;
    AtPmParam aissP;
    AtPmParam sasP;
    AtPmParam cssP;
    AtPmParam uasP;

    AtPmParam sefsPfe;
    AtPmParam esPfe;
    AtPmParam sesPfe;
    AtPmParam cssPfe;
    AtPmParam fcPfe;
    AtPmParam uasPfe;

    AtPmParam fcP;
    }tAtSurEnginePdhDe1;

typedef struct tAtSurEnginePw
    {
    tAtSurEngine super;

    /* Private data */
    AtPmParam es;
    AtPmParam ses;
    AtPmParam uas;
    AtPmParam fc;

    AtPmParam esfe;
    AtPmParam sesfe;
    AtPmParam uasfe;
    }tAtSurEnginePw;

typedef struct tSurEngineEventListener
    {
    tAtSurEngineListener *callbacks;
    void *userData;
    }tSurEngineEventListener;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtSurEngine AtSurEngineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine AtSurEngineSdhLineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine AtSurEngineSdhPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine AtSurEnginePdhDe1ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine AtSurEnginePdhDe3ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);
AtSurEngine AtSurEnginePwObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel);

/* Internal functions */
void AtSurEngineFailureNotify(AtSurEngine self, uint32 failureType, uint32 currentStatus);
void AtSurEngineTcaNotify(AtSurEngine self, AtPmParam param, AtPmRegister pmRegister);
void AtSurEngineAllParamsPeriodTcaNotify(AtSurEngine self, uint32 paramMask);

eAtRet AtSurEngineProvision(AtSurEngine self, eBool provision);
eBool AtSurEngineIsProvisioned(AtSurEngine self);
eAtRet AtSurEnginePeriodExpire(AtSurEngine self, void *data);
uint32 AtSurEngineDefaultOffset(AtSurEngine self);
eBool AtSurEnginePmCanEnable(AtSurEngine self, eBool enable);

eAtRet AtSurEngineFailureNotificationEnable(AtSurEngine self, eBool enable);
eBool AtSurEngineFailureNotificationIsEnabled(AtSurEngine self);
void AtSurEngineNotificationProcess(AtSurEngine self, uint32 offset);
void AtSurEngineTcaNotificationProcess(AtSurEngine self, uint32 offset);
eAtRet AtSurEngineTcaNotificationEnable(AtSurEngine self, eBool enable);
eBool AtSurEngineTcaNotificationIsEnabled(AtSurEngine self);

/* SES threshold profile */
eAtRet AtSurEngineSesThresholdProfileSet(AtSurEngine self, AtPmSesThresholdProfile profile);
AtPmSesThresholdProfile AtSurEngineSesThresholdProfileGet(AtSurEngine self);

/* SES threshold entry. This is used for products that support hardware base
 * Surveillance Monitoring */
uint32 AtSurEngineSesThresholdPoolEntryGet(AtSurEngine self);
eAtRet AtSurEngineSesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex);

/* TCA threshold entry. This is used for products that support hardware base
 * Surveillance Monitoring */
eAtRet AtSurEngineTcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex);
uint32 AtSurEngineTcaThresholdPoolEntryGet(AtSurEngine self);
uint32 AtSurEngineTcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType);

uint32 AtSurEngineAutonomousFailuresGet(AtSurEngine self);
uint32 AtSurEngineNonAutonomousFailuresGet(AtSurEngine self);
eBool AtSurEngineInAccessible(AtSurEngine self);
eBool AtSurEnginePmParamIsDefined(AtSurEngine self, uint32 paramType);
eBool AtSurEngineTcaIsSupported(AtSurEngine self);


/* Singletons */
uint32 AtSurEngineDe1FarEndParams(void);
uint32 AtSurEngineDs3CbitParams(void);

#endif /* _ATSURENGINEINTERNAL_H_ */

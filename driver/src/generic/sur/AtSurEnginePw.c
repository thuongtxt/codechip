/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtSurEnginePw.c
 *
 * Created Date: Mar 19, 2015
 *
 * Description : PW Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtSurEngineInternal.h"
#include "AtModuleSurInternal.h"
#include "AtPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtSurEnginePw *)self)

/*--------------------------- Macros -----------------------------------------*/
#define mParamType(field)                                                      \
    {                                                                          \
    if (engine->field == NULL)                                                 \
        engine->field = AtModuleSurPwParamCreate(AtSurEngineModuleGet(self), self, #field, paramType); \
    return engine->field;                                                      \
    }

#define mPmCounterGet(regtype, field)                                          \
    {                                                                          \
    counters->field = AtPmRegisterValue(AtPmParamRegisterGet(engine->field, regtype));\
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtSurEngineMethods m_AtSurEngineOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPmParam PmParamGet(AtSurEngine self, uint32 paramType)
    {
    AtSurEnginePw engine = (AtSurEnginePw)self;

    switch (paramType)
        {
        case cAtSurEnginePwPmParamEs :
            mParamType(es)
        case cAtSurEnginePwPmParamSes:
            mParamType(ses)
        case cAtSurEnginePwPmParamUas:
            mParamType(uas)
        case cAtSurEnginePwPmParamFeEs :
            mParamType(esfe)
        case cAtSurEnginePwPmParamFeSes:
            mParamType(sesfe)
        case cAtSurEnginePwPmParamFeUas:
            mParamType(uasfe)
        case cAtSurEnginePwPmParamFc:
            mParamType(fc)
        default:
            return NULL;
        }

    return NULL;
    }

static eAtRet CountersReadOnClear(AtSurEngine self, eAtPmRegisterType type, tAtSurEnginePwPmCounters *counters, eBool readOnClear)
    {
    AtSurEnginePw engine = (AtSurEnginePw)self;
    eAtRet ret = AtSurEngineRegistersLatchAndClear(self, type, readOnClear);
    if (ret != cAtOk)
        return ret;

    /* Let the active driver fill each field, only clear memory on standby as it
     * is not able to access hardware */
    if (AtSurEngineInAccessible(self))
        {
        AtOsalMemInit(counters, 0, sizeof(tAtSurEnginePwPmCounters));
        return cAtOk;
        }

    mPmCounterGet(type, es);
    mPmCounterGet(type, ses);
    mPmCounterGet(type, uas);
    mPmCounterGet(type, fc);
    mPmCounterGet(type, esfe);
    mPmCounterGet(type, sesfe);
    mPmCounterGet(type, uasfe);

    return cAtOk;
    }

static eAtRet RegisterAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEnginePwPmCounters*)counters, cAtFalse);
    }

static eAtRet RegisterAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEnginePwPmCounters*)counters, cAtTrue);
    }

static void DeletePmParam(AtPmParam *param)
    {
    AtObjectDelete((AtObject)*param);
    *param = NULL;
    }

static void Delete(AtObject self)
    {
    DeletePmParam(&mThis(self)->es);
    DeletePmParam(&mThis(self)->ses);
    DeletePmParam(&mThis(self)->uas);
    DeletePmParam(&mThis(self)->fc);
    DeletePmParam(&mThis(self)->esfe);
    DeletePmParam(&mThis(self)->sesfe);
    DeletePmParam(&mThis(self)->uasfe);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSurEnginePw object = (AtSurEnginePw)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(es);
    mEncodeObject(ses);
    mEncodeObject(uas);
    mEncodeObject(esfe);
    mEncodeObject(sesfe);
    mEncodeObject(uasfe);
    mEncodeObject(fc);
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmParamGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersClear);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtObject(self);
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEnginePw);
    }

AtSurEngine AtSurEnginePwObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtModuleSur.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : Surveillance module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModuleSurInternal.h"
#include "AtPmThresholdProfileInternal.h"
#include "AtPmSesThresholdProfileInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cDefaultHoldOffTimerMs 2500
#define cDefaultHoldOnTimerMs  10000

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleSur)self)
#define mInAccessible(self) AtModuleInAccessible((AtModule)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtModuleSurMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleMethods m_AtModuleOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "sur";
    }

static AtSurEngine SdhLineEngineCreate(AtModuleSur self, AtSdhLine line)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(line);
    return NULL;
    }

static AtSurEngine SdhHoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }

static AtSurEngine SdhLoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }

static AtSurEngine PdhDe1EngineCreate(AtModuleSur self, AtPdhDe1 de1)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(de1);
    return NULL;
    }

static AtSurEngine PdhDe3EngineCreate(AtModuleSur self, AtPdhDe3 de3)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }

static AtSurEngine PwEngineCreate(AtModuleSur self, AtPw pw)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(pw);
    return NULL;
    }

static uint32 RecommendedPeriodInMs(AtModule self)
    {
    AtUnused(self);
    return 100;
    }

static eAtRet SdhLineEngineDelete(AtModuleSur self, AtSurEngine engine)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engine);
    return cAtOk;
    }

static eAtRet SdhHoPathEngineDelete(AtModuleSur self, AtSurEngine engine)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engine);
    return cAtOk;
    }

static eAtRet SdhLoPathEngineDelete(AtModuleSur self, AtSurEngine engine)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engine);
    return cAtOk;
    }

static eAtRet PdhDe1EngineDelete(AtModuleSur self, AtSurEngine engine)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engine);
    return cAtOk;
    }

static eAtRet PdhDe3EngineDelete(AtModuleSur self, AtSurEngine engine)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engine);
    return cAtOk;
    }

static eAtRet PwEngineDelete(AtModuleSur self, AtSurEngine engine)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(engine);
    return cAtOk;
    }

static AtPmRegister CurrentSecondRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return NULL;
    }

static AtPmRegister CurrentPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return NULL;
    }

static AtPmRegister PreviousPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return NULL;
    }

static AtPmRegister CurrentDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return NULL;
    }

static AtPmRegister PreviousDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    AtUnused(self);
    AtUnused(param);
    return NULL;
    }

static AtPmRegister RecentRegisterCreate(AtModuleSur self, AtPmParam param, uint8 recentPeriod)
    {
    AtUnused(self);
    AtUnused(param);
    AtUnused(recentPeriod);
    return NULL;
    }

static AtPmParam SdhLineParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(engine);
    AtUnused(name);
    AtUnused(paramType);
    return NULL;
    }

static AtPmParam SdhPathParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(engine);
    AtUnused(name);
    AtUnused(paramType);
    return NULL;
    }

static AtPmParam PdhDe3ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(engine);
    AtUnused(name);
    AtUnused(paramType);
    return NULL;
    }

static AtPmParam PdhDe1ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(engine);
    AtUnused(name);
    AtUnused(paramType);
    return NULL;
    }

static AtPmParam PwParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(engine);
    AtUnused(name);
    AtUnused(paramType);
    return NULL;
    }

static uint32 NumThresholdProfiles(AtModuleSur self)
    {
    AtUnused(self);
    return 0;
    }

static AtPmThresholdProfile ThresholdProfileCreate(AtModuleSur self, uint32 profileId)
    {
    AtUnused(self);
    AtUnused(profileId);
    return NULL;
    }

static AtPmThresholdProfile ThresholdProfileGet(AtModuleSur self, uint32 profileId)
    {
    if (self->allProfiles == NULL)
        return NULL;

    if (profileId < AtModuleSurNumThresholdProfiles(self))
        return self->allProfiles[profileId];

    return NULL;
    }

static eAtRet AllProfilesCreate(AtModuleSur self)
    {
    uint32 profileId, i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 numProfiles = AtModuleSurNumThresholdProfiles(self);
    AtPmThresholdProfile *allProfiles;

    /* Not support profiles */
    if (numProfiles == 0)
        return cAtOk;

    allProfiles = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtPmThresholdProfile) * numProfiles);
    if (allProfiles == NULL)
        return cAtErrorRsrcNoAvail;

    /* Then create all Profile objects */
    for (profileId = 0; profileId < numProfiles; profileId++)
        {
        allProfiles[profileId] = (AtPmThresholdProfile)mMethodsGet(self)->ThresholdProfileCreate(self, profileId);

        /* Cannot create one line, free all of created resources */
        if (allProfiles[profileId] == NULL)
            {
            for (i = 0; i < profileId; i++)
                AtObjectDelete((AtObject)allProfiles[i]);
            mMethodsGet(osal)->MemFree(osal, allProfiles);
            return cAtErrorRsrcNoAvail;
            }
        }

    /* Creating done */
    self->allProfiles = allProfiles;
    return cAtOk;
    }

static void AllProfilesDelete(AtModuleSur self)
    {
    uint32 profileId;
    AtOsal osal;
    if (self->allProfiles == NULL)
        return;

    osal = AtSharedDriverOsalGet();
    for (profileId = 0; profileId < AtModuleSurNumThresholdProfiles(self); profileId++)
        {
        AtObjectDelete((AtObject)self->allProfiles[profileId]);
        self->allProfiles[profileId] = NULL;
        }

    mMethodsGet(osal)->MemFree(AtSharedDriverOsalGet(), self->allProfiles);
    self->allProfiles = NULL;
    }

static eBool ShouldInitThresholdProfiles(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet AllProfilesInit(AtModuleSur self)
    {
    uint32 profileId;
    eAtRet ret = cAtOk;

    if (self->allProfiles == NULL)
        return cAtOk;

    if (!mMethodsGet(self)->ShouldInitThresholdProfiles(self))
        return cAtOk;


    for (profileId = 0; profileId < AtModuleSurNumThresholdProfiles(self); profileId++)
        ret |= AtPmThresholdProfileInit(self->allProfiles[profileId]);

    return ret;
    }

static AtPmSesThresholdProfile SesThresholdProfileGet(AtModuleSur self, uint32 profileId)
    {
    if (self->allSesProfiles == NULL)
        return NULL;

    if (profileId < AtModuleSurNumSesThresholdProfiles(self))
        return self->allSesProfiles[profileId];

    return NULL;
    }

static AtPmSesThresholdProfile SesThresholdProfileCreate(AtModuleSur self, uint32 profileId)
    {
    AtUnused(self);
    AtUnused(profileId);
    return NULL;
    }

static uint32 NumSesThresholdProfiles(AtModuleSur self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet AllSesProfilesCreate(AtModuleSur self)
    {
    uint32 profileId, i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 numProfiles = AtModuleSurNumSesThresholdProfiles(self);
    AtPmSesThresholdProfile *allProfiles;

    /* Not support profiles */
    if (numProfiles == 0)
        return cAtOk;

    allProfiles = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtPmSesThresholdProfile) * numProfiles);
    if (allProfiles == NULL)
        return cAtErrorRsrcNoAvail;

    /* Then create all Profile objects */
    for (profileId = 0; profileId < numProfiles; profileId++)
        {
        allProfiles[profileId] = (AtPmSesThresholdProfile)mMethodsGet(self)->SesThresholdProfileCreate(self, profileId);

        /* Cannot create one line, free all of created resources */
        if (allProfiles[profileId] == NULL)
            {
            for (i = 0; i < profileId; i++)
                AtObjectDelete((AtObject)allProfiles[i]);
            mMethodsGet(osal)->MemFree(osal, allProfiles);
            return cAtErrorRsrcNoAvail;
            }
        }

    /* Creating done */
    self->allSesProfiles = allProfiles;
    return cAtOk;
    }

static void AllSesProfilesDelete(AtModuleSur self)
    {
    uint32 profileId;
    AtOsal osal;
    if (self->allSesProfiles == NULL)
        return;

    osal = AtSharedDriverOsalGet();
    for (profileId = 0; profileId < AtModuleSurNumSesThresholdProfiles(self); profileId++)
        {
        AtObjectDelete((AtObject)self->allSesProfiles[profileId]);
        self->allSesProfiles[profileId] = NULL;
        }

    mMethodsGet(osal)->MemFree(AtSharedDriverOsalGet(), self->allSesProfiles);
    self->allSesProfiles = NULL;
    }

static eAtRet AllSesProfilesInit(AtModuleSur self)
    {
    uint32 profileId;
    eAtRet ret = cAtOk;

    if (self->allSesProfiles == NULL)
        return cAtOk;

    for (profileId = 0; profileId < AtModuleSurNumSesThresholdProfiles(self); profileId++)
        ret |= AtPmSesThresholdProfileInit(self->allSesProfiles[profileId]);

    return ret;
    }

static eAtRet FailureTimerInit(AtModuleSur self)
    {
    eAtRet ret = cAtOk;

    if (mMethodsGet(self)->FailureHoldOffTimerConfigurable(self))
        ret |= AtModuleSurFailureHoldOffTimerSet(self, cDefaultHoldOffTimerMs);
    if (mMethodsGet(self)->FailureHoldOnTimerConfigurable(self))
        ret |= AtModuleSurFailureHoldOnTimerSet(self, cDefaultHoldOnTimerMs);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    AtModuleSur surModule = mThis(self);
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AllProfilesInit(surModule);
    ret |= AllSesProfilesInit(surModule);
    if (ret != cAtOk)
        return ret;

    ret |= FailureTimerInit(surModule);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static eAtRet Setup(AtModule self)
    {
	eAtRet ret;

    AllProfilesDelete(mThis(self));
    AllSesProfilesDelete(mThis(self));

    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    ret = AllProfilesCreate(mThis(self));
    if (ret != cAtOk)
        return ret;

    return AllSesProfilesCreate(mThis(self));
    }

static void Delete(AtObject self)
    {
    AllProfilesDelete(mThis(self));
    AllSesProfilesDelete(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static eAtRet PeriodExpire(AtModuleSur self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleSur object = (AtModuleSur)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(allProfiles, AtModuleSurNumThresholdProfiles(object));
    mEncodeObjects(allSesProfiles, AtModuleSurNumSesThresholdProfiles(object));
    }

static eAtRet PeriodAsyncExpire(AtModuleSur self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 PeriodElapsedSecondsGet(AtModuleSur self)
    {
    AtUnused(self);
    return 0;
    }

static eBool LineIsSupported(AtModuleSur self)
    {
    /* Sub class must know */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet FailureHoldOffTimerSet(AtModuleSur self, uint32 timerInMs)
    {
    AtUnused(self);
    return (timerInMs == cDefaultHoldOffTimerMs) ? cAtOk : cAtErrorNotEditable;
    }

static uint32 FailureHoldOffTimerGet(AtModuleSur self)
    {
    AtUnused(self);
    return cDefaultHoldOffTimerMs;
    }

static eAtRet FailureHoldOnTimerSet(AtModuleSur self, uint32 timerInMs)
    {
    AtUnused(self);
    return (timerInMs == cDefaultHoldOnTimerMs) ? cAtOk : cAtErrorNotEditable;
    }

static uint32 FailureHoldOnTimerGet(AtModuleSur self)
    {
    AtUnused(self);
    return cDefaultHoldOnTimerMs;
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    /* Most of designs so far cannot change these timers */
    AtUnused(self);
    return cAtFalse;
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    /* Most of designs so far cannot change these timers */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ExpireMethodSet(AtModuleSur self, eAtSurPeriodExpireMethod method)
    {
    AtUnused(self);
    return (method == cAtSurPeriodExpireMethodAuto) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtSurPeriodExpireMethod ExpireMethodGet(AtModuleSur self)
    {
    AtUnused(self);
    return cAtSurPeriodExpireMethodAuto;
    }

static eAtRet TickSourceSet(AtModuleSur self, eAtSurTickSource source)
    {
    AtUnused(self);
    return (source == cAtSurTickSourceInternal) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtSurTickSource TickSourceGet(AtModuleSur self)
    {
    AtUnused(self);
    return cAtSurTickSourceInternal;
    }

static eAtRet ExpireNotificationEnable(AtModuleSur self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool ExpireNotificationIsEnabled(AtModuleSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }
    
static eBool FailureIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }
    
static eBool ShouldMaskFailuresWhenNonAutonomousFailures(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void EventListenerCall(AtModuleSurEventListenerWrapper self)
    {
    AtModuleEventListenerWrapper wrapper = (AtModuleEventListenerWrapper)self;
    if (self->listener.ExpireNotify)
        self->listener.ExpireNotify((AtModuleSur)wrapper->module, wrapper->userData);
    }

static uint32 EventListenerWrapperObjectSize(void)
    {
    return sizeof(tAtModuleSurEventListenerWrapper);
    }

static AtModuleEventListenerWrapper EventListenerWrapperObjectInit(AtModuleEventListenerWrapper wrapper, AtModule module, void* listener, void* userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSurEventListenerWrapper thisWrapper = (AtModuleSurEventListenerWrapper)wrapper;

    mMethodsGet(osal)->MemInit(osal, wrapper, 0, EventListenerWrapperObjectSize());

    AtModuleEventListenerWrapperObjectInit(wrapper, module, listener, userData);
    mMethodsGet(osal)->MemCpy(osal, &(thisWrapper->listener), listener, sizeof(tAtModuleSurEventListener));

    return wrapper;
    }

static AtModuleEventListenerWrapper EventListenerWrapperNew(AtModule self, void* listener, void* userData)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEventListenerWrapper newListener = mMethodsGet(osal)->MemAlloc(osal, EventListenerWrapperObjectSize());

    if (newListener)
        return EventListenerWrapperObjectInit(newListener, self, listener, userData);

    return NULL;
    }

static AtModuleEventListenerWrapper ListenerWrapperCreate(AtModule self, void* listener, void* userData)
    {
    return EventListenerWrapperNew(self, listener, userData);
    }

static eBool EventListenersAreIdentical(AtModule self, AtModuleEventListenerWrapper registeredListener, void* listener)
    {
    tAtModuleSurEventListener *input = (tAtModuleSurEventListener *)listener;
    AtModuleSurEventListenerWrapper local = (AtModuleSurEventListenerWrapper)registeredListener;
    AtUnused(self);

    if (input->ExpireNotify != local->listener.ExpireNotify)
        return cAtFalse;

    return cAtTrue;
    }

static eBool TcaIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtModuleSur self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(object);
        AtOsalMemCpy(&m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, RecommendedPeriodInMs);
        mMethodOverride(m_AtModuleOverride, ListenerWrapperCreate);
        mMethodOverride(m_AtModuleOverride, EventListenersAreIdentical);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

static void MethodsInit(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PeriodExpire);
        mMethodOverride(m_methods, PeriodAsyncExpire);
        mMethodOverride(m_methods, PeriodElapsedSecondsGet);
        mMethodOverride(m_methods, ExpireMethodSet);
        mMethodOverride(m_methods, ExpireMethodGet);
        mMethodOverride(m_methods, TickSourceSet);
        mMethodOverride(m_methods, TickSourceGet);
        mMethodOverride(m_methods, ExpireNotificationEnable);
        mMethodOverride(m_methods, ExpireNotificationIsEnabled);
        mMethodOverride(m_methods, FailureIsSupported);
        mMethodOverride(m_methods, SdhLineEngineCreate);
        mMethodOverride(m_methods, SdhHoPathEngineCreate);
        mMethodOverride(m_methods, SdhLoPathEngineCreate);
        mMethodOverride(m_methods, PdhDe1EngineCreate);
        mMethodOverride(m_methods, PdhDe3EngineCreate);
        mMethodOverride(m_methods, PwEngineCreate);
        mMethodOverride(m_methods, SdhLineEngineDelete);
        mMethodOverride(m_methods, SdhHoPathEngineDelete);
        mMethodOverride(m_methods, SdhLoPathEngineDelete);
        mMethodOverride(m_methods, PdhDe1EngineDelete);
        mMethodOverride(m_methods, PdhDe3EngineDelete);
        mMethodOverride(m_methods, PwEngineDelete);
        mMethodOverride(m_methods, CurrentSecondRegisterCreate);
        mMethodOverride(m_methods, CurrentPeriodRegisterCreate);
        mMethodOverride(m_methods, PreviousPeriodRegisterCreate);
        mMethodOverride(m_methods, CurrentDayRegisterCreate);
        mMethodOverride(m_methods, PreviousDayRegisterCreate);
        mMethodOverride(m_methods, RecentRegisterCreate);
        mMethodOverride(m_methods, SdhLineParamCreate);
        mMethodOverride(m_methods, SdhPathParamCreate);
        mMethodOverride(m_methods, PdhDe3ParamCreate);
        mMethodOverride(m_methods, PdhDe1ParamCreate);
        mMethodOverride(m_methods, PwParamCreate);
        mMethodOverride(m_methods, LineIsSupported);
        mMethodOverride(m_methods, ShouldMaskFailuresWhenNonAutonomousFailures);
        mMethodOverride(m_methods, TcaIsSupported);
        mMethodOverride(m_methods, FailureHoldOffTimerSet);
        mMethodOverride(m_methods, FailureHoldOffTimerGet);
        mMethodOverride(m_methods, FailureHoldOffTimerConfigurable);
        mMethodOverride(m_methods, FailureHoldOnTimerSet);
        mMethodOverride(m_methods, FailureHoldOnTimerGet);
        mMethodOverride(m_methods, FailureHoldOnTimerConfigurable);

        /* PM Threshold Profile */
        mMethodOverride(m_methods, NumThresholdProfiles);
        mMethodOverride(m_methods, ThresholdProfileGet);
        mMethodOverride(m_methods, ThresholdProfileCreate);
        mMethodOverride(m_methods, ShouldInitThresholdProfiles);

        /* SES Threshold Profile */
        mMethodOverride(m_methods, NumSesThresholdProfiles);
        mMethodOverride(m_methods, SesThresholdProfileGet);
        mMethodOverride(m_methods, SesThresholdProfileCreate);
        }

    mMethodsSet(self, &m_methods);
    }

AtModuleSur AtModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModuleSur));

    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModuleSur, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtSurEngine AtModuleSurSdhLineEngineCreate(AtModuleSur self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->SdhLineEngineCreate(self, line);
    return NULL;
    }

AtSurEngine AtModuleSurSdhHoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    if (self)
        return mMethodsGet(self)->SdhHoPathEngineCreate(self, path);
    return NULL;
    }

AtSurEngine AtModuleSurSdhLoPathEngineCreate(AtModuleSur self, AtSdhPath path)
    {
    if (self)
        return mMethodsGet(self)->SdhLoPathEngineCreate(self, path);
    return NULL;
    }

AtSurEngine AtModuleSurPdhDe3EngineCreate(AtModuleSur self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->PdhDe3EngineCreate(self, de3);
    return NULL;
    }

AtSurEngine AtModuleSurPdhDe1EngineCreate(AtModuleSur self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->PdhDe1EngineCreate(self, de1);
    return NULL;
    }

AtSurEngine AtModuleSurPwEngineCreate(AtModuleSur self, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->PwEngineCreate(self, pw);
    return NULL;
    }

eAtRet AtModuleSurEngineSdhLineDelete(AtModuleSur self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->SdhLineEngineDelete(self, engine);
    return cAtOk;
    }

eAtRet AtModuleSurEngineSdhHoPathDelete(AtModuleSur self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->SdhHoPathEngineDelete(self, engine);
    return cAtOk;
    }

eAtRet AtModuleSurEngineSdhLoPathDelete(AtModuleSur self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->SdhLoPathEngineDelete(self, engine);
    return cAtOk;
    }

eAtRet AtModuleSurEnginePdhDe3Delete(AtModuleSur self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->PdhDe3EngineDelete(self, engine);
    return cAtOk;
    }

eAtRet AtModuleSurEnginePdhDe1Delete(AtModuleSur self, AtSurEngine engine)
    {
    if (self)
        return mMethodsGet(self)->PdhDe1EngineDelete(self, engine);
    return cAtOk;
    }

eAtRet AtModuleSurEnginePwDelete(AtModuleSur self, AtSurEngine pw)
    {
    if (self)
        return mMethodsGet(self)->PwEngineDelete(self, pw);
    return cAtOk;
    }

AtPmRegister AtModuleSurCurrentSecondRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    if (self)
        return mMethodsGet(self)->CurrentSecondRegisterCreate(self, param);
    return NULL;
    }

AtPmRegister AtModuleSurCurrentPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    if (self)
        return mMethodsGet(self)->CurrentPeriodRegisterCreate(self, param);
    return NULL;
    }

AtPmRegister AtModuleSurPreviousPeriodRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    if (self)
        return mMethodsGet(self)->PreviousPeriodRegisterCreate(self, param);
    return NULL;
    }

AtPmRegister AtModuleSurCurrentDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    if (self)
        return mMethodsGet(self)->CurrentDayRegisterCreate(self, param);
    return NULL;
    }

AtPmRegister AtModuleSurPreviousDayRegisterCreate(AtModuleSur self, AtPmParam param)
    {
    if (self)
        return mMethodsGet(self)->PreviousDayRegisterCreate(self, param);
    return NULL;
    }

AtPmRegister AtModuleSurRecentRegisterCreate(AtModuleSur self, AtPmParam param, uint8 recentPeriod)
    {
    if (self)
        return mMethodsGet(self)->RecentRegisterCreate(self, param, recentPeriod);
    return NULL;
    }

AtPmParam AtModuleSurSdhLineParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    if (self)
        return mMethodsGet(self)->SdhLineParamCreate(self, engine, name, paramType);
    return NULL;
    }

AtPmParam AtModuleSurSdhPathParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    if (self)
        return mMethodsGet(self)->SdhPathParamCreate(self, engine, name, paramType);
    return NULL;
    }

AtPmParam AtModuleSurPdhDe3ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    if (self)
        return mMethodsGet(self)->PdhDe3ParamCreate(self, engine, name, paramType);
    return NULL;
    }

AtPmParam AtModuleSurPdhDe1ParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    if (self)
        return mMethodsGet(self)->PdhDe1ParamCreate(self, engine, name, paramType);
    return NULL;
    }

AtPmParam AtModuleSurPwParamCreate(AtModuleSur self, AtSurEngine engine, const char *name, uint32 paramType)
    {
    if (self)
        return mMethodsGet(self)->PwParamCreate(self, engine, name, paramType);
    return NULL;
    }

eBool AtModuleSurLineIsSupported(AtModuleSur self)
    {
    if (self)
        return mMethodsGet(self)->LineIsSupported(self);
    return cAtFalse;
    }

void AtModuleSurAllEventListenersCall(AtModuleSur self)
    {
    AtModule module = (AtModule)self;
    AtModuleSurEventListenerWrapper aListener;
    AtIterator iterator;

    if ((module == NULL) || (module->allEventListeners == NULL))
        return;

    /* For all event listeners */
    iterator = AtListIteratorCreate(module->allEventListeners);
    while((aListener = (AtModuleSurEventListenerWrapper)AtIteratorNext(iterator)) != NULL)
        EventListenerCall(aListener);

    AtObjectDelete((AtObject)iterator);
    }

uint32 AtModuleSurNumSesThresholdProfiles(AtModuleSur self)
    {
    mAttributeGet(NumSesThresholdProfiles, uint32, 0);
    }

AtPmSesThresholdProfile AtModuleSurSesThresholdProfileGet(AtModuleSur self, uint32 profileId)
    {
    mOneParamAttributeGet(SesThresholdProfileGet, profileId, AtPmSesThresholdProfile, NULL);
    }

eBool AtModuleSurShouldMaskFailuresWhenNonAutonomousFailures(AtModuleSur self)
    {
    if (self)
        return mMethodsGet(self)->ShouldMaskFailuresWhenNonAutonomousFailures(self);
    return cAtFalse;
    }

eBool AtModuleSurFailureIsSupported(AtModuleSur self)
    {
    mAttributeGet(FailureIsSupported, eBool, cAtFalse);
    }

eBool AtModuleSurTcaIsSupported(AtModuleSur self)
    {
    mAttributeGet(TcaIsSupported, eBool, cAtFalse);
    }

/**
 * @addtogroup AtModuleSur
 * @{
 */

/**
 * Apply 15-minutes period expiration on hardware mechanism. When a period
 * expires, application need to call this API. This API will internally request
 * hardware and block caller until hardware returns ready indication.
 *
 * @param self This module
 *
 * @return AT return code
 */
eAtRet AtModuleSurPeriodExpire(AtModuleSur self)
    {
    mDoNothingOnStandby(cAtOk);
    mAttributeGet(PeriodExpire, eAtRet, cAtErrorNullPointer);
    }

/**
 * Apply 15-minutes period expiration on hardware mechanism. When a period
 * expires, application need to call this API.
 *
 * Does not like the AtModuleSurPeriodExpire() which is blocking call, this API
 * will make request to hardware, if hardware finishes request immediately, it
 * then returns success error code. Otherwise, it will return cAtErrorAgain error
 * code to let application know that it should call again.
 *
 * @note If period expiring is in progress, application should not call any APIs
 * to query counters on any engines. Doing so may lead to undetermined situation
 * as hardware is busy for moving counters.
 *
 * @param self This module
 *
 * @retval cAtErrorAgain If expire call has not been complete and still waiting
 *         for hardware ready indication
 * @retval cAtOk If expire call is complete
 * @retval Other AT error code for failure
 */
eAtRet AtModuleSurPeriodAsyncExpire(AtModuleSur self)
    {
    if (self)
        {
        mDoNothingOnStandby(cAtOk);
        return mMethodsGet(self)->PeriodAsyncExpire(self);
        }

    return cAtErrorNullPointer;
    }

/**
 * Get elapsed time in seconds of the current period.
 *
 * @param self This module
 *
 * @return Elapsed time in seconds.
 */
uint32 AtModuleSurPeriodElapsedSecondsGet(AtModuleSur self)
    {
    mAttributeGet(PeriodElapsedSecondsGet, uint32, 0);
    }

/**
 * Get Number of PM Threshold Profiles.
 *
 * @param self This module
 *
 * @return Number of PM Threshold Profiles.
 */
uint32 AtModuleSurNumThresholdProfiles(AtModuleSur self)
    {
    mAttributeGet(NumThresholdProfiles, uint32, 0);
    }

/**
 * Get PM Threshold Profile by profile index.
 *
 * @param self This module
 * @param profileId The profile index
 *
 * @return PM Threshold Profile
 */
AtPmThresholdProfile AtModuleSurThresholdProfileGet(AtModuleSur self, uint32 profileId)
    {
    mOneParamAttributeGet(ThresholdProfileGet, profileId, AtPmThresholdProfile, NULL);
    }

/**
 * Set failure hold-off timer.
 *
 * @param self This module
 * @param timerInMs Failure hold-off timer. When defect persists for timerInMs +/- 500ms
 *                  then corresponding failure will raise
 *
 * @return AT return code
 */
eAtRet AtModuleSurFailureHoldOffTimerSet(AtModuleSur self, uint32 timerInMs)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (mMethodsGet(self)->FailureHoldOffTimerConfigurable(self))
        return mMethodsGet(self)->FailureHoldOffTimerSet(self, timerInMs);

    return (timerInMs == cDefaultHoldOffTimerMs) ? cAtOk : cAtErrorNotEditable;
    }

/**
 * Get failure hold-off timer
 *
 * @param self This timer
 *
 * @return Failure hold-off timer. When defect persists for timerInMs +/- 500ms then
 * corresponding failure will raise
 */
uint32 AtModuleSurFailureHoldOffTimerGet(AtModuleSur self)
    {
    if (self == NULL)
        return cInvalidUint32;

    if (mMethodsGet(self)->FailureHoldOffTimerConfigurable(self))
        return mMethodsGet(self)->FailureHoldOffTimerGet(self);

    return cDefaultHoldOffTimerMs;
    }

/**
 * Set failure hold-on timer.
 *
 * @param self This module
 * @param timerInMs Failure hold-on timer. When defect clears for timerInMs +/- 500ms,
 *                  then corresponding failure will clear
 *
 * @return AT return code
 */
eAtRet AtModuleSurFailureHoldOnTimerSet(AtModuleSur self, uint32 timerInMs)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (mMethodsGet(self)->FailureHoldOnTimerConfigurable(self))
        return mMethodsGet(self)->FailureHoldOnTimerSet(self, timerInMs);

    return (timerInMs == cDefaultHoldOnTimerMs) ? cAtOk : cAtErrorNotEditable;
    }

/**
 * Get failure hold-on timer
 *
 * @param self This module
 *
 * @return Failure hold-on timer. When defect clears for timerInMs +/- 500ms, then
 * corresponding failure will clear
 */
uint32 AtModuleSurFailureHoldOnTimerGet(AtModuleSur self)
    {
    if (self == NULL)
        return cInvalidUint32;

    if (mMethodsGet(self)->FailureHoldOnTimerConfigurable(self))
        return mMethodsGet(self)->FailureHoldOnTimerGet(self);

    return cDefaultHoldOnTimerMs;
    }

/**
 * Set period expire method (manual or auto)
 *
 * @param self This module
 * @param method @ref eAtSurPeriodExpireMethod "Expire method"
 *
 * @return AT return code
 */
eAtRet AtModuleSurExpireMethodSet(AtModuleSur self, eAtSurPeriodExpireMethod method)
    {
    mNumericalAttributeSet(ExpireMethodSet, method);
    }

/**
 * Get period expire method (manual or auto)
 *
 * @param self This module
 *
 * @return @ref eAtSurPeriodExpireMethod "Expire method"
 */
eAtSurPeriodExpireMethod AtModuleSurExpireMethodGet(AtModuleSur self)
    {
    mAttributeGet(ExpireMethodGet, eAtSurPeriodExpireMethod, cAtSurPeriodExpireMethodUnknown);
    }

/**
 * Select tick source
 *
 * @param self This module
 * @param source @ref eAtSurTickSource "Tick source"
 *
 * @return AT return code
 */
eAtRet AtModuleSurTickSourceSet(AtModuleSur self, eAtSurTickSource source)
    {
    mNumericalAttributeSet(TickSourceSet, source);
    }

/**
 * Get tick source
 *
 * @param self This module
 *
 * @return @ref eAtSurTickSource "Tick source"
 */
eAtSurTickSource AtModuleSurTickSourceGet(AtModuleSur self)
    {
    mAttributeGet(TickSourceGet, eAtSurTickSource, cAtSurTickSourceUnknown);
    }

/**
 * Enable/disable period expiration notification
 *
 * @param self This module
 * @param enable Notification enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtModuleSurExpireNotificationEnable(AtModuleSur self, eBool enable)
    {
    mNumericalAttributeSet(ExpireNotificationEnable, enable);
    }

/**
 * Get the enable state of period expiration notification
 *
 * @param self This module
 *
 * @retval cAtTrue if expiration notification is enabled
 * @retval cAtFalse if expiration notification is disabled
 */
eBool AtModuleSurExpireNotificationIsEnabled(AtModuleSur self)
    {
    mAttributeGet(ExpireNotificationIsEnabled, eBool, cAtFalse);
    }

/**
 * @}
 */

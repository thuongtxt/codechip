/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtSurEngineSdhLine.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : SDH Line Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtSurEngineInternal.h"
#include "AtModuleSurInternal.h"
#include "AtPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtSurEngineSdhLine *)self)

/*--------------------------- Macros -----------------------------------------*/
#define mParamType(field)                                                      \
    {                                                                          \
    if (engine->field == NULL)                                                 \
        engine->field = AtModuleSurSdhLineParamCreate(AtSurEngineModuleGet(self), self, #field, paramType); \
    return engine->field;                                                      \
    }

#define mPmCounterGet(regtype, field)                                          \
    {                                                                          \
    counters->field = AtPmRegisterValue(AtPmParamRegisterGet(engine->field, regtype));\
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtSurEngineMethods m_AtSurEngineOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPmParam PmParamGet(AtSurEngine self, uint32 paramType)
    {
    AtSurEngineSdhLine engine = (AtSurEngineSdhLine)self;

    switch (paramType)
        {
        case cAtSurEngineSdhLinePmParamSefsS :
            mParamType(sefsS)
        case cAtSurEngineSdhLinePmParamCvS   :
            mParamType(cvS)
        case cAtSurEngineSdhLinePmParamEsS   :
            mParamType(esS)
        case cAtSurEngineSdhLinePmParamSesS  :
            mParamType(sesS)
        case cAtSurEngineSdhLinePmParamCvL   :
            mParamType(cvL)
        case cAtSurEngineSdhLinePmParamEsL   :
            mParamType(esL)
        case cAtSurEngineSdhLinePmParamSesL  :
            mParamType(sesL)
        case cAtSurEngineSdhLinePmParamUasL  :
            mParamType(uasL)
        case cAtSurEngineSdhLinePmParamFcL   :
            mParamType(fcL)
        case cAtSurEngineSdhLinePmParamCvLfe :
            mParamType(cvLfe)
        case cAtSurEngineSdhLinePmParamEsLfe :
            mParamType(esLfe)
        case cAtSurEngineSdhLinePmParamSesLfe:
            mParamType(sesLfe)
        case cAtSurEngineSdhLinePmParamUasLfe:
            mParamType(uasLfe)
        case cAtSurEngineSdhLinePmParamFcLfe :
            mParamType(fcLfe)
        default:
            return NULL;
        }

    return NULL;
    }

static eAtRet CountersReadOnClear(AtSurEngine self, eAtPmRegisterType type, tAtSurEngineSdhLinePmCounters *counters, eBool readOnClear)
    {
    AtSurEngineSdhLine engine = (AtSurEngineSdhLine)self;
    eAtRet ret = AtSurEngineRegistersLatchAndClear(self, type, readOnClear);
    if (ret != cAtOk)
        return ret;

    /* Let the active driver fill each field, only clear memory on standby as it
     * is not able to access hardware */
    if (AtSurEngineInAccessible(self))
        {
        AtOsalMemInit(counters, 0, sizeof(tAtSurEngineSdhLinePmCounters));
        return cAtOk;
        }

    mPmCounterGet(type, sefsS);
    mPmCounterGet(type, cvS);
    mPmCounterGet(type, esS);
    mPmCounterGet(type, sesS);
    mPmCounterGet(type, cvL);
    mPmCounterGet(type, esL);
    mPmCounterGet(type, sesL);
    mPmCounterGet(type, uasL);
    mPmCounterGet(type, fcL);
    mPmCounterGet(type, cvLfe);
    mPmCounterGet(type, esLfe);
    mPmCounterGet(type, sesLfe);
    mPmCounterGet(type, uasLfe);
    mPmCounterGet(type, fcLfe);

    return cAtOk;
    }

static eAtRet RegisterAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEngineSdhLinePmCounters*)counters, cAtFalse);
    }

static eAtRet RegisterAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEngineSdhLinePmCounters*)counters, cAtTrue);
    }

static uint32 CurrentFailuresGet(AtSurEngine self)
    {
    static const uint32 cSectionFailures = cAtSdhLineAlarmLos|cAtSdhLineAlarmLof|cAtSdhLineAlarmTim;
    uint32 failures = AtSurEngineNonAutonomousFailuresGet(self);

    /* If Line AIS failure caused by Section maintenance signal, it should mask all of Line failures. */
    if (failures && AtModuleSurShouldMaskFailuresWhenNonAutonomousFailures(AtSurEngineModuleGet(self)))
        return (AtSurEngineAutonomousFailuresGet(self) & cSectionFailures);

    failures |= AtSurEngineAutonomousFailuresGet(self);
    return failures;
    }

static void DeletePmParam(AtPmParam *param)
    {
    AtObjectDelete((AtObject)*param);
    *param = NULL;
    }

static void Delete(AtObject self)
    {
    DeletePmParam(&mThis(self)->sefsS);
    DeletePmParam(&mThis(self)->cvS);
    DeletePmParam(&mThis(self)->esS);
    DeletePmParam(&mThis(self)->sesS);
    DeletePmParam(&mThis(self)->cvL);
    DeletePmParam(&mThis(self)->esL);
    DeletePmParam(&mThis(self)->sesL);
    DeletePmParam(&mThis(self)->uasL);
    DeletePmParam(&mThis(self)->fcL);
    DeletePmParam(&mThis(self)->cvLfe);
    DeletePmParam(&mThis(self)->esLfe);
    DeletePmParam(&mThis(self)->sesLfe);
    DeletePmParam(&mThis(self)->uasLfe);
    DeletePmParam(&mThis(self)->fcLfe);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSurEngineSdhLine object = (AtSurEngineSdhLine)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(sefsS);
    mEncodeObject(cvS);
    mEncodeObject(esS);
    mEncodeObject(sesS);
    mEncodeObject(cvL);
    mEncodeObject(esL);
    mEncodeObject(sesL);
    mEncodeObject(uasL);
    mEncodeObject(fcL);
    mEncodeObject(cvLfe);
    mEncodeObject(esLfe);
    mEncodeObject(sesLfe);
    mEncodeObject(uasLfe);
    mEncodeObject(fcLfe);
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmParamGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersClear);
        mMethodOverride(m_AtSurEngineOverride, CurrentFailuresGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtObject(self);
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEngineSdhLine);
    }

AtSurEngine AtSurEngineSdhLineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

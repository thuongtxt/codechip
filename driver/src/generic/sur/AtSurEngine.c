/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Surveillance
 *
 * File        : AtSurEngine.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : Surveillance engine abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtList.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../common/AtChannelInternal.h"
#include "AtSurEngineInternal.h"
#include "AtModuleSurInternal.h"
#include "AtPmThresholdProfile.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtSurEngine)(self))

/*--------------------------- Macros -----------------------------------------*/
#define mInAccessible(self) AtSurEngineInAccessible(self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtSurEngineMethods m_methods;

/* Override */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/* Save super implementation */
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtSurEngine self)
    {
    eAtRet ret = cAtOk;
    AtModuleSur module = AtSurEngineModuleGet(self);

    if (AtModuleSurNumThresholdProfiles(module))
        ret |= AtSurEngineThresholdProfileSet(self, AtModuleSurThresholdProfileGet(module, 0));

    if (AtModuleSurNumSesThresholdProfiles(module))
        ret |= AtSurEngineSesThresholdProfileSet(self, AtModuleSurSesThresholdProfileGet(module, 0));
    if (AtModuleSurFailureIsSupported((AtModuleSur)module))
        ret |= AtSurEngineFailureNotificationMaskSet(self, AtSurEngineFailureNotificationMaskGet(self), 0);
    ret |= AtSurEngineEnable(self, cAtTrue);

    if (AtSurEnginePmCanEnable(self, cAtFalse))
        ret |= AtSurEnginePmEnable(self, cAtFalse);

    return ret;
    }

static eAtRet Enable(AtSurEngine self, eBool enable)
    {
    self->enable = enable;
    return cAtOk;
    }

static eBool IsEnabled(AtSurEngine self)
    {
    return self->enable;
    }

static eAtRet PmEnable(AtSurEngine self, eBool enable)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PmIsEnabled(AtSurEngine self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtFalse;
    }

static eBool PmCanEnable(AtSurEngine self, eBool enable)
    {
    AtUnused(self);
    /* As default, PM is always enabled */
    return enable;
    }

static uint32 CurrentFailuresGet(AtSurEngine self)
    {
    uint32 failures = AtSurEngineNonAutonomousFailuresGet(self);

    /* If AIS failure caused by maintenance signal, it should mask another failures. */
    if (failures && AtModuleSurShouldMaskFailuresWhenNonAutonomousFailures(AtSurEngineModuleGet(self)))
        return 0;

    failures |= AtSurEngineAutonomousFailuresGet(self);
    return failures;
    }

static uint32 FailureChangedHistoryGet(AtSurEngine self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static uint32 FailureChangedHistoryClear(AtSurEngine self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static uint32 TcaChangedHistoryGet(AtSurEngine self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static uint32 TcaChangedHistoryClear(AtSurEngine self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static uint32 SesThresholdPoolEntryGet(AtSurEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet SesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtErrorNotImplemented;
    }

static uint32 TcaThresholdPoolEntryGet(AtSurEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet TcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    AtUnused(self);
    AtUnused(entryIndex);
    return cAtErrorNotImplemented;
    }

static tSurEngineEventListener *EventListenerCreate(void* userData, const tAtSurEngineListener *callbacks)
    {
    tSurEngineEventListener *newListener = AtOsalMemAlloc((uint32)sizeof(tSurEngineEventListener));
    if (newListener == NULL)
        return NULL;

    /* Need to alloc memory to cache callback */
    newListener->callbacks = AtOsalMemAlloc(sizeof(tAtSurEngineListener));
    if (newListener->callbacks == NULL)
        {
        AtOsalMemFree(newListener);
        return NULL;
        }

    AtOsalMemCpy((void*)(newListener->callbacks), (const void*)callbacks, (uint32)sizeof(tAtSurEngineListener));
    newListener->userData = userData;

    return newListener;
    }

static eBool CallbacksAreSame(const tAtSurEngineListener *callback1, const tAtSurEngineListener *callback2)
    {
    return (AtOsalMemCmp(callback1, callback2, sizeof(tAtSurEngineListener)) == 0) ? cAtTrue : cAtFalse;
    }

static tSurEngineEventListener *EventListenerFind(AtSurEngine self,  void *userData, const tAtSurEngineListener *callbacks, uint32 *listenerIndex)
    {
    uint32 i;

    if (self->listeners == NULL)
        return NULL;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tSurEngineEventListener *aListener = (tSurEngineEventListener *)AtListObjectGet(self->listeners, i);
        if ((aListener->userData == userData) && CallbacksAreSame(aListener->callbacks, callbacks))
            {
            if (listenerIndex)
                *listenerIndex = i;
            return aListener;
            }
        }

    return NULL;
    }

static eAtRet ListenerAdd(AtSurEngine self, void *userData, const tAtSurEngineListener *callback)
    {
    if (callback == NULL)
        return cAtErrorNullPointer;

    /* Already add, ignore */
    if (EventListenerFind(self, userData, callback, NULL))
        return cAtOk;

    return AtListObjectAdd(self->listeners, (AtObject)EventListenerCreate(userData, callback));
    }

static void EventListenerDelete(tSurEngineEventListener* eventListener)
    {
    if (eventListener->callbacks)
        AtOsalMemFree((void*)(eventListener->callbacks));

    AtOsalMemFree(eventListener);
    }

static eAtRet ListenerRemove(AtSurEngine self, void *userData, const tAtSurEngineListener *callback)
    {
    uint32 listenerIndex;

    if (callback == NULL)
        return cAtErrorNullPointer;

    if (EventListenerFind(self, userData, callback, &listenerIndex))
        {
        tSurEngineEventListener* wrapListener = (tSurEngineEventListener *)AtListObjectRemoveAtIndex((AtList)(mThis(self)->listeners), listenerIndex);
        EventListenerDelete(wrapListener);
        }

    return cAtOk;
    }

static AtPmParam PmParamGet(AtSurEngine self, uint32 paramType)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(paramType);
    return NULL;
    }

static eBool PmParamIsSupported(AtSurEngine self, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(paramType);
    /* Default support full. Let concrete class determine what would not be supported. */
    return cAtTrue;
    }

static eBool PmParamIsDefined(AtSurEngine self, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(paramType);
    /* Default all defined. Let concrete class determine what would not be applicably defined. */
    return cAtTrue;
    }

static void DeleteAllListeners(AtObject self)
    {
    AtSurEngine engine = mThis(self);
    AtIterator iterator;

    if (engine->listeners == NULL)
        return;

    /* Delete all listeners in list */
    iterator = AtListIteratorCreate(engine->listeners);
    if (iterator)
        {
        tSurEngineEventListener *aListener;
        while((aListener = (tSurEngineEventListener *)AtIteratorNext(iterator)) != NULL)
            EventListenerDelete(aListener);
        }
    AtObjectDelete((AtObject)iterator);

    /* Delete event listener list */
    AtObjectDelete((AtObject)(engine->listeners));
    engine->listeners = NULL;
    }

static void DeleteAllTimer(AtSurEngine self)
    {
    if (self->periodStartTime)
        {
        AtOsalMemFree(self->periodStartTime);
        self->periodStartTime = NULL;
        }
    if (self->currentTime)
        {
        AtOsalMemFree(self->currentTime);
        self->currentTime = NULL;
        }
    }

static void Delete(AtObject self)
    {
    DeleteAllTimer((AtSurEngine)self);
    DeleteAllListeners(self);
    m_AtObjectMethods->Delete(self);
    }

static const char *ToString(AtObject self)
    {
    static char description[200];
    AtChannel channel = (AtChannel)AtSurEngineChannelGet((AtSurEngine)self);

    AtSprintf(description, "%s.%s", "sur_engine", AtObjectToString((AtObject)channel));

    return description;
    }

static void WillDeleteNotify(AtSurEngine self)
    {
    uint32 i;

    if ((self == NULL) || (self->listeners == NULL))
        return;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tSurEngineEventListener *listener = (tSurEngineEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks && listener->callbacks->WillDelete)
            listener->callbacks->WillDelete(self, listener->userData);
        }
    }

static void WillDelete(AtObject self)
    {
    WillDeleteNotify((AtSurEngine) self);
    }

static eAtRet PeriodExpire(AtSurEngine self, void *data)
    {
    AtUnused(self);
    AtUnused(data);
    if (!self->periodStartTime)
        self->periodStartTime = AtOsalMemAlloc(sizeof(tAtOsalCurTime));
    AtOsalMemInit(self->periodStartTime, sizeof(tAtOsalCurTime), 0);
    AtOsalCurTimeGet(self->periodStartTime);
    return cAtOk;
    }

static eAtRet TimeUpdate(AtSurEngine self)
    {
    if (!self->currentTime)
        self->currentTime = AtOsalMemAlloc(sizeof(tAtOsalCurTime));
    if (self->currentTime && self->periodStartTime)
        {
        AtOsalCurTimeGet(self->currentTime);
        AtOsalCurTimeGet(self->currentTime);
        self->differenceTime = (int32)AtOsalDifferenceTimeInMs(self->currentTime, self->periodStartTime)/1000;
        }
    if (!self->periodStartTime)
        self->differenceTime = 900;
    return cAtOk;
    }

static eAtRet AllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    AtUnused(self);
    AtUnused(clear);
    return TimeUpdate(self);
    }

static eAtRet RegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    AtUnused(self);
    AtUnused(type);
    AtUnused(clear);
    return TimeUpdate(self);
    }

static eAtRet RegisterAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    AtUnused(self);
    AtUnused(type);
    AtUnused(counters);
    return cAtErrorNotImplemented;
    }

static eAtRet RegisterAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    AtUnused(self);
    AtUnused(type);
    AtUnused(counters);
    return cAtErrorNotImplemented;
    }

static uint32 PmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type)
    {
    AtUnused(self);
    AtUnused(type);
    return 0;
    }

static eAtRet Provision(AtSurEngine self, eBool provision)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(provision);
    return cAtOk;
    }

static eAtRet FailureNotificationMaskSet(AtSurEngine self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    AtUnused(enableMask);
    return cAtErrorNotImplemented;
    }

static uint32 FailureNotificationMaskGet(AtSurEngine self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtRet TcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(paramMask);
    AtUnused(enableMask);
    return cAtErrorNotImplemented;
    }

static uint32 TcaNotificationMaskGet(AtSurEngine self)
    {
    AtUnused(self);
    return 0x0;
    }

static void FailureNotificationProcess(AtSurEngine self, uint32 offset)
    {
    AtUnused(self);
    AtUnused(offset);
    }

static void TcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    AtUnused(self);
    AtUnused(offset);
    }

static uint32 DefaultOffset(AtSurEngine self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet FailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool FailureNotificationIsEnabled(AtSurEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool TcaNotificationIsEnabled(AtSurEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 AutonomousFailuresGet(AtSurEngine self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NonAutonomousFailuresGet(AtSurEngine self)
    {
    AtUnused(self);
    return 0;
    }

static AtChannel ChannelGet(AtSurEngine self)
    {
    return self->channel;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSurEngine object = (AtSurEngine)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeChannelIdString(channel);
    mEncodeUInt(isProvisioned);
    mEncodeUInt(enable);
    mEncodeObjectDescription(module);

    mEncodeNone(listeners);
    mEncodeNone(periodStartTime);
    mEncodeNone(currentTime);
    mEncodeNone(differenceTime);
    }

static eAtRet ThresholdProfileSet(AtSurEngine self, AtPmThresholdProfile profile)
    {
    uint32 profileId;

    if (profile == NULL)
        return cAtErrorNullPointer;

    profileId = AtPmThresholdProfileIdGet(profile);
    AtSurEngineTcaThresholdPoolEntrySet(self, profileId);
    return cAtOk;
    }

static AtPmThresholdProfile ThresholdProfileGet(AtSurEngine self)
    {
    uint32 profileId = AtSurEngineTcaThresholdPoolEntryGet(self);
    return AtModuleSurThresholdProfileGet(AtSurEngineModuleGet(self), profileId);
    }

static eAtRet SesThresholdProfileSet(AtSurEngine self, AtPmSesThresholdProfile profile)
    {
    uint32 profileId;

    if (profile == NULL)
        return cAtErrorNullPointer;

    profileId = AtPmSesThresholdProfileIdGet(profile);
    AtSurEngineSesThresholdPoolEntrySet(self, profileId);
    return cAtOk;
    }

static AtPmSesThresholdProfile SesThresholdProfileGet(AtSurEngine self)
    {
    uint32 profileId = AtSurEngineSesThresholdPoolEntryGet(self);
    return AtModuleSurSesThresholdProfileGet(AtSurEngineModuleGet(self), profileId);
    }

static uint32 TcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    AtUnused(self);
    AtUnused(paramType);
    return 0;
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, WillDelete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void MethodsInit(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, PmEnable);
        mMethodOverride(m_methods, PmIsEnabled);
        mMethodOverride(m_methods, PmCanEnable);
        mMethodOverride(m_methods, ChannelGet);
        mMethodOverride(m_methods, CurrentFailuresGet);
        mMethodOverride(m_methods, FailureChangedHistoryGet);
        mMethodOverride(m_methods, FailureChangedHistoryClear);
        mMethodOverride(m_methods, ListenerAdd);
        mMethodOverride(m_methods, ListenerRemove);
        mMethodOverride(m_methods, AllCountersLatchAndClear);
        mMethodOverride(m_methods, RegistersLatchAndClear);
        mMethodOverride(m_methods, RegisterAllCountersGet);
        mMethodOverride(m_methods, RegisterAllCountersClear);
        mMethodOverride(m_methods, PmInvalidFlagsGet);

        /* Thresholds */
        mMethodOverride(m_methods, SesThresholdPoolEntryGet);
        mMethodOverride(m_methods, SesThresholdPoolEntrySet);
        mMethodOverride(m_methods, TcaThresholdPoolEntryGet);
        mMethodOverride(m_methods, TcaThresholdPoolEntrySet);
        mMethodOverride(m_methods, TcaPeriodThresholdValueGet);

        /* Notifications */
        mMethodOverride(m_methods, FailureNotificationMaskSet);
        mMethodOverride(m_methods, FailureNotificationMaskGet);
        mMethodOverride(m_methods, FailureNotificationProcess);
        
        mMethodOverride(m_methods, TcaNotificationMaskSet);
        mMethodOverride(m_methods, TcaNotificationMaskGet);
        mMethodOverride(m_methods, TcaNotificationProcess);
        
        /* Internal methods */
        mMethodOverride(m_methods, PmParamGet);
        mMethodOverride(m_methods, PmParamIsSupported);
        mMethodOverride(m_methods, PmParamIsDefined);
        mMethodOverride(m_methods, Provision);
        mMethodOverride(m_methods, PeriodExpire);
        mMethodOverride(m_methods, FailureNotificationEnable);
        mMethodOverride(m_methods, FailureNotificationIsEnabled);
        mMethodOverride(m_methods, TcaNotificationEnable);
        mMethodOverride(m_methods, TcaNotificationIsEnabled);
        mMethodOverride(m_methods, AutonomousFailuresGet);
        mMethodOverride(m_methods, NonAutonomousFailuresGet);
        
        /* Work with registers */
        mMethodOverride(m_methods, DefaultOffset);

        /* Threshold Profile */
        mMethodOverride(m_methods, ThresholdProfileSet);
        mMethodOverride(m_methods, ThresholdProfileGet);
        mMethodOverride(m_methods, SesThresholdProfileSet);
        mMethodOverride(m_methods, SesThresholdProfileGet);
        
        /* TCA Alarm */
        mMethodOverride(m_methods, TcaChangedHistoryGet);
        mMethodOverride(m_methods, TcaChangedHistoryClear);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEngine);
    }

AtSurEngine AtSurEngineObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    OverrideAtObject(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->module  = module;
    self->channel = channel;
    self->listeners = AtListCreate(0);

    return self;
    }

void AtSurEngineFailureNotify(AtSurEngine self, uint32 failureType, uint32 currentStatus)
    {
    uint32 i;

    if ((self == NULL) || (self->listeners == NULL))
        return;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tSurEngineEventListener *listener = (tSurEngineEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks && listener->callbacks->FailureNotify)
            listener->callbacks->FailureNotify(self, listener->userData, failureType, currentStatus);
        }
    }

void AtSurEngineTcaNotify(AtSurEngine self, AtPmParam param, AtPmRegister pmRegister)
    {
    uint32 i;

    if ((self == NULL) || (self->listeners == NULL))
        return;

    for (i = 0; i < AtListLengthGet(self->listeners); i++)
        {
        tSurEngineEventListener *listener = (tSurEngineEventListener *)AtListObjectGet(self->listeners, i);
        if (listener->callbacks && listener->callbacks->TcaNotify)
            listener->callbacks->TcaNotify(self, listener->userData, param, pmRegister);
        }
    }

void AtSurEngineAllParamsPeriodTcaNotify(AtSurEngine self, uint32 paramMask)
    {
    AtPmParam param;
    AtPmRegister pmRegister;
    uint32 param_i;

    for (param_i = cBit0; param_i != 0; param_i <<= 1)
        {
        if (param_i & paramMask)
            {
            param = AtSurEnginePmParamGet(self, param_i);
            pmRegister = AtPmParamCurrentPeriodRegister(param);
            AtSurEngineTcaNotify(self, param, pmRegister);
            }
        }
    }

eAtRet AtSurEngineProvision(AtSurEngine self, eBool provision)
    {
    return (self) ? mMethodsGet(self)->Provision(self, provision) : cAtErrorNullPointer;
    }

eBool AtSurEngineIsProvisioned(AtSurEngine self)
    {
    return (eBool)((self) ? self->isProvisioned : cAtFalse);
    }

eAtRet AtSurEnginePeriodExpire(AtSurEngine self, void *data)
    {
    if (self)
        return mMethodsGet(self)->PeriodExpire(self, data);
    return cAtErrorNullPointer;
    }

uint32 AtSurEngineDefaultOffset(AtSurEngine self)
    {
    if (self)
        return mMethodsGet(self)->DefaultOffset(self);
    return 0;
    }

eAtRet AtSurEngineFailureNotificationEnable(AtSurEngine self, eBool enable)
    {
    mNumericalAttributeSet(FailureNotificationEnable, enable);
    }

eBool AtSurEngineFailureNotificationIsEnabled(AtSurEngine self)
    {
    mAttributeGet(FailureNotificationIsEnabled, eBool, cAtFalse);
    }

void AtSurEngineNotificationProcess(AtSurEngine self, uint32 offset)
    {
    if (self)
        mMethodsGet(self)->FailureNotificationProcess(self, offset);
    }

eAtRet AtSurEngineTcaNotificationEnable(AtSurEngine self, eBool enable)
    {
    mNumericalAttributeSet(TcaNotificationEnable, enable);
    }

eBool AtSurEngineTcaNotificationIsEnabled(AtSurEngine self)
    {
    mAttributeGet(TcaNotificationIsEnabled, eBool, cAtFalse);
    }

void AtSurEngineTcaNotificationProcess(AtSurEngine self, uint32 offset)
    {
    if (self)
        mMethodsGet(self)->TcaNotificationProcess(self, offset);
    }

eAtRet AtSurEngineAllCountersLatch(AtSurEngine self)
    {
    return AtSurEngineAllCountersLatchAndClear(self, cAtFalse);
    }

eAtRet AtSurEngineAllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    mDoNothingOnStandby(cAtOk);
    mNumericalAttributeSet(AllCountersLatchAndClear, clear);
    }

eAtRet AtSurEngineRegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    mDoNothingOnStandby(cAtOk);
    mTwoParamAttributeGet(RegistersLatchAndClear, type, clear, eAtRet, cAtErrorNullPointer);
    }

eAtRet AtSurEngineSesThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    if (self)
        return mMethodsGet(self)->SesThresholdPoolEntrySet(self, entryIndex);
    return cAtErrorNullPointer;
    }

uint32 AtSurEngineSesThresholdPoolEntryGet(AtSurEngine self)
    {
    if (self)
        return mMethodsGet(self)->SesThresholdPoolEntryGet(self);
    return 0;
    }

eAtRet AtSurEngineTcaThresholdPoolEntrySet(AtSurEngine self, uint32 entryIndex)
    {
    if (self)
        return mMethodsGet(self)->TcaThresholdPoolEntrySet(self, entryIndex);
    return cAtErrorNullPointer;
    }

uint32 AtSurEngineTcaThresholdPoolEntryGet(AtSurEngine self)
    {
    if (self)
        return mMethodsGet(self)->TcaThresholdPoolEntryGet(self);
    return cBit31_0;
    }

uint32 AtSurEngineTcaPeriodThresholdValueGet(AtSurEngine self, uint32 paramType)
    {
    if (self)
        return mMethodsGet(self)->TcaPeriodThresholdValueGet(self, paramType);
    return 0;
    }

uint32 AtSurEngineAutonomousFailuresGet(AtSurEngine self)
    {
    if (self)
        return mMethodsGet(self)->AutonomousFailuresGet(self);
    return 0;
    }

uint32 AtSurEngineNonAutonomousFailuresGet(AtSurEngine self)
    {
    if (self)
        return mMethodsGet(self)->NonAutonomousFailuresGet(self);
    return 0;
    }

eBool AtSurEngineInAccessible(AtSurEngine self)
    {
    if (self)
        return AtModuleInAccessible((AtModule)AtSurEngineModuleGet(self));
    return cAtFalse;
    }

eBool AtSurEnginePmCanEnable(AtSurEngine self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PmCanEnable(self, enable);
    return cAtFalse;
    }

eBool AtSurEnginePmParamIsDefined(AtSurEngine self, uint32 paramType)
    {
    if (self)
        return mMethodsGet(self)->PmParamIsDefined(self, paramType);
    return cAtFalse;
    }

eBool AtSurEngineTcaIsSupported(AtSurEngine self)
    {
    return AtModuleSurTcaIsSupported(AtSurEngineModuleGet(self));
    }

/**
 * @addtogroup AtSurEngine
 * @{
 */

/**
 * Initialize surveillance engine
 *
 * @param self This engine
 *
 * @return AT return code
 */
eAtRet AtSurEngineInit(AtSurEngine self)
    {
    mNoParamCall(Init, eAtRet, cAtErrorNullPointer);
    }

/**
 * Get the channel that this engine is monitoring
 *
 * @param self This engine
 * @return Channel that is being monitored by this engine
 */
AtChannel AtSurEngineChannelGet(AtSurEngine self)
    {
    mNoParamObjectGet(ChannelGet, AtChannel);
    }

/**
 * Get module that this engine belongs to
 *
 * @param self This engine
 *
 * @return Module that it belongs to.
 */
AtModuleSur AtSurEngineModuleGet(AtSurEngine self)
    {
    return self ? self->module : NULL;
    }

/**
 * Enable or Disable a surveillance engine
 *
 * @param self This engine
 * @param enable Enable or Disable
 *
 * @return AT return code
 */
eAtRet AtSurEngineEnable(AtSurEngine self, eBool enable)
    {
    mNumericalAttributeSet(Enable, enable);
    }

/**
 * Get surveillance engine enable status
 *
 * @param self This engine
 *
 * @return cAtTrue : enable
 *         cAtFalse: disable
 */
eBool AtSurEngineIsEnabled(AtSurEngine self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * Enable or Disable PM monitoring of an Surveillance engine
 *
 * @param self This engine
 * @param enable Enable or Disable
 *
 * @return AT return code
 */
eAtRet AtSurEnginePmEnable(AtSurEngine self, eBool enable)
    {
    mNumericalAttributeSet(PmEnable, enable);
    }

/**
 * Get the enable status of PM monitoring of an Surveillance engine
 *
 * @param self This engine
 *
 * @return cAtTrue : enable
 *         cAtFalse: disable
 */
eBool AtSurEnginePmIsEnabled(AtSurEngine self)
    {
    mAttributeGet(PmIsEnabled, eBool, cAtFalse);
    }

/**
 * Get current failures of a surveillance engine
 *
 * @param self This engine
 *
 * @return current failures
 */
uint32 AtSurEngineCurrentFailuresGet(AtSurEngine self)
    {
    mAttributeGet(CurrentFailuresGet, uint32, 0);
    }

/**
 * Get failure history of a surveillance engine
 *
 * @param self This engine
 *
 * @return failure history
 */
uint32 AtSurEngineFailureChangedHistoryGet(AtSurEngine self)
    {
    mAttributeGet(FailureChangedHistoryGet, uint32, 0);
    }

/**
 * Clear failure history of a surveillance engine
 *
 * @param self This engine
 *
 * @return failure history mask
 */
uint32 AtSurEngineFailureChangedHistoryClear(AtSurEngine self)
    {
    mAttributeGet(FailureChangedHistoryClear, uint32, 0);
    }

/**
 * Get TCA change history of a surveillance engine
 *
 * @param self This engine
 *
 * @return TCA change history
 */
uint32 AtSurEngineTcaChangedHistoryGet(AtSurEngine self)
    {
    mAttributeGet(TcaChangedHistoryGet, uint32, 0);
    }

/**
 * Clear TCA change history of a surveillance engine
 *
 * @param self This engine
 *
 * @return TCA change history
 */
uint32 AtSurEngineTcaChangedHistoryClear(AtSurEngine self)
    {
    mAttributeGet(TcaChangedHistoryClear, uint32, 0);
    }

/**
 * Add listener to a surveillance engine
 *
 * @param self This engine
 * @param appContext Application context. When a callback is called, this context
 *                   will be input.
 * @param callback callback function
 *
 * @return AT return code
 */
eAtRet AtSurEngineListenerAdd(AtSurEngine self, void *appContext, const tAtSurEngineListener *callback)
    {
    if (self)
        return mMethodsGet(self)->ListenerAdd(self, appContext, callback);
    return cAtErrorNullPointer;
    }

/**
 * Remove listener from a surveillance engine
 *
 * @param self This engine
 * @param appContext Application context. See AtSurEngineListenerAdd().
 * @param callback callback function
 *
 * @return AT return code
 */
eAtRet AtSurEngineListenerRemove(AtSurEngine self, void *appContext, const tAtSurEngineListener *callback)
    {
    if (self)
        return mMethodsGet(self)->ListenerRemove(self, appContext, callback);
    return cAtErrorNullPointer;
    }

/**
 * Enable/disable sending failure notification.
 *
 * @param self This engine
 * @param failureMask Mask of failures to apply
 * @param enableMask Each bit is corresponding to one failure type.
 *                   - 0: disable
 *                   - 1: enable
 *
 * @return AT return code.
 */
eAtRet AtSurEngineFailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask)
    {
    if (failureMask == 0)
        return cAtOk;

    mTwoParamsAttributeSet(FailureNotificationMaskSet, failureMask, enableMask);
    }

/**
 * Check if failure notification is enabled or not
 *
 * @param self This engine
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
uint32 AtSurEngineFailureNotificationMaskGet(AtSurEngine self)
    {
    mAttributeGet(FailureNotificationMaskGet, uint32, 0);
    }

/**
 * Enable/disable TCA notification.
 *
 * @param self This engine
 * @param paramMask Mask of parameters to enable/disable TCA notification.
 * @param enableMask Each bit is corresponding to one parameter type
 *                   - 0: disable TCA notification
 *                   - 1: enable TCA notification
 *
 * @return AT return code.
 */
eAtRet AtSurEngineTcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask)
    {
    mTwoParamsAttributeSet(TcaNotificationMaskSet, paramMask, enableMask);
    }

/**
 * Check if TCA notification is enabled or not
 *
 * @param self This engine
 *
 * @retval Mask of parameters that have TCA notification enabled
 */
uint32 AtSurEngineTcaNotificationMaskGet(AtSurEngine self)
    {
    mAttributeGet(TcaNotificationMaskGet, uint32, 0);
    }

/**
 * Get PM parameter from a surveillance engine
 *
 * @param self This engine
 * @param paramType Refer:
 *         - @ref eAtSurEngineSdhLinePmParam "SDH line PM parameter types"
 *         - @ref eAtSurEngineSdhPathPmParam "SDH path PM parameter types"
 *         - @ref eAtSurEnginePdhDe1PmParam "PDH DE1 PM parameter types"
 *         - @ref eAtSurEnginePdhDe3PmParam "PDH DE3 PM parameter types"
 *         - @ref eAtSurEnginePwPmParam "PW PM parameter types"
 *
 * @return PM parameter
 */
AtPmParam AtSurEnginePmParamGet(AtSurEngine self, uint32 paramType)
    {
    mOneParamObjectGet(PmParamGet, AtPmParam, paramType);
    }

/**
 * Check if an PM parameter is supported or not
 *
 * @param self This engine
 * @param paramType Refer:
 *         - @ref eAtSurEngineSdhLinePmParam "SDH line PM parameter types"
 *         - @ref eAtSurEngineSdhPathPmParam "SDH path PM parameter types"
 *         - @ref eAtSurEnginePdhDe1PmParam "PDH DE1 PM parameter types"
 *         - @ref eAtSurEnginePdhDe3PmParam "PDH DE3 PM parameter types"
 *         - @ref eAtSurEnginePwPmParam "PW PM parameter types"
 *
 * @retval cAtTrue if the parameter is supported.
 * @retval cAtFalse if the parameter is not supported.
 */
eBool AtSurEnginePmParamIsSupported(AtSurEngine self, uint32 paramType)
    {
    mOneParamAttributeGet(PmParamIsSupported, paramType, eBool, cAtFalse);
    }

/**
 * Read-only PM parameters of a dedicated register type from a surveillance engine
 *
 * @param self This engine
 * @param type Register type @ref eAtPmRegisterType "PM Register Type"
 * @param counters Refer:
 *         - @ref tAtSurEngineSdhLinePmCounters "SDH line PM parameter counters"
 *         - @ref tAtSurEngineSdhPathPmCounters "SDH path PM parameter counters"
 *         - @ref tAtSurEnginePdhDe1PmCounters "PDH DE1 PM parameter counters"
 *         - @ref tAtSurEnginePdhDe3PmCounters "PDH DE3 PM parameter counters"
 *         - @ref tAtSurEnginePwPmCounters "PW PM parameter counters"
 *
 * @return AT return code
 */
eAtRet AtSurEngineAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    if ((self == NULL) || (counters == NULL))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    return mMethodsGet(self)->RegisterAllCountersGet(self, type, counters);
    }

/**
 * Read and clear PM parameters of a dedicated register type from a surveillance engine
 *
 * @param self This engine
 * @param type Register type @ref eAtPmRegisterType "PM Register Type"
 * @param counters Refer:
 *         - @ref tAtSurEngineSdhLinePmCounters "SDH line PM parameter counters"
 *         - @ref tAtSurEngineSdhPathPmCounters "SDH path PM parameter counters"
 *         - @ref tAtSurEnginePdhDe1PmCounters "PDH DE1 PM parameter counters"
 *         - @ref tAtSurEnginePdhDe3PmCounters "PDH DE3 PM parameter counters"
 *         - @ref tAtSurEnginePwPmCounters "PW PM parameter counters"
 *
 * @return AT return code
 */
eAtRet AtSurEngineAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    if ((self == NULL) || (counters == NULL))
        {
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    return mMethodsGet(self)->RegisterAllCountersClear(self, type, counters);
    }

/**
 * Read invalid flag of PM parameters of a dedicated register type from a surveillance engine
 *
 * @param self This engine
 * @param type Register type @ref eAtPmRegisterType "PM Register Type"
 *
 * @return Flags value in which each bit is corresponding to one parameter type
 *         - 0: valid PM parameter
 *         - 1: invalid PM parameter
 */
uint32 AtSurEnginePmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type)
    {
    mOneParamAttributeGet(PmInvalidFlagsGet, type, uint32, 0);
    }

/**
 * Set PM TCA Threshold Profile
 *
 * @param self This engine
 * @param profile PM TCA threshold profile
 *
 * @return AT return code
 */
eAtRet AtSurEngineThresholdProfileSet(AtSurEngine self, AtPmThresholdProfile profile)
    {
    mObjectSet(ThresholdProfileSet, profile);
    }

/**
 * Get PM TCA Threshold Profile
 *
 * @param self This engine
 *
 * @return PM TCA Threshold Profile
 */
AtPmThresholdProfile AtSurEngineThresholdProfileGet(AtSurEngine self)
    {
    mAttributeGet(ThresholdProfileGet, AtPmThresholdProfile, NULL);
    }

/**
 * Set PM SES Threshold Profile
 *
 * @param self This engine
 * @param profile PM SES threshold profile
 *
 * @return AT return code
 */
eAtRet AtSurEngineSesThresholdProfileSet(AtSurEngine self, AtPmSesThresholdProfile profile)
    {
    mObjectSet(SesThresholdProfileSet, profile);
    }

/**
 * Get PM SES Threshold Profile
 *
 * @param self This engine
 *
 * @return PM SES Threshold Profile
 */
AtPmSesThresholdProfile AtSurEngineSesThresholdProfileGet(AtSurEngine self)
    {
    mAttributeGet(SesThresholdProfileGet, AtPmSesThresholdProfile, NULL);
    }

/**
 * @}
 */

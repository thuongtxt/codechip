/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtSurEngineSdhPath.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : SDH Path Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtSdhChannel.h"
#include "AtSurEngineInternal.h"
#include "AtModuleSurInternal.h"
#include "AtPmRegisterInternal.h"
#include "AtPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtSurEngineSdhPath *)self)

/*--------------------------- Macros -----------------------------------------*/
#define mParamType(field)                                                      \
    if (engine->field == NULL)                                                 \
        engine->field = AtModuleSurSdhPathParamCreate(AtSurEngineModuleGet(self), self, #field, paramType); \
    return engine->field;                                                      \

#define mPmCounterGet(regtype, field, paramType)                               \
    {                                                                          \
    if (AtSurEnginePmParamIsSupported(self, paramType))                        \
        counters->field = AtPmRegisterValue(AtPmParamRegisterGet(engine->field, regtype));\
    else                                                                       \
        counters->field = 0;                                                   \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtSurEngineMethods m_AtSurEngineOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtSurEngineMethods *m_AtSurEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPmParam PmParamGet(AtSurEngine self, uint32 paramType)
    {
    AtSurEngineSdhPath engine = (AtSurEngineSdhPath)self;

    switch (paramType)
        {
        case cAtSurEngineSdhPathPmParamPpjcPdet:
            mParamType(ppjcPdet)
        case cAtSurEngineSdhPathPmParamNpjcPdet:
            mParamType(npjcPdet)
        case cAtSurEngineSdhPathPmParamPpjcPgen:
            mParamType(ppjcPgen)
        case cAtSurEngineSdhPathPmParamNpjcPgen:
            mParamType(npjcPgen)
        case cAtSurEngineSdhPathPmParamPjcDiff:
            mParamType(pjcDiff)
        case cAtSurEngineSdhPathPmParamPjcsPdet:
            mParamType(pjcsPdet)
        case cAtSurEngineSdhPathPmParamPjcsPgen:
            mParamType(pjcsPgen)
        case cAtSurEngineSdhPathPmParamCvP:
            mParamType(cvP)
        case cAtSurEngineSdhPathPmParamEsP:
            mParamType(esP)
        case cAtSurEngineSdhPathPmParamSesP:
            mParamType(sesP)
        case cAtSurEngineSdhPathPmParamUasP:
            mParamType(uasP)
        case cAtSurEngineSdhPathPmParamFcP:
            mParamType(fcP)
        case cAtSurEngineSdhPathPmParamCvPfe:
            mParamType(cvPfe)
        case cAtSurEngineSdhPathPmParamEsPfe:
            mParamType(esPfe)
        case cAtSurEngineSdhPathPmParamSesPfe:
            mParamType(sesPfe)
        case cAtSurEngineSdhPathPmParamUasPfe:
            mParamType(uasPfe)
        case cAtSurEngineSdhPathPmParamFcPfe:
            mParamType(fcPfe)
        case cAtSurEngineSdhPathPmParamEfsP:
            mParamType(efsP)
        case cAtSurEngineSdhPathPmParamEfsPfe:
            mParamType(efsPfe)
        case cAtSurEngineSdhPathPmParamAsP:
            mParamType(asP)
        case cAtSurEngineSdhPathPmParamAsPfe:
            mParamType(asPfe)
        case cAtSurEngineSdhPathPmParamEbP:
            mParamType(ebP)
        case cAtSurEngineSdhPathPmParamBbeP:
            mParamType(bbeP)
        case cAtSurEngineSdhPathPmParamEsrP:
            mParamType(esrP)
        case cAtSurEngineSdhPathPmParamSesrP:
            mParamType(sesrP)
        case cAtSurEngineSdhPathPmParamBberP:
            mParamType(bberP)
        default:
            return NULL;
        }
    }

static eAtRet CountersReadOnClear(AtSurEngine self, eAtPmRegisterType type, tAtSurEngineSdhPathPmCounters *counters, eBool readOnClear)
    {
    AtSurEngineSdhPath engine = (AtSurEngineSdhPath)self;
    eAtRet ret = AtSurEngineRegistersLatchAndClear(self, type, readOnClear);
    if (ret != cAtOk)
        return ret;

    /* Let the active driver fill each field, only clear memory on standby as it
     * is not able to access hardware */
    if (AtSurEngineInAccessible(self))
        {
        AtOsalMemInit(counters, 0, sizeof(tAtSurEngineSdhPathPmCounters));
        return cAtOk;
        }

    mPmCounterGet(type, ppjcPdet, cAtSurEngineSdhPathPmParamPpjcPdet);
    mPmCounterGet(type, ppjcPgen, cAtSurEngineSdhPathPmParamPpjcPgen);
    mPmCounterGet(type, npjcPdet, cAtSurEngineSdhPathPmParamNpjcPdet);
    mPmCounterGet(type, npjcPgen, cAtSurEngineSdhPathPmParamNpjcPgen);
    mPmCounterGet(type, pjcDiff,  cAtSurEngineSdhPathPmParamPjcDiff);
    mPmCounterGet(type, pjcsPdet, cAtSurEngineSdhPathPmParamPjcsPdet);
    mPmCounterGet(type, pjcsPgen, cAtSurEngineSdhPathPmParamPjcsPgen);
    mPmCounterGet(type, cvP, cAtSurEngineSdhPathPmParamCvP);
    mPmCounterGet(type, esP, cAtSurEngineSdhPathPmParamEsP);
    mPmCounterGet(type, sesP, cAtSurEngineSdhPathPmParamSesP);
    mPmCounterGet(type, uasP, cAtSurEngineSdhPathPmParamUasP);
    mPmCounterGet(type, fcP, cAtSurEngineSdhPathPmParamFcP);
    mPmCounterGet(type, cvPfe, cAtSurEngineSdhPathPmParamCvPfe);
    mPmCounterGet(type, esPfe, cAtSurEngineSdhPathPmParamEsPfe);
    mPmCounterGet(type, sesPfe, cAtSurEngineSdhPathPmParamSesPfe);
    mPmCounterGet(type, uasPfe, cAtSurEngineSdhPathPmParamUasPfe);
    mPmCounterGet(type, fcPfe, cAtSurEngineSdhPathPmParamFcPfe);
    mPmCounterGet(type, efsP, cAtSurEngineSdhPathPmParamEfsP);
    mPmCounterGet(type, efsPfe, cAtSurEngineSdhPathPmParamEfsPfe);
    mPmCounterGet(type, asP, cAtSurEngineSdhPathPmParamAsP);
    mPmCounterGet(type, asPfe, cAtSurEngineSdhPathPmParamAsPfe);
    mPmCounterGet(type, ebP, cAtSurEngineSdhPathPmParamEbP);
    mPmCounterGet(type, bbeP, cAtSurEngineSdhPathPmParamBbeP);
    mPmCounterGet(type, esrP, cAtSurEngineSdhPathPmParamEsrP);
    mPmCounterGet(type, sesrP, cAtSurEngineSdhPathPmParamSesrP);
    mPmCounterGet(type, bberP, cAtSurEngineSdhPathPmParamBberP);

    return cAtOk;
    }

static eAtRet RegisterAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEngineSdhPathPmCounters*)counters, cAtFalse);
    }

static eAtRet RegisterAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEngineSdhPathPmCounters*)counters, cAtTrue);
    }

static void DeletePmParam(AtPmParam *param)
    {
    AtObjectDelete((AtObject)*param);
    *param = NULL;
    }

static void Delete(AtObject self)
    {
    DeletePmParam(&mThis(self)->ppjcPdet);
    DeletePmParam(&mThis(self)->npjcPdet);
    DeletePmParam(&mThis(self)->ppjcPgen);
    DeletePmParam(&mThis(self)->npjcPgen);
    DeletePmParam(&mThis(self)->pjcDiff);
    DeletePmParam(&mThis(self)->pjcsPdet);
    DeletePmParam(&mThis(self)->pjcsPgen);
    DeletePmParam(&mThis(self)->cvP);
    DeletePmParam(&mThis(self)->esP);
    DeletePmParam(&mThis(self)->sesP);
    DeletePmParam(&mThis(self)->uasP);
    DeletePmParam(&mThis(self)->fcP);
    DeletePmParam(&mThis(self)->cvPfe);
    DeletePmParam(&mThis(self)->esPfe);
    DeletePmParam(&mThis(self)->sesPfe);
    DeletePmParam(&mThis(self)->uasPfe);
    DeletePmParam(&mThis(self)->fcPfe);
    DeletePmParam(&mThis(self)->efsP);
    DeletePmParam(&mThis(self)->efsPfe);

    DeletePmParam(&mThis(self)->asP);
    DeletePmParam(&mThis(self)->asPfe);
    DeletePmParam(&mThis(self)->ebP);
    DeletePmParam(&mThis(self)->bbeP);
    DeletePmParam(&mThis(self)->esrP);
    DeletePmParam(&mThis(self)->sesrP);
    DeletePmParam(&mThis(self)->bberP);
    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void UpdateSwValue_EfsP_EfsPfe_AsP_AsPfe(AtSurEngine self, AtPmRegister reg, eAtSurEngineSdhPathPmParam type)
    {
    int32 es = 0, value = 0;
    es = AtPmRegisterValue(AtPmParamPreviousPeriodRegister(AtSurEnginePmParamGet(self, type)));
    value = 900 - es;
    AtPmRegisterValueSet(reg, value);
    }

static void UpdateSwValue_EsrP_SesrP(AtSurEngine self, AtPmRegister reg, eAtSurEngineSdhPathPmParam type)
    {
    int32 es_ses=0, uas=0, value=0;
    es_ses = AtPmRegisterValue(AtPmParamPreviousPeriodRegister(AtSurEnginePmParamGet(self, type)));
    uas = AtPmRegisterValue(AtPmParamPreviousPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamUasP)));
    if (uas <900)
        value = es_ses/(900 - uas);
    else
        value = 0;
    AtPmRegisterValueSet(reg, value);
    }

static int32 BlockPerSecond(AtSurEngine self)
    {
    int32 ret = 0;
    uint32 channelType = AtSdhChannelTypeGet((AtSdhChannel)AtSurEngineChannelGet(self));
    switch (channelType)
        {
        case cAtSdhChannelTypeAu3:
        case cAtSdhChannelTypeAu4:
            ret = (2400 *10)/3;
            break;
        case cAtSdhChannelTypeTu12:
        case cAtSdhChannelTypeTu11:
            ret = (600 *10)/3;
            break;
        default:
            break;
        }
    return ret;
    }

static void UpdateSwValue_BberP(AtSurEngine self, AtPmRegister reg)
    {
    int32 uas=0, bbe=0,cSES=0, value=0;
    int32 blocks_per_second=0;
    bbe = AtPmRegisterValue(AtPmParamPreviousPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamBbeP)));
    uas = AtPmRegisterValue(AtPmParamPreviousPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamUasP)));
    cSES = AtPmRegisterValue(AtPmParamPreviousPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamSesP)));
    blocks_per_second = BlockPerSecond(self);
    if ((uas +cSES) < 900)
        value = bbe/((900 - uas -cSES)*blocks_per_second);
    else
        value = 0;
    AtPmRegisterValueSet(reg, value);
    }

static void LatchSwValue_EfsP_EfsPfe_AsP_AsPfe(AtSurEngine self, AtPmRegister reg, eAtSurEngineSdhPathPmParam type)
    {
    int32 es=0, value=0;
    es = AtPmRegisterValue(AtPmParamCurrentPeriodRegister(AtSurEnginePmParamGet(self, type)));
    value = self->differenceTime - es;
    AtPmRegisterValueSet(reg, value);
    }

static void LatchSwValue_EsrP_SesrP(AtSurEngine self, AtPmRegister reg, eAtSurEngineSdhPathPmParam type)
    {
    int32 es_ses=0, uas=0, value=0;
    es_ses = AtPmRegisterValue(AtPmParamCurrentPeriodRegister(AtSurEnginePmParamGet(self, type)));
    uas = AtPmRegisterValue(AtPmParamCurrentPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamUasP)));
    value = es_ses/(self->differenceTime - uas);
    AtPmRegisterValueSet(reg, value);
    }

static void LatchSwValue_BberP(AtSurEngine self, AtPmRegister reg)
    {
    int32 uas=0, bbe=0,cSES=0, value=0;
    int32 blocks_per_second=0;
    bbe = AtPmRegisterValue(AtPmParamCurrentPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamBbeP)));
    uas = AtPmRegisterValue(AtPmParamCurrentPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamUasP)));
    cSES = AtPmRegisterValue(AtPmParamCurrentPeriodRegister(AtSurEnginePmParamGet(self, cAtSurEngineSdhPathPmParamSesP)));
    blocks_per_second = BlockPerSecond(self);
    value = bbe/(self->differenceTime - uas -cSES)*blocks_per_second;
    AtPmRegisterValueSet(reg, value);
    }

static void LatchSwValue(AtSurEngine self,eAtSurEngineSdhPathPmParam type)
    {
    AtPmParam    param = AtSurEnginePmParamGet(self, type);
    AtPmRegister reg   = AtPmParamCurrentPeriodRegister(param);
    switch ((uint32)type)
        {
        case cAtSurEngineSdhPathPmParamEfsP:
            LatchSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamEsP);
            break;
        case cAtSurEngineSdhPathPmParamEfsPfe:
            LatchSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamEsPfe);
            break;
        case cAtSurEngineSdhPathPmParamAsP:
            LatchSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamUasP);
            break;
        case cAtSurEngineSdhPathPmParamAsPfe:
            LatchSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamUasPfe);
            break;
        case cAtSurEngineSdhPathPmParamEsrP:
            LatchSwValue_EsrP_SesrP(self,reg, cAtSurEngineSdhPathPmParamEsP);
            break;
        case cAtSurEngineSdhPathPmParamSesrP:
            LatchSwValue_EsrP_SesrP(self, reg,cAtSurEngineSdhPathPmParamSesP);
            break;
        case cAtSurEngineSdhPathPmParamBberP:
            LatchSwValue_BberP(self, reg);
            break;
        default:
            break;
        }
    }

static void UpdateSwValue(AtSurEngine self, eAtSurEngineSdhPathPmParam type)
    {
    AtPmParam param = AtSurEnginePmParamGet(self, type);
    AtPmRegister reg = AtPmParamPreviousPeriodRegister(param);

    AtPmParamRecentPeriodShiftLeft(param);

    switch ((uint32)type)
        {
        case cAtSurEngineSdhPathPmParamEfsP:
            UpdateSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamEsP);
            break;
        case cAtSurEngineSdhPathPmParamEfsPfe:
            UpdateSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamEsPfe);
            break;
        case cAtSurEngineSdhPathPmParamAsP:
            UpdateSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamUasP);
            break;
        case cAtSurEngineSdhPathPmParamAsPfe:
            UpdateSwValue_EfsP_EfsPfe_AsP_AsPfe(self,reg, cAtSurEngineSdhPathPmParamUasPfe);
            break;
        case cAtSurEngineSdhPathPmParamEsrP:
            UpdateSwValue_EsrP_SesrP(self,reg, cAtSurEngineSdhPathPmParamEsP);
            break;
        case cAtSurEngineSdhPathPmParamSesrP:
            UpdateSwValue_EsrP_SesrP(self, reg,cAtSurEngineSdhPathPmParamSesP);
            break;
        case cAtSurEngineSdhPathPmParamBberP:
            UpdateSwValue_BberP(self, reg);
            break;
        default:
            break;
        }
    }

static eAtRet PeriodExpire(AtSurEngine self, void *data)
    {
    eAtRet ret = m_AtSurEngineMethods->PeriodExpire(self, data);

    if (ret != cAtOk)
        return ret;

    /* Just can update software parameters */
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamEfsP);
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamEfsPfe);
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamAsP);
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamAsPfe);

    /* Not applicable for VT/TU ??? */
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamEbP);
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamBbeP);
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamEsrP);
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamSesrP);
    UpdateSwValue(self, cAtSurEngineSdhPathPmParamBberP);

    return cAtOk;
    }

static eAtRet AllCountersLatchAndClear(AtSurEngine self, eBool clear)
    {
    eAtRet ret = m_AtSurEngineMethods->AllCountersLatchAndClear(self, clear);
    if (ret != cAtOk)
        return ret;

    LatchSwValue(self, cAtSurEngineSdhPathPmParamEfsP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamEfsPfe);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamAsP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamAsPfe);

    /* Are they not applicable for VT/TU ? */
    LatchSwValue(self, cAtSurEngineSdhPathPmParamEbP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamBbeP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamEsrP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamSesrP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamBberP);

    return cAtOk;
    }

static eAtRet RegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear)
    {
    eAtRet ret = m_AtSurEngineMethods->RegistersLatchAndClear(self, type, clear);
    if (ret != cAtOk)
        return ret;

    LatchSwValue(self, cAtSurEngineSdhPathPmParamEfsP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamEfsPfe);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamAsP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamAsPfe);

    /* Are they not applicable for VT/TU ? */
    LatchSwValue(self, cAtSurEngineSdhPathPmParamEbP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamBbeP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamEsrP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamSesrP);
    LatchSwValue(self, cAtSurEngineSdhPathPmParamBberP);

    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSurEngineSdhPath object = (AtSurEngineSdhPath)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(ppjcPdet);
    mEncodeObject(npjcPdet);
    mEncodeObject(ppjcPgen);
    mEncodeObject(npjcPgen);
    mEncodeObject(pjcDiff);
    mEncodeObject(pjcsPdet);
    mEncodeObject(pjcsPgen);
    mEncodeObject(cvP);
    mEncodeObject(esP);
    mEncodeObject(sesP);
    mEncodeObject(uasP);
    mEncodeObject(fcP);
    mEncodeObject(cvPfe);
    mEncodeObject(esPfe);
    mEncodeObject(sesPfe);
    mEncodeObject(uasPfe);
    mEncodeObject(fcPfe);
    mEncodeObject(efsP);
    mEncodeObject(efsPfe);
    mEncodeObject(asP);
    mEncodeObject(asPfe);
    mEncodeObject(ebP);
    mEncodeObject(bbeP);
    mEncodeObject(esrP);
    mEncodeObject(sesrP);
    mEncodeObject(bberP);
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSurEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmParamGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersClear);
        mMethodOverride(m_AtSurEngineOverride, PeriodExpire);
        mMethodOverride(m_AtSurEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtSurEngineOverride, RegistersLatchAndClear);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtObject(self);
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEngineSdhPath);
    }

AtSurEngine AtSurEngineSdhPathObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

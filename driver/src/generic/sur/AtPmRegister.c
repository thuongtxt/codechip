/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Surveillance
 *
 * File        : AtPmRegister.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : PM register abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPmRegisterInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tAtPmRegister *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPmRegisterMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static int32 Reset(AtPmRegister self)
    {
    int32 value = self->value;
    self->value = 0;
    return value;
    }

static int32 Value(AtPmRegister self)
    {
    /* Let concrete class do */
    return self->value;
    }

static eBool IsValid(AtPmRegister self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtPmRegister self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Reset);
        mMethodOverride(m_methods, Value);
        mMethodOverride(m_methods, IsValid);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPmRegister);
    }

AtPmRegister AtPmRegisterObjectInit(AtPmRegister self, AtPmParam param)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    m_methodsInit = 1;
    self->param = param;

    return self;
    }

AtPmParam AtPmRegisterParamGet(AtPmRegister self)
    {
    return self ? self->param : NULL;
    }

void AtPmRegisterValueSet(AtPmRegister self, int32 value)
    {
    if (self)
        mThis(self)->value = value;
    }

/**
 * @addtogroup AtPmRegister
 * @{
 */

/**
 * Read then reset PM register
 *
 * @param self This PM register
 *
 * @return Register value before reseting.
 */
int32 AtPmRegisterReset(AtPmRegister self)
    {
    mAttributeGet(Reset, int32, 0);
    }

/**
 * Get PM register value
 *
 * @param self This PM register
 *
 * @return value
 */
int32 AtPmRegisterValue(AtPmRegister self)
    {
    mAttributeGet(Value, int32, 0);
    }

/**
 * Get valid status of PM register
 *
 * @param self This PM register
 *
 * @return cAtTrue:  valid
 *         cAtFalse: invalid
 */
eBool AtPmRegisterIsValid(AtPmRegister self)
    {
    mAttributeGet(IsValid, eBool, cAtFalse);
    }

/**
 * Get PM register type of PM register
 *
 * @param self This PM register
 *
 * @return PM register type @ref eAtPmRegisterType
 */
eAtPmRegisterType AtPmRegisterTypeGet(AtPmRegister self)
    {
    if (self)
        return self->type;
    return cAtPmUnknownRegister;
    }

/**
 * @}
 */

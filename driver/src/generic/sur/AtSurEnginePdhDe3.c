/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : AtSurEnginePdhDe3.c
 *
 * Created Date: Mar 17, 2015
 *
 * Description : DS3/E3 Surveillance engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPdhDe3.h"
#include "AtSurEngineInternal.h"
#include "AtModuleSurInternal.h"
#include "AtPmParamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tAtSurEnginePdhDe3 *)self)

/*--------------------------- Macros -----------------------------------------*/
#define mParamType(field)                                                      \
    {                                                                          \
    if (engine->field == NULL)                                                 \
        engine->field = AtModuleSurPdhDe3ParamCreate(AtSurEngineModuleGet(self), self, #field, paramType); \
    return engine->field;                                                      \
    }

#define mPmCounterGet(regtype, field)                                          \
    {                                                                          \
    counters->field = AtPmRegisterValue(AtPmParamRegisterGet(engine->field, regtype));\
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtSurEngineMethods m_AtSurEngineOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPmParam PmParamGet(AtSurEngine self, uint32 paramType)
    {
    AtSurEnginePdhDe3 engine = (AtSurEnginePdhDe3)self;

    switch (paramType)
        {
        case cAtSurEnginePdhDe3PmParamCvL:
            mParamType(cvL)
        case cAtSurEnginePdhDe3PmParamEsL:
            mParamType(esL)
        case cAtSurEnginePdhDe3PmParamSesL:
            mParamType(sesL)
        case cAtSurEnginePdhDe3PmParamLossL:
            mParamType(lossL)
        case cAtSurEnginePdhDe3PmParamCvpP:
            mParamType(cvpP)
        case cAtSurEnginePdhDe3PmParamCvcpP:
            mParamType(cvcpP)
        case cAtSurEnginePdhDe3PmParamEspP:
            mParamType(espP)
        case cAtSurEnginePdhDe3PmParamEscpP:
            mParamType(escpP)
        case cAtSurEnginePdhDe3PmParamSespP:
            mParamType(sespP)
        case cAtSurEnginePdhDe3PmParamSescpP:
            mParamType(sescpP)
        case cAtSurEnginePdhDe3PmParamSasP:
            mParamType(sasP)
        case cAtSurEnginePdhDe3PmParamAissP:
            mParamType(aissP)
        case cAtSurEnginePdhDe3PmParamUaspP:
            mParamType(uaspP)
        case cAtSurEnginePdhDe3PmParamUascpP:
            mParamType(uascpP)
        case cAtSurEnginePdhDe3PmParamCvcpPfe:
            mParamType(cvcpPfe)
        case cAtSurEnginePdhDe3PmParamEscpPfe:
            mParamType(escpPfe)
        case cAtSurEnginePdhDe3PmParamSescpPfe:
            mParamType(sescpPfe)
        case cAtSurEnginePdhDe3PmParamSascpPfe:
            mParamType(sascpPfe)
        case cAtSurEnginePdhDe3PmParamUascpPfe:
            mParamType(uascpPfe)
        case cAtSurEnginePdhDe3PmParamEsbcpPfe:
            mParamType(esbcpPfe)
        case cAtSurEnginePdhDe3PmParamEsaL:
            mParamType(esaL)
        case cAtSurEnginePdhDe3PmParamEsbL:
            mParamType(esbL)
        case cAtSurEnginePdhDe3PmParamEsapP:
            mParamType(esapP)
        case cAtSurEnginePdhDe3PmParamEsacpP:
            mParamType(esacpP)
        case cAtSurEnginePdhDe3PmParamEsbpP:
            mParamType(esbpP)
        case cAtSurEnginePdhDe3PmParamEsbcpP:
            mParamType(esbcpP)
        case cAtSurEnginePdhDe3PmParamFcP:
            mParamType(fcP)
        case cAtSurEnginePdhDe3PmParamEsacpPfe:
            mParamType(esacpPfe)
        case cAtSurEnginePdhDe3PmParamFccpPfe :
            mParamType(fccpPfe)
        default:
            return NULL;
        }
    }

static eAtRet CountersReadOnClear(AtSurEngine self, eAtPmRegisterType type, tAtSurEnginePdhDe3PmCounters *counters, eBool readOnClear)
    {
    AtSurEnginePdhDe3 engine = (AtSurEnginePdhDe3)self;
    eAtRet ret = AtSurEngineRegistersLatchAndClear(self, type, readOnClear);
    if (ret != cAtOk)
        return ret;

    /* Let the active driver fill each field, only clear memory on standby as it
     * is not able to access hardware */
    if (AtSurEngineInAccessible(self))
        {
        AtOsalMemInit(counters, 0, sizeof(tAtSurEnginePdhDe3PmCounters));
        return cAtOk;
        }

    mPmCounterGet(type, cvL);
    mPmCounterGet(type, esL);
    mPmCounterGet(type, sesL);
    mPmCounterGet(type, lossL);
    mPmCounterGet(type, cvpP);
    mPmCounterGet(type, cvcpP);
    mPmCounterGet(type, espP);
    mPmCounterGet(type, escpP);
    mPmCounterGet(type, sespP);
    mPmCounterGet(type, sescpP);
    mPmCounterGet(type, sasP);
    mPmCounterGet(type, aissP);
    mPmCounterGet(type, uaspP);
    mPmCounterGet(type, uascpP);
    mPmCounterGet(type, cvcpPfe);
    mPmCounterGet(type, escpPfe);
    mPmCounterGet(type, sescpPfe);
    mPmCounterGet(type, sascpPfe);
    mPmCounterGet(type, uascpPfe);
    mPmCounterGet(type, esbcpPfe);
    mPmCounterGet(type, esaL);
    mPmCounterGet(type, esbL);
    mPmCounterGet(type, esapP);
    mPmCounterGet(type, esacpP);
    mPmCounterGet(type, esbpP);
    mPmCounterGet(type, esbcpP);
    mPmCounterGet(type, fcP);
    mPmCounterGet(type, esacpPfe);
    mPmCounterGet(type, fccpPfe);

    return cAtOk;
    }

static eAtRet RegisterAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEnginePdhDe3PmCounters*)counters, cAtFalse);
    }

static eAtRet RegisterAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters)
    {
    return CountersReadOnClear(self, type, (tAtSurEnginePdhDe3PmCounters*)counters, cAtTrue);
    }

static eBool PmParamIsDefined(AtSurEngine self, uint32 paramType)
    {
    if (AtPdhDe3IsDs3CbitFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)AtSurEngineChannelGet(self))))
        return cAtTrue;

    switch (paramType)
        {
        case cAtSurEnginePdhDe3PmParamCvcpP:    return cAtFalse;
        case cAtSurEnginePdhDe3PmParamEscpP:    return cAtFalse;
        case cAtSurEnginePdhDe3PmParamSescpP:   return cAtFalse;
        case cAtSurEnginePdhDe3PmParamUascpP:   return cAtFalse;
        case cAtSurEnginePdhDe3PmParamEsacpP:   return cAtFalse;
        case cAtSurEnginePdhDe3PmParamEsbcpP:   return cAtFalse;
        case cAtSurEnginePdhDe3PmParamCvcpPfe:  return cAtFalse;
        case cAtSurEnginePdhDe3PmParamEscpPfe:  return cAtFalse;
        case cAtSurEnginePdhDe3PmParamSescpPfe: return cAtFalse;
        case cAtSurEnginePdhDe3PmParamUascpPfe: return cAtFalse;
        case cAtSurEnginePdhDe3PmParamSascpPfe: return cAtFalse;
        case cAtSurEnginePdhDe3PmParamEsacpPfe: return cAtFalse;
        case cAtSurEnginePdhDe3PmParamEsbcpPfe: return cAtFalse;
        case cAtSurEnginePdhDe3PmParamFccpPfe:  return cAtFalse;
        default: return cAtTrue;
        }
    }

static uint32 CurrentFailuresGet(AtSurEngine self)
    {
    static const uint32 cLineFailures = cAtPdhDe3AlarmLos;
    uint32 failures = AtSurEngineNonAutonomousFailuresGet(self);

    /* If AIS failure caused by Line maintenance signal, it should mask all of Path failures. */
    if (failures && AtModuleSurShouldMaskFailuresWhenNonAutonomousFailures(AtSurEngineModuleGet(self)))
        return (AtSurEngineAutonomousFailuresGet(self) & cLineFailures) | failures;

    failures |= AtSurEngineAutonomousFailuresGet(self);
    return failures;
    }

static void DeletePmParam(AtPmParam *param)
    {
    AtObjectDelete((AtObject)*param);
    *param = NULL;
    }

static void Delete(AtObject self)
    {
    DeletePmParam(&mThis(self)->cvL);
    DeletePmParam(&mThis(self)->esL);
    DeletePmParam(&mThis(self)->sesL);
    DeletePmParam(&mThis(self)->lossL);
    DeletePmParam(&mThis(self)->cvpP);
    DeletePmParam(&mThis(self)->cvcpP);
    DeletePmParam(&mThis(self)->espP);
    DeletePmParam(&mThis(self)->escpP);
    DeletePmParam(&mThis(self)->sespP);
    DeletePmParam(&mThis(self)->sescpP);
    DeletePmParam(&mThis(self)->sasP);
    DeletePmParam(&mThis(self)->aissP);
    DeletePmParam(&mThis(self)->uaspP);
    DeletePmParam(&mThis(self)->uascpP);
    DeletePmParam(&mThis(self)->cvcpPfe);
    DeletePmParam(&mThis(self)->escpPfe);
    DeletePmParam(&mThis(self)->esbcpPfe);
    DeletePmParam(&mThis(self)->sescpPfe);
    DeletePmParam(&mThis(self)->sascpPfe);
    DeletePmParam(&mThis(self)->uascpPfe);

    DeletePmParam(&mThis(self)->esaL);
    DeletePmParam(&mThis(self)->esbL);
    DeletePmParam(&mThis(self)->esapP);
    DeletePmParam(&mThis(self)->esacpP);
    DeletePmParam(&mThis(self)->esbpP);
    DeletePmParam(&mThis(self)->esbcpP);
    DeletePmParam(&mThis(self)->fcP);
    DeletePmParam(&mThis(self)->esacpPfe);
    DeletePmParam(&mThis(self)->fccpPfe);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSurEnginePdhDe3 object = (AtSurEnginePdhDe3)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(cvL);
    mEncodeObject(esL);
    mEncodeObject(sesL);
    mEncodeObject(lossL);
    mEncodeObject(cvpP);
    mEncodeObject(cvcpP);
    mEncodeObject(espP);
    mEncodeObject(escpP);
    mEncodeObject(sespP);
    mEncodeObject(sescpP);
    mEncodeObject(sasP);
    mEncodeObject(aissP);
    mEncodeObject(uaspP);
    mEncodeObject(uascpP);
    mEncodeObject(cvcpPfe);
    mEncodeObject(escpPfe);
    mEncodeObject(esbcpPfe);
    mEncodeObject(sescpPfe);
    mEncodeObject(sascpPfe);
    mEncodeObject(uascpPfe);
    mEncodeObject(esaL);
    mEncodeObject(esbL);
    mEncodeObject(esapP);
    mEncodeObject(esacpP);
    mEncodeObject(esbpP);
    mEncodeObject(esbcpP);
    mEncodeObject(fcP);
    mEncodeObject(esacpPfe);
    mEncodeObject(fccpPfe);
    }

static void OverrideAtObject(AtSurEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSurEngine(AtSurEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSurEngineOverride, mMethodsGet(self), sizeof(m_AtSurEngineOverride));

        mMethodOverride(m_AtSurEngineOverride, PmParamGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersGet);
        mMethodOverride(m_AtSurEngineOverride, RegisterAllCountersClear);
        mMethodOverride(m_AtSurEngineOverride, PmParamIsDefined);
        mMethodOverride(m_AtSurEngineOverride, CurrentFailuresGet);
        }

    mMethodsSet(self, &m_AtSurEngineOverride);
    }

static void Override(AtSurEngine self)
    {
    OverrideAtObject(self);
    OverrideAtSurEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtSurEnginePdhDe3);
    }

AtSurEngine AtSurEnginePdhDe3ObjectInit(AtSurEngine self, AtModuleSur module, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtSurEngineObjectInit(self, module, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint32 AtSurEngineDs3CbitParams(void)
    {
    return (cAtSurEnginePdhDe3PmParamCvcpP|
            cAtSurEnginePdhDe3PmParamEscpP|
            cAtSurEnginePdhDe3PmParamSescpP|
            cAtSurEnginePdhDe3PmParamUascpP|
            cAtSurEnginePdhDe3PmParamEsacpP|
            cAtSurEnginePdhDe3PmParamEsbcpP|
            cAtSurEnginePdhDe3PmParamCvcpPfe|
            cAtSurEnginePdhDe3PmParamEscpPfe|
            cAtSurEnginePdhDe3PmParamSescpPfe|
            cAtSurEnginePdhDe3PmParamUascpPfe|
            cAtSurEnginePdhDe3PmParamSascpPfe|
            cAtSurEnginePdhDe3PmParamEsacpPfe|
            cAtSurEnginePdhDe3PmParamEsbcpPfe|
            cAtSurEnginePdhDe3PmParamFccpPfe);
    }

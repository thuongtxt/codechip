/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsLinearSoftEngine.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : Soft Linear APS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSLINEARSOFTENGINE_H_
#define _ATAPSLINEARSOFTENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsEngine.h" /* Super class */
#include "aps/atlaps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsLinearSoftEngine * AtApsLinearSoftEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine AtApsLinearSoftEngineNew(AtModuleAps module, uint32 engineId, AtSdhLine lines[], uint8 numLine, AtLApsEngine lapsHandler);

#endif /* _ATAPSLINEARSOFTENGINE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsSelector.c
 *
 * Created Date: Aug 4, 2016
 *
 * Description : Generic 2-to-1 APS selector.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtApsSelector.h"
#include "AtApsSelectorInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtApsSelectorMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool ChannelExist(AtApsSelector self, AtChannel selectedChannel)
    {
    if ((selectedChannel == self->working) || (selectedChannel == self->protection))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet Init(AtApsSelector self)
    {
    return AtApsSelectorChannelSelect(self, AtApsSelectorWorkingChannelGet(self));
    }

static eAtRet Enable(AtApsSelector self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    /* Subclass will implement. */
    return cAtErrorNotImplemented;
    }

static eBool IsEnabled(AtApsSelector self)
    {
    AtUnused(self);
    /* Subclass will implement. */
    return cAtFalse;
    }

static eAtModuleApsRet ChannelSelect(AtApsSelector self, AtChannel selectedChannel)
    {
    AtUnused(self);
    AtUnused(selectedChannel);
    /* Sub class must know how to select channel */
    return cAtErrorNotImplemented;
    }

static AtChannel SelectedChannel(AtApsSelector self)
    {
    AtUnused(self);
    /* Sub class must know how to query hardware */
    return NULL;
    }

static eAtModuleApsRet GroupSet(AtApsSelector self, AtApsGroup group)
    {
    self->group = group;
    return cAtOk;
    }

static AtApsGroup GroupGet(AtApsSelector self)
    {
    return self->group;
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtApsSelector selector = (AtApsSelector)self;
    AtDevice dev = AtModuleDeviceGet((AtModule)AtApsSelectorModuleGet(selector));

    AtSnprintf(description, sizeof(description), "%sApsSelector.%d", AtDeviceIdToString(dev), AtApsSelectorIdGet((AtApsSelector)self) + 1);
    return description;
    }

static void MethodsInit(AtApsSelector self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, ChannelSelect);
        mMethodOverride(m_methods, SelectedChannel);
        mMethodOverride(m_methods, GroupSet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtApsSelector self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtApsSelector self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsSelector);
    }

AtApsSelector AtApsSelectorObjectInit(AtApsSelector self,
                                      AtModuleAps   module,
                                      uint32        selectorId,
                                      AtChannel     working,
                                      AtChannel     protection,
                                      AtChannel     outgoing)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    AtObjectInit((AtObject)self);

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->module      = module;
    self->selectorId  = selectorId;
    self->working     = working;
    self->protection  = protection;
    self->outgoing    = outgoing;
    self->group       = NULL;

    return self;
    }

eAtModuleApsRet AtApsSelectorGroupSet(AtApsSelector self, AtApsGroup group)
    {
    if (self)
        return mMethodsGet(self)->GroupSet(self, group);
    return cAtErrorNullPointer;
    }

AtApsGroup AtApsSelectorGroupGet(AtApsSelector self)
    {
    if (self)
        return GroupGet(self);
    return NULL;
    }

/**
 * @addtogroup AtApsSelector
 * @{
 */

/**
 * Get APS Module that manage this Selector
 *
 * @param self This APS selector
 *
 * @return Module APS Object
 */
AtModuleAps AtApsSelectorModuleGet(AtApsSelector self)
    {
    return self ? self->module : NULL;
    }

/**
 * Initialize APS Selector
 *
 * @param self This APS selector
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtApsSelectorInit(AtApsSelector self)
    {
    mNoParamCall(Init, eAtModuleApsRet, cAtErrorObjectNotExist);
    }

/**
 * Get APS Selector ID
 *
 * @param self This APS selector
 *
 * @return Engine ID
 */
uint32 AtApsSelectorIdGet(AtApsSelector self)
    {
    return (self) ? self->selectorId : cInvalidUint32;
    }

/**
 * Enable/Disable APS Selector
 *
 * @param self This APS selector
 * @param enabled Enabling
 *                - cAtTrue to enable
 *                - cAtFalse to disable
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtApsSelectorEnable(AtApsSelector self, eBool enabled)
    {
    mNumericalAttributeSet(Enable, enabled);
    }

/**
 * Get enabling status of APS Selector
 *
 * @param self This APS selector
 *
 * @return cAtFalse : Disable, cAtTrue : Enable
 */
eBool AtApsSelectorIsEnabled(AtApsSelector self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse)
    }

/**
 * Selecting the channel for out going channel
 *
 * @param self This APS selector
 * @param selectedChannel Channel to select
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtApsSelectorChannelSelect(AtApsSelector self, AtChannel selectedChannel)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ChannelExist(self, selectedChannel))
        {
        mObjectSet(ChannelSelect, selectedChannel);
        }

    return cAtErrorObjectNotExist;
    }

/**
 * Get selected channel of the APS selector
 *
 * @param self This APS selector
 *
 * @return Selected channel
 */
AtChannel AtApsSelectorSelectedChannel(AtApsSelector self)
    {
    mAttributeGet(SelectedChannel, AtChannel, NULL)
    }

/**
 * Get working channel of the APS selector
 *
 * @param self This APS selector
 *
 * @return Selected channel
 */
AtChannel AtApsSelectorWorkingChannelGet(AtApsSelector self)
    {
    return (self) ? self->working : NULL;
    }

/**
 * Get protection channel of the APS selector
 *
 * @param self This APS selector
 *
 * @return Selected channel
 */
AtChannel AtApsSelectorProtectionChannelGet(AtApsSelector self)
    {
    return (self) ? self->protection : NULL;
    }

/**
 * Get out going channel of the APS selector
 *
 * @param self This APS selector
 *
 * @return Selected channel
 */
AtChannel AtApsSelectorOutgoingChannelGet(AtApsSelector self)
    {
    return (self) ? self->outgoing : NULL;
    }

/**
 * @}
 */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtModuleSoftAps.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : Soft Linear APS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESOFTAPS_H_
#define _ATMODULESOFTAPS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSoftAps * AtModuleSoftAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps AtModuleSoftApsNew(AtDevice device);

#endif /* _ATMODULESOFTAPS_H_ */


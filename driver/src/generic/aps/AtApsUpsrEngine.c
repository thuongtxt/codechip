/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsUpsrEngine.c
 *
 * Created Date: Jul 1, 2016
 *
 * Description : UPSR engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtApsUpsrEngine.h"
#include "AtApsUpsrEngineInternal.h"
#include "../sdh/AtSdhPathInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtApsUpsrEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tAtApsUpsrEngineMethods m_methods;

/* Override */
static tAtChannelMethods   m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods   *m_AtChannelMethods   = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool EngineIsStarted(AtApsEngine self)
    {
    return (mMethodsGet(self)->StateGet(self) == cAtApsEngineStateStart) ? cAtTrue : cAtFalse;
    }

static eAtModuleApsRet ExtCmdSet(AtApsUpsrEngine engine, eAtApsUpsrExtCmd extlCmd)
    {
    AtUnused(engine);
    AtUnused(extlCmd);
    return cAtErrorNotImplemented;
    }

static eAtApsUpsrExtCmd ExtCmdGet(AtApsUpsrEngine engine)
    {
    AtUnused(engine);

    return cAtApsUpsrExtCmdUnknown;
    }

static AtSdhPath WorkingPathGet(AtApsUpsrEngine engine)
    {
    return engine ? engine->working : NULL;
    }

static AtSdhPath ProtectionPathGet(AtApsUpsrEngine engine)
    {
    return engine ? engine->protection : NULL;
    }

static AtSdhPath ActivePathGet(AtApsUpsrEngine self)
    {
    return self->working;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "upsr_engine";
    }

static uint32 DefaultSfConditions(void)
    {
    return (cAtSdhPathAlarmAis   |
            cAtSdhPathAlarmLop   |
            cAtSdhPathAlarmUneq  |
            cAtSdhPathAlarmPlm   |
            cAtSdhPathAlarmBerSf);
    }

static uint32 DefaultSdConditions(void)
    {
    return cAtSdhPathAlarmBerSd;
    }

static uint32 DefaultNoneConditions(void)
    {
    return cAtSdhPathAlarmTim;
    }

static eAtRet Init(AtChannel self)
    {
    AtApsEngine engine = (AtApsEngine)self;
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtApsUpsrEngineExtCmdSet(mThis(self), cAtApsUpsrExtCmdClear);
    ret |= AtApsEngineSwitchTypeSet(engine, cAtApsSwitchTypeNonRev);
    ret |= AtApsEngineWtrSet(engine, 5 * 60); /* 5 minutes */

    ret |= AtApsEngineSwitchingConditionSet(engine, DefaultSfConditions(), cAtApsSwitchingConditionSf);
    ret |= AtApsEngineSwitchingConditionSet(engine, DefaultSdConditions(), cAtApsSwitchingConditionSd);
    ret |= AtApsEngineSwitchingConditionSet(engine, DefaultNoneConditions(), cAtApsSwitchingConditionNone);

    return ret;
    }

static eBool ExtCmdIsValid(eAtApsUpsrExtCmd extCmd)
    {
    switch (extCmd)
        {
        case cAtApsUpsrExtCmdClear : return cAtTrue;
        case cAtApsUpsrExtCmdFs2Wrk: return cAtTrue;
        case cAtApsUpsrExtCmdFs2Prt: return cAtTrue;
        case cAtApsUpsrExtCmdMs2Wrk: return cAtTrue;
        case cAtApsUpsrExtCmdMs2Prt: return cAtTrue;
        case cAtApsUpsrExtCmdLp    : return cAtTrue;

        case cAtApsUpsrExtCmdUnknown:
        default:
            return cAtFalse;
        }
    }

static uint32 CommandFlatPriority(eAtApsUpsrExtCmd extCmd)
    {
    switch (extCmd)
        {
        case cAtApsUpsrExtCmdLp     : return 3;
        case cAtApsUpsrExtCmdClear  : return 3;
        case cAtApsUpsrExtCmdFs2Wrk : return 2;
        case cAtApsUpsrExtCmdFs2Prt : return 2;
        case cAtApsUpsrExtCmdMs2Wrk : return 1;
        case cAtApsUpsrExtCmdMs2Prt : return 1;

        case cAtApsUpsrExtCmdUnknown:
        default:
            return 0;
        }
    }

static eAtModuleApsRet ExtCmdCheck(AtApsUpsrEngine self, eAtApsUpsrExtCmd extCmd)
    {
    eAtApsUpsrExtCmd currentCommand;

    if (!ExtCmdIsValid(extCmd))
        return cAtErrorInvlParm;

    /* Clear command is always applicable. */
    if (extCmd == cAtApsUpsrExtCmdClear)
        return cAtOk;

    currentCommand = AtApsUpsrEngineExtCmdGet(self);
    if (extCmd == currentCommand)
        return cAtOk;

    if (currentCommand == cAtApsUpsrExtCmdClear)
        return cAtOk;

    if (CommandFlatPriority(extCmd) <= CommandFlatPriority(currentCommand))
        return cAtModuleApsErrorExtCmdLowPriority;

    return cAtOk;
    }

static void OverrideAtChannel(AtApsUpsrEngine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtApsUpsrEngine self)
    {
    OverrideAtChannel(self);
    }

static void MethodsInit(AtApsUpsrEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ExtCmdSet);
        mMethodOverride(m_methods, ExtCmdGet);
        mMethodOverride(m_methods, ExtCmdCheck);
        mMethodOverride(m_methods, WorkingPathGet);
        mMethodOverride(m_methods, ProtectionPathGet);
        mMethodOverride(m_methods, ActivePathGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsUpsrEngine);
    }

AtApsEngine AtApsUpsrEngineObjectInit(AtApsEngine self,
                                      AtModuleAps module,
                                      uint32 engineId,
                                      AtSdhPath working,
                                      AtSdhPath protection)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsEngineObjectInit(self, module, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->working    = working;
    mThis(self)->protection = protection;

    return self;
    }

eAtModuleApsRet AtApsUpsrEngineExtCmdCheck(AtApsUpsrEngine self, eAtApsUpsrExtCmd extCmd)
    {
    if (self)
        return mMethodsGet(self)->ExtCmdCheck(self, extCmd);
    return cAtErrorNullPointer;
    }

/**
 * @addtogroup AtApsUpsrEngine
 * @{
 */

/**
 * Issue external command
 *
 * @param self This engine
 * @param extCmd @ref eAtApsUpsrExtCmd "External command"
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsUpsrEngineExtCmdSet(AtApsUpsrEngine self, eAtApsUpsrExtCmd extCmd)
    {
    if (self == NULL)
        mChannelError(self, cAtErrorNullPointer);

    if (EngineIsStarted((AtApsEngine)self))
        {
        eAtRet ret = AtApsUpsrEngineExtCmdCheck(self, extCmd);
        if (ret != cAtOk)
            mChannelError(self, ret);

        mNumericalAttributeSet(ExtCmdSet, extCmd);
        }

    /* As the engine has not been started so there would be no active external
     * command. When Clear command is applied, just simply ignore it rather than
     * returning error */
    if (extCmd == cAtApsUpsrExtCmdClear)
        return cAtOk;

    mChannelError(self, cAtErrorNotReady);
    }

/**
 * Get external command that is being active
 *
 * @param self This engine
 *
 * @return @ref eAtApsUpsrExtCmd "Active external command"
 */
eAtApsUpsrExtCmd AtApsUpsrEngineExtCmdGet(AtApsUpsrEngine self)
    {
    mAttributeGet(ExtCmdGet, eAtApsUpsrExtCmd, cAtApsUpsrExtCmdUnknown);
    }

/**
 * Get Working Path provisioned to APS engine
 *
 * @param self This engine
 *
 * @return Working Path
 */
AtSdhPath AtApsUpsrEngineWorkingPathGet(AtApsUpsrEngine self)
    {
    mAttributeGet(WorkingPathGet, AtSdhPath, NULL);
    }

/**
 * Get Protection Path provisioned to APS engine
 *
 * @param self This engine
 *
 * @return Protection Path
 */
AtSdhPath AtApsUpsrEngineProtectionPathGet(AtApsUpsrEngine self)
    {
    mAttributeGet(ProtectionPathGet, AtSdhPath, NULL);
    }

/**
 * Get Active Path provisioned to APS engine
 *
 * @param self This engine
 *
 * @return Protection Path
 */
AtSdhPath AtApsUpsrEngineActivePathGet(AtApsUpsrEngine self)
    {
    mAttributeGet(ActivePathGet, AtSdhPath, NULL);
    }

/**
 * @}
 */

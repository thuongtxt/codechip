/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsLinearEngine.c
 *
 * Created Date: Jun 6, 2013
 *
 * Description : Linear APS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtApsLinearEngineInternal.h"
#include "../sdh/AtSdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tAtApsLinearEngineMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleApsRet OpMdSet(AtApsLinearEngine self, eAtApsLinearOpMode opMd)
    {
	AtUnused(opMd);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtApsLinearOpMode OpMdGet(AtApsLinearEngine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtApsLinearOpModeUni;
    }

static eAtModuleApsRet ArchSet(AtApsLinearEngine self, eAtApsLinearArch arch)
    {
	AtUnused(arch);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtApsLinearArch ArchGet(AtApsLinearEngine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtApsLinearArch11;
    }

static eAtModuleApsRet ExtCmdSet(AtApsLinearEngine engine, eAtApsLinearExtCmd extlCmd, AtSdhLine affectedLine)
    {
	AtUnused(affectedLine);
	AtUnused(extlCmd);
	AtUnused(engine);
    return cAtErrorNotImplemented;
    }

static eAtApsLinearExtCmd ExtCmdGet(AtApsLinearEngine engine)
    {
	AtUnused(engine);
    return cAtApsLinearExtCmdUnknown;
    }

static AtSdhLine ExtCmdAffectedLineGet(AtApsLinearEngine engine)
    {
	AtUnused(engine);
    return NULL;
    }

static AtSdhLine SwitchLineGet(AtApsLinearEngine self)
    {
	AtUnused(self);
    return NULL;
    }

static void MethodsInit(AtApsLinearEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, OpMdGet);
        mMethodOverride(m_methods, OpMdSet);
        mMethodOverride(m_methods, ArchGet);
        mMethodOverride(m_methods, ArchSet);
        mMethodOverride(m_methods, ExtCmdSet);
        mMethodOverride(m_methods, ExtCmdGet);
        mMethodOverride(m_methods, ExtCmdAffectedLineGet);
        mMethodOverride(m_methods, SwitchLineGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void LinesReferenceDelete(AtApsLinearEngine self)
    {
    AtIterator iterator;
    AtSdhLine sdhLine;

    if (self->sdhLines == NULL)
        return;

    iterator = AtListIteratorCreate(self->sdhLines);
    while ((sdhLine = (AtSdhLine)AtIteratorNext(iterator)) != NULL)
        AtSdhLineApsEngineSet(sdhLine, NULL);
    AtObjectDelete((AtObject)iterator);

    AtObjectDelete((AtObject)(self->sdhLines));
    self->sdhLines = NULL;
    }

static void Delete(AtObject self)
    {
    LinesReferenceDelete((AtApsLinearEngine)self);
    m_AtObjectMethods->Delete(self);
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "apsl_engine";
    }

static void OverrideAtChannel(AtApsLinearEngine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtApsLinearEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtApsLinearEngine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsLinearEngine);
    }

static eAtRet SdhLinesCache(AtApsLinearEngine self, AtSdhLine lines[], uint8 numLines)
    {
    uint8 i;

    if (self->sdhLines != NULL)
        return cAtErrorChannelBusy;

    self->sdhLines = AtListCreate(numLines);
    if (self->sdhLines == NULL)
        return cAtErrorRsrcNoAvail;

    for (i = 0; i < numLines; i++)
        {
        AtListObjectAdd(self->sdhLines, (AtObject)lines[i]);
        AtSdhLineApsEngineSet(lines[i], (AtApsEngine)self);
        }

    return cAtOk;
    }

AtApsEngine AtApsLinearEngineObjectInit(AtApsEngine self, AtModuleAps module, uint32 engineId, AtSdhLine lines[], uint8 numLine)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsEngineObjectInit((AtApsEngine)self, module, engineId) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((AtApsLinearEngine)self);
    Override((AtApsLinearEngine)self);
    m_methodsInit = 1;

    /* Cache */
    if (SdhLinesCache((AtApsLinearEngine)self, lines, numLine) != cAtOk)
        {
        AtObjectDelete((AtObject)self);
        return NULL;
        }

    return self;
    }

eAtModuleApsRet AtApsLinearEngineSwitchTypeSet(AtApsLinearEngine self, eAtApsLinearSwitchType swType)
    {
    return AtApsEngineSwitchTypeSet((AtApsEngine)self, swType);
    }

eAtApsLinearSwitchType AtApsLinearEngineSwitchTypeGet(AtApsLinearEngine self)
    {
    return AtApsEngineSwitchTypeGet((AtApsEngine)self);
    }

/**
 * @addtogroup AtApsLinearEngine
 * @{
 */

/**
 * Set operation mode
 *
 * @param self This engine
 * @param opMd @ref eAtApsLinearOpMode "Operation mode"
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsLinearEngineOpModeSet(AtApsLinearEngine self, eAtApsLinearOpMode opMd)
    {
    mNumericalAttributeSet(OpMdSet, opMd);
    }

/**
 * Get operation mode
 *
 * @param self This engine
 * @return @ref eAtApsLinearOpMode "Operation mode"
 */
eAtApsLinearOpMode AtApsLinearEngineOpModeGet(AtApsLinearEngine self)
    {
    mAttributeGet(OpMdGet, eAtApsLinearOpMode, cAtApsLinearOpModeBid);
    }

/**
 * Set architecture
 *
 * @param self This engine
 * @param arch @ref eAtApsLinearArch "Architecture"
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsLinearEngineArchSet(AtApsLinearEngine self, eAtApsLinearArch arch)
    {
    mNumericalAttributeSet(ArchSet, arch);
    }

/**
 * Get engine architecture
 *
 * @param self This engine
 *
 * @return @ref eAtApsLinearArch "Architecture"
 */
eAtApsLinearArch AtApsLinearEngineArchGet(AtApsLinearEngine self)
    {
    mAttributeGet(ArchGet, eAtApsLinearArch, cAtApsLinearArch11);
    }

/**
 * Issue external command
 *
 * @param self This engine
 * @param extlCmd @ref eAtApsLinearExtCmd "External commands"
 * @param affectedLine The line that this command will issue.
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsLinearEngineExtCmdSet(AtApsLinearEngine self, eAtApsLinearExtCmd extlCmd, AtSdhLine affectedLine)
    {
    if (self)
        return mMethodsGet(self)->ExtCmdSet(self, extlCmd, affectedLine);

    return cAtErrorNotImplemented;
    }

/**
 * Get external command that is being issued on engine
 *
 * @param self This engine
 *
 * @return @ref eAtApsLinearExtCmd "External command" that is being issued.
 */
eAtApsLinearExtCmd AtApsLinearEngineExtCmdGet(AtApsLinearEngine self)
    {
    mAttributeGet(ExtCmdGet, eAtApsLinearExtCmd, cAtApsLinearExtCmdUnknown);
    }

/**
 * Get the line that external is being issued to
 *
 * @param self This engine
 *
 * @return Line that external is being issued to. NULL if no external command is
 * issued.
 */
AtSdhLine AtApsLinearEngineExtCmdAffectedLineGet(AtApsLinearEngine self)
    {
    mNoParamObjectGet(ExtCmdAffectedLineGet, AtSdhLine);
    }

/**
 * Get the line that switching is taking place
 *
 * @param self This engine
 *
 * @return Line that switching is taking place, or NULL if engine is in normal
 * state which no line is switched.
 */
AtSdhLine AtApsLinearEngineSwitchLineGet(AtApsLinearEngine self)
    {
    mNoParamObjectGet(SwitchLineGet, AtSdhLine);
    }

/**
 * Get all of lines provisioned to APS engine
 *
 * @param self This engine
 *
 * @return List of lines
 */
AtList AtApsLinearEngineLinesGet(AtApsLinearEngine self)
    {
    return self ? self->sdhLines : NULL;
    }

/**
 * @}
 */

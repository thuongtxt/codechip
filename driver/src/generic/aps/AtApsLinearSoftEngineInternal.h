/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsLinearSoftEngineInternal.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : Linear APS soft engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSLINEARSOFTENGINEINTERNAL_H_
#define _ATAPSLINEARSOFTENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsLinearEngineInternal.h"
#include "AtApsLinearSoftEngine.h"
#include "aps/atlaps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsLinearSoftEngine
    {
    tAtApsLinearEngine super;

    AtLApsEngine lapsHandler;
    }tAtApsLinearSoftEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _ATAPSLINEARSOFTENGINEINTERNAL_H_ */


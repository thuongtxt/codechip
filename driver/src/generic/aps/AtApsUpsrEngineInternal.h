/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsUpsrEngineInternal.h
 * 
 * Created Date: Jul 13, 2016
 *
 * Description : Upsr APS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSUPSRENGINEINTERNAL_H_
#define _ATAPSUPSRENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../include/aps/AtApsUpsrEngine.h"
#include "AtApsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsUpsrEngineMethods
    {
    /* External command */
    eAtModuleApsRet (*ExtCmdSet)(AtApsUpsrEngine engine, eAtApsUpsrExtCmd extlCmd);
    eAtApsUpsrExtCmd (*ExtCmdGet)(AtApsUpsrEngine engine);
    eAtModuleApsRet (*ExtCmdCheck)(AtApsUpsrEngine self, eAtApsUpsrExtCmd extCmd);
    AtSdhPath (*WorkingPathGet)(AtApsUpsrEngine engine);
    AtSdhPath (*ProtectionPathGet)(AtApsUpsrEngine engine);
    AtSdhPath (*ActivePathGet)(AtApsUpsrEngine engine);
    }tAtApsUpsrEngineMethods;

typedef struct tAtApsUpsrEngine
    {
    tAtApsEngine super;
    const tAtApsUpsrEngineMethods *methods;

    /* Private data */
    AtSdhPath working;
    AtSdhPath protection;
    }tAtApsUpsrEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine AtApsUpsrEngineObjectInit(AtApsEngine self,
                                      AtModuleAps module,
                                      uint32 engineId,
                                      AtSdhPath working,
                                      AtSdhPath protection);

eAtModuleApsRet AtApsUpsrEngineExtCmdCheck(AtApsUpsrEngine self, eAtApsUpsrExtCmd extCmd);

#endif /* _ATAPSUPSRENGINEINTERNAL_H_ */


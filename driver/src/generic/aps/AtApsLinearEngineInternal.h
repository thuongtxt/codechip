/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsLinearEngineInternal.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : Linear APS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSLINEARENGINEINTERNAL_H_
#define _ATAPSLINEARENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsLinearEngine.h"
#include "AtApsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsLinearEngineMethods
    {
    /* Operation mode */
    eAtModuleApsRet (*OpMdSet)(AtApsLinearEngine self, eAtApsLinearOpMode opMd);
    eAtApsLinearOpMode (*OpMdGet)(AtApsLinearEngine self);

    /* Architecture */
    eAtModuleApsRet (*ArchSet)(AtApsLinearEngine self, eAtApsLinearArch arch);
    eAtApsLinearArch (*ArchGet)(AtApsLinearEngine self);

    /* External command */
    eAtModuleApsRet (*ExtCmdSet)(AtApsLinearEngine engine, eAtApsLinearExtCmd extlCmd, AtSdhLine affectedLine);
    eAtApsLinearExtCmd (*ExtCmdGet)(AtApsLinearEngine engine);
    AtSdhLine (*ExtCmdAffectedLineGet)(AtApsLinearEngine engine);
    AtSdhLine (*SwitchLineGet)(AtApsLinearEngine self);
    }tAtApsLinearEngineMethods;

typedef struct tAtApsLinearEngine
    {
    tAtApsEngine super;
    const tAtApsLinearEngineMethods *methods;
    AtList sdhLines;
    }tAtApsLinearEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine AtApsLinearEngineObjectInit(AtApsEngine self, AtModuleAps module, uint32 engineId, AtSdhLine lines[], uint8 numLine);

#endif /* _ATAPSLINEARENGINEINTERNAL_H_ */


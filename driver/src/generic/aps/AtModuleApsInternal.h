/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtModuleApsInternal.h
 * 
 * Created Date: Jun 3, 2013
 *
 * Description : APS abstract module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEAPSINTERNAL_H_
#define _ATMODULEAPSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h"
#include "AtApsUpsrEngine.h"
#include "AtApsLinearEngine.h"
#include "AtApsGroup.h"
#include "AtApsSelector.h"
#include "AtList.h"
#include "../man/AtModuleInternal.h"
#include "../common/AtChannelInternal.h"
#include "../sdh/AtStsGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleApsMethods
    {
    /* Linear engines management */
    AtApsEngine (*LinearEngineCreate)(AtModuleAps self, uint32 engineId, AtSdhLine lines[], uint8 numLines, eAtApsLinearArch arch);
    eAtModuleApsRet (*LinearEngineDelete)(AtModuleAps self, uint32 engineId);

    uint32 (*NumLinearEngines)(AtModuleAps self);
    uint32 (*MaxNumLinearEngines)(AtModuleAps self);
    uint8 (*PeriodicProcess)(AtModuleAps self, uint32 periodInMs);

    /* UPSR engines management */
    AtApsEngine (*UpsrEngineCreate)(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection);
    eAtModuleApsRet (*UpsrEngineDelete)(AtModuleAps self, uint32 engineId);
    uint32 (*MaxNumUpsrEngines)(AtModuleAps self);

    /* APS Selector management. */
    uint32 (*MaxNumSelectors)(AtModuleAps self);
    AtApsSelector (*SelectorCreate)(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing);
    eAtModuleApsRet (*SelectorDelete)(AtModuleAps self, uint32 selectorId);

    /* APS Group management. */
    uint32 (*MaxNumGroups)(AtModuleAps self);
    AtApsGroup (*GroupCreate)(AtModuleAps self, uint32 groupId);
    eAtRet (*GroupDelete)(AtModuleAps self, uint32 groupId);

    /* STS pass-through Group management */
    uint32 (*MaxNumStsGroups)(AtModuleAps self);
    AtStsGroup (*StsGroupCreate)(AtModuleAps self, uint32 groupId);
    eAtRet (*StsGroupDelete)(AtModuleAps self, uint32 groupId);
    }tAtModuleApsMethods;

typedef struct tAtModuleAps
    {
    tAtModule super;
    const tAtModuleApsMethods *methods;

    /* Private data */
    AtApsEngine *linearEngines;
    uint32 maxNumLinearEngines;
    AtApsEngine *upsrEngines;
    uint32 maxNumUpsrEngines;
    AtApsSelector *selectors;
    uint32 maxNumSelectors;
    AtApsGroup *groups;
    uint32 maxNumGroups;
    AtStsGroup *stsGroups;
    uint32 maxNumStsGroups;
    }tAtModuleAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps AtModuleApsObjectInit(AtModuleAps self, AtDevice device);

/* Module internal methods */
void AtApsModuleAllEnginesDelete(AtModuleAps self);

#endif /* _ATMODULEAPSINTERNAL_H_ */


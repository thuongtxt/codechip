/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsEngine.c
 *
 * Created Date: Jun 19, 2013
 *
 * Description : Abstract APS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtApsEngine.h"
#include "AtApsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtApsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtApsEngineMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtApsEngineState StateGet(AtApsEngine self)
    {
    AtUnused(self);

    /* Let concrete class do */
    return cAtApsEngineStateUnknown;
    }

static uint32 WtrGet(AtApsEngine self)
    {
    AtUnused(self);

    /* Let concrete class do */
    return 0;
    }

static eAtModuleApsRet WtrSet(AtApsEngine self, uint32 wtrInSec)
    {
    AtUnused(self);
    AtUnused(wtrInSec);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtModuleApsRet SwitchingConditionSet(AtApsEngine self, uint32 defects, eAtApsSwitchingCondition condition)
    {
    AtUnused(self);
    AtUnused(defects);
    AtUnused(condition);

    return cAtErrorNotImplemented;
    }

static eAtApsSwitchingCondition SwitchingConditionGet(AtApsEngine self, uint32 defect)
    {
    AtUnused(self);
    AtUnused(defect);

    return cAtApsSwitchingConditionNone;
    }

static eAtModuleApsRet SwitchTypeSet(AtApsEngine self, eAtApsSwitchType swType)
    {
    AtUnused(self);
    AtUnused(swType);

    return cAtErrorNotImplemented;
    }

static eAtApsSwitchType SwitchTypeGet(AtApsEngine self)
    {
    AtUnused(self);

    return cAtApsSwitchTypeNonRev;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "aps_engine";
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eBool SwitchTypeIsValid(eAtApsSwitchType swType)
    {
    if ((swType == cAtApsSwitchTypeNonRev) ||
        (swType == cAtApsSwitchTypeRev))
        return cAtTrue;
    return cAtFalse;
    }

static eAtApsRequestState RequestStateGet(AtApsEngine self)
    {
    AtUnused(self);
    return cAtApsRequestStateUnknown;
    }

static void OverrideAtChannel(AtApsEngine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void MethodsInit(AtApsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StateGet);
        mMethodOverride(m_methods, WtrGet);
        mMethodOverride(m_methods, WtrSet);
        mMethodOverride(m_methods, SwitchTypeSet);
        mMethodOverride(m_methods, SwitchTypeGet);
        mMethodOverride(m_methods, SwitchingConditionSet);
        mMethodOverride(m_methods, SwitchingConditionGet);
        mMethodOverride(m_methods, RequestStateGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtApsEngine self)
    {
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsEngine);
    }

AtApsEngine AtApsEngineObjectInit(AtApsEngine self, AtModuleAps module, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, engineId, (AtModule)module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

/*
 * Get switching Reason
 *
 * @param self This engine
 *
 * @return @ref eAtApsSwitchingReason "Switching reason" for the switching
 */
eAtApsSwitchingReason AtApsEngineSwitchingReasonGet(AtApsEngine self)
    {
    if (self)
        return (eAtApsSwitchingReason)AtApsEngineRequestStateGet(self);
    return cAtApsSwitchingReasonNone;
    }

/**
 * @addtogroup AtApsEngine
 * @{
 */

/**
 * Engine start
 * @param self This engine
 *
 * @return At return code
 */
eAtModuleApsRet AtApsEngineStart(AtApsEngine self)
    {
    mNoParamWrapCall(eAtModuleApsRet, AtChannelEnable((AtChannel)self, cAtTrue));
    }

/**
 * Engine stop
 *
 * @param self This engine
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsEngineStop(AtApsEngine self)
    {
    mNoParamWrapCall(eAtModuleApsRet, AtChannelEnable((AtChannel)self, cAtFalse));
    }

/**
 * Get engine's state
 *
 * @param self This engine
 *
 * @return APS engine state @ref eAtApsEngineState "APS engine state"
 */
eAtApsEngineState AtApsEngineStateGet(AtApsEngine self)
    {
    mAttributeGet(StateGet, eAtApsEngineState, cAtApsEngineStateUnknown);
    }

/**
 * Check if engine can be started
 * @param self This engine
 *
 * @retval cAtTrue if engine can be started
 * @retval cAtFalse if engine cannot be started
 */
eBool AtApsEngineCanStart(AtApsEngine self)
    {
    return AtChannelCanEnable((AtChannel)self, cAtTrue);
    }

/**
 * Get Wait-To-Restore time in number of second
 *
 * @param self This engine
 *
 * @return Wait-To-Restore time in number of second
 */
uint32 AtApsEngineWtrGet(AtApsEngine self)
    {
    mAttributeGet(WtrGet, uint32, 0);
    }

/**
 * Set Wait-To-Restore time for engine
 *
 * @param self This Engine
 * @param wtrInSec Wait-To-Restore time in number of second
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsEngineWtrSet(AtApsEngine self, uint32 wtrInSec)
    {
    mNumericalAttributeSet(WtrSet, wtrInSec);
    }

/**
 * Set switching type
 *
 * @param self This engine
 * @param swType Switching type
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsEngineSwitchTypeSet(AtApsEngine self, eAtApsSwitchType swType)
    {
    if (self == NULL)
        mChannelError(self, cAtErrorNullPointer);

    if (SwitchTypeIsValid(swType))
        {
        mNumericalAttributeSet(SwitchTypeSet, swType);
        }

    mChannelError(self, cAtErrorInvlParm);
    }

/**
 * Get switching type
 *
 * @param self This Engine
 *
 * @return Switching type
 */
eAtApsSwitchType AtApsEngineSwitchTypeGet(AtApsEngine self)
    {
    mAttributeGet(SwitchTypeGet, eAtApsSwitchType, cAtApsSwitchTypeNonRev);
    }

/**
 * Set switching condition
 *
 * @param self This engine
 * @param defects Defect to configure
 * @param condition @ref eAtApsSwitchingCondition "Switching condition" for the input defect
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsEngineSwitchingConditionSet(AtApsEngine self, uint32 defects, eAtApsSwitchingCondition condition)
    {
    mTwoParamsAttributeSet(SwitchingConditionSet, defects, condition);
    }

/**
 * Get switching condition
 *
 * @param self This engine
 * @param defect Defect
 *
 * @return @ref eAtApsSwitchingCondition "Switching condition" for the input defect
 */
eAtApsSwitchingCondition AtApsEngineSwitchingConditionGet(AtApsEngine self, uint32 defect)
    {
    mOneParamAttributeGet(SwitchingConditionGet, defect, eAtApsSwitchingCondition, cAtApsSwitchingConditionNone)
    }

/**
 * Get the current local request state
 *
 * @param self This engine
 *
 * @return @ref eAtApsRequestState "Local request state"
 */
eAtApsRequestState AtApsEngineRequestStateGet(AtApsEngine self)
    {
    mAttributeGet(RequestStateGet, eAtApsRequestState, cAtApsRequestStateUnknown);
    }

/**
 * Get engine ID
 *
 * @param self This engine
 *
 * @return Engine ID
 */
uint32 AtApsEngineIdGet(AtApsEngine self)
    {
    if (self)
        return AtChannelIdGet((AtChannel)self);
    return cInvalidUint32;
    }

/**
 * Initialize engine to bring it to default state
 *
 * @param self This engine
 *
 * @return AT return code
 */
eAtModuleApsRet AtApsEngineInit(AtApsEngine self)
    {
    mNoParamWrapCall(eAtModuleApsRet, AtChannelInit((AtChannel)self));
    }

/**
 * @}
 */

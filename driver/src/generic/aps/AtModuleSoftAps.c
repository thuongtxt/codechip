/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtModuleSoftAps.c
 *
 * Created Date: Jun 6, 2013
 *
 * Description : Software Linear APS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleSoftApsInternal.h"
#include "AtApsLinearSoftEngineInternal.h"
#include "AtApsLinearEngine.h"
#include "../sdh/AtSdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMaxNumLinesInEngine 16

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtModuleSoftAps)self)
#define mLApsModule(self) (AtModuleSoftApsLApsModuleGet((AtModuleSoftAps)self))

#define mDefectMaskConvert(defStat_, defMsk_, sdkAlarm, componentAlarm)       \
    if(changedAlarms & sdkAlarm)                       \
        {                                              \
        defMsk_ |= componentAlarm;                     \
        if (currentStatus & sdkAlarm)                  \
            defStat_ |= componentAlarm;                \
        }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* For override */
static tAtModuleMethods    m_AtModuleOverride;
static tAtObjectMethods    m_AtObjectOverride;
static tAtModuleApsMethods m_AtModuleApsOverride;

/* To hold super implementation */
static const tAtModuleMethods    *m_AtModuleMethods    = NULL;
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtModuleApsMethods *m_AtModuleApsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtModule self)
    {
    return m_AtModuleMethods->Init(self);
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void LApsHandlerDelete(AtModuleSoftAps self, AtLApsModule lapsHandler)
    {
    AtLApsDelete(lapsHandler);
    mThis(self)->lapsHandler = NULL;
    }

static eAtRet Setup(AtModule self)
    {
    AtModuleSoftAps apsModule = (AtModuleSoftAps)self;
    uint16 maxNumLine;
    AtModuleSdh moduleSdh;
    eAtRet ret;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Delete first */
    if (mLApsModule(self) != NULL)
        LApsHandlerDelete(apsModule, mLApsModule(self));

    /* Create APS handler */
    moduleSdh = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleSdh);
    maxNumLine = AtModuleSdhMaxLinesGet(moduleSdh);
    apsModule->lapsHandler = AtLApsCreate(maxNumLine, AtApsOsIntf());
    if (apsModule->lapsHandler == NULL)
        return cAtError;

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtApsModuleAllEnginesDelete((AtModuleAps)self);
    LApsHandlerDelete(mThis(self), mLApsModule(self));
    m_AtObjectMethods->Delete(self);
    }

static void LineIdArrayGet(AtSdhLine lines[], uint8 numLines, uint8 *lineIds)
    {
    uint8 i;
    for (i = 0; i < numLines; i++)
        lineIds[i] = (uint8)AtChannelIdGet((AtChannel)lines[i]);
    }

static eAtLApsRet SwFunc(AtLApsEngine engine,
                         uint8 w_line,
                         uint8 p_line,
                         atbool release,
                         void *pUsrData,
                         uint32 usrDataLen)
    {
    AtModuleAps moduleAps    = *((AtModuleAps*)pUsrData);
    AtModuleSdh moduleSdh    = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)moduleAps), cAtModuleSdh);
    AtSdhLine workingLine    = AtModuleSdhLineGet(moduleSdh, w_line);
    AtSdhLine protectionLine = AtModuleSdhLineGet(moduleSdh, p_line);
    eAtModuleSdhRet ret;
	AtUnused(usrDataLen);
	AtUnused(engine);

    if (release)
        ret = AtSdhLineSwitchRelease(workingLine);
    else
        ret = AtSdhLineSwitch(workingLine, protectionLine);

    return (ret == cAtOk) ? cAtLApsErr : cAtLApsOk;
    }

static eAtLApsRet BrFunc(AtLApsEngine engine,
                         uint8 w_line,
                         uint8 p_line,
                         atbool release,
                         void *pUsrData,
                         uint32 usrDataLen)
    {
    AtModuleAps moduleAps    = *((AtModuleAps*)pUsrData);
    AtModuleSdh moduleSdh    = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)moduleAps), cAtModuleSdh);
    AtSdhLine workingLine    = AtModuleSdhLineGet(moduleSdh, w_line);
    AtSdhLine protectionLine = AtModuleSdhLineGet(moduleSdh, p_line);
    eAtModuleSdhRet ret;
	AtUnused(usrDataLen);
	AtUnused(engine);

    if (release)
        ret = AtSdhLineBridgeRelease(workingLine);
    else
        ret = AtSdhLineBridge(workingLine, protectionLine);

    return (ret == cAtOk) ? cAtLApsErr : cAtLApsOk;
    }

static eAtLApsRet KByteSendFunc(AtLApsEngine engine,
                                uint8 line,
                                uint8 k1,
                                uint8 k2,
                                void *pUsrData,
                                uint32 usrDataLen)
    {
    AtModuleAps moduleAps = *((AtModuleAps*)pUsrData);
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)moduleAps), cAtModuleSdh);
    AtSdhLine sdhLine = AtModuleSdhLineGet(moduleSdh, line);
    eAtModuleSdhRet ret = cAtOk;
	AtUnused(usrDataLen);
	AtUnused(engine);

    ret |= AtSdhLineTxK1Set(sdhLine, k1);
    ret |= AtSdhLineTxK2Set(sdhLine, k2);

    return (ret == cAtOk) ? cAtLApsErr : cAtLApsOk;
    }

static eAtLApsArch ArchGet(eAtApsLinearArch arch)
    {
    if (arch == cAtApsLinearArch11) return cAtLApsArch11;
    if (arch == cAtApsLinearArch1n) return cAtLApsArch1n;

    return cAtLApsArch11;
    }

static AtLApsEngine LAspEngineCreateWraper(AtModuleAps self,
                                           AtSdhLine lines[],
                                           uint8 numLines,
                                           eAtApsLinearArch arch)
    {
    AtLApsEngine lApsEngine;
    uint8 lineIds[cMaxNumLinesInEngine];
    eAtLApsArch lapsArch     = ArchGet(arch);
    AtLApsEngBrSwFunc swFunc = SwFunc;
    AtLApsEngBrSwFunc brFunc = BrFunc;
    AtLApsEngKByteSendFunc kByteSendFunc = KByteSendFunc;

    LineIdArrayGet(lines, numLines, lineIds);

    lApsEngine = AtLApsEngCreate(mLApsModule(self),
                                 lineIds,
                                 numLines,
                                 lapsArch,
                                 cAtLApsOpMdUni,
                                 cAtLApsSwTypeNonRev,
                                 0,
                                 swFunc,
                                 brFunc,
                                 kByteSendFunc,
                                 &self,
                                 sizeof(AtModuleAps));

    return lApsEngine;
    }

static void ConvertDefectMask(uint32 changedAlarms, uint32 currentStatus, uint32 *defMsk, uint32 *defStat)
    {
    uint32 status = 0, mask = 0;

    mDefectMaskConvert(status, mask, cAtSdhLineAlarmLos  , cAtLApsSwCondLos);
    mDefectMaskConvert(status, mask, cAtSdhLineAlarmOof  , cAtLApsSwCondLof);
    mDefectMaskConvert(status, mask, cAtSdhLineAlarmLof  , cAtLApsSwCondOof);
    mDefectMaskConvert(status, mask, cAtSdhLineAlarmTim  , cAtLApsSwCondTims);
    mDefectMaskConvert(status, mask, cAtSdhLineAlarmAis  , cAtLApsSwCondAisl);
    mDefectMaskConvert(status, mask, cAtSdhLineAlarmBerSf, cAtLApsSwCondBerSf);
    mDefectMaskConvert(status, mask, cAtSdhLineAlarmBerSd, cAtLApsSwCondBerSd);

    *defMsk  = mask;
    *defStat = status;
    }

static void AlarmChangeState(AtChannel channel, uint32 changedAlarms, uint32 currentStatus)
    {
    AtSdhLine sdhLine = (AtSdhLine)channel;
    AtApsEngine engine = AtSdhLineApsEngineGet(sdhLine);
    AtModuleSoftAps module = (AtModuleSoftAps)AtChannelModuleGet((AtChannel)engine);
    uint32 defMsk, defStat;

    ConvertDefectMask(changedAlarms, currentStatus, &defMsk, &defStat);
    AtLApsLineDefNotify(mLApsModule(module), (uint8)AtChannelIdGet(channel), defMsk, defStat);

    if (changedAlarms & cAtSdhLineAlarmKByteChange)
        {
        uint8 k1 = AtSdhLineRxK1Get(sdhLine);
        uint8 k2 = AtSdhLineRxK2Get(sdhLine);

        AtLApsLineKByteChange(mLApsModule(module), (uint8)AtChannelIdGet(channel), k1, k2);
        }
    }

static void LineListenersRegister(AtModuleAps self, AtSdhLine lines[], uint8 numLine)
    {
    tAtChannelEventListener listener;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 i;
	AtUnused(self);

    mMethodsGet(osal)->MemInit(osal, &listener, 0, sizeof(listener));
    listener.AlarmChangeState = AlarmChangeState;

    for (i = 0; i < numLine; i++)
        AtChannelEventListenerAdd((AtChannel)lines[i], &listener);
    }

/* Create APS engine */
static AtApsEngine LinearEngineCreate(AtModuleAps self,
                                      uint32 engineId,
                                      AtSdhLine lines[],
                                      uint8 numLines,
                                      eAtApsLinearArch arch)
    {
    AtApsEngine apsEngine;
    AtLApsEngine lapsHandler;

    if (mLApsModule(self) == NULL)
        return NULL;

    lapsHandler = LAspEngineCreateWraper(self, lines, numLines, arch);
    if (lapsHandler == NULL)
        return NULL;

    apsEngine = AtApsLinearSoftEngineNew(self, engineId, lines, numLines, lapsHandler);
    if (apsEngine == NULL)
        {
        AtLApsEngDelete(lapsHandler);
        return NULL;
        }

    LineListenersRegister(self, lines, numLines);

    return apsEngine;
    }

static uint8 PeriodicProcess(AtModuleAps self, uint32 periodInMs)
    {
	AtUnused(periodInMs);
    if (mLApsModule(self) == NULL)
        return 0;

    return AtLApsPeriodicProcess(mThis(self));
    }

static void OverrideAtModuleAps(AtModuleAps self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleApsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleApsOverride, m_AtModuleApsMethods, sizeof(m_AtModuleApsOverride));

        mMethodOverride(m_AtModuleApsOverride, LinearEngineCreate);
        mMethodOverride(m_AtModuleApsOverride, PeriodicProcess);
        }

    mMethodsSet(self, &m_AtModuleApsOverride);
    }

static void OverrideAtModule(AtModuleAps self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleAps self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleAps self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    OverrideAtModuleAps(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleSoftAps);
    }

AtModuleAps AtModuleSoftApsObjectInit(AtModuleAps self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleApsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleAps AtModuleSoftApsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleAps newApsModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return AtModuleSoftApsObjectInit(newApsModule, device);
    }

AtLApsModule AtModuleSoftApsLApsModuleGet(AtModuleSoftAps self)
    {
    if (self)
        return self->lapsHandler;
    return NULL;
    }

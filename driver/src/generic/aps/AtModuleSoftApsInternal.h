/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtModuleSoftApsInternal.h
 * 
 * Created Date: Jun 6, 2013
 *
 * Description : Soft Linear APS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESOFTAPSINTERNAL_H_
#define _ATMODULESOFTAPSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleApsInternal.h"
#include "AtModuleSoftAps.h"
#include "aps/atlaps.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleSoftAps
    {
    tAtModuleAps super;

    /* Private data */
    AtLApsModule lapsHandler;
    }tAtModuleSoftAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps AtModuleSoftApsObjectInit(AtModuleAps self, AtDevice device);

tAtLApsOsIntf* AtApsOsIntf(void);
AtLApsModule AtModuleSoftApsLApsModuleGet(AtModuleSoftAps self);

#endif /* _ATMODULESOFTAPSINTERNAL_H_ */


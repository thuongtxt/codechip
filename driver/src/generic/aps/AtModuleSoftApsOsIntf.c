/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsOsIntf.c
 *
 * Created Date: Jun 3, 2013
 *
 * Description : OS interface to work with Linear APS software component
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtOsal.h"
#include "aps/atlaps.h"
#include "../../generic/common/AtObjectInternal.h"
#include "AtModuleSoftApsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_osIntfInit = 0;
static tAtLApsOsIntf m_OsIntf;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void* semCreate(void)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtOsalSem sem = mMethodsGet(osal)->SemCreate(osal, cAtFalse, 1);
    return sem;
    }

static int semDelete(void *pSem)
    {
    AtOsalSem sem = (AtOsalSem)pSem;
    if (mMethodsGet(sem)->Delete(sem) == cAtOsalOk)
        return 1;

    return 0;
    }

static int semTake(void *pSem)
    {
    AtOsalSem sem = (AtOsalSem)pSem;
    if(mMethodsGet(sem)->Take(sem) == cAtOsalOk)
        return 1;

    return 0;
    }

static int semGive(void *pSem)
    {
    AtOsalSem sem = (AtOsalSem)pSem;
    if(mMethodsGet(sem)->Give(sem) == cAtOsalOk)
        return 1;

    return 0;
    }

tAtLApsOsIntf* AtApsOsIntf(void)
    {
    if (!m_osIntfInit)
        {
        m_OsIntf.semCreate  = semCreate;
        m_OsIntf.semDelete  = semDelete;
        m_OsIntf.semTake    = semTake;
        m_OsIntf.semGive    = semGive;

        /* Just setup one time */
        m_osIntfInit     = 1;
        }

    return &m_OsIntf;
    }

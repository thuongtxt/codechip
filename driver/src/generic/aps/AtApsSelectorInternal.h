/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsSelectorInternal.h
 * 
 * Created Date: Aug 4, 2016
 *
 * Description : Generic 2-to-1 APS selector.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSSELECTORINTERNAL_H_
#define ATAPSSELECTORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtObjectInternal.h"
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsSelectorMethods
    {
    eAtRet (*Init)(AtApsSelector self);
    eAtRet (*Enable)(AtApsSelector self, eBool enable);
    eBool (*IsEnabled)(AtApsSelector self);
    eAtModuleApsRet (*ChannelSelect)(AtApsSelector self, AtChannel selectedChannel);
    AtChannel (*SelectedChannel)(AtApsSelector self);
    eAtModuleApsRet (*GroupSet)(AtApsSelector self, AtApsGroup group);
    }tAtApsSelectorMethods;

typedef struct tAtApsSelector
    {
    tAtObject   super;
    const tAtApsSelectorMethods *methods;

    /* private data */
    AtModuleAps                 module;
    uint32                      selectorId;
    AtChannel                   working;
    AtChannel                   protection;
    AtChannel                   outgoing;
    AtApsGroup                  group;
    }tAtApsSelector;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps AtApsSelectorModuleGet(AtApsSelector self);
eAtModuleApsRet AtApsSelectorGroupSet(AtApsSelector self, AtApsGroup group);
AtApsGroup AtApsSelectorGroupGet(AtApsSelector self);
AtApsSelector AtApsSelectorObjectInit(AtApsSelector self,
                                      AtModuleAps   module,
                                      uint32        selectorId,
                                      AtChannel     working,
                                      AtChannel     protection,
                                      AtChannel     outgoing);

#ifdef __cplusplus
}
#endif
#endif /* ATAPSSELECTORINTERNAL_H_ */


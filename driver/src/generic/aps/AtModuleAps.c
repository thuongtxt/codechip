/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtModuleAps.c
 *
 * Created Date: Jun 3, 2013
 *
 * Description : APS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtStsGroup.h"
#include "AtModuleApsInternal.h"
#include "../../util/AtIteratorInternal.h"
#include "../../generic/sdh/AtSdhLineInternal.h"
#include "../../generic/sdh/AtSdhPathInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../../generic/util/AtUtil.h"
#include "AtApsSelectorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMinNumLinesInEngine 2

#define mThis(self) (AtModuleAps)self

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleApsMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtModuleMethods * m_AtModuleMethods = NULL;
static const tAtObjectMethods * m_AtObjectMethods = NULL;

/* Array Iterator initialize */
static char m_arrayIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_AtArrayIteratorOverride;
static tAtIteratorMethods      m_AtIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool EngineIdIsValid(AtModuleAps self, uint32 engineId)
    {
    return (engineId >= AtModuleApsMaxNumLinearEngines(self)) ? cAtFalse : cAtTrue;
    }

static eBool UpsrEngineIdIsValid(AtModuleAps self, uint32 engineId)
    {
    return (engineId >= AtModuleApsMaxNumUpsrEngines(self)) ? cAtFalse : cAtTrue;
    }

static eBool SelectorIdIsValid(AtModuleAps self, uint32 selectorId)
    {
    return (selectorId >= AtModuleApsMaxNumSelectors(self)) ? cAtFalse : cAtTrue;
    }

static eBool GroupIdIsValid(AtModuleAps self, uint32 groupId)
    {
    return (groupId >= AtModuleApsMaxNumGroups(self)) ? cAtFalse : cAtTrue;
    }

static eBool StsGroupIdIsValid(AtModuleAps self, uint32 groupId)
    {
    return (groupId >= AtModuleApsMaxNumStsGroups(self)) ? cAtFalse : cAtTrue;
    }

static eBool EngineIsBusy(AtModuleAps self, uint32 engineId)
    {
    return self->linearEngines[engineId] ? cAtTrue : cAtFalse;
    }

static eBool SelectorIsBusy(AtModuleAps self, uint32 selectorId)
    {
    return self->selectors[selectorId] ? cAtTrue : cAtFalse;
    }

static eBool GroupIsBusy(AtModuleAps self, uint32 groupId)
    {
    return self->groups[groupId] ? cAtTrue : cAtFalse;
    }

static eBool StsGroupIsBusy(AtModuleAps self, uint32 groupId)
    {
    return self->stsGroups[groupId] ? cAtTrue : cAtFalse;
    }

static eBool LinearArchValid(eAtApsLinearArch arch)
    {
    return ((arch == cAtApsLinearArch11) || (arch == cAtApsLinearArch1n)) ? cAtTrue : cAtFalse;
    }

static eBool UpsrEngineIsBusy(AtModuleAps self, uint32 engineId)
    {
    return self->upsrEngines[engineId] ? cAtTrue : cAtFalse;
    }

static eBool LinesAreValid(AtModuleAps self, AtSdhLine lines[], uint8 numLines)
    {
    uint8 i;

    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    if ((numLines < cMinNumLinesInEngine) || (numLines > AtModuleSdhMaxLinesGet(sdhModule)))
        return cAtFalse;

    for (i = 0; i < numLines; i++)
        {
        if (lines[i] == NULL)
            return cAtFalse;

        if (AtSdhLineApsEngineGet(lines[i]) != NULL)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool PathsAreValid(AtModuleAps self, AtSdhPath working, AtSdhPath protection)
    {
    AtUnused(self);
    if ((working == NULL) || (protection == NULL))
        return cAtFalse;

    if (!AtSdhChannelIsVc((AtSdhChannel)working) ||
        !AtSdhChannelIsVc((AtSdhChannel)protection))
        return cAtFalse;

    if (AtSdhChannelNumSts((AtSdhChannel)working) != AtSdhChannelNumSts((AtSdhChannel)protection))
        return cAtFalse;

    if (AtSdhPathApsEngineGet(working) != NULL)
        return cAtFalse;

    if (AtSdhPathApsEngineGet(protection) != NULL)
        return cAtFalse;

    return cAtTrue;
    }

static eBool PathCanJoin(AtChannel path)
    {
    if (path == NULL)
        return cAtFalse;

    return cAtTrue;
    }

static eBool LinearParameterIsValid(AtModuleAps self, uint32 engineId, AtSdhLine lines[], uint8 numLines, eAtApsLinearArch arch)
    {
    if (!EngineIdIsValid(self, engineId)      ||
        !LinesAreValid(self, lines, numLines) ||
        EngineIsBusy(self, engineId)          ||
        !LinearArchValid(arch))
        return cAtFalse;
    return cAtTrue;
    }

static eBool UpsrParameterIsValid(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection)
    {
    if (!UpsrEngineIdIsValid(self, engineId)      ||
        !PathsAreValid(self, working, protection) ||
        UpsrEngineIsBusy(self, engineId))
        return cAtFalse;
    return cAtTrue;
    }

static eBool SelectorParameterIsValid(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing)
    {
    if (!SelectorIdIsValid(self, selectorId) || SelectorIsBusy(self, selectorId))
        return cAtFalse;

    if (!PathCanJoin(working) || !PathCanJoin(protection) || !PathCanJoin(outgoing))
        return cAtFalse;

    return cAtTrue;
    }

static void UpsrEngineCacheAndInit(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection, AtApsEngine newEngine)
    {
    AtApsEngineInit(newEngine);
    self->upsrEngines[engineId] = newEngine;
    AtSdhPathApsEngineSet(working, newEngine);
    AtSdhPathApsEngineSet(protection, newEngine);
    }

static void ApsSelectorCacheAndInit(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing, AtApsSelector newSelector)
    {
    AtApsSelectorInit(newSelector);
    self->selectors[selectorId] = newSelector;

    AtSdhPathApsSelectorJoin((AtSdhPath)working, newSelector);
    AtSdhPathApsSelectorJoin((AtSdhPath)protection, newSelector);
    AtSdhPathApsSelectorSet((AtSdhPath)outgoing, newSelector);
    }

static AtApsEngine LinearEngineCreate(AtModuleAps self,
                                      uint32 engineId,
                                      AtSdhLine lines[],
                                      uint8 numLines,
                                      eAtApsLinearArch arch)
    {
	AtUnused(arch);
	AtUnused(numLines);
	AtUnused(lines);
	AtUnused(engineId);
	AtUnused(self);
    return NULL;
    }

static eAtModuleApsRet LinearEngineDelete(AtModuleAps self, uint32 engineId)
    {
    AtApsEngine engine;

    if (self->linearEngines == NULL)
       return cAtOk;

    if (!EngineIdIsValid(self, engineId))
        return cAtError;

    engine = AtModuleApsLinearEngineGet(self, engineId);
    if (engine == NULL)
        return cAtOk;

    AtObjectDelete((AtObject)engine);
    self->linearEngines[engineId] = NULL;

    return cAtOk;
    }

static uint32 MaxNumLinearEngines(AtModuleAps self)
    {
    return self->maxNumLinearEngines;
    }

static AtApsEngine UpsrEngineCreate(AtModuleAps self,
                                      uint32 engineId,
									  AtSdhPath working,
									  AtSdhPath protection)
    {
	AtUnused(working);
	AtUnused(protection);
	AtUnused(engineId);
	AtUnused(self);
    return NULL;
    }

static eAtModuleApsRet UpsrEngineDelete(AtModuleAps self, uint32 engineId)
    {
    AtApsEngine engine = AtModuleApsUpsrEngineGet(self, engineId);
    AtSdhPathApsEngineSet(AtApsUpsrEngineWorkingPathGet((AtApsUpsrEngine)engine), NULL);
    AtSdhPathApsEngineSet(AtApsUpsrEngineProtectionPathGet((AtApsUpsrEngine)engine), NULL);

    AtObjectDelete((AtObject)engine);
    self->upsrEngines[engineId] = NULL;

    return cAtOk;
    }

static uint32 MaxNumUpsrEngines(AtModuleAps self)
    {
    return self->maxNumUpsrEngines;
    }

static uint8 PeriodicProcess(AtModuleAps self, uint32 periodInMs)
    {
	AtUnused(periodInMs);
	AtUnused(self);

    /* Sub class will do */
    return 0;
    }

static uint32 MaxNumSelectors(AtModuleAps self)
    {
    AtUnused(self);
    return 0;
    }

static AtApsSelector SelectorCreate(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing)
    {
    AtUnused(self);
    AtUnused(selectorId);
    AtUnused(working);
    AtUnused(protection);
    AtUnused(outgoing);
    return NULL;
    }

static eAtModuleApsRet SelectorDelete(AtModuleAps self, uint32 selectorId)
    {
    AtApsSelector selector = AtModuleApsSelectorGet(self, selectorId);

    AtSdhPathApsSelectorLeave((AtSdhPath)AtApsSelectorWorkingChannelGet(selector), selector);
    AtSdhPathApsSelectorLeave((AtSdhPath)AtApsSelectorProtectionChannelGet(selector), selector);
    AtSdhPathApsSelectorSet((AtSdhPath)AtApsSelectorOutgoingChannelGet(selector), NULL);

    AtObjectDelete((AtObject)selector);
    self->selectors[selectorId] = NULL;

    return cAtOk;
    }

static uint32 MaxNumGroups(AtModuleAps self)
    {
    AtUnused(self);
    return 0;
    }

static AtApsGroup GroupCreate(AtModuleAps self, uint32 groupId)
    {
    AtUnused(self);
    AtUnused(groupId);
    return NULL;
    }

static eAtRet GroupDelete(AtModuleAps self, uint32 groupId)
    {
    AtApsGroup group;

    group = AtModuleApsGroupGet(self, groupId);
    if (group == NULL)
        return cAtOk;

    if (AtApsGroupNumSelectors(group) > 0)
        return cAtErrorChannelBusy;

    AtObjectDelete((AtObject)group);
    self->groups[groupId] = NULL;

    return cAtOk;
    }

static uint32 MaxNumStsGroups(AtModuleAps self)
    {
    AtUnused(self);
    return 0;
    }

static AtStsGroup StsGroupCreate(AtModuleAps self, uint32 groupId)
    {
    AtUnused(self);
    AtUnused(groupId);
    return NULL;
    }

static eAtRet StsGroupDelete(AtModuleAps self, uint32 groupId)
    {
    AtStsGroup group;

    group = AtModuleApsStsGroupGet(self, groupId);
    if (group == NULL)
        return cAtOk;

    if (AtStsGroupNumSts(group) > 0)
        return cAtErrorChannelBusy;

    AtObjectDelete((AtObject)group);
    self->stsGroups[groupId] = NULL;

    return cAtOk;
    }

static void MethodsInit(AtModuleAps self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_methods, LinearEngineCreate);
        mMethodOverride(m_methods, LinearEngineDelete);
        mMethodOverride(m_methods, MaxNumLinearEngines);
        mMethodOverride(m_methods, PeriodicProcess);
        mMethodOverride(m_methods, UpsrEngineCreate);
        mMethodOverride(m_methods, UpsrEngineDelete);
        mMethodOverride(m_methods, MaxNumUpsrEngines);
        mMethodOverride(m_methods, MaxNumSelectors);
        mMethodOverride(m_methods, SelectorCreate);
        mMethodOverride(m_methods, SelectorDelete);
        mMethodOverride(m_methods, MaxNumGroups);
        mMethodOverride(m_methods, GroupCreate);
        mMethodOverride(m_methods, GroupDelete);
        mMethodOverride(m_methods, MaxNumStsGroups);
        mMethodOverride(m_methods, StsGroupCreate);
        mMethodOverride(m_methods, StsGroupDelete);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 DefaultMaxNumEngines(AtModuleAps self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    const uint8 cMinNumLinesPerEngine = 2;
    return AtModuleSdhMaxLinesGet(sdhModule) / cMinNumLinesPerEngine;
    }

static eAtRet ApsGroupSetup(AtModuleAps moduleAps)
    {
    AtOsal osal;
    uint32 memorySize;
    uint32 maxNumGroups;
    maxNumGroups = AtModuleApsMaxNumGroups(moduleAps);

    /* Already setup */
    if (moduleAps->groups != NULL)
        return cAtOk;

    if (maxNumGroups == 0)
        return cAtOk;

    /* Allocate memory to hold all of engines */
    osal = AtSharedDriverOsalGet();
    memorySize = sizeof(AtApsGroup) * maxNumGroups;
    moduleAps->groups = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (moduleAps->groups == NULL)
        return cAtErrorRsrcNoAvail ;

    /* Save it */
    mMethodsGet(osal)->MemInit(osal, moduleAps->groups, 0, memorySize);
    moduleAps->maxNumGroups = maxNumGroups;
    return cAtOk;
    }

static eAtRet ApsSelectorSetup(AtModuleAps moduleAps)
    {
    AtOsal osal;
    uint32 memorySize;
    uint32 maxNumSelectors;
    maxNumSelectors = AtModuleApsMaxNumSelectors(moduleAps);

    /* Already setup */
    if (moduleAps->selectors != NULL)
        return cAtOk;

    if (maxNumSelectors == 0)
        return cAtOk;

    /* Allocate memory to hold all of engines */
    osal = AtSharedDriverOsalGet();
    memorySize = sizeof(AtApsSelector) * maxNumSelectors;
    moduleAps->selectors = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (moduleAps->selectors == NULL)
        return cAtErrorRsrcNoAvail;

    /* Save it */
    mMethodsGet(osal)->MemInit(osal, moduleAps->selectors, 0, memorySize);
    moduleAps->maxNumSelectors   = maxNumSelectors;

    return cAtOk;
    }

static eAtRet ApsUpsrEngineSetup(AtModuleAps moduleAps)
    {
    AtOsal osal;
    uint32 memorySize;
    uint32 maxNumUpsrEngines;
    maxNumUpsrEngines = AtModuleApsMaxNumUpsrEngines(moduleAps);

    /* Already setup */
    if (moduleAps->upsrEngines != NULL)
        return cAtOk;

    if (maxNumUpsrEngines == 0)
        return cAtOk;

    /* Allocate memory to hold all of engines */
    osal = AtSharedDriverOsalGet();
    memorySize = sizeof(AtApsEngine) * maxNumUpsrEngines;
    moduleAps->upsrEngines = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (moduleAps->upsrEngines == NULL)
        return cAtErrorRsrcNoAvail;

    /* Save it */
    mMethodsGet(osal)->MemInit(osal, moduleAps->upsrEngines, 0, memorySize);
    moduleAps->maxNumUpsrEngines = maxNumUpsrEngines;
    return cAtOk;
    }

static eAtRet ApsLinearEngineSetup(AtModuleAps moduleAps)
    {
    AtOsal osal;
    uint32 maxNumEngines;
    uint32 memorySize;
    AtApsEngine *allEngines;

    /* Already setup */
    if (moduleAps->linearEngines != NULL)
       return cAtOk;

    /* Allocate memory to hold all of engines */
    osal = AtSharedDriverOsalGet();
    maxNumEngines = DefaultMaxNumEngines(moduleAps);
    memorySize = sizeof(AtApsEngine) * maxNumEngines;
    allEngines = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allEngines == NULL)
        return cAtErrorRsrcNoAvail;

    /* Save it */
    mMethodsGet(osal)->MemInit(osal, allEngines, 0, memorySize);
    moduleAps->linearEngines = allEngines;
    moduleAps->maxNumLinearEngines = maxNumEngines;
    return cAtOk;
    }

static eAtRet ApsStsGroupSetup(AtModuleAps moduleAps)
    {
    AtOsal osal;
    uint32 memorySize;
    uint32 maxNumStsGroups;
    maxNumStsGroups = AtModuleApsMaxNumStsGroups(moduleAps);

    /* Already setup */
    if (moduleAps->stsGroups != NULL)
        return cAtOk;

    if (maxNumStsGroups == 0)
        return cAtOk;

    /* Allocate memory to hold all of engines */
    osal = AtSharedDriverOsalGet();
    memorySize = sizeof(AtApsGroup) * maxNumStsGroups;
    moduleAps->stsGroups = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (moduleAps->stsGroups == NULL)
        return cAtErrorRsrcNoAvail ;

    /* Save it */
    mMethodsGet(osal)->MemInit(osal, moduleAps->stsGroups, 0, memorySize);
    moduleAps->maxNumStsGroups = maxNumStsGroups;
    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;

    /* Super job */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Delete first */
    AtApsModuleAllEnginesDelete((AtModuleAps)self);

    ret = ApsGroupSetup((AtModuleAps)self);
    if (ret != cAtOk)
        return ret;

    ret = ApsSelectorSetup((AtModuleAps)self);
    if (ret != cAtOk)
        return ret;

    ret = ApsUpsrEngineSetup((AtModuleAps)self);
    if (ret != cAtOk)
        return ret;

    ret = ApsLinearEngineSetup((AtModuleAps)self);
    if (ret != cAtOk)
        return ret;

    ret = ApsStsGroupSetup((AtModuleAps)self);
    if (ret != cAtOk)
        return ret;

    return cAtOk;
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "aps";
    }

static const char *CapacityDescription(AtModule self)
    {
    static char string[128];
    AtSnprintf(string, sizeof(string), "linears: %d, upsr: %d, groups: %d, selectors: %d",
              AtModuleApsMaxNumLinearEngines((AtModuleAps)self),
              AtModuleApsMaxNumUpsrEngines((AtModuleAps)self),
              AtModuleApsMaxNumGroups((AtModuleAps)self),
              AtModuleApsMaxNumSelectors((AtModuleAps)self));
    return string;
    }

static void OverrideAtModule(AtModuleAps self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void ApsGroupDelete(AtModuleAps self)
    {
    AtIterator iterator;
    AtObject object;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->groups == NULL)
        return;

    iterator = AtModuleApsGroupIteratorCreate(self);
    while((object = (AtObject)AtIteratorNext(iterator)) != NULL)
        AtObjectDelete(object);

    AtObjectDelete((AtObject)iterator);
    mMethodsGet(osal)->MemFree(osal, self->groups);
    self->groups = NULL;
    }

static void ApsSelectorDelete(AtModuleAps self)
    {
    AtIterator iterator;
    AtObject object;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->selectors == NULL)
        return;

    iterator = AtModuleApsSelectorIteratorCreate(self);
    while((object = (AtObject)AtIteratorNext(iterator)) != NULL)
        AtObjectDelete(object);

    AtObjectDelete((AtObject)iterator);
    mMethodsGet(osal)->MemFree(osal, self->selectors);
    self->selectors = NULL;
    }

static void ApsUpsrEngineDelete(AtModuleAps self)
    {
    AtIterator iterator;
    AtObject object;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->upsrEngines == NULL)
        return;

    iterator = AtModuleApsUpsrEngineIteratorCreate(self);
    while((object = (AtObject)AtIteratorNext(iterator)) != NULL)
        AtObjectDelete(object);

    AtObjectDelete((AtObject)iterator);
    mMethodsGet(osal)->MemFree(osal, self->upsrEngines);
    self->upsrEngines = NULL;
    }

static void ApsLinearEngineDelete(AtModuleAps self)
    {
    AtIterator iterator;
    AtObject object;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->linearEngines == NULL)
        return;

    iterator = AtModuleApsLinearEngineIteratorCreate(self);
    while((object = (AtObject)AtIteratorNext(iterator)) != NULL)
        AtObjectDelete(object);

    AtObjectDelete((AtObject)iterator);
    mMethodsGet(osal)->MemFree(osal, self->linearEngines);
    self->linearEngines = NULL;
    }

static void ApsStsGroupDelete(AtModuleAps self)
    {
    AtIterator iterator;
    AtObject object;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->stsGroups == NULL)
        return;

    iterator = AtModuleApsStsGroupIteratorCreate(self);
    while((object = (AtObject)AtIteratorNext(iterator)) != NULL)
        AtObjectDelete(object);

    AtObjectDelete((AtObject)iterator);
    mMethodsGet(osal)->MemFree(osal, self->stsGroups);
    self->stsGroups = NULL;
    }

void AtApsModuleAllEnginesDelete(AtModuleAps self)
    {
    ApsGroupDelete(self);
    ApsSelectorDelete(self);
    ApsUpsrEngineDelete(self);
    ApsLinearEngineDelete(self);
    ApsStsGroupDelete(self);
    }

static void Delete(AtObject self)
    {
    AtApsModuleAllEnginesDelete((AtModuleAps)self);

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleAps object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(linearEngines, object->maxNumLinearEngines);
    mEncodeObjects(upsrEngines, object->maxNumUpsrEngines);
    mEncodeObjects(selectors, object->maxNumSelectors);
    mEncodeObjects(groups, object->maxNumGroups);
    mEncodeObjects(stsGroups, object->maxNumStsGroups);
    }

static void OverrideAtObject(AtModuleAps self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleAps self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static eBool DataAtIndexIsValid(AtArrayIterator self, uint32 channelIndex)
    {
    AtObject *engines = self->array;
    return ((engines[channelIndex]) != NULL);
    }

static AtObject DataAtIndex(AtArrayIterator self, uint32 channelIndex)
    {
    AtObject *engines = self->array;
    return engines[channelIndex];
    }

static uint16 EngineCountGet(AtArrayIterator iterator)
    {
    uint16 count = 0;
    uint16 i;
    AtObject *engines = iterator->array;
    if (engines == NULL)
        return 0;

    for (i = 0; i < iterator->capacity; i++)
        if (engines[i])
            count++;
    return count;
    }

static uint32 Count(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;

    arrayIterator->count = (int32)EngineCountGet(arrayIterator);
    return (uint32)arrayIterator->count;
    }

static AtApsEngine DoLinearEngineCreate(AtModuleAps self, uint32 engineId, AtSdhLine lines[], uint8 numLines, eAtApsLinearArch arch)
    {
    AtApsEngine newEngine;

    if (self == NULL)
        return NULL;

    if (LinearParameterIsValid(self, engineId, lines, numLines, arch) == cAtFalse)
        return NULL;

    /* Create new one */
    newEngine = mMethodsGet(self)->LinearEngineCreate(self, engineId, lines, numLines, arch);
    if (newEngine == NULL)
        return NULL;

    /* Initialize and cache it */
    AtChannelInit((AtChannel)newEngine);
    self->linearEngines[engineId] = newEngine;

    return newEngine;
    }

static AtApsEngine DoUpsrEngineCreate(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection)
    {
    AtApsEngine newEngine;

    if (self == NULL)
        return NULL;

    if (UpsrParameterIsValid(self, engineId, working, protection) == cAtFalse)
        return NULL;

    /* Create new one */
    newEngine = mMethodsGet(self)->UpsrEngineCreate(self, engineId, working, protection);
    if (newEngine == NULL)
        return NULL;

    /* Initialize and cache it */
    UpsrEngineCacheAndInit(self, engineId, working, protection, newEngine);

    return newEngine;
    }

static AtApsSelector DoSelectorCreate(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing)
    {
    AtApsSelector newSelector;
    if (self == NULL)
        return NULL;

    if (SelectorParameterIsValid(self, selectorId, working, protection, outgoing) == cAtFalse)
        return NULL;

    if (AtSdhPathApsSelectorGet((AtSdhPath)outgoing) != NULL)
        return NULL;

    newSelector = mMethodsGet(self)->SelectorCreate(self, selectorId, working, protection, outgoing);
    if (newSelector == NULL)
        return NULL;

    /* Initialize and cache it */
    ApsSelectorCacheAndInit(self, selectorId, working, protection, outgoing, newSelector);

    return newSelector;
    }

static AtApsGroup DoGroupCreate(AtModuleAps self, uint32 groupId)
    {
    AtApsGroup newGroup;

    if (self == NULL)
        return NULL;

    if (!GroupIdIsValid(self, groupId) || GroupIsBusy(self, groupId))
        return NULL;

    newGroup = mMethodsGet(self)->GroupCreate(self, groupId);
    if (newGroup == NULL)
        return NULL;

    /* Initialize and cache it */
    AtApsGroupInit(newGroup);
    self->groups[groupId] = newGroup;

    return newGroup;
    }

static AtStsGroup DoStsGroupCreate(AtModuleAps self, uint32 groupId)
    {
    AtStsGroup newStsGroup;

    if (self == NULL)
        return NULL;

    if (!StsGroupIdIsValid(self, groupId) || StsGroupIsBusy(self, groupId))
        return NULL;

    newStsGroup = mMethodsGet(self)->StsGroupCreate(self, groupId);
    if (newStsGroup == NULL)
        return NULL;

    /* Cache it */
    self->stsGroups[groupId] = newStsGroup;

    return newStsGroup;
    }

static void LinearEngineCreateApiLogStart(AtModuleAps self, uint32 engineId, AtSdhLine lines[], uint8 numLines, eAtApsLinearArch arch,
                                          const char *file, uint32 line, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize;
        char *buffer, *lineString;

        AtDriverLogLock();
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);
        lineString = AtObjectArrayToString((AtObject *)lines, numLines, buffer, bufferSize);
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                        file, line, "API: %s([%s], %u, %s, %u, %u)\r\n",
                                        function, AtObjectToString((AtObject)self), engineId, lineString, numLines, arch);
        AtDriverLogUnLock();
        }
    }

static void UpsrEngineCreateApiLogStart(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection,
                                        const char *file, uint32 line, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize;
        char *buffer, *workingStr, *protectionStr;

        AtDriverLogLock();

        /* Build object descriptions */
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);
        workingStr = buffer;
        protectionStr = buffer + (bufferSize / 2);
        AtSnprintf(workingStr, (bufferSize / 2), "%s", AtObjectToString((AtObject)working));
        AtSnprintf(protectionStr, (bufferSize / 2), "%s", AtObjectToString((AtObject)protection));

        /* Log the call */
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                                file, line, "API: %s([%s], %u, %s, %s)\r\n",
                                                function,
                                                AtObjectToString((AtObject)self), engineId, workingStr, protectionStr);
        AtDriverLogUnLock();
        }
    }

static void SelectorCreateApiLogStart(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing,
                                      const char *file, uint32 line, const char *function)
    {
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize, channelBufferSize;
        char *buffer, *workingStr, *protectionStr, *outgoingStr;

        AtDriverLogLock();

        /* Build object descriptions */
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);
        channelBufferSize = bufferSize / 3;
        workingStr = buffer;
        protectionStr = buffer + channelBufferSize;
        outgoingStr = protectionStr + channelBufferSize;
        AtSnprintf(workingStr, channelBufferSize, "%s", AtObjectToString((AtObject)working));
        AtSnprintf(protectionStr, channelBufferSize, "%s", AtObjectToString((AtObject)protection));
        AtSnprintf(outgoingStr, channelBufferSize, "%s", AtObjectToString((AtObject)outgoing));

        /* Log the call */
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                                file, line, "API: %s([%s], %u, %s, %s, %s)\r\n",
                                                function,
                                                AtObjectToString((AtObject)self), selectorId,
                                                workingStr, protectionStr, outgoingStr);
        AtDriverLogUnLock();
        }
    }

static eAtRet DoUpsrEngineDelete(AtModuleAps self, uint32 engineId)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (!UpsrEngineIdIsValid(self, engineId))
        return cAtErrorOutOfRangParm;

    if ((self->upsrEngines == NULL) || (self->upsrEngines[engineId] == NULL))
        return cAtOk;

    AtModuleLock((AtModule)self);
    ret = mMethodsGet(self)->UpsrEngineDelete(self, engineId);
    AtModuleUnLock((AtModule)self);

    return ret;
    }

static eAtRet DoApsSelectorDelete(AtModuleAps self, uint32 selectorId)
    {
    if (self == NULL)
       return cAtErrorObjectNotExist;

    if (!SelectorIdIsValid(self, selectorId))
        return cAtErrorOutOfRangParm;

    if ((self->selectors == NULL) || (self->selectors[selectorId] == NULL))
       return cAtOk;

    if (AtApsSelectorGroupGet(self->selectors[selectorId]) != NULL)
        return cAtErrorChannelBusy;

    return mMethodsGet(self)->SelectorDelete(self, selectorId);
    }

static void OverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_arrayIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtArrayIteratorOverride, mMethodsGet(self), sizeof(m_AtArrayIteratorOverride));
        mMethodOverride(m_AtArrayIteratorOverride, DataAtIndexIsValid);
        mMethodOverride(m_AtArrayIteratorOverride, DataAtIndex);
        }
    mMethodsSet(self, &m_AtArrayIteratorOverride);
    }

static void OverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_arrayIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(iterator), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, Count);
        }
    mMethodsSet(iterator, &m_AtIteratorOverride);
    }

static void IteratorOverride(AtArrayIterator self)
    {
    OverrideAtArrayIterator(self);
    OverrideAtIterator(self);
    }

static AtArrayIterator AtArrayIteratorInit(void *array, uint32 capacity)
    {
    AtArrayIterator iterator;
    iterator = AtArrayIteratorNew(array, capacity);
    if (iterator == NULL)
        return NULL;

    /* Override */
    IteratorOverride(iterator);
    m_arrayIteratorMethodsInit = 1;

    return iterator;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleAps);
    }

AtModuleAps AtModuleApsObjectInit(AtModuleAps self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleAps, device) == NULL)
        return NULL;

    /* Initialize implementation */
    MethodsInit(self);

    /* Override */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtModuleAps
 * @{
 */

/**
 * Get maximum number of Linear APS engines that this module can support
 *
 * @param self This module
 *
 * @return Number of Linear APS engine
 */
uint32 AtModuleApsMaxNumLinearEngines(AtModuleAps self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumLinearEngines(self);
    return 0;
    }

/**
 * Get engine by engine's ID
 *
 * @param self This module
 * @param engineId Engine ID
 *
 * @return Engine, return NULL when engine with this ID is not existing
 */
AtApsEngine AtModuleApsLinearEngineGet(AtModuleAps self, uint32 engineId)
    {
    if (!EngineIdIsValid(self, engineId))
        return NULL;

    return self->linearEngines[engineId];
    }

/**
 * Create Linear APS engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param lines List of lines. One engine can have maximum 16 lines. Channel 0
 *              is used for protection line and remained indexes are used for
 *              working lines
 * @param numLines Number of SDH line that new engine will take-over
 * @param arch @ref eAtApsLinearArch "APS architecture"
 *
 * @return APS engine, engine ID is embedded inside this engine and can be retrieved
 *         by using API "AtApsEngineIdGet"
 */
AtApsEngine AtModuleApsLinearEngineCreate(AtModuleAps self, uint32 engineId, AtSdhLine lines[], uint8 numLines, eAtApsLinearArch arch)
    {
    AtApsEngine newEngine;

    LinearEngineCreateApiLogStart(self, engineId, lines, numLines, arch, AtSourceLocationNone, AtFunction);
    newEngine = DoLinearEngineCreate(self, engineId, lines, numLines, arch);
    AtDriverApiLogStop();

    return newEngine;
    }

/**
 * Delete APS engine
 *
 * @param self This module
 * @param engineId Engine ID, each created Engine has it's own ID and can be retrieved by API "AtChannelIdGet"
 * @return Module APS return code
 */
eAtModuleApsRet AtModuleApsLinearEngineDelete(AtModuleAps self, uint32 engineId)
    {
    mNumericalAttributeSet(LinearEngineDelete, engineId);
    }

/**
 * Create Engine iterator
 *
 * @param self This module
 * @return AtIterator
 */
AtIterator AtModuleApsLinearEngineIteratorCreate(AtModuleAps self)
    {
    uint32 maxNumEngines;

    if (self == NULL)
        return NULL;

    maxNumEngines = AtModuleApsMaxNumLinearEngines(self);
    return (AtIterator)AtArrayIteratorInit(self->linearEngines, maxNumEngines);
    }

/**
 * Create UPSR Engine iterator
 *
 * @param self This module
 * @return AtIterator
 */
AtIterator AtModuleApsUpsrEngineIteratorCreate(AtModuleAps self)
    {
    uint32 maxNumEngines;

    if (self == NULL)
        return NULL;

    maxNumEngines = AtModuleApsMaxNumUpsrEngines(self);
    return (AtIterator)AtArrayIteratorInit(self->upsrEngines, maxNumEngines);
    }

/**
 * Periodic processing function. This function should be called by the application
 * periodically so that WTR can be processed and L-APS defects and failures can
 * be reported or it can help this module perform switching/bridging action on
 * K-byte failure.
 *
 * @param self This Module
 * @param periodInMs Period that this function is called.
 *
 * @retval Number of lines need to be processed.
 * @retval 0 if nothing to do
 */
uint8 AtModuleApsPeriodicProcess(AtModuleAps self, uint32 periodInMs)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->PeriodicProcess(self, periodInMs);
    }

/**
 * Get maximum number of UPSR engines
 *
 * @param self This module
 * @return Maximum number of UPSR engines
 */
uint32 AtModuleApsMaxNumUpsrEngines(AtModuleAps self)
    {
	if (self)
		return mMethodsGet(self)->MaxNumUpsrEngines(self);
	return 0;
    }

/**
 * Create UPSR engine
 *
 * @param self This module
 * @param engineId Engine ID
 * @param working Working path
 * @param protection Protection path
 *
 * @return UPSR engine on success. Otherwise, NULL is returned.
 */
AtApsEngine AtModuleApsUpsrEngineCreate(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection)
    {
    AtApsEngine newEngine;

    UpsrEngineCreateApiLogStart(self, engineId, working, protection, AtSourceLocationNone, AtFunction);
    newEngine = DoUpsrEngineCreate(self, engineId, working, protection);
    AtDriverApiLogStop();

	return newEngine;
    }

/**
 * Delete an engine
 *
 * @param self This module
 * @param engineId Engine ID to delete
 *
 * @return AT return code
 */
eAtModuleApsRet AtModuleApsUpsrEngineDelete(AtModuleAps self, uint32 engineId)
    {
    eAtRet ret;

    mOneParamApiLogStart(engineId);
    ret = DoUpsrEngineDelete(self, engineId);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get Upsr engine by engine's ID
 *
 * @param self This module
 * @param engineId Engine ID
 *
 * @return Engine, return NULL when engine with this ID is not existing
 */
AtApsEngine AtModuleApsUpsrEngineGet(AtModuleAps self, uint32 engineId)
    {
    if (UpsrEngineIdIsValid(self, engineId))
        return self->upsrEngines[engineId];
    return NULL;
    }

/**
 * Get Max number of supported selectors
 *
 * @param self This module
 *
 * @return Max number of supported selectors
 */
uint32 AtModuleApsMaxNumSelectors(AtModuleAps self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumSelectors(self);
    return 0;
    }

/**
 * Creating an APS selector
 *
 * @param self This module
 * @param selectorId ID of created APS selector
 * @param working working path
 * @param protection protection path
 * @param outgoing outgoing path
 *
 * @return Max number of supported selectors
 */
AtApsSelector AtModuleApsSelectorCreate(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing)
    {
    AtApsSelector newSelector;

    SelectorCreateApiLogStart(self, selectorId, working, protection, outgoing, AtSourceLocationNone, AtFunction);
    newSelector = DoSelectorCreate(self, selectorId, working, protection, outgoing);
    AtDriverApiLogStop();

    return newSelector;
    }

/**
 * Deleting an APS selector by selector ID
 *
 * @param self This module
 * @param selectorId ID of the selector needed to be deleted
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtModuleApsSelectorDelete(AtModuleAps self, uint32 selectorId)
    {
    eAtRet ret;

    mOneParamApiLogStart(selectorId);
    ret = DoApsSelectorDelete(self, selectorId);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get an APS selector object by selector ID
 *
 * @param self This module
 * @param selectorId ID of the selector
 *
 * @return APS Selector Object
 */
AtApsSelector AtModuleApsSelectorGet(AtModuleAps self, uint32 selectorId)
    {
    if (SelectorIdIsValid(self, selectorId))
        return self->selectors[selectorId];
    return NULL;
    }

/**
 * Creating an APS selector iterator managed by APS Module
 *
 * @param self This module
 *
 * @return APS Selector selector Object
 */
AtIterator AtModuleApsSelectorIteratorCreate(AtModuleAps self)
    {
    uint32 maxNumSelectors;

    if (self == NULL)
        return NULL;

    maxNumSelectors = AtModuleApsMaxNumSelectors(self);
    return (AtIterator)AtArrayIteratorInit(self->selectors, maxNumSelectors);
    }

/**
 * Get Max number of supported APS group
 *
 * @param self This module
 *
 * @return APS group Object
 */
uint32 AtModuleApsMaxNumGroups(AtModuleAps self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumGroups(self);
    return 0;
    }

/**
 * Creating an APS group
 *
 * @param self This module
 * @param groupId group ID of created APS group object
 *
 * @return APS group Object
 */
AtApsGroup AtModuleApsGroupCreate(AtModuleAps self, uint32 groupId)
    {
    mOneParamWrapCall(groupId, AtApsGroup, DoGroupCreate(self, groupId));
    }

/**
 * Deleting an APS group
 *
 * @param self This module
 * @param groupId group ID of the APS group object need to be deleted
 *
 * @return Device return code
 */
eAtRet AtModuleApsGroupDelete(AtModuleAps self, uint32 groupId)
    {
    if (self == NULL)
       return cAtErrorObjectNotExist;

    if (GroupIdIsValid(self, groupId))
        {
        mNumericalAttributeSet(GroupDelete, groupId);
        }

    return cAtErrorOutOfRangParm;
    }

/**
 * Get an APS group
 *
 * @param self This module
 * @param groupId group ID of the APS group object need to be gotten
 *
 * @return APS Group Object
 */
AtApsGroup AtModuleApsGroupGet(AtModuleAps self, uint32 groupId)
    {
    if (GroupIdIsValid(self, groupId))
        return self->groups[groupId];
    return NULL;
    }

/**
 * Creating an APS Group iterator managed by APS Module
 *
 * @param self This module
 *
 * @return APS Group Object
 */
AtIterator AtModuleApsGroupIteratorCreate(AtModuleAps self)
    {
    uint32 maxNumGroups;

    if (self == NULL)
        return NULL;

    maxNumGroups = AtModuleApsMaxNumGroups(self);
    return (AtIterator)AtArrayIteratorInit(self->groups, maxNumGroups);
    }

/**
 * Get maximum number of STS Pass-through Groups
 *
 * @param self This module
 *
 * @return Number of STS Pass-through groups
 */
uint32 AtModuleApsMaxNumStsGroups(AtModuleAps self)
    {
    if (self)
        return mMethodsGet(self)->MaxNumStsGroups(self);
    return 0;
    }

/**
 * Create a STS Pass-through Group
 *
 * @param self This module
 * @param groupId group ID of STS Pass-through Group
 *
 * @return STS Pass-through Group Object
 */
AtStsGroup AtModuleApsStsGroupCreate(AtModuleAps self, uint32 groupId)
    {
    mOneParamWrapCall(groupId, AtStsGroup, DoStsGroupCreate(self, groupId));
    }

/**
 * Delete a STS Pass-through Group
 *
 * @param self This module
 * @param groupId group ID of STS Pass-through Group
 *
 * @return Module APS return code
 */
eAtRet AtModuleApsStsGroupDelete(AtModuleAps self, uint32 groupId)
    {
    if (self == NULL)
       return cAtErrorObjectNotExist;

    if (StsGroupIdIsValid(self, groupId))
        {
        mNumericalAttributeSet(StsGroupDelete, groupId);
        }

    return cAtErrorOutOfRangParm;
    }

/**
 * Get a STS Pass-through Group object
 *
 * @param self This module
 * @param groupId group ID of STS Pass-through Group
 *
 * @return STS Pass-through Group object
 */
AtStsGroup AtModuleApsStsGroupGet(AtModuleAps self, uint32 groupId)
    {
    if (self && StsGroupIdIsValid(self, groupId))
        return self->stsGroups[groupId];

    return NULL;
    }

/**
 * Creating an STS Pass-through Group iterator managed by APS Module
 *
 * @param self This module
 *
 * @return AtIterator Object
 */
AtIterator AtModuleApsStsGroupIteratorCreate(AtModuleAps self)
    {
    uint32 maxNumGroups;

    if (self == NULL)
        return NULL;

    maxNumGroups = AtModuleApsMaxNumStsGroups(self);
    return (AtIterator)AtArrayIteratorInit(self->stsGroups, maxNumGroups);
    }

/**
 * @}
 */

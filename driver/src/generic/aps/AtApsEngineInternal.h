/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsEngineInternal.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : Abstract APS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSENGINEINTERNAL_H_
#define _ATAPSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelInternal.h"
#include "AtApsEngine.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtApsEngineMethods
    {
    eAtApsEngineState (*StateGet)(AtApsEngine self);

    /* Switching time */
    eAtModuleApsRet (*SwitchTypeSet)(AtApsEngine self, eAtApsSwitchType swType);
    eAtApsSwitchType (*SwitchTypeGet)(AtApsEngine self);

    /* Switching condition */
    eAtModuleApsRet (*SwitchingConditionSet)(AtApsEngine self, uint32 defects, eAtApsSwitchingCondition condition);
    eAtApsSwitchingCondition (*SwitchingConditionGet)(AtApsEngine self, uint32 defect);

    /* Local request state */
    eAtApsRequestState (*RequestStateGet)(AtApsEngine self);

    /* Wait-To-Restore */
    uint32 (*WtrGet)(AtApsEngine self);
    eAtModuleApsRet (*WtrSet)(AtApsEngine self, uint32 wtrInSec);
    }tAtApsEngineMethods;

typedef struct tAtApsEngine
    {
    tAtChannel super;
    const tAtApsEngineMethods *methods;
    }tAtApsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtApsEngine AtApsEngineObjectInit(AtApsEngine self, AtModuleAps module, uint32 engineId);

#endif /* _ATAPSENGINEINTERNAL_H_ */


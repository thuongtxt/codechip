/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsGroupInternal.h
 * 
 * Created Date: Aug 4, 2016
 *
 * Description : Generic APS group interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATAPSGROUPINTERNAL_H_
#define ATAPSGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h"
#include "AtApsGroup.h"
#include "../common/AtObjectInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/* Methods */
typedef struct tAtApsGroupMethods
    {
    eAtModuleApsRet (*Init)(AtApsGroup self);
    eAtModuleApsRet (*SelectorAdd)(AtApsGroup self, AtApsSelector selector);
    eAtModuleApsRet (*SelectorRemove)(AtApsGroup self, AtApsSelector selector);
    uint32 (*NumSelectors)(AtApsGroup self);
    AtApsSelector (*SelectorAtIndex)(AtApsGroup self, uint32 selectorIndex);
    eAtModuleApsRet (*StateSet)(AtApsGroup self, eAtApsGroupState state);
    eAtApsGroupState (*StateGet)(AtApsGroup self);
    }tAtApsGroupMethods;

typedef struct tAtApsGroup
    {
    tAtObject super;
    const tAtApsGroupMethods *methods;

    /* private data */
    AtModuleAps module;
    uint32 groupId;
    AtList selectorList;
    }tAtApsGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps AtApsGroupModuleGet(AtApsGroup self);
AtApsGroup AtApsGroupObjectInit(AtApsGroup self, AtModuleAps module, uint32 groupId);

#ifdef __cplusplus
}
#endif
#endif /* ATAPSGROUPINTERNAL_H_ */


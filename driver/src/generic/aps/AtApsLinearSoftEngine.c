/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : ThaApsEngine.c
 *
 * Created Date: Jun 6, 2013
 *
 * Description : Linear APS software engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtApsLinearSoftEngineInternal.h"
#include "AtModuleSoftApsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSoftEngine(self) ((self) ? ((AtApsLinearSoftEngine)(self))->lapsHandler : NULL)
#define mThis(self) ((AtApsLinearSoftEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtApsLinearEngineMethods m_AtApsLinearEngineOverride;
static tAtApsEngineMethods       m_AtApsEngineOverride;
static tAtObjectMethods          m_AtObjectOverride;
static tAtChannelMethods         m_AtChannelOverride;

/* Save super implementation */
static const tAtApsEngineMethods       *m_AtApsEngineMethods       = NULL;
static const tAtApsLinearEngineMethods *m_AtApsLinearEngineMethods = NULL;
static const tAtObjectMethods          *m_AtObjectMethods          = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Enable(AtChannel self, eBool enable)
    {
    if (enable)
        return ((AtLApsEngStart(mSoftEngine(self)) == cAtLApsOk) ? cAtOk : cAtError);
    return ((AtLApsEngStop(mSoftEngine(self)) == cAtLApsOk) ? cAtOk : cAtError);
    }

static eAtLApsSwType SwitchTypeToComponent(eAtApsSwitchType swType)
    {
    return (swType == cAtApsSwitchTypeNonRev) ? cAtLApsSwTypeNonRev : cAtLApsSwTypeRev;
    }

static eAtApsLinearSwitchType SwitchTypeFromComponent(eAtLApsSwType swType)
    {
    return (swType == cAtLApsSwTypeNonRev) ? cAtApsLinearSwitchTypeNonRev : cAtApsLinearSwitchTypeRev;
    }

static eAtLApsOpMd OpModeToComponent (eAtApsLinearOpMode opmode)
    {
    if (opmode == cAtApsLinearOpModeUni) return cAtLApsOpMdUni;
    return cAtLApsOpMdBid;
    }

static eAtApsLinearOpMode OpModeFromComponent (eAtLApsOpMd opmode)
    {
    return (opmode == cAtLApsOpMdUni) ? cAtApsLinearOpModeUni : cAtApsLinearOpModeBid;
    }

static eAtLApsArch ArchToComponent(eAtApsLinearArch arch)
    {
    return (arch == cAtApsLinearArch11) ? cAtLApsArch11 : cAtLApsArch1n;
    }

static eAtApsLinearArch ArchFromComponent(eAtLApsArch arch)
    {
    return (arch == cAtLApsArch11) ? cAtApsLinearArch11 : cAtApsLinearArch1n;
    }

static eAtApsEngineState StateGet(AtApsEngine self)
    {
    eAtLApsEngState state = AtLApsEngState(mSoftEngine(self));

    if (state == cAtLApsEngStateStop)  return cAtApsEngineStateStop;
    if (state == cAtLApsEngStateStart) return cAtApsEngineStateStart;

    return cAtApsEngineStateUnknown;
    }

static uint32 WtrGet(AtApsEngine self)
    {
    return (AtLApsEngWtrGet(mSoftEngine(self)));
    }

static eAtModuleApsRet WtrSet(AtApsEngine self, uint32 wtrInSec)
    {
    return ((AtLApsEngWtrSet(mSoftEngine(self), wtrInSec) == cAtLApsOk) ? cAtOk : cAtError);
    }

static eAtModuleApsRet SwitchTypeSet(AtApsEngine self, eAtApsSwitchType swType)
    {
    return ((AtLApsEngSwTypeSet(mSoftEngine(self), SwitchTypeToComponent(swType)) == cAtLApsOk) ? cAtOk : cAtError);
    }

static eAtApsSwitchType SwitchTypeGet(AtApsEngine self)
    {
    return SwitchTypeFromComponent(AtLApsEngSwTypeGet(mSoftEngine(self)));
    }

/* Operation mode */
static eAtModuleApsRet OpMdSet(AtApsLinearEngine self, eAtApsLinearOpMode opMd)
    {
    return ((AtLApsEngOpMdSet(mSoftEngine(self), OpModeToComponent(opMd)) == cAtLApsOk) ? cAtOk : cAtError);
    }

static eAtApsLinearOpMode OpMdGet(AtApsLinearEngine self)
    {
    return OpModeFromComponent(AtLApsEngOpMdGet(mSoftEngine(self)));
    }

    /* Architecture */
static eAtModuleApsRet ArchSet(AtApsLinearEngine self, eAtApsLinearArch arch)
    {
    return ((AtLApsEngArchSet(mSoftEngine(self), ArchToComponent(arch)) == cAtLApsOk) ? cAtOk : cAtError);
    }

static eAtApsLinearArch ArchGet(AtApsLinearEngine self)
    {
    return ArchFromComponent(AtLApsEngArchGet(mSoftEngine(self)));
    }

static eAtRet Debug(AtChannel self)
    {
    AtLApsModule lapsModule = AtModuleSoftApsLApsModuleGet((AtModuleSoftAps)AtChannelModuleGet(self));
    AtLApsEngine softEngine = mSoftEngine(self);

    if (softEngine)
        LApsEngShow(lapsModule, AtLApsLineFromChnGet(softEngine, 0));

    return cAtOk;
    }

static eAtLApsExtlCmd ExtCmdToComponent(eAtApsLinearExtCmd extCmd)
    {
    switch (extCmd)
        {
        case cAtApsLinearExtCmdClear: return cAtLApsExtlCmdClear;
        case cAtApsLinearExtCmdExer : return cAtLApsExtlCmdExer;
        case cAtApsLinearExtCmdMs   : return cAtLApsExtlCmdMs;
        case cAtApsLinearExtCmdFs   : return cAtLApsExtlCmdFs;
        case cAtApsLinearExtCmdLp   : return cAtLApsExtlCmdLp;

        case cAtApsLinearExtCmdUnknown:
        default                     : return cAtLApsExtlCmdClear;
        }
    }

static eAtApsLinearExtCmd ExtCmdFromComponent(eAtLApsExtlCmd extCmd)
    {
    switch (extCmd)
        {
        case cAtLApsExtlCmdClear: return cAtApsLinearExtCmdClear;
        case cAtLApsExtlCmdExer : return cAtApsLinearExtCmdExer;
        case cAtLApsExtlCmdMs   : return cAtApsLinearExtCmdMs;
        case cAtLApsExtlCmdFs   : return cAtApsLinearExtCmdFs;
        case cAtLApsExtlCmdLp   : return cAtApsLinearExtCmdLp;
        default                 : return cAtApsLinearExtCmdUnknown;
        }
    }

static eAtModuleApsRet ExtCmdSet(AtApsLinearEngine self, eAtApsLinearExtCmd extlCmd, AtSdhLine affectedLine)
    {
    AtLApsModule lapsModule = AtModuleSoftApsLApsModuleGet((AtModuleSoftAps)AtChannelModuleGet((AtChannel)self));
    uint8 lineIdAffected    = (uint8)AtChannelHwIdGet((AtChannel)affectedLine);
    uint8 channelIdOfLine   = AtLApsLineChnGet(lapsModule, lineIdAffected);

    return AtLApsEngExtlCmdSet(mSoftEngine(self),
                               ExtCmdToComponent(extlCmd),
                               lineIdAffected,
                               channelIdOfLine);
    }

static eAtApsLinearExtCmd ExtCmdGet(AtApsLinearEngine self)
    {
    uint8 lineAffect, channel;
    return ExtCmdFromComponent(AtLApsEngExtlCmdGet(mSoftEngine(self), &lineAffect, &channel));
    }

static AtSdhLine ExtCmdAffectedLineGet(AtApsLinearEngine self)
    {
    uint8 lineAffect, channel;
    AtModuleSdh sdhModule  = (AtModuleSdh)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleSdh);

    AtLApsEngExtlCmdGet(mSoftEngine(self), &lineAffect, &channel);
    return AtModuleSdhLineGet(sdhModule, lineAffect);
    }

static void OverrideAtChannel(AtApsLinearSoftEngine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtApsEngine(AtApsLinearSoftEngine self)
    {
    AtApsEngine engine = (AtApsEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtApsEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsEngineOverride, m_AtApsEngineMethods, sizeof(m_AtApsEngineOverride));
        mMethodOverride(m_AtApsEngineOverride, StateGet);
        mMethodOverride(m_AtApsEngineOverride, WtrGet);
        mMethodOverride(m_AtApsEngineOverride, WtrSet);
        mMethodOverride(m_AtApsEngineOverride, SwitchTypeGet);
        mMethodOverride(m_AtApsEngineOverride, SwitchTypeSet);
        }

    mMethodsSet(engine, &m_AtApsEngineOverride);
    }

static void OverrideAtApsLinearEngine(AtApsLinearSoftEngine self)
    {
    AtApsLinearEngine engine = (AtApsLinearEngine)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtApsLinearEngineMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_AtApsLinearEngineOverride, m_AtApsLinearEngineMethods, sizeof(m_AtApsLinearEngineOverride));
        mMethodOverride(m_AtApsLinearEngineOverride, OpMdGet);
        mMethodOverride(m_AtApsLinearEngineOverride, OpMdSet);
        mMethodOverride(m_AtApsLinearEngineOverride, ArchGet);
        mMethodOverride(m_AtApsLinearEngineOverride, ArchSet);
        mMethodOverride(m_AtApsLinearEngineOverride, ExtCmdSet);
        mMethodOverride(m_AtApsLinearEngineOverride, ExtCmdGet);
        mMethodOverride(m_AtApsLinearEngineOverride, ExtCmdAffectedLineGet);
        }

    mMethodsSet(engine, &m_AtApsLinearEngineOverride);
    }

static void Delete(AtObject self)
    {
    AtApsEngineStop((AtApsEngine)self);
    AtLApsEngDelete(mSoftEngine((AtApsEngine)self));
    mThis(self)->lapsHandler = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtApsLinearSoftEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtApsLinearSoftEngine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtApsEngine(self);
    OverrideAtApsLinearEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsLinearSoftEngine);
    }

static AtApsEngine ObjectInit(AtApsEngine self, AtModuleAps module, uint32 engineId, AtSdhLine lines[], uint8 numLine, AtLApsEngine lapsHandler)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtApsLinearEngineObjectInit(self, module, engineId, lines, numLine) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    /* Save private data */
    mThis(self)->lapsHandler = lapsHandler;

    return self;
    }

AtApsEngine AtApsLinearSoftEngineNew(AtModuleAps module, uint32 engineId, AtSdhLine lines[], uint8 numLine, AtLApsEngine lapsHandler)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtApsEngine newApsEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newApsEngine, module, engineId, lines, numLine, lapsHandler);
    }

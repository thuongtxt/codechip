/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtApsGroup.c
 *
 * Created Date: Aug 4, 2016
 *
 * Description : Generic APS group interface.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtApsSelector.h"
#include "AtApsGroup.h"
#include "AtApsGroupInternal.h"
#include "AtApsSelectorInternal.h"
#include "AtModuleApsInternal.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtApsGroupMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool StateIsValid(eAtApsGroupState state)
    {
    switch (state)
        {
        case cAtApsGroupStateWorking   : return cAtTrue;
        case cAtApsGroupStateProtection: return cAtTrue;
        case cAtApsGroupStateUnknown:
        default:
            return cAtFalse;
        }
    }

static eBool SelectorIsBusy(AtApsSelector selector)
    {
    return AtApsSelectorGroupGet(selector) ? cAtTrue : cAtFalse;
    }

static eAtModuleApsRet Init(AtApsGroup self)
    {
    return AtApsGroupStateSet(self, cAtApsGroupStateWorking);
    }

static eAtModuleApsRet SelectorAdd(AtApsGroup self, AtApsSelector selector)
    {
    return AtListObjectAdd(self->selectorList, (AtObject)selector);
    }

static eAtModuleApsRet SelectorRemove(AtApsGroup self, AtApsSelector selector)
    {
    return AtListObjectRemove(self->selectorList, (AtObject)selector);
    }

static uint32 NumSelectors(AtApsGroup self)
    {
    return AtListLengthGet(self->selectorList);
    }

static AtApsSelector SelectorAtIndex(AtApsGroup self, uint32 selectorIndex)
    {
    return (AtApsSelector)AtListObjectGet(self->selectorList, selectorIndex);
    }

static eAtModuleApsRet StateSet(AtApsGroup self, eAtApsGroupState state)
    {
    AtUnused(self);
    AtUnused(state);
    return cAtErrorNotImplemented;
    }

static eAtApsGroupState StateGet(AtApsGroup self)
    {
    AtUnused(self);
    return cAtApsGroupStateUnknown;
    }

static void MethodsInit(AtApsGroup self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, SelectorAdd);
        mMethodOverride(m_methods, SelectorRemove);
        mMethodOverride(m_methods, NumSelectors);
        mMethodOverride(m_methods, SelectorAtIndex);
        mMethodOverride(m_methods, StateSet);
        mMethodOverride(m_methods, StateGet);
        }

    mMethodsGet(self) = &m_methods;
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtApsGroup apsGroup = (AtApsGroup)self;
    AtDevice dev = AtModuleDeviceGet((AtModule)AtApsGroupModuleGet(apsGroup));

    AtSprintf(description, "%sApsGroup.%d", AtDeviceIdToString(dev), apsGroup->groupId);

    return description;
    }

static void Delete(AtObject self)
    {
    AtApsGroup apsGroup = (AtApsGroup)self;

    if (apsGroup->selectorList)
        {
        AtObjectDelete((AtObject)apsGroup->selectorList);
        apsGroup->selectorList = NULL;
        }

    /* Call object delete */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtApsGroup object = (AtApsGroup)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescriptionList(selectorList);
    }

static void OverrideAtObject(AtApsGroup self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        /* Save reference to super implementation */
        m_AtObjectMethods = mMethodsGet((AtObject)self);

        /* Copy to reuse implementation of super class. But override some methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    /* Change behavior of super class level 1 */
    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

static void Override(AtApsGroup self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtApsGroup);
    }

AtApsGroup AtApsGroupObjectInit(AtApsGroup self, AtModuleAps module, uint32 groupId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    self->module       = module;
    self->groupId      = groupId;
    self->selectorList = AtListCreate(0);

    return self;
    }

/**
 * @addtogroup AtApsGroup
 * @{
 */

/**
 * Get APS Module that manage this group
 *
 * @param self This APS Group
 *
 * @return APS Module Object
 */
AtModuleAps AtApsGroupModuleGet(AtApsGroup self)
    {
    return self ? self->module : NULL;
    }

/**
 * Initialize APS Group
 *
 * @param self This APS Group
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtApsGroupInit(AtApsGroup self)
    {
    mNoParamCall(Init, eAtModuleApsRet, cAtErrorObjectNotExist);
    }

/**
 * Get APS Group ID
 *
 * @param self This APS Group
 *
 * @return Group ID
 */
uint32 AtApsGroupIdGet(AtApsGroup self)
    {
    return self ? self->groupId : cInvalidUint32;
    }

/**
 * Adding APS selector to APS Group
 *
 * @param self This APS Group
 * @param selector object is added to the group
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtApsGroupSelectorAdd(AtApsGroup self, AtApsSelector selector)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorNullPointer;

    /* Selector joined, just does nothing */
    if (AtApsSelectorGroupGet(selector) == self)
        return cAtOk;

    /* It may belong to another group */
    if (SelectorIsBusy(selector))
        return cAtErrorResourceBusy;

    mOneObjectApiLogStart(selector);
    ret = mMethodsGet(self)->SelectorAdd(self, selector);
    if (ret == cAtOk)
        ret = AtApsSelectorGroupSet(selector, self);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Removing APS selector from APS Group
 *
 * @param self This APS Group
 * @param selector object is remove from the group
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtApsGroupSelectorRemove(AtApsGroup self, AtApsSelector selector)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorNullPointer;

    /* A NULL selector will not belong to any group */
    if (selector == NULL)
        return cAtOk;

    /* This selector does not join to this group */
    if (AtApsSelectorGroupGet(selector) != self)
        return cAtErrorObjectNotExist;

    /* Remove it */
    mOneObjectApiLogStart(selector);
    ret = mMethodsGet(self)->SelectorRemove(self, selector);
    if (ret == cAtOk)
        ret = AtApsSelectorGroupSet(selector, NULL);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Get number of current selectors in APS Group
 *
 * @param self This APS Group
 *
 * @return number of current selectors
 */
uint32 AtApsGroupNumSelectors(AtApsGroup self)
    {
    mAttributeGet(NumSelectors, uint32, 0)
    }

/**
 * Get an APS selector from an APS Group by index
 *
 * @param self This APS Group
 * @param selectorIndex Index of selector in list of selector
 *
 * @return APS selector object
 */
AtApsSelector AtApsGroupSelectorAtIndex(AtApsGroup self, uint32 selectorIndex)
    {
    if (self)
        return mMethodsGet(self)->SelectorAtIndex(self, selectorIndex);
    return NULL;
    }

/**
 * Set state of APS Group
 *
 * @param self This APS Group
 * @param state APS Group State. reference eAtApsGroupState for details.
 *
 * @return Module APS return code
 */
eAtModuleApsRet AtApsGroupStateSet(AtApsGroup self, eAtApsGroupState state)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (StateIsValid(state))
        {
        mNumericalAttributeSet(StateSet, state);
        }

    return cAtErrorInvlParm;
    }

/**
 * Get state of APS Group
 *
 * @param self This APS Group
 *
 * @return state of APS Group. reference eAtApsGroupState for details.
 */
eAtApsGroupState AtApsGroupStateGet(AtApsGroup self)
    {
    mAttributeGet(StateGet, eAtApsGroupState, cAtApsGroupStateUnknown)
    }

/**
 * @}
 */

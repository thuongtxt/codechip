/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtModulePdh.c
 *
 * Created Date: Aug 6, 2012
 *
 * Author      : namnn
 *
 * Description : Default implementation of PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePdhInternal.h"
#include "AtModuleClock.h"
#include "AtClockExtractor.h"
#include "AtPdhSerialLineInternal.h"
#include "../common/AtChannelInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mModuleIsValid(self) ((self == NULL) ? cAtFalse : cAtTrue)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtModulePdhMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Methods ----------------------------------------*/
static eAtRet AllDe1sInit(AtModulePdh self)
    {
    eAtRet ret = cAtOk;
    uint32 i;
    uint32 numDe1s = AtModulePdhNumberOfDe1sGet(self);

    /* No channel to init */
    if (self->allDe1s == NULL)
        return cAtOk;

    /* Initialize all of them */
    for (i = 0; i < numDe1s; i++)
        ret |= AtChannelInit((AtChannel)(self->allDe1s[i]));

    return ret;
    }

static eAtRet AllDe3sInit(AtModulePdh self)
    {
    eAtRet ret = cAtOk;
    uint16 i;
    uint32 numDe3s = AtModulePdhNumberOfDe3sGet(self);

    /* No channel to init */
    if (self->allDe3s == NULL)
        return cAtOk;

    /* Initialize all of them */
    for (i = 0; i < numDe3s; i++)
        ret |= AtChannelInit((AtChannel)self->allDe3s[i]);

    return ret;
    }

static eAtRet AllSerialLinesInit(AtModulePdh self)
    {
    eAtRet ret = cAtOk;
    uint32 serial_i;
    uint32 numSerialLines = AtModulePdhNumberOfDe3SerialLinesGet(self);

    /* No channel to init */
    if (self->allSerialLines == NULL)
        return cAtOk;

    /* Initialize all of them */
    for (serial_i = 0; serial_i < numSerialLines; serial_i++)
        ret |= AtChannelInit((AtChannel)(self->allSerialLines[serial_i]));

    return ret;
    }

static eAtRet AllChannelsInit(AtModulePdh self)
    {
    eAtRet ret1, ret2, ret3;

    ret1 = AllDe1sInit(self);
    mModuleErrorLog(self, ret1, "AllDe1sInit");
    ret2 = AllDe3sInit(self);
    mModuleErrorLog(self, ret2, "AllDe3sInit");
    ret3 = AllSerialLinesInit(self);
    mModuleErrorLog(self, ret3, "AllSerialLinesInit");

    return ret1 | ret2 | ret3;
    }

static eAtRet AllDe1Create(AtModulePdh self)
    {
    uint32 i, size;
    AtPdhDe1 *allDe1s = NULL;
    uint32 numDe1s;
    AtOsal osal;

    /* Allocate memory for all channels */
    numDe1s = AtModulePdhNumberOfDe1sGet(self);
    if (numDe1s == 0)
        return cAtOk;

    /* Already create */
    if (self->allDe1s)
        return cAtOk;
        
    osal = AtSharedDriverOsalGet();
    size = sizeof(AtPdhDe1) * numDe1s;
    allDe1s = mMethodsGet(osal)->MemAlloc(osal, size);
    if (allDe1s == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allDe1s, 0, size);

    /* Create all DS1/E1 objects */
    for (i = 0; i < numDe1s; i++)
        {
        allDe1s[i] = mMethodsGet(self)->De1Create(self, i);
        if (allDe1s[i] == NULL)
            return cAtErrorRsrcNoAvail;
        }
    self->allDe1s = allDe1s;

    return cAtOk;
    }

static eAtRet AllDe3Create(AtModulePdh self)
    {
    uint32 i, size;
    AtPdhDe3 *allDe3s = NULL;
    uint32 numDe3s;
    AtOsal osal;

    /* Allocate memory for all channels */
    numDe3s = AtModulePdhNumberOfDe3sGet(self);
    if (numDe3s == 0)
        return cAtOk;

    /* Already create */
    if (self->allDe3s)
        return cAtOk;
        
    osal = AtSharedDriverOsalGet();
    size = sizeof(AtPdhDe3) * numDe3s;
    allDe3s = mMethodsGet(osal)->MemAlloc(osal, size);
    if (allDe3s == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allDe3s, 0, size);

    /* Create all DS1/E1 objects */
    for (i = 0; i < numDe3s; i++)
        {
        allDe3s[i] = mMethodsGet(self)->De3Create(self, i);
        if (allDe3s[i] == NULL)
            return cAtErrorRsrcNoAvail;
        }
    self->allDe3s = allDe3s;

    return cAtOk;
    }

static eAtRet AllSerialLinesCreate(AtModulePdh self)
    {
    uint32 serialLine_i, size;
    AtPdhSerialLine *allSerialLines = NULL;
    uint32 numSerialLines;
    AtOsal osal;

    /* Allocate memory for all channels */
    numSerialLines = AtModulePdhNumberOfDe3SerialLinesGet(self);
    if (numSerialLines == 0)
        return cAtOk;

    /* Already create */
    if (self->allSerialLines)
        return cAtOk;

    osal = AtSharedDriverOsalGet();
    size = sizeof(AtPdhDe3) * numSerialLines;
    allSerialLines = mMethodsGet(osal)->MemAlloc(osal, size);
    if (allSerialLines == NULL)
        return cAtErrorRsrcNoAvail;

    /* Initialize */
    mMethodsGet(osal)->MemInit(osal, allSerialLines, 0, size);

    /* Create all DS3/E3 Serial line objects */
    for (serialLine_i = 0; serialLine_i < numSerialLines; serialLine_i++)
        {
        allSerialLines[serialLine_i] = mMethodsGet(self)->De3SerialCreate(self, serialLine_i);
        if (allSerialLines[serialLine_i] == NULL)
            return cAtErrorRsrcNoAvail;
        }

    self->allSerialLines = allSerialLines;
    return cAtOk;
    }

static eAtRet AllDe3sDelete(AtModulePdh self)
    {
    uint32 i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Nothing to destroy */
    if (self->allDe3s == NULL)
        return cAtOk;

    /* Delete all of DS3/E3 objects */
    for (i = 0; i < AtModulePdhNumberOfDe3sGet(self); i++)
        AtObjectDelete((AtObject)self->allDe3s[i]);

    /* Delete memory used to store all objects */
    mMethodsGet(osal)->MemFree(osal, self->allDe3s);
    self->allDe3s = NULL;

    return cAtOk;
    }

static eAtRet AllDe1sDelete(AtModulePdh self)
    {
    uint32 i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Nothing to destroy */
    if (self->allDe1s == NULL)
        return cAtOk;

    /* Delete all of DS1/E1 objects */
    for (i = 0; i < AtModulePdhNumberOfDe1sGet(self); i++)
        AtObjectDelete((AtObject)(self->allDe1s[i]));

    /* Delete memory used to store all objects */
    mMethodsGet(osal)->MemFree(osal, self->allDe1s);
    self->allDe1s = NULL;

    return cAtOk;
    }

static eAtRet AllDe3SerialLinesDelete(AtModulePdh self)
    {
    uint32 serialLine_i;
    AtOsal osal = AtSharedDriverOsalGet();

    /* Nothing to destroy */
    if (self->allSerialLines == NULL)
        return cAtOk;

    /* Delete all of DS3/E3 objects */
    for (serialLine_i = 0; serialLine_i < AtModulePdhNumberOfDe3SerialLinesGet(self); serialLine_i++)
        AtObjectDelete((AtObject)self->allSerialLines[serialLine_i]);

    /* Delete memory used to store all objects */
    mMethodsGet(osal)->MemFree(osal, self->allSerialLines);
    self->allSerialLines = NULL;

    return cAtOk;
    }

static eAtRet LiuFrequencyMonitorSet(AtModulePdh self, AtPdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAtErrorModeNotSupport;
    }

static eAtRet LiuFrequencyMonitorGet(AtModulePdh self, tAtModulePdhLiuMonitor *clockMonitor)
    {
    AtUnused(self);
    AtUnused(clockMonitor);
    return cAtErrorModeNotSupport;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    AtModulePdh pdhModule = (AtModulePdh)self;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Delete all channels if they were created */
    if (pdhModule->allDe1s || pdhModule->allDe3s || pdhModule->allSerialLines)
        AtModulePdhAllManagedObjectsDelete(pdhModule);

    /* And create them */
    if (AtModulePdhAllManagedObjectsCreate(pdhModule) != cAtOk)
        {
        AtModulePdhAllManagedObjectsDelete(pdhModule);
        return cAtErrorRsrcNoAvail;
        }

    return cAtOk;
    }

/* Delete module */
static void Delete(AtObject self)
    {
    AtModulePdhAllManagedObjectsDelete((AtModulePdh)self);

    if (((AtModulePdh)self)->att)
        AtObjectDelete((AtObject)((AtModulePdh)self)->att);
    ((AtModulePdh)self)->att = NULL;

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret=cAtOk;
    AtAttPdhManager mngr = AtModulePdhAttManager((AtModulePdh)self);
    /* Super init */
    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Initialize all channels */
    AllChannelsInit((AtModulePdh)self);

    /* ATT */
    if (mngr)
        ret |= AtAttPdhManagerInit(mngr);

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "pdh";
    }

static const char *CapacityDescription(AtModule self)
    {
    AtModulePdh modulePdh = (AtModulePdh)self;
    static char string[32];

    AtSprintf(string, "de1s: %u, de3s: %u", AtModulePdhNumberOfDe1sGet(modulePdh), AtModulePdhNumberOfDe3sGet(modulePdh));
    return string;
    }

static void StatusClear(AtModule self)
    {
    AtModulePdh pdhModule = (AtModulePdh)self;
    uint32 numDe1s = AtModulePdhNumberOfDe1sGet(pdhModule);
    uint32 numDe3s = AtModulePdhNumberOfDe3sGet(pdhModule);
    uint32 i;

    m_AtModuleMethods->StatusClear(self);

    for (i = 0; i < numDe1s; i++)
        {
        AtChannel de1 = (AtChannel)AtModulePdhDe1Get(pdhModule, i);
        AtChannelStatusClear(de1);
        }

    for (i = 0; i < numDe3s; i++)
        {
        AtChannel de3 = (AtChannel)AtModulePdhDe3Get(pdhModule, i);
        AtChannelStatusClear(de3);
        }
    }

static AtPdhDe2 De2Create(AtModulePdh self, uint32 de2Id)
    {
    /* Conctete must know */
    AtUnused(self);
    AtUnused(de2Id);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModulePdh object = (AtModulePdh)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(allDe1s, AtModulePdhNumberOfDe1sGet(object));
    mEncodeObjects(allDe3s, AtModulePdhNumberOfDe3sGet(object));
    mEncodeObjects(allSerialLines, AtModulePdhNumberOfDe3SerialLinesGet(object));
    mEncodeObject(att);
    }

static void AllAlarmsAndErrorsUnforce(AtPdhChannel channel)
    {
    uint8 i;
    AtChannel pdhChannel = (AtChannel)channel;

    if (channel == NULL)
        return;

    if (AtChannelTxForcableAlarmsGet(pdhChannel) != 0)
        AtChannelTxAlarmUnForce(pdhChannel, cBit31_0);

    if (AtChannelRxForcableAlarmsGet(pdhChannel) != 0)
        AtChannelRxAlarmUnForce(pdhChannel, cBit31_0);

    if (AtChannelTxForcableErrorsGet(pdhChannel) != 0)
        AtChannelTxErrorUnForce(pdhChannel, cBit31_0);

    for (i = 0; i < AtPdhChannelNumberOfSubChannelsGet(channel); i++)
        AllAlarmsAndErrorsUnforce(AtPdhChannelSubChannelGet(channel, i));
    }

static void FindPdhChannelAndUnforceAllAlarmsAndErrors(AtSdhChannel channel)
    {
    uint8 i;
    uint8 numSubChannels;

    if (channel == NULL)
        return;

    numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);

    if (numSubChannels == 0)
        {
        AtPdhChannel pdhChannel = (AtPdhChannel)AtSdhChannelMapChannelGet(channel);
        if (pdhChannel)
            AllAlarmsAndErrorsUnforce(pdhChannel);
        }
    else
        {
        for (i = 0; i < numSubChannels; i++)
            {
            AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, i);
            if (subChannel)
                FindPdhChannelAndUnforceAllAlarmsAndErrors(subChannel);
            }
        }
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    uint32 de1Id;
    uint32 de3Id;
    uint8 lineId;
    AtModuleSdh sdhModule;
    AtModulePdh pdhModule = (AtModulePdh)self;

    for (de1Id = 0; de1Id < AtModulePdhNumberOfDe1sGet(pdhModule); de1Id++)
        {
        AtChannel de1 = (AtChannel)AtModulePdhDe1Get(pdhModule, de1Id);
        if (de1 == NULL)
            continue;
            
        AtChannelTxAlarmUnForce(de1, cBit31_0);
        AtChannelRxAlarmUnForce(de1, cBit31_0);
        AtChannelTxErrorUnForce(de1, cBit31_0);
        }

    for (de3Id = 0; de3Id < AtModulePdhNumberOfDe3sGet(pdhModule); de3Id++)
        {
        AtChannel de3 = (AtChannel)AtModulePdhDe3Get(pdhModule, de3Id);
        if (de3 == NULL)
            continue;
            
        AtChannelTxAlarmUnForce(de3, cBit31_0);
        AtChannelRxAlarmUnForce(de3, cBit31_0);
        AtChannelTxErrorUnForce(de3, cBit31_0);
        }

    sdhModule = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleSdh);
    if (sdhModule == NULL)
        return cAtOk;

    for (lineId = 0; lineId < AtModuleSdhMaxLinesGet(sdhModule); lineId++)
        FindPdhChannelAndUnforceAllAlarmsAndErrors((AtSdhChannel)AtModuleSdhLineGet(sdhModule, lineId));

    return cAtOk;
    }

static void PdhChannelListenedDefectRecursiveClear(AtPdhChannel self)
    {
    uint8 i, numSubChannel;

    AtChannelListenedDefectClear((AtChannel)self, NULL);
    numSubChannel = AtPdhChannelNumberOfSubChannelsGet(self);
    for (i = 0; i < numSubChannel; i++)
        PdhChannelListenedDefectRecursiveClear(AtPdhChannelSubChannelGet(self, i));
    }

static void AllChannelsListenedDefectClear(AtModule self)
    {
    uint8 i;
    AtModulePdh modulePdh = (AtModulePdh)self;

    for (i = 0; i < AtModulePdhNumberOfDe1sGet(modulePdh); i++)
        PdhChannelListenedDefectRecursiveClear((AtPdhChannel)AtModulePdhDe1Get(modulePdh, i));

    for (i = 0; i < AtModulePdhNumberOfDe3sGet(modulePdh); i++)
        PdhChannelListenedDefectRecursiveClear((AtPdhChannel)AtModulePdhDe3Get(modulePdh, i));
    }

static eAtRet AllChannelsInterruptDisable(AtModule self)
    {
    AtModulePdh module = (AtModulePdh)self;
    uint32 de1Id;
    uint32 de3Id;

    for (de1Id = 0; de1Id < AtModulePdhNumberOfDe1sGet(module); de1Id++)
        {
        AtPdhChannel de1 = (AtPdhChannel)AtModulePdhDe1Get(module, de1Id);
        if (de1)
            AtPdhChannelSubChannelsInterruptDisable(de1, cAtTrue);
        }

    for (de3Id = 0; de3Id < AtModulePdhNumberOfDe3sGet(module); de3Id++)
        {
        AtPdhChannel de3 = (AtPdhChannel)AtModulePdhDe3Get(module, de3Id);
        if (de3)
            AtPdhChannelSubChannelsInterruptDisable(de3, cAtFalse);
        }

    return cAtOk;
    }

static eBool NeedClearanceNotifyLogic(AtModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtQuerier QuerierGet(AtModule self)
    {
    AtUnused(self);
    return AtModulePdhSharedQuerier();
    }

static void OverrideAtObject(AtModulePdh self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtObject pdhObject = (AtObject)self;

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet(pdhObject);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(pdhObject, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        mMethodOverride(m_AtModuleOverride, AllServicesDestroy);
        mMethodOverride(m_AtModuleOverride, AllChannelsListenedDefectClear);
        mMethodOverride(m_AtModuleOverride, AllChannelsInterruptDisable);
        mMethodOverride(m_AtModuleOverride, QuerierGet);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

static uint32 NumberOfDe1sGet(AtModulePdh self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static AtPdhDe1 De1Get(AtModulePdh self, uint32 de1Id)
    {
    /* Database has not been created */
    if (self->allDe1s == NULL)
        return NULL;

    /* Return corresponding object in database */
    if (de1Id < mMethodsGet(self)->NumberOfDe1sGet(self))
        return self->allDe1s[de1Id];

    return NULL;
    }

static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
	AtUnused(de1Id);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
	AtUnused(sdhVc);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(sdhVc);
    return NULL;
    }

static AtPdhNxDS0 NxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap)
    {
	AtUnused(timeslotBitmap);
	AtUnused(de1);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtClockExtractor ClockExtractor(AtModulePdh self, uint8 refOutId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleClock clockModule = (AtModuleClock)AtDeviceModuleGet(device, cAtModuleClock);
    return AtModuleClockExtractorGet(clockModule, refOutId);
    }

static AtAttPdhManager AttPdhManagerCreate(AtModulePdh self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return AtModulePdhDe1Create(self, de1Id);
    }

static AtPdhDe3 De3Create(AtModulePdh self, uint32 de3Id)
    {
    AtUnused(self);
    AtUnused(de3Id);
    return NULL;
    }

static uint32 NumberOfDe3sGet(AtModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static AtPdhDe3 De3Get(AtModulePdh self, uint32 de3Id)
    {
    /* Database has not been created */
    if (self->allDe3s == NULL)
        return NULL;

    /* Return corresponding object in database */
    if (de3Id < mMethodsGet(self)->NumberOfDe3sGet(self))
        return self->allDe3s[de3Id];

    return NULL;
    }

static uint32 NumberOfDe3SerialLinesGet(AtModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static AtPdhSerialLine De3SerialCreate(AtModulePdh self, uint32 serialLineId)
    {
    AtUnused(self);
    AtUnused(serialLineId);
    return NULL;
    }

static AtPdhSerialLine De3SerialLineGet(AtModulePdh self, uint32 serialLineId)
    {
    /* Database has not been created */
    if (self->allSerialLines == NULL)
        return NULL;

    /* Return corresponding object in database */
    if (serialLineId < mMethodsGet(self)->NumberOfDe3SerialLinesGet(self))
        return self->allSerialLines[serialLineId];

    return NULL;
    }

static eAtModulePdhRet PrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PrmCrCompareIsEnabled(AtModulePdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NumberOfDe1sGet);
        mMethodOverride(m_methods, De1Get);
        mMethodOverride(m_methods, AttPdhManagerCreate);
        mMethodOverride(m_methods, De1Create);
        mMethodOverride(m_methods, NxDs0Create);
        mMethodOverride(m_methods, VcDe1Create);
        mMethodOverride(m_methods, VcDe3Create);
        mMethodOverride(m_methods, De2De1Create);
        mMethodOverride(m_methods, De3Create);
        mMethodOverride(m_methods, NumberOfDe3sGet);
        mMethodOverride(m_methods, De3Get);
        mMethodOverride(m_methods, De2Create);
        mMethodOverride(m_methods, AllDe1Create);
        mMethodOverride(m_methods, NumberOfDe3SerialLinesGet);
        mMethodOverride(m_methods, De3SerialLineGet);
        mMethodOverride(m_methods, De3SerialCreate);
        mMethodOverride(m_methods, PrmCrCompareEnable);
        mMethodOverride(m_methods, PrmCrCompareIsEnabled);
        mMethodOverride(m_methods, AllDe1sDelete);
        mMethodOverride(m_methods, LiuFrequencyMonitorSet);
        mMethodOverride(m_methods, LiuFrequencyMonitorGet);
        mMethodOverride(m_methods, NeedClearanceNotifyLogic);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize Module PDH object */
AtModulePdh AtModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModulePdh));

    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModulePdh, device) == NULL)
        return NULL;

    /* Override Delete method of AtObject class */
    Override(self);

    /* Initialize Methods */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }
    
eAtRet AtModulePdhAllManagedObjectsDelete(AtModulePdh self)
    {
    return mMethodsGet(self)->AllDe1sDelete(self) |
           AllDe3sDelete(self) |
           AllDe3SerialLinesDelete(self);
    }

eAtRet AtModulePdhAllManagedObjectsCreate(AtModulePdh self)
    {
    return mMethodsGet(self)->AllDe1Create(self) |
           AllDe3Create(self) |
           AllSerialLinesCreate(self);
    }

AtPdhChannel AtModulePdhHwVtId2PdhChannel(AtModulePdh self, uint8 stsId, uint8 vtId)
    {
	AtUnused(vtId);
	AtUnused(stsId);
	AtUnused(self);
    return NULL;
    }

AtPdhDe1 AtModulePdhDe1Create(AtModulePdh self, uint32 de1Id)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->De1Create(self, de1Id);

    return NULL;
    }

AtPdhDe1 AtModulePdhVcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->VcDe1Create(self, sdhVc);

    return NULL;
    }

AtPdhDe3 AtModulePdhVcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->VcDe3Create(self, sdhVc);

    return NULL;
    }

AtPdhDe1 AtModulePdhDe2De1Create(AtModulePdh self, AtPdhDe2 de2, uint8 de1Id)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->De2De1Create(self, de2, de1Id);

    return NULL;
    }

AtPdhNxDS0 AtModulePdhNxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->NxDs0Create(self, de1, timeslotBitmap);
    return NULL;
    }

AtPdhDe2 AtModulePdhDe2Create(AtModulePdh self, uint32 de2Id)
    {
    if (mModuleIsValid(self))
        return mMethodsGet(self)->De2Create(self, de2Id);
    return NULL;
    }

/*
 * Select what channel that recovered clock on it will output to external PLL.
 *
 * @param self This module
 * @param refOutId The ID of clock signal comes to external PLL
 * @param source Clock source that clock is recovered. Set to NULL to disable
 *               clock output.
 *
 * @return AT return code
 */
eAtModulePdhRet AtModulePdhOutputClockSourceSet(AtModulePdh self, uint8 refOutId, AtPdhChannel source)
    {
    AtClockExtractor clockExtractor;
    eAtRet ret = cAtOk;

    if (!mModuleIsValid(self))
        return cAtError;

    clockExtractor = ClockExtractor(self, refOutId);
    if (source)
        ret |= AtClockExtractorPdhDe1ClockExtract(clockExtractor, (AtPdhDe1)source);
    ret |= AtClockExtractorEnable(clockExtractor, source ? cAtTrue : cAtFalse);

    return ret;
    }

/*
 * Get clock source that is selected to go to external PLL
 *
 * @param self This module
 * @param refOutId The ID of clock signal comes to external PLL
 *
 * @return Channel that recovered clock is output
 */
AtPdhChannel AtModulePdhOutputClockSourceGet(AtModulePdh self, uint8 refOutId)
    {
    AtClockExtractor clockExtractor;

    if (!mModuleIsValid(self))
        return NULL;

    clockExtractor = ClockExtractor(self, refOutId);
    if (AtClockExtractorIsEnabled(clockExtractor))
        return (AtPdhChannel)AtClockExtractorPdhDe1Get(clockExtractor);

    return NULL;
    }

eAtRet AtModulePdhLiuFrequencyMonitorSet(AtModulePdh self, AtPdhChannel channel)
    {
    if (self)
        return mMethodsGet(self)->LiuFrequencyMonitorSet(self, channel);
    return cAtErrorNullPointer;
    }

eAtRet AtModulePdhLiuFrequencyMonitorGet(AtModulePdh self, tAtModulePdhLiuMonitor *clockMonitor)
    {
    if (self)
        return mMethodsGet(self)->LiuFrequencyMonitorGet(self, clockMonitor);
    return cAtErrorNullPointer;
    }

eBool AtModulePdhNeedClearanceNotifyLogic(AtModulePdh self)
    {
    if (self)
        return mMethodsGet(self)->NeedClearanceNotifyLogic(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtModulePdh
 * @{
 */
/**
 * Get maximum number of DS1s/E1s that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of DS1s/E1s
 */
uint32 AtModulePdhNumberOfDe1sGet(AtModulePdh self)
    {
    mAttributeGet(NumberOfDe1sGet, uint32, 0);
    }

/**
 * Get DS1/E1 object by its ID
 *
 * @param self This module
 * @param de1Id DS1/E1 ID
 *
 * @return DS1/E1 object on success or NULL on failure
 */
AtPdhDe1 AtModulePdhDe1Get(AtModulePdh self, uint32 de1Id)
    {
    mOneParamObjectGet(De1Get, AtPdhDe1, de1Id);
    }

/**
 * Get maximum number of DS3s/E3s that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of DS3s/E3s
 */
uint32 AtModulePdhNumberOfDe3sGet(AtModulePdh self)
    {
    mAttributeGet(NumberOfDe3sGet, uint32, 0);
    }

/**
 * Get DS3/E3 object by its ID
 *
 * @param self This module
 * @param de3Id DS3/E3 flat ID
 *
 * @return DS3/E3 object or NULL if ID is invalid
 */
AtPdhDe3 AtModulePdhDe3Get(AtModulePdh self, uint32 de3Id)
    {
    mOneParamObjectGet(De3Get, AtPdhDe3, de3Id);
    }

/**
 * Get maximum number of DS3/E3 Serial lines that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of DS3/E3 Serial lines
 */
uint32 AtModulePdhNumberOfDe3SerialLinesGet(AtModulePdh self)
    {
    mAttributeGet(NumberOfDe3SerialLinesGet, uint32, 0);
    }

/**
 * Get DS3/E3 Serial lines object by its ID
 *
 * @param self This module
 * @param serialLineId DS3/E3 Serial line ID
 *
 * @return DS3/E3 Serial lines object on success or NULL on failure
 */
AtPdhSerialLine AtModulePdhDe3SerialLineGet(AtModulePdh self, uint32 serialLineId)
    {
    mOneParamObjectGet(De3SerialLineGet, AtPdhSerialLine, serialLineId);
    }

/**
 * Enable CR bits comparing
 *
 * @param self This module
 * @param enable Enable/disable CR bit comparing
 *
 * @return AT return code
 */
eAtModulePdhRet AtModulePdhPrmCrCompareEnable(AtModulePdh self, eBool enable)
    {
    mNumericalAttributeSet(PrmCrCompareEnable, enable);
    }

/**
 * Check if CR bits comparing is enabled
 *
 * @param self This module
 *
 * @retval cAtTrue - CR bits comparing is enabled
 * @retval cAtFalse - CR bits comparing is disabled
 */
eBool AtModulePdhPrmCrCompareIsEnabled(AtModulePdh self)
    {
    mAttributeGet(PrmCrCompareIsEnabled, eBool, cAtFalse);
    }

/** @} */

AtAttPdhManager AtModulePdhAttManager(AtModulePdh self)
    {
    if (self)
        {
        if (self->att == NULL)
            {
            self->att = mMethodsGet(self)->AttPdhManagerCreate(self);
            if (self->att)
                {
                AtAttPdhManagerInit(self->att);
                }
            }
        return self->att;
        }
    return NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhNxDs0.c
 *
 * Created Date: Aug 6, 2012
 *
 * Author      : namnn
 *
 * Description : Default implementation of NxDS0
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPdhNxDs0Internal.h"
#include "AtPdhDe1.h"
#include "AtPwCESoP.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPdhNxDS0)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtPdhNxDs0Methods  m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DataRateInBytesPer125Us(AtChannel self)
    {
    return AtChannelDataRateInBytesPerMs(self) / 8;
    }

static uint32 DataRateInBytesPerMs(AtChannel self)
    {
    static const uint8 cDataRateInBytesPerMsOfOneSlot = 8;
    uint32 numSlots = AtPdhNxDs0NumTimeslotsGet(mThis(self));

    return numSlots * cDataRateInBytesPerMsOfOneSlot;
    }

static eBool De1HasCesopWithCas(AtPdhNxDS0 self)
    {
    AtPdhNxDS0 nxDs0;
    AtIterator nxDs0Iterator;
    eBool hasCesopWithCas = cAtFalse;
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(self);
    if (de1 == NULL)
        return cAtFalse;

    nxDs0Iterator = AtPdhDe1nxDs0IteratorCreate(de1);
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(nxDs0Iterator)) != NULL)
        {
        AtPw pw = AtChannelBoundPwGet((AtChannel)nxDs0);
        if ((pw) && AtPwCESoPModeGet((AtPwCESoP)pw) == cAtPwCESoPModeWithCas)
            {
            hasCesopWithCas = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)nxDs0Iterator);

    return hasCesopWithCas;
    }

static eBool Timeslot16IsBusy(AtPdhNxDS0 self)
    {
    AtPdhNxDS0 nxDs0;
    AtIterator nxDs0Iterator;
    eBool busy = cAtFalse;
    AtPdhDe1 de1 = AtPdhNxDS0De1Get(self);
    if (de1 == NULL)
        return cAtFalse;

    nxDs0Iterator = AtPdhDe1nxDs0IteratorCreate(de1);
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(nxDs0Iterator)) != NULL)
        {
        if ((AtPdhNxDS0BitmapGet((AtPdhNxDS0)nxDs0) & cBit16) && AtChannelBoundPwGet((AtChannel)nxDs0))
            {
            busy = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)nxDs0Iterator);

    return busy;
    }

static eAtRet E1NxDS0CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eBool hasTimeslot16 = (AtPdhNxDS0BitmapGet((AtPdhNxDS0)self) & cBit16) ? cAtTrue : cAtFalse;
    eAtPwCESoPMode cesMode;

    if (pseudowire == NULL)
        return cAtOk;

    if (AtPwTypeGet(pseudowire) != cAtPwTypeCESoP)
        return cAtErrorInvlParm;

    cesMode = AtPwCESoPModeGet((AtPwCESoP)pseudowire);
    if (cesMode == cAtPwCESoPModeWithCas)
        {
        if (hasTimeslot16)
            return cAtErrorInvlParm;
        if (Timeslot16IsBusy((AtPdhNxDS0)self))
            return cAtErrorInvlParm;
        }

    if (hasTimeslot16 && De1HasCesopWithCas((AtPdhNxDS0)self))
        return cAtErrorInvlParm;

    return cAtOk;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtPdhDe1 de1 = AtPdhNxDS0De1Get((AtPdhNxDS0)self);

    if (AtPdhDe1IsE1(de1))
        return E1NxDS0CanBindToPseudowire(self, pseudowire);

    return cAtOk;
    }

static eAtPdhChannelType TypeGet(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtPdhChannelTypeNxDs0;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    if (m_AtChannelMethods->ServiceIsRunning(self))
        return cAtTrue;

    return (AtChannelPrbsEngineGet(self)) ? cAtTrue : cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPdhNxDS0 object = (AtPdhNxDS0)self;
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeChannelIdString(de1);
    mEncodeUInt(mask);
    mEncodeUInt(isEnable);
    }

static eAtRet CasIdleCodeSet(AtPdhNxDS0 self, uint8 abcd1)
    {
    AtUnused(self);
    AtUnused(abcd1);
    return cAtErrorNotImplemented;
    }

static uint8 CasIdleCodeGet(AtPdhNxDS0 self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(AtPdhNxDS0 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, CasIdleCodeSet);
        mMethodOverride(m_methods, CasIdleCodeGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtPdhChannel(AtPdhNxDS0 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, mMethodsGet(channel), sizeof(m_AtPdhChannelOverride));
        mMethodOverride(m_AtPdhChannelOverride, TypeGet);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtObject(AtPdhNxDS0 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPdhNxDS0 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DataRateInBytesPer125Us);
        mMethodOverride(m_AtChannelOverride, DataRateInBytesPerMs);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhNxDS0 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

AtPdhNxDS0 AtPdhNxDS0ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask)
    {
    /* Clear memory */
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPdhNxDS0));
    
    /* Super constructor */
    if (AtPdhChannelObjectInit(pdhChannel, mask, (AtModulePdh)AtChannelModuleGet((AtChannel)de1)) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private variables */
    self->mask = mask;
    self->de1  = de1;
    AtPdhChannelVcSet(pdhChannel, AtPdhChannelVcInternalGet((AtPdhChannel)de1));
    AtPdhChannelParentChannelSet(pdhChannel, (AtPdhChannel)de1);
    
    return self;
    }

/**
 * @addtogroup AtPdhNxDs0
 * @{
 */

/**
 * Get bitmap
 *
 * @param self This channel
 *
 * @return Bitmap
 */
uint32 AtPdhNxDS0BitmapGet(AtPdhNxDS0 self)
    {
    if (self)
        return self->mask;
    return 0x0;
    }

/**
 * Get DS1/E1 self from nxDS0
 *
 * @param self This channel
 *
 * @return AtPdhDe1 object
 */
AtPdhDe1 AtPdhNxDS0De1Get(AtPdhNxDS0 self)
    {
    if (self)
        return self->de1;

    return NULL;
    }

/**
 * Get number of DS0 timeslots
 *
 * @param self This channel
 * @return Number of DS0 timeslots
 */
uint8 AtPdhNxDs0NumTimeslotsGet(AtPdhNxDS0 self)
    {
    uint32 mask;
    uint8 numSlots = 0;

    if (self == NULL)
        return 0;

    mask = AtPdhNxDS0BitmapGet(self);
    while (mask > 0)
        {
        if (mask & cBit0)
            numSlots = (uint8)(numSlots + 1);
        mask = mask >> 1;
        }

    return numSlots;
    }

/**
 * Set CAS idle code for a nxDS0
 *
 * @param self This channel
 * @param casIdle Idle code of CAS
 *
 * @return AT return code
 */
eAtRet AtPdhNxDs0CasIdleCodeSet(AtPdhNxDS0 self, uint8 casIdle)
    {
    mNumericalAttributeSet(CasIdleCodeSet, casIdle);
    }

/**
 * Get configured CAS idle code from nxDS0
 *
 * @param self This channel
 *
 * @return CAS idle code value
 */
uint8 AtPdhNxDs0CasIdleCodeGet(AtPdhNxDS0 self)
    {
    mAttributeGet(CasIdleCodeGet, uint8, 0);
    }

/**
 * @}
 */

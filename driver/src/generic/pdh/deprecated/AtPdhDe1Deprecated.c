/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhDe1Deprecated.c
 *
 * Created Date: Nov 27, 2015
 *
 * Description : Deprecated PDH DS1/E1 implementation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtPdhDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

/*
 * Send loopcode
 *
 * @param self This T1 SF or T1 ESF
 * @param loopcode @ref eAtPdhDe1Loopcode "Loopcode"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1LoopcodeSend(AtPdhDe1 self, eAtPdhDe1Loopcode loopcode)
    {
    return AtPdhDe1TxLoopcodeSet(self, loopcode);
    }

/*
 * Receive a loopcode
 *
 * @param self This T1 SF or T1 ESF
 *
 * @return @ref eAtPdhDe1Loopcode "Loopcodes"
 */
eAtPdhDe1Loopcode AtPdhDe1LoopcodeReceive(AtPdhDe1 self)
    {
    return AtPdhDe1RxLoopcodeGet(self);
    }

/*
 * Send BOM for per channels
 *
 * @param self This DS1-ESF channel
 * @param message Message to send
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1BomSend(AtPdhDe1 self, uint8 message)
    {
    return AtPdhDe1TxBomSet(self, message);
    }

/*
 * Get received BOM for per channels
 *
 * @param self This DS1-ESF channel
 *
 * @return AT return code
 */
uint8 AtPdhDe1BomReceive(AtPdhDe1 self)
    {
    return AtPdhDe1RxBomGet(self);
    }

/*
 * Set enable LoopCode for per channels
 *
 * @param self This Ds1/E1 channel
 * @param enable [cAtTrue, cAtFalse] for enable/disable
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1LoopcodeEnable(AtPdhDe1 self, eBool enable)
    {
    if (self)
        self->loopcodeEnable = enable;
    return cAtOk;
    }

/*
 * Get enable LoopCode for per channels
 *
 * @param self This Ds1/E1 channel
 *
 * @return cAtFalse if this channel disable loop code
 * @return cAtTrue  if this channel enable loop code
 */
eBool AtPdhDe1LoopcodeIsEnabled(AtPdhDe1 self)
    {
    if (self)
        return self->loopcodeEnable;
    return cAtFalse;
    }

/*
 * Set transmitted FDL BOM code along with N times of transmitted bomCode for DS1 ESF channel
 * This (maximum times number) is also the number to indicate BOM requested to send continuously.
 * Zero times number is also a the number to indicated BOM requested to stop sending.
 *
 * @param self This DS1 ESF channel
 * @param bomCode 6-bits ESF BOM code (111111110xxxxxx0).
 * @param nTimes number of times to send the BOM pattern.
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1TxBomNTimesSend(AtPdhDe1 self, uint8 bomCode, uint32 nTimes)
    {
    mTwoParamsAttributeSet(TxBomNTimesSend, bomCode, nTimes);
    }

/*
 * Get maximum times of transmitted FDL BOM code allowed for DS1 ESF channel.
 * This maximum times number is also the number to indicate BOM requested to send continuously.
 *
 * @param self This DS1 ESF channel
 * @return Maximum number of times that BOM can be sent.
 */
uint32 AtPdhDe1TxBomSentMaxNTimes(AtPdhDe1 self)
    {
    AtUnused(self);
    return 0xF;
    }

/*
 * Get configured N-times of transmitted FDL BOM code allowed for DS1 ESF channel.
 * This maximum times number is also the number to indicate BOM requested to send continuously.
 * Zero times number is also a the number to indicated BOM requested to stop sending.
 *
 * @param self This DS1 ESF channel
 * @return Maximum number of times that BOM can be sent.
 */
uint32 AtPdhDe1TxBomSentCfgNTimes(AtPdhDe1 self)
    {
    mAttributeGet(TxBomSentCfgNTimesGet, uint32, 0);
    }


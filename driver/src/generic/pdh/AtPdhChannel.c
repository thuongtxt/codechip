/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhChannel.c
 *
 * Created Date: Aug 6, 2012
 *
 * Author      : namnn
 *
 * Description : Default implementation of PDH channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../ber/AtModuleBerInternal.h"
#include "../diag/AtErrorGeneratorInternal.h"
#include "../sur/AtSurEngineInternal.h"
#include "AtPdhChannelInternal.h"
#include "AtModulePdhInternal.h"
#include "../att/AtAttController.h"
/*--------------------------- Define -----------------------------------------*/
typedef struct tListenerWraper
    {
    void* userData;
    tAtPdhChannelPropertyListener listener;
    }tListenerWraper;

/*--------------------------- Macros -----------------------------------------*/
#define mChannelIsValid(self) ((self == NULL) ? cAtFalse : cAtTrue)
#define mThis(self)  ((AtPdhChannel)self)
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPdhChannelMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "pdh_channel";
    }

static const char *IdString(AtChannel self)
    {
    static char idString[32];
    AtPdhChannel channel = (AtPdhChannel)self;

    if (AtPdhChannelParentChannelGet(channel) == NULL)
        return m_AtChannelMethods->IdString(self);

    AtSprintf(idString, "%s.%d",
              AtChannelIdString((AtChannel)AtPdhChannelParentChannelGet(channel)),
              AtChannelIdGet(self) + 1);

    return idString;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
	AtUnused(frameType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }
    
static uint16 FrameTypeGet(AtPdhChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }
    
static eAtRet LineCodeSet(AtPdhChannel self, uint16 lineCode)
    {
	AtUnused(lineCode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint16 LineCodeGet(AtPdhChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eBool NationalBitsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet NationalBitsSet(AtPdhChannel self, uint8 pattern)
    {
	AtUnused(pattern);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 NationalBitsGet(AtPdhChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8 RxNationalBitsGet(AtPdhChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet TxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtFalse;
    }

static eBool TxSlipBufferIsEnabled(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RxSlipBufferIsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet RxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    AtUnused(self);
    if (!enable)
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eBool  RxSlipBufferIsEnabled(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxSsmSet(AtPdhChannel self, uint8 value)
    {
    AtUnused(value);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 TxSsmGet(AtPdhChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8 RxSsmGet(AtPdhChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet SsmEnable(AtPdhChannel self, eBool enable)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool SsmIsEnabled(AtPdhChannel self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtFalse;
    }

static AtBerController PathBerControllerGet(AtPdhChannel self)
    {
    AtBerController newController;

    if (self->pathBerController)
        return self->pathBerController;

    /* Create one */
    newController = mMethodsGet(self)->PathBerControllerCreate(self);
    if (newController == NULL)
        return NULL;

    AtBerControllerInit(newController);
    self->pathBerController = newController;

    return self->pathBerController;
    }

static AtBerController LineBerControllerGet(AtPdhChannel self)
    {
    AtBerController newController;

    if (self->lineBerController)
        return self->lineBerController;

    /* Create one */
    newController = mMethodsGet(self)->LineBerControllerCreate(self);
    if (newController == NULL)
        return NULL;

    AtBerControllerInit(newController);
    self->lineBerController = newController;

    return self->lineBerController;
    }

static eAtRet LedStateSet(AtPdhChannel self, eAtLedState ledState)
    {
	AtUnused(ledState);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eAtLedState LedStateGet(AtPdhChannel self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtLedStateOff;
    }

static eAtRet AutoRaiEnable(AtPdhChannel self, eBool enable)
	{
	AtUnused(enable);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
	}

static eBool AutoRaiIsEnabled(AtPdhChannel self)
	{
	AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
	}

static eBool IsPayloadLoopback(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);

    if ((loopbackMode == cAtPdhLoopbackModeLocalPayload) ||
        (loopbackMode == cAtPdhLoopbackModeRemotePayload))
        return cAtTrue;

    return cAtFalse;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet(mThis(self));
    eBool isUnframed = mMethodsGet(mThis(self))->IsUnframed(mThis(self), frameType);
    eBool isBoundPw  = (AtChannelBoundPwGet(self) != NULL) ? cAtTrue : cAtFalse;

    if (IsPayloadLoopback(self, loopbackMode) && isUnframed)
        return cAtFalse;

    if (IsPayloadLoopback(self, loopbackMode) && isBoundPw)
        return cAtFalse;

    if ((loopbackMode == cAtPdhLoopbackModeRelease)      ||
        (loopbackMode == cAtPdhLoopbackModeLocalLine)    ||
        (loopbackMode == cAtPdhLoopbackModeRemoteLine)   ||
        (loopbackMode == cAtPdhLoopbackModeLocalPayload) ||
        (loopbackMode == cAtPdhLoopbackModeRemotePayload))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 DefaultFrameMode(AtPdhChannel self)
    {
	AtUnused(self);
    /* Concrete should know */
    return 0;
    }

static eBool FrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
	AtUnused(frameType);
	AtUnused(self);
    /* Concrete should know */
    return cAtFalse;
    }

static uint8 NumberOfSubChannelsGet(AtPdhChannel self)
    {
    return self->numSubChannels;
    }

static AtPdhChannel SubChannelGet(AtPdhChannel self, uint8 subChannelId)
    {
    if (self->subChannels == NULL)
        return NULL;

    if (subChannelId < self->numSubChannels)
        return self->subChannels[subChannelId];

    return NULL;
    }

static AtPdhChannel ParentChannelGet(AtPdhChannel self)
    {
    return self->parent;
    }

static eAtRet SubChannelsInterruptDisable(AtPdhChannel self, eBool onlySubChannels)
    {
    AtUnused(self);
    AtUnused(onlySubChannels);
    /* Let sub-class do itself. */
    return cAtOk;
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    AtUnused(self);
    AtUnused(subChannelsOnly);
    }

static eBool HasParentAlarmForwarding(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasAlarmForwarding(AtPdhChannel self)
    {
    uint32 defect = AtChannelDefectGet((AtChannel)self);
    return (defect & AtPdhChannelDefectsCauseAlarmForwarding(self)) ? cAtTrue : cAtFalse;
    }

static void FailureForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    AtUnused(self);
    AtUnused(subChannelsOnly);
    }

static void ErrorGeneratorReferenceRelease(AtPdhChannel self)
    {
    AtErrorGenerator generator = AtPdhChannelTxErrorGeneratorRefGet(self);
    if (generator != NULL)
        AtErrorGeneratorRefSourceRelease(generator);

    generator = AtPdhChannelRxErrorGeneratorRefGet(self);
    if (generator != NULL)
        AtErrorGeneratorRefSourceRelease(generator);
    }

static void ListenerDelete(AtObject listener)
    {
    AtOsalMemFree(listener);
    }

static void ListenerListDelete(AtPdhChannel self)
    {
    AtListDeleteWithObjectHandler(mThis(self)->listeners, ListenerDelete);
    self->listeners = NULL;
    }

static void Delete(AtObject self)
    {
    AtPdhChannelAllSubChannelsDelete((AtPdhChannel)self);
    AtObjectDelete((AtObject)mThis(self)->lineBerController);
    mThis(self)->lineBerController = NULL;
    AtObjectDelete((AtObject)mThis(self)->pathBerController);
    mThis(self)->pathBerController = NULL;
    ErrorGeneratorReferenceRelease((AtPdhChannel)self);
    ListenerListDelete((AtPdhChannel)self);

    m_AtObjectMethods->Delete(self);
    }

static eBool IdleSignalIsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet IdleEnable(AtPdhChannel self, eBool enable)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool IdleIsEnabled(AtPdhChannel self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet IdleCodeSet(AtPdhChannel self, uint8 idleCode)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(idleCode);
    return cAtErrorModeNotSupport;
    }

static uint8 IdleCodeGet(AtPdhChannel self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0x0;
    }

static eAtRet DataLinkSend(AtPdhChannel self, const uint8 *messageBuffer, uint32 bufferLength)
    {
    AtUnused(self);
    AtUnused(messageBuffer);
    AtUnused(bufferLength);
    return 0;
    }

static uint32 DataLinkReceive(AtPdhChannel self, uint8 *messageBuffer, uint32 bufferLength)
    {
    AtUnused(self);
    AtUnused(messageBuffer);
    AtUnused(bufferLength);
    return 0;
    }

static eAtRet DataLinkEnable(AtPdhChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool DataLinkIsEnabled(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DataLinkCounterGet(AtPdhChannel self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 DataLinkCounterClear(AtPdhChannel self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static eAtRet DataLinkFcsBitOrderSet(AtPdhChannel self, eAtBitOrder order)
    {
    AtUnused(self);
    AtUnused(order);
    /* Let concrete class do */
    return cAtError;
    }

static eAtBitOrder DataLinkFcsBitOrderGet(AtPdhChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtBitOrderUnknown;
    }

static eAtPdhChannelType TypeGet(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtPdhChannelTypeUnknown;
    }

static eAtRet CascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5)
    {
    AtUnused(self);
    AtUnused(phyModule);
    *layer1 = 0;
    *layer2 = 0;
    *layer3 = 0;
    *layer4 = 0;
    *layer5 = 0;
    return cAtErrorNotImplemented;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPdhChannel object = (AtPdhChannel)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeChannelIdString(vc);

    mEncodeObjects(subChannels, object->numSubChannels);
    mEncodeObject(pathBerController);
    mEncodeObject(lineBerController);
    mEncodeObjectDescription(txErrorGeneratorRef);
    mEncodeObjectDescription(rxErrorGeneratorRef);
    mEncodeChannelIdString(parent);
    mEncodeUInt(channelType);
    mEncodeNone(listeners);
    }

static AtPrbsEngine LinePrbsEngineGet(AtPdhChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static AtModuleBer BerModule(AtPdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (AtModuleBer)AtDeviceModuleGet(device, cAtModuleBer);
    }

static AtBerController PathBerControllerCreate(AtPdhChannel self)
    {
    return AtModuleBerPdhChannelPathBerControlerCreate(BerModule(self), (AtChannel)self);
    }

static AtBerController LineBerControllerCreate(AtPdhChannel self)
    {
    return AtModuleBerPdhChannelLineBerControlerCreate(BerModule(self), (AtChannel)self);
    }

static eBool LiuIsLoc(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasLineLayer(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtPdhChannelCache);
    }

static tAtPdhChannelCache *Cache(AtPdhChannel self)
    {
    return (tAtPdhChannelCache *)AtChannelCacheGet((AtChannel)self);
    }

static void CacheFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    tAtPdhChannelCache *cache = Cache(self);
    if (cache)
        cache->frameType = frameType;
    }

static uint16 CacheFrameTypeGet(AtPdhChannel self)
    {
    tAtPdhChannelCache *cache = Cache(self);
    return (uint16)(cache ? cache->frameType : 0);
    }

static eBool LedStateIsSupported(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsUnframed(AtPdhChannel self, uint16 frameType)
    {
    /* Sub class would know */
    AtUnused(self);
    AtUnused(frameType);
    return cAtFalse;
    }

static eAtRet LosDetectionEnable(AtPdhChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool LosDetectionIsEnabled(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet AisAllOnesDetectionEnable(AtPdhChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AisAllOnesDetectionIsEnabled(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtErrorGenerator TxErrorGeneratorGet(AtPdhChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static AtErrorGenerator RxErrorGeneratorGet(AtPdhChannel self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet BerControllerDefaultSet(AtPdhChannel self)
    {
    AtModuleBer berModule;
    AtBerController controller = AtPdhChannelPathBerControllerGet((AtPdhChannel)self);
    if (controller == NULL)
        return cAtOk;

    /* To avoid unnecessary memory will be created for BER controller, only
     * explicitly enable BER engine when the BER module is expecting that */
    berModule  = (AtModuleBer)AtBerControllerModuleGet(controller);
    if (AtModuleBerChannelBerShouldBeEnabledByDefault(berModule))
        return AtBerControllerEnable(controller, cAtTrue);

    return cAtOk;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;
    uint8 i;

    if (m_AtChannelMethods->ServiceIsRunning(self))
        return cAtTrue;

    /* Just ask its sub channels */
    for (i = 0; i < AtPdhChannelNumberOfSubChannelsGet(channel); i++)
        {
        if (AtChannelServiceIsRunning((AtChannel)AtPdhChannelSubChannelGet(channel, i)))
            return cAtTrue;
        }

    return cAtFalse;
    }

static uint32 DefectsCauseAlarmForwarding(AtPdhChannel self)
    {
    AtUnused(self);
    /* Let sub-class determine. */
    return 0;
    }

static AtSdhVc VcGet(AtPdhChannel self)
    {
    return AtPdhChannelVcInternalGet(self);
    }

static eBool CanJoinVcg(AtChannel self, AtConcateGroup vcg)
    {
    eAtPdhChannelType channelType = AtPdhChannelTypeGet((AtPdhChannel)self);
    uint8 memberType = AtConcateGroupMemberTypeGet(vcg);

    switch (memberType)
        {
        case cAtConcateMemberTypeDs1:     return (channelType == cAtPdhChannelTypeDs1) ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeE1:      return (channelType == cAtPdhChannelTypeE1)  ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeDs3:     return (channelType == cAtPdhChannelTypeDs3) ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeE3:      return (channelType == cAtPdhChannelTypeE3)  ? cAtTrue : cAtFalse;

        default:    return cAtFalse;
        }
    }

static eAtRet SubChannelsHardwareCleanup(AtPdhChannel self)
    {
    uint8 numSubChannels, subChannels_i;
    eAtRet ret = cAtOk;

    numSubChannels = self->numSubChannels;
    if (numSubChannels == 0)
        return ret;

    for (subChannels_i = 0; subChannels_i < numSubChannels; subChannels_i++)
        {
        AtChannel channel = (AtChannel)self->subChannels[subChannels_i];
        if (channel)
            ret |= AtChannelHardwareCleanup(channel);
        }

    return ret;
    }

static eAtRet HardwareCleanup(AtChannel self)
    {
    eAtRet ret;
    AtPdhChannel channel = (AtPdhChannel)self;

    ret = m_AtChannelMethods->HardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    if (AtPdhChannelIdleSignalIsSupported(channel))
        ret |= AtPdhChannelIdleEnable(channel, cAtFalse);

    ret |= AtChannelLoopbackSet(self, cAtPdhLoopbackModeRelease);
    if (ret != cAtOk)
        return ret;

    return SubChannelsHardwareCleanup(channel);
    }

static eAtRet AllServicesDestroy(AtChannel self)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    uint8 subChannel_i;
    eAtRet ret = cAtOk;

    for (subChannel_i = 0; subChannel_i < AtPdhChannelNumberOfSubChannelsGet(pdhChannel); subChannel_i++)
        {
        AtPdhChannel subChannel = AtPdhChannelSubChannelGet(pdhChannel, subChannel_i);
        ret |= AtChannelAllServicesDestroy((AtChannel)subChannel);
        }

    return ret;
    }

static AtList ListenerList(AtPdhChannel self)
    {
    if (mThis(self)->listeners == NULL)
        mThis(self)->listeners = AtListCreate(0);

    return mThis(self)->listeners;
    }

static tListenerWraper* NewListenerWrapper(void* userData, const tAtPdhChannelPropertyListener* listener)
    {
    tListenerWraper* newWrapper = AtOsalMemAlloc(sizeof(tListenerWraper));
    if (newWrapper == NULL)
        return NULL;

    AtOsalMemInit(newWrapper, 0, sizeof(tListenerWraper));
    newWrapper->userData = userData;
    newWrapper->listener.FrameTypeDidChange = listener->FrameTypeDidChange;
    return newWrapper;
    }

static tListenerWraper* ListenerWrapperSearch(AtPdhChannel self, void* userData, const tAtPdhChannelPropertyListener* listener)
    {
    AtIterator iterator;
    tListenerWraper* wrapper = NULL;

    if (mThis(self)->listeners == NULL)
        return NULL;

    iterator = AtListIteratorCreate(mThis(self)->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if ((wrapper->userData == userData) &&
            (wrapper->listener.FrameTypeDidChange == listener->FrameTypeDidChange))
            break;
        }

    AtObjectDelete((AtObject)iterator);
    return wrapper;
    }

static void FrameTypeDidChangeNotify(AtPdhChannel self, uint16 preFrameType, uint16 newFrameType)
    {
    tListenerWraper* wrapper;
    AtIterator iterator;

    if (mThis(self)->listeners == NULL)
        return;

    iterator = AtListIteratorCreate(mThis(self)->listeners);
    while ((wrapper = (tListenerWraper*)AtIteratorNext(iterator)) != NULL)
        {
        if (wrapper->listener.FrameTypeDidChange)
            wrapper->listener.FrameTypeDidChange(self, preFrameType, newFrameType, wrapper->userData);
        }

    AtObjectDelete((AtObject)iterator);
    }

static const char *LoopbackMode2String(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);

    switch (loopbackMode)
        {
        case cAtPdhLoopbackModeRelease      : return "release";
        case cAtPdhLoopbackModeLocalPayload : return "payloadloopin";
        case cAtPdhLoopbackModeRemotePayload: return "payloadloopout";
        case cAtPdhLoopbackModeLocalLine    : return "lineloopin";
        case cAtPdhLoopbackModeRemoteLine   : return "lineloopout";
        default:
            return "unknown";
        }
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtPdhChannelSharedQuerier();
    }

static eAtRet AlarmAffectingEnable(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    AtUnused(enable);
    AtUnused(alarmType);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool AlarmAffectingIsEnabled(AtPdhChannel self, uint32 alarmType)
    {
    AtUnused(alarmType);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool CanChangeAlarmAffecting(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    AtUnused(alarmType);
    AtUnused(self);
    AtUnused(enable);

    /* Default all alarm cannot change affecting, concrete class do differently may override */
    return cAtFalse;
    }

static uint32 AlarmAffectingMaskGet(AtPdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet AlarmAffectingDefaultSet(AtPdhChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePdhRet DoFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret;
    AtModule modulePdh = AtChannelModuleGet((AtChannel)self);
    uint16 currentFrameType = AtPdhChannelFrameTypeGet(self);

    AtModuleLock(modulePdh);
    ret = AtPdhChannelFrameTypeNoLockSet(self, frameType);
    AtModuleUnLock(modulePdh);

    if (ret == cAtOk)
        FrameTypeDidChangeNotify(self, currentFrameType, frameType);

    return ret;
    }

static void StatusClear(AtChannel self)
    {
    AtPdhChannel sdhChannel = (AtPdhChannel)self;
    uint8 subChannel_i;

    /* Or all of sub channels */
    for (subChannel_i = 0; subChannel_i < AtPdhChannelNumberOfSubChannelsGet(sdhChannel); subChannel_i++)
        {
        AtPdhChannel subChannel = AtPdhChannelSubChannelGet(sdhChannel, subChannel_i);
        AtChannelStatusClear((AtChannel)subChannel);
        }

    /* And status of itself */
    m_AtChannelMethods->StatusClear(self);
    }

static void OverrideAtObject(AtPdhChannel self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPdhChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackMode2String);
        mMethodOverride(m_AtChannelOverride, CacheSize);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, CanJoinVcg);
        mMethodOverride(m_AtChannelOverride, HardwareCleanup);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        mMethodOverride(m_AtChannelOverride, AllServicesDestroy);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(AtPdhChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, FrameTypeIsSupported);
        mMethodOverride(m_methods, FrameTypeSet);
        mMethodOverride(m_methods, FrameTypeGet);
        mMethodOverride(m_methods, IsUnframed);
        mMethodOverride(m_methods, LineCodeSet);
        mMethodOverride(m_methods, LineCodeGet);
        mMethodOverride(m_methods, NationalBitsSupported);
        mMethodOverride(m_methods, NationalBitsSet);
        mMethodOverride(m_methods, NationalBitsGet);
        mMethodOverride(m_methods, RxNationalBitsGet);
        mMethodOverride(m_methods, LedStateSet);
        mMethodOverride(m_methods, LedStateGet);
        mMethodOverride(m_methods, PathBerControllerGet);
        mMethodOverride(m_methods, LineBerControllerGet);
        mMethodOverride(m_methods, AutoRaiEnable);
        mMethodOverride(m_methods, AutoRaiIsEnabled);
        mMethodOverride(m_methods, DefaultFrameMode);
        mMethodOverride(m_methods, NumberOfSubChannelsGet);
        mMethodOverride(m_methods, SubChannelGet);
        mMethodOverride(m_methods, ParentChannelGet);
        mMethodOverride(m_methods, IdleSignalIsSupported);
        mMethodOverride(m_methods, IdleEnable);
        mMethodOverride(m_methods, IdleIsEnabled);
        mMethodOverride(m_methods, IdleCodeSet);
        mMethodOverride(m_methods, IdleCodeGet);
        mMethodOverride(m_methods, DataLinkSend);
        mMethodOverride(m_methods, DataLinkReceive);
        mMethodOverride(m_methods, DataLinkEnable);
        mMethodOverride(m_methods, DataLinkIsEnabled);
        mMethodOverride(m_methods, DataLinkCounterGet);
        mMethodOverride(m_methods, DataLinkCounterClear);
        mMethodOverride(m_methods, TypeGet);
        mMethodOverride(m_methods, LinePrbsEngineGet);
        mMethodOverride(m_methods, PathBerControllerCreate);
        mMethodOverride(m_methods, LineBerControllerCreate);
        mMethodOverride(m_methods, TxSlipBufferEnable);
        mMethodOverride(m_methods, TxSlipBufferIsEnabled);
        mMethodOverride(m_methods, TxSsmSet);
        mMethodOverride(m_methods, TxSsmGet);
        mMethodOverride(m_methods, RxSsmGet);
        mMethodOverride(m_methods, SsmEnable);
        mMethodOverride(m_methods, SsmIsEnabled);
        mMethodOverride(m_methods, LedStateIsSupported);
        mMethodOverride(m_methods, LosDetectionEnable);
        mMethodOverride(m_methods, LosDetectionIsEnabled);
        mMethodOverride(m_methods, SubChannelsInterruptDisable);
        mMethodOverride(m_methods, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_methods, HasParentAlarmForwarding);
        mMethodOverride(m_methods, HasAlarmForwarding);
        mMethodOverride(m_methods, FailureForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_methods, TxErrorGeneratorGet);
        mMethodOverride(m_methods, RxErrorGeneratorGet);
        mMethodOverride(m_methods, DataLinkFcsBitOrderSet);
        mMethodOverride(m_methods, DataLinkFcsBitOrderGet);
        mMethodOverride(m_methods, DefectsCauseAlarmForwarding);
        mMethodOverride(m_methods, CascadeHwIdGet);
        mMethodOverride(m_methods, AisAllOnesDetectionEnable);
        mMethodOverride(m_methods, AisAllOnesDetectionIsEnabled);
        mMethodOverride(m_methods, VcGet);
        mMethodOverride(m_methods, LiuIsLoc);
        mMethodOverride(m_methods, HasLineLayer);
        mMethodOverride(m_methods, AlarmAffectingEnable);
        mMethodOverride(m_methods, AlarmAffectingIsEnabled);
        mMethodOverride(m_methods, AlarmAffectingMaskGet);
        mMethodOverride(m_methods, CanChangeAlarmAffecting);
        mMethodOverride(m_methods, AlarmAffectingDefaultSet);
        mMethodOverride(m_methods, RxSlipBufferIsSupported);
        mMethodOverride(m_methods, RxSlipBufferEnable);
        mMethodOverride(m_methods, RxSlipBufferIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

/* To initialize PDH channel object */
AtPdhChannel AtPdhChannelObjectInit(AtPdhChannel self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPdhChannel));

    /* Super constructor should be called first */
    if (AtChannelObjectInit((AtChannel)self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Initialize Methods */
    Override(self);
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

void AtPdhChannelVcSet(AtPdhChannel self, AtSdhVc vc1x)
    {
    self->vc = vc1x;
    }

void AtPdhChannelAllSubChannelsDelete(AtPdhChannel self)
    {
    uint8 channel_i;
    AtOsal osal = AtSharedDriverOsalGet();

    for (channel_i = 0; channel_i < self->numSubChannels; channel_i++)
        {
        AtObjectDelete((AtObject)self->subChannels[channel_i]);
        }

    mMethodsGet(osal)->MemFree(osal, self->subChannels);
    self->numSubChannels = 0;
    self->subChannels = NULL;
    }

void AtPdhChannelAllSubChannelsSet(AtPdhChannel self, AtPdhChannel *subChannels, uint8 numSubChannels)
    {
    self->subChannels    = subChannels;
    self->numSubChannels = numSubChannels;
    }

void AtPdhChannelParentChannelSet(AtPdhChannel self, AtPdhChannel parent)
    {
    self->parent = parent;
    }

AtPdhChannel *AtPdhChannelAllSubChannelsCreate(AtPdhChannel self,
                                              uint8 numSubChannels,
                                              AtPdhChannel(*SubChannelCreate)(AtPdhChannel self, uint8 subChannelId))
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhChannel *newSubChannels = NULL;
    uint8 subChannel_i;

    newSubChannels = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtPdhChannel) * numSubChannels);
    if (newSubChannels == NULL)
        return NULL;

    /* Create all sub channels */
    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        AtPdhChannel newSubChannel = SubChannelCreate(self, subChannel_i);

        /* Create fail, need to free all of resources created before */
        if (newSubChannel == NULL)
            {
            uint8 createdChannel_i;

            for (createdChannel_i = 0; createdChannel_i < subChannel_i; createdChannel_i++)
                AtObjectDelete((AtObject)self->subChannels[createdChannel_i]);

            mMethodsGet(osal)->MemFree(osal, newSubChannels);

            return NULL;
            }

        /* Setup and cache this */
        AtPdhChannelParentChannelSet(newSubChannel, self);
        AtChannelInit((AtChannel)newSubChannel);
        newSubChannels[subChannel_i] = newSubChannel;
        }

    return newSubChannels;
    }

AtPdhChannel *AtPdhChannelAllSubChannelsGet(AtPdhChannel self, uint8 *numSubChannels)
    {
    if (self == NULL)
        return NULL;

    if (numSubChannels)
        *numSubChannels = self->numSubChannels;

    return self->subChannels;
    }

eBool AtPdhChannelLedStateIsSupported(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->LedStateIsSupported(self);

    return cAtFalse;
    }

/*
 * Get BER controller that monitors BER for this channel
 *
 * @param self This channel
 * @return BER controller
 */
AtBerController AtPdhChannelBerControllerGet(AtPdhChannel self)
    {
    return AtPdhChannelPathBerControllerGet(self);
    }

eAtRet AtPdhChannelSubChannelsInterruptDisable(AtPdhChannel self, eBool onlySubChannels)
    {
    if (self)
        return mMethodsGet(self)->SubChannelsInterruptDisable(self, onlySubChannels);
    return cAtErrorNullPointer;
    }

void AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    if (self == NULL)
        return;

    if (AtModulePdhNeedClearanceNotifyLogic((AtModulePdh)AtChannelModuleGet((AtChannel)self)))
        mMethodsGet(self)->AlarmForwardingClearanceNotifyWithSubChannelOnly(self, subChannelsOnly);
    }

eBool AtPdhChannelHasParentAlarmForwarding(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->HasParentAlarmForwarding(self);
    return cAtFalse;
    }

eBool AtPdhChannelHasAlarmForwarding(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->HasAlarmForwarding(self);
    return cAtFalse;
    }

void AtPdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    if (self == NULL)
        return;

    if (AtModulePdhNeedClearanceNotifyLogic((AtModulePdh)AtChannelModuleGet((AtChannel)self)))
        mMethodsGet(self)->FailureForwardingClearanceNotifyWithSubChannelOnly(self, subChannelsOnly);
    }

void AtPdhChannelTxErrorGeneratorRefSet(AtPdhChannel self, AtErrorGenerator generator)
    {
    if (self)
        mThis(self)->txErrorGeneratorRef = generator;
    }

AtErrorGenerator AtPdhChannelTxErrorGeneratorRefGet(AtPdhChannel self)
    {
    if (self)
        return mThis(self)->txErrorGeneratorRef;
    return NULL;
    }

void AtPdhChannelRxErrorGeneratorRefSet(AtPdhChannel self, AtErrorGenerator generator)
    {
    if (self)
        mThis(self)->rxErrorGeneratorRef = generator;
    }

AtErrorGenerator AtPdhChannelRxErrorGeneratorRefGet(AtPdhChannel self)
    {
    if (self)
        return mThis(self)->rxErrorGeneratorRef;
    return NULL;
    }

eAtRet AtPdhChannelFrameTypeNoLockSet(AtPdhChannel self, uint16 frameType)
    {
	AtAttController att = AtChannelAttController((AtChannel)self);
	if (att)
		AtAttControllerDe1CrcGapSet(att, frameType);

    mNumericalAttributeSetWithCache(FrameTypeSet, frameType);
    }

eAtRet AtPdhChannelBerControllerDefaultSet(AtPdhChannel self)
    {
    if (self)
        return BerControllerDefaultSet(self);
    return cAtErrorObjectNotExist;
    }

uint32 AtPdhChannelDefectsCauseAlarmForwarding(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->DefectsCauseAlarmForwarding(self);
    return 0;
    }

eAtRet AtPdhChannelCascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5)
    {
    if (self)
        return mMethodsGet(self)->CascadeHwIdGet(self, phyModule, layer1, layer2, layer3, layer4, layer5);
    return cAtErrorNullPointer;
    }

eAtRet AtPdhChannelAisAllOnesDetectionEnable(AtPdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(AisAllOnesDetectionEnable, enable);
    }

eBool AtPdhChannelAisAllOnesDetectionIsEnabled(AtPdhChannel self)
    {
    mAttributeGet(AisAllOnesDetectionIsEnabled, eBool, cAtFalse);
    }

/* There is case an PDH channel has a mapped VC but it doesn't want to public to user */
AtSdhVc AtPdhChannelVcInternalGet(AtPdhChannel self)
    {
    if (self)
        return self->vc;
    return NULL;
    }

eBool AtPdhChannelLiuIsLoc(AtPdhChannel self)
    {
    mAttributeGet(LiuIsLoc, eBool, cAtFalse)
    }

eBool AtPdhChannelHasLineLayer(AtPdhChannel self)
    {
    mAttributeGet(HasLineLayer, eBool, cAtFalse)
    }

eAtRet AtPdhChannelPropertyListenerAdd(AtPdhChannel self, const tAtPdhChannelPropertyListener* listener, void* userData)
    {
    if (listener == NULL)
        return cAtErrorNullPointer;

    /* Identical listener is existing */
    if (ListenerWrapperSearch(self, userData, listener))
        return cAtOk;

    return AtListObjectAdd(ListenerList(self), (AtObject)NewListenerWrapper(userData, listener));
    }

eAtRet AtPdhChannelPropertyListenerRemove(AtPdhChannel self, const tAtPdhChannelPropertyListener* listener, void* userData)
    {
    eAtRet ret;
    tListenerWraper* wrapper = ListenerWrapperSearch(self, userData, listener);
    if (wrapper == NULL)
        return cAtOk;

    ret = AtListObjectRemove(mThis(self)->listeners, (AtObject)wrapper);
    AtOsalMemFree(wrapper);
    return ret;
    }

eAtRet AtPdhChannelSubChannelsHardwareCleanup(AtPdhChannel self)
    {
    if (self)
        return SubChannelsHardwareCleanup(self);
    return cAtErrorNullPointer;
    }

uint32 AtPdhChannelAlarmAffectingMaskGet(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->AlarmAffectingMaskGet(self);
    return 0;
    }

eBool AtPdhChannelCanChangeAlarmAffecting(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->CanChangeAlarmAffecting(self, alarmType, enable);
    return cAtFalse;
    }

eAtRet AtPdhChannelAlarmAffectingDisable(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->AlarmAffectingDefaultSet(self, cAtFalse);
    return cAtErrorNullPointer;
    }

eAtRet AtPdhChannelAlarmAffectingRestore(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->AlarmAffectingDefaultSet(self, cAtTrue);
    return cAtErrorNullPointer;
    }

void AtPdhChannelCacheFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    CacheFrameTypeSet(self, frameType);
    }

/**
 * @addtogroup AtPdhChannel
 * @{
 */

/**
 * Get TX error generator
 *
 * @param self This channel
 *
 * @return TX error generator
 */
AtErrorGenerator AtPdhChannelTxErrorGeneratorGet(AtPdhChannel self)
    {
    mAttributeGet(TxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

/**
 * Get RX error generator
 *
 * @param self This channel
 *
 * @return RX error generator
 */
AtErrorGenerator AtPdhChannelRxErrorGeneratorGet(AtPdhChannel self)
    {
    mAttributeGet(RxErrorGeneratorGet, AtErrorGenerator, NULL);
    }

/**
 * Check if a specified frame type is supported
 *
 * @param self This channel
 * @param frameType Frame type. Refer
 *                  - @ref eAtPdhDe1FrameType "DS1/E1 frame types"
 *                  - @ref eAtPdhDe3FrameType "DS3/E3 frame types"
 *
 * @retval cAtTrue if supported
 * @retval cAtTrue if not supported
 */
eBool AtPdhChannelFrameTypeIsSupported(AtPdhChannel self, uint16 frameType)
    {
    mOneParamAttributeGet(FrameTypeIsSupported, frameType, eBool, cAtFalse);
    }

/**
 * Set frame type of PDH channel
 *
 * @param self This channel
 * @param frameType Frame type. Refer
 *                  - @ref eAtPdhDe1FrameType "DS1/E1 frame types"
 *                  - TBD
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelFrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    mOneParamWrapCall(frameType, eAtModulePdhRet, DoFrameTypeSet(self, frameType));
    }

/**
 * Get frame type of PDH channel
 *
 * @param self This PDH chanenl
 *
 * @return Frame type of this channel. Refer
 *         - @ref eAtPdhDe1FrameType "DS1/E1 frame types"
 *         - TBD
 */
uint16 AtPdhChannelFrameTypeGet(AtPdhChannel self)
    {
    mAttributeGetWithCache(FrameTypeGet, uint16, 0);
    }

/**
 * Set line code of PDH channel
 *
 * @param self This PDH channel
 * @param lineCode Line code. Refer:
 *                 - @ref eAtPdhDe1LineCode "DS1/E1 Line Codes"
 *                 - TBD
 *
 * @return AT return codes
 */
eAtModulePdhRet AtPdhChannelLineCodeSet(AtPdhChannel self, uint16 lineCode)
    {
    mNumericalAttributeSet(LineCodeSet, lineCode);
    }

/**
 * Get Line code of PDH channel
 *
 * @param self This PDH channel
 *
 * @return Line code. Refer:
 *         - @ref eAtPdhDe1LineCode "DS1/E1 Line Codes"
 *         - TBD
 */
uint16 AtPdhChannelLineCodeGet(AtPdhChannel self)
    {
    mAttributeGet(LineCodeGet, uint16, 0);
    }

/**
 * Set LED state. Turn it ON/OFF or make it blink
 *
 * @param self This channel
 * @param ledState @ref eAtLedState "LED states"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelLedStateSet(AtPdhChannel self, eAtLedState ledState)
    {
    mNumericalAttributeSet(LedStateSet, ledState);
    }

/**
 * Get LED state to determine it is ON/OFF or blinking
 *
 * @param self This channel
 *
 * @return @ref eAtLedState "LED state"
 */
eAtLedState AtPdhChannelLedStateGet(AtPdhChannel self)
    {
    mAttributeGet(LedStateGet, eAtLedState, cAtLedStateOff);
    }

/**
 * Send datalink message (DS1/E1 FDL, DS3 MDL)
 *
 * @param self This channel
 * @param messageBuffer Message buffer
 * @param length Buffer length
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelDataLinkSend(AtPdhChannel self, const uint8 *messageBuffer, uint32 length)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->DataLinkSend(self, messageBuffer, length);

    return cAtError;
    }

/**
 * Get received datalink message (DS1/E1 FDL, DS3 MDL) if it is available
 *
 * @param self This channel
 * @param [out] messageBuffer Message buffer
 * @param bufferLength Buffer length
 *
 * @return Message length
 */
uint32 AtPdhChannelDataLinkReceive(AtPdhChannel self, uint8 *messageBuffer, uint32 bufferLength)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->DataLinkReceive(self, messageBuffer, bufferLength);

    return 0;
    }

/**
 * Enable DLK channel (DS1/E1 FDL, DS3 MDL)
 *
 * @param self This channel
 * @param enable Enable
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelDataLinkEnable(AtPdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(DataLinkEnable, enable);
    }

/**
 * Get enable status of DLK channel (DS1/E1 FDL, DS3 MDL)
 *
 * @param self This channel
 *
 * @return cAtTrue if DLK is enable
 */
eBool AtPdhChannelDataLinkIsEnabled(AtPdhChannel self)
    {
    mAttributeGet(DataLinkIsEnabled, eBool, cAtFalse);
    }

/**
 * Get statistic counter of DLK channel (DS1/E1 FDL, DS3 MDL)
 *
 * @param self This channel
 * @param counterType Type of statistic of DLK
 *
 * @return Number of statistic counter
 */
uint32 AtPdhChannelDataLinkCounterGet(AtPdhChannel self, uint32 counterType)
    {
    mOneParamAttributeGet(DataLinkCounterGet, counterType, uint32, 0);
    }

/**
 * Get statistic counter of DLK channel (DS1/E1 FDL, DS3 MDL)
 *
 * @param self This channel
 * @param counterType Type of statistic of DLK
 *
 * @return Number of statistic counter, and clear the accumulation
 */
uint32 AtPdhChannelDataLinkCounterClear(AtPdhChannel self, uint32 counterType)
    {
    mOneParamAttributeGet(DataLinkCounterClear, counterType, uint32, 0);
    }

/**
 * Get national bit pattern current status. This interface is applicable for E1 and E3
 *
 * @param self This channel
 *
 * @return National bit pattern
 *         - [0..31]: For E1
 *         - [0..1]: For E3
 */
uint8 AtPdhChannelRxNationalBitsGet(AtPdhChannel self)
    {
    mAttributeGet(RxNationalBitsGet, uint8, 0);
    }

/**
 * Set national bit pattern Tx direct. This interface is applicable for E1 and E3
 *
 * @param self This channel
 * @param pattern National bit pattern
 *                - [0..31]: for E1
 *                - [0..1]: for E3
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelNationalBitsSet(AtPdhChannel self, uint8 pattern)
    {
    mNumericalAttributeSet(NationalBitsSet, pattern);
    }

/**
 * Get national bit pattern Tx direct. This interface is applicable for E1 and E3
 *
 * @param self This channel
 *
 * @return National bit pattern
 *         - [0..31]: For E1
 *         - [0..1]: For E3
 */
uint8 AtPdhChannelNationalBitsGet(AtPdhChannel self)
    {
    mAttributeGet(NationalBitsGet, uint8, 0);
    }

/**
 * Check if national bits feature is supported
 *
 * @param self This channel
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtPdhChannelNationalBitsSupported(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->NationalBitsSupported(self);
    return cAtFalse;
    }

/**
 * Get BER controller that monitors BER at path layer of this channel
 *
 * @param self This channel
 * @return BER controller
 */
AtBerController AtPdhChannelPathBerControllerGet(AtPdhChannel self)
    {
    mNoParamObjectGet(PathBerControllerGet, AtBerController);
    }

/**
 * Get BER controller that monitors BER at line layer of this channel
 *
 * @param self This channel
 * @return BER controller
 */
AtBerController AtPdhChannelLineBerControllerGet(AtPdhChannel self)
    {
    mNoParamObjectGet(LineBerControllerGet, AtBerController);
    }

/**
 * Enable/disable backward RAI alarm when critical alarms happen
 *
 * @param self This channel
 * @param enable Enable/disable backward RAI
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 *
 * @note This API is applicable for @ref AtPdhDe1 and @ref AtPdhDe3 subclasses
 */
eAtRet AtPdhChannelAutoRaiEnable(AtPdhChannel self, eBool enable)
	{
    mNumericalAttributeSet(AutoRaiEnable, enable);
    }

/**
 * Check if RAI backward is enabled or not
 *
 * @param self This channel
 *
 * @retval cAtTrue If enabled
 * @retval cAtFalse If disabled
 */
eBool AtPdhChannelAutoRaiIsEnabled(AtPdhChannel self)
	{
    mAttributeGet(AutoRaiIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable AIS downstream.
 *
 * @param self This channel
 * @param alarmType Alarm type.
 *                  - @ref eAtPdhDe1AlarmType "DS1/E1 alarm types"
 *                  - @ref eAtPdhDe2AlarmType "DS2/E2 alarm types"
 *                  - @ref eAtPdhDe3AlarmType "DS3/E3 alarm types"
 * @param enable [cAtTrue, cAtFalse] to enable or disable alarm affecting
 *
 * @return AT return code
 *
 * @note This API is applicable for @ref AtPdhDe3, @ref AtPdhDe2 and @ref AtPdhDe1 subclasses
 */
eAtModulePdhRet AtPdhChannelAlarmAffectingEnable(AtPdhChannel self, uint32 alarmType, eBool enable)
    {
    mTwoParamsAttributeSet(AlarmAffectingEnable, alarmType, enable);
    }

/**
 * Check if alarm affecting is enabled
 *
 * @param self This channel
 * @param alarmType Alarm type.
 *                  - @ref eAtPdhDe1AlarmType "DS1/E1 alarm types"
 *                  - @ref eAtPdhDe2AlarmType "DS2/E2 alarm types"
 *                  - @ref eAtPdhDe3AlarmType "DS3/E3 alarm types"
 *
 * @retval cAtTrue If enabled
 * @retval cAtFalse If disabled
 */
eBool AtPdhChannelAlarmAffectingIsEnabled(AtPdhChannel self, uint32 alarmType)
    {
    mOneParamAttributeGet(AlarmAffectingIsEnabled, alarmType, eBool, cAtFalse);
    }

/**
 * Get number of sub-channels
 *
 * @param self This channel
 *
 * @return Number of sub-channels
 */
uint8 AtPdhChannelNumberOfSubChannelsGet(AtPdhChannel self)
    {
    mAttributeGet(NumberOfSubChannelsGet, uint8, 0);
    }

/**
 * Get a sub-channel of this channel in mapping tree.
 *
 * @param self This channel
 * @param subChannelId Sub-channel ID
 *
 * @return A sub-channel on success or NULL on failure
 */
AtPdhChannel AtPdhChannelSubChannelGet(AtPdhChannel self, uint8 subChannelId)
    {
    mOneParamObjectGet(SubChannelGet, AtPdhChannel, subChannelId);
    }

/**
 * Get parent of this channel in mapping tree
 *
 * @param self This channel
 *
 * @return Parent channel
 */
AtPdhChannel AtPdhChannelParentChannelGet(AtPdhChannel self)
    {
    mNoParamObjectGet(ParentChannelGet, AtPdhChannel);
    }

/**
 * Check if IDLE signal is supported
 *
 * @param self This channel
 *
 * @return cAtTrue: supported, cAtFalse: Unsupported
 */
eBool AtPdhChannelIdleSignalIsSupported(AtPdhChannel self)
    {
    mAttributeGet(IdleSignalIsSupported, eBool, cAtFalse);
    }

/**
 * Enable/disable IDLE code transmitting
 *
 * @param self This channel
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelIdleEnable(AtPdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(IdleEnable, enable);
    }

/**
 * Check if IDLE code transmitting is enabled
 *
 * @param self This channel
 *
 * @retval cAtTrue IDLE code transmitting is enabled
 * @retval cAtFalse IDLE code transmitting is disabled 
 */
eBool AtPdhChannelIdleIsEnabled(AtPdhChannel self)
    {
    mAttributeGet(IdleIsEnabled, eBool, cAtFalse);
    }

/**
 * Set IDLE code to transmit
 *
 * @param self This channel
 * @param idleCode IDLE code
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelIdleCodeSet(AtPdhChannel self, uint8 idleCode)
    {
    mNumericalAttributeSet(IdleCodeSet, idleCode);
    }

/**
 * Get IDLE code to transmit
 *
 * @param self This channel
 *
 * @return IDLE code
 */
uint8 AtPdhChannelIdleCodeGet(AtPdhChannel self)
    {
    mAttributeGet(IdleCodeGet, uint8, 0);
    }

/**
 * Get the SDH VC that this channel is mapped to
 *
 * @param self This channel
 *
 * @return SDH VC or NULL if this channel is not mapped to any SDH VC.
 */
AtSdhVc AtPdhChannelVcGet(AtPdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->VcGet(self);

    return NULL;
    }

/**
 * Get PDH channel type
 *
 * @param self This channel
 * @return @ref eAtPdhChannelType "PDH channel type"
 */
eAtPdhChannelType AtPdhChannelTypeGet(AtPdhChannel self)
    {
    mAttributeGet(TypeGet, eAtPdhChannelType, cAtPdhChannelTypeUnknown);
    }

/**
 * Get Line PRBS engine associated with this channel
 *
 * @param self This channel
 * @return AtPrbsEngine object if this channel support PRBS, otherwise, NULL is
 *         returned
 */
AtPrbsEngine AtPdhChannelLinePrbsEngineGet(AtPdhChannel self)
    {
    mNoParamObjectGet(LinePrbsEngineGet, AtPrbsEngine);
    }

/**
 * Enable/disable slip buffer for the direction PSN to TDM
 *
 * @param self This channel
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtPdhChannelTxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(TxSlipBufferEnable, enable);
    }

/**
 * Check if slip buffer for direction from PSN to TDM is enabled or not.
 *
 * @param self This channel 
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPdhChannelTxSlipBufferIsEnabled(AtPdhChannel self)
    {
    mAttributeGet(TxSlipBufferIsEnabled, eBool, cAtFalse);
    }

/**
 * Get current value of SSM. This interface is applicable for E1 frame, E3 and T3.
 *
 * @param self This channel
 *
 * @return National bit pattern
 *         - [0..31]: For E1
 *         - [0..1]: For E3
 */
uint8 AtPdhChannelRxSsmGet(AtPdhChannel self)
    {
    mAttributeGet(RxSsmGet, uint8, 0);
    }

/**
 * Set Tx SSM value. This interface is applicable for E1 frame, E3 and T3.
 *
 * @param self This channel
 * @param value SSM value
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelTxSsmSet(AtPdhChannel self, uint8 value)
    {
    mNumericalAttributeSet(TxSsmSet, value);
    }

/**
 * Get Tx SSM value. This interface is applicable for E1 frame, E3 and T3.
 *
 * @param self This channel
 *
 * @return value SSM value
 */
uint8 AtPdhChannelTxSsmGet(AtPdhChannel self)
    {
    mAttributeGet(TxSsmGet, uint8, 0);
    }

/**
 * Enable/disable SSM code transmitting
 *
 * @param self This channel
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhChannelSsmEnable(AtPdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(SsmEnable, enable);
    }

/**
 * Check if SSM code transmitting is enabled
 *
 * @param self This channel
 *
 * @retval cAtTrue SSM code transmitting is enabled
 * @retval cAtFalse SSM code transmitting is disabled
 */
eBool AtPdhChannelSsmIsEnabled(AtPdhChannel self)
    {
    mAttributeGet(SsmIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable RX LOS detection in FPGA
 *
 * @param self This channel
 * @param enable LOS detection enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtPdhChannelLosDetectionEnable(AtPdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(LosDetectionEnable, enable);
    }

/**
 * Check if LOS detection is enabled or not.
 *
 * @param self This channel
 *
 * @retval cAtTrue if LOS detection is enabled
 * @retval cAtFalse  if LOS detection is disabled
 */
eBool AtPdhChannelLosDetectionIsEnabled(AtPdhChannel self)
    {
    mAttributeGet(LosDetectionIsEnabled, eBool, cAtFalse);
    }

/**
 * Set DataLink FCS bit ordering
 *
 * @param self This channel
 * @param bitOrder @ref eAtBitOrder "FCS Bit ordering"
 *
 * @return AT return code
 */
eAtRet  AtPdhChannelDataLinkFcsBitOrderSet(AtPdhChannel self, eAtBitOrder bitOrder)
    {
    mNumericalAttributeSet(DataLinkFcsBitOrderSet, bitOrder);
    }

/**
 * Get DataLink FCS bit ordering
 *
 * @param self This channel
 *
 * @return @ref eAtBitOrder "FCS Bit ordering"
 */
eAtBitOrder AtPdhChannelDataLinkFcsBitOrderGet(AtPdhChannel self)
    {
    mAttributeGet(DataLinkFcsBitOrderGet, eBool, cAtPrbsBitOrderUnknown);
    }

/**
 * Check if slip buffer for direction from TDM to PSN is supported or not.
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPdhChannelRxSlipBufferIsSupported(AtPdhChannel self)
    {
    mAttributeGet(RxSlipBufferIsSupported, eBool, cAtFalse);
    }

/**
 * Enable/disable slip buffer for the direction TDM to PSN
 *
 * @param self This channel
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtPdhChannelRxSlipBufferEnable(AtPdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(RxSlipBufferEnable, enable);
    }

/**
 * Check if slip buffer for direction from TDM to PSN is enabled or not.
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPdhChannelRxSlipBufferIsEnabled(AtPdhChannel self)
    {
    mAttributeGet(RxSlipBufferIsEnabled, eBool, cAtFalse);
    }

/**
 * @}
 */

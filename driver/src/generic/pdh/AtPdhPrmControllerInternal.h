/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhPrmControllerInternal.h
 *
 * Created Date: Oct 27, 2015
 *
 * Description : PRM Controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHPRMCONTROLLERINTERNAL_H_
#define _ATPDHPRMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtDriverInternal.h"
#include "AtPdhDe1.h"
#include "AtPdhPrmController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhPrmControllerMethods
    {
    /* Fcs Bit-order */
    eAtModulePdhRet (*FcsBitOrderSet)(AtPdhPrmController self, eAtBitOrder order);
    eAtBitOrder (*FcsBitOrderGet)(AtPdhPrmController self);

    /* PRM standard */
    eAtModulePdhRet (*StandardSet)(AtPdhPrmController self, eAtPdhDe1PrmStandard standard);
    eAtPdhDe1PrmStandard (*StandardGet)(AtPdhPrmController self);
    eAtModulePdhRet (*TxStandardSet)(AtPdhPrmController self, eAtPdhDe1PrmStandard standard);
    eAtPdhDe1PrmStandard (*TxStandardGet)(AtPdhPrmController self);
    eAtModulePdhRet (*RxStandardSet)(AtPdhPrmController self, eAtPdhDe1PrmStandard standard);
    eAtPdhDe1PrmStandard (*RxStandardGet)(AtPdhPrmController self);

    /* PRM status */
    uint32 (*CounterGet)(AtPdhPrmController self, uint16 counterType);
    uint32 (*CounterClear)(AtPdhPrmController self, uint16 counterType);
    uint32 (*AlarmInterruptGet)(AtPdhPrmController self);
    uint32 (*AlarmInterruptClear)(AtPdhPrmController self);

    /* PRM enabling */
    eAtModulePdhRet (*Enable)(AtPdhPrmController self, eBool enable);
    eBool (*IsEnabled)(AtPdhPrmController self);

    /* PRM C/R bit */
    eAtModulePdhRet (*TxCRBitSet)(AtPdhPrmController self, uint8 value);
    uint8 (*TxCRBitGet)(AtPdhPrmController self);
    eAtModulePdhRet (*CRBitExpectSet)(AtPdhPrmController self, uint8 value);
    uint8 (*CRBitExpectGet)(AtPdhPrmController self);

    /* PRM LB bit */
    eAtModulePdhRet (*TxLBBitSet)(AtPdhPrmController self, uint8 value);
    uint8 (*TxLBBitGet)(AtPdhPrmController self);
    uint8 (*RxLBBitGet)(AtPdhPrmController self);
    eAtModulePdhRet (*TxLBBitEnable)(AtPdhPrmController self, eBool enable);
    eBool (*TxLBBitIsEnabled)(AtPdhPrmController self);

    /* Interrupt process */
    uint32 (*DefectHistoryClear)(AtPdhPrmController self);
    uint32 (*DefectHistoryGet)(AtPdhPrmController self);
    eAtModulePdhRet (*InterruptMaskSet)(AtPdhPrmController self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtPdhPrmController self);
    void (*InterruptProcess)(AtPdhPrmController self, uint8 slice, uint8 de3, uint8 de2, uint8 de1, AtHal hal);

    eAtModulePdhRet (*Init)(AtPdhPrmController self);
    AtList (*CaptureRxMessages)(AtPdhPrmController self);
    eAtRet (*CaptureStart)(AtPdhPrmController self);
    eAtRet (*CaptureStop)(AtPdhPrmController self);
    eBool (*CaptureIsStarted)(AtPdhPrmController self);
    }tAtPdhPrmControllerMethods;

typedef struct tAtPdhPrmController
    {
    tAtObject super;
    const tAtPdhPrmControllerMethods *methods;

    /* Private data */
    AtPdhDe1 de1;
    }tAtPdhPrmController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhPrmController AtPdhPrmControllerObjectInit(AtPdhPrmController self, AtPdhDe1 de1);
void AtPdhPrmControllerInterruptProcess(AtPdhPrmController self, uint8 slice, uint8 de3, uint8 de2, uint8 de1, AtHal hal);
eAtModulePdhRet AtPdhPrmControllerInit(AtPdhPrmController self);
eAtModulePdhRet AtPdhPrmFcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order);
eAtBitOrder AtPdhPrmFcsBitOrderGet(AtPdhPrmController self);

#endif /* _ATPDHPRMCONTROLLERINTERNAL_H_ */


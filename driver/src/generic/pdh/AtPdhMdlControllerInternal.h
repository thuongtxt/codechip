/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhMdlControllerInternal.h
 *
 * Created Date: May 31, 2015
 *
 * Description : MDL controller
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHMDLCONTROLLERINTERNAL_H_
#define _ATPDHMDLCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePdh.h"
#include "AtPdhDe3.h"
#include "AtPdhMdlController.h"
#include "AtPdhMdlController.h"
#include "../man/AtModuleInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhMdlControllerMethods
    {
    eAtPdhMdlControllerType (*TypeGet)(AtPdhMdlController self);

	eAtRet (*Enable)(AtPdhMdlController self, eBool enable);
    eBool  (*IsEnabled)(AtPdhMdlController self);

    eAtRet (*Send)(AtPdhMdlController self);
    eBool  (*MessageReceived)(AtPdhMdlController self);

    uint32 (*CountersGet)(AtPdhMdlController self, uint32 counterType);
    uint32 (*CountersClearGet)(AtPdhMdlController self, uint32 counterType);

	eAtRet (*TxDataElementSet)(AtPdhMdlController self, eAtPdhMdlDataElement element, const uint8 *data, uint16 length);
    uint16 (*TxDataElementGet)(AtPdhMdlController self, eAtPdhMdlDataElement element, uint8 *data, uint16 bufferSize);
    uint16 (*TxMesssageGet)(AtPdhMdlController self, uint8 *data, uint16 bufferSize);

    uint16 (*RxDataElementGet)(AtPdhMdlController self, eAtPdhMdlDataElement element, uint8 *data, uint16 bufferSize);
    uint16 (*RxMesssageGet)(AtPdhMdlController self, uint8 *data, uint16 bufferSize);

    /* For Check message is Command or Respond, should be per MDL */
    eAtRet (*TxCRBitSet)(AtPdhMdlController self, uint8 value);
    uint8  (*TxCRBitGet)(AtPdhMdlController self);
    eAtRet (*ExpectedCRBitSet)(AtPdhMdlController self, uint8 value);
    uint8  (*ExpectedCRBitGet)(AtPdhMdlController self);
    uint8  (*RxCRBitGet)(AtPdhMdlController self);

    /* Interrupt process */
    uint32 (*DefectHistoryClear)(AtPdhMdlController self);
    uint32 (*DefectHistoryGet)(AtPdhMdlController self);
    eAtRet (*InterruptMaskSet)(AtPdhMdlController self, uint32 defectMask, uint32 enableMask);
    uint32 (*InterruptMaskGet)(AtPdhMdlController self);
    void (*InterruptProcess)(AtPdhMdlController self, uint8 slice, uint8 de3, AtHal hal);

    /* Debug methods */
    void (*Debug)(AtPdhMdlController self);
    }tAtPdhMdlControllerMethods;

typedef struct tAtPdhMdlController
    {
	tAtObject super;
	const tAtPdhMdlControllerMethods *methods;

	/* Private data */
	AtPdhDe3 de3;
    }tAtPdhMdlController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhMdlController AtPdhMdlControllerObjectInit(AtPdhMdlController self, AtPdhDe3 de3);

uint32 AtPdhMdlControllerDefectHistoryClear(AtPdhMdlController self);
uint32 AtPdhMdlControllerDefectHistoryGet(AtPdhMdlController self);
eAtRet AtPdhMdlControllerInterruptMaskSet(AtPdhMdlController self, uint32 defectMask, uint32 enableMask);
uint32 AtPdhMdlControllerInterruptMaskGet(AtPdhMdlController self);
void AtPdhMdlControllerInterruptProcess(AtPdhMdlController self, uint8 slice, uint8 de3, AtHal hal);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHMDLCONTROLLERINTERNAL_H_ */


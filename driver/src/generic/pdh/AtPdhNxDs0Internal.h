/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhNxDs0Internal.h
 * 
 * Created Date: Sep 2, 2012
 *
 * Description : NxDS0 group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHNXDS0INTERNAL_H_
#define _ATPDHNXDS0INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhNxDs0.h"
#include "AtPdhChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhNxDs0Methods
    {
    eAtRet (*CasIdleCodeSet)(AtPdhNxDS0 self, uint8 abcd1);
    uint8 (*CasIdleCodeGet)(AtPdhNxDS0 self);
    }tAtPdhNxDs0Methods;

typedef struct tAtPdhNxDS0
    {
    tAtPdhChannel super;
    const tAtPdhNxDs0Methods *methods;

    /* Private variables */
    AtPdhDe1 de1; /* DS1/E1 contains these DS0s */
    uint32 mask;  /* DS0 mask */
    eBool isEnable; /* state of these Ds0s */
    }tAtPdhNxDS0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhNxDS0 AtPdhNxDS0ObjectInit(AtPdhNxDS0 self, AtPdhDe1 de1, uint32 mask);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHNXDS0INTERNAL_H_ */


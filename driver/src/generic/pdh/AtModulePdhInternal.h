/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtModulePdhInternal.h
 * 
 * Created Date: Aug 6, 2012
 *
 * Author      : namnn
 * 
 * Description : Descriptor of all PDH classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPDHINTERNAL_H_
#define _ATMODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePdh.h"
#include "AtSdhChannel.h"

#include "../man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModulePdhLiuMonitor
    {
    AtPdhChannel channel;
    uint32 txClockRate;
    uint32 rxClockRate;
    } tAtModulePdhLiuMonitor;

/* Methods of module PDH */
typedef struct tAtModulePdhMethods
    {
    /* DS1/E1 */
    uint32   (*NumberOfDe1sGet)(AtModulePdh self);
    AtPdhDe1 (*De1Get)(AtModulePdh self, uint32 de1Id);

    /* DS3/E3 */
    uint32 (*NumberOfDe3sGet)(AtModulePdh self);
    AtPdhDe3 (*De3Get)(AtModulePdh self, uint32 de3Id);

    /* DS3/E3 Serial Lines */
    uint32 (*NumberOfDe3SerialLinesGet)(AtModulePdh self);
    AtPdhSerialLine (*De3SerialLineGet)(AtModulePdh self, uint32 serialLineId);

    /* PRM CR Bit */
    eAtModulePdhRet (*PrmCrCompareEnable)(AtModulePdh self, eBool enable);
    eBool (*PrmCrCompareIsEnabled)(AtModulePdh self);

    /* Internal methods */
    AtAttPdhManager (*AttPdhManagerCreate)(AtModulePdh pdh);
    AtPdhDe1 (*De1Create)(AtModulePdh self, uint32 de1Id);
    AtPdhDe1 (*De2De1Create)(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id);
    AtPdhDe1 (*VcDe1Create)(AtModulePdh self, AtSdhChannel sdhVc);
    AtPdhDe3 (*VcDe3Create)(AtModulePdh self, AtSdhChannel sdhVc);
    AtPdhNxDS0 (*NxDs0Create)(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap);
    AtPdhDe2 (*De2Create)(AtModulePdh self, uint32 de2Id);
    AtPdhDe3 (*De3Create)(AtModulePdh self, uint32 de3Id);
    eAtRet (*AllDe1Create)(AtModulePdh self);
    eAtRet (*AllDe1sDelete)(AtModulePdh self);
    AtPdhSerialLine (*De3SerialCreate)(AtModulePdh self, uint32 serialLineId);
    eBool (*NeedClearanceNotifyLogic)(AtModulePdh self);

    eAtRet (*LiuFrequencyMonitorSet)(AtModulePdh self, AtPdhChannel channel);
    eAtRet (*LiuFrequencyMonitorGet)(AtModulePdh self, tAtModulePdhLiuMonitor *clockMonitor);
    
    }tAtModulePdhMethods;

/* PDH module */
typedef struct tAtModulePdh
    {
    tAtModule super;
    const tAtModulePdhMethods *methods;

    /* Private data */
    AtPdhDe1 *allDe1s;
    AtPdhDe3 *allDe3s;
    AtPdhSerialLine *allSerialLines;
    AtAttPdhManager att;
    }tAtModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh AtModulePdhObjectInit(AtModulePdh self, AtDevice device);
eAtRet AtModulePdhAllManagedObjectsDelete(AtModulePdh self);
eAtRet AtModulePdhAllManagedObjectsCreate(AtModulePdh self);

/* Channel factory methods */
AtPdhDe1 AtModulePdhDe1Create(AtModulePdh self, uint32 de1Id);
AtPdhDe1 AtModulePdhVcDe1Create(AtModulePdh self, AtSdhChannel sdhVc);
AtPdhDe3 AtModulePdhVcDe3Create(AtModulePdh self, AtSdhChannel sdhVc);
AtPdhDe1 AtModulePdhDe2De1Create(AtModulePdh self, AtPdhDe2 de2, uint8 de1Id);
AtPdhNxDS0 AtModulePdhNxDs0Create(AtModulePdh self, AtPdhDe1 de1, uint32 timeslotBitmap);
AtPdhDe2 AtModulePdhDe2Create(AtModulePdh self, uint32 de2Id);

AtPdhChannel AtModulePdhHwVtId2PdhChannel(AtModulePdh self, uint8 stsId, uint8 vtId);

eAtRet AtModulePdhLiuFrequencyMonitorSet(AtModulePdh self, AtPdhChannel channel);
eAtRet AtModulePdhLiuFrequencyMonitorGet(AtModulePdh self, tAtModulePdhLiuMonitor *clockMonitor);
eBool AtModulePdhNeedClearanceNotifyLogic(AtModulePdh self);


#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPDHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhChannelInternal.h
 * 
 * Created Date: Sep 2, 2012
 *
 * Description : PDH channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHCHANNELINTERNAL_H_
#define _ATPDHCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h"
#include "AtModulePdh.h"
#include "AtModuleClock.h"

#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhChannelCache
    {
    tAtChannelCache super;
    uint16 frameType;
    }tAtPdhChannelCache;

/* Methods of class PDH channel */
typedef struct tAtPdhChannelMethods
    {
    eAtPdhChannelType (*TypeGet)(AtPdhChannel self);
    eAtRet (*CascadeHwIdGet)(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5);

    /* Frame type */
    eBool (*FrameTypeIsSupported)(AtPdhChannel self, uint16 frameType);
    eAtRet (*FrameTypeSet)(AtPdhChannel self, uint16 frameType);
    uint16 (*FrameTypeGet)(AtPdhChannel self);
    
    /* Line code */
    eAtRet (*LineCodeSet)(AtPdhChannel self, uint16 lineCode);
    uint16 (*LineCodeGet)(AtPdhChannel self);

    /* LED */
    eBool (*LedStateIsSupported)(AtPdhChannel self);
    eAtRet (*LedStateSet)(AtPdhChannel self, eAtLedState ledState);
    eAtLedState (*LedStateGet)(AtPdhChannel self);

    /* BER monitoring */
    AtBerController (*LineBerControllerGet)(AtPdhChannel self);
    AtBerController (*PathBerControllerGet)(AtPdhChannel self);

    /* Access sub channels */
    uint8 (*NumberOfSubChannelsGet)(AtPdhChannel self);
    AtPdhChannel (*SubChannelGet)(AtPdhChannel self, uint8 subChannelId);
    AtPdhChannel (*ParentChannelGet)(AtPdhChannel self);
    eAtRet (*SubChannelsInterruptDisable)(AtPdhChannel self, eBool onlySubChannels);

    /* Notify itself and its sub-channels when alarm forwarding from upper layer is terminated. */
    void (*AlarmForwardingClearanceNotifyWithSubChannelOnly)(AtPdhChannel self, eBool subChannelsOnly);
    eBool (*HasParentAlarmForwarding)(AtPdhChannel self); /* AIS forwarding status from upstream/parent channel. */
    eBool (*HasAlarmForwarding)(AtPdhChannel self); /* AIS forwarding downstream to its sub-channels.*/
    uint32 (*DefectsCauseAlarmForwarding)(AtPdhChannel self);
    void (*FailureForwardingClearanceNotifyWithSubChannelOnly)(AtPdhChannel self, eBool subChannelsOnly);

    /* DLK */
    eAtRet (*DataLinkSend)(AtPdhChannel self, const uint8 *messageBuffer, uint32 length);
    uint32 (*DataLinkReceive)(AtPdhChannel self, uint8 *messageBuffer, uint32 bufferLength);
    eAtRet (*DataLinkEnable)(AtPdhChannel self, eBool enable);
    eBool  (*DataLinkIsEnabled)(AtPdhChannel self);
    uint32 (*DataLinkCounterGet)(AtPdhChannel self, uint32 counterType);
    uint32 (*DataLinkCounterClear)(AtPdhChannel self, uint32 counterType);

    /* National bit */
    eAtRet (*NationalBitsSet)(AtPdhChannel self, uint8 pattern);
    uint8 (*NationalBitsGet)(AtPdhChannel self);
    uint8 (*RxNationalBitsGet)(AtPdhChannel self);
    eBool (*NationalBitsSupported)(AtPdhChannel self);
	
	/* RAI backward */
    eAtRet (*AutoRaiEnable)(AtPdhChannel self, eBool enable);
    eBool (*AutoRaiIsEnabled)(AtPdhChannel self);

    /* Alarm affecting option, a.k.a. AIS forwarding */
    eAtRet (*AlarmAffectingEnable)(AtPdhChannel self, uint32 alarmType, eBool enable);
    eBool (*AlarmAffectingIsEnabled)(AtPdhChannel self, uint32 alarmType);
    uint32 (*AlarmAffectingMaskGet)(AtPdhChannel self);
    eBool (*CanChangeAlarmAffecting)(AtPdhChannel self, uint32 alarmType, eBool enable);
    eAtRet (*AlarmAffectingDefaultSet)(AtPdhChannel self, eBool enable);

    /* Internal methods */
    uint8 (*DefaultFrameMode)(AtPdhChannel self);
    AtSdhVc (*VcGet)(AtPdhChannel self);
    eBool (*IsUnframed)(AtPdhChannel self, uint16 frameType);

    /* IDLE code */
    eAtRet (*IdleEnable)(AtPdhChannel self, eBool enable);
    eBool (*IdleIsEnabled)(AtPdhChannel self);
    eAtRet (*IdleCodeSet)(AtPdhChannel self, uint8 idleCode);
    uint8 (*IdleCodeGet)(AtPdhChannel self);
    eBool (*IdleSignalIsSupported)(AtPdhChannel self);

    /* Line prbs engine */
    AtPrbsEngine (*LinePrbsEngineGet)(AtPdhChannel self);

    /* BER */
    AtBerController (*PathBerControllerCreate)(AtPdhChannel self);
    AtBerController (*LineBerControllerCreate)(AtPdhChannel self);

    /* Retime/Slip buffer */
    eAtRet (*TxSlipBufferEnable)(AtPdhChannel self, eBool enable);
    eBool (*TxSlipBufferIsEnabled)(AtPdhChannel self);
    eBool (*RxSlipBufferIsSupported)(AtPdhChannel self);
    eAtRet (*RxSlipBufferEnable)(AtPdhChannel self, eBool enable);
    eBool  (*RxSlipBufferIsEnabled)(AtPdhChannel self);

    /* SSM */
    eAtRet (*SsmEnable)(AtPdhChannel self, eBool enable);
    eBool (*SsmIsEnabled)(AtPdhChannel self);
    eAtRet (*TxSsmSet)(AtPdhChannel self, uint8 value);
    uint8 (*TxSsmGet)(AtPdhChannel self);
    uint8 (*RxSsmGet)(AtPdhChannel self);

    /* LOS detection */
    eAtRet (*LosDetectionEnable)(AtPdhChannel self, eBool enable);
    eBool (*LosDetectionIsEnabled)(AtPdhChannel self);

    /* AIS detection */
    eAtRet (*AisAllOnesDetectionEnable)(AtPdhChannel self, eBool enable);
    eBool (*AisAllOnesDetectionIsEnabled)(AtPdhChannel self);

    /* Error generator*/
    AtErrorGenerator (*TxErrorGeneratorGet)(AtPdhChannel self);
    AtErrorGenerator (*RxErrorGeneratorGet)(AtPdhChannel self);

    /* FCS Bit-Order */
    eAtRet (*DataLinkFcsBitOrderSet)(AtPdhChannel self, eAtBitOrder order);
    eAtBitOrder (*DataLinkFcsBitOrderGet)(AtPdhChannel self);

    /* Internal */
    eBool (*LiuIsLoc)(AtPdhChannel self);
    eBool (*HasLineLayer)(AtPdhChannel self);
    }tAtPdhChannelMethods;

typedef struct tAtPdhChannelPropertyListener
    {
    void (*FrameTypeDidChange)(AtPdhChannel self, uint16 preFrameType, uint16 newFrameType, void *userData);
    }tAtPdhChannelPropertyListener;

/* PDH channel */
typedef struct tAtPdhChannel
    {
	tAtChannel super;
	const tAtPdhChannelMethods *methods;

	/* Private data */
	/* The SDH VC-1x that this DS1/E1 is mapped to */
	AtSdhVc vc;

	/* For mapping hierarchy */
	AtPdhChannel *subChannels;
	uint8 numSubChannels;
	AtPdhChannel parent;

	uint8 channelType;
	AtBerController pathBerController;
    AtBerController lineBerController;
    AtErrorGenerator txErrorGeneratorRef;
    AtErrorGenerator rxErrorGeneratorRef;

    /* Listeners */
    AtList listeners;
    }tAtPdhChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhChannel AtPdhChannelObjectInit(AtPdhChannel self, uint32 channelId, AtModulePdh module);
eAtRet AtPdhChannelCascadeHwIdGet(AtPdhChannel self, uint32 phyModule, uint32 *layer1, uint32 *layer2, uint32 *layer3, uint32 *layer4, uint32 *layer5);

/* Access private data */
void AtPdhChannelVcSet(AtPdhChannel self, AtSdhVc vc);
AtSdhVc AtPdhChannelVcInternalGet(AtPdhChannel self);

/* Hierarchy */
void AtPdhChannelAllSubChannelsDelete(AtPdhChannel self);
void AtPdhChannelAllSubChannelsSet(AtPdhChannel self, AtPdhChannel *subChannels, uint8 numSubChannels);
AtPdhChannel *AtPdhChannelAllSubChannelsGet(AtPdhChannel self, uint8 *numSubChannels);
void AtPdhChannelParentChannelSet(AtPdhChannel self, AtPdhChannel parent);
AtPdhChannel *AtPdhChannelAllSubChannelsCreate(AtPdhChannel self,
                                              uint8 numSubChannels,
                                              AtPdhChannel(*SubChannelCreate)(AtPdhChannel self, uint8 subChannelId));
eBool AtPdhChannelLedStateIsSupported(AtPdhChannel self);

/* Initialize itself and all sub-channels. */
eAtRet AtPdhChannelSubChannelsInterruptDisable(AtPdhChannel self, eBool onlySubChannels);

/* Notify alarm forwarding clearance itself and its sub-channels. */
void AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly);
eBool AtPdhChannelHasParentAlarmForwarding(AtPdhChannel self);
eBool AtPdhChannelHasAlarmForwarding(AtPdhChannel self);
uint32 AtPdhChannelDefectsCauseAlarmForwarding(AtPdhChannel self);
void AtPdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly);
eAtRet AtPdhChannelSubChannelsHardwareCleanup(AtPdhChannel self);

void AtPdhChannelTxErrorGeneratorRefSet(AtPdhChannel self, AtErrorGenerator generator);
AtErrorGenerator AtPdhChannelTxErrorGeneratorRefGet(AtPdhChannel self);
void AtPdhChannelRxErrorGeneratorRefSet(AtPdhChannel self, AtErrorGenerator generator);
AtErrorGenerator AtPdhChannelRxErrorGeneratorRefGet(AtPdhChannel self);
eAtRet AtPdhChannelFrameTypeNoLockSet(AtPdhChannel self, uint16 frameType);
eAtRet AtPdhChannelBerControllerDefaultSet(AtPdhChannel self);

/* Alarm affection */
uint32 AtPdhChannelAlarmAffectingMaskGet(AtPdhChannel self);
eBool AtPdhChannelCanChangeAlarmAffecting(AtPdhChannel self, uint32 alarmType, eBool enable);
eAtRet AtPdhChannelAlarmAffectingDisable(AtPdhChannel self);
eAtRet AtPdhChannelAlarmAffectingRestore(AtPdhChannel self);

/* AIS detection by all ones pattern */
eAtRet AtPdhChannelAisAllOnesDetectionEnable(AtPdhChannel self, eBool enable);
eBool AtPdhChannelAisAllOnesDetectionIsEnabled(AtPdhChannel self);

/* Line interface */
eBool AtPdhChannelLiuIsLoc(AtPdhChannel self);
eBool AtPdhChannelHasLineLayer(AtPdhChannel self);

/* Listener control */
eAtRet AtPdhChannelPropertyListenerAdd(AtPdhChannel self, const tAtPdhChannelPropertyListener* listener, void* userData);
eAtRet AtPdhChannelPropertyListenerRemove(AtPdhChannel self, const tAtPdhChannelPropertyListener* listener, void* userData);

/* Warmrestore */
void AtPdhChannelCacheFrameTypeSet(AtPdhChannel self, uint16 frameType);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHCHANNELINTERNAL_H_ */


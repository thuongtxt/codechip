/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe1Internal.h
 * 
 * Created Date: Sep 2, 2012
 *
 * Description : DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE1INTERNAL_H_
#define _ATPDHDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannelInternal.h"
#include "AtPdhDe1.h"
#include "AtPdhPrmController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of class PDH DE1 */
typedef struct tAtPdhDe1Methods
    {
	/* Signaling */
	eAtRet (*SignalingEnable)(AtPdhDe1 self, eBool enable);
	eAtRet (*TxSignalingEnable)(AtPdhDe1 self, eBool enable);
	eAtRet (*RxSignalingEnable)(AtPdhDe1 self, eBool enable);
	eBool (*SignalingIsEnabled)(AtPdhDe1 self);
	eAtRet (*SignalingDs0MaskSet)(AtPdhDe1 self, uint32 sigBitmap);
	uint32 (*SignalingDs0MaskGet)(AtPdhDe1 self);
    eAtRet (*SignalingPatternSet)(AtPdhDe1 self, uint32 ts, uint8 abcd);
    uint8 (*SignalingPatternGet)(AtPdhDe1 self, uint32 ts);
    eBool (*SignalingIsSupported)(AtPdhDe1 self);

	/* International Bit */
	eAtRet (*InternationalBitSet)(AtPdhDe1 self, uint8 zeroOrOne);
	uint8 (*InternationalBitGet)(AtPdhDe1 self);

    /* Inband Loop-code */
    eAtModulePdhRet (*TxLoopcodeSet)(AtPdhDe1 self, eAtPdhDe1Loopcode loopcode);
    eAtPdhDe1Loopcode (*TxLoopcodeGet)(AtPdhDe1 self);
    eAtPdhDe1Loopcode (*RxLoopcodeGet)(AtPdhDe1 self);

    /* BOM */
    eAtModulePdhRet (*TxBomSet)(AtPdhDe1 self, uint8 bomCode);
    uint8 (*TxBomGet)(AtPdhDe1 self);
    uint8 (*RxBomGet)(AtPdhDe1 self);
    uint8 (*RxCurrentBomGet)(AtPdhDe1 self);
    eAtModulePdhRet (*TxBomWithModeSet)(AtPdhDe1 self, uint8 bomCode, eAtPdhBomSentMode sentMode);
    eAtPdhBomSentMode (*TxBomModeGet)(AtPdhDe1 self);

    /* Deprecated */
    eAtModulePdhRet (*TxBomNTimesSend)(AtPdhDe1 self, uint8 bomCode, uint32 nTimes);
    uint32 (*TxBomSentCfgNTimesGet)(AtPdhDe1 self);

	/* Access NxDS0 */
	eBool (*NxDs0IsSupported)(AtPdhDe1 self);
	AtPdhNxDS0 (*NxDs0Create)(AtPdhDe1 self, uint32 timeslotBitmap);
	eAtRet (*NxDs0Delete)(AtPdhDe1 self, AtPdhNxDS0 nxDs0);
	AtPdhNxDS0 (*NxDs0Get)(AtPdhDe1 self, uint32 timeslotBitmap);

	/* NxDS0 Iterator */
	AtIterator (*NxDs0IteratorCreate)(AtPdhDe1 self);

	/* SA bits for E1 DataLink */
    eAtRet (*DataLinkSaBitsMaskSet)(AtPdhDe1 self, uint8 saBitsMask);
    uint8  (*DataLinkSaBitsMaskGet)(AtPdhDe1 self);

    /* SA bits for E1 SSM */
    eAtRet (*SsmSaBitsMaskSet)(AtPdhDe1 self, uint8 saBitsMask);
    uint8  (*SsmSaBitsMaskGet)(AtPdhDe1 self);

    /* PRM */
    AtPdhPrmController (*PrmControllerGet)(AtPdhDe1 self);
    eBool (*HasFarEndPerformance)(AtPdhDe1 self, uint16 frameType);

    /* Enable auto tx ais */
    eAtRet (*AutoTxAisEnable)(AtPdhDe1 self, eBool enable);
    eBool (*AutoTxAisIsEnabled)(AtPdhDe1 self);
    }tAtPdhDe1Methods;

/* DS1/E1 */
typedef struct tAtPdhDe1
    {
	tAtPdhChannel super;
	const tAtPdhDe1Methods *methods;

	/* Private data */
	uint32 mask;
	void *cache;
	eBool loopcodeEnable; /* For backward compatible only. */
    }tAtPdhDe1;

typedef enum eAtPdhDe1PrmCounterType
    {
    cAtPdhDe1PrmCounterTxMessage,        /* Tx Message counter */
    cAtPdhDe1PrmCounterTxByteMessage,    /* Tx Bytes counter */
    cAtPdhDe1PrmCounterRxGoodMessage,    /* Rx Good Message counter */
    cAtPdhDe1PrmCounterRxByteMessage,    /* Rx Bytes counter */
    cAtPdhDe1PrmCounterRxDiscardMessage, /* Rx Discard Message counter */
    cAtPdhDe1PrmCounterRxMissMessage     /* Rx Miss Message counter */
    }eAtPdhDe1PrmCounterType;

typedef enum eAtPdhDe1PrmAlarm
    {
    cAtPdhDe1PrmAlarmNone           = 0,     /* No alarm */
    cAtPdhDe1PrmAlarmDiscardMessage = cBit0, /* Discard Message */
    cAtPdhDe1PrmAlarmMissMessage    = cBit1  /* Miss Message */
    }eAtPdhDe1PrmAlarm;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 AtPdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

/* Access private data */
void AtPdhDe1Ds0MaskSet(AtPdhDe1 self, uint32 mask);
uint32 AtPdhDe1Ds0MaskGet(AtPdhDe1 self);

/* PRM counters */
uint32 AtPdhDe1PrmCounterGet(AtPdhDe1 self, uint16 counterType);
uint32 AtPdhDe1PrmCounterClear(AtPdhDe1 self, uint16 counterType);

/* PRM status */
uint32 AtPdhDe1PrmAlarmInterruptGet(AtPdhDe1 self);
uint32 AtPdhDe1PrmAlarmInterruptClear(AtPdhDe1 self);

/* LB bit enable */
eBool AtPdhDe1PrmTxLBBitIsEnabled(AtPdhDe1 self);
eAtModulePdhRet AtPdhDe1PrmTxLBBitEnable(AtPdhDe1 self, eBool enable);

AtPdhPrmController AtPdhDe1PrmControllerGet(AtPdhDe1 self);
eBool AtPdhDe1HasFarEndPerformance(AtPdhDe1 self, uint16 frameType);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHDE1INTERNAL_H_ */


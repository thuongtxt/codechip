/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhDe1.c
 *
 * Created Date: Aug 6, 2012
 *
 * Author      : namnn
 *
 * Description : Default implementation of PDH DS1/E1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "../sur/AtModuleSurInternal.h"
#include "../sur/AtSurEngineInternal.h"
#include "../lab/profiler/AtFailureProfilerFactory.h"
#include "../concate/AtModuleConcateInternal.h"
#include "AtPdhDe1Internal.h"
#include "atclib.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPdhDe1)self)
#define mChannelIsValid(self) ((self == NULL) ? cAtFalse : cAtTrue)

#define mPutCounter(counterField, counterType) \
    value = CounterGetFunc(self, counterType); \
    if (counters)                              \
        counters->counterField = value;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPdhDe1Methods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;
static tAtPdhChannelMethods     m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtPdhChannelBerControllerDefaultSet((AtPdhChannel)self);
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "de1";
    }

/* Signaling */
static eAtRet SignalingEnable(AtPdhDe1 self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TxSignalingEnable(AtPdhDe1 self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet RxSignalingEnable(AtPdhDe1 self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool SignalingIsEnabled(AtPdhDe1 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet SignalingDs0MaskSet(AtPdhDe1 self, uint32 sigBitmap)
    {
	AtUnused(sigBitmap);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 SignalingDs0MaskGet(AtPdhDe1 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet SignalingPatternSet(AtPdhDe1 self, uint32 ts, uint8 abcd)
    {
    AtUnused(self);
    AtUnused(ts);
    AtUnused(abcd);
    return cAtErrorNotImplemented;
    }

static uint8 SignalingPatternGet(AtPdhDe1 self, uint32 ts)
    {
    AtUnused(self);
    AtUnused(ts);
    return 0;
    }

static eBool SignalingIsSupported(AtPdhDe1 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet InternationalBitSet(AtPdhDe1 self, uint8 zeroOrOne)
    {
	AtUnused(zeroOrOne);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 InternationalBitGet(AtPdhDe1 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePdhRet TxLoopcodeSet(AtPdhDe1 self, eAtPdhDe1Loopcode loopcode)
    {
    AtUnused(self);
    AtUnused(loopcode);
    return cAtErrorNotImplemented;
    }

static eAtPdhDe1Loopcode TxLoopcodeGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return cAtPdhDe1LoopcodeInvalid;
    }

static eAtPdhDe1Loopcode RxLoopcodeGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return cAtPdhDe1LoopcodeInvalid;
    }

static uint32 TxBomSentCfgNTimesGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePdhRet TxBomNTimesSend(AtPdhDe1 self, uint8 bomCode, uint32 nTimes)
    {
    AtUnused(self);
    AtUnused(bomCode);
    AtUnused(nTimes);
    return cAtErrorNotImplemented;
    }

static eAtModulePdhRet TxBomSet(AtPdhDe1 self, uint8 bomCode)
    {
    AtUnused(self);
    AtUnused(bomCode);
    return cAtErrorNotImplemented;
    }

static uint8 TxBomGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 RxBomGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 RxCurrentBomGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return cAtPdhDe1FdlIdleFlag;
    }

static eAtModulePdhRet TxBomWithModeSet(AtPdhDe1 self, uint8 bomCode, eAtPdhBomSentMode sentMode)
    {
    AtUnused(self);
    AtUnused(bomCode);
    AtUnused(sentMode);
    return cAtErrorNotImplemented;
    }

static eAtPdhBomSentMode TxBomModeGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return cAtPdhBomSentModeInvalid;
    }

static AtPdhNxDS0 NxDs0Create(AtPdhDe1 self, uint32 timeslotBitmap)
    {
	AtUnused(timeslotBitmap);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtRet NxDs0Delete(AtPdhDe1 self, AtPdhNxDS0 nxDs0)
    {
	AtUnused(nxDs0);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static AtPdhNxDS0 NxDs0Get(AtPdhDe1 self, uint32 timeslotBitmap)
    {
	AtUnused(timeslotBitmap);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtIterator NxDs0IteratorCreate(AtPdhDe1 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eBool SaBitsIsApplicable(AtPdhDe1 self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if ((frameType == cAtPdhE1Frm)||(frameType == cAtPdhE1MFCrc))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet DataLinkSaBitsMaskSet(AtPdhDe1 self, uint8 saBitsMask)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(saBitsMask);
    return cAtError;
    }

static uint8 DataLinkSaBitsMaskGet(AtPdhDe1 self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static eAtRet SsmSaBitsMaskSet(AtPdhDe1 self, uint8 saBitsMask)
    {
    /* Let concrete class do */
    AtUnused(self);
    AtUnused(saBitsMask);
    return cAtError;
    }

static uint8 SsmSaBitsMaskGet(AtPdhDe1 self)
    {
    /* Let concrete class do */
    AtUnused(self);
    return 0;
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool clear)
	{
	tAtPdhDe1Counters *counters = (tAtPdhDe1Counters *)pAllCounters;
	uint32 (*CounterGetFunc)(AtChannel, uint16) = AtChannelCounterGet;
	uint32 value;

	if (counters)
	    AtOsalMemInit(counters, 0, sizeof(tAtPdhDe1Counters));

	if (clear)
	    CounterGetFunc = AtChannelCounterClear;

	mPutCounter(bpvExz   , cAtPdhDe1CounterBpvExz);
	mPutCounter(crcError , cAtPdhDe1CounterCrc);
	mPutCounter(fBitError, cAtPdhDe1CounterFe);
	mPutCounter(rei      , cAtPdhDe1CounterRei);
	mPutCounter(txCs     , cAtPdhDe1CounterTxCs);

	return cAtOk;
	}

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
	{
	return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
	}

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
	return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static uint32 DataRateInBytesPer125Us(AtChannel self)
    {
    return (AtPdhDe1IsE1((AtPdhDe1)self) ? 256 : 193) / 8;
    }

static uint32 DataRateInBytesPerMs(AtChannel self)
    {
    /* Need to override this method here because calculate from rate per 125Us will be not exact in case of DS1 */
    return (AtPdhDe1IsE1((AtPdhDe1)self) ? 256 : 193);
    }

static eBool NxDs0ServiceIsRunning(AtPdhDe1 self)
    {
    AtIterator nxds0Iterator = AtPdhDe1nxDs0IteratorCreate(self);
    AtChannel nxDs0;
    eBool nxDs0ServiceIsRunning = cAtFalse;

    while ((nxDs0 = (AtChannel)AtIteratorNext(nxds0Iterator)) != NULL)
        {
        if (AtChannelServiceIsRunning(nxDs0))
            {
            nxDs0ServiceIsRunning = cAtTrue;
            break;
            }
        }

    AtObjectDelete((AtObject)nxds0Iterator);
    return nxDs0ServiceIsRunning;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    if (pseudowire == NULL)
        return cAtOk;

    if (AtPwTypeGet(pseudowire) != cAtPwTypeSAToP)
        return cAtErrorInvlParm;

    if (NxDs0ServiceIsRunning((AtPdhDe1)self))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

static eBool NxDs0IsSupported(AtPdhDe1 self)
    {
	AtUnused(self);
    /* Let concrete classes determine */
    return cAtFalse;
    }

static eAtPdhChannelType TypeGet(AtPdhChannel self)
    {
    eAtPdhDe1FrameType frameMode = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if (frameMode == cAtPdhDe1FrameUnknown)
        return cAtPdhChannelTypeDe1Unknown;

    if ((frameMode == cAtPdhE1UnFrm) ||
        (frameMode == cAtPdhE1Frm)   ||
        (frameMode == cAtPdhE1MFCrc))
        return cAtPdhChannelTypeE1;

    return cAtPdhChannelTypeDs1;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPdhDe1 object = (AtPdhDe1)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(mask);
    mEncodeUInt(loopcodeEnable);
    mEncodeNone(cache);
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static AtModuleSur SurModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static AtSurEngine SurEngineObjectCreate(AtChannel self)
    {
    return AtModuleSurPdhDe1EngineCreate(SurModule(self), (AtPdhDe1)self);
    }

static void SurEngineObjectDelete(AtChannel self, AtSurEngine engine)
    {
    AtModuleSurEnginePdhDe1Delete(SurModule(self), engine);
    }

static AtPdhPrmController PrmControllerGet(AtPdhDe1 self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool IsUnframed(AtPdhChannel self, uint16 frameType)
    {
    AtUnused(self);
    return AtPdhDe1IsUnframeMode(frameType);
    }

static eAtRet SubChannelsInterruptDisable(AtPdhChannel self, eBool onlySubChannels)
    {
    AtUnused(onlySubChannels);
    AtChannelSurEngineInterruptDisable((AtChannel)self);
    return AtChannelInterruptMaskSet((AtChannel)self, AtChannelInterruptMaskGet((AtChannel)self), 0);
    }

static uint32 DefectsCauseAlarmForwarding(AtPdhChannel self)
    {
    AtUnused(self);
    return (cAtPdhDe1AlarmLos|cAtPdhDe1AlarmLof|cAtPdhDe1AlarmAis);
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    uint32 defect = AtChannelDefectGet((AtChannel)self);
    AtUnused(subChannelsOnly);

    if (defect & AtPdhChannelDefectsCauseAlarmForwarding(self))
        return;

    AtChannelAllAlarmListenersCall((AtChannel)self, cAtPdhDe1AlarmAis, 0);
    }

static void FailureForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    AtSurEngine engine = AtChannelSurEngineGet((AtChannel)self);
    uint32 autonomousFailures = AtSurEngineAutonomousFailuresGet(engine);
    AtUnused(subChannelsOnly);

    if (autonomousFailures & AtPdhChannelDefectsCauseAlarmForwarding(self))
        return;

    AtChannelAllFailureListenersCall((AtChannel)self, cAtPdhDe1AlarmAis, 0);
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    if (m_AtChannelMethods->ServiceIsRunning(self))
        return cAtTrue;

    return NxDs0ServiceIsRunning((AtPdhDe1)self);
    }

static eAtRet DataLinkFcsBitOrderSet(AtPdhChannel self, eAtBitOrder order)
    {
    AtUnused(self);
    return (order == cAtBitOrderLsb) ? cAtOk: cAtErrorModeNotSupport;
    }

static eAtBitOrder DataLinkFcsBitOrderGet(AtPdhChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtBitOrderLsb;
    }

static eAtRet AllNxDs0Destroy(AtPdhDe1 self)
    {
    eAtPdhDe1FrameType frameType = AtPdhDe1IsE1(self) ? cAtPdhE1UnFrm : cAtPdhDs1J1UnFrm;
    return AtPdhChannelFrameTypeSet((AtPdhChannel)self, frameType);
    }

static eAtRet AllServicesDestroy(AtChannel self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)self;
    eAtRet ret = cAtOk;

    if (AtPdhDe1Ds0MaskGet(de1))
        ret |= AllNxDs0Destroy(de1);

    ret |= m_AtChannelMethods->AllServicesDestroy(self);

    return ret;
    }

static eAtRet AutoTxAisEnable(AtPdhDe1 self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorNotImplemented : cAtOk;
    }

static eBool AutoTxAisIsEnabled(AtPdhDe1 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasFarEndPerformance(AtPdhDe1 self, uint16 frameType)
    {
    AtUnused(self);

    /* So far, just can be sure that DS1/J1 ESF has Far-End Performance. For E1, no clear standard for now. */
    if (AtPdhDe1FrameTypeIsE1(frameType))
        return cAtTrue;

    if ((frameType == cAtPdhDs1FrmEsf) ||
        (frameType == cAtPdhJ1FrmEsf))
        return cAtTrue;

    return cAtFalse;
    }

static AtFailureProfiler FailureProfilerObjectCreate(AtChannel self)
    {
    return AtFailureProfilerFactoryPdhDe1ProfilerCreate(AtChannelProfilerFactory(self), self);
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtPdhDe1SharedQuerier();
    }

static eAtRet HardwareCleanup(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->HardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    return AtPdhDe1TxLoopcodeSet((AtPdhDe1)self, cAtPdhDe1LoopcodeInbandDisable);
    }

static AtModuleConcate ConcateModule(AtChannel self)
    {
    return (AtModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static AtVcgBinder VcgBinderCreate(AtChannel self)
    {
    return AtModuleConcateCreateVcgBinderForDe1(ConcateModule(self), (AtPdhDe1)self);
    }

static void OverrideAtObject(AtPdhDe1 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe1 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, mMethodsGet(channel), sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, TypeGet);
        mMethodOverride(m_AtPdhChannelOverride, IsUnframed);
        mMethodOverride(m_AtPdhChannelOverride, SubChannelsInterruptDisable);
        mMethodOverride(m_AtPdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtPdhChannelOverride, DefectsCauseAlarmForwarding);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkFcsBitOrderSet);
        mMethodOverride(m_AtPdhChannelOverride, DataLinkFcsBitOrderGet);
        mMethodOverride(m_AtPdhChannelOverride, FailureForwardingClearanceNotifyWithSubChannelOnly);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtChannel(AtPdhDe1 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, DataRateInBytesPer125Us);
        mMethodOverride(m_AtChannelOverride, DataRateInBytesPerMs);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectCreate);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectDelete);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        mMethodOverride(m_AtChannelOverride, AllServicesDestroy);
        mMethodOverride(m_AtChannelOverride, FailureProfilerObjectCreate);
        mMethodOverride(m_AtChannelOverride, HardwareCleanup);
        mMethodOverride(m_AtChannelOverride, VcgBinderCreate);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void MethodsInit(AtPdhDe1 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, SignalingEnable);
        mMethodOverride(m_methods, TxSignalingEnable);
        mMethodOverride(m_methods, RxSignalingEnable);
        mMethodOverride(m_methods, SignalingIsEnabled);
        mMethodOverride(m_methods, SignalingDs0MaskSet);
        mMethodOverride(m_methods, SignalingDs0MaskGet);
        mMethodOverride(m_methods, SignalingPatternSet);
        mMethodOverride(m_methods, SignalingPatternGet);
        mMethodOverride(m_methods, SignalingIsSupported);
        mMethodOverride(m_methods, NxDs0IsSupported);
        mMethodOverride(m_methods, NxDs0Create);
        mMethodOverride(m_methods, NxDs0Delete);
        mMethodOverride(m_methods, NxDs0Get);
        mMethodOverride(m_methods, InternationalBitSet);
        mMethodOverride(m_methods, InternationalBitGet);
        mMethodOverride(m_methods, NxDs0IteratorCreate);
        mMethodOverride(m_methods, TxLoopcodeSet);
        mMethodOverride(m_methods, TxLoopcodeGet);
        mMethodOverride(m_methods, RxLoopcodeGet);
        mMethodOverride(m_methods, DataLinkSaBitsMaskSet);
        mMethodOverride(m_methods, DataLinkSaBitsMaskGet);
        mMethodOverride(m_methods, TxBomNTimesSend);
        mMethodOverride(m_methods, TxBomSentCfgNTimesGet);
        mMethodOverride(m_methods, TxBomSet);
        mMethodOverride(m_methods, TxBomGet);
        mMethodOverride(m_methods, RxBomGet);
        mMethodOverride(m_methods, RxCurrentBomGet);
        mMethodOverride(m_methods, TxBomWithModeSet);
        mMethodOverride(m_methods, TxBomModeGet);
        mMethodOverride(m_methods, SsmSaBitsMaskSet);
        mMethodOverride(m_methods, SsmSaBitsMaskGet);
        mMethodOverride(m_methods, PrmControllerGet);
        mMethodOverride(m_methods, AutoTxAisEnable);
        mMethodOverride(m_methods, AutoTxAisIsEnabled);
        mMethodOverride(m_methods, HasFarEndPerformance);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtPdhDe1 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

AtPdhDe1 AtPdhDe1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPdhDe1));

    /* Super constructor */
    if (AtPdhChannelObjectInit((AtPdhChannel)self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

void AtPdhDe1Ds0MaskSet(AtPdhDe1 self, uint32 mask)
    {
    self->mask = mask;
    }

uint32 AtPdhDe1Ds0MaskGet(AtPdhDe1 self)
    {
    return self->mask;
    }

eBool AtPdhDe1FrameTypeIsE1(eAtPdhDe1FrameType frameType)
    {
    if ((frameType == cAtPdhE1UnFrm) ||
        (frameType == cAtPdhE1Frm) ||
        (frameType == cAtPdhE1MFCrc))
        return cAtTrue;

    return cAtFalse;
    }

eBool AtPdhDe1IsUnframeMode(uint16 frameType)
    {
    return ((frameType == cAtPdhDs1J1UnFrm) || (frameType == cAtPdhE1UnFrm));
    }

eBool AtPdhDe1IsE1(AtPdhDe1 self)
    {
    eAtPdhDe1FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    return AtPdhDe1FrameTypeIsE1(frameType);
    }

eAtModulePdhRet AtPdhDe1PrmTxLBBitEnable(AtPdhDe1 self, eBool enable)
    {
    return AtPdhPrmControllerTxLBBitEnable(AtPdhDe1PrmControllerGet(self), enable);
    }

eBool AtPdhDe1PrmTxLBBitIsEnabled(AtPdhDe1 self)
    {
    return AtPdhPrmControllerTxLBBitIsEnabled(AtPdhDe1PrmControllerGet(self));
    }

uint32 AtPdhDe1PrmCounterGet(AtPdhDe1 self, uint16 counterType)
    {
    return AtPdhPrmControllerCounterGet(AtPdhDe1PrmControllerGet(self), counterType);
    }

uint32 AtPdhDe1PrmCounterClear(AtPdhDe1 self, uint16 counterType)
    {
    return AtPdhPrmControllerCounterClear(AtPdhDe1PrmControllerGet(self), counterType);
    }

uint32 AtPdhDe1PrmAlarmInterruptGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerAlarmInterruptGet(AtPdhDe1PrmControllerGet(self));
    }

uint32 AtPdhDe1PrmAlarmInterruptClear(AtPdhDe1 self)
    {
    return AtPdhPrmControllerAlarmInterruptClear(AtPdhDe1PrmControllerGet(self));
    }

AtPdhPrmController AtPdhDe1PrmControllerGet(AtPdhDe1 self)
    {
    if (self)
        return mMethodsGet(self)->PrmControllerGet(self);
    return NULL;
    }

eBool AtPdhDe1HasFarEndPerformance(AtPdhDe1 self, uint16 frameType)
    {
    if (self)
        return mMethodsGet(self)->HasFarEndPerformance(self, frameType);
    return cAtFalse;
    }

/**
 * @addtogroup AtPdhDe1
 * @{
 */
/**
 * Enable signaling
 *
 * @param self This DS1/E1
 * @param enable Enable/disable signaling
 *
 * @return AT return codes
 */
eAtModulePdhRet AtPdhDe1SignalingEnable(AtPdhDe1 self, eBool enable)
    {
    mNumericalAttributeSet(SignalingEnable, enable);
    }

eAtModulePdhRet AtPdhDe1TxSignalingEnable(AtPdhDe1 self, eBool enable)
    {
    mNumericalAttributeSet(TxSignalingEnable, enable);
    }

eAtModulePdhRet AtPdhDe1RxSignalingEnable(AtPdhDe1 self, eBool enable)
    {
    mNumericalAttributeSet(RxSignalingEnable, enable);
    }

/**
 * Check if signaling of DS1/E1 is enabled
 *
 * @param self This DS1/E1
 *
 * @return cAtTrue if signaling is enabled, otherwise, cAtFalse is returned
 */
eBool AtPdhDe1SignalingIsEnabled(AtPdhDe1 self)
    {
    mAttributeGet(SignalingIsEnabled, eBool, cAtFalse);
    }

/**
 * set signaling Ds0 bitmap of DS1/E1
 *
 * @param self This DS1/E1
 * @param sigBitmap signaling Ds0 bitmap
 *
 * @return AT return codes
 */
eAtModulePdhRet AtPdhDe1SignalingDs0MaskSet(AtPdhDe1 self, uint32 sigBitmap)
    {
    mNumericalAttributeSet(SignalingDs0MaskSet, sigBitmap);
    }

/**
 * get signaling Ds0 bitmap of DS1/E1
 *
 * @param self This DS1/E1
 *
 * @return sigBitmap signaling Ds0 bitmap
 *
 */
uint32 AtPdhDe1SignalingDs0MaskGet(AtPdhDe1 self)
    {
    mAttributeGet(SignalingDs0MaskGet, uint32, 0);
    }

/**
 * Set transmitted signaling pattern for one Ds0 of DS1/E1
 *
 * @param self This DS1/E1
 * @param ts a Ds0 timeslot
 * @param abcd Signalling pattern
 *
 * @return AT return codes
 */
eAtRet AtPdhDe1SignalingPatternSet(AtPdhDe1 self, uint32 ts, uint8 abcd)
    {
    mTwoParamsAttributeSet(SignalingPatternSet, ts, abcd);
    }

/**
 * get signaling Ds0 pattern of one Ds0 of DS1/E1
 *
 * @param self This DS1/E1
 * @param ts a Ds0 timeslot
 *
 * @return abcd signaling pattern of this timeslot
 *
 */
uint8 AtPdhDe1SignalingPatternGet(AtPdhDe1 self, uint32 ts)
    {
    mOneParamAttributeGet(SignalingPatternGet, ts, uint8, 0);
    }

/**
 * Check if this DS1/E1 support signaling
 *
 * @param self This DS1/E1
  *
 * @return cAtTrue: this DS1/E1 implementation support signaling,
 *         cAtFalse: this DS1/E1 implementation does not support signaling
 *
 */
eBool AtPdhDe1SignalingIsSupported(AtPdhDe1 self)
    {
    mAttributeGet(SignalingIsSupported, eBool, cAtFalse);
    }

/**
 * Set international bit
 *
 * @param self This E1
 * @param zeroOrOne [0..1] International bit value
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1InternationalBitSet(AtPdhDe1 self, uint8 zeroOrOne)
    {
    mNumericalAttributeSet(InternationalBitSet, zeroOrOne);
    }

/**
 * Get international bit
 *
 * @param self This E1
 *
 * @return [0..1] International bit value
 */
uint8 AtPdhDe1InternationalBitGet(AtPdhDe1 self)
    {
    mAttributeGet(InternationalBitGet, uint8, 0);
    }

/**
 * Check if NxDs0 is supported
 *
 * @param self This channel
 *
 * @retval cAtTrue if NxDs0 is supported
 * @retval cAtFalse if NxDs0 is not supported
 */
eBool AtPdhDe1NxDs0IsSupported(AtPdhDe1 self)
    {
    mAttributeGet(NxDs0IsSupported, eBool, cAtFalse);
    }

/**
 * Create NxDS0 with bitmap
 *
 * @param self This DS1/E1
 * @param timeslotBitmap Timeslot bitmap
 *
 * @return NxDS0 instance on success or NULL on failure
 */
AtPdhNxDS0 AtPdhDe1NxDs0Create(AtPdhDe1 self, uint32 timeslotBitmap)
    {
    mOneParamObjectCreate(NxDs0Create, AtPdhNxDS0, timeslotBitmap);
    }

/**
 * Delete NxDS0 instance
 *
 * @param self This DS1/E1
 * @param nxDs0 NxDS0 instance
 *
 * @return AT return codes
 */
eAtModulePdhRet AtPdhDe1NxDs0Delete(AtPdhDe1 self, AtPdhNxDS0 nxDs0)
    {
    mObjectSet(NxDs0Delete, nxDs0);
    }

/**
 * Get NxDS0 instance by its bitmap
 *
 * @param self This DS1/E1
 * @param timeslotBitmap NxDS0 timeslot bitmap
 *
 * @return NxDS0 instance if it was created, othewise, null is returned
 */
AtPdhNxDS0 AtPdhDe1NxDs0Get(AtPdhDe1 self, uint32 timeslotBitmap)
    {
    mOneParamObjectGet(NxDs0Get, AtPdhNxDS0, timeslotBitmap);
    }

/**
 * create NxDS0 iterator
 *
 * @param self This DS1/E1
 *
 * @return NxDS0 iterator if it was created, otherwise, null is returned
 */
AtIterator AtPdhDe1nxDs0IteratorCreate(AtPdhDe1 self)
    {
    mNoParamObjectGet(NxDs0IteratorCreate, AtIterator);
    }

/**
 * Set transmitted DS1 SF/ESF loop code
 *
 * @param self This DS1 SF/ESF
 * @param loopcode @ref eAtPdhDe1Loopcode "DS1 Loop code"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1TxLoopcodeSet(AtPdhDe1 self, eAtPdhDe1Loopcode loopcode)
    {
    mNumericalAttributeSet(TxLoopcodeSet, loopcode);
    }

/**
 * Get transmitted DS1 SF/ESF loop code
 *
 * @param self This DS1 SF/ESF
 *
 * @return @ref eAtPdhDe1Loopcode "DS1 Loop code"
 */
eAtPdhDe1Loopcode AtPdhDe1TxLoopcodeGet(AtPdhDe1 self)
    {
    mAttributeGet(TxLoopcodeGet, eAtPdhDe1Loopcode, cAtPdhDe1LoopcodeInvalid);
    }

/**
 * Get received DS1 SF/ESF loop code
 *
 * @param self This DS1 SF/ESF
 *
 * @return @ref eAtPdhDe1Loopcode "DS1 Loop code"
 */
eAtPdhDe1Loopcode AtPdhDe1RxLoopcodeGet(AtPdhDe1 self)
    {
    mAttributeGet(RxLoopcodeGet, eAtPdhDe1Loopcode, cAtPdhDe1LoopcodeInvalid);
    }

/**
 * Set Sa-Bits Mask for DataLink on E1 frame
 *
 * @param self This E1
 * @param saBitsMask [0x0..0x3F] SaBitsMask value
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1DataLinkSaBitsMaskSet(AtPdhDe1 self, uint8 saBitsMask)
    {
    if (!AtPdhDe1SaBitsIsApplicable(self))
        return cAtErrorNotApplicable;

    mNumericalAttributeSet(DataLinkSaBitsMaskSet, saBitsMask);
    }

/**
 * Get Sa-Bits Mask for DataLink on E1 frame
 *
 * @param self This E1
 *
 * @return SaBitMask [0x0..0x3F] SaBitsMask value
 */
uint8 AtPdhDe1DataLinkSaBitsMaskGet(AtPdhDe1 self)
    {
    if (!AtPdhDe1SaBitsIsApplicable(self))
        return 0;

    mAttributeGet(DataLinkSaBitsMaskGet, uint8, 0);
    }

/**
 * Set transmitted FDL BOM code along with a sending mode of transmitted bomCode for DS1 ESF channel.
 * This API is a wrapper for the AtPdhDe1TxBomNTimesSend.
 *
 * @param self This DS1 ESF channel
 * @param bomCode 6-bits ESF BOM code (111111110xxxxxx0).
 * @param sentMode mode to send the bomCode, see @ref eAtPdhBomSentMode "BOM sent mode"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1TxBomWithModeSet(AtPdhDe1 self, uint8 bomCode, eAtPdhBomSentMode sentMode)
    {
    mTwoParamsAttributeSet(TxBomWithModeSet, bomCode, sentMode);
    }

/**
 * Get sending mode of transmitted bomCode for DS1 ESF channel.
 *
 * @param self This DS1 ESF channel
 *
 * @return see @ref eAtPdhBomSentMode "BOM sent mode"
 */
eAtPdhBomSentMode AtPdhDe1TxBomModeGet(AtPdhDe1 self)
    {
    mAttributeGet(TxBomModeGet, eAtPdhBomSentMode, cAtPdhBomSentModeInvalid);
    }

/**
 * Set transmitted FDL BOM code for DS1 ESF channel. N times = 10 for transmitted bomCode.
 *
 * @param self This DS1 ESF channel
 * @param bomCode 6-bits ESF BOM code (111111110xxxxxx0).
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1TxBomSet(AtPdhDe1 self, uint8 bomCode)
    {
    mNumericalAttributeSet(TxBomSet, bomCode);
    }

/**
 * Get transmitted ESF BOM of DS1 ESF channel
 *
 * @param self This DS1 ESF channel
 *
 * @return 6-bits FDL BOM code (111111110xxxxxx0).
 */
uint8 AtPdhDe1TxBomGet(AtPdhDe1 self)
    {
    mAttributeGet(TxBomGet, uint8, 0);
    }

/**
 * Get received latched ESF BOM of DS1 ESF channel
 *
 * @param self This DS1 ESF channel
 *
 * @return 6-bits FDL BOM code (111111110xxxxxx0).
 */
uint8 AtPdhDe1RxBomGet(AtPdhDe1 self)
    {
    mAttributeGet(RxBomGet, uint8, 0);
    }

/**
 * Get received current ESF BOM of DS1 ESF channel.
 *
 * @param self This DS1 ESF channel
 *
 * @return 6-bits FDL BOM code (111111110xxxxxx0). The returned value 0x7E indicate the current value is IDLE.
 */
uint8 AtPdhDe1RxCurrentBomGet(AtPdhDe1 self)
    {
    mAttributeGet(RxCurrentBomGet, uint8, 0);
    }

/**
 * Set PRM standard for both TX/RX directions
 *
 * @param self This DS1-ESF channel
 * @param standard @ref eAtPdhDe1PrmStandard "PRM standard"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1PrmStandardSet(AtPdhDe1 self, eAtPdhDe1PrmStandard standard)
    {
    mOneParamWrapCall(standard, eAtModulePdhRet, AtPdhPrmControllerStandardSet(AtPdhDe1PrmControllerGet(self), standard));
    }

/**
 * Get PRM standard
 *
 * @param self This DS1-ESF channel
 *
 * @return @ref eAtPdhDe1PrmStandard "PRM standard"
 */
eAtPdhDe1PrmStandard AtPdhDe1PrmStandardGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerStandardGet(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Set Sa-Bits Mask for SSM on E1 frame
 *
 * @param self This E1
 * @param saBit @ref eAtPdhE1Sa "Sa bit"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1SsmSaBitsMaskSet(AtPdhDe1 self, eAtPdhE1Sa saBit)
    {
    if (!AtPdhDe1SaBitsIsApplicable(self))
        return cAtErrorNotApplicable;

    mNumericalAttributeSet(SsmSaBitsMaskSet, saBit);
    }

/**
 * Get Sa-Bits Mask for SSM on E1 frame
 *
 * @param self This E1
 *
 * @return @ref eAtPdhE1Sa "Sa bit"
 */
eAtPdhE1Sa AtPdhDe1SsmSaBitsMaskGet(AtPdhDe1 self)
    {
    if (!AtPdhDe1SaBitsIsApplicable(self))
        return 0;

    mAttributeGet(SsmSaBitsMaskGet, uint8, 0);
    }

/**
 * Set PRM standard at transmit direction
 *
 * @param self This DS1-ESF channel
 * @param standard @ref eAtPdhDe1PrmStandard "PRM standard"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1PrmTxStandardSet(AtPdhDe1 self, eAtPdhDe1PrmStandard standard)
    {
    mOneParamWrapCall(standard, eAtModulePdhRet, AtPdhPrmControllerTxStandardSet(AtPdhDe1PrmControllerGet(self), standard));
    }

/**
 * Get PRM standard at transmit direction
 *
 * @param self This DS1-ESF channel
 *
 * @return @ref eAtPdhDe1PrmStandard "PRM standard"
 */
eAtPdhDe1PrmStandard AtPdhDe1PrmTxStandardGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerTxStandardGet(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Set PRM standard at receive direction
 *
 * @param self This DS1-ESF channel
 * @param standard @ref eAtPdhDe1PrmStandard "PRM standard"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1PrmRxStandardSet(AtPdhDe1 self, eAtPdhDe1PrmStandard standard)
    {
    mOneParamWrapCall(standard, eAtModulePdhRet, AtPdhPrmControllerRxStandardSet(AtPdhDe1PrmControllerGet(self), standard));
    }

/**
 * Get PRM standard at receive direction
 *
 * @param self This DS1-ESF channel
 *
 * @return @ref eAtPdhDe1PrmStandard "PRM standard"
 */
uint32 AtPdhDe1PrmRxStandardGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerRxStandardGet(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Enable/disable PRM
 *
 * @param self This Ds1/E1 channel
 * @param enable [cAtTrue, cAtFalse] for enable/disable
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1PrmEnable(AtPdhDe1 self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtModulePdhRet, AtPdhPrmControllerEnable(AtPdhDe1PrmControllerGet(self), enable));
    }

/**
 * Check if PRM is enabled or not
 *
 * @param self This Ds1/E1 channel
 *
 * @return cAtFalse if PRM is enabled
 * @return cAtTrue  if PRM is disabled
 */
eBool AtPdhDe1PrmIsEnabled(AtPdhDe1 self)
    {
    return AtPdhPrmControllerIsEnabled(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Set TX C/R bit
 *
 * @param self This Ds1/E1 channel
 * @param value C/R bit value
 *
 * @return AT Return code
 */
eAtModulePdhRet AtPdhDe1PrmTxCRBitSet(AtPdhDe1 self, uint8 value)
    {
    mOneParamWrapCall(value, eAtModulePdhRet, AtPdhPrmControllerTxCRBitSet(AtPdhDe1PrmControllerGet(self), value));
    }

/**
 * Get TX C/R bit
 *
 * @param self This Ds1/E1 channel
 *
 * @return TX C/R bit
 */
uint8 AtPdhDe1PrmTxCRBitGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerTxCRBitGet(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Set Expected C/R bit
 *
 * @param self This Ds1/E1 channel
 * @param value LB bit value
 *
 * @return AT Return code
 */
eAtModulePdhRet AtPdhDe1PrmExpectedCRBitSet(AtPdhDe1 self, uint8 value)
    {
    mOneParamWrapCall(value, eAtModulePdhRet, AtPdhPrmControllerExpectedCRBitSet(AtPdhDe1PrmControllerGet(self), value));
    }

/**
 * Get Expected C/R bit
 *
 * @param self This Ds1/E1 channel
 *
 * @return Expected C/R bit
 */
uint8 AtPdhDe1PrmExpectedCRBitGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerExpectedCRBitGet(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Set TX LB bit
 *
 * @param self This Ds1/E1 channel
 * @param value LB bit value
 *
 * @return AT Return code
 */
eAtModulePdhRet AtPdhDe1PrmTxLBBitSet(AtPdhDe1 self, uint8 value)
    {
    mOneParamWrapCall(value, eAtModulePdhRet, AtPdhPrmControllerTxLBBitSet(AtPdhDe1PrmControllerGet(self), value));
    }

/**
 * Get TX LB bit
 *
 * @param self This Ds1/E1 channel
 *
 * @return TX LB bit
 */
uint8 AtPdhDe1PrmTxLBBitGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerTxLBBitGet(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Get RX LB bit
 *
 * @param self This Ds1/E1 channel
 *
 * @return RX LB bit
 */
uint8 AtPdhDe1PrmRxLBBitGet(AtPdhDe1 self)
    {
    return AtPdhPrmControllerRxLBBitGet(AtPdhDe1PrmControllerGet(self));
    }

/**
 * Check if SA bits are applicable
 *
 * @param self This Ds1/E1 channel
 *
 * @retval cAtTrue if applicable
 * @retval cAtFalse if not applicable
 */
eBool AtPdhDe1SaBitsIsApplicable(AtPdhDe1 self)
    {
    if (self)
        return SaBitsIsApplicable(self);
    return cAtFalse;
    }

/**
 * Enable/disable auto AIS when PWs receive L-Bit. In case of NxDs0, if this
 * option is enabled, when any CESoP of NxDS0s belonging to this DE1 receive L-Bit
 * packets, AIS will be playout onto DE1 layer. If this option is disabled (default)
 * all-one will be playout onto NxDs0s that have there own PWs receive L-Bit.
 *
 * @param self This De1 channel
 * @param enable Enabling. cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe1AutoTxAisEnable(AtPdhDe1 self, eBool enable)
    {
    mNumericalAttributeSet(AutoTxAisEnable, enable);
    }

/**
 * Check if auto AIS is enabled when PWs receive L-Bit.
 *
 * @param self This De1 channel
 *
 * @retval cAtTrue if enabled.
 * @retval cAtFalse if disabled
 *
 * @see AtPdhDe1AutoTxAisEnable()
 */
eBool AtPdhDe1AutoTxAisIsEnabled(AtPdhDe1 self)
    {
    mAttributeGet(AutoTxAisIsEnabled, eBool, cAtFalse);
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe2Internal.h
 * 
 * Created Date: Jun 12, 2014
 *
 * Description : DS2/E2
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE2INTERNAL_H_
#define _ATPDHDE2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannelInternal.h"
#include "AtPdhDe2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhDe2
    {
    tAtPdhChannel super;

    /* Private data */
    void *cache;
    }tAtPdhDe2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe2 AtPdhDe2ObjectInit(AtPdhDe2 self, uint32 channelId, AtModulePdh module);
eAtRet AtPdhDe2AllDe1sReCreate(AtPdhChannel self);

#endif /* _ATPDHDE2INTERNAL_H_ */


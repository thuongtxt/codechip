/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhDe3.c
 *
 * Created Date: Sep 19, 2012
 *
 * Description : DS3/E3 class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModuleBer.h"
#include "AtPdhDe2.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../sur/AtModuleSurInternal.h"
#include "../sur/AtSurEngineInternal.h"
#include "../lab/profiler/AtFailureProfilerFactory.h"
#include "../concate/AtModuleConcateInternal.h"
#include "AtPdhDe3Internal.h"
#include "AtModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPutCounter(counterField, counterType) \
    value = CounterGetFunc(self, counterType); \
    if (counters)                              \
        counters->counterField = value;

#define mThis(self) ((AtPdhDe3)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPdhDe3Methods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe3);
    }

static eAtRet Init(AtChannel self)
    {
    AtPdhChannel pdhChannel = (AtPdhChannel)self;
    eAtRet ret;

    /* Super */
    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AtChannelEnable(self, cAtTrue);
    ret |= AtPdhChannelFrameTypeNoLockSet(pdhChannel, mMethodsGet(pdhChannel)->DefaultFrameMode(pdhChannel));
    ret |= AtPdhChannelBerControllerDefaultSet(pdhChannel);

    return ret;
    }

static eAtRet TxFeacSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 code)
    {
    AtUnused(self);
    AtUnused(signalType);
    AtUnused(code);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *code)
    {
    AtUnused(self);
    AtUnused(signalType);
    AtUnused(code);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet RxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *code)
    {
    AtUnused(self);
    AtUnused(signalType);
    AtUnused(code);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TxFeacWithModeSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 xcode, eAtPdhBomSentMode sentMode)
    {
    AtUnused(self);
    AtUnused(signalType);
    AtUnused(xcode);
    AtUnused(sentMode);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPdhBomSentMode TxFeacModeGet(AtPdhDe3 self)
    {
    AtUnused(self);
    return cAtPdhBomSentModeInvalid;
    }

static eAtRet TxTtiSet(AtPdhDe3 self, const tAtPdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TxTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet ExpectedTtiSet(AtPdhDe3 self, const tAtPdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet ExpectedTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet RxTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TmEnable(AtPdhDe3 self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool TmIsEnabled(AtPdhDe3 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet TxTmCodeSet(AtPdhDe3 self, uint8 tmCode)
    {
	AtUnused(tmCode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8  TxTmCodeGet(AtPdhDe3 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8  RxTmCodeGet(AtPdhDe3 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet TxPldTypeSet(AtPdhDe3 self, uint8 pldType)
    {
	AtUnused(pldType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8  TxPldTypeGet(AtPdhDe3 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8  RxPldTypeGet(AtPdhDe3 self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePdhRet TxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    AtUnused(self);
    AtUnused(bitOrder);
    return cAtErrorModeNotSupport;
    }

static eAtBitOrder TxUdlBitOrderGet(AtPdhDe3 self)
    {
    AtUnused(self);
    return cAtBitOrderUnknown;
    }

static eAtModulePdhRet RxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    AtUnused(self);
    AtUnused(bitOrder);
    return cAtErrorModeNotSupport;
    }

static eAtBitOrder RxUdlBitOrderGet(AtPdhDe3 self)
    {
    AtUnused(self);
    return cAtBitOrderUnknown;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "de3";
    }

static const char *IdString(AtChannel self)
    {
    AtSdhVc sdhVc = AtPdhChannelVcInternalGet((AtPdhChannel)self);

    if (sdhVc)
        return mMethodsGet(mThis(self))->VcIdString(mThis(self), sdhVc);

    else
        {
        static char idBuf[16];
        AtSprintf(idBuf, "%u", AtChannelIdGet(self) + 1);
        return idBuf;
        }
    }

static uint32 DataRateInBytesPer125Us(AtChannel self)
    {
    return AtPdhDe3IsE3((AtPdhDe3)self) ? cE3DataRateInBytesPer125Us : cDs3DataRateInBytesPer125Us;
    }

static uint8 NumSubChannelsForFrameType(AtPdhChannel self, uint16 frameType)
    {
    AtUnused(self);

    if ((frameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (frameType == cAtPdhDs3FrmCbitChnl21E1s) ||
        (frameType == cAtPdhDs3FrmM13Chnl28Ds1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl21E1s))
        return 7;

    if (frameType == cAtPdhE3FrmG751Chnl16E1s)
        return 4;

    return 0x0;
    }

static AtPdhChannel De2Create(AtPdhChannel self, uint8 de2Id)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtChannelModuleGet((AtChannel)self);
    return (AtPdhChannel)AtModulePdhDe2Create(pdhModule, de2Id);
    }

static AtPdhChannel *AllSubChannelsCreate(AtPdhChannel self, uint8 numSubChannels)
    {
    return AtPdhChannelAllSubChannelsCreate(self, numSubChannels, De2Create);
    }

static eAtRet SubChannelsInterruptDisable(AtPdhChannel self, eBool onlySubChannels)
    {
    uint8 numSubChannels, channel_i;

    numSubChannels = AtPdhChannelNumberOfSubChannelsGet(self);
    for (channel_i = 0; channel_i < numSubChannels; channel_i++)
        {
        AtPdhChannel de2 = AtPdhChannelSubChannelGet(self, channel_i);
        AtPdhChannelSubChannelsInterruptDisable(de2, cAtFalse);
        }

    if (!onlySubChannels)
        {
        AtChannelSurEngineInterruptDisable((AtChannel)self);
        AtChannelInterruptMaskSet((AtChannel)self, AtChannelInterruptMaskGet((AtChannel)self), 0);
        }

    return cAtOk;
    }

static uint32 DefectsCauseAlarmForwarding(AtPdhChannel self)
    {
    AtUnused(self);
    return (cAtPdhDe3AlarmLos|cAtPdhDe3AlarmLof|cAtPdhDe3AlarmAis);
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    uint32 defect = AtChannelDefectGet((AtChannel)self);
    uint8 numSubChannels, channel_i;

    if (defect & AtPdhChannelDefectsCauseAlarmForwarding(self))
        return;

    if (subChannelsOnly == cAtFalse)
        AtChannelAllAlarmListenersCall((AtChannel)self, cAtPdhDe3AlarmAis, 0);

    numSubChannels = AtPdhChannelNumberOfSubChannelsGet(self);
    for (channel_i = 0; channel_i < numSubChannels; channel_i++)
        {
        AtPdhChannel de2 = AtPdhChannelSubChannelGet(self, channel_i);
        AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(de2, cAtFalse);
        }
    }

static void FailureForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    AtSurEngine engine = AtChannelSurEngineGet((AtChannel)self);
    uint32 autonomousFailures = AtSurEngineAutonomousFailuresGet(engine);
    uint8 numSubChannels, channel_i;

    if (autonomousFailures & AtPdhChannelDefectsCauseAlarmForwarding(self))
        return;

    if (subChannelsOnly == cAtFalse)
        AtChannelAllFailureListenersCall((AtChannel)self, cAtPdhDe3AlarmAis, 0);

    numSubChannels = AtPdhChannelNumberOfSubChannelsGet(self);
    for (channel_i = 0; channel_i < numSubChannels; channel_i++)
        {
        AtPdhChannel de2 = AtPdhChannelSubChannelGet(self, channel_i);
        AtPdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly(de2, cAtFalse);
        }
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    AtPdhChannel *newSubChannels = NULL;
    uint8 numSubChannels = NumSubChannelsForFrameType(self, frameType);

    /* Recreate all sub channels to have a clean database */
    AtPdhChannelAllSubChannelsDelete(self);

    /* No sub channels */
    if (numSubChannels == 0)
        return cAtOk;

    /* Create all of sub channels */
    newSubChannels = AllSubChannelsCreate(self, numSubChannels);
    if (newSubChannels == NULL)
        return cAtErrorRsrcNoAvail;

    AtPdhChannelAllSubChannelsSet(self, newSubChannels, numSubChannels);

    return cAtOk;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    uint8 i;

    if (pseudowire == NULL)
        return cAtOk;

    if (AtPwTypeGet(pseudowire) != cAtPwTypeSAToP)
        return cAtErrorInvlParm;

    /* If any of sub channels is busy, do now allow to bind PW */
    for (i = 0; i < AtPdhChannelNumberOfSubChannelsGet((AtPdhChannel)self); i++)
        {
        if (AtChannelServiceIsRunning((AtChannel)AtPdhChannelSubChannelGet((AtPdhChannel)self, i)))
            return cAtErrorChannelBusy;
        }

    return cAtOk;
    }

static eAtPdhChannelType TypeGet(AtPdhChannel self)
    {
    eAtPdhDe3FrameType frameMode = AtPdhChannelFrameTypeGet(self);

    if (frameMode == cAtPdhDe3FrameUnknown)
        return cAtPdhChannelTypeDe3Unknown;

    if((frameMode == cAtPdhE3Unfrm)           ||
       (frameMode == cAtPdhE3Frmg832)         ||
       (frameMode == cAtPdhE3FrmG751)         ||
       (frameMode == cAtPdhE3FrmG751Chnl16E1s))
        return cAtPdhChannelTypeE3;

    return cAtPdhChannelTypeDs3;
    }

static AtPdhMdlController PathMessageMdlController(AtPdhDe3 self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPdhMdlController TestSignalMdlController(AtPdhDe3 self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtPdhMdlController IdleSignalMdlController(AtPdhDe3 self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eBool FeacIsApplicable(AtPdhDe3 self)
    {
    return AtPdhDe3IsDs3CbitFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)self));
    }

static eBool SsmIsApplicable(AtPdhDe3 self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if (AtPdhDe3IsDs3CbitFrameType(frameType))
        return cAtTrue;

    if (frameType == cAtPdhE3Frmg832)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet MdlStandardSet(AtPdhDe3 self, eAtPdhMdlStandard standard)
    {
    AtUnused(self);
    AtUnused(standard);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 MdlStandardGet(AtPdhDe3 self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtPdhMdlStandardUnknown;
    }

static eAtRet DataLinkOptionSet(AtPdhDe3 self, eAtPdhDe3DataLinkOption option)
    {
    AtUnused(self);
    AtUnused(option);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPdhDe3DataLinkOption DataLinkOptionGet(AtPdhDe3 self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtPdhDe3DataLinkOptionUnknown;
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(((AtPdhDe3)self)->cache);
    }

static AtModuleSur SurModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static AtSurEngine SurEngineObjectCreate(AtChannel self)
    {
    return AtModuleSurPdhDe3EngineCreate(SurModule(self), (AtPdhDe3)self);
    }

static void SurEngineObjectDelete(AtChannel self, AtSurEngine engine)
    {
    AtModuleSurEnginePdhDe3Delete(SurModule(self), engine);
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool clear)
	{
	tAtPdhDe3Counters *counters = (tAtPdhDe3Counters *)pAllCounters;
	uint32 value;
	uint32 (*CounterGetFunc)(AtChannel, uint16) = AtChannelCounterGet;

	if (counters)
		AtOsalMemInit(counters, 0, sizeof(tAtPdhDe3Counters));

	if (clear)
	    CounterGetFunc = AtChannelCounterClear;

	mPutCounter(bip8Error      , cAtPdhDe3CounterBip8);
	mPutCounter(bpvExz  	   , cAtPdhDe3CounterBpvExz);
	mPutCounter(fBitError      , cAtPdhDe3CounterFBit);
	mPutCounter(pBitError      , cAtPdhDe3CounterPBit);
	mPutCounter(cBitParityError, cAtPdhDe3CounterCPBit);
	mPutCounter(rei            , cAtPdhDe3CounterRei);
	mPutCounter(txCs           , cAtPdhDe3CounterTxCs);

	return cAtOk;
	}

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
	{
	return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
	}

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
	return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static AtPdhMdlController RxCurrentMdlControllerGet(AtPdhDe3 self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool IsUnframed(AtPdhChannel self, uint16 frameType)
    {
    AtUnused(self);
    return AtPdhDe3FrameTypeIsUnframed(frameType);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(cache);
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtPdhDe3SharedQuerier();
    }

static eBool De3IsValid(AtPdhDe3 self)
    {
    return (self == NULL) ? cAtFalse : cAtTrue;
    }

static eBool NationalBitsSupported(AtPdhChannel self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet(self);

    if ((frameType == cAtPdhE3FrmG751) ||
        (frameType == cAtPdhE3FrmG751Chnl16E1s))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsE3G832(AtPdhDe3 self)
    {
    if (AtPdhChannelFrameTypeGet((AtPdhChannel)self) == cAtPdhE3Frmg832)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsDs3Cbit(AtPdhDe3 self)
    {
    if (AtPdhDe3IsDs3CbitFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)self)))
        return cAtTrue;
    return cAtFalse;
    }

static uint8 RxAicGet(AtPdhDe3 self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet AlarmAffectingDefaultSet(AtPdhChannel self, eBool enable)
    {
    static const uint32 defaultAlarms[] = {cAtPdhDe3AlarmAis, cAtPdhDe3AlarmLof};
    uint32 changedAlarm = 0;
    eAtRet ret = cAtOk;
    uint8 alarm_i;

    /* Backup enabled alarms for restoring */
    for (alarm_i = 0; alarm_i < mCount(defaultAlarms); alarm_i++)
        {
        if (mBoolToBin(enable) == mBoolToBin(AtPdhChannelAlarmAffectingIsEnabled(self, defaultAlarms[alarm_i])))
            continue;

        if (AtPdhChannelCanChangeAlarmAffecting(self, defaultAlarms[alarm_i], enable))
            changedAlarm |= defaultAlarms[alarm_i];
        }

    /* Change all alarms affect */
    if (changedAlarm)
        ret = AtPdhChannelAlarmAffectingEnable(self, changedAlarm, enable);

    return ret;
    }

static uint8 DefaultFrameMode(AtPdhChannel self)
    {
    AtUnused(self);
    return cAtPdhE3Unfrm;
    }

static eAtRet StuffModeSet(AtPdhDe3 self, eAtPdhDe3StuffMode mode)
    {
    AtUnused (self);
    AtUnused (mode);

    return cAtOk;
    }

static eAtPdhDe3StuffMode StuffModeGet(AtPdhDe3 self)
    {
    AtUnused (self);
    return cAtPdhDe3StuffModeFineStuff;
    }

static const char *VcIdString(AtPdhDe3 self, AtSdhVc vc)
    {
    AtUnused(self);
    return AtChannelIdString((AtChannel)vc);
    }

static AtFailureProfiler FailureProfilerObjectCreate(AtChannel self)
    {
    return AtFailureProfilerFactoryPdhDe3ProfilerCreate(AtChannelProfilerFactory(self), self);
    }

static AtModuleConcate ConcateModule(AtChannel self)
    {
    return (AtModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static AtVcgBinder VcgBinderCreate(AtChannel self)
    {
    return AtModuleConcateCreateVcgBinderForDe3(ConcateModule(self), (AtPdhDe3)self);
    }

static void MethodsInit(AtPdhDe3 self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TxFeacSet);
        mMethodOverride(m_methods, TxFeacGet);
        mMethodOverride(m_methods, RxFeacGet);
        mMethodOverride(m_methods, TxFeacWithModeSet);
        mMethodOverride(m_methods, TxFeacModeGet);

        mMethodOverride(m_methods, TxTtiSet);
        mMethodOverride(m_methods, TxTtiGet);
        mMethodOverride(m_methods, ExpectedTtiSet);
        mMethodOverride(m_methods, ExpectedTtiGet);
        mMethodOverride(m_methods, RxTtiGet);
        mMethodOverride(m_methods, TmEnable);
        mMethodOverride(m_methods, TmIsEnabled);
        mMethodOverride(m_methods, TxTmCodeSet);
        mMethodOverride(m_methods, TxTmCodeGet);
        mMethodOverride(m_methods, RxTmCodeGet);
        mMethodOverride(m_methods, TxPldTypeSet);
        mMethodOverride(m_methods, TxPldTypeGet);
        mMethodOverride(m_methods, RxPldTypeGet);
        mMethodOverride(m_methods, PathMessageMdlController);
        mMethodOverride(m_methods, TestSignalMdlController);
        mMethodOverride(m_methods, IdleSignalMdlController);
        mMethodOverride(m_methods, RxCurrentMdlControllerGet);
        mMethodOverride(m_methods, MdlStandardSet);
        mMethodOverride(m_methods, MdlStandardGet);
        mMethodOverride(m_methods, DataLinkOptionSet);
        mMethodOverride(m_methods, DataLinkOptionGet);
        mMethodOverride(m_methods, TxUdlBitOrderSet);
        mMethodOverride(m_methods, TxUdlBitOrderGet);
        mMethodOverride(m_methods, RxUdlBitOrderSet);
        mMethodOverride(m_methods, RxUdlBitOrderGet);
        mMethodOverride(m_methods, RxAicGet);
        mMethodOverride(m_methods, StuffModeSet);
        mMethodOverride(m_methods, StuffModeGet);
        mMethodOverride(m_methods, VcIdString);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtPdhChannel(AtPdhDe3 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, mMethodsGet(channel), sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, TypeGet);
        mMethodOverride(m_AtPdhChannelOverride, IsUnframed);
        mMethodOverride(m_AtPdhChannelOverride, SubChannelsInterruptDisable);
        mMethodOverride(m_AtPdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtPdhChannelOverride, DefectsCauseAlarmForwarding);
        mMethodOverride(m_AtPdhChannelOverride, NationalBitsSupported);
        mMethodOverride(m_AtPdhChannelOverride, FailureForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtPdhChannelOverride, AlarmAffectingDefaultSet);
        mMethodOverride(m_AtPdhChannelOverride, DefaultFrameMode);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, DataRateInBytesPer125Us);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectCreate);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectDelete);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, FailureProfilerObjectCreate);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        mMethodOverride(m_AtChannelOverride, VcgBinderCreate);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtPdhDe3 self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

AtPdhDe3 AtPdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelObjectInit((AtPdhChannel)self, channelId, module) == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eBool AtPdhDe3IsDs3(AtPdhDe3 self)
    {
    eAtPdhDe3FrameType frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if ((frameType == cAtPdhDs3Unfrm)             ||
        (frameType == cAtPdhDs3FrmCbitUnChn)      ||
        (frameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (frameType == cAtPdhDs3FrmCbitChnl21E1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl28Ds1s)  ||
        (frameType == cAtPdhDs3FrmM13Chnl21E1s)   ||
        (frameType == cAtPdhDs3FrmM23))
        return cAtTrue;

    return cAtFalse;
    }

eBool AtPdhDe3IsE3(AtPdhDe3 self)
    {
    return AtPdhDe3FrameTypeIsE3(AtPdhChannelFrameTypeGet((AtPdhChannel)self));
    }

eBool AtPdhDe3FrameTypeIsE3(eAtPdhDe3FrameType frameType)
    {
    if ((frameType == cAtPdhE3Unfrm)   ||
        (frameType == cAtPdhE3Frmg832) ||
        (frameType == cAtPdhE3FrmG751) ||
        (frameType == cAtPdhE3FrmG751Chnl16E1s))
        return cAtTrue;

    return cAtFalse;
    }

eBool AtPdhDe3IsChannelized(AtPdhDe3 self)
    {
    return AtPdhDe3IsChannelizedFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)self));
    }

eBool AtPdhDe3IsE1Channelized(AtPdhDe3 self)
    {
    return AtPdhDe3IsE1ChannelizedFrameType(AtPdhChannelFrameTypeGet((AtPdhChannel)self));
    }

eBool AtPdhDe3IsDs1Channelized(AtPdhDe3 self)
    {
    return AtPdhDe3IsChannelizedToDs1(AtPdhChannelFrameTypeGet((AtPdhChannel)self));
    }

eBool AtPdhDe3IsChannelizedFrameType(uint16 de3FrameType)
    {
    if ((de3FrameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (de3FrameType == cAtPdhDs3FrmCbitChnl21E1s)  ||
        (de3FrameType == cAtPdhDs3FrmM13Chnl21E1s)   ||
        (de3FrameType == cAtPdhDs3FrmM13Chnl28Ds1s)  ||
        (de3FrameType == cAtPdhE3FrmG751Chnl16E1s))
        return cAtTrue;
    return cAtFalse;
    }

eBool AtPdhDe3IsE1ChannelizedFrameType(uint16 de3FrameType)
    {
    if ((de3FrameType == cAtPdhDs3FrmCbitChnl21E1s)  ||
        (de3FrameType == cAtPdhDs3FrmM13Chnl21E1s)   ||
        (de3FrameType == cAtPdhE3FrmG751Chnl16E1s))
        return cAtTrue;
    return cAtFalse;
    }

eBool AtPdhDe3IsChannelizedToDs1(uint16 de3FrameType)
    {
    if ((de3FrameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (de3FrameType == cAtPdhDs3FrmM13Chnl28Ds1s))
        return cAtTrue;

    return cAtFalse;
    }

eBool AtPdhDe3IsDs3CbitFrameType(uint32 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs3FrmCbitUnChn     : return cAtTrue;
        case cAtPdhDs3FrmCbitChnl28Ds1s: return cAtTrue;
        case cAtPdhDs3FrmCbitChnl21E1s : return cAtTrue;
        default: return cAtFalse;
        }
    }

eBool AtPdhDe3FrameTypeIsUnframed(uint32 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs3Unfrm : return cAtTrue;
        case cAtPdhE3Unfrm  : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

eBool AtPdhDs3DataLinkIsApplicable(AtPdhDe3 self)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);

    if ((frameType == cAtPdhDs3FrmCbitChnl21E1s) ||
        (frameType == cAtPdhDs3FrmCbitChnl28Ds1s) ||
        (frameType == cAtPdhDs3FrmCbitUnChn))
        return cAtTrue;

    return cAtFalse;
    }

/*
 * (DS3) Set data-link option
 *
 * @param self This channel
 * @param option @ref eAtPdhDe3DataLinkOption "Data-Link option".
 *
 * @return AT Return code
 */
eAtRet AtPdhDe3DataLinkOptionSet(AtPdhDe3 self, eAtPdhDe3DataLinkOption option)
    {
    mNumericalAttributeSet(DataLinkOptionSet, option);
    }

/*
 * (DS3) Get data-link option of DS3 controller
 *
 * @param self This channel
 *
 * @retval @ref eAtPdhDe3DataLinkOption "Data-Link option"
 */
eAtPdhDe3DataLinkOption AtPdhDe3DataLinkOptionGet(AtPdhDe3 self)
    {
    mAttributeGet(DataLinkOptionGet, uint32, cAtPdhDe3DataLinkOptionUnknown);
    }

uint16 AtPdhDe3UnChannelizedFrameType(uint16 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs3Unfrm            : return cAtPdhDs3Unfrm;
        case cAtPdhDs3FrmCbitUnChn     : return cAtPdhDs3FrmCbitUnChn;
        case cAtPdhDs3FrmCbitChnl28Ds1s: return cAtPdhDs3FrmCbitUnChn;
        case cAtPdhDs3FrmCbitChnl21E1s : return cAtPdhDs3FrmCbitUnChn;
        case cAtPdhE3Unfrm             : return cAtPdhE3Unfrm;
        case cAtPdhE3Frmg832           : return cAtPdhE3Frmg832;
        default:
            return cAtPdhDe3FrameUnknown;
        }
    }

/**
 * @addtogroup AtPdhDe3
 * @{
 */

/**
 * (DS3) Enable/disable IDLE code transmitting
 *
 * @param self This channel
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 * @see AtPdhChannelIdleEnable()
 */
eAtModulePdhRet AtPdhDe3TxIdleEnable(AtPdhDe3 self, eBool enable)
    {
    mOneParamWrapCall(enable, eAtModulePdhRet, AtPdhChannelIdleEnable((AtPdhChannel)self, enable));
    }

/**
 * (DS3) Check if IDLE code transmitting is enabled
 *
 * @param self This channel
 *
 * @retval cAtTrue IDLE code transmitting is enabled
 * @retval cAtFalse IDLE code transmitting is disabled
 */
eBool AtPdhDe3TxIdleIsEnabled(AtPdhDe3 self)
    {
    return AtPdhChannelIdleIsEnabled((AtPdhChannel)self);
    }

/**
 * (DS3) Send FEAC code
 *
 * @param self This channel
 * @param signalType @ref eAtPdhDe3FeacType "Type of signal".
 * @param xcode Six bit (x) in the FEAC pattern 0xxxxxx0-11111111. See @ref eAtPdhDe3FeacType "FEAC types"
 *
 * @return AT return
 */
eAtModulePdhRet AtPdhDe3TxFeacSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 xcode)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (FeacIsApplicable(self))
        {
        mTwoParamsAttributeSet(TxFeacSet, signalType, xcode);
        }

    return cAtErrorNotApplicable;
    }

/**
 * (DS3) Get sent FEAC code
 *
 * @param self This channel
 * @param [out] signalType @ref eAtPdhDe3FeacType "Type of signal"
 * @param [out] xcode Six bit (x) in the FEAC pattern 0xxxxxx0-11111111.
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *xcode)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (FeacIsApplicable(self))
        return mMethodsGet(self)->TxFeacGet(self, signalType, xcode);

    return cAtErrorNotApplicable;
    }

/**
 * (DS3) Get received FEAC code
 *
 * @param self This channel
 * @param [out] signalType @ref eAtPdhDe3FeacType "Type of signal"
 * @param [out] xcode Six bit (x) in the FEAC pattern 0xxxxxx0-11111111.
 *
 * @return AT return
 */
eAtModulePdhRet AtPdhDe3RxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *xcode)
    {
    if (signalType)
        *signalType = cAtPdhDe3FeacTypeUnknown;

    if (!De3IsValid(self))
         return cAtErrorNullPointer;

    if (FeacIsApplicable(self))
        return mMethodsGet(self)->RxFeacGet(self, signalType, xcode);

    return cAtErrorNotApplicable;
    }


/**
 * (DS3) Send FEAC code with sending mode
 *
 * @param self This channel
 * @param signalType @ref eAtPdhDe3FeacType "Type of signal".
 * @param xcode Six bit (x) in the FEAC pattern 0xxxxxx0-11111111. See @ref eAtPdhDe3FeacType "FEAC types"
 * @param sentMode @ref eAtPdhBomSentMode "Sending mode".
 *
 * @return AT return
 */
eAtModulePdhRet AtPdhDe3TxFeacWithModeSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 xcode, eAtPdhBomSentMode sentMode)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (FeacIsApplicable(self))
        {
        mThreeParamsAttributeSet(TxFeacWithModeSet, signalType, xcode, sentMode);
        }

    return cAtErrorNotApplicable;
    }

/**
 * (DS3) Get FEAC sending mode
 *
 * @param self This channel
 *
 * @return mode @ref eAtPdhBomSentMode "Sending mode".
 */
eAtPdhBomSentMode AtPdhDe3TxFeacModeGet(AtPdhDe3 self)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    return mMethodsGet(self)->TxFeacModeGet(self);
    }

/**
 * (E3) Set transmitted TTI (Jn)
 *
 * @param self This channel
 * @param tti TTI message
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TxTtiSet(AtPdhDe3 self, const tAtPdhTti *tti)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->TxTtiSet(self, tti);

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Get transmitted TTI (Jn)
 *
 * @param self This channel
 * @param [out] tti TTI message
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TxTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->TxTtiGet(self, tti);

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Set expected TTI (Jn)
 *
 * @param self This channel
 * @param tti TTI message
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3ExpectedTtiSet(AtPdhDe3 self, const tAtPdhTti *tti)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->ExpectedTtiSet(self, tti);

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Get expected TTI (Jn)
 *
 * @param self This channel
 * @param [out] tti TTI message
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3ExpectedTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->ExpectedTtiGet(self, tti);

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Get received TTI (Jn)
 *
 * @param self This channel
 * @param [out] tti TTI message
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3RxTtiGet(AtPdhDe3 self, tAtPdhTti *tti)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->RxTtiGet(self, tti);

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Enable/disable Timing Maker transmitting
 *
 * @param self This channel
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TmEnable(AtPdhDe3 self, eBool enable)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        {
        mNumericalAttributeSet(TmEnable, enable);
        }

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Check if Timing Maker transmitting is enabled
 *
 * @param self This channel
 *
 * @retval cAtTrue Timing Maker transmitting is enabled
 * @retval cAtFalse Timing Maker transmitting is disabled
 */
eBool AtPdhDe3TmIsEnabled(AtPdhDe3 self)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->TmIsEnabled(self);

    return cAtFalse;
    }

/**
 * (E3) Set transmitted Timing Maker code
 *
 * @param self This channel
 * @param tmCode Timing Maker code
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TxTmCodeSet(AtPdhDe3 self, uint8 tmCode)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        {
        mNumericalAttributeSet(TxTmCodeSet, tmCode);
        }

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Get transmitted Timing Maker code
 *
 * @param self This channel
 *
 * @return Transmitted Timing Maker code
 */
uint8 AtPdhDe3TxTmCodeGet(AtPdhDe3 self)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->TxTmCodeGet(self);

    return 0;
    }

/**
 * (E3) Get received Timing Maker code
 *
 * @param self This channel
 *
 * @return Received Timing Maker code
 */
uint8 AtPdhDe3RxTmCodeGet(AtPdhDe3 self)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->RxTmCodeGet(self);

    return 0;
    }

/**
 * (E3-G832/T3-CBIT) Set transmitted SSM
 *
 * @param self This channel
 * @param ssm SSM value
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TxSsmSet(AtPdhDe3 self, uint8 ssm)
    {
    if (SsmIsApplicable(self))
        {
        mOneParamWrapCall(ssm, eAtModulePdhRet, AtPdhChannelTxSsmSet((AtPdhChannel) self, ssm));
        }

    return cAtErrorNotApplicable;
    }

/**
 * (E3-G832/T3-CBIT) Get transmitted SSM
 *
 * @param self This channel
 *
 * @return Transmitted SSM
 */
uint8 AtPdhDe3TxSsmGet(AtPdhDe3 self)
    {
    if (SsmIsApplicable(self))
        return AtPdhChannelTxSsmGet((AtPdhChannel) self);
    return 0;
    }

/**
 * (E3-G832/T3-CBIT) Get received SSM value
 *
 * @param self This channel
 *
 * @return Received SSM value
 */
uint8 AtPdhDe3RxSsmGet(AtPdhDe3 self)
    {
    if (SsmIsApplicable(self))
        return AtPdhChannelRxSsmGet((AtPdhChannel) self);
    return 0;
    }

/**
 * (E3) Set transmitted payload type value
 *
 * @param self This channel
 * @param pldType Payload type value
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TxPldTypeSet(AtPdhDe3 self, uint8 pldType)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        {
        mNumericalAttributeSet(TxPldTypeSet, pldType);
        }

    return cAtErrorNotApplicable;
    }

/**
 * (E3) Get transmitted payload type value
 *
 * @param self This channel
 *
 * @return Transmitted payload type value
 */
uint8 AtPdhDe3TxPldTypeGet(AtPdhDe3 self)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->TxPldTypeGet(self);

    return 0;
    }

/**
 * (E3) Get received payload type value
 *
 * @param self This channel
 *
 * @return Received payload type value
 */
uint8 AtPdhDe3RxPldTypeGet(AtPdhDe3 self)
    {
    if (!De3IsValid(self))
        return cAtErrorNullPointer;

    if (IsE3G832(self))
        return mMethodsGet(self)->RxPldTypeGet(self);

    return 0;
    }

/**
 * Get DE2 channel mapped to this DE3
 *
 * @param self DE3 channel
 * @param de2Id DE2 ID
 *
 * @return DE2 channel
 */
AtPdhDe2 AtPdhDe3De2Get(AtPdhDe3 self, uint8 de2Id)
    {
    if (De3IsValid(self))
        return (AtPdhDe2)AtPdhChannelSubChannelGet((AtPdhChannel)self, de2Id);

    return NULL;
    }

/**
 * Get DE1 channel mapped to this DE3
 *
 * @param self DE3 channel
 * @param de2Id DE2 ID
 * @param de1Id DE1 ID
 *
 * @return DE1 channel
 */
AtPdhDe1 AtPdhDe3De1Get(AtPdhDe3 self, uint8 de2Id, uint8 de1Id)
    {
    AtPdhChannel de2;

    if (!De3IsValid(self))
        return NULL;

    de2 = AtPdhChannelSubChannelGet((AtPdhChannel)self, de2Id);
    if (de2 == NULL)
        return NULL;

    return (AtPdhDe1)AtPdhChannelSubChannelGet(de2, de1Id);
    }

/**
 * (DS3) Get path message MDL controller
 *
 * @param self This channel
 *
 * @return Path message @ref AtPdhMdlController "MDL controller"
 */
AtPdhMdlController AtPdhDe3PathMessageMdlController(AtPdhDe3 self)
    {
    mNoParamObjectGet(PathMessageMdlController, AtPdhMdlController);
    }

/**
 * (DS3) Get test signal MDL controller
 *
 * @param self This channel
 *
 * @return Test signal @ref AtPdhMdlController "MDL controller
 */
AtPdhMdlController AtPdhDe3TestSignalMdlController(AtPdhDe3 self)
    {
    mNoParamObjectGet(TestSignalMdlController, AtPdhMdlController);
    }

/**
 * (DS3) Get MDL controller
 *
 * @param self This channel
 *
 * @return IDLE signal @ref AtPdhMdlController "MDL controller
 */
AtPdhMdlController AtPdhDe3IdleSignalMdlController(AtPdhDe3 self)
    {
    mNoParamObjectGet(IdleSignalMdlController, AtPdhMdlController);
    }

/**
 * (DS3) Get current MDL controller of RX DS3 to get RX MDL message
 *
 * @param self This channel
 *
 * @return IDLE signal @ref AtPdhMdlController "MDL controller
 */
AtPdhMdlController AtPdhDe3RxCurrentMdlControllerGet(AtPdhDe3 self)
    {
    if (self)
        return mMethodsGet(self)->RxCurrentMdlControllerGet(self);
    return NULL;
    }

/**
 * (DS3) Set standard
 *
 * @param self This channel
 *
 * @param standard @ref eAtPdhMdlStandard "Standard"
 *
 * @return AT Return code
 */
eAtRet AtPdhDe3MdlStandardSet(AtPdhDe3 self, eAtPdhMdlStandard standard)
    {
    mNumericalAttributeSet(MdlStandardSet, standard);
    }

/**
 * (DS3) Get STD of controller
 *
 * @param self This channel
 *
 * @return @ref eAtPdhMdlStandard "MDL standard".
 */
uint32 AtPdhDe3MdlStandardGet(AtPdhDe3 self)
    {
    mAttributeGet(MdlStandardGet, uint32, cAtPdhMdlStandardUnknown);
    }

/**
 * (T3-CBIT) Set UDL transmit bit ordering
 *
 * @param self This channel
 * @param bitOrder @ref eAtBitOrder "Bit ordering"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3TxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (AtPdhDs3DataLinkIsApplicable(self))
        {
        mNumericalAttributeSet(TxUdlBitOrderSet, bitOrder);
        }

    return cAtErrorModeNotSupport;
    }

/**
 * (T3-CBIT) Get UDL transmit bit ordering
 *
 * @param self This channel
 *
 * @return @ref eAtBitOrder "Bit ordering"
 */
eAtBitOrder AtPdhDe3TxUdlBitOrderGet(AtPdhDe3 self)
    {
    if (self == NULL)
        return cAtBitOrderUnknown;

    if (AtPdhDs3DataLinkIsApplicable(self))
        return mMethodsGet(self)->TxUdlBitOrderGet(self);

    return cAtBitOrderNotApplicable;
    }

/**
 * (T3-CBIT) Set UDL receive bit ordering
 *
 * @param self This channel
 * @param bitOrder @ref eAtBitOrder "Bit ordering"
 *
 * @return AT return code
 */
eAtModulePdhRet AtPdhDe3RxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (AtPdhDs3DataLinkIsApplicable(self))
        {
        mNumericalAttributeSet(RxUdlBitOrderSet, bitOrder);
        }

    return cAtErrorModeNotSupport;
    }

/**
 * (T3-CBIT) Get UDL receive bit ordering
 *
 * @param self This channel
 *
 * @return @ref eAtBitOrder "Bit ordering"
 */
eAtBitOrder AtPdhDe3RxUdlBitOrderGet(AtPdhDe3 self)
    {
    if (self == NULL)
        return cAtPrbsBitOrderUnknown;

    if (AtPdhDs3DataLinkIsApplicable(self))
        return mMethodsGet(self)->RxUdlBitOrderGet(self);

    return cAtBitOrderNotApplicable;
    }

/**
 * (T3-CBIT) Get received AIC bit
 *
 * @param self This channel
 *
 * @return Received AIC bit
 */
uint8 AtPdhDe3RxAicGet(AtPdhDe3 self)
    {
    if (!De3IsValid(self))
        return 0;

    if (IsDs3Cbit(self))
        return mMethodsGet(self)->RxAicGet(self);

    return 0;
    }

eAtRet AtPdhDe3StuffModeSet(AtPdhDe3 self, eAtPdhDe3StuffMode mode)
    {
    if (self)
        return mMethodsGet(self)->StuffModeSet(self, mode);

    return cAtOk;
    }

eAtPdhDe3StuffMode AtPdhDe3StuffModeGet(AtPdhDe3 self)
    {
    if (self)
        return mMethodsGet(self)->StuffModeGet(self);

    return cAtPdhDe3StuffModeFineStuff;
    }


/**
 * @}
 */

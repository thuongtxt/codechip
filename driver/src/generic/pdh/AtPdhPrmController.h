/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhPrmController.h
 * 
 * Created Date: Nov 7, 2015
 *
 * Description : PRM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHPRMCONTROLLER_H_
#define _ATPDHPRMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhPrmController *AtPdhPrmController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 AtPdhPrmControllerDe1Get(AtPdhPrmController self);

/* LB bit */
eAtModulePdhRet AtPdhPrmControllerTxLBBitEnable(AtPdhPrmController self, eBool enable);
eBool AtPdhPrmControllerTxLBBitIsEnabled(AtPdhPrmController self);
eAtModulePdhRet AtPdhPrmControllerTxLBBitSet(AtPdhPrmController self, uint8 value);
uint8 AtPdhPrmControllerTxLBBitGet(AtPdhPrmController self);
uint8 AtPdhPrmControllerRxLBBitGet(AtPdhPrmController self);

/* CR bit */
eAtModulePdhRet AtPdhPrmControllerTxCRBitSet(AtPdhPrmController self, uint8 value);
uint8 AtPdhPrmControllerTxCRBitGet(AtPdhPrmController self);
eAtModulePdhRet AtPdhPrmControllerExpectedCRBitSet(AtPdhPrmController self, uint8 value);
uint8 AtPdhPrmControllerExpectedCRBitGet(AtPdhPrmController self);

/* Enable */
eAtModulePdhRet AtPdhPrmControllerEnable(AtPdhPrmController self, eBool enable);
eBool AtPdhPrmControllerIsEnabled(AtPdhPrmController self);

/* Standard */
eAtModulePdhRet AtPdhPrmControllerRxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard);
eAtPdhDe1PrmStandard AtPdhPrmControllerRxStandardGet(AtPdhPrmController self);
eAtModulePdhRet AtPdhPrmControllerTxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard);
eAtPdhDe1PrmStandard AtPdhPrmControllerTxStandardGet(AtPdhPrmController self);
eAtModulePdhRet AtPdhPrmControllerStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard);
eAtPdhDe1PrmStandard AtPdhPrmControllerStandardGet(AtPdhPrmController self);

/* Counter and alarm */
uint32 AtPdhPrmControllerAlarmInterruptGet(AtPdhPrmController self);
uint32 AtPdhPrmControllerAlarmInterruptClear(AtPdhPrmController self);
uint32 AtPdhPrmControllerCounterGet(AtPdhPrmController self, uint16 counterType);
uint32 AtPdhPrmControllerCounterClear(AtPdhPrmController self, uint16 counterType);

uint32 AtPdhPrmControllerDefectHistoryClear(AtPdhPrmController self);
uint32 AtPdhPrmControllerDefectHistoryGet(AtPdhPrmController self);
eAtModulePdhRet AtPdhPrmControllerInterruptMaskSet(AtPdhPrmController self, uint32 defectMask, uint32 enableMask);
uint32 AtPdhPrmControllerInterruptMaskGet(AtPdhPrmController self);

/* Debug */
AtList AtPdhPrmControllerCaptureRxMessages(AtPdhPrmController self);
eAtRet AtPdhPrmControllerCaptureStart(AtPdhPrmController self);
eAtRet AtPdhPrmControllerCaptureStop(AtPdhPrmController self);
eBool AtPdhPrmControllerCaptureIsStarted(AtPdhPrmController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHPRMCONTROLLER_H_ */


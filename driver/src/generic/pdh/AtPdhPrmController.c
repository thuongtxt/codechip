/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhPrmController.c
 *
 * Created Date: Oct 27, 2015
 *
 * Description : PRM Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPdhPrmControllerInternal.h"
#include "../man/AtDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPdhPrmControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePdhRet StandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    AtUnused(self);
    AtUnused(standard);
    return cAtErrorNotImplemented;
    }

static eAtPdhDe1PrmStandard StandardGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtPdhDe1PrmStandardUnknown;
    }

static eAtModulePdhRet TxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    AtUnused(self);
    AtUnused(standard);
    return cAtErrorNotImplemented;
    }

static eAtPdhDe1PrmStandard TxStandardGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtPdhDe1PrmStandardUnknown;
    }

static eAtModulePdhRet RxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    AtUnused(self);
    AtUnused(standard);
    return cAtErrorNotImplemented;
    }

static eAtPdhDe1PrmStandard RxStandardGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtPdhDe1PrmStandardUnknown;
    }

static uint32 CounterGet(AtPdhPrmController self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 CounterClear(AtPdhPrmController self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 AlarmInterruptGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmInterruptClear(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePdhRet Enable(AtPdhPrmController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool IsEnabled(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePdhRet TxCRBitSet(AtPdhPrmController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint8 TxCRBitGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePdhRet CRBitExpectSet(AtPdhPrmController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint8 CRBitExpectGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePdhRet TxLBBitSet(AtPdhPrmController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorNotImplemented;
    }

static uint8 TxLBBitGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePdhRet TxLBBitEnable(AtPdhPrmController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool TxLBBitIsEnabled(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 RxLBBitGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePdhRet InterruptMaskSet(AtPdhPrmController self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    AtUnused(enableMask);
    return 0;
    }

static uint32 InterruptMaskGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return 0;
    }

static void InterruptProcess(AtPdhPrmController self, uint8 slice, uint8 de3, uint8 de2, uint8 de1, AtHal hal)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(de3);
    AtUnused(de2);
    AtUnused(de1);
    AtUnused(hal);
    }

static eAtModulePdhRet FcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order)
    {
    AtUnused(self);
    AtUnused(order);
    return cAtErrorNotImplemented;
    }

static eAtBitOrder FcsBitOrderGet(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtBitOrderUnknown;
    }

static eAtModulePdhRet Init(AtPdhPrmController self)
    {
    static const uint8 cResetValue = 0x0;

    AtPdhPrmControllerEnable(self, cAtFalse);
    AtPdhPrmControllerTxCRBitSet(self, cResetValue);
    AtPdhPrmControllerExpectedCRBitSet(self, cResetValue);
    AtPdhPrmFcsBitOrderSet(self, cAtBitOrderLsb);
    AtPdhPrmControllerStandardSet(self, cAtPdhDe1PrmStandardAnsi);
    return cAtOk;
    }

static const char *ToString(AtObject self)
    {
    static char string[32];
    AtChannel channel = (AtChannel)AtPdhPrmControllerDe1Get((AtPdhPrmController)self);
    AtDevice dev = AtChannelDeviceGet(channel);

    AtSnprintf(string, sizeof(string), "%sprm_%s.%s", AtDeviceIdToString(dev), AtChannelTypeString(channel), AtChannelIdString(channel));
    return string;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPdhPrmController object = (AtPdhPrmController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(de1);
    }

static AtList CaptureRxMessages(AtPdhPrmController self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet CaptureStart(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet CaptureStop(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eBool CaptureIsStarted(AtPdhPrmController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtPdhPrmController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPdhPrmController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtPdhPrmController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, StandardSet);
        mMethodOverride(m_methods, StandardGet);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, TxStandardSet);
        mMethodOverride(m_methods, TxStandardGet);
        mMethodOverride(m_methods, RxStandardSet);
        mMethodOverride(m_methods, RxStandardGet);
        mMethodOverride(m_methods, TxCRBitSet);
        mMethodOverride(m_methods, TxCRBitGet);
        mMethodOverride(m_methods, CounterGet);
        mMethodOverride(m_methods, CounterClear);
        mMethodOverride(m_methods, AlarmInterruptGet);
        mMethodOverride(m_methods, AlarmInterruptClear);
        mMethodOverride(m_methods, CRBitExpectSet);
        mMethodOverride(m_methods, CRBitExpectGet);
        mMethodOverride(m_methods, TxLBBitSet);
        mMethodOverride(m_methods, TxLBBitGet);
        mMethodOverride(m_methods, TxLBBitEnable);
        mMethodOverride(m_methods, TxLBBitIsEnabled);
        mMethodOverride(m_methods, RxLBBitGet);
        mMethodOverride(m_methods, DefectHistoryClear);
        mMethodOverride(m_methods, DefectHistoryGet);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, FcsBitOrderSet);
        mMethodOverride(m_methods, FcsBitOrderGet);
        mMethodOverride(m_methods, CaptureRxMessages);
        mMethodOverride(m_methods, CaptureStart);
        mMethodOverride(m_methods, CaptureStop);
        mMethodOverride(m_methods, CaptureIsStarted);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhPrmController);
    }

AtPdhPrmController AtPdhPrmControllerObjectInit(AtPdhPrmController self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->de1 = de1;

    return self;
    }

AtPdhDe1 AtPdhPrmControllerDe1Get(AtPdhPrmController self)
    {
    return self ? self->de1 : NULL;
    }

eAtModulePdhRet AtPdhPrmControllerTxLBBitEnable(AtPdhPrmController self, eBool enable)
    {
    mNumericalAttributeSet(TxLBBitEnable, enable);
    }

eBool AtPdhPrmControllerTxLBBitIsEnabled(AtPdhPrmController self)
    {
    mAttributeGet(TxLBBitIsEnabled, eBool, cAtFalse);
    }

uint32 AtPdhPrmControllerCounterGet(AtPdhPrmController self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterGet(self, counterType);
    return 0;
    }

uint32 AtPdhPrmControllerCounterClear(AtPdhPrmController self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterClear(self, counterType);
    return 0;
    }

uint32 AtPdhPrmControllerAlarmInterruptGet(AtPdhPrmController self)
    {
    if (self)
        return mMethodsGet(self)->AlarmInterruptGet(self);
    return 0;
    }

uint32 AtPdhPrmControllerAlarmInterruptClear(AtPdhPrmController self)
    {
    if(self)
        return mMethodsGet(self)->AlarmInterruptClear(self);
    return 0;
    }

eAtModulePdhRet AtPdhPrmControllerStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    mNumericalAttributeSet(StandardSet, standard);
    }

eAtPdhDe1PrmStandard AtPdhPrmControllerStandardGet(AtPdhPrmController self)
    {
    mAttributeGet(StandardGet, uint32, cAtPdhDe1PrmStandardUnknown);
    }

eAtModulePdhRet AtPdhPrmControllerTxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    mNumericalAttributeSet(TxStandardSet, standard);
    }

eAtPdhDe1PrmStandard AtPdhPrmControllerTxStandardGet(AtPdhPrmController self)
    {
    mAttributeGet(TxStandardGet, uint32, cAtPdhDe1PrmStandardUnknown);
    }

eAtModulePdhRet AtPdhPrmControllerRxStandardSet(AtPdhPrmController self, eAtPdhDe1PrmStandard standard)
    {
    mNumericalAttributeSet(RxStandardSet, standard);
    }

eAtPdhDe1PrmStandard AtPdhPrmControllerRxStandardGet(AtPdhPrmController self)
    {
    mAttributeGet(RxStandardGet, uint32, cAtPdhDe1PrmStandardUnknown);
    }

eAtModulePdhRet AtPdhPrmControllerEnable(AtPdhPrmController self, eBool enable)
    {
    mNumericalAttributeSet(Enable, enable);
    }

eBool AtPdhPrmControllerIsEnabled(AtPdhPrmController self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

eAtModulePdhRet AtPdhPrmControllerTxCRBitSet(AtPdhPrmController self, uint8 value)
    {
    mNumericalAttributeSet(TxCRBitSet, value);
    }

uint8 AtPdhPrmControllerTxCRBitGet(AtPdhPrmController self)
    {
    mAttributeGet(TxCRBitGet, uint8, 0);
    }

eAtModulePdhRet AtPdhPrmControllerExpectedCRBitSet(AtPdhPrmController self, uint8 value)
    {
    mNumericalAttributeSet(CRBitExpectSet, value);
    }

uint8 AtPdhPrmControllerExpectedCRBitGet(AtPdhPrmController self)
    {
    mAttributeGet(CRBitExpectGet, uint8, 0);
    }

eAtModulePdhRet AtPdhPrmControllerTxLBBitSet(AtPdhPrmController self, uint8 value)
    {
    mNumericalAttributeSet(TxLBBitSet, value);
    }

uint8 AtPdhPrmControllerTxLBBitGet(AtPdhPrmController self)
    {
    mAttributeGet(TxLBBitGet, uint8, 0);
    }

uint8 AtPdhPrmControllerRxLBBitGet(AtPdhPrmController self)
    {
    mAttributeGet(RxLBBitGet, uint8, 0);
    }

uint32 AtPdhPrmControllerDefectHistoryClear(AtPdhPrmController self)
    {
    mAttributeGet(DefectHistoryClear, uint32, 0);
    }

uint32 AtPdhPrmControllerDefectHistoryGet(AtPdhPrmController self)
    {
    mAttributeGet(DefectHistoryGet, uint32, 0);
    }

eAtModulePdhRet AtPdhPrmControllerInterruptMaskSet(AtPdhPrmController self, uint32 defectMask, uint32 enableMask)
    {
    mTwoParamsAttributeSet(InterruptMaskSet, defectMask, enableMask);
    }

uint32 AtPdhPrmControllerInterruptMaskGet(AtPdhPrmController self)
    {
    mAttributeGet(InterruptMaskGet, uint32, 0);
    }

void AtPdhPrmControllerInterruptProcess(AtPdhPrmController self, uint8 slice, uint8 de3, uint8 de2, uint8 de1, AtHal hal)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self, slice, de3, de2, de1, hal);
    }

eAtModulePdhRet AtPdhPrmControllerInit(AtPdhPrmController self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);

    return cAtErrorNullPointer;
    }

eAtModulePdhRet AtPdhPrmFcsBitOrderSet(AtPdhPrmController self, eAtBitOrder order)
    {
    if (self)
        return mMethodsGet(self)->FcsBitOrderSet(self, order);

    return cAtErrorNullPointer;
    }

eAtBitOrder AtPdhPrmFcsBitOrderGet(AtPdhPrmController self)
    {
    if (self)
        return mMethodsGet(self)->FcsBitOrderGet(self);

    return cAtBitOrderUnknown;
    }

AtList AtPdhPrmControllerCaptureRxMessages(AtPdhPrmController self)
    {
    if (self)
        return mMethodsGet(self)->CaptureRxMessages(self);
    return NULL;
    }

eAtRet AtPdhPrmControllerCaptureStart(AtPdhPrmController self)
    {
    if (self)
        return mMethodsGet(self)->CaptureStart(self);
    return cAtErrorObjectNotExist;
    }

eAtRet AtPdhPrmControllerCaptureStop(AtPdhPrmController self)
    {
    if (self)
        return mMethodsGet(self)->CaptureStop(self);
    return cAtErrorObjectNotExist;
    }

eBool AtPdhPrmControllerCaptureIsStarted(AtPdhPrmController self)
    {
    if (self)
        return mMethodsGet(self)->CaptureIsStarted(self);
    return cAtFalse;
    }

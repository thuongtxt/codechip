/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhDe2.c
 *
 * Created Date: Jun 12, 2014
 *
 * Description : DS2/E2
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhDe2Internal.h"
#include "AtModulePdhInternal.h"
#include "AtPdhDe3.h"
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumDs1InDs2 4
#define cNumE1InDs2  3
#define cNumE1InE2   4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPdhDe2)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* Save super implementation */
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "de2";
    }

static AtPdhChannel De1Create(AtPdhChannel self, uint8 de1Id)
    {
    AtModulePdh pdhModule = (AtModulePdh)AtChannelModuleGet((AtChannel)self);
    return (AtPdhChannel)AtModulePdhDe2De1Create(pdhModule, (AtPdhDe2)self, de1Id);
    }

static AtPdhChannel *AllSubChannelsCreate(AtPdhChannel self, uint8 numSubChannels)
    {
    return AtPdhChannelAllSubChannelsCreate(self, numSubChannels, De1Create);
    }

static uint8 NumSubChannelsForFrameType(AtPdhChannel self, uint16 frameType)
    {
    AtUnused(self);

    if ((frameType == cAtPdhDs2T1_107Carrying4Ds1s) ||
        (frameType == cAtPdhE2G_742Carrying4E1s))
        return cNumDs1InDs2;

    if (frameType == cAtPdhDs2G_747Carrying3E1s)
        return cNumE1InDs2;

    return 0x0;
    }

static uint16 De1FrameTypeDefault(uint16 frameType)
    {
    if (frameType == cAtPdhDs2T1_107Carrying4Ds1s)
        return cAtPdhDs1J1UnFrm;

    if ((frameType == cAtPdhDs2G_747Carrying3E1s) ||
        (frameType == cAtPdhE2G_742Carrying4E1s))
        return cAtPdhE1UnFrm;

    return cAtPdhDe1FrameUnknown;
    }

static eBool ShouldCreateSubChannels(AtPdhChannel self)
    {
    AtPdhChannel de3 = AtPdhChannelParentChannelGet(self);
    AtPw pw = AtChannelBoundPwGet((AtChannel)de3);
    return (pw) ? cAtFalse : cAtTrue;
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    uint8 numSubChannels, channel_i;
    AtPdhChannel *newSubChannels = NULL;

    numSubChannels = NumSubChannelsForFrameType(self, frameType);

    /* Number of subChannel is not change, don't need to re-create sub channels
     * but frame type of DE3 is changed so frame type of it's sub-channels need to be updated follow */
    if (numSubChannels == AtPdhChannelNumberOfSubChannelsGet(self))
        {
        eAtRet ret = cAtOk;
        for (channel_i = 0; channel_i < numSubChannels; channel_i++)
            {
            AtPdhChannel de1 = AtPdhChannelSubChannelGet(self, channel_i);
            ret |= AtPdhChannelFrameTypeNoLockSet(de1, De1FrameTypeDefault(frameType));
            }

        return ret;
        }

    /* Recreate all sub channels to have a clean database */
    AtPdhChannelAllSubChannelsDelete(self);

    /* No sub channels */
    if (numSubChannels == 0)
        return cAtOk;

    if (!ShouldCreateSubChannels(self))
        return cAtOk;

    /* Create all of sub channels */
    newSubChannels = AllSubChannelsCreate(self, numSubChannels);
    if (newSubChannels == NULL)
        return cAtErrorRsrcNoAvail;
    AtPdhChannelAllSubChannelsSet(self, newSubChannels, numSubChannels);

    return cAtOk;
    }
    
static eAtPdhChannelType TypeGet(AtPdhChannel self)
    {
    eAtPdhDe2FrameType frameMode = AtPdhChannelFrameTypeGet(self);

    if (frameMode == cAtPdhDe2FrameUnknown)
        return cAtPdhChannelTypeDe2Unknown;

    if (frameMode == cAtPdhE2G_742Carrying4E1s)
        return cAtPdhChannelTypeE2;

    return cAtPdhChannelTypeDs2;
    }

static eAtRet SubChannelsInterruptDisable(AtPdhChannel self, eBool onlySubChannels)
    {
    uint8 numSubChannels, channel_i;

    numSubChannels = AtPdhChannelNumberOfSubChannelsGet(self);
    for (channel_i = 0; channel_i < numSubChannels; channel_i++)
        {
        AtPdhChannel de1 = AtPdhChannelSubChannelGet(self, channel_i);
        AtPdhChannelSubChannelsInterruptDisable(de1, cAtFalse);
        }

    if (!onlySubChannels)
        {
        AtChannelSurEngineInterruptDisable((AtChannel)self);
        AtChannelInterruptMaskSet((AtChannel)self, AtChannelInterruptMaskGet((AtChannel)self), 0);
        }

    return cAtOk;
    }

static uint32 DefectsCauseAlarmForwarding(AtPdhChannel self)
    {
    AtUnused(self);
    return (cAtPdhDe2AlarmLof | cAtPdhDe2AlarmAis);
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    uint32 defect = AtChannelDefectGet((AtChannel)self);
    uint8 numSubChannels, channel_i;

    if (defect & AtPdhChannelDefectsCauseAlarmForwarding(self))
        return;

    AtUnused(subChannelsOnly);

    numSubChannels = AtPdhChannelNumberOfSubChannelsGet(self);
    for (channel_i = 0; channel_i < numSubChannels; channel_i++)
        {
        AtPdhChannel de1 = AtPdhChannelSubChannelGet(self, channel_i);
        AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(de1, cAtFalse);
        }
    }

static void FailureForwardingClearanceNotifyWithSubChannelOnly(AtPdhChannel self, eBool subChannelsOnly)
    {
    uint8 numSubChannels, channel_i;
    AtUnused(subChannelsOnly);

    numSubChannels = AtPdhChannelNumberOfSubChannelsGet(self);
    for (channel_i = 0; channel_i < numSubChannels; channel_i++)
        {
        AtPdhChannel de1 = AtPdhChannelSubChannelGet(self, channel_i);
        AtPdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly(de1, cAtFalse);
        }
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtPdhDe2SharedQuerier();
    }

static void OverrideAtChannel(AtPdhDe2 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtPdhChannel(AtPdhDe2 self)
    {
    AtPdhChannel channel = (AtPdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));

        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtPdhChannelOverride, TypeGet);
        mMethodOverride(m_AtPdhChannelOverride, SubChannelsInterruptDisable);
        mMethodOverride(m_AtPdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtPdhChannelOverride, DefectsCauseAlarmForwarding);
        mMethodOverride(m_AtPdhChannelOverride, FailureForwardingClearanceNotifyWithSubChannelOnly);
        }

    mMethodsSet(channel, &m_AtPdhChannelOverride);
    }

static void Override(AtPdhDe2 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhDe2);
    }

AtPdhDe2 AtPdhDe2ObjectInit(AtPdhDe2 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPdhChannelObjectInit((AtPdhChannel)self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet AtPdhDe2AllDe1sReCreate(AtPdhChannel self)
    {
    uint8 numSubChannels;
    AtPdhChannel *newSubChannels;

    if (self->subChannels)
        return cAtOk;

    numSubChannels = NumSubChannelsForFrameType(self, AtPdhChannelFrameTypeGet(self));
    newSubChannels = AllSubChannelsCreate(self, numSubChannels);
    if (newSubChannels == NULL)
        return cAtErrorRsrcNoAvail;

    AtPdhChannelAllSubChannelsSet(self, newSubChannels, numSubChannels);

    return cAtOk;
    }

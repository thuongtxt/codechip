/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhSerialLine.c
 *
 * Created Date: Apr 1, 2015
 *
 * Description : PDH Serial Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtPdhSerialLine.h"
#include "atclib.h"
#include "AtPdhSerialLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mChannelIsValid(self) ((self == NULL) ? cAtFalse : cAtTrue)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
static char m_methodsInit = 0;
static tAtPdhSerialLineMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "serial_line"; /*< Serial Line >*/
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    AtChannel channel = AtPdhSerialLineChannelGet((AtPdhSerialLine)self);
    if (channel)
        return AtChannelServiceIsRunning(channel);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPdhSerialLine object = (AtPdhSerialLine)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(mode);
    }

static void OverrideAtObject(AtPdhSerialLine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtPdhSerialLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static eAtRet ModeSet(AtPdhSerialLine self, uint32 mode)
    {
    self->mode = mode;
    return cAtOk;
    }

static uint32 ModeGet(AtPdhSerialLine self)
    {
    return self->mode;
    }

static AtChannel ChannelGet(AtPdhSerialLine self)
    {
    AtUnused(self);
    return NULL;
    }

static void MethodsInit(AtPdhSerialLine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ModeSet);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, ChannelGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtPdhSerialLine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

AtPdhSerialLine AtPdhSerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPdhSerialLine));

    /* Super constructor */
    if (AtPdhChannelObjectInit((AtPdhChannel)self, channelId, module) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Initialize implementation */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

AtChannel AtPdhSerialLineChannelGet(AtPdhSerialLine self)
    {
    if (self)
        return mMethodsGet(self)->ChannelGet(self);
    return NULL;
    }

eBool AtPdhSerialLineIsDe3Mode(AtPdhSerialLine self)
    {
    uint32 mode;

    if (self == NULL)
        return cAtFalse;

    mode = AtPdhSerialLineModeGet(self);
    if ((mode == cAtPdhDe3SerialLineModeDs3) || (mode == cAtPdhDe3SerialLineModeE3))
        return cAtTrue;

    return cAtFalse;
    }

/**
 * @addtogroup AtPdhSerialLine
 * @{
 */

/**
 * Set Serial Line working mode
 *
 * @param self This Serial Line
 * @param mode Working mode.
 *             - See @ref eAtPdhDe3SerialLineMode "DS3/E3 Serial Line working mode"
 *
 * @return AT return code.
 */
eAtRet AtPdhSerialLineModeSet(AtPdhSerialLine self, uint32 mode)
    {
    mNumericalAttributeSet(ModeSet, mode);
    }

/**
 * Get Serial Line working mode
 *
 * @param self This Serial Line
 *
 * @return Serial Line working mode. See @ref eAtPdhDe3SerialLineMode "DS3/E3 Serial Line working mode".
 */
uint32 AtPdhSerialLineModeGet(AtPdhSerialLine self)
    {
    mAttributeGet(ModeGet, uint32, cAtPdhDe3SerialLineModeUnknown);
    }

/**
 * @}
 */

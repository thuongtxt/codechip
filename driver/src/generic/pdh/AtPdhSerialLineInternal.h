/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhSerialLineInternal.h
 * 
 * Created Date: Sep 2, 2012
 *
 * Description : SerialLine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDSERIALLINEINTERNAL_H_
#define _ATPDHDSERIALLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannelInternal.h"
#include "AtPdhSerialLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of class PDH DS3/E3 Serial line */
typedef struct tAtPdhSerialLineMethods
    {
    /* Mode Setting */
    eAtRet (*ModeSet)(AtPdhSerialLine self, uint32 mode);
    uint32 (*ModeGet)(AtPdhSerialLine self);

    /* Associated channel */
    AtChannel (*ChannelGet)(AtPdhSerialLine self);
    }tAtPdhSerialLineMethods;

/* Serial line */
typedef struct tAtPdhSerialLine
    {
    tAtPdhChannel super;
    const tAtPdhSerialLineMethods *methods;

    /* Private data */
    uint32 mode;
    }tAtPdhSerialLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhSerialLine AtPdhSerialLineObjectInit(AtPdhSerialLine self, uint32 channelId, AtModulePdh module);

/* Associated channel */
AtChannel AtPdhSerialLineChannelGet(AtPdhSerialLine self);

eBool AtPdhSerialLineIsDe3Mode(AtPdhSerialLine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHDSERIALLINEINTERNAL_H_ */


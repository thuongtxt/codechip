/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : AtPdhMdlController.c
 *
 * Created Date: May 30, 2015
 *
 * Description : MDL Controller class
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtOsal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../common/AtChannelInternal.h"
#include "AtPdhMdlControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((AtPdhMdlController)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class*/
static char m_methodsInit = 0;
static tAtPdhMdlControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool MdlControllerIsValid(AtPdhMdlController self)
    {
    return (self == NULL) ? cAtFalse : cAtTrue;
    }

static eAtPdhMdlControllerType TypeGet(AtPdhMdlController self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtPdhMdlControllerTypeUnknown;
    }

static void Debug(AtPdhMdlController self)
    {
    AtUnused(self);
    }

static eAtRet Enable(AtPdhMdlController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);

    /* Let concrete class do */
    return cAtError;
    }

static eBool IsEnabled(AtPdhMdlController self)
    {
    AtUnused(self);

    /* Let concrete class do */
    return cAtFalse;
    }

static eAtRet Send(AtPdhMdlController self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool MessageReceived(AtPdhMdlController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 CountersGet(AtPdhMdlController self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    /* Let concrete class do */
    return 0;
    }

static uint32 CountersClearGet(AtPdhMdlController self, uint32 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    /* Let concrete class do */
    return 0;
    }

static eAtRet TxDataElementSet(AtPdhMdlController self,
                               eAtPdhMdlDataElement element,
                               const uint8 *data,
                               uint16 length)
    {
    AtUnused(self);
    AtUnused(element);
    AtUnused(data);
    AtUnused(length);

    /* Let concrete class do */
    return cAtError;
    }

static uint16 TxDataElementGet(AtPdhMdlController self,
                               eAtPdhMdlDataElement element,
                               uint8 *data,
                               uint16 bufferSize)
    {
    AtUnused(self);
    AtUnused(element);
    AtUnused(data);
    AtUnused(bufferSize);

    /* Let concrete class do */
    return cAtError;
    }

static uint16 RxDataElementGet(AtPdhMdlController self,
                               eAtPdhMdlDataElement element,
                               uint8 *data,
                               uint16 bufferSize)
    {
    AtUnused(self);
    AtUnused(element);
    AtUnused(data);
    AtUnused(bufferSize);

    /* Let concrete class do */
    return cAtError;
    }

static uint16 RxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    AtUnused(self);
    AtUnused(data);
    AtUnused(bufferSize);

    /* Let concrete class do */
    return cAtError;
    }

static eAtRet TxCRBitSet(AtPdhMdlController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 TxCRBitGet(AtPdhMdlController self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 RxCRBitGet(AtPdhMdlController self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtRet ExpectedCRBitSet(AtPdhMdlController self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 ExpectedCRBitGet(AtPdhMdlController self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint16 TxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    AtUnused(self);
    AtUnused(data);
    AtUnused(bufferSize);

    /* Let concrete class do */
    return cAtError;
    }

static uint32 DefectHistoryClear(AtPdhMdlController self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtPdhMdlController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtPdhMdlController self, uint32 defectMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(defectMask);
    AtUnused(enableMask);
    return cAtOk;
    }

static uint32 InterruptMaskGet(AtPdhMdlController self)
    {
    AtUnused(self);
    return 0;
    }

static void InterruptProcess(AtPdhMdlController self, uint8 slice, uint8 de3, AtHal hal)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(de3);
    AtUnused(hal);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPdhMdlController);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPdhMdlController object = (AtPdhMdlController)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeChannelIdString(de3);
    }

static void OverrideAtObject(AtPdhMdlController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPdhMdlController self)
    {
	OverrideAtObject(self);
    }

/* Initialize implementation */
static void MethodsInit(AtPdhMdlController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TypeGet);
        mMethodOverride(m_methods, Debug);
        mMethodOverride(m_methods, Enable);
        mMethodOverride(m_methods, IsEnabled);
        mMethodOverride(m_methods, TxDataElementSet);
        mMethodOverride(m_methods, TxDataElementGet);
        mMethodOverride(m_methods, RxDataElementGet);
        mMethodOverride(m_methods, RxMesssageGet);
        mMethodOverride(m_methods, TxCRBitSet);
        mMethodOverride(m_methods, TxCRBitGet);
        mMethodOverride(m_methods, ExpectedCRBitSet);
        mMethodOverride(m_methods, ExpectedCRBitGet);
        mMethodOverride(m_methods, RxCRBitGet);
        mMethodOverride(m_methods, TxMesssageGet);
        mMethodOverride(m_methods, Send);
        mMethodOverride(m_methods, MessageReceived);
        mMethodOverride(m_methods, CountersGet);
        mMethodOverride(m_methods, CountersClearGet);
        mMethodOverride(m_methods, DefectHistoryClear);
        mMethodOverride(m_methods, DefectHistoryGet);
        mMethodOverride(m_methods, InterruptMaskSet);
        mMethodOverride(m_methods, InterruptMaskGet);
        mMethodOverride(m_methods, InterruptProcess);
        }

    mMethodsGet(self) = &m_methods;
    }

AtPdhMdlController AtPdhMdlControllerObjectInit(AtPdhMdlController self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup methods */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    mThis(self)->de3 = de3;
    return self;
    }

uint32 AtPdhMdlControllerDefectHistoryClear(AtPdhMdlController self)
    {
    if (self)
        return mMethodsGet(self)->DefectHistoryClear(self);
    return 0;
    }

uint32 AtPdhMdlControllerDefectHistoryGet(AtPdhMdlController self)
    {
    if (self)
        return mMethodsGet(self)->DefectHistoryGet(self);
    return 0;
    }

eAtRet AtPdhMdlControllerInterruptMaskSet(AtPdhMdlController self, uint32 defectMask, uint32 enableMask)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaskSet(self, defectMask, enableMask);
    return cAtErrorNullPointer;
    }

uint32 AtPdhMdlControllerInterruptMaskGet(AtPdhMdlController self)
    {
    if (self)
        return mMethodsGet(self)->InterruptMaskGet(self);
    return 0;
    }

void AtPdhMdlControllerInterruptProcess(AtPdhMdlController self, uint8 slice, uint8 de3, AtHal hal)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self, slice, de3, hal);
    }

/**
 * @addtogroup AtPdhMdlController
 * @{
 */

/**
 * Get MDL type
 *
 * @param self This controller
 *
 * @return @ref eAtPdhMdlControllerType MDL type
 */
eAtPdhMdlControllerType AtPdhMdlControllerTypeGet(AtPdhMdlController self)
    {
    mAttributeGet(TypeGet, eAtPdhMdlControllerType, cAtPdhMdlControllerTypeUnknown);
    }

/**
 * Enable/disable controller
 *
 * @param self This controller
 * @param enable Enabling.
 *               - cAtTrue to enable.
 *               - cAtFalse to disable
 *
 * @return AT Return code
 */
eAtRet AtPdhMdlControllerEnable(AtPdhMdlController self, eBool enable)
    {
    mNumericalAttributeSet(Enable, enable);
    }

/**
 * To check if a controller is enabled.
 *
 * @param self This controller
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPdhMdlControllerIsEnabled(AtPdhMdlController self)
    {
    mAttributeGet(IsEnabled, eBool, cAtFalse);
    }

/**
 * To send a mdl message.
 *
 * @param self This controller
 *
 * @return AT Return code
 */
eAtRet AtPdhMdlControllerSend(AtPdhMdlController self)
    {
    if (self)
        return mMethodsGet(self)->Send(self);

    return cAtError;
    }

/**
 * To check if a controller is detected new message.
 *
 * @param self This controller
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPdhMdlControllerMessageReceived(AtPdhMdlController self)
    {
    mAttributeGet(MessageReceived, eBool, cAtFalse);
    }

/**
 * Configure TX data element.
 *
 * @param self This controller
 * @param element @ref eAtPdhMdlDataElement "Data element".
 * @param data Element data buffer
 * @param length Element data buffer length
 *
 * @return AT Return code
 */
eAtRet AtPdhMdlControllerTxDataElementSet(AtPdhMdlController self, eAtPdhMdlDataElement element, const uint8 *data, uint16 length)
    {
    if (MdlControllerIsValid(self))
        return mMethodsGet(self)->TxDataElementSet(self, element, data, length);

    return cAtError;
    }

/**
 * Get TX data element.
 *
 * @param self This controller
 * @param element @ref eAtPdhMdlDataElement "Data element".
 * @param [out] data Buffer to store element data
 * @param bufferSize Size of the input buffer to prevent buffer overflow
 *
 * @return Real length of data element.
 */
uint16 AtPdhMdlControllerTxDataElementGet(AtPdhMdlController self, eAtPdhMdlDataElement element, uint8 *data, uint16 bufferSize)
    {
    if (MdlControllerIsValid(self))
        return mMethodsGet(self)->TxDataElementGet(self, element, data, bufferSize);

    return 0;
    }

/**
 * Get RX data element.
 *
 * @param self This controller
 * @param element @ref eAtPdhMdlDataElement "Data element".
 * @param [out] data Buffer to store element data
 * @param bufferSize Size of the input buffer to prevent buffer overflow
 *
 * @return Real length of data element.
 */
uint16 AtPdhMdlControllerRxDataElementGet(AtPdhMdlController self, eAtPdhMdlDataElement element, uint8 *data, uint16 bufferSize)
    {
    if (MdlControllerIsValid(self))
        return mMethodsGet(self)->RxDataElementGet(self, element, data, bufferSize);

    return 0;
    }

/**
 * Get the raw RX message
 *
 * @param self This controller
 * @param [out] data Buffer to store message data
 * @param bufferSize Size of the input buffer to prevent buffer overflow
 *
 * @return Real length of message.
 */
uint16 AtPdhMdlControllerRxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    if (MdlControllerIsValid(self))
        return mMethodsGet(self)->RxMesssageGet(self, data, bufferSize);

    return 0;
    }

/**
 * Get the raw TX message
 *
 * @param self This controller
 * @param [out] data Buffer to store message data
 * @param bufferSize Size of the input buffer to prevent buffer overflow
 *
 * @return Real length of message.
 */
uint16 AtPdhMdlControllerTxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize)
    {
    if (MdlControllerIsValid(self))
        return mMethodsGet(self)->TxMesssageGet(self, data, bufferSize);

    return 0;
    }

/**
 * Set TX CR bit
 *
 * @param self This controller
 * @param value CR bit value
 *
 * @return AT Return code
 */
eAtRet AtPdhMdlControllerTxCRBitSet(AtPdhMdlController self, uint8 value)
    {
    mNumericalAttributeSet(TxCRBitSet, value);
    }

/**
 * Get TX CR bit
 *
 * @param self This channel
 *
 * @return TX CR bit
 */
uint8 AtPdhMdlControllerTxCRBitGet(AtPdhMdlController self)
    {
    mAttributeGet(TxCRBitGet, uint8, 0);
    }

/**
 * Set Expected CR bit
 *
 * @param self This controller
 * @param value CR bit value
 *
 * @return AT Return code
 */
eAtRet AtPdhMdlControllerExpectedCRBitSet(AtPdhMdlController self, uint8 value)
    {
    mNumericalAttributeSet(ExpectedCRBitSet, value);
    }

/**
 * Get Expected CR bit
 *
 * @param self This channel
 *
 * @return TX CR bit
 */
uint8 AtPdhMdlControllerExpectedCRBitGet(AtPdhMdlController self)
    {
    mAttributeGet(ExpectedCRBitGet, uint8, 0);
    }

/**
 * Get RX CR bit
 *
 * @param self This channel
 *
 * @return RX CR bit
 */
uint8 AtPdhMdlControllerRxCRBitGet(AtPdhMdlController self)
    {
    mAttributeGet(RxCRBitGet, uint8, 0);
    }

/**
 * Get DS3/E3 channel this control is working on.
 *
 * @param self This controller
 *
 * @return @ref AtPdhDe3 "DS3/E3".
 */
AtPdhDe3 AtPdhMdlControllerDe3Get(AtPdhMdlController self)
    {
    if (MdlControllerIsValid(self))
        return mThis(self)->de3;

    return 0;
    }

/**
 * To show debug information
 *
 * @param self This controller
 */
void AtPdhMdlControllerDebug(AtPdhMdlController self)
    {
    if (MdlControllerIsValid(self))
        mMethodsGet(self)->Debug(self);
    }

/**
 * Get counters
 *
 * @param self This channel
 * @param counterType @ref eAtPdhMdlControllerCountersType "Counter type"
 *
 * @return Counters
 */
uint32 AtPdhMdlControllerCountersGet(AtPdhMdlController self, uint32 counterType)
    {
    if (self)
        return mMethodsGet(self)->CountersGet(self, counterType);
    return cAtError;
    }

/**
 * Get counters
 *
 * @param self This channel
 * @param counterType @ref eAtPdhMdlControllerCountersType "Counter type"
 *
 * @return Counters
 */
uint32 AtPdhMdlControllerCountersClear(AtPdhMdlController self, uint32 counterType)
    {
    if (self)
        return mMethodsGet(self)->CountersClearGet(self, counterType);
    return cAtError;
    }
/**
 * @}
 */

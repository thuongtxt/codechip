/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe3Internal.h
 * 
 * Created Date: Feb 2, 2013
 *
 * Description : DS3/E3
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE3INTERNAL_H_
#define _ATPDHDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannelInternal.h"
#include "AtPdhDe3.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cE3DataRateInBytesPer125Us  (4296 / 8)
#define cDs3DataRateInBytesPer125Us (5592 / 8)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPdhDe3Methods
    {
    const char * (*VcIdString)(AtPdhDe3 self, AtSdhVc vc);

    /* DS3 FEAC */
    eAtRet (*TxFeacSet)(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 code);
    eAtRet (*TxFeacGet)(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *code);
    eAtRet (*RxFeacGet)(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *code);
    eAtRet (*TxFeacWithModeSet)(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 code, eAtPdhBomSentMode sentMode);
    eAtPdhBomSentMode (*TxFeacModeGet)(AtPdhDe3 self);

    /* E3 TTI */
    eAtRet (*TxTtiSet)(AtPdhDe3 self, const tAtPdhTti *tti);
    eAtRet (*TxTtiGet)(AtPdhDe3 self, tAtPdhTti *tti);
    eAtRet (*ExpectedTtiSet)(AtPdhDe3 self, const tAtPdhTti *tti);
    eAtRet (*ExpectedTtiGet)(AtPdhDe3 self, tAtPdhTti *tti);
    eAtRet (*RxTtiGet)(AtPdhDe3 self, tAtPdhTti *tti);

    /* E3 Timing Maker */
    eAtRet (*TmEnable)(AtPdhDe3 self, eBool enable);
    eBool (*TmIsEnabled)(AtPdhDe3 self);
    eAtRet (*TxTmCodeSet)(AtPdhDe3 self, uint8 tmCode);
    uint8  (*TxTmCodeGet)(AtPdhDe3 self);
    uint8  (*RxTmCodeGet)(AtPdhDe3 self);

    /* Payload type */
    eAtRet (*TxPldTypeSet)(AtPdhDe3 self, uint8 pldType);
    uint8  (*TxPldTypeGet)(AtPdhDe3 self);
    uint8  (*RxPldTypeGet)(AtPdhDe3 self);

    /* MDL controller */
    AtPdhMdlController (*PathMessageMdlController)(AtPdhDe3 self);
    AtPdhMdlController (*TestSignalMdlController)(AtPdhDe3 self);
    AtPdhMdlController (*IdleSignalMdlController)(AtPdhDe3 self);
    AtPdhMdlController (*RxCurrentMdlControllerGet)(AtPdhDe3 self);

    /* STD */
    eAtRet (*MdlStandardSet)(AtPdhDe3 self, eAtPdhMdlStandard standard);
    uint32 (*MdlStandardGet)(AtPdhDe3 self);

    /* DataLink-Options */
    eAtRet (*DataLinkOptionSet)(AtPdhDe3 self, eAtPdhDe3DataLinkOption option);
    eAtPdhDe3DataLinkOption (*DataLinkOptionGet)(AtPdhDe3 self);

    AtPdhDe1 (*De1Get)(AtPdhDe3 self, uint8 de2Id, uint8 de1Id);

    /* T3-CBIT UDL */
    eAtModulePdhRet (*TxUdlBitOrderSet)(AtPdhDe3 self, eAtBitOrder bitOrder);
    eAtBitOrder (*TxUdlBitOrderGet)(AtPdhDe3 self);
    eAtModulePdhRet (*RxUdlBitOrderSet)(AtPdhDe3 self, eAtBitOrder bitOrder);
    eAtBitOrder (*RxUdlBitOrderGet)(AtPdhDe3 self);

    /* T3-CBIT AIC */
    uint8 (*RxAicGet)(AtPdhDe3 self);
    eAtRet (*StuffModeSet)(AtPdhDe3 self, eAtPdhDe3StuffMode mode);
    eAtPdhDe3StuffMode (*StuffModeGet)(AtPdhDe3 self);
    }tAtPdhDe3Methods;

typedef struct tAtPdhDe3
    {
    tAtPdhChannel super;
    const tAtPdhDe3Methods *methods;

    /* Private data */
    void *cache;
    }tAtPdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 AtPdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);

eBool AtPdhDs3DataLinkIsApplicable(AtPdhDe3 self);
uint16 AtPdhDe3UnChannelizedFrameType(uint16 frameType);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHDE3INTERNAL_H_ */


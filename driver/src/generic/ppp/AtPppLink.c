/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtPppLink.c
 *
 * Created Date: Aug 7, 2012
 *
 * Description : PPP Link
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPppLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mPppLinkIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtPppLinkMethods m_methods;

/* Override */
static tAtChannelMethods m_AtChannelOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    AtHdlcChannel hdlc = AtHdlcLinkHdlcChannelGet((AtHdlcLink)self);
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet(hdlc);

    if (frameType == cAtHdlcFrmCiscoHdlc)
        return "chdlc";
    if (frameType == cAtHdlcFrmPpp)
        return "ppp";
    if (frameType == cAtHdlcFrmFr)
        return "frame-relay";

    return "unknown";
    }

static eAtModulePppRet PhaseSet(AtPppLink self, eAtPppLinkPhase phase)
    {
	AtUnused(phase);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtPppLinkPhase PhaseGet(AtPppLink self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtPppLinkPhaseDead;
    }

static AtPidTable PidTableGet(AtPppLink self)
    {
	AtUnused(self);
    return NULL;
    }

static eAtModulePppRet TxProtocolFieldCompress(AtPppLink self, eBool compress)
    {
	AtUnused(compress);
	AtUnused(self);
    return cAtError;
    }

static eBool TxProtocolFieldIsCompressed(AtPppLink self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    if ((pseudowire == NULL) || (AtPwTypeGet(pseudowire) == cAtPwTypePpp))
        return cAtOk;

    return cAtErrorInvlParm;
    }

static eBool ShouldDeletePwOnCleanup(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePppRet BcpLanFcsInsertionEnable(AtPppLink self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool BcpLanFcsInsertionIsEnabled(AtPppLink self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtPppLink self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, PhaseSet);
        mMethodOverride(m_methods, PhaseGet);
        mMethodOverride(m_methods, PidTableGet);
        mMethodOverride(m_methods, TxProtocolFieldCompress);
        mMethodOverride(m_methods, TxProtocolFieldIsCompressed);
        mMethodOverride(m_methods, BcpLanFcsInsertionIsEnabled);
        mMethodOverride(m_methods, BcpLanFcsInsertionEnable);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtPppLink self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, ShouldDeletePwOnCleanup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideAtChannel(self);
    }

AtPppLink AtPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtPppLink));
        
    /* Super constructor */
    if (AtHdlcLinkObjectInit((AtHdlcLink)self, hdlcChannel) == NULL)
        return NULL;
    
    /* Setup methods */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtPppLink
 * @{
 */

/**
 * Set ppp link state
 *
 * @param self PppLink
 * @param phase Link State
 *                - @ref eAtPppLinkPhase "Ppp Link State"
 * @return AT return code
 */
eAtModulePppRet AtPppLinkPhaseSet(AtPppLink self, eAtPppLinkPhase phase)
    {
    if (mPppLinkIsValid(self))
        return mMethodsGet(self)->PhaseSet(self, phase);

    return cAtError;
    }

/**
 * Get ppp link state
 *
 * @param self PppLink
 *
 * @return Ppp Link State
 */
eAtPppLinkPhase AtPppLinkPhaseGet(AtPppLink self)
    {
    if (mPppLinkIsValid(self))
        return mMethodsGet(self)->PhaseGet(self);

    return cAtPppLinkPhaseDead;
    }

/**
 * @}
 */

/*
 * Get Pid Table
 *
 * @param self PppLink
 *
 * @return Pid Table
 */
AtPidTable AtPppLinkPidTableGet(AtPppLink self)
    {
    if (mPppLinkIsValid(self))
        return mMethodsGet(self)->PidTableGet(self);

    return NULL;
    }

/**
 * @addtogroup AtPppLink
 * @{
 */

/**
 * Enable/disable Protocol fields compression at TX direction
 *
 * @param self This channel
 * @param compress cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePppRet AtPppLinkTxProtocolFieldCompress(AtPppLink self, eBool compress)
    {
    if (mPppLinkIsValid(self))
        return mMethodsGet(self)->TxProtocolFieldCompress(self, compress);

    return cAtError;
    }

/**
 * Check if Protocol fields compression at TX direction
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPppLinkTxProtocolFieldIsCompressed(AtPppLink self)
    {
    if (mPppLinkIsValid(self))
        return mMethodsGet(self)->TxProtocolFieldIsCompressed(self);

    return cAtFalse;
    }

/**
 * Enable/disable LAN FCS field insertion in BCP header at TX direction
 *
 * @param self This channel
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModulePppRet AtPppLinkBcpLanFcsInsertionEnable(AtPppLink self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->BcpLanFcsInsertionEnable(self, enable);
    return cAtErrorNullPointer;
    }

/**
 * Check if LAN FCS field is inserted in BCP header at TX direction
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtPppLinkBcpLanFcsInsertionIsEnabled(AtPppLink self)
    {
    if (self)
        return mMethodsGet(self)->BcpLanFcsInsertionIsEnabled(self);
    return cAtFalse;
    }

/**
 * @}
 */

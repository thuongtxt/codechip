/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtPidTable.c
 *
 * Created Date: Sep 20, 2013
 *
 * Description : Table to translate between PID values and Ethernet type
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPidTableInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mPidTableIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtPidTable)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPidTableMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(AtPidTable self)
    {
	AtUnused(self);
    return 0;
    }

static AtPidTableEntry EntryObjectCreate(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(entryIndex);
	AtUnused(self);
    return NULL;
    }

static eAtRet AllEntriesDelete(AtPidTable self)
    {
    AtObject entry;

    if (self->entries == NULL)
        return cAtOk;

    /* Remove all entries first */
    while ((entry = AtListObjectRemoveAtIndex(self->entries, 0)) != NULL)
        AtObjectDelete(entry);

    /* And the list that holds them */
    AtObjectDelete((AtObject)(self->entries));
    self->entries = NULL;

    return cAtOk;
    }

static eAtRet AllEntriesCreate(AtPidTable self)
    {
    uint32 entry_i;
    uint32 maxNumberOfEntry = mMethodsGet(self)->MaxNumEntries(self);

    /* Create list to cache entry */
    self->entries = AtListCreate(maxNumberOfEntry);
    if (self->entries == NULL)
        return cAtErrorRsrcNoAvail;

    /* Create each entry in table */
    for (entry_i = 0; entry_i < maxNumberOfEntry; entry_i++)
        AtListObjectAdd(self->entries, (AtObject)mMethodsGet(self)->EntryObjectCreate(self, entry_i));

    return cAtOk;
    }

static eBool NeedDefaultSettings(AtPidTable self)
    {
	AtUnused(self);
    /* Let's concrete class determine */
    return cAtFalse;
    }

static eAtRet EntryDefaultSet(AtPidTable self)
    {
	AtUnused(self);
    return cAtOk;
    }

static eAtRet Setup(AtPidTable self)
    {
    if (self->entries == NULL)
        return AllEntriesCreate(self);
    return cAtOk;
    }

static eAtRet Init(AtPidTable self)
    {
    eAtRet ret = mMethodsGet(self)->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Set default */
    if (mMethodsGet(self)->NeedDefaultSettings(self))
        return mMethodsGet(self)->EntryDefaultSet(self);

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AllEntriesDelete((AtPidTable)self);
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPidTable object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(module);
    mEncodeList(entries);
    }

static eBool EntryIsEditable(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(entryIndex);
	AtUnused(self);
    return cAtTrue;
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtPidTable table = mThis(self);

    AtSnprintf(description, sizeof(description), "%s.pid_table", AtObjectToString((AtObject)AtPidTableModuleGet(table)));
    return description;
    }

static void MethodsInit(AtPidTable self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, EntryObjectCreate);
        mMethodOverride(m_methods, MaxNumEntries);
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, EntryIsEditable);
        mMethodOverride(m_methods, Setup);
        mMethodOverride(m_methods, EntryDefaultSet);
        mMethodOverride(m_methods, NeedDefaultSettings);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtPidTable self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPidTable self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPidTable);
    }

AtPidTable AtPidTableObjectInit(AtPidTable self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->module = module;

    return self;
    }

/*
 * Get module that this table belongs to
 *
 * @param self This PID table
 *
 * @return Module of this table
 */
AtModule AtPidTableModuleGet(AtPidTable self)
    {
    if (!mPidTableIsValid(self))
        return NULL;

    return self->module;
    }

/*
 * Get maximum number of entries that this PID table can support
 *
 * @param self This PID table
 *
 * @return Maximum number of entries
 */
uint32 AtPidTableMaxNumEntries(AtPidTable self)
    {
    if (mPidTableIsValid(self))
        return mMethodsGet(self)->MaxNumEntries(self);

    return 0;
    }

/*
 * Get PID entry object by index
 *
 * @param self This PID table
 * @param entryIndex entry index
 *
 * @return A PID entry object on success or NULL on failure
 */
AtPidTableEntry AtPidTableEntryGet(AtPidTable self, uint32 entryIndex)
    {
    if (mPidTableIsValid(self))
        return (AtPidTableEntry)AtListObjectGet(self->entries, entryIndex);

    return NULL;
    }

/*
 * To check if entry is editable
 *
 * @param self This table
 * @param entryIndex Entry index
 *
 * @return cAtTrue if editable and cAtFalse if not editable
 */
eBool AtPidTableEntryIsEditable(AtPidTable self, uint32 entryIndex)
    {
    if (mPidTableIsValid(self))
        return mMethodsGet(self)->EntryIsEditable(self, entryIndex);
    return cAtFalse;
    }

/*
 * Initialize PID table
 *
 * @param self This PID table
 *
 * @return AT code
 */
eAtRet AtPidTableInit(AtPidTable self)
    {
    if (mPidTableIsValid(self))
        return mMethodsGet(self)->Init(self);

    return cAtError;
    }


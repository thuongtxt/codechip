/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtModulePpp.c
 *
 * Created Date: Aug 14, 2012
 *
 * Description : PPP module
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePppInternal.h"

#include "../../util/AtIteratorInternal.h"
#include "../eth/AtModuleEth.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mModulePppIsValid(self)  (self ? cAtTrue : cAtFalse)
#define mThis(self) ((AtModulePpp)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* To store implementation of this class (require) */
static tAtModulePppMethods m_methods;

/* To override super methods (require if need to override some Methods) */
static tAtObjectMethods m_AtObjectOverride;
static tAtModuleMethods m_AtModuleOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/* To prevent initializing implementation structures more than one times */
static char m_methodsInit = 0;

/* Bundles iterator */
static char m_bundleIteratorMethodsInit = 0;
static tAtArrayIteratorMethods m_bundleAtArrayIteratorOverride;
static tAtIteratorMethods m_bundleAtIteratorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool BundleAtIndexIsValid(AtArrayIterator self, uint32 bundleIndex)
    {
    AtMpBundle *bundles = self->array;
    return AtHdlcBundleIsValid((AtHdlcBundle)bundles[bundleIndex]);
    }

static AtObject BundleAtIndex(AtArrayIterator self, uint32 bundleIndex)
    {
    AtMpBundle *bundles = self->array;
    return (AtObject)bundles[bundleIndex];
    }

static void BundleIteratorOverrideAtArrayIterator(AtArrayIterator self)
    {
    if (!m_bundleIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_bundleAtArrayIteratorOverride, mMethodsGet(self), sizeof(m_bundleAtArrayIteratorOverride));
        m_bundleAtArrayIteratorOverride.DataAtIndexIsValid = BundleAtIndexIsValid;
        m_bundleAtArrayIteratorOverride.DataAtIndex = BundleAtIndex;
        }
    mMethodsSet(self, &m_bundleAtArrayIteratorOverride);
    }

static uint16 AtModulePppMpBundleCountGet(AtArrayIterator self)
    {
    uint16 count = 0;
    uint16 i;
    AtMpBundle *bundles = self->array;
    if (NULL == bundles)
        return 0;

    for (i = 0; i < self->capacity; i++)
        if (AtHdlcBundleIsValid((AtHdlcBundle)bundles[i]))
            count++;
    return count;
    }

static uint32 MpBundleCount(AtIterator self)
    {
    AtArrayIterator arrayIterator = (AtArrayIterator)self;
    if (arrayIterator->count >= 0)
        return (uint32)arrayIterator->count;

    arrayIterator->count = (int32)AtModulePppMpBundleCountGet(arrayIterator);
    return (uint32)arrayIterator->count;
    }

static void BundleIteratorOverrideAtIterator(AtArrayIterator self)
    {
    AtIterator iterator = (AtIterator)self;

    if (!m_bundleIteratorMethodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_bundleAtIteratorOverride, mMethodsGet(iterator), sizeof(m_bundleAtIteratorOverride));
        m_bundleAtIteratorOverride.Count = MpBundleCount;
        }
    mMethodsSet(iterator, &m_bundleAtIteratorOverride);
    }

static void BundleIteratorOverride(AtArrayIterator self)
    {
    BundleIteratorOverrideAtArrayIterator(self);
    BundleIteratorOverrideAtIterator(self);
    }

static eAtRet AllBundleCreate(AtModulePpp modulePpp)
    {
    uint32 maxNumOfBundles;
    uint32 bundleId;

    AtOsal osal = AtSharedDriverOsalGet();

    /* Get maximum number of supported bundles */
    maxNumOfBundles = mMethodsGet(modulePpp)->MaxNumOfMpBundleGet(modulePpp);
    if (maxNumOfBundles == 0)
        return cAtOk;

    /* Initialize all bundles managed by this module */
    modulePpp->bundles = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtMpBundle) * maxNumOfBundles);
    if (modulePpp->bundles == NULL)
        return cAtErrorRsrcNoAvail;

    /* Allocate each mp bundle */
    for (bundleId = 0; bundleId < maxNumOfBundles; bundleId++)
        modulePpp->bundles[bundleId] = mMethodsGet(modulePpp)->MpBundleObjectCreate(modulePpp, bundleId);

    return cAtOk;
    }

static eAtRet AllBundleDelete(AtModulePpp modulePpp)
    {
    uint32 maxNumOfBundles;
    uint32 bundleId;
    AtOsal osal = AtSharedDriverOsalGet();

    if (modulePpp->bundles == NULL)
        return cAtOk;

    /* Get maximum number of supported bundles */
    maxNumOfBundles = mMethodsGet(modulePpp)->MaxNumOfMpBundleGet(modulePpp);

    /* Delete each bundle */
    for (bundleId = 0; bundleId < maxNumOfBundles; bundleId++)
        AtObjectDelete((AtObject)(modulePpp->bundles[bundleId]));

    /* free  bundles managed by this module */
    mMethodsGet(osal)->MemFree(osal, modulePpp->bundles);
    modulePpp->bundles = NULL;

    return cAtOk;
    }

static void PidTableDelete(AtModulePpp self)
    {
    AtObjectDelete((AtObject)(self->pidTable));
    self->pidTable = NULL;
    }

static eAtRet PidTableCreate(AtModulePpp self)
    {
    AtPidTable newTable = mMethodsGet(self)->PidTableObjectCreate(self);

    /* Not all products supports global PID table */
    if (newTable)
        AtPidTableInit(newTable);

    self->pidTable = newTable;

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AllBundleDelete((AtModulePpp)self);
    PidTableDelete((AtModulePpp)self);

    /* Call super function to fully delete */
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModulePpp object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(bundles, AtModulePppMaxBundlesGet(object));
    mEncodeObject(pidTable);
    }

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "ppp";
    }

static const char *CapacityDescription(AtModule self)
    {
    AtModulePpp modulePpp = (AtModulePpp)self;
    static char string[128];

    AtSnprintf(string, 128, "max bundles: %u, max links per bundle: %u",
               AtModulePppMaxBundlesGet(modulePpp),
               AtModulePppMaxLinksPerBundleGet(modulePpp));

    return string;
    }

static void OverrideAtObject(AtModulePpp self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        /* Save reference to super methods */
        m_AtObjectMethods = mMethodsGet(object);

        /* Copy to reuse methods of super class. But override some Methods */
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    /* Change behavior of super class level 2 */
    mMethodsSet(object, &m_AtObjectOverride);
    }

static eAtRet Setup(AtModule self)
    {
    AtModulePpp modulePpp = (AtModulePpp)self;
    eAtRet ret;

    /* Super init */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Delete database first */
    if (modulePpp->bundles)
        AllBundleDelete(modulePpp);
    if (modulePpp->pidTable)
        PidTableDelete(modulePpp);

    /* Recreate database */
    AllBundleCreate(modulePpp);
    PidTableCreate(modulePpp);

    return cAtOk;
    }

static eAtRet DestroyAllResourcesOfBundle(AtMpBundle bundle)
    {
    AtIterator iterator;
    AtPppLink link;
    AtEthFlow flow;
    eAtRet ret = cAtOk;

    /* Delete all links */
    iterator = AtHdlcBundleLinkIteratorCreate((AtHdlcBundle)bundle);
    while ((link = (AtPppLink)AtIteratorNext(iterator)) != NULL)
        ret |= AtHdlcBundleLinkRemove((AtHdlcBundle)bundle, (AtHdlcLink)link);
    AtObjectDelete((AtObject)iterator);

    /* Delete all flows */
    iterator = AtMpBundleFlowIteratorCreate(bundle);
    while ((flow = (AtEthFlow)AtIteratorNext(iterator)) != NULL)
        ret |= AtMpBundleFlowRemove(bundle, flow);
    AtObjectDelete((AtObject)iterator);

    return ret;
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    AtModulePpp pppModule = (AtModulePpp)self;
    AtIterator bundleIterator;
    AtMpBundle bundle;
    eAtRet ret = cAtOk;

    bundleIterator = AtModulePppMpBundleIteratorCreate(pppModule);
    while ((bundle = (AtMpBundle)AtIteratorNext(bundleIterator)) != NULL)
        {
        ret |= DestroyAllResourcesOfBundle(bundle);
        ret |= AtModulePppMpBundleDelete(pppModule, AtChannelIdGet((AtChannel)bundle));
        }
    AtObjectDelete((AtObject)bundleIterator);

    return ret;
    }

static void StatusClear(AtModule self)
    {
    AtIterator bundleIterator = AtModulePppMpBundleIteratorCreate((AtModulePpp)self);
    AtChannel bundle;

    m_AtModuleMethods->StatusClear(self);

    while ((bundle = (AtChannel)AtIteratorNext(bundleIterator)) != NULL)
        AtChannelStatusClear(bundle);

    AtObjectDelete((AtObject)bundleIterator);
    }

static void OverrideAtModule(AtModulePpp self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        mMethodOverride(m_AtModuleOverride, AllServicesDestroy);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    }

static eBool BundleIdIsValid(AtModulePpp self, uint32 bundleId)
    {
    return (bundleId < AtModulePppMaxBundlesGet(self)) ? cAtTrue : cAtFalse;
    }

static AtMpBundle MpBundleCreate(AtModulePpp self, uint32 bundleId)
    {
    AtMpBundle bundle;

	/* Cannot create an invalid bundle */
    if (!BundleIdIsValid(self, bundleId))
        return NULL;

    /* Database has not been allocated */
    if (self->bundles == NULL)
        return NULL;

    /* Cannot create a bundle for more than one times */
    bundle = self->bundles[bundleId];
    if ((bundle == NULL) || AtHdlcBundleIsValid((AtHdlcBundle)bundle))
        return NULL;

    /* Validate it and initialize it */
    AtHdlcBundleValidate((AtHdlcBundle)bundle, cAtTrue);
    AtChannelInit((AtChannel)bundle);

	return bundle;
    }

static AtMpBundle MpBundleGet(AtModulePpp self, uint32 bundleId)
    {
    AtMpBundle bundle;

    if (!BundleIdIsValid(self, bundleId))
        return NULL;

    /* Database has not been allocated */
    if (self->bundles == NULL)
        return NULL;

    bundle = self->bundles[bundleId];
    if (!AtHdlcBundleIsValid((AtHdlcBundle)bundle))
        return NULL;

    return bundle;
    }

static uint32 FreeMpBundleGet(AtModulePpp self)
    {
    uint32 i, maxNumBundles;

    maxNumBundles = AtModulePppMaxBundlesGet(self);
    for (i = 0; i < maxNumBundles; i++)
        {
        if (!AtHdlcBundleIsValid((AtHdlcBundle)(self->bundles[i])))
            return i;
        }

    return maxNumBundles;
    }

static eAtRet MpBundleDelete(AtModulePpp self, uint32 bundleId)
    {
    AtMpBundle bundle;

    if (bundleId >= AtModulePppMaxBundlesGet(self))
        return cAtErrorOutOfRangParm;

    bundle = AtModulePppMpBundleGet(self, bundleId);
    if (bundle == NULL)
        return cAtOk;

    if (!AtHdlcBundleIsValid((AtHdlcBundle)bundle))
        return cAtErrorInvlParm;

    /* Only delete bundle when removed all its flows */
    if (AtListLengthGet(bundle->flows))
        return cAtErrorChannelBusy;

    AtHdlcBundleValidate((AtHdlcBundle)bundle, cAtFalse);

    return cAtOk;
    }

static AtMpBundle MpBundleObjectCreate(AtModulePpp self, uint32 bundleId)
    {
	AtUnused(bundleId);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint32 MaxLinksPerBundle(AtModulePpp self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static void MethodsInit(AtModulePpp self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MpBundleCreate);
        mMethodOverride(m_methods, MpBundleGet);
        mMethodOverride(m_methods, FreeMpBundleGet);
        mMethodOverride(m_methods, MpBundleDelete);
        mMethodOverride(m_methods, MpBundleObjectCreate);
        mMethodOverride(m_methods, MaxNumOfMpBundleGet);
        mMethodOverride(m_methods, MaxLinksPerBundle);
        }

    mMethodsSet(self, &m_methods);
    }

AtModulePpp AtModulePppObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModulePpp));
    
    /* Super constructor should be called first */
    if (AtModuleObjectInit((AtModule)self, cAtModulePpp, device) == NULL)
        return NULL;
    
    /* Override Delete method of AtObject class */
    Override(self);
    
    /* Initialize methods */
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;
    
    return self;
    }

/**
 * @addtogroup AtModulePpp
 * @{
 */

/**
 * Get maximum number of bundles that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of bundles that this module can support
 */
uint32 AtModulePppMaxBundlesGet(AtModulePpp self)
    {
    if (mModulePppIsValid(self))
        return mMethodsGet(self)->MaxNumOfMpBundleGet(self);

    return 0;
    }

/**
 * Get maximum number of links per bundle that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of links per bundle that this module can support
 */
uint32 AtModulePppMaxLinksPerBundleGet(AtModulePpp self)
    {
    if (mModulePppIsValid(self))
        return mMethodsGet(self)->MaxLinksPerBundle(self);

    return 0;
    }

/**
 * Create a MLPPP bundle from bundle ID
 *
 * @param self This module
 * @param bundleId Bundle ID
 *
 * @return Instance of MLPPP bundle
 * @note For products that have bundle ID space shared between MLPPP and MFR,
 * using this API is not recommended. Use bundle manager returned by
 * AtModulePppBundleManagerGet() to create bundle.
 */
AtMpBundle AtModulePppMpBundleCreate(AtModulePpp self, uint32 bundleId){
    if (mModulePppIsValid(self))
        return mMethodsGet(self)->MpBundleCreate(self, bundleId);
    
    return NULL;
}

/**
 * Get bundle object by ID
 *
 * @param self This module
 * @param bundleId Bundle ID
 *
 * @return A bundle object on success or NULL on failure
 */
AtMpBundle AtModulePppMpBundleGet(AtModulePpp self, uint32 bundleId){
    if (mModulePppIsValid(self))
        return mMethodsGet(self)->MpBundleGet(self, bundleId);
    
    return NULL;
}

/**
 * Get free MLPPP bundle
 *
 * @param self This module
 *
 * @return ID of free bundle. If there is no free bundle,
 * AtModulePppMaxBundlesGet() is returned
 */
uint32 AtModulePppFreeMpBundleGet(AtModulePpp self)
    {
    if (mModulePppIsValid(self))
        return mMethodsGet(self)->FreeMpBundleGet(self);

    return AtModulePppMaxBundlesGet(self);
    }

/**
 * Delete bundle
 *
 * @param self This module
 * @param bundleId Bundle ID
 *
 * @return AT return code
 */
eAtModulePppRet AtModulePppMpBundleDelete(AtModulePpp self, uint32 bundleId)
    {
    eAtRet ret;

    if (!mModulePppIsValid(self))
        return cAtError;
    
    AtModuleLock((AtModule)self);
    ret = mMethodsGet(self)->MpBundleDelete(self, bundleId);
    AtModuleUnLock((AtModule)self);

    return ret;
    }

/**
 * Create MP bundles iterator
 *
 * @param self This module
 *
 * @return AtIterator object
 */
AtIterator AtModulePppMpBundleIteratorCreate(AtModulePpp self)
    {
    uint32 maxNumBundles;
    AtArrayIterator iterator;

    if (!mModulePppIsValid(self))
        return NULL;

    /* New iterator */
    maxNumBundles = mMethodsGet(self)->MaxNumOfMpBundleGet(self);
    iterator = AtArrayIteratorNew(self->bundles, maxNumBundles);
    if (NULL == iterator)
        return NULL;

    /* Setup class */
    BundleIteratorOverride(iterator);
    m_bundleIteratorMethodsInit = 1;

    return (AtIterator)iterator;
    }

/**
 * Get Pid table from this module
 *
 * @param self This module
 *
 * @return AtPidTable object
 */
AtPidTable AtModulePppPidTableGet(AtModulePpp self)
    {
    return mModulePppIsValid(self) ? self->pidTable : NULL;
    }
/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtPidTableEntry.c
 *
 * Created Date: Sep 20, 2013
 *
 * Description : PID table entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPidTableEntryInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mTableEntryIsValid(self)  (self ? cAtTrue : cAtFalse)
#define mThis(self) ((AtPidTableEntry)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;
static tAtPidTableEntryMethods m_methods;

/* Override */
static tAtObjectMethods          m_AtObjectOverride;

/* Super implementation */
static const tAtObjectMethods   *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PsnToTdmPidSet(AtPidTableEntry self, uint32 pid)
    {
	AtUnused(pid);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet PsnToTdmEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
	AtUnused(ethType);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet PsnToTdmEnable(AtPidTableEntry self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 PsnToTdmPidGet(AtPidTableEntry self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 PsnToTdmEthTypeGet(AtPidTableEntry self)
    {
	AtUnused(self);
    return 0;
    }

static eBool PsnToTdmIsEnabled(AtPidTableEntry self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet TdmToPsnPidSet(AtPidTableEntry self, uint32 pid)
    {
	AtUnused(pid);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet TdmToPsnEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
	AtUnused(ethType);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet TdmToPsnEnable(AtPidTableEntry self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 TdmToPsnPidGet(AtPidTableEntry self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 TdmToPsnEthTypeGet(AtPidTableEntry self)
    {
	AtUnused(self);
    return 0;
    }

static eBool TdmToPsnIsEnabled(AtPidTableEntry self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet TdmToPsnPidMaskSet(AtPidTableEntry self, uint16 pidMask)
    {
	AtUnused(pidMask);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static uint32 TdmToPsnPidMaskGet(AtPidTableEntry self)
    {
	AtUnused(self);
    return 0;
    }

static void MethodsInit(AtPidTableEntry self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, PsnToTdmPidSet);
        mMethodOverride(m_methods, PsnToTdmEthTypeSet);
        mMethodOverride(m_methods, PsnToTdmEnable);
        mMethodOverride(m_methods, PsnToTdmPidGet);
        mMethodOverride(m_methods, PsnToTdmEthTypeGet);
        mMethodOverride(m_methods, PsnToTdmIsEnabled);
        mMethodOverride(m_methods, TdmToPsnPidSet);
        mMethodOverride(m_methods, TdmToPsnEthTypeSet);
        mMethodOverride(m_methods, TdmToPsnEnable);
        mMethodOverride(m_methods, TdmToPsnPidGet);
        mMethodOverride(m_methods, TdmToPsnEthTypeGet);
        mMethodOverride(m_methods, TdmToPsnIsEnabled);
        mMethodOverride(m_methods, TdmToPsnPidMaskSet);
        mMethodOverride(m_methods, TdmToPsnPidMaskGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtPidTableEntry object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(pidTable);
    mEncodeUInt(entryIndex);
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtPidTableEntry entry = (AtPidTableEntry)self;
    AtPidTable pidTable = AtPidTableEntryTableGet(entry);

    AtSnprintf(description, sizeof(description), "%s.%d",
               AtObjectToString((AtObject)pidTable),
               AtPidTableEntryIndexGet(entry) + 1);

    return description;
    }

static void OverrideAtObject(AtPidTableEntry self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPidTableEntry self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtPidTableEntry);
    }

AtPidTableEntry AtPidTableEntryObjectInit(AtPidTableEntry self, uint32 entryIndex, AtPidTable pidTable)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Catch value */
    self->pidTable   = pidTable;
    self->entryIndex = entryIndex;

    return self;
    }

uint32 AtPidTableEntryRead(AtPidTableEntry self, uint32 address, eAtModule moduleId)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->Read(self, address, moduleId);

    return 0;
    }

void AtPidTableEntryWrite(AtPidTableEntry self, uint32 address, uint32 value, eAtModule moduleId)
    {
    if (mTableEntryIsValid(self))
       mMethodsGet(self)->Write(self, address, value, moduleId);
    }

/*
 * Set PID for direction from PSN to TDM
 *
 * @param self This entry
 * @param pid PID
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryPsnToTdmPidSet(AtPidTableEntry self, uint32 pid)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->PsnToTdmPidSet(self, pid);

    return cAtError;
    }

/*
 * Set Ethernet type for direction from PSN to TDM
 *
 * @param self This entry
 * @param ethType Ethernet type value
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryPsnToTdmEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->PsnToTdmEthTypeSet(self, ethType);

    return cAtError;
    }

/*
 * Enable/Disable entry for direction from PSN to TDM
 *
 * @param self This entry
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryPsnToTdmEnable(AtPidTableEntry self, eBool enable)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->PsnToTdmEnable(self, enable);

    return cAtError;
    }

/*
 * Get PID of direction from PSN to TDM
 *
 * @param self This entry
 *
 * @return PID value
 */
uint32 AtPidTableEntryPsnToTdmPidGet(AtPidTableEntry self)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->PsnToTdmPidGet(self);
    return 0;
    }

/*
 * Get Ethernet type of direction from PSN to TDM
 *
 * @param self This entry
 *
 * @return Ethernet type value
 */
uint32 AtPidTableEntryPsnToTdmEthTypeGet(AtPidTableEntry self)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->PsnToTdmEthTypeGet(self);
    return 0;
    }

/*
 * Check if direction from PSN to TDM is enabled or not
 *
 * @param self This entry
 *
 * @return cAtTrue if enabled, cAtFalse if disabled
 */
eBool AtPidTableEntryPsnToTdmIsEnabled(AtPidTableEntry self)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->PsnToTdmIsEnabled(self);
    return cAtFalse;
    }

/*
 * Set PID for direction from TDM to PSN
 *
 * @param self This entry
 * @param pid PID valud
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryTdmToPsnPidSet(AtPidTableEntry self, uint32 pid)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnPidSet(self, pid);
    return cAtError;
    }

/*
 * Set Ethernet type for direction from TDM to PSN
 *
 * @param self This entry
 * @param ethType Ethernet type value
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryTdmToPsnEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnEthTypeSet(self, ethType);
    return cAtError;
    }

/*
 * Enable/Disable entry for direction from TDM to PSN
 *
 * @param self This entry
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryTdmToPsnEnable(AtPidTableEntry self, eBool enable)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnEnable(self, enable);
    return cAtError;
    }

/*
 * Get PID type of direction from TDM to PSN
 *
 * @param self This entry
 *
 * @return PID value
 */
uint32 AtPidTableEntryTdmToPsnPidGet(AtPidTableEntry self)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnPidGet(self);
    return 0;
    }

/*
 * Get Ethernet type of direction from TDM to PSN
 *
 * @param self This entry
 *
 * @return Ethernet type value
 */
uint32 AtPidTableEntryTdmToPsnEthTypeGet(AtPidTableEntry self)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnEthTypeGet(self);
    return 0;
    }

/*
 * Check if direction from TDM to PSN is enabled or not
 *
 * @param self This entry
 *
 * @return cAtTrue if enabled, cAtFalse if disabled
 */
eBool AtPidTableEntryTdmToPsnIsEnabled(AtPidTableEntry self)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnIsEnabled(self);
    return cAtFalse;
    }

/*
 * Set PID for 2 directions
 *
 * @param self This entry
 * @param pid PID value
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryPidSet(AtPidTableEntry self, uint32 pid)
    {
    eAtRet ret = cAtOk;

    ret |= AtPidTableEntryTdmToPsnPidSet(self, pid);
    ret |= AtPidTableEntryPsnToTdmPidSet(self, pid);

    return ret;
    }

/*
 * Set Ethernet type for 2 directions
 *
 * @param self This entry
 * @param ethType Ethernet type value
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryEthTypeSet(AtPidTableEntry self, uint32 ethType)
    {
    eAtRet ret = cAtOk;

    ret |= AtPidTableEntryPsnToTdmEthTypeSet(self, ethType);
    ret |= AtPidTableEntryTdmToPsnEthTypeSet(self, ethType);

    return ret;
    }

/*
 * Get the table that this entry belongs to
 *
 * @param self This entry
 *
 * @return PID table
 */
AtPidTable AtPidTableEntryTableGet(AtPidTableEntry self)
    {
    return mTableEntryIsValid(self) ? self->pidTable : NULL;
    }

/*
 * Get index of this entry in its table
 *
 * @param self This entry
 *
 * @return Index in its table
 */
uint32 AtPidTableEntryIndexGet(AtPidTableEntry self)
    {
    return mTableEntryIsValid(self) ? self->entryIndex : 0xFFFF;
    }

/*
 * Set PID Mask for direction from TDM to PSN
 *
 * @param self This entry
 * @param pidMask PID mask value
 *
 * @return AT return code
 */
eAtRet AtPidTableEntryTdmToPsnPidMaskSet(AtPidTableEntry self, uint16 pidMask)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnPidMaskSet(self, pidMask);
    return cAtError;
    }

/*
 * Get PID Mask for direction from TDM to PSN
 *
 * @param self This entry
 *
 * @return PID mask
 */
uint32 AtPidTableEntryTdmToPsnPidMaskGet(AtPidTableEntry self)
    {
    if (mTableEntryIsValid(self))
        return mMethodsGet(self)->TdmToPsnPidMaskGet(self);
    return 0;
    }

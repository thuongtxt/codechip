/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtPidTableEntryInternal.h
 * 
 * Created Date: Sep 20, 2013
 *
 * Description : PID table entry
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPIDTABLEENTRYINTERNAL_H_
#define _ATPIDTABLEENTRYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPidTableInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPidTableEntryMethods
    {
    /* PSN to TDM */
    eAtRet (*PsnToTdmPidSet)(AtPidTableEntry self, uint32 pid);
    eAtRet (*PsnToTdmEthTypeSet)(AtPidTableEntry self, uint32 ethType);
    eAtRet (*PsnToTdmEnable)(AtPidTableEntry self, eBool enable);
    uint32 (*PsnToTdmPidGet)(AtPidTableEntry self);
    uint32 (*PsnToTdmEthTypeGet)(AtPidTableEntry self);
    eBool  (*PsnToTdmIsEnabled)(AtPidTableEntry self);

    /* TDM to PSN */
    eAtRet (*TdmToPsnPidSet)(AtPidTableEntry self, uint32 pid);
    eAtRet (*TdmToPsnEthTypeSet)(AtPidTableEntry self, uint32 ethType);
    eAtRet (*TdmToPsnEnable)(AtPidTableEntry self, eBool enable);
    uint32 (*TdmToPsnPidGet)(AtPidTableEntry self);
    uint32 (*TdmToPsnEthTypeGet)(AtPidTableEntry self);
    eBool  (*TdmToPsnIsEnabled)(AtPidTableEntry self);
    eAtRet (*TdmToPsnPidMaskSet)(AtPidTableEntry self, uint16 pidMask);
    uint32 (*TdmToPsnPidMaskGet)(AtPidTableEntry self);

    uint32 (*Read)(AtPidTableEntry self, uint32 address, eAtModule moduleId);
    void   (*Write)(AtPidTableEntry self, uint32 address, uint32 value, eAtModule moduleId);
    }tAtPidTableEntryMethods;

typedef struct tAtPidTableEntry
    {
    tAtObject super;
    const tAtPidTableEntryMethods *methods;

    /* Private */
    AtPidTable pidTable;
    uint32 entryIndex;
    }tAtPidTableEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTableEntry AtPidTableEntryObjectInit(AtPidTableEntry self, uint32 entryIndex, AtPidTable pidTable);

uint32 AtPidTableEntryRead(AtPidTableEntry self, uint32 address, eAtModule moduleId);
void AtPidTableEntryWrite(AtPidTableEntry self, uint32 address, uint32 value, eAtModule moduleId);

#ifdef __cplusplus
}
#endif
#endif /* _ATPIDTABLEENTRYINTERNAL_H_ */


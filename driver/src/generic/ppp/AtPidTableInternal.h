/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtPidTableInternal.h
 * 
 * Created Date: Sep 20, 2013
 *
 * Description : Table to translate between PID values and Ethernet type
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPIDTABLEINTERNAL_H_
#define _ATPIDTABLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePpp.h"
#include "AtPidTable.h"
#include "AtList.h"

#include "../man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPidTableMethods
    {
    uint32 (*MaxNumEntries)(AtPidTable self);
    eAtRet (*Init)(AtPidTable self);

    /* Internal */
    eAtRet (*Setup)(AtPidTable self);
    eBool (*NeedDefaultSettings)(AtPidTable self);
    eAtRet (*EntryDefaultSet)(AtPidTable self);
    AtPidTableEntry (*EntryObjectCreate)(AtPidTable self, uint32 entryIndex);
    eBool (*EntryIsEditable)(AtPidTable self, uint32 entryIndex);
    }tAtPidTableMethods;

typedef struct tAtPidTable
    {
    tAtObject super;
    const tAtPidTableMethods *methods;

    /* Private */
    AtModule module;
    AtList entries;
    }tAtPidTable;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTable AtPidTableObjectInit(AtPidTable self, AtModule module);

eAtRet AtPidTableSetup(AtPidTable self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPIDTABLEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtMpBundle.c
 *
 * Created Date: Aug 3, 2012
 *
 * Description : PPP bundle
 *
 * Notes       : None
 *
 *----------------------------------------------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtMpBundleInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define mBundleIsValid(self)  (self ? cAtTrue : cAtFalse)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtMpBundle)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtMpBundleMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtChannelMethods     m_AtChannelOverride;
static tAtEncapBundleMethods m_AtEncapBundleOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "mpbundle";
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    if ((pseudowire == NULL) || (AtPwTypeGet(pseudowire) == cAtPwTypeMlppp))
        return cAtOk;

    return cAtErrorInvlParm;
    }

static eAtRet MpBunbleAllFlowsAllocate(AtMpBundle self)
    {
    uint32 maxNumOfFlowInBundle;

    /* Do nothing if resource is already allocated */
    if (self->flows)
        return cAtOk;

    /* Allocate */
    maxNumOfFlowInBundle = mMethodsGet(self)->MaxNumOfFlowsGet(self);
    self->flows = AtListCreate(maxNumOfFlowInBundle);
    if (self->flows == NULL)
        return cAtErrorRsrcNoAvail;

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    AtMpBundle bundle = (AtMpBundle)self;
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Allocate buffer to cache all of flows added to this bundle */
    ret = MpBunbleAllFlowsAllocate((AtMpBundle)self);
    if (ret != cAtOk)
        return ret;

    /* Set default configuration for this bundle. Not all of product support all
     * of these default settings, just ignore return code */
    AtMpBundleMrruSet(bundle, 1500);
    AtMpBundleWorkingModeSet(bundle, cAtMpWorkingModeLfi);
    AtMpBundleSequenceModeSet(bundle, mMethodsGet(bundle)->DefaultSequenceModeGet(bundle));
    AtHdlcBundleFragmentSizeSet((AtHdlcBundle)self, cAtNoFragment);
    AtHdlcBundleMaxDelaySet((AtHdlcBundle)self, AtHdlcBundleDefaultMaxLinkDelay((AtHdlcBundle)self));
    AtMpBundleSchedulingModeSet(bundle, cAtMpSchedulingModeRandom);
    
    return cAtOk;
    }

static void MpBunbleAllFlowsDelete(AtMpBundle self)
    {
    if (self->flows == NULL)
        return;

    AtObjectDelete((AtObject)(self->flows));
    self->flows = NULL;
    }

static void Delete(AtObject self)
    {
    /* Delete all flows */
    MpBunbleAllFlowsDelete((AtMpBundle)self);

    /* Fully delete this object */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtMpBundle object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescriptionList(flows);
    }

static eAtModulePppRet SequenceModeSet(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
    eAtRet ret = cAtOk;

    ret |= AtMpBundleTxSequenceModeSet(self, sequenceMode);
    ret |= AtMpBundleRxSequenceModeSet(self, sequenceMode);

    return ret;
    }

static eAtMpSequenceMode SequenceModeGet(AtMpBundle self)
    {
    eAtMpSequenceMode rxMode, txMode;

    rxMode = AtMpBundleRxSequenceModeGet(self);
    txMode = AtMpBundleTxSequenceModeGet(self);
    if (rxMode != txMode)
        return cAtMpSequenceModeUnknown;

    return rxMode;
    }

static eAtModulePppRet RxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
	AtUnused(sequenceMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePppRet TxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode sequenceMode)
    {
	AtUnused(sequenceMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtMpSequenceMode RxSequenceModeGet(AtMpBundle self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtMpSequenceModeShort;
    }

static eAtMpSequenceMode TxSequenceModeGet(AtMpBundle self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtMpSequenceModeShort;
    }

static eAtModulePppRet SequenceNumberSet(AtMpBundle self, uint8 classNumber, uint32 sequenceNumber)
    {
	AtUnused(sequenceNumber);
	AtUnused(classNumber);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModulePppRet WorkingModeSet(AtMpBundle self, eAtMpWorkingMode workingMode)
    {
	AtUnused(workingMode);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtMpWorkingMode WorkingModeGet(AtMpBundle self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtMpWorkingModeLfi;
    }

static eAtModulePppRet MrruSet(AtMpBundle self, uint32 mrru)
    {
	AtUnused(mrru);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint32 MrruGet(AtMpBundle self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePppRet FlowAdd(AtMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
	AtUnused(classNumber);
	AtUnused(flow);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 FlowClassNumberGet(AtMpBundle self, AtEthFlow flow)
    {
	AtUnused(flow);
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModulePppRet FlowRemove(AtMpBundle self, AtEthFlow flow)
    {
    AtListObjectRemove(self->flows, (AtObject)flow);

    return cAtOk;
    }

static uint8 NumberOfFlowsGet(AtMpBundle self)
    {
    return (uint8)AtListLengthGet(self->flows);
    }

static uint8 AllFlowsGet(AtMpBundle self, AtEthFlow *flows)
    {
    uint32 wIndex;
    uint32 numTrafficInBundle = AtListLengthGet(self->flows);

    if (flows == NULL)
        return (uint8)numTrafficInBundle;

    for (wIndex = 0; wIndex < numTrafficInBundle; wIndex++)
        flows[wIndex] = (AtEthFlow)AtListObjectGet(self->flows, wIndex);

    return (uint8)numTrafficInBundle;
    }

static uint32 MaxNumOfFlowsGet(AtMpBundle self)
    {
	AtUnused(self);
    return 16; /* For 16 classes */
    }

static uint32 TypeGet(AtEncapBundle self)
    {
    AtUnused(self);
    return cAtEncapBundleTypeMlppp;
    }

static eAtMpSequenceMode DefaultSequenceModeGet(AtMpBundle self)
    {
    AtUnused(self);
    return cAtMpSequenceModeShort;
    }

static eBool CanConfigureSequenceNumber(AtMpBundle self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtMpBundle self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, SequenceModeSet);
        mMethodOverride(m_methods, SequenceModeGet);
        mMethodOverride(m_methods, RxSequenceModeSet);
        mMethodOverride(m_methods, TxSequenceModeSet);
        mMethodOverride(m_methods, RxSequenceModeGet);
        mMethodOverride(m_methods, TxSequenceModeGet);
        mMethodOverride(m_methods, SequenceNumberSet);
        mMethodOverride(m_methods, WorkingModeSet);
        mMethodOverride(m_methods, WorkingModeGet);
        mMethodOverride(m_methods, MrruSet);
        mMethodOverride(m_methods, MrruGet);
        mMethodOverride(m_methods, FlowAdd);
        mMethodOverride(m_methods, FlowClassNumberGet);
        mMethodOverride(m_methods, FlowRemove);
        mMethodOverride(m_methods, NumberOfFlowsGet);
        mMethodOverride(m_methods, AllFlowsGet);
        mMethodOverride(m_methods, MaxNumOfFlowsGet);
        mMethodOverride(m_methods, DefaultSequenceModeGet);
        mMethodOverride(m_methods, CanConfigureSequenceNumber);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtEncapBundle(AtMpBundle self)
    {
    AtEncapBundle bundle = (AtEncapBundle) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapBundleOverride, mMethodsGet(bundle), sizeof(m_AtEncapBundleOverride));

        mMethodOverride(m_AtEncapBundleOverride, TypeGet);
        }

    mMethodsSet(bundle, &m_AtEncapBundleOverride);
    }

static void OverrideAtChannel(AtMpBundle self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtMpBundle self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtMpBundle self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEncapBundle(self);
    }

AtMpBundle AtMpBundleObjectInit(AtMpBundle self, uint32 channelId, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtMpBundle));

    /* Super constructor */
    if (AtHdlcBundleObjectInit((AtHdlcBundle)self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/**
 * @addtogroup AtMpBundle
 * @{
 */

/**
 * Set sequence number mode for MLPPP bundle
 *
 * @param self MLPPP bundle
 * @param seqMd Sequence number mode. Refer
 *                - @ref eAtMpSequenceMode "MLPPP bundle sequence mode"
 * @return AT return code
 */
eAtModulePppRet AtMpBundleSequenceModeSet(AtMpBundle self, eAtMpSequenceMode seqMd)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->SequenceModeSet(self, seqMd);

    return cAtError;
    }

/**
 * Get sequence number mode for MLPPP bundle
 *
 * @param self MLPPP bundle
 * @return Sequence number mode. Refer
 *                - @ref eAtMpSequenceMode "MLPPP bundle sequence mode"
 *
 * @note This API is only applicable when two directions have the same
 * sequence mode. If not, unknown mode can be returned
 */
eAtMpSequenceMode AtMpBundleSequenceModeGet(AtMpBundle self)
	{
    if (mBundleIsValid(self))
        return mMethodsGet(self)->SequenceModeGet(self);
    
    return cAtMpSequenceModeShort;
	}
	
/**
 * Set sequence number mode for direction from TDM to Ethernet
 *
 * @param self MLPPP bundle
 * @param seqMd Sequence number mode. Refer
 *                - @ref eAtMpSequenceMode "MLPPP bundle sequence mode"
 * @return AT return code
 */
eAtModulePppRet AtMpBundleRxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode seqMd)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->RxSequenceModeSet(self, seqMd);

    return cAtError;
    }

/**
 * Set sequence number mode for direction from Ethernet to TDM
 *
 * @param self MLPPP bundle
 * @param seqMd Sequence number mode. Refer
 *                - @ref eAtMpSequenceMode "MLPPP bundle sequence mode"
 * @return AT return code
 */
eAtModulePppRet AtMpBundleTxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode seqMd)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->TxSequenceModeSet(self, seqMd);

    return cAtError;
    }

/**
 * Get sequence number mode of direction from Ethernet to TDM
 *
 * @param self MLPPP bundle
 * @return Sequence number mode. Refer
 *                - @ref eAtMpSequenceMode "MLPPP bundle sequence mode"
 */
eAtMpSequenceMode AtMpBundleTxSequenceModeGet(AtMpBundle self)
	{
    if (mBundleIsValid(self))
        return mMethodsGet(self)->TxSequenceModeGet(self);
    
    return cAtMpSequenceModeShort;
	}

/**
 * Get sequence number mode for direction from TDM to Ethernet
 *
 * @param self MLPPP bundle
 * @return Sequence number mode. Refer
 *                - @ref eAtMpSequenceMode "MLPPP bundle sequence mode"
 */
eAtMpSequenceMode AtMpBundleRxSequenceModeGet(AtMpBundle self)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->RxSequenceModeGet(self);

    return cAtMpSequenceModeShort;
    }

/**
 * Set value for sequence field in MLPPP header. When this API is called, that
 * field is reset to input sequenceNumber and start increasing from this value.
 * This API can be called any time after bundle is created.
 *
 * @param self This bundle
 * @param classNumber Class number
 * @param sequenceNumber Start sequence number
 *
 * @return AT return code
 */
eAtModulePppRet AtMpBundleSequenceNumberSet(AtMpBundle self, uint8 classNumber, uint32 sequenceNumber)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->SequenceNumberSet(self, classNumber, sequenceNumber);

    return cAtErrorModeNotSupport;
    }

/**
 * Set working mode for MLPPP bundle
 *
 * @param self MLPPP bundle
 * @param workingMd Working mode. Refer
 *                  - @ref eAtMpWorkingMode "MLPPP bundle working mode"
 * @return AT return code
 */
eAtModulePppRet AtMpBundleWorkingModeSet(AtMpBundle self, eAtMpWorkingMode workingMd)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->WorkingModeSet(self, workingMd);
    
    return cAtError;
    }

/**
 * Get MLPPP bundle 's working mode
 *
 * @param self MLPPP bundle
 * @return Working mode. Refer
 *                  - @ref eAtMpWorkingMode "MLPPP bundle working mode"
 */
eAtMpWorkingMode AtMpBundleWorkingModeGet(AtMpBundle self)
	{
    if (mBundleIsValid(self))
        return mMethodsGet(self)->WorkingModeGet(self);
    
    return cAtMpWorkingModeLfi;
	}

/**
 * Set Maximum Receive Re-construct Unit for MLPPP bundle
 *
 * @param self MLPPP bundle
 * @param mrru Maximum Receive Re-construct Unit
 * @return AT return code
 */
eAtModulePppRet AtMpBundleMrruSet(AtMpBundle self, uint32 mrru)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->MrruSet(self, mrru);
    
    return cAtError;
    }

/**
 * Get Maximum Receive Re-construct Unit for MLPPP bundle
 *
 * @param self MLPPP bundle
 * @return Maximum Receive Re-construct Unit
 */
uint32 AtMpBundleMrruGet(AtMpBundle self)
	{
    if (mBundleIsValid(self))
        return mMethodsGet(self)->MrruGet(self);
    
    return 0;
	}

/**
 * Add a flow to bundle
 *
 * @param self This bundle
 * @param flow Flow to add
 * @param classNumber Class number assigned for this flow
 *
 * @return AT return code
 */
eAtModulePppRet AtMpBundleFlowAdd(AtMpBundle self, AtEthFlow flow, uint8 classNumber)
    {
    if ((!mBundleIsValid(self)) || (flow == NULL))
        return cAtErrorInvlParm;

    return mMethodsGet(self)->FlowAdd(self, flow, classNumber);
    }

/**
 * Get class number of flow that is added to bundle
 *
 * @param self This bundle
 * @param flow Flow to get class number
 *
 * @return Class number of the input flow.
 */
uint8 AtMpBundleFlowClassNumberGet(AtMpBundle self, AtEthFlow flow)
    {
    if ((!mBundleIsValid(self)) || (flow == NULL))
        return 0xFF;
    
    return mMethodsGet(self)->FlowClassNumberGet(self, flow);
    }

/**
 * Remove a flow that was added to bundle
 *
 * @param self This bundle
 * @param flow Flow
 *
 * @return AT return code
 */
eAtModulePppRet AtMpBundleFlowRemove(AtMpBundle self, AtEthFlow flow)
    {
    if (!mBundleIsValid(self))
        return cAtError;

    /* Do nothing for a NULL flow */
    if (flow == NULL)
        return cAtOk;

    return mMethodsGet(self)->FlowRemove(self, flow);
    }

/**
 * Get number of added flows
 *
 * @param self This bundle
 *
 * @return Number of added flows
 */
uint8 AtMpBundleNumberOfFlowsGet(AtMpBundle self)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->NumberOfFlowsGet(self);
    
    return 0;
    }

/**
 * Get all flows
 *
 * @param self
 * @param flows pointer to output flows array. Note, this is an array and it
 *        must be able to hold all of added flows. It is ignored if null
 *
 * @return Number of flows
 */
uint8 AtMpBundleAllFlowsGet(AtMpBundle self, AtEthFlow *flows)
    {
    if (mBundleIsValid(self))
        return mMethodsGet(self)->AllFlowsGet(self, flows);
    
    return 0;
    }

/**
 * Set scheduling mode
 *
 * @param self This bundle
 * @param schedulingMode Scheduling mode. Refer @ref eAtMpSchedulingMode "Scheduling modes"
 * @return AT return code
 */
eAtModulePppRet AtMpBundleSchedulingModeSet(AtMpBundle self, eAtMpSchedulingMode schedulingMode)
    {
    return AtHdlcBundleSchedulingModeSet((AtHdlcBundle)self, schedulingMode);
    }

/**
 * Get scheduling mode
 *
 * @param self This bundle
 * @return @ref eAtMpSchedulingMode "Scheduling mode"
 */
eAtMpSchedulingMode AtMpBundleSchedulingModeGet(AtMpBundle self)
    {
    return AtHdlcBundleSchedulingModeGet((AtHdlcBundle)self);
    }

/**
 * Create flows iterator
 *
 * @param self This bundle
 *
 * @return AtIterator object
 */
AtIterator AtMpBundleFlowIteratorCreate(AtMpBundle self)
    {
    if (mBundleIsValid(self))
        return AtListIteratorCreate(self->flows);

    return NULL;
    }
/**
 * @}
 */

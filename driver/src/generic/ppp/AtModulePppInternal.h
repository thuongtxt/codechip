/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : AtModulePppInternal.h
 *
 * Created Date: Aug 31, 2012
 *
 * Description : PPP module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPPPINTERNAL_H_
#define _ATMODULEPPPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#include "AtModulePpp.h"
#include "AtMpBundleInternal.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods of module PPP */
typedef struct tAtModulePppMethods
    {
    /* MLPPP bundle manage */
    AtMpBundle (*MpBundleCreate)(AtModulePpp self, uint32 bundleId);
    AtMpBundle (*MpBundleGet)(AtModulePpp self, uint32 bundleId);
    uint32 (*FreeMpBundleGet)(AtModulePpp self);
    eAtRet (*MpBundleDelete)(AtModulePpp self, uint32 bundleId);
    uint32 (*MaxNumOfMpBundleGet)(AtModulePpp self);
    AtMpBundle (*MpBundleObjectCreate)(AtModulePpp self, uint32 bundleId);
    uint32 (*MaxLinksPerBundle)(AtModulePpp self);
    
    /* PID table */
    AtPidTable (*PidTableObjectCreate)(AtModulePpp self);
    }tAtModulePppMethods;

/* Structure of class AtModulePpp */
typedef struct tAtModulePpp
    {
    tAtModule super;
    const tAtModulePppMethods *methods;

    /* Private attribute */
    AtMpBundle *bundles; /* Cache all bundles supported by this module */
    AtPidTable pidTable;
    }tAtModulePpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ---------	-------------------------------*/
AtModulePpp AtModulePppObjectInit(AtModulePpp self, AtDevice device);

/* Access private data */
AtMpBundle ModulePppBundleGet(AtModulePpp self, uint16 bundleId);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPPPINTERNAL_H_ */

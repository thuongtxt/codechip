/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtPppLinkInternal.h
 * 
 * Created Date: Sep 2, 2012
 *
 * Description : PPP Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPPPLINKINTERNAL_H_
#define _ATPPPLINKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPppLink.h"
#include "../encap/AtHdlcLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Link methods */
typedef struct tAtPppLinkMethods
    {
    eAtModulePppRet (*PhaseSet)(AtPppLink self, eAtPppLinkPhase phase);
    eAtPppLinkPhase (*PhaseGet)(AtPppLink self);
    AtPidTable (*PidTableGet)(AtPppLink self);
    eAtModulePppRet (*TxProtocolFieldCompress)(AtPppLink self, eBool compress);
    eBool (*TxProtocolFieldIsCompressed)(AtPppLink self);

    eAtModulePppRet (*BcpLanFcsInsertionEnable)(AtPppLink self, eBool enable);
    eBool (*BcpLanFcsInsertionIsEnabled)(AtPppLink self);
    }tAtPppLinkMethods;

/* PPP link */
typedef struct tAtPppLink
    {
    tAtHdlcLink super;
    const tAtPppLinkMethods *methods;
    }tAtPppLink;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPppLink AtPppLinkObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel);

#ifdef __cplusplus
}
#endif
#endif /* _ATPPPLINKINTERNAL_H_ */


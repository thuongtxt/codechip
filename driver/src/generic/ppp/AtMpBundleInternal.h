/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtMpBundleInternal.h
 * 
 * Created Date: Sep 2, 2012
 *
 * Description : PPP bundle Internal Interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMPBUNDLEINTERNAL_H_
#define _ATMPBUNDLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtMpBundle.h"
#include "AtModulePpp.h"
#include "../encap/AtHdlcBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtMpBundleMethods
    {
    /* Accessors */
    eAtModulePppRet (*SequenceModeSet)(AtMpBundle self, eAtMpSequenceMode sequenceMode);
    eAtMpSequenceMode (*SequenceModeGet)(AtMpBundle self);
    eAtModulePppRet (*TxSequenceModeSet)(AtMpBundle self, eAtMpSequenceMode sequenceMode);
    eAtModulePppRet (*RxSequenceModeSet)(AtMpBundle self, eAtMpSequenceMode sequenceMode);
    eAtMpSequenceMode (*TxSequenceModeGet)(AtMpBundle self);
    eAtMpSequenceMode (*RxSequenceModeGet)(AtMpBundle self);

    eAtModulePppRet (*SequenceNumberSet)(AtMpBundle self, uint8 classNumber, uint32 sequenceNumber);
    eAtModulePppRet (*WorkingModeSet)(AtMpBundle self, eAtMpWorkingMode workingMode);
    eAtMpWorkingMode (*WorkingModeGet)(AtMpBundle self);
    eAtModulePppRet (*MrruSet)(AtMpBundle self, uint32 mrru);
    uint32 (*MrruGet)(AtMpBundle self);
    
    /* Methods */
    eAtModulePppRet (*FlowAdd)(AtMpBundle self, AtEthFlow flow, uint8 classNumber);
    uint8 (*FlowClassNumberGet)(AtMpBundle self, AtEthFlow flow);
    eAtModulePppRet (*FlowRemove)(AtMpBundle self, AtEthFlow flow);
    uint8 (*NumberOfFlowsGet)(AtMpBundle self);
    uint8 (*AllFlowsGet)(AtMpBundle self, AtEthFlow *flows);

    /* Private methods */
    uint32 (*MaxNumOfFlowsGet)(AtMpBundle self);
    eAtMpSequenceMode (*DefaultSequenceModeGet)(AtMpBundle self);
    eBool (*CanConfigureSequenceNumber)(AtMpBundle self);
    }tAtMpBundleMethods;

/* Representation */
typedef struct tAtMpBundle
    {
    tAtHdlcBundle super;
    const tAtMpBundleMethods *methods;

    /* private data */
    AtList flows; /* Cache all flows in this bundle */
    } tAtMpBundle;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtMpBundle AtMpBundleObjectInit(AtMpBundle self, uint32 channelId, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _ATMPBUNDLEINTERNAL_H_ */


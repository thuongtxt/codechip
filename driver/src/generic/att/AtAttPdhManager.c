/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT PDH Manager
 *
 * File        : AtAttPdhManager.c
 *
 * Created Date: Jul 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of ATT PDH Manager of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAttPdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtAttPdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtAttPdhManagerMethods m_methods;

/* Override */

/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet De3AttInit(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet De1AttInit(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool HasDe3Tie(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }
static eBool HasDe1Tie(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet InitErrorGap(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }
static eAtRet De1TieInit(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet De1TieBufRead(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static void De3TieProcess(AtAttPdhManager self)
    {
    AtUnused(self);
    }

static void De1TieProcess(AtAttPdhManager self)
    {
    AtUnused(self);
    }
static void SaveDe1TieProcess(AtAttPdhManager self)
    {
    AtUnused(self);
    }

static void SaveDe3TieProcess(AtAttPdhManager self)
    {
    AtUnused(self);
    }

static eAtRet De3TieBufRead(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet De3TieInit(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static uint32 TiePeriodicProcess(AtAttPdhManager self, uint32 periodInMs)
    {
    AtUnused(periodInMs);
    if (mMethodsGet(mThis(self))->HasDe3Tie(mThis(self)))
       {
       mMethodsGet(mThis(self))->De3TieProcess(mThis(self));
       }
    if (mMethodsGet(mThis(self))->HasDe1Tie(mThis(self)))
       {
       /*mMethodsGet(mThis(self))->De1TieProcess(mThis(self));*/
       mMethodsGet(mThis(self))->De1TieBufRead(mThis(self));
       }
    return 0;
    }

static uint32 SaveTiePeriodicProcess(AtAttPdhManager self, uint32 periodInMs)
    {
    AtUnused(self);
    AtUnused(periodInMs);
    if (mMethodsGet(mThis(self))->HasDe3Tie(mThis(self)))
        {
        mMethodsGet(mThis(self))->SaveDe3TieProcess(mThis(self));
        }
    if (mMethodsGet(mThis(self))->HasDe1Tie(mThis(self)))
        {
        mMethodsGet(mThis(self))->SaveDe1TieProcess(mThis(self));
        }
    return 0;
    }


static eAtRet Init(AtAttPdhManager self)
    {
    eAtRet ret = cAtOk;

    mMethodsGet(mThis(self))->De3AttInit(mThis(self));
    mMethodsGet(mThis(self))->De1AttInit(mThis(self));

    /* TIE */
    if (mMethodsGet(mThis(self))->HasDe1Tie(mThis(self)))
        ret |= mMethodsGet(mThis(self))->De1TieInit(mThis(self));
    if (mMethodsGet(mThis(self))->HasDe3Tie(mThis(self)))
        ret |= mMethodsGet(mThis(self))->De3TieInit(mThis(self));

    /*FORCE*/
    mMethodsGet(mThis(self))->De3ForcePeriodSet(mThis(self), 1000);
    mMethodsGet(mThis(self))->De1ForcePeriodSet(mThis(self), 0x12EBB);
    mMethodsGet(mThis(self))->InitErrorGap(mThis(self));
    mMethodsGet(mThis(self))->ForcePerframeSet(mThis(self),cAtTrue);
    return ret;
    }


static eAtRet De3ForcePeriodSet(AtAttPdhManager self, uint32 numSeconds)
    {
    AtUnused(self);
    AtUnused(numSeconds);
    return cAtErrorNotImplemented;
    }
static uint32 De3ForcePeriodGet(AtAttPdhManager self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet De3ForcePerframeEnable(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet De3ForcePerframeDisable(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eBool De3ForcePerframeIsEnabled(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet De1ForcePeriodSet(AtAttPdhManager self, uint32 numSeconds)
    {
    AtUnused(self);
    AtUnused(numSeconds);
    return cAtErrorNotImplemented;
    }
static uint32 De1ForcePeriodGet(AtAttPdhManager self)
    {
    AtUnused(self);
    return 0;
    }
static eAtRet ForcePerframeSet(AtAttPdhManager self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool HasRegister(AtAttPdhManager self, uint32 address)
	{
	AtUnused(self);
	AtUnused(address);
	return cAtFalse;
	}

static uint16 LongRead(AtAttPdhManager self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	AtUnused(self);
	AtUnused(localAddress);
	AtUnused(dataBuffer);
	AtUnused(bufferLen);
	AtUnused(core);
	return 0;
	}

static uint16 LongWrite(AtAttPdhManager self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	AtUnused(self);
	AtUnused(localAddress);
	AtUnused(dataBuffer);
	AtUnused(bufferLen);
	AtUnused(core);
	return 0;
	}


static void Override(AtAttPdhManager self)
    {
    AtUnused(self);
    }

static void MethodsInit(AtAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, De3AttInit);
        mMethodOverride(m_methods, De1AttInit);

        /* TIE */
        mMethodOverride(m_methods, De1TieInit);
        mMethodOverride(m_methods, De3TieInit);

        mMethodOverride(m_methods, HasDe3Tie);
        mMethodOverride(m_methods, HasDe1Tie);
        mMethodOverride(m_methods, De1TieBufRead);
        mMethodOverride(m_methods, De3TieBufRead);
        mMethodOverride(m_methods, De1TieProcess);
        mMethodOverride(m_methods, De3TieProcess);
        mMethodOverride(m_methods, SaveDe1TieProcess);
        mMethodOverride(m_methods, SaveDe3TieProcess);
        mMethodOverride(m_methods, TiePeriodicProcess);
        mMethodOverride(m_methods, SaveTiePeriodicProcess);

        /* Others */
        mMethodOverride(m_methods, InitErrorGap);
        mMethodOverride(m_methods, De1ForcePeriodSet);
        mMethodOverride(m_methods, De1ForcePeriodGet);
        mMethodOverride(m_methods, De3ForcePeriodSet);
        mMethodOverride(m_methods, De3ForcePeriodGet);
        mMethodOverride(m_methods, De3ForcePerframeEnable);
        mMethodOverride(m_methods, De3ForcePerframeDisable);
        mMethodOverride(m_methods, De3ForcePerframeIsEnabled);
        mMethodOverride(m_methods, ForcePerframeSet);
        mMethodOverride(m_methods, HasRegister);
        mMethodOverride(m_methods, LongRead);
        mMethodOverride(m_methods, LongWrite);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtAttPdhManager);
    }

AtAttPdhManager AtAttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    self->pdh = pdh;
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }
AtModulePdh AtAttPdhManagerModulePdh(AtAttPdhManager self)
    {
    return self->pdh;
    }
eAtRet  AtAttPdhManagerInit(AtAttPdhManager self)
    {
    if (self)
        return  mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }
eAtRet AtAttPdhManagerDe1TieInit(AtAttPdhManager self)
    {
    if (self)
        return  mMethodsGet(self)->De1TieInit(self);
    return cAtErrorNullPointer;
    }

eAtRet AtAttPdhManagerDe1TieBufRead(AtAttPdhManager self)
    {
    if (self)
        return  mMethodsGet(self)->De1TieBufRead(self);
    return cAtErrorNullPointer;
    }

eAtRet AtAttPdhManagerDe1ForcePeriodSet(AtAttPdhManager self, uint32 numSeconds)
    {
    if (self)
        return mMethodsGet(self)->De1ForcePeriodSet(self, numSeconds);
    return cAtErrorNullPointer;
    }

uint32 AtAttPdhManagerDe1ForcePeriodGet(AtAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->De1ForcePeriodGet(self);
    return cAtErrorNullPointer;
    }

eAtRet AtAttPdhManagerDe3ForcePeriodSet(AtAttPdhManager self, uint32 numSeconds)
    {
    if (self)
        return mMethodsGet(self)->De3ForcePeriodSet(self, numSeconds);
    return cAtErrorNullPointer;
    }

uint32 AtAttPdhManagerDe3ForcePeriodGet(AtAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->De3ForcePeriodGet(self);
    return 0;
    }

eAtRet AtAttPdhManagerDe3ForcePerframeEnable(AtAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->De3ForcePerframeEnable(self);
    return 0;
    }

eAtRet AtAttPdhManagerDe3ForcePerframeDisable(AtAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->De3ForcePerframeDisable(self);
    return cAtErrorNullPointer;
    }

eBool AtAttPdhManagerDe3ForcePerframeIsEnabled(AtAttPdhManager self)
    {
    if (self)
        return mMethodsGet(self)->De3ForcePerframeIsEnabled(self);
    return cAtFalse;
    }

uint32 AtAttPdhManagerTiePeriodicProcess(AtAttPdhManager self, uint32 periodInMs)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->TiePeriodicProcess(self, periodInMs);
    }

uint32 AtAttPdhManagerSaveTiePeriodicProcess(AtAttPdhManager self, uint32 periodInMs)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->SaveTiePeriodicProcess(self, periodInMs);
    }

eAtRet AtAttPdhManagerForcePerframeSet(AtAttPdhManager self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;
    return mMethodsGet(self)->ForcePerframeSet(self, enable);
    }

eBool AtModulePdhAttManagerHasRegister(AtAttPdhManager self, uint32 localAddress)
	{
	if (self == NULL)
		return cAtFalse;
	return mMethodsGet(self)->HasRegister(self, localAddress);
	}

uint16 AtModulePdhAttManagerLongRead(AtAttPdhManager self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	if (self == NULL)
		return cAtFalse;
	return mMethodsGet(self)->LongRead(self, localAddress, dataBuffer, bufferLen, core);
	}

uint16 AtModulePdhAttManagerLongWrite(AtAttPdhManager self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core)
	{
	if (self == NULL)
		return cAtFalse;
	return mMethodsGet(self)->LongWrite(self, localAddress, dataBuffer, bufferLen, core);
	}

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaAttPdhManagerInternal.h
 * 
 * Author      : chaudpt
 *
 * Created Date: July 26, 2017
 *
 * Description : PDH module class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTPDHMANAGERINTERNAL_H_
#define _ATATTPDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../generic/common/AtObjectInternal.h"
#include "../../../include/att/AtAttPdhManager.h"
#include "../../generic/pdh/AtModulePdhInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAttPdhManagerMethods
    {
    eAtRet   (*Init)(AtAttPdhManager self);
    eAtRet   (*De1TieInit)(AtAttPdhManager self);
    eAtRet   (*De3TieInit)(AtAttPdhManager self);
    eAtRet   (*De3AttInit)(AtAttPdhManager self);
    eAtRet   (*De1AttInit)(AtAttPdhManager self);

    eBool    (*HasDe3Tie)(AtAttPdhManager self);
    eBool    (*HasDe1Tie)(AtAttPdhManager self);

    eAtRet   (*De1TieBufRead)(AtAttPdhManager self);
    eAtRet   (*De3TieBufRead)(AtAttPdhManager self);

    uint32   (*TiePeriodicProcess)(AtAttPdhManager self, uint32 periodInMs);
    void     (*De3TieProcess)(AtAttPdhManager self);
    void     (*De1TieProcess)(AtAttPdhManager self);

    uint32   (*SaveTiePeriodicProcess)(AtAttPdhManager self, uint32 periodInMs);
    void     (*SaveDe3TieProcess)(AtAttPdhManager self);
    void     (*SaveDe1TieProcess)(AtAttPdhManager self);

    eAtRet (*De3ForcePeriodSet)(AtAttPdhManager self, uint32 numMiliSeconds);
    uint32 (*De3ForcePeriodGet)(AtAttPdhManager self);
    eAtRet (*De3ForcePerframeEnable)(AtAttPdhManager self);
    eAtRet (*De3ForcePerframeDisable)(AtAttPdhManager self);
    eBool  (*De3ForcePerframeIsEnabled)(AtAttPdhManager self);
    eAtRet (*De1ForcePeriodSet)(AtAttPdhManager self, uint32 numMiliSeconds);
    uint32 (*De1ForcePeriodGet)(AtAttPdhManager self);
    eAtRet (*InitErrorGap)(AtAttPdhManager self);
    eAtRet (*ForcePerframeSet)(AtAttPdhManager self, eBool enable);
    eBool (*HasRegister)(AtAttPdhManager self, uint32 address);
    uint16 (*LongRead)(AtAttPdhManager self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
    uint16 (*LongWrite)(AtAttPdhManager self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
    }tAtAttPdhManagerMethods;

typedef struct tAtAttPdhManager
    {
    tAtObject super;
    const tAtAttPdhManagerMethods *methods;

    /* Private data */
    AtModulePdh pdh;
    }tAtAttPdhManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPdhManager AtAttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh);
AtModulePdh AtAttPdhManagerModulePdh(AtAttPdhManager self);

#ifdef __cplusplus
}
#endif
#endif /* _ATATTPDHMANAGERINTERNAL_H_ */


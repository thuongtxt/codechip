/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaAttPdhManagerInternal.h
 * 
 * Author      : chaudpt
 *
 * Created Date: July 26, 2017
 *
 * Description : PDH module class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTSDHMANAGERINTERNAL_H_
#define _ATATTSDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../generic/common/AtObjectInternal.h"
#include "../../../include/att/AtAttPdhManager.h"
#include "../../../include/att/AtAttSdhManager.h"
#include "../../generic/sdh/AtModuleSdhInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAttSdhManagerMethods
    {
    eAtRet   (*Init)(AtAttSdhManager self);
    eAtRet   (*LineAttInit)(AtAttSdhManager self);
    eAtRet   (*LoPathAttInit)(AtAttSdhManager self);
    eAtRet   (*HoPathAttInit)(AtAttSdhManager self);

    eAtRet   (*HoTieInit)(AtAttSdhManager self);
    eAtRet   (*LoTieInit)(AtAttSdhManager self);

    eBool    (*HasHoTie)(AtAttSdhManager self);
    eBool    (*HasLoTie)(AtAttSdhManager self);

    eAtRet (*LoPathForcePointerAdjEnable)(AtAttSdhManager self, eBool en);
    eAtRet (*LoForcePeriodSet)(AtAttSdhManager self, uint32 numSeconds);
    uint32 (*LoForcePeriodGet)(AtAttSdhManager self);

    eAtRet (*HoPathForcePointerAdjEnable)(AtAttSdhManager self, eBool en);
    eAtRet (*HoForcePeriodSet)(AtAttSdhManager self, uint32 numSeconds);
    uint32 (*HoForcePeriodGet)(AtAttSdhManager self);

    uint32   (*TiePeriodicProcess)(AtAttSdhManager self, uint32 periodInMs);
    void     (*HoTieProcess)(AtAttSdhManager self);
    void     (*LoTieProcess)(AtAttSdhManager self);

    uint32   (*SaveTiePeriodicProcess)(AtAttSdhManager self, uint32 periodInMs);
    void     (*SaveHoTieProcess)(AtAttSdhManager self);
    void     (*SaveLoTieProcess)(AtAttSdhManager self);

    eBool    (*HasForcePointer)(AtAttSdhManager self);
    uint32   (*ForcePointerPeriodicProcess)(AtAttSdhManager self, uint32 periodInMs);
    void     (*ForcePointerProcess)(AtAttSdhManager self);

    }tAtAttSdhManagerMethods;

typedef struct tAtAttSdhManager
    {
    tAtObject super;
    const tAtAttSdhManagerMethods *methods;

    /* Private data */
    AtModuleSdh sdh;
    uint32 process_index;
    }tAtAttSdhManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttSdhManager AtAttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh);
AtModuleSdh AtAttSdhManagerModuleSdh(AtAttSdhManager self);
#ifdef __cplusplus
}
#endif
#endif /* _ATATTPDHMANAGERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT PDH Manager
 *
 * File        : AtAttPdhManager.c
 *
 * Created Date: Jul 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of ATT PDH Manager of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAttSdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtAttSdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtAttSdhManagerMethods m_methods;

/* Override */

/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HasForcePointer(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LineAttInit(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet LoPathAttInit(AtAttSdhManager self)
    {
    AtUnused(self);
   return cAtErrorNotImplemented;
    }

static eAtRet HoPathAttInit(AtAttSdhManager self)
    {
    AtUnused(self);
   return cAtErrorNotImplemented;
    }

static eAtRet Init(AtAttSdhManager self)
    {
    mMethodsGet(mThis(self))->LineAttInit(mThis(self));
    mMethodsGet(mThis(self))->HoPathAttInit(mThis(self));
    mMethodsGet(mThis(self))->LoPathAttInit(mThis(self));
    return cAtOk;
    }

static eAtRet LoPathForcePointerAdjEnable(AtAttSdhManager self, eBool en)
    {
    AtUnused(self);
    AtUnused(en);
    return cAtErrorModeNotSupport;
    }

static eAtRet LoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds)
    {
    AtUnused(self);
    AtUnused(numSeconds);
    return cAtErrorModeNotSupport;
    }

static uint32 LoForcePeriodGet(AtAttSdhManager self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet HoPathForcePointerAdjEnable(AtAttSdhManager self, eBool en)
    {
    AtUnused(self);
    AtUnused(en);
    return cAtErrorModeNotSupport;
    }

static eAtRet HoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds)
    {
    AtUnused(self);
    AtUnused(numSeconds);
    return cAtErrorModeNotSupport;
    }

static uint32 HoForcePeriodGet(AtAttSdhManager self)
    {
    AtUnused(self);
    return 0;
    }

static void    ForcePointerProcess(AtAttSdhManager self)
    {
    AtUnused(self);
    }

static uint32   ForcePointerPeriodicProcess(AtAttSdhManager self, uint32 periodInMs)
    {
    AtUnused(periodInMs);
    if (mMethodsGet(mThis(self))->HasForcePointer(mThis(self)))
       {
       mMethodsGet(mThis(self))->ForcePointerProcess(mThis(self));
       }
    return 0;
    }

static void Override(AtAttSdhManager self)
    {
    AtUnused(self);
    }

static void MethodsInit(AtAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, LineAttInit);
        mMethodOverride(m_methods, LoPathAttInit);
        mMethodOverride(m_methods, HoPathAttInit);
        mMethodOverride(m_methods, LoPathForcePointerAdjEnable);
        mMethodOverride(m_methods, LoForcePeriodSet);
        mMethodOverride(m_methods, LoForcePeriodGet);
        mMethodOverride(m_methods, HoPathForcePointerAdjEnable);
        mMethodOverride(m_methods, HoForcePeriodSet);
        mMethodOverride(m_methods, HoForcePeriodGet);
        mMethodOverride(m_methods, HasForcePointer);
        mMethodOverride(m_methods, ForcePointerProcess);
        mMethodOverride(m_methods, ForcePointerPeriodicProcess);
        }

    mMethodsSet(self, &m_methods);
    }

AtAttSdhManager AtAttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtAttSdhManager));

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    self->sdh = sdh;
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

eAtRet  AtAttSdhManagerInit(AtAttSdhManager self)
    {
    if (self)
        return  mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }


eAtRet AtAttSdhManagerLoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds)
    {
    if (self)
           return mMethodsGet(self)->LoForcePeriodSet(self, numSeconds);
    return cAtErrorModeNotSupport;
    }

uint32 AtAttSdhManagerLoForcePeriodGet(AtAttSdhManager self)
    {
    if (self)
        return mMethodsGet(self)->HoForcePeriodGet(self);
    return 0;
    }

eAtRet AtAttSdhManagerForcePointerAdjEnable(AtAttSdhManager self, eBool en)
    {
    if (self)
        {
        eAtRet ret = cAtOk;
        ret |= mMethodsGet(self)->HoPathForcePointerAdjEnable(self, en);
        ret |= mMethodsGet(self)->LoPathForcePointerAdjEnable(self, en);
        return ret;
        }
    return cAtErrorModeNotSupport;
    }

eAtRet AtAttSdhManagerHoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds)
    {
    if (self)
       return mMethodsGet(self)->HoForcePeriodSet(self, numSeconds);
    return cAtErrorModeNotSupport;
    }

uint32 AtAttSdhManagerHoForcePeriodGet(AtAttSdhManager self)
    {
    if (self)
       return mMethodsGet(self)->HoForcePeriodGet(self);
    return 0;
    }

uint32 AtAttSdhManagerTiePeriodicProcess(AtAttSdhManager self, uint32 periodInMs)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->TiePeriodicProcess(self, periodInMs);
    }

uint32 AtAttSdhManagerSaveTiePeriodicProcess(AtAttSdhManager self, uint32 periodInMs)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->SaveTiePeriodicProcess(self, periodInMs);
    }

uint32 AtAttSdhManagerForcePointerPeriodicProcess(AtAttSdhManager self, uint32 periodInMs)
    {
    if (self == NULL)
        return 0;

    return mMethodsGet(self)->ForcePointerPeriodicProcess(self, periodInMs);
    }


AtModuleSdh AtAttSdhManagerModuleSdh(AtAttSdhManager self)
    {
    if (self)
        return self->sdh;
    return NULL;
    }

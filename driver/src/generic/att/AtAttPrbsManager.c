/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT PDH Manager
 *
 * File        : AtAttPdhManager.c
 *
 * Created Date: Jul 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of ATT PDH Manager of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAttPrbsManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtAttPrbsManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtAttPrbsManagerMethods m_methods;

/* Override */

/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Init(AtAttPrbsManager self)
    {
    AtUnused(self);
    self->forceLogic = cAttForceLogicAtt;
    self->UseComplexId = cAtFalse;
    return cAtErrorNotImplemented;
    }

static eBool DelayIsSupported(AtAttPrbsManager self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static eAtRet TieReset(AtAttPrbsManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet TieBufRead(AtAttPrbsManager self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }
static eAtRet ForceLogicSet(AtAttPrbsManager self, eAttForceLogic forceLogic)
    {
    AtUnused(self);
    self->forceLogic = forceLogic;
    return cAtOk;
    }

static eAttForceLogic ForceLogicGet(AtAttPrbsManager self)
    {
    AtUnused(self);
    return self->forceLogic;
    }
static eAtRet UseComplexIdSet(AtAttPrbsManager self, eBool en)
    {
    AtUnused(self);
    AtUnused(en);
    self->UseComplexId = en;
    return cAtOk;
    }


static eBool UseComplexIdGet(AtAttPrbsManager self)
    {
    AtUnused(self);
    return self->UseComplexId;
    }


static eAtRet TimeMeasurementErrorSet(AtAttPrbsManager self, uint32 error)
    {
    AtUnused(self);
    AtUnused(error);
    return cAtErrorNotImplemented;
    }

static uint32 TimeMeasurementErrorGet(AtAttPrbsManager self)
    {
    AtUnused(self);
    return 0;
    }

static void Override(AtAttPrbsManager self)
    {
    AtUnused(self);
    }

static void MethodsInit(AtAttPrbsManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, ForceLogicGet);
        mMethodOverride(m_methods, ForceLogicSet);
        mMethodOverride(m_methods, UseComplexIdSet);
        mMethodOverride(m_methods, UseComplexIdGet);
        mMethodOverride(m_methods, TimeMeasurementErrorSet);
        mMethodOverride(m_methods, TimeMeasurementErrorGet);
        mMethodOverride(m_methods, TieReset);
        mMethodOverride(m_methods, TieBufRead);
        mMethodOverride(m_methods, DelayIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtAttPrbsManager);
    }

AtAttPrbsManager AtAttPrbsManagerObjectInit(AtAttPrbsManager self, AtModulePrbs prbs)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    self->prbs = prbs;
    self->forceLogic = cAttForceLogicAtt;
    Override(mThis(self));
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

eAtRet  AtAttPrbsManagerInit(AtAttPrbsManager self)
    {
    if (self)
        return  mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }

/**
 * Get PRBS Module
 *
 * @param self This module
 * @param en The error of time measurement
 *
 * @return Maximum detection time
 */
AtModulePrbs AtAttPrbsManagerModulePrbs(AtAttPrbsManager self)
    {
    return self->prbs;
    }
/**
 * Set Complex Id using
 *
 * @param self This module
 * @param en The error of time measurement
 *
 * @return Maximum detection time
 */
eAtRet AtAttPrbsManagerForceLogicSet(AtAttPrbsManager self, eAttForceLogic forceLogic)
    {
    mNumericalAttributeSet(ForceLogicSet, forceLogic);
    }

/**
 * Set Complex Id using
 *
 * @param self This module
 * @param en The error of time measurement
 *
 * @return Maximum detection time
 */
eAttForceLogic AtAttPrbsManagerForceLogicGet(AtAttPrbsManager self)
    {
    mAttributeGet(ForceLogicGet, eAttForceLogic, cAttForceLogicDut);
    }
/**
 * Set Complex Id using
 *
 * @param self This module
 * @param en The error of time measurement
 *
 * @return Maximum detection time
 */
eAtRet AtAttPrbsManagerUseComplexIdSet(AtAttPrbsManager self, eBool en)
    {
    mNumericalAttributeSet(UseComplexIdSet, en);
    }

/**
 * Get Complex Id using
 *
 * @param self This module
 * @param error The error of time measurement
 *
 * @return Maximum detection time
 */
eBool AtAttPrbsManagerUseComplexIdGet(AtAttPrbsManager self)
    {
    mAttributeGet(UseComplexIdGet, eBool, cAtFalse);
    }

/**
 * Set PRBS Disruption Threshold
 *
 * @param self This module
 * @param error The error of time measurement
 *
 * @return Maximum detection time
 */

eAtRet AtAttPrbsManagerTimeMeasurementErrorSet(AtAttPrbsManager self, uint32 error)
    {
    mNumericalAttributeSet(TimeMeasurementErrorSet, error);
    }

/**
 * Get PRBS Time measurement Error
 *
 * @param self This module
 *
 * @return Maximum detection time
 *
 */
uint32 AtAttPrbsManagerTimeMeasurementErrorGet(AtAttPrbsManager self)
    {
    mAttributeGet(TimeMeasurementErrorGet, uint32, 0);
    }

/**
 * Reset TIE
 *
 * @param self This module
 *
 * @return Maximum detection time
 *
 */

eAtRet AtAttPrbsManagerTieReset(AtAttPrbsManager self)
    {
    mNoParamAttributeSet(TieReset);
    }

/**
 * Get to know PRBS support delay or not
 *
 * @param self This module
 *
 * @return Maximum detection time
 *
 */
eBool AtAttPrbsManagerDelayIsSupported(AtAttPrbsManager self)
    {
	mAttributeGet(DelayIsSupported, eBool, cAtFalse);
    }

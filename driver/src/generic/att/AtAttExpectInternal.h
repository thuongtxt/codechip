/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtAttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTEXPECTINTERNAL_H_
#define _ATATTEXPECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtObjectInternal.h"
#include "../../../include/att/AtAttExpect.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tAtAttExpectChannelMethods
    {
    eAtRet (*Xxx)(AtAttExpectChannel self);
    }tAtAttExpectChannelMethods;

typedef struct tAtAttExpectChannel
    {
    tAtObject super;
    const tAtAttExpectChannelMethods *methods;

    /* Private data */
    AtChannel channel;
    char      alarmString[50];
    uint32    channelType;
    uint32    channel_type;
    eAtAttForceAlarmMode    alarmRxExpectMode;
    uint32    alarmRxExpectDuration; /*seconds*/
    uint32    alarmRxExpectNumEvent;
    uint32    alarmRxExpectCount;
    uint32    setCount;
    uint32    clearCount;
    uint32    diff;
    AtList    timeList;
    eBool     debug;
    }tAtAttExpectChannel;

typedef struct tAtAttExpectMethods
    {
    eAtRet (*Xxx)(AtAttExpect self);
    }tAtAttExpectMethods;

typedef struct tAtAttExpect
    {
    tAtObject super;
    const tAtAttExpectMethods *methods;

    /* Private data */
    AtList channels;
    }tAtAttExpect;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttExpect AtAttExpectObjectInit(AtAttExpect self);

#ifdef __cplusplus
}
#endif
#endif /* _ATATTEXPECTINTERNAL_H_ */


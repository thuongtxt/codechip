/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtAttWanderChannelInternal.h
 * 
 * Created Date: Jun 07, 2017
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTWANDERCHANNELINTERNAL_H_
#define _ATATTWANDERCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtObjectInternal.h"
#include "../../../include/common/AtChannel.h"
#include "../../../include/att/AtAttWanderChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
#define cAtAttWanderChannelFileLength 200
typedef struct tAtAttWanderChannelMethods
    {
    eAtRet (*Init)(AtAttWanderChannel self);
    eAtRet (*FileNameSet)(AtAttWanderChannel self, const char* filename);
    eAtRet (*SaveAs)(AtAttWanderChannel self, const char* filename);
    uint32 (*WanderValue)(AtAttWanderChannel self, eBool *sign, uint8 *NClkCnt);
    uint32 (*ClockRate)(AtAttWanderChannel self);
    float (*SystemClockTime)(AtAttWanderChannel self);
    float (*ChannelClockTime)(AtAttWanderChannel self);
    }tAtAttWanderChannelMethods;

typedef struct tAtAttWanderChannel
    {
    tAtObject super;
    const tAtAttWanderChannelMethods *methods;

    /* Private data */
    AtChannel channel;
    char      fileName[cAtAttWanderChannelFileLength];
    uint32    duration;
    tAtOsalCurTime start_time;
    tAtOsalCurTime time_reset;
    AtList    timeList[2];
    eBool     debug;
    uint8     updatePage;
    uint32    i;
    AtFile    file;
    eBool     isRecording;
    eBool     needSave;
    uint8     savePage;
    AtOsalMutex mutex;
    AtOsalMutex mutexSave;
    }tAtAttWanderChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttWanderChannel AtAttWanderChannelObjectInit(AtAttWanderChannel self, AtChannel channel);
eAtRet AtAttWanderChannelAddValue(AtAttWanderChannel self, uint32 numclock, eBool NClockSign, uint8 NClockCount, uint32 regVal);
#ifdef __cplusplus
}
#endif
#endif /* _ATATTWANDERCHANNELINTERNAL_H_ */


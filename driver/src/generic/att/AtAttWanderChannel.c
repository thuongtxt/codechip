/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : AtAttExpect.c
 *
 * Created Date: May 16, 2016
 *
 * Description : ATT controller abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtAttWanderChannelInternal.h"
#include "time.h"
#include "math.h"
#include "../../../include/pdh/AtPdhDe1.h"
#include "../../../include/pdh/AtPdhDe3.h"
#include <stdlib.h> /* system */
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtAttWanderChannel)self)
#define cMaxRecordingLength 100
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtAttWanderChannelMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 GetCurrentPage(AtAttWanderChannel self)
    {
    return self->updatePage;
    }
static uint32 WanderValue(AtAttWanderChannel self, eBool *sign, uint8 *NClkCnt)
    {
    AtUnused(self);
    if (self->i == 30)
        self->i = 0;
    self->i = self->i+1;
    *sign = 1;
    *NClkCnt = 20;
    return self->i;
    }

static float SystemClockTime(AtAttWanderChannel self)
    {
    AtUnused(self);
    return (float)1/(float)116640000;
    }

static uint32 ClockRate(AtAttWanderChannel self)
    {
    AtUnused(self);
    return 2048;
    }

static float ChannelClockTime(AtAttWanderChannel self)
    {
    uint32 clock_rate = mMethodsGet(self)->ClockRate(self)  * 1000;
    return (float)1/(float)clock_rate;
    }

static eAtRet PageSwitch(AtAttWanderChannel self)
    {
    AtOsalMutexLock(self->mutexSave);
    self->savePage =self->updatePage;
    self->updatePage = self->updatePage ? 0 : 1;
    self->needSave = cAtTrue;
    AtOsalMutexUnLock(self->mutexSave);
    return cAtOk;
    }

static void DeleteItemTimeList(AtAttWanderChannel self, uint8 page)
    {
    while (AtListLengthGet(mThis(self)->timeList[page]))
        {
        tAtAttWanderChannelTime*  stime=NULL;
        stime = (tAtAttWanderChannelTime*)AtListObjectRemoveAtIndex(mThis(self)->timeList[page], 0);
        AtOsalMemFree(stime);
        }
    }

static void DeleteTimeList(AtAttWanderChannel self)
    {
    uint8 page=0;
    for (page=0; page<2; page++)
        {
        if (mThis(self)->timeList[page] != NULL)
           {
            DeleteItemTimeList(self, page);
            AtObjectDelete((AtObject)mThis(self)->timeList[page]);
            }
        mThis(self)->timeList[page] = NULL;
        }
    }


static void DeleteFile(AtAttWanderChannel self)
    {
    if (self->file)
        AtFileClose(self->file);
    self->file = NULL;
    }
static void DeleteMutex(AtAttWanderChannel self)
    {
    if (self->mutex)
            AtOsalMutexDestroy(self->mutex);
    if (self->mutexSave)
            AtOsalMutexDestroy(self->mutexSave);
    self->mutex = NULL;
    self->mutexSave = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteTimeList((AtAttWanderChannel)self);
    DeleteFile((AtAttWanderChannel)self);
    DeleteMutex((AtAttWanderChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtObject self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtAttWanderChannel self)
    {
    OverrideAtObject((AtObject)self);
    }

static eAtRet Init(AtAttWanderChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet FileNameSet (AtAttWanderChannel self, const char* filename)
    {
    AtOsalMemInit(self->fileName, 0, cAtAttWanderChannelFileLength);
    AtOsalMemCpy(self->fileName, filename, AtStrlen(filename));
    return cAtOk;
    }

static eAtRet SaveAs (AtAttWanderChannel self, const char* filename)
    {
    AtOsalMutexLock(self->mutexSave);
    if (self->isRecording)
        {
        char cmd[cAtAttWanderChannelFileLength];
        AtStd std = AtStdSharedStdGet();
        if (std)
            {
            AtFileClose(self->file);
            }
        AtSprintf(cmd, "cp -f %s %s", self->fileName, filename);
        system(cmd);
        if (std)
            {
            uint8 mode = cAtFileOpenModeAppend;
            if (AtStrcmp(self->fileName,"")==0)
                AtSnprintf(self->fileName,sizeof(self->fileName),"Tie%s.csv", AtObjectToString((AtObject)self->channel));
            self->file = AtStdFileOpen(std,self->fileName, mode);
            }
        }
    AtOsalMutexUnLock(self->mutexSave);
    AtPrintf("   SaveAs File %s Done\r\n", filename);
    return cAtOk;
    }

static void MethodsInit(AtAttWanderChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, FileNameSet);
        mMethodOverride(m_methods, SaveAs);
        mMethodOverride(m_methods, WanderValue);
        mMethodOverride(m_methods, SystemClockTime);
        mMethodOverride(m_methods, ClockRate);
        mMethodOverride(m_methods, ChannelClockTime);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtAttWanderChannel);
    }

AtAttWanderChannel AtAttWanderChannelObjectInit(AtAttWanderChannel self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    self->channel = channel;
    self->isRecording = cAtFalse;
    self->needSave = cAtFalse;
    self->mutex = AtOsalMutexCreate();
    self->mutexSave = AtOsalMutexCreate();
    Init(self);

    return self;
    }

AtAttWanderChannel AtAttWanderChannelNew(AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttWanderChannel newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return AtAttWanderChannelObjectInit(newObject, channel);
    }

static const char* Time2String(tAtOsalCurTime *t, uint8 numMilisecondDigit)
    {
    struct tm* tm_info;
    static char strTime[35];
    char tmp[35];
    uint32 length;

    AtOsalMemInit(strTime, 0, sizeof(strTime));
    AtOsalMemInit(tmp, 0, sizeof(tmp));
    tm_info = localtime(((const time_t*)&t->sec));
    if (numMilisecondDigit==6)
        {
        strftime(tmp, 26, "%Y-%m-%d %H:%M:%S", tm_info);
        AtSnprintf(&tmp[AtStrlen(tmp)], 8, ".%06ld", (long)t->usec);
        }
    else
        {
        strftime(tmp, 26, "%H:%M:%S", tm_info);
        AtSnprintf(&tmp[AtStrlen(tmp)], 8, ".%03ld", (long)t->usec);
        }
    length = AtStrlen(tmp);
    AtStrncpy(strTime,tmp, length-3 > 0? length-3: length);
    if (length==13)
        AtStrncat(strTime, "00",sizeof(strTime));
    if (length==14)
        AtStrncat(strTime, "0",sizeof(strTime));
    if (length<13)
        AtStrncat(strTime, "000",sizeof(strTime));
    return strTime;
    }
static char* ConvertAlarmToString(uint32 alarm)
    {
    static char alarmString[cAtAlarmStrBufLength];
    AtOsalMemInit(alarmString,0, sizeof(alarmString));
    if (alarm & cAtPdhDe1AlarmLos)
        AtStrncat(alarmString, "LOS", sizeof(alarmString));
    if (alarm & cAtPdhDe1AlarmLof)
        AtStrncat(alarmString, "LOF", sizeof(alarmString));
    if (alarm & cAtPdhDe1AlarmAis)
        AtStrncat(alarmString, "AIS", sizeof(alarmString));
    if (alarm & cAtPdhDe1AlarmRai)
        AtStrncat(alarmString, "RAI", sizeof(alarmString));
    return (char*)alarmString;
    }
static const char* TimeItem2String(eBool debug, tAtAttWanderChannelTime *stime)
    {
    static char strItemTime[cAtAlarmStrBufLength];
    static char strItemValue[cAtAlarmStrBufLength];
    static char stralarm[cAtAlarmStrBufLength];
    uint8 numMilisecondDigit = 3;
    float second = stime->value;
    stime->type = 0;
    AtOsalMemInit(strItemTime, 0, sizeof(strItemTime));
    AtOsalMemInit(stralarm, 0, sizeof(stralarm));
    AtSnprintf(stralarm, sizeof(stralarm), "%s", (const char*)ConvertAlarmToString(stime->alarm));
    if (second<0.9999999999)
        {
        AtOsalMemInit(strItemValue, 0, sizeof(strItemValue));
        second = second * 1000;
        stime->type = 3;
        AtSnprintf(strItemValue, sizeof(strItemValue),"%3.7fE-3", second);

        if (second<0.9999999999)
            {
            AtOsalMemInit(strItemTime, 0, sizeof(strItemTime));
            second = second * 1000;
            stime->type = 6;
            AtSnprintf(strItemValue, sizeof(strItemValue),"%3.7fE-6", second);

            if (second<0.9999999999999)
                {
                AtOsalMemInit(strItemTime, 0, sizeof(strItemTime));
                second = second * 1000;
                stime->type = 9;
                AtSnprintf(strItemValue,sizeof(strItemValue),"%3.7fE-9", second);

                if (second<0.9999999999)
                    {
                    AtOsalMemInit(strItemTime, 0, sizeof(strItemTime));
                    second = second * 1000;
                    stime->type = 12;
                    AtSnprintf(strItemValue,sizeof(strItemValue),"%3.7fE-12", second);
                    }
                }
            }
        }else {
            AtOsalMemInit(strItemTime, 0, sizeof(strItemTime));
            stime->type = 0;
            AtSnprintf(strItemValue,sizeof(strItemValue),"%3.7fE0", second);
        }

    if (stime->alarm ==0)
        {
        if (debug)
            AtSnprintf(strItemTime,sizeof(strItemValue), "%s; %s%s;0x%X",
                              Time2String(&stime->time, numMilisecondDigit),
                              stime->sign? "" : "-", strItemValue, stime->regVal);
        else
            AtSnprintf(strItemTime,sizeof(strItemValue), "%s; %s%s;",
                                          Time2String(&stime->time, numMilisecondDigit),
                                          stime->sign? "" : "-", strItemValue);
        }
    else
        {
        AtSnprintf(strItemValue,sizeof(strItemValue),"%s","****");
        AtSnprintf(strItemTime,sizeof(strItemValue), "%s; %s; %s",
                  Time2String(&stime->time, numMilisecondDigit),
                  strItemValue,
                  (char*) stralarm);
        }
    return strItemTime;
    }


static uint32 AtAttWanderListLength(AtAttWanderChannel self, uint8 page)
    {
    AtList list = AtAttWanderChannelListGet(self, page);
    return AtListLengthGet(list);
    }

static float ConvertWanderValueV1(float UnitCnt, float Tservice, eBool sign, float NClockCount, float Tsample)
    {
    float tie=0;
    /*If UnitCnt < 0x1000000 (hex), RX clock is faster than Tx Clock.
        + signcnt is 1 : TIE = (0x1000000 - UnitCnt) x Tservice   -  NClkCnt x Tsample.
        + signcnt is 0 : TIE = (0x1000000 - UnitCnt) x Tservice  +  NClkCnt x Tsample.

    If UnitCnt > 0x1000000 (hex), TX clock is faster than Rx Clock.
    + signcnt is 1 : TIE = (UnitCnt - 0x1000000) x Tservice   +  NClkCnt x Tsample.
    + signcnt is 0 : TIE = (UnitCnt - 0x1000000) x Tservice   -  NClkCnt x Tsample.*/
    if (UnitCnt < 0x1000000)
        {
        if (sign) tie =  (0x1000000 - UnitCnt)*Tservice - (NClockCount*Tsample);
        else      tie =  (0x1000000 - UnitCnt)*Tservice + (NClockCount*Tsample);
        }
    else
        {
        if (sign) tie =  (UnitCnt - 0x1000000)*Tservice + (NClockCount*Tsample);
        else      tie =  (UnitCnt - 0x1000000)*Tservice - (NClockCount*Tsample);
        }
    return tie;
    }

static float ConvertWanderValueV2(float UnitCnt, float Tservice, eBool sign, float NClockCount, float Tsample)
    {
    float tie=0;
    /*If UnitCnt < 0x40000 (hex), RX clock is faster than Tx Clock.
        + signcnt is 1 : TIE = (0x40000 - UnitCnt) x Tservice   -  NClkCnt x Tsample.
        + signcnt is 0 : TIE = (0x40000 - UnitCnt) x Tservice  +  NClkCnt x Tsample.

    If UnitCnt > 0x40000 (hex), TX clock is faster than Rx Clock.
    + signcnt is 1 : TIE = (UnitCnt - 0x40000) x Tservice   +  NClkCnt x Tsample.
    + signcnt is 0 : TIE = (UnitCnt - 0x40000) x Tservice   -  NClkCnt x Tsample.*/
    if (UnitCnt < 0x40000)
        {
        if (sign) tie =  (0x40000 - UnitCnt)*Tservice - (NClockCount*Tsample);
        else      tie =  (0x40000 - UnitCnt)*Tservice + (NClockCount*Tsample);
        }
    else
        {
        if (sign) tie =  (UnitCnt - 0x40000)*Tservice + (NClockCount*Tsample);
        else      tie =  (UnitCnt - 0x40000)*Tservice - (NClockCount*Tsample);
        }
    return tie;
    }

static float ConvertWanderValueV3(float UnitCnt, float Tservice, eBool sign, float NClockCount, float Tsample)
    {
    float tie=0;
    /*If UnitCnt < 0x20000 (hex), RX clock is faster than Tx Clock.
        + signcnt is 1 : TIE = (0x20000 - UnitCnt) x Tservice   -  NClkCnt x Tsample.
        + signcnt is 0 : TIE = (0x40000 - UnitCnt) x Tservice  +  NClkCnt x Tsample.

    If UnitCnt > 0x20000 (hex), TX clock is faster than Rx Clock.
    + signcnt is 1 : TIE = (UnitCnt - 0x20000) x Tservice   +  NClkCnt x Tsample.
    + signcnt is 0 : TIE = (UnitCnt - 0x20000) x Tservice   -  NClkCnt x Tsample.*/
    if (UnitCnt < 0x20000)
        {
        if (sign) tie =  (0x20000 - UnitCnt)*Tservice - (NClockCount*Tsample);
        else      tie =  (0x20000 - UnitCnt)*Tservice + (NClockCount*Tsample);
        }
    else
        {
        if (sign) tie =  (UnitCnt - 0x20000)*Tservice + (NClockCount*Tsample);
        else      tie =  (UnitCnt - 0x20000)*Tservice - (NClockCount*Tsample);
        }
    return tie;
    }

static float ConvertWanderValueV4(float UnitCnt, float Tservice, eBool sign, float NClockCount, float Tsample)
    {
    float tie=0;
    /*If UnitCnt < 0x20000 (hex), RX clock is faster than Tx Clock.
        + signcnt is 1 : TIE = (0x20000 - UnitCnt) x Tservice   -  NClkCnt x Tsample.
        + signcnt is 0 : TIE = (0x40000 - UnitCnt) x Tservice  +  NClkCnt x Tsample.

    If UnitCnt > 0x20000 (hex), TX clock is faster than Rx Clock.
    + signcnt is 1 : TIE = (UnitCnt - 0x20000) x Tservice   +  NClkCnt x Tsample.
    + signcnt is 0 : TIE = (UnitCnt - 0x20000) x Tservice   -  NClkCnt x Tsample.*/
    if (UnitCnt < 0x20000)
        {
        if (sign) tie =  (0x20000 - UnitCnt)*Tservice + (NClockCount*Tsample);
        else      tie =  (0x20000 - UnitCnt)*Tservice - (NClockCount*Tsample);
        }
    else
        {
        if (sign) tie =  (UnitCnt - 0x20000)*Tservice - (NClockCount*Tsample);
        else      tie =  (UnitCnt - 0x20000)*Tservice + (NClockCount*Tsample);
        }
    return tie;
    }

static void GetTimeLog(AtAttWanderChannel self, tAtAttWanderChannelTime *stime)
    {
    /*AtOsalCurTimeGet(&stime->time);*/
    AtAttWanderChannelIncrease(self);
    AtOsalMemCpy(&stime->time, &self->time_reset, sizeof(tAtOsalCurTime));
    }

static uint32 SampleWanderValueV3(uint32 regValue, eBool *sign, uint8 *NClkCnt, uint32 *portId)
    {
    uint32 regFieldMask = cBit25_8;
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
    uint32 numclock_of_system_clock = (uint32)mRegField(regValue, regField);

    regFieldMask = cBit6_0;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *NClkCnt = (uint8)mRegField(regValue, regField);

    regFieldMask = cBit7;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *sign = (uint8)mRegField(regValue, regField);

    regFieldMask = cBit31_26;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    *portId = (uint32)mRegField(regValue, regField);
    return numclock_of_system_clock;
    }



static void Sample3(uint32 regVal )
    {
    /*
     ./apps/ep_app/epapp -s 6A210021
     device init
     att pdh de1 wander filename 1 a.txt
     */
    float Tservice = (float)1/(float)2048000;
    float Tsample = (float)1/(float)116640000;
    eBool sign;
    uint8 NClockCount;
    uint32 portId;
    uint32 regValue = regVal;
    float value;
    uint32 UnitCnt = SampleWanderValueV3(regValue, &sign, &NClockCount, &portId);
    AtPrintf("regValue       = 0x%x\r\n", regValue);
    AtPrintf("->portId         = %d\r\n", portId);
    AtPrintf("->sign=NClockSign= %d\r\n", sign);
    AtPrintf("->NClockCount    = %d\r\n", NClockCount);
    AtPrintf("->UnitCnt        = %d\r\n", UnitCnt);
    AtPrintf("->Tservice       = %3.15f\r\n", Tservice);
    AtPrintf("->Tsample        = %3.15f\r\n", Tsample);
    value = ConvertWanderValueV4((float)UnitCnt, Tservice, sign, (float)NClockCount, Tsample);
    AtPrintf("%3.15f\r\n", value);
    if (UnitCnt < 0x20000)
        {
        float tie=0;
        AtPrintf("RX clock is faster than Tx Clock\r\n");
        AtPrintf("-> (0x20000 - UnitCnt)*Tservice      = %3.15f\r\n", (0x20000 - (float)UnitCnt)*Tservice);
        AtPrintf("-> NClockCount*Tsample               = %3.15f\r\n", (float)NClockCount*Tsample);
        if (sign) tie =  (0x20000 - (float)UnitCnt)*Tservice + ((float)NClockCount*Tsample);
        else      tie =  (0x20000 - (float)UnitCnt)*Tservice - ((float)NClockCount*Tsample);
        AtPrintf("->tie        = %3.15f\r\n", tie);
        }
    else
        {

        AtPrintf("TX clock is faster than Rx Clock\r\n");
        }
    AtPrintf("-----------------------------\r\n");

    }
static void CheckReg (void)
    {
    uint32 regValue = 0x00011616, rxhdrsize=01;
    uint32 regFieldMask = cBit15_8;
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
    uint32 rxmonhdrsize = (uint32)mRegField(regValue, regField);

    regFieldMask = cBit7_0;
    regFieldShift = AtRegMaskToShift(regFieldMask);
    rxhdrsize = (uint32)mRegField(regValue, regField);
    AtPrintf("rxmonhdrsize %d \r\n", rxmonhdrsize);
    AtPrintf("rxhdrsize %d \r\n", rxhdrsize);
    }

eAtRet AtAttWanderChannelAddValue(AtAttWanderChannel self, uint32 numclock, eBool NClockSign, uint8 NClockCount, uint32 regVal)
    {
    AtOsalMutexLock(self->mutex);
    if (self->isRecording)
        {
        uint8 page               = GetCurrentPage(self);
        AtList list              = AtAttWanderChannelListGet(self, page);
        float default_clock      = mMethodsGet(self)->SystemClockTime(self);
        float channel_clock      = mMethodsGet(self)->ChannelClockTime(self);
        tAtAttWanderChannelTime *stime=NULL;
        stime = AtOsalMemAlloc(sizeof(tAtAttWanderChannelTime));
        GetTimeLog(self, stime);
        if (0) stime->value = ConvertWanderValueV1((float)numclock, channel_clock, NClockSign, (float)NClockCount, default_clock);
        if (0) stime->value = ConvertWanderValueV2((float)numclock, channel_clock, NClockSign, (float)NClockCount, default_clock);
        if (0) stime->value = ConvertWanderValueV3((float)numclock, channel_clock, NClockSign, (float)NClockCount, default_clock); /* Right Function*/
        stime->value =  ConvertWanderValueV4((float)numclock, channel_clock, NClockSign, (float)NClockCount, default_clock);/*Modify for test */
        stime->sign  = stime->value>0?0:1;
        stime->value = (float)fabs((double)stime->value);
        stime->alarm = AtChannelAlarmGet(self->channel);
        stime->regVal = regVal;
        AtListObjectAdd(list, (AtObject)stime);
        self->i = self->i + 1;
        if (AtListLengthGet(list) >= cMaxRecordingLength)
            {
            PageSwitch(self);
            }
        }
    AtOsalMutexUnLock(self->mutex);
    if (self->isRecording)
        {
        if (self->duration > 0)
            {
            uint32 TimeMs;
            tAtOsalCurTime cur_time;
            AtOsalCurTimeGet(&cur_time);
            TimeMs = mTimeIntervalInMsGet(self->start_time, cur_time);
            if (TimeMs > (self->duration * 1000))
                AtAttWanderChannelStop(self);
            }
        }
    return cAtOk;
    }

static eAtRet AtAttWanderAddItemToList(AtAttWanderChannel self)
    {
    uint8 NClockCount        = 0;
    eBool NClockSign         = cAtFalse;
    uint32 numclock          = mMethodsGet(self)->WanderValue(self, &NClockSign, &NClockCount);
    return AtAttWanderChannelAddValue(self, numclock, NClockSign, NClockCount, 0);
    }

static eAtRet AddSomeSampleDataToList(AtAttWanderChannel self)
    {
    uint32 i = 0;
    self->i = 0;
    for (i=0; i<50; i++)
        {
        AtAttWanderAddItemToList(self);
        AtOsalUSleep(33333);
        }
    return cAtOk;
    }

static void WanderFileHeaderLog(AtAttWanderChannel self, AtFile file)
    {
    uint32 frequency = (uint32)(mMethodsGet(self)->ClockRate(self));
    AtUnused(self);
    AtFilePrintf(file, "%s\r\n", "ANT-20 - O.172 Jitter Generator/Analyzer");
    AtFilePrintf(file, "%s\r\n", "Start of measurement: 17/08/03 14:12:21.0");
    AtFilePrintf(file, "%s\r\n", "End of measurement:   17/08/03 14:12:54.0");
    AtFilePrintf(file, "%s\r\n", "");
    AtFilePrintf(file, "%s\r\n", "RX structure: STM64 optical AU4 VC12  2M framed PRBS15");
    AtFilePrintf(file, "%s\r\n", "TX structure: STM64 optical AU4 VC12  2M framed PRBS15");
    AtFilePrintf(file, "%s\r\n", "");
    AtFilePrintf(file, "%s\r\n", "TX Jitter:;Off");
    AtFilePrintf(file, "%s\r\n", "TX Wander:;Off");
    AtFilePrintf(file, "%s\r\n", "");
    AtFilePrintf(file, "%s%d%s\r\n", "Reference frequency:;",frequency," kHz");
    AtFilePrintf(file, "%s\r\n", "Samples per second:;30");
    AtFilePrintf(file, "%s\r\n", "LP Filter:;10  Hz");
    AtFilePrintf(file, "%s\r\n", "");
    AtFilePrintf(file, "%s\r\n", "WANDER");
    AtFilePrintf(file, "%s\r\n", "MTIE:;218.5E-6 s;     ;TIE:;218.5E-6 s");
    AtFilePrintf(file, "%s\r\n", "");
    AtFilePrintf(file, "%s\r\n", "TIE versus Time:");
    AtFilePrintf(file, "%s\r\n", "");
    AtFilePrintf(file, "%s\r\n", "Time;TIE/s;Alarm");
    }

static AtFile FileGet(AtAttWanderChannel self)
    {
    AtStd std = AtStdSharedStdGet();
    if (std)
        {
        if (self->file == NULL)
            {
            uint8 mode = cAtFileOpenModeWrite | cAtFileOpenModeTruncate;
            if (AtStrcmp(self->fileName,"")==0)
                AtSnprintf(self->fileName,sizeof(self->fileName),"Tie%s.csv", AtObjectToString((AtObject)self->channel));
            self->file = AtStdFileOpen(std,self->fileName, mode);
            if (self->file == NULL)
                {
                AtFileClose(self->file);
                return NULL;
                }
            WanderFileHeaderLog(self, self->file);
            }
        }
    return self->file;
    }

static void WanderFileItemLog(AtAttWanderChannel self, uint8 page)
    {
    uint32 i=0;
    AtList list = AtAttWanderChannelListGet(self, page);
    uint32 length = AtListLengthGet(list);
    AtFile file = FileGet(self);
    /*AtPrintf("WanderFileItemLog Length=%d\n", length);*/
    for (i = 0; i <length; i++)
        {
        AtObject stime = AtListObjectGet(list, i);
        AtFilePrintf(file, "%s\r\n", TimeItem2String(self->debug, (tAtAttWanderChannelTime*)stime));
        }
    }

static void WanderSample(AtAttWanderChannel self)
    {
    DeleteTimeList(self);
    AddSomeSampleDataToList(self);
    }
static void TimeSample(AtAttWanderChannel self)
    {
    uint8 i;
    AtPrintf("%s\n", Time2String(&self->time_reset, 3));
    for (i=0;i<30; i++)
        {
        AtAttWanderChannelIncrease(self);
        AtPrintf("%s\n", Time2String(&self->time_reset, 3));
        }
    }
eAtRet AtAttWanderChannelFileNameSet(AtAttWanderChannel self, const char* filename)
    {
    if (self)
        {
        if (0) WanderSample(self);
        if (0) TimeSample(self);
        if (0) Sample3(0x1FFFF39);
        if (0) Sample3(0x1FFFF38);
        if (0) CheckReg();
        return mMethodsGet(self)->FileNameSet(self, filename);
        }
    return cAtErrorNullPointer;
    }

eAtRet AtAttWanderChannelSaveAs(AtAttWanderChannel self, const char* filename)
    {
    if (self)
        {
        return mMethodsGet(self)->SaveAs(self, filename);
        }
    return cAtErrorNullPointer;
    }

eAtRet AtAttWanderChannelDurationSet(AtAttWanderChannel self, uint32 duration)
    {
    if (self)
        {
        self->duration = duration;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

uint32 AtAttWanderChannelDurationGet(AtAttWanderChannel self)
    {
    if (self)
        return self->duration;
    return 0;
    }

eAtRet AtAttWanderChannelDebugEnable(AtAttWanderChannel self)
    {
    if (self)
        self->debug = cAtTrue;
    return cAtOk;
    }

eAtRet AtAttWanderChannelDebugDisable(AtAttWanderChannel self)
    {
    if (self)
        {
        self->debug = cAtFalse;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

eBool AtAttWanderChannelDebugIsEnabled(AtAttWanderChannel self)
    {
    if (self)
        return self->debug;
    return cAtFalse;
    }
AtList AtAttWanderChannelListGet(AtAttWanderChannel self, uint8 page)
    {
    if (page>1)
        return NULL;
    if (self->timeList[page]==NULL)
        self->timeList[page] = AtListCreate(0);
    return self->timeList[page];
    }

eAtRet AtAttWanderChannelIncrease(AtAttWanderChannel self)
    {
    self->time_reset.sec = self->time_reset.sec + 0 ;
    self->time_reset.usec = self->time_reset.usec + 33333 ;
    if (self->time_reset.usec >= 1000000L) {           /* Carry? */
        self->time_reset.sec++ ;
        self->time_reset.usec = (uint32)(self->time_reset.usec - 1000000L) ;
    }
    return cAtOk;
    }

eAtRet AtAttWanderChannelSetTimeReset(AtAttWanderChannel self, tAtOsalCurTime *time_reset)
    {
    AtOsalMemCpy(&self->time_reset, time_reset, sizeof(tAtOsalCurTime));
    return cAtOk;
    }

eAtRet AtAttWanderChannelSavePeriodicProcess(AtAttWanderChannel self)
    {
    AtUnused(self);
    if (self->isRecording)
        {
        if (self->needSave == cAtTrue)
            {
            /*if (self->file)
                {
                AtStd std = AtStdSharedStdGet();
                uint8 mode = cAtFileOpenModeWrite | cAtFileOpenModeAppend;
                AtFileClose(self->file);
                self->file = AtStdFileOpen(std,self->fileName, mode);
                }*/
            AtOsalMutexLock(self->mutexSave);
            WanderFileItemLog(self, self->savePage);
            DeleteItemTimeList(self,self->savePage);
            AtOsalMutexUnLock(self->mutexSave);
            self->needSave = cAtFalse;
            }
        }

    return cAtOk;
    }

eAtRet AtAttWanderChannelPeriodicProcess(AtAttWanderChannel self)
    {
    uint8 page = 0;
    uint32 length;
    AtOsalMutexLock(self->mutex);
    if (self->isRecording)
        {
        AtAttWanderAddItemToList(self);
        page = GetCurrentPage(self);
        length = AtAttWanderListLength(self, page);
        if (length==cMaxRecordingLength)
            {
            PageSwitch(self);
            /*WanderFileItemLog(self, page);
            DeleteItemTimeList(self,page);*/
            }
        }
    AtOsalMutexUnLock(self->mutex);

    if (self->duration > 0)
        {
        uint32 TimeMs;
        tAtOsalCurTime cur_time;
        AtOsalCurTimeGet(&cur_time);
        TimeMs = mTimeIntervalInMsGet(self->start_time, cur_time);
        if (TimeMs>(self->duration * 1000))
            AtAttWanderChannelStop(self);
        }

    return cAtOk;
    }

eAtRet AtAttWanderChannelInit(AtAttWanderChannel self)
    {
    if (self)
        {
        self->isRecording = cAtFalse;
        self->savePage = 0;
        self->needSave = cAtFalse;
        return cAtOk;
        }
    return cAtErrorNullPointer;
    }

eAtRet AtAttWanderChannelStart(AtAttWanderChannel self)
    {
    AtOsalMutexLock(self->mutex);
    self->isRecording = cAtTrue;
    if (self->duration >0)
        AtOsalCurTimeGet(&self->start_time);
    AtOsalMutexUnLock(self->mutex);
    return cAtOk;
    }

eAtRet AtAttWanderChannelStop(AtAttWanderChannel self)
    {
    eAtRet ret = cAtOk;
    uint8 page = 0;
    tAtOsalCurTime startTime;
    if (self->debug)
        AtOsalCurTimeGet(&startTime);
    AtOsalMutexLock(self->mutex);
    page = GetCurrentPage(self);
    self->isRecording = cAtFalse;
    PageSwitch(self);

    AtOsalMutexLock(self->mutexSave);
    WanderFileItemLog(self, page);
    DeleteItemTimeList(self, page);
    DeleteFile(self);
    AtOsalMutexUnLock(self->mutexSave);
    if (1)
        AtPrintf("   Close File %s Done\r\n", self->fileName);
    AtOsalMutexUnLock(self->mutex);
    /*------------------------------------*/
    return ret;
    }

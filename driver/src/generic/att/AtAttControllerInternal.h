/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : AtAttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTCONTROLLERINTERNAL_H_
#define _ATATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtAttWanderChannelInternal.h"
#include "AtAttController.h"
#include "AtAttExpectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tAtAttControllerMethods
    {
    eAtRet (*ForceErrorModeSet)(AtAttController self, uint32 errorType, eAtAttForceErrorMode mode);
    eAtRet (*ForceErrorDuration)(AtAttController self, uint32 errorType, uint32 duration);
    eAtRet (*ForceErrorDataMaskSet)(AtAttController self, uint32 errorType, uint32 dataMask);
    eAtRet (*ForceErrorCrcMaskSet)(AtAttController self, uint32 errorType,  uint32 crcMask);
    eAtRet (*ForceErrorCrcMask2Set)(AtAttController self, uint32 errorType,  uint32 crcMask);
    eAtRet (*ForceErrorNumErrorsSet)(AtAttController self, uint32 errorType, uint32 numErrors);
    eAtRet (*ForceErrorBitPositionSet)(AtAttController self, uint32 errorType, uint32 bitPosition);
    eAtRet (*ForceErrorBitPosition2Set)(AtAttController self, uint32 errorType, uint32 bitPosition);
    eAtRet (*ForceErrorContinuous)(AtAttController self, uint32 errorType);
    eAtRet (*ForceErrorPerSecond)(AtAttController self, uint32 errorType);
    eAtRet (*ForceErrorRate)(AtAttController self, uint32 errorType, eAtBerRate rate);
    eAtRet (*ForceErrorStepSet)(AtAttController self, uint32 errorType, uint32 step);
    eAtRet (*ForceErrorStep2Set)(AtAttController self, uint32 errorType, uint32 step);
    uint32 (*ForceErrorStepGet)(AtAttController self, uint32 errorType);
    eAtRet (*ForceError)(AtAttController self, uint32 errorType);
    eAtRet (*UnForceError)(AtAttController self, uint32 errorType);
    eAtRet (*ForceErrorGet)(AtAttController self, uint32 errorType, tAtAttForceErrorConfiguration *configuration);
    eAtRet (*ForceErrorStatusGet)(AtAttController self, uint32 errorType, tAtAttForceErrorStatus *status);

    eAtRet (*ForceAlarmBitPositionSet)(AtAttController self, uint32 alarmType, uint32 bitPosition);
    eAtRet (*ForceAlarmModeSet)(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode);
    eAtRet (*ForceAlarmNumEventSet)(AtAttController self, uint32 alarmType, uint32 numEvent);
    eAtRet (*ForceAlarmNumFrameSet)(AtAttController self, uint32 alarmType, uint32 numFrame);
    eAtRet (*ForceAlarmPerSecond)(AtAttController self, uint32 alarmType);
    eAtRet (*ForceAlarmContinuous)(AtAttController self, uint32 alarmType);
    eAtRet (*ForceAlarmDurationSet)(AtAttController self, uint32 alarmType, uint32 duration);
    uint32 (*ForceAlarmDurationGet)(AtAttController self, uint32 alarmType);
    eAtRet (*ForceAlarmStepSet)(AtAttController self, uint32 alarmType, uint32 step);
    uint32 (*ForceAlarmStepGet)(AtAttController self, uint32 alarmType);
    eAtRet (*ForceAlarm)(AtAttController self, uint32 alarmType);
    eAtRet (*UnForceAlarm)(AtAttController self, uint32 alarmType);
    eAtRet (*ForceAlarmGet)(AtAttController self, uint32 alarmType, tAtAttForceAlarmConfiguration *configuration);
    eAtRet (*ForceAlarmStatusGet)(AtAttController self, uint32 alarmType, tAtAttForceAlarmStatus *status);
    eAtRet (*Init)(AtAttController self);
    eAtRet (*SetUp)(AtAttController self);
    eAtRet (*DebugAlarm)(AtAttController self, uint32 alarmType);
    eAtRet (*RxAlarmCaptureDurationSet)(AtAttController self, uint32 alarmType, uint32 duration);
    uint32 (*RxAlarmCaptureDurationGet)(AtAttController self, uint32 alarmType);
    eAtRet (*RxAlarmCaptureModeSet)(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode);
    eAtAttForceAlarmMode (*RxAlarmCaptureModeGet)(AtAttController self, uint32 alarmType);
    eAtRet (*RxAlarmCaptureNumEventSet)(AtAttController self, uint32 alarmType, uint32 numEvent);
    uint32 (*RxAlarmCaptureNumEventGet)(AtAttController self, uint32 alarmType);
    AtList (*RecordTime)(AtAttController self, uint32 alarmType);
    eAtRet (*RecordTimeAdd)(AtAttController self, uint32 alarmType, const char* state);
    eAtRet (*RecordTimeClear)(AtAttController self, uint32 alarmType);
    uint32 (*RecordTimeNumSet)(AtAttController self, uint32 alarmType);
    uint32 (*RecordTimeNumClear)(AtAttController self, uint32 alarmType);
    uint32 (*RecordTimeDiffGet)(AtAttController self, uint32 alarmType);
    eBool  (*RecordTimeIsOkToClear)(AtAttController self, uint32 alarmType);
    eBool  (*RecordTimeIsOk)(AtAttController self, uint32 alarmType);
    eAtRet (*DebugError)(AtAttController self, uint32 errorType);
    eBool  (*AlarmForceIsSupported)(AtAttController self, uint32 alarmType);
    eBool  (*ErrorForceIsSupported)(AtAttController self, uint32 errorType);
    uint16 (*LongNumDw)(AtAttController self);
    uint16 (*LongWrite)(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen);
    uint16 (*LongRead)(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen);
    uint16 (*PointerLongWrite)(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen);
    uint16 (*PointerLongWrite2nd)(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen);
    uint16 (*PointerLongRead)(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen);
    void   (*Write)(AtAttController self, uint32 regAddr,uint32 regVal);
    uint32 (*Read)(AtAttController self, uint32 regAddr);

    eAtRet (*De1FbitTypeSet)(AtAttController self, uint32 type, eBool en);
    eBool (*De1FbitTypeGet)(AtAttController self, uint32 type);
    eAtRet (*De1CrcGapSet)(AtAttController self, uint16 frameType);

    eAtRet (*FrequenceOffsetSet)(AtAttController self, float ppm);
    float (*FrequenceOffsetGet)(AtAttController self);
    AtAttWanderChannel (*WanderCreate)(AtChannel channel);
    float (*DelayGet)(AtAttController self);

    eAtRet (*ForcePointerAdjModeSet)(AtAttController self, uint32 pointerType, eAtAttForcePointerAdjMode mode);
    eAtAttForcePointerAdjMode (*ForcePointerAdjModeGet)(AtAttController self, uint32 pointerType);
    eAtRet (*ForcePointerAdjNumEventSet)(AtAttController self, uint32 pointerType, uint32 numEvent);
    eAtRet (*ForcePointerAdjDurationSet)(AtAttController self, uint32 pointerType, uint32 duration);
    uint32 (*ForcePointerAdjDurationGet)(AtAttController self, uint32 pointerType);
    eAtRet (*ForcePointerAdj)(AtAttController self, uint32 pointerType);
    eAtRet (*G783ForcePointerAdj)(AtAttController self, uint32 pointerType);
	eAtRet (*UnForcePointerAdj)(AtAttController self, uint32 pointerType);
	eAtRet (*HwUnForcePointerAdj)(AtAttController self, uint32 pointerType);
	eAtRet (*ForcePointerAdjDataMaskSet)(AtAttController self, uint32 pointerType, uint32 dataMask);
    eAtRet (*ForcePointerAdjBitPositionSet)(AtAttController self, uint32 pointerType, uint32 bitPosition);
    eAtRet (*ForcePointerAdjStepSet)(AtAttController self, uint32 pointerType, uint32 step);
    eAtRet (*PointerAdjForcingHwDataMaskSet)(AtAttController self, uint32 pointerType, uint32 dataMask);
    eAtRet (*PointerAdjEnable)(AtAttController self, eBool force);


    eAtRet (*TxSignalingPatternSet)(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern);
    eAtAttSignalingMode (*TxSignalingPatternGet)(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *threshold);
    eAtRet (*TxSignalingEnable)(AtAttController self, uint8 tsId, eBool enable);
    eBool (*TxSignalingIsEnabled)(AtAttController self, uint8 tsId);
    eAtRet (*RxExpectedSignalingPatternSet)(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern);
    eAtAttSignalingMode (*RxExpectedSignalingPatternGet)(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *threshold);
    eAtRet (*RxSignalingEnable)(AtAttController self, uint8 tsId, eBool enable);
    eBool (*RxSignalingIsEnabled)(AtAttController self, uint8 tsId);
    eAtRet (*TxSignalingDump)(AtAttController self, uint8 tsId, uint8 *value, uint8 length);
    eAtRet (*RxSignalingDump)(AtAttController self, uint8 tsId, uint8 *value, uint8 length);
    eAtRet (*TxSignalingDumpDebug)(AtAttController self, uint8 tsId, uint8 *value, uint8 length);
    eAtRet (*RxSignalingDumpDebug)(AtAttController self, uint8 tsId, uint8 *value, uint8 length);

    eBool  (*SignalingLopAlarmGet)(AtAttController self, uint8 tsId); /* Loss of pattern*/
    uint32  (*SignalingLopStickyGet)(AtAttController self, uint8 tsId, eBool r2c); /* Loss of pattern*/
    eBool  (*SignalingFramerStickyGet)(AtAttController self, uint8 tsId, eBool r2c);
    uint32 (*BipCounterGet)(AtAttController self, eBool r2c);
    }tAtAttControllerMethods;

typedef struct tAtAttController
    {
    tAtObject super;
    const tAtAttControllerMethods *methods;

    /* Private data */
    char name[64];
    char *pName;
    AtChannel channel;
    AtAttWanderChannel wander;
    uint32 txAlarmTypes;
    }tAtAttController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController AtAttControllerObjectInit(AtAttController self, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _ATATTCONTROLLERINTERNAL_H_ */


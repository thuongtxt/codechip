/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : AtAttController.c
 *
 * Created Date: May 16, 2016
 *
 * Description : ATT controller abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../common/AtChannelInternal.h"
#include "AtAttWanderChannelInternal.h"
#include "AtAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtAttController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtAttControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet ForcePointerAdjDataMaskSet(AtAttController self, uint32 pointerType, uint32 dataMask)
    {
    AtUnused(self);
    AtUnused(pointerType);
    AtUnused(dataMask);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }
static eAtRet ForcePointerAdjBitPositionSet(AtAttController self, uint32 pointerType, uint32 bitPosition)
    {
    AtUnused(self);
    AtUnused(pointerType);
    AtUnused(bitPosition);

    /* Let concrete class do */
    return cAtErrorNotImplemented;

    }
static eAtRet ForcePointerAdjStepSet(AtAttController self, uint32 pointerType, uint32 step)
    {
    AtUnused(self);
    AtUnused(pointerType);
    AtUnused(step);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet PointerAdjForcingHwDataMaskSet(AtAttController self, uint32 pointerType, uint32 step)
    {
    AtUnused(self);
    AtUnused(pointerType);
    AtUnused(step);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtAttForcePointerAdjMode ForcePointerAdjModeGet(AtAttController self, uint32 pointerType)
    {
    AtUnused(self);
    AtUnused(pointerType);
    return cAtAttForcePointerAdjModeOneshot;
    }

static eAtRet ForcePointerAdjModeSet(AtAttController self, uint32 pointerType, eAtAttForcePointerAdjMode mode)
	{
    AtUnused(self);
    AtUnused(pointerType);
    AtUnused(mode);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForcePointerAdjNumEventSet(AtAttController self, uint32 pointerType, uint32 numEvent)
	{
	AtUnused(self);
	AtUnused(pointerType);
	AtUnused(numEvent);

	/* Let concrete class do */
	return cAtErrorNotImplemented;
	}

static eAtRet ForcePointerAdjDurationSet(AtAttController self, uint32 pointerType, uint32 duration)
	{
	AtUnused(self);
	AtUnused(pointerType);
	AtUnused(duration);
	return cAtErrorNotImplemented;
	}

static uint32 ForcePointerAdjDurationGet(AtAttController self, uint32 pointerType)
    {
    AtUnused(self);
    AtUnused(pointerType);
    return 0;
    }

static eAtRet G783ForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    AtUnused(self);
    AtUnused(pointerType);
    return cAtErrorNotImplemented;
    }

static eAtRet ForcePointerAdj(AtAttController self, uint32 pointerType)
	{
	AtUnused(self);
	AtUnused(pointerType);
	return cAtErrorNotImplemented;
	}

static eAtRet UnForcePointerAdj(AtAttController self, uint32 pointerType)
	{
	AtUnused(self);
	AtUnused(pointerType);
	return cAtErrorNotImplemented;
	}
static eAtRet HwUnForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    AtUnused(self);
    AtUnused(pointerType);
    return cAtErrorNotImplemented;
    }

static AtAttWanderChannel WanderCreate(AtChannel channel)
    {
    AtUnused(channel);
    return NULL;
    }

static eAtRet ForceErrorNumErrorsSet(AtAttController self, uint32 errorType, uint32 numErrors)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(numErrors);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorBitPositionSet(AtAttController self, uint32 errorType, uint32 bitPosition)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(bitPosition);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorBitPosition2Set(AtAttController self, uint32 errorType, uint32 bitPosition)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(bitPosition);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorDataMaskSet(AtAttController self, uint32 errorType, uint32 dataMask)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(dataMask);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorCrcMaskSet(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(crcMask);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorCrcMask2Set(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(crcMask);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceError(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorContinuous(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorPerSecond(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorRate(AtAttController self, uint32 errorType, eAtBerRate rate)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(rate);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorDuration(AtAttController self, uint32 errorType, uint32 duration)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(duration);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }
static eAtRet ForceErrorModeSet(AtAttController self, uint32 errorType, eAtAttForceErrorMode mode)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(mode);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorStepSet(AtAttController self, uint32 errorType, uint32 step)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(step);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorStep2Set(AtAttController self, uint32 errorType, uint32 step)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(step);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint32 ForceErrorStepGet(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);

    /* Let concrete class do */
    return 0;
    }

static eAtRet UnForceError(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorGet(AtAttController self, uint32 errorType, tAtAttForceErrorConfiguration *configuration)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(configuration);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceErrorStatusGet(AtAttController self, uint32 errorType, tAtAttForceErrorStatus *status)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(status);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmModeSet(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(mode);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmNumEventSet(AtAttController self, uint32 alarmType, uint32 numEvent)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(numEvent);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmNumFrameSet(AtAttController self, uint32 alarmType, uint32 numFrame)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(numFrame);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmBitPositionSet(AtAttController self, uint32 alarmType, uint32 numEvent)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(numEvent);
    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarm(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmPerSecond(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmContinuous(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmDurationSet(AtAttController self, uint32 alarmType, uint32 duration)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(duration);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint32 ForceAlarmDurationGet(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);

    /* Let concrete class do */
    return 0;
    }

static eAtRet ForceAlarmStepSet(AtAttController self, uint32 alarmType, uint32 step)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(step);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static uint32 ForceAlarmStepGet(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);

    /* Let concrete class do */
    return 0;
    }

static eAtRet UnForceAlarm(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmConfiguration *configuration)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(configuration);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet ForceAlarmStatusGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmStatus *status)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(status);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }
static void DeleteWander(AtObject self)
    {
    if (mThis(self)->wander)
        AtObjectDelete((AtObject)mThis(self)->wander);
    mThis(self)->wander = NULL;
    }
static void Delete(AtObject self)
    {
    mThis(self)->channel = NULL;
    DeleteWander(self);
    m_AtObjectMethods->Delete(self);
    }

static const char *ToString(AtObject self)
    {
    AtAttController controller = mThis(self);
    AtChannel channel = AtAttControllerChannelGet(controller);

    if (controller->pName)
        return controller->pName;

    AtSnprintf(controller->name,
               sizeof(controller->name) - 1,
               "%s.%s_att_controller", AtChannelTypeString(channel), AtChannelIdString(channel));
    controller->pName = controller->name;

    return controller->pName;
    }

static eAtRet Init(AtAttController self)
    {
    AtUnused(self);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eAtRet SetUp(AtAttController self)
    {
    AtUnused(self);

    /* Let concrete class do */
    return cAtErrorNotImplemented;
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);

    /* Let concrete class do */
    return cAtFalse;
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);

    /* Let concrete class do */
    return cAtFalse;
    }

static uint16 LongNumDw(AtAttController self)
    {
    AtUnused(self);
    return 0;
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtUnused(self);
    AtUnused(regAddr);
    AtUnused(dataBuffer);
    AtUnused(bufferLen);
    return 0;
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtUnused(self);
    AtUnused(regAddr);
    AtUnused(dataBuffer);
    AtUnused(bufferLen);
    return 0;
    }


static uint16 PointerLongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtUnused(self);
    AtUnused(regAddr);
    AtUnused(dataBuffer);
    AtUnused(bufferLen);
    return 0;
    }

static uint16 PointerLongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtUnused(self);
    AtUnused(regAddr);
    AtUnused(dataBuffer);
    AtUnused(bufferLen);
    return 0;
    }

static void   Write(AtAttController self, uint32 regAddr,uint32 regVal)
    {
    AtUnused(self);
    AtUnused(regAddr);
    AtUnused(regVal);
    }

static uint32 Read(AtAttController self, uint32 regAddr)
    {
    AtUnused(self);
    AtUnused(regAddr);
    return 0;
    }

static eAtRet De1FbitTypeSet(AtAttController self, uint32 type, eBool en)
    {
    AtUnused(self);
    AtUnused(type);
    AtUnused(en);
    return cAtErrorNotImplemented;
    }

static eBool De1FbitTypeGet(AtAttController self, uint32 type)
    {
    AtUnused(self);
    AtUnused(type);
    return cAtFalse;
    }

static eAtRet FrequenceOffsetSet(AtAttController self, float ppm)
    {
    AtUnused(self);
    AtUnused(ppm);
    return cAtErrorNotImplemented;
    }

static float FrequenceOffsetGet(AtAttController self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static float DelayGet(AtAttController self)
    {
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtRet DebugError(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorNotImplemented;
    }

static eAtRet DebugAlarm(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorNotImplemented;
    }

static AtList RecordTime(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return NULL;
    }

static eAtRet RecordTimeAdd(AtAttController self, uint32 alarmType, const char* state)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(state);
    return cAtErrorNotImplemented;
    }

static eAtRet RecordTimeClear(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorNotImplemented;
    }

static uint32 RecordTimeNumSet(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return 0;
    }

static uint32 RecordTimeNumClear(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return 0;
    }

static uint32 RecordTimeDiffGet(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return 0;
    }
static eBool RecordTimeIsOkToClear(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static eBool RecordTimeIsOk(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static eAtRet TxSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(sigMode);
    AtUnused(threshold);
    AtUnused(sigPattern);
    return cAtError;
    }

static eAtRet RxExpectedSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(sigMode);
    AtUnused(threshold);
    AtUnused(sigPattern);
    return cAtError;
    }

static eAtAttSignalingMode TxSignalingPatternGet(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *threshold)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(sigPattern);
    AtUnused(threshold);
    return cAtAttSignalingModeInvalid;
    }

static eAtAttSignalingMode RxExpectedSignalingPatternGet(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *threshold)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(sigPattern);
    AtUnused(threshold);
    return cAtAttSignalingModeInvalid;
    }

static eAtRet TxSignalingEnable(AtAttController self, uint8 tsId, eBool enable)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(enable);
    return cAtError;
    }

static eAtRet RxSignalingEnable(AtAttController self, uint8 tsId, eBool enable)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(enable);
    return cAtError;
    }

static eBool TxSignalingIsEnabled(AtAttController self, uint8 tsId)
    {
    AtUnused(self);
    AtUnused(tsId);
    return cAtFalse;
    }

static eBool RxSignalingIsEnabled(AtAttController self, uint8 tsId)
    {
    AtUnused(self);
    AtUnused(tsId);
    return cAtFalse;
    }

static eAtRet TxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(value);
    AtUnused(length);
    return cAtError;
    }

static eAtRet RxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(value);
    AtUnused(length);
    return cAtError;
    }

static eAtRet TxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(value);
    AtUnused(length);
    return cAtError;
    }

static eAtRet RxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(value);
    AtUnused(length);
    return cAtError;
    }

static eBool SignalingLopAlarmGet(AtAttController self, uint8 tsId)
    {
    AtUnused(self);
    AtUnused(tsId);
    return cAtFalse;
    }
static uint32 SignalingLopStickyGet(AtAttController self, uint8 tsId, eBool r2c)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(r2c);
    return 0;
    }
static eBool SignalingFramerStickyGet(AtAttController self, uint8 tsId, eBool r2c)
    {
    AtUnused(self);
    AtUnused(tsId);
    AtUnused(r2c);
    return cAtFalse;
    }

static uint32 BipCounterGet(AtAttController self, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);
    return 0;
    }

static uint16 PointerLongWrite2nd(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtUnused(self);
    AtUnused(regAddr);
    AtUnused(dataBuffer);
    AtUnused(bufferLen);
    return 0;
    }

static eAtRet PointerAdjEnable(AtAttController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static void OverrideAtObject(AtAttController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtAttController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ErrorForceIsSupported);
        mMethodOverride(m_methods, ForceErrorDataMaskSet);
        mMethodOverride(m_methods, ForceErrorCrcMaskSet);
        mMethodOverride(m_methods, ForceErrorCrcMask2Set);
        mMethodOverride(m_methods, ForceErrorModeSet);
        mMethodOverride(m_methods, ForceErrorNumErrorsSet);
        mMethodOverride(m_methods, ForceErrorBitPositionSet);
        mMethodOverride(m_methods, ForceErrorBitPosition2Set);
        mMethodOverride(m_methods, ForceErrorContinuous);
        mMethodOverride(m_methods, ForceErrorPerSecond);
        mMethodOverride(m_methods, ForceErrorRate);
        mMethodOverride(m_methods, ForceErrorDuration);
        mMethodOverride(m_methods, ForceErrorStepSet);
        mMethodOverride(m_methods, ForceErrorStep2Set);
        mMethodOverride(m_methods, ForceErrorStepGet);
        mMethodOverride(m_methods, ForceError);
        mMethodOverride(m_methods, UnForceError);
        mMethodOverride(m_methods, ForceErrorGet);
        mMethodOverride(m_methods, ForceErrorStatusGet);
        mMethodOverride(m_methods, DebugError);

        mMethodOverride(m_methods, AlarmForceIsSupported);
        mMethodOverride(m_methods, ForceAlarmModeSet);
        mMethodOverride(m_methods, ForceAlarmNumEventSet);
        mMethodOverride(m_methods, ForceAlarmNumFrameSet);
        mMethodOverride(m_methods, ForceAlarmBitPositionSet);
        mMethodOverride(m_methods, ForceAlarmPerSecond);
        mMethodOverride(m_methods, ForceAlarmContinuous);
        mMethodOverride(m_methods, ForceAlarmDurationSet);
        mMethodOverride(m_methods, ForceAlarmDurationGet);
        mMethodOverride(m_methods, ForceAlarmStepSet);
        mMethodOverride(m_methods, ForceAlarmStepGet);
        mMethodOverride(m_methods, ForceAlarm);
        mMethodOverride(m_methods, UnForceAlarm);
        mMethodOverride(m_methods, ForceAlarmGet);
        mMethodOverride(m_methods, ForceAlarmStatusGet);
        mMethodOverride(m_methods, DebugAlarm);
        mMethodOverride(m_methods, RecordTime);
        mMethodOverride(m_methods, RecordTimeAdd);
        mMethodOverride(m_methods, RecordTimeClear);
        mMethodOverride(m_methods, RecordTimeNumSet);
        mMethodOverride(m_methods, RecordTimeNumClear);
        mMethodOverride(m_methods, RecordTimeDiffGet);
        mMethodOverride(m_methods, RecordTimeIsOkToClear);
        mMethodOverride(m_methods, RecordTimeIsOk);

        mMethodOverride(m_methods, ForcePointerAdjModeSet);
        mMethodOverride(m_methods, ForcePointerAdjModeGet);
        mMethodOverride(m_methods, ForcePointerAdjNumEventSet);
        mMethodOverride(m_methods, ForcePointerAdjDurationSet);
        mMethodOverride(m_methods, ForcePointerAdjDurationGet);
        mMethodOverride(m_methods, G783ForcePointerAdj);
        mMethodOverride(m_methods, ForcePointerAdj);
        mMethodOverride(m_methods, HwUnForcePointerAdj);
        mMethodOverride(m_methods, UnForcePointerAdj);
        mMethodOverride(m_methods, ForcePointerAdjDataMaskSet);
        mMethodOverride(m_methods, ForcePointerAdjStepSet);
        mMethodOverride(m_methods, ForcePointerAdjBitPositionSet);
        mMethodOverride(m_methods, PointerAdjForcingHwDataMaskSet);
        mMethodOverride(m_methods, TxSignalingPatternSet);
        mMethodOverride(m_methods, TxSignalingPatternGet);
        mMethodOverride(m_methods, TxSignalingEnable);
        mMethodOverride(m_methods, TxSignalingIsEnabled);
        mMethodOverride(m_methods, RxExpectedSignalingPatternSet);
        mMethodOverride(m_methods, RxExpectedSignalingPatternGet);
        mMethodOverride(m_methods, RxSignalingEnable);
        mMethodOverride(m_methods, RxSignalingIsEnabled);
        mMethodOverride(m_methods, TxSignalingDump);
        mMethodOverride(m_methods, RxSignalingDump);
        mMethodOverride(m_methods, TxSignalingDumpDebug);
        mMethodOverride(m_methods, RxSignalingDumpDebug);
        mMethodOverride(m_methods, SignalingLopAlarmGet);
        mMethodOverride(m_methods, SignalingLopStickyGet);
        mMethodOverride(m_methods, SignalingFramerStickyGet);
        mMethodOverride(m_methods, BipCounterGet);
        mMethodOverride(m_methods, PointerAdjEnable);

        mMethodOverride(m_methods, Init);
        mMethodOverride(m_methods, SetUp);
        mMethodOverride(m_methods, LongNumDw);
        mMethodOverride(m_methods, LongRead);
        mMethodOverride(m_methods, LongWrite);
        mMethodOverride(m_methods, PointerLongWrite);
        mMethodOverride(m_methods, PointerLongRead);
        mMethodOverride(m_methods, Read);
        mMethodOverride(m_methods, Write);

        mMethodOverride(m_methods, De1FbitTypeSet);
        mMethodOverride(m_methods, De1FbitTypeGet);

        mMethodOverride(m_methods, FrequenceOffsetSet);
        mMethodOverride(m_methods, FrequenceOffsetGet);
        mMethodOverride(m_methods, WanderCreate);
        mMethodOverride(m_methods, DelayGet);
        mMethodOverride(m_methods, PointerLongWrite2nd);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtAttController);
    }

AtAttController AtAttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    self->channel = channel;

    return self;
    }

eAtRet AtAttControllerForceErrorNumErrorsSet(AtAttController self, uint32 errorType, uint32 numErrors)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorNumErrorsSet(self, errorType, numErrors);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorBitPositionSet(AtAttController self, uint32 errorType, uint32 bitPosition)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorBitPositionSet(self, errorType, bitPosition);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorBitPosition2Set(AtAttController self, uint32 errorType, uint32 bitPosition)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorBitPosition2Set(self, errorType, bitPosition);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorDataMaskSet(AtAttController self, uint32 errorType, uint32 dataMask)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorDataMaskSet(self, errorType, dataMask);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorCrcMaskSet(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorCrcMaskSet(self, errorType, crcMask);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorCrcMask2Set(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorCrcMask2Set(self, errorType, crcMask);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceError(AtAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->ForceError(self, errorType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorContinuous(AtAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorContinuous(self, errorType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorPerSecond(AtAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorPerSecond(self, errorType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorModeSet(AtAttController self, uint32 errorType, eAtAttForceErrorMode mode)
    {
    if (self)
       return mMethodsGet(self)->ForceErrorModeSet(self, errorType, mode);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorRate(AtAttController self, uint32 errorType, eAtBerRate rate)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorRate(self, errorType, rate);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorDuration(AtAttController self, uint32 errorType, uint32 duration)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorDuration(self, errorType, duration);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorStepSet(AtAttController self, uint32 errorType, uint32 step)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorStepSet(self, errorType, step);
    return cAtErrorNullPointer;
    }

uint32 AtAttControllerForceErrorStepGet(AtAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorStepGet(self, errorType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorStep2Set(AtAttController self, uint32 errorType, uint32 step)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorStep2Set(self, errorType, step);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerUnForceError(AtAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->UnForceError(self, errorType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorGet(AtAttController self, uint32 errorType, tAtAttForceErrorConfiguration *configuration)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorGet(self, errorType, configuration);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceErrorStatusGet(AtAttController self, uint32 errorType, tAtAttForceErrorStatus *status)
    {
    if (self)
        return mMethodsGet(self)->ForceErrorStatusGet(self, errorType, status);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarm(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->ForceAlarm(self, alarmType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmPerSecond(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->ForceAlarmPerSecond(self, alarmType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmContinuous(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->ForceAlarmContinuous(self, alarmType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmDurationSet(AtAttController self, uint32 alarmType, uint32 duration)
    {
    if (self)
        return mMethodsGet(self)->ForceAlarmDurationSet(self, alarmType, duration);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerRxAlarmCaptureDurationSet(AtAttController self, uint32 alarmType, uint32 duration)
    {
    if (self)
        return mMethodsGet(self)->RxAlarmCaptureDurationSet(self, alarmType, duration);
    return cAtErrorNullPointer;
    }

uint32 AtAttControllerRxAlarmCaptureDurationGet(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RxAlarmCaptureDurationGet(self, alarmType);
    return 0;
    }

eAtRet AtAttControllerRxAlarmCaptureModeSet(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode)
    {
    if (self)
        return mMethodsGet(self)->RxAlarmCaptureModeSet(self, alarmType, mode);
    return cAtErrorNullPointer;
    }

eAtAttForceAlarmMode AtAttControllerRxAlarmCaptureModeGet(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RxAlarmCaptureModeGet(self, alarmType);
    return 0;
    }

eAtRet AtAttControllerRxAlarmCaptureNumEventSet(AtAttController self, uint32 alarmType, uint32 mode)
    {
    if (self)
        return mMethodsGet(self)->RxAlarmCaptureNumEventSet(self, alarmType, mode);
    return cAtErrorNullPointer;
    }

uint32 AtAttControllerRxAlarmCaptureNumEventGet(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RxAlarmCaptureNumEventGet(self, alarmType);
    return 0;
    }

eAtRet AtAttControllerForceAlarmStepSet(AtAttController self, uint32 alarmType, uint32 step)
    {
    if (self)
        return mMethodsGet(self)->ForceAlarmStepSet(self, alarmType, step);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerUnForceAlarm(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->UnForceAlarm(self, alarmType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmConfiguration *configuration)
    {
    if (self)
        return mMethodsGet(self)->ForceAlarmGet(self, alarmType, configuration);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmStatusGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmStatus *status)
    {
    if (self)
        return mMethodsGet(self)->ForceAlarmStatusGet(self, alarmType, status);
    return cAtErrorNullPointer;
    }

AtChannel AtAttControllerChannelGet(AtAttController self)
    {
    return (self) ? self->channel : NULL;
    }

eAtRet AtAttControllerInit(AtAttController self)
    {
    if (self)
        return mMethodsGet(self)->Init(self);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerSetUp(AtAttController self)
    {
    if (self)
        return mMethodsGet(self)->SetUp(self);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmModeSet(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode)
    {
    if (self)
       return mMethodsGet(self)->ForceAlarmModeSet(self, alarmType, mode);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmNumEventSet(AtAttController self, uint32 alarmType, uint32 numEvent)
    {
    if (self)
       return mMethodsGet(self)->ForceAlarmNumEventSet(self, alarmType, numEvent);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmNumFrameSet(AtAttController self, uint32 alarmType, uint32 numFrame)
    {
    if (self)
       return mMethodsGet(self)->ForceAlarmNumFrameSet(self, alarmType, numFrame);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForceAlarmBitPositionSet(AtAttController self, uint32 alarmType, uint32 bitPosition)
    {
    if (self)
      return mMethodsGet(self)->ForceAlarmBitPositionSet(self, alarmType, bitPosition);
   return cAtErrorNullPointer;
    }
eBool AtAttControllerAlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->AlarmForceIsSupported(self, alarmType);
    return cAtFalse;
    }

eBool AtAttControllerErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->ErrorForceIsSupported(self, errorType);
    return cAtFalse;
    }

eAtRet AtAttControllerDe1ForceFbitTypeSet(AtAttController self, uint32 type, eBool en)
    {
    if (self)
        return mMethodsGet(self)->De1FbitTypeSet(self, type, en);
    return cAtErrorNullPointer;
    }

eBool AtAttControllerDe1ForceFbitTypeGet(AtAttController self, uint32 type)
    {
    if (self)
        return mMethodsGet(self)->De1FbitTypeGet(self, type);
    return cAtFalse;
    }

eAtRet AtAttControllerFrequenceOffsetSet(AtAttController self, float ppm)
    {
    if (self)
        return mMethodsGet(self)->FrequenceOffsetSet(self, ppm);
    return cAtErrorNullPointer;
    }

float AtAttControllerFrequenceOffsetGet(AtAttController self)
    {
    if (self)
        return mMethodsGet(self)->FrequenceOffsetGet(self);
    return 0;
    }

eAtRet AtAttControllerDebugError(AtAttController self, uint32 errorType)
    {
    if (self)
        return mMethodsGet(self)->DebugError(self, errorType);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerDebugAlarm(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->DebugAlarm(self, alarmType);
    return cAtErrorNullPointer;
    }
AtList AtAttControllerRecordTime(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RecordTime(self, alarmType);
    return NULL;
    }
eAtRet AtAttControllerRecordTimeAdd(AtAttController self, uint32 alarmType, const char* state)
    {
    if (self)
        return mMethodsGet(self)->RecordTimeAdd(self, alarmType, state);
    return cAtErrorNullPointer;
    }
eAtRet AtAttControllerRecordTimeClear(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RecordTimeClear(self, alarmType);
    return cAtErrorNullPointer;
    }

uint32 AtAttControllerRecordTimeNumSet(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RecordTimeNumSet(self, alarmType);
    return cAtErrorNullPointer;
    }

uint32 AtAttControllerRecordTimeNumClear(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RecordTimeNumClear(self, alarmType);
    return cAtErrorNullPointer;
    }

uint32 AtAttControllerRecordTimeDiffGet(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RecordTimeDiffGet(self, alarmType);
    return cAtErrorNullPointer;
    }

eBool AtAttControllerRecordTimeIsOkToClear(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RecordTimeIsOkToClear(self, alarmType);
    return cAtFalse;
    }

eBool AtAttControllerRecordTimeIsOk(AtAttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->RecordTimeIsOk(self, alarmType);
    return cAtFalse;
    }

AtAttWanderChannel AtAttControllerWanderGet(AtAttController self)
    {
    if (self==NULL)
        return NULL;
    if (self->wander ==NULL)
        self->wander = mMethodsGet(self)->WanderCreate(self->channel);
    return self->wander;
    }

eAtRet AtAttControllerTxAlarmsSet(AtAttController self, uint32 txAlarmTypes)
	{
	self->txAlarmTypes = self->txAlarmTypes | txAlarmTypes;
	return cAtOk;
	}

eAtRet AtAttControllerTxAlarmsClear(AtAttController self, uint32 txAlarmTypes)
	{
	self->txAlarmTypes = self->txAlarmTypes & (~txAlarmTypes);
	return cAtOk;
	}

uint32 AtAttControllerTxAlarmsGet(AtAttController self)
	{
	return self->txAlarmTypes;
	}

eAtRet AtAttControllerDe1CrcGapSet(AtAttController self, uint16 frameType)
	{
	if (self)
		 return mMethodsGet(self)->De1CrcGapSet(self, frameType);
	return cAtError;
	}

eAtRet AtAttControllerForcePointerAdjModeSet(AtAttController self, uint32 pointerType, eAtAttForcePointerAdjMode mode)
	{
	if (self)
		 return mMethodsGet(self)->ForcePointerAdjModeSet(self, pointerType, mode);
	return cAtError;
	}

eAtAttForcePointerAdjMode AtAttControllerForcePointerAdjModeGet(AtAttController self,     uint32 pointerType)
    {
    if (self)
         return mMethodsGet(self)->ForcePointerAdjModeGet(self, pointerType);
    return cAtAttForcePointerAdjModeOneshot;
    }


eAtRet AtAttControllerForcePointerAdjNumEventSet(AtAttController self, uint32 pointerType, uint32 numEvent)
	{
	if (self)
		 return mMethodsGet(self)->ForcePointerAdjNumEventSet(self, pointerType, numEvent);
	return cAtError;
	}

eAtRet AtAttControllerForcePointerAdjDurationSet(AtAttController self, uint32 pointerType, uint32 duration)
	{
	if (self)
		 return mMethodsGet(self)->ForcePointerAdjDurationSet(self, pointerType, duration);
	return cAtError;
	}

uint32 AtAttControllerForcePointerAdjDurationGet(AtAttController self, uint32 pointerType)
    {
    if (self)
         return mMethodsGet(self)->ForcePointerAdjDurationGet(self, pointerType);
    return cAtError;
    }

eAtRet AtAttControllerHwForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    if (self)
         return mMethodsGet(self)->G783ForcePointerAdj(self, pointerType);
    return cAtError;
    }

eAtRet AtAttControllerForcePointerAdj(AtAttController self, uint32 pointerType)
	{
	if (self)
		 return mMethodsGet(self)->ForcePointerAdj(self, pointerType);
	return cAtError;
	}

eAtRet AtAttControllerHwUnForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    if (self)
         return mMethodsGet(self)->HwUnForcePointerAdj(self, pointerType);
    return cAtError;
    }
eAtRet AtAttControllerUnForcePointerAdj(AtAttController self, uint32 pointerType)
	{
	if (self)
		 return mMethodsGet(self)->UnForcePointerAdj(self, pointerType);
	return cAtError;
	}

eAtRet AtAttControllerForcePointerAdjBitPositionSet(AtAttController self, uint32 pointerType, uint32 bitPosition)
    {
    if (self)
        return mMethodsGet(self)->ForcePointerAdjBitPositionSet(self, pointerType, bitPosition);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForcePointerAdjDataMaskSet(AtAttController self, uint32 pointerType, uint32 dataMask)
    {
    if (self)
        return mMethodsGet(self)->ForcePointerAdjDataMaskSet(self, pointerType, dataMask);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerForcePointerAdjStepSet(AtAttController self, uint32 pointerType, uint32 step)
    {
    if (self)
        return mMethodsGet(self)->ForcePointerAdjStepSet(self, pointerType, step);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerPointerAdjForcingHwDataMaskSet(AtAttController self, uint32 errorType, uint32 dataMask)
    {
    if (self)
        return mMethodsGet(self)->PointerAdjForcingHwDataMaskSet(self, errorType, dataMask);
    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerTxSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern)
    {
    if (self)
        return mMethodsGet(self)->TxSignalingPatternSet(self, tsId, sigMode, threshold, sigPattern);

    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerRxExpectedSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern)
    {
    if (self)
        return mMethodsGet(self)->RxExpectedSignalingPatternSet(self, tsId, sigMode, threshold, sigPattern);

    return cAtErrorNullPointer;
    }

eAtAttSignalingMode AtAttControllerTxSignalingPatternGet(AtAttController self, uint8 tsId , uint8 *sigPattern, uint8 *threshold)
    {
    if (self)
        return mMethodsGet(self)->TxSignalingPatternGet(self, tsId, sigPattern, threshold);

    return cAtAttSignalingModeInvalid;
    }

eAtAttSignalingMode AtAttControllerRxExpectedSignalingPatternGet(AtAttController self, uint8 tsId , uint8 *sigPattern, uint8 *threshold)
    {
    if (self)
        return mMethodsGet(self)->RxExpectedSignalingPatternGet(self, tsId, sigPattern, threshold);

    return cAtAttSignalingModeInvalid;
    }

eAtRet AtAttControllerTxSignalingEnable(AtAttController self, uint8 tsId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TxSignalingEnable(self, tsId, enable);

    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerRxSignalingEnable(AtAttController self, uint8 tsId, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->RxSignalingEnable(self, tsId, enable);

    return cAtErrorNullPointer;
    }

eBool AtAttControllerTxSignalingIsEnabled(AtAttController self, uint8 tsId)
    {
    if (self)
        return mMethodsGet(self)->TxSignalingIsEnabled(self, tsId);

    return cAtFalse;
    }

eBool AtAttControllerRxSignalingIsEnabled(AtAttController self, uint8 tsId)
    {
    if (self)
        return mMethodsGet(self)->RxSignalingIsEnabled(self, tsId);

    return cAtFalse;
    }

eAtRet AtAttControllerTxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    if (self)
        return mMethodsGet(self)->TxSignalingDump(self, tsId, value, length);

    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerRxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    if (self)
        return mMethodsGet(self)->RxSignalingDump(self, tsId, value, length);

    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerTxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    if (self)
        return mMethodsGet(self)->TxSignalingDumpDebug(self, tsId, value, length);

    return cAtErrorNullPointer;
    }

eAtRet AtAttControllerRxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    if (self)
        return mMethodsGet(self)->RxSignalingDumpDebug(self, tsId, value, length);

    return cAtErrorNullPointer;
    }

eBool AtAttControllerSignalingLopAlarmGet(AtAttController self, uint8 tsId)
    {
    if (self)
        return mMethodsGet(self)->SignalingLopAlarmGet(self, tsId);

    return cAtFalse;
    }

uint32 AtAttControllerSignalingLopStickyGet(AtAttController self, uint8 tsId, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->SignalingLopStickyGet(self, tsId, r2c);

    return cAtFalse;
    }

eBool AtAttControllerSignalingFramerStickyGet(AtAttController self, uint8 tsId, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->SignalingFramerStickyGet(self, tsId, r2c);

    return cAtFalse;
    }

uint32 AtAttControllerBipCounterGet(AtAttController self, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->BipCounterGet(self, r2c);

    return cAtFalse;
    }

/**
 * @param self this AtAttController
 * @param force cAtTrue if it is forced or cAtFalse if it set normal or init
 * @return At return code
 *
 */
eAtRet AtAttControllerPointerAdjEnable(AtAttController self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PointerAdjEnable(self, enable);
    return cAtErrorModeNotSupport;
    }

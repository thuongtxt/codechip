/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : AtAttExpect.c
 *
 * Created Date: May 16, 2016
 *
 * Description : ATT controller abstract
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../common/AtChannelInternal.h"
#include "../../../include/prbs/AtModulePrbs.h"
#include "AtAttControllerInternal.h"
#include "AtAttExpectInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtAttExpectChannel)self)
#define cMaxTimeString 250

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtAttExpectChannelMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet Xxx(AtAttExpectChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void DeleteChannel(AtAttExpectChannel self)
    {
    mThis(self)->channel = NULL;
    }

static void DeleteTimeList(AtAttExpectChannel self)
    {
    if (mThis(self)->timeList != NULL)
        {
        while (AtListLengthGet(mThis(self)->timeList))
            {
            tAtAttExpectChannelTime*  stime=NULL;
            stime = (tAtAttExpectChannelTime*)AtListObjectRemoveAtIndex(mThis(self)->timeList, 0);
            AtOsalMemFree(stime);
            }
        AtObjectDelete((AtObject)mThis(self)->timeList);
        }
    mThis(self)->timeList = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteChannel((AtAttExpectChannel)self);
    DeleteTimeList((AtAttExpectChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtObject self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtAttExpectChannel self)
    {
    OverrideAtObject((AtObject)self);
    }

static void MethodsInit(AtAttExpectChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, Xxx);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtAttExpectChannel);
    }

static AtAttExpectChannel AtAttExpectChannelObjectInit(AtAttExpectChannel self, AtChannel channel, const char* alarmString)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;
    self->channel = channel;
    AtStrcpy(self->alarmString, alarmString);
    AtAttExpectChannelInit((AtAttExpectChannel) self);
    return self;
    }

AtAttExpectChannel AtAttExpectChannelNew(AtChannel channel, const char* alarmString)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttExpectChannel expectChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (expectChannel == NULL)
        return NULL;

    /* Construct it */
    return AtAttExpectChannelObjectInit(expectChannel, channel, alarmString);
    }

eAtRet AtAttExpectChannelInit(AtAttExpectChannel self)
    {
    self->timeList = NULL;
    self->alarmRxExpectDuration = 0;
    self->setCount              = 0;
    mThis(self)->alarmRxExpectMode= cAtAttForceAlarmModeContinuous;
    mThis(self)->alarmRxExpectDuration = 1;
    mThis(self)->alarmRxExpectNumEvent = 2;
    mThis(self)->debug = cAtFalse;
    return cAtOk;
    }

eAtRet AtAttExpectChannelDurationSet(AtAttExpectChannel self, uint32 duration)
    {
    self->alarmRxExpectDuration = duration;
    return cAtOk;
    }

uint32 AtAttExpectChannelDurationGet(AtAttExpectChannel self)
    {
    return self->alarmRxExpectDuration;
    }

eAtRet AtAttExpectChannelModeSet(AtAttExpectChannel self, eAtAttForceAlarmMode mode)
    {
    self->alarmRxExpectMode = mode;
    return cAtOk;
    }

eAtAttForceAlarmMode AtAttExpectChannelModeGet(AtAttExpectChannel self)
    {
    return self->alarmRxExpectMode;
    }

eAtRet AtAttExpectChannelNumEventSet(AtAttExpectChannel self, uint32 numEvent)
    {
    self->alarmRxExpectNumEvent = numEvent;
    return cAtOk;
    }

uint32 AtAttExpectChannelNumEventGet(AtAttExpectChannel self)
    {
    return self->alarmRxExpectNumEvent;
    }



eAtRet AtAttExpectChannelTypeSet(AtAttExpectChannel self, uint32 type)
    {
    self->channelType = type;
    return cAtOk;
    }

uint32 AtAttExpectChannelTypeGet(AtAttExpectChannel self)
    {
    return self->channelType;
    }

eAtRet AtAttExpectChannelAlarmCountSet(AtAttExpectChannel self, uint32 count)
    {
    self->alarmRxExpectCount = count;
    return cAtOk;
    }

uint32 AtAttExpectChannelAlarmCountGet(AtAttExpectChannel self)
    {
    return self->alarmRxExpectCount;
    }

static uint32 TimeMeasurementErrorGet(AtAttExpectChannel self)
    {
    AtChannel channel = mThis(self)->channel;
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePrbs module = (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    return AtModulePrbsTimeMeasurementErrorGet(module);
    }

eBool  AtAttExpectChannelIsOk(AtAttExpectChannel self)
    {
    AtList list = AtAttExpectChannelTimeList(mThis(self));
    uint32 length = AtListLengthGet(list);
    uint32 error =TimeMeasurementErrorGet(mThis(self));

    if (mThis(self)->alarmRxExpectMode ==cAtAttForceAlarmModeContinuous)
        {
        if (mThis(self)->clearCount != 1)
            {
            if (mThis(self)->debug)
                AtPrintf("SetInT Invalid clearCount =%d != 1\n", mThis(self)->clearCount);
            return cAtFalse;
            }

        if (mThis(self)->setCount != 1)
            {
            if (mThis(self)->debug)
                AtPrintf("SetInT Invalid setCount = %d != 1\n",mThis(self)->setCount);
            return cAtFalse;
            }

        if (length !=2)
            {
            if (mThis(self)->debug)
                AtPrintf("SetInT Invalid length=%d !=2\n", length);
            return cAtFalse;
            }
        }

    if (mThis(self)->alarmRxExpectMode ==cAtAttForceAlarmModeSetInT) /* set_in_t */
        {
        uint32 max = self->alarmRxExpectDuration*1000 + error;
        uint32 min = self->alarmRxExpectDuration*1000 - error;
        if (mThis(self)->debug)
            AtPrintf("max:%d min:%d diff:%d\n", max, min, self->diff);
        if (mThis(self)->clearCount != 1)
            {
            if (mThis(self)->debug)
                AtPrintf("SetInT Invalid clearCount =%d != 1\n", mThis(self)->clearCount);
            return cAtFalse;
            }
        if (mThis(self)->setCount != 1)
            {
            if (mThis(self)->debug)
                AtPrintf("SetInT Invalid setCount = %d != 1\n",mThis(self)->setCount);
            return cAtFalse;
            }
        if (length !=2)
            {
            if (mThis(self)->debug)
                AtPrintf("SetInT Invalid length=%d !=2\n", length);
            return cAtFalse;
            }
        if (self->diff < min)
            {
            if (mThis(self)->debug)
                AtPrintf("SetInT Invalid diff=%d < %d\n", self->diff, min);
            return cAtFalse;
            }
        if (self->diff > max)
            {
            AtPrintf("SetInT Invalid diff=%d > %d\n", self->diff, max);
            return cAtFalse;
            }
        }

    if (mThis(self)->alarmRxExpectMode ==cAtAttForceAlarmModeNeventInT) /* set_in_t */
        {
        uint8 i=8;
        if (mThis(self)->clearCount != (mThis(self)->alarmRxExpectNumEvent * mThis(self)->alarmRxExpectDuration))
            {
            if (mThis(self)->debug)
                AtPrintf("NeventInT Invalid clearCount =%d != %d \n", mThis(self)->clearCount, mThis(self)->alarmRxExpectNumEvent * mThis(self)->alarmRxExpectDuration);
            return cAtFalse;
            }
        if (mThis(self)->setCount != (mThis(self)->alarmRxExpectNumEvent * mThis(self)->alarmRxExpectDuration))
            {
            if (mThis(self)->debug)
                AtPrintf("NeventInT Invalid setCount=%d != %d\n", mThis(self)->setCount, mThis(self)->alarmRxExpectNumEvent * mThis(self)->alarmRxExpectDuration);
            return cAtFalse;
            }
        if (length !=2 *mThis(self)->alarmRxExpectNumEvent * mThis(self)->alarmRxExpectDuration)
            {
            if (mThis(self)->debug)
                AtPrintf("NeventInT Invalid recorded events =%d != %d\n",length, 2 *mThis(self)->alarmRxExpectNumEvent * mThis(self)->alarmRxExpectDuration);
            return cAtFalse;
            }

        for (i=0; i<mThis(self)->alarmRxExpectNumEvent * mThis(self)->alarmRxExpectDuration; i++)
            {
            uint32 diff =0 , d_max=0, d_min=0;
            tAtAttExpectChannelTime *s_time=NULL, *c_time=NULL;
            s_time = (tAtAttExpectChannelTime *)AtListObjectGet(mThis(self)->timeList, (uint32)(i*2));
            c_time = (tAtAttExpectChannelTime *)AtListObjectGet(mThis(self)->timeList, (uint32)(i*2 + 1));
            diff = AtOsalDifferenceTimeInMs(&c_time->time, &s_time->time);
            d_max = 64 + 5;
            d_min = 64 - 5;
            if ( diff< d_min)
                {
                if (mThis(self)->debug)
                    AtPrintf("Invalid 64ms between set and clear \n");
                return cAtFalse;
                }
            if (diff > d_max)
                {
                if (mThis(self)->debug)
                    AtPrintf("Invalid 64ms between set and clear \n");
                return cAtFalse;
                }
            }
        }

    return cAtTrue;
    }

eBool  AtAttExpectChannelIsOkToClear(AtAttExpectChannel self)
    {
    AtList list = AtAttExpectChannelTimeList(mThis(self));
    uint32 length = AtListLengthGet(list);
    if (length != 0) {
        uint32 diff =0;
        AtOsal osal = AtSharedDriverOsalGet();
        tAtAttExpectChannelTime *e_time=NULL, *e_first=NULL;
        e_time = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtAttExpectChannelTime));
        mMethodsGet(osal)->MemInit(osal, e_time, 0, sizeof(tAtAttExpectChannelTime));
        AtOsalCurTimeGet(&e_time->time);
        e_first = (tAtAttExpectChannelTime*)AtListObjectGet(list, 0);
        diff = AtOsalDifferenceTimeInMs(&e_time->time, &e_first->time);
        mMethodsGet(osal)->MemFree(osal, e_time);
        if (diff > self->alarmRxExpectDuration*1000)
            return cAtTrue;
        return cAtFalse;
    }
    return cAtTrue;
    }

AtList AtAttExpectChannelTimeList(AtAttExpectChannel self)
    {
    if (mThis(self)->timeList==NULL)
         mThis(self)->timeList = AtListCreate(0);
    return self->timeList;
    }

static void CountEventSet(AtAttExpectChannel self, const char* state)
    {
    if (AtStrcmp(state, "set")==0)
         mThis(self)->setCount++;
    }

static void CountEventClear(AtAttExpectChannel self, const char* state)
    {
    if (AtStrcmp(state, "clear")==0)
         mThis(self)->clearCount++;
    }

static void AddTimeToList(AtAttExpectChannel self, tAtAttExpectChannelTime* e_time)
    {
    AtList list = AtAttExpectChannelTimeList(mThis(self));
    uint32 length = AtListLengthGet(list);
    if (length==0)
         self->diff = 0;
    else
        {
        tAtAttExpectChannelTime* first = (tAtAttExpectChannelTime*)AtListObjectGet(list, 0);
        self->diff = AtOsalDifferenceTimeInMs(&e_time->time, &first->time);
        }
    AtListObjectAdd(list, (AtObject)e_time);
    }

eAtRet AtAttExpectChannelRecordTimeAdd(AtAttExpectChannel self, const char* state)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    tAtAttExpectChannelTime* e_time=NULL;
    e_time = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtAttExpectChannelTime));
    mMethodsGet(osal)->MemInit(osal, e_time, 0, sizeof(tAtAttExpectChannelTime));
    AtOsalCurTimeGet(&e_time->time);
    if (AtStrcmp(state, "set")==0)
        e_time->set =cAtTrue;
    else
        e_time->set =cAtFalse;
    CountEventSet(self, state);
    CountEventClear(self, state);
    AddTimeToList(self,e_time);
    AtPrintf("-RecordTimeAdd %s %s %s->s%d c%d\n",AtObjectToString((AtObject)self->channel), self->alarmString, state, mThis(self)->setCount, mThis(self)->clearCount);
    return cAtOk;
    }

eAtRet AtAttExpectChannelRecordTimeClear(AtAttExpectChannel self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if (mThis(self)->timeList != NULL)
       {
       while (AtListLengthGet(mThis(self)->timeList))
           {
           tAtAttExpectChannelTime*  e_time=NULL;
           e_time = (tAtAttExpectChannelTime*)AtListObjectRemoveAtIndex(mThis(self)->timeList, 0);
           mMethodsGet(osal)->MemFree(osal,e_time);
           }
       AtObjectDelete((AtObject)mThis(self)->timeList);
       mThis(self)->timeList = NULL;
       mThis(self)->setCount = 0;
       mThis(self)->clearCount = 0;
       mThis(self)->diff       = 0;
       }
    return cAtOk;
    }

uint32 AtAttExpectChannelRecordTimeNumSet(AtAttExpectChannel self)
    {
    if (self)
        return mThis(self)->setCount;
    return 0;
    }

uint32 AtAttExpectChannelRecordTimeNumClear(AtAttExpectChannel self)
    {
    if (self)
        return mThis(self)->clearCount;
    return 0;
    }

uint32 AtAttExpectChannelRecordTimeDiffGet(AtAttExpectChannel self)
    {
    if (self)
        return mThis(self)->diff;
    return 0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaAttPdhManagerInternal.h
 * 
 * Author      : chaudpt
 *
 * Created Date: July 26, 2017
 *
 * Description : PDH module class descriptor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTPRBSMANAGERINTERNAL_H_
#define _ATATTPRBSMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../generic/common/AtObjectInternal.h"
#include "../../../include/att/AtAttPrbsManager.h"
#include "../../generic/prbs/AtModulePrbsInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAttPrbsManagerMethods
    {
    eAtRet   (*Init)(AtAttPrbsManager self);
    eAtRet (*ForceLogicSet)(AtAttPrbsManager self, eAttForceLogic forceLogic);
    eAttForceLogic (*ForceLogicGet)(AtAttPrbsManager self);
    eAtRet (*UseComplexIdSet)(AtAttPrbsManager self, eBool en);
    eBool  (*UseComplexIdGet)(AtAttPrbsManager self);

    eAtRet (*TimeMeasurementErrorSet)(AtAttPrbsManager self, uint32 error);
    uint32 (*TimeMeasurementErrorGet)(AtAttPrbsManager self);
    eAtRet (*TieReset)(AtAttPrbsManager self);
    eAtRet (*TieBufRead)(AtAttPrbsManager self);
    eBool (*DelayIsSupported)(AtAttPrbsManager self);
    }tAtAttPrbsManagerMethods;

typedef struct tAtAttPrbsManager
    {
    tAtObject super;
    const tAtAttPrbsManagerMethods *methods;

    /* Private data */
    AtModulePrbs prbs;
    eAttForceLogic forceLogic;
    eBool UseComplexId;
    }tAtAttPrbsManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttPrbsManager AtAttPrbsManagerObjectInit(AtAttPrbsManager self, AtModulePrbs prbs);
AtModulePrbs AtAttPrbsManagerModulePrbs(AtAttPrbsManager self);
#ifdef __cplusplus
}
#endif
#endif /* _ATATTPRBSMANAGERINTERNAL_H_ */


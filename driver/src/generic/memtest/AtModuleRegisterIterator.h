/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Memory test
 * 
 * File        : AtModuleRegisterIterator.h
 * 
 * Created Date: Jan 7, 2013
 *
 * Description : Module register iterator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEREGISTERITERATOR_H_
#define _ATMODULEREGISTERITERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIterator.h" /* Super class */
#include "AtRegister.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleRegisterIterator * AtModuleRegisterIterator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRegister AtModuleRegisterIteratorCurrentRegister(AtModuleRegisterIterator self);
AtRegister AtModuleRegisterIteratorCurrentRegisterUpdate(AtModuleRegisterIterator self,
                                                         uint8 coreId,
                                                         uint32 address,
                                                         uint32 noneEditableFieldsMask);

uint32 AtModuleRegisterIteratorCurrentBaseAddressGet(AtModuleRegisterIterator self);
void AtModuleRegisterIteratorCurrentBaseAddressSet(AtModuleRegisterIterator self, uint32 baseAddress);

AtModule AtModuleRegisterIteratorModuleGet(AtModuleRegisterIterator self);

uint32 AtModuleRegisterIteratorCurrentChannel(AtModuleRegisterIterator self);
#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEREGISTERITERATOR_H_ */


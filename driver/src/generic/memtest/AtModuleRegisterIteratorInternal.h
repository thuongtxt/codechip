/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Memory test
 * 
 * File        : AtModuleRegisterIteratorInternal.h
 * 
 * Created Date: Jan 7, 2013
 *
 * Description : Module register iterator class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEREGISTERITERATORINTERNAL_H_
#define _ATMODULEREGISTERITERATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleRegisterIterator.h"
#include "AtRegister.h"

#include "../../util/AtIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mMakeReg(iterator, coreId, address, noneEditableFieldsMask)            \
    AtModuleRegisterIteratorCurrentRegisterUpdate((AtModuleRegisterIterator)iterator, coreId, address, noneEditableFieldsMask)

#define mNextReg(iterator, currentReg, nextReg)                                \
    do                                                                         \
        {                                                                      \
        if (!AtModuleRegisterIteratorRegisterAddressIsValid(iterator, nextReg))\
            return NULL;                                                       \
                                                                               \
        if (AtModuleRegisterIteratorCurrentBaseAddressGet((AtModuleRegisterIterator)iterator) == currentReg) \
			{                                                                 \
			AtModuleRegisterIteratorCurrentBaseAddressSet(iterator, nextReg); \
			return (AtObject)mMakeReg(iterator, mMethodsGet(iterator)->DefaultCore(iterator), nextReg, nextReg##NoneEditableFieldsMask); \
			}                                                              \
        }while(0)

#define mNextChannelRegister(regIterator, currentRegister, nextRegister)       \
    do                                                                         \
        {                                                                      \
        if (AtModuleRegisterIteratorCurrentBaseAddressGet(regIterator) == (uint32)currentRegister)\
            {                                                                  \
            if (mMethodsGet(regIterator)->NoChannelLeft(regIterator))          \
                {                                                              \
				if (!AtModuleRegisterIteratorRegisterAddressIsValid(iterator, (nextRegister)))\
					return NULL;                                                 \
                                                                               \
                AtModuleRegisterIteratorCurrentBaseAddressSet(regIterator, (nextRegister));\
                mMethodsGet(regIterator)->ResetChannel(regIterator);           \
                return (AtObject)mMakeReg(regIterator,                         \
                                          mMethodsGet(regIterator)->DefaultCore(regIterator), \
                                          (nextRegister),                        \
                                          nextRegister##NoneEditableFieldsMask); \
                }                                                              \
                                                                               \
            /* Next register of next channel */                                \
            return (AtObject)mMakeReg(regIterator,                             \
                                      mMethodsGet(regIterator)->DefaultCore(regIterator),\
                                      currentRegister + mMethodsGet(regIterator)->NextOffset(regIterator), \
                                      currentRegister##NoneEditableFieldsMask);\
            }                                                                  \
        }while(0)

#define cAtModuleRegisterIteratorEnd 0xFFFFFF
#define cAtModuleRegisterIteratorEndNoneEditableFieldsMask 0

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleRegisterIteratorMethods
    {
    uint8 (*DefaultCore)(AtModuleRegisterIterator self);
    uint32 (*MaxNumChannels)(AtModuleRegisterIterator self);
    uint32 (*NextOffset)(AtModuleRegisterIterator self);
    eBool (*NoChannelLeft)(AtModuleRegisterIterator self);
    void (*ResetChannel)(AtModuleRegisterIterator self);
    uint32 (*CurrentChannel)(AtModuleRegisterIterator self);
    }tAtModuleRegisterIteratorMethods;

typedef struct tAtModuleRegisterIterator
    {
    tAtIterator super;
    const tAtModuleRegisterIteratorMethods *methods;

    /* Private data */
    AtModule module;
    uint32 currentBaseAddress;
    AtRegister currentRegister;
    uint32 currentChannelId;
    }tAtModuleRegisterIterator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtIterator AtModuleXxxRegisterIteratorObjectInit(AtIterator self, AtModule module);
AtIterator AtModuleXxxRegisterIteratorCreate(AtModule module);
AtModuleRegisterIterator AtModuleRegisterIteratorObjectInit(AtModuleRegisterIterator self, AtModule module);
eBool AtModuleRegisterIteratorRegisterAddressIsValid(AtModuleRegisterIterator self, uint32 regAddr);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEREGISTERITERATORINTERNAL_H_ */


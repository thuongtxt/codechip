/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Memory test
 *
 * File        : AtModuleRegisterIterator.c
 *
 * Created Date: Jan 7, 2013
 *
 * Description : Module register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleRegisterIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleRegisterIteratorMethods m_methods;

static tAtObjectMethods m_AtObjectOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 DefaultCore(AtModuleRegisterIterator self)
    {
	AtUnused(self);
    return 0;
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
	AtUnused(self);
    /* Let the sub class do */
    return 0;
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    self->currentChannelId = self->currentChannelId + 1;
    return self->currentChannelId;
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    return (mMethodsGet(self)->CurrentChannel(self) >= (mMethodsGet(self)->MaxNumChannels(self)) - 1) ? cAtTrue : cAtFalse;
    }

static uint32 CurrentChannel(AtModuleRegisterIterator self)
    {
    return self->currentChannelId;
    }

static void ResetChannel(AtModuleRegisterIterator self)
    {
    self->currentChannelId = 0;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleRegisterIterator);
    }

static void Delete(AtObject self)
    {
    AtModuleRegisterIterator registerIterator = (AtModuleRegisterIterator)self;

    /* Delete register object */
    AtObjectDelete((AtObject)(registerIterator->currentRegister));
    registerIterator->currentRegister = NULL;

    /* Let the super do the whole other thing */
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtModuleRegisterIterator self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleRegisterIterator self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtModuleRegisterIterator self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxNumChannels);
        mMethodOverride(m_methods, NextOffset);
        mMethodOverride(m_methods, NoChannelLeft);
        mMethodOverride(m_methods, ResetChannel);
        mMethodOverride(m_methods, DefaultCore);
        mMethodOverride(m_methods, CurrentChannel);
        }

    mMethodsSet(self, &m_methods);
    }

static AtRegister CurrentRegister(AtModuleRegisterIterator self)
    {
    if (self->currentRegister == NULL)
        self->currentRegister = AtRegisterNew(0, 0, 0);

    return self->currentRegister;
    }

AtRegister AtModuleRegisterIteratorCurrentRegisterUpdate(AtModuleRegisterIterator self,
                                                         uint8 coreId,
                                                         uint32 address,
                                                         uint32 noneEditableFieldsMask)
    {
    AtRegister reg = CurrentRegister(self);
    AtRegisterCoreIdSet(reg, coreId);
    AtRegisterAddressSet(reg, address);
    AtRegisterNoneEditableFieldsMaskSet(reg, noneEditableFieldsMask);

    return reg;
    }

uint32 AtModuleRegisterIteratorCurrentBaseAddressGet(AtModuleRegisterIterator self)
    {
    return self->currentBaseAddress;
    }

void AtModuleRegisterIteratorCurrentBaseAddressSet(AtModuleRegisterIterator self, uint32 baseAddress)
    {
    self->currentBaseAddress = baseAddress;
    }

AtModule AtModuleRegisterIteratorModuleGet(AtModuleRegisterIterator self)
    {
    return self->module;
    }

AtModuleRegisterIterator AtModuleRegisterIteratorObjectInit(AtModuleRegisterIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtIteratorObjectInit((AtIterator)self) == NULL)
        return NULL;

    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Setup private data */
    self->module = module;

    return self;
    }

eBool AtModuleRegisterIteratorRegisterAddressIsValid(AtModuleRegisterIterator self, uint32 regAddr)
	{
    AtUnused(self);
	return (regAddr != cAtModuleRegisterIteratorEnd) ? cAtTrue : cAtFalse;
	}

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtRegister.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : To encapsulate an register
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"
#include "../common/AtObjectInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mRegisterIsValid(self) ((self) ? cAtTrue : cAtFalse)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtRegister
    {
    tAtObject super;

    /* Private data */
    uint32 address;
    uint32 noneEditableFieldsMask;
    uint8 coreId; /* The core that register belong to */
    }tAtRegister;


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtObjectMethods m_AtObjectOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtRegister);
    }

static AtObject Clone(AtObject self)
    {
    AtRegister reg = (AtRegister)self;
    return (AtObject)AtRegisterNew(AtRegisterCoreId(reg),
                                   AtRegisterAddress(reg),
                                   AtRegisterNoneEditableFieldsMask(reg));
    }

static void Override(AtRegister self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsalMemCpy(&m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Clone);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static AtRegister ObjectInit(AtRegister self,
                             uint8 coreId,
                             uint32 address,
                             uint32 noneEditableFieldsMask)
    {
    AtOsalMemInit(self, 0, ObjectSize());
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    /* Initialize private data */
    self->coreId  = coreId;
    self->address = address;
    self->noneEditableFieldsMask = noneEditableFieldsMask;

    return self;
    }

/* Create new register */
AtRegister AtRegisterNew(uint8 coreId, uint32 address, uint32 noneEditableFieldsMask)
    {
    /* Allocate memory */
    AtRegister newRegister = AtOsalMemAlloc(ObjectSize());
    if (newRegister == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newRegister, coreId, address, noneEditableFieldsMask);
    }

uint8 AtRegisterCoreId(AtRegister self)
    {
    if (mRegisterIsValid(self))
        return self->coreId;

    return 0;
    }

void AtRegisterCoreIdSet(AtRegister self, uint8 coreId)
    {
    if (mRegisterIsValid(self))
        self->coreId = coreId;
    }

uint32 AtRegisterAddress(AtRegister self)
    {
    if (mRegisterIsValid(self))
        return self->address;

    return 0;
    }

void AtRegisterAddressSet(AtRegister self, uint32 address)
    {
    if (mRegisterIsValid(self))
        self->address = address;
    }

uint32 AtRegisterRead(AtRegister self, AtHal byHal)
    {
    uint32 readValue;

    if (!mRegisterIsValid(self))
        return 0;

    readValue = AtHalRead(byHal, AtRegisterAddress(self));

    return readValue & (~AtRegisterNoneEditableFieldsMask(self));
    }

uint32 AtRegisterWrite(AtRegister self, uint32 value, AtHal byHal)
    {
    uint32 writtenValue = value & (~AtRegisterNoneEditableFieldsMask(self));

    if (!mRegisterIsValid(self))
        return 0;

    AtHalWrite(byHal, AtRegisterAddress(self), writtenValue);

    return writtenValue;
    }

uint32 AtRegisterNoneEditableFieldsMask(AtRegister self)
    {
    if (mRegisterIsValid(self))
        return self->noneEditableFieldsMask;

    return 0;
    }

void AtRegisterNoneEditableFieldsMaskSet(AtRegister self, uint32 noneEditableFieldsMask)
    {
    if (mRegisterIsValid(self))
        self->noneEditableFieldsMask = noneEditableFieldsMask;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : AtModuleMemTest.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : Module memory testing
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtDevice.h"
#include "AtModule.h"
#include "AtIterator.h"
#include "AtRegister.h"

#include "../man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHal HalOfRegister(AtModule self, AtRegister reg)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return AtDeviceIpCoreHalGet(device, AtRegisterCoreId(reg));
    }

static eAtRet DataBusTestForRegister(AtModule self, AtRegister reg)
    {
    uint32 pattern, writtenPattern, actualValue;
    AtHal hal = HalOfRegister(self, reg);

    /* Perform a walking 1's test at the given address. */
    for (pattern = 1; pattern != 0; pattern <<= 1)
        {
        /* Write the test pattern. */
        writtenPattern = pattern & (~AtRegisterNoneEditableFieldsMask(reg));
        AtRegisterWrite(reg, pattern, hal);

        /* Read it back (immediately is okay for this test). */
        actualValue = AtRegisterRead(reg, hal);
        if (actualValue != writtenPattern)
            {
            AtPrintc(cSevCritical,
                     "(%s, %d) ERROR: address 0x%08x, write 0x%08x, but read 0x%08x\n",
                     __FILE__,
                     __LINE__,
                     AtRegisterAddress(reg),
                     writtenPattern,
                     actualValue);

            return cAtErrorMemoryTestFail;
            }
        }

    return cAtOk;
    }

static eAtRet DataBusTest(AtModule self)
    {
    eAtRet ret = cAtOk;
    AtIterator registerIterator;
    AtRegister reg;

    /* If one register is fail, exit right away */
    registerIterator = AtModuleRegisterIteratorCreate(self);
    while ((reg = (AtRegister)AtIteratorNext(registerIterator)) != NULL)
        {
        ret = DataBusTestForRegister(self, reg);
        if (ret != cAtOk)
            break;
        }

    AtObjectDelete((AtObject)registerIterator);

    return ret;
    }

static uint32 DefaultPattern(void)
    {
    return 0xAAAAAAAA;
    }

static uint32 AntiPattern(void)
    {
    return 0x55555555;
    }

static AtRegister RegisterAtOffset(AtIterator registerIterator, uint32 currentOffset, uint32 offset)
    {
    uint32 i;
    AtRegister reg;

    reg = NULL;
    for (i = currentOffset; i <= offset; i++)
        {
        reg = (AtRegister)AtIteratorNext(registerIterator);
        if (reg == NULL)
            break;
        }

    return reg;
    }

static void WriteDefaultPattern(AtModule self)
    {
    AtIterator registerIterator = AtModuleRegisterIteratorCreate(self);
    uint32 offset = 1, currentOffset = 0;
    AtRegister reg;

    while ((reg = RegisterAtOffset(registerIterator, currentOffset, offset)) != NULL)
        {
        AtRegisterWrite(reg, DefaultPattern(), HalOfRegister(self, reg));

        /* Next power-of-two offset */
        currentOffset = offset;
        offset = offset << 1;
        }

    AtObjectDelete((AtObject)registerIterator);
    }

static eBool RegisterIsAffected(AtRegister reg, uint32 expectedValue, AtHal byHal)
    {
    uint32 actualValue;

    expectedValue = expectedValue & (~AtRegisterNoneEditableFieldsMask(reg));
    actualValue = AtRegisterRead(reg, byHal);
    if (actualValue != expectedValue)
        {
        AtPrintc(cSevCritical,
                 "(%s, %d) ERROR: Address 0x%08x is affected, expected 0x%08x, but got 0x%08x\n",
                 __FILE__,
                 __LINE__,
                 AtRegisterAddress(reg),
                 expectedValue,
                 actualValue);
        return cAtTrue;
        }

    return cAtFalse;
    }

static eBool AddressBitsStuckHigh(AtModule self)
    {
    uint32 offset, currentOffset;
    AtIterator registerIterator = AtModuleRegisterIteratorCreate(self);
    AtRegister firstReg, reg;
    eBool stuckHigh = cAtFalse;

    /* Just write one register with anti-pattern */
    firstReg = (AtRegister)AtObjectClone(AtIteratorNext(registerIterator));
    AtRegisterWrite(firstReg, AntiPattern(), HalOfRegister(self, firstReg));

    /* Then check if there is any register stuck high */
    currentOffset = 1;
    offset = 1;
    while ((reg = RegisterAtOffset(registerIterator, currentOffset, offset)) != NULL)
        {
        /* Check if registers are affected */
        if (RegisterIsAffected(reg, DefaultPattern(), HalOfRegister(self, reg)))
            {
            stuckHigh = cAtTrue;
            break;
            }

        /* Next power-of-two offset */
        currentOffset = offset;
        offset = offset << 1;
        }

    /* Recover the previous state of first register */
    AtRegisterWrite(firstReg, DefaultPattern(), HalOfRegister(self, firstReg));

    /* Release when done */
    AtObjectDelete((AtObject)registerIterator);
    AtObjectDelete((AtObject)firstReg);

    return stuckHigh;
    }

static eBool AddressBitsStuckLow(AtModule self)
    {
    uint32 testOffset = 1, testCurrentOffset;
    uint32 offset     = 1, currentOffset;
    AtIterator registerIterator = AtModuleRegisterIteratorCreate(self);
    AtIterator testRegisterIterator = AtModuleRegisterIteratorCreate(self);
    AtRegister testReg, reg, firstReg;
    eBool stuckLow = cAtFalse;

    currentOffset     = 0;
    firstReg          = (AtRegister)AtObjectClone(AtIteratorNext(testRegisterIterator));
    testCurrentOffset = 1;

    while ((testReg = RegisterAtOffset(testRegisterIterator, testCurrentOffset, testOffset)) != NULL)
        {
        /* Write first */
        AtRegisterWrite(testReg, AntiPattern(), HalOfRegister(self, testReg));

        /* Check the first register */
        if (RegisterIsAffected(firstReg, DefaultPattern(), HalOfRegister(self, firstReg)))
            {
            stuckLow = cAtTrue;
            break;
            }

        /* Then check if other register affect */
        while ((reg = RegisterAtOffset(registerIterator, currentOffset, offset)) != NULL)
            {
            /* Check if other register is affected */
            if ((offset != testOffset) &&
                (RegisterIsAffected(reg, DefaultPattern(), HalOfRegister(self, reg))))
                {
                stuckLow = cAtTrue;
                break;
                }

            /* Next power-of-two offset */
            currentOffset = offset;
            offset        = offset << 1;
            }

        /* Exit on stuck */
        if (stuckLow)
            break;

        /* Revert the previous state of test register that has just been written */
        AtRegisterWrite(testReg, DefaultPattern(), HalOfRegister(self, testReg));

        /* Next test offset */
        testCurrentOffset = testOffset;
        testOffset        = testOffset << 1;
        }

    /* Release resource when done */
    AtObjectDelete((AtObject)registerIterator);
    AtObjectDelete((AtObject)testRegisterIterator);
    AtObjectDelete((AtObject)firstReg);

    return stuckLow;
    }

static eAtRet AddressBusTest(AtModule self)
    {
    WriteDefaultPattern(self);

    if (AddressBitsStuckHigh(self) ||
        AddressBitsStuckLow(self))
        return cAtErrorMemoryTestFail;

    return cAtOk;
    }

static eAtRet FillMemoryWithKnownPattern(AtModule self, uint32 startPattern)
    {
    AtIterator registerIterator = AtModuleRegisterIteratorCreate(self);
    AtRegister reg;
    eAtRet ret = cAtOk;

    while ((reg = (AtRegister)AtIteratorNext(registerIterator)) != NULL)
        {
        uint32 expectedValue, actualValue;

        /* Write this pattern and make sure that it is writtern properly */
        AtRegisterWrite(reg, startPattern, HalOfRegister(self, reg));

        expectedValue = startPattern & (~AtRegisterNoneEditableFieldsMask(reg));
        actualValue = AtRegisterRead(reg, HalOfRegister(self, reg));
        if (actualValue != expectedValue)
            {
            AtPrintc(cSevCritical,
                     "(%s, %d) ERROR: Address 0x%08x, written 0x%08x, but read 0x%08x\n",
                     __FILE__,
                     __LINE__,
                     AtRegisterAddress(reg),
                     expectedValue,
                     actualValue);
            ret = cAtErrorMemoryTestFail;
            break;
            }

        /* Next pattern */
        startPattern = startPattern + 1;
        }

    AtObjectDelete((AtObject)registerIterator);

    return ret;
    }

static eBool MemoryIsFilledWithKnownPattern(AtModule self, uint32 startPattern)
    {
    AtIterator registerIterator = AtModuleRegisterIteratorCreate(self);
    AtRegister reg;
    eBool filledProperly = cAtTrue;

    while ((reg = (AtRegister)AtIteratorNext(registerIterator)) != NULL)
        {
        if (RegisterIsAffected(reg, startPattern, HalOfRegister(self, reg)))
            {
            filledProperly = cAtFalse;
            break;
            }

        startPattern = startPattern + 1;
        }

    AtObjectDelete((AtObject)registerIterator);

    return filledProperly;
    }

static eAtRet RevertMemory(AtModule self)
    {
    AtIterator registerIterator = AtModuleRegisterIteratorCreate(self);
    AtRegister reg;
    eAtRet ret = cAtOk;

    while ((reg = (AtRegister)AtIteratorNext(registerIterator)) != NULL)
        {
        uint32 revertValue, actualValue;

        /* Apply a revert value */
        AtHal hal = HalOfRegister(self, reg);
        revertValue = ~AtRegisterRead(reg, hal);
        AtRegisterWrite(reg, revertValue, hal);

        /* Make sure that it is written */
        revertValue = revertValue & (~AtRegisterNoneEditableFieldsMask(reg));
        actualValue = AtRegisterRead(reg, hal);
        if (actualValue != revertValue)
                {
                AtPrintc(cSevCritical,
                         "(%s, %d) ERROR: Address 0x%08x, written 0x%08x, but read 0x%08x\n",
                         __FILE__,
                         __LINE__,
                         AtRegisterAddress(reg),
                         revertValue,
                         actualValue);
                ret = cAtErrorMemoryTestFail;
                break;
                }
        }

    AtObjectDelete((AtObject)registerIterator);

    return ret;
    }

static eBool MemoryIsRevertedByKnownPattern(AtModule self, uint32 startPattern)
    {
    AtIterator registerIterator = AtModuleRegisterIteratorCreate(self);
    AtRegister reg;
    eBool revertedProperly = cAtTrue;

    while ((reg = (AtRegister)AtIteratorNext(registerIterator)) != NULL)
        {
        if (RegisterIsAffected(reg, ~startPattern, HalOfRegister(self, reg)))
            {
            revertedProperly = cAtFalse;
            break;
            }

        startPattern = startPattern + 1;
        }

    AtObjectDelete((AtObject)registerIterator);

    return revertedProperly;
    }

static eAtRet DeviceTest(AtModule self)
    {
    uint32 startPattern = 1;
    eAtRet ret;

    /* Fill the whole memory then check if all registers are written properly */
    ret = FillMemoryWithKnownPattern(self, startPattern);
    if (ret != cAtOk)
        return ret;
    if (!MemoryIsFilledWithKnownPattern(self, startPattern))
        return cAtErrorMemoryTestFail;

    /* Revert written value of all registers */
    ret = RevertMemory(self);
    if (ret != cAtOk)
        return ret;
    if (!MemoryIsRevertedByKnownPattern(self, startPattern))
        return cAtErrorMemoryTestFail;

    return cAtOk;
    }

eAtRet HelperAtModuleMemoryTest(AtModule self)
    {
    eAtRet ret;

    ret = DataBusTest(self);
    if (ret != cAtOk)
        return ret;

    ret = AddressBusTest(self);
    if (ret != cAtOk)
        return ret;

    ret = DeviceTest(self);
    if (ret != cAtOk)
        return ret;

    /* Other testings may be added in the future */

    return cAtOk;
    }

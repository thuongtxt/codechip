/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XXX
 *
 * File        : AtModuleXxxRegisterIterator.c
 *
 * Created Date: Jan 6, 2013
 *
 * Description : XXX register iterator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtRegister.h"

#include "../../util/AtIteratorInternal.h"
#include "AtModuleRegisterIteratorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tAtModuleXxxRegisterIterator
    {
    tAtModuleRegisterIterator super;

    /* Private data */
    }tAtModuleXxxRegisterIterator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtIteratorMethods m_AtIteratorOverride;
static tAtModuleRegisterIteratorMethods m_AtModuleRegisterIteratorOverride;

static const tAtModuleRegisterIteratorMethods *m_AtModuleRegisterIteratorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleXxxRegisterIterator);
    }

static uint8 DefaultCore(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->DefaultCore(self);
    }

static uint32 MaxNumChannels(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->MaxNumChannels(self);
    }

static uint32 NextOffset(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->NextOffset(self);
    }

static eBool NoChannelLeft(AtModuleRegisterIterator self)
    {
    /* May change this */
    return m_AtModuleRegisterIteratorMethods->NoChannelLeft(self);
    }

static void ResetChannel(AtModuleRegisterIterator self)
    {
    /* May change this */
    m_AtModuleRegisterIteratorMethods->ResetChannel(self);
    }

static AtObject NextGet(AtIterator self)
    {
	AtUnused(self);
    return NULL;
    }

static void OverrideAtIterator(AtIterator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtIteratorOverride, mMethodsGet(self), sizeof(m_AtIteratorOverride));
        mMethodOverride(m_AtIteratorOverride, NextGet);
        }

    mMethodsSet(self, &m_AtIteratorOverride);
    }

static void OverrideAtModuleRegisterIterator(AtIterator self)
    {
    AtModuleRegisterIterator iterator = (AtModuleRegisterIterator)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRegisterIteratorMethods = mMethodsGet(iterator);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRegisterIteratorOverride, mMethodsGet(iterator), sizeof(m_AtModuleRegisterIteratorOverride));
        mMethodOverride(m_AtModuleRegisterIteratorOverride, MaxNumChannels);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NextOffset);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, NoChannelLeft);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, ResetChannel);
        mMethodOverride(m_AtModuleRegisterIteratorOverride, DefaultCore);
        }

    mMethodsSet(iterator, &m_AtModuleRegisterIteratorOverride);
    }

static void Override(AtIterator self)
    {
    OverrideAtIterator(self);
    OverrideAtModuleRegisterIterator(self);
    }

AtIterator AtModuleXxxRegisterIteratorObjectInit(AtIterator self, AtModule module)
    {
    AtOsalMemInit(self, 0, ObjectSize());

    if (AtModuleRegisterIteratorObjectInit((AtModuleRegisterIterator)self, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIterator AtModuleXxxRegisterIteratorCreate(AtModule module)
    {
    AtIterator newIterator = AtOsalMemAlloc(ObjectSize());

    return AtModuleXxxRegisterIteratorObjectInit(newIterator, module);
    }

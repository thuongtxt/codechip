/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhTug.c
 *
 * Created Date: Sep 4, 2012
 *
 * Description : TUG class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtSdhTugInternal.h"
#include "AtSdhAugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSts(AtSdhChannel self)
    {
	AtUnused(self);
    return 1;
    }

static uint8 Tug3SubChannelType(uint8 mapType)
    {
    if (mapType == cAtSdhTugMapTypeTug3MapVc3)     return cAtSdhChannelTypeTu3;
    if (mapType == cAtSdhTugMapTypeTug3Map7xTug2s) return cAtSdhChannelTypeTug2;

    return cAtSdhChannelTypeTu3;
    }

static uint8 Tug3NumSubChannels(uint8 mapType)
    {
    if (mapType == cAtSdhTugMapTypeTug3MapVc3)     return 1;
    if (mapType == cAtSdhTugMapTypeTug3Map7xTug2s) return 7;

    return 1;
    }

static eAtRet Tug3MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 numSubChannels, subChannelType, i;
    AtSdhChannel subChannel;
    eAtRet ret;
    uint8 lineId;

    /* Do nothing if mapping type is not changed */
    if (mapType == AtSdhChannelMapTypeGet(self))
        return cAtOk;

    /* Delete all sub channels first */
    AtSdhChannelAllSubChannelsHardwareCleanup(self);
    SdhChannelSubChannelsDelete(self);

    /* Create sub channels */
    lineId = AtSdhChannelLineGet(self);
    subChannelType = Tug3SubChannelType(mapType);
    numSubChannels = Tug3NumSubChannels(mapType);
    ret = SdhChannelSubChannelsCreate(self, lineId, numSubChannels, subChannelType);
    if (ret == cAtOk)
        SdhChannelMapTypeSet(self, mapType);

    /* For TU-3, create its VC sub channels */
    if (subChannelType == cAtSdhChannelTypeTu3)
        ret |= SdhChannelSubChannelsCreate(AtSdhChannelSubChannelGet(self, 0), lineId, 1, cAtSdhChannelTypeVc3);
    if (ret != cAtOk)
        return ret;

    /* Cache Line, STS for TU-3 and VC-3 */
    if (subChannelType == cAtSdhChannelTypeTu3)
        {
        AtSdhChannel tu3, vc3;

        /* Cache ID for TU-3 */
        tu3 = AtSdhChannelSubChannelGet(self, 0);
        SdhChannelLineIdSet(tu3, AtSdhChannelLineGet(self));
        SdhChannelSts1IdSet(tu3, AtSdhChannelSts1Get(self));

        /* Cache ID for VC-3 */
        vc3 = AtSdhChannelSubChannelGet(tu3, 0);
        SdhChannelLineIdSet(vc3, AtSdhChannelLineGet(self));
        SdhChannelSts1IdSet(vc3, AtSdhChannelSts1Get(self));
        }

    /* Cache Line, STS and TUG-2 ID for TUG-2 */
    else
        {
        for (i = 0; i < numSubChannels; i++)
            {
            subChannel = AtSdhChannelSubChannelGet(self, i);
            SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
            SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
            SdhChannelTug2IdSet(subChannel, i);
            }
        }

    return ret;
    }

static eAtRet Tug2MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 numSubChannels = 0, subChannelType, i, vcType;
    eAtRet ret;
    uint8 lineId;

    /* Do nothing if mapping type is not changed */
    if (mapType == AtSdhChannelMapTypeGet(self))
        return cAtOk;

    /* Delete all sub channels first */
    AtSdhChannelAllSubChannelsHardwareCleanup(self);
    SdhChannelSubChannelsDelete(self);

    /* Map TU-12 */
    if (mapType == cAtSdhTugMapTypeTug2Map3xTu12s)
        {
        numSubChannels = 3;
        subChannelType = cAtSdhChannelTypeTu12;
        vcType         = cAtSdhChannelTypeVc12;
        }

    /* Map TU-11 */
    else if (mapType == cAtSdhTugMapTypeTug2Map4xTu11s)
        {
        numSubChannels = 4;
        subChannelType = cAtSdhChannelTypeTu11;
        vcType         = cAtSdhChannelTypeVc11;
        }

    else
        return cAtErrorModeNotSupport;

    /* Create sub channels */
    lineId = AtSdhChannelLineGet(self);
    ret = SdhChannelSubChannelsCreate(self, lineId, numSubChannels, subChannelType);
    if (ret == cAtOk)
        SdhChannelMapTypeSet(self, mapType);

    /* Create VC sub channels */
    for (i = 0; i < numSubChannels; i++)
        ret |= SdhChannelSubChannelsCreate(AtSdhChannelSubChannelGet(self, i), lineId, 1, vcType);

    /* Cache Line, STS and TUG-2 ID */
    for (i = 0; i < numSubChannels; i++)
        {
        AtSdhChannel tu, vc;

        /* Cache ID for TU */
        tu = AtSdhChannelSubChannelGet(self, i);
        SdhChannelLineIdSet(tu, AtSdhChannelLineGet(self));
        SdhChannelSts1IdSet(tu, AtSdhChannelSts1Get(self));
        SdhChannelTug2IdSet(tu, AtSdhChannelTug2Get(self));
        SdhChannelTu1xIdSet(tu, i);

        /* Cache ID for VC */
        vc = AtSdhChannelSubChannelGet(tu, 0);
        SdhChannelLineIdSet(vc, AtSdhChannelLineGet(self));
        SdhChannelSts1IdSet(vc, AtSdhChannelSts1Get(self));
        SdhChannelTug2IdSet(vc, AtSdhChannelTug2Get(self));
        SdhChannelTu1xIdSet(vc, i);
        }

    return ret;
    }

static eBool CanChangeMapping(AtSdhChannel self, uint8 newMapType)
    {
    uint8 mapType = AtSdhChannelMapTypeGet(self);

    if (Aug1RestoreIsInProgress(self))
        return cAtTrue;

    if ((mapType != newMapType) && AtSdhChannelServiceIsRunning(self))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;
    uint8 channelType = AtSdhChannelTypeGet(self);

    if (!CanChangeMapping(self, mapType))
        return cAtErrorChannelBusy;

    if (mapType != AtSdhChannelMapTypeGet(self))
        AtSdhChannelSubChannelsInterruptDisable(self, cAtTrue);

    /* Let the super do first because mapping changing may relate with resource
     * management */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    if (channelType == cAtSdhChannelTypeTug3)
        return Tug3MapTypeSet(self, mapType);
    if (channelType == cAtSdhChannelTypeTug2)
        return Tug2MapTypeSet(self, mapType);

    return cAtErrorModeNotSupport;
    }

static const char *TypeString(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)self);

    if (channelType == cAtSdhChannelTypeTug3) return "tug3";
    if (channelType == cAtSdhChannelTypeTug2) return "tug2";

    return "unknown";
    }

static const char *IdString(AtChannel self)
    {
    static char idString[16];
    AtChannel parent = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)self);

    /* Make an empty string */
    AtSprintf(idString, "%s.%u", AtChannelIdString(parent), AtChannelIdGet(self) + 1);

    return idString;
    }

static void SerializeMode(AtSdhChannel object, AtCoder encoder)
    {
    /* VC will need to manage the mode */
    AtUnused(object);
    AtUnused(encoder);
    }

static void OverrideAtChannel(AtSdhTug self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhTug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, SerializeMode);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhTug self)
    {
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    }

AtSdhTug AtSdhTugObjectInit(AtSdhTug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhTug));

    /* Super constructor */
    if (AtSdhChannelObjectInit((AtSdhChannel)self, channelId, channelType, module) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

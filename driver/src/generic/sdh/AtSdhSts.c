/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhSts.c
 *
 * Created Date: May 17, 2017
 *
 * Description : SDH STS-1 class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleAps.h"
#include "AtSdhSts.h"
#include "AtSdhStsInternal.h"
#include "AtModuleSdhInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* To save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    AtUnused(mapType);
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleSdhRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    AtUnused(mapType);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    AtUnused(alarmType);
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumberOfSubChannelsGet(AtSdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static AtSdhChannel SubChannelGet(AtSdhChannel self, uint8 subChannelId)
    {
    AtUnused(self);
    AtUnused(subChannelId);
    return NULL;
    }

static eAtRet ReferenceTransfer(AtChannel self, AtChannel dest)
    {
    AtUnused(self);
    AtUnused(dest);
    return cAtError;
    }

static uint8 NumSts(AtSdhChannel self)
    {
    AtUnused(self);
    return 1;
    }

static void StatusClear(AtChannel self)
    {
    AtUnused(self);
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "sts1";
    }

static eAtRet WarmRestore(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSdhSts object = (AtSdhSts)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(stsGroup);
    }

static eBool CanHaveMapping(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanChangeMode(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet SubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    AtUnused(self);
    AtUnused(onlySubChannels);
    return cAtOk;
    }

static void OverrideAtSdhChannel(AtSdhSts self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));

        /* Setup methods */
        mMethodOverride(m_AtSdhChannelOverride, MapTypeIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, NumberOfSubChannelsGet);
        mMethodOverride(m_AtSdhChannelOverride, SubChannelGet);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeMapping);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtSdhChannelOverride, CanHaveMapping);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeMode);
        mMethodOverride(m_AtSdhChannelOverride, SubChannelsInterruptDisable);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhSts self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, ReferenceTransfer);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhSts self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSdhSts self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

AtSdhSts AtSdhStsObjectInit(AtSdhSts self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhSts));

    /* Super constructor */
    if (AtSdhChannelObjectInit((AtSdhChannel)self, channelId, channelType, module) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhSts AtSdhStsNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhSts newSts = mMethodsGet(osal)->MemAlloc(osal, sizeof(tAtSdhSts));
    if (newSts == NULL)
        return NULL;

    /* Construct it */
    return AtSdhStsObjectInit(newSts, channelId, channelType, module);
    }

void AtSdhStsGroupSet(AtSdhSts self, AtStsGroup group)
    {
    if (self)
        self->stsGroup = group;
    }

/**
 * @addtogroup AtSdhSts
 * @{
 */

/**
 * Get the STS group that is being joined to.
 *
 * @param self This STS
 *
 * @return AtStsGroup object that is being joined to or NULL
 */
AtStsGroup AtSdhStsGroupGet(AtSdhSts self)
    {
    if (self)
        return self->stsGroup;
    return NULL;
    }

/** @} */

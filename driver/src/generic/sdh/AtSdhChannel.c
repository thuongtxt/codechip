/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhChannel.c
 *
 * Created Date: Jul 28, 2012
 *
 * Author      : namnn
 *
 * Description : SDH abstract channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannelInternal.h"
#include "AtModuleSdhInternal.h"

#include "../ber/AtModuleBerInternal.h"
#include "../ber/AtBerControllerInternal.h"
#include "../util/AtUtil.h"
#include "../../util/coder/AtCoderUtil.h"

#include "AtSdhVc.h"
#include "AtSdhVcInternal.h"
#include "../pdh/AtPdhChannelInternal.h"
#include "../sur/AtSurEngineInternal.h"
#include "../prbs/AtModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSdhChannel)self)
#define mChannelIsValid(self) (self ? cAtTrue : cAtFalse)
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

#define mTtiWrapCall(call)                                                     \
do                                                                             \
    {                                                                          \
    eAtModuleSdhRet ret;                                                       \
                                                                               \
    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))           \
        {                                                                      \
        uint32 bufferSize;                                                     \
        char *buffer, *ttiString;                                              \
                                                                               \
        /* Build TTI string */                                                 \
        AtDriverLogLock();                                                     \
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize); \
        ttiString = Tti2String(tti, buffer, bufferSize);                       \
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,    \
                                AtSourceLocationNone, "API: %s([%s], %s)\r\n", \
                                AtFunction, AtObjectToString((AtObject)self), ttiString); \
        AtDriverLogUnLock();                                                   \
        }                                                                      \
                                                                               \
    ret = call;                                                                \
    AtDriverApiLogStop();                                                      \
                                                                               \
    return ret;                                                                \
    }while(0)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtSdhChannelMethods m_methods;
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* To save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
void SdhChannelSubChannelsCleanUp(AtSdhChannel self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->subChannels == NULL)
        return;

    /* Delete memory that used to hold all sub channels */
    mMethodsGet(osal)->MemFree(osal, self->subChannels);
    self->subChannels    = NULL;
    self->numSubChannels = 0;
    self->mapType        = 0;
    }

void SdhChannelSubChannelsDelete(AtSdhChannel self)
    {
    uint8 i;
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(self);

    /* Delete sub channels */
    if (numSubChannels == 0)
        return;

    /* Sub channels have not been created yet */
    if (self->subChannels == NULL)
        return;

    /* Delete sub channels */
    for (i = 0; i < numSubChannels; i++)
        {
        AtObjectDelete((AtObject)(self->subChannels[i]));
        self->subChannels[i] = NULL;
        }

    SdhChannelSubChannelsCleanUp(self);
    }

static void SdhChannelBerControllerSet(AtSdhChannel self, AtBerController berController)
    {
    if (self)
        self->berController = berController;
    }

static void BerControllerDelete(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleBer berModule = (AtModuleBer)AtDeviceModuleGet(device, cAtModuleBer);
    AtBerController controler = SdhChannelBerControllerGet(self);

    if (controler)
        {
        AtModuleBerControllerDelete(berModule, controler);
        SdhChannelBerControllerSet(self, NULL);
        }
    }

static void Delete(AtObject self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    mMethodsGet(channel)->BerControllerDelete(channel);

    SdhChannelSubChannelsDelete(channel);
    SdhChannelSlaveChannelsDelete(channel);

    if (channel->allHwSts)
        {
        AtOsalMemFree(channel->allHwSts);
        channel->allHwSts = NULL;
        }

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);

    if(!AtSdhChannelNeedToUpdateMapType(self, mapType))
        return cAtOk;

    if (!AtSdhChannelMapTypeIsSupported(self, mapType))
        return cAtErrorModeNotSupport;

    /* This abstract class does not know how to map, but it does know how to
     * delete sub channels when new mapping is applied */

    /* AUG-1 will decide to delete it's sub channel by its self */
    if (channelType != cAtSdhChannelTypeAug1)
        {
        AtSdhChannelAllSubChannelsHardwareCleanup(self);
        SdhChannelSubChannelsDelete(self);
        }

    return cAtOk;
    }

static uint8 MapTypeGet(AtSdhChannel self)
    {
    return self->mapType;
    }

static AtChannel MapChannelGet(AtSdhChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eBool MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
	AtUnused(mapType);
	AtUnused(self);
    return cAtTrue;
    }

static eAtModuleSdhRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    AtUnused(mapType);
    AtUnused(self);
    return cAtOk;
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
	AtUnused(tti);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
	AtUnused(enable);
	AtUnused(alarmType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
	AtUnused(alarmType);
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    AtUnused(alarmType);
    AtUnused(self);

    /* Default all alarm can change affecting, concrete class do differently may override */
    return cAtTrue;
    }

static uint8 NumberOfSubChannelsGet(AtSdhChannel self)
    {
    return self->numSubChannels;
    }

static AtSdhChannel SubChannelGet(AtSdhChannel self, uint8 subChannelId)
    {
    if (self->subChannels == NULL)
        return NULL;

    if (subChannelId < self->numSubChannels)
        return self->subChannels[subChannelId];

    return NULL;
    }

static AtSdhChannel ParentChannelGet(AtSdhChannel self)
    {
    return self->parent;
    }

static AtBerController BerControllerGet(AtSdhChannel self)
    {
    AtBerController newController;

    if (self->berController)
        return self->berController;

    /* Create one */
    newController = mMethodsGet(self)->BerControllerCreate(self);
    if (newController == NULL)
        return NULL;

    AtBerControllerInit(newController);
    self->berController = newController;

    return self->berController;
    }

static AtBerController BerControllerCreate(AtSdhChannel self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let sub class do */
    return cAtError;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
	AtUnused(self);
    /* Let sub class do */
    return cAtFalse;
    }

static eAtRet ReferenceTransfer(AtChannel self, AtChannel dest)
    {
    AtSdhChannel selfSdh = (AtSdhChannel)self;
    AtSdhChannel destSdh = (AtSdhChannel)dest;
    uint8 i, numSubChannel;

    /* Super's job */
    if (m_AtChannelMethods->ReferenceTransfer(self, dest) != cAtOk)
        return cAtError;

    destSdh->berController = selfSdh->berController;
    if (destSdh->berController)
        destSdh->berController->monitoredChannel = dest;

    selfSdh->berController = NULL;

    /* This channel should not have sub channels anymore,
    * it's sub channels will be handled by it's cloned child */
    numSubChannel = AtSdhChannelNumberOfSubChannelsGet(selfSdh);
    destSdh->numSubChannels = numSubChannel;

    for (i = 0; i < numSubChannel; i++)
        SdhChannelParentSet(AtSdhChannelSubChannelGet(destSdh, i), destSdh);

    SdhChannelSubChannelsSet(selfSdh, NULL, 0);
    return cAtOk;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    uint8 i;

    if (m_AtChannelMethods->ServiceIsRunning(self))
        return cAtTrue;

    /* Just ask its sub channels */
    for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(channel); i++)
        {
        if (AtChannelServiceIsRunning((AtChannel)AtSdhChannelSubChannelGet(channel, i)))
            return cAtTrue;
        }

    return cAtFalse;
    }

static uint8 NumSts(AtSdhChannel self)
    {
	AtUnused(self);
    return 0;
    }

static void StatusClear(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 subChannel_i;

    /* Clear status of the mapped channel */
    AtChannelStatusClear(AtSdhChannelMapChannelGet(sdhChannel));

    /* Or all of sub channels */
    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(sdhChannel); subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(sdhChannel, subChannel_i);
        AtChannelStatusClear((AtChannel)subChannel);
        }

    /* And status of itself */
    m_AtChannelMethods->StatusClear(self);
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
	AtUnused(counterType);
	AtUnused(self);
    return 0;
    }

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
	AtUnused(counterType);
	AtUnused(self);
    return 0;
    }

static AtSdhLine LineFromChannel(AtSdhChannel self)
    {
    return AtModuleSdhLineGet((AtModuleSdh)AtChannelModuleGet((AtChannel)self), AtSdhChannelLineGet(self));
    }

static eAtRet InterruptAndDefectGet(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear)
    {
    AtChannel channel = (AtChannel)self;

    *currentStatus = AtChannelDefectGet(channel);
    if (read2Clear)
        *changedDefects = AtChannelDefectInterruptClear(channel);
    else
        *changedDefects = AtChannelDefectInterruptGet(channel);

    return cAtOk;
    }

static eAtRet InterruptAndAlarmGet(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear)
    {
    return mMethodsGet(self)->InterruptAndDefectGet(self, changedAlarms, currentStatus, read2Clear);
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
	AtUnused(overheadByteValue);
	AtUnused(overheadByte);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
	AtUnused(overheadByte);
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
	AtUnused(overheadByte);
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eBool TxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    AtUnused(overheadByte);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eBool RxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    AtUnused(overheadByte);
    AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "sdhchannel";
    }

static eAtRet AutoRdiEnable(AtSdhChannel self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool AutoRdiIsEnabled(AtSdhChannel self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet AutoReiEnable(AtSdhChannel self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eBool AutoReiIsEnabled(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet WarmRestore(AtChannel self)
    {
    eAtRet ret;
    uint8 subChannel_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 hwMapType = mMethodsGet(sdhChannel)->HwMapTypeGet(sdhChannel);

    /* Re-configure map type to build database */
    ret = AtSdhChannelMapTypeSet(sdhChannel, hwMapType);
    if (ret != cAtOk)
        return ret;

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(sdhChannel); subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(sdhChannel, subChannel_i);
        ret |= AtChannelWarmRestore((AtChannel)subChannel);
        }

    /* Restore line object */
    AtSdhChannelLineObjectGet(sdhChannel);

    return ret;
    }

static uint8 HwMapTypeGet(AtSdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint16 *AllHwSts(AtSdhChannel self)
    {
    uint8 numSts = AtSdhChannelNumSts(self);
    uint16 *hwSts;
    uint8 sts_i;

    if (self->allHwSts)
        return self->allHwSts;

    /* There may be not enough memory */
    hwSts = AtOsalMemAlloc(numSts * sizeof(uint16));
    if (hwSts == NULL)
        return NULL;

    /* Initialize */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        hwSts[sts_i] = cBit15_0;

    self->allHwSts = hwSts;
    return self->allHwSts;
    }

static void SerializeMode(AtSdhChannel object, AtCoder encoder)
    {
    mEncodeUInt(mode);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSdhChannel object = (AtSdhChannel)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(channelType);

    mEncodeChannelIdString(parent);
    mEncodeChannelIdString(line);

    mEncodeUInt(mapType);
    mEncodeUInt(enabledAlarmAffect);
    mEncodeUInt(lineId);
    mEncodeUInt(stsId);
    mEncodeUInt(tug2Id);
    mEncodeUInt(tu1xId);
    mEncodeUInt(alarmForwardingTemporaryDisabled);
    mEncodeUInt(alarmForwardingMask);
    mEncodeObject(berController);

    mMethodsGet(mThis(self))->SerializeMode(mThis(self), encoder);
    mEncodeNone(mode); /* Just to make the tool happy */

    AtCoderEncodeObjects(encoder, (AtObject *)object->subChannels, object->numSubChannels, "subChannels");
    AtCoderEncodeUInt16Array(encoder, object->allHwSts, AtSdhChannelNumSts(object), "allHwSts");
    mEncodeObjectDescriptionArray(slaveChannels, object->numSlaveChannels);
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtSdhChannelCache);
    }

static eAtRet CacheTxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, tti, &(cache->txTti), sizeof(tAtSdhTti));
        }

    return cAtOk;
    }

static void CacheTxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &(cache->txTti), tti, sizeof(tAtSdhTti));
        }
    }

static eAtRet CacheExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, tti, &(cache->expectedTti), sizeof(tAtSdhTti));
        }

    return cAtOk;
    }

static void CacheExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &(cache->expectedTti), tti, sizeof(tAtSdhTti));
        }
    }

static eAtModuleSdhRet CacheAlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return cAtOk;

    if (enable)
        cache->alarmsAffect = cache->alarmsAffect | alarmType;
    else
        cache->alarmsAffect = cache->alarmsAffect & (~alarmType);

    return cAtOk;
    }

static eBool CacheAlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);

    if (cache == NULL)
        return cAtFalse;

    return (cache->alarmsAffect & alarmType) ? cAtTrue : cAtFalse;
    }

static eAtRet RxTrafficSwitch(AtSdhChannel self, AtSdhChannel otherChannel)
    {
    /* This feature is being developed, it will be available by next release */
    AtUnused(self);
    AtUnused(otherChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet TxTrafficBridge(AtSdhChannel self, AtSdhChannel otherChannel)
    {
    /* This feature is being developed, it will be available by next release */
    AtUnused(self);
    AtUnused(otherChannel);
    return cAtErrorNotImplemented;
    }

static eAtRet TxTrafficCut(AtSdhChannel self)
    {
    /* This feature is being developed, it will be available by next release */
    AtUnused(self);
    return cAtErrorNotImplemented;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    self->mode = mode;
    return cAtOk;
    }

static eAtSdhChannelMode ModeGet(AtSdhChannel self)
    {
    return self->mode;
    }

static eBool CanHaveMapping(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ChannelCanConcate(AtSdhChannel channel)
    {
    if (AtSdhChannelIsInConcatenation(channel))
        return cAtFalse;

    if (AtSdhChannelServiceIsRunning(channel))
        return cAtFalse;

    if (AtSdhChannelIsChannelizedVc(channel))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet ConcateCheck(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    uint8 i;
    uint8 channelType = AtSdhChannelTypeGet(self);
    uint8 masterSts1 = AtSdhChannelSts1Get(self);
    uint8 lineId = AtSdhChannelLineGet(self);
    uint8 numSts = AtSdhChannelNumSts(self);

    if (!AtModuleSdhChannelConcateIsSupported((AtModuleSdh)AtChannelModuleGet((AtChannel)self), channelType, (uint8)(numSlaves + 1)))
        return cAtErrorModeNotSupport;

    if (!ChannelCanConcate(self))
        return cAtErrorChannelBusy;

    for (i = 0; i < numSlaves; i++)
        {
        AtSdhChannel slave = slaveList[i];

        if (slave == NULL)
            return cAtErrorNullPointer;

        if (channelType != AtSdhChannelTypeGet(slave))
            return cAtModuleSdhErrorInvalidConcatenation;

        /* If master and slave channels belong to one SDH line, they must be in
         * ascending sequence to construct correct contiguous concatenation */
        if (lineId == AtSdhChannelLineGet(slave))
            {
            uint8 expectedSts1 = (uint8)(masterSts1 + numSts * (i + 1));

            if (expectedSts1 != AtSdhChannelSts1Get(slave))
                return cAtModuleSdhErrorInvalidConcatenation;
            }

        if (!ChannelCanConcate(slave))
            return cAtErrorChannelBusy;
        }

    return cAtOk;
    }

static uint8 ConcateType(AtSdhChannel self)
    {
    uint8 channelType = AtSdhChannelTypeGet(self);
    switch (channelType)
        {
        case cAtSdhChannelTypeAu4_16c:  return cAtSdhChannelTypeAu4_16nc;
        case cAtSdhChannelTypeAu4:      return cAtSdhChannelTypeAu4_nc;
        default:                        return cAtSdhChannelTypeUnknown;
        }
    }

static uint8 NonConcateType(AtSdhChannel self)
    {
    uint8 channelType = AtSdhChannelTypeGet(self);
    switch (channelType)
        {
        case cAtSdhChannelTypeAu4_16nc:  return cAtSdhChannelTypeAu4_16c;
        case cAtSdhChannelTypeAu4_nc:    return cAtSdhChannelTypeAu4;
        default:                         return cAtSdhChannelTypeUnknown;
        }
    }

static eAtRet Concate(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    uint8 slaveChannel_i;
    eAtRet ret;

    /* Make sure that these channels can be concatenated together */
    ret = AtSdhChannelConcateCheck(self, slaveList, numSlaves);
    if (ret != cAtOk)
        return ret;

    for (slaveChannel_i = 0; slaveChannel_i < numSlaves; slaveChannel_i++)
        AtSdhChannelSubChannelsInterruptDisable(slaveList[slaveChannel_i], cAtFalse);

    /* Clear sub-channels information from their parents */
    for (slaveChannel_i = 0; slaveChannel_i < numSlaves; slaveChannel_i++)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet(slaveList[slaveChannel_i]);

        SdhChannelSubChannelAtIndexSet(parent, 0, NULL);
        parent->numSubChannels = 0;
        }

    ret |= SdhChannelSlaveChannelsSetUp(self, slaveList, numSlaves);
    SdhChannelTypeSet(self, ConcateType(self));

    return ret;
    }

static eAtRet Deconcate(AtSdhChannel self)
    {
    uint8 slaveChannel_i;
    eAtRet ret = cAtOk;

    /* Restore sub-channels information from their parents */
    for (slaveChannel_i = 0; slaveChannel_i < AtSdhChannelNumSlaveChannels(self); slaveChannel_i++)
        {
        AtSdhChannel slave = AtSdhChannelSlaveChannelAtIndex(self, slaveChannel_i);
        AtSdhChannel parent = AtSdhChannelParentChannelGet(slave);

        parent->numSubChannels = 1;
        SdhChannelSubChannelAtIndexSet(parent, 0, slave);
        }

    /* Restore channel type */
    SdhChannelSlaveChannelsCleanUp(self);
    SdhChannelTypeSet(self, NonConcateType(self));

    return ret;
    }

static uint8 Sts1Get(AtSdhChannel self)
    {
    return self->stsId;
    }

static AtSdhChannel SlaveChannelAtIndex(AtSdhChannel self, uint8 slaveIndex)
    {
    if (slaveIndex < self->numSlaveChannels)
        return self->slaveChannels[slaveIndex];
    return NULL;
    }

static uint8 NumSlaveChannels(AtSdhChannel self)
    {
    return self->numSlaveChannels;
    }

static uint32 HoldOffTimerGet(AtSdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eBool HoldOffTimerSupported(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet HoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    AtUnused(self);
    AtUnused(timerInMs);
    return cAtErrorNotImplemented;
    }

static eBool HoldOnTimerSupported(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 HoldOnTimerGet(AtSdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet HoldOnTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    AtUnused(self);
    AtUnused(timerInMs);
    return cAtErrorNotImplemented;
    }

static eBool IsJoiningAps(AtSdhChannel self)
    {
    uint8 subChannel_i;

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(self); subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(self, subChannel_i);
        if (AtSdhChannelIsJoiningAps(subChannel))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool CanChangeMode(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    AtUnused(self);
    AtUnused(mode);
    return cAtTrue;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (AtChannelShouldFullyInit(self))
        {
        ret |= AtSdhChannelDefaultModeSet((AtSdhChannel)self);
        ret |= AtSdhChannelBerControllerDefaultSet((AtSdhChannel)self);
        }

    return ret;
    }

static eAtSdhChannelMode DefaultMode(AtSdhChannel self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(self);
    if (line)
        return AtSdhChannelModeGet((AtSdhChannel)line);
    return cAtSdhChannelModeSdh;
    }

static eAtRet DefaultModeSet(AtSdhChannel self)
    {
    return AtSdhChannelModeSet(self, mMethodsGet(self)->DefaultMode(self));
    }

static eAtRet SubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    uint8 i, numOfSubChannels = AtSdhChannelNumberOfSubChannelsGet(self);

    for (i = 0; i < numOfSubChannels; i++)
        AtSdhChannelSubChannelsInterruptDisable(AtSdhChannelSubChannelGet(self, i), cAtFalse);

    if (!onlySubChannels)
        {
        AtChannelInterruptMaskSet((AtChannel)self, AtChannelInterruptMaskGet((AtChannel)self), 0);
        AtChannelSurEngineInterruptDisable((AtChannel)self);
        }

    return cAtOk;
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentDefects, eBool subChannelsOnly)
    {
    uint8 i, numOfSubChannels = AtSdhChannelNumberOfSubChannelsGet(self);
    AtUnused(subChannelsOnly);

    for (i = 0; i < numOfSubChannels; i++)
        AtSdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannelSubChannelGet(self, i), currentDefects, cAtFalse);
    }

static eBool HasAlarmForwarding(AtSdhChannel self, uint32 currentDefects)
    {
    AtUnused(self);
    AtUnused(currentDefects);
    return cAtFalse;
    }

static eBool HasParentAlarmForwarding(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void FailureForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentFailures, eBool subChannelsOnly)
    {
    uint8 i, numOfSubChannels = AtSdhChannelNumberOfSubChannelsGet(self);
    AtUnused(subChannelsOnly);

    for (i = 0; i < numOfSubChannels; i++)
        AtSdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannelSubChannelGet(self, i), currentFailures, cAtFalse);
    }

static void CopyAllHwSts(AtSdhChannel src, AtSdhChannel dest)
    {
    AtOsal osal;
    uint8 numSts;

    if (src->allHwSts == NULL)
        return;

    osal = AtSharedDriverOsalGet();
    numSts = AtSdhChannelNumSts(src);

    if (dest->allHwSts == NULL)
        dest->allHwSts = AtOsalMemAlloc(numSts * sizeof(uint16));

    mMethodsGet(osal)->MemCpy(osal, dest->allHwSts, src->allHwSts, numSts);
    }

static AtObject Clone (AtObject self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    AtModuleSdh module = (AtModuleSdh)AtChannelModuleGet((AtChannel)sdhChannel);
    AtSdhChannel newChannel = AtModuleSdhChannelCreate(module,
                                                       AtSdhChannelLineGet(sdhChannel),
                                                       AtSdhChannelParentChannelGet(sdhChannel),
                                                       AtSdhChannelTypeGet(sdhChannel),
                                                       (uint8)AtChannelIdGet((AtChannel)sdhChannel));
    if (newChannel == NULL)
        return NULL;

    CopyAllHwSts(sdhChannel, newChannel);

    return (AtObject)newChannel;
    }

static eAtRet BerControllerDefaultSet(AtSdhChannel self)
    {
    AtModuleBer berModule;
    AtBerController controller = AtSdhChannelBerControllerGet(self);
    if (controller == NULL)
        return cAtOk;

    /* To avoid unnecessary memory will be created for BER controller, only
     * explicitly enable BER engine when the BER module is expecting that */
    berModule  = (AtModuleBer)AtBerControllerModuleGet(controller);
    if (AtModuleBerChannelBerShouldBeEnabledByDefault(berModule))
        return AtBerControllerEnable(controller, cAtTrue);

    return cAtOk;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eBool IsAug(AtSdhChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);
    if ((channelType == cAtSdhChannelTypeAug1)  ||
        (channelType == cAtSdhChannelTypeAug4)  ||
        (channelType == cAtSdhChannelTypeAug16) ||
        (channelType == cAtSdhChannelTypeAug64))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsAu(AtSdhChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);

    if ((channelType == cAtSdhChannelTypeAu4_64c) ||
        (channelType == cAtSdhChannelTypeAu4_16c) ||
        (channelType == cAtSdhChannelTypeAu4_4c)  ||
        (channelType == cAtSdhChannelTypeAu4)     ||
        (channelType == cAtSdhChannelTypeAu3)     ||
        (channelType == cAtSdhChannelTypeAu4_16nc)||
        (channelType == cAtSdhChannelTypeAu4_nc))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsTu(AtSdhChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);

    if ((channelType == cAtSdhChannelTypeTu3)  ||
        (channelType == cAtSdhChannelTypeTu12) ||
        (channelType == cAtSdhChannelTypeTu11))
        return cAtTrue;

    return cAtFalse;
    }

static eBool HwIsReady(AtChannel self)
    {
    AtChannel parent = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)self);

    if (parent)
        return AtChannelHwIsReady(parent);

    return m_AtChannelMethods->HwIsReady(self);
    }

static eBool IsInConcatenation(AtSdhChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(self);

    /* Only master channel is updated as Au4_Xnc type, slave channels will keep
     * their original type. */
    if ((channelType == cAtSdhChannelTypeAu4_16nc) ||
        (channelType == cAtSdhChannelTypeAu4_nc))
        return cAtTrue;

    return cAtFalse;
    }

static void Sts1IdSet(AtSdhChannel self, uint8 stsId)
    {
    self->stsId = stsId;
    }

static eBool CanJoinVcg(AtChannel self, AtConcateGroup vcg)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)self);
    uint8 memberType = AtConcateGroupMemberTypeGet(vcg);

    switch (memberType)
        {
        case cAtConcateMemberTypeVc4_64c: return (channelType == cAtSdhChannelTypeVc4_64c) ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeVc4_16c: return (channelType == cAtSdhChannelTypeVc4_16c) ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeVc4_4c:  return (channelType == cAtSdhChannelTypeVc4_4c)  ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeVc4_nc:  return (channelType == cAtSdhChannelTypeVc4_nc)  ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeVc4:     return (channelType == cAtSdhChannelTypeVc4)     ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeVc3:     return (channelType == cAtSdhChannelTypeVc3)     ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeVc12:    return (channelType == cAtSdhChannelTypeVc12)    ? cAtTrue : cAtFalse;
        case cAtConcateMemberTypeVc11:    return (channelType == cAtSdhChannelTypeVc11)    ? cAtTrue : cAtFalse;

        default:    return cAtFalse;
        }
    }

static eAtRet MappingReset(AtSdhChannel self)
    {
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(self);
    uint8 subChannel_i;
    eAtRet ret = cAtOk;

    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(self, subChannel_i);
        if (subChannel)
            ret |= AtSdhChannelMappingReset(subChannel);
        }

    return ret;
    }

static eAtRet SubChannelsHardwareCleanup(AtSdhChannel self)
    {
    uint8 numSubChannels, subChannels_i;
    eAtRet ret = cAtOk;

    numSubChannels = AtSdhChannelNumberOfSubChannelsGet(self);
    if (numSubChannels == 0)
        return ret;

    for (subChannels_i = 0; subChannels_i < numSubChannels; subChannels_i++)
        {
        AtChannel channel = (AtChannel)AtSdhChannelSubChannelGet(self, subChannels_i);
        if (channel)
            ret |= AtChannelHardwareCleanup(channel);
        }

    return ret;
    }

static eAtRet MapChannelHardwareCleanup(AtSdhChannel self)
    {
    AtChannel channel = AtSdhChannelMapChannelGet(self);
    if (channel)
        return AtChannelHardwareCleanup(channel);
    return cAtOk;
    }

static eAtRet AllSubChannelsHardwareCleanup(AtSdhChannel self)
    {
    eAtRet ret = cAtOk;

    ret |= SubChannelsHardwareCleanup(self);

    if (mMethodsGet(self)->ShouldCleanupMapChannel(self))
        ret |= MapChannelHardwareCleanup(self);

    return ret;
    }

static eAtRet HardwareCleanup(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->HardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    return AllSubChannelsHardwareCleanup(mThis(self));
    }

static eAtRet VcAlarmAffectingDisable(AtSdhChannel sdhChannel)
    {
    uint32 enabledAlarm = 0;
    eAtRet ret = cAtOk;
    uint8 subChannel_i;

    if (sdhChannel == NULL)
        return cAtErrorNullPointer;

    /* Backup enabled alarms for restoring */
    if (AtSdhChannelAlarmAffectingIsEnabled(sdhChannel, cAtSdhPathAlarmUneq))
        enabledAlarm |= cAtSdhPathAlarmUneq;

    if (AtSdhChannelAlarmAffectingIsEnabled(sdhChannel, cAtSdhPathAlarmPlm))
        enabledAlarm |= cAtSdhPathAlarmPlm;

    if (AtSdhChannelAlarmAffectingIsEnabled(sdhChannel, cAtSdhPathAlarmTim))
        enabledAlarm |= cAtSdhPathAlarmTim;

    sdhChannel->enabledAlarmAffect |= enabledAlarm;

    /* Disable all alarms affect */
    if (enabledAlarm)
        ret = AtSdhChannelAlarmAffectingEnable(sdhChannel, enabledAlarm , cAtFalse);

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(sdhChannel); subChannel_i++)
        ret |= VcAlarmAffectingDisable(AtSdhChannelSubChannelGet(sdhChannel, subChannel_i));

    return ret;
    }

static eAtRet VcAlarmAffectingRestore(AtSdhChannel self)
    {
    eAtRet ret = cAtOk;
    uint8 subChannel_i;

    if (self == NULL)
        return cAtErrorNullPointer;

    if (self->enabledAlarmAffect != 0)
        ret |= AtSdhChannelAlarmAffectingEnable(self, self->enabledAlarmAffect, cAtTrue);

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(self); subChannel_i++)
        ret |= VcAlarmAffectingRestore(AtSdhChannelSubChannelGet(self, subChannel_i));

    return ret;
    }

static uint8 ActualMapTypeGet(AtSdhChannel self)
    {
    return mMethodsGet(self)->MapTypeGet(self);
    }

static eAtRet AllServicesDestroy(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 subChannel_i;
    eAtRet ret = cAtOk;

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)self); subChannel_i++)
        {
        AtChannel subChannel = (AtChannel)AtSdhChannelSubChannelGet(sdhChannel, subChannel_i);
        if (subChannel)
            ret |= AtChannelAllServicesDestroy(subChannel);
        }

    return ret;
    }

static AtSdhLine LineObjectGet(AtSdhChannel self)
    {
    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeLine)
        return (AtSdhLine)self;

    if (self->line == NULL)
        self->line = LineFromChannel(self);

    return self->line;
    }

static eAtModuleSdhRet AlarmAutoRdiEnable(AtSdhChannel self, uint32 alarms, eBool enable)
    {
    AtUnused(self);
    AtUnused(alarms);
    AtUnused(enable);
    return cAtErrorModeNotSupport;
    }

static eBool AlarmAutoRdiIsEnabled(AtSdhChannel self, uint32 alarms)
    {
    AtUnused(self);
    AtUnused(alarms);
    return cAtFalse;
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtSdhChannelSharedQuerier();
    }

static eAtModuleSdhRet DoMapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    AtModule sdhModule, berModule;
    eAtRet ret;

    if (!mChannelIsValid(self))
        return cAtError;

    sdhModule = AtChannelModuleGet((AtChannel)self);
    berModule = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleBer);

    if (berModule)
        AtModuleLock(berModule);
    AtModuleLock(sdhModule);

    ret = mMethodsGet(self)->MapTypeSet(self, mapType);

    AtModuleUnLock(sdhModule);
    if (berModule)
        AtModuleUnLock(berModule);

    return ret;
    }

static char *Tti2String(const tAtSdhTti *tti, char *buffer, uint32 bufferSize)
    {
    uint8 ttiLength = AtSdhTtiLengthForTtiMode(tti->mode);
    return AtUtilBytes2String(tti->message, ttiLength, 16, buffer, bufferSize);
    }

static eAtModuleSdhRet DoTxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    if (mChannelIsValid(self))
        {
        CacheTxTtiSet(self, tti);
        return mMethodsGet(self)->TxTtiSet(self, tti);
        }

    return cAtError;
    }

static eAtModuleSdhRet DoExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    if (mChannelIsValid(self))
        {
        CacheExpectedTtiSet(self, tti);
        return mMethodsGet(self)->ExpectedTtiSet(self, tti);
        }

    return cAtError;
    }

static void CacheModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->mode = mode;
    }

static eAtSdhChannelMode CacheModeGet(AtSdhChannel self)
    {
    tAtSdhChannelCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        return cache->mode;
    return cAtSdhChannelModeUnknown;
    }

static eAtModuleSdhRet DoModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    eAtRet ret;
    uint8 i, numSubChannel;
    eAtSdhChannelMode currentMode;

    if (self == NULL)
        return cAtErrorNullPointer;

    CacheModeSet(self, mode);

    /* Set same old mode, just update database if it is caching invalid mode. Otherwise, warm restore testing will be fail */
    currentMode = AtSdhChannelModeGet(self);
    if ((mode == currentMode) && AtChannelShouldPreventReconfigure((AtChannel)self))
        {
        if (AtSdhChannelInternalDbModeGet(self) == cAtSdhChannelModeUnknown)
            AtSdhChannelInternalDbModeSet(self, mode);
        return cAtOk;
        }

    if ((mode != cAtSdhChannelModeSdh) && (mode != cAtSdhChannelModeSonet))
        return cAtErrorModeNotSupport;

    if (!AtSdhChannelCanChangeMode(self, mode))
        return cAtErrorChannelBusy;

    ret = mMethodsGet(self)->ModeSet(self, mode);
    if (ret != cAtOk)
        return ret;

    /* Change hierarchy for all of it's sub channels */
    numSubChannel = AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)self);
    for (i = 0; i < numSubChannel; i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet((AtSdhChannel)self, i);
        ret = AtSdhChannelModeSet(subChannel, mode);
        if (ret != cAtOk)
            return ret;
        }

    return cAtOk;
    }

static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    AtUnused(self);
    AtUnused(counterType);
    AtUnused(mode);
    return cAtErrorNotImplemented;
    }

static eAtSdhChannelCounterMode DefaultCounterMode(AtSdhChannel self)
    {
    AtModuleSdh module = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);

    if (AtModuleSdhShouldUseBitErrorCounter(module))
        return cAtSdhChannelCounterModeBit;

    return (AtSdhChannelModeGet(self) == cAtSdhChannelModeSdh) ? cAtSdhChannelCounterModeBlock : cAtSdhChannelCounterModeBit;
    }

static AtSdhChannel HideChannelGet(AtSdhChannel self)
    {
    AtSdhChannel parent = AtSdhChannelParentChannelGet(self);
    AtSdhChannel hideParent = AtSdhChannelHideChannelGet(parent);
    uint8 subChannel_i;

    if (hideParent == NULL)
        return NULL;

    for (subChannel_i = 0; subChannel_i < AtSdhChannelNumberOfSubChannelsGet(parent); subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(parent, subChannel_i);
        if (subChannel == self)
            return AtSdhChannelSubChannelGet(hideParent, subChannel_i);
        }

    return NULL;
    }

static eBool HasCrossConnect(AtSdhChannel self)
    {
    uint8 i, numSubChannel;

    if (AtChannelHasCrossConnect((AtChannel)self))
        return cAtTrue;

    /* Ask for its sub-channels if any cross-connect is connecting. */
    numSubChannel = AtSdhChannelNumberOfSubChannelsGet(self);
    for (i = 0; i < numSubChannel; i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(self, i);
        if (HasCrossConnect(subChannel))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool ShouldCleanupMapChannel(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void MethodsInit(AtSdhChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MapTypeSet);
        mMethodOverride(m_methods, MapTypeGet);
        mMethodOverride(m_methods, ActualMapTypeGet);
        mMethodOverride(m_methods, MapChannelGet);
        mMethodOverride(m_methods, MapTypeIsSupported);
        mMethodOverride(m_methods, MappingReset);
        mMethodOverride(m_methods, TxTtiGet);
        mMethodOverride(m_methods, TxTtiSet);
        mMethodOverride(m_methods, ExpectedTtiGet);
        mMethodOverride(m_methods, ExpectedTtiSet);
        mMethodOverride(m_methods, RxTtiGet);
        mMethodOverride(m_methods, TtiCompareEnable);
        mMethodOverride(m_methods, TtiCompareIsEnabled);
        mMethodOverride(m_methods, AlarmAffectingEnable);
        mMethodOverride(m_methods, AlarmAffectingIsEnabled);
        mMethodOverride(m_methods, AlarmAffectingMaskGet);
        mMethodOverride(m_methods, NumberOfSubChannelsGet);
        mMethodOverride(m_methods, SubChannelGet);
        mMethodOverride(m_methods, ParentChannelGet);
        mMethodOverride(m_methods, BerControllerGet);
        mMethodOverride(m_methods, BerControllerCreate);
        mMethodOverride(m_methods, BerControllerDelete);
        mMethodOverride(m_methods, TimMonitorEnable);
        mMethodOverride(m_methods, TimMonitorIsEnabled);
        mMethodOverride(m_methods, NumSts);
        mMethodOverride(m_methods, BlockErrorCounterGet);
        mMethodOverride(m_methods, BlockErrorCounterClear);
        mMethodOverride(m_methods, TxOverheadByteSet);
        mMethodOverride(m_methods, TxOverheadByteGet);
        mMethodOverride(m_methods, RxOverheadByteGet);
        mMethodOverride(m_methods, TxOverheadByteIsSupported);
        mMethodOverride(m_methods, RxOverheadByteIsSupported);
        mMethodOverride(m_methods, InterruptAndDefectGet);
        mMethodOverride(m_methods, InterruptAndAlarmGet);
        mMethodOverride(m_methods, AutoRdiEnable);
        mMethodOverride(m_methods, AutoRdiIsEnabled);
        mMethodOverride(m_methods, HwMapTypeGet);
        mMethodOverride(m_methods, RxTrafficSwitch);
        mMethodOverride(m_methods, TxTrafficBridge);
        mMethodOverride(m_methods, TxTrafficCut);
        mMethodOverride(m_methods, CanChangeMapping);
        mMethodOverride(m_methods, ModeSet);
        mMethodOverride(m_methods, ModeGet);
        mMethodOverride(m_methods, Concate);
        mMethodOverride(m_methods, Deconcate);
        mMethodOverride(m_methods, ConcateCheck);
        mMethodOverride(m_methods, Sts1Get);
        mMethodOverride(m_methods, CanChangeAlarmAffecting);
        mMethodOverride(m_methods, CanHaveMapping);
        mMethodOverride(m_methods, HoldOffTimerSet);
        mMethodOverride(m_methods, HoldOffTimerGet);
        mMethodOverride(m_methods, HoldOffTimerSupported);
        mMethodOverride(m_methods, HoldOnTimerSet);
        mMethodOverride(m_methods, HoldOnTimerGet);
        mMethodOverride(m_methods, HoldOnTimerSupported);
        mMethodOverride(m_methods, CanChangeMode);
        mMethodOverride(m_methods, SubChannelsInterruptDisable);
        mMethodOverride(m_methods, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_methods, HasAlarmForwarding);
        mMethodOverride(m_methods, HasParentAlarmForwarding);
        mMethodOverride(m_methods, Sts1IdSet);
        mMethodOverride(m_methods, LineObjectGet);
        mMethodOverride(m_methods, AlarmAutoRdiEnable);
        mMethodOverride(m_methods, AlarmAutoRdiIsEnabled);
        mMethodOverride(m_methods, FailureForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_methods, SerializeMode);
        mMethodOverride(m_methods, IsJoiningAps);
        mMethodOverride(m_methods, CounterModeSet);
        mMethodOverride(m_methods, HideChannelGet);
        mMethodOverride(m_methods, DefaultMode);
        mMethodOverride(m_methods, AutoReiEnable);
        mMethodOverride(m_methods, AutoReiIsEnabled);
        mMethodOverride(m_methods, ShouldCleanupMapChannel);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtSdhChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, ReferenceTransfer);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, CacheSize);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, HwIsReady);
        mMethodOverride(m_AtChannelOverride, CanJoinVcg);
        mMethodOverride(m_AtChannelOverride, HardwareCleanup);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        mMethodOverride(m_AtChannelOverride, AllServicesDestroy);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhChannel self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Clone);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSdhChannel self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

AtSdhChannel AtSdhChannelObjectInit(AtSdhChannel self,
                                    uint32 channelId,
                                    uint8 channelType,
                                    AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhChannel));

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(self);

    /* Setup private data */
    self->channelType = channelType;

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

uint8 AtSdhChannelSts1Get(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->Sts1Get(self);

    return 0;
    }

uint8 AtSdhChannelLineGet(AtSdhChannel self)
    {
    if (self)
        return self->lineId;

    return 0;
    }

uint8 AtSdhChannelTug2Get(AtSdhChannel self)
    {
    if (self)
        return self->tug2Id;

    return 0;
    }

uint8 AtSdhChannelTu1xGet(AtSdhChannel self)
    {
    if (self)
        return self->tu1xId;

    return 0;
    }

void SdhChannelSubChannelsSet(AtSdhChannel self, AtSdhChannel *subChannels, uint8 numSubChannels)
    {
    if (self == NULL)
        return;

    self->subChannels = subChannels;
    self->numSubChannels = numSubChannels;
    }

void SdhChannelParentSet(AtSdhChannel self, AtSdhChannel parent)
    {
    if (self)
        self->parent = parent;
    }

void SdhChannelMapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    if (self)
        self->mapType = mapType;
    }

uint8 SdhChannelMapTypeGet(AtSdhChannel self)
    {
    if (self)
        return self->mapType;
    return 0;
    }

void SdhChannelSubChannelAtIndexSet(AtSdhChannel self, uint8 subChannelIndex, AtSdhChannel subChannel)
    {
    if (self == NULL)
        return;

    if (subChannelIndex >= self->numSubChannels)
        return;

    self->subChannels[subChannelIndex] = subChannel;
    }

eAtRet SdhChannelSubChannelsCreate(AtSdhChannel self, uint8 lineId, uint8 numSubChannels, uint8 subChannelType)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh sdhModule;
    AtSdhChannel *subChannels;
    uint8 i, j;

    /* Do nothing if there is no sub channel */
    if (numSubChannels == 0)
        return cAtOk;

    /* Create memory to store all sub channels */
    subChannels = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtSdhChannel) * numSubChannels);
    if (subChannels == NULL)
        return cAtErrorRsrcNoAvail;

    sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    if (sdhModule == NULL)
        return cAtErrorNullPointer;

    /* Create all sub channels */
    for (i = 0; i < numSubChannels; i++)
        {
        subChannels[i] = mMethodsGet(sdhModule)->ChannelCreate(sdhModule, lineId, self, subChannelType, i);

        /* Cannot create channel, free allocated memory */
        if (subChannels[i] == NULL)
            {
            /* Delete instances */
            for (j = 0; j < i; j++)
                AtObjectDelete((AtObject)subChannels[j]);

            /* And memory that hold them */
            mMethodsGet(osal)->MemFree(osal, subChannels);

            return cAtErrorRsrcNoAvail;
            }

        /* Set its parent */
        SdhChannelParentSet(subChannels[i], self);

        SdhChannelLineIdSet(subChannels[i], AtSdhChannelLineGet(self));
        }

    /* Cache to database */
    SdhChannelSubChannelsSet(self, subChannels, numSubChannels);

    return cAtOk;
    }

eAtRet SdhChannelSlaveChannelsSetUp(AtSdhChannel self, AtSdhChannel *slaveChannels, uint8 numSlaveChannels)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    /* Do nothing if there is no slaves channel */
    if (numSlaveChannels == 0)
        return cAtOk;

    /* Create memory to store all slave channels */
    if (self->slaveChannels == NULL)
        {
        self->slaveChannels = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtSdhChannel) * numSlaveChannels);
        if (self->slaveChannels == NULL)
            return cAtErrorRsrcNoAvail;
        }

    mMethodsGet(osal)->MemCpy(osal, self->slaveChannels, slaveChannels, sizeof(AtSdhChannel) * numSlaveChannels);
    self->numSlaveChannels = numSlaveChannels;

    return cAtOk;
    }

void SdhChannelSlaveChannelsCleanUp(AtSdhChannel self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->slaveChannels == NULL)
        return;

    /* Delete memory that used to hold all slave channels */
    mMethodsGet(osal)->MemFree(osal, self->slaveChannels);
    self->slaveChannels    = NULL;
    self->numSlaveChannels = 0;
    }

void SdhChannelSlaveChannelsDelete(AtSdhChannel self)
    {
    uint8 i;
    uint8 numSlaveChannels = AtSdhChannelNumSlaveChannels(self);

    if (numSlaveChannels == 0)
        return;

    /* Slave channels do not exist */
    if (self->slaveChannels == NULL)
        return;

    /* Delete slave channels */
    for (i = 0; i < numSlaveChannels; i++)
        {
        AtObjectDelete((AtObject)(self->slaveChannels[i]));
        self->slaveChannels[i] = NULL;
        }

    SdhChannelSlaveChannelsCleanUp(self);
    }

void SdhChannelTypeSet(AtSdhChannel self, uint8 channelType)
    {
    if (self)
        self->channelType = channelType;
    }

void SdhChannelLineIdSet(AtSdhChannel self, uint8 lineId)
    {
    if (self)
        self->lineId = lineId;
    }

void SdhChannelSts1IdSet(AtSdhChannel self, uint8 stsId)
    {
    if (self)
        mMethodsGet(self)->Sts1IdSet(self, stsId);
    }

void SdhChannelTug2IdSet(AtSdhChannel self, uint8 tug2Id)
    {
    if (self)
        self->tug2Id = tug2Id;
    }

void SdhChannelTu1xIdSet(AtSdhChannel self, uint8 tu1xId)
    {
    if (self)
        self->tu1xId = tu1xId;
    }

AtBerController SdhChannelBerControllerGet(AtSdhChannel self)
    {
    if (self)
        return self->berController;

    return NULL;
    }

eBool AtSdhChannelServiceIsRunning(AtSdhChannel self)
    {
    return AtChannelServiceIsRunning((AtChannel)self);
    }

eBool AtSdhChannelNeedToUpdateMapType(AtSdhChannel self, uint8 mapType)
    {
    uint8 currentMappingType;

    if (Aug1RestoreIsInProgress(self))
        return cAtTrue;

    currentMappingType = AtSdhChannelActualMapTypeGet(self);
    if (mapType == currentMappingType)
        return cAtFalse;

    return cAtTrue;
    }

uint8 AtSdhChannelNumSts(AtSdhChannel self)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->NumSts(self);

    return 0;
    }

void SdhChannelAllBerControllersDelete(AtSdhChannel self)
    {
    uint8 i;

    if (self == NULL)
        return;

    /* Delete controller of this channel */
    if (SdhChannelBerControllerGet(self))
        mMethodsGet(self)->BerControllerDelete(self);

    /* Delete controllers of all of its sub channels */
    for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(self); i++)
        SdhChannelAllBerControllersDelete(AtSdhChannelSubChannelGet(self, i));
    }

AtSdhLine AtSdhChannelLineObjectGet(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->LineObjectGet(self);
    return NULL;
    }

uint16 *AtSdhChannelAllHwSts(AtSdhChannel self)
    {
    if (self)
        return AllHwSts(self);
    return NULL;
    }

eBool AtSdhChannelIsHoVc(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhChannel);

    if ((vcType == cAtSdhChannelTypeVc4_64c) ||
        (vcType == cAtSdhChannelTypeVc4_16c) ||
        (vcType == cAtSdhChannelTypeVc4_4c)  ||
        (vcType == cAtSdhChannelTypeVc4_16nc)||
        (vcType == cAtSdhChannelTypeVc4_nc)  ||
        (vcType == cAtSdhChannelTypeVc4))
        return cAtTrue;

    if (vcType != cAtSdhChannelTypeVc3)
        return cAtFalse;

    /* In case of VC-3, it can be mapped to TU-3 and it is low-order VC-3 */
    if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(sdhChannel)) == cAtSdhChannelTypeTu3)
        return cAtFalse;

    return cAtTrue;
    }

eBool AtSdhChannelIsChannelizedVc(AtSdhChannel sdhChannel)
    {
    uint8 mapType;
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)sdhChannel), cAtModulePrbs);

    if (!AtSdhChannelIsHoVc(sdhChannel))
        return cAtFalse;

    /* If PRBS engine is provisioned, then it is not channelized anymore. This
     * happens when channelized PRBS is allowed. This is determined by
     * ThaModulePrbs.ChannelizedPrbsAllowed() */
    if (AtModulePrbsChannelizedPrbsAllowed(prbsModule) && AtChannelPrbsEngineIsCreated((AtChannel)sdhChannel))
        return cAtFalse;

    mapType = AtSdhChannelMapTypeGet(sdhChannel);
    if ((mapType == cAtSdhVcMapTypeVc3Map7xTug2s) ||
        (mapType == cAtSdhVcMapTypeVc3MapDe3)     ||
        (mapType == cAtSdhVcMapTypeVc4Map3xTug3s))
        return cAtTrue;

    return cAtFalse;
    }

eAtRet AtSdhChannelCanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    if (self)
        return mMethodsGet(self)->CanChangeMapping(self, mapType);

    return cAtErrorNullPointer;
    }

eBool AtSdhChannelIsVc(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhChannel);

    if ((vcType == cAtSdhChannelTypeVc4_64c) ||
        (vcType == cAtSdhChannelTypeVc4_16c) ||
        (vcType == cAtSdhChannelTypeVc4_16nc)||
        (vcType == cAtSdhChannelTypeVc4_4c)  ||
        (vcType == cAtSdhChannelTypeVc4_nc)  ||
        (vcType == cAtSdhChannelTypeVc4)     ||
        (vcType == cAtSdhChannelTypeVc3)     ||
        (vcType == cAtSdhChannelTypeVc12)    ||
        (vcType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

eBool AtSdhChannelCanEnableAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->CanChangeAlarmAffecting(self, alarmType);
    return cAtFalse;
    }

eAtRet AtSdhChannelSubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    if (self)
        return mMethodsGet(self)->SubChannelsInterruptDisable(self, onlySubChannels);
    return cAtErrorNullPointer;
    }

void AtSdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentDefect, eBool subChannelsOnly)
    {
    if (self == NULL)
        return;

    if (AtModuleSdhNeedClearanceNotifyLogic((AtModuleSdh)AtChannelModuleGet((AtChannel)self)))
        mMethodsGet(self)->AlarmForwardingClearanceNotifyWithSubChannelOnly(self, currentDefect, subChannelsOnly);
    }

eBool AtSdhChannelHasAlarmForwarding(AtSdhChannel self, uint32 currentDefects)
    {
    if (self)
        return mMethodsGet(self)->HasAlarmForwarding(self, currentDefects);
    return cAtFalse;
    }

eAtRet AtSdhChannelBerControllerDefaultSet(AtSdhChannel self)
    {
    if (self)
        return BerControllerDefaultSet(self);
    return cAtErrorObjectNotExist;
    }

eBool AtSdhChannelHasParentAlarmForwarding(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->HasParentAlarmForwarding(self);
    return cAtFalse;
    }

void AtSdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentFailures, eBool subChannelsOnly)
    {
    if (self == NULL)
        return;

    if (AtModuleSdhNeedClearanceNotifyLogic((AtModuleSdh)AtChannelModuleGet((AtChannel)self)))
        mMethodsGet(self)->FailureForwardingClearanceNotifyWithSubChannelOnly(self, currentFailures, subChannelsOnly);
    }

uint32 AtSdhChannelAlarmAffectingMaskGet(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->AlarmAffectingMaskGet(self);
    return 0;
    }

eBool AtSdhChannelIsAug(AtSdhChannel self)
    {
    if (self)
        return IsAug(self);
    return cAtFalse;
    }

eBool AtSdhChannelIsAu(AtSdhChannel self)
    {
    if (self)
        return IsAu(self);
    return cAtFalse;
    }

eBool AtSdhChannelIsTu(AtSdhChannel self)
    {
    if (self)
        return IsTu(self);
    return cAtFalse;
    }

eBool AtSdhChannelIsInConcatenation(AtSdhChannel self)
    {
    if (self)
        return IsInConcatenation(self);
    return cAtFalse;
    }

eAtRet AtSdhChannelConcateCheck(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    if (self)
        return mMethodsGet(self)->ConcateCheck(self, slaveList, numSlaves);
    return cAtErrorNullPointer;
    }

eAtRet AtSdhChannelMappingReset(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->MappingReset(self);
    return cAtErrorNullPointer;
    }

eAtRet AtSdhChannelAlarmAffectingDisable(AtSdhChannel self)
    {
    if (self)
        return VcAlarmAffectingDisable(self);
    return cAtErrorNullPointer;
    }

eAtRet AtSdhChannelAlarmAffectingRestore(AtSdhChannel self)
    {
    if (self)
        return VcAlarmAffectingRestore(self);
    return cAtErrorNullPointer;
    }

eBool AtSdhChannelHoldOffTimerSupported(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->HoldOffTimerSupported(self);
    return cAtFalse;
    }

void AtSdhChannelAllHwStsDelete(AtSdhChannel self)
    {
    if ((self == NULL) || (self->allHwSts == NULL))
        return;

    AtOsalMemFree(self->allHwSts);
    self->allHwSts = NULL;
    }

eBool AtSdhChannelHoldOnTimerSupported(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->HoldOnTimerSupported(self);
    return cAtFalse;
    }

uint8 AtSdhChannelActualMapTypeGet(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->ActualMapTypeGet(self);
    return 0;
    }

eAtRet AtSdhChannelAllSubChannelsHardwareCleanup(AtSdhChannel self)
    {
    if (self)
        return AllSubChannelsHardwareCleanup(self);
    return cAtErrorNullPointer;
    }

eAtRet AtSdhChannelDefaultModeSet(AtSdhChannel self)
    {
    if (self)
        return DefaultModeSet(self);
    return cAtErrorObjectNotExist;
    }

eAtModuleSdhRet AtSdhChannelInternalDbModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    return ModeSet(self, mode);
    }

eAtSdhChannelMode AtSdhChannelInternalDbModeGet(AtSdhChannel self)
    {
    return ModeGet(self);
    }

eBool AtSdhChannelIsJoiningAps(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->IsJoiningAps(self);
    return cAtFalse;
    }

static eAtRet SdhChannelDeconcateWithLog(AtSdhChannel self)
    {
    eAtRet value;

    mNoParamApiLogStart();
    if (self)
        {
        if (!AtSdhChannelIsInConcatenation(self))
            value = cAtOk;
        else
            value = mMethodsGet(self)->Deconcate(self);
        }
    else
        {
        value = cAtErrorNullPointer;
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                                AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        }

    AtDriverApiLogStop();

    return value;
    }

eAtModuleSdhRet AtSdhChannelCounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    if (self)
        return mMethodsGet(self)->CounterModeSet(self, counterType, mode);
    return cAtErrorNullPointer;
    }

eAtRet AtSdhChannelDefaultCounterModeSet(AtSdhChannel self, uint16 counterType)
    {
    return AtSdhChannelCounterModeSet(self, counterType, DefaultCounterMode(self));
    }

AtSdhChannel AtSdhChannelHideChannelGet(AtSdhChannel self)
    {
    if (self)
        return mMethodsGet(self)->HideChannelGet(self);
    return NULL;
    }

eBool AtSdhChannelHasCrossConnect(AtSdhChannel self)
    {
    if (self)
        return HasCrossConnect(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtSdhChannel
 * @{
 */
/**
 * Get channel type
 * @param self This channel
 *
 * @return @ref eAtSdhChannelType "SDH channel type"
 */
eAtSdhChannelType AtSdhChannelTypeGet(AtSdhChannel self)
    {
    if (mChannelIsValid(self))
        return self->channelType;

    return cAtSdhChannelTypeUnknown;
    }

/**
 * Set mapping type
 *
 * @param self This channel
 * @param mapType Mapping type. Not all of SDH channel are mappable. The
 *                following list show all of valid mapping types of mappable object
 *                - AtSdhAug: @ref eAtSdhAugMapType "AUG mapping type"
 *                - AtSdhTug: @ref eAtSdhTugMapType "TUG mapping type"
 *                - AtSdhTu: @ref eAtSdhTuMapType "TU mapping type"
 *                - AtSdhVc: @ref eAtSdhVcMapType "VC mapping type"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelMapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    mOneParamWrapCall(mapType, eAtModuleSdhRet, DoMapTypeSet(self, mapType));
    }

/**
 * Get mapping type
 *
 * @param self This channel
 *
 * @return Mapping type. Not all of SDH channel are mappable. The following list
 *         show all of valid mapping types of mappable object
 *         - AtSdhAug: @ref eAtSdhAugMapType "AUG mapping type"
 *         - AtSdhTug: @ref eAtSdhTugMapType "TUG mapping type"
 *         - AtSdhTu: @ref eAtSdhTuMapType "TU mapping type"
 *         - AtSdhVc: @ref eAtSdhVcMapType "VC mapping type"
 */
uint8 AtSdhChannelMapTypeGet(AtSdhChannel self)
    {
    mAttributeGet(MapTypeGet, uint8, 0);
    }

/**
 * Check if a channel can support input mapping type
 *
 * @param self This channel
 * @param mapType Mapping type
 *
 * @retval cAtTrue if it supports
 * @retval cAtFalse if it does not support
 */
eBool AtSdhChannelMapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    mOneParamAttributeGet(MapTypeIsSupported, mapType, eBool, cAtFalse);
    }

/**
 * Get the AtChannel that is mapped to this channel. For example, when a VC-12 is
 * mapped E1, this API returns AtPdhDe1 object that is internally mapped to this
 * VC.
 *
 * @param self This channel
 *
 * @return AtChannel object that is mapped to this channel in case of success. Or
 *         NULL is returned in case of failure.
 */
AtChannel AtSdhChannelMapChannelGet(AtSdhChannel self)
    {
    mNoParamObjectGet(MapChannelGet, AtChannel);
    }

/**
 * Get transmitted TTI (J0, J1, J2)
 *
 * @param self This channel
 * @param [out] tti @ref tAtSdhTti "Transmitted TTI"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelTxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (!mChannelIsValid(self))
        return cAtErrorNullPointer;

    if (mInAccessible(self))
        return CacheTxTtiGet(self, tti);

    return mMethodsGet(self)->TxTtiGet(self, tti);
    }

/**
 * Set transmitted TTI (J0, J1, J2)
 *
 * @param self This channel
 * @param tti  @ref tAtSdhTti "TTI message"
 *
 * @return AT return codes
 */
eAtModuleSdhRet AtSdhChannelTxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    mTtiWrapCall(DoTxTtiSet(self, tti));
    }

/**
 * Get expected TTI (J0, J1, J2)
 *
 * @param self This channel
 * @param [out] tti @ref tAtSdhTti "Expected TTI"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (!mChannelIsValid(self))
        return cAtErrorNullPointer;

    if (mInAccessible(self))
        return CacheExpectedTtiGet(self, tti);

    return mMethodsGet(self)->ExpectedTtiGet(self, tti);
    }

/**
 * Set expected TTI (J0, J1, J2)
 *
 * @param self This channel
 * @param tti @ref tAtSdhTti "Expected TTI"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    mTtiWrapCall(DoExpectedTtiSet(self, tti));
    }

/**
 * Get received TTI  (J0, J1, J2)
 *
 * @param self This channel
 * @param [out] tti @ref tAtSdhTti @"Received TTI"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelRxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mChannelIsValid(self))
        return mMethodsGet(self)->RxTtiGet(self, tti);

    return cAtError;
    }

/**
 * Enable/disable TTI comparing.
 *
 * @param self This channel
 * @param enable TTI comparing
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code.
 */
eAtModuleSdhRet AtSdhChannelTtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(TtiCompareEnable, enable);
    }

/**
 * Check if TTI comparing is enabled or not
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtSdhChannelTtiCompareIsEnabled(AtSdhChannel self)
    {
    mAttributeGet(TtiCompareIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable AIS downstream and RDI to remote side.
 *
 * @param self This channel
 * @param alarmType Alarm type.
 *                  - @ref eAtSdhLineAlarmType "Line alarm types"
 *                  - @ref eAtSdhPathAlarmType "Path alarm types"
 * @param enable [cAtTrue, cAtFalse] to enable or disable alarm affecting
 *
 * @return AT return code
 *
 * @note This API is applicable for @ref AtSdhLine and @ref AtSdhPath subclasses
 */
eAtModuleSdhRet AtSdhChannelAlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    mTwoParamsAttributeSetWithCache(AlarmAffectingEnable, alarmType, enable);
    }

/**
 * Check if alarm affecting is enabled
 *
 * @param self This channel
 * @param alarmType Alarm type
 *
 * @retval cAtTrue If enabled
 * @retval cAtFalse If disabled
 */
eBool AtSdhChannelAlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    mOneParamAttributeGetWithCache(AlarmAffectingIsEnabled, alarmType, eBool, cAtFalse);
    }

/**
 * Get number of sub-channels
 *
 * @param self This channel
 *
 * @return Number of sub-channels
 */
uint8 AtSdhChannelNumberOfSubChannelsGet(AtSdhChannel self)
    {
    mAttributeGet(NumberOfSubChannelsGet, uint8, 0);
    }

/**
 * Get a sub-channel of this channel in mapping tree.
 *
 * @param self This channel
 * @param subChannelId Sub-channel ID
 *
 * @return A sub-channel on success or NULL on failure
 */
AtSdhChannel AtSdhChannelSubChannelGet(AtSdhChannel self, uint8 subChannelId)
    {
    if (self == NULL)
        {
        AtDriverLog(AtDriverSharedDriverGet(), cAtLogLevelCritical, "%s", "none");
        }
    mOneParamObjectGet(SubChannelGet, AtSdhChannel, subChannelId);
    }

/**
 * Get parent of this channel in mapping tree
 *
 * @param self This channel
 *
 * @return Parent channel
 */
AtSdhChannel AtSdhChannelParentChannelGet(AtSdhChannel self)
    {
    mNoParamObjectGet(ParentChannelGet, AtSdhChannel);
    }

/**
 * This function is used to create TTI data structure from TTI mode and message.
 *
 * @param mode TTI mode
 * @param message Message
 * @param len Message length
 * @param [out] tti @ref tAtSdhTti "TTI message"
 *
 * @return tti if message mode is valid. Otherwise, NULL is returned.
 */
tAtSdhTti *AtSdhTtiMake(eAtSdhTtiMode mode, const uint8 *message, uint8 len, tAtSdhTti *tti)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    eBool ttiIsValid;

    /* Check if TTI mode is valid */
    ttiIsValid = (mode == cAtSdhTtiMode1Byte) || (mode == cAtSdhTtiMode16Byte) || (mode == cAtSdhTtiMode64Byte);
    if (!ttiIsValid)
        return NULL;

    /* Check length */
    if (len > cAtSdhChannelMaxTtiLength)
        return NULL;

    /* Build */
    mMethodsGet(osal)->MemInit(osal, tti, 0, sizeof(tAtSdhTti));
    tti->mode = mode;
    mMethodsGet(osal)->MemCpy(osal, tti->message, message, len);

    return tti;
    }

/**
 * Get TTI length from the input TTI mode
 *
 * @param ttiMode @ref eAtSdhTtiMode "TTI modes"
 * @return TTI length
 */
uint8 AtSdhTtiLengthForTtiMode(eAtSdhTtiMode ttiMode)
    {
    if (ttiMode == cAtSdhTtiMode1Byte)  return 1;
    if (ttiMode == cAtSdhTtiMode16Byte) return 16;
    if (ttiMode == cAtSdhTtiMode64Byte) return 64;

    return 0;
    }

/**
 * Make 16-bytes TTI message with padding
 *
 * @param [out] tti TTI message
 * @param message Message content, its length must be less than 16 bytes
 *        (because one byte is for CRC)
 * @param len Message length
 * @param padding Padding
 *
 * @return The tti data structure if success, otherwise, NULL is returned.
 */
tAtSdhTti *AtSdh16BytesTtiMake(tAtSdhTti *tti, const uint8 *message, uint8 len, uint8 padding)
    {
    uint8 messageWithPadding[16];

    /* Build a message with padding */
    AtAssert(len <= 16);
    AtOsalMemInit(messageWithPadding, (uint32)padding, sizeof(messageWithPadding));
    AtOsalMemCpy(messageWithPadding, message, len);

    /* Make a message with buffer that already has been padded */
    return AtSdhTtiMake(cAtSdhTtiMode16Byte, messageWithPadding, sizeof(messageWithPadding), tti);
    }

/**
 * Make 64-bytes TTI message with padding
 *
 * @param [out] tti TTI message
 * @param message Message content, its length must be less than 63 bytes
 *                (because two bytes are for CR/LF)
 * @param len Message length
 * @param padding Padding
 *
 * @return The tti data structure if success, otherwise, NULL is returned.
 */
tAtSdhTti *AtSdh64BytesTtiMake(tAtSdhTti *tti, const uint8 *message, uint8 len, uint8 padding)
    {
    uint8 messageWithPadding[64];

    /* Build a message with padding */
    AtAssert(len <= 64);
    AtOsalMemInit(messageWithPadding, (uint32)padding, sizeof(messageWithPadding));
    AtOsalMemCpy(messageWithPadding, message, len);
    messageWithPadding[62] = '\r';
    messageWithPadding[63] = '\n';

    /* Make a message with buffer that already has been padded */
    return AtSdhTtiMake(cAtSdhTtiMode64Byte, messageWithPadding, sizeof(messageWithPadding), tti);
    }

/**
 * Enable/disable TIM monitoring
 *
 * @param self This channel
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelTimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(TimMonitorEnable, enable);
    }

/**
 * Check if TIM monitoring is enabled
 *
 * @param self This channel
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtSdhChannelTimMonitorIsEnabled(AtSdhChannel self)
    {
    mAttributeGet(TimMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * Get BER controller that monitors BER for this channel
 *
 * @param self This channel
 * @return BER controller
 */
AtBerController AtSdhChannelBerControllerGet(AtSdhChannel self)
    {
    mNoParamObjectGet(BerControllerGet, AtBerController);
    }

/**
 * To get block error counter
 *
 * @param self This channel
 * @param counterType Counter type. See:
 *                    - @ref eAtSdhLineCounterType "Line counters"
 *                    - @ref eAtSdhPathCounterType "Path counters"
 *
 * @return Counter value
 */
uint32 AtSdhChannelBlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
    mOneParamAttributeGet(BlockErrorCounterGet, counterType, uint32, 0);
    }

/**
 * To read then clear block error counter
 *
 * @param self This channel
 * @param counterType Counter type. See:
 *                    - @ref eAtSdhLineCounterType "Line counters"
 *                    - @ref eAtSdhPathCounterType "Path counters"
 *
 * @return Counter value before clearing
 */
uint32 AtSdhChannelBlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
    mOneParamAttributeGet(BlockErrorCounterClear, counterType, uint32, 0);
    }

/**
 * Set overhead value to be transmitted at TX direction
 *
 * @param self This channel
 * @param overheadByte Overhead byte, see:
 *                     - @ref eAtSdhLineOverheadByte "Line overhead"
 *                     - @ref eAtSdhPathOverheadByte "Path overhead"
 * @param value Overhead value
 *
 * @return AT return code.
 */
eAtModuleSdhRet AtSdhChannelTxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 value)
    {
    mTwoParamsAttributeSet(TxOverheadByteSet, overheadByte, value);
    }

/**
 * Get overhead value at TX direction
 *
 * @param self This channel
 * @param overheadByte Overhead byte, see:
 *                     - @ref eAtSdhLineOverheadByte "Line overhead"
 *                     - @ref eAtSdhPathOverheadByte "Path overhead"
 *
 * @return Overhead byte value
 */
uint8 AtSdhChannelTxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    mOneParamAttributeGet(TxOverheadByteGet, overheadByte, uint8, 0);
    }

/**
 * Get overhead byte value received at RX direction
 *
 * @param self This channel
 * @param overheadByte Overhead byte, see:
 *                     - @ref eAtSdhLineOverheadByte "Line overhead"
 *                     - @ref eAtSdhPathOverheadByte "Path overhead"
 *
 * @return Overhead byte value received at RX direction
 */
uint8 AtSdhChannelRxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    mOneParamAttributeGet(RxOverheadByteGet, overheadByte, uint8, 0);
    }

/**
 * Check if specific overhead byte can be transmitted from CPU
 *
 * @param self This channel
 * @param overheadByte Overhead byte, see:
 *                     - @ref eAtSdhLineOverheadByte "Line overhead"
 *                     - @ref eAtSdhPathOverheadByte "Path overhead"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtSdhChannelTxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    mOneParamAttributeGet(TxOverheadByteIsSupported, overheadByte, eBool, cAtFalse);
    }

/**
 * Check if specific overhead byte can be captured for CPU reading
 *
 * @param self This channel
 * @param overheadByte Overhead byte, see:
 *                     - @ref eAtSdhLineOverheadByte "Line overhead"
 *                     - @ref eAtSdhPathOverheadByte "Path overhead"
 *
 * @retval cAtTrue if supported
 * @retval cAtFalse if not supported
 */
eBool AtSdhChannelRxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    mOneParamAttributeGet(RxOverheadByteIsSupported, overheadByte, eBool, cAtFalse);
    }

/**
 * For some applications that do not use the AtDeviceInterruptProcess to process
 * interrupt and they may use AtChannelAlarmGet/AtChannelAlarmInterruptGet/AtChannelAlarmInterruptClear.
 * But doing so, there may be lost of events because both hardware and software
 * tasks run concurrently, so sticky writing by software and sticky updating by
 * hardware may conflict each other.
 *
 * So, the purpose of this API is to ask hardware return both changed alarms
 * along with current status, the "read2Clear" is to tell hardware that whether
 * interrupt should be cleared by hardware after reading. By this way, only writing
 * action happens inside hardware and conflict will not happen.
 *
 * When AtChannelAlarmGet/AtChannelAlarmInterruptGet/AtChannelAlarmInterruptClear
 * are used, the code that process interrupt may have following calling sequence:
 *      currentAlarms = AtChannelAlarmGet();
 *      changedAlarms = AtChannelAlarmInterruptGet();
 *      Examine currentAlarms and changedAlarms to see what changed alarms and their current status
 *      AtChannelAlarmInterruptClear();
 *
 * When this API is used, the above sequence should be changed to:
 *      AtSdhChannelInterruptAndAlarmGet(channel, &changedAlarms, &currentAlarms, cAtTrue);
 *      Examine currentAlarms and changedAlarms to see what changed alarms and their current status
 * By inputing read2Clear = cAtTrue, it asks hardware clear interrupt after reading.
 * So AtChannelAlarmInterruptClear() is not necessary to be called to avoid conflicting.
 *
 * @param self This channel
 * @param [out] changedAlarms Changed alarms
 * @param [out] currentStatus Alarm current status
 * @param read2Clear Read to clear
 *                   - cAtTrue: ask hardware clear interrupt after reading.
 *                   - cAtFalse: ask hardware only return interrupt status.
 *
 * @return AT return code.
 */
eAtRet AtSdhChannelInterruptAndAlarmGet(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear)
    {
    if (self)
        return mMethodsGet(self)->InterruptAndAlarmGet(self, changedAlarms, currentStatus, read2Clear);
    return cAtError;
    }

/**
 * See description of AtSdhChannelInterruptAndAlarmGet().
 *
 * @param self This channel
 * @param [out] changedDefects Changed defects
 * @param [out] currentStatus Defect current status
 * @param read2Clear Read to clear
 *                   - cAtTrue: ask hardware clear interrupt after reading.
 *                   - cAtFalse: ask hardware only return interrupt status.
 *
 * @return AT return code.
 */
eAtRet AtSdhChannelInterruptAndDefectGet(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear)
    {
    if (self)
        return mMethodsGet(self)->InterruptAndDefectGet(self, changedDefects, currentStatus, read2Clear);
    return cAtError;
    }

/**
 * Enable/disable RDI auto generating when critical defects happen
 *
 * @param self This channel
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtSdhChannelAutoRdiEnable(AtSdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(AutoRdiEnable, enable);
    }

/**
 * Check if RDI auto generating when critical defects happen
 *
 * @param self This channel
 *
 * @retval cAtTrue if RDI auto backward is enabled
 * @retval cAtFalse if RDI auto backward is disabled
 */
eBool AtSdhChannelAutoRdiIsEnabled(AtSdhChannel self)
    {
    mAttributeGet(AutoRdiIsEnabled, eBool, cAtFalse);
    }

/**
 * Switch the dropped traffic from one channel to other channel.
 *
 * @param self This channel. It is dropping traffic.
 * @param otherChannel The other channel that will drop its traffic to the
 *        terminated resources that being dropped by the 'self'
 *
 * @return AT return code
 */
eAtRet AtSdhChannelRxTrafficSwitch(AtSdhChannel self, AtSdhChannel otherChannel)
    {
    mObjectSet(RxTrafficSwitch, otherChannel);
    }

/**
 * Bridge the added traffic to other channel
 *
 * @param self This channel which is adding traffic
 * @param otherChannel The other channel that the added traffic of 'self' will
 *                     be duplicated to.
 *
 * @return AT return code.
 */
eAtRet AtSdhChannelTxTrafficBridge(AtSdhChannel self, AtSdhChannel otherChannel)
    {
    mObjectSet(TxTrafficBridge, otherChannel);
    }

/**
 * Cut the traffic that is being added to a channel
 *
 * @param self This channel
 *
 * @return AT return code
 */
eAtRet AtSdhChannelTxTrafficCut(AtSdhChannel self)
    {
    mAttributeGet(TxTrafficCut, eAtRet, cAtErrorNullPointer);
    }

/**
 * Set APS hold-off timer.
 *
 * @param self SDH Channel
 * @param timerInMs Timer in milliseconds
 *
 * @return AT return code
 */
eAtRet AtSdhChannelHoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    mOneParamAttributeGet(HoldOffTimerSet, timerInMs, eAtRet, cAtErrorNullPointer);
    }

/**
 * Get APS hold-off timer
 *
 * @param self SDH Channel
 *
 * @return Hold-off timer in milliseconds
 */
uint32 AtSdhChannelHoldOffTimerGet(AtSdhChannel self)
    {
    mAttributeGet(HoldOffTimerGet, uint32, 0);
    }

/**
 * Set APS hold-on timer.
 *
 * @param self SDH Channel
 * @param timerInMs Timer in milliseconds
 *
 * @return AT return code
 */
eAtRet AtSdhChannelHoldOnTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    mOneParamAttributeGet(HoldOnTimerSet, timerInMs, eAtRet, cAtErrorNullPointer);
    }

/**
 * Get APS hold-on timer
 *
 * @param self SDH Channel
 *
 * @return Hold-on timer in milliseconds
 */
uint32 AtSdhChannelHoldOnTimerGet(AtSdhChannel self)
    {
    mAttributeGet(HoldOnTimerGet, uint32, 0);
    }

/**
 * Concatenate channels into a contiguous concatenation group.
 *
 * @param self This channel
 * @param slaveList list of slave channels
 * @param numSlaves number of slave channels
 *
 * @return AT return code.
 */
eAtRet AtSdhChannelConcate(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    eAtRet ret = cAtErrorNullPointer;

    if ((slaveList == NULL) || (numSlaves == 0))
        return cAtErrorInvlParm;

    if (AtDriverApiLogStart() && AtDriverShouldLog(cApiLogSeverity))
        {
        uint32 bufferSize;
        char *buffer, *slaveString;

        AtDriverLogLock();
        buffer = AtDriverLogSharedBufferGet(AtDriverSharedDriverGet(), &bufferSize);
        slaveString = AtObjectArrayToString((AtObject *)slaveList, numSlaves, buffer, bufferSize);
        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cApiLogSeverity,
                                AtSourceLocationNone, "API: %s([%s], [%s], %u)\r\n",
                                AtFunction, AtObjectToString((AtObject)self), slaveString, numSlaves);
        AtDriverLogUnLock();
        }

    if (self)
        ret = mMethodsGet(self)->Concate(self, slaveList, numSlaves);

    AtDriverApiLogStop();

    return ret;
    }

/**
 * De-concatenate a channel and its all slave channels from a contiguous
 * concatenation group.
 *
 * @param self This channel
 *
 * @return AT return code.
 */
eAtRet AtSdhChannelDeconcate(AtSdhChannel self)
    {
    return SdhChannelDeconcateWithLog(self);
    }

/**
 * Get slave channel at index
 *
 * @param self This channel
 * @param slaveIndex slave index
 *
 * @return slave channel.
 */
AtSdhChannel AtSdhChannelSlaveChannelAtIndex(AtSdhChannel self, uint8 slaveIndex)
    {
    if (self)
        return SlaveChannelAtIndex(self, slaveIndex);
    return NULL;
    }

/**
 * Get number of slave channels
 *
 * @param self This channel
 *
 * @return number of channels.
 */
uint8 AtSdhChannelNumSlaveChannels(AtSdhChannel self)
    {
    if (self)
        return NumSlaveChannels(self);
    return 0;
    }

/**
 * Check if the channel can change its operation mode or not
 *
 * @param self This channel
 * @param mode @ref eAtSdhChannelMode "Channel mode"
 *
 * @return cAtTrue if the channel can change the operation mode
 * @return cAtFalse if the channel cannot change the operation mode
 */
eBool AtSdhChannelCanChangeMode(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    uint8 i, numSubChannel;

    if (self == NULL)
        return cAtFalse;

    /* Can itself change ? */
    if (!mMethodsGet(self)->CanChangeMode(self, mode))
        return cAtFalse;

    /* How about it's sub-channels ? */
    numSubChannel = AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)self);
    for (i = 0; i < numSubChannel; i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet((AtSdhChannel)self, i);
        if (!AtSdhChannelCanChangeMode(subChannel, mode))
            return cAtFalse;
        }

    return cAtTrue;
    }

/**
 * Set SONET/SDH mode
 *
 * @param self This channel
 * @param mode @ref eAtSdhChannelMode "Channel mode"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    mOneParamWrapCall(mode, eAtModuleSdhRet, DoModeSet(self, mode));
    }

/**
 * Get SONET/SDH mode
 *
 * @param self This channel
 *
 * @return Mode @ref eAtSdhChannelMode "Channel mode"
 */
eAtSdhChannelMode AtSdhChannelModeGet(AtSdhChannel self)
    {
    mAttributeGetWithCache(ModeGet, eAtSdhChannelMode, cAtSdhChannelModeUnknown);
    }

/**
 * Enable/disable RDI auto generating on detecting specified alarm types
 *
 * @param self This channel
 * @param alarms Alarm types, can be ORed.
 *               - @ref eAtSdhLineAlarmType "Line alarm types"
 *               - @ref eAtSdhPathAlarmType "Path alarm types"
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhChannelAlarmAutoRdiEnable(AtSdhChannel self, uint32 alarms, eBool enable)
    {
    mTwoParamsAttributeSet(AlarmAutoRdiEnable, alarms, enable);
    }

/**
 * Check if RDI auto generating on detecting specified alarm types is enabled or not
 *
 * @param self This channel
 * @param alarms Alarm type to check. Note, only one alarm type should be input at a time.
 *                  - @ref eAtSdhLineAlarmType "Line alarm types"
 *                  - @ref eAtSdhPathAlarmType "Path alarm types"
 *
 * @retval cAtTrue if RDI auto auto generating is enabled
 * @retval cAtFalse if RDI auto generating is disabled
 */
eBool AtSdhChannelAlarmAutoRdiIsEnabled(AtSdhChannel self, uint32 alarms)
    {
    mOneParamAttributeGet(AlarmAutoRdiIsEnabled, alarms, eBool, cAtFalse);
    }

/**
 * Enable/disable REI auto generating when BIP errors are detected
 *
 * @param self This channel
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtSdhChannelAutoReiEnable(AtSdhChannel self, eBool enable)
    {
    mNumericalAttributeSet(AutoReiEnable, enable);
    }

/**
 * Check if REI auto generating when BIP errors are detected
 *
 * @param self This channel
 *
 * @retval cAtTrue if REI auto backward is enabled
 * @retval cAtFalse if REI auto backward is disabled
 */
eBool AtSdhChannelAutoReiIsEnabled(AtSdhChannel self)
    {
    mAttributeGet(AutoReiIsEnabled, eBool, cAtFalse);
    }

/** @} */

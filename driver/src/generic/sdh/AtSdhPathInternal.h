/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhPathInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description : SDH Path class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHPATHINTERNAL_H_
#define _ATSDHPATHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannelInternal.h"
#include "AtSdhPath.h"
#include "../../../include/aps/AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhPathCache
    {
    tAtSdhChannelCache super;

    /* Additional attributes */
    uint8 txPsl, expectedPsl;
    uint8 txSs, expectedSs, ssCompared;
    uint8 erdiEnabled;
    }tAtSdhPathCache;

typedef struct tAtSdhPathApsInfo
    {
    AtApsEngine apsEngine;
    AtList selectors;          /* Join to APS selectors as working/protection role */
    AtApsSelector apsSelector; /* Protected by APS selector */
    }tAtSdhPathApsInfo;

/* Methods */
typedef struct tAtSdhPathMethods
    {
    /* Path signal label */
    eBool (*PslValueIsValid)(AtSdhPath self, uint8 psl);
    eAtModuleSdhRet (*TxPslSet)(AtSdhPath self, uint8 psl);
    uint8 (*TxPslGet)(AtSdhPath self);
    uint8 (*RxPslGet)(AtSdhPath self);
    eAtModuleSdhRet (*ExpectedPslSet)(AtSdhPath self, uint8 psl);
    uint8 (*ExpectedPslGet)(AtSdhPath self);

    /* SS bits */
    eAtModuleSdhRet (*TxSsSet)(AtSdhPath self, uint8 value);
    uint8 (*TxSsGet)(AtSdhPath self);
    eAtModuleSdhRet (*ExpectedSsSet)(AtSdhPath self, uint8 value);
    uint8 (*ExpectedSsGet)(AtSdhPath self);
    eAtModuleSdhRet (*SsCompareEnable)(AtSdhPath self, eBool enable);
    eBool (*SsCompareIsEnabled)(AtSdhPath self);
    eBool (*SsBitCompareIsEnabledAsDefault)(AtSdhPath self);
    eAtRet (*SsInsertionEnable)(AtSdhPath self, uint8 value);
    eBool (*SsInsertionIsEnabled)(AtSdhPath self);

    /* Enable Enhanced RDI */
    eAtModuleSdhRet (*ERdiEnable)(AtSdhPath self, eBool enable);
    eBool (*ERdiIsEnabled)(AtSdhPath self);
    eBool (*ERdiDatabaseIsEnabled)(AtSdhPath self);

    eAtModuleSdhRet (*PlmMonitorEnable)(AtSdhPath self, eBool enable);
    eBool (*PlmMonitorIsEnabled)(AtSdhPath self);

    /* Automatic protection */
    eAtModuleSdhRet (*ApsSwitchingConditionSet)(AtSdhPath self, uint32 defects, eAtApsSwitchingCondition condition);
    eAtApsSwitchingCondition (*ApsSwitchingConditionGet)(AtSdhPath self, uint32 defect);
    eAtModuleSdhRet (*ApsForcedSwitchingConditionSet)(AtSdhPath self, eAtApsSwitchingCondition condition);
    eAtApsSwitchingCondition (*ApsForcedSwitchingConditionGet)(AtSdhPath self);

    /* Internal */
    eBool (*HasPohProcessor)(AtSdhPath self);
    eBool (*HasPointerProcessor)(AtSdhPath self);
    eAtRet (*PohMonitorEnable)(AtSdhPath self, eBool enabled);
    eBool (*PohMonitorIsEnabled)(AtSdhPath self);
    tAtSdhPathApsInfo* (*ApsInfoGet)(AtSdhPath self);
    eBool (*UpsrIsApplicable)(AtSdhPath self);
    eAtRet (*PohInsertionEnable)(AtSdhPath self, eBool enabled);
    eBool (*PohInsertionIsEnabled)(AtSdhPath self);

    /* Interrupt process */
    void (*InterruptProcess)(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal);
    }tAtSdhPathMethods;

/* Representation */
typedef struct tAtSdhPath
    {
    tAtSdhChannel super;
    const tAtSdhPathMethods *methods;
    }tAtSdhPath;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhPath AtSdhPathObjectInit(AtSdhPath self, uint32 channelId, uint8 channelType, AtModuleSdh module);

eBool AtSdhPathHasPohProcessor(AtSdhPath self);
eBool AtSdhPathHasPointerProcessor(AtSdhPath self);
void AtSdhPathInterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal);
eAtRet AtSdhPathPohMonitorEnable(AtSdhPath self, eBool enabled);
eBool AtSdhPathPohMonitorIsEnabled(AtSdhPath self);
eAtRet AtSdhPathPohInsertionEnable(AtSdhPath self, eBool enabled);
eBool AtSdhPathPohInsertionIsEnabled(AtSdhPath self);
eBool AtSdhPathERdiDatabaseIsEnabled(AtSdhPath self);
void AtSdhPathApsEngineSet(AtSdhPath self, AtApsEngine engine);
void AtSdhPathApsSelectorJoin(AtSdhPath self, AtApsSelector selector);
void AtSdhPathApsSelectorLeave(AtSdhPath self, AtApsSelector selector);
AtList AtSdhPathApsSelectors(AtSdhPath self);
void AtSdhPathApsSelectorSet(AtSdhPath self, AtApsSelector selector);
AtApsSelector AtSdhPathApsSelectorGet(AtSdhPath self);
eBool AtSdhPathSsBitCompareIsEnabledAsDefault(AtSdhPath self);

eBool AtSdhPathUpsrIsApplicable(AtSdhPath self);
eBool AtSdhPathCounterIsPointerAdjust(uint16 counterType);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHPATHINTERNAL_H_ */


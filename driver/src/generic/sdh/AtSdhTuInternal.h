/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhTuInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description : SDH TU
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHTUINTERNAL_H_
#define _ATSDHTUINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhPathInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Representation */
typedef struct tAtSdhTu
    {
    tAtSdhPath super;

    void *cache;
    }tAtSdhTu;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhTu AtSdhTuObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHTUINTERNAL_H_ */


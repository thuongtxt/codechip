/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhStsInternal.h
 * 
 * Created Date: May 17, 2017
 *
 * Description : SDH STS-1 class.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHSTSINTERNAL_H_
#define _ATSDHSTSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannelInternal.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhSts
    {
    tAtSdhChannel super;

    /* Private data */
    AtStsGroup stsGroup;
    }tAtSdhSts;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhSts AtSdhStsObjectInit(AtSdhSts self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhSts AtSdhStsNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
void AtSdhStsGroupSet(AtSdhSts self, AtStsGroup group);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHSTSINTERNAL_H_ */


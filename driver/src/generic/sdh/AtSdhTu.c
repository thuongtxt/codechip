/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhTu.c
 *
 * Created Date: Sep 4, 2012
 *
 * Description : SDH TU class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "../../util/coder/AtCoderUtil.h"
#include "AtSdhTuInternal.h"
#include "AtModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSdhTu)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSts(AtSdhChannel self)
    {
	AtUnused(self);
    return 1;
    }

static uint8 DefaultSsValue(AtSdhPath path)
    {
    eAtSdhChannelType tuType = AtSdhChannelTypeGet((AtSdhChannel)path);

    if (tuType == cAtSdhChannelTypeTu12) return 0x2;
    if (tuType == cAtSdhChannelTypeTu11) return 0x3;
    if (tuType == cAtSdhChannelTypeTu3)  return 0x2;

    return 0x0;
    }

static eBool ShouldCompareSsBit(AtSdhPath self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    return AtModuleSdhShouldCompareTuSsBit(sdhModule, self);
    }

static eAtRet DefaultSsSet(AtSdhPath path)
    {
    eAtRet ret = cAtOk;
	uint8 ssValue = DefaultSsValue(path);

    ret |= AtSdhPathTxSsSet(path, ssValue);
    ret |= AtSdhPathExpectedSsSet(path, ssValue);
    ret |= AtSdhPathSsCompareEnable(path, ShouldCompareSsBit(path));

    return ret;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
	AtUnused(mapType);
	AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtSdhTuDefaultSsSet((AtSdhPath)self);
    }

static AtSdhChannel VcGet(AtChannel sdhChannel)
    {
    return AtSdhChannelSubChannelGet((AtSdhChannel)sdhChannel, 0);
    }

static uint32 DefectGet(AtChannel self)
    {
    return AtChannelDefectGet((AtChannel)VcGet(self));
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return AtChannelDefectInterruptGet((AtChannel)VcGet(self));
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return AtChannelDefectInterruptClear((AtChannel)VcGet(self));
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return AtChannelCounterGet((AtChannel)VcGet(self), counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return AtChannelCounterClear((AtChannel)VcGet(self), counterType);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
	return AtChannelSupportedInterruptMasks((AtChannel) VcGet(self));
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    return AtChannelInterruptMaskSet((AtChannel)VcGet(self), defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    return AtChannelInterruptMaskGet((AtChannel)VcGet(self));
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    return AtSdhChannelTimMonitorEnable(VcGet((AtChannel)self), enable);
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    return AtSdhChannelTimMonitorIsEnabled(VcGet((AtChannel)self));
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    return AtSdhChannelTtiCompareEnable(VcGet((AtChannel)self), enable);
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    return AtSdhChannelTtiCompareIsEnabled(VcGet((AtChannel)self));
    }

static const char *TypeString(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)self);

    if (channelType == cAtSdhChannelTypeTu3)     return "tu3";
    if (channelType == cAtSdhChannelTypeTu12)    return "tu12";
    if (channelType == cAtSdhChannelTypeTu11)    return "tu11";

    return "unknown";
    }

static const char *IdString(AtChannel self)
    {
    static char idString[16];
    AtChannel parent = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)self);

    if (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeTu3)
        return AtChannelIdString(parent);

    AtSprintf(idString, "%s.%u", AtChannelIdString(parent), AtChannelIdGet(self) + 1);

    return idString;
    }

static AtSdhPath TuVcGet(AtSdhPath self)
    {
    return (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    return AtSdhPathTxPslSet(TuVcGet(self), psl);
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    return AtSdhPathTxPslGet(TuVcGet(self));
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    return AtSdhPathRxPslGet(TuVcGet(self));
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    return AtSdhPathExpectedPslSet(TuVcGet(self), psl);
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    return AtSdhPathExpectedPslGet(TuVcGet(self));
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    return AtSdhChannelTxTtiGet((AtSdhChannel)TuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    return AtSdhChannelTxTtiSet((AtSdhChannel)TuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    return AtSdhChannelExpectedTtiGet((AtSdhChannel)TuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    return AtSdhChannelExpectedTtiSet((AtSdhChannel)TuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    return AtSdhChannelRxTtiGet((AtSdhChannel)TuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    return AtSdhChannelAlarmAffectingEnable((AtSdhChannel)TuVcGet((AtSdhPath)self), alarmType, enable);
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    return AtSdhChannelAlarmAffectingIsEnabled((AtSdhChannel)TuVcGet((AtSdhPath)self), alarmType);
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    return AtSdhChannelAlarmAffectingMaskGet((AtSdhChannel)TuVcGet((AtSdhPath)self));
    }

static eAtRet HoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    return AtSdhChannelHoldOffTimerSet((AtSdhChannel)TuVcGet((AtSdhPath)self), timerInMs);
    }

static uint32 HoldOffTimerGet(AtSdhChannel self)
    {
    return AtSdhChannelHoldOffTimerGet((AtSdhChannel)TuVcGet((AtSdhPath)self));
    }

static eAtRet WarmRestore(AtChannel self)
    {
    AtSdhChannelLineObjectGet((AtSdhChannel)self);

    return AtChannelWarmRestore((AtChannel)TuVcGet((AtSdhPath)self));
    }
    
static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static eBool CanHaveMapping(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtSurEngine SurEngineGet(AtChannel self)
    {
    return AtChannelSurEngineGet((AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentDefect, eBool subChannelsOnly)
    {
    if (AtSdhChannelHasAlarmForwarding(self, currentDefect))
        return;

    if (subChannelsOnly == cAtFalse)
        AtChannelAllAlarmListenersCall((AtChannel)self, cAtSdhPathAlarmAis, 0);

    m_AtSdhChannelMethods->AlarmForwardingClearanceNotifyWithSubChannelOnly(self, currentDefect, subChannelsOnly);
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohMonitorEnable(vc, enabled);
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohMonitorIsEnabled(vc);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeNone(cache);
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohInsertionEnable(vc, enabled);
    }

static eBool PohInsertionIsEnabled(AtSdhPath self)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohInsertionIsEnabled(vc);
    }

static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    return AtSdhChannelCounterModeSet((AtSdhChannel)TuVcGet((AtSdhPath)self), counterType, mode);
    }

static void OverrideAtSdhChannel(AtSdhTu self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(sdhChannel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(sdhChannel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, CanHaveMapping);
        mMethodOverride(m_AtSdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareEnable);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, CounterModeSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerGet);
        }

    mMethodsSet(sdhChannel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhTu self)
    {
    AtSdhPath path = (AtSdhPath)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtChannel(AtSdhTu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, SurEngineGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhTu self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSdhTu self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    }

AtSdhTu AtSdhTuObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhTu));

    /* Super constructor */
    if (AtSdhPathObjectInit((AtSdhPath)self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet AtSdhTuDefaultSsSet(AtSdhPath path)
    {
    if (path)
        return DefaultSsSet(path);
    return cAtErrorNullPointer;
	}

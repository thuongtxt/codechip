/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhLineInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description : SDH Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHLINEINTERNAL_H_
#define _ATSDHLINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "AtSdhSts.h"
#include "AtSdhChannelInternal.h"
#include "../aps/AtModuleApsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtSdhLineNumDccLayers 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhLineSimulationDb
    {
    tAtSdhChannelSimulationDb super;

    uint8 txK1, txK2, txS1;
    eBool b1Forced, b2Forced, reiForced;
    }tAtSdhLineSimulationDb;

/* Methods */
typedef struct tAtSdhLineMethods
    {
    /* Rate */
    eAtModuleSdhRet (*RateSet)(AtSdhLine self, eAtSdhLineRate rate);
    eBool (*RateIsSupported)(AtSdhLine self, eAtSdhLineRate rate);
    eAtSdhLineRate (*RateGet)(AtSdhLine self);
    eAtModuleSdhRet (*CanChangeRate)(AtSdhLine self, eAtSdhLineRate rate);
    void (*RateWillChange)(AtSdhLine self, eAtSdhLineRate oldRate, eAtSdhLineRate newRate);
    void (*RateDidChange)(AtSdhLine self, eAtSdhLineRate oldRate, eAtSdhLineRate newRate);

    /* Scramble */
    eAtModuleSdhRet (*ScrambleEnable)(AtSdhLine self, eBool enable);
    eBool (*ScrambleIsEnabled)(AtSdhLine self);

    /* APS */
    eAtModuleSdhRet (*Switch)(AtSdhLine self, AtSdhLine toLine);
    AtSdhLine (*SwitchedLineGet)(AtSdhLine self);
    eAtModuleSdhRet (*SwitchRelease)(AtSdhLine self);
    eAtModuleSdhRet (*Bridge)(AtSdhLine self, AtSdhLine toLine);
    AtSdhLine (*BridgedLineGet)(AtSdhLine self);
    eAtModuleSdhRet (*BridgeRelease)(AtSdhLine self);

    /* LED */
    eAtModuleSdhRet (*LedStateSet)(AtSdhLine self, eAtLedState ledState);
    eAtLedState (*LedStateGet)(AtSdhLine self);

    /* S1 */
    eAtModuleSdhRet (*TxS1Set)(AtSdhLine self, uint8 value);
    uint8 (*TxS1Get)(AtSdhLine self);
    uint8 (*RxS1Get)(AtSdhLine self);

    /* K1 */
    eAtModuleSdhRet (*TxK1Set)(AtSdhLine self, uint8 value);
    uint8 (*TxK1Get)(AtSdhLine self);
    uint8 (*RxK1Get)(AtSdhLine self);

    /* K2 */
    eAtModuleSdhRet (*TxK2Set)(AtSdhLine self, uint8 value);
    uint8 (*TxK2Get)(AtSdhLine self);
    uint8 (*RxK2Get)(AtSdhLine self);

    /* Enable/disable insert TOH (K1, K2, S1, ...) */
    eAtModuleSdhRet (*OverheadBytesInsertEnable)(AtSdhLine self, eBool enable);
    eBool (*OverheadBytesInsertIsEnabled)(AtSdhLine self);
    eBool (*HasFramer)(AtSdhLine self);

    /* BER monitoring */
    AtBerController (*RsBerControllerGet)(AtSdhLine self);
    AtBerController (*RsBerControllerCreate)(AtSdhLine self);
    AtBerController (*MsBerControllerGet)(AtSdhLine self);
    AtBerController (*MsBerControllerCreate)(AtSdhLine self);
    eAtModuleSdhRet (*BerControllersReCreate)(AtSdhLine self);
    eBool (*HasBerControllers)(AtSdhLine self);

    /* Serdes */
    AtSerdesController (*SerdesController)(AtSdhLine self);
    eBool (*ShouldInitSerdesController)(AtSdhLine self);

    AtSdhAu (*Au4_ncGet)(AtSdhLine self, uint8 aug1Id);
    AtSdhVc (*Vc4_ncGet)(AtSdhLine self, uint8 aug1Id);

    /* Interrupt process */
    void (*InterruptProcess)(AtSdhLine self, uint8 line, AtHal hal);

    eBool (*CanSetTxK1)(AtSdhLine self);
    eBool (*CanSetTxK2)(AtSdhLine self);
    eBool (*CanSetTxS1)(AtSdhLine self);

    /* Zn monitoring */
    eAtRet (*Z1MonitorPositionSet)(AtSdhLine self, uint8 sts1);
    uint8 (*Z1MonitorPositionGet)(AtSdhLine self);
    eBool (*Z1CanMonitor)(AtSdhLine self);
    eAtRet (*Z2MonitorPositionSet)(AtSdhLine self, uint8 sts1);
    uint8 (*Z2MonitorPositionGet)(AtSdhLine self);
    eBool (*Z2CanMonitor)(AtSdhLine self);

    /* Line prbs engine */
    AtPrbsEngine (*PrbsEngineGet)(AtSdhLine self);

    /* DCC HDLC cleanup */
    eAtRet (*DccHdlcCleanup)(AtSdhLine line);

    /* For products that have XC hiding logic */
    AtSdhLine (*HideLineGet)(AtSdhLine self, uint8 sts1);
    }tAtSdhLineMethods;

typedef struct tAtSdhLineCache
    {
    tAtSdhChannelCache super;
    uint8 rate;
    uint8 txS1, txK1, txK2;
    }tAtSdhLineCache;

/* Representation */
typedef struct tAtSdhLine
    {
    tAtSdhChannel super;
    const tAtSdhLineMethods *methods;

    /* Private */
    AtBerController msBerController;
    AtBerController rsBerController;
    AtApsEngine apsEngine;
    AtHdlcChannel dccChannel[cAtSdhLineNumDccLayers];
    uint8 dccLayersMask;
    AtSdhSts *allSts1s;

    /* For CPU protection */
    void *cache;
    }tAtSdhLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine AtSdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);

/* BER controllers */
AtBerController SdhLineRsBerControllerGet(AtSdhLine self);
AtBerController SdhLineMsBerControllerGet(AtSdhLine self);

/* Linear APS */
AtApsEngine AtSdhLineApsEngineGet(AtSdhLine self);
void AtSdhLineApsEngineSet(AtSdhLine self, AtApsEngine engine);

/* Interrupt process */
void AtSdhLineInterruptProcess(AtSdhLine self, uint8 line, AtHal hal);

eBool AtSdhLineCanSetTxK1(AtSdhLine self);
eBool AtSdhLineCanSetTxK2(AtSdhLine self);
eBool AtSdhLineCanSetTxS1(AtSdhLine self);
eBool AtSdhLineHasFramer(AtSdhLine self);
eAtRet AtSdhLineUnuse(AtSdhLine self);

/* To work with database */
eAtModuleSdhRet AtSdhLineSoftwareRateSet(AtSdhLine self, eAtSdhLineRate rate);

/* To get overhead byte naming by row/column */
eAtSdhLineOverheadByte AtSdhLineOverheadByteGet(AtSdhLine self, uint32 row, uint32 column);

/*DCC HDLC clean up */
eAtRet AtSdhLineDccHdlcCleanup(AtSdhLine self);

/* Work with hiding XC */
AtSdhLine AtSdhLineHideLineGet(AtSdhLine self, uint8 sts1);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHLINEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtStsGroupInternal.h
 * 
 * Created Date: Nov 17, 2016
 *
 * Description : STS pass-through group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTSGROUPINTERNAL_H_
#define _ATSTSGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtStsGroupMethods
    {
    eAtRet (*StsAdd)(AtStsGroup self, AtSdhSts sts1);
    eAtRet (*StsDelete)(AtStsGroup self, AtSdhSts sts1);
    eAtRet (*DestGroupSet)(AtStsGroup self, AtStsGroup destGroup);
    eAtRet (*PassThroughEnable)(AtStsGroup self, eBool enabled);
    eBool  (*PassThroughIsEnabled)(AtStsGroup self);
    }tAtStsGroupMethods;

typedef struct tAtStsGroup
    {
    tAtChannel super;
    tAtStsGroupMethods *methods;

    /* private */
    AtStsGroup destGroup;
    AtStsGroup srcGroup;
    AtList stsList;
    }tAtStsGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtStsGroup AtStsGroupObjectInit (AtStsGroup self, uint32 groupId, AtModule module);

void AtStsGroupSrcGroupSet(AtStsGroup self, AtStsGroup srcGroup);
AtStsGroup AtStsGroupSrcGroupGet(AtStsGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSTSGROUPINTERNAL_H_ */


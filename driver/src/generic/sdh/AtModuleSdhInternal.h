/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhModuleInternal.h
 * 
 * Created Date: Jul 26, 2012
 *
 * Description : SDH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHMODULEINTERNAL_H_
#define _ATSDHMODULEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDrp.h"
#include "AtModuleSdh.h"
#include "AtSdhLine.h"
#include "../man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* SDH Module implementation */
typedef struct tAtModuleSdhMethods
    {
    /* Methods */
    uint8 (*MaxLinesGet) (AtModuleSdh self);
    AtSdhLine (*LineGet) (AtModuleSdh self, uint8 lineId);
    uint32 (*MaxLineRate)(AtModuleSdh self);
    eBool (*LineCanBeUsed)(AtModuleSdh self, uint8 lineId);
    eAtSdhChannelMode (*LineDefaultMode)(AtModuleSdh self, AtSdhLine line);

    /* TFI-5 lines management */
    uint8 (*NumTfi5Lines)(AtModuleSdh self);
    AtSdhLine (*Tfi5LineGet)(AtModuleSdh self, uint8 tfi5LineId);

    /* Channel factory */
    AtSdhChannel (*ChannelCreate)(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId);

    /* SS bit is enable as default */
    eBool (*SsBitCompareIsEnabledAsDefault)(AtModuleSdh self);
    eBool (*ShouldCompareTuSsBit)(AtModuleSdh self, AtSdhPath tu);

    /* Counter mode */
    eBool (*BitErrorCounterModeAsDefault)(AtModuleSdh self);

    uint8 (*DbLineIdFromLineId)(AtModuleSdh self, uint8 lineId);
    uint8 (*LineIdFromDbLineId)(AtModuleSdh self, uint8 dbLineId);
    uint32 (*StartEc1LineIdGet)(AtModuleSdh self);
    uint32 (*NumEc1LinesGet)(AtModuleSdh self);
    eBool (*NeedClearanceNotifyLogic)(AtModuleSdh self);

    eAtSdhTtiMode (*DefaultTtiMode)(AtModuleSdh self);

    /* Capability */
    eBool (*LineRateIsSupported)(AtModuleSdh self, AtSdhLine line, eAtSdhLineRate rate);
    eBool (*LineHasFramer)(AtModuleSdh self, AtSdhLine line);
    eBool (*ChannelConcateIsSupported)(AtModuleSdh self, uint8 channelType, uint8 numChannels);
    
    /* DRP */
    AtDrp (*SerdesDrp)(AtModuleSdh self, uint32 serdesId);
    AtDrp (*SerdesDrpCreate)(AtModuleSdh self, uint32 serdesId);
    uint32 (*SerdesDrpBaseAddress)(AtModuleSdh self);
    eBool (*SerdesHasDrp)(AtModuleSdh self, uint32 serdesId);

    /* DCC */
    AtHdlcChannel (*DccChannelCreate)(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layer);
    eBool (*DccLayerIsSupported)(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layer);

    AtObjectAny (*AttManager)(AtModuleSdh pdh);
    AtAttSdhManager (*AttSdhManagerCreate)(AtModuleSdh sdh);
    }tAtModuleSdhMethods;

/* SDH Module representation */
typedef struct tAtModuleSdh
    {
    tAtModule super;
    const tAtModuleSdhMethods *methods;

    /* Private data */
    AtSdhLine *allLines;
    AtDrp serdesDrp;
    AtAttSdhManager att;
    }tAtModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtModuleSdh AtModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
void AtSdhAllLinesDelete(AtModuleSdh self);

AtSdhChannel AtModuleSdhChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId);
eAtSdhTtiMode AtModuleSdhDefaultTtiMode(AtModuleSdh self);
AtDrp AtModuleSdhSerdesDrp(AtModuleSdh self, uint32 serdesId);
eBool AtModuleSdhLineCanBeUsed(AtModuleSdh self, uint8 lineId);
eAtRet AtModuleSdhLineUnuse(AtModuleSdh self, uint8 lineId);
eBool AtModuleSdhShouldCompareTuSsBit(AtModuleSdh self, AtSdhPath tu);
eBool AtModuleSdhNeedClearanceNotifyLogic(AtModuleSdh self);
eAtSdhChannelMode AtModuleSdhLineDefaultMode(AtModuleSdh self, AtSdhLine line);

/* Capacity */
eBool AtModuleSdhLineRateIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineRate rate);
eBool AtModuleSdhLineHasFramer(AtModuleSdh self, AtSdhLine line);
eBool AtModuleSdhChannelConcateIsSupported(AtModuleSdh self, uint8 channelType, uint8 numChannels);
eBool AtModuleSdhShouldUseBitErrorCounter(AtModuleSdh self);

/* DCC */
eBool AtModuleSdhDccLayerIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layers);
AtHdlcChannel AtModuleSdhDccChannelCreate(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layers);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHMODULEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhModule.c
 *
 * Created Date: Jul 26, 2012
 *
 * Author      : namnn
 *
 * Description : SONET/SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../util/coder/AtCoderUtil.h"
#include "AtModuleSdhInternal.h"
#include "AtSdhLineInternal.h"
#include "AtModuleClock.h"
#include "AtClockExtractor.h"
#include "AtSdhChannelInternal.h"

#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mLineIdIsValid(self, lineId) (lineId < mMethodsGet(self)->MaxLinesGet(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static tAtModuleSdhMethods m_methods;
static char m_methodsInit = 0;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectImplement = NULL;
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static AtAttSdhManager AttSdhManagerCreate(AtModuleSdh self)
    {
    AtUnused(self);
    return NULL;
    }

void AtSdhAllLinesDelete(AtModuleSdh self)
    {
    uint8 numLines, i;
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule berModule;

    /* Do nothing if there is no created line */
    if (self->allLines == NULL)
        return;

    /* Polling tasks may be running, need to lock related modules */
    berModule = AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleBer);
    if (berModule != NULL)
    AtModuleLock(berModule);
    AtModuleLock((AtModule)self);

    /* Delete all line objects first */
    numLines = mMethodsGet(self)->MaxLinesGet(self);
    for (i = 0; i < numLines; i++)
        AtObjectDelete((AtObject)(self->allLines[i]));

    /* Delete memory that holds all Line objects */
    mMethodsGet(osal)->MemFree(osal, self->allLines);
    self->allLines = NULL;

    AtModuleUnLock((AtModule)self);

    if (berModule)
    AtModuleUnLock(berModule);
    }

static uint8 DbLineIdFromLineId(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    return lineId;
    }

static uint8 LineIdFromDbLineId(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    return lineId;
    }

static AtSdhLine LineCreate(AtModuleSdh self, uint8 lineId)
    {
    return (AtSdhLine)mMethodsGet(self)->ChannelCreate(self, lineId, NULL, cAtSdhChannelTypeLine, lineId);
    }

static eAtRet AllLinesCreate(AtModuleSdh self)
    {
    uint8 dbLineId, i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 numLines = AtModuleSdhMaxLinesGet(self);
    uint32 memorySize = sizeof(AtSdhLine) * numLines;
    AtSdhLine *allLines;

    /* Allocate enough memory */
    allLines = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allLines == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allLines, 0, memorySize);

    /* Then create all Line objects */
    for (dbLineId = 0; dbLineId < numLines; dbLineId++)
        {
        uint8 lineId = mMethodsGet(self)->LineIdFromDbLineId(self, dbLineId);

        if (!mMethodsGet(self)->LineCanBeUsed(self, lineId))
            continue;

        allLines[dbLineId] = LineCreate(self, lineId);

        /* Cannot create one line, free all of created resources */
        if (allLines[dbLineId] == NULL)
            {
            for (i = 0; i < dbLineId; i++)
                AtObjectDelete((AtObject)allLines[i]);
            mMethodsGet(osal)->MemFree(osal, allLines);

            return cAtErrorRsrcNoAvail;
            }
        }

    /* Creating done */
    self->allLines = allLines;
    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    AtModuleSdh sdhModule = (AtModuleSdh)self;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    /* Delete all existing lines */
    if (sdhModule->allLines)
        AtSdhAllLinesDelete(sdhModule);

    /* Lines depend on version of FPGA so can not be created at this time
     * They will be created when they are needed, and at that time
     * we have already had enough version information */
    return cAtOk;
    }

static eAtRet AllChannelsInit(AtModuleSdh self)
    {
    uint8 lineId, numLines;
    eAtRet ret = cAtOk;

    /* No line to initialize */
    if (self->allLines == NULL)
        return cAtOk;

    numLines = AtModuleSdhMaxLinesGet(self);
    for (lineId = 0; lineId < numLines; lineId++)
        {
        if (self->allLines[lineId])
            ret |= AtChannelInit((AtChannel)(self->allLines[lineId]));
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    /* Super init */
    eAtRet ret = m_AtModuleMethods->Init(self);
    AtAttSdhManager mngr = AtModuleSdhAttManager((AtModuleSdh)self);

    if (ret != cAtOk)
        return ret;

    /* Initialize all managed channels */
    ret = AllChannelsInit((AtModuleSdh)self);

    /* ATT SDH init */
    if (mngr)
        ret |= AtAttSdhManagerInit(mngr);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void Delete(AtObject self)
    {
    /* Delete all lines */
    AtSdhAllLinesDelete((AtModuleSdh)self);

    if (((AtModuleSdh)self)->att)
        AtObjectDelete((AtObject)((AtModuleSdh)self)->att);
    ((AtModuleSdh)self)->att = NULL;

    /* Super deletion */
    m_AtObjectImplement->Delete(self);
    }

static AtDrp SerdesDrp(AtModuleSdh self, uint32 serdesId)
    {
    if (!mMethodsGet(self)->SerdesHasDrp(self, serdesId))
        return NULL;

    if (self->serdesDrp == NULL)
        self->serdesDrp = mMethodsGet(self)->SerdesDrpCreate(self, serdesId);

    return self->serdesDrp;
    }

static AtDrp DefaultSerdesDrpCreate(AtModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    AtDrp drp = AtDrpNew(mMethodsGet(self)->SerdesDrpBaseAddress(self), hal);
    AtDrpNumberOfSerdesControllersSet(drp, 1);
    return drp;
    }

static AtDrp SerdesDrpCreate(AtModuleSdh self, uint32 serdesId)
    {
    AtUnused(serdesId);
    return DefaultSerdesDrpCreate(self);
    }

static uint32 SerdesDrpBaseAddress(AtModuleSdh self)
    {
    AtUnused(self);
    return cBit31_0;
    }

static eBool SerdesHasDrp(AtModuleSdh self, uint32 serdesId)
    {
    AtUnused(self);
    AtUnused(serdesId);
    return cAtFalse;
    }

/* Implement methods */
static uint8 MaxLinesGet(AtModuleSdh self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint32 MaxLineRate(AtModuleSdh self)
    {
	AtUnused(self);
    return cAtSdhLineRateStm1;
    }

static AtSdhLine LineGet(AtModuleSdh self, uint8 lineId)
    {
    uint8 dbLineId = mMethodsGet(self)->DbLineIdFromLineId(self, lineId);
    if (!mLineIdIsValid(self, dbLineId))
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "SDH Line %d does not exist\r\n", lineId);
        return NULL;
        }

    if (self->allLines)
        {
        if (self->allLines[dbLineId] == NULL)
            self->allLines[dbLineId] = LineCreate(self, dbLineId);
        return self->allLines[dbLineId];
        }

    /* Database has not been created, create */
    if (AllLinesCreate(self) == cAtOk)
        return self->allLines[dbLineId];

    return NULL;
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    AtUnused(lineId);
	AtUnused(channelId);
	AtUnused(channelType);
	AtUnused(parent);
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eBool SsBitCompareIsEnabledAsDefault(AtModuleSdh self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static const char *TypeString(AtModule self)
    {
	AtUnused(self);
    return "sdh";
    }

static const char *CapacityDescription(AtModule self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)self;
    static char string[32];

    AtSprintf(string, "lines: %d", AtModuleSdhMaxLinesGet(moduleSdh));
    return string;
    }

static void StatusClear(AtModule self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint8 dbLineId;

    m_AtModuleMethods->StatusClear(self);

    for (dbLineId = 0; dbLineId < AtModuleSdhMaxLinesGet(sdhModule); dbLineId++)
        {
        uint8 lineId = mMethodsGet(sdhModule)->LineIdFromDbLineId(sdhModule, dbLineId);
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
        AtChannelStatusClear((AtChannel)line);
        }
    }

static eAtRet WarmRestore(AtModule self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint8 dbLineId;
    eAtRet ret = cAtOk;

    for (dbLineId = 0; dbLineId < AtModuleSdhMaxLinesGet(sdhModule); dbLineId++)
        {
        uint8 lineId = mMethodsGet(sdhModule)->LineIdFromDbLineId(sdhModule, dbLineId);
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
        ret |= AtChannelWarmRestore((AtChannel)line);
        }

    return ret;
    }

static AtClockExtractor ClockExtractor(AtModuleSdh self, uint8 refOutId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleClock clockModule = (AtModuleClock)AtDeviceModuleGet(device, cAtModuleClock);
    return AtModuleClockExtractorGet(clockModule, refOutId);
    }

static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static AtSdhLine Tfi5LineGet(AtModuleSdh self, uint8 tfi5LineId)
    {
    AtUnused(self);
    AtUnused(tfi5LineId);
    return NULL;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleSdh module = (AtModuleSdh)self;

    m_AtObjectImplement->Serialize(self, encoder);

    AtCoderEncodeObjects(encoder, (AtObject *)module->allLines, AtModuleSdhMaxLinesGet(module), "allLines");
    AtCoderEncodeString(encoder, AtDrpToString(module->serdesDrp), "serdesDrp");
    mEncodeNone(module->att);
    }

static void SdhChannelAllAlarmsAndErrorsUnforce(AtSdhChannel channel)
    {
    uint8 i;
    AtChannel sdhChannel = (AtChannel)channel;

    if (channel == NULL)
        return;

    if (AtChannelTxForcableAlarmsGet(sdhChannel) != 0)
        AtChannelTxAlarmUnForce(sdhChannel, cBit31_0);

    if (AtChannelRxForcableAlarmsGet(sdhChannel) != 0)
        AtChannelRxAlarmUnForce(sdhChannel, cBit31_0);

    if (AtChannelTxForcableErrorsGet(sdhChannel) != 0)
        AtChannelTxErrorUnForce(sdhChannel, cBit31_0);

    for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet(channel); i++)
        SdhChannelAllAlarmsAndErrorsUnforce(AtSdhChannelSubChannelGet(channel, i));
    }

static eAtRet AllServicesDestroy(AtModule self)
    {
    uint8 lineId;
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    eAtRet ret = cAtOk;

    for (lineId = 0; lineId < AtModuleSdhMaxLinesGet(sdhModule); lineId++)
        SdhChannelAllAlarmsAndErrorsUnforce((AtSdhChannel)AtModuleSdhLineGet(sdhModule, lineId));

    for (lineId = 0; lineId < AtModuleSdhMaxLinesGet(sdhModule); lineId++)
        {
        AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
        if (line)
            ret |= AtChannelAllServicesDestroy((AtChannel)line);
        }

    return cAtOk;
    }

static void SdhChannelListenedDefectRecursiveClear(AtSdhChannel self)
    {
    uint8 i, numSubChannel;

    AtChannelListenedDefectClear((AtChannel)self, NULL);
    numSubChannel = AtSdhChannelNumberOfSubChannelsGet(self);
    for (i = 0; i < numSubChannel; i++)
        SdhChannelListenedDefectRecursiveClear(AtSdhChannelSubChannelGet(self, i));

    AtChannelListenedDefectClear(AtSdhChannelMapChannelGet(self), NULL);
    }

static void AllChannelsListenedDefectClear(AtModule self)
    {
    uint8 lineId;
    AtModuleSdh sdhModule = (AtModuleSdh)self;

    for (lineId = 0; lineId < AtModuleSdhMaxLinesGet(sdhModule); lineId++)
        SdhChannelListenedDefectRecursiveClear((AtSdhChannel)AtModuleSdhLineGet(sdhModule, lineId));
    }

static eAtSdhTtiMode DefaultTtiMode(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhTtiMode1Byte;
    }

static eBool LineRateIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineRate rate)
    {
    /* Sub class has to determine */
    AtUnused(self);
    AtUnused(line);
    AtUnused(rate);
    return cAtFalse;
    }

static eAtRet AllChannelsInterruptDisable(AtModule self)
    {
    AtModuleSdh module = (AtModuleSdh)self;
    uint8 lineId, numLines;
    eAtRet ret = cAtOk;

    /* No line to initialize */
    if (module->allLines == NULL)
        return cAtOk;

    numLines = AtModuleSdhMaxLinesGet(module);
    for (lineId = 0; lineId < numLines; lineId++)
        {
        if (module->allLines[lineId])
            ret |= AtSdhChannelSubChannelsInterruptDisable((AtSdhChannel)(module->allLines[lineId]), cAtFalse);
        }

    return ret;
    }

static eBool LineHasFramer(AtModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtTrue;
    }

static eBool DccLayerIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(layer);
    return cAtFalse;
    }

static AtHdlcChannel DccChannelCreate(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(layer);
    return NULL;
    }

static eBool LineCanBeUsed(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    AtUnused(lineId);
    return cAtTrue;
    }

static uint32 StartEc1LineIdGet(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 NumEc1LinesGet(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet LineUnuse(AtModuleSdh self, uint8 lineId)
    {
    uint8 dbLineId;
    AtSdhLine line;
    eAtRet ret = cAtOk;

    /* No lines have been created yet */
    if (self->allLines == NULL)
        return cAtOk;

    /* This line has not been created yet */
    dbLineId = mMethodsGet(self)->DbLineIdFromLineId(self, lineId);
    line = self->allLines[dbLineId];
    if (line == NULL)
        return cAtOk;

    /* Let the line control how to free all of its hardware resource before deleting it */
    ret = AtSdhLineUnuse(line);
    ret |= AtChannelAllServicesDestroy((AtChannel)line);
    if (ret == cAtOk)
        {
        AtObjectDelete((AtObject)line);
        self->allLines[dbLineId] = NULL;
        }

    return ret;
    }

static eBool ShouldCompareTuSsBit(AtModuleSdh self, AtSdhPath tu)
    {
    AtSdhLine line = AtModuleSdhLineGet(self, AtSdhChannelLineGet((AtSdhChannel)tu));
    eAtSdhChannelMode mode = AtSdhChannelModeGet((AtSdhChannel)line);

    if (mode == cAtSdhChannelModeSdh)
        return mMethodsGet(self)->SsBitCompareIsEnabledAsDefault(self);

    return cAtFalse;
    }

static eBool NeedClearanceNotifyLogic(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtSdhChannelMode LineDefaultMode(AtModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    AtUnused(line);
    return cAtSdhChannelModeSdh;
    }

static AtObjectAny AttManager(AtModuleSdh self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool ChannelConcateIsSupported(AtModuleSdh self, uint8 channelType, uint8 numChannels)
    {
    AtUnused(self);
    AtUnused(channelType);
    AtUnused(numChannels);
    return cAtFalse;
    }

static AtQuerier QuerierGet(AtModule self)
    {
    AtUnused(self);
    return AtModuleSdhSharedQuerier();
    }

static eBool BitErrorCounterModeAsDefault(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, MaxLinesGet);
        mMethodOverride(m_methods, LineGet);
        mMethodOverride(m_methods, MaxLineRate);
        mMethodOverride(m_methods, ChannelCreate);
        mMethodOverride(m_methods, SsBitCompareIsEnabledAsDefault);
        mMethodOverride(m_methods, NumTfi5Lines);
        mMethodOverride(m_methods, Tfi5LineGet);
        mMethodOverride(m_methods, DbLineIdFromLineId);
        mMethodOverride(m_methods, LineIdFromDbLineId);
        mMethodOverride(m_methods, DefaultTtiMode);
        mMethodOverride(m_methods, LineRateIsSupported);
        mMethodOverride(m_methods, LineHasFramer);
        mMethodOverride(m_methods, SerdesDrp);
        mMethodOverride(m_methods, SerdesDrpCreate);
        mMethodOverride(m_methods, SerdesDrpBaseAddress);
        mMethodOverride(m_methods, SerdesHasDrp);
        mMethodOverride(m_methods, DccLayerIsSupported);
        mMethodOverride(m_methods, DccChannelCreate);
        mMethodOverride(m_methods, LineCanBeUsed);
        mMethodOverride(m_methods, ShouldCompareTuSsBit);
        mMethodOverride(m_methods, NeedClearanceNotifyLogic);
        mMethodOverride(m_methods, LineDefaultMode);
        mMethodOverride(m_methods, StartEc1LineIdGet);
        mMethodOverride(m_methods, NumEc1LinesGet);
        mMethodOverride(m_methods, AttManager);
        mMethodOverride(m_methods, AttSdhManagerCreate);
        mMethodOverride(m_methods, ChannelConcateIsSupported);
        mMethodOverride(m_methods, BitErrorCounterModeAsDefault);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtModuleSdh self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectImplement = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectImplement, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleSdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        mMethodOverride(m_AtModuleOverride, WarmRestore);
        mMethodOverride(m_AtModuleOverride, AllServicesDestroy);
        mMethodOverride(m_AtModuleOverride, AllChannelsListenedDefectClear);
        mMethodOverride(m_AtModuleOverride, AllChannelsInterruptDisable);
        mMethodOverride(m_AtModuleOverride, QuerierGet);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

AtModuleSdh AtModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtModuleSdh));

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleSdh, device) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

eAtSdhTtiMode AtModuleSdhDefaultTtiMode(AtModuleSdh self)
    {
    return (self) ? mMethodsGet(self)->DefaultTtiMode(self) : cAtSdhTtiMode1Byte;
    }

AtDrp AtModuleSdhSerdesDrp(AtModuleSdh self, uint32 serdesId)
    {
    if (self)
        return mMethodsGet(self)->SerdesDrp(self, serdesId);
    return NULL;
    }

eBool AtModuleSdhDccLayerIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    if (self)
        return mMethodsGet(self)->DccLayerIsSupported(self, line, layer);
    return cAtFalse;
    }

AtHdlcChannel AtModuleSdhDccChannelCreate(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layer)
    {
    if (self)
        return mMethodsGet(self)->DccChannelCreate(self, line, layer);
    return NULL;
    }

/*
 * Select what line clock source to go to External PLL
 *
 * @param self This module
 * @param refOutId The ID of clock signal comes to external PLL
 * @param source Line clock source
 *
 * @return AT return code
 */
eAtModuleSdhRet AtModuleSdhOutputClockSourceSet(AtModuleSdh self, uint8 refOutId, AtSdhLine source)
    {
    AtClockExtractor clockExtractor;
    eAtRet ret = cAtOk;

    if (self == NULL)
        return cAtError;

    clockExtractor = ClockExtractor(self, refOutId);
    if (clockExtractor == NULL)
        return cAtErrorInvlParm;

    if (source == NULL)
        ret |= AtClockExtractorEnable(clockExtractor, cAtFalse);
    else
        {
        ret |= AtClockExtractorSdhLineClockExtract(clockExtractor, source);
        ret |= AtClockExtractorEnable(clockExtractor, cAtTrue);
        }

    return ret;
    }

/*
 * Get clock source that is selected to go to external PLL
 *
 * @param self This module
 * @param refOutId The ID of clock signal comes to external PLL
 *
 * @return Line clock source
 */
AtSdhLine AtModuleSdhOutputClockSourceGet(AtModuleSdh self, uint8 refOutId)
    {
    AtClockExtractor clockExtractor;

    if (self == NULL)
        return NULL;

    clockExtractor = ClockExtractor(self, refOutId);
    if (!AtClockExtractorIsEnabled(clockExtractor))
        return NULL;

    return AtClockExtractorSdhLineGet(clockExtractor);
    }

AtSdhChannel AtModuleSdhChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    if (self)
        return mMethodsGet(self)->ChannelCreate(self, lineId, parent, channelType, channelId);
    return NULL;
    }

/*
 * Check if a rate is supported for a line
 *
 * @param self This module
 * @param self Line instance
 * @param rate @ref eAtSdhLineRate "Rate to check"
 *
 * @return cAtTrue if Line can support this rate
 * @return cAtFalse if Line can not support this rate
 */
eBool AtModuleSdhLineRateIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineRate rate)
    {
    if (self && line)
        return mMethodsGet(self)->LineRateIsSupported(self, line, rate);
    return cAtFalse;
    }

eBool AtModuleSdhLineHasFramer(AtModuleSdh self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->LineHasFramer(self, line);
    return cAtFalse;
    }

eBool AtModuleSdhLineCanBeUsed(AtModuleSdh self, uint8 lineId)
    {
    if (self)
        return mMethodsGet(self)->LineCanBeUsed(self, lineId);
    return cAtFalse;
    }

eAtRet AtModuleSdhLineUnuse(AtModuleSdh self, uint8 lineId)
    {
    if (self)
        return LineUnuse(self, lineId);
    return cAtErrorObjectNotExist;
    }

eBool AtModuleSdhShouldCompareTuSsBit(AtModuleSdh self, AtSdhPath tu)
    {
    if (self)
        return mMethodsGet(self)->ShouldCompareTuSsBit(self, tu);
    return cAtFalse;
    }

eBool AtModuleSdhNeedClearanceNotifyLogic(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->NeedClearanceNotifyLogic(self);
    return cAtFalse;
    }

eAtSdhChannelMode AtModuleSdhLineDefaultMode(AtModuleSdh self, AtSdhLine line)
    {
    if (self)
        return mMethodsGet(self)->LineDefaultMode(self, line);
    return cAtSdhChannelModeUnknown;
    }

eBool AtModuleSdhChannelConcateIsSupported(AtModuleSdh self, uint8 channelType, uint8 numChannels)
    {
    if (self)
        return mMethodsGet(self)->ChannelConcateIsSupported(self, channelType, numChannels);
    return cAtFalse;
    }

eBool AtModuleSdhShouldUseBitErrorCounter(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->BitErrorCounterModeAsDefault(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtModuleSdh
 * @{
 */
/**
 * Get maximum number of lines that this module can support
 *
 * @param self This module
 *
 * @return Maximum number of Lines
 */
uint8 AtModuleSdhMaxLinesGet(AtModuleSdh self)
    {
    mAttributeGet(MaxLinesGet, uint8, 0);
    }

/**
 * Get Line object by its ID
 *
 * @param self This module
 * @param lineId Line ID
 *
 * @return Line instance
 */
AtSdhLine AtModuleSdhLineGet(AtModuleSdh self, uint8 lineId)
    {
    mOneParamObjectGet(LineGet, AtSdhLine, lineId);
    }

/**
 * Get max line rate of all Lines
 *
 * @param self This module
 *
 * @return @ref eAtSdhLineRate "Line rate"
 */
uint32 AtModuleSdhMaxLineRate(AtModuleSdh self)
    {
    mAttributeGet(MaxLineRate, uint32, cAtSdhLineRateInvalid);
    }

/**
 * Get number of TFI-5 Lines
 *
 * @param self This module
 *
 * @return Number of TFI-5 Lines
 */
uint8 AtModuleSdhNumTfi5Lines(AtModuleSdh self)
    {
    mAttributeGet(NumTfi5Lines, uint8, 0);
    }

/**
 * Get TFI-5 Line instance
 *
 * @param self This module
 * @param tfi5LineId TFI-5 Line ID
 *
 * @return TFI-5 Line instance if success or NULL if ID is invalid
 */
AtSdhLine AtModuleSdhTfi5LineGet(AtModuleSdh self, uint8 tfi5LineId)
    {
    mOneParamObjectGet(Tfi5LineGet, AtSdhLine, tfi5LineId);
    }

/**
 * Get start EC1 line ID
 *
 * @param self This module
 * @return Start EC1 line ID
 */
uint32 AtModuleSdhStartEc1LineIdGet(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->StartEc1LineIdGet(self);

    return 0;
    }

/**
 * Get number of EC1 lines
 *
 * @param self This module
 * @return Number of EC1 line
 */
uint32 AtModuleSdhNumEc1LinesGet(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(self)->NumEc1LinesGet(self);

    return 0;
    }

/** @} */

AtObjectAny AtModuleSdhAttManager(AtModuleSdh self)
    {
    if (self)
        {
        if (self->att == NULL)
            {
            self->att = mMethodsGet(self)->AttSdhManagerCreate(self);

            if (self->att)
                AtAttSdhManagerInit(self->att);

            }
        return self->att;
        }
    return NULL;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhAug.c
 *
 * Created Date: Sep 4, 2012
 *
 * Description : AUG class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtModulePdh.h"
#include "AtSdhTug.h"
#include "AtPdhDe1.h"

#include "../pdh/AtPdhChannelInternal.h"

#include "../../util/coder/AtCoderUtil.h"
#include "AtSdhVcInternal.h"
#include "AtSdhAugInternal.h"
#include "AtChannelClasses.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSdhAugMethods m_methods;

/* Override */
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtObjectMethods     m_AtObjectOverride;

/* Save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSts(AtSdhChannel self)
    {
    eAtSdhChannelType augType = AtSdhChannelTypeGet(self);

    if (augType == cAtSdhChannelTypeAug64) return 192;
    if (augType == cAtSdhChannelTypeAug16) return 48;
    if (augType == cAtSdhChannelTypeAug4)  return 12;
    if (augType == cAtSdhChannelTypeAug1)  return 3;

    return 0;
    }

static eAtRet Aug64MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 numSubChannels = 0, subChannelType, i, sts1;
    eAtRet ret;
    AtSdhChannel subChannel;
    static const uint8 numStsInAug16    = 48;
    static const uint8 numAug16sInAug64 = 4;
    uint8 lineId = AtSdhChannelLineGet(self);

    subChannelType = cAtSdhChannelTypeUnknown;

    /* Map 4xAUG-16s */
    if (mapType == cAtSdhAugMapTypeAug64Map4xAug16s)
        {
        numSubChannels = numAug16sInAug64;
        subChannelType = cAtSdhChannelTypeAug16;
        }

    /* Map VC4-64c */
    if (mapType == cAtSdhAugMapTypeAug64MapVc4_64c)
        {
        numSubChannels = 1;
        subChannelType = cAtSdhChannelTypeAu4_64c;
        }

    if (subChannelType == cAtSdhChannelTypeUnknown)
        return cAtErrorInvlParm;

    /* Create sub channels */
    ret = SdhChannelSubChannelsCreate(self, lineId, numSubChannels, subChannelType);
    if (ret == cAtOk)
        SdhChannelMapTypeSet(self, mapType);

    /* For AU, create its VC sub channels */
    if (subChannelType == cAtSdhChannelTypeAu4_64c)
        ret |= SdhChannelSubChannelsCreate(AtSdhChannelSubChannelGet(self, 0), lineId, 1, cAtSdhChannelTypeVc4_64c);
    if (ret != cAtOk)
        return ret;

    /* Cache line ID and STS */
    if (subChannelType == cAtSdhChannelTypeAu4_64c)
        {
        /* Cache IDs for AU */
        subChannel = AtSdhChannelSubChannelGet(self, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));

        /* Cache IDs for VC */
        subChannel = AtSdhChannelSubChannelGet(subChannel, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
        }
    else
        {
        for (i = 0; i < numSubChannels; i++)
            {
            subChannel = AtSdhChannelSubChannelGet(self, i);
            sts1 = AtSdhChannelSts1Get(self);
            SdhChannelSts1IdSet(subChannel, (uint8)(sts1 + (i * numStsInAug16)));
            SdhChannelLineIdSet(subChannel, (uint8)AtSdhChannelLineGet(self));
            }
        }

    return ret;

    }

static eAtRet Aug16MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 numSubChannels = 0, subChannelType, i, sts1;
    eAtRet ret;
    AtSdhChannel subChannel;
    static const uint8 numStsInAug4    = 12;
    static const uint8 numAug4sInAug16 = 4;
    uint8 lineId = AtSdhChannelLineGet(self);

    subChannelType = cAtSdhChannelTypeUnknown;

    /* Map 4xAUG-4s */
    if (mapType == cAtSdhAugMapTypeAug16Map4xAug4s)
        {
        numSubChannels = numAug4sInAug16;
        subChannelType = cAtSdhChannelTypeAug4;
        }

    /* Map VC */
    if (mapType == cAtSdhAugMapTypeAug16MapVc4_16c)
        {
        numSubChannels = 1;
        subChannelType = cAtSdhChannelTypeAu4_16c;
        }

    if (subChannelType == cAtSdhChannelTypeUnknown)
        return cAtErrorInvlParm;

    /* Create sub channels */
    ret = SdhChannelSubChannelsCreate(self, lineId, numSubChannels, subChannelType);
    if (ret == cAtOk)
        SdhChannelMapTypeSet(self, mapType);

    /* For AU, create its VC sub channels */
    if (subChannelType == cAtSdhChannelTypeAu4_16c)
        ret |= SdhChannelSubChannelsCreate(AtSdhChannelSubChannelGet(self, 0), lineId, 1, cAtSdhChannelTypeVc4_16c);
    if (ret != cAtOk)
        return ret;

    /* Cache line ID and STS */
    if (subChannelType == cAtSdhChannelTypeAu4_16c)
        {
        /* Cache IDs for AU */
        subChannel = AtSdhChannelSubChannelGet(self, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));

        /* Cache IDs for VC */
        subChannel = AtSdhChannelSubChannelGet(subChannel, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
        }
    else
        {
        for (i = 0; i < numSubChannels; i++)
            {
            subChannel = AtSdhChannelSubChannelGet(self, i);
            sts1 = AtSdhChannelSts1Get(self);
            SdhChannelSts1IdSet(subChannel, (uint8)(sts1 + (i * numStsInAug4)));
            SdhChannelLineIdSet(subChannel, (uint8)AtSdhChannelLineGet(self));
            }
        }

    return ret;
    }

static eAtRet Aug4MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 numSubChannels = 0, subChannelType, i, aug4Sts1;
    eAtRet ret;
    AtSdhChannel subChannel;
    uint8 lineId;
    static const uint8 numStsInAug1 = 3;
    static const uint8 numAug1sIAug4 = 4;

    /* Map 4xAUG-1s */
    subChannelType = cAtSdhChannelTypeUnknown;
    if (mapType == cAtSdhAugMapTypeAug4Map4xAug1s)
        {
        numSubChannels = numAug1sIAug4;
        subChannelType = cAtSdhChannelTypeAug1;
        }

    /* Map VC */
    if (mapType == cAtSdhAugMapTypeAug4MapVc4_4c)
        {
        numSubChannels = 1;
        subChannelType = cAtSdhChannelTypeAu4_4c;
        }

    if (subChannelType == cAtSdhChannelTypeUnknown)
        return cAtErrorInvlParm;

    /* Create sub channels */
    lineId = AtSdhChannelLineGet(self);
    ret = SdhChannelSubChannelsCreate(self, lineId, numSubChannels, subChannelType);
    if (ret == cAtOk)
        SdhChannelMapTypeSet(self, mapType);

    /* For AU, create its VC sub channels */
    if (subChannelType == cAtSdhChannelTypeAu4_4c)
        ret |= SdhChannelSubChannelsCreate(AtSdhChannelSubChannelGet(self, 0), lineId, 1, cAtSdhChannelTypeVc4_4c);
    if (ret != cAtOk)
        return ret;

    /* Cache line ID and STS */
    if (subChannelType == cAtSdhChannelTypeAu4_4c)
        {
        /* Cache IDs for AU */
        subChannel = AtSdhChannelSubChannelGet(self, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));

        /* Cache IDs for VC */
        subChannel = AtSdhChannelSubChannelGet(subChannel, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
        }
    else
        {
        for (i = 0; i < numSubChannels; i++)
            {
            subChannel = AtSdhChannelSubChannelGet(self, i);
            aug4Sts1 = AtSdhChannelSts1Get(self);
            SdhChannelSts1IdSet(subChannel, (uint8)(aug4Sts1 + (i * numStsInAug1)));
            SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
            }
        }

    return ret;
    }

static AtSdhChannel SdhChannelClone(AtSdhChannel self)
    {
    AtSdhChannel newChannel = (AtSdhChannel)AtObjectClone((AtObject)self);

    /* Transfer reference */
    AtChannelReferenceTransfer((AtChannel)self, (AtChannel)newChannel);
    return newChannel;
    }

static eAtRet Vc3ConfigureBackup(AtSdhChannel vc3, tAtSdhVc3Config *vc3Configure)
    {
    eAtRet ret;
    uint8 i;
    static const eAtSdhPathAlarmType alarmTypes[] = {
            cAtSdhPathAlarmAis,
            cAtSdhPathAlarmLop,
            cAtSdhPathAlarmTim,
            cAtSdhPathAlarmUneq,
            cAtSdhPathAlarmPlm,
            cAtSdhPathAlarmRdi,
            cAtSdhPathAlarmErdiS,
            cAtSdhPathAlarmErdiP,
            cAtSdhPathAlarmErdiC,
            cAtSdhPathAlarmBerSd,
            cAtSdhPathAlarmBerSf,
            cAtSdhPathAlarmLom,
            cAtSdhPathAlarmRfi,
            cAtSdhPathAlarmPayloadUneq};

    ret = AtSdhChannelTxTtiGet(vc3, &vc3Configure->txTti);
    ret |= AtSdhChannelExpectedTtiGet(vc3, &vc3Configure->expectTti);
    vc3Configure->timMonEnable = AtSdhChannelTimMonitorIsEnabled(vc3);

    vc3Configure->alarmAffectEnableMask = 0;
    for (i = 0; i < (sizeof(alarmTypes) / sizeof(eAtSdhPathAlarmType)); i++)
        {
        if (AtSdhChannelAlarmAffectingIsEnabled(vc3, alarmTypes[i]))
            vc3Configure->alarmAffectEnableMask |= (cBit0 << i);
        }

    vc3Configure->txPls      = AtSdhPathTxPslGet((AtSdhPath)vc3);
    vc3Configure->expectPls  = AtSdhPathExpectedPslGet((AtSdhPath)vc3);
    vc3Configure->erdiEnable = AtSdhPathERdiIsEnabled((AtSdhPath)vc3);

    /* Timing information */
    vc3Configure->timingMode = AtChannelTimingModeGet((AtChannel)vc3);
    vc3Configure->timingSource = AtChannelTimingSourceGet((AtChannel)vc3);

    /* Clone vc3 to hold references */
    vc3Configure->vc3 = SdhChannelClone(vc3);

    return ret;
    }

static void Vc3ConfigureRestore(AtSdhChannel vc3, const tAtSdhVc3Config *vc3Configure)
    {
    eBool enable;
    uint8 i;
    static const eAtSdhPathAlarmType alarmTypes[] = {
        cAtSdhPathAlarmAis,
        cAtSdhPathAlarmLop,
        cAtSdhPathAlarmTim,
        cAtSdhPathAlarmUneq,
        cAtSdhPathAlarmPlm,
        cAtSdhPathAlarmRdi,
        cAtSdhPathAlarmErdiS,
        cAtSdhPathAlarmErdiP,
        cAtSdhPathAlarmErdiC,
        cAtSdhPathAlarmBerSd,
        cAtSdhPathAlarmBerSf,
        cAtSdhPathAlarmLom,
        cAtSdhPathAlarmRfi,
        cAtSdhPathAlarmPayloadUneq};

    AtSdhChannelTxTtiSet(vc3, &vc3Configure->txTti);
    AtSdhChannelExpectedTtiSet(vc3, &vc3Configure->expectTti);
    AtSdhChannelTimMonitorEnable(vc3, vc3Configure->timMonEnable);

    for (i = 0; i < (sizeof(alarmTypes) / sizeof(eAtSdhPathAlarmType)); i++)
        {
        enable = (vc3Configure->alarmAffectEnableMask & (cBit0 << i)) ? cAtTrue : cAtFalse;
        AtSdhChannelAlarmAffectingEnable(vc3, alarmTypes[i], enable);
        }

    AtSdhPathTxPslSet((AtSdhPath)vc3, vc3Configure->txPls);
    AtSdhPathExpectedPslSet((AtSdhPath)vc3, vc3Configure->expectPls);
    AtSdhPathERdiEnable((AtSdhPath)vc3, vc3Configure->erdiEnable);

    /* Restore timing configuration */
    AtChannelTimingSet((AtChannel)vc3, vc3Configure->timingMode, vc3Configure->timingSource);
    }

static uint8 Tug3SubMappingGet(AtSdhChannel self, tAtTug3Mapping *tug3Mapping)
    {
    static const uint8 numTug2sInTug3 = 7;
    static const uint8 numVc3sInTug3 = 1;
    AtSdhChannel subChannel;

    tug3Mapping->mapType = AtSdhChannelMapTypeGet(self);
    if (tug3Mapping->mapType == cAtSdhTugMapTypeTug3Map7xTug2s)
        {
        uint8 i;

        for (i = 0; i < numTug2sInTug3; i++)
            tug3Mapping->tug3Info.tug2[i] = SdhChannelClone(AtSdhChannelSubChannelGet(self, i));

        return numTug2sInTug3;
        }

    subChannel = AtSdhChannelSubChannelGet(self, 0);
    subChannel = AtSdhChannelSubChannelGet(subChannel, 0);

    if (AtSdhChannelMapTypeGet(subChannel) != cAtSdhVcMapTypeVc3MapDe3)
        return 0;

    if (Vc3ConfigureBackup(subChannel, &(tug3Mapping->tug3Info.vc3Cfg)) != cAtOk)
        return 0;

    return numVc3sInTug3;
    }

static uint8 Au3SubMappingGet(AtSdhChannel self, tAtTug3Mapping *tug3Mapping)
    {
    static const uint8 numTug2sInVc3 = 7;
    static const uint8 numC3sInVc3 = 1;
    AtSdhChannel vc3 = AtSdhChannelSubChannelGet(self, 0);
    uint8 vc3MapType = AtSdhChannelMapTypeGet(vc3);

    if (vc3MapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        {
        uint8 i;
        for (i = 0; i < numTug2sInVc3; i++)
            tug3Mapping->tug3Info.tug2[i] = SdhChannelClone(AtSdhChannelSubChannelGet(vc3, i));

        tug3Mapping->mapType = cAtSdhTugMapTypeTug3Map7xTug2s;
        return numTug2sInVc3;
        }

    if (vc3MapType == cAtSdhVcMapTypeVc3MapDe3)
        {
        tug3Mapping->mapType = cAtSdhTugMapTypeTug3MapVc3;

        if (Vc3ConfigureBackup(vc3, &(tug3Mapping->tug3Info.vc3Cfg)) != cAtOk)
            return 0;

        return numC3sInVc3;
        }

    tug3Mapping->mapType = cAtSdhTugMapTypeTugMapNone;
    return 0;
    }

static uint8 Au4SubMappingGet(AtSdhChannel self, tAtAug1SubMapping *subMapping)
    {
    uint8 i, numSubMapping = 0;
    static const uint8 numTug3sInVc4 = 3;
    AtSdhChannel vc4 = AtSdhChannelSubChannelGet(self, 0);

    if (AtSdhChannelMapTypeGet(vc4) == cAtSdhVcMapTypeVc4MapC4)
        return 0;

    for (i = 0; i < numTug3sInVc4; i++)
        numSubMapping = (uint8)(numSubMapping + Tug3SubMappingGet(AtSdhChannelSubChannelGet(vc4, i), &(subMapping->tug3Mapping[i])));

    return numSubMapping;
    }

static uint8 SubMappingSave(AtSdhChannel self, tAtAug1SubMapping *subMapping)
    {
    uint8 i, numSubMapping = 0;
    static const uint8 numAu3sInAug1 = 3;
    uint8 mapType = AtSdhChannelMapTypeGet(self);

    if (mapType == cAtSdhAugMapTypeAug1MapVc4)
        return Au4SubMappingGet(AtSdhChannelSubChannelGet(self, 0), subMapping);

    if (mapType == cAtSdhAugMapTypeAug1Map3xVc3s)
        {
        for (i = 0; i < numAu3sInAug1; i++)
            numSubMapping = (uint8)(numSubMapping + Au3SubMappingGet(AtSdhChannelSubChannelGet(self, i), &(subMapping->tug3Mapping[i])));

        return numSubMapping;
        }

    return 0;
    }

static eAtRet EncapChannelRestore(AtChannel self, AtEncapChannel encapChannel)
    {
    AtDevice device = AtChannelDeviceGet(self);
    AtEncapBinder encapBinder = AtDeviceEncapBinder(device);
    if (encapBinder)
        return AtEncapBinderAugEncapChannelRestore(encapBinder, self, encapChannel);
    return cAtOk;
    }

static eAtRet NxDs0sRestore(AtPdhDe1 de1)
    {
    AtIterator nxDs0Iterator;
    AtPdhNxDS0 nxDs0;
    AtEncapChannel encapChannel;
    eAtRet ret = cAtOk;

    nxDs0Iterator = AtPdhDe1nxDs0IteratorCreate(de1);
    while ((nxDs0 = (AtPdhNxDS0)AtIteratorNext(nxDs0Iterator)) != NULL)
        {
        encapChannel = AtChannelBoundEncapChannel((AtChannel)nxDs0);
        if (encapChannel)
            ret |= EncapChannelRestore((AtChannel)nxDs0, encapChannel);
        }

    AtObjectDelete((AtObject)nxDs0Iterator);
    return ret;
    }

static eAtRet PdhChannelRestore(AtSdhChannel self, AtPdhChannel pdhChannel)
    {
    eAtRet ret = cAtOk;
    AtEncapChannel encapChannel;

    /* If this PDH channel is bound to an Encapsulation channel */
    encapChannel = AtChannelBoundEncapChannel((AtChannel)pdhChannel);
    if (encapChannel)
        ret |= EncapChannelRestore((AtChannel)pdhChannel, encapChannel);

    /* PDH channel is DE1 */
    if (AtSdhChannelTypeGet(self) >= cAtSdhChannelTypeVc12)
        {
        NxDs0sRestore((AtPdhDe1)pdhChannel);
        }

    return ret;
    }

static eAtRet RestoreVc3FromDb(AtSdhChannel vc3, const tAtTug3Mapping *tug3Mapping)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel oldVc3 = tug3Mapping->tug3Info.vc3Cfg.vc3;
    AtEncapChannel encapChannel;
    AtPdhChannel pdhChannel;

    if (oldVc3 == NULL)
        return ret;

    /* If this channel has been bound to an Encapsulation channel, re-bind it */
    encapChannel = AtChannelBoundEncapChannel((AtChannel)oldVc3);
    if (encapChannel)
        ret |= EncapChannelRestore((AtChannel)vc3, encapChannel);

    /* If this channel contains PDH channel, restore it's PDH channel */
    pdhChannel = SdhVcPdhChannelGet((AtSdhVc)oldVc3);
    if (pdhChannel)
        {
        SdhVcPdhChannelSet((AtSdhVc)vc3, pdhChannel);
        AtPdhChannelVcSet(pdhChannel, (AtSdhVc)vc3);

        mMethodsGet(vc3)->MapTypeSet(vc3, cAtSdhVcMapTypeVc3MapDe3);
        ret |= PdhChannelRestore(vc3, pdhChannel);
        }

    /* Restore configuration of from old VC3 to new VC3 */
    AtChannelReferenceTransfer((AtChannel)oldVc3, (AtChannel)vc3);
    Vc3ConfigureRestore(vc3, &(tug3Mapping->tug3Info.vc3Cfg));
    AtObjectDelete((AtObject)oldVc3);

    return ret;
    }

static eAtRet Assign7xTug2sFromDb(AtSdhChannel tug3vc3, const tAtTug3Mapping *tug3Mapping)
    {
    AtSdhChannel *subChannels;
    AtOsal osal = AtSharedDriverOsalGet();
    static const uint8 numTug2sInVc3 = 7;
    uint8 i;

    /* Create memory to store all sub channels */
    subChannels = mMethodsGet(osal)->MemAlloc(osal, sizeof(AtSdhChannel) * numTug2sInVc3);
    if (subChannels == NULL)
        return cAtErrorRsrcNoAvail;

    /* Restore all sub channels */
    for (i = 0; i < numTug2sInVc3; i++)
        {
        subChannels[i] = tug3Mapping->tug3Info.tug2[i];

        /* Set its parent */
        SdhChannelParentSet(subChannels[i], tug3vc3);

        /* Cache Line and STS */
        SdhChannelLineIdSet(subChannels[i], AtSdhChannelLineGet(tug3vc3));
        SdhChannelSts1IdSet(subChannels[i], AtSdhChannelSts1Get(tug3vc3));
        }

    /* Cache to database */
    SdhChannelSubChannelsSet(tug3vc3, subChannels, numTug2sInVc3);

    /* For other mapping type, let determine when there is a channel mapped to it */
    return cAtOk;
    }

static eBool ChannelCanMap(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);

    if ((channelType == cAtSdhChannelTypeTu12) ||
        (channelType == cAtSdhChannelTypeTu11) ||
        (channelType == cAtSdhChannelTypeTu3))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet SubMappingRecover(AtSdhChannel sdhChannel)
    {
    AtSdhChannel subChannel;
    eAtRet ret = cAtOk;
    AtEncapChannel encapChannel;
    uint8 i, numSubChannel = AtSdhChannelNumberOfSubChannelsGet(sdhChannel);

    if (sdhChannel == NULL)
        return cAtErrorNullPointer;

    if (ChannelCanMap(sdhChannel))
        ret = mMethodsGet(sdhChannel)->MapTypeSet(sdhChannel, AtSdhChannelMapTypeGet(sdhChannel));

    /* If this channel has been bound to an Encapsulation channel, re-bind it */
    encapChannel = AtChannelBoundEncapChannel((AtChannel)sdhChannel);
    if (encapChannel)
        ret |= EncapChannelRestore((AtChannel)sdhChannel, encapChannel);

    /* If this channel contains PDH channel, restore it's PDH channel */
    if (AtSdhChannelTypeGet(sdhChannel) >= cAtSdhChannelTypeVc4_16c)
        {
        AtPdhChannel pdhChannel = SdhVcPdhChannelGet((AtSdhVc)sdhChannel);
        if (pdhChannel)
            ret |= PdhChannelRestore(sdhChannel, pdhChannel);
        }

    /* Recursive restore for it 's sub channels */
    for (i = 0; i < numSubChannel; i++)
        {
        subChannel = AtSdhChannelSubChannelGet(sdhChannel, i);
        ret |= SubMappingRecover(subChannel);
        }

    return ret;
    }

static eAtRet Vc3Mapping7xTug2sRetore(AtSdhChannel vc3, const tAtTug3Mapping *tug3Mapping)
    {
    eAtRet ret;

    /* Delete sub channels of tug3 if it has */
    SdhChannelSubChannelsDelete(vc3);

    /* Assign 7 TUG-2 from DB to this VC-3 */
    ret = Assign7xTug2sFromDb(vc3, tug3Mapping);

    /* Set map type first to prevent change database when re-map */
    SdhChannelMapTypeSet(vc3, cAtSdhVcMapTypeVc3Map7xTug2s);

    /* To this step, new vc3 has already have all of it sub mapping,
     * Recover it's sub mapping tree */
    ret |= SubMappingRecover(vc3);

    return ret;
    }

static eAtRet Au3SubMappingRetore(AtSdhChannel au3, const tAtTug3Mapping *tug3Mapping)
    {
    AtSdhChannel vc3 = AtSdhChannelSubChannelGet(au3, 0);

    if (tug3Mapping->mapType == cAtSdhTugMapTypeTug3Map7xTug2s)
        return Vc3Mapping7xTug2sRetore(vc3, tug3Mapping);

    /* Restore DB */
    return RestoreVc3FromDb(vc3, tug3Mapping);
    }

static eAtRet Tug3Mapping7xTug2sRestore(AtSdhChannel tug3, const tAtTug3Mapping *tug3Mapping)
    {
    eAtRet ret;

    /* Delete sub channels of tug3 if it has */
    SdhChannelSubChannelsDelete(tug3);

    /* Assign 7 TUG-2 from DB to this TUG-3 */
    ret = Assign7xTug2sFromDb(tug3, tug3Mapping);

    /* Set map type first to prevent change database when re-mapping */
    SdhChannelMapTypeSet(tug3, cAtSdhTugMapTypeTug3Map7xTug2s);

    /* To this step, new TUG-3 has already have all of it sub mapping,
     * Recover it's sub mapping tree */
    ret |= SubMappingRecover(tug3);

    return ret;
    }

static eAtRet Tug3MappingVc3Restore(AtSdhChannel tug3, const tAtTug3Mapping *tug3Mapping)
    {
    eAtRet ret;
    AtSdhChannel vc3, tu3;

    mMethodsGet(tug3)->MapTypeSet(tug3, cAtSdhTugMapTypeTug3MapVc3);

    /* Restore DB */
    tu3 = AtSdhChannelSubChannelGet(tug3, 0);
    vc3 = AtSdhChannelSubChannelGet(tu3, 0);

    ret = RestoreVc3FromDb(vc3, tug3Mapping);
    return ret;
    }

static eAtRet Vc4SubMappingRestore(AtSdhChannel vc4, const tAtAug1SubMapping *subMapping)
    {
    eAtRet ret = cAtOk;
    static const uint8 numTug3sInVc4 = 3;
    uint8 i;
    AtSdhChannel tug3;

    /* Map VC4 to 3xTUg3 */
    ret = mMethodsGet(vc4)->MapTypeSet(vc4, cAtSdhVcMapTypeVc4Map3xTug3s);
    if (ret != cAtOk)
        return ret;

    for (i = 0; i < numTug3sInVc4; i++)
        {
        tug3 = AtSdhChannelSubChannelGet(vc4, i);
        if (subMapping->tug3Mapping[i].mapType == cAtSdhTugMapTypeTug3MapVc3)
            ret |= Tug3MappingVc3Restore(tug3, &subMapping->tug3Mapping[i]);

        if (subMapping->tug3Mapping[i].mapType == cAtSdhTugMapTypeTug3Map7xTug2s)
            ret |= Tug3Mapping7xTug2sRestore(tug3, &subMapping->tug3Mapping[i]);
        }

    return ret;
    }

/* Check if a channel can be a sub channel of AUG1 */
static uint8 AtSdhChannelIsSubChannelOfAug1(AtSdhChannel self)
    {
    return (AtSdhChannelTypeGet(self) >= cAtSdhChannelTypeAu4) ? 1 : 0;
    }

static void EnterRestoreProgress(AtSdhAug aug)
    {
    aug->isInRestoreProgress = cAtTrue;
    }

static void ExitRestoreProgress(AtSdhAug aug)
    {
    aug->isInRestoreProgress = cAtFalse;
    }

uint8 Aug1RestoreIsInProgress(AtSdhChannel self)
    {
    if (self == NULL)
        return 0;

    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAug1)
        return ((AtSdhAug)self)->isInRestoreProgress;

    if (AtSdhChannelIsSubChannelOfAug1(self))
        return Aug1RestoreIsInProgress(AtSdhChannelParentChannelGet(self));

    return 0;
    }

static eBool NeedToRestoreSubMapping(AtSdhAug self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool AtSdhAug1NeedToRestoreSubMapping(AtSdhAug self)
    {
	if (self)
	    return mMethodsGet(self)->NeedToRestoreSubMapping(self);
	return cAtFalse;
    }

eAtRet HelperAug1SubMappingRestore(AtSdhChannel self)
    {
    uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)self);
    eAtRet ret = cAtOk;
    AtSdhAug aug = (AtSdhAug)self;
    AtSdhChannel subChannel;

    if (!Aug1RestoreIsInProgress(self))
        return ret;

    if (mapType == cAtSdhAugMapTypeAug1MapVc4)
        {
        subChannel = AtSdhChannelSubChannelGet(AtSdhChannelSubChannelGet(self, 0), 0);
        ret |= Vc4SubMappingRestore(subChannel, &(aug->subMapping));
        }

    /* For VC-3 mapping, create VC-3 sub channels for AU-3s */
    else
        {
        static const uint8 numVc3sInAug1 = 3;
        uint8 i;
        for (i = 0; i < numVc3sInAug1; i++)
            {
            /* Get VC3 */
            subChannel = AtSdhChannelSubChannelGet(self, i);
            ret |= Au3SubMappingRetore(subChannel, &(aug->subMapping.tug3Mapping[i]));
            }
        }

    ExitRestoreProgress(aug);
    return ret;
    }

static eAtRet Aug1MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 numSubChannels = 0, i, aug1Sts1;
    eAtRet ret;
    AtSdhChannel subChannel;
    static const uint8 numVc3sInAug1 = 3;
    uint8 subChannelType = cAtSdhChannelTypeUnknown;
    AtSdhAug aug = (AtSdhAug)self;
    uint8 lineId;

    if (AtSdhChannelServiceIsRunning(self))
        return cAtErrorChannelBusy;

    /* Get back up information */
    if (AtSdhAug1NeedToRestoreSubMapping(aug))
        {
        if (SubMappingSave(self, &(aug->subMapping)))
            EnterRestoreProgress(aug);
        }

    /* Delete sub channels */
    AtSdhChannelAllSubChannelsHardwareCleanup(self);
    SdhChannelSubChannelsDelete(self);

    /* Map VC-4 */
    if (mapType == cAtSdhAugMapTypeAug1MapVc4)
        {
        numSubChannels = 1;
        subChannelType = cAtSdhChannelTypeAu4;
        }

    /* Map VC-3 */
    if (mapType == cAtSdhAugMapTypeAug1Map3xVc3s)
        {
        numSubChannels = numVc3sInAug1;
        subChannelType = cAtSdhChannelTypeAu3;
        }

    if (subChannelType == cAtSdhChannelTypeUnknown)
        return cAtErrorInvlParm;

    /* Create sub channels */
    lineId = AtSdhChannelLineGet(self);
    ret = SdhChannelSubChannelsCreate(self, lineId, numSubChannels, subChannelType);
    if (ret == cAtOk)
        SdhChannelMapTypeSet(self, mapType);

    /* For AU, create its VC sub channels */
    if (subChannelType == cAtSdhChannelTypeAu4)
        ret |= SdhChannelSubChannelsCreate(AtSdhChannelSubChannelGet(self, 0), lineId, 1, cAtSdhChannelTypeVc4);

    /* For VC-3 mapping, create VC-3 sub channels for AU-3s */
    else
        {
        for (i = 0; i < numSubChannels; i++)
            {
            subChannel = AtSdhChannelSubChannelGet(self, i);
            ret |= SdhChannelSubChannelsCreate(subChannel, lineId, 1, cAtSdhChannelTypeVc3);
            }
        }

    /* Cache line ID and STS */
    if (subChannelType == cAtSdhChannelTypeAu4)
        {
        /* Cache ID for AU */
        subChannel = AtSdhChannelSubChannelGet(self, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));

        /* Cache ID for VC */
        subChannel = AtSdhChannelSubChannelGet(subChannel, 0);
        SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
        SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
        }
    else
        {
        aug1Sts1 = AtSdhChannelSts1Get(self);
        for (i = 0; i < numSubChannels; i++)
            {
            /* Cache ID for AU-3 */
            subChannel = AtSdhChannelSubChannelGet(self, i);
            SdhChannelSts1IdSet(subChannel, (uint8)(aug1Sts1 + i));
            SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));

            /* Cache ID for VC-3 */
            subChannel = AtSdhChannelSubChannelGet(subChannel, 0);
            SdhChannelSts1IdSet(subChannel, (uint8)(aug1Sts1 + i));
            SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
            }
        }

    return ret;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 augType = AtSdhChannelTypeGet(self);
    eAtRet ret = cAtOk;

    if (mapType != AtSdhChannelMapTypeGet(self))
        AtSdhChannelSubChannelsInterruptDisable(self, cAtTrue);

    /* Let the super do first because mapping changing may relate with resource
     * management */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* Do nothing if mapping type is not changed */
    if (mapType == AtSdhChannelMapTypeGet(self))
        return cAtOk;

    /* Set new mapping */
    ret = cAtErrorInvlParm;
    if (augType == cAtSdhChannelTypeAug64)
        ret = Aug64MapTypeSet(self, mapType);
    if (augType == cAtSdhChannelTypeAug16)
        ret = Aug16MapTypeSet(self, mapType);
    if (augType == cAtSdhChannelTypeAug4)
        ret = Aug4MapTypeSet(self, mapType);
    if (augType == cAtSdhChannelTypeAug1)
        ret = Aug1MapTypeSet(self, mapType);

    return ret;
    }

static const char *TypeString(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)self);

    if (channelType == cAtSdhChannelTypeAug64) return "aug64";
    if (channelType == cAtSdhChannelTypeAug16) return "aug16";
    if (channelType == cAtSdhChannelTypeAug4)  return "aug4";
    if (channelType == cAtSdhChannelTypeAug1)  return "aug1";

    return "unknown";
    }

static const char *IdString(AtChannel self)
    {
    static char idString[16];
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    /* Make an empty string */
    AtSprintf(idString, "%u.%u", AtSdhChannelLineGet(sdhChannel) + 1, AtChannelIdGet(self) + 1);

    return idString;
    }

static eAtModuleSdhRet DefaultMappingSet(AtSdhChannel aug)
    {
    eAtSdhChannelType augType = AtSdhChannelTypeGet(aug);

    if (augType == cAtSdhChannelTypeAug64)
        return mMethodsGet(aug)->MapTypeSet(aug, cAtSdhAugMapTypeAug64Map4xAug16s);
    if (augType == cAtSdhChannelTypeAug16)
        return mMethodsGet(aug)->MapTypeSet(aug, cAtSdhAugMapTypeAug16Map4xAug4s);
    if (augType == cAtSdhChannelTypeAug4)
        return mMethodsGet(aug)->MapTypeSet(aug, cAtSdhAugMapTypeAug4Map4xAug1s);
    if (augType == cAtSdhChannelTypeAug1)
        return mMethodsGet(aug)->MapTypeSet(aug, cAtSdhAugMapTypeAug1Map3xVc3s);

    return cAtOk;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    uint8 augType = AtSdhChannelTypeGet(self);
    AtSdhChannel subChannel;

    /* If change from SONET to SDH or AUG type is not AUG-1, nothing to do */
    if ((mode == cAtSdhChannelModeSdh) || (augType != cAtSdhChannelTypeAug1))
        return cAtOk;

    /* Just need to change in case this AUG-1 is mapping AU4->VC4->3xTUG3 */
    if (AtSdhChannelMapTypeGet(self) == cAtSdhAugMapTypeAug1Map3xVc3s)
        return cAtOk;

    /* Get VC4 */
    subChannel = AtSdhChannelSubChannelGet(self, 0);
    if (subChannel)
        subChannel = AtSdhChannelSubChannelGet(subChannel, 0);
    if ((subChannel == NULL) || (AtSdhChannelMapTypeGet(subChannel) != cAtSdhVcMapTypeVc4Map3xTug3s))
        return cAtOk;

    /* AUG-1 is mapping AU4->VC4->3xTUG3, switch to mapping VC3 */
    return mMethodsGet(self)->MapTypeSet(self, cAtSdhAugMapTypeAug1Map3xVc3s);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    uint8 i;

    /* Set default mapping */
    ret = AtSdhChannelDefaultModeSet((AtSdhChannel)self);
    ret |= DefaultMappingSet((AtSdhChannel)self);

    /* Initialize all sub channels */
    for (i = 0; i < AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)self); i++)
        ret |= AtChannelInit((AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, i));

    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSdhAug object = (AtSdhAug)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(isInRestoreProgress);
    mEncodeNone(subMapping);
    }

static eBool ShouldResetMapping(uint8 mapType)
    {
    switch (mapType)
        {
        case cAtSdhAugMapTypeAug64MapVc4_64c: return cAtTrue;
        case cAtSdhAugMapTypeAug16MapVc4_16c: return cAtTrue;
        case cAtSdhAugMapTypeAug4MapVc4_4c  : return cAtTrue;
        case cAtSdhAugMapTypeAug1MapVc4     : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint8 MapTypeForReset(AtSdhChannel self)
    {
    uint32 augType = AtSdhChannelTypeGet(self);

    switch (augType)
        {
        case cAtSdhChannelTypeAug64: return cAtSdhAugMapTypeAug64Map4xAug16s;
        case cAtSdhChannelTypeAug16: return cAtSdhAugMapTypeAug16Map4xAug4s;
        case cAtSdhChannelTypeAug4:  return cAtSdhAugMapTypeAug4Map4xAug1s;
        case cAtSdhChannelTypeAug1:  return cAtSdhAugMapTypeAug1Map3xVc3s;
        default:
            return cAtSdhChannelTypeUnknown;
        }
    }

static eAtRet MappingReset(AtSdhChannel self)
    {
    uint8 mapType = AtSdhChannelMapTypeGet(self);

    if (ShouldResetMapping(mapType))
        return mMethodsGet(self)->MapTypeSet(self, MapTypeForReset(self));

    return m_AtSdhChannelMethods->MappingReset(self);
    }

static eAtModuleSdhRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = m_AtSdhChannelMethods->CanChangeMapping(self, mapType);
    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelIsJoiningAps(self))
        return cAtErrorChannelBusy;

    if (AtSdhChannelServiceIsRunning(self))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

static void MethodsInit(AtSdhAug self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, NeedToRestoreSubMapping);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtSdhAug self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(AtSharedDriverOsalGet(), &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhAug self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhAug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, MappingReset);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeMapping);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

AtSdhAug AtSdhAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhAug));

    /* Super constructor */
    if (AtSdhChannelObjectInit((AtSdhChannel)self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

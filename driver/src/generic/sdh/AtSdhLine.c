/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhLine.c
 *
 * Created Date: Jul 26, 2012
 *
 * Author      : namnn
 *
 * Description : SDH Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtBerController.h"
#include "AtSdhAug.h"
#include "AtSdhTug.h"

#include "../sur/AtModuleSurInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../lab/profiler/AtFailureProfilerFactory.h"
#include "AtSdhLineInternal.h"
#include "AtModuleSdhInternal.h"
#include "AtSdhStsInternal.h"
#include "AtSdhVcInternal.h"
#include "AtPw.h"
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#define cAtSdhLineBothSecAndLineDccId 0
#define cAtSdhLineSecDccId            0
#define cAtSdhLineLineDccId           1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSdhLine)self)
#define mLineIsValid(self) ((self) ? cAtTrue : cAtFalse)
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSdhLineMethods m_methods;

static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static void AllStsDelele(AtSdhLine self);

/*--------------------------- Implementation ---------------------------------*/
static uint8 ChannelTypeFromLineRate(eAtSdhLineRate rate)
    {
    if (rate == cAtSdhLineRateStm1)
        return cAtSdhChannelTypeAug1;
    if (rate == cAtSdhLineRateStm4)
        return cAtSdhChannelTypeAug4;
    if (rate == cAtSdhLineRateStm16)
        return cAtSdhChannelTypeAug16;
    if (rate == cAtSdhLineRateStm64)
        return cAtSdhChannelTypeAug64;
    if (rate == cAtSdhLineRateStm0)
        return cAtSdhChannelTypeAu3;
    return cAtSdhChannelTypeUnknown;
    }

static uint8 NumSts(AtSdhChannel self)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet((AtSdhLine)self);
    if (rate == cAtSdhLineRateStm0)  return 1;
    if (rate == cAtSdhLineRateStm1)  return 3;
    if (rate == cAtSdhLineRateStm4)  return 12;
    if (rate == cAtSdhLineRateStm16) return 48;
    if (rate == cAtSdhLineRateStm64) return 192;

    return 0;
    }

static AtModuleBer BerModule(AtSdhLine self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (AtModuleBer)AtDeviceModuleGet(device, cAtModuleBer);
    }

static AtBerController RsBerControllerGet(AtSdhLine self)
    {
    AtBerController newController;

    if (self->rsBerController)
        return self->rsBerController;

    /* Create one */
    newController = mMethodsGet(self)->RsBerControllerCreate(self);
    if (newController == NULL)
        return NULL;

    AtBerControllerInit(newController);
    self->rsBerController = newController;

    return self->rsBerController;
    }

static AtBerController MsBerControllerGet(AtSdhLine self)
    {
    AtBerController newController;

    if (self->msBerController)
        return self->msBerController;

    /* Create one */
    newController = mMethodsGet(self)->MsBerControllerCreate(self);
    if (newController == NULL)
        return NULL;

    AtBerControllerInit(newController);
    self->msBerController = newController;

    return self->msBerController;
    }

static AtBerController RsBerControllerCreate(AtSdhLine self)
    {
    return AtModuleBerSdhLineRsBerControlerCreate(BerModule(self), (AtChannel)self);
    }

static AtBerController MsBerControllerCreate(AtSdhLine self)
    {
    return AtModuleBerSdhLineMsBerControlerCreate(BerModule(self), (AtChannel)self);
    }

static AtBerController BerControllerCreate(AtSdhChannel self)
    {
	AtUnused(self);
    /* Let's handle RS and MS BER controller instead of the default one */
    return NULL;
    }

static eAtModuleSdhRet BerControllerReCreate(AtSdhLine self, AtBerController* controllerCache, AtBerController (*ControlerCreate)(AtModuleBer moduleBer, AtChannel line))
    {
    AtModuleBer berModule = BerModule(self);
    AtBerController oldController, newController;
    eAtRet ret = cAtOk;
    eAtBerRate sd, sf, tca;
    eBool isEnabled;

    /* Just do nothing if it has not been created */
    oldController = *controllerCache;
    if (oldController == NULL)
        return cAtOk;

    /* Save configuration of old controller */
    sd = AtBerControllerSdThresholdGet(oldController);
    sf = AtBerControllerSfThresholdGet(oldController);
    tca = AtBerControllerTcaThresholdGet(oldController);
    isEnabled = AtBerControllerIsEnabled(oldController);

    /* Delete the old one */
    AtModuleBerControllerDelete(berModule, oldController);

    /* Recreate new one with same configuration */
    newController = ControlerCreate(berModule, (AtChannel)self);
    ret |= AtBerControllerInit(newController);
    ret |= AtBerControllerSdThresholdSet(newController, sd);
    ret |= AtBerControllerSfThresholdSet(newController, sf);
    ret |= AtBerControllerTcaThresholdSet(newController, tca);
    ret |= AtBerControllerEnable(newController, isEnabled);
    *controllerCache = newController;

    return (eAtModuleSdhRet)ret;
    }

static eAtModuleSdhRet MsBerControllerReCreate(AtSdhLine self)
    {
    return BerControllerReCreate(self, &mThis(self)->msBerController, AtModuleBerSdhLineMsBerControlerCreate);
    }

static eAtModuleSdhRet RsBerControllerReCreate(AtSdhLine self)
    {
    return BerControllerReCreate(self, &mThis(self)->rsBerController, AtModuleBerSdhLineRsBerControlerCreate);
    }

static eAtModuleSdhRet BerControllersReCreate(AtSdhLine self)
    {
    eAtRet ret = cAtOk;

    ret |= RsBerControllerReCreate(self);
    ret |= MsBerControllerReCreate(self);

    return (eAtModuleSdhRet)ret;
    }

static AtBerController BerControllerGet(AtSdhChannel self)
    {
    return AtSdhLineMsBerControllerGet((AtSdhLine)self);
    }

static void BerControllerDelete(AtSdhChannel self)
    {
    AtModuleBer berModule = BerModule((AtSdhLine)self);
    if (mThis(self)->rsBerController)
        AtModuleBerControllerDelete(berModule, mThis(self)->rsBerController);
    if (mThis(self)->msBerController)
        AtModuleBerControllerDelete(berModule, mThis(self)->msBerController);
    }

static eBool RateIsChanged(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtSdhChannel aug = AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    uint8 newChannelType = ChannelTypeFromLineRate(rate);
    eBool subChannelIsCreated = cAtFalse;

    if (AtSdhChannelTypeGet(aug) == newChannelType)
        subChannelIsCreated = cAtTrue;
    if ((rate == AtSdhLineRateGet(self)) && subChannelIsCreated)
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleSdhRet SoftwareRateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtSdhChannel aug;
    uint8 newChannelType;
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 channelId = (uint8)AtChannelIdGet((AtChannel)self);

    /* Do nothing if rate is not changed */
    if (!RateIsChanged(self, rate))
        return cAtOk;

    /* Delete all Hw STS */
    AtSdhChannelAllHwStsDelete(sdhChannel);
    AllStsDelele(self);

    newChannelType = ChannelTypeFromLineRate(rate);

    /* Delete BER engine and sub engines */
    SdhChannelAllBerControllersDelete(sdhChannel);

    /* The application may be in polling progress, need to lock this module
     * before deleting objects */
    SdhChannelSubChannelsDelete(sdhChannel);

    /* Save lineId for itself */
    SdhChannelLineIdSet(sdhChannel, channelId);

    /* Recreate sub channels */
    SdhChannelSubChannelsCreate(sdhChannel, channelId, 1, newChannelType);

    /* Cache STS ID */
    aug = AtSdhChannelSubChannelGet(sdhChannel, 0);
    SdhChannelLineIdSet(aug, channelId);
    SdhChannelSts1IdSet(aug, 0);

    return cAtOk;
    }

static eAtModuleSdhRet RateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    return AtSdhLineSoftwareRateSet(self, rate);
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
	AtUnused(rate);
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static void RateDidChange(AtSdhLine self, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    AtUnused(self);
    AtUnused(oldRate);
    AtUnused(newRate);
    /* Let concrete class do */
    }

static void RateWillChange(AtSdhLine self, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    AtUnused(self);
    AtUnused(oldRate);
    AtUnused(newRate);
    /* Let concrete class do */
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtSdhLineRateInvalid;
    }

static eAtModuleSdhRet CanChangeRate(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtUnused(self);
    AtUnused(rate);
    return cAtOk;
    }

static eAtModuleSdhRet TxS1Set(AtSdhLine self, uint8 value)
    {
	AtUnused(value);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static uint8 TxS1Get(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8 RxS1Get(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

/* K1 */
static eAtModuleSdhRet TxK1Set(AtSdhLine self, uint8 value)
    {
	AtUnused(value);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static uint8 TxK1Get(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8 RxK1Get(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

/* K2 */
static eAtModuleSdhRet TxK2Set(AtSdhLine self, uint8 value)
    {
	AtUnused(value);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static uint8 TxK2Get(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8 RxK2Get(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModuleSdhRet LedStateSet(AtSdhLine self, eAtLedState ledState)
    {
	AtUnused(ledState);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eAtLedState LedStateGet(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtLedStateOff;
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "line";
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    tAtSdhLineCounters *counters = (tAtSdhLineCounters *)pAllCounters;

    if (pAllCounters == NULL)
        return cAtOk;

    counters->b1  = AtChannelCounterGet(self, cAtSdhLineCounterTypeB1);
    counters->b2  = AtChannelCounterGet(self, cAtSdhLineCounterTypeB2);
    counters->rei = AtChannelCounterGet(self, cAtSdhLineCounterTypeRei);

    return cAtOk;
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    uint32 b1, b2, rei;

    b1  = AtChannelCounterClear(self, cAtSdhLineCounterTypeB1);
    b2  = AtChannelCounterClear(self, cAtSdhLineCounterTypeB2);
    rei = AtChannelCounterClear(self, cAtSdhLineCounterTypeRei);

    if (pAllCounters)
        {
        tAtSdhLineCounters *counters = (tAtSdhLineCounters *)pAllCounters;

        counters->b1  = b1;
        counters->b2  = b2;
        counters->rei = rei;
        }

    return cAtOk;
    }

static eAtRet SubChannelInit(AtChannel self)
    {
    AtSdhChannel subChannel = AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    eAtRet ret = AtChannelInit((AtChannel)subChannel);

    /* In case of STM-0, should have default mapping for VC-3 layer */
    if (AtSdhChannelTypeGet(subChannel) == cAtSdhChannelTypeAu3)
        {
        AtSdhChannel vc = AtSdhChannelSubChannelGet(subChannel, 0);
        if (vc)
            ret |= mMethodsGet(vc)->MapTypeSet(vc, (uint8)AtSdhVcDefaultMapType((AtSdhVc)vc));
        }

    return ret;
    }

static eAtRet DefaultCounterModeSet(AtSdhChannel self)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    eAtSdhLineCounterType counterTypes[] = {cAtSdhLineCounterTypeB1, cAtSdhLineCounterTypeB2, cAtSdhLineCounterTypeRei};

    /* Set default counter mode */
    for (i = 0; i < mCount(counterTypes); i++)
        ret |= AtSdhChannelDefaultCounterModeSet(self, counterTypes[i]);

    return ret;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    eAtRet ret = m_AtSdhChannelMethods->ModeSet(self, mode);
    if (ret != cAtOk)
        return ret;

    return DefaultCounterModeSet(self);
    }

static eAtRet DefaultAlarmAffectingSet(AtSdhChannel self)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    eAtSdhLineAlarmType alarmTypeList[] = {cAtSdhLineAlarmLos, cAtSdhLineAlarmLof,
                                           cAtSdhLineAlarmTim, cAtSdhLineAlarmAis};

    if (!mMethodsGet(mThis(self))->HasFramer(mThis(self)))
        return cAtOk;

    /* Set default alarm affecting */
    for (i = 0; i < mCount(alarmTypeList); i++)
        {
        if (!AtSdhChannelCanEnableAlarmAffecting(self, alarmTypeList[i]))
            continue;

        ret |= AtSdhChannelAlarmAffectingEnable(self, alarmTypeList[i], cAtTrue);
        }

    /* For TIM, disable AIS downstream as default */
    if (AtSdhChannelCanEnableAlarmAffecting(self, cAtSdhLineAlarmTim))
        ret |= AtSdhChannelAlarmAffectingEnable(self, cAtSdhLineAlarmTim, cAtFalse);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    AtSdhLine line = (AtSdhLine)self;
    tAtSdhTti tti;
    AtSerdesController serdesController;
    AtSurEngine surEngine;

    /* Super initialization */
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (!AtChannelShouldFullyInit(self))
        return cAtOk;

    /* Insert default TOH */
    if (AtSdhLineCanSetTxK1(line))
        AtSdhLineTxK1Set(line, 0x0);

    if (AtSdhLineCanSetTxK2(line))
        AtSdhLineTxK2Set(line, 0x0);

    if (AtSdhLineCanSetTxS1(line))
        AtSdhLineTxS1Set(line, 0x0);

    AtSdhTtiMake(AtModuleSdhDefaultTtiMode((AtModuleSdh)AtChannelModuleGet(self)), (const uint8 *)"a", 1, &tti);
    AtSdhChannelTxTtiSet((AtSdhChannel)self, &tti);
    AtSdhChannelExpectedTtiSet((AtSdhChannel)self, &tti);
    AtSdhChannelTimMonitorEnable((AtSdhChannel)self, cAtTrue);

    /* Initialize its sub channel */
    SubChannelInit(self);

    DefaultAlarmAffectingSet((AtSdhChannel)self);

    /* Clear channel interrupt mask. */
    AtChannelInterruptMaskSet(self, AtChannelInterruptMaskGet(self), 0x0);

    /* Release loopback */
    if (AtChannelLoopbackGet(self) != cAtLoopbackModeRelease)
        AtChannelLoopbackSet(self, cAtLoopbackModeRelease);

    serdesController = AtSdhLineSerdesController(line);
    if (serdesController && mMethodsGet(line)->ShouldInitSerdesController(line))
        AtSerdesControllerInit(serdesController);

    /* Initialize SUR engine as default */
    surEngine = AtChannelSurEngineGet(self);
    if (surEngine)
        AtSurEngineInit(surEngine);

    return cAtOk;
    }

static eAtModuleSdhRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtErrorModeNotSupport;
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static AtSerdesController SerdesController(AtSdhLine self)
    {
	AtUnused(self);
    /* Sub class should do */
    return NULL;
    }

static AtSdhLine SwitchedLineGet(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static AtSdhLine BridgedLineGet(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return NULL;
    }

static eAtModuleSdhRet Switch(AtSdhLine self, AtSdhLine toLine)
    {
	AtUnused(toLine);
	AtUnused(self);
    /* Let concrete class implement it */
    return cAtError;
    }

static eAtModuleSdhRet SwitchRelease(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class implement it */
    return cAtError;
    }

static eAtModuleSdhRet Bridge(AtSdhLine self, AtSdhLine toLine)
    {
	AtUnused(toLine);
	AtUnused(self);
    /* Let concrete class implement it */
    return cAtError;
    }

static eAtModuleSdhRet BridgeRelease(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class implement it */
    return cAtError;
    }

static eAtModuleSdhRet OverheadBytesInsertEnable(AtSdhLine self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class implement it */
    return cAtError;
    }

static eBool OverheadBytesInsertIsEnabled(AtSdhLine self)
    {
	AtUnused(self);
    /* Let concrete class implement it */
    return cAtTrue;
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    AtSdhLine line = (AtSdhLine)self;

    switch (overheadByte)
        {
        case cAtSdhLineMsOverheadByteK1:
            return AtSdhLineTxK1Set(line, overheadByteValue);
        case cAtSdhLineMsOverheadByteK2:
            return AtSdhLineTxK2Set(line, overheadByteValue);
        case cAtSdhLineMsOverheadByteS1:
            return AtSdhLineTxS1Set(line, overheadByteValue);

        /* Sub class may know how to do */
        default:
            return cAtErrorModeNotSupport;
        }
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    AtSdhLine line = (AtSdhLine)self;

    switch (overheadByte)
        {
        case cAtSdhLineMsOverheadByteK1:
            return AtSdhLineTxK1Get(line);
        case cAtSdhLineMsOverheadByteK2:
            return AtSdhLineTxK2Get(line);
        case cAtSdhLineMsOverheadByteS1:
            return AtSdhLineTxS1Get(line);

        /* Sub class may know how to do */
        default:
            return 0x0;
        }
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    AtSdhLine line = (AtSdhLine)self;

    switch (overheadByte)
        {
        case cAtSdhLineMsOverheadByteK1:
            return AtSdhLineRxK1Get(line);
        case cAtSdhLineMsOverheadByteK2:
            return AtSdhLineRxK2Get(line);
        case cAtSdhLineMsOverheadByteS1:
            return AtSdhLineRxS1Get(line);

        /* Sub class may know how to do */
        default:
            return 0x0;
        }
    }

static eBool OverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    AtUnused(self);

    if ((overheadByte == cAtSdhLineMsOverheadByteK1) ||
        (overheadByte == cAtSdhLineMsOverheadByteK2) ||
        (overheadByte == cAtSdhLineMsOverheadByteS1))
        return cAtTrue;

    return cAtFalse;
    }

static eBool TxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    return OverheadByteIsSupported(self, overheadByte);
    }

static eBool RxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    return OverheadByteIsSupported(self, overheadByte);
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    AtUnused(self);
    if ((pseudowire == NULL) || (AtPwTypeGet(pseudowire) == cAtPwTypeToh))
        return cAtOk;

    return cAtErrorInvlParm;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
	AtUnused(self);
    if ((loopbackMode == cAtLoopbackModeLocal)  ||
        (loopbackMode == cAtLoopbackModeRemote) ||
        (loopbackMode == cAtLoopbackModeRelease))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtSdhLineCache);
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static void CacheRateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->rate = rate;
    }

static eAtSdhLineRate CacheRateGet(AtSdhLine self)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    return cache ? cache->rate : cAtSdhLineRateInvalid;
    }

static uint8 CacheTxS1Get(AtSdhLine self)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->txS1 : 0);
    }

static void CacheTxS1Set(AtSdhLine self, uint8 value)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->txS1 = value;
    }

static void CacheTxK1Set(AtSdhLine self, uint8 value)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->txK1 = value;
    }

static uint8 CacheTxK1Get(AtSdhLine self)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->txK1 : 0);
    }

static void CacheTxK2Set(AtSdhLine self, uint8 value)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->txK2 = value;
    }

static uint8 CacheTxK2Get(AtSdhLine self)
    {
    tAtSdhLineCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->txK2 : 0);
    }

static eBool IsAugMappingVc(AtSdhChannel aug)
    {
    uint8 channelType = AtSdhChannelTypeGet(aug);
    switch (channelType)
        {
        case cAtSdhChannelTypeAug64:
            return (AtSdhChannelMapTypeGet(aug) == cAtSdhAugMapTypeAug64MapVc4_64c) ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeAug16:
            return (AtSdhChannelMapTypeGet(aug) == cAtSdhAugMapTypeAug16MapVc4_16c) ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeAug4:
            return (AtSdhChannelMapTypeGet(aug) == cAtSdhAugMapTypeAug4MapVc4_4c) ? cAtTrue : cAtFalse;
        case cAtSdhChannelTypeAug1:
            return (AtSdhChannelMapTypeGet(aug) == cAtSdhAugMapTypeAug1MapVc4) ? cAtTrue : cAtFalse;
        default:
            return cAtFalse;
        }
    }

static AtSdhAu Aux_ncGet(AtSdhLine self, uint8 augId, AtSdhAug (*AugGet)(AtSdhLine self, uint8 augId))
    {
    AtSdhChannel aug;

    /* Just get AUG first */
    aug = (AtSdhChannel)AugGet(self, augId);

    /* Make sure that this AUG map VC */
    if (!IsAugMappingVc(aug))
        return NULL;

    if (aug->subChannels == NULL)
        return NULL;

    if (aug->subChannels[0] == NULL)
        return NULL;

    if (AtSdhChannelIsInConcatenation(aug->subChannels[0]))
        return (AtSdhAu)aug->subChannels[0];

    return NULL;
    }

static AtSdhAu Au4_ncGet(AtSdhLine self, uint8 aug1Id)
    {
    return Aux_ncGet(self, aug1Id, AtSdhLineAug1Get);
    }

static AtSdhVc Vc4_ncGet(AtSdhLine self, uint8 aug1Id)
    {
    AtSdhChannel au = (AtSdhChannel)AtSdhLineAu4_ncGet(self, aug1Id);
    if (au == NULL)
        return NULL;
    return (AtSdhVc)AtSdhChannelSubChannelGet(au, 0);
    }

static AtSdhAu Au4_16ncGet(AtSdhLine self, uint8 aug16Id)
    {
    return Aux_ncGet(self, aug16Id, AtSdhLineAug16Get);
    }

static AtSdhVc Vc4_16ncGet(AtSdhLine self, uint8 aug16Id)
    {
    AtSdhChannel au = (AtSdhChannel)AtSdhLineAu4_16ncGet(self, aug16Id);
    if (au == NULL)
        return NULL;
    return (AtSdhVc)AtSdhChannelSubChannelGet(au, 0);
    }

static void InterruptProcess(AtSdhLine self, uint8 line, AtHal hal)
    {
    AtUnused(self);
    AtUnused(line);
    AtUnused(hal);
    }

static eBool CanHaveMapping(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtModuleSur SurModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static AtSurEngine SurEngineObjectCreate(AtChannel self)
    {
    return AtModuleSurSdhLineEngineCreate(SurModule(self), (AtSdhLine)self);
    }

static void SurEngineObjectDelete(AtChannel self, AtSurEngine engine)
    {
    AtModuleSurEngineSdhLineDelete(SurModule(self), engine);
    }

static void DccStatusClear(AtSdhLine self)
    {
    static const uint32 dccType[] = {cAtSdhLineDccLayerSection,
                                     cAtSdhLineDccLayerLine,
                                     cAtSdhLineDccLayerAll};
    uint8 type;

    for (type = 0; type < mCount(dccType); type++)
        {
        AtChannel dcc;
        if ((dcc = (AtChannel)AtSdhLineDccChannelGet((AtSdhLine)self, dccType[type])) != NULL)
            AtChannelStatusClear(dcc);
        }
    }

static void StatusClear(AtChannel self)
    {
    m_AtChannelMethods->StatusClear(self);

    /* Clear block-error counters */
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)self, cAtSdhLineCounterTypeB1);
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)self, cAtSdhLineCounterTypeB2);
    AtSdhChannelBlockErrorCounterClear((AtSdhChannel)self, cAtSdhLineCounterTypeRei);

    /* Associated HDLC/DCC channels */
    DccStatusClear((AtSdhLine)self);
    }

static eBool HasBerControllers(AtSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasAlarmForwarding(AtSdhChannel self, uint32 currentDefects)
    {
    /* Note: if TIM-S to AIS-L is enabled, it finally also cause AIS-L defect raised.
     * Therefore, we do not need to check current TIM-S. */
    AtUnused(self);
    return (currentDefects & (cAtSdhLineAlarmLos | cAtSdhLineAlarmLof | cAtSdhLineAlarmAis)) ? cAtTrue : cAtFalse;
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentDefects, eBool subChannelsOnly)
    {
    if (AtSdhChannelHasAlarmForwarding(self, currentDefects))
        return;

    AtChannelAllAlarmListenersCall((AtChannel)self, cAtSdhLineAlarmAis, 0);

    m_AtSdhChannelMethods->AlarmForwardingClearanceNotifyWithSubChannelOnly(self, currentDefects, subChannelsOnly);
    }

static void FailureForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentFailures, eBool subChannelsOnly)
    {
    AtUnused(subChannelsOnly);

    if (AtSdhChannelHasAlarmForwarding(self, currentFailures))
        return;

    AtChannelAllFailureListenersCall((AtChannel)self, cAtSdhLineAlarmAis, 0);

    m_AtSdhChannelMethods->FailureForwardingClearanceNotifyWithSubChannelOnly(self, currentFailures, cAtFalse);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSdhLine object = (AtSdhLine)self;

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(msBerController);
    mEncodeObject(rsBerController);
    mEncodeObject(apsEngine);
    mEncodeObjects(dccChannel, cAtSdhLineNumDccLayers);
    mEncodeUInt(dccLayersMask);
    mEncodeNone(cache);
    mEncodeObjects(dccChannel, cAtSdhLineNumDccLayers);
    mEncodeUInt(dccLayersMask);
    mEncodeObjects(allSts1s, AtSdhChannelNumSts((AtSdhChannel)object));
    }

static void DeleteAllDccs(AtSdhLine self)
    {
    if (self->dccChannel[cAtSdhLineSecDccId])
        {
        AtObjectDelete((AtObject) self->dccChannel[cAtSdhLineSecDccId]);
        self->dccChannel[cAtSdhLineSecDccId] = NULL;
        }

    if (self->dccChannel[cAtSdhLineLineDccId])
        {
        AtObjectDelete((AtObject) self->dccChannel[cAtSdhLineLineDccId]);
        self->dccChannel[cAtSdhLineLineDccId] = NULL;
        }

    self->dccLayersMask = 0;
    }

static void Delete(AtObject self)
    {
    DeleteAllDccs((AtSdhLine) self);
    AllStsDelele((AtSdhLine)self);

    m_AtObjectMethods->Delete(self);
    }

static uint32 SimulationDatabaseSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtSdhLineSimulationDb);
    }

static eBool ShouldInitSerdesController(AtSdhLine self)
    {
    /* Some products do not want this, then let them determine */
    AtUnused(self);
    return cAtTrue;
    }

static AtSdhSts StsCreate(AtSdhLine self, uint8 sts1)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    AtSdhSts newSts = AtSdhStsNew(sts1, cAtSdhChannelTypeSts, sdhModule);
    if (newSts == NULL)
        return NULL;

    SdhChannelLineIdSet((AtSdhChannel)newSts, (uint8)AtChannelIdGet((AtChannel)self));
    SdhChannelSts1IdSet((AtSdhChannel)newSts, sts1);

    return newSts;
    }

static eAtRet AllStsCreate(AtSdhLine self)
    {
    uint8 sts1, i;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)self);
    uint32 memorySize = sizeof(AtSdhSts) * numSts;
    AtSdhSts *allSts1s;

    /* Allocate enough memory */
    allSts1s = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allSts1s == NULL)
        return cAtErrorRsrcNoAvail;
    mMethodsGet(osal)->MemInit(osal, allSts1s, 0, memorySize);

    /* Then create all Line objects */
    for (sts1 = 0; sts1 < numSts; sts1++)
        {
        allSts1s[sts1] = StsCreate(self, sts1);

        /* Cannot create one STS1, free all of created resources */
        if (allSts1s[sts1] == NULL)
            {
            for (i = 0; i < sts1; i++)
                AtObjectDelete((AtObject)allSts1s[i]);
            mMethodsGet(osal)->MemFree(osal, allSts1s);

            return cAtErrorRsrcNoAvail;
            }
        }

    /* Creating done */
    self->allSts1s = allSts1s;
    return cAtOk;
    }

static AtSdhSts StsGet(AtSdhLine self, uint8 stsId)
    {
    if (stsId >= AtSdhChannelNumSts((AtSdhChannel)self))
        return NULL;

    if (self->allSts1s == NULL)
        AllStsCreate(self);

    if (self->allSts1s == NULL)
        return NULL;

    return self->allSts1s[stsId];
    }

static void AllStsDelele(AtSdhLine self)
    {
    uint8 numSts, i;
    AtOsal osal = AtSharedDriverOsalGet();

    if (self->allSts1s == NULL)
        return;

    numSts = AtSdhChannelNumSts((AtSdhChannel)self);
    for (i = 0; i < numSts; i++)
        AtObjectDelete((AtObject)self->allSts1s[i]);

    mMethodsGet(osal)->MemFree(osal, self->allSts1s);
    self->allSts1s = NULL;
    }

static AtFailureProfiler FailureProfilerObjectCreate(AtChannel self)
    {
    return AtFailureProfilerFactorySdhLineProfilerCreate(AtChannelProfilerFactory(self), self);
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtSdhLineSharedQuerier();
    }

static eAtModuleSdhRet DoRateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    eAtRet ret;
    AtModule sdhModule, berModule;
    eAtSdhLineRate currentRate;

    if (!mLineIsValid(self))
        {
        AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation, "%s is called with NULL object\r\n", AtFunction);
        return cAtErrorNullPointer;
        }

    if (!AtSdhLineRateIsSupported(self, rate))
        mChannelReturn(self, cAtErrorModeNotSupport);

    if (!RateIsChanged(self, rate))
        mChannelReturn(self, cAtOk);

    ret = mMethodsGet(self)->CanChangeRate(self, rate);
    if (ret != cAtOk)
        mChannelReturn(self, ret);

    sdhModule = AtChannelModuleGet((AtChannel)self);
    berModule = AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleBer);

    if (berModule)
        AtModuleLock(berModule);

    /* Make changes on SDH module */
    currentRate = AtSdhLineRateGet(self);

    mMethodsGet(self)->RateWillChange(self, currentRate, rate);

    AtModuleLock(sdhModule);
    CacheRateSet(self, rate);
    ret = mMethodsGet(self)->RateSet(self, rate);

    /* Not init when device is in warm restore process */
    if ((ret == cAtOk) && (!AtChannelDeviceInWarmRestore((AtChannel)self)))
        ret |= AtChannelInit((AtChannel)self);
    AtModuleUnLock(sdhModule);

    /* BER engines need to be recreated too, just because Line rate is changed */
    if ((rate != currentRate) && mMethodsGet(self)->HasBerControllers(self))
        ret |= mMethodsGet(self)->BerControllersReCreate(self);

    if (berModule)
        AtModuleUnLock(berModule);

    if (ret == cAtOk)
        mMethodsGet(self)->RateDidChange(self, currentRate, rate);

    mChannelReturn(self, ret);
    }

static eAtRet DccChannelInterruptDisable(AtChannel dccChannel)
    {
    eAtRet ret = cAtOk;
    AtPw pw;

    if (dccChannel == NULL)
        return cAtOk;

    ret |= AtChannelInterruptMaskSet(dccChannel, AtChannelInterruptMaskGet(dccChannel), 0);
    pw = AtChannelBoundPwGet(dccChannel);
    if (pw)
        ret |= AtChannelInterruptMaskSet((AtChannel)pw, AtChannelInterruptMaskGet((AtChannel)pw), 0);

    return ret;
    }

static eAtRet AllDccChannelsInterruptDisable(AtSdhLine self)
    {
    eAtRet ret = cAtOk;
    ret |= DccChannelInterruptDisable((AtChannel)AtSdhLineDccChannelGet(self, cAtSdhLineDccLayerSection));
    ret |= DccChannelInterruptDisable((AtChannel)AtSdhLineDccChannelGet(self, cAtSdhLineDccLayerLine));
    return ret;
    }

static eAtRet SubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    eAtRet ret;
    ret  = AllDccChannelsInterruptDisable((AtSdhLine)self);
    ret |= m_AtSdhChannelMethods->SubChannelsInterruptDisable(self, onlySubChannels);
    return ret;
    }

static AtSdhChannel HideChannelGet(AtSdhChannel self)
    {
    return (AtSdhChannel)AtSdhLineHideLineGet((AtSdhLine)self, 0);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, BerControllerGet);
        mMethodOverride(m_AtSdhChannelOverride, BerControllerCreate);
        mMethodOverride(m_AtSdhChannelOverride, BerControllerDelete);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, CanHaveMapping);
        mMethodOverride(m_AtSdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtSdhChannelOverride, HasAlarmForwarding);
        mMethodOverride(m_AtSdhChannelOverride, FailureForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, SubChannelsInterruptDisable);
        mMethodOverride(m_AtSdhChannelOverride, HideChannelGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, SimulationDatabaseSize);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        mMethodOverride(m_AtChannelOverride, StatusClear);

        /* For standby driver */
        mMethodOverride(m_AtChannelOverride, CacheSize);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);

        /* Work with Surveillance */
        mMethodOverride(m_AtChannelOverride, SurEngineObjectCreate);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectDelete);
        mMethodOverride(m_AtChannelOverride, FailureProfilerObjectCreate);

        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhLine self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

static eBool CanSetTxK1(AtSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanSetTxK2(AtSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanSetTxS1(AtSdhLine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPrbsEngine PrbsEngineGet(AtSdhLine self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool HasFramer(AtSdhLine self)
    {
    AtModuleSdh module = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    return AtModuleSdhLineHasFramer(module, self);
    }

static eBool CanCreateDccChannel(AtSdhLine self, eAtSdhLineDccLayer layers)
    {
    if (self->dccLayersMask == cInvalidUint8)
        return cAtFalse;

    return (layers & self->dccLayersMask) ? cAtFalse : cAtTrue;
    }

static uint8 DccObjectIndex(eAtSdhLineDccLayer layers)
    {
    switch ((uint32)layers)
        {
        case cAtSdhLineDccLayerSection      : return cAtSdhLineSecDccId;
        case cAtSdhLineDccLayerLine         : return cAtSdhLineLineDccId;
        case cAtSdhLineDccLayerAll          : return cAtSdhLineBothSecAndLineDccId;
        default: return cAtSdhLineSecDccId;
        }
    }

static AtHdlcChannel DccChannelCreate(AtSdhLine self, eAtSdhLineDccLayer layers)
    {
    eAtRet ret;
    uint8 objectIndex;
    AtChannel newDcc;
    AtModuleSdh module = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);

    if (!AtModuleSdhDccLayerIsSupported(module, self, layers))
        return NULL;

    /* Already created */
    objectIndex = DccObjectIndex(layers);
    if (self->dccChannel[objectIndex])
        return self->dccChannel[objectIndex];

    if (!CanCreateDccChannel(self, layers))
        return NULL;

    if (self->dccChannel[objectIndex] == NULL)
        {
        self->dccChannel[objectIndex] = AtModuleSdhDccChannelCreate(module, self, layers);
        if (self->dccChannel[objectIndex] == NULL)
            return NULL;

        newDcc = (AtChannel) self->dccChannel[objectIndex];
        ret = AtChannelInit(newDcc);
        if (ret != cAtOk)
            {
            AtChannelLog(newDcc,
                         cAtLogLevelCritical,
                         AtSourceLocation,
                         " DCC Init is failed with ret = %s \r\n",
                         AtRet2String(ret));
            }

        if (layers == cAtSdhLineDccLayerAll)
            self->dccLayersMask = cInvalidUint8;
        else
            self->dccLayersMask = (uint8)(self->dccLayersMask | layers);
        }

    return self->dccChannel[objectIndex];
    }

static eBool DccLineAndSectionIsUsed(AtSdhLine self)
    {
    return (self->dccLayersMask == cInvalidUint8) ? cAtTrue : cAtFalse;
    }

static AtHdlcChannel DccChannelGet(AtSdhLine self, eAtSdhLineDccLayer layers)
    {
    AtModuleSdh module = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);

    if (!AtModuleSdhDccLayerIsSupported(module, self, layers))
        return NULL;

    if (DccLineAndSectionIsUsed(self))
        {
        if (layers == cAtSdhLineDccLayerAll)
            return self->dccChannel[cAtSdhLineBothSecAndLineDccId];

        return NULL;
        }

    return self->dccChannel[DccObjectIndex(layers)];
    }

static eAtRet DccChannelDelete(AtSdhLine self, AtHdlcChannel dccChannel)
    {
    eAtRet ret = cAtOk;
    /* Not exist */
    if (dccChannel == NULL)
        return cAtOk;

    ret = AtChannelEnable((AtChannel)dccChannel, cAtFalse);
    if (ret != cAtOk)
        return ret;

    AtChannelInterruptMaskSet((AtChannel)dccChannel, AtChannelInterruptMaskGet((AtChannel)dccChannel), 0);

    if (DccLineAndSectionIsUsed(self))
        {
        if (self->dccChannel[cAtSdhLineBothSecAndLineDccId] != dccChannel)
            return cAtErrorInvlParm;

        AtObjectDelete((AtObject) dccChannel);
        self->dccChannel[cAtSdhLineBothSecAndLineDccId] = NULL;
        self->dccLayersMask = 0;
        return cAtOk;
        }

    if (self->dccChannel[cAtSdhLineSecDccId] == dccChannel)
        {
        AtObjectDelete((AtObject) dccChannel);
        self->dccChannel[cAtSdhLineSecDccId] = NULL;
        self->dccLayersMask &= (uint8) (~cAtSdhLineDccLayerSection);
        return cAtOk;
        }

    if (self->dccChannel[cAtSdhLineLineDccId] == dccChannel)
        {
        AtObjectDelete((AtObject) dccChannel);
        self->dccChannel[cAtSdhLineLineDccId] = NULL;
        self->dccLayersMask &= (uint8) (~cAtSdhLineDccLayerLine);
        return cAtOk;
        }

    return cAtErrorInvlParm;
    }

static eAtRet OneHdlcCleanup(AtSdhLine self, AtHdlcChannel hdlc)
    {
    eAtRet ret = cAtOk;
    AtPw pw;
    AtChannel hdlcChannel = (AtChannel)hdlc;

    /* Nothing to cleanup */
    if (hdlc == NULL)
        return cAtOk;

    ret |= AtChannelRxEnable(hdlcChannel, cAtFalse);
    ret |= AtChannelTxEnable(hdlcChannel, cAtFalse);

    pw = AtChannelBoundPwGet(hdlcChannel);
    if (pw)
        {
        AtModulePw pwModule = (AtModulePw)AtChannelModuleGet((AtChannel)pw);
        ret |= AtModulePwDeletePwObject(pwModule, pw);
        }

    ret |= AtSdhLineDccChannelDelete(self, hdlc);

    return ret;
    }

static eAtRet DccHdlcCleanup(AtSdhLine self)
    {
    eAtRet ret = cAtOk;

    ret |= OneHdlcCleanup(self, AtSdhLineDccChannelGet(self, cAtSdhLineDccLayerSection));
    ret |= OneHdlcCleanup(self, AtSdhLineDccChannelGet(self, cAtSdhLineDccLayerLine));

    return ret;
    }

static eAtRet AllChannelsBerMonitoringDisable(AtSdhChannel channel)
    {
    AtBerController controller = AtSdhChannelBerControllerGet(channel);
    eAtRet ret = cAtOk;
    uint8 subChannel_i;
    uint8 numSubChannels = AtSdhChannelNumberOfSubChannelsGet(channel);

    /* Disable itself */
    if (controller)
        ret |= AtBerControllerEnable(controller, cAtFalse);

    /* All of its sub channels */
    for (subChannel_i = 0; subChannel_i < numSubChannels; subChannel_i++)
        {
        AtSdhChannel subChannel = AtSdhChannelSubChannelGet(channel, subChannel_i);
        ret |= AllChannelsBerMonitoringDisable(subChannel);
        }

    return ret;
    }

static eAtRet Unuse(AtSdhLine self)
    {
    AtSdhChannel line = (AtSdhChannel)self;
    eAtRet ret = cAtOk;

    ret |= AtSdhChannelSubChannelsInterruptDisable(line, cAtFalse);
    ret |= AllChannelsBerMonitoringDisable(line);

    return ret;
    }

static eAtRet Z1MonitorPositionSet(AtSdhLine self, uint8 sts1)
    {
    AtUnused(self);
    AtUnused(sts1);
    return cAtErrorModeNotSupport;
    }

static uint8 Z1MonitorPositionGet(AtSdhLine self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static eBool Z1CanMonitor(AtSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Z2MonitorPositionSet(AtSdhLine self, uint8 sts1)
    {
    AtUnused(self);
    AtUnused(sts1);
    return cAtErrorModeNotSupport;
    }

static uint8 Z2MonitorPositionGet(AtSdhLine self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static eBool Z2CanMonitor(AtSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool RowIsValid(uint32 row)
    {
    return (row < 9) ? cAtTrue : cAtFalse;
    }

static eBool ColumnIsValid(AtSdhLine self, uint32 column)
    {
    return (column < (AtSdhChannelNumSts((AtSdhChannel)self) * 3)) ? cAtTrue : cAtFalse;
    }

static uint8 ColumnToSts1(AtSdhLine self, uint32 column)
    {
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)self);
    uint8 numSts3 = (uint8)(numSts / 3);
    uint8 sts3 = (uint8)(column % numSts3);
    uint8 sts1 = (uint8)(((column % numSts) / numSts3) % 3);
    return (uint8)(sts3 * 3 + sts1);
    }

static uint8 ColumnInOneSts1(AtSdhLine self, uint32 column)
    {
    uint8 numSts = AtSdhChannelNumSts((AtSdhChannel)self);
    return (uint8)((column / numSts) % 3);
    }

static eAtSdhLineOverheadByte Sts1No0_OverheadByteGet(AtSdhLine self, uint32 row, uint32 column)
    {
    static uint32 overheadBytes[9][3] = {
             {cAtSdhLineRsOverheadByteA1,  cAtSdhLineRsOverheadByteA2,  cAtSdhLineRsOverheadByteJ0},
             {cAtSdhLineRsOverheadByteB1,  cAtSdhLineRsOverheadByteE1,  cAtSdhLineRsOverheadByteF1},
             {cAtSdhLineRsOverheadByteD1,  cAtSdhLineRsOverheadByteD2,  cAtSdhLineRsOverheadByteD3},
             {cAtSdhLineRsOverheadByteH1,  cAtSdhLineRsOverheadByteH2,  cAtSdhLineRsOverheadByteH3},
             {cAtSdhLineMsOverheadByteB2,  cAtSdhLineMsOverheadByteK1,  cAtSdhLineMsOverheadByteK2},
             {cAtSdhLineMsOverheadByteD4,  cAtSdhLineMsOverheadByteD5,  cAtSdhLineMsOverheadByteD6},
             {cAtSdhLineMsOverheadByteD7,  cAtSdhLineMsOverheadByteD8,  cAtSdhLineMsOverheadByteD9},
             {cAtSdhLineMsOverheadByteD10, cAtSdhLineMsOverheadByteD11, cAtSdhLineMsOverheadByteD12},
             {cAtSdhLineMsOverheadByteS1,  cAtSdhLineMsOverheadByteM1,  cAtSdhLineMsOverheadByteE2}};

    if (AtSdhLineRateGet(self) == cAtSdhLineRateStm0)
        overheadBytes[8][1] = cAtSdhLineMsOverheadByteM1;
    else
        overheadBytes[8][1] = cAtSdhLineMsOverheadByteZ2;

    return overheadBytes[row][column];
    }

static eBool IsZ2Byte(eAtSdhLineOverheadByte overheadByte)
    {
    return (overheadByte == cAtSdhLineMsOverheadByteZ2) ? cAtTrue : cAtFalse;
    }

static eAtSdhLineOverheadByte Sts1NoX_OverheadByteGet(AtSdhLine self, uint8 sts1, uint32 row, uint32 column)
    {
    static uint32 overheadBytes[9][3] = {
             {cAtSdhLineRsOverheadByteA1,        cAtSdhLineRsOverheadByteA2,        cAtSdhLineRsOverheadByteZ0},
             {cAtSdhLineRsOverheadByteUndefined, cAtSdhLineRsOverheadByteUndefined, cAtSdhLineRsOverheadByteUndefined},
             {cAtSdhLineRsOverheadByteUndefined, cAtSdhLineRsOverheadByteUndefined, cAtSdhLineRsOverheadByteUndefined},
             {cAtSdhLineRsOverheadByteH1,        cAtSdhLineRsOverheadByteH2,        cAtSdhLineRsOverheadByteH3},
             {cAtSdhLineMsOverheadByteB2,        cAtSdhLineMsOverheadByteUndefined, cAtSdhLineMsOverheadByteUndefined},
             {cAtSdhLineMsOverheadByteUndefined, cAtSdhLineMsOverheadByteUndefined, cAtSdhLineMsOverheadByteUndefined},
             {cAtSdhLineMsOverheadByteUndefined, cAtSdhLineMsOverheadByteUndefined, cAtSdhLineMsOverheadByteUndefined},
             {cAtSdhLineMsOverheadByteUndefined, cAtSdhLineMsOverheadByteUndefined, cAtSdhLineMsOverheadByteUndefined},
             {cAtSdhLineMsOverheadByteZ1,        cAtSdhLineMsOverheadByteZ2,        cAtSdhLineMsOverheadByteUndefined}};
    eAtSdhLineRate rate = AtSdhLineRateGet(self);

    if (sts1 == 0)
        return 0;

    switch (rate)
        {
        case cAtSdhLineRateStm0:
            return 0;

        case cAtSdhLineRateStm1:
            if (IsZ2Byte(overheadBytes[row][column]))
                return (sts1 == 2) ? cAtSdhLineMsOverheadByteM1 : cAtSdhLineMsOverheadByteZ2;
            break;

        case cAtSdhLineRateStm4:
            if (IsZ2Byte(overheadBytes[row][column]))
                return (sts1 == 6) ? cAtSdhLineMsOverheadByteM1 : cAtSdhLineMsOverheadByteZ2;
            break;

        case cAtSdhLineRateStm16:
            if (IsZ2Byte(overheadBytes[row][column]))
                return (sts1 == 6) ? cAtSdhLineMsOverheadByteM1 : cAtSdhLineMsOverheadByteZ2;
            break;

        case cAtSdhLineRateStm64:
            if (overheadBytes[row][column] == cAtSdhLineMsOverheadByteZ1)
                if (sts1 >= 48)
                    return cAtSdhLineMsOverheadByteUndefined;

            if (IsZ2Byte(overheadBytes[row][column]))
                {
                if (sts1 >= 48)
                    return cAtSdhLineMsOverheadByteUndefined;

                if (sts1 == 3)
                    {
                    if (AtSdhChannelModeGet((AtSdhChannel)self) == cAtSdhChannelModeSdh)
                        return cAtSdhLineMsOverheadByteM0;
                    else
                        return cAtSdhLineMsOverheadByteZ2;
                    }

                return (sts1 == 6) ? cAtSdhLineMsOverheadByteM1 : cAtSdhLineMsOverheadByteZ2;
                }
            break;

        case cAtSdhLineRateInvalid:
            return 0;
        default:
            return 0;
        }

    return overheadBytes[row][column];
    }

static eAtSdhLineOverheadByte Stm0OverheadByteGet(AtSdhLine self, uint32 row, uint32 column)
    {
    return Sts1No0_OverheadByteGet(self, row, column);
    }

static eAtSdhLineOverheadByte StmMOverheadByteGet(AtSdhLine self, uint32 row, uint32 column)
    {
    uint8 sts1 = ColumnToSts1(self, column);

    if (sts1 == 0)
        return Sts1No0_OverheadByteGet(self, row, ColumnInOneSts1(self, column));

    return Sts1NoX_OverheadByteGet(self, sts1, row, ColumnInOneSts1(self, column));
    }

static eAtSdhLineOverheadByte OverheadByteGet(AtSdhLine self, uint32 row, uint32 column)
    {
    if (!RowIsValid(row) || !ColumnIsValid(self, column))
        return 0;

    if (AtSdhLineRateGet(self) == cAtSdhLineRateStm0)
        return Stm0OverheadByteGet(self, row, column);

    return StmMOverheadByteGet(self, row, column);
    }

static AtSdhLine HideLineGet(AtSdhLine self, uint8 sts1)
    {
    AtUnused(self);
    AtUnused(sts1);
    return NULL;
    }

static void MethodsInit(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, RateSet);
        mMethodOverride(m_methods, RateIsSupported);
        mMethodOverride(m_methods, RateGet);
        mMethodOverride(m_methods, CanChangeRate);
        mMethodOverride(m_methods, RateDidChange);
        mMethodOverride(m_methods, RateWillChange);
        mMethodOverride(m_methods, TxS1Set);
        mMethodOverride(m_methods, TxS1Get);
        mMethodOverride(m_methods, RxS1Get);
        mMethodOverride(m_methods, TxK1Set);
        mMethodOverride(m_methods, TxK1Get);
        mMethodOverride(m_methods, RxK1Get);
        mMethodOverride(m_methods, TxK2Set);
        mMethodOverride(m_methods, TxK2Get);
        mMethodOverride(m_methods, RxK2Get);
        mMethodOverride(m_methods, LedStateSet);
        mMethodOverride(m_methods, LedStateGet);
        mMethodOverride(m_methods, RsBerControllerGet);
        mMethodOverride(m_methods, MsBerControllerGet);
        mMethodOverride(m_methods, RsBerControllerCreate);
        mMethodOverride(m_methods, MsBerControllerCreate);
        mMethodOverride(m_methods, BerControllersReCreate);
        mMethodOverride(m_methods, HasBerControllers);
        mMethodOverride(m_methods, ScrambleEnable);
        mMethodOverride(m_methods, ScrambleIsEnabled);
        mMethodOverride(m_methods, SerdesController);
        mMethodOverride(m_methods, SwitchedLineGet);
        mMethodOverride(m_methods, BridgedLineGet);
        mMethodOverride(m_methods, Switch);
        mMethodOverride(m_methods, SwitchRelease);
        mMethodOverride(m_methods, Bridge);
        mMethodOverride(m_methods, BridgeRelease);
        mMethodOverride(m_methods, OverheadBytesInsertEnable);
        mMethodOverride(m_methods, OverheadBytesInsertIsEnabled);
        mMethodOverride(m_methods, Au4_ncGet);
        mMethodOverride(m_methods, Vc4_ncGet);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, CanSetTxK1);
        mMethodOverride(m_methods, CanSetTxK2);
        mMethodOverride(m_methods, CanSetTxS1);
        mMethodOverride(m_methods, PrbsEngineGet);
        mMethodOverride(m_methods, HasFramer);
        mMethodOverride(m_methods, ShouldInitSerdesController);
        mMethodOverride(m_methods, HideLineGet);

        /* Zn monitoring */
        mMethodOverride(m_methods, Z1MonitorPositionSet);
        mMethodOverride(m_methods, Z1MonitorPositionGet);
        mMethodOverride(m_methods, Z1CanMonitor);
        mMethodOverride(m_methods, Z2MonitorPositionSet);
        mMethodOverride(m_methods, Z2MonitorPositionGet);
        mMethodOverride(m_methods, Z2CanMonitor);

        /* DCC HDLC */
        mMethodOverride(m_methods, DccHdlcCleanup);
        }

    mMethodsSet(self, &m_methods);
    }

AtSdhLine AtSdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhLine));

    /* Super constructor */
    if (AtSdhChannelObjectInit((AtSdhChannel)self, channelId, cAtSdhChannelTypeLine, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController SdhLineRsBerControllerGet(AtSdhLine self)
    {
    if (self)
        return self->rsBerController;

    return NULL;
    }

AtBerController SdhLineMsBerControllerGet(AtSdhLine self)
    {
    if (self)
        return self->msBerController;

    return NULL;
    }

AtApsEngine AtSdhLineApsEngineGet(AtSdhLine self)
    {
    if (self)
        return self->apsEngine;

    return NULL;
    }

void AtSdhLineApsEngineSet(AtSdhLine self, AtApsEngine engine)
    {
    if (self)
        self->apsEngine = engine;
    }

void AtSdhLineInterruptProcess(AtSdhLine self, uint8 line, AtHal hal)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self, line, hal);
    }

eBool AtSdhLineCanSetTxK1(AtSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->CanSetTxK1(self);
    return cAtFalse;
    }

eBool AtSdhLineCanSetTxK2(AtSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->CanSetTxK2(self);
    return cAtFalse;
    }

eBool AtSdhLineCanSetTxS1(AtSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->CanSetTxS1(self);
    return cAtFalse;
    }

eAtModuleSdhRet AtSdhLineSoftwareRateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    if (self)
        return SoftwareRateSet(self, rate);
    return cAtErrorObjectNotExist;
    }

eBool AtSdhLineHasFramer(AtSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->HasFramer(self);
    return cAtFalse;
    }

eAtRet AtSdhLineDccHdlcCleanup(AtSdhLine self)
    {
    if (self)
        return mMethodsGet(self)->DccHdlcCleanup(self);
    return cAtErrorNullPointer;
    }

/*
 * Get associated eye scan controller
 *
 * @param self This line
 *
 * @return AtEyeScanController object if has. Otherwise, NULL is returned.
 */
AtEyeScanController AtSdhLineEyeScanControllerGet(AtSdhLine self)
    {
    AtSerdesController serdesController = AtSdhLineSerdesController(self);
    if (serdesController)
        return AtSerdesControllerEyeScanControllerGet(serdesController);
    return NULL;
    }

eAtRet AtSdhLineUnuse(AtSdhLine self)
    {
    if (self)
        return Unuse(self);
    return cAtErrorObjectNotExist;
    }

/*
 * Set line mode to SONET or SDH
 *
 * @param self This line
 * @param mode
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineModeSet(AtSdhLine self, eAtSdhLineMode mode)
    {
    return AtSdhChannelModeSet((AtSdhChannel)self, (eAtSdhChannelMode)mode);
    }

/*
 * Get line mode (SONET/SDH)
 *
 * @param self This line
 *
 * @return Line mode
 */
eAtSdhLineMode AtSdhLineModeGet(AtSdhLine self)
    {
    return (eAtSdhChannelMode)AtSdhChannelModeGet((AtSdhChannel)self);
    }

eAtSdhLineOverheadByte AtSdhLineOverheadByteGet(AtSdhLine self, uint32 row, uint32 column)
    {
    if (self)
        return OverheadByteGet(self, row, column);
    return 0;
    }

AtSdhLine AtSdhLineHideLineGet(AtSdhLine self, uint8 sts1)
    {
    if (self)
        return mMethodsGet(self)->HideLineGet(self, sts1);
    return NULL;
    }

/**
 * @addtogroup AtSdhLine
 * @{
 */
/**
 * Set Line rate
 *
 * @param self This line
 * @param rate @ref eAtSdhLineRate "Line Rate"
 */
eAtModuleSdhRet AtSdhLineRateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    eAtRet ret;

    mOneParamApiLogStart(rate);
    ret = DoRateSet(self, rate);
    AtDriverApiLogStop();

    return ret;
    }

/**
 * Check if a rate is supported
 *
 * @param self This Line
 * @param rate @ref eAtSdhLineRate "Rate to check"
 *
 * @return cAtTrue if this Line can support this rate. Otherwise, cAtFalse is
 *         returned
 */
eBool AtSdhLineRateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    mOneParamAttributeGet(RateIsSupported, rate, eBool, cAtFalse);
    }

/**
 * Get rate
 *
 * @param self This line
 *
 * @return @ref eAtSdhLineRate "Line rate"
 */
eAtSdhLineRate AtSdhLineRateGet(AtSdhLine self)
    {
    mAttributeGetWithCache(RateGet, eAtSdhLineRate, cAtSdhLineRateInvalid);
    }

/**
 * Switch traffic that is being selected from this line to another line
 *
 * @param self This line
 * @param toLine The line that traffic will be switched to
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineSwitch(AtSdhLine self, AtSdhLine toLine)
    {
    mObjectSet(Switch, toLine);
    }

/**
 * Get line that traffic of this line is switched to
 *
 * @param self This line
 *
 * @return Line that traffic of this line is switched to
 */
AtSdhLine AtSdhLineSwitchedLineGet(AtSdhLine self)
    {
    mNoParamObjectGet(SwitchedLineGet, AtSdhLine);
    }

/**
 * Release switch to bring traffic back to this line
 *
 * @param self This line
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineSwitchRelease(AtSdhLine self)
    {
    mNoParamCall(SwitchRelease, eAtModuleSdhRet, cAtError);
    }

/**
 * Bridge (duplicate) traffic from this line to another line
 *
 * @param self This line
 * @param toLine Line that traffic of this line will be bridged (duplicated) on
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineBridge(AtSdhLine self, AtSdhLine toLine)
    {
    mObjectSet(Bridge, toLine);
    }

/**
 * Get line that traffic of this line is bridged (duplicated) on
 *
 * @param self This line
 *
 * @return AT return code
 */
AtSdhLine AtSdhLineBridgedLineGet(AtSdhLine self)
    {
    mNoParamObjectGet(BridgedLineGet, AtSdhLine);
    }

/**
 * Release traffic bridging
 *
 * @param self This line
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineBridgeRelease(AtSdhLine self)
    {
    mNoParamCall(BridgeRelease, eAtModuleSdhRet, cAtError);
    }

/**
 * Set LED state. Turn it ON/OFF or make it blink
 *
 * @param self This line
 * @param ledState @ref eAtLedState "LED states"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineLedStateSet(AtSdhLine self, eAtLedState ledState)
    {
    mNumericalAttributeSet(LedStateSet, ledState);
    }

/**
 * Get LED state to determine it is ON/OFF or blinking
 *
 * @param self This line
 *
 * @return @ref eAtLedState "LED state"
 */
eAtLedState AtSdhLineLedStateGet(AtSdhLine self)
    {
    mAttributeGet(LedStateGet, eAtLedState, cAtLedStateOff);
    }

/**
 * Transmit S1 byte
 *
 * @param self This line
 * @param value S1 value
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineTxS1Set(AtSdhLine self, uint8 value)
    {
    mNumericalAttributeSetWithCache(TxS1Set, value);
    }

/**
 * Get transmitted S1 byte
 *
 * @param self This line
 *
 * @return Transmitted S1 byte
 */
uint8 AtSdhLineTxS1Get(AtSdhLine self)
    {
    mAttributeGetWithCache(TxS1Get, uint8, 0);
    }

/**
 * Get received S1 byte
 *
 * @param self This line
 *
 * @return Received S1 byte
 */
uint8 AtSdhLineRxS1Get(AtSdhLine self)
    {
    mAttributeGet(RxS1Get, uint8, 0);
    }

/**
 * Transmit K1 byte
 *
 * @param self This line
 * @param value K1 value
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineTxK1Set(AtSdhLine self, uint8 value)
    {
    mNumericalAttributeSetWithCache(TxK1Set, value);
    }

/**
 * Get transmitted K1 byte
 *
 * @param self This line
 *
 * @return Transmitted K1 byte
 */
uint8 AtSdhLineTxK1Get(AtSdhLine self)
    {
    mAttributeGetWithCache(TxK1Get, uint8, 0);
    }

/**
 * Get received K1 byte
 *
 * @param self This line
 *
 * @return Received K1 byte
 */
uint8 AtSdhLineRxK1Get(AtSdhLine self)
    {
    mAttributeGet(RxK1Get, uint8, 0);
    }

/**
 * Transmit K2 byte
 *
 * @param self This line
 * @param value K2 value
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineTxK2Set(AtSdhLine self, uint8 value)
    {
    mNumericalAttributeSetWithCache(TxK2Set, value);
    }

/**
 * Get transmitted K2 byte
 *
 * @param self This line
 *
 * @return Transmitted K2 byte
 */
uint8 AtSdhLineTxK2Get(AtSdhLine self)
    {
    mAttributeGetWithCache(TxK2Get, uint8, 0);
    }

/**
 * Get received K2 byte
 *
 * @param self This line
 *
 * @return Received K2 byte
 */
uint8 AtSdhLineRxK2Get(AtSdhLine self)
    {
    mAttributeGet(RxK2Get, uint8, 0);
    }

/**
 * Get AUG-64 in line
 *
 * @param self This line
 * @param aug64Id AUG-64 ID in line
 *
 * @return AUG-64
 */
AtSdhAug AtSdhLineAug64Get(AtSdhLine self, uint8 aug64Id)
    {
    /* Maximum line rate is STM-64 */
    if (AtSdhLineRateGet(self) != cAtSdhLineRateStm64)
        return NULL;

    if (aug64Id > 0)
        return NULL;

    return (AtSdhAug)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    }

/**
 * Get AUG-16 in line
 *
 * @param self This line
 * @param aug16Id AUG-16 ID in line
 *
 * @return AUG-16
 */
AtSdhAug AtSdhLineAug16Get(AtSdhLine self, uint8 aug16Id)
    {
    eAtSdhLineRate rate;
    const uint8 cNumAug16sInAug64 = 4;
    AtSdhChannel aug64;

    if (self == NULL)
        return NULL;

    rate = AtSdhLineRateGet(self);
    if (rate < cAtSdhLineRateStm16)
        return NULL;

    if (rate == cAtSdhLineRateStm16)
        return (aug16Id == 0) ? (AtSdhAug)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0) : NULL;

    if (aug16Id >= cNumAug16sInAug64)
        return NULL;

    /* Need to find AUG-64 to get its AUG-16 */
    aug64 = (AtSdhChannel)AtSdhLineAug64Get(self, 0);
    if (aug64 == NULL)
        return NULL;

    if (AtSdhChannelMapTypeGet(aug64) != cAtSdhAugMapTypeAug64Map4xAug16s)
        return NULL;

    return (AtSdhAug)AtSdhChannelSubChannelGet((AtSdhChannel)aug64, aug16Id);
    }

/**
 * Get AUG-4
 *
 * @param self This line
 * @param aug4Id AUG-4 ID in line
 *
 * @return AUG-4
 */
AtSdhAug AtSdhLineAug4Get(AtSdhLine self, uint8 aug4Id)
    {
    eAtSdhLineRate rate;
    AtSdhChannel aug16;
    static const uint8 cNumAug4sInAug16 = 4;
    uint8 localAug4Id;

    if (self == NULL)
        return NULL;

    rate = AtSdhLineRateGet(self);
    if (rate < cAtSdhLineRateStm4)
        return NULL;

    /* 1 AUG-4 in STM-4 */
    if (rate == cAtSdhLineRateStm4)
        return (aug4Id == 0) ? (AtSdhAug)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0) : NULL;

    /* Need to find AUG-16 to get its AUG-4 */
    aug16 = (AtSdhChannel)AtSdhLineAug16Get(self, (aug4Id / cNumAug4sInAug16));
    if (aug16 == NULL)
        return NULL;

    if (AtSdhChannelMapTypeGet(aug16) != cAtSdhAugMapTypeAug16Map4xAug4s)
        return NULL;

    localAug4Id = aug4Id % cNumAug4sInAug16;
    if (localAug4Id >= cNumAug4sInAug16)
        return NULL;

    return (AtSdhAug)AtSdhChannelSubChannelGet(aug16, localAug4Id);
    }

/**
 * Get AUG-1
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 *
 * @return AUG-1 which is of @ref AtSdhAug
 */
AtSdhAug AtSdhLineAug1Get(AtSdhLine self, uint8 aug1Id)
    {
    eAtSdhLineRate rate;
    uint16 aug4Id, localAug1InAug4;
    static const uint8 cNumAug1sInAug4 = 4;
    AtSdhChannel aug4;

    if (self == NULL)
        return NULL;

    rate = AtSdhLineRateGet(self);
    if (rate < cAtSdhLineRateStm1)
        return NULL;

    /* Only one AUG-1 in STM-1 */
    if (rate == cAtSdhLineRateStm1)
        return (aug1Id == 0) ? (AtSdhAug)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0) : NULL;

    /* Need to find AUG-4 to get its AUG-1 */
    aug4Id = (aug1Id / cNumAug1sInAug4);
    aug4 = (AtSdhChannel)AtSdhLineAug4Get(self, (uint8)aug4Id);
    if (aug4 == NULL)
        return NULL;

    /* Make sure that this AUG-4 map AUG-1s */
    if (AtSdhChannelMapTypeGet(aug4) != cAtSdhAugMapTypeAug4Map4xAug1s)
        return NULL;

    /* Return it */
    localAug1InAug4 = aug1Id % cNumAug1sInAug4;
    return (AtSdhAug)AtSdhChannelSubChannelGet(aug4, (uint8)localAug1InAug4);
    }

/**
 * Get AU-4
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 *
 * @return AU-4 which is of @ref AtSdhAu
 */
AtSdhAu AtSdhLineAu4Get(AtSdhLine self, uint8 aug1Id)
    {
    AtSdhChannel aug1;

    /* Just get AUG-1 first */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(self, aug1Id);
    if (aug1 == NULL)
        return NULL;

    /* Make sure that this AUG-1 map VC */
    if ((aug1 == NULL) || AtSdhChannelMapTypeGet(aug1) != cAtSdhAugMapTypeAug1MapVc4)
        return NULL;

    /* Return AU object inside this AUG-1 */
    return (AtSdhAu)AtSdhChannelSubChannelGet(aug1, 0);
    }

/**
 * Get AU-3
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 IDs
 * @param au3Id [0..2] AU-3 ID
 *
 * @return AU-3 which is of @ref AtSdhAu
 */
AtSdhAu AtSdhLineAu3Get(AtSdhLine self, uint8 aug1Id, uint8 au3Id)
    {
    AtSdhChannel aug1;
    eAtSdhLineRate rate = AtSdhLineRateGet(self);

    /* STM0 rate, only support one AU3 */
    if (rate == cAtSdhLineRateStm0)
        return (AtSdhAu)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);

    /* Just get AUG-1 first */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(self, aug1Id);
    if (aug1 == NULL)
        return NULL;

    /* Make sure that it maps VC-3 */
    if (AtSdhChannelMapTypeGet(aug1) != cAtSdhAugMapTypeAug1Map3xVc3s)
        return NULL;

    /* Return local AU-3 */
    return (AtSdhAu)AtSdhChannelSubChannelGet(aug1, au3Id);
    }

/**
 * Get AU-4-64c
 *
 * @param self This line
 * @param aug64Id AUG-64 ID
 *
 * @return @ref AtSdhAu "AU-4-64c"
 */
AtSdhAu AtSdhLineAu4_64cGet(AtSdhLine self, uint8 aug64Id)
    {
    AtSdhChannel aug64 = (AtSdhChannel)AtSdhLineAug64Get(self, aug64Id);
    if (aug64 == NULL)
        return NULL;

    if (AtSdhChannelMapTypeGet(aug64) == cAtSdhAugMapTypeAug64MapVc4_64c)
        return (AtSdhAu)AtSdhChannelSubChannelGet(aug64, 0);

    return NULL;
    }

/**
 * Get AU-4-16c
 *
 * @param self This line
 * @param aug16Id AUG-16 ID
 *
 * @return @ref AtSdhAu "AU-4-16c"
 */
AtSdhAu AtSdhLineAu4_16cGet(AtSdhLine self, uint8 aug16Id)
    {
    AtSdhChannel aug16 = (AtSdhChannel)AtSdhLineAug16Get(self, aug16Id);
    if (aug16 == NULL)
        return NULL;

    if (AtSdhChannelMapTypeGet(aug16) == cAtSdhAugMapTypeAug16MapVc4_16c)
        return (AtSdhAu)AtSdhChannelSubChannelGet(aug16, 0);

    return NULL;
    }

/**
 * Get AU-4-4c
 *
 * @param self This line
 * @param aug4Id AUG-4 ID in line
 *
 * @return AU-4-4c which is of @ref AtSdhAu
 */
AtSdhAu AtSdhLineAu4_4cGet(AtSdhLine self, uint8 aug4Id)
    {
    /* Just get AUG-4 first */
    AtSdhChannel aug4 = (AtSdhChannel)AtSdhLineAug4Get(self, aug4Id);
    if (aug4 == NULL)
        return NULL;

    /* Make sure that it maps VC */
    if (AtSdhChannelMapTypeGet(aug4) != cAtSdhAugMapTypeAug4MapVc4_4c)
        return NULL;

    /* Return it AU-4-4c sub channel */
    return (AtSdhAu)AtSdhChannelSubChannelGet(aug4, 0);
    }

/**
 * Get TUG-3
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 * @param tug3Id [0..2] TUG-3 ID
 *
 * @return TUG-3 which is of @ref AtSdhTug
 */
AtSdhTug AtSdhLineTug3Get(AtSdhLine self, uint8 aug1Id, uint8 tug3Id)
    {
    /* Just get VC-4 first */
    AtSdhChannel vc4 = (AtSdhChannel)AtSdhLineVc4Get(self, aug1Id);
    if (vc4 == NULL)
        return NULL;

    /* Make sure that it maps TUG-3 */
    if (AtSdhChannelMapTypeGet(vc4) != cAtSdhVcMapTypeVc4Map3xTug3s)
        return NULL;

    /* Return its local TUG-3 sub channel */
    return (AtSdhTug)AtSdhChannelSubChannelGet(vc4, tug3Id);
    }

/**
 * Get TUG-2
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 * @param au3Tug3Id [0..2] AU-3/TUG-3 ID
 * @param tug2Id [0..6] TUG-2 ID
 *
 * @return TUG-2 which is of @ref AtSdhTug
 */
AtSdhTug AtSdhLineTug2Get(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id)
    {
    AtSdhChannel tug3, vc3, aug1;
    eAtSdhLineRate rate = AtSdhLineRateGet(self);

    /* Special handle for STM0 rate */
    if (rate == cAtSdhLineRateStm0)
        {
        vc3 = (AtSdhChannel) AtSdhLineVc3Get(self, 0, 0);
        if (vc3 == NULL)
            return NULL;

        if (AtSdhChannelMapTypeGet(vc3) != cAtSdhVcMapTypeVc3Map7xTug2s)
            return NULL;

        return (AtSdhTug) AtSdhChannelSubChannelGet(vc3, tug2Id);
        }

    /* Get AUG-1 and check whether it maps VC-3 or VC-4 to determine if TUG-2
     * is mapped to TUG-3 or VC-3 */
    aug1 = (AtSdhChannel)AtSdhLineAug1Get(self, aug1Id);
    if (aug1 == NULL)
        return NULL;

    /* TUG-2 can be mapped to TUG-3 */
    if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
        {
        tug3 = (AtSdhChannel)AtSdhLineTug3Get(self, aug1Id, au3Tug3Id);
        if (tug3 == NULL)
            return NULL;

        if (AtSdhChannelMapTypeGet(tug3) != cAtSdhTugMapTypeTug3Map7xTug2s)
            return NULL;

        return (AtSdhTug)AtSdhChannelSubChannelGet(tug3, tug2Id);
        }

    /* TUG-2 can be mapped to VC-3 */
    if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
        {
        vc3 = (AtSdhChannel)AtSdhLineVc3Get(self, aug1Id, au3Tug3Id);
        if (vc3 == NULL)
            return NULL;

        if (AtSdhChannelMapTypeGet(vc3) != cAtSdhVcMapTypeVc3Map7xTug2s)
            return NULL;

        return (AtSdhTug)AtSdhChannelSubChannelGet(vc3, tug2Id);
        }

    /* Unknown mapping type */
    return NULL;
    }

/**
 * Get TU-3
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 * @param tug3Id [0..2] TUG-3 ID
 *
 * @return TU-3 which is of @ref AtSdhTu
 */
AtSdhTu AtSdhLineTu3Get(AtSdhLine self, uint8 aug1Id, uint8 tug3Id)
    {
    /* Get TUG-3 first */
    AtSdhChannel tug3 = (AtSdhChannel)AtSdhLineTug3Get(self, aug1Id, tug3Id);
    if (tug3 == NULL)
        return NULL;

    /* Make sure that it maps VC */
    if (AtSdhChannelMapTypeGet(tug3) != cAtSdhTugMapTypeTug3MapVc3)
        return NULL;

    /* Return is TU-3 sub channel */
    return (AtSdhTu)AtSdhChannelSubChannelGet(tug3, 0);
    }

/**
 * Get TU-11/TU-12
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 * @param au3Tug3Id [0..2] AU-3/TUG-3 ID
 * @param tug2Id [0..7] TUG-2 ID
 * @param tuId [0..3] TU ID
 *
 * @return TU11/TU-12 which is of @ref AtSdhTu
 */
AtSdhTu AtSdhLineTu1xGet(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id, uint8 tuId)
    {
    /* Get TUG-2 first */
    AtSdhChannel tug2 = (AtSdhChannel)AtSdhLineTug2Get(self, aug1Id, au3Tug3Id, tug2Id);
    if (tug2 == NULL)
        return NULL;

    /* Return its TU sub channel */
    return (AtSdhTu)AtSdhChannelSubChannelGet(tug2, tuId);
    }

/**
 * Get VC-4-64c
 *
 * @param self This line
 * @param aug64Id AUG-16 ID
 *
 * @return @ref AtSdhVc "VC-4-64c"
 */
AtSdhVc AtSdhLineVc4_64cGet(AtSdhLine self, uint8 aug64Id)
    {
    /* Get AU-4-64c first */
    AtSdhChannel au4_64c = (AtSdhChannel)AtSdhLineAu4_64cGet(self, aug64Id);
    if (au4_64c == NULL)
        return NULL;

    /* Just return its VC sub channel */
    return (AtSdhVc)AtSdhChannelSubChannelGet(au4_64c, 0);
    }

/**
 * Get VC-4-16c
 *
 * @param self This line
 * @param aug16Id AUG-16 ID
 *
 * @return @ref AtSdhVc "VC-4-16c"
 */
AtSdhVc AtSdhLineVc4_16cGet(AtSdhLine self, uint8 aug16Id)
    {
    /* Get AU-4-16c first */
    AtSdhChannel au4_16c = (AtSdhChannel)AtSdhLineAu4_16cGet(self, aug16Id);
    if (au4_16c == NULL)
        return NULL;

    /* Just return its VC sub channel */
    return (AtSdhVc)AtSdhChannelSubChannelGet(au4_16c, 0);
    }

/**
 * Get VC-4-4c
 *
 * @param self This line
 * @param aug4Id AUG-4 ID in line
 *
 * @return VC-4-4c of @ref AtSdhVc
 */
AtSdhVc AtSdhLineVc4_4cGet(AtSdhLine self, uint8 aug4Id)
    {
    /* Get AU-4-4c first */
    AtSdhChannel au4_4c = (AtSdhChannel)AtSdhLineAu4_4cGet(self, aug4Id);
    if (au4_4c == NULL)
        return NULL;

    /* Just return its VC sub channel */
    return (AtSdhVc)AtSdhChannelSubChannelGet(au4_4c, 0);
    }

/**
 * Get VC-4
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 *
 * @return VC-4 which is of @ref AtSdhVc
 */
AtSdhVc AtSdhLineVc4Get(AtSdhLine self, uint8 aug1Id)
    {
    /* Get AU-4 first */
    AtSdhChannel au4 = (AtSdhChannel)AtSdhLineAu4Get(self, aug1Id);
    if (au4 == NULL)
        return NULL;

    /* Just return its VC sub channel */
    return (AtSdhVc)AtSdhChannelSubChannelGet(au4, 0);
    }

/**
 * Get VC-3
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 * @param au3Tug3Id [0..2] AU-3/TUG-3 ID
 *
 * @return VC-3 which is of @ref AtSdhVc
 */
AtSdhVc AtSdhLineVc3Get(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id)
    {
    /* Try to get AU-3 or TU-3 */
    AtSdhChannel au3Tu3 = (AtSdhChannel)AtSdhLineAu3Get(self, aug1Id, au3Tug3Id);
    if (au3Tu3 == NULL)
        au3Tu3 = (AtSdhChannel)AtSdhLineTu3Get(self, aug1Id, au3Tug3Id);
    if (au3Tu3 == NULL)
        return NULL;

    /* Just return its VC sub channel */
    return (AtSdhVc)AtSdhChannelSubChannelGet(au3Tu3, 0);
    }

/**
 * Get VC-11/VC-12
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 * @param au3Tug3Id [0..2] AU-3/TUG-3 ID
 * @param tug2Id [0..6] TUG-2 ID
 * @param tuId [0..3] TU ID
 *
 * @return VC-11/VC-12 which is of @ref AtSdhVc
 */
AtSdhVc AtSdhLineVc1xGet(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id, uint8 tuId)
    {
    /* Get TU-1x first */
    AtSdhChannel tu1x = (AtSdhChannel)AtSdhLineTu1xGet(self, aug1Id, au3Tug3Id, tug2Id, tuId);
    if (tu1x == NULL)
        return NULL;

    /* Just return its VC sub channel */
    return (AtSdhVc)AtSdhChannelSubChannelGet(tu1x, 0);
    }

/**
 * Get AU-4-nc
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 *
 * @return AU-4-nc which is of @ref AtSdhAu
 */
AtSdhAu AtSdhLineAu4_ncGet(AtSdhLine self, uint8 aug1Id)
    {
    if (mLineIsValid(self))
        return mMethodsGet(self)->Au4_ncGet(self, aug1Id);
    return NULL;
    }

/**
 * Get VC-4-nc
 *
 * @param self This line
 * @param aug1Id [0..15] AUG-1 ID
 *
 * @return VC-4-nc which is of @ref AtSdhVc
 */
AtSdhVc AtSdhLineVc4_ncGet(AtSdhLine self, uint8 aug1Id)
    {
    if (mLineIsValid(self))
        return mMethodsGet(self)->Vc4_ncGet(self, aug1Id);
    return NULL;
    }

/**
 * Get AU-4-16nc
 *
 * @param self This line
 * @param aug16Id [0..3] AUG-16 ID
 *
 * @return AU-4-16nc which is of @ref AtSdhAu
 */
AtSdhAu AtSdhLineAu4_16ncGet(AtSdhLine self, uint8 aug16Id)
    {
    if (mLineIsValid(self))
        return Au4_16ncGet(self, aug16Id);
    return NULL;
    }

/**
 * Get VC-4-16nc
 *
 * @param self This line
 * @param aug16Id [0..3] AUG-16 ID
 *
 * @return VC-4-16nc which is of @ref AtSdhVc
 */
AtSdhVc AtSdhLineVc4_16ncGet(AtSdhLine self, uint8 aug16Id)
    {
    if (mLineIsValid(self))
        return Vc4_16ncGet(self, aug16Id);
    return NULL;
    }

/**
 * Get STS-1
 *
 * @param self This line
 * @param stsId STS-1 ID
 *
 * @return STS-1 which is of @ref AtSdhSts
 */
AtSdhSts AtSdhLineStsGet(AtSdhLine self, uint8 stsId)
    {
    if (self)
        return StsGet(self, stsId);
    return NULL;
    }

/**
 * Enable/disable scramble
 *
 * @param self This line
 * @param enable cAtTrue to enable, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineScrambleEnable(AtSdhLine self, eBool enable)
    {
    mNumericalAttributeSet(ScrambleEnable, enable);
    }

/**
 * Check if scramble is enabled or not
 *
 * @param self This line
 * @retval cAtTrue if scramble is enabled
 * @retval cAtFalse if scramble is disabled
 */
eBool AtSdhLineScrambleIsEnabled(AtSdhLine self)
    {
    mAttributeGet(ScrambleIsEnabled, eBool, cAtFalse);
    }

/**
 * Get SERDES controller
 *
 * @param self This line
 *
 * @return AtSerdesController it this line has, otherwise, NULL is returned
 */
AtSerdesController AtSdhLineSerdesController(AtSdhLine self)
    {
    mNoParamObjectGet(SerdesController, AtSerdesController);
    }

/**
 * Get BER Monitoring controller of RS layer
 *
 * @param self This line
 *
 * @return AtBerController object on success or NULL on failure
 */
AtBerController AtSdhLineRsBerControllerGet(AtSdhLine self)
    {
    mNoParamObjectGet(RsBerControllerGet, AtBerController);
    }

/**
 * Get BER Monitoring controller of MS layer
 *
 * @param self This line
 *
 * @return AtBerController object on success or NULL on failure
 */
AtBerController AtSdhLineMsBerControllerGet(AtSdhLine self)
    {
    mNoParamObjectGet(MsBerControllerGet, AtBerController);
    }

/**
 * Enable/disable insert overhead bytes (K1, K2, S1)
 *
 * @param self This line
 * @param enable Enable/disable
 * @return AT return code
 */
eAtModuleSdhRet AtSdhLineOverheadBytesInsertEnable(AtSdhLine self, eBool enable)
    {
    mNumericalAttributeSet(OverheadBytesInsertEnable, enable);
    }

/**
 * Get insert overhead bytes is enabled/disabled
 * @param self This line
 * @return Insert overhead bytes is enabled or disabled
 */
eBool AtSdhLineOverheadBytesInsertIsEnabled(AtSdhLine self)
    {
    mAttributeGet(OverheadBytesInsertIsEnabled, eBool, cAtFalse);
    }

/**
 * Get Line PRBS engine associated with this channel
 *
 * @param self This line
 * @return AtPrbsEngine object if this channel support PRBS, otherwise, NULL is
 *         returned
 */
AtPrbsEngine AtSdhLinePrbsEngineGet(AtSdhLine self)
    {
    mNoParamObjectGet(PrbsEngineGet, AtPrbsEngine);
    }

/**
 * Create a DCC channel
 *
 * @param self This line
 * @param layer @ref eAtSdhLineDccLayer "DCC layer"
 *
 * @return AtHdlcChannel object if this channel DCC-PW, otherwise, NULL is returned
 */
AtHdlcChannel AtSdhLineDccChannelCreate(AtSdhLine self, eAtSdhLineDccLayer layer)
    {
    if (self)
        {
        mOneParamWrapCall(layer, AtHdlcChannel, DccChannelCreate(self, layer));
        }

    return NULL;
    }

/**
 * Get a DCC channel
 *
 * @param self This line
 * @param layer @ref eAtSdhLineDccLayer "DCC layer".
 *
 * @return AtHdlcChannel object if this channel DCC-PW, otherwise, NULL is returned
 */
AtHdlcChannel AtSdhLineDccChannelGet(AtSdhLine self, eAtSdhLineDccLayer layer)
    {
    if (self)
        return DccChannelGet(self, layer);
    return NULL;
    }

/**
 * Delete DCC channel
 *
 * @param self This SDH/SONET line
 * @param dccChannel DCC channel
 *
 * @return AT return code
 */
eAtRet AtSdhLineDccChannelDelete(AtSdhLine self, AtHdlcChannel dccChannel)
    {
    if (self)
        {
        mOneObjectWrapCall(dccChannel, eAtRet, DccChannelDelete(self, dccChannel));
        }
    return cAtErrorNotExist;
    }

/**
 * Set STS-1 to monitor Z1
 *
 * @param self This line
 * @param sts1 STS-1 ID
 *
 * @return AT return code
 */
eAtRet AtSdhLineZ1MonitorPositionSet(AtSdhLine self, uint8 sts1)
    {
    mNumericalAttributeSet(Z1MonitorPositionSet, sts1);
    }

/**
 * Get STS-1 to monitor Z1
 *
 * @param self This line
 *
 * @return STS-1 ID
 */
uint8 AtSdhLineZ1MonitorPositionGet(AtSdhLine self)
    {
    mAttributeGet(Z1MonitorPositionGet, uint8, cInvalidUint8);
    }

/**
 * Check Z1 can be monitored or not
 *
 * @param self This line
 *
 * @retval cAtTrue if Z1 can be monitored
 * @retval cAtFalse if Z1 cannot be monitored
 */
eBool AtSdhLineZ1CanMonitor(AtSdhLine self)
    {
    mAttributeGet(Z1CanMonitor, eBool, cAtFalse);
    }

/**
 * Set STS-1 to monitor Z2
 *
 * @param self This line
 * @param sts1 STS-1 ID to monitor
 *
 * @return AT return code
 */
eAtRet AtSdhLineZ2MonitorPositionSet(AtSdhLine self, uint8 sts1)
    {
    mNumericalAttributeSet(Z2MonitorPositionSet, sts1);
    }

/**
 * Get STS-1 to monitor Z2
 *
 * @param self This line
 *
 * @return STS-1 ID to monitor
 */
uint8 AtSdhLineZ2MonitorPositionGet(AtSdhLine self)
    {
    mAttributeGet(Z2MonitorPositionGet, uint8, cInvalidUint8);
    }

/**
 * Check Z2 can be monitored or not
 *
 * @param self This line
 *
 * @retval cAtTrue if Z2 can be monitored
 * @retval cAtFalse if Z2 cannot be monitored
 */
eBool AtSdhLineZ2CanMonitor(AtSdhLine self)
    {
    mAttributeGet(Z2CanMonitor, eBool, cAtFalse);
    }

/** @} */

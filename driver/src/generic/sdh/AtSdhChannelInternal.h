/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhChannelInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description :
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHCHANNELINTERNAL_H_
#define _ATSDHCHANNELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"
#include "AtSdhLine.h"
#include "../common/AtChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhChannelSimulationDb
    {
    tAtChannelSimulationDb super;
    tAtSdhTti txTti, expectedTti;
    eBool timMonitorEnabled;
    uint32 affectedAlarms;
    }tAtSdhChannelSimulationDb;

typedef enum eAtSdhChannelCounterMode
    {
    cAtSdhCounterModeUnknown,
    cAtSdhChannelCounterModeBit,
    cAtSdhChannelCounterModeBlock
    }eAtSdhChannelCounterMode;

/* Methods */
typedef struct tAtSdhChannelMethods
    {
    /* Mapping */
    eAtModuleSdhRet (*MapTypeSet)(AtSdhChannel self, uint8 mapType);
    uint8 (*MapTypeGet)(AtSdhChannel self);
    AtChannel (*MapChannelGet)(AtSdhChannel self);
    eBool (*MapTypeIsSupported)(AtSdhChannel self, uint8 mapType);
    uint8 (*HwMapTypeGet)(AtSdhChannel self);
    eAtModuleSdhRet (*CanChangeMapping)(AtSdhChannel self, uint8 mapType);
    eBool (*CanHaveMapping)(AtSdhChannel self);
    eAtRet (*MappingReset)(AtSdhChannel self);
    eBool (*ShouldCleanupMapChannel)(AtSdhChannel self);

    /* TTI */
    eAtModuleSdhRet (*TxTtiGet)(AtSdhChannel self, tAtSdhTti *tti);
    eAtModuleSdhRet (*TxTtiSet)(AtSdhChannel self, const tAtSdhTti *tti);
    eAtModuleSdhRet (*ExpectedTtiGet)(AtSdhChannel self, tAtSdhTti *tti);
    eAtModuleSdhRet (*ExpectedTtiSet)(AtSdhChannel self, const tAtSdhTti *tti);
    eAtModuleSdhRet (*RxTtiGet)(AtSdhChannel self, tAtSdhTti *tti);

    /* TTI compare */
    eAtModuleSdhRet (*TtiCompareEnable)(AtSdhChannel self, eBool enable);
    eBool (*TtiCompareIsEnabled)(AtSdhChannel self);

    /* Alarm affecting option */
    eAtModuleSdhRet (*AlarmAffectingEnable)(AtSdhChannel self, uint32 alarmType, eBool enable);
    eBool (*AlarmAffectingIsEnabled)(AtSdhChannel self, uint32 alarmType);
    uint32 (*AlarmAffectingMaskGet)(AtSdhChannel self);
    eAtRet (*AutoRdiEnable)(AtSdhChannel self, eBool enable);
    eBool (*AutoRdiIsEnabled)(AtSdhChannel self);
    eBool (*CanChangeAlarmAffecting)(AtSdhChannel self, uint32 alarmType);
    eAtModuleSdhRet (*AlarmAutoRdiEnable)(AtSdhChannel self, uint32 alarms, eBool enable);
    eBool (*AlarmAutoRdiIsEnabled)(AtSdhChannel self, uint32 alarms);
    eAtRet (*AutoReiEnable)(AtSdhChannel self, eBool enable);
    eBool (*AutoReiIsEnabled)(AtSdhChannel self);

    /* BER monitoring */
    AtBerController (*BerControllerGet)(AtSdhChannel self);
    AtBerController (*BerControllerCreate)(AtSdhChannel self);
    void (*BerControllerDelete)(AtSdhChannel self);

    /* Alarm and interrupt retrieving, use for applications that does not use AtDeviceInterruptProcess */
    eAtRet (*InterruptAndDefectGet)(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear);
    eAtRet (*InterruptAndAlarmGet)(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear);

    /* Interfaces to handle objects in mapping tree */
    uint8 (*NumberOfSubChannelsGet)(AtSdhChannel self);
    AtSdhChannel (*SubChannelGet)(AtSdhChannel self, uint8 subChannelId);
    AtSdhChannel (*ParentChannelGet)(AtSdhChannel self);
    eAtRet (*SubChannelsInterruptDisable)(AtSdhChannel self, eBool onlySubChannels);
    AtSdhLine (*LineObjectGet)(AtSdhChannel self);

    /* Notify itself and its sub-channels when alarm forwarding from upper layer is terminated. */
    void (*AlarmForwardingClearanceNotifyWithSubChannelOnly)(AtSdhChannel self, uint32 currentDefect, eBool subChannelsOnly);
    eBool (*HasAlarmForwarding)(AtSdhChannel self, uint32 currentDefects); /* AIS forwarding downstream to its sub-channels.*/
    eBool (*HasParentAlarmForwarding)(AtSdhChannel self); /* AIS forwarding status from its parent channel. */
    void (*FailureForwardingClearanceNotifyWithSubChannelOnly)(AtSdhChannel self, uint32 currentFailures, eBool subChannelsOnly);

    /* TIM monitor enable/disable */
    eAtModuleSdhRet (*TimMonitorEnable)(AtSdhChannel self, eBool enable);
    eBool (*TimMonitorIsEnabled)(AtSdhChannel self);

    /* Block error counters */
    uint32 (*BlockErrorCounterGet)(AtSdhChannel self, uint16 counterType);
    uint32 (*BlockErrorCounterClear)(AtSdhChannel self, uint16 counterType);
    eAtModuleSdhRet (*CounterModeSet)(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode);

    uint8 (*NumSts)(AtSdhChannel self);

    /* Overhead */
    eAtModuleSdhRet (*TxOverheadByteSet)(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue);
    uint8  (*TxOverheadByteGet)(AtSdhChannel self, uint32 overheadByte);
    uint8  (*RxOverheadByteGet)(AtSdhChannel self, uint32 overheadByte);
    eBool  (*TxOverheadByteIsSupported)(AtSdhChannel self, uint32 overheadByte);
    eBool  (*RxOverheadByteIsSupported)(AtSdhChannel self, uint32 overheadByte);

    eAtRet (*RxTrafficSwitch)(AtSdhChannel self, AtSdhChannel otherChannel);
    eAtRet (*TxTrafficBridge)(AtSdhChannel self, AtSdhChannel otherChannel);
    eAtRet (*TxTrafficCut)(AtSdhChannel self);

    /* Mode */
    eAtModuleSdhRet (*ModeSet)(AtSdhChannel self, eAtSdhChannelMode mode);
    eAtSdhChannelMode (*ModeGet)(AtSdhChannel self);
    eBool (*CanChangeMode)(AtSdhChannel self, eAtSdhChannelMode mode);
    void (*SerializeMode)(AtSdhChannel object, AtCoder encoder);
    eAtSdhChannelMode (*DefaultMode)(AtSdhChannel self);

    /* Concatenation */
    eAtRet (*Concate)(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves);
    eAtRet (*Deconcate)(AtSdhChannel self);
    eAtRet (*ConcateCheck)(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves);

    /* APS */
    eBool (*HoldOffTimerSupported)(AtSdhChannel self);
    eAtRet (*HoldOffTimerSet)(AtSdhChannel self, uint32 timerInMs);
    uint32 (*HoldOffTimerGet)(AtSdhChannel self);
    eAtRet (*HoldOnTimerSet)(AtSdhChannel self, uint32 timerInMs);
    uint32 (*HoldOnTimerGet)(AtSdhChannel self);
    eBool (*HoldOnTimerSupported)(AtSdhChannel self);
    eBool (*IsJoiningAps)(AtSdhChannel self);

    /* STS-1 information */
    uint8 (*Sts1Get)(AtSdhChannel self);
    void (*Sts1IdSet)(AtSdhChannel self, uint8 stsId);

    /* For products that have XC hiding logic */
    uint8 (*ActualMapTypeGet)(AtSdhChannel self);
    AtSdhChannel (*HideChannelGet)(AtSdhChannel self);
    }tAtSdhChannelMethods;

/* Representation */
typedef struct tAtSdhChannel
    {
    tAtChannel super;
    const tAtSdhChannelMethods *methods;

    /* Private data */
    uint8 channelType;
    AtSdhChannel parent;
    AtSdhLine line;
    AtSdhChannel *subChannels;
    uint8 numSubChannels;
    uint8 mapType;
    uint32 enabledAlarmAffect;

    /* Concatenation */
    AtSdhChannel *slaveChannels;
    uint8 numSlaveChannels;

    /* Cache Line, STS ID, TUG-2 for fast converting */
    uint8 lineId;
    uint8 stsId;
    uint8 tug2Id;
    uint8 tu1xId;

    /* Save hardware slice and STS. All implementations of Arrive so far use
     * these attributes to access hardware, so this may be a good place to put
     * them as generic private attributes. */
    uint16 *allHwSts;

    /* BER */
    AtBerController berController;

    /* Some applications require temporary disable path alarm forwarding. The
     * following private data is for this purpose */
    eBool alarmForwardingTemporaryDisabled;
    uint32 alarmForwardingMask;

    /* Some applications need to configure line mode at VC level */
    eAtSdhChannelMode mode;
    }tAtSdhChannel;

typedef struct tAtSdhChannelCache
    {
    tAtChannelCache super;

    /* Additional fields */
    tAtSdhTti txTti, expectedTti;
    uint32 alarmsAffect;
    uint8 mode;
    }tAtSdhChannelCache;

typedef enum eAtSdhChannelTypeInternal
    {
    cAtSdhChannelTypeInternal = cAtSdhChannelTypeVc4_nc,
    cAtSdhChannelTypeSts
    }eAtSdhChannelTypeInternal;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhChannel AtSdhChannelObjectInit(AtSdhChannel self, uint32 channelId, uint8 channelType, AtModuleSdh module);

/* Access private data */
void SdhChannelSubChannelsSet(AtSdhChannel self, AtSdhChannel *subChannels, uint8 numSubChannels);
void SdhChannelParentSet(AtSdhChannel self, AtSdhChannel parent);
void SdhChannelMapTypeSet(AtSdhChannel self, uint8 mapType);
uint8 SdhChannelMapTypeGet(AtSdhChannel self);
eAtRet SdhChannelSubChannelsCreate(AtSdhChannel self, uint8 lineId, uint8 numSubChannels, uint8 subChannelType);
void SdhChannelSubChannelsDelete(AtSdhChannel channel);
void SdhChannelSubChannelsCleanUp(AtSdhChannel channel);
eAtRet SdhChannelSlaveChannelsSetUp(AtSdhChannel self, AtSdhChannel *slaveChannels, uint8 numSlaveChannels);
void SdhChannelSlaveChannelsCleanUp(AtSdhChannel self);
void SdhChannelSlaveChannelsDelete(AtSdhChannel self);
void SdhChannelTypeSet(AtSdhChannel self, uint8 channelType);
void SdhChannelLineIdSet(AtSdhChannel self, uint8 lineId);
void SdhChannelSts1IdSet(AtSdhChannel self, uint8 stsId);
void SdhChannelTug2IdSet(AtSdhChannel self, uint8 tug2Id);
void SdhChannelTu1xIdSet(AtSdhChannel self, uint8 tu1xId);
eAtRet AtSdhAuDefaultSsSet(AtSdhPath path);
eAtRet AtSdhTuDefaultSsSet(AtSdhPath path);
eBool AtSdhChannelNeedToUpdateMapType(AtSdhChannel self, uint8 mapType);
uint8 Aug1RestoreIsInProgress(AtSdhChannel self);
AtSdhLine AtSdhChannelLineObjectGet(AtSdhChannel self);
uint16 *AtSdhChannelAllHwSts(AtSdhChannel self);
eBool AtSdhChannelIsHoVc(AtSdhChannel sdhChannel);
eBool AtSdhChannelIsAug(AtSdhChannel self);
eBool AtSdhChannelIsAu(AtSdhChannel self);
eBool AtSdhChannelIsTu(AtSdhChannel self);
void SdhChannelSubChannelAtIndexSet(AtSdhChannel self, uint8 subChannelIndex, AtSdhChannel subChannel);
eBool AtSdhChannelIsChannelizedVc(AtSdhChannel sdhChannel);
eBool AtSdhChannelIsVc(AtSdhChannel sdhChannel);
eBool AtSdhChannelCanEnableAlarmAffecting(AtSdhChannel self, uint32 alarmType);
eAtRet AtSdhChannelBerControllerDefaultSet(AtSdhChannel self);
uint32 AtSdhChannelAlarmAffectingMaskGet(AtSdhChannel self);
eBool AtSdhChannelIsInConcatenation(AtSdhChannel self);
eAtRet AtSdhChannelConcateCheck(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves);
eAtRet AtSdhChannelMappingReset(AtSdhChannel self);
uint8 AtSdhChannelActualMapTypeGet(AtSdhChannel self);
eAtRet AtSdhChannelAllSubChannelsHardwareCleanup(AtSdhChannel self);
eAtRet AtSdhChannelDefaultModeSet(AtSdhChannel self);
eAtModuleSdhRet AtSdhChannelInternalDbModeSet(AtSdhChannel self, eAtSdhChannelMode mode);
eAtSdhChannelMode AtSdhChannelInternalDbModeGet(AtSdhChannel self);
eAtModuleSdhRet AtSdhChannelCounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode);
eAtRet AtSdhChannelDefaultCounterModeSet(AtSdhChannel self, uint16 counterType);
eBool AtSdhChannelHasCrossConnect(AtSdhChannel self);

/* Work with hiding XC */
AtSdhChannel AtSdhChannelHideChannelGet(AtSdhChannel self);

/* Initialize itself and all of sub-channels. */
eAtRet AtSdhChannelSubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels);
eAtRet AtSdhChannelAlarmAffectingDisable(AtSdhChannel self);
eAtRet AtSdhChannelAlarmAffectingRestore(AtSdhChannel self);

/* BER controller */
AtBerController SdhChannelBerControllerGet(AtSdhChannel self);
void SdhChannelAllBerControllersDelete(AtSdhChannel self);

/* Service handling */
eBool AtSdhChannelServiceIsRunning(AtSdhChannel self);
eAtRet AtSdhChannelCanChangeMapping(AtSdhChannel self, uint8 mapType);

/* Notify alarm forwarding clearance itself and its sub-channels. */
void AtSdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentDefect, eBool subChannelsOnly);
eBool AtSdhChannelHasAlarmForwarding(AtSdhChannel self, uint32 currentDefect);
eBool AtSdhChannelHasParentAlarmForwarding(AtSdhChannel self);
void AtSdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentFailures, eBool subChannelsOnly);

/* APS */
eBool AtSdhChannelHoldOffTimerSupported(AtSdhChannel self);
eBool AtSdhChannelHoldOnTimerSupported(AtSdhChannel self);
eBool AtSdhChannelIsJoiningAps(AtSdhChannel self);

void AtSdhChannelAllHwStsDelete(AtSdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHCHANNELINTERNAL_H_ */

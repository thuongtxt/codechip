/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhVcInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description : VC Class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHVCINTERNAL_H_
#define _ATSDHVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhPathInternal.h"
#include "AtSdhVc.h"
#include "AtPdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Methods */
typedef struct tAtSdhVcMethods
    {
    uint16 (*DefaultMapType)(AtSdhVc self);

    /* Stuffing */
    eAtRet (*CanChangeStuffing)(AtSdhVc self, eBool enable);
    eAtRet (*StuffEnable)(AtSdhVc self, eBool enable);
    eBool (*StuffIsEnabled)(AtSdhVc self);

    }tAtSdhVcMethods;

/* Representation */
typedef struct tAtSdhVc
    {
    tAtSdhPath super;
    const tAtSdhVcMethods *methods;

    /* Private data */
    AtPdhChannel pdhChannel; /* The PDH channel that is mapped to this VC */
    void *cache;
    }tAtSdhVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc AtSdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

/* Access private data */
void SdhVcPdhChannelSet(AtSdhVc self, AtPdhChannel pdhChannel);
AtPdhChannel SdhVcPdhChannelGet(AtSdhVc self);
AtSdhChannel AtSdhVcAugGet(AtSdhChannel self);
eBool AtSdhVcIsHoVc(AtSdhVc self);
eBool AtSdhVcIsLoVc(AtSdhVc self);
eBool AtSdhVcTu1xChannelized(AtSdhVc self);
uint16 AtSdhVcDefaultMapType(AtSdhVc self);
eAtRet AtSdhVcCanChangeStuffing(AtSdhVc self, eBool enable);
eAtRet AtSdhVcDefaultStuffingSet(AtSdhVc self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHVCINTERNAL_H_ */


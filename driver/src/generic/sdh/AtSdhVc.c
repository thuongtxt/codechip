/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhVc.c
 *
 * Created Date: Sep 4, 2012
 *
 * Description : SDH VC abstract class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtPwCep.h"
#include "AtSdhTug.h"
#include "AtSdhVcInternal.h"
#include "AtSdhAugInternal.h"
#include "../pdh/AtPdhChannelInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "../sur/AtModuleSurInternal.h"
#include "../sur/AtSurEngineInternal.h"
#include "../concate/AtModuleConcateInternal.h"
#include "AtModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtSdhVcMethods     m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;
static tAtChannelMethods    m_AtChannelOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;
static const tAtSdhPathMethods    *m_AtSdhPathMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSts(AtSdhChannel self)
    {
    return AtSdhChannelNumSts(AtSdhChannelParentChannelGet(self));
    }

static AtBerController BerControllerCreate(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleBer berModule = (AtModuleBer)AtDeviceModuleGet(device, cAtModuleBer);
    return AtModuleBerSdhPathBerControlerCreate(berModule, (AtChannel)self);
    }

static uint32 DefectMaskIsSupported(AtChannel self)
    {
	AtUnused(self);
    return cAtSdhPathAlarmAis | cAtSdhPathAlarmLop;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
	return DefectMaskIsSupported(self);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    eAtSdhPathAlarmType hiAlarms;
    eAtRet ret = cAtOk;

    /* Application may input LOP/AIS which are of higher layer of this VC, so
     * just delegate this action to its parent */
    hiAlarms = DefectMaskIsSupported(self);
    if (defectMask & hiAlarms)
        ret = AtChannelInterruptMaskSet((AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)self),
                                        defectMask & hiAlarms,
                                        enableMask & hiAlarms);

    /* And let concrete class determine how to control interrupt mask of this VC */
    return ret;
    }

static eAtRet Vc4MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 numSubChannels, i, vc4Sts1;
    uint8 subChannelType = cAtSdhChannelTypeUnknown;
    AtSdhChannel subChannel;
    eAtRet ret;
    uint8 lineId;
    static const uint8 numTug3sInVc4 = 3;

    /* Map 3xTUG-3 */
    if (mapType == cAtSdhVcMapTypeVc4Map3xTug3s)
        {
        numSubChannels = numTug3sInVc4;
        subChannelType = cAtSdhChannelTypeTug3;
        }

    /* Map C-4 (will be determined when there is a channel mapped to this VC-4) */
    else if (mapType == cAtSdhVcMapTypeVc4MapC4)
        numSubChannels = 0;

    else
        return cAtErrorModeNotSupport;
        
    /* Create sub channels */
    lineId = AtSdhChannelLineGet(self);
    ret = SdhChannelSubChannelsCreate(self, lineId, numSubChannels, subChannelType);
    if (ret == cAtOk)
        SdhChannelMapTypeSet(self, mapType);

    /* Cache IDs */
    if (mapType == cAtSdhVcMapTypeVc4Map3xTug3s)
        {
        vc4Sts1 = AtSdhChannelSts1Get(self);
        for (i = 0; i < numSubChannels; i++)
            {
            subChannel = AtSdhChannelSubChannelGet(self, i);
            SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
            SdhChannelSts1IdSet(subChannel, (uint8)(vc4Sts1 + i));
            }
        }

    return ret;
    }

static eAtRet Vc3MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    uint8 i;
    static const uint8 numTug2sInVc3 = 7;
    AtSdhChannel subChannel;
    uint8 lineId = AtSdhChannelLineGet(self);

    /* Map TUG-2s */
    if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s)
        {
        eAtRet ret;

        /* Cannot map TUG-2s if this VC-3 is mapped to TU-3 */
        if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(self)) == cAtSdhChannelTypeTu3)
            return cAtErrorModeNotSupport;

        /* Create sub channels */
        ret = SdhChannelSubChannelsCreate(self, lineId, numTug2sInVc3, cAtSdhChannelTypeTug2);
        if (ret != cAtOk)
            return ret;

        /* Cache Line and STS */
        for (i = 0; i < numTug2sInVc3; i++)
            {
            subChannel = AtSdhChannelSubChannelGet(self, i);
            SdhChannelLineIdSet(subChannel, AtSdhChannelLineGet(self));
            SdhChannelSts1IdSet(subChannel, AtSdhChannelSts1Get(self));
            SdhChannelTug2IdSet(subChannel, i);
            }
        }

    SdhChannelMapTypeSet(self, mapType);

    /* For other mapping type, let determine when there is a channel mapped to it */
    return cAtOk;
    }

static eBool CanChangeMapping(AtSdhChannel self, uint8 newNapType)
    {
    uint8 mapType = AtSdhChannelActualMapTypeGet(self);

    if (Aug1RestoreIsInProgress(self))
        return cAtTrue;

    /* Cannot change mapping if mapping is changed and service is running */
    if ((mapType != newNapType) && (AtSdhChannelServiceIsRunning(self)))
        return cAtFalse;

    return cAtTrue;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;

    if (!CanChangeMapping(self, mapType))
        return cAtErrorChannelBusy;

    if (mapType != AtSdhChannelActualMapTypeGet(self))
        AtSdhChannelSubChannelsInterruptDisable(self, cAtTrue);

    /* Let the super do first because mapping changing may relate with resource
     * management */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* Do nothing if mapping type is not changed */
    if (mapType == AtSdhChannelActualMapTypeGet(self))
        return cAtOk;

    /* VC-4 mapping */
    if ((mapType == cAtSdhVcMapTypeVc4Map3xTug3s) || (mapType == cAtSdhVcMapTypeVc4MapC4))
        return Vc4MapTypeSet(self, mapType);

    /* VC-3 mapping */
    if ((mapType == cAtSdhVcMapTypeVc3MapC3) ||
        (mapType == cAtSdhVcMapTypeVc3Map7xTug2s) ||
        (mapType == cAtSdhVcMapTypeVc3MapDe3))
        return Vc3MapTypeSet(self, mapType);

    /* Let's sub class does another mapping */
    SdhChannelMapTypeSet(self, mapType);

    return cAtOk;
    }

static eAtRet SubChannelsInterruptDisable(AtSdhChannel self, eBool onlySubChannels)
    {
    uint8 mapType = AtSdhChannelMapTypeGet(self);

    if ((mapType == cAtSdhVcMapTypeVc1xMapDe1) || (mapType == cAtSdhVcMapTypeVc3MapDe3))
        {
        AtChannel mapChannel = AtSdhChannelMapChannelGet(self);

        if (!onlySubChannels)
            {
            AtChannelInterruptMaskSet((AtChannel)self, AtChannelInterruptMaskGet((AtChannel)self), 0);
            AtChannelSurEngineInterruptDisable((AtChannel)self);
            }

        if (mMethodsGet(self)->ShouldCleanupMapChannel(self))
            return AtPdhChannelSubChannelsInterruptDisable((AtPdhChannel)mapChannel, cAtFalse);

        return cAtOk;
        }

    return m_AtSdhChannelMethods->SubChannelsInterruptDisable(self, onlySubChannels);
    }

static const char *TypeString(AtChannel self)
    {
    switch (AtSdhChannelTypeGet((AtSdhChannel)self))
        {
        case cAtSdhChannelTypeVc4_64c : return "vc4_64c";
        case cAtSdhChannelTypeVc4_16c : return "vc4_16c";
        case cAtSdhChannelTypeVc4_4c  : return "vc4_4c";
        case cAtSdhChannelTypeVc4_16nc: return "vc4_16nc";
        case cAtSdhChannelTypeVc4_nc  : return "vc4_nc";
        case cAtSdhChannelTypeVc4     : return "vc4";
        case cAtSdhChannelTypeVc3     : return "vc3";
        case cAtSdhChannelTypeVc12    : return "vc12";
        case cAtSdhChannelTypeVc11    : return "vc11";

        case cAtSdhChannelTypeUnknown:
        case cAtSdhChannelTypeLine   :
        case cAtSdhChannelTypeAug64  :
        case cAtSdhChannelTypeAug16  :
        case cAtSdhChannelTypeAug4   :
        case cAtSdhChannelTypeAug1   :
        case cAtSdhChannelTypeAu4_64c:
        case cAtSdhChannelTypeAu4_16c:
        case cAtSdhChannelTypeAu4_4c :
        case cAtSdhChannelTypeAu4_16nc:
        case cAtSdhChannelTypeAu4_nc :
        case cAtSdhChannelTypeAu4    :
        case cAtSdhChannelTypeAu3    :
        case cAtSdhChannelTypeTug3   :
        case cAtSdhChannelTypeTug2   :
        case cAtSdhChannelTypeTu3    :
        case cAtSdhChannelTypeTu12   :
        case cAtSdhChannelTypeTu11   :
        default:
            return "unknown";
        }
    }

static const char *IdString(AtChannel self)
    {
    AtChannel parent = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)self);
    return AtChannelIdString(parent);
    }

static AtSdhPath ParentPath(AtSdhPath self)
    {
    return (AtSdhPath)AtSdhChannelParentChannelGet((AtSdhChannel)self);
    }

static eAtModuleSdhRet TxSsSet(AtSdhPath self, uint8 value)
    {
    return AtSdhPathTxSsSet(ParentPath(self), value);
    }

static uint8 TxSsGet(AtSdhPath self)
    {
    return AtSdhPathTxSsGet(ParentPath(self));
    }

static eAtRet SsInsertionEnable(AtSdhPath self, eBool enable)
    {
    return AtSdhPathSsInsertionEnable(ParentPath(self), enable);
    }

static eBool SsInsertionIsEnabled(AtSdhPath self)
    {
    return AtSdhPathSsInsertionIsEnabled(ParentPath(self));
    }

static eAtModuleSdhRet ExpectedSsSet(AtSdhPath self, uint8 value)
    {
    return AtSdhPathExpectedSsSet(ParentPath(self), value);
    }

static uint8 ExpectedSsGet(AtSdhPath self)
    {
    return AtSdhPathExpectedSsGet(ParentPath(self));
    }

static eAtModuleSdhRet SsCompareEnable(AtSdhPath self, eBool enable)
    {
    return AtSdhPathSsCompareEnable(ParentPath(self), enable);
    }

static eBool SsCompareIsEnabled(AtSdhPath self)
    {
    return AtSdhPathSsCompareIsEnabled(ParentPath(self));
    }

static eAtRet Init(AtChannel self)
    {
    AtSdhChannel path = (AtSdhChannel)self;
    tAtSdhTti tti;

    /* Set default TTI */
    AtSdhTtiMake(AtModuleSdhDefaultTtiMode((AtModuleSdh)AtChannelModuleGet(self)), (const uint8 *)"a", 1, &tti);
    AtSdhChannelTxTtiSet(path, &tti);
    AtSdhChannelExpectedTtiSet(path, &tti);

    /* Set default stuffing */
    AtSdhVcDefaultStuffingSet((AtSdhVc)self);

    /* Release loopback */
    AtChannelLoopbackSet(self, cAtLoopbackModeRelease);

    /* No additional initialization yet */
    return m_AtChannelMethods->Init(self);
    }

static eBool IsTu3Vc3(AtChannel self)
    {
    if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet((AtSdhChannel)self)) == cAtSdhChannelTypeTu3)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 DataRateInBytesPer125Us(AtChannel self)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet((AtSdhChannel)self);
    static const uint32 cSts1DataRate = 783;

    if (vcType == cAtSdhChannelTypeVc11)    return 26;
    if (vcType == cAtSdhChannelTypeVc12)    return 35;
    if (vcType == cAtSdhChannelTypeVc3)
        {
        if (IsTu3Vc3(self))
            return 765; /* 9 * 85, VC3 in TU-3 only has 85 column */

        return cSts1DataRate;
        }

    if (vcType == cAtSdhChannelTypeVc4)     return cSts1DataRate * 3;
    if (vcType == cAtSdhChannelTypeVc4_4c)  return cSts1DataRate * 12;
    if (vcType == cAtSdhChannelTypeVc4_16c) return cSts1DataRate * 48;
    if (vcType == cAtSdhChannelTypeVc4_64c) return cSts1DataRate * 192;
    if (vcType == cAtSdhChannelTypeVc4_nc)  return cSts1DataRate * AtSdhChannelNumSts((AtSdhChannel)self);

    return 0;
    }

static eAtRet ReferenceTransfer(AtChannel self, AtChannel dest)
    {
    AtSdhVc srcVc = (AtSdhVc)self;
    AtSdhVc destVc = (AtSdhVc)dest;

    /* Super's job */
    if (m_AtChannelMethods->ReferenceTransfer(self, dest) != cAtOk)
        return cAtError;

    destVc->pdhChannel = srcVc->pdhChannel;
    if (destVc->pdhChannel)
        destVc->pdhChannel->vc = destVc;

    srcVc->pdhChannel = NULL;
    return cAtOk;
    }

static eBool IsJoiningAps(AtSdhChannel self)
    {
    AtSdhPath path = (AtSdhPath)self;
    AtList selectors;

    if (AtSdhPathApsEngineGet(path) != NULL)
        return cAtTrue;

    if (AtSdhPathApsSelectorGet(path) != NULL)
        return cAtTrue;

    selectors = AtSdhPathApsSelectors(path);
    if ((selectors != NULL) && AtListLengthGet(selectors) > 0)
        return cAtTrue;

    return cAtFalse;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    eBool serviceIsRunning = m_AtChannelMethods->ServiceIsRunning(self);

    if (serviceIsRunning)
        return serviceIsRunning;

    return AtChannelServiceIsRunning(AtSdhChannelMapChannelGet(sdhChannel));
    }

static AtCrossConnect XcEngine(AtChannel self)
    {
    AtDevice device = AtModuleDeviceGet(AtChannelModuleGet(self));
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    return AtModuleXcVcCrossConnectGet(xcModule);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtSdhVc object = (AtSdhVc)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeChannelIdString(pdhChannel);
    mEncodeNone(cache);
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(((AtSdhVc)self)->cache);
    }

static eBool PslValueIsValid(AtSdhPath self, uint8 psl)
    {
    uint32 vcType = AtSdhChannelTypeGet((AtSdhChannel)self);
    eBool vslIsInRange = (psl <= 7) ? cAtTrue : cAtFalse;

    switch (vcType)
        {
        case cAtSdhChannelTypeTu12: return vslIsInRange;
        case cAtSdhChannelTypeTu11: return vslIsInRange;
        case cAtSdhChannelTypeVc12: return vslIsInRange;
        case cAtSdhChannelTypeVc11: return vslIsInRange;
        default:
            return m_AtSdhPathMethods->PslValueIsValid(self, psl);
        }
    }

static eBool IsHoVc(AtSdhVc self)
    {
    eAtSdhChannelType sdhChannelType;

    if (!AtSdhChannelIsVc((AtSdhChannel)self))
        return cAtFalse;

    sdhChannelType = AtSdhChannelTypeGet((AtSdhChannel)self);
    if ((sdhChannelType == cAtSdhChannelTypeVc12) ||
        (sdhChannelType == cAtSdhChannelTypeVc11))
        return cAtFalse;

    if (sdhChannelType != cAtSdhChannelTypeVc3)
        return cAtTrue;

    if (AtSdhChannelIsAu(AtSdhChannelParentChannelGet((AtSdhChannel)self)))
        return cAtTrue;

    return cAtFalse;
    }

static AtModuleSur SurModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleSur)AtDeviceModuleGet(device, cAtModuleSur);
    }

static AtSurEngine SurEngineObjectCreate(AtChannel self)
    {
    if (IsHoVc((AtSdhVc)self))
        return AtModuleSurSdhHoPathEngineCreate(SurModule(self), (AtSdhPath)self);
    return AtModuleSurSdhLoPathEngineCreate(SurModule(self), (AtSdhPath)self);
    }

static void SurEngineObjectDelete(AtChannel self, AtSurEngine engine)
    {
    if (IsHoVc((AtSdhVc)self))
        AtModuleSurEngineSdhHoPathDelete(SurModule(self), engine);
    else
        AtModuleSurEngineSdhLoPathDelete(SurModule(self), engine);
    }

static eBool CanChangeMode(AtSdhChannel self, eAtSdhChannelMode lineMode)
    {
    if (lineMode == cAtSdhChannelModeSdh)
        return cAtTrue;

    if ((AtSdhChannelTypeGet(self) == cAtSdhChannelTypeVc4) &&
        (AtSdhChannelMapTypeGet(self) == cAtSdhVcMapTypeVc4Map3xTug3s))
        return cAtFalse;

    return cAtTrue;
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentDefects, eBool subChannelsOnly)
    {
    uint8 mapType = AtSdhChannelMapTypeGet(self);
    AtSdhChannel auTu = AtSdhChannelParentChannelGet(self);

    if (AtSdhChannelHasAlarmForwarding(auTu, currentDefects))
        return;

    if (mapType == cAtSdhVcMapTypeVc1xMapDe1 || mapType == cAtSdhVcMapTypeVc3MapDe3)
        {
        AtPdhChannelAlarmForwardingClearanceNotifyWithSubChannelOnly((AtPdhChannel)AtSdhChannelMapChannelGet(self), cAtFalse);
        return;
        }

    m_AtSdhChannelMethods->AlarmForwardingClearanceNotifyWithSubChannelOnly(self, currentDefects, subChannelsOnly);
    }

static AtSdhChannel AugGet(AtSdhChannel self)
    {
    AtSdhChannel au = AtSdhChannelParentChannelGet((AtSdhChannel)self);
    AtSdhChannel parent = AtSdhChannelParentChannelGet(au);
    return AtSdhChannelIsAug(parent) ? parent : NULL;
    }

static void FailureForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentFailures, eBool subChannelsOnly)
    {
    uint8 mapType;
    AtSurEngine engine = AtChannelSurEngineGet((AtChannel)self);
    uint32 autonomousFailures = AtSurEngineAutonomousFailuresGet(engine);

    if (AtSdhChannelHasAlarmForwarding(self, currentFailures))
        return;

    if (AtSdhChannelHasAlarmForwarding(self, autonomousFailures))
        return;

    if (subChannelsOnly == cAtFalse)
        AtChannelAllFailureListenersCall((AtChannel)self, cAtSdhPathAlarmAis, 0);

    mapType = AtSdhChannelMapTypeGet(self);
    if (mapType == cAtSdhVcMapTypeVc1xMapDe1 || mapType == cAtSdhVcMapTypeVc3MapDe3)
        {
        AtPdhChannelFailureForwardingClearanceNotifyWithSubChannelOnly((AtPdhChannel)AtSdhChannelMapChannelGet(self), cAtFalse);
        return;
        }

    m_AtSdhChannelMethods->FailureForwardingClearanceNotifyWithSubChannelOnly(self, autonomousFailures, cAtFalse);
    }

static eAtRet MappingReset(AtSdhChannel self)
    {
    /* Mapping is reset by AUG, there would be nothing to do at this layer */
    AtUnused(self);
    return cAtOk;
    }

static uint8 CxMappingType(AtSdhChannel self)
    {
    uint32 channelType = AtSdhChannelTypeGet(self);

    switch (channelType)
        {
        case cAtSdhChannelTypeVc4_64c: return cAtSdhVcMapTypeVc4_64cMapC4_64c;
        case cAtSdhChannelTypeVc4_16c: return cAtSdhVcMapTypeVc4_16cMapC4_16c;
        case cAtSdhChannelTypeVc4_4c : return cAtSdhVcMapTypeVc4_4cMapC4_4c;
        case cAtSdhChannelTypeVc4    : return cAtSdhVcMapTypeVc4MapC4;
        case cAtSdhChannelTypeVc3    : return cAtSdhVcMapTypeVc3MapC3;
        case cAtSdhChannelTypeVc12   : return cAtSdhVcMapTypeVc1xMapC1x;
        case cAtSdhChannelTypeVc11   : return cAtSdhVcMapTypeVc1xMapC1x;
        case cAtSdhChannelTypeVc4_nc : return cAtSdhVcMapTypeVc4_ncMapC4_nc;
        default                      : return cAtSdhVcMapTypeNone;
        }
    }

static eAtRet AllServicesDestroy(AtChannel self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    AtChannel mapChannel = AtSdhChannelMapChannelGet(sdhChannel);
    eAtRet ret = cAtOk;

    if (mapChannel)
        {
        ret |= AtChannelAllServicesDestroy(mapChannel);
        ret |= mMethodsGet(sdhChannel)->MapTypeSet(sdhChannel, CxMappingType(sdhChannel));
        }

    ret |= m_AtChannelMethods->AllServicesDestroy(self);

    return ret;
    }

static eAtRet Debug(AtChannel self)
    {
    AtSdhVc vc = (AtSdhVc)self;

    m_AtChannelMethods->Debug(self);

    if (vc->pdhChannel)
        {
        AtPrintc(cSevNormal, "* PDH channel: %p (%s.%s)\r\n",
                 (void *)vc->pdhChannel,
                 AtChannelTypeString((AtChannel)vc->pdhChannel),
                 AtChannelIdString((AtChannel)vc->pdhChannel));
        }
    else
        AtPrintc(cSevNormal, "* PDH channel: none\r\n");

    return cAtOk;
    }

static eBool Vc4IsChannelizedToTu1x(AtSdhChannel vc4)
    {
    uint8 tug3Id;

    if (AtSdhChannelMapTypeGet(vc4) != cAtSdhVcMapTypeVc4Map3xTug3s)
        return cAtFalse;

    for (tug3Id = 0; tug3Id < AtSdhChannelNumberOfSubChannelsGet(vc4); tug3Id++)
        {
        AtSdhChannel tug3 = AtSdhChannelSubChannelGet(vc4, tug3Id);

        if (tug3 == NULL)
            continue;

        if (AtSdhChannelMapTypeGet(tug3) == cAtSdhTugMapTypeTug3Map7xTug2s)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool Tu1xChannelized(AtSdhVc self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;
    uint8 mapType = AtSdhChannelMapTypeGet(sdhChannel);
    uint32 channelType = AtSdhChannelTypeGet(sdhChannel);

    switch (channelType)
        {
        case cAtSdhChannelTypeVc3:
            return (mapType == cAtSdhVcMapTypeVc3Map7xTug2s) ? cAtTrue : cAtFalse;

        case cAtSdhChannelTypeVc4:
            return Vc4IsChannelizedToTu1x(sdhChannel);

        default:
            return cAtFalse;
        }
    }

static uint16 DefaultMapType(AtSdhVc self)
    {
    if (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc3)
        return cAtSdhVcMapTypeVc3MapC3;
    
    /* To be added more */
    return cAtSdhVcMapTypeNone;
    }

static eBool DefaultStuffing(AtSdhVc self)
    {
    return AtSdhVcIsHoVc(self) ? cAtTrue : cAtFalse;
    }

static eAtRet CanChangeStuffing(AtSdhVc self, eBool enable)
    {
    return (DefaultStuffing(self) == enable) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet StuffEnable(AtSdhVc self, eBool enable)
    {
    return CanChangeStuffing(self, enable);
    }

static eBool StuffIsEnabled(AtSdhVc self)
    {
    return DefaultStuffing(self);
    }

static eAtRet HardwareCleanup(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->HardwareCleanup(self);
    if (ret != cAtOk)
        return ret;

    return AtSdhVcDefaultStuffingSet((AtSdhVc)self);
    }

static AtModuleConcate ConcateModule(AtChannel self)
    {
    return (AtModuleConcate)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleConcate);
    }

static AtVcgBinder VcgBinderCreate(AtChannel self)
    {
    return AtModuleConcateCreateVcgBinderForVc(ConcateModule(self), (AtSdhVc)self);
    }

static void MethodsInit(AtSdhVc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, DefaultMapType);
        mMethodOverride(m_methods, CanChangeStuffing);
        mMethodOverride(m_methods, StuffEnable);
        mMethodOverride(m_methods, StuffIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, MappingReset);
        mMethodOverride(m_AtSdhChannelOverride, BerControllerCreate);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeMode);
        mMethodOverride(m_AtSdhChannelOverride, SubChannelsInterruptDisable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtSdhChannelOverride, FailureForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtSdhChannelOverride, IsJoiningAps);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, ReferenceTransfer);
        mMethodOverride(m_AtChannelOverride, DataRateInBytesPer125Us);
        mMethodOverride(m_AtChannelOverride, XcEngine);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectCreate);
        mMethodOverride(m_AtChannelOverride, SurEngineObjectDelete);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, AllServicesDestroy);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, HardwareCleanup);
        mMethodOverride(m_AtChannelOverride, VcgBinderCreate);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path  = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, TxSsSet);
        mMethodOverride(m_AtSdhPathOverride, TxSsGet);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsGet);
        mMethodOverride(m_AtSdhPathOverride, SsCompareEnable);
        mMethodOverride(m_AtSdhPathOverride, SsCompareIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PslValueIsValid);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    }

AtSdhVc AtSdhVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhVc));

    /* Super constructor */
    if (AtSdhPathObjectInit((AtSdhPath)self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void SdhVcPdhChannelSet(AtSdhVc self, AtPdhChannel pdhChannel)
    {
    self->pdhChannel = pdhChannel;
    }

AtPdhChannel SdhVcPdhChannelGet(AtSdhVc self)
    {
    return self ? self->pdhChannel : NULL;
    }

AtSdhChannel AtSdhVcAugGet(AtSdhChannel self)
    {
    if (self)
        return AugGet(self);
    return NULL;
    }

eBool AtSdhVcIsHoVc(AtSdhVc self)
    {
    if (self)
        return IsHoVc(self);
    return cAtFalse;
    }

eBool AtSdhVcIsLoVc(AtSdhVc self)
    {
    if (self)
        return AtSdhVcIsHoVc(self) ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

eBool AtSdhVcTu1xChannelized(AtSdhVc self)
    {
    if (self)
        return Tu1xChannelized(self);
    return cAtFalse;
    }

uint16 AtSdhVcDefaultMapType(AtSdhVc self)
    {
    if (self)
        return mMethodsGet(self)->DefaultMapType(self);

    return cAtSdhVcMapTypeNone;
    }

eAtRet AtSdhVcCanChangeStuffing(AtSdhVc self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->CanChangeStuffing(self, enable);
    return cAtErrorNullPointer;
    }

eAtRet AtSdhVcDefaultStuffingSet(AtSdhVc self)
    {
    return AtSdhVcStuffEnable((AtSdhVc)self, DefaultStuffing((AtSdhVc)self));
    }

/**
 * @addtogroup AtSdhVc
 * @{
 */

/**
 * Enable/disable the fixed stuff columns (e.g. column 30 and column 59 of an VC3)
 *
 * @param self This VC
 * @param enable cAtTrue to enable and cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtSdhVcStuffEnable(AtSdhVc self, eBool enable)
    {
    mNumericalAttributeSet(StuffEnable, enable);
    }

/**
 * Check if the fixed stuff columns are enabled or not
 *
 * @param self This VC
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtSdhVcStuffIsEnabled(AtSdhVc self)
    {
    mAttributeGet(StuffIsEnabled, eBool, cAtFalse);
    }

/** @} */


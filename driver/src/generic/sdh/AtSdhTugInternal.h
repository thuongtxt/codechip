/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhTugInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description : TUG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHTUGINTERNAL_H_
#define _ATSDHTUGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannelInternal.h"
#include "AtSdhTug.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Representation */
typedef struct tAtSdhTug
    {
    tAtSdhChannel super;
    }tAtSdhTug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhTug AtSdhTugObjectInit(AtSdhTug self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHTUGINTERNAL_H_ */


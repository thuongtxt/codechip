/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH module
 * 
 * File        : AtSdhAuInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description : AU class representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHAUINTERNAL_H_
#define _ATSDHAUINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhPathInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhAu
    {
    tAtSdhPath super;

    void *cache;
    }tAtSdhAu;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu AtSdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHAUINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhAu.c
 *
 * Created Date: Sep 4, 2012
 *
 * Description : AU class
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "atclib.h"
#include "AtSdhAuInternal.h"
#include "AtModuleSdhInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSdhAu)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;

/* To save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSts(AtSdhChannel self)
    {
    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAu3)
        return 1;

    return AtSdhChannelNumSts(AtSdhChannelParentChannelGet(self));
    }

static AtChannel VcGet(AtChannel sdhChannel)
    {
    return (AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)sdhChannel, 0);
    }

static uint32 DefectGet(AtChannel self)
    {
    return AtChannelDefectGet(VcGet(self));
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return AtChannelDefectInterruptGet(VcGet(self));
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return AtChannelDefectInterruptClear(VcGet(self));
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    return AtChannelSupportedInterruptMasks(VcGet(self));
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    return AtChannelInterruptMaskSet(VcGet(self), defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    return AtChannelInterruptMaskGet(VcGet(self));
    }

static eAtRet SsSet(AtSdhPath path, eAtSdhChannelMode mode)
    {
    eAtModuleSdhRet ret = cAtOk;
    AtModuleSdh sdhModule;
    eBool ssBitCompareEnabled = cAtFalse;

    if (!mMethodsGet(path)->HasPointerProcessor(path))
        return cAtOk;

    sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)path);
    if (sdhModule == NULL)
        return cAtError;

    if (mode == cAtSdhChannelModeSonet)
        {
        ret |= AtSdhPathTxSsSet(path, 0x0);
        ret |= AtSdhPathExpectedSsSet(path, 0x0);
        }

    else
        {
        ret |= AtSdhPathTxSsSet(path, 0x2);
        ret |= AtSdhPathExpectedSsSet(path, 0x2);
        ssBitCompareEnabled = AtSdhPathSsBitCompareIsEnabledAsDefault(path);
        }

    ret |= AtSdhPathSsCompareEnable(path, ssBitCompareEnabled);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Set default SS bit and initialize its VC */
    ret |= AtSdhAuDefaultSsSet((AtSdhPath)self);
    ret |= AtChannelInit((AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));

    return ret;
    }

static const char *TypeString(AtChannel self)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)self);

    if (channelType == cAtSdhChannelTypeAu4_64c) return "au4_64c";
    if (channelType == cAtSdhChannelTypeAu4_16c) return "au4_16c";
    if (channelType == cAtSdhChannelTypeAu4_4c)  return "au4_4c";
    if (channelType == cAtSdhChannelTypeAu4_16nc)return "au4_16nc";
    if (channelType == cAtSdhChannelTypeAu4_nc)  return "au4_nc";
    if (channelType == cAtSdhChannelTypeAu4)     return "au4";
    if (channelType == cAtSdhChannelTypeAu3)     return "au3";

    return "unknown";
    }

static const char *IdString(AtChannel self)
    {
    AtChannel aug = (AtChannel)AtSdhChannelParentChannelGet((AtSdhChannel)self);

    /* Use parent ID if it map 1 sub channel */
    if (AtSdhChannelNumberOfSubChannelsGet((AtSdhChannel)aug) <= 1)
        return AtChannelIdString(aug);

    /* Use parent ID plus local ID of sub channel */
    else
        {
        static char idString[16];
        AtSprintf(idString, "%s.%u", AtChannelIdString(aug), AtChannelIdGet((AtChannel)self) + 1);

        return idString;
        }
    }

static AtSdhPath AuVcGet(AtSdhPath self)
    {
    return (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    return AtSdhPathTxPslSet(AuVcGet(self), psl);
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    return AtSdhPathTxPslGet(AuVcGet(self));
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    return AtSdhPathRxPslGet(AuVcGet(self));
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    return AtSdhPathExpectedPslSet(AuVcGet(self), psl);
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    return AtSdhPathExpectedPslGet(AuVcGet(self));
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    return AtSdhChannelTxTtiGet((AtSdhChannel)AuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    return AtSdhChannelTxTtiSet((AtSdhChannel)AuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    return AtSdhChannelExpectedTtiGet((AtSdhChannel)AuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    return AtSdhChannelExpectedTtiSet((AtSdhChannel)AuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    return AtSdhChannelRxTtiGet((AtSdhChannel)AuVcGet((AtSdhPath)self), tti);
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    return AtSdhChannelTimMonitorEnable((AtSdhChannel)AuVcGet((AtSdhPath)self), enable);
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    return AtSdhChannelTimMonitorIsEnabled((AtSdhChannel)AuVcGet((AtSdhPath)self));
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    return AtSdhChannelTtiCompareEnable((AtSdhChannel)AuVcGet((AtSdhPath)self), enable);
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    return AtSdhChannelTtiCompareIsEnabled((AtSdhChannel)AuVcGet((AtSdhPath)self));
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return AtChannelCounterGet((AtChannel)AuVcGet((AtSdhPath)self), counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return AtChannelCounterClear((AtChannel)AuVcGet((AtSdhPath)self), counterType);
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    return AtSdhChannelAlarmAffectingEnable((AtSdhChannel)AuVcGet((AtSdhPath)self), alarmType, enable);
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    return AtSdhChannelAlarmAffectingIsEnabled((AtSdhChannel)AuVcGet((AtSdhPath)self), alarmType);
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    return AtSdhChannelAlarmAffectingMaskGet((AtSdhChannel)AuVcGet((AtSdhPath)self));
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    eAtRet ret = m_AtSdhChannelMethods->ModeSet(self, mode);
    if (ret != cAtOk)
        return ret;

    return SsSet((AtSdhPath)self, mode);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    AtUnused(self);
    AtUnused(mapType);
    return cAtErrorModeNotSupport;
    }

static eAtRet HoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    return AtSdhChannelHoldOffTimerSet((AtSdhChannel)AuVcGet((AtSdhPath)self), timerInMs);
    }

static uint32 HoldOffTimerGet(AtSdhChannel self)
    {
    return AtSdhChannelHoldOffTimerGet((AtSdhChannel)AuVcGet((AtSdhPath)self));
    }

static eAtRet WarmRestore(AtChannel self)
    {
    /* Restore line object */
    AtSdhChannelLineObjectGet((AtSdhChannel)self);

    return AtChannelWarmRestore((AtChannel)AuVcGet((AtSdhPath)self));
    }

static eAtRet DefaultSsSet(AtSdhPath path)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)path);
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, AtSdhChannelLineGet((AtSdhChannel)path));
    eAtSdhChannelMode lineMode = AtSdhChannelModeGet((AtSdhChannel)line);
    return SsSet(path, lineMode);
    }

static eBool HasPohProcessor(AtSdhPath self)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathHasPohProcessor(vc);
    }

static void **CacheMemoryAddress(AtChannel self)
    {
    return &(mThis(self)->cache);
    }

static eBool CanHaveMapping(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtSurEngine SurEngineGet(AtChannel self)
    {
    return AtChannelSurEngineGet((AtChannel)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(cache);
    }

static void AlarmForwardingClearanceNotifyWithSubChannelOnly(AtSdhChannel self, uint32 currentDefects, eBool subChannelsOnly)
    {
    if (AtSdhChannelHasAlarmForwarding(self, currentDefects))
        return;

    if (subChannelsOnly == cAtFalse)
        AtChannelAllAlarmListenersCall((AtChannel)self, cAtSdhPathAlarmAis, 0);

    m_AtSdhChannelMethods->AlarmForwardingClearanceNotifyWithSubChannelOnly(self, currentDefects, subChannelsOnly);
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohMonitorEnable(vc, enabled);
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohMonitorIsEnabled(vc);
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohInsertionEnable(vc, enabled);
    }

static eBool PohInsertionIsEnabled(AtSdhPath self)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathPohInsertionIsEnabled(vc);
    }

static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    AtSdhChannel vc = (AtSdhChannel)AuVcGet((AtSdhPath)self);

    if (vc)
        return AtSdhChannelCounterModeSet(vc, counterType, mode);
    return cAtOk;
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(sdhChannel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(sdhChannel), sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, NumSts);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, CanHaveMapping);
        mMethodOverride(m_AtSdhChannelOverride, AlarmForwardingClearanceNotifyWithSubChannelOnly);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareEnable);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, CounterModeSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerGet);
        }

    mMethodsSet(sdhChannel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhAu self)
    {
    AtSdhPath path = (AtSdhPath)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, HasPohProcessor);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionIsEnabled);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        mMethodOverride(m_AtChannelOverride, CacheMemoryAddress);
        mMethodOverride(m_AtChannelOverride, SurEngineGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhAu self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhPath(self);
    OverrideAtSdhChannel(self);
    }

AtSdhAu AtSdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhAu));

    /* Super constructor */
    if ((AtSdhAu)AtSdhPathObjectInit((AtSdhPath)self, channelId, channelType, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

eAtRet AtSdhAuDefaultSsSet(AtSdhPath path)
    {
    if (path == NULL)
        return cAtErrorNullPointer;
    return DefaultSsSet(path);
    }

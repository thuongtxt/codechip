/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhAugInternal.h
 * 
 * Created Date: Sep 4, 2012
 *
 * Description : SDH AUG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHAUGINTERNAL_H_
#define _ATSDHAUGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannelInternal.h"
#include "AtSdhAug.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtSdhVc3Config
    {
    tAtSdhTti     txTti;
    tAtSdhTti     expectTti;
    eBool         timMonEnable;
    uint8         txPls;
    uint8         expectPls;
    eBool         erdiEnable;
    uint32        alarmAffectEnableMask;
    eAtTimingMode timingMode;
    AtChannel     timingSource;
    AtSdhChannel  vc3;
    }tAtSdhVc3Config;

typedef struct tAtTug3Mapping
    {
    uint8 mapType;
    union uTug3Info
        {
        tAtSdhVc3Config vc3Cfg;
        AtSdhChannel tug2[7];
        }tug3Info;
    }tAtTug3Mapping;

typedef struct tAtAug1SubMapping
    {
    tAtTug3Mapping tug3Mapping[3];
    }tAtAug1SubMapping;

typedef struct tAtSdhAugMethods
    {
    eBool (*NeedToRestoreSubMapping)(AtSdhAug self);
    }tAtSdhAugMethods;

/* Representation */
typedef struct tAtSdhAug
    {
    tAtSdhChannel super;
    const tAtSdhAugMethods *methods;

    uint8 isInRestoreProgress;
    tAtAug1SubMapping subMapping;
    }tAtSdhAug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAug AtSdhAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module);
eBool AtSdhAug1NeedToRestoreSubMapping(AtSdhAug self);
eAtRet HelperAug1SubMappingRestore(AtSdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHAUGINTERNAL_H_ */


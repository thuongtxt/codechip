/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : AtSdhPath.c
 *
 * Created Date: Aug 22, 2012
 *
 * Author      : namnn
 *
 * Description : AtSdhPath implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleSdhInternal.h"
#include "AtSdhPathInternal.h"
#include "AtApsSelector.h"
#include "AtApsUpsrEngine.h"
#include "../lab/profiler/AtFailureProfilerFactory.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtSdhPath)self)
#define mPathIsValid(self) ((self != NULL) && ((AtSdhChannelTypeGet((AtSdhChannel)self) <= cAtSdhChannelTypeAug1) ? cAtFalse : cAtTrue))
#define mInAccessible(self) AtChannelInAccessible((AtChannel)self)

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtSdhPathMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    tAtSdhPathCounters *counters = (tAtSdhPathCounters *)pAllCounters;
    uint32 bip, rei, rxPPJC, rxNPJC, txPPJC, txNPJC;

    bip    = AtChannelCounterClear(self, cAtSdhPathCounterTypeBip);
    rei    = AtChannelCounterClear(self, cAtSdhPathCounterTypeRei);
    rxPPJC = AtChannelCounterClear(self, cAtSdhPathCounterTypeRxPPJC);
    rxNPJC = AtChannelCounterClear(self, cAtSdhPathCounterTypeRxNPJC);
    txPPJC = AtChannelCounterClear(self, cAtSdhPathCounterTypeTxPPJC);
    txNPJC = AtChannelCounterClear(self, cAtSdhPathCounterTypeTxNPJC);

    if (counters)
        {
        counters->bip    = bip;
        counters->rei    = rei;
        counters->rxPPJC = rxPPJC;
        counters->rxNPJC = rxNPJC;
        counters->txPPJC = txPPJC;
        counters->txNPJC = txNPJC;
        }

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    tAtSdhPathCounters *counters = (tAtSdhPathCounters *)pAllCounters;

    if (pAllCounters == NULL)
        return cAtErrorNullPointer;

    counters->bip    = AtChannelCounterGet(self, cAtSdhPathCounterTypeBip);
    counters->rei    = AtChannelCounterGet(self, cAtSdhPathCounterTypeRei);
    counters->rxPPJC = AtChannelCounterGet(self, cAtSdhPathCounterTypeRxPPJC);
    counters->rxNPJC = AtChannelCounterGet(self, cAtSdhPathCounterTypeRxNPJC);
    counters->txPPJC = AtChannelCounterGet(self, cAtSdhPathCounterTypeTxPPJC);
    counters->txNPJC = AtChannelCounterGet(self, cAtSdhPathCounterTypeTxNPJC);

    return cAtOk;
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
	AtUnused(psl);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static uint8 RxPslGet(AtSdhPath self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
	AtUnused(psl);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModuleSdhRet TxSsSet(AtSdhPath self, uint8 value)
    {
	AtUnused(value);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 TxSsGet(AtSdhPath self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtRet SsInsertionEnable(AtSdhPath self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorNotImplemented : cAtFalse;
    }

static eBool SsInsertionIsEnabled(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleSdhRet ExpectedSsSet(AtSdhPath self, uint8 value)
    {
	AtUnused(value);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static uint8 ExpectedSsGet(AtSdhPath self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return 0;
    }

static eAtModuleSdhRet SsCompareEnable(AtSdhPath self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    /* Let concrete class do */
    return cAtError;
    }

static eBool SsCompareIsEnabled(AtSdhPath self)
    {
	AtUnused(self);
    /* Let concrete class do */
    return cAtFalse;
    }

static eAtModuleSdhRet ERdiEnable(AtSdhPath self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtError;
    }

static eBool ERdiIsEnabled(AtSdhPath self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eAtRet DefaultAlarmAffectingSet(AtChannel self)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    eAtSdhPathAlarmType alarmTypeList[] = {cAtSdhPathAlarmAis, cAtSdhPathAlarmLop, cAtSdhPathAlarmAis,
                                           cAtSdhPathAlarmPlm, cAtSdhPathAlarmUneq};

    if (!mMethodsGet(mThis(self))->HasPohProcessor(mThis(self)))
        return cAtOk;

    /* Set default alarm affecting */
    for (i = 0; i < mCount(alarmTypeList); i++)
        {
        if (!AtSdhChannelCanEnableAlarmAffecting((AtSdhChannel)self, alarmTypeList[i]))
            continue;

        ret |= AtSdhChannelAlarmAffectingEnable((AtSdhChannel)self, alarmTypeList[i], cAtTrue);
        }

    /* For TIM, disable AIS downstream as default */
    if (AtSdhChannelCanEnableAlarmAffecting((AtSdhChannel)self, cAtSdhPathAlarmTim))
        ret |= AtSdhChannelAlarmAffectingEnable((AtSdhChannel)self, cAtSdhPathAlarmTim, cAtFalse);

    return ret;
    }

static eAtRet DefaultCounterModeSet(AtChannel self)
    {
    uint8 i;
    eAtRet ret = cAtOk;
    eAtSdhPathCounterType counterTypes[] = {cAtSdhPathCounterTypeBip, cAtSdhPathCounterTypeRei};

    /* Set default counter mode */
    for (i = 0; i < mCount(counterTypes); i++)
        ret |= AtSdhChannelDefaultCounterModeSet((AtSdhChannel)self, counterTypes[i]);

    return ret;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    eAtRet ret = m_AtSdhChannelMethods->ModeSet(self, mode);
    if (ret != cAtOk)
        return ret;

    return DefaultCounterModeSet((AtChannel)self);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Clear channel interrupt mask. */
    AtChannelInterruptMaskSet(self, AtChannelInterruptMaskGet(self), 0x0);

    return DefaultAlarmAffectingSet(self);
    }

static const char *TypeString(AtChannel self)
    {
	AtUnused(self);
    return "sdhpath";
    }

static eBool HasPohProcessor(AtSdhPath self)
    {
    /* Most of channels have this */
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasPointerProcessor(AtSdhPath self)
    {
    /* Just for some special cases. But mostly, all of AU has pointer processor */
    AtUnused(self);
    return cAtTrue;
    }

static eBool PslValueIsValid(AtSdhPath self, uint8 psl)
    {
    AtUnused(self);
    AtUnused(psl);
    return cAtTrue;
    }

static uint32 CacheSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tAtSdhPathCache);
    }

static void CacheTxPslSet(AtSdhPath self, uint8 psl)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->txPsl = psl;
    }

static uint8 CacheTxPslGet(AtSdhPath self)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->txPsl : 0);
    }

static void CacheExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->expectedPsl = psl;
    }

static uint8 CacheExpectedPslGet(AtSdhPath self)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->expectedPsl : 0);
    }

static void CacheTxSsSet(AtSdhPath self, uint8 value)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->txSs = value;
    }

static uint8 CacheTxSsGet(AtSdhPath self)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->txSs : 0);
    }

static void CacheExpectedSsSet(AtSdhPath self, uint8 value)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->expectedSs = value;
    }

static uint8 CacheExpectedSsGet(AtSdhPath self)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    return (uint8)(cache ? cache->expectedSs : 0);
    }

static void CacheSsCompareEnable(AtSdhPath self, eBool enable)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->ssCompared = enable;
    }

static eBool CacheSsCompareIsEnabled(AtSdhPath self)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    return (eBool)(cache ? cache->ssCompared : cAtFalse);
    }

static void CacheERdiEnable(AtSdhPath self, eBool enable)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    if (cache)
        cache->erdiEnabled = enable;
    }

static eBool CacheERdiIsEnabled(AtSdhPath self)
    {
    tAtSdhPathCache *cache = AtChannelCacheGet((AtChannel)self);
    return (eBool)(cache ? cache->erdiEnabled : cAtFalse);
    }

static eAtModuleSdhRet PlmMonitorEnable(AtSdhPath self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtErrorNotImplemented;
    }

static eBool PlmMonitorIsEnabled(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void InterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(sts);
    AtUnused(vt);
    AtUnused(hal);
    }

static eBool HasAlarmForwarding(AtSdhChannel self, uint32 currentDefects)
    {
    uint32 alarmAffectingMask;

    /* Note: TU path will have no LOM defect, but it is not a problem in alarm forwarding calculation here. */
    if (currentDefects & (cAtSdhPathAlarmAis | cAtSdhPathAlarmLop | cAtSdhPathAlarmLom))
        return cAtTrue;

    if ((currentDefects & (cAtSdhPathAlarmTim | cAtSdhPathAlarmUneq | cAtSdhPathAlarmPlm)) == 0)
        return cAtFalse;

    alarmAffectingMask = AtSdhChannelAlarmAffectingMaskGet(self);

    if ((currentDefects & cAtSdhPathAlarmTim) && (alarmAffectingMask & cAtSdhPathAlarmTim))
        return cAtTrue;

    if ((currentDefects & cAtSdhPathAlarmUneq) && (alarmAffectingMask & cAtSdhPathAlarmUneq))
        return cAtTrue;

    if ((currentDefects & cAtSdhPathAlarmPlm) && (alarmAffectingMask & cAtSdhPathAlarmPlm))
        return cAtTrue;

    return AtSdhChannelHasParentAlarmForwarding(self);
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    AtUnused(self);
    return enabled ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ERdiDatabaseIsEnabled(AtSdhPath self)
    {
    /* Concrete should handle */
    AtUnused(self);
    return cAtFalse;
    }

static tAtSdhPathApsInfo* ApsInfoGet(AtSdhPath self)
    {
    /* Concrete should handle */
    AtUnused(self);
    return NULL;
    }

static AtApsEngine ApsEngineGet(AtSdhPath self)
    {
    tAtSdhPathApsInfo *apsInfo = mMethodsGet(self)->ApsInfoGet(self);

    if (apsInfo)
        return apsInfo->apsEngine;

    return NULL;
    }

static void ApsEngineSet(AtSdhPath self, AtApsEngine selector)
    {
    tAtSdhPathApsInfo* apsInfo = mMethodsGet(self)->ApsInfoGet(self);
    if (apsInfo)
        apsInfo->apsEngine = selector;
    }

static AtList ApsSelectorsGet(AtSdhPath self)
    {
    tAtSdhPathApsInfo* apsInfo = mMethodsGet(self)->ApsInfoGet(self);
    return apsInfo ? apsInfo->selectors : NULL;
    }

static AtList ApsSelectors(AtSdhPath self)
    {
    tAtSdhPathApsInfo *apsInfo = mMethodsGet(self)->ApsInfoGet(self);

    if (apsInfo == NULL)
        return NULL;

    if (apsInfo->selectors == NULL)
        apsInfo->selectors = AtListCreate(0);

    return apsInfo->selectors;
    }

static eAtRet ApsSelectorJoin(AtSdhPath self, AtApsSelector selector)
    {
    AtList selectors = ApsSelectors(self);

    if (AtListContainsObject(selectors, (AtObject)selector))
        return cAtOk;

    return AtListObjectAdd(selectors, (AtObject)selector);
    }

static eAtRet ApsSelectorLeave(AtSdhPath self, AtApsSelector selector)
    {
    AtList selectors = ApsSelectors(self);

    if (AtListContainsObject(selectors, (AtObject)selector))
        return AtListObjectRemove(selectors, (AtObject)selector);

    return cAtOk;
    }

static AtApsSelector ApsSelectorGet(AtSdhPath self)
    {
    tAtSdhPathApsInfo *apsInfo = mMethodsGet(self)->ApsInfoGet(self);

    if (apsInfo)
        return apsInfo->apsSelector;

    return NULL;
    }

static void ApsSelectorSet(AtSdhPath self, AtApsSelector selector)
    {
    tAtSdhPathApsInfo* apsInfo = mMethodsGet(self)->ApsInfoGet(self);
    if (apsInfo)
        apsInfo->apsSelector = selector;
    }

static void ApsCleanup(AtSdhPath self)
    {
    tAtSdhPathApsInfo *apsInfo = mMethodsGet(self)->ApsInfoGet(self);
    if (apsInfo)
        {
        AtObjectDelete((AtObject)apsInfo->selectors);
        apsInfo->selectors = NULL;
        }
    }

static eBool UpsrIsApplicable(AtSdhPath self)
    {
    /* Not much products support so far, let them determine */
    AtUnused(self);
    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    ApsCleanup(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    AtUnused(self);
    return enabled ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PohInsertionIsEnabled(AtSdhPath self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleSdhRet ApsSwitchingConditionSet(AtSdhPath self, uint32 defects, eAtApsSwitchingCondition condition)
    {
    AtUnused(self);
    AtUnused(defects);
    AtUnused(condition);
    return cAtErrorNotImplemented;
    }

static eAtApsSwitchingCondition ApsSwitchingConditionGet(AtSdhPath self, uint32 defect)
    {
    AtUnused(self);
    AtUnused(defect);
    return cAtApsSwitchingConditionInvalid;
    }

static eAtModuleSdhRet ApsForcedSwitchingConditionSet(AtSdhPath self, eAtApsSwitchingCondition condition)
    {
    AtUnused(self);
    AtUnused(condition);
    return cAtErrorNotImplemented;
    }

static eAtApsSwitchingCondition ApsForcedSwitchingConditionGet(AtSdhPath self)
    {
    AtUnused(self);
    return cAtApsSwitchingConditionInvalid;
    }

static eBool SsBitCompareIsEnabledAsDefault(AtSdhPath self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    return mMethodsGet(sdhModule)->SsBitCompareIsEnabledAsDefault(sdhModule);
    }

static AtFailureProfiler FailureProfilerObjectCreate(AtChannel self)
    {
    return AtFailureProfilerFactorySdhPathProfilerCreate(AtChannelProfilerFactory(self), self);
    }

static AtQuerier QuerierGet(AtChannel self)
    {
    AtUnused(self);
    return AtSdhPathSharedQuerier();
    }

static void OverrideAtObject(AtSdhPath self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhPath self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, CacheSize);
        mMethodOverride(m_AtChannelOverride, FailureProfilerObjectCreate);
        mMethodOverride(m_AtChannelOverride, QuerierGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhPath self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, HasAlarmForwarding);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhPath self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

static void MethodsInit(AtSdhPath self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, TxPslSet);
        mMethodOverride(m_methods, TxPslGet);
        mMethodOverride(m_methods, RxPslGet);
        mMethodOverride(m_methods, ExpectedPslSet);
        mMethodOverride(m_methods, ExpectedPslGet);
        mMethodOverride(m_methods, PslValueIsValid);
        mMethodOverride(m_methods, TxSsSet);
        mMethodOverride(m_methods, TxSsGet);
        mMethodOverride(m_methods, SsInsertionEnable);
        mMethodOverride(m_methods, SsInsertionIsEnabled);
        mMethodOverride(m_methods, ExpectedSsSet);
        mMethodOverride(m_methods, ExpectedSsGet);
        mMethodOverride(m_methods, SsCompareEnable);
        mMethodOverride(m_methods, SsCompareIsEnabled);
        mMethodOverride(m_methods, SsBitCompareIsEnabledAsDefault);
        mMethodOverride(m_methods, ERdiEnable);
        mMethodOverride(m_methods, ERdiIsEnabled);
        mMethodOverride(m_methods, ERdiDatabaseIsEnabled);
        mMethodOverride(m_methods, HasPohProcessor);
        mMethodOverride(m_methods, HasPointerProcessor);
        mMethodOverride(m_methods, PlmMonitorEnable);
        mMethodOverride(m_methods, PlmMonitorIsEnabled);
        mMethodOverride(m_methods, InterruptProcess);
        mMethodOverride(m_methods, PohMonitorEnable);
        mMethodOverride(m_methods, PohMonitorIsEnabled);
        mMethodOverride(m_methods, ApsInfoGet);
        mMethodOverride(m_methods, UpsrIsApplicable);
        mMethodOverride(m_methods, PohInsertionEnable);
        mMethodOverride(m_methods, PohInsertionIsEnabled);
        mMethodOverride(m_methods, ApsSwitchingConditionSet);
        mMethodOverride(m_methods, ApsSwitchingConditionGet);
        mMethodOverride(m_methods, ApsForcedSwitchingConditionSet);
        mMethodOverride(m_methods, ApsForcedSwitchingConditionGet);
        }

    mMethodsSet(self, &m_methods);
    }

AtSdhPath AtSdhPathObjectInit(AtSdhPath self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tAtSdhPath));

    /* Super constructor */
    if (AtSdhChannelObjectInit((AtSdhChannel)self, channelId, channelType, module) == NULL)
        return NULL;

    /* Initialize implementation */
    Override(self);
    MethodsInit(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }

eBool AtSdhPathHasPohProcessor(AtSdhPath self)
    {
    if (self)
        return mMethodsGet(self)->HasPohProcessor(self);
    return cAtFalse;
    }

eBool AtSdhPathHasPointerProcessor(AtSdhPath self)
    {
    if (self)
        return mMethodsGet(self)->HasPointerProcessor(self);
    return cAtFalse;
    }

void AtSdhPathInterruptProcess(AtSdhPath self, uint8 slice, uint8 sts, uint8 vt, AtHal hal)
    {
    if (self)
        mMethodsGet(self)->InterruptProcess(self, slice, sts, vt, hal);
    }

void AtSdhPathApsEngineSet(AtSdhPath self, AtApsEngine engine)
    {
    if (self)
        ApsEngineSet(self, engine);
    }

/* Join to an APS selector as a working path or a protection path */
void AtSdhPathApsSelectorJoin(AtSdhPath self, AtApsSelector selector)
    {
    if (self)
        ApsSelectorJoin(self, selector);
    }

/* Leave from an APS selector as a working path or a protection path */
void AtSdhPathApsSelectorLeave(AtSdhPath self, AtApsSelector selector)
    {
    if (self)
        ApsSelectorLeave(self, selector);
    }

AtList AtSdhPathApsSelectors(AtSdhPath self)
    {
    if (self)
        return ApsSelectors(self);
    return NULL;
    }

/* Set an APS selector for an outgoing protected path */
void AtSdhPathApsSelectorSet(AtSdhPath self, AtApsSelector selector)
    {
    if (self)
        ApsSelectorSet(self, selector);
    }

/* Get an APS selector of an outgoing protected path */
AtApsSelector AtSdhPathApsSelectorGet(AtSdhPath self)
    {
    if (self)
        return ApsSelectorGet(self);
    return NULL;
    }

/*
 * Enable/disable POH processor
 *
 * @param self This path
 * @param enabled This path
 *
 * @return AT return code
 */
eAtRet AtSdhPathPohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->PohMonitorEnable(self, enabled);
    return cAtErrorNullPointer;
    }

/*
 * Check of POH monitoring is enabled
 *
 * @param self This path
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtSdhPathPohMonitorIsEnabled(AtSdhPath self)
    {
    if (self)
        return mMethodsGet(self)->PohMonitorIsEnabled(self);
    return cAtFalse;
    }

eAtRet AtSdhPathPohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    if (self)
        return mMethodsGet(self)->PohInsertionEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool AtSdhPathPohInsertionIsEnabled(AtSdhPath self)
    {
    if (self)
        return mMethodsGet(self)->PohInsertionIsEnabled(self);
    return cAtFalse;
    }

eBool AtSdhPathERdiDatabaseIsEnabled(AtSdhPath self)
    {
    if (self)
        return mMethodsGet(self)->ERdiDatabaseIsEnabled(self);
    return cAtFalse;
    }

eBool AtSdhPathUpsrIsApplicable(AtSdhPath self)
    {
    if (self)
        return mMethodsGet(self)->UpsrIsApplicable(self);
    return cAtFalse;
    }

eBool AtSdhPathCounterIsPointerAdjust(uint16 counterType)
    {
    switch (counterType)
        {
        case cAtSdhPathCounterTypeTxPPJC: return cAtTrue;
        case cAtSdhPathCounterTypeRxPPJC: return cAtTrue;
        case cAtSdhPathCounterTypeTxNPJC: return cAtTrue;
        case cAtSdhPathCounterTypeRxNPJC: return cAtTrue;
        default:
            return cAtFalse;
        }
    }

eBool AtSdhPathSsBitCompareIsEnabledAsDefault(AtSdhPath self)
    {
    if (self)
        return mMethodsGet(self)->SsBitCompareIsEnabledAsDefault(self);
    return cAtFalse;
    }

/**
 * @addtogroup AtSdhPath
 * @{
 */
/**
 * Set transmitted Path Signal Label
 *
 * @param self This channel
 * @param psl Path Signal Label value
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathTxPslSet(AtSdhPath self, uint8 psl)
    {
    if (!mPathIsValid(self))
        return cAtError;

    if (!mMethodsGet(self)->PslValueIsValid(self, psl))
        return cAtErrorInvlParm;

    CacheTxPslSet(self, psl);
    mNumericalAttributeSet(TxPslSet, psl);
    }

/**
 * Get transmitted Path Signal Label
 *
 * @param self This channel
 *
 * @return Transmitted PSL value
 */
uint8 AtSdhPathTxPslGet(AtSdhPath self)
    {
    mAttributeGetWithCache(TxPslGet, uint8, 0);
    }

/**
 * Get received Path Signal Label value
 *
 * @param self This channel
 *
 * @return Received Path Signal Label value
 */
uint8 AtSdhPathRxPslGet(AtSdhPath self)
    {
    mAttributeGet(RxPslGet, uint8, 0);
    }

/**
 * Set expected Path Signal Label
 *
 * @param self This channel
 * @param psl Expected Path Signal Label value
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    if (!mPathIsValid(self))
        return cAtError;

    if (!mMethodsGet(self)->PslValueIsValid(self, psl))
        return cAtErrorInvlParm;

    CacheExpectedPslSet(self, psl);
    mNumericalAttributeSet(ExpectedPslSet, psl);
    }

/**
 * Get expected Path Signal Label
 *
 * @param self This channel
 *
 * @return Expected Path Signal Label value
 */
uint8 AtSdhPathExpectedPslGet(AtSdhPath self)
    {
    mAttributeGetWithCache(ExpectedPslGet, uint8, 0);
    }

/**
 * Transmit SS bits
 *
 * @param self This path
 * @param value SS bits value
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathTxSsSet(AtSdhPath self, uint8 value)
    {
    mNumericalAttributeSetWithCache(TxSsSet, value);
    }

/**
 * Get transmitted SS bits
 *
 * @param self This path
 *
 * @return Transmitted SS bits
 */
uint8 AtSdhPathTxSsGet(AtSdhPath self)
    {
    mAttributeGetWithCache(TxSsGet, uint8, 0);
    }

/**
 * Set expected SS bits
 *
 * @param self This path
 * @param value SS bits value
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathExpectedSsSet(AtSdhPath self, uint8 value)
    {
    mNumericalAttributeSetWithCache(ExpectedSsSet, value);
    }

/**
 * Get expected SS bits
 *
 * @param self This path
 *
 * @return SS bits
 */
uint8 AtSdhPathExpectedSsGet(AtSdhPath self)
    {
    mAttributeGetWithCache(ExpectedSsGet, uint8, 0);
    }

/**
 * Enable SS bits comparing
 *
 * @param self This path
 * @param enable Enable/disable SS bit comparing
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathSsCompareEnable(AtSdhPath self, eBool enable)
    {
    mNumericalAttributeSetWithCache(SsCompareEnable, enable);
    }

/**
 * Check if SS bits comparing is enabled
 *
 * @param self This path
 *
 * @retval cAtTrue - SS bits comparing is enabled
 * @retval cAtFalse - SS bits comparing is disabled
 */
eBool AtSdhPathSsCompareIsEnabled(AtSdhPath self)
    {
    mAttributeGetWithCache(SsCompareIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable SS bit insertion from value configured by CPU
 *
 * @param self This path
 * @param enable Enabling
 *               - cAtTrue to enable
 *               - cAtFalse to disable
 *
 * @return AT return code
 */
eAtRet AtSdhPathSsInsertionEnable(AtSdhPath self, eBool enable)
    {
    mNumericalAttributeSet(SsInsertionEnable, enable);
    }

/**
 * Check if SS bit insertion from CPU is enabled
 * @param self This path
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtSdhPathSsInsertionIsEnabled(AtSdhPath self)
    {
    mAttributeGet(SsInsertionIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable E-RDI
 *
 * @param self This path
 * @param enable cAtTrue to enable E-RDI, cAtFalse to disable
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathERdiEnable(AtSdhPath self, eBool enable)
    {
    mNumericalAttributeSetWithCache(ERdiEnable, enable);
    }

/**
 * Check if E-RDI is enabled
 *
 * @param self This path
 *
 * @return cAtTrue if E-RDI is enabled, cAtFalse if it is disabled
 */
eBool AtSdhPathERdiIsEnabled(AtSdhPath self)
    {
    mAttributeGetWithCache(ERdiIsEnabled, eBool, cAtFalse);
    }

/**
 * Enable/disable PLM monitoring
 *
 * @param self This path
 * @param enable Enabling.
 *               - cAtTrue: enable
 *               - cAtFalse: disable
 *
 * @return AT return code.
 */
eAtModuleSdhRet AtSdhPathPlmMonitorEnable(AtSdhPath self, eBool enable)
    {
    mNumericalAttributeSet(PlmMonitorEnable, enable);
    }

/**
 * Check if PLM monitoring is enabled or not
 *
 * @param self This path
 *
 * @retval cAtTrue if enabled
 * @retval cAtFalse if disabled
 */
eBool AtSdhPathPlmMonitorIsEnabled(AtSdhPath self)
    {
    mAttributeGet(PlmMonitorIsEnabled, eBool, cAtFalse);
    }

/**
 * To get all of selectors that path is joining
 *
 * @param self This path
 *
 * @return List of all of selectors that path is joining
 */
AtList AtSdhPathApsSelectorsGet(AtSdhPath self)
    {
    if (self)
        return ApsSelectorsGet(self);
    return NULL;
    }

/**
 * Get APS engine (UPSR) that this path is joining
 *
 * @param self This path
 *
 * @return APS engine (UPSR) that this path is joining
 */
AtApsEngine AtSdhPathApsEngineGet(AtSdhPath self)
    {
    if (self)
        return ApsEngineGet(self);
    return NULL;
    }

/**
 * Set APS auto-switching condition
 *
 * @param self This path
 * @param defects Defect to configure
 * @param condition @ref eAtApsSwitchingCondition "Switching condition" for the input defect
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathApsSwitchingConditionSet(AtSdhPath self, uint32 defects, eAtApsSwitchingCondition condition)
    {
    mTwoParamsAttributeSet(ApsSwitchingConditionSet, defects, condition);
    }

/**
 * Get APS auto-switching condition
 *
 * @param self This path
 * @param defect Defect
 *
 * @return @ref eAtApsSwitchingCondition "Switching condition" for the input defect
 */
eAtApsSwitchingCondition AtSdhPathApsSwitchingConditionGet(AtSdhPath self, uint32 defect)
    {
    mOneParamAttributeGet(ApsSwitchingConditionGet, defect, eAtApsSwitchingCondition, cAtApsSwitchingConditionNone)
    }

/**
 * Set APS user-initiated auto-switching condition
 *
 * @param self This path
 * @param condition @ref eAtApsSwitchingCondition "Forced switching condition"
 *
 * @return AT return code
 */
eAtModuleSdhRet AtSdhPathApsForcedSwitchingConditionSet(AtSdhPath self, eAtApsSwitchingCondition condition)
    {
    mNumericalAttributeSet(ApsForcedSwitchingConditionSet, condition);
    }

/**
 * Get APS user-initiated auto-switching condition
 *
 * @param self This path
 *
 * @return @ref eAtApsSwitchingCondition "User-forced switching condition"
 */
eAtApsSwitchingCondition AtSdhPathApsForcedSwitchingConditionGet(AtSdhPath self)
    {
    mAttributeGet(ApsForcedSwitchingConditionGet, eAtApsSwitchingCondition, cAtApsSwitchingConditionInvalid);
    }

/** @} */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : AtStsGroup.c
 *
 * Created Date: Nov 17, 2016
 *
 * Description : STS pass-through group
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtStsGroup.h"
#include "AtList.h"
#include "AtStsGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtStsGroupMethods m_methods;

/* Override */
static tAtObjectMethods        m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DeleteStsList(AtStsGroup self)
    {
    if (self->stsList == NULL)
        return;

    AtListDeleteWithObjectHandler(self->stsList, NULL);
    self->stsList = NULL;
    }

static AtList StsListGet(AtStsGroup self)
    {
    if (self->stsList != NULL)
        return self->stsList;

    self->stsList = AtListCreate(0);
    return self->stsList;
    }

static eAtRet StsAdd(AtStsGroup self, AtSdhSts sts1)
    {
    AtList stsList = StsListGet(self);
    if (stsList == NULL)
        return cAtErrorRsrcNoAvail;

    if (AtListContainsObject(stsList, (AtObject)sts1))
        return cAtOk;

    return AtListObjectAdd(stsList, (AtObject)sts1);
    }

static eAtRet StsDelete(AtStsGroup self, AtSdhSts sts1)
    {
    AtList stsList = StsListGet(self);
    if (stsList == NULL)
        return cAtErrorRsrcNoAvail;

    return AtListObjectRemove(stsList, (AtObject)sts1);
    }

static uint32 NumSts(AtStsGroup self)
    {
    AtList stsList = StsListGet(self);
    if (stsList == NULL)
        return 0;

    return AtListLengthGet(stsList);
    }

static AtSdhSts StsAtIndexGet(AtStsGroup self, uint32 stsIndex)
    {
    AtList stsList = StsListGet(self);
    if (stsList == NULL)
        return NULL;

    return (AtSdhSts)AtListObjectGet(stsList, stsIndex);
    }

static eAtRet DestGroupSet(AtStsGroup self, AtStsGroup destGroup)
    {
    if (destGroup)
        AtStsGroupSrcGroupSet(destGroup, self);
    else
        AtStsGroupSrcGroupSet(self->destGroup, NULL);

    self->destGroup = destGroup;
    return cAtOk;
    }

static eAtRet PassThroughEnable(AtStsGroup self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtErrorNotImplemented;
    }

static eBool PassThroughIsEnabled(AtStsGroup self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool GroupIsBusy(AtStsGroup self)
    {
    if (AtStsGroupPassThroughIsEnabled(self) == cAtTrue)
        return cAtErrorChannelBusy;

    if ((AtStsGroupDestGroupGet(self) != NULL) || (AtStsGroupSrcGroupGet(self) != NULL))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

static const char *ToString(AtObject self)
    {
    AtModule module = AtStsGroupModuleGet((AtStsGroup)self);
    AtDevice dev = AtModuleDeviceGet(module);
    static char str[64];

    AtSprintf(str, "%ssts_group", AtDeviceIdToString(dev));
    return str;
    }

static void Delete(AtObject self)
    {
    DeleteStsList((AtStsGroup)self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(AtStsGroup self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtStsGroup self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(AtStsGroup self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StsAdd);
        mMethodOverride(m_methods, StsDelete);
        mMethodOverride(m_methods, DestGroupSet);
        mMethodOverride(m_methods, PassThroughEnable);
        mMethodOverride(m_methods, PassThroughIsEnabled);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtStsGroup);
    }

AtStsGroup AtStsGroupObjectInit(AtStsGroup self, uint32 groupId, AtModule module)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (AtChannelObjectInit((AtChannel)self, groupId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

void AtStsGroupSrcGroupSet(AtStsGroup self, AtStsGroup srcGroup)
    {
    if (self)
        self->srcGroup = srcGroup;
    }

AtStsGroup AtStsGroupSrcGroupGet(AtStsGroup self)
    {
    return (self) ? self->srcGroup : NULL;
    }

/**
 * @addtogroup AtStsGroup
 * @{
 */

/**
 * Get module that manage this group
 *
 * @param self This STS Group
 *
 * @return Module Object
 */
AtModule AtStsGroupModuleGet(AtStsGroup self)
    {
    return AtChannelModuleGet((AtChannel)self);
    }

/**
 * Get STS group ID
 *
 * @param self This STS Group
 *
 * @return STS group ID number
 */
uint32 AtStsGroupIdGet(AtStsGroup self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

/**
 * Add an STS-1 into the STS group
 *
 * @param self This STS Group
 * @param sts1 STS-1 object @ref AtSdhSts
 *
 * @return AT return code
 */
eAtRet AtStsGroupStsAdd(AtStsGroup self, AtSdhSts sts1)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* While pass-through is enabled, do not allow user to modify this configuration. */
    if (GroupIsBusy(self))
        return cAtErrorChannelBusy;

    return mMethodsGet(self)->StsAdd(self, sts1);
    }

/**
 * Delete an STS-1 from the STS group
 *
 * @param self This STS Group
 * @param sts1 STS-1 object @ref AtSdhSts
 *
 * @return AT return code
 */
eAtRet AtStsGroupStsDelete(AtStsGroup self, AtSdhSts sts1)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* While pass-through is enabled, do not allow user to modify this configuration. */
    if (GroupIsBusy(self))
        return cAtErrorChannelBusy;

    return mMethodsGet(self)->StsDelete(self, sts1);
    }

/**
 * Get the number of STS-1s were added to group.
 *
 * @param self This STS Group
 *
 * @retval Number of STS-1s added to group.
 */
uint32 AtStsGroupNumSts(AtStsGroup self)
    {
    if (self)
        return NumSts(self);
    return 0;
    }

/**
 * Get the STS-1 object at index
 *
 * @param self This STS Group
 * @param stsIndex STS index in the group
 *
 * @return STS object
 */
AtSdhSts AtStsGroupStsAtIndex(AtStsGroup self, uint32 stsIndex)
    {
    if (self)
        return StsAtIndexGet(self, stsIndex);
    return NULL;
    }

/**
 * Set the destination STS pass-through group. This operation requires that this group and
 * the destination group must have same number of STS-1s and they fully added STS-1s. If
 * the destination group is NULL, the pass-through state must be disabled.
 *
 * @param self This STS Group
 * @param destGroup Destination STS group or NULL
 *
 * @return AT return code
 */
eAtRet AtStsGroupDestGroupSet(AtStsGroup self, AtStsGroup destGroup)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (AtStsGroupDestGroupGet(self) == destGroup)
        return cAtOk;

    if (destGroup != NULL)
        {
        if ((AtStsGroupNumSts(self) == 0) || (AtStsGroupNumSts(self) != AtStsGroupNumSts(destGroup)))
            return cAtErrorNotReady;
        }
    else
        {
        if (AtStsGroupPassThroughIsEnabled(self) == cAtTrue)
            return cAtErrorChannelBusy;
        }

    return mMethodsGet(self)->DestGroupSet(self, destGroup);
    }

/**
 * Get the destination STS pass-through group
 *
 * @param self This STS Group
 *
 * @return Destination STS group
 */
AtStsGroup AtStsGroupDestGroupGet(AtStsGroup self)
    {
    return (self) ? self->destGroup : NULL;
    }

/**
 * Enable unidirectional STS-1s pass-though from this group to the destination group.
 * This operation requires that destination group must be previously set.
 *
 * @param self This STS Group
 * @param enable Unidirectional pass-through enable
 *
 * @return AT return code
 */
eAtRet AtStsGroupPassThroughEnable(AtStsGroup self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    /* We need to set destination group so that pass-through can be enabled. */
    if (AtStsGroupDestGroupGet(self) == NULL)
        return cAtErrorNotReady;

    return mMethodsGet(self)->PassThroughEnable(self, enable);
    }

/**
 * Get the pass-through enable state of STS group.
 *
 * @param self This STS Group
 *
 * @retval cAtTrue if pass-through is enabled
 * @retval cAtFalse if pass-through is disabled
 */
eBool AtStsGroupPassThroughIsEnabled(AtStsGroup self)
    {
    if (self)
        return mMethodsGet(self)->PassThroughIsEnabled(self);
    return cAtFalse;
    }

/**
 * @}
 */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtVcgInternal.h
 * 
 * Created Date: Sep 25, 2014
 *
 * Description : VCG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATVCGINTERNAL_H_
#define _ATVCGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtVcg.h"
#include "AtConcateGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtVcgMethods
    {
    eBool (*LcasIsSupported)(AtVcg self);
    eAtModuleConcateRet (*LcasEnable)(AtVcg self, eBool enable);
    eBool (*LcasIsEnabled)(AtVcg self);

    eAtModuleConcateRet (*HoldOffTimerSet)(AtVcg self, uint32 timerMs);
    uint32 (*HoldOffTimerGet)(AtVcg self);

    eAtModuleConcateRet (*WtrTimerSet)(AtVcg self, uint32 timerMs);
    uint32 (*WtrTimerGet)(AtVcg self);

    eAtModuleConcateRet (*RmvTimerSet)(AtVcg self, uint32 timerMs);
    uint32 (*RmvTimerGet)(AtVcg self);

    uint8 (*SourceRsAckGet)(AtVcg self);
    uint8 (*SinkRsAckGet)(AtVcg self);

    uint8 (*SinkMstGet)(AtVcg self, AtConcateMember member);
    uint8 (*SourceMstGet)(AtVcg self, AtConcateMember member);

    eAtModuleConcateRet (*SinkMemberAdd)(AtVcg self, AtConcateMember member);
    eAtModuleConcateRet (*SinkMemberRemove)(AtVcg self, AtConcateMember member);
    eAtModuleConcateRet (*SourceMemberAdd)(AtVcg self, AtConcateMember member);
    eAtModuleConcateRet (*SourceMemberRemove)(AtVcg self, AtConcateMember member);
    }tAtVcgMethods;

typedef struct tAtVcg
    {
    tAtConcateGroup super;
    const tAtVcgMethods *methods;
    }tAtVcg;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtVcg AtVcgObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType);

eBool AtVcgCanAddSinkMember(AtVcg self, AtConcateMember member);
eBool AtVcgCanAddSourceMember(AtVcg self, AtConcateMember member);
eBool AtVcgCanRemoveMember(AtVcg self, AtConcateMember member);

uint8 AtVcgSinkMstGet(AtVcg self, AtConcateMember member);
uint8 AtVcgSourceMstGet(AtVcg self, AtConcateMember member);

#endif /* _ATVCGINTERNAL_H_ */


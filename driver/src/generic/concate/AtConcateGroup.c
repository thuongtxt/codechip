/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : AtConcateGroup.c
 *
 * Created Date: May 7, 2014
 *
 * Description : VCG Concate Group
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtConcateMember.h"
#include "AtConcateGroupInternal.h"
#include "AtConcateMemberInternal.h"
#include "../eth/AtModuleEthInternal.h"
#include "../encap/AtEncapChannelInternal.h"
#include "../prbs/AtModulePrbsInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtConcateGroup)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtConcateGroupMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    switch (mThis(self)->concateType)
        {
        case cAtConcateGroupTypeVcat:        return "vcat";
        case cAtConcateGroupTypeCcat:        return "ccat";
        case cAtConcateGroupTypeNVcat:       return "non_vcat";
        case cAtConcateGroupTypeNVcat_g804:  return "non_vcat_g804";
        case cAtConcateGroupTypeNVcat_g8040: return "non_vcat_g8040";

        default:
            return NULL;
        }

    return NULL;
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    eAtRet ret = cAtOk;
    AtDevice device = AtChannelDeviceGet(self);
    AtEncapBinder encapBinder;

    /* Hardware binding */
    encapBinder = AtDeviceEncapBinder(device);
    ret = AtEncapBinderBindConcateGroupToEncapChannel(encapBinder, self, encapChannel);
    if (ret != cAtOk)
        return ret;

    /* Let super deal with database */
    return m_AtChannelMethods->BindToEncapChannel(self, encapChannel);
    }

static uint32 EncapHwIdAllocate(AtChannel self)
    {
    return AtChannelHwIdGet(self);
    }

static AtList SourceMemberList(AtConcateGroup self)
    {
    if (self->soMemberList == NULL)
        self->soMemberList = AtListCreate(AtConcateGroupMaxNumMembersGet(self));
    return self->soMemberList;
    }

static AtList SinkMemberList(AtConcateGroup self)
    {
    if (self->skMemberList == NULL)
        self->skMemberList = AtListCreate(AtConcateGroupMaxNumMembersGet(self));
    return self->skMemberList;
    }

static AtConcateMember SourceMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember member;

    if (channel == NULL)
        return NULL;

    /* Already provisioned */
    member = AtConcateGroupSourceMemberGetByChannel(self, channel);
    if (member != NULL)
        return member;

    /* Enough members */
    if (AtConcateGroupNumSourceMembersGet(self) == AtConcateGroupMaxNumMembersGet(self))
        return NULL;

    member = AtVcgBinderSourceProvision(AtChannelVcgBinder(channel), self);
    if (member)
        AtListObjectAdd(SourceMemberList(self), (AtObject)member);

    return member;
    }

static AtConcateMember SinkMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember member;

    if (channel == NULL)
        return NULL;

    /* Already provisioned */
    member = AtConcateGroupSinkMemberGetByChannel(self, channel);
    if (member != NULL)
        return member;

    /* Enough members */
    if (AtConcateGroupNumSinkMembersGet(self) == AtConcateGroupMaxNumMembersGet(self))
        return NULL;

    member = AtVcgBinderSinkProvision(AtChannelVcgBinder(channel), self);
    if (member)
        AtListObjectAdd(SinkMemberList(self), (AtObject)member);

    return member;
    }

static eAtModuleConcateRet SourceMemberDeprovision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember member = AtConcateGroupSourceMemberGetByChannel(self, channel);
    eAtRet ret = cAtOk;

    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    ret = AtVcgBinderSourceDeProvision(AtChannelVcgBinder(channel), self);
    if (ret == cAtOk)
        ret = AtListObjectRemove(self->soMemberList, (AtObject)member);

    return ret;
    }

static eAtModuleConcateRet SinkMemberDeprovision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember member;
    eAtRet ret = cAtOk;

    /* Do nothing if member has not been provisioned yet */
    member = AtConcateGroupSinkMemberGetByChannel(self, channel);
    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    /* De-provision and remove it out of member list */
    ret = AtVcgBinderSinkDeProvision(AtChannelVcgBinder(channel), self);
    if (ret == cAtOk)
        ret = AtListObjectRemove(self->skMemberList, (AtObject)member);

    return ret;
    }

static AtConcateMember SoMemberGetByChannel(AtConcateGroup self, AtChannel channel)
    {
    uint32 i;

    if ((self == NULL) || (channel == NULL))
        return NULL;

    for (i = 0; i < AtListLengthGet(self->soMemberList); i++)
        {
        AtConcateMember member = (AtConcateMember)AtListObjectGet(self->soMemberList, i);
        if (AtConcateMemberSourceChannelGet(member) == channel)
            return member;
        }

    return NULL;
    }

static AtConcateMember SkMemberGetByChannel(AtConcateGroup self, AtChannel channel)
    {
    uint32 i;

    if ((self == NULL) || (channel == NULL))
        return NULL;

    for (i = 0; i < AtListLengthGet(self->skMemberList); i++)
        {
        AtConcateMember member = (AtConcateMember)AtListObjectGet(self->skMemberList, i);
        if (AtConcateMemberSinkChannelGet(member) == channel)
            return member;
        }

    return NULL;
    }

static AtConcateMember SoMemberGetByIndex(AtConcateGroup self, uint32 memberIndex)
    {
    return (AtConcateMember)AtListObjectGet(self->soMemberList, memberIndex);
    }

static AtConcateMember SkMemberGetByIndex(AtConcateGroup self, uint32 memberIndex)
    {
    return (AtConcateMember)AtListObjectGet(self->skMemberList, memberIndex);
    }

static uint32 MaxNumMembersGet(AtConcateGroup self)
    {
    switch (self->memberType)
        {
        case cAtConcateMemberTypeVc4_64c: return 256;
        case cAtConcateMemberTypeVc4_16c: return 256;
        case cAtConcateMemberTypeVc4_4c:  return 256;
        case cAtConcateMemberTypeVc4_nc:  return 256;
        case cAtConcateMemberTypeVc4:     return 256;
        case cAtConcateMemberTypeVc3:     return 256;
        case cAtConcateMemberTypeVc12:    return 64;
        case cAtConcateMemberTypeVc11:    return 64;
        case cAtConcateMemberTypeDs1:     return 16;
        case cAtConcateMemberTypeE1:      return 16;
        case cAtConcateMemberTypeDs3:     return 8;
        case cAtConcateMemberTypeE3:      return 8;
        default: return 0;
        }
    }

static eAtConcateGroupType ConcatTypeGet(AtConcateGroup self)
    {
    return self->concateType;
    }

static eAtConcateMemberType MemberTypeGet(AtConcateGroup self)
    {
    return self->memberType;
    }

static uint32 NumSoMembersGet(AtConcateGroup self)
    {
    return AtListLengthGet(self->soMemberList);
    }

static uint32 NumSkMembersGet(AtConcateGroup self)
    {
    return AtListLengthGet(self->skMemberList);
    }

static eAtRet AllSoMemberDelete(AtConcateGroup self)
    {
    if (self->soMemberList == NULL)
        return cAtOk;

    /* Remove all member first */
    while (AtListLengthGet(self->soMemberList))
        {
        AtConcateMember member = (AtConcateMember)AtListObjectRemoveAtIndex(self->soMemberList, 0);

        /* Sink member list may still hold this object, to avoid duplicate
         * deleting, also remove this object out of this list */
        AtListObjectRemove(self->skMemberList, (AtObject)member);

        AtObjectDelete((AtObject)member);
        }

    /* And the list that holds them */
    AtObjectDelete((AtObject)self->soMemberList);
    self->soMemberList = NULL;

    return cAtOk;
    }

static eAtRet AllSkMemberDelete(AtConcateGroup self)
    {
    if (self->skMemberList == NULL)
        return cAtOk;

    /* Remove all member first */
    while (AtListLengthGet(self->skMemberList))
        {
        AtConcateMember member = (AtConcateMember)AtListObjectRemoveAtIndex(self->skMemberList, 0);

        /* Source member list may still hold this object, to avoid duplicate
         * deleting, also remove this object out of this list */
        AtListObjectRemove(self->soMemberList, (AtObject)member);

        AtObjectDelete((AtObject)member);
        }

    /* And the list that holds them */
    AtObjectDelete((AtObject)self->skMemberList);
    self->skMemberList = NULL;

    return cAtOk;
    }

static eAtRet AllMemberDelete(AtConcateGroup self)
    {
    eAtRet ret  = cAtOk;

    ret |= AllSoMemberDelete(self);
    ret |= AllSkMemberDelete(self);

    return ret;
    }

static void PrbsEgineDelete(AtConcateGroup self)
    {
    if (self->prbsEngine)
        {
        AtDevice device = AtChannelDeviceGet((AtChannel) self);
        AtModulePrbs modulePrbs = (AtModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
        AtModulePrbsConcateGroupPrbsEngineDelete(modulePrbs, self->prbsEngine);
        self->prbsEngine = NULL;
        }
    }

static void Delete(AtObject self)
    {
    PrbsEgineDelete((AtConcateGroup)self);
    AllMemberDelete((AtConcateGroup)self);
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtConcateGroup object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(concateType);
    mEncodeUInt(memberType);
    mEncodeObjectDescriptionList(soMemberList);
    mEncodeObjectDescriptionList(skMemberList);
    mEncodeObjectDescription(prbsEngine);
    }

static AtEncapChannel EncapChannelGet(AtConcateGroup self)
    {
    return AtChannelBoundEncapChannel((AtChannel)self);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    AtUnused(loopbackMode);
    return cAtFalse;
    }

static eAtRet Enable(AtChannel self, eBool enable)
	{
	AtUnused(self);
	return enable ? cAtOk: cAtErrorModeNotSupport;
	}

static eBool IsEnabled(AtChannel self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static eBool CanEnable(AtChannel self, eBool enable)
	{
	AtUnused(self);
	return enable;
	}

static eAtRet TxEnable(AtChannel self, eBool enable)
	{
	AtUnused(self);
	return enable ? cAtOk : cAtErrorModeNotSupport;
	}

static eBool TxIsEnabled(AtChannel self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static eBool TxCanEnable(AtChannel self, eBool enable)
	{
	AtUnused(self);
	return enable;
	}

static eAtRet RxEnable(AtChannel self, eBool enable)
	{
	AtUnused(self);
	return enable ? cAtOk : cAtErrorModeNotSupport;
	}

static eBool RxIsEnabled(AtChannel self)
	{
	AtUnused(self);
	return cAtTrue;
	}

static eBool RxCanEnable(AtChannel self, eBool enable)
	{
	AtUnused(self);
	return enable;
	}

static eAtModuleConcateRet EncapTypeSet(AtConcateGroup self, eAtEncapType encapType)
    {
    if (!AtConcateGroupEncapTypeIsSupported(self, encapType))
        return cAtErrorModeNotSupport;
    return cAtOk;
    }

static uint32 EncapTypeGet(AtConcateGroup self)
    {
    AtUnused(self);
    return cAtEncapUnknown;
    }

static eBool EncapTypeIsSupported(AtConcateGroup self, eAtEncapType encapType)
    {
    AtUnused(self);
    AtUnused(encapType);
    return cAtFalse;
    }

static eAtRet EncapChannelBusyCheck(AtConcateGroup self, AtEncapChannel newChannel)
    {
    AtEncapChannel boundChannel = AtChannelBoundEncapChannel((AtChannel)self);

    /* Has not been set yet */
    if (boundChannel == NULL)
        return cAtOk;

    /* The same channel is going to be set */
    if (boundChannel == newChannel)
        return cAtOk;

    /* Concatenation group is being assigned to another encapsulation channel
     * and new channel is being assigned */
    if (newChannel)
        return cAtErrorChannelBusy;

    /* NULL encapsulation channel is always assignable */
    return cAtOk;
    }

static eAtModuleConcateRet EncapChannelSet(AtConcateGroup self, AtEncapChannel channel)
    {
    eAtRet ret;

    ret = EncapChannelBusyCheck(self, channel);
    if (ret != cAtOk)
        return ret;

    AtChannelBoundEncapChannelSet((AtChannel)self, channel);

    return cAtOk;
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    mThis(self)->prbsEngine = engine;
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    return mThis(self)->prbsEngine;
    }

static eBool PrbsEngineIsCreated(AtChannel self)
    {
    if (mThis(self)->prbsEngine)
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanProvision(AtConcateGroup self, AtChannel channel)
    {
    return AtChannelCanJoinVcg(channel, self);
    }

static uint32 MemberTypeNumStsGet(uint32 memberType)
    {
    switch (memberType)
        {
        case cAtConcateMemberTypeVc4_64c:   return 192;
        case cAtConcateMemberTypeVc4_16c:   return 48;
        case cAtConcateMemberTypeVc4_4c:    return 12;
        case cAtConcateMemberTypeVc4:       return 3;
        case cAtConcateMemberTypeVc3:       return 1;
        case cAtConcateMemberTypeDs3:       return 1;
        case cAtConcateMemberTypeE3:        return 1;

        default:                            return 0;
        }
    }

static uint32 Vc1xMemberNumStsGet(uint32 numMembersPerSts, uint32 numMembers)
    {
    if (numMembersPerSts == 0)
        return 0;

    return (numMembers / numMembersPerSts);
    }

static uint32 BandwidthInNumStsGet(AtConcateGroup self, uint32 numMembers)
    {
    eAtConcateMemberType memberType = AtConcateGroupMemberTypeGet(self);
    uint32 numStsPerMember = MemberTypeNumStsGet(memberType);

    if (numStsPerMember)
        return numStsPerMember * numMembers;

    if (memberType == cAtConcateMemberTypeVc12)
        return Vc1xMemberNumStsGet(21U, numMembers);

    if (memberType == cAtConcateMemberTypeVc11)
        return Vc1xMemberNumStsGet(28U, numMembers);

    return 0;
    }

static AtConcateMember DoSourceMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember newMember;
    AtModule module;

    if (self == NULL || channel == NULL)
        return NULL;

    if (!CanProvision(self, channel))
        return NULL;

    module = AtChannelModuleGet((AtChannel)self);

    AtModuleLock(module);
    newMember = mMethodsGet(self)->SourceMemberProvision(self, channel);
    AtModuleUnLock(module);

    return newMember;
    }

static eAtRet CanDeprovSourceMember(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember member = AtConcateGroupSourceMemberGetByChannel(self, channel);

    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    if (AtConcateGroupIsLcasVcg(self) && AtConcateMemberSourceIsAdded(member))
        return cAtErrorInvalidOperation;

    return cAtOk;
    }

static eAtModuleConcateRet DoSourceMemberDeProvision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret;
    AtModule module;

    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (channel == NULL)
        return cAtOk;

    ret = CanDeprovSourceMember(self, channel);
    if (ret != cAtOk)
        return ret;

    module = AtChannelModuleGet((AtChannel)self);

    AtModuleLock(module);
    ret = mMethodsGet(self)->SourceMemberDeprovision(self, channel);
    AtModuleUnLock(module);

    return ret;
    }

static AtConcateMember DoSinkMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember newMember;
    AtModule module;

    if (self == NULL || channel == NULL)
        return NULL;

    if (!CanProvision(self, channel))
        return NULL;

    module = AtChannelModuleGet((AtChannel)self);

    AtModuleLock(module);
    newMember = mMethodsGet(self)->SinkMemberProvision(self, channel);
    AtModuleUnLock(module);

    return newMember;
    }

static eAtRet CanDeprovSinkMember(AtConcateGroup self, AtChannel channel)
    {
    AtConcateMember member = AtConcateGroupSinkMemberGetByChannel(self, channel);

    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    if (AtConcateGroupIsLcasVcg(self) && AtConcateMemberSinkIsAdded(member))
        return cAtErrorInvalidOperation;

    return cAtOk;
    }

static eAtModuleConcateRet DoSinkMemberDeProvision(AtConcateGroup self, AtChannel channel)
    {
    eAtRet ret;
    AtModule module;

    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (channel == NULL)
        return cAtOk;

    ret = CanDeprovSinkMember(self, channel);
    if (ret != cAtOk)
        return ret;

    module = AtChannelModuleGet((AtChannel)self);

    AtModuleLock(module);
    ret = mMethodsGet(self)->SinkMemberDeprovision(self, channel);
    AtModuleUnLock(module);

    return ret;
    }

static eAtModuleConcateRet AllMembersDeProvision(AtConcateGroup self)
    {
    while (AtConcateGroupNumSourceMembersGet(self) != 0)
        {
        AtConcateMember soMember = AtConcateGroupSourceMemberGetByIndex(self, 0);
        AtChannel channel = AtConcateMemberSourceChannelGet(soMember);
        mMethodsGet(self)->SourceMemberDeprovision(self, channel);
        }

    while (AtConcateGroupNumSinkMembersGet(self) != 0)
        {
        AtConcateMember sinkMember = AtConcateGroupSinkMemberGetByIndex(self, 0);
        AtChannel channel = AtConcateMemberSinkChannelGet(sinkMember);
        mMethodsGet(self)->SinkMemberDeprovision(self, channel);
        }

    return cAtOk;
    }

static void StatusClear(AtChannel self)
    {
    AtConcateGroup group = (AtConcateGroup)self;
    uint32 numSink = AtConcateGroupNumSinkMembersGet(group);
    uint32 sinkId;

    for (sinkId = 0; sinkId < numSink; sinkId++)
        {
        AtChannel member = (AtChannel)AtConcateGroupSinkMemberGetByIndex(group, sinkId);
        AtChannelStatusClear(member);
        }

    m_AtChannelMethods->StatusClear(self);
    }

static void MethodsInit(AtConcateGroup self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));
        mMethodOverride(m_methods, SourceMemberProvision);
        mMethodOverride(m_methods, SinkMemberProvision);
        mMethodOverride(m_methods, SourceMemberDeprovision);
        mMethodOverride(m_methods, SinkMemberDeprovision);
        mMethodOverride(m_methods, SoMemberGetByChannel);
        mMethodOverride(m_methods, SkMemberGetByChannel);
        mMethodOverride(m_methods, SoMemberGetByIndex);
        mMethodOverride(m_methods, SkMemberGetByIndex);
        mMethodOverride(m_methods, ConcatTypeGet);
        mMethodOverride(m_methods, MemberTypeGet);
        mMethodOverride(m_methods, NumSoMembersGet);
        mMethodOverride(m_methods, NumSkMembersGet);
        mMethodOverride(m_methods, EncapChannelGet);
        mMethodOverride(m_methods, EncapTypeSet);
        mMethodOverride(m_methods, EncapTypeGet);
        mMethodOverride(m_methods, EncapTypeIsSupported);
        mMethodOverride(m_methods, MaxNumMembersGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtChannel(AtConcateGroup self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));
        
        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, EncapHwIdAllocate);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineIsCreated);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtConcateGroup self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtConcateGroup self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtConcateGroup);
    }

AtConcateGroup AtConcateGroupObjectInit(AtConcateGroup self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtChannelObjectInit((AtChannel)self, vcgId, (AtModule)concateModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Save private data */
    self->memberType  = memberType;
    self->concateType = concateType;

    return self;
    }

eBool AtConcateGroupIsNoneVcatG8040(AtConcateGroup self)
    {
    return (AtConcateGroupConcatTypeGet(self) == cAtConcateGroupTypeNVcat_g8040) ? cAtTrue : cAtFalse;
    }

eBool AtConcateGroupIsVcatVcg(AtConcateGroup self)
    {
    return (AtConcateGroupConcatTypeGet(self) == cAtConcateGroupTypeVcat) ? cAtTrue : cAtFalse;
    }

eBool AtConcateGroupIsLcasVcg(AtConcateGroup self)
    {
    if (AtConcateGroupConcatTypeGet(self) != cAtConcateGroupTypeVcat)
        return cAtFalse;

    return AtVcgLcasIsEnabled((AtVcg)self);
    }

eAtModuleConcateRet AtConcateGroupEncapChannelSet(AtConcateGroup self, AtEncapChannel channel)
    {
    if (self)
    	return EncapChannelSet(self, channel);
    return cAtErrorObjectNotExist;
    }

uint32 AtConcateGroupSourceBandwidthInNumStsGet(AtConcateGroup self)
    {
    if (self)
        return BandwidthInNumStsGet(self, AtConcateGroupNumSourceMembersGet(self));
    return 0;
    }

uint32 AtConcateGroupSinkBandwidthInNumStsGet(AtConcateGroup self)
    {
    if (self)
        return BandwidthInNumStsGet(self, AtConcateGroupNumSinkMembersGet(self));
    return 0;
    }

eAtModuleConcateRet AtConcateGroupAllMembersDeProvision(AtConcateGroup self)
    {
    if (self)
        return AllMembersDeProvision(self);
    return cAtErrorNullPointer;
    }

/**
* @addtogroup AtConcateGroup
* @{
*/

/**
 * Provision a source member
 *
 * @param self This concatenation group
 * @param channel The channel associated to the member
 *
 * @return Source member if it is succeed, otherwise, NULL.
 */
AtConcateMember AtConcateGroupSourceMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    mOneObjectWrapCall(channel, AtConcateMember, DoSourceMemberProvision(self, channel));
    }

/**
 *  Delete Provision source member of VCG
 *
 * @param self is VCG
 * @param channel is channel of member
 *
 * @return AT return code
 */
eAtModuleConcateRet AtConcateGroupSourceMemberDeProvision(AtConcateGroup self, AtChannel channel)
    {
    mOneObjectWrapCall(channel, eAtModuleConcateRet, DoSourceMemberDeProvision(self, channel));
    }

/**
 *  Get source member by the associated channel
 *
 * @param self This concatenation group
 * @param channel The channel is bound to source member
 *
 * @return Source member @ref AtConcateMember
 */
AtConcateMember AtConcateGroupSourceMemberGetByChannel(AtConcateGroup self, AtChannel channel)
    {
    mOneParamAttributeGet(SoMemberGetByChannel, channel, AtConcateMember, NULL);
    }

/**
 *  Get source member by its index in the group
 *
 * @param self This concatenation group
 * @param memberIndex Index of source member in the group
 *
 * @return Source member @ref AtConcateMember
 */
AtConcateMember AtConcateGroupSourceMemberGetByIndex(AtConcateGroup self, uint32 memberIndex)
    {
    mOneParamAttributeGet(SoMemberGetByIndex, memberIndex, AtConcateMember, NULL);
    }

/**
 *  Get numbers of provisioned source members
 *
 * @param self This concatenation group
 *
 * @return The number of provisioned source members
 */
uint32 AtConcateGroupNumSourceMembersGet(AtConcateGroup self)
    {
    mAttributeGet(NumSoMembersGet, uint32, 0);
    }

/**
 * Provision a sink member
 *
 * @param self This concatenation group
 * @param channel The channel associated to the member
 *
 * @return Sink member if it is succeed, otherwise, NULL.
 */
AtConcateMember AtConcateGroupSinkMemberProvision(AtConcateGroup self, AtChannel channel)
    {
    mOneObjectWrapCall(channel, AtConcateMember, DoSinkMemberProvision(self, channel));
    }

/**
 *  Delete Provision sink member of VCG
 *
 * @param self This concatenation group
 * @param channel is channel of member
 *
 * @return AT return code
 */
eAtModuleConcateRet AtConcateGroupSinkMemberDeProvision(AtConcateGroup self, AtChannel channel)
    {
    mOneObjectWrapCall(channel, eAtModuleConcateRet, DoSinkMemberDeProvision(self, channel));
    }

/**
 *  Get sink member by its index in the group
 *
 * @param self This concatenation group
 * @param memberIndex Index of sink member in the group
 *
 * @return Sink member @ref AtConcateMember
 */
AtConcateMember AtConcateGroupSinkMemberGetByIndex(AtConcateGroup self, uint32 memberIndex)
    {
    mOneParamAttributeGet(SkMemberGetByIndex, memberIndex, AtConcateMember, NULL);
    }

/**
 *  Get sink member by the associated channel
 *
 * @param self This concatenation group
 * @param channel The channel is bound to sink member
 *
 * @return Sink member @ref AtConcateMember
 */
AtConcateMember AtConcateGroupSinkMemberGetByChannel(AtConcateGroup self, AtChannel channel)
    {
    mOneParamAttributeGet(SkMemberGetByChannel, channel, AtConcateMember, NULL);
    }

/**
 *  Get numbers of provisioned sink members
 *
 * @param self This concatenation group
 *
 * @return The number of provisioned sink members
 */
uint32 AtConcateGroupNumSinkMembersGet(AtConcateGroup self)
    {
    mAttributeGet(NumSkMembersGet, uint32, 0);
    }

/**
 * Get member type
 *
 * @param self This concatenation group
 *
 * @return Member type @ref eAtConcateMemberType "Member types"
 */
eAtConcateMemberType AtConcateGroupMemberTypeGet(AtConcateGroup self)
    {
    mAttributeGet(MemberTypeGet, eAtConcateMemberType, cAtConcateMemberTypeUnknown);
    }

/**
 * Get concatenation type
 *
 * @param self This concatenation group
 *
 * @return Concatenation type @ref eAtConcateGroupType "Concatenation types"
 */
eAtConcateGroupType AtConcateGroupConcatTypeGet(AtConcateGroup self)
    {
    mAttributeGet(ConcatTypeGet, eAtConcateGroupType, cAtConcateGroupTypeUnknown);
    }

/**
 * Get maximum number of members can be provisioned
 *
 * @param self This concatenation group
 *
 * @return Maximum number of members can be provisioned
 */
uint32 AtConcateGroupMaxNumMembersGet(AtConcateGroup self)
    {
    mAttributeGet(MaxNumMembersGet, uint32, 0);
    }

/**
 * Get the bound Encapsulation channel
 *
 * @param self This concatenation group
 * @return The associated Encapsulation channel used to encapsulate ETH Flow
 *         traffics or NULL if the concatenation group is not bound to an
 *         Encapsulation channel.
 */
AtEncapChannel AtConcateGroupEncapChannelGet(AtConcateGroup self)
    {
    mNoParamObjectGet(EncapChannelGet, AtEncapChannel);
    }

/**
 * Check if encapsulation type is supported
 *
 * @param self This concatenation group
 * @param encapType encapsulation type
 *
 * @return cAtTrue if this encapsulation mode is supported, other will be cAtFalse.
 */
eBool AtConcateGroupEncapTypeIsSupported(AtConcateGroup self, eAtEncapType encapType)
    {
    mOneParamAttributeGet(EncapTypeIsSupported, encapType, eBool, cAtFalse);
    }

/**
 * Set encapsulation type for this concatenation group
 *
 * @param self This concatenation group
 * @param encapType encapsulation type
 *
 * @return AT return code
 */
eAtModuleConcateRet AtConcateGroupEncapTypeSet(AtConcateGroup self, eAtEncapType encapType)
    {
    mNumericalAttributeSet(EncapTypeSet, encapType);
    }

/**
 * Get encapsulation type of this concatenation group
 *
 * @param self This concatenation group
 *
 * @return @ref eAtEncapType "Encapsulation type"
 */
uint32 AtConcateGroupEncapTypeGet(AtConcateGroup self)
    {
    mAttributeGet(EncapTypeGet, uint32, cAtEncapUnknown);
    }

/**
* @}
*/

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtModuleConcateInternal.h
 * 
 * Created Date: May 7, 2014
 *
 * Description : Concate Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULECONCATEINTERNAL_H_
#define _ATMODULECONCATEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../man/AtModuleInternal.h"
#include "AtModuleConcate.h"
#include "AtConcateGroup.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtModuleConcateMethods
    {
    eBool (*GroupMemberTypeIsSupported)(AtModuleConcate self, eAtConcateMemberType memberType);
    eBool (*GroupTypeIsSupported)(AtModuleConcate self, eAtConcateGroupType concateType);
    AtConcateGroup (*ConcateGroupCreate)(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType, eAtConcateGroupType concateType);
    eAtModuleConcateRet (*ConcateGroupDelete)(AtModuleConcate self, uint32 groupId);
    AtConcateGroup (*ConcateGroupGet)(AtModuleConcate self, uint32 groupId);
    uint32 (*MaxGroupGet)(AtModuleConcate self);
    uint32 (*FreeGroupGet)(AtModuleConcate self);

    /* Internal methods */
    AtConcateGroup (*VcatVcgObjectCreate)(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType);
    AtConcateGroup (*NonVcatVcgObjectCreate)(AtModuleConcate self, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType);
    eAtModuleConcateRet (*VcatVcgObjectDelete)(AtModuleConcate self, uint32 vcgId);
    eAtModuleConcateRet (*NonVcatVcgObjectDelete)(AtModuleConcate self, uint32 vcgId);
    eBool (*CheckVcgBusyBeforeDeleting)(AtModuleConcate self);
    uint32 (*VcgBandwidthThreshold)(AtModuleConcate self, uint32 vcgId);
    eAtModuleConcateRet (*VcatVcgHwDelete)(AtModuleConcate self, uint32 vcgId);
    eAtModuleConcateRet (*NonVcatVcgHwDelete)(AtModuleConcate self, uint32 vcgId);
    eBool (*NeedAdjustVcgBandwidthOnMemberAddRemove)(AtModuleConcate self);
    eBool (*CanCreateGroup)(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType, eAtConcateMemberType memberType);

    /* Binder factory */
    AtVcgBinder (*CreateVcgBinderForVc)(AtModuleConcate self, AtSdhVc vc);
    AtVcgBinder (*CreateVcgBinderForDe1)(AtModuleConcate self, AtPdhDe1 de1);
    AtVcgBinder (*CreateVcgBinderForDe3)(AtModuleConcate self, AtPdhDe3 de3);

    /* Concatenation member factory */
    AtConcateMember (*CreateConcateMemberForSdhVc)(AtModuleConcate self, AtSdhVc vc, AtConcateGroup group);
    AtConcateMember (*CreateConcateMemberForPdhDe1)(AtModuleConcate self, AtPdhDe1 de1, AtConcateGroup group);
    AtConcateMember (*CreateConcateMemberForPdhDe3)(AtModuleConcate self, AtPdhDe3 de3, AtConcateGroup group);

    /* Some products supports a fixed binding between VCG and GFP channel where
     * an explicit binding is unnecessary. */
    eBool (*IsVcgFixedToGfpChannel)(AtModuleConcate self);

    /* Deskew Latency Threshold */
    uint32 (*DeskewLatencyResolutionInFrames)(AtModuleConcate self, eAtConcateMemberType memberType);
    eAtRet (*DeskewLatencyThresholdSet)(AtModuleConcate self, eAtConcateMemberType memberType, uint32 numUnits);
    uint32 (*DeskewLatencyThresholdGet)(AtModuleConcate self, eAtConcateMemberType memberType);
    }tAtModuleConcateMethods;

typedef struct tAtModuleConcate
    {
    tAtModule super;
    const tAtModuleConcateMethods *methods;

    /* Private data */
    AtConcateGroup *groups;
    uint32 numCreatedGroups;
    }tAtModuleConcate;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleConcate AtModuleConcateObjectInit(AtModuleConcate self, AtDevice device);

uint32 AtModuleConcateVcgBandwidthThreshold(AtModuleConcate self, uint32 vcgId);
AtGfpChannel AtModuleConcateFixAssignedGfpChannelForGroup(AtModuleConcate self, AtConcateGroup group);

uint32 AtModuleConcateNumCreatedGroups(AtModuleConcate self);
eBool AtModuleConcateNeedAdjustVcgBandwidthOnMemberAddRemove(AtModuleConcate self);

/* VCG binder factory */
AtVcgBinder AtModuleConcateCreateVcgBinderForVc(AtModuleConcate self, AtSdhVc vc);
AtVcgBinder AtModuleConcateCreateVcgBinderForDe1(AtModuleConcate self, AtPdhDe1 de1);
AtVcgBinder AtModuleConcateCreateVcgBinderForDe3(AtModuleConcate self, AtPdhDe3 de3);

/* VCG member factory */
AtConcateMember AtModuleConcateCreateConcateMemberForSdhVc(AtModuleConcate self, AtSdhVc vc, AtConcateGroup group);
AtConcateMember AtModuleConcateCreateConcateMemberForPdhDe1(AtModuleConcate self, AtPdhDe1 de1, AtConcateGroup group);
AtConcateMember AtModuleConcateCreateConcateMemberForPdhDe3(AtModuleConcate self, AtPdhDe3 de3, AtConcateGroup group);

#endif /* _ATMODULECONCATEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : AtVcg.c
 *
 * Created Date: Sep 25, 2014
 *
 * Description : VCG
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtVcgInternal.h"
#include "AtConcateMemberInternal.h"
#include "../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xFFFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtVcg)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtVcgMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;

/* Save super's implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *TypeString(AtChannel self)
    {
    if (AtVcgLcasIsEnabled((AtVcg)self))
        return "lcas";
    return m_AtChannelMethods->TypeString(self);
    }

static eBool LcasIsSupported(AtVcg self)
	{
	AtUnused(self);
	return cAtFalse;
	}

static eAtModuleConcateRet LcasEnable(AtVcg self, eBool enable)
    {
    AtConcateGroup group = (AtConcateGroup)self;
    AtConcateMember member;
    uint32 numMembers;
    uint32 memberIdx;

    if (enable)
        return cAtOk;

    /* Clear all adding state when disabling LCAS. */
    numMembers = AtConcateGroupNumSinkMembersGet(group);
    for (memberIdx = 0; memberIdx < numMembers; memberIdx++)
        {
        member = AtConcateGroupSinkMemberGetByIndex(group, memberIdx);
        AtConcateMemberSinkRemove(member);
        }

    numMembers = AtConcateGroupNumSourceMembersGet(group);
    for (memberIdx = 0; memberIdx < numMembers; memberIdx++)
        {
        member = AtConcateGroupSourceMemberGetByIndex(group, memberIdx);
        AtConcateMemberSourceRemove(member);
        }

    return cAtOk;
    }

static eBool LcasIsEnabled(AtVcg self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleConcateRet HoldOffTimerSet(AtVcg self, uint32 timerInMs)
    {
    AtUnused(self);
    AtUnused(timerInMs);
    return cAtErrorNotImplemented;
    }

static uint32 HoldOffTimerGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleConcateRet WtrTimerSet(AtVcg self, uint32 timerInMs)
    {
    AtUnused(self);
    AtUnused(timerInMs);
    return cAtErrorNotImplemented;
    }

static uint32 WtrTimerGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RmvTimerGet(AtVcg self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleConcateRet RmvTimerSet(AtVcg self, uint32 timerInMs)
    {
    AtUnused(self);
    AtUnused(timerInMs);
    return cAtErrorNotImplemented;
    }

static eAtModuleConcateRet SinkMemberAdd(AtVcg self, AtConcateMember member)
    {
    if (!AtVcgCanAddSinkMember(self, member))
        return cAtErrorChannelBusy;

    AtConcateMemberSinkAdd(member);
    return cAtOk;
    }

static eAtModuleConcateRet SinkMemberRemove(AtVcg self, AtConcateMember member)
    {
    if (!AtVcgCanRemoveMember(self, member))
        return cAtErrorChannelBusy;
    AtConcateMemberSinkRemove(member);
    return cAtOk;
    }

static eAtModuleConcateRet SourceMemberAdd(AtVcg self, AtConcateMember member)
    {
    if (!AtVcgCanAddSourceMember(self, member))
        return cAtErrorChannelBusy;

    AtConcateMemberSourceAdd(member);
    return cAtOk;
    }

static eAtModuleConcateRet SourceMemberRemove(AtVcg self, AtConcateMember member)
    {
    if (!AtVcgCanRemoveMember(self, member))
        return cAtErrorChannelBusy;
    AtConcateMemberSourceRemove(member);
    return cAtOk;
    }

static uint8 SourceRsAckGet(AtVcg self)
    {
    /* Sub class must know */
    AtUnused(self);
    return 0;
    }

static uint8 SinkRsAckGet(AtVcg self)
    {
    /* Sub class must know */
    AtUnused(self);
    return 0;
    }

static uint8 SinkMstGet(AtVcg self, AtConcateMember member)
    {
    AtUnused(self);
    AtUnused(member);
    return 0;
    }

static uint8 SourceMstGet(AtVcg self, AtConcateMember member)
    {
    AtUnused(self);
    AtUnused(member);
    return 0;
    }

static eAtRet DoSinkMemberAdd(AtVcg self, AtConcateMember member)
    {
    if (self == NULL || member == NULL)
        return cAtErrorObjectNotExist;

    if (!AtVcgLcasIsEnabled(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->SinkMemberAdd(self, member);
    }

static eAtRet DoSourceMemberAdd(AtVcg self, AtConcateMember member)
    {
    if (self == NULL || member == NULL)
        return cAtErrorObjectNotExist;

    if (!AtVcgLcasIsEnabled(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->SourceMemberAdd(self, member);
    }

static eAtRet DoSinkMemberRemove(AtVcg self, AtConcateMember member)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (member == NULL)
        return cAtOk;

    if (!AtVcgLcasIsEnabled(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->SinkMemberRemove(self, member);
    }

static eAtRet DoSourceMemberRemove(AtVcg self, AtConcateMember member)
    {
    if (self == NULL)
        return cAtErrorObjectNotExist;

    if (member == NULL)
        return cAtOk;

    if (!AtVcgLcasIsEnabled(self))
        return cAtErrorModeNotSupport;

    return mMethodsGet(self)->SourceMemberRemove(self, member);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtVcg);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);
    }

static void OverrideAtObject(AtVcg self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtVcg self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TypeString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void MethodsInit(AtVcg self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, LcasIsSupported);
        mMethodOverride(m_methods, LcasEnable);
        mMethodOverride(m_methods, LcasIsEnabled);
        mMethodOverride(m_methods, HoldOffTimerSet);
        mMethodOverride(m_methods, HoldOffTimerGet);
        mMethodOverride(m_methods, WtrTimerSet);
        mMethodOverride(m_methods, WtrTimerGet);
        mMethodOverride(m_methods, RmvTimerSet);
        mMethodOverride(m_methods, RmvTimerGet);
        mMethodOverride(m_methods, SourceRsAckGet);
        mMethodOverride(m_methods, SinkRsAckGet);
        mMethodOverride(m_methods, SinkMemberAdd);
        mMethodOverride(m_methods, SinkMemberRemove);
        mMethodOverride(m_methods, SourceMemberAdd);
        mMethodOverride(m_methods, SourceMemberRemove);
        mMethodOverride(m_methods, SinkMstGet);
        mMethodOverride(m_methods, SourceMstGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtVcg self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    }

AtVcg AtVcgObjectInit(AtVcg self, AtModuleConcate concateModule, uint32 vcatId, eAtConcateMemberType memberType)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtConcateGroupObjectInit((AtConcateGroup)self, concateModule, vcatId, memberType, cAtConcateGroupTypeVcat) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

eBool AtVcgCanAddSinkMember(AtVcg self, AtConcateMember member)
    {
    if ((self == NULL) || (member == NULL))
        return cAtFalse;

    if (AtConcateMemberSinkIsAdded(member))
        return cAtFalse;

    if (AtConcateMemberConcateGroupGet(member) != (AtConcateGroup)self)
        return cAtFalse;

    return cAtTrue;
    }

eBool AtVcgCanAddSourceMember(AtVcg self, AtConcateMember member)
    {
    if ((self == NULL) || (member == NULL))
        return cAtFalse;

    if (AtConcateMemberSourceIsAdded(member))
        return cAtFalse;

    if (AtConcateMemberConcateGroupGet(member) != (AtConcateGroup)self)
        return cAtFalse;

    return cAtTrue;
    }

eBool AtVcgCanRemoveMember(AtVcg self, AtConcateMember member)
    {
    if ((self == NULL) || (member == NULL))
        return cAtFalse;

    if (AtConcateMemberConcateGroupGet(member) != (AtConcateGroup)self)
        return cAtFalse;

    return cAtTrue;
    }

uint8 AtVcgSinkMstGet(AtVcg self, AtConcateMember member)
    {
    if (self)
        return mMethodsGet(self)->SinkMstGet(self, member);
    return 0;
    }

uint8 AtVcgSourceMstGet(AtVcg self, AtConcateMember member)
    {
    if (self)
        return mMethodsGet(self)->SourceMstGet(self, member);
    return 0;
    }

/**
* @addtogroup AtVcg
* @{
*/

/**
 * Check if LCAS is supported or not
 *
 * @param self This VCG
 *
 * @return cAtTrue if LCAS is supported.
 * @return cAtFalse if LCAS is not supported.
 */
eBool AtVcgLcasIsSupported(AtVcg self)
    {
    mAttributeGet(LcasIsSupported, eBool, cAtFalse);
    }

/**
 * To enable/disable LCAS
 *
 * @param self This VCG
 * @param enable Enable/disable LCAS
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgLcasEnable(AtVcg self, eBool enable)
    {
    mNumericalAttributeSet(LcasEnable, enable);
    }

/**
 * To get the LCAS enabling state
 *
 * @param self This VCG
 *
 * @return cAtTrue if LCAS is enabled
 * @return cAtFalse if LCAS is disabled
 */
eBool AtVcgLcasIsEnabled(AtVcg self)
    {
    mAttributeGet(LcasIsEnabled, eBool, cAtFalse);
    }

/**
 * To set the hold-off timer
 *
 * @param self This VCG
 * @param timerInMs Hold-off timer in milliseconds
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgHoldOffTimerSet(AtVcg self, uint32 timerInMs)
    {
    mNumericalAttributeSet(HoldOffTimerSet, timerInMs);
    }

/**
 * To get hold-off timer
 *
 * @param self This VCG
 *
 * @return Hold-off timer in milliseconds
 */
uint32 AtVcgHoldOffTimerGet(AtVcg self)
    {
    mAttributeGet(HoldOffTimerGet, uint32, 0);
    }

/**
 * To set wait-to-restore timer
 *
 * @param self This VCG
 * @param timerInMs Wait-to-restore timer in milliseconds
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgWtrTimerSet(AtVcg self, uint32 timerInMs)
    {
    mNumericalAttributeSet(WtrTimerSet, timerInMs);
    }

/**
 * To get wait-to-restore timer
 *
 * @param self This VCG
 *
 * @return Wait-to-restore timer in milliseconds
 */
uint32 AtVcgWtrTimerGet(AtVcg self)
    {
    mAttributeGet(WtrTimerGet, uint32, 0);
    }

/**
 * To set remove timer
 *
 * @param self This VCG
 * @param timerInMs Remove timer in milliseconds
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgRmvTimerSet(AtVcg self, uint32 timerInMs)
    {
    mNumericalAttributeSet(RmvTimerSet, timerInMs);
    }

/**
 * To get remove timer
 *
 * @param self This VCG
 *
 * @return Remove timer in milliseconds
 */
uint32 AtVcgRmvTimerGet(AtVcg self)
    {
    mAttributeGet(RmvTimerGet, uint32, 0);
    }

/**
 * To get source RS-ACK
 *
 * @param self This VCG
 *
 * @return Source RS-ACK
 */
uint8 AtVcgSourceRsAckGet(AtVcg self)
    {
    mAttributeGetIfCondition(SourceRsAckGet, uint8, 0, AtVcgLcasIsEnabled(self));
    }

/**
 * To get Sink RS-ACK
 *
 * @param self This VCG
 *
 * @return Sink RS-ACK
 */
uint8 AtVcgSinkRsAckGet(AtVcg self)
    {
    mAttributeGetIfCondition(SinkRsAckGet, uint8, 0, AtVcgLcasIsEnabled(self));
    }

/**
 * To add a sink member
 *
 * @param self This VCG
 * @param member The sink member to be added
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgSinkMemberAdd(AtVcg self, AtConcateMember member)
    {
    mOneObjectWrapCall(member, eAtModuleConcateRet, DoSinkMemberAdd(self, member));
    }

/**
 * To remove a sink member
 *
 * @param self This VCG
 * @param member The sink member to be removed
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgSinkMemberRemove(AtVcg self, AtConcateMember member)
    {
    mOneObjectWrapCall(member, eAtModuleConcateRet, DoSinkMemberRemove(self, member));
    }

/**
 * To add a source member
 *
 * @param self This VCG
 * @param member The source member to be added
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgSourceMemberAdd(AtVcg self, AtConcateMember member)
    {
    mOneObjectWrapCall(member, eAtModuleConcateRet, DoSourceMemberAdd(self, member));
    }

/**
 * To remove a source member
 *
 * @param self This VCG
 * @param member The source member to be removed
 *
 * @return AT return codes
 */
eAtModuleConcateRet AtVcgSourceMemberRemove(AtVcg self, AtConcateMember member)
    {
    mOneObjectWrapCall(member, eAtModuleConcateRet, DoSourceMemberRemove(self, member));
    }

/**
 *@}
 */

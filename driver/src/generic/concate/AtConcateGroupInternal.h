/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtConcateGroupInternal.h
 * 
 * Created Date: Sep 25, 2014
 *
 * Description : Concate Group VCG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONCATEGROUPINTERNAL_H_
#define _ATCONCATEGROUPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleConcateInternal.h"
#include "AtConcateGroup.h"
#include "AtPrbsEngine.h"
#include "../common/AtChannelInternal.h"
#include "../../util/AtListInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtConcateGroupMethods
    {
    AtConcateMember (*SourceMemberProvision)(AtConcateGroup self, AtChannel channel);
    eAtModuleConcateRet (*SourceMemberDeprovision)(AtConcateGroup self, AtChannel channel);
    AtConcateMember (*SoMemberGetByChannel)(AtConcateGroup self, AtChannel channel);
    AtConcateMember (*SoMemberGetByIndex)(AtConcateGroup self, uint32 memberIndex);
    uint32 (*NumSoMembersGet)(AtConcateGroup self);

    AtConcateMember (*SinkMemberProvision)(AtConcateGroup self, AtChannel channel);
    eAtModuleConcateRet (*SinkMemberDeprovision)(AtConcateGroup self, AtChannel channel);
    AtConcateMember (*SkMemberGetByChannel)(AtConcateGroup self, AtChannel channel);
    AtConcateMember (*SkMemberGetByIndex)(AtConcateGroup self, uint32 memberIndex);
    uint32 (*NumSkMembersGet)(AtConcateGroup self);

    uint32 (*MaxNumMembersGet)(AtConcateGroup self);
    eAtConcateGroupType (*ConcatTypeGet)(AtConcateGroup self);
    eAtConcateMemberType (*MemberTypeGet)(AtConcateGroup self);
    
    /* Encapsulation */
    AtEncapChannel (*EncapChannelGet)(AtConcateGroup self);
    eAtModuleConcateRet (*EncapTypeSet)(AtConcateGroup self, eAtEncapType encapType);
    uint32 (*EncapTypeGet)(AtConcateGroup self);
    eBool (*EncapTypeIsSupported)(AtConcateGroup self, eAtEncapType encapType);

    }tAtConcateGroupMethods;

typedef struct tAtConcateGroup
    {
    tAtChannel super;
    const tAtConcateGroupMethods *methods;

    /* Private Data */
    uint8 concateType;
    uint8 memberType; /* Value declared in eAtConcateMember */
    AtList soMemberList;
    AtList skMemberList;
    AtPrbsEngine prbsEngine;
    }tAtConcateGroup;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConcateGroup AtConcateGroupObjectInit(AtConcateGroup self, AtModuleConcate concateModule, uint32 vcgId, eAtConcateMemberType memberType, eAtConcateGroupType concateType);

eBool AtConcateGroupIsNoneVcatG8040(AtConcateGroup self);
eBool AtConcateGroupIsLcasVcg(AtConcateGroup self);
eBool AtConcateGroupIsVcatVcg(AtConcateGroup self);

/* To set Encap channel */
eAtModuleConcateRet AtConcateGroupEncapChannelSet(AtConcateGroup self, AtEncapChannel channel);

/* Bandwidth control */
uint32 AtConcateGroupSourceBandwidthInNumStsGet(AtConcateGroup self);
uint32 AtConcateGroupSinkBandwidthInNumStsGet(AtConcateGroup self);

/* Utility */
eAtModuleConcateRet AtConcateGroupAllMembersDeProvision(AtConcateGroup self);

#endif /* _ATCONCATEGROUPINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : AtModuleConcate.c
 *
 * Created Date: May 7, 2014
 *
 * Description : CCAT/VCAT/LCAS module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"
#include "AtCommon.h"
#include "AtConcateMember.h"
#include "AtVcg.h"
#include "AtModuleConcateInternal.h"
#include "../common/AtChannelInternal.h"
#include "../../util/coder/AtCoderUtil.h"
#include "AtConcateGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cInvalidValue 0xFF
#define mThis(self) ((AtModuleConcate)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtModuleConcateMethods m_methods;

/* Override */
static tAtModuleMethods m_AtModuleOverride;
static tAtObjectMethods m_AtObjectOverride;

/* To save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtConcateGroup ConcateGroupObjectCreate(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    if (concateType == cAtConcateGroupTypeVcat)
        return mMethodsGet(self)->VcatVcgObjectCreate(self, groupId, memberType);
    return mMethodsGet(self)->NonVcatVcgObjectCreate(self, groupId, memberType, concateType);
    }

static eAtModuleConcateRet VcgObjectDelete(AtModuleConcate self, uint32 vcgId)
    {
    AtConcateGroup vcg = AtModuleConcateGroupGet(self, vcgId);

    if (AtConcateGroupConcatTypeGet(vcg) == cAtConcateGroupTypeVcat)
        return mMethodsGet(self)->VcatVcgHwDelete(self, vcgId);
    return mMethodsGet(self)->NonVcatVcgHwDelete(self, vcgId);
    }

static eBool GroupIdIdIsValid(AtModuleConcate self, uint32 groupId)
    {
    return (groupId < AtModuleConcateMaxGroupGet(self));
    }

static eBool GroupTypeIsSupported(AtModuleConcate self, eAtConcateGroupType concateType)
    {
    /* Concrete must know */
    AtUnused(self);
    AtUnused(concateType);
    return cAtFalse;
    }

static eBool GroupMemberTypeIsSupported(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    /* Concrete must know */
    AtUnused(self);
    AtUnused(memberType);
    return cAtFalse;
    }

static eBool CheckVcgBusyBeforeDeleting(AtModuleConcate self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleConcateRet AllMembersRemove(AtVcg vcg)
    {
    uint32 member_i;
    eAtModuleConcateRet retCode = cAtOk;
    AtConcateGroup concateGroup = (AtConcateGroup)vcg;

    /* Remove member at source */
    for (member_i = 0; member_i < AtConcateGroupNumSourceMembersGet(concateGroup);)
        {
        AtConcateMember member = AtConcateGroupSourceMemberGetByIndex(concateGroup, member_i);
        if (member)
             retCode |= AtVcgSourceMemberRemove(vcg, member);
        }

    /* Remove member at sink */
    for (member_i = 0; member_i < AtConcateGroupNumSinkMembersGet(concateGroup);)
        {
        AtConcateMember member = AtConcateGroupSinkMemberGetByIndex(concateGroup, member_i);
        if (member)
             retCode |= AtVcgSinkMemberRemove(vcg, member);
        }

    return retCode;
    }

static eAtModuleConcateRet AllMembersDeprovision(AtVcg vcg)
    {
    uint32 member_i;
    eAtModuleConcateRet retCode = cAtOk;
    AtConcateGroup group = (AtConcateGroup)vcg;

    /* Remove member at source */
    for (member_i = 0; member_i < AtConcateGroupNumSourceMembersGet(group); member_i++)
        {
        AtConcateMember member = AtConcateGroupSourceMemberGetByIndex(group, member_i);
        if (member)
            retCode |= AtConcateGroupSourceMemberDeProvision(group, AtConcateMemberSourceChannelGet(member));
        }

    /* Remove member at sink */
    for (member_i = 0; member_i < AtConcateGroupNumSinkMembersGet(group); member_i++)
        {
        AtConcateMember member = AtConcateGroupSinkMemberGetByIndex(group, member_i);
        if (member)
            retCode |= AtConcateGroupSinkMemberDeProvision(group, AtConcateMemberSinkChannelGet(member));
        }

    return retCode;
    }

static eAtModuleConcateRet VcatVcgHwDelete(AtModuleConcate self, uint32 vcgId)
    {
    AtVcg vcg = (AtVcg)AtModuleConcateGroupGet(self, vcgId);
    eAtModuleConcateRet ret = cAtOk;

    if (AtConcateGroupIsLcasVcg((AtConcateGroup)vcg))
        ret |= AllMembersRemove(vcg);

    ret |= AllMembersDeprovision(vcg);

    return ret;
    }

static eAtModuleConcateRet NonVcatVcgHwDelete(AtModuleConcate self, uint32 vcgId)
    {
    return AllMembersDeprovision((AtVcg)AtModuleConcateGroupGet(self, vcgId));
    }

static uint32 VcgBandwidthThreshold(AtModuleConcate self, uint32 vcgId)
    {
    AtUnused(self);
    AtUnused(vcgId);
    return 0;
    }

static AtConcateGroup ConcateGroupCreate(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    AtConcateGroup newGroups;

    if (!GroupIdIdIsValid(self, groupId))
        return NULL;

    /* Already created */
    if (AtModuleConcateGroupGet(self, groupId))
        return NULL;

    if (!AtModuleConcateGroupTypeIsSupported(self, concateType) ||
        !AtModuleConcateMemberTypeIsSupported(self, memberType))
        return NULL;

    if (!mMethodsGet(self)->CanCreateGroup(self, groupId, concateType, memberType))
        return NULL;

    /* Create new VCG and initialize it */
    newGroups = ConcateGroupObjectCreate(self, groupId, memberType, concateType);
    if (newGroups)
        {
        AtChannelInit((AtChannel)newGroups);

        self->groups[groupId] = newGroups;
        self->numCreatedGroups = self->numCreatedGroups + 1;
        }

    return newGroups;
    }

static eAtModuleConcateRet ConcateGroupDeleteBusyCheck(AtModuleConcate self, AtConcateGroup group)
    {
    if (!mMethodsGet(self)->CheckVcgBusyBeforeDeleting(self))
        return cAtOk;

    if ((AtConcateGroupNumSinkMembersGet(group)   > 0) ||
        (AtConcateGroupNumSourceMembersGet(group) > 0))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

static eAtModuleConcateRet ConcateGroupHardwareDelete(AtModuleConcate self, uint32 groupId)
    {
    AtConcateGroup group = AtModuleConcateGroupGet(self, groupId);

    if (AtConcateGroupConcatTypeGet(group) == cAtConcateGroupTypeVcat)
        return mMethodsGet(self)->VcatVcgHwDelete(self, groupId);

    return mMethodsGet(self)->NonVcatVcgHwDelete(self, groupId);
    }

static eAtModuleConcateRet ConcateGroupDatabaseDelete(AtModuleConcate self, uint32 groupId)
    {
    eAtModuleConcateRet ret = cAtOk;

    ret = VcgObjectDelete(self, groupId);
    if (ret != cAtOk)
        return ret;

    AtObjectDelete((AtObject)self->groups[groupId]);
    self->groups[groupId] = NULL;
    self->numCreatedGroups = self->numCreatedGroups - 1;

    return cAtOk;
    }

static eAtModuleConcateRet ConcateGroupDelete(AtModuleConcate self, uint32 groupId)
    {
    AtConcateGroup group = AtModuleConcateGroupGet(self, groupId);
    eAtModuleConcateRet ret = cAtOk;

    if (!GroupIdIdIsValid(self, groupId))
        return cAtErrorOutOfRangParm;

    if (group == NULL)
        return cAtOk;

    /* Have Service Encap Binding, then can not delete VCG,
     * User should unbind Encap first be fore delete VCG */
    if (AtChannelServiceIsRunning((AtChannel)group))
        return cAtErrorChannelBusy;

    ret = ConcateGroupDeleteBusyCheck(self, group);
    if (ret != cAtOk)
        return ret;

    ret |= AtConcateGroupAllMembersDeProvision(group);
    ret |= ConcateGroupHardwareDelete(self, groupId);
    if (ret != cAtOk)
        return ret;

    return ConcateGroupDatabaseDelete(self, groupId);
    }

static AtConcateGroup ConcateGroupGet(AtModuleConcate self, uint32 groupId)
    {
    return GroupIdIdIsValid(self, groupId) ? self->groups[groupId] : NULL;
    }

static uint32 MaxGroupGet(AtModuleConcate self)
    {
    /* Let sub class do this */
    AtUnused(self);
    return 0;
    }

static uint32 FreeGroupGet(AtModuleConcate self)
    {
    uint32 vcg_i;

    for (vcg_i = 0; vcg_i < AtModuleConcateMaxGroupGet(self); vcg_i++)
        {
        if (AtModuleConcateGroupGet(self, vcg_i) == NULL)
            return vcg_i;
        }

    return AtModuleConcateMaxGroupGet(self);
    }

static const char *TypeString(AtModule self)
    {
    AtUnused(self);
    return "concate";
    }

static const char *CapacityDescription(AtModule self)
    {
    static char string[32];

    AtSprintf(string, "vcgs: %u", AtModuleConcateMaxGroupGet((AtModuleConcate)self));
    return string;
    }

static void GroupsDelete(AtModuleConcate self)
    {
    AtOsal osal;
    uint32 group_i, maxGroups;

    if (self->groups == NULL)
        return;

    osal = AtSharedDriverOsalGet();
    maxGroups = AtModuleConcateMaxGroupGet(self);
    for (group_i = 0; group_i < maxGroups; group_i++)
        {
        if (self->groups[group_i])
            AtObjectDelete((AtObject)self->groups[group_i]);
        }

    mMethodsGet(osal)->MemFree(osal, self->groups);
    self->groups = NULL;
    }

static eAtRet VcgsSetup(AtModule self)
    {
    AtModuleConcate concateModule = (AtModuleConcate)self;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 maxNumGroups, size;
    AtConcateGroup *groups;

    /* Delete all VCGs if they were created */
    if (concateModule->groups)
        GroupsDelete(concateModule);

    /* Create memory to hold all of encapsulation channels */
    maxNumGroups = AtModuleConcateMaxGroupGet(concateModule);
    if (maxNumGroups)
        {
        size = (uint32)(sizeof(AtConcateGroup) * maxNumGroups);
        groups = mMethodsGet(osal)->MemAlloc(osal, size);
        if (groups == NULL)
            return cAtErrorRsrcNoAvail;
        mMethodsGet(osal)->MemInit(osal, groups, 0, size);

        /* Save this */
        concateModule->groups = groups;
        }

    concateModule->numCreatedGroups = 0;
    return cAtOk;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;

    /* Super setup */
    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    return VcgsSetup(self);
    }

static void Delete(AtObject self)
    {
    GroupsDelete((AtModuleConcate)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtModuleConcate object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(groups, AtModuleConcateMaxGroupGet(object));
    mEncodeUInt(numCreatedGroups);
    }

static AtConcateGroup VcatVcgObjectCreate(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType)
    {
    /* Sub class must know */
    AtUnused(self);
    AtUnused(groupId);
    AtUnused(memberType);
    return NULL;
    }

static AtConcateGroup NonVcatVcgObjectCreate(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    /* Sub class must know */
    AtUnused(self);
    AtUnused(groupId);
    AtUnused(memberType);
    AtUnused(concateType);
    return NULL;
    }

static AtGfpChannel FixAssignedGfpChannelForGroup(AtModuleConcate self, AtConcateGroup group)
    {
    uint16 vcgId = (uint16)AtChannelIdGet((AtChannel)group);
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtGfpChannel gfp = (AtGfpChannel)AtModuleEncapChannelGet(encapModule, vcgId);

    if (gfp == NULL)
        gfp = AtModuleEncapGfpChannelCreate(encapModule, vcgId);

    return gfp;
    }

static eBool IsVcgFixedToGfpChannel(AtModuleConcate self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedAdjustVcgBandwidthOnMemberAddRemove(AtModuleConcate self)
    {
    /* Let concrete determine */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DeskewLatencyResolutionInFrames(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    AtUnused(memberType);
    return 0;
    }

static eAtRet DeskewLatencyThresholdSet(AtModuleConcate self, eAtConcateMemberType memberType, uint32 thresholdInRes)
    {
    AtUnused(self);
    AtUnused(memberType);
    AtUnused(thresholdInRes);
    return cAtErrorModeNotSupport;
    }

static uint32 DeskewLatencyThresholdGet(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    AtUnused(memberType);
    return 0;
    }

static AtConcateMember CreateConcateMemberForSdhVc(AtModuleConcate self, AtSdhVc vc, AtConcateGroup group)
    {
    AtUnused(self);
    AtUnused(vc);
    AtUnused(group);
    return NULL;
    }

static AtConcateMember CreateConcateMemberForPdhDe1(AtModuleConcate self, AtPdhDe1 de1, AtConcateGroup group)
    {
    AtUnused(self);
    AtUnused(de1);
    AtUnused(group);
    return NULL;
    }

static AtConcateMember CreateConcateMemberForPdhDe3(AtModuleConcate self, AtPdhDe3 de3, AtConcateGroup group)
    {
    AtUnused(self);
    AtUnused(de3);
    AtUnused(group);
    return NULL;
    }

static eBool CanCreateGroup(AtModuleConcate self, uint32 vcgId, eAtConcateGroupType concateType, eAtConcateMemberType memberType)
    {
    AtUnused(self);
    AtUnused(vcgId);
    AtUnused(concateType);
    AtUnused(memberType);
    return cAtTrue;
    }

static AtConcateGroup DoConcateGroupCreate(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    AtConcateGroup newGroup;

    if (self == NULL)
        return NULL;

    AtModuleLock((AtModule)self);
    newGroup = mMethodsGet(self)->ConcateGroupCreate(self, groupId, memberType, concateType);
    AtModuleUnLock((AtModule)self);

    return newGroup;
    }

static eAtRet DoConcateGroupDelete(AtModuleConcate self, uint32 groupId)
    {
    eAtRet ret;

    if (self == NULL)
        return cAtErrorObjectNotExist;

    AtModuleLock((AtModule)self);
    ret = mMethodsGet(self)->ConcateGroupDelete(self, groupId);
    AtModuleUnLock((AtModule)self);

    return ret;
    }

static void StatusClear(AtModule self)
    {
    AtModuleConcate module = (AtModuleConcate)self;
    uint32 maxGroup = mMethodsGet(module)->MaxGroupGet(module);
    uint32 groupId;

    for (groupId = 0; groupId < maxGroup; groupId++)
        {
        AtChannel group = (AtChannel)AtModuleConcateGroupGet(module, groupId);
        if (group == NULL)
            continue;

        AtChannelStatusClear(group);
        }

    m_AtModuleMethods->StatusClear(self);
    }

static void MethodsInit(AtModuleConcate self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, GroupMemberTypeIsSupported);
        mMethodOverride(m_methods, GroupTypeIsSupported);
        mMethodOverride(m_methods, ConcateGroupCreate);
        mMethodOverride(m_methods, ConcateGroupDelete);
        mMethodOverride(m_methods, ConcateGroupGet);
        mMethodOverride(m_methods, MaxGroupGet);
        mMethodOverride(m_methods, FreeGroupGet);
        mMethodOverride(m_methods, VcatVcgObjectCreate);
        mMethodOverride(m_methods, NonVcatVcgObjectCreate);
        mMethodOverride(m_methods, CheckVcgBusyBeforeDeleting);
        mMethodOverride(m_methods, VcgBandwidthThreshold);
        mMethodOverride(m_methods, VcatVcgHwDelete);
        mMethodOverride(m_methods, NonVcatVcgHwDelete);
        mMethodOverride(m_methods, NeedAdjustVcgBandwidthOnMemberAddRemove);
        mMethodOverride(m_methods, IsVcgFixedToGfpChannel);
        mMethodOverride(m_methods, DeskewLatencyResolutionInFrames);
        mMethodOverride(m_methods, DeskewLatencyThresholdSet);
        mMethodOverride(m_methods, DeskewLatencyThresholdGet);
        mMethodOverride(m_methods, CreateConcateMemberForSdhVc);
        mMethodOverride(m_methods, CreateConcateMemberForPdhDe1);
        mMethodOverride(m_methods, CreateConcateMemberForPdhDe3);
        mMethodOverride(m_methods, CanCreateGroup);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModuleConcate self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, TypeString);
        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, StatusClear);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleConcate self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleConcate self)
    {
    OverrideAtModule(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtModuleConcate);
    }

AtModuleConcate AtModuleConcateObjectInit(AtModuleConcate self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtModuleObjectInit((AtModule)self, cAtModuleConcate, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

uint32 AtModuleConcateVcgBandwidthThreshold(AtModuleConcate self, uint32 vcgId)
    {
    if (self)
        return mMethodsGet(self)->VcgBandwidthThreshold(self, vcgId);
    return 0;
    }

AtGfpChannel AtModuleConcateFixAssignedGfpChannelForGroup(AtModuleConcate self, AtConcateGroup group)
    {
    if (self == NULL)
        return NULL;

    if (!mMethodsGet(self)->IsVcgFixedToGfpChannel(self))
        return NULL;

    return FixAssignedGfpChannelForGroup(self, group);
    }

uint32 AtModuleConcateNumCreatedGroups(AtModuleConcate self)
    {
    return self ? self->numCreatedGroups : 0;
    }

eBool AtModuleConcateNeedAdjustVcgBandwidthOnMemberAddRemove(AtModuleConcate self)
    {
    if (self)
        return mMethodsGet(self)->NeedAdjustVcgBandwidthOnMemberAddRemove(self);
    return cAtFalse;
    }

/*
 * Create VcgBinder for VC channel type
 *
 * @param self This module
 * @param vc @ref AtSdhVc channel
 *
 * @retval AtVcgBinder
 */
AtVcgBinder AtModuleConcateCreateVcgBinderForVc(AtModuleConcate self, AtSdhVc vc)
    {
    if (self)
        return mMethodsGet(self)->CreateVcgBinderForVc(self, vc);
    return NULL;
    }

/*
 * Create VcgBinder for De1 channel type
 *
 * @param self This module
 * @param vc @ref AtPdhDe1 channel
 *
 * @retval AtVcgBinder
 */
AtVcgBinder AtModuleConcateCreateVcgBinderForDe1(AtModuleConcate self, AtPdhDe1 de1)
    {
    if (self)
        return mMethodsGet(self)->CreateVcgBinderForDe1(self, de1);
    return NULL;
    }

/*
 * Create VcgBinder for De3 channel type
 *
 * @param self This module
 * @param vc @ref AtPdhDe3 channel
 *
 * @retval AtVcgBinder
 */
AtVcgBinder AtModuleConcateCreateVcgBinderForDe3(AtModuleConcate self, AtPdhDe3 de3)
    {
    if (self)
        return mMethodsGet(self)->CreateVcgBinderForDe3(self, de3);
    return NULL;
    }

AtConcateMember AtModuleConcateCreateConcateMemberForSdhVc(AtModuleConcate self, AtSdhVc vc, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->CreateConcateMemberForSdhVc(self, vc, group);
    return NULL;
    }

AtConcateMember AtModuleConcateCreateConcateMemberForPdhDe1(AtModuleConcate self, AtPdhDe1 de1, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->CreateConcateMemberForPdhDe1(self, de1, group);
    return NULL;
    }

AtConcateMember AtModuleConcateCreateConcateMemberForPdhDe3(AtModuleConcate self, AtPdhDe3 de3, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->CreateConcateMemberForPdhDe3(self, de3, group);
    return NULL;
    }

/**
 * @addtogroup AtModuleConcate
 * @{
 */

/**
 * To check if concatenation type is supported
 *
 * @param self This module
 * @param concateType @ref eAtConcateGroupType "Concatenation types"
 *
 * @retval cAtTrue if the input concatenation mode is supported
 * @retval cAtFalse if the input concatenation mode is not supported
 */
eBool AtModuleConcateGroupTypeIsSupported(AtModuleConcate self, eAtConcateGroupType concateType)
    {
    mOneParamAttributeGet(GroupTypeIsSupported, concateType, eBool, cAtFalse);
    }

/**
 * Check if a member type is supported
 *
 * @param self This module
 * @param memberType @ref eAtConcateMemberType "VCG member types"
 *
 * @retval cAtTrue if input member is supported
 * @retval cAtFalse if input member is not supported
 */
eBool AtModuleConcateMemberTypeIsSupported(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    mOneParamAttributeGet(GroupMemberTypeIsSupported, memberType, eBool, cAtFalse);
    }

/**
 * Create a Concate Group
 *
 * @param self This module
 * @param groupId Concate Group ID
 * @param memberType @ref eAtConcateMemberType "VCG member types"
 * @param concateType @ref eAtConcateGroupType "VCG concatenation types"
 *
 * @return AtConcateGroup object if success or NULL if failure
 */
AtConcateGroup AtModuleConcateGroupCreate(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType, eAtConcateGroupType concateType)
    {
    AtConcateGroup newGroup;

    mThreeParamsApiLogStart(groupId, memberType, concateType);
    newGroup = DoConcateGroupCreate(self, groupId, memberType, concateType);
    AtDriverApiLogStop();

    return newGroup;
    }

/**
 *  Delete Concate Group
 *
 * @param self this module
 * @param groupId is Id of Concate Group
 *
 * @return AT return code
 */
eAtModuleConcateRet AtModuleConcateGroupDelete(AtModuleConcate self, uint32 groupId)
    {
    eAtRet ret;

    mOneParamApiLogStart(groupId);
    ret = DoConcateGroupDelete(self, groupId);
    AtDriverApiLogStop();

    return ret;
    }

/**
 *  Get Concate Group from Id of Concate Group
 *
 * @param self this module
 * @param groupId is Id of Concate Group
 *
 * @return AtConcateGroup
 */
AtConcateGroup AtModuleConcateGroupGet(AtModuleConcate self, uint32 groupId)
    {
    mOneParamAttributeGet(ConcateGroupGet, groupId, AtConcateGroup, NULL);
    }

/**
 *  Get maximum number of Concate Group
 *
 * @param self this module
 *
 * @return maximum number of Concate Group
 */
uint32 AtModuleConcateMaxGroupGet(AtModuleConcate self)
    {
    mAttributeGet(MaxGroupGet, uint32, 0);
    }

/**
 *  Get ConcateGroup free
 *
 * @param self this module
 *
 * @return Id of ConcateGroup free
 */
uint32 AtModuleConcateFreeGroupGet(AtModuleConcate self)
    {
    mAttributeGet(FreeGroupGet, uint32, 0);
    }

/**
 * Translate error code to string
 *
 * @param ret Error code
 *
 * @return Error code string
 */
const char *AtModuleConcateRet2String(eAtRet ret)
    {
    mRetCodeStr(cAtConcateErrorMemNotProvisioned);
    mRetCodeStr(cAtConcateErrorMemberProvisioned);
    mRetCodeStr(cAtConcateErrorMemberRemoveTimeout);

    return NULL;
    }

/**
 *  Get Deskew Latency Resolution in frames
 *
 * @param self this module
 * @param memberType @ref eAtConcateMemberType "VCG member types"
 *
 * @return number of frames
 */
uint32 AtModuleConcateDeskewLatencyResolutionInFrames(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    mOneParamAttributeGet(DeskewLatencyResolutionInFrames, memberType, uint32, 0);
    }

/**
 *  Set Threshold of Deskew Latency.
 *
 * @param self this module
 * @param memberType @ref eAtConcateMemberType "VCG member types"
 * @param thresholdInRes  Threshold in resolution nxFrames
 *
 * @return AT return code
 */
eAtRet AtModuleConcateDeskewLatencyThresholdSet(AtModuleConcate self, eAtConcateMemberType memberType, uint32 thresholdInRes)
    {
    mTwoParamsAttributeSet(DeskewLatencyThresholdSet, memberType, thresholdInRes);
    }

/**
 *  Get Threshold of Deskew Latency.
 *
 * @param self this module
 * @param memberType @ref eAtConcateMemberType "VCG member types"
 *
 * @return Number threshold.
 */
uint32 AtModuleConcateDeskewLatencyThresholdGet(AtModuleConcate self, eAtConcateMemberType memberType)
    {
    mOneParamAttributeGet(DeskewLatencyThresholdGet, memberType, uint32, 0);
    }

/**
 * @}
 */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : AtConcateGroupDeprecated.c
 *
 * Created Date: May 18, 2016
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtConcateGroupInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * Get the associated GFP channel used to encapsulate ETH Flow traffics
 *
 * @param self This VCG
 * @return The associated GFP channel used to encapsulate ETH Flow traffics or
 *         NULL if it does not have any GFP channel.
 */
AtGfpChannel AtConcateGroupGfpChannelGet(AtConcateGroup self)
    {
    AtEncapChannel encapChannel;

    encapChannel = AtConcateGroupEncapChannelGet(self);
    if (AtEncapChannelEncapTypeGet(encapChannel) == cAtEncapGfp)
        return (AtGfpChannel)encapChannel;

    return NULL;
    }

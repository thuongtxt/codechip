/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtVcgBinder.h
 * 
 * Created Date: Jul 22, 2014
 *
 * Description : VCG binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATVCGBINDER_H_
#define _ATVCGBINDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtModuleConcate.h"
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtVcgBinderSdhVc  *AtVcgBinderSdhVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtConcateMember AtVcgBinderSinkProvision(AtVcgBinder self, AtConcateGroup group);
eAtRet AtVcgBinderSinkDeProvision(AtVcgBinder self, AtConcateGroup group);
AtConcateMember AtVcgBinderSourceProvision(AtVcgBinder self, AtConcateGroup group);
eAtRet AtVcgBinderSourceDeProvision(AtVcgBinder self, AtConcateGroup group);

AtConcateMember AtVcgBinderSinkMemberGet(AtVcgBinder self);
AtConcateMember AtVcgBinderSourceMemberGet(AtVcgBinder self);
AtChannel AtVcgBinderChannelGet(AtVcgBinder self);

/* Helper to access hardware */
uint32 AtVcgBinderRead(AtVcgBinder self, uint32 localAddress, eAtModule module);
void AtVcgBinderWrite(AtVcgBinder self, uint32 localAddress, uint32 value, eAtModule module);

/* SDH VC */
uint16 AtVcgBinderSdhVcNumSts(AtVcgBinderSdhVc self);
uint16 *AtVcgBinderSdhVcAllHwSts(AtVcgBinderSdhVc self);

#endif /* _ATVCGBINDER_H_ */


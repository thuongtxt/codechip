/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : AtVcgBinderPdhDe3.c
 *
 * Created Date: Jun 19, 2019
 *
 * Description : VCG binder for PDH DS3/E3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../AtModuleConcateInternal.h"
#include "AtVcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtVcgBinderMethods      m_AtVcgBinderOverride;

/* Super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtConcateMember MemberObjectCreate(AtVcgBinder self, AtConcateGroup group)
    {
    AtModuleConcate moduleConcate = (AtModuleConcate)AtChannelModuleGet((AtChannel)group);
    return AtModuleConcateCreateConcateMemberForPdhDe3(moduleConcate, (AtPdhDe3)AtVcgBinderChannelGet(self), group);
    }

static void OverrideAtVcgBinder(AtVcgBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgBinderOverride, mMethodsGet(self), sizeof(m_AtVcgBinderOverride));

        mMethodOverride(m_AtVcgBinderOverride, MemberObjectCreate);
        }

    mMethodsSet(self, &m_AtVcgBinderOverride);
    }

static void Override(AtVcgBinder self)
    {
    OverrideAtVcgBinder(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtVcgBinderPdhDe1);
    }

AtVcgBinder AtVcgBinderPdhDe3ObjectInit(AtVcgBinder self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtVcgBinderObjectInit(self, (AtChannel)de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

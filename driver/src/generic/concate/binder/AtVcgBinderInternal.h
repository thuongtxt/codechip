/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtVcgBinderInternal.h
 * 
 * Created Date: Jul 22, 2014
 *
 * Description : VCG binder
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATVCGBINDERINTERNAL_H_
#define _ATVCGBINDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannelClasses.h"
#include "../../man/AtDriverInternal.h"
#include "AtVcgBinder.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtVcgBinderMethods
    {
    AtConcateMember (*SinkProvision)(AtVcgBinder self, AtConcateGroup group);
    eAtRet (*SinkDeProvision)(AtVcgBinder self, AtConcateGroup group);
    AtConcateMember (*SourceProvision)(AtVcgBinder self, AtConcateGroup group);
    eAtRet (*SourceDeProvision)(AtVcgBinder self, AtConcateGroup group);

    /* Internal */
    AtConcateMember (*MemberObjectCreate)(AtVcgBinder self, AtConcateGroup group);
    }tAtVcgBinderMethods;

typedef struct tAtVcgBinder
    {
    tAtObject super;
    const tAtVcgBinderMethods *methods;

    /* Private data */
    AtChannel channel;
    AtConcateMember sinkVcgMember;
    AtConcateMember sourceVcgMember;
    }tAtVcgBinder;

typedef struct tAtVcgBinderSdhVc
    {
    tAtVcgBinder super;

    /* Save hardware STSs to eliminate the repetition of STS conversion. */
    uint16 *allHwSts;
    }tAtVcgBinderSdhVc;

typedef struct tAtVcgBinderPdhDe1
    {
    tAtVcgBinder super;
    }tAtVcgBinderPdhDe1;

typedef struct tAtVcgBinderPdhDe3
    {
    tAtVcgBinder super;
    }tAtVcgBinderPdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtVcgBinder AtVcgBinderObjectInit(AtVcgBinder self, AtChannel channel);
AtVcgBinder AtVcgBinderSdhVcObjectInit(AtVcgBinder self, AtSdhVc vc);
AtVcgBinder AtVcgBinderPdhDe1ObjectInit(AtVcgBinder self, AtPdhDe1 de1);
AtVcgBinder AtVcgBinderPdhDe3ObjectInit(AtVcgBinder self, AtPdhDe3 de3);

#endif /* _ATVCGBINDERINTERNAL_H_ */


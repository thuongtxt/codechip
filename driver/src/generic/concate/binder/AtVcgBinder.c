/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : AtVcgBinder.c
 *
 * Created Date: Jul 22, 2014
 *
 * Description : VCG binder
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCommon.h"
#include "../../../util/coder/AtCoderUtil.h"
#include "../AtConcateMemberInternal.h"
#include "AtVcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((AtVcgBinder)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tAtVcgBinderMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtConcateMember MemberObjectCreate(AtVcgBinder self, AtConcateGroup group)
    {
    /* Sub class must know */
    AtUnused(self);
    AtUnused(group);
    return NULL;
    }

static AtConcateMember SinkProvision(AtVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret;
    AtConcateMember member;

    /* Already provisioned */
    if (self->sinkVcgMember)
        return NULL;

    if (self->sourceVcgMember)
        member = self->sourceVcgMember;
    else
        member = mMethodsGet(self)->MemberObjectCreate(self, group);

    if (member == NULL)
        return NULL;

    /* Create and initialize this member */
    self->sinkVcgMember = member;
    AtConcateMemberSinkChannelSet(member, AtVcgBinderChannelGet(self));
    ret = AtConcateMemberSinkInit(member);
    if (ret != cAtOk)
        {
        AtChannelLog(AtVcgBinderChannelGet(self), cAtLogLevelCritical, AtSourceLocation,
                    "Cannot initialize sink member, ret = %s\r\n", AtRet2String(ret));

        AtConcateMemberSinkChannelSet(member, NULL);
        self->sinkVcgMember = NULL;

        if (self->sourceVcgMember == NULL)
            AtObjectDelete((AtObject)member);

        return NULL;
        }

    ret  = AtChannelBindToSinkGroup(AtVcgBinderChannelGet(self), group);
    ret |= AtChannelRxEnable((AtChannel)member, cAtTrue);
    if (ret != cAtOk)
        {
        AtChannelLog(AtVcgBinderChannelGet(self), cAtLogLevelCritical, AtSourceLocation,
                    "Cannot bind to sink group %d, ret = %s\r\n",
                    AtChannelIdGet((AtChannel)group), AtRet2String(ret));

        AtVcgBinderSinkDeProvision(self, group);
        return NULL;
        }

    return self->sinkVcgMember;
    }

static eAtRet SinkDeProvision(AtVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret;
    AtConcateMember member = self->sinkVcgMember;
    AtUnused(group);

    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    ret  = AtChannelRxEnable((AtChannel)member, cAtFalse);
    ret |= AtChannelBindToSinkGroup(AtVcgBinderChannelGet(self), NULL);
    ret |= AtConcateMemberSinkHardwareCleanup(member);

    AtConcateMemberSinkChannelSet(member, NULL);
    self->sinkVcgMember = NULL;

    if (self->sourceVcgMember == NULL)
        AtObjectDelete((AtObject)member);

    return ret;
    }

static AtConcateMember SourceProvision(AtVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret;
    AtConcateMember member;

    /* Already provisioned */
    if (self->sourceVcgMember)
        return NULL;

    /* Create member if it has not been created */
    if (self->sinkVcgMember)
        member = self->sinkVcgMember;
    else
        member = mMethodsGet(self)->MemberObjectCreate(self, group);

    if (member == NULL)
        return NULL;

    /* Create and initialize this member */
    self->sourceVcgMember = member;
    AtConcateMemberSourceChannelSet(member, AtVcgBinderChannelGet(self));
    ret = AtConcateMemberSourceInit(member);
    if (ret != cAtOk)
        {
        AtChannelLog(AtVcgBinderChannelGet(self), cAtLogLevelCritical, AtSourceLocation,
                    "Cannot initialize source member, ret = %s\r\n", AtRet2String(ret));

        AtConcateMemberSourceChannelSet(member, NULL);
        self->sourceVcgMember = NULL;

        if (self->sinkVcgMember == NULL)
            AtObjectDelete((AtObject)member);

        return NULL;
        }

    ret  = AtChannelBindToSourceGroup(AtVcgBinderChannelGet(self), group);
    ret |= AtChannelTxEnable((AtChannel)member, cAtTrue);
    if (ret != cAtOk)
        {
        AtChannelLog(AtVcgBinderChannelGet(self), cAtLogLevelCritical, AtSourceLocation,
                    "Cannot bind to source group %d, ret = %s\r\n",
                    AtChannelIdGet((AtChannel)group), AtRet2String(ret));

        AtVcgBinderSourceDeProvision(self, group);
        return NULL;
        }

    return self->sourceVcgMember;
    }

static eAtRet SourceDeProvision(AtVcgBinder self, AtConcateGroup group)
    {
    eAtRet ret;
    AtConcateMember member = self->sourceVcgMember;
    AtUnused(group);

    if (member == NULL)
        return cAtConcateErrorMemNotProvisioned;

    ret  = AtChannelTxEnable((AtChannel)member, cAtFalse);
    ret |= AtChannelBindToSourceGroup(AtVcgBinderChannelGet(self), NULL);
    ret |= AtConcateMemberSourceHardwareCleanup(member);

    AtConcateMemberSourceChannelSet(member, NULL);
    self->sourceVcgMember = NULL;

    if (self->sinkVcgMember == NULL)
        AtObjectDelete((AtObject)member);

    return cAtOk;
    }

static void MethodsInit(AtVcgBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SinkProvision);
        mMethodOverride(m_methods, SinkDeProvision);
        mMethodOverride(m_methods, SourceProvision);
        mMethodOverride(m_methods, SourceDeProvision);
        mMethodOverride(m_methods, MemberObjectCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtVcgBinder object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(channel);
    mEncodeObject(sinkVcgMember);
    mEncodeObject(sourceVcgMember);
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtObject channel = (AtObject)AtVcgBinderChannelGet(mThis(self));

    AtSnprintf(description, sizeof(description), "%s.vcg_binder", AtObjectToString(channel));
    return description;
    }

static void OverrideAtObject(AtVcgBinder self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtVcgBinder self)
    {
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtVcgBinder);
    }

AtVcgBinder AtVcgBinderObjectInit(AtVcgBinder self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->channel = channel;

    return self;
    }

AtConcateMember AtVcgBinderSinkProvision(AtVcgBinder self, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->SinkProvision(self, group);
    return NULL;
    }

eAtRet AtVcgBinderSinkDeProvision(AtVcgBinder self, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->SinkDeProvision(self, group);
    return cAtErrorObjectNotExist;
    }

AtConcateMember AtVcgBinderSourceProvision(AtVcgBinder self, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->SourceProvision(self, group);
    return NULL;
    }

eAtRet AtVcgBinderSourceDeProvision(AtVcgBinder self, AtConcateGroup group)
    {
    if (self)
        return mMethodsGet(self)->SourceDeProvision(self, group);
    return cAtErrorObjectNotExist;
    }

AtConcateMember AtVcgBinderSinkMemberGet(AtVcgBinder self)
    {
    return self ? self->sinkVcgMember : NULL;
    }

AtConcateMember AtVcgBinderSourceMemberGet(AtVcgBinder self)
    {
    return self ? self->sourceVcgMember : NULL;
    }

AtChannel AtVcgBinderChannelGet(AtVcgBinder self)
    {
    return self ? self->channel : NULL;
    }

uint32 AtVcgBinderRead(AtVcgBinder self, uint32 localAddress, eAtModule module)
    {
    return mChannelHwRead(AtVcgBinderChannelGet(self), localAddress, module);
    }

void AtVcgBinderWrite(AtVcgBinder self, uint32 localAddress, uint32 value, eAtModule module)
    {
    mChannelHwWrite(AtVcgBinderChannelGet(self), localAddress, value, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Concate
 *
 * File        : AtVcgBinderSdhVc.c
 *
 * Created Date: Jun 19, 2019
 *
 * Description : VCG binder for SDH VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../util/coder/AtCoderUtil.h"
#include "../AtModuleConcateInternal.h"
#include "AtVcgBinderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtVcgBinderMethods      m_AtVcgBinderOverride;

/* Super implementation */
static const tAtObjectMethods  *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtConcateMember MemberObjectCreate(AtVcgBinder self, AtConcateGroup group)
    {
    AtModuleConcate moduleConcate = (AtModuleConcate)AtChannelModuleGet((AtChannel)group);
    return AtModuleConcateCreateConcateMemberForSdhVc(moduleConcate, (AtSdhVc)AtVcgBinderChannelGet(self), group);
    }

static uint16 *AllHwSts(AtVcgBinderSdhVc self)
    {
    uint16 numSts = AtVcgBinderSdhVcNumSts(self);
    uint16 *hwSts;
    uint8 sts_i;

    if (self->allHwSts)
        return self->allHwSts;

    /* There may be not enough memory */
    hwSts = AtOsalMemAlloc(numSts * sizeof(uint16));
    if (hwSts == NULL)
        return NULL;

    /* Initialize */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        hwSts[sts_i] = cBit15_0;

    self->allHwSts = hwSts;
    return self->allHwSts;
    }

static void Delete(AtObject self)
    {
    AtVcgBinderSdhVc binder = (AtVcgBinderSdhVc)self;

    if (binder->allHwSts)
        {
        AtOsalMemFree(binder->allHwSts);
        binder->allHwSts = NULL;
        }

    /* Super deletion */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    AtVcgBinderSdhVc object = (AtVcgBinderSdhVc)self;

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeUInt16Array(encoder, object->allHwSts, AtVcgBinderSdhVcNumSts(object), "allHwSts");
    }

static void OverrideAtVcgBinder(AtVcgBinder self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtVcgBinderOverride, mMethodsGet(self), sizeof(m_AtVcgBinderOverride));

        mMethodOverride(m_AtVcgBinderOverride, MemberObjectCreate);
        }

    mMethodsSet(self, &m_AtVcgBinderOverride);
    }

static void OverrideAtObject(AtVcgBinder self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtVcgBinder self)
    {
    OverrideAtVcgBinder(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tAtVcgBinderSdhVc);
    }

AtVcgBinder AtVcgBinderSdhVcObjectInit(AtVcgBinder self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtVcgBinderObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

uint16 AtVcgBinderSdhVcNumSts(AtVcgBinderSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtVcgBinderChannelGet((AtVcgBinder)self);
    return AtSdhChannelNumSts(channel);
    }

uint16 *AtVcgBinderSdhVcAllHwSts(AtVcgBinderSdhVc self)
    {
    if (self)
        return AllHwSts(self);
    return NULL;
    }
